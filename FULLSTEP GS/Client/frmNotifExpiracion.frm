VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmNotifExpiracion 
   BackColor       =   &H00808000&
   Caption         =   "Notificaciones de expiraci�n"
   ClientHeight    =   3690
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   11730
   Icon            =   "frmNotifExpiracion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3690
   ScaleWidth      =   11730
   StartUpPosition =   2  'CenterScreen
   Begin SSDataWidgets_B.SSDBGrid sdbgNotifExpiracion 
      Height          =   1455
      Left            =   840
      TabIndex        =   4
      Top             =   345
      Width           =   16260
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   14
      DividerType     =   2
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   14
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID_GRUPO"
      Columns(0).Name =   "ID_GRUPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "GRUPO"
      Columns(1).Name =   "GRUPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   3
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID_CAMPO"
      Columns(2).Name =   "ID_CAMPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "CAMPO"
      Columns(3).Name =   "CAMPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   3
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "ID_SUBCAMPO"
      Columns(4).Name =   "ID_SUBCAMPO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "SUBCAMPO"
      Columns(5).Name =   "SUBCAMPO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "CMB_ANTELACION"
      Columns(6).Name =   "CMB_ANTELACION"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "CMB_DURACION"
      Columns(7).Name =   "CMB_DURACION"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "NOTIFICADOS"
      Columns(8).Name =   "NOTIFICADOS"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Style=   4
      Columns(8).ButtonsAlways=   -1  'True
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ID_AVISO_EXPIRACION_CAMPOS"
      Columns(9).Name =   "ID_AVISO_EXPIRACION_CAMPOS"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ID_AVISO_EXPIRACION"
      Columns(10).Name=   "ID_AVISO_EXPIRACION"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "CAMPO_ES_DESGLOSE"
      Columns(11).Name=   "CAMPO_ES_DESGLOSE"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   11
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "PLAZO_ANTELACION"
      Columns(12).Name=   "PLAZO_ANTELACION"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "PLAZO_DURACION"
      Columns(13).Name=   "PLAZO_DURACION"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      _ExtentX        =   28681
      _ExtentY        =   2566
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   5160
      TabIndex        =   3
      Top             =   3120
      Width           =   1050
   End
   Begin VB.CommandButton cmdEliminarNotificacion 
      Appearance      =   0  'Flat
      Height          =   312
      Left            =   360
      Picture         =   "frmNotifExpiracion.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   720
      Width           =   432
   End
   Begin VB.CommandButton cmdAnyadirNotificacion 
      Appearance      =   0  'Flat
      Height          =   312
      Left            =   360
      Picture         =   "frmNotifExpiracion.frx":01DC
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   360
      Width           =   432
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddGrupo 
      Height          =   555
      Left            =   6000
      TabIndex        =   5
      Top             =   2040
      Width           =   3750
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6615
      Columns(1).Caption=   "DESTINO"
      Columns(1).Name =   "DESTINO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6615
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddCampo 
      Height          =   555
      Left            =   3120
      TabIndex        =   6
      Top             =   1440
      Width           =   1575
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6615
      Columns(1).Caption=   "DESTINO"
      Columns(1).Name =   "DESTINO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ES_DESGLOSE"
      Columns(2).Name =   "ES_DESGLOSE"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   11
      Columns(2).FieldLen=   256
      _ExtentX        =   2778
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddSubcampo 
      Height          =   555
      Left            =   5040
      TabIndex        =   7
      Top             =   1560
      Width           =   2055
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6615
      Columns(1).Caption=   "DESTINO"
      Columns(1).Name =   "DESTINO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3625
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddPlazoAntes 
      Height          =   555
      Left            =   7320
      TabIndex        =   8
      Top             =   2640
      Width           =   3750
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6615
      Columns(1).Caption=   "DESTINO"
      Columns(1).Name =   "DESTINO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6615
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddPlazoDuracion 
      Height          =   555
      Left            =   7800
      TabIndex        =   9
      Top             =   1920
      Width           =   3750
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6615
      Columns(1).Caption=   "DESTINO"
      Columns(1).Name =   "DESTINO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6615
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin VB.Label lblDef 
      BackColor       =   &H00808000&
      Caption         =   "Por cada notificaci�n de expiraci�n seleccione el campo de tipo fecha, el plazo de expiraci�n, los notificados y el asunto:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   60
      Width           =   9000
   End
End
Attribute VB_Name = "frmNotifExpiracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_sIdioma(1 To 12) As String
Public lIdFlujo As Long
Public m_lIdFormulario As Long
Private m_oFormulario As CFormulario

Private m_bCambios As Boolean
Private m_bCambiosElim As Boolean

Private m_bElimUltima As Boolean

Private Sub cmdAnyadirNotificacion_Click()
    sdbgNotifExpiracion.Scroll 0, sdbgNotifExpiracion.Rows - sdbgNotifExpiracion.Row
    
    If sdbgNotifExpiracion.VisibleRows > 0 Then
        
        If sdbgNotifExpiracion.VisibleRows >= sdbgNotifExpiracion.Rows Then
            If sdbgNotifExpiracion.VisibleRows = sdbgNotifExpiracion.Rows Then
                sdbgNotifExpiracion.Row = sdbgNotifExpiracion.Rows - 1
            Else
                sdbgNotifExpiracion.Row = sdbgNotifExpiracion.Rows
            End If
        Else
            sdbgNotifExpiracion.Row = sdbgNotifExpiracion.Rows - (sdbgNotifExpiracion.Rows - sdbgNotifExpiracion.VisibleRows) - 1
        End If
    End If
    
    If Me.Visible Then sdbgNotifExpiracion.SetFocus
End Sub

Private Function GuardarCambios() As Boolean
    Dim oWorkflow As cworkflow
    Dim iae As String
    Dim iaec As String
    Dim lPlazoAntes As Long, lPlazoDuracion As Long
    Dim b As Boolean
    Set oWorkflow = oFSGSRaiz.Generar_CWorkflow
    LockWindowUpdate Me.hWnd
    b = False
    With sdbgNotifExpiracion
        If .Rows > 0 Then
            If m_bCambios Or .DataChanged Then
                .MoveFirst
                Dim i As Integer
                For i = 0 To .Rows - 1
                    If .Columns("ID_GRUPO").Value <> "" And .Columns("ID_CAMPO").Value <> "" Then 'Si me ha seleccionado algo en la fila, si no, no se guarda
                        lPlazoAntes = 1
                        lPlazoDuracion = 7
                        If .Columns("PLAZO_ANTELACION").Value <> "" Then lPlazoAntes = CLng(.Columns("PLAZO_ANTELACION").Value)
                        If .Columns("PLAZO_DURACION").Value <> "" Then lPlazoDuracion = CLng(.Columns("PLAZO_DURACION").Value)
                        
                        If .Columns("ID_AVISO_EXPIRACION_CAMPOS").Value = "" Then
                            If Not (.Columns("ID_SUBCAMPO").Value = "") Then
                                iaec = oWorkflow.Cargar_Aviso_Expiracion_Campos(CLng(lIdFlujo), CLng(.Columns("ID_GRUPO").Value), CLng(.Columns("ID_CAMPO").Value), CLng(.Columns("ID_SUBCAMPO").Value), lPlazoAntes, lPlazoDuracion)
                            Else
                                iaec = oWorkflow.Cargar_Aviso_Expiracion_Campos(CLng(lIdFlujo), CLng(.Columns("ID_GRUPO").Value), CLng(.Columns("ID_CAMPO").Value), , lPlazoAntes, lPlazoDuracion)
                            End If
                            .Columns("ID_AVISO_EXPIRACION_CAMPOS").Value = iaec
                        Else
                            If .Columns("ID_CAMPO").Value = "" And .Columns("ID_SUBCAMPO").Value = "" Then
                                oWorkflow.ModificarWorkflowNotificacion_Campos .Columns("ID_AVISO_EXPIRACION_CAMPOS").Value, .Columns("ID_GRUPO").Value, 0, 0, lPlazoAntes, lPlazoDuracion
                            ElseIf .Columns("ID_SUBCAMPO").Value = "" Then
                                oWorkflow.ModificarWorkflowNotificacion_Campos .Columns("ID_AVISO_EXPIRACION_CAMPOS").Value, .Columns("ID_GRUPO").Value, .Columns("ID_CAMPO").Value, 0, lPlazoAntes, lPlazoDuracion
                            Else
                                oWorkflow.ModificarWorkflowNotificacion_Campos .Columns("ID_AVISO_EXPIRACION_CAMPOS").Value, .Columns("ID_GRUPO").Value, .Columns("ID_CAMPO").Value, .Columns("ID_SUBCAMPO").Value, lPlazoAntes, lPlazoDuracion
                            End If
                        End If
                        b = True
                    End If
                                            
                    .MoveNext
                Next
            End If
        End If
    End With
    If b Or m_bCambiosElim Then
        frmFlujos.HayCambios
    End If
    LockWindowUpdate 0&
    GuardarCambios = True
End Function

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub cmdEliminarNotificacion_Click()
    If sdbgNotifExpiracion.IsAddRow Then
        If sdbgNotifExpiracion.DataChanged Then
            sdbgNotifExpiracion.CancelUpdate
        End If
    Else
        If sdbgNotifExpiracion.Rows = 0 Then Exit Sub
        
        If sdbgNotifExpiracion.DataChanged Then
            sdbgNotifExpiracion.CancelUpdate
        End If
        
        Dim oWorkflow As cworkflow
    
        Set oWorkflow = oFSGSRaiz.Generar_CWorkflow
        
        If sdbgNotifExpiracion.Columns("ID_AVISO_EXPIRACION_CAMPOS").Value <> "" Then
            oWorkflow.Eliminar_Aviso_Expiracion CLng(sdbgNotifExpiracion.Columns("ID_AVISO_EXPIRACION_CAMPOS").Value)
        End If
        
        m_bElimUltima = False
        If Me.sdbgNotifExpiracion.SelBookmarks.Count = 0 Then
            If Me.sdbgNotifExpiracion.Rows = (sdbgNotifExpiracion.AddItemRowIndex(sdbgNotifExpiracion.Bookmark) + 1) Then
                m_bElimUltima = True
            End If
        End If
        
        If sdbgNotifExpiracion.AddItemRowIndex(sdbgNotifExpiracion.Bookmark) > -1 Then
            sdbgNotifExpiracion.RemoveItem (sdbgNotifExpiracion.AddItemRowIndex(sdbgNotifExpiracion.Bookmark))
        Else
            sdbgNotifExpiracion.RemoveItem (0)
        End If
        
        m_bCambiosElim = True
        
    End If
End Sub

Private Sub Form_Load()
    PonerFieldSeparator Me
    m_bCambios = False
    m_bCambiosElim = False
    m_bElimUltima = False
    CargarRecursos
    ConfigurarSeguridad
    CargarGridNotificacion
End Sub

Private Sub CargarGridNotificacion()
    Dim oWorkflow As cworkflow
    Dim oNotificados As CNotificadosAccion
    Dim textoNotificados As String
    
    Dim textoAntelacion As String
    Dim textoDuracion As String
    Dim IdAntelacion As Integer
    Dim IdDuracion As Integer
    
    Set oWorkflow = oFSGSRaiz.Generar_CWorkflow
    
    Dim rs As ADODB.Recordset
    
    Set rs = oWorkflow.ObtenerWorkflowNotificacion(basPublic.gParametrosInstalacion.gIdioma, lIdFlujo)
    
    'Valores por defecto
    If rs.EOF Then
    Else
        While Not rs.EOF
            Set oNotificados = oFSGSRaiz.Generar_CNotificadosAccion
            oNotificados.CargarNotificados OrigenNotificadoAccion.Expiracion, rs("AVISO_EXPIRACION_CAMPOS").Value
            If oNotificados.Count > 0 Then
                textoNotificados = "..."
            Else
                textoNotificados = " "
            End If
        
            If IsNull(rs("PLAZO_ANTELACION").Value) Then
                textoAntelacion = m_sIdioma(1)
                IdAntelacion = 1
                
                m_bCambios = True
            Else
                Select Case CInt(rs("PLAZO_ANTELACION").Value)
                    Case 1
                        textoAntelacion = m_sIdioma(1)
                    Case 2, 3, 4, 5, 6
                        textoAntelacion = rs("PLAZO_ANTELACION").Value & " " & m_sIdioma(2)
                    Case 7
                        textoAntelacion = "1 " & m_sIdioma(3)
                    Case 14, 21
                        textoAntelacion = (rs("PLAZO_ANTELACION").Value / 7) & " " & m_sIdioma(4)
                    Case 30
                        textoAntelacion = "1 " & m_sIdioma(5)
                    Case 60, 90, 120, 150, 180
                        textoAntelacion = (rs("PLAZO_ANTELACION").Value / 30) & " " & m_sIdioma(6)
                End Select
                IdAntelacion = CInt(rs("PLAZO_ANTELACION").Value)
            End If
        
            If IsNull(rs("PLAZO_DURACION").Value) Then
                textoDuracion = "1 " & m_sIdioma(9)
                IdDuracion = 7
                
                m_bCambios = True
            Else
                Select Case CInt(rs("PLAZO_DURACION").Value)
                    Case 1
                        textoDuracion = "1 " & m_sIdioma(7)
                    Case 2, 3, 4, 5, 6
                        textoDuracion = rs("PLAZO_DURACION").Value & " " & m_sIdioma(8)
                    Case 7
                        textoDuracion = "1 " & m_sIdioma(9)
                    Case 14, 21
                        textoDuracion = (rs("PLAZO_DURACION").Value / 7) & " " & m_sIdioma(10)
                    Case 30
                        textoDuracion = "1 " & m_sIdioma(11)
                    Case 60, 90, 120, 150, 180
                        textoDuracion = (rs("PLAZO_DURACION").Value / 30) & " " & m_sIdioma(12)
                End Select
                IdDuracion = CInt(rs("PLAZO_DURACION").Value)
            End If
        
            If Not IsNull(rs("FORMSUBCAMPO_DEN").Value) Then
                sdbgNotifExpiracion.AddItem rs("GRUPO").Value & Chr(m_lSeparador) & rs("FORMGRUPO_DEN").Value & Chr(m_lSeparador) & rs("CAMPO").Value & Chr(m_lSeparador) & rs("FORMCAMPO_DEN").Value & Chr(m_lSeparador) & rs("SUBCAMPO").Value & Chr(m_lSeparador) & rs("FORMSUBCAMPO_DEN").Value & Chr(m_lSeparador) & textoAntelacion & Chr(m_lSeparador) & textoDuracion & Chr(m_lSeparador) & textoNotificados & Chr(m_lSeparador) & rs("AVISO_EXPIRACION_CAMPOS").Value & Chr(m_lSeparador) & rs("AVISO_EXPIRACION_CAMPOS").Value & Chr(m_lSeparador) & rs("CAMPO_ES_DESGLOSE").Value & Chr(m_lSeparador) & IdAntelacion & Chr(m_lSeparador) & IdDuracion
            Else
                sdbgNotifExpiracion.AddItem rs("GRUPO").Value & Chr(m_lSeparador) & rs("FORMGRUPO_DEN").Value & Chr(m_lSeparador) & rs("CAMPO").Value & Chr(m_lSeparador) & rs("FORMCAMPO_DEN").Value & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & textoAntelacion & Chr(m_lSeparador) & textoDuracion & Chr(m_lSeparador) & textoNotificados & Chr(m_lSeparador) & rs("AVISO_EXPIRACION_CAMPOS").Value & Chr(m_lSeparador) & rs("AVISO_EXPIRACION_CAMPOS").Value & Chr(m_lSeparador) & rs("CAMPO_ES_DESGLOSE").Value & Chr(m_lSeparador) & IdAntelacion & Chr(m_lSeparador) & IdDuracion
            End If
            
            If sdbgNotifExpiracion.DataChanged Then sdbgNotifExpiracion.Update
            
            rs.MoveNext
        Wend
    End If

    rs.Close
    Set rs = Nothing
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        If Me.Height < 4280 Then Me.Height = 4280
        If Me.Width < 9800 Then Me.Width = 9800
                
        With sdbgNotifExpiracion
            .Width = Me.ScaleWidth - sdbgNotifExpiracion.Left - 150
            
            Dim dColumnsWidth As Double
            dColumnsWidth = .Width - .Columns("NOTIFICADOS").Width - 300
            .Columns("GRUPO").Width = dColumnsWidth * 0.2
            .Columns("CAMPO").Width = dColumnsWidth * 0.2
            .Columns("SUBCAMPO").Width = dColumnsWidth * 0.2
            .Columns("CMB_ANTELACION").Width = dColumnsWidth * 0.18
            .Columns("CMB_DURACION").Width = dColumnsWidth * 0.18
        End With
                                
        cmdCerrar.Left = (Me.ScaleWidth / 2) - (cmdCerrar.Width / 2)
        cmdCerrar.Top = Me.ScaleHeight - cmdCerrar.Height - 150
         
        sdbgNotifExpiracion.Height = Me.cmdCerrar.Top - 545
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Cancel = Not GuardarCambios
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    On Error Resume Next
      
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_NOTIF_EXPIRACION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        For i = 1 To 12
            m_sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        
        lblDef.caption = Ador(0).Value
        Ador.MoveNext
        sdbgNotifExpiracion.Columns("CMB_ANTELACION").caption = Ador(0).Value
        Ador.MoveNext
        sdbgNotifExpiracion.Columns("CMB_DURACION").caption = Ador(0).Value
        Ador.MoveNext
        cmdCerrar.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sdbgNotifExpiracion.Columns("GRUPO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgNotifExpiracion.Columns("CAMPO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgNotifExpiracion.Columns("SUBCAMPO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgNotifExpiracion.Columns("NOTIFICADOS").caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyadirNotificacion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarNotificacion.ToolTipText = Ador(0).Value


        Ador.Close
    End If
    Set Ador = Nothing
End Sub

Private Sub ConfigurarSeguridad()
    sdbddGrupo.AddItem 0 & Chr(m_lSeparador) & ""
    sdbgNotifExpiracion.Columns("GRUPO").DropDownHwnd = sdbddGrupo.hWnd
    sdbddCampo.AddItem 0 & Chr(m_lSeparador) & ""
    sdbgNotifExpiracion.Columns("CAMPO").DropDownHwnd = sdbddCampo.hWnd
    sdbddSubcampo.AddItem 0 & Chr(m_lSeparador) & ""
    sdbgNotifExpiracion.Columns("SUBCAMPO").DropDownHwnd = sdbddSubcampo.hWnd
    
    sdbddPlazoAntes.AddItem 0 & Chr(m_lSeparador) & ""
    sdbgNotifExpiracion.Columns("CMB_ANTELACION").DropDownHwnd = sdbddPlazoAntes.hWnd
    sdbddPlazoDuracion.AddItem 0 & Chr(m_lSeparador) & ""
    sdbgNotifExpiracion.Columns("CMB_DURACION").DropDownHwnd = sdbddPlazoDuracion.hWnd
End Sub

Private Sub sdbddGrupo_DropDown()
    Dim oSolicitud As CSolicitud
    Dim rs As ADODB.Recordset
    
    Screen.MousePointer = vbHourglass
    sdbddGrupo.RemoveAll
    
    Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
    Set rs = oSolicitud.ObtenerGruposNotificacion(basPublic.gParametrosInstalacion.gIdioma, lIdFlujo)

    While Not rs.EOF
        If Not IsNull(rs("FORMGRUPO_ID").Value) Then
            sdbddGrupo.AddItem rs("FORMGRUPO_ID").Value & Chr(m_lSeparador) & rs("FORMGRUPO_DEN").Value
        End If

        rs.MoveNext
    Wend

    rs.Close
    Set rs = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddCampo_DropDown()
    Dim oSolicitud As CSolicitud
    Dim rs As ADODB.Recordset
    
    Screen.MousePointer = vbHourglass
    sdbddCampo.RemoveAll
    
    If sdbgNotifExpiracion.Columns("ID_GRUPO").Value <> "" Then
        Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
        Set rs = oSolicitud.ObtenerCamposNotificacion(basPublic.gParametrosInstalacion.gIdioma, lIdFlujo, sdbgNotifExpiracion.Columns("ID_GRUPO").Value)
        
        Dim camposPadre As Dictionary
        Set camposPadre = New Dictionary
        
        While Not rs.EOF
            Dim strPadreId As String
            strPadreId = NullToStr(rs.Fields("CAMPO_PADRE").Value)
            
            If (strPadreId = "") Then
                sdbddCampo.AddItem rs("FORMCAMPO_ID").Value & Chr(m_lSeparador) & rs("FORMCAMPO_DEN").Value & Chr(m_lSeparador) & CStr(False)
            Else
                If Not (camposPadre.Exists(rs.Fields("CAMPO_PADRE").Value)) Then
                    sdbddCampo.AddItem rs("CAMPO_PADRE").Value & Chr(m_lSeparador) & rs("PADRE_DEN").Value & Chr(m_lSeparador) & CStr(True)
                    camposPadre.Add rs.Fields("CAMPO_PADRE").Value, rs.Fields("PADRE_DEN").Value
                End If
            End If
            
            rs.MoveNext
        Wend
    
        rs.Close
        Set rs = Nothing
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddPlazoAntes_CloseUp()
    With sdbgNotifExpiracion
        If .Columns("CMB_ANTELACION").Value = "" Or val(.Columns("PLAZO_ANTELACION").Value) <> sdbddPlazoAntes.Columns("ID").Value Then
            .Columns("PLAZO_ANTELACION").Value = ""
            .Columns("CMB_ANTELACION").Value = ""
            
            If sdbddPlazoAntes.Columns("ID").Value = "" Then Exit Sub
        End If
            
        .Columns("PLAZO_ANTELACION").Value = sdbddPlazoAntes.Columns("ID").Value
        .Columns("CMB_ANTELACION").Value = sdbddPlazoAntes.Columns("DESTINO").Value
    End With
End Sub

Private Sub sdbddPlazoAntes_DropDown()
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    With sdbddPlazoAntes
        .RemoveAll
        
        .AddItem 1 & Chr(m_lSeparador) & m_sIdioma(1)
        For i = 2 To 6
            .AddItem i & Chr(m_lSeparador) & i & " " & m_sIdioma(2)
        Next
        .AddItem 7 & Chr(m_lSeparador) & "1 " & m_sIdioma(3)
        For i = 2 To 3
            .AddItem i * 7 & Chr(m_lSeparador) & i & " " & m_sIdioma(4)
        Next
        .AddItem 30 & Chr(m_lSeparador) & "1 " & m_sIdioma(5)
        For i = 2 To 6
            .AddItem i * 30 & Chr(m_lSeparador) & i & " " & m_sIdioma(6)
        Next
            
    End With
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddPlazoAntes_InitColumnProps()
    sdbddPlazoAntes.DataFieldList = "Column 0"
    sdbddPlazoAntes.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddPlazoAntes_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddPlazoAntes.MoveFirst

    If sdbgNotifExpiracion.Columns("CMB_ANTELACION").Value <> "" Then
        For i = 0 To sdbddPlazoAntes.Rows - 1
            bm = sdbddPlazoAntes.GetBookmark(i)
            If UCase(sdbgNotifExpiracion.Columns("CMB_ANTELACION").Value) = UCase(Mid(sdbddPlazoAntes.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("CMB_ANTELACION").Value))) Then
                sdbgNotifExpiracion.Columns("CMB_ANTELACION").Value = Mid(sdbddPlazoAntes.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("CMB_ANTELACION").Value))
                sdbddPlazoAntes.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbddPlazoDuracion_CloseUp()
    With sdbgNotifExpiracion
        If .Columns("CMB_DURACION").Value = "" Or val(.Columns("PLAZO_DURACION").Value) <> sdbddPlazoDuracion.Columns("ID").Value Then
            .Columns("PLAZO_DURACION").Value = ""
            .Columns("CMB_DURACION").Value = ""
            
            If sdbddPlazoDuracion.Columns("ID").Value = "" Then Exit Sub
        End If
            
        .Columns("PLAZO_DURACION").Value = sdbddPlazoDuracion.Columns("ID").Value
        .Columns("CMB_DURACION").Value = sdbddPlazoDuracion.Columns("DESTINO").Value
    End With
End Sub

Private Sub sdbddPlazoDuracion_DropDown()
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    sdbddPlazoDuracion.RemoveAll
    
    sdbddPlazoDuracion.AddItem 1 & Chr(m_lSeparador) & "1 " & m_sIdioma(7)
    For i = 2 To 6
        sdbddPlazoDuracion.AddItem i & Chr(m_lSeparador) & i & " " & m_sIdioma(8)
    Next
    sdbddPlazoDuracion.AddItem 7 & Chr(m_lSeparador) & "1 " & m_sIdioma(9)
    For i = 2 To 3
        sdbddPlazoDuracion.AddItem i * 7 & Chr(m_lSeparador) & i & " " & m_sIdioma(10)
    Next
    sdbddPlazoDuracion.AddItem 30 & Chr(m_lSeparador) & "1 " & m_sIdioma(11)
    For i = 2 To 6
        sdbddPlazoDuracion.AddItem i * 30 & Chr(m_lSeparador) & i & " " & m_sIdioma(12)
    Next
        
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbddPlazoDuracion_InitColumnProps()
    sdbddPlazoDuracion.DataFieldList = "Column 0"
    sdbddPlazoDuracion.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddPlazoDuracion_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddPlazoDuracion.MoveFirst

    If sdbgNotifExpiracion.Columns("CMB_DURACION").Value <> "" Then
        For i = 0 To sdbddPlazoDuracion.Rows - 1
            bm = sdbddPlazoDuracion.GetBookmark(i)
            If UCase(sdbgNotifExpiracion.Columns("CMB_DURACION").Value) = UCase(Mid(sdbddPlazoDuracion.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("CMB_DURACION").Value))) Then
                sdbgNotifExpiracion.Columns("CMB_DURACION").Value = Mid(sdbddPlazoDuracion.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("CMB_DURACION").Value))
                sdbddPlazoDuracion.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbddSubcampo_DropDown()
    Dim oSolicitud As CSolicitud
    Dim rs As ADODB.Recordset
    Dim insertados As Boolean
    
    Screen.MousePointer = vbHourglass
    sdbddSubcampo.RemoveAll
    
    insertados = False
    
    If sdbgNotifExpiracion.Columns("ID_GRUPO").Value <> "" And sdbgNotifExpiracion.Columns("ID_CAMPO").Value <> "" Then
        Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
        Set rs = oSolicitud.ObtenerSubcamposNotificacion(basPublic.gParametrosInstalacion.gIdioma, lIdFlujo, sdbgNotifExpiracion.Columns("ID_GRUPO").Value, sdbgNotifExpiracion.Columns("ID_CAMPO").Value)
    
        While Not rs.EOF
            If Not IsNull(rs("FORMCAMPO_ID").Value) Then
                sdbddSubcampo.AddItem rs("FORMCAMPO_ID").Value & Chr(m_lSeparador) & rs("FORMCAMPO_DEN").Value
                insertados = True
            End If
            
            rs.MoveNext
        Wend
    
        rs.Close
        Set rs = Nothing
        
        If (insertados = False) Then
            sdbgNotifExpiracion.Columns("SUBCAMPO").Locked = True
        Else
            sdbgNotifExpiracion.Columns("SUBCAMPO").Locked = False
        End If
    End If

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddGrupo_InitColumnProps()
    sdbddGrupo.DataFieldList = "Column 0"
    sdbddGrupo.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddCampo_InitColumnProps()
    sdbddCampo.DataFieldList = "Column 0"
    sdbddCampo.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddSubcampo_InitColumnProps()
    sdbddSubcampo.DataFieldList = "Column 0"
    sdbddSubcampo.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbddGrupo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddGrupo.MoveFirst

    If sdbgNotifExpiracion.Columns("GRUPO").Value <> "" Then
        For i = 0 To sdbddGrupo.Rows - 1
            bm = sdbddGrupo.GetBookmark(i)
            If UCase(sdbgNotifExpiracion.Columns("GRUPO").Value) = UCase(Mid(sdbddGrupo.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("GRUPO").Value))) Then
                sdbgNotifExpiracion.Columns("GRUPO").Value = Mid(sdbddGrupo.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("GRUPO").Value))
                sdbddGrupo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbddCampo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddCampo.MoveFirst

    If sdbgNotifExpiracion.Columns("CAMPO").Value <> "" Then
        For i = 0 To sdbddCampo.Rows - 1
            bm = sdbddCampo.GetBookmark(i)
            If UCase(sdbgNotifExpiracion.Columns("CAMPO").Value) = UCase(Mid(sdbddCampo.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("CAMPO").Value))) Then
                sdbgNotifExpiracion.Columns("CAMPO").Value = Mid(sdbddCampo.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("CAMPO").Value))
                sdbddCampo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbddSubcampo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddSubcampo.MoveFirst

    If sdbgNotifExpiracion.Columns("SUBCAMPO").Value <> "" Then
        For i = 0 To sdbddSubcampo.Rows - 1
            bm = sdbddSubcampo.GetBookmark(i)
            If UCase(sdbgNotifExpiracion.Columns("SUBCAMPO").Value) = UCase(Mid(sdbddSubcampo.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("SUBCAMPO").Value))) Then
                sdbgNotifExpiracion.Columns("SUBCAMPO").Value = Mid(sdbddSubcampo.Columns(1).CellText(bm), 1, Len(sdbgNotifExpiracion.Columns("SUBCAMPO").Value))
                sdbddSubcampo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbddGrupo_CloseUp()
    With sdbgNotifExpiracion
        If .Columns("GRUPO").Value = "" Or val(.Columns("ID_GRUPO").Value) <> sdbddGrupo.Columns("ID").Value Then
            .Columns("ID_GRUPO").Value = ""
            .Columns("GRUPO").Value = ""
            .Columns("ID_CAMPO").Value = ""
            .Columns("CAMPO").Value = ""
            .Columns("ID_SUBCAMPO").Value = ""
            .Columns("SUBCAMPO").Value = ""
            
            If sdbddGrupo.Columns("ID").Value = "" Then Exit Sub
        End If
            
        .Columns("ID_GRUPO").Value = sdbddGrupo.Columns("ID").Value
        .Columns("GRUPO").Value = sdbddGrupo.Columns("DESTINO").Value
    End With
End Sub

Private Sub sdbddCampo_CloseUp()
    With sdbgNotifExpiracion
        If .Columns("CAMPO").Value = "" Or val(.Columns("ID_CAMPO").Value) <> sdbddCampo.Columns("ID").Value Then
            .Columns("ID_CAMPO").Value = ""
            .Columns("CAMPO").Value = ""
            .Columns("ID_SUBCAMPO").Value = ""
            .Columns("SUBCAMPO").Value = ""
            .Columns("CAMPO_ES_DESGLOSE").Value = False
            
            If sdbddCampo.Columns("ID").Value = "" Then Exit Sub
        End If
    
        .Columns("ID_CAMPO").Value = sdbddCampo.Columns("ID").Value
        .Columns("CAMPO").Value = sdbddCampo.Columns("DESTINO").Value
        .Columns("CAMPO_ES_DESGLOSE").Value = sdbddCampo.Columns("ES_DESGLOSE").Value
    End With
End Sub

Private Sub sdbddSubcampo_CloseUp()
    With sdbgNotifExpiracion
        If .Columns("SUBCAMPO").Value = "" Then Exit Sub
    
        .Columns("ID_SUBCAMPO").Value = sdbddSubcampo.Columns("ID").Value
        .Columns("SUBCAMPO").Value = sdbddSubcampo.Columns("DESTINO").Value
    End With
End Sub

Private Sub sdbgNotifExpiracion_BeforeUpdate(Cancel As Integer)
    m_bCambios = True
End Sub

Private Sub sdbgNotifExpiracion_BtnClick()
    Dim oWorkflow As cworkflow
    Dim idAvisoExpiracion As String
    Dim idAvisoExpiracionCampos As String
    Dim oNotificados As CNotificadosAccion
    Dim lPlazoAntes As Long, lPlazoDuracion As Long
    
    Set oWorkflow = oFSGSRaiz.Generar_CWorkflow
    
    If sdbgNotifExpiracion.col < 0 Then Exit Sub
    If sdbgNotifExpiracion.Columns("ID_GRUPO").Value = "" And sdbgNotifExpiracion.Columns("ID_CAMPO").Value = "" Then Exit Sub
    
    If Not sdbgNotifExpiracion.IsAddRow And sdbgNotifExpiracion.Columns("ID_AVISO_EXPIRACION_CAMPOS").Value <> "" Then
       
        frmFlujosNotificadosEnlace.g_sOrigen = "Expiracion"
        
        frmFlujosNotificadosEnlace.lidexpiracion = sdbgNotifExpiracion.Columns("ID_AVISO_EXPIRACION_CAMPOS").Value
        frmFlujosNotificadosEnlace.lIdFlujo = lIdFlujo
        frmFlujosNotificadosEnlace.m_bModifFlujo = True
        frmFlujosNotificadosEnlace.lIdFormulario = m_lIdFormulario
        Set frmFlujosNotificadosEnlace.Formulario = Me.Formulario
        frmFlujosNotificadosEnlace.Show vbModal
        
        Set oNotificados = oFSGSRaiz.Generar_CNotificadosAccion
        oNotificados.CargarNotificados OrigenNotificadoAccion.Expiracion, CLng(sdbgNotifExpiracion.Columns("ID_AVISO_EXPIRACION_CAMPOS").Value)
        If oNotificados.Count > 0 Then
            sdbgNotifExpiracion.Columns("NOTIFICADOS").Value = "..."
        Else
            sdbgNotifExpiracion.Columns("NOTIFICADOS").Value = " "
        End If
        
        If sdbgNotifExpiracion.DataChanged Then
            sdbgNotifExpiracion.Update
        End If
        
    Else
        If sdbgNotifExpiracion.Columns("ID_AVISO_EXPIRACION_CAMPOS").Value = "" Then
            lPlazoAntes = 1
            lPlazoDuracion = 7
            If sdbddPlazoAntes.Columns(0).Text <> "" Then lPlazoAntes = CLng(Me.sdbddPlazoAntes.Columns(0).Text)
            If sdbddPlazoDuracion.Columns(0).Text <> "" Then lPlazoDuracion = CLng(sdbddPlazoDuracion.Columns(0).Text)
                        
            If Not (sdbgNotifExpiracion.Columns("ID_SUBCAMPO").Value = "") Then
                idAvisoExpiracionCampos = oWorkflow.Cargar_Aviso_Expiracion_Campos(CLng(lIdFlujo), CLng(sdbgNotifExpiracion.Columns("ID_GRUPO").Value), CLng(sdbgNotifExpiracion.Columns("ID_CAMPO").Value), CLng(sdbgNotifExpiracion.Columns("ID_SUBCAMPO").Value), lPlazoAntes, lPlazoDuracion)
            Else
                idAvisoExpiracionCampos = oWorkflow.Cargar_Aviso_Expiracion_Campos(CLng(lIdFlujo), CLng(sdbgNotifExpiracion.Columns("ID_GRUPO").Value), CLng(sdbgNotifExpiracion.Columns("ID_CAMPO").Value), , lPlazoAntes, lPlazoDuracion)
            End If
            
            sdbgNotifExpiracion.Columns("ID_AVISO_EXPIRACION_CAMPOS").Value = idAvisoExpiracionCampos
        End If
                
        frmFlujosNotificadosEnlace.g_sOrigen = "Expiracion"
        
        frmFlujosNotificadosEnlace.lidexpiracion = CLng(idAvisoExpiracionCampos)
        frmFlujosNotificadosEnlace.lIdFlujo = lIdFlujo
        frmFlujosNotificadosEnlace.m_bModifFlujo = True
        frmFlujosNotificadosEnlace.lIdFormulario = m_lIdFormulario
        Set frmFlujosNotificadosEnlace.Formulario = Me.Formulario
        frmFlujosNotificadosEnlace.Show vbModal
        
        Set oNotificados = oFSGSRaiz.Generar_CNotificadosAccion
        oNotificados.CargarNotificados OrigenNotificadoAccion.Expiracion, CLng(sdbgNotifExpiracion.Columns("ID_AVISO_EXPIRACION_CAMPOS").Value)
        If oNotificados.Count > 0 Then
            sdbgNotifExpiracion.Columns("NOTIFICADOS").Value = "..."
        Else
            sdbgNotifExpiracion.Columns("NOTIFICADOS").Value = " "
        End If
    
        If sdbgNotifExpiracion.DataChanged Then
            sdbgNotifExpiracion.Update
        End If
                        
    End If
     
End Sub

Public Function Formulario() As CFormulario
    If m_oFormulario Is Nothing Then
        Set m_oFormulario = oFSGSRaiz.Generar_CFormulario
        m_oFormulario.Id = m_lIdFormulario
        m_oFormulario.CargarTodosLosGrupos
    End If
    Set Formulario = m_oFormulario
End Function

Private Sub sdbgNotifExpiracion_KeyPress(KeyAscii As Integer)
    With sdbgNotifExpiracion
        Select Case KeyAscii
            Case vbKeyBack
                Select Case .Columns(.col).Name
                    Case "GRUPO"
                        .Columns("ID_GRUPO").Value = ""
                        .Columns("GRUPO").Value = ""
                        .Columns("ID_CAMPO").Value = ""
                        .Columns("CAMPO").Value = ""
                        .Columns("ID_SUBCAMPO").Value = ""
                        .Columns("SUBCAMPO").Value = ""
                    Case "CAMPO"
                        .Columns("ID_CAMPO").Value = ""
                        .Columns("CAMPO").Value = ""
                        .Columns("ID_SUBCAMPO").Value = ""
                        .Columns("SUBCAMPO").Value = ""
                    Case "SUBCAMPO"
                        .Columns("ID_SUBCAMPO").Value = ""
                        .Columns("SUBCAMPO").Value = ""
                    Case "CMB_ANTELACION"
                        .Columns("PLAZO_ANTELACION").Value = ""
                        .Columns("CMB_ANTELACION").Value = ""
                    Case "CMB_DURACION"
                        .Columns("PLAZO_DURACION").Value = ""
                        .Columns("CMB_DURACION").Value = ""
                End Select
        End Select
    End With
End Sub

Private Sub sdbgNotifExpiracion_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    With sdbgNotifExpiracion
        If .col > -1 Then
            'Habilitar/deshabilitar la columna subcampo
            If .Columns(.col).Name = "SUBCAMPO" Then
                If .Columns("CAMPO_ES_DESGLOSE").Value Then
                    .Columns(.col).Locked = False
                    .Columns(.col).DropDownHwnd = sdbddSubcampo.hWnd
                Else
                    .Columns(.col).Locked = True
                    .Columns(.col).DropDownHwnd = 0
                End If
            End If
            
            If .IsAddRow Then
                cmdAnyadirNotificacion.Enabled = False
            Else
                cmdAnyadirNotificacion.Enabled = True
            End If
        End If
        
        If m_bElimUltima Then
            m_bElimUltima = False
            Exit Sub
        End If
            
        LockWindowUpdate Me.hWnd
        If Not sdbgNotifExpiracion.IsAddRow Then
        ElseIf sdbddPlazoAntes.Columns(0).Value = 0 Or .Columns("CMB_ANTELACION").Value = "" Then
            sdbddPlazoAntes.Columns(0).Value = 1 'El mismo d�a
            sdbddPlazoAntes.Columns(1).Value = m_sIdioma(1)
            
            sdbddPlazoDuracion.Columns(0).Value = 7 '1 semana
            sdbddPlazoDuracion.Columns(1).Text = "1 " & m_sIdioma(9)
            
            .Columns("PLAZO_ANTELACION").Value = sdbddPlazoAntes.Columns(0).Value
            .Columns("PLAZO_DURACION").Value = sdbddPlazoDuracion.Columns(0).Value
            
            .Columns("CMB_ANTELACION").Value = sdbddPlazoAntes.Columns(1).Value
            .Columns("CMB_DURACION").Value = sdbddPlazoDuracion.Columns(1).Value
        End If
        LockWindowUpdate 0&
    End With
End Sub
