VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstESTRORG 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado del organigrama (Opciones)"
   ClientHeight    =   2400
   ClientLeft      =   2235
   ClientTop       =   1575
   ClientWidth     =   7245
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstESTRORG.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2400
   ScaleWidth      =   7245
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   7245
      TabIndex        =   15
      Top             =   2025
      Width           =   7245
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   5790
         TabIndex        =   14
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1935
      Left            =   0
      TabIndex        =   16
      Top             =   30
      Width           =   7140
      _ExtentX        =   12594
      _ExtentY        =   3413
      _Version        =   393216
      Style           =   1
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Tipo de listado"
      TabPicture(0)   =   "frmLstESTRORG.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Selecci�n"
      TabPicture(1)   =   "frmLstESTRORG.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Orden"
      TabPicture(2)   =   "frmLstESTRORG.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraTipoPer"
      Tab(2).Control(1)=   "fraTipoOrg"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Opciones"
      TabPicture(3)   =   "frmLstESTRORG.frx":0D06
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraOpcOrg"
      Tab(3).Control(1)=   "fraOpcPer"
      Tab(3).ControlCount=   2
      Begin VB.Frame fraTipoOrg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -74850
         TabIndex        =   21
         Top             =   585
         Width           =   6870
         Begin VB.OptionButton opOrdOCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   135
            TabIndex        =   4
            Top             =   240
            Value           =   -1  'True
            Width           =   4260
         End
         Begin VB.OptionButton opOrdODen 
            Caption         =   "Denominaci�n"
            Height          =   195
            Left            =   135
            TabIndex        =   5
            Top             =   615
            Width           =   4335
         End
      End
      Begin VB.Frame fraTipoPer 
         Height          =   1275
         Left            =   -74865
         TabIndex        =   22
         Top             =   420
         Width           =   6870
         Begin VB.OptionButton opOrdPOrg 
            Caption         =   "Posici�n en el organigrama"
            Height          =   195
            Left            =   120
            TabIndex        =   8
            Top             =   960
            Width           =   2595
         End
         Begin VB.OptionButton opOrdPNom 
            Caption         =   "Apellidos y nombre de las personas"
            Height          =   195
            Left            =   120
            TabIndex        =   7
            Top             =   600
            Width           =   2955
         End
         Begin VB.OptionButton opOrdPCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   105
            TabIndex        =   6
            Top             =   240
            Value           =   -1  'True
            Width           =   1515
         End
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1245
         Left            =   -74850
         TabIndex        =   18
         Top             =   420
         Width           =   6825
         Begin VB.CommandButton cmdBorrar 
            Height          =   285
            Left            =   5985
            Picture         =   "frmLstESTRORG.frx":0D22
            Style           =   1  'Graphical
            TabIndex        =   25
            Top             =   705
            Width           =   315
         End
         Begin VB.TextBox txtUO 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1770
            Locked          =   -1  'True
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   705
            Width           =   4155
         End
         Begin VB.CommandButton cmdEstruc 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6330
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   705
            Width           =   270
         End
         Begin VB.OptionButton opTodos 
            Caption         =   "Listado completo"
            Height          =   195
            Left            =   180
            TabIndex        =   2
            Top             =   315
            Width           =   4530
         End
         Begin VB.OptionButton opRama 
            Caption         =   "Comenzar en:"
            Height          =   195
            Left            =   165
            TabIndex        =   3
            Top             =   750
            Value           =   -1  'True
            Width           =   1335
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1245
         Left            =   150
         TabIndex        =   17
         Top             =   435
         Width           =   6870
         Begin VB.OptionButton opTipoEmp 
            Caption         =   "Listado de empresas"
            Height          =   195
            Left            =   210
            TabIndex        =   32
            Top             =   870
            Value           =   -1  'True
            Width           =   4860
         End
         Begin VB.OptionButton opTipoPer 
            Caption         =   "DRelaci�n de personas de la organizaci�n"
            Height          =   195
            Left            =   210
            TabIndex        =   1
            Top             =   555
            Width           =   4860
         End
         Begin VB.OptionButton opTipoOrg 
            Caption         =   "DListado estructurado del organigrama"
            Height          =   195
            Left            =   210
            TabIndex        =   0
            Top             =   255
            Width           =   4860
         End
      End
      Begin VB.Frame fraOpcPer 
         Height          =   1245
         Left            =   -74805
         TabIndex        =   24
         Top             =   525
         Width           =   6825
         Begin VB.CheckBox chkBajaLog 
            Caption         =   "Incluir bajas l�gicas"
            Height          =   285
            Left            =   240
            TabIndex        =   30
            Top             =   870
            Width           =   2775
         End
         Begin VB.CheckBox chkNoCompPer 
            Caption         =   "DNo Compradoras"
            Height          =   270
            Left            =   3360
            TabIndex        =   27
            Top             =   825
            Value           =   1  'Checked
            Width           =   1700
         End
         Begin VB.CheckBox chkCompPer 
            Caption         =   "DCompradoras"
            Height          =   315
            Left            =   3360
            TabIndex        =   26
            Top             =   240
            Value           =   1  'Checked
            Width           =   1700
         End
         Begin VB.CheckBox chkIncluirPos 
            Caption         =   "Incluir posici�n en el organigrama"
            Height          =   285
            Left            =   240
            TabIndex        =   10
            Top             =   570
            Width           =   3750
         End
         Begin VB.CheckBox chkIncluirDet 
            Caption         =   "Incluir detalles de personas"
            Height          =   285
            Left            =   240
            TabIndex        =   9
            Top             =   270
            Width           =   2775
         End
      End
      Begin VB.Frame fraOpcOrg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1400
         Left            =   -74805
         TabIndex        =   23
         Top             =   400
         Width           =   6825
         Begin VB.CheckBox chkEmpresa 
            Caption         =   "Incluir datos de empresa"
            Height          =   195
            Left            =   4320
            TabIndex        =   33
            Top             =   225
            Value           =   1  'Checked
            Width           =   2295
         End
         Begin VB.CheckBox chkBajaLogOrg 
            Caption         =   "Incluir bajas l�gicas"
            Height          =   195
            Left            =   240
            TabIndex        =   31
            Top             =   1080
            Width           =   2800
         End
         Begin VB.CheckBox chkNoCompOrg 
            Caption         =   "No compradoras"
            Height          =   225
            Left            =   3480
            TabIndex        =   29
            Top             =   975
            Value           =   1  'Checked
            Width           =   1600
         End
         Begin VB.CheckBox chkCompOrg 
            Caption         =   "Compradoras"
            Height          =   210
            Left            =   3480
            TabIndex        =   28
            Top             =   645
            Value           =   1  'Checked
            Width           =   1600
         End
         Begin VB.CheckBox chkDetallePer 
            Caption         =   "Incluir Detalles de Personas"
            Height          =   195
            Left            =   690
            TabIndex        =   13
            Top             =   825
            Width           =   2800
         End
         Begin VB.CheckBox chkIncluirPer 
            Caption         =   "Incluir Personas"
            Height          =   195
            Left            =   440
            TabIndex        =   12
            Top             =   525
            Value           =   1  'Checked
            Width           =   2905
         End
         Begin VB.CheckBox chkIncluirDep 
            Caption         =   "Incluir Departamentos"
            Height          =   195
            Left            =   195
            TabIndex        =   11
            Top             =   225
            Value           =   1  'Checked
            Width           =   3375
         End
      End
   End
End
Attribute VB_Name = "frmLstESTRORG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de restricciones
Public bRUO As Boolean
Public bRDep As Boolean
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String
Public sUON4 As String
Private SelecUON(0 To 2) As Variant

Private TipoListado As String

'Variables de idioma
Private sIdiSelText(1 To 5) As String
Private txtTitulo1 As String
Private txtTitulo2 As String
Private txtTitulo3 As String
Private txtPag As String
Private txtDe As String
Private txtSeleccion As String
Private txtCar As String
Private txtDep As String
Private txtEmail As String
Private txtFax As String
Private txtTfno As String
Private txtDeps As String
Private txtDepG As String
Private txtComprador As String
Private txtNoComprador As String
Private txtEquipo As String
Private txtBajaLog As String
Private txtNif As String
Private txtRazonSocial As String
Private txtDireccion As String
Private txtCentrosCoste As String


Private sIdiCodigo As String
Private sIdiDen As String


Private Sub chkIncluirDep_Click()
    If chkIncluirDep Then
        chkIncluirPer.Enabled = True
        If chkIncluirPer Then
            chkDetallePer.Enabled = True
            chkCompOrg.Enabled = True
            chkNoCompOrg.Enabled = True
        Else
            chkDetallePer.Enabled = False
            chkCompOrg.Enabled = False
            chkNoCompOrg.Enabled = False
        End If
     Else
        chkIncluirPer.Enabled = False
        chkDetallePer.Enabled = False
        chkCompOrg.Enabled = False
        chkNoCompOrg.Enabled = False
     End If
End Sub

Private Sub chkIncluirPer_Click()
    If chkIncluirPer Then
        chkDetallePer.Enabled = True
        chkCompOrg.Enabled = True
        chkNoCompOrg.Enabled = True
    Else
        chkDetallePer.Enabled = False
        chkCompOrg.Enabled = False
        chkNoCompOrg.Enabled = False
    End If
End Sub

Private Sub cmdBorrar_Click()
    txtUO.Text = ""
End Sub

Private Sub cmdEstruc_Click()
    frmSELUO.sOrigen = "LstESTRORG"
    frmSELUO.bRUO = bRUO
    frmSELUO.bMostrarBajas = True
    frmSELUO.Show 1

End Sub
Public Sub MostrarUOSeleccionada()
    
    If frmSELUO.sUON3 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sUON3 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = frmSELUO.sUON3

        Exit Sub
    End If
    
    If frmSELUO.sUON2 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = ""
        Exit Sub
    End If
    
    If frmSELUO.sUON1 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = ""
        sUON3 = ""
        Exit Sub
    End If
    
    txtUO.Text = ""
    txtUO.Refresh
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    
End Sub

Private Sub cmdObtener_Click()

    If opTipoOrg Then
        TipoListado = "Estructura"
    Else
        If Me.opTipoEmp Then
            TipoListado = "Empresa"
        Else
            If chkIncluirPos Then
                 opOrdOCod = True
                 chkIncluirDep = vbChecked
                 chkIncluirPer = vbChecked
                 chkDetallePer = chkIncluirDet
                 TipoListado = "Estructura"
            Else
                 TipoListado = "Personas"
            End If
        End If
    End If
    ObtenerListado

End Sub


Private Sub ObtenerListado()
    Dim SelectionText As String
    Dim sTitulo As String
    Dim IncluirDepartamentos As String
    Dim IncluirDepartamentosGenerales As String
    Dim IncluirDep1 As String
    Dim IncluirDep2 As String
    Dim IncluirDep3 As String
    Dim IncluirPersonas As String
    Dim IncluirDetalles As String
    Dim IncluirEmpresas, IncluirUON4 As String
    Dim OrdenarPorDen As String
    Dim oReport As CRAXDRT.Report
    Dim Srpt As CRAXDRT.Report
    Dim oCROrganigrama As CROrganigrama
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim pv As Preview
    Dim i As Integer
    Dim intComp As Integer
    Dim bBajaLog As Boolean
    
    ''' * Objetivo: Obtener un listado de la estructura de la organizaci�n
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    If TipoListado = "Estructura" Then
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptESTRORG_ORG.rpt"
    ElseIf TipoListado = "Empresa" Then
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptEMP.rpt"
    Else
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptESTRORG_PER.rpt"
    End If
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
       
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
                                                            
    If txtUO = "" Then
        opTodos.Value = True
        opRama.Value = False
    Else
        opRama.Value = True
        opTodos.Value = False
    End If
    
    SelectionText = ""
    If TipoListado = "Estructura" Then
        SelectionText = sIdiSelText(1)
        If Not opTodos Then
            SelectionText = sIdiSelText(3) & " " & txtUO
        End If
        sTitulo = txtTitulo1
    ElseIf TipoListado = "Empresa" Then
        SelectionText = sIdiSelText(5)
        If Not opTodos Then
            SelectionText = sIdiSelText(3) & " " & txtUO
        End If
        sTitulo = txtTitulo3
    Else
    
        SelectionText = sIdiSelText(2)
        If Not opTodos Then
            SelectionText = sIdiSelText(4) & " " & txtUO
        End If
        sTitulo = txtTitulo2
    End If
   
    
    IncluirDepartamentos = "N"
    IncluirPersonas = "N"
    IncluirDetalles = "N"
    IncluirDepartamentosGenerales = "N"
    IncluirEmpresas = "N"
    IncluirUON4 = "N"
    
    If TipoListado = "Estructura" Then  'Listado de estructura
        If chkEmpresa.Value = vbChecked Then
            IncluirEmpresas = "S"
        End If
        If opOrdOCod Then
            OrdenarPorDen = False
        Else
            OrdenarPorDen = True
        End If
        If chkIncluirDep Then
            IncluirDepartamentos = "S"
            If chkIncluirPer Then
                IncluirPersonas = "S"
                If chkDetallePer Then
                    IncluirDetalles = "S"
                End If
            End If
        End If
        If chkIncluirDep Then
            If txtUO.Text = "" Then
                IncluirDepartamentosGenerales = "S"
            End If
        End If
        
        If opTipoPer Then 'Tipolistado=Estructura pero es la opci�n listado de personas
            If chkIncluirDet Then IncluirDetalles = "S"
            If chkIncluirPos Then
                IncluirDepartamentos = "S"
                IncluirPersonas = "S"
            End If
        End If
        
        If chkBajaLogOrg.Value = vbChecked Then
            bBajaLog = True
        Else
            bBajaLog = False
        End If
        
    ElseIf TipoListado = "Empresa" Then
        If Me.opOrdODen Then
            OrdenarPorDen = True
        Else
            OrdenarPorDen = False
        End If
    
    Else  'Listado de personas
        If opOrdPCod Or opOrdPOrg Then
            OrdenarPorDen = False
        Else
            OrdenarPorDen = True
        End If
        If chkIncluirDet Then IncluirDetalles = "S"
        
        If chkBajaLog.Value = vbChecked Then
            bBajaLog = True
        Else
            bBajaLog = False
        End If
    End If
      
    'Personas compradoras o no
    If chkIncluirPos = vbUnchecked And TipoListado = "Estructura" Then 'Listado de estructura
        If chkCompOrg.Value = vbChecked And chkNoCompOrg.Value = vbChecked Then
            intComp = 0  'todas las personas
        ElseIf chkCompOrg.Value = vbChecked And Not chkNoCompOrg.Value = vbChecked Then
            intComp = 1  's�lo las personas compradoras
        ElseIf Not (chkCompOrg.Value = vbChecked) And chkNoCompOrg.Value = vbChecked Then
            intComp = 2  's�lo las no compradoras
        Else
            intComp = 0  'todas las personas
        End If
        
    Else  'Listado de personas
        If chkCompPer.Value = vbChecked And chkNoCompPer.Value = vbChecked Then
            intComp = 0  'todas las personas
        ElseIf chkCompPer.Value = vbChecked And Not chkNoCompPer.Value = vbChecked Then
            intComp = 1  's�lo las personas compradoras
        ElseIf Not (chkCompPer.Value = vbChecked) And chkNoCompPer.Value = vbChecked Then
            intComp = 2  's�lo las no compradoras
        Else
            intComp = 0  'todas las personas
        End If
    End If
    
    IncluirDepartamentosGenerales = "N"
    IncluirDep1 = "N"
    IncluirDep2 = "N"
    IncluirDep3 = "N"
    
    If bRUO Then
        If oUsuarioSummit.Persona.UON1 <> "" Then
            If oUsuarioSummit.Persona.UON2 = "" Then 'NIVEL 1
                If sUON2 = "" Then
                    IncluirDep1 = "S"
                    IncluirDep2 = "S"
                    IncluirDep3 = "S"
                Else
                    If sUON3 = "" Then
                        IncluirDep2 = "S"
                        IncluirDep3 = "S"
                    Else
                        IncluirDep3 = "S"
                    End If
                End If
            Else
                If oUsuarioSummit.Persona.UON3 = "" Then
                    If sUON3 = "" Then
                        IncluirDep2 = "S"
                        IncluirDep3 = "S"
                    Else
                        IncluirDep3 = "S"
                    End If
                Else
                    IncluirDep3 = "S"
                End If
            End If
        Else 'RUO UON0
            IncluirDepartamentosGenerales = "S"
            IncluirDep1 = "S"
            IncluirDep2 = "S"
            IncluirDep3 = "S"
        End If
    Else 'SIN RESTRICCIONES
        If sUON1 = "" Then
            IncluirDepartamentosGenerales = "S"
            IncluirDep1 = "S"
            IncluirDep2 = "S"
            IncluirDep3 = "S"
               
        Else
            If sUON2 = "" Then
                IncluirDep1 = "S"
                IncluirDep2 = "S"
                IncluirDep3 = "S"
            Else
                If sUON3 = "" Then
                    IncluirDep2 = "S"
                    IncluirDep3 = "S"
                Else
                    IncluirDep3 = "S"
                End If
            End If
        End If
    End If
    
    If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
        IncluirUON4 = "S"
    End If
    
    If TipoListado = "Empresa" Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
        
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & sTitulo & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & txtDe & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & txtSeleccion & """"
    Else  'los otros
    
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "DETALLES")).Text = """" & IncluirDetalles & """"
        If TipoListado = "Estructura" Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "UON0NOM")).Text = """" & gParametrosGenerales.gsDEN_UON0 & """"
        End If
        oReport.FormulaFields(crs_FormulaIndex(oReport, "UON1NOM")).Text = """" & gParametrosGenerales.gsDEN_UON1 & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "UON2NOM")).Text = """" & gParametrosGenerales.gsDEN_UON2 & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "UON3NOM")).Text = """" & gParametrosGenerales.gsDEN_UON3 & ":" & """"
        
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & sTitulo & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & txtDe & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & txtSeleccion & """"
        If bBajaLog = True Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "BAJALOG")).Text = """S"""
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtBajaLog")).Text = """" & txtBajaLog & """"
        Else
            oReport.FormulaFields(crs_FormulaIndex(oReport, "BAJALOG")).Text = """N"""
        End If

    End If
    If TipoListado = "Estructura" Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "DEPART")).Text = """" & IncluirDepartamentos & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "PERSONAS")).Text = """" & IncluirPersonas & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "DEP_UON0")).Text = """" & IncluirDepartamentosGenerales & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "DEP_UON1")).Text = """" & IncluirDep1 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "DEP_UON2")).Text = """" & IncluirDep2 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "DEP_UON3")).Text = """" & IncluirDep3 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "INCLUIREMPRESAS")).Text = """" & IncluirEmpresas & """"
        'oReport.FormulaFields(crs_FormulaIndex(oReport, "INCLUIRUON4")).Text = """" & IncluirUON4 & """"
        For i = 1 To 3
            Set Srpt = Nothing
            Set Srpt = oReport.OpenSubreport("rptUON" & i & "_DEP_PER")
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtCar")).Text = """" & txtCar & """"
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtDeps")).Text = """" & txtDeps & """"
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtEmail")).Text = """" & txtEmail & """"
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtFax")).Text = """" & txtFax & """"
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtTfno")).Text = """" & txtTfno & """"
            If Not FSEPConf Then
                Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtComp")).Text = """" & txtComprador & """"
                Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtNoComp")).Text = """" & txtNoComprador & """"
                Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtEquipo")).Text = """" & txtEquipo & """"
            End If
        Next i
        Set Srpt = Nothing
        Set Srpt = oReport.OpenSubreport("rptUON0_DEP_PER")
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtCar")).Text = """" & txtCar & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtDepG")).Text = """" & txtDepG & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtEmail")).Text = """" & txtEmail & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtFax")).Text = """" & txtFax & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtTfno")).Text = """" & txtTfno & """"
        If Not FSEPConf Then
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtComp")).Text = """" & txtComprador & """"
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtNoComp")).Text = """" & txtNoComprador & """"
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtEquipo")).Text = """" & txtEquipo & """"
        End If
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtComp")).Text = """" & txtComprador & """"
        
'        If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
'            Set Srpt = Nothing
'            Set Srpt = oReport.OpenSubreport("rptUON4")
'            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtCentrosCoste")).Text = """" & txtCentrosCoste & """"
'        End If
        

    ElseIf TipoListado = "Empresa" Then
    
    Else  'Listado de personas
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCar")).Text = """" & txtCar & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDep")).Text = """" & txtDep & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEmail")).Text = """" & txtEmail & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFax")).Text = """" & txtFax & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTfno")).Text = """" & txtTfno & """"
        If Not FSEPConf Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComp")).Text = """" & txtComprador & """"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNoComp")).Text = """" & txtNoComprador & """"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEquipo")).Text = """" & txtEquipo & """"
        End If
    End If
    
    If TipoListado = "Empresa" Or TipoListado = "Estructura" Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNIF")).Text = """" & txtNif & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRazonSocial")).Text = """" & txtRazonSocial & ":" & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDIRECCION")).Text = """" & txtDireccion & ":" & """"
    End If
    
    SelecUON(0) = ""
    SelecUON(1) = ""
    SelecUON(2) = ""
    If (sUON1 = "" And sUON2 = "" And sUON3 = "" And Not opTodos) Or (bRUO And bRDep) Then
        If bRUO Then
            SelecUON(0) = basOptimizacion.gUON1Usuario
            SelecUON(1) = basOptimizacion.gUON2Usuario
            SelecUON(2) = basOptimizacion.gUON3Usuario
        End If
    Else
        SelecUON(0) = sUON2
        SelecUON(1) = sUON3
        SelecUON(2) = ""
    End If
    If IsEmpty(SelecUON(0)) Or IsNull(SelecUON(0)) Then SelecUON(0) = ""
    If IsEmpty(SelecUON(1)) Or IsNull(SelecUON(1)) Then SelecUON(1) = ""
    If IsEmpty(SelecUON(2)) Or IsNull(SelecUON(2)) Then SelecUON(2) = ""
    
    Set oCROrganigrama = GenerarCROrganigrama(SelecUON)
    
    If TipoListado = "Empresa" Then
        If Not oCROrganigrama.ListadoEmpresas(oGestorInformes, oReport, TipoListado, SelecUON, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, sUON1, sUON2, sUON3, OrdenarPorDen, intComp, bBajaLog) Then
            oMensajes.ImposibleMostrarInforme
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
            
    
    Else
        oCROrganigrama.Listado oGestorInformes, oReport, TipoListado, SelecUON, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, sUON1, sUON2, sUON3, OrdenarPorDen, intComp, bBajaLog
    End If
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    pv.Show
    Unload Me
    Screen.MousePointer = vbNormal
            
End Sub

Private Sub Form_Load()

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    If FSEPConf Then
        
        chkCompPer.Value = vbUnchecked
        chkNoCompPer.Value = vbUnchecked
        chkCompPer.Visible = False
        chkNoCompPer.Visible = False
        chkCompOrg.Value = vbUnchecked
        chkNoCompOrg.Value = vbUnchecked
        chkCompOrg.Visible = False
        chkNoCompOrg.Visible = False
        
    End If
    
    txtUO.Enabled = False
    opTipoOrg.Value = True
    opTodos.Value = True
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    
    ' Configurar la seguridad
    ConfigurarSeguridad
  
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTESTRORG, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        SSTab1.TabCaption(0) = Ador(0).Value      '1
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value     '2
        Ador.MoveNext
        SSTab1.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(3) = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value '5
        Ador.MoveNext
        opTipoOrg.caption = Ador(0).Value
        Ador.MoveNext
        opTipoPer.caption = Ador(0).Value
        Ador.MoveNext
        frmLstESTRORG.caption = Ador(0).Value
        Ador.MoveNext
        opTodos.caption = Ador(0).Value
        Ador.MoveNext
        opRama.caption = Ador(0).Value '10
        Ador.MoveNext
        opOrdOCod.caption = Ador(0).Value
        opOrdPCod.caption = Ador(0).Value
        sIdiCodigo = Ador(0).Value
        
        Ador.MoveNext
        opOrdODen.caption = Ador(0).Value
        sIdiDen = Ador(0).Value
        Ador.MoveNext
        chkIncluirDet.caption = Ador(0).Value
        chkDetallePer.caption = Ador(0).Value
        Ador.MoveNext
        chkIncluirPos.caption = Ador(0).Value
        Ador.MoveNext
        opOrdPNom.caption = Ador(0).Value '15
        Ador.MoveNext
        opOrdPOrg.caption = Ador(0).Value
        Ador.MoveNext
        chkIncluirDep.caption = Ador(0).Value
        Ador.MoveNext
        chkIncluirPer.caption = Ador(0).Value
        Ador.MoveNext
        sIdiSelText(1) = Ador(0).Value
        Ador.MoveNext
        sIdiSelText(2) = Ador(0).Value '20
        Ador.MoveNext
        sIdiSelText(3) = Ador(0).Value
        Ador.MoveNext
        sIdiSelText(4) = Ador(0).Value
        'Idiomas del RPT
        Ador.MoveNext
        txtTitulo1 = Ador(0).Value '200
        Ador.MoveNext
        txtTitulo2 = Ador(0).Value
        Ador.MoveNext
        txtPag = Ador(0).Value
        Ador.MoveNext
        txtDe = Ador(0).Value
        Ador.MoveNext
        txtSeleccion = Ador(0).Value
        Ador.MoveNext
        txtCar = Ador(0).Value
        Ador.MoveNext
        txtDep = Ador(0).Value
        Ador.MoveNext
        txtEmail = Ador(0).Value
        Ador.MoveNext
        txtFax = Ador(0).Value
        Ador.MoveNext
        txtTfno = Ador(0).Value
        Ador.MoveNext
        txtDeps = Ador(0).Value
        Ador.MoveNext
        txtDepG = Ador(0).Value
        Ador.MoveNext
        chkCompOrg.caption = Ador(0).Value
        chkCompPer.caption = Ador(0).Value
        Ador.MoveNext
        chkNoCompOrg.caption = Ador(0).Value
        chkNoCompPer.caption = Ador(0).Value
        Ador.MoveNext
        txtComprador = Ador(0).Value
        Ador.MoveNext
        txtNoComprador = Ador(0).Value
        Ador.MoveNext
        txtEquipo = Ador(0).Value
        
        Ador.MoveNext
        chkBajaLog.caption = Ador(0).Value   'Incluir bajas l�gicas
        chkBajaLogOrg.caption = Ador(0).Value
        Ador.MoveNext
        txtBajaLog = Ador(0).Value
        Ador.MoveNext
        txtNif = Ador(0).Value
        Ador.MoveNext
        txtRazonSocial = Ador(0).Value
        Ador.MoveNext
        txtDireccion = Ador(0).Value
        Ador.MoveNext
        txtTitulo3 = Ador(0).Value
        opTipoEmp.caption = Ador(0).Value
        Ador.MoveNext
        sIdiSelText(5) = Ador(0).Value
        Ador.MoveNext
        Me.chkEmpresa.caption = Ador(0).Value
        
        txtCentrosCoste = "Centros de Coste"

        Ador.Close
    End If
    
    Set Ador = Nothing

End Sub

Private Sub ConfigurarSeguridad()

    bRUO = False
    bRDep = False
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestUO)) Is Nothing) Then
            bRUO = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestDep)) Is Nothing) Then
            bRDep = True
        End If
        
    End If

End Sub
Private Sub opOrdPCod_Click()
    chkIncluirPos.Enabled = True
End Sub
Private Sub opOrdPNom_Click()
    chkIncluirPos.Enabled = True
End Sub
Private Sub opOrdPOrg_Click()
    chkIncluirPos.Enabled = False
    chkIncluirPos.Value = vbChecked
End Sub
Private Sub opRama_Click()
    If opRama Then
        cmdEstruc.Enabled = True
        cmdBorrar.Enabled = True
    End If
End Sub

Private Sub opTipoEmp_Click()
    If opTipoEmp = True Then
        Me.SSTab1.TabVisible(3) = False
    End If
    
End Sub

Private Sub opTipoOrg_Click()
    If opTipoOrg = True Then
        fraTipoOrg.Visible = True
        fraTipoPer.Visible = False
        fraOpcOrg.Visible = True
        fraOpcPer.Visible = False
        Me.SSTab1.TabVisible(3) = True
        Me.chkEmpresa.Visible = True
    End If
End Sub
Private Sub opTipoPer_Click()

    If opTipoPer = True Then
        fraTipoPer.Visible = True
        fraTipoOrg.Visible = False
        fraOpcPer.Visible = True
        fraOpcOrg.Visible = False
        Me.chkEmpresa.Visible = False
        Me.SSTab1.TabVisible(3) = True
   End If

End Sub
Private Sub opTodos_Click()
    If opTodos Then
        txtUO.Text = ""
        txtUO.Refresh
        sUON1 = ""
        sUON2 = ""
        sUON3 = ""
        cmdEstruc.Enabled = False
        cmdBorrar.Enabled = False
    End If
End Sub


Private Sub SSTab1_Click(PreviousTab As Integer)
If SSTab1.Tab = 2 Then
    If Me.opTipoEmp.Value Then
        Me.opOrdOCod.caption = txtNif
        Me.opOrdPCod.caption = txtNif
        Me.opOrdODen.caption = txtRazonSocial
    Else
        Me.opOrdOCod.caption = sIdiCodigo
        Me.opOrdPCod.caption = sIdiCodigo
        Me.opOrdODen.caption = sIdiDen
    End If
End If
        
End Sub


Private Sub txtUO_Change()
    If txtUO.Text <> "" Then
        opRama.Value = True
    End If
End Sub


