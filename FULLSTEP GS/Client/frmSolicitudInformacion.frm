VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSolicitudInformacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DInformaci�n de procesos de compra/pedidos de solicitud"
   ClientHeight    =   6300
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudInformacion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6300
   ScaleWidth      =   10680
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab ssTabDetalle 
      Height          =   6168
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   10572
      _ExtentX        =   18653
      _ExtentY        =   10874
      _Version        =   393216
      Style           =   1
      Tabs            =   7
      TabsPerRow      =   7
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Procesos"
      TabPicture(0)   =   "frmSolicitudInformacion.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "sdbgProcesos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Publicaciones"
      TabPicture(1)   =   "frmSolicitudInformacion.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sdbgPublicaciones"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Pedidos de cat�logo"
      TabPicture(2)   =   "frmSolicitudInformacion.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "sdbgPedidosCat"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Pedidos directos"
      TabPicture(3)   =   "frmSolicitudInformacion.frx":0D06
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "sdbgPedidosDir"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Pedidos negociados"
      TabPicture(4)   =   "frmSolicitudInformacion.frx":0D22
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "sdbgPedidosNeg"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Pedidos libres"
      TabPicture(5)   =   "frmSolicitudInformacion.frx":0D3E
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "sdbgPedidosLibres"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "DPedidos abiertos"
      TabPicture(6)   =   "frmSolicitudInformacion.frx":0D5A
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "sdbgPedidosAbiertos"
      Tab(6).ControlCount=   1
      Begin SSDataWidgets_B.SSDBGrid sdbgPublicaciones 
         Height          =   5595
         Left            =   -74880
         TabIndex        =   1
         Top             =   480
         Width           =   10335
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         Col.Count       =   17
         stylesets.count =   15
         stylesets(0).Name=   "PendComObj"
         stylesets(0).BackColor=   16744576
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSolicitudInformacion.frx":0D76
         stylesets(1).Name=   "Bold"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSolicitudInformacion.frx":0D92
         stylesets(2).Name=   "PendAdj"
         stylesets(2).BackColor=   8289982
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSolicitudInformacion.frx":0DAE
         stylesets(3).Name=   "Normal"
         stylesets(3).BackColor=   -2147483647
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmSolicitudInformacion.frx":0DCA
         stylesets(4).Name=   "Anulado"
         stylesets(4).BackColor=   8421504
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmSolicitudInformacion.frx":0DE6
         stylesets(5).Name=   "ActiveRow"
         stylesets(5).ForeColor=   16777215
         stylesets(5).BackColor=   8388608
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmSolicitudInformacion.frx":0E02
         stylesets(6).Name=   "SmallFont"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmSolicitudInformacion.frx":0E1E
         stylesets(7).Name=   "EnRecOfe"
         stylesets(7).BackColor=   12632256
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmSolicitudInformacion.frx":0E3A
         stylesets(8).Name=   "PendAsigProve"
         stylesets(8).BackColor=   13171450
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmSolicitudInformacion.frx":0E56
         stylesets(9).Name=   "AdjYNotificado"
         stylesets(9).BackColor=   13750691
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmSolicitudInformacion.frx":0E72
         stylesets(10).Name=   "Header"
         stylesets(10).ForeColor=   0
         stylesets(10).BackColor=   -2147483633
         stylesets(10).HasFont=   -1  'True
         BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(10).Picture=   "frmSolicitudInformacion.frx":0E8E
         stylesets(11).Name=   "PendEnvPet"
         stylesets(11).BackColor=   8454016
         stylesets(11).HasFont=   -1  'True
         BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(11).Picture=   "frmSolicitudInformacion.frx":0EAA
         stylesets(12).Name=   "ParcCerrado"
         stylesets(12).HasFont=   -1  'True
         BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(12).Picture=   "frmSolicitudInformacion.frx":0EC6
         stylesets(13).Name=   "PendValidar"
         stylesets(13).BackColor=   8421631
         stylesets(13).HasFont=   -1  'True
         BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(13).Picture=   "frmSolicitudInformacion.frx":0EE2
         stylesets(14).Name=   "AdjSinNotificar"
         stylesets(14).BackColor=   12632256
         stylesets(14).HasFont=   -1  'True
         BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(14).Picture=   "frmSolicitudInformacion.frx":0EFE
         MultiLine       =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         BalloonHelp     =   0   'False
         HeadStyleSet    =   "Header"
         StyleSet        =   "SmallFont"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   17
         Columns(0).Width=   979
         Columns(0).Caption=   "Pub."
         Columns(0).Name =   "PUB"
         Columns(0).Alignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).Style=   2
         Columns(1).Width=   2037
         Columns(1).Caption=   "Despublicaci�n"
         Columns(1).Name =   "DESPUB"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   7
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   2196
         Columns(2).Caption=   "Proceso"
         Columns(2).Name =   "PROCE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16777215
         Columns(3).Width=   2672
         Columns(3).Caption=   "Art�culo"
         Columns(3).Name =   "ART"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16777215
         Columns(4).Width=   2037
         Columns(4).Caption=   "Proveedor"
         Columns(4).Name =   "PROVE"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(4).Style=   1
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   16777215
         Columns(5).Width=   1138
         Columns(5).Caption=   "Destino"
         Columns(5).Name =   "DEST"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   1217
         Columns(6).Caption=   "Cant. Adj."
         Columns(6).Name =   "CANTADJ"
         Columns(6).Alignment=   1
         Columns(6).CaptionAlignment=   2
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   5
         Columns(6).NumberFormat=   "Standard"
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   16777215
         Columns(7).Width=   1323
         Columns(7).Caption=   "U. base"
         Columns(7).Name =   "UB"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).HasBackColor=   -1  'True
         Columns(7).BackColor=   16777215
         Columns(8).Width=   1482
         Columns(8).Caption=   "Precio base (EUR)"
         Columns(8).Name =   "PrecioUB"
         Columns(8).Alignment=   1
         Columns(8).CaptionAlignment=   2
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   5
         Columns(8).NumberFormat=   "#,##0.0####################"
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   11073522
         Columns(9).Width=   873
         Columns(9).Caption=   "UP"
         Columns(9).Name =   "UP"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(10).Width=   847
         Columns(10).Caption=   "FC"
         Columns(10).Name=   "FC"
         Columns(10).Alignment=   1
         Columns(10).CaptionAlignment=   2
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   5
         Columns(10).NumberFormat=   "Standard"
         Columns(10).FieldLen=   256
         Columns(10).Locked=   -1  'True
         Columns(11).Width=   1402
         Columns(11).Caption=   "Cant.m�n.ped."
         Columns(11).Name=   "CANTMIN"
         Columns(11).Alignment=   1
         Columns(11).CaptionAlignment=   2
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   5
         Columns(11).NumberFormat=   "Standard"
         Columns(11).FieldLen=   256
         Columns(11).Locked=   -1  'True
         Columns(12).Width=   3200
         Columns(12).Visible=   0   'False
         Columns(12).Caption=   "ID"
         Columns(12).Name=   "ID"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "ANYO"
         Columns(13).Name=   "ANYO"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   2
         Columns(13).FieldLen=   256
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "GMN1"
         Columns(14).Name=   "GMN1"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "PROCECOD"
         Columns(15).Name=   "PROCECOD"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   2
         Columns(15).FieldLen=   256
         Columns(16).Width=   3200
         Columns(16).Visible=   0   'False
         Columns(16).Caption=   "ARTCOD"
         Columns(16).Name=   "ARTCOD"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         _ExtentX        =   18230
         _ExtentY        =   9869
         _StockProps     =   79
         BackColor       =   -2147483644
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgProcesos 
         Height          =   5565
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   10275
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   9
         stylesets.count =   15
         stylesets(0).Name=   "PendComObj"
         stylesets(0).BackColor=   16744576
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSolicitudInformacion.frx":0F1A
         stylesets(1).Name=   "Bold"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSolicitudInformacion.frx":0F36
         stylesets(2).Name=   "PendAdj"
         stylesets(2).BackColor=   8289982
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSolicitudInformacion.frx":0F52
         stylesets(3).Name=   "Normal"
         stylesets(3).BackColor=   -2147483647
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmSolicitudInformacion.frx":0F6E
         stylesets(4).Name=   "Anulado"
         stylesets(4).BackColor=   8421504
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmSolicitudInformacion.frx":0F8A
         stylesets(5).Name=   "ActiveRow"
         stylesets(5).ForeColor=   16777215
         stylesets(5).BackColor=   8388608
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmSolicitudInformacion.frx":0FA6
         stylesets(6).Name=   "SmallFont"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmSolicitudInformacion.frx":0FC2
         stylesets(7).Name=   "EnRecOfe"
         stylesets(7).BackColor=   12632256
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmSolicitudInformacion.frx":0FDE
         stylesets(8).Name=   "PendAsigProve"
         stylesets(8).BackColor=   13171450
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmSolicitudInformacion.frx":0FFA
         stylesets(9).Name=   "AdjYNotificado"
         stylesets(9).BackColor=   13750691
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmSolicitudInformacion.frx":1016
         stylesets(10).Name=   "Header"
         stylesets(10).ForeColor=   0
         stylesets(10).BackColor=   -2147483633
         stylesets(10).HasFont=   -1  'True
         BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(10).Picture=   "frmSolicitudInformacion.frx":1032
         stylesets(11).Name=   "PendEnvPet"
         stylesets(11).BackColor=   8454016
         stylesets(11).HasFont=   -1  'True
         BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(11).Picture=   "frmSolicitudInformacion.frx":104E
         stylesets(12).Name=   "ParcCerrado"
         stylesets(12).HasFont=   -1  'True
         BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(12).Picture=   "frmSolicitudInformacion.frx":106A
         stylesets(13).Name=   "PendValidar"
         stylesets(13).BackColor=   8421631
         stylesets(13).HasFont=   -1  'True
         BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(13).Picture=   "frmSolicitudInformacion.frx":1086
         stylesets(14).Name=   "AdjSinNotificar"
         stylesets(14).BackColor=   12632256
         stylesets(14).HasFont=   -1  'True
         BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(14).Picture=   "frmSolicitudInformacion.frx":10A2
         BeveColorScheme =   0
         BevelColorHighlight=   14737632
         BevelColorShadow=   128
         CheckBox3D      =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         HeadStyleSet    =   "Header"
         StyleSet        =   "SmallFont"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   9
         Columns(0).Width=   741
         Columns(0).Caption=   "A�o"
         Columns(0).Name =   "ANYO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasHeadForeColor=   -1  'True
         Columns(0).HeadForeColor=   16777215
         Columns(0).HeadStyleSet=   "Header"
         Columns(1).Width=   794
         Columns(1).Caption=   "Mat."
         Columns(1).Name =   "GMN1"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HeadForeColor=   16777215
         Columns(1).HeadStyleSet=   "Header"
         Columns(2).Width=   820
         Columns(2).Caption=   "Cod."
         Columns(2).Name =   "COD"
         Columns(2).Alignment=   1
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HeadForeColor=   16777215
         Columns(2).HeadStyleSet=   "Header"
         Columns(3).Width=   7091
         Columns(3).Caption=   "Descripci�n"
         Columns(3).Name =   "DESCR"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasHeadForeColor=   -1  'True
         Columns(3).HeadForeColor=   16777215
         Columns(3).HeadStyleSet=   "Header"
         Columns(4).Width=   4419
         Columns(4).Caption=   "Estado"
         Columns(4).Name =   "EST"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(4).HasHeadForeColor=   -1  'True
         Columns(4).HeadForeColor=   16777215
         Columns(4).HeadStyleSet=   "Header"
         Columns(5).Width=   1323
         Columns(5).Caption=   "Resp."
         Columns(5).Name =   "RESP"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(5).Style=   1
         Columns(5).HasHeadForeColor=   -1  'True
         Columns(5).HeadForeColor=   16777215
         Columns(5).HeadStyleSet=   "Header"
         Columns(6).Width=   714
         Columns(6).Caption=   "Dir."
         Columns(6).Name =   "ADJDIR"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).Style=   2
         Columns(6).HasHeadForeColor=   -1  'True
         Columns(6).HeadForeColor=   16777215
         Columns(6).HeadStyleSet=   "Header"
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "ESTID"
         Columns(7).Name =   "ESTID"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   1746
         Columns(8).Caption=   "Fec. pres."
         Columns(8).Name =   "FECPRES"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(8).HasHeadForeColor=   -1  'True
         Columns(8).HeadForeColor=   16777215
         Columns(8).HeadStyleSet=   "Header"
         _ExtentX        =   18119
         _ExtentY        =   9821
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   -2147483644
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgPedidosCat 
         Height          =   5565
         Left            =   -74880
         TabIndex        =   3
         Top             =   480
         Width           =   10300
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   10
         stylesets.count =   17
         stylesets(0).Name=   "PendComObj"
         stylesets(0).BackColor=   16744576
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSolicitudInformacion.frx":10BE
         stylesets(1).Name=   "PendAdj"
         stylesets(1).BackColor=   8289982
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSolicitudInformacion.frx":10DA
         stylesets(2).Name=   "Bold"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSolicitudInformacion.frx":10F6
         stylesets(3).Name=   "Anulado"
         stylesets(3).BackColor=   8421504
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmSolicitudInformacion.frx":1112
         stylesets(4).Name=   "Normal"
         stylesets(4).BackColor=   16777215
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmSolicitudInformacion.frx":112E
         stylesets(5).Name=   "SmallFont"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmSolicitudInformacion.frx":114A
         stylesets(6).Name=   "ActiveRow"
         stylesets(6).ForeColor=   16777215
         stylesets(6).BackColor=   8388608
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmSolicitudInformacion.frx":1166
         stylesets(7).Name=   "EnRecOfe"
         stylesets(7).BackColor=   12632256
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmSolicitudInformacion.frx":1182
         stylesets(8).Name=   "IntHeader"
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmSolicitudInformacion.frx":119E
         stylesets(8).AlignmentPicture=   0
         stylesets(9).Name=   "AdjYNotificado"
         stylesets(9).BackColor=   13750691
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmSolicitudInformacion.frx":17C8
         stylesets(10).Name=   "PendAsigProve"
         stylesets(10).BackColor=   13171450
         stylesets(10).HasFont=   -1  'True
         BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(10).Picture=   "frmSolicitudInformacion.frx":17E4
         stylesets(11).Name=   "ParcCerrado"
         stylesets(11).HasFont=   -1  'True
         BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(11).Picture=   "frmSolicitudInformacion.frx":1800
         stylesets(12).Name=   "PendEnvPet"
         stylesets(12).BackColor=   8454016
         stylesets(12).HasFont=   -1  'True
         BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(12).Picture=   "frmSolicitudInformacion.frx":181C
         stylesets(13).Name=   "Header"
         stylesets(13).ForeColor=   0
         stylesets(13).BackColor=   -2147483633
         stylesets(13).HasFont=   -1  'True
         BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(13).Picture=   "frmSolicitudInformacion.frx":1838
         stylesets(14).Name=   "PendValidar"
         stylesets(14).BackColor=   8421631
         stylesets(14).HasFont=   -1  'True
         BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(14).Picture=   "frmSolicitudInformacion.frx":1854
         stylesets(15).Name=   "AdjSinNotificar"
         stylesets(15).BackColor=   12632256
         stylesets(15).HasFont=   -1  'True
         BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(15).Picture=   "frmSolicitudInformacion.frx":1870
         stylesets(16).Name=   "styAzul"
         stylesets(16).BackColor=   16777153
         stylesets(16).HasFont=   -1  'True
         BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(16).Picture=   "frmSolicitudInformacion.frx":188C
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         CellNavigation  =   1
         MaxSelectedRows =   0
         HeadStyleSet    =   "Header"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   10
         Columns(0).Width=   900
         Columns(0).Caption=   "DAnyo"
         Columns(0).Name =   "ANYO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   1032
         Columns(1).Caption=   "DPed"
         Columns(1).Name =   "PED"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1005
         Columns(2).Caption=   "DNum"
         Columns(2).Name =   "NUM"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   4048
         Columns(3).Caption=   "DProve"
         Columns(3).Name =   "PROVE"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).Style=   1
         Columns(3).ButtonsAlways=   -1  'True
         Columns(4).Width=   2117
         Columns(4).Caption=   "DFecEmi"
         Columns(4).Name =   "FECEMI"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2196
         Columns(5).Caption=   "DNumExt"
         Columns(5).Name =   "NUMEXT"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   4207
         Columns(6).Caption=   "DEstado"
         Columns(6).Name =   "ESTADO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   2117
         Columns(7).Caption=   "DFecha"
         Columns(7).Name =   "FECHA"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).ButtonsAlways=   -1  'True
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID"
         Columns(8).Name =   "ID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "PROVECOD"
         Columns(9).Name =   "PROVECOD"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         _ExtentX        =   18168
         _ExtentY        =   9816
         _StockProps     =   79
         BackColor       =   -2147483644
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgPedidosDir 
         Height          =   5565
         Left            =   -74880
         TabIndex        =   4
         Top             =   480
         Width           =   10305
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   10
         stylesets.count =   17
         stylesets(0).Name=   "PendComObj"
         stylesets(0).BackColor=   16744576
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSolicitudInformacion.frx":18A8
         stylesets(1).Name=   "Bold"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSolicitudInformacion.frx":18C4
         stylesets(2).Name=   "PendAdj"
         stylesets(2).BackColor=   8289982
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSolicitudInformacion.frx":18E0
         stylesets(3).Name=   "Normal"
         stylesets(3).BackColor=   16777215
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmSolicitudInformacion.frx":18FC
         stylesets(4).Name=   "Anulado"
         stylesets(4).BackColor=   8421504
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmSolicitudInformacion.frx":1918
         stylesets(5).Name=   "ActiveRow"
         stylesets(5).ForeColor=   16777215
         stylesets(5).BackColor=   8388608
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmSolicitudInformacion.frx":1934
         stylesets(6).Name=   "SmallFont"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmSolicitudInformacion.frx":1950
         stylesets(7).Name=   "IntHeader"
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmSolicitudInformacion.frx":196C
         stylesets(7).AlignmentPicture=   0
         stylesets(8).Name=   "EnRecOfe"
         stylesets(8).BackColor=   12632256
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmSolicitudInformacion.frx":1F96
         stylesets(9).Name=   "PendAsigProve"
         stylesets(9).BackColor=   13171450
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmSolicitudInformacion.frx":1FB2
         stylesets(10).Name=   "AdjYNotificado"
         stylesets(10).BackColor=   13750691
         stylesets(10).HasFont=   -1  'True
         BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(10).Picture=   "frmSolicitudInformacion.frx":1FCE
         stylesets(11).Name=   "Header"
         stylesets(11).ForeColor=   0
         stylesets(11).BackColor=   -2147483633
         stylesets(11).HasFont=   -1  'True
         BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(11).Picture=   "frmSolicitudInformacion.frx":1FEA
         stylesets(12).Name=   "PendEnvPet"
         stylesets(12).BackColor=   8454016
         stylesets(12).HasFont=   -1  'True
         BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(12).Picture=   "frmSolicitudInformacion.frx":2006
         stylesets(13).Name=   "ParcCerrado"
         stylesets(13).HasFont=   -1  'True
         BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(13).Picture=   "frmSolicitudInformacion.frx":2022
         stylesets(14).Name=   "PendValidar"
         stylesets(14).BackColor=   8421631
         stylesets(14).HasFont=   -1  'True
         BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(14).Picture=   "frmSolicitudInformacion.frx":203E
         stylesets(15).Name=   "styAzul"
         stylesets(15).BackColor=   16777153
         stylesets(15).HasFont=   -1  'True
         BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(15).Picture=   "frmSolicitudInformacion.frx":205A
         stylesets(16).Name=   "AdjSinNotificar"
         stylesets(16).BackColor=   12632256
         stylesets(16).HasFont=   -1  'True
         BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(16).Picture=   "frmSolicitudInformacion.frx":2076
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         CellNavigation  =   1
         MaxSelectedRows =   0
         HeadStyleSet    =   "Header"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   10
         Columns(0).Width=   900
         Columns(0).Caption=   "DAnyo"
         Columns(0).Name =   "ANYO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   1032
         Columns(1).Caption=   "DPed"
         Columns(1).Name =   "PED"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1005
         Columns(2).Caption=   "DNum"
         Columns(2).Name =   "NUM"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3995
         Columns(3).Caption=   "DProve"
         Columns(3).Name =   "PROVE"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).Style=   1
         Columns(3).ButtonsAlways=   -1  'True
         Columns(4).Width=   2117
         Columns(4).Caption=   "DFecEmi"
         Columns(4).Name =   "FECEMI"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2514
         Columns(5).Caption=   "DNumExt"
         Columns(5).Name =   "NUMEXT"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   4022
         Columns(6).Caption=   "DEstado"
         Columns(6).Name =   "ESTADO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   2117
         Columns(7).Caption=   "DFecha"
         Columns(7).Name =   "FECHA"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).ButtonsAlways=   -1  'True
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID"
         Columns(8).Name =   "ID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "PROVECOD"
         Columns(9).Name =   "PROVECOD"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         _ExtentX        =   18168
         _ExtentY        =   9816
         _StockProps     =   79
         BackColor       =   -2147483644
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgPedidosLibres 
         Height          =   5565
         Left            =   -74880
         TabIndex        =   5
         Top             =   480
         Width           =   10305
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   10
         stylesets.count =   17
         stylesets(0).Name=   "PendComObj"
         stylesets(0).BackColor=   16744576
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSolicitudInformacion.frx":2092
         stylesets(1).Name=   "Bold"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSolicitudInformacion.frx":20AE
         stylesets(2).Name=   "PendAdj"
         stylesets(2).BackColor=   8289982
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSolicitudInformacion.frx":20CA
         stylesets(3).Name=   "Normal"
         stylesets(3).BackColor=   16777215
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmSolicitudInformacion.frx":20E6
         stylesets(4).Name=   "Anulado"
         stylesets(4).BackColor=   8421504
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmSolicitudInformacion.frx":2102
         stylesets(5).Name=   "ActiveRow"
         stylesets(5).ForeColor=   16777215
         stylesets(5).BackColor=   8388608
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmSolicitudInformacion.frx":211E
         stylesets(6).Name=   "SmallFont"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmSolicitudInformacion.frx":213A
         stylesets(7).Name=   "IntHeader"
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmSolicitudInformacion.frx":2156
         stylesets(7).AlignmentPicture=   0
         stylesets(8).Name=   "EnRecOfe"
         stylesets(8).BackColor=   12632256
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmSolicitudInformacion.frx":2780
         stylesets(9).Name=   "PendAsigProve"
         stylesets(9).BackColor=   13171450
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmSolicitudInformacion.frx":279C
         stylesets(10).Name=   "AdjYNotificado"
         stylesets(10).BackColor=   13750691
         stylesets(10).HasFont=   -1  'True
         BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(10).Picture=   "frmSolicitudInformacion.frx":27B8
         stylesets(11).Name=   "Header"
         stylesets(11).ForeColor=   0
         stylesets(11).BackColor=   -2147483633
         stylesets(11).HasFont=   -1  'True
         BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(11).Picture=   "frmSolicitudInformacion.frx":27D4
         stylesets(12).Name=   "PendEnvPet"
         stylesets(12).BackColor=   8454016
         stylesets(12).HasFont=   -1  'True
         BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(12).Picture=   "frmSolicitudInformacion.frx":27F0
         stylesets(13).Name=   "ParcCerrado"
         stylesets(13).HasFont=   -1  'True
         BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(13).Picture=   "frmSolicitudInformacion.frx":280C
         stylesets(14).Name=   "PendValidar"
         stylesets(14).BackColor=   8421631
         stylesets(14).HasFont=   -1  'True
         BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(14).Picture=   "frmSolicitudInformacion.frx":2828
         stylesets(15).Name=   "styAzul"
         stylesets(15).BackColor=   16777153
         stylesets(15).HasFont=   -1  'True
         BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(15).Picture=   "frmSolicitudInformacion.frx":2844
         stylesets(16).Name=   "AdjSinNotificar"
         stylesets(16).BackColor=   12632256
         stylesets(16).HasFont=   -1  'True
         BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(16).Picture=   "frmSolicitudInformacion.frx":2860
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         CellNavigation  =   1
         MaxSelectedRows =   0
         HeadStyleSet    =   "Header"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   10
         Columns(0).Width=   900
         Columns(0).Caption=   "DAnyo"
         Columns(0).Name =   "ANYO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   1032
         Columns(1).Caption=   "DPed"
         Columns(1).Name =   "PED"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1005
         Columns(2).Caption=   "DNum"
         Columns(2).Name =   "NUM"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3995
         Columns(3).Caption=   "DProve"
         Columns(3).Name =   "PROVE"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).Style=   1
         Columns(3).ButtonsAlways=   -1  'True
         Columns(4).Width=   2117
         Columns(4).Caption=   "DFecEmi"
         Columns(4).Name =   "FECEMI"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2514
         Columns(5).Caption=   "DNumExt"
         Columns(5).Name =   "NUMEXT"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   4022
         Columns(6).Caption=   "DEstado"
         Columns(6).Name =   "ESTADO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   2117
         Columns(7).Caption=   "DFecha"
         Columns(7).Name =   "FECHA"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).ButtonsAlways=   -1  'True
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID"
         Columns(8).Name =   "ID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "PROVECOD"
         Columns(9).Name =   "PROVECOD"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         _ExtentX        =   18177
         _ExtentY        =   9816
         _StockProps     =   79
         BackColor       =   -2147483644
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgPedidosNeg 
         Height          =   5565
         Left            =   -74880
         TabIndex        =   6
         Top             =   480
         Width           =   10305
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   10
         stylesets.count =   17
         stylesets(0).Name=   "PendComObj"
         stylesets(0).BackColor=   16744576
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSolicitudInformacion.frx":287C
         stylesets(1).Name=   "PendAdj"
         stylesets(1).BackColor=   8289982
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSolicitudInformacion.frx":2898
         stylesets(2).Name=   "Bold"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSolicitudInformacion.frx":28B4
         stylesets(3).Name=   "Anulado"
         stylesets(3).BackColor=   8421504
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmSolicitudInformacion.frx":28D0
         stylesets(4).Name=   "Normal"
         stylesets(4).BackColor=   16777215
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmSolicitudInformacion.frx":28EC
         stylesets(5).Name=   "SmallFont"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmSolicitudInformacion.frx":2908
         stylesets(6).Name=   "ActiveRow"
         stylesets(6).ForeColor=   16777215
         stylesets(6).BackColor=   8388608
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmSolicitudInformacion.frx":2924
         stylesets(7).Name=   "EnRecOfe"
         stylesets(7).BackColor=   12632256
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmSolicitudInformacion.frx":2940
         stylesets(8).Name=   "IntHeader"
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmSolicitudInformacion.frx":295C
         stylesets(8).AlignmentPicture=   0
         stylesets(9).Name=   "AdjYNotificado"
         stylesets(9).BackColor=   13750691
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmSolicitudInformacion.frx":2F86
         stylesets(10).Name=   "PendAsigProve"
         stylesets(10).BackColor=   13171450
         stylesets(10).HasFont=   -1  'True
         BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(10).Picture=   "frmSolicitudInformacion.frx":2FA2
         stylesets(11).Name=   "ParcCerrado"
         stylesets(11).HasFont=   -1  'True
         BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(11).Picture=   "frmSolicitudInformacion.frx":2FBE
         stylesets(12).Name=   "PendEnvPet"
         stylesets(12).BackColor=   8454016
         stylesets(12).HasFont=   -1  'True
         BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(12).Picture=   "frmSolicitudInformacion.frx":2FDA
         stylesets(13).Name=   "Header"
         stylesets(13).ForeColor=   0
         stylesets(13).BackColor=   -2147483633
         stylesets(13).HasFont=   -1  'True
         BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(13).Picture=   "frmSolicitudInformacion.frx":2FF6
         stylesets(14).Name=   "PendValidar"
         stylesets(14).BackColor=   8421631
         stylesets(14).HasFont=   -1  'True
         BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(14).Picture=   "frmSolicitudInformacion.frx":3012
         stylesets(15).Name=   "AdjSinNotificar"
         stylesets(15).BackColor=   12632256
         stylesets(15).HasFont=   -1  'True
         BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(15).Picture=   "frmSolicitudInformacion.frx":302E
         stylesets(16).Name=   "styAzul"
         stylesets(16).BackColor=   16777153
         stylesets(16).HasFont=   -1  'True
         BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(16).Picture=   "frmSolicitudInformacion.frx":304A
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         CellNavigation  =   1
         MaxSelectedRows =   0
         HeadStyleSet    =   "Header"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   10
         Columns(0).Width=   900
         Columns(0).Caption=   "DAnyo"
         Columns(0).Name =   "ANYO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   1032
         Columns(1).Caption=   "DPed"
         Columns(1).Name =   "PED"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1005
         Columns(2).Caption=   "DNum"
         Columns(2).Name =   "NUM"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3995
         Columns(3).Caption=   "DProve"
         Columns(3).Name =   "PROVE"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).Style=   1
         Columns(3).ButtonsAlways=   -1  'True
         Columns(4).Width=   2117
         Columns(4).Caption=   "DFecEmi"
         Columns(4).Name =   "FECEMI"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2514
         Columns(5).Caption=   "DNumExt"
         Columns(5).Name =   "NUMEXT"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   4022
         Columns(6).Caption=   "DEstado"
         Columns(6).Name =   "ESTADO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   2117
         Columns(7).Caption=   "DFecha"
         Columns(7).Name =   "FECHA"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).ButtonsAlways=   -1  'True
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID"
         Columns(8).Name =   "ID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "PROVECOD"
         Columns(9).Name =   "PROVECOD"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         _ExtentX        =   18168
         _ExtentY        =   9816
         _StockProps     =   79
         BackColor       =   -2147483644
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgPedidosAbiertos 
         Height          =   5565
         Left            =   -74880
         TabIndex        =   7
         Top             =   480
         Width           =   10305
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   10
         stylesets.count =   17
         stylesets(0).Name=   "PendComObj"
         stylesets(0).BackColor=   16744576
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmSolicitudInformacion.frx":3066
         stylesets(1).Name=   "Bold"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmSolicitudInformacion.frx":3082
         stylesets(2).Name=   "PendAdj"
         stylesets(2).BackColor=   8289982
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmSolicitudInformacion.frx":309E
         stylesets(3).Name=   "Normal"
         stylesets(3).BackColor=   16777215
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmSolicitudInformacion.frx":30BA
         stylesets(4).Name=   "Anulado"
         stylesets(4).BackColor=   8421504
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmSolicitudInformacion.frx":30D6
         stylesets(5).Name=   "ActiveRow"
         stylesets(5).ForeColor=   16777215
         stylesets(5).BackColor=   8388608
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmSolicitudInformacion.frx":30F2
         stylesets(6).Name=   "SmallFont"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmSolicitudInformacion.frx":310E
         stylesets(7).Name=   "IntHeader"
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmSolicitudInformacion.frx":312A
         stylesets(7).AlignmentPicture=   0
         stylesets(8).Name=   "EnRecOfe"
         stylesets(8).BackColor=   12632256
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmSolicitudInformacion.frx":3754
         stylesets(9).Name=   "PendAsigProve"
         stylesets(9).BackColor=   13171450
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "frmSolicitudInformacion.frx":3770
         stylesets(10).Name=   "AdjYNotificado"
         stylesets(10).BackColor=   13750691
         stylesets(10).HasFont=   -1  'True
         BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(10).Picture=   "frmSolicitudInformacion.frx":378C
         stylesets(11).Name=   "Header"
         stylesets(11).ForeColor=   0
         stylesets(11).BackColor=   -2147483633
         stylesets(11).HasFont=   -1  'True
         BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(11).Picture=   "frmSolicitudInformacion.frx":37A8
         stylesets(12).Name=   "PendEnvPet"
         stylesets(12).BackColor=   8454016
         stylesets(12).HasFont=   -1  'True
         BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(12).Picture=   "frmSolicitudInformacion.frx":37C4
         stylesets(13).Name=   "ParcCerrado"
         stylesets(13).HasFont=   -1  'True
         BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(13).Picture=   "frmSolicitudInformacion.frx":37E0
         stylesets(14).Name=   "PendValidar"
         stylesets(14).BackColor=   8421631
         stylesets(14).HasFont=   -1  'True
         BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(14).Picture=   "frmSolicitudInformacion.frx":37FC
         stylesets(15).Name=   "styAzul"
         stylesets(15).BackColor=   16777153
         stylesets(15).HasFont=   -1  'True
         BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(15).Picture=   "frmSolicitudInformacion.frx":3818
         stylesets(16).Name=   "AdjSinNotificar"
         stylesets(16).BackColor=   12632256
         stylesets(16).HasFont=   -1  'True
         BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(16).Picture=   "frmSolicitudInformacion.frx":3834
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         CellNavigation  =   1
         MaxSelectedRows =   0
         HeadStyleSet    =   "Header"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   106
         Columns.Count   =   10
         Columns(0).Width=   900
         Columns(0).Caption=   "DAnyo"
         Columns(0).Name =   "ANYO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   1032
         Columns(1).Caption=   "DPed"
         Columns(1).Name =   "PED"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1005
         Columns(2).Caption=   "DNum"
         Columns(2).Name =   "NUM"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3995
         Columns(3).Caption=   "DProve"
         Columns(3).Name =   "PROVE"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).Style=   1
         Columns(3).ButtonsAlways=   -1  'True
         Columns(4).Width=   2117
         Columns(4).Caption=   "DFecEmi"
         Columns(4).Name =   "FECEMI"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2514
         Columns(5).Caption=   "DNumExt"
         Columns(5).Name =   "NUMEXT"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   4022
         Columns(6).Caption=   "DEstado"
         Columns(6).Name =   "ESTADO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   2117
         Columns(7).Caption=   "DFecha"
         Columns(7).Name =   "FECHA"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).ButtonsAlways=   -1  'True
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID"
         Columns(8).Name =   "ID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "PROVECOD"
         Columns(9).Name =   "PROVECOD"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         _ExtentX        =   18168
         _ExtentY        =   9816
         _StockProps     =   79
         BackColor       =   -2147483644
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmSolicitudInformacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_oSolicitudSeleccionada As CInstancia

'Variables privadas
Private m_sIdiEst(1 To 10) As String
Private m_sEstborrado As String
Private m_sIdiEstPedido(1 To 10) As String
Private m_bAperturaProc As Boolean
Private m_bControlDblClick As Boolean

Private Sub Form_Load()
    Me.Height = 6780
    Me.Width = 10770
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    If Not gParametrosGenerales.gbActPedAbierto Then Me.ssTabDetalle.TabVisible(6) = False
    
    PonerFieldSeparator Me
    
    CargarGridProcesos
End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOLICITUD_INF, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value  '1 Informaci�n de procesos de compra/pedidos de solicitud
        Ador.MoveNext
        ssTabDetalle.TabCaption(0) = Ador(0).Value   '2  Procesos
        Ador.MoveNext
        ssTabDetalle.TabCaption(1) = Ador(0).Value   '3  Publicaciones
        Ador.MoveNext
        ssTabDetalle.TabCaption(2) = Ador(0).Value   '4  Pedidos de cat�logo
        Ador.MoveNext
        ssTabDetalle.TabCaption(4) = Ador(0).Value   '5 Pedidos negociados
        
        Ador.MoveNext
        sdbgProcesos.Columns("ANYO").caption = Ador(0).Value  '6  A�o
        Ador.MoveNext
        sdbgProcesos.Columns("GMN1").caption = Ador(0).Value  '7  MAt
        Ador.MoveNext
        sdbgProcesos.Columns("COD").caption = Ador(0).Value  '8  Cod.
        Ador.MoveNext
        sdbgProcesos.Columns("DESCR").caption = Ador(0).Value  '9  Descripci�n
        Ador.MoveNext
        sdbgProcesos.Columns("EST").caption = Ador(0).Value  '10  Estado
        Ador.MoveNext
        sdbgProcesos.Columns("RESP").caption = Ador(0).Value  '11  Resp.
        Ador.MoveNext
        sdbgProcesos.Columns("ADJDIR").caption = Ador(0).Value  '12  Dir.
        Ador.MoveNext
        sdbgProcesos.Columns("FECPRES").caption = Ador(0).Value  '13  Fec.Pres
        
        Ador.MoveNext
        sdbgPublicaciones.Columns("PUB").caption = Ador(0).Value  '14  Pub.
        Ador.MoveNext
        sdbgPublicaciones.Columns("DESPUB").caption = Ador(0).Value  '15  Despublicaci�n.
        Ador.MoveNext
        sdbgPublicaciones.Columns("PROCE").caption = Ador(0).Value  '16  Proceso
        Ador.MoveNext
        sdbgPublicaciones.Columns("ART").caption = Ador(0).Value  '17  Art�culo
        Ador.MoveNext
        sdbgPublicaciones.Columns("PROVE").caption = Ador(0).Value  '18  Proveedor
        Ador.MoveNext
        sdbgPublicaciones.Columns("DEST").caption = Ador(0).Value  '19  Destino
        Ador.MoveNext
        sdbgPublicaciones.Columns("CANTADJ").caption = Ador(0).Value  '20  Cant.
        Ador.MoveNext
        sdbgPublicaciones.Columns("UB").caption = Ador(0).Value  '21  U.base
        Ador.MoveNext
        sdbgPublicaciones.Columns("PrecioUB").caption = Ador(0).Value  '22  Precio
        Ador.MoveNext
        sdbgPublicaciones.Columns("UP").caption = Ador(0).Value  '23  UP
        Ador.MoveNext
        sdbgPublicaciones.Columns("FC").caption = Ador(0).Value  '24  FC
        Ador.MoveNext
        sdbgPublicaciones.Columns("OTRAS").caption = Ador(0).Value  '25  Otras
        Ador.MoveNext
        sdbgPublicaciones.Columns("CANTMIN").caption = Ador(0).Value  '26  Cant. m�n. ped.
        
        Ador.MoveNext
        m_sIdiEst(1) = Ador(0).Value  '27  Pendiente de validar apertura
        Ador.MoveNext
        m_sIdiEst(2) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(3) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(4) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(5) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(6) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(7) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(8) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(9) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(10) = Ador(0).Value  '36 Anulado
        
        Ador.MoveNext
        m_sIdiEstPedido(1) = Ador(0).Value '37 Pendiente de aprobar
        Ador.MoveNext
        m_sIdiEstPedido(2) = Ador(0).Value '38 Aprobada
        Ador.MoveNext
        m_sIdiEstPedido(3) = Ador(0).Value '39  Recibido parcialmente
        Ador.MoveNext
        m_sIdiEstPedido(4) = Ador(0).Value '40  Recibido totalmente
        Ador.MoveNext
        m_sIdiEstPedido(5) = Ador(0).Value '41  Denegado
        Ador.MoveNext
        m_sIdiEstPedido(6) = Ador(0).Value '42  Esperando aprobacion de compras
        
        Ador.MoveNext
        sdbgPedidosDir.Columns("ANYO").caption = Ador(0).Value  '43 a�o
        sdbgPedidosCat.Columns("ANYO").caption = Ador(0).Value
        sdbgPedidosLibres.Columns("ANYO").caption = Ador(0).Value
        sdbgPedidosNeg.Columns("ANYO").caption = Ador(0).Value
        sdbgPedidosAbiertos.Columns("ANYO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPedidosDir.Columns("PED").caption = Ador(0).Value   '44 Pedido
        sdbgPedidosCat.Columns("PED").caption = Ador(0).Value
        sdbgPedidosLibres.Columns("PED").caption = Ador(0).Value
        sdbgPedidosNeg.Columns("PED").caption = Ador(0).Value
        sdbgPedidosAbiertos.Columns("PED").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPedidosDir.Columns("NUM").caption = Ador(0).Value   '45 Orden
        sdbgPedidosCat.Columns("NUM").caption = Ador(0).Value
        sdbgPedidosLibres.Columns("NUM").caption = Ador(0).Value
        sdbgPedidosNeg.Columns("NUM").caption = Ador(0).Value
        sdbgPedidosAbiertos.Columns("NUM").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPedidosDir.Columns("PROVE").caption = Ador(0).Value  '46 Proveedor
        sdbgPedidosCat.Columns("PROVE").caption = Ador(0).Value
        sdbgPedidosLibres.Columns("PROVE").caption = Ador(0).Value
        sdbgPedidosNeg.Columns("PROVE").caption = Ador(0).Value
        sdbgPedidosAbiertos.Columns("PROVE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPedidosDir.Columns("FECEMI").caption = Ador(0).Value  '47 Fec.Emisi�n
        sdbgPedidosCat.Columns("FECEMI").caption = Ador(0).Value
        sdbgPedidosLibres.Columns("FECEMI").caption = Ador(0).Value
        sdbgPedidosNeg.Columns("FECEMI").caption = Ador(0).Value
        sdbgPedidosAbiertos.Columns("FECEMI").caption = Ador(0).Value
        Ador.MoveNext
        If gParametrosGenerales.gbVerNumeroProveedor Then
            sdbgPedidosDir.Columns("NUMEXT").caption = Ador(0).Value  '48 N�m.externo
            sdbgPedidosCat.Columns("NUMEXT").caption = Ador(0).Value
            sdbgPedidosLibres.Columns("NUMEXT").caption = Ador(0).Value
            sdbgPedidosNeg.Columns("NUMEXT").caption = Ador(0).Value
            sdbgPedidosAbiertos.Columns("NUMEXT").caption = Ador(0).Value
        Else
            sdbgPedidosDir.Columns("NUMEXT").Visible = False
            sdbgPedidosCat.Columns("NUMEXT").Visible = False
            sdbgPedidosLibres.Columns("NUMEXT").Visible = False
            sdbgPedidosNeg.Columns("NUMEXT").Visible = False
            sdbgPedidosAbiertos.Columns("NUMEXT").Visible = False
        End If
        Ador.MoveNext
        sdbgPedidosDir.Columns("ESTADO").caption = Ador(0).Value  '49 Estado actual
        sdbgPedidosCat.Columns("ESTADO").caption = Ador(0).Value
        sdbgPedidosLibres.Columns("ESTADO").caption = Ador(0).Value
        sdbgPedidosNeg.Columns("ESTADO").caption = Ador(0).Value
        sdbgPedidosAbiertos.Columns("ESTADO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPedidosDir.Columns("FECHA").caption = Ador(0).Value   '50 Fec.cambio est.
        sdbgPedidosCat.Columns("FECHA").caption = Ador(0).Value
        sdbgPedidosLibres.Columns("FECHA").caption = Ador(0).Value
        sdbgPedidosNeg.Columns("FECHA").caption = Ador(0).Value
        sdbgPedidosAbiertos.Columns("FECHA").caption = Ador(0).Value
        
        Ador.MoveNext
        m_sIdiEstPedido(7) = Ador(0).Value '51 Recibido y cerrado
        Ador.MoveNext
        m_sIdiEstPedido(8) = Ador(0).Value '52 Anulado
        Ador.MoveNext
        m_sIdiEstPedido(9) = Ador(0).Value '53  Rechazado por proveedor
        Ador.MoveNext
        m_sIdiEstPedido(10) = Ador(0).Value '54  Denegado total por aprobador
            
        Ador.MoveNext
        ssTabDetalle.TabCaption(5) = Ador(0).Value '55 Pedidos libres
        Ador.MoveNext
        ssTabDetalle.TabCaption(3) = Ador(0).Value   '56 Pedidos directps
        Ador.MoveNext
        ssTabDetalle.TabCaption(6) = Ador(0).Value   '56 Pedidos abiertos
        
        Ador.MoveNext
        m_sEstborrado = Ador(0).Value   '58 Borrado  (Pedido borrado logico)
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oSolicitudSeleccionada = Nothing
End Sub

Private Sub sdbgPedidosAbiertos_BtnClick()
    If sdbgPedidosAbiertos.col = -1 Then Exit Sub
      
    If (Left(sdbgPedidosAbiertos.Columns(sdbgPedidosAbiertos.col).Name, 5) = "PROVE") Then
        MostrarDetalleProveedor (sdbgPedidosAbiertos.Columns("PROVECOD").Value)
    End If
End Sub

Private Sub sdbgPedidosCat_BtnClick()
    If sdbgPedidosCat.col = -1 Then Exit Sub
      
    If (Left(sdbgPedidosCat.Columns(sdbgPedidosCat.col).Name, 5) = "PROVE") Then
        MostrarDetalleProveedor (sdbgPedidosCat.Columns("PROVECOD").Value)
    End If
End Sub

Private Sub sdbgPedidosDir_BtnClick()
    If sdbgPedidosDir.col = -1 Then Exit Sub
      
    If (Left(sdbgPedidosDir.Columns(sdbgPedidosDir.col).Name, 5) = "PROVE") Then
        MostrarDetalleProveedor (sdbgPedidosDir.Columns("PROVECOD").Value)
    End If
End Sub
Private Sub sdbgPedidosneg_BtnClick()
    If sdbgPedidosNeg.col = -1 Then Exit Sub
      
    If (Left(sdbgPedidosNeg.Columns(sdbgPedidosNeg.col).Name, 5) = "PROVE") Then
        MostrarDetalleProveedor (sdbgPedidosNeg.Columns("PROVECOD").Value)
    End If
End Sub

Private Sub sdbgPedidosLibres_BtnClick()
    If sdbgPedidosLibres.col = -1 Then Exit Sub
      
    If (Left(sdbgPedidosLibres.Columns(sdbgPedidosLibres.col).Name, 5) = "PROVE") Then
        MostrarDetalleProveedor (sdbgPedidosLibres.Columns("PROVECOD").Value)
    End If
End Sub

Private Sub sdbgProcesos_BtnClick()
    'Muestra el detalle del responsable del proceso
    If sdbgProcesos.col = -1 Then Exit Sub
    
    If sdbgProcesos.Columns(sdbgProcesos.col).Name = "RESP" Then
        If sdbgProcesos.Columns("RESP").Value = "" Then Exit Sub
        
        frmDetallePersona.g_sPersona = sdbgProcesos.Columns("RESP").Value
        frmDetallePersona.Show vbModal
    End If
End Sub

Private Sub sdbgProcesos_DblClick()
    'Si se hace dobleClock en una fila de la grid abre la apertura de procesos
    'correspondiente al proceso seleccionado

    If sdbgProcesos.Rows = 0 Then Exit Sub
    
    If m_bAperturaProc = True And Not m_bControlDblClick Then
        m_bControlDblClick = True
        MostrarDetalleProceso sdbgProcesos.Columns("ANYO").Value, sdbgProcesos.Columns("GMN1").Value, sdbgProcesos.Columns("COD").Value
        m_bControlDblClick = False
        Unload Me
    End If
    
End Sub

Private Sub sdbgPublicaciones_BtnClick()
    'Muestra el detalle del proveedor de la l�nea del cat�logo
    If sdbgPublicaciones.Columns(sdbgPublicaciones.col).Name = "PROVE" Then
        If sdbgPublicaciones.Columns("PROVE").Value = "" Then Exit Sub
        MostrarDetalleProveedor sdbgPublicaciones.Columns("PROVE").Value
    End If
        
End Sub


Private Sub ssTabDetalle_Click(PreviousTab As Integer)
    Select Case ssTabDetalle.Tab
        Case 0
            'Procesos
            CargarGridProcesos
        Case 1
            'Publicaciones
            CargarGridPublicaciones
        Case 2
            'Cat�logos
            CargarGridPedidos (ssTabDetalle.Tab)
        Case 3
            'Pedidos directos
            CargarGridPedidos (ssTabDetalle.Tab)
        Case 4
            'Pedidos negociados
            CargarGridPedidos (ssTabDetalle.Tab)
        Case 5
            'Pedidos libres
            CargarGridPedidos (ssTabDetalle.Tab)
        Case 6
            'Pedidos abiertos
            CargarGridPedidos (ssTabDetalle.Tab)
    End Select
End Sub

Private Sub CargarGridProcesos()
    Dim Ador As Ador.Recordset
    Dim sestado As String
    Dim sFecha As Variant
    
    'Carga los procesos relacionados con la solicitud
    sdbgProcesos.RemoveAll
    
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    Set Ador = g_oSolicitudSeleccionada.DevolverProcesosRelacionados
    If Not Ador Is Nothing Then
        While Not Ador.EOF
        
            Select Case Ador("EST").Value

                Case TipoEstadoProceso.sinitems, TipoEstadoProceso.ConItemsSinValidar
                    sestado = m_sIdiEst(1)
                        
                Case TipoEstadoProceso.validado, TipoEstadoProceso.Conproveasignados
                    sestado = m_sIdiEst(2)
                        
                Case TipoEstadoProceso.conasignacionvalida
                    sestado = m_sIdiEst(3)
                
                Case TipoEstadoProceso.conpeticiones, TipoEstadoProceso.conofertas
                    sestado = m_sIdiEst(4)
                        
                Case TipoEstadoProceso.ConObjetivosSinNotificar, TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado
                    sestado = m_sIdiEst(5)
                
                Case TipoEstadoProceso.PreadjYConObjNotificados
                    sestado = m_sIdiEst(6)
                
                Case TipoEstadoProceso.ParcialmenteCerrado
                    sestado = m_sIdiEst(7)
                    
                Case TipoEstadoProceso.conadjudicaciones
                    sestado = m_sIdiEst(8)
                
                Case TipoEstadoProceso.ConAdjudicacionesNotificadas
                    sestado = m_sIdiEst(9)
                
                Case TipoEstadoProceso.Cerrado
                    sestado = m_sIdiEst(10)
                
            End Select
                    
            If IsNull(Ador("FECPRES").Value) Then
                sFecha = ""
            Else
                sFecha = Format(Ador("FECPRES").Value, "Short date")
            End If
            
            sdbgProcesos.AddItem Ador("ANYO").Value & Chr(m_lSeparador) & Ador("GMN1").Value & Chr(m_lSeparador) & Ador("COD").Value & Chr(m_lSeparador) & Ador("DEN").Value & Chr(m_lSeparador) & sestado & Chr(m_lSeparador) & NullToStr(Ador("COM").Value) & Chr(m_lSeparador) & Ador("ADJDIR").Value & Chr(m_lSeparador) & Ador("EST").Value & Chr(m_lSeparador) & sFecha
            Ador.MoveNext
        Wend
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub CargarGridPublicaciones()
    Dim Ador As Ador.Recordset
    Dim sFecha As String
    Dim sProceso As String
    
    'Carga los procesos relacionados con la solicitud
    sdbgPublicaciones.RemoveAll
    
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    Set Ador = g_oSolicitudSeleccionada.DevolverArticulosCatRelacionados
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            
            If IsNull(Ador("FECHA_DESPUB").Value) Then
                sFecha = ""
            Else
                sFecha = Format(Ador("FECHA_DESPUB").Value, "Short date")
            End If
            
            If IsNull(Ador("ANYO").Value) Then
                sProceso = ""
            Else
                sProceso = Ador("ANYO").Value & "/" & Ador("GMN1").Value & "/" & Ador("PROCE").Value
            End If
            
            sdbgPublicaciones.AddItem Ador("PUB").Value & Chr(m_lSeparador) & sFecha & Chr(m_lSeparador) & sProceso & Chr(m_lSeparador) & NullToStr(Ador("DENART").Value) & Chr(m_lSeparador) & NullToStr(Ador("PROVE").Value) & Chr(m_lSeparador) & NullToStr(Ador("DEST").Value) & Chr(m_lSeparador) & NullToStr(Ador("CANT_ADJ").Value) & Chr(m_lSeparador) & NullToStr(Ador("UC").Value) & Chr(m_lSeparador) & NullToStr(Ador("PRECIO").Value) & Chr(m_lSeparador) & NullToStr(Ador("UP_DEF").Value) & Chr(m_lSeparador) & NullToStr(Ador("FC_DEF").Value) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(Ador("CANT_MIN_DEF").Value) & Chr(m_lSeparador) & Ador("ID").Value & Chr(m_lSeparador) & Ador("ANYO").Value & Chr(m_lSeparador) & Ador("GMN1").Value & Chr(m_lSeparador) & Ador("PROCE").Value & Chr(m_lSeparador) & NullToStr(Ador("CODART").Value)
            Ador.MoveNext
        Wend
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub MostrarDetalleProveedor(ByVal CodProve As String)
Dim oProves As CProveedores
Dim oProve As CProveedor
Dim oCon As CContacto
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    
   oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    
    Set frmProveDetalle.g_oProveSeleccionado = oProve
    frmProveDetalle.caption = CodProve & "  " & oProve.Den
    frmProveDetalle.lblCodPortProve = NullToStr(oProve.CodPortal)
    frmProveDetalle.g_bPremium = oProve.EsPremium
    frmProveDetalle.g_bActivo = oProve.EsPremiumActivo
    frmProveDetalle.lblDIR = NullToStr(oProve.Direccion)
    frmProveDetalle.lblCP = NullToStr(oProve.cP)
    frmProveDetalle.lblPOB = NullToStr(oProve.Poblacion)
    frmProveDetalle.lblPaiCod = NullToStr(oProve.CodPais)
    frmProveDetalle.lblPaiDen = NullToStr(oProve.DenPais)
    frmProveDetalle.lblMonCod = NullToStr(oProve.CodMon)
    frmProveDetalle.lblMonDen = NullToStr(oProve.DenMon)
    frmProveDetalle.lblProviCod = NullToStr(oProve.CodProvi)
    frmProveDetalle.lblProviDen = NullToStr(oProve.DenProvi)
    frmProveDetalle.lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    frmProveDetalle.lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    frmProveDetalle.lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If Trim(oProve.Calif1) <> "" Then
        frmProveDetalle.lblCal1Den = oProve.Calif1
    End If
    
    If Trim(oProve.Calif2) <> "" Then
        frmProveDetalle.lblcal2den = oProve.Calif2
    End If
    
    If Trim(oProve.Calif3) <> "" Then
        frmProveDetalle.lblcal3den = oProve.Calif3
    End If
    
    frmProveDetalle.lblCal1Val = DblToStr(oProve.Val1)
    frmProveDetalle.lblCal2Val = DblToStr(oProve.Val2)
    frmProveDetalle.lblCal3Val = DblToStr(oProve.Val3)

    frmProveDetalle.lblURL = NullToStr(oProve.URLPROVE)

    oProve.CargarTodosLosContactos , , , , True, , , , , , , True
    
    For Each oCon In oProve.Contactos
        
        frmProveDetalle.sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
        & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
    
    Next
        
    Set oProves = Nothing
    Set oProve = Nothing
    
    
    frmProveDetalle.Show 1
    
End Sub

Public Sub MostrarDetalleProceso(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal ProceCod As Long)
        
    frmPROCE.g_sOrigen = "frmSolicitudDetalle"
    frmPROCE.sdbcAnyo.Value = Anyo
    frmPROCE.sdbcGMN1_4Cod.Value = GMN1
    frmPROCE.GMN1Seleccionado
    frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSTodos
    frmPROCE.sdbcProceCod.Value = ProceCod
    frmPROCE.sdbcProceCod_Validate False

End Sub


Private Sub CargarGridPedidos(ByVal iTab As Integer)
    Dim Ador As Ador.Recordset
    Dim sestado As String
    Dim sFecEmision As String
    Dim sFecha As String
    Dim sProve As String
    Dim sCadena As String
    Dim i As Integer
    
    If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
    
    'Carga los procesos relacionados con la solicitud
    Select Case iTab
        Case 2 'Pedidos de cat�logo
            sdbgPedidosCat.RemoveAll
            Set Ador = g_oSolicitudSeleccionada.DevolverPedidosRelacionados(1)
            
        Case 3 ' Pedidos directos
            sdbgPedidosDir.RemoveAll
            Set Ador = g_oSolicitudSeleccionada.DevolverPedidosRelacionados(2)
            
        Case 4 'Pedidos negociados
            sdbgPedidosNeg.RemoveAll
            Set Ador = g_oSolicitudSeleccionada.DevolverPedidosRelacionados(0)
        Case 5 'Pedidos libres
            sdbgPedidosLibres.RemoveAll
            Set Ador = g_oSolicitudSeleccionada.DevolverPedidosRelacionados(, True)
        Case 6 'Pedidos abiertos
            sdbgPedidosAbiertos.RemoveAll
            Set Ador = g_oSolicitudSeleccionada.DevolverPedidosRelacionados(5)
    End Select
    
    
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            Select Case Ador("EST").Value
                Case TipoEstadoOrdenEntrega.PendienteDeAprobacion
                    sestado = m_sIdiEstPedido(1)
                    
                Case TipoEstadoOrdenEntrega.DenegadoParcialAprob
                    sestado = m_sIdiEstPedido(2)
                    
                Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                    sestado = m_sIdiEstPedido(3)
                    
                Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                    sestado = m_sIdiEstPedido(4)
                    
                Case TipoEstadoOrdenEntrega.EnCamino
                    sestado = m_sIdiEstPedido(5)
                    
                Case TipoEstadoOrdenEntrega.EnRecepcion
                    sestado = m_sIdiEstPedido(6)
                        
                Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                    sestado = m_sIdiEstPedido(7)
                    
                Case TipoEstadoOrdenEntrega.Anulado
                    sestado = m_sIdiEstPedido(8)
                    
                Case TipoEstadoOrdenEntrega.RechazadoPorProveedor
                    sestado = m_sIdiEstPedido(9)
                    
                Case TipoEstadoOrdenEntrega.DenegadoTotalAprobador
                    sestado = m_sIdiEstPedido(10)
            End Select
            
            If IsNull(Ador("PROVE").Value) Then
                sProve = ""
            Else
                sProve = Ador("PROVE").Value & "-" & NullToStr(Ador("DEN").Value)
            End If
            
            If IsNull(Ador("FECEMISION").Value) Then
                sFecEmision = ""
            Else
                sFecEmision = Format(Ador("FECEMISION").Value, "Short date")
            End If
            
            If IsNull(Ador("FECHA").Value) Then
                sFecha = ""
            Else
                sFecha = Format(Ador("FECHA").Value, "Short date")
            End If
            
            If gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS Then
                If Ador("BAJA_LOG").Value = 1 Then
                    sestado = m_sEstborrado
                    sFecha = Format(Ador("FEC_BAJA_LOG").Value, "Short date")
                End If
            End If
            
            sCadena = Ador("ANYO").Value & Chr(m_lSeparador) & Ador("PNUM").Value & Chr(m_lSeparador) & Ador("ONUM").Value & Chr(m_lSeparador) & sProve & Chr(m_lSeparador) & sFecEmision & Chr(m_lSeparador) & NullToStr(Ador("NUMEXT").Value) & Chr(m_lSeparador) & sestado & Chr(m_lSeparador) & sFecha & Chr(m_lSeparador) & Ador("ID").Value & Chr(m_lSeparador) & NullToStr(Ador("PROVE").Value)
            
            If Not gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS And Ador("BAJA_LOG") = 1 Then
                Ador.MoveNext
            Else
                Select Case iTab
                    Case 2 'Pedidos de cat�logo
                        sdbgPedidosCat.AddItem sCadena
                    Case 3 ' Pedidos directos
                        sdbgPedidosDir.AddItem sCadena
                    Case 4 ' Pedidos negociados
                        sdbgPedidosNeg.AddItem sCadena
                    Case 5 'Pedidos libres
                        sdbgPedidosLibres.AddItem sCadena
                    Case 6 'Pedidos abiertos
                        sdbgPedidosAbiertos.AddItem sCadena
                End Select
        
                Ador.MoveNext
            End If
        Wend
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub


Private Sub ConfigurarSeguridad()
    'Si no es el usuario administrador
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        'consultar apertura de procesos
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing) Then
            m_bAperturaProc = True
        End If
        
    Else
        m_bAperturaProc = True
    End If
   

End Sub

