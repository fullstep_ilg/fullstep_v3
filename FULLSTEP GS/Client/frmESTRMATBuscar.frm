VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmESTRMATBuscar 
   BackColor       =   &H00808000&
   Caption         =   "Buscar material"
   ClientHeight    =   7350
   ClientLeft      =   4095
   ClientTop       =   2235
   ClientWidth     =   8970
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRMATBuscar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7350
   ScaleWidth      =   8970
   Begin TabDlg.SSTab sstabArticulos 
      Height          =   7095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   12515
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BackColor       =   8421376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos de busqueda"
      TabPicture(0)   =   "frmESTRMATBuscar.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraDatosBusqueda"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Art�culos"
      TabPicture(1)   =   "frmESTRMATBuscar.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraArticulos"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraDatosBusqueda 
         Height          =   6615
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   8415
         Begin VB.CommandButton cmdLimpiarUon 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7515
            Picture         =   "frmESTRMATBuscar.frx":0182
            Style           =   1  'Graphical
            TabIndex        =   40
            TabStop         =   0   'False
            Top             =   960
            Width           =   345
         End
         Begin VB.CommandButton cmdBuscarUon 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7890
            Picture         =   "frmESTRMATBuscar.frx":0227
            Style           =   1  'Graphical
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   960
            Width           =   345
         End
         Begin VB.CheckBox chkArtiCent 
            Caption         =   "dBuscar solo art�culos centrales"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   2520
            TabIndex        =   38
            Top             =   240
            Width           =   2685
         End
         Begin VB.CheckBox chkArti 
            Caption         =   "Buscar solo art�culos"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   120
            TabIndex        =   33
            Top             =   240
            Value           =   1  'Checked
            Width           =   2090
         End
         Begin VB.Frame fraRecepcion 
            BorderStyle     =   0  'None
            Caption         =   "DRecepci�n"
            ForeColor       =   &H00000000&
            Height          =   1245
            Left            =   5640
            TabIndex        =   26
            Top             =   5280
            Width           =   2115
            Begin VB.CheckBox chkRecep 
               Caption         =   "DOpcional"
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   2
               Left            =   120
               TabIndex        =   29
               Top             =   990
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               Caption         =   "DNo recepcionar"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   28
               Top             =   690
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               Caption         =   "DObligatoria"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   27
               Top             =   390
               Width           =   1935
            End
            Begin VB.Label lblRecepcion 
               Caption         =   "DRecepci�n"
               Height          =   255
               Left            =   120
               TabIndex        =   32
               Top             =   0
               Width           =   1215
            End
         End
         Begin VB.Frame fraAlmacen 
            BorderStyle     =   0  'None
            Caption         =   "DAlmacenamiento"
            ForeColor       =   &H00000000&
            Height          =   1245
            Left            =   2880
            TabIndex        =   22
            Top             =   5280
            Width           =   2115
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "DOpcional"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   25
               Top             =   990
               Width           =   1875
            End
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "DNo almacenar"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   24
               Top             =   690
               Width           =   1935
            End
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "DObligatorio"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   23
               Top             =   390
               Width           =   1905
            End
            Begin VB.Label lblAlmacenamiento 
               Caption         =   "DAlmacenamiento"
               Height          =   375
               Left            =   120
               TabIndex        =   31
               Top             =   0
               Width           =   1575
            End
         End
         Begin VB.Frame fraConcepto 
            BorderStyle     =   0  'None
            Caption         =   "DConcepto"
            ForeColor       =   &H00000000&
            Height          =   1245
            Left            =   120
            TabIndex        =   18
            Top             =   5280
            Width           =   2115
            Begin VB.CheckBox chkConcepto 
               Caption         =   "DGasto/Inversi�n"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   21
               Top             =   990
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               Caption         =   "DInversi�n"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   20
               Top             =   690
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               Caption         =   "DGasto"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   19
               Top             =   390
               Width           =   1935
            End
            Begin VB.Label lblConcepto 
               Caption         =   "DConcepto"
               Height          =   255
               Left            =   120
               TabIndex        =   30
               Top             =   0
               Width           =   975
            End
         End
         Begin VB.CheckBox chkArtiGen 
            Caption         =   "Buscar s�lo art�culos genericos"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   5400
            TabIndex        =   10
            Top             =   240
            Width           =   2850
         End
         Begin VB.CommandButton cmdAyuda 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   7.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   8010
            Picture         =   "frmESTRMATBuscar.frx":0293
            Style           =   1  'Graphical
            TabIndex        =   9
            Top             =   1500
            Width           =   225
         End
         Begin VB.TextBox txtCod 
            Height          =   315
            Left            =   1200
            TabIndex        =   8
            Top             =   1440
            Width           =   1605
         End
         Begin VB.TextBox txtDen 
            Height          =   315
            Left            =   4200
            TabIndex        =   7
            Top             =   1440
            Width           =   3750
         End
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7890
            Picture         =   "frmESTRMATBuscar.frx":04C4
            Style           =   1  'Graphical
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   600
            Width           =   345
         End
         Begin VB.CommandButton cmdBorrar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7520
            Picture         =   "frmESTRMATBuscar.frx":0530
            Style           =   1  'Graphical
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   600
            Width           =   345
         End
         Begin VB.CommandButton cmdBuscaAtrib 
            Height          =   285
            Left            =   7920
            Picture         =   "frmESTRMATBuscar.frx":05D5
            Style           =   1  'Graphical
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   2040
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddOper 
            Height          =   915
            Left            =   1560
            TabIndex        =   2
            Top             =   3000
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRMATBuscar.frx":0662
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
            Height          =   915
            Left            =   3720
            TabIndex        =   3
            Top             =   3000
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRMATBuscar.frx":067E
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
            Height          =   2535
            Left            =   120
            TabIndex        =   11
            Top             =   2520
            Width           =   8145
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            RecordSelectors =   0   'False
            Col.Count       =   12
            stylesets.count =   3
            stylesets(0).Name=   "Yellow"
            stylesets(0).BackColor=   11862015
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRMATBuscar.frx":069A
            stylesets(1).Name=   "Normal"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   16777215
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmESTRMATBuscar.frx":06B6
            stylesets(2).Name=   "Header"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmESTRMATBuscar.frx":06D2
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            HeadStyleSet    =   "Header"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   12
            Columns(0).Width=   979
            Columns(0).Name =   "USAR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Style=   2
            Columns(1).Width=   3201
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   4419
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   1058
            Columns(3).Caption=   "OPER"
            Columns(3).Name =   "OPER"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3889
            Columns(4).Caption=   "VALOR"
            Columns(4).Name =   "VALOR"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "INTRO"
            Columns(5).Name =   "INTRO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "IDTIPO"
            Columns(6).Name =   "IDTIPO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "ID_ATRIB"
            Columns(7).Name =   "ID_ATRIB"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "MAXIMO"
            Columns(8).Name =   "MAXIMO"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "MINIMO"
            Columns(9).Name =   "MINIMO"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "VALOR_ATRIB"
            Columns(10).Name=   "VALOR_ATRIB"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "MAT"
            Columns(11).Name=   "MAT"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            _ExtentX        =   14367
            _ExtentY        =   4471
            _StockProps     =   79
            BackColor       =   -2147483633
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblUnidadOrg 
            BackStyle       =   0  'Transparent
            Caption         =   "dUnidad Org."
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   120
            TabIndex        =   42
            Top             =   1005
            Width           =   1170
         End
         Begin VB.Label txtUonSeleccionada 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   1200
            TabIndex        =   41
            Top             =   960
            Width           =   6240
         End
         Begin VB.Label lblCod 
            Caption         =   "C�digo:"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   120
            TabIndex        =   16
            Top             =   1515
            Width           =   885
         End
         Begin VB.Label lblDen 
            Caption         =   "Denominaci�n:"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   3000
            TabIndex        =   15
            Top             =   1515
            Width           =   1470
         End
         Begin VB.Label lblMaterial 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   1200
            TabIndex        =   14
            Top             =   600
            Width           =   6240
         End
         Begin VB.Label lblMat 
            BackStyle       =   0  'Transparent
            Caption         =   "Material:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   120
            TabIndex        =   13
            Top             =   645
            Width           =   1050
         End
         Begin VB.Label lblBusqAtrib 
            Caption         =   "B�squeda por atributos"
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   2160
            Width           =   1935
         End
      End
      Begin VB.Frame fraArticulos 
         BorderStyle     =   0  'None
         Height          =   5775
         Left            =   -74880
         TabIndex        =   34
         Top             =   360
         Width           =   8535
         Begin VB.CommandButton cmdCerrar 
            Cancel          =   -1  'True
            Caption         =   "&Cerrar"
            Height          =   315
            Left            =   4395
            TabIndex        =   37
            Top             =   5400
            Width           =   1125
         End
         Begin VB.CommandButton cmdSeleccionar 
            Caption         =   "&Seleccionar"
            Height          =   315
            Left            =   3000
            TabIndex        =   36
            Top             =   5400
            Width           =   1125
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgMateriales 
            Height          =   5100
            Left            =   0
            TabIndex        =   35
            Top             =   120
            Width           =   8460
            _Version        =   196617
            DataMode        =   2
            RecordSelectors =   0   'False
            Col.Count       =   9
            stylesets.count =   4
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRMATBuscar.frx":06EE
            stylesets(1).Name=   "ActiveRow"
            stylesets(1).ForeColor=   16777215
            stylesets(1).BackColor=   8421376
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmESTRMATBuscar.frx":070A
            stylesets(2).Name=   "ActiveRowBlue"
            stylesets(2).ForeColor=   16777215
            stylesets(2).BackColor=   8388608
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmESTRMATBuscar.frx":0726
            stylesets(3).Name=   "Adjudica"
            stylesets(3).ForeColor=   -2147483632
            stylesets(3).BackColor=   -2147483633
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmESTRMATBuscar.frx":0742
            stylesets(3).AlignmentText=   2
            stylesets(3).AlignmentPicture=   1
            AllowUpdate     =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ActiveRowStyleSet=   "ActiveRowBlue"
            Columns.Count   =   9
            Columns(0).Width=   1270
            Columns(0).Caption=   "GMN1"
            Columns(0).Name =   "GMN1"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16449500
            Columns(0).StyleSet=   "Normal"
            Columns(1).Width=   1455
            Columns(1).Caption=   "GMN2"
            Columns(1).Name =   "GMN2"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16449500
            Columns(1).StyleSet=   "Normal"
            Columns(2).Width=   1720
            Columns(2).Caption=   "GMN3"
            Columns(2).Name =   "GMN3"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).HasBackColor=   -1  'True
            Columns(2).BackColor=   16449500
            Columns(3).Width=   1984
            Columns(3).Caption=   "GMN4"
            Columns(3).Name =   "GMN4"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).HasBackColor=   -1  'True
            Columns(3).BackColor=   16449500
            Columns(3).StyleSet=   "Normal"
            Columns(4).Width=   2408
            Columns(4).Caption=   "Art�culo"
            Columns(4).Name =   "COD"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).StyleSet=   "Normal"
            Columns(5).Width=   5371
            Columns(5).Caption=   "Denominaci�n"
            Columns(5).Name =   "DEN"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   2037
            Columns(6).Caption=   "ART"
            Columns(6).Name =   "ART"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Caption=   "AGREGADO"
            Columns(7).Name =   "AGREGADO"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(7).Style=   4
            Columns(7).ButtonsAlways=   -1  'True
            Columns(8).Width=   3200
            Columns(8).Caption=   "UON_DEN"
            Columns(8).Name =   "UON_DEN"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(8).Locked=   -1  'True
            _ExtentX        =   14922
            _ExtentY        =   8996
            _StockProps     =   79
            BackColor       =   -2147483633
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgProveedores 
         Height          =   2805
         Left            =   -74880
         TabIndex        =   17
         Top             =   480
         Width           =   7320
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   6
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmESTRMATBuscar.frx":0AC4
         stylesets(1).Name=   "Selected"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmESTRMATBuscar.frx":0AE0
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   1773
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3969
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1111
         Columns(2).Caption=   "CAL1"
         Columns(2).Name =   "CAL1"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1111
         Columns(3).Caption=   "CAL2"
         Columns(3).Name =   "CAL2"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1111
         Columns(4).Caption=   "CAL3"
         Columns(4).Name =   "CAL3"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3201
         Columns(5).Caption=   "C�d.web"
         Columns(5).Name =   "CodWeb"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         _ExtentX        =   12912
         _ExtentY        =   4948
         _StockProps     =   79
         BackColor       =   12632256
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmESTRMATBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnEspacio As Long = 120

Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String
Public sGMNDen As String

Private oGrupsMN1 As CGruposMatNivel1
Private oGrupsMN2 As CGruposMatNivel2
Private oGrupsMN3 As CGruposMatNivel3
Private oGrupsMN4 As CGruposMatNivel4
Private oArticulos As CArticulos
Private arOper As Variant
Private msTrue As String
Private msFalse As String
Private msMsgMinMax As String

Private m_arConcepto() As Boolean
Private m_arAlmacen() As Boolean
Private m_arRecep() As Boolean

Private m_oUonsSeleccionadas As CUnidadesOrganizativas
Private m_bRestrMantUsu As Boolean
Private m_bRestrMantPerf As Boolean
Private m_sVariasUnidades As String
Private oAtribs As CAtributos

Private Sub chkArti_Click()
    If chkArti.Value = vbUnchecked Then
        chkArtiGen.Value = vbUnchecked
    End If
    chkArtiGen.Enabled = chkArti.Value
End Sub

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonUsu)) Is Nothing) Then
        m_bRestrMantUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonPerf)) Is Nothing) Then
        m_bRestrMantPerf = True
    End If
End Sub
Private Sub cmdBuscarUon_Click()
    
    '''Cargar unidades organizativas en funci�n de las restricciones
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    

    Dim frm As frmSELUO
    Set frm = New frmSELUO
    frm.multiselect = True
    frm.CheckChildren = False

    
    oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id, , , False
    oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id, , , False
    oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id, , False

    frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
    
    If Not m_oUonsSeleccionadas Is Nothing Then
        Set frm.UonsSeleccionadas = m_oUonsSeleccionadas
    End If
    frm.Show vbModal
    If frm.Aceptar Then
        Me.txtUonSeleccionada.caption = frm.UonsSeleccionadas.titulo
        Set m_oUonsSeleccionadas = frm.UonsSeleccionadas
    End If
    
    Set frm = Nothing
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    CargarAtributos
End Sub

Private Sub cmdLimpiarUon_Click()
    Me.txtUonSeleccionada.caption = ""
    m_oUonsSeleccionadas.clear
End Sub

Private Sub cmdSeleccionar_Click()
    Dim i As Long
    Dim j As Long
    Dim oRow As SSRow
    
    On Error GoTo NoSeEncuentra

    If Trim(sdbgMateriales.Columns(4).Value) <> "" Then
        j = 5
    Else
        If Trim(sdbgMateriales.Columns(3).Value) <> "" Then
            j = 4
        Else
            If Trim(sdbgMateriales.Columns(2).Value) <> "" Then
                j = 3
            Else
                If Trim(sdbgMateriales.Columns(1).Value) <> "" Then
                    j = 2
                Else
                    If Trim(sdbgMateriales.Columns(0).Value) <> "" Then
                        j = 1
                    End If
                End If
            End If
        End If
    End If

    Select Case j
        Case 1
            Set oRow = BuscarMaterial(j)
            If Not oRow Is Nothing Then
                Set frmESTRMAT.ssMateriales.ActiveRow = oRow
                frmESTRMAT.ConfigurarInterfazSeguridad oRow.Band.Index
            Else
                GoTo NoSeEncuentra
            End If
            
        Case 2
            Set oRow = BuscarMaterial(1)
            If Not oRow Is Nothing Then
                Set oRow = BuscarMaterial(j, oRow)
                If Not oRow Is Nothing Then
                    Set frmESTRMAT.ssMateriales.ActiveRow = oRow
                    frmESTRMAT.ConfigurarInterfazSeguridad oRow.Band.Index
                Else
                    GoTo NoSeEncuentra
                End If
            Else
                GoTo NoSeEncuentra
            End If
        
        Case 3
            Set oRow = BuscarMaterial(1)
            If Not oRow Is Nothing Then
                Set oRow = BuscarMaterial(2, oRow)
                If Not oRow Is Nothing Then
                    Set oRow = BuscarMaterial(j, oRow)
                    If Not oRow Is Nothing Then
                        Set frmESTRMAT.ssMateriales.ActiveRow = oRow
                        frmESTRMAT.ConfigurarInterfazSeguridad oRow.Band.Index
                    Else
                        GoTo NoSeEncuentra
                    End If
                Else
                    GoTo NoSeEncuentra
                End If
            Else
                GoTo NoSeEncuentra
            End If
                    
        Case 4
            Set oRow = BuscarMaterial(1)
            If Not oRow Is Nothing Then
                Set oRow = BuscarMaterial(2, oRow)
                If Not oRow Is Nothing Then
                    Set oRow = BuscarMaterial(3, oRow)
                    If Not oRow Is Nothing Then
                        Set oRow = BuscarMaterial(j, oRow)
                        If Not oRow Is Nothing Then
                            Set frmESTRMAT.ssMateriales.ActiveRow = oRow
                            frmESTRMAT.ConfigurarInterfazSeguridad oRow.Band.Index
                        Else
                            GoTo NoSeEncuentra
                        End If
                    Else
                        GoTo NoSeEncuentra
                    End If
                Else
                    GoTo NoSeEncuentra
                End If
            Else
                GoTo NoSeEncuentra
            End If
        
        Case 5
            If sdbgMateriales.Columns("COD").Value = "" Then Exit Sub
            
            Set oRow = BuscarMaterial(1)
            If Not oRow Is Nothing Then
                Set oRow = BuscarMaterial(2, oRow)
                If Not oRow Is Nothing Then
                    Set oRow = BuscarMaterial(3, oRow)
                    If Not oRow Is Nothing Then
                        Set oRow = BuscarMaterial(j, oRow)
                        If Not oRow Is Nothing Then
                            Set frmESTRMAT.ssMateriales.ActiveRow = oRow
                            frmESTRMAT.ConfigurarInterfazSeguridad oRow.Band.Index
                            
                            frmESTRMAT.g_sCodBusqueda = sdbgMateriales.Columns("COD").Value
                            
                            frmESTRMAT.SSTabEstrMat_Click 0
                            frmESTRMAT.SSTabEstrMat.TabVisible(1) = True
                            frmESTRMAT.m_DesactivarTabClick = True
                            frmESTRMAT.SSTabEstrMat.Tab = 1
                            frmESTRMAT.m_DesactivarTabClick = False
        
                            frmESTRMAT.sdbgArticulos.MoveFirst
                            For i = 1 To frmESTRMAT.g_oArticulos.Count
                                If frmESTRMAT.g_oArticulos.Item(i).Cod = sdbgMateriales.Columns("COD").Value Then
                                    Exit For
                                End If
                                frmESTRMAT.sdbgArticulos.MoveNext
                            Next
                        Else
                            GoTo NoSeEncuentra
                        End If
                    Else
                        GoTo NoSeEncuentra
                    End If
                Else
                    GoTo NoSeEncuentra
                End If
            Else
                GoTo NoSeEncuentra
            End If
        
    End Select
    
    Unload Me
    Exit Sub
    
NoSeEncuentra:
    oMensajes.MaterialNuevo
    Unload Me
End Sub

Private Sub cmdAyuda_Click()
    MostrarFormPROCEAyuda oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Private Sub cmdBorrar_Click()
    lblMaterial.caption = ""
    If sdbgMateriales.Rows > 0 Then
        sdbgMateriales.RemoveAll
    End If
        
    sGMN1Cod = vbNullString
    sGMN2Cod = vbNullString
    sGMN3Cod = vbNullString
    sGMN4Cod = vbNullString
    sGMNDen = vbNullString
    
    VaciarAtributosMaterial
End Sub

''' <summary>Elimina del grid de atributos los atributos de material</summary>
''' <remarks>Llamada desde: cmdSelMat_Click, cmdBorrar_Click </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub VaciarAtributosMaterial()
    Dim iRow As Long
        
'    With sdbgAtributos
'        iRow = .Rows
'        Do While iRow >= 0
'            .Bookmark = iRow
'            'Se elimina si es un atributo de material
'            If .Columns("MAT").Value = 1 Then
'                oAtribs.Remove CStr(.Columns("ID_ATRIB").Value)
'                .RemoveItem .AddItemRowIndex(.Bookmark)
'
'            End If
'
'            iRow = iRow - 1
'        Loop
'    End With
'Jim : Borro todos los atributos , para evitar posibles conflictos con los a�adidos por uon, por material o individualmente.
'Si se puede esto ya se arreglar� m�s adelante con tiempo.
    sdbgAtributos.RemoveAll
    Set oAtribs = Nothing
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
End Sub

Private Sub cmdBuscaAtrib_Click()
    Dim ofrmATRIB As frmAtribMod

    Set ofrmATRIB = New frmAtribMod

    ofrmATRIB.g_sOrigen = "frmESTRMATBuscar"

    ofrmATRIB.g_sGmn1 = sGMN1Cod
    ofrmATRIB.g_sGmn2 = sGMN2Cod
    ofrmATRIB.g_sGmn3 = sGMN3Cod
    ofrmATRIB.g_sGmn4 = sGMN4Cod

    ofrmATRIB.sstabGeneral.Tab = 0
    ofrmATRIB.g_GMN1RespetarCombo = True

    ofrmATRIB.sdbcGMN1_4Cod.Text = sGMN1Cod

    If sGMN1Cod & sGMN2Cod & sGMN3Cod & sGMN4Cod = vbNullString Then
        ofrmATRIB.sdbcGMN1_4Cod_Validate False
        ofrmATRIB.sdbcGMN2_4Cod_Validate False
        ofrmATRIB.sdbcGMN3_4Cod_Validate False
        ofrmATRIB.sdbcGMN4_4Cod_Validate False
    Else
        If sGMN1Cod <> vbNullString Then
            ofrmATRIB.sdbcGMN1_4Cod.Text = sGMN1Cod
            ofrmATRIB.sdbcGMN1_4Cod_Validate False
        End If
        ofrmATRIB.g_GMN1RespetarCombo = False
                
        If sGMN2Cod <> vbNullString Then
            ofrmATRIB.g_GMN2RespetarCombo = True
            ofrmATRIB.sdbcGMN2_4Cod.Text = sGMN2Cod
            ofrmATRIB.sdbcGMN2_4Cod_Validate False
        End If
        ofrmATRIB.g_GMN2RespetarCombo = False
                
        If sGMN3Cod <> vbNullString Then
            ofrmATRIB.g_GMN3RespetarCombo = True
            ofrmATRIB.sdbcGMN3_4Cod.Text = sGMN3Cod
            ofrmATRIB.sdbcGMN3_4Cod_Validate False
        End If
        ofrmATRIB.g_GMN3RespetarCombo = False
                
        If sGMN4Cod <> vbNullString Then
            ofrmATRIB.g_GMN4RespetarCombo = True
            ofrmATRIB.sdbcGMN4_4Cod.Text = sGMN4Cod
            ofrmATRIB.sdbcGMN4_4Cod_Validate False
        End If
        ofrmATRIB.g_GMN4RespetarCombo = False
    End If

    ofrmATRIB.cmdSeleccionar.Visible = True

    ofrmATRIB.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeMultiSelectRange
    ofrmATRIB.g_bSoloSeleccion = True

    ofrmATRIB.Show vbModal
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

'''<summary>Carga los materiales o art�culos segun el filtro establecido y las restricciones de usuario</summary>
'''<remarks>Tiempo m�ximo: >2 seg</remarks>
'''<revision>LTG 23/05/2012</revision>

Private Sub CargarArticulos()
    Dim sGMN1 As String
    Dim sGMN2 As String
    Dim sGMN3 As String
    Dim sGMN4 As String
    Dim i As Integer
    Dim oIMaterialComprador As IMaterialAsignado
    Dim oAtributos As CAtributos
    Dim iNumAtrib As Integer
    Dim bCodFinalizanPor As Boolean
    Dim bDenFinalizanPor As Boolean
    Dim bCoincidenciaCod As Boolean
    Dim bCoincidenciaDen As Boolean
    Dim sCod As String
    Dim sDen As String
    Dim bCoincidenciaTotalDoble As Boolean
    Dim vBmk As Variant
    Dim o As CAtributo
    
    sdbgMateriales.RemoveAll
    
    If txtCod.Text <> "" Then
        If InStr(txtCod.Text, "*") > 0 Or InStr(txtCod.Text, "*") = Len(txtCod.Text) Then
            bCoincidenciaCod = False
            If InStr(txtCod.Text, "*") > 1 Then 'div*
                sCod = Left(txtCod.Text, Len(txtCod.Text) - 1)
                bCodFinalizanPor = False
            Else '*div � *div*
                sCod = Mid(txtCod.Text, 2)
                bCodFinalizanPor = True
                If InStr(sCod, "*") > 1 Then '*div*
                    sCod = Replace(sCod, "*", "%", , , vbTextCompare)
                End If
            End If
        Else
            sCod = txtCod.Text
            bCoincidenciaCod = True
            bCodFinalizanPor = False
        End If
    End If
    If txtDen.Text <> "" Then
        If InStr(txtDen.Text, "*") > 0 Or InStr(txtDen.Text, "*") = Len(txtDen.Text) Then
            bCoincidenciaDen = False
            If InStr(txtDen.Text, "*") > 1 Then 'div*
                sDen = Left(txtDen.Text, Len(txtDen.Text) - 1)
                bDenFinalizanPor = False
            Else '*div � *div*
                sDen = Mid(txtDen.Text, 2)
                bDenFinalizanPor = True
                If InStr(sDen, "*") > 1 Then '*div*
                    sDen = Replace(sDen, "*", "%", , , vbTextCompare)
                End If
            End If
        Else
            sDen = txtDen.Text
            bCoincidenciaDen = True
            bDenFinalizanPor = False
        End If
    End If

    If sGMN1Cod & sGMN2Cod & sGMN3Cod & sGMN4Cod <> vbNullString Then
        sGMN1 = sGMN1Cod
        sGMN2 = IIf(sGMN2Cod <> vbNullString, sGMN2Cod, Trim(sCod))
        sGMN3 = IIf(sGMN3Cod <> vbNullString, sGMN3Cod, Trim(sCod))
        sGMN4 = IIf(sGMN4Cod <> vbNullString, sGMN4Cod, Trim(sCod))
    Else
        sGMN1 = Trim(sCod)
        sGMN2 = Trim(sCod)
        sGMN3 = Trim(sCod)
        sGMN4 = Trim(sCod)
    End If

    'Atributos
    If sdbgAtributos.Rows > 0 Then
        Set oAtributos = oFSGSRaiz.Generar_CAtributos
        
        With sdbgAtributos
            .Update
            
            iNumAtrib = 0
            Do While iNumAtrib < .Rows
                vBmk = .GetBookmark(iNumAtrib)
                If .Columns("USAR").CellValue(vBmk) Then
                    If .Columns("VALOR").CellValue(vBmk) <> "" Then
                        Select Case .Columns("IDTIPO").CellValue(vBmk)
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                                    Set o = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk), valor_text:=Replace(.Columns("VALOR").CellValue(vBmk), "*", "%"))
                                End If
                            Case TiposDeAtributos.TipoNumerico
                                If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                                    Set o = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk), valor_num:=.Columns("VALOR").CellValue(vBmk), Formula:=.Columns("OPER").CellValue(vBmk))
                                End If
                            Case TiposDeAtributos.TipoFecha
                                If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                                    Set o = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk), valor_fec:=.Columns("VALOR").CellValue(vBmk))
                                End If
                            Case TiposDeAtributos.TipoBoolean
                                If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                                    Set o = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk), valor_bool:=.Columns("VALOR_ATRIB").CellValue(vBmk))
                                End If
                        End Select
                    Else
                        If oAtributos.Item(.Columns("ID_ATRIB").CellValue(vBmk)) Is Nothing Then
                            
                            Set o = oAtributos.Add(.Columns("ID_ATRIB").CellValue(vBmk), .Columns("COD").CellValue(vBmk), .Columns("DEN").CellValue(vBmk), .Columns("IDTIPO").CellValue(vBmk))
                            
                        End If
                    End If
                    o.AmbitoAtributo = oAtribs.Item(CStr(sdbgAtributos.Columns("ID_ATRIB").CellValue(vBmk))).AmbitoAtributo
                End If
                
                            
                iNumAtrib = iNumAtrib + 1
            Loop
        End With
    End If

    If chkArti.Value <> vbChecked Then
        If frmESTRMAT.g_bRComprador Then
            Set oIMaterialComprador = oUsuarioSummit.comprador
            Set oGrupsMN1 = oIMaterialComprador.DevolverGruposMN1Asignados(sGMN1, sDen, (bCoincidenciaCod And bCoincidenciaDen), , False, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen)
            Set oGrupsMN2 = oIMaterialComprador.DevolverGruposMN2Asignados(sGMN2, sDen, (bCoincidenciaCod And bCoincidenciaDen), , False, , , , , sGMN1Cod, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen)
            Set oGrupsMN3 = oIMaterialComprador.DevolverGruposMN3Asignados(sGMN3, sDen, (bCoincidenciaCod And bCoincidenciaDen), , False, , , , , sGMN1Cod, sGMN2Cod, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen)
            Set oGrupsMN4 = oIMaterialComprador.DevolverGruposMN4Asignados(sGMN4, sDen, (bCoincidenciaCod And bCoincidenciaDen), , False, , , , , , sGMN1Cod, sGMN2Cod, sGMN3Cod, oAtributos, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen)
        Else
            Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            Set oGrupsMN2 = oFSGSRaiz.Generar_CGruposMatNivel2
            Set oGrupsMN3 = oFSGSRaiz.Generar_CGruposMatNivel3
            Set oGrupsMN4 = oFSGSRaiz.Generar_CGruposMatNivel4
            
            oGrupsMN1.CargarTodosLosGruposMat sGMN1, sDen, (bCoincidenciaCod And bCoincidenciaDen), False, False, , bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen
            oGrupsMN2.CargarTodosLosGruposMat sGMN2, sDen, (bCoincidenciaCod And bCoincidenciaDen), False, False, sGMN1Cod, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen
            oGrupsMN3.CargarTodosLosGruposMat sGMN3, sDen, (bCoincidenciaCod And bCoincidenciaDen), False, False, sGMN1Cod, sGMN2Cod, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen
            oGrupsMN4.CargarTodosLosGruposMat sGMN4, sDen, (bCoincidenciaCod And bCoincidenciaDen), False, False, sGMN1Cod, sGMN2Cod, sGMN3Cod, oAtributos, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen
        End If
        
        CargarGrid "GMN1"
        CargarGrid "GMN2"
        CargarGrid "GMN3"
        CargarGrid "GMN4"
    End If
    
    'Preparo los checks de los conceptos en arrays
    ReDim m_arConcepto(0 To 2)
    ReDim m_arAlmacen(0 To 2)
    ReDim m_arRecep(0 To 2)
    For i = 0 To 2
        If chkConcepto(i).Value = vbChecked Then m_arConcepto(i) = True
        If chkAlmacen(i).Value = vbChecked Then m_arAlmacen(i) = True
        If chkRecep(i).Value = vbChecked Then m_arRecep(i) = True
    Next

    Dim iPerfil As Integer
    If Not oUsuarioSummit.Perfil Is Nothing Then
        iPerfil = oUsuarioSummit.Perfil.Id
    Else
        iPerfil = 0
    End If
    
    If frmESTRMAT.g_bRComprador Then
        Set oIMaterialComprador = oUsuarioSummit.comprador
        Set oArticulos = oIMaterialComprador.DevolverArticulos(sCod, sDen, (bCoincidenciaCod And bCoincidenciaDen), , False, chkArtiGen, m_arConcepto, m_arAlmacen, m_arRecep, oAtributos, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaCod, bCoincidenciaDen, IIf(Me.chkArtiCent = 1, True, False), basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, iPerfil, m_oUonsSeleccionadas, False, True, False)
    Else
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        bCoincidenciaTotalDoble = (sCod <> vbNullString) And (sDen <> vbNullString)
        oArticulos.CargarTodosLosArticulos sCod, sDen, (bCoincidenciaCod Or bCoincidenciaDen), False, False, , sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, chkArtiGen, bCodFinalizanPor, bDenFinalizanPor, bCoincidenciaTotalDoble, bCoincidenciaCod, bCoincidenciaDen, m_arConcepto, m_arAlmacen, m_arRecep, oAtributos, IIf(Me.chkArtiCent = 1, True, False), True, m_oUonsSeleccionadas, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, iPerfil, , False, , False
    End If
                        
    CargarGrid "ARTI"
End Sub

Private Sub cmdSelMat_Click()
    Dim sMaterial As String
    
    SeleccionarMaterial

    VaciarAtributosMaterial
    
    If sGMN1Cod & sGMN2Cod & sGMN3Cod & sGMN4Cod & sGMNDen <> "" Then
        sMaterial = sGMN1Cod
        sMaterial = sMaterial & IIf(sGMN2Cod <> vbNullString, "-" & sGMN2Cod, vbNullString)
        sMaterial = sMaterial & IIf(sGMN3Cod <> vbNullString, "-" & sGMN3Cod, vbNullString)
        sMaterial = sMaterial & IIf(sGMN4Cod <> vbNullString, "-" & sGMN4Cod, vbNullString)
        sMaterial = sMaterial & IIf(sGMNDen <> vbNullString, "-" & sGMNDen, vbNullString)
        
        lblMaterial.caption = sMaterial
        
        CargarAtributos
    End If

    sdbgMateriales.RemoveAll
End Sub

'''<summary>Carga los datos del material seleccionado en frmSELMAT</summary>
'''<remarks>Tiempo m�ximo: >2 seg</remarks>

Public Sub PonerMatSeleccionado()
    With frmSELMAT
        sGMN1Cod = vbNullString
        sGMN2Cod = vbNullString
        sGMN3Cod = vbNullString
        sGMN4Cod = vbNullString
        sGMNDen = vbNullString
        
        If Not .oGMN1Seleccionado Is Nothing Then
            sGMN1Cod = .oGMN1Seleccionado.Cod
            sGMNDen = .oGMN1Seleccionado.Den
        End If
    
        If Not .oGMN2Seleccionado Is Nothing Then
            sGMN2Cod = .oGMN2Seleccionado.Cod
            sGMNDen = .oGMN2Seleccionado.Den
        End If
    
        If Not .oGMN3Seleccionado Is Nothing Then
            sGMN3Cod = .oGMN3Seleccionado.Cod
            sGMNDen = .oGMN3Seleccionado.Den
        End If
    
        If Not .oGMN4Seleccionado Is Nothing Then
            sGMN4Cod = .oGMN4Seleccionado.Cod
            sGMNDen = .oGMN4Seleccionado.Den
        End If
    End With
End Sub

'''<summary>Abre la pantalla de selecci�n de material</summary>
'''<remarks>Tiempo m�ximo: >2 seg</remarks>

Public Sub SeleccionarMaterial()
    frmSELMAT.bPermProcMultiMaterial = False
    frmSELMAT.sOrigen = "frmESTRMATBuscar"
    frmSELMAT.bRComprador = frmESTRMAT.g_bRComprador
    frmSELMAT.bRCompResponsable = frmESTRMAT.g_bRCompResp
    frmSELMAT.Show 1
End Sub

Private Sub Form_Initialize()
    Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
End Sub

Private Sub Form_Load()
    Dim sMaterial As String

    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    ConfigurarGrid
    CargarRecursos
    ConfigurarSeguridad
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
    PonerFieldSeparator Me
    
    CargarComboOperandos
        
    Me.chkArtiCent.Visible = gParametrosGenerales.gbArticulosCentrales
    
    If sGMN1Cod & sGMN2Cod & sGMN3Cod & sGMN4Cod & sGMNDen <> "" Then
        sMaterial = sGMN1Cod
        sMaterial = sMaterial & IIf(sGMN2Cod <> vbNullString, "-" & sGMN2Cod, vbNullString)
        sMaterial = sMaterial & IIf(sGMN3Cod <> vbNullString, "-" & sGMN3Cod, vbNullString)
        sMaterial = sMaterial & IIf(sGMN4Cod <> vbNullString, "-" & sGMN4Cod, vbNullString)
        sMaterial = sMaterial & IIf(sGMNDen <> vbNullString, "-" & sGMNDen, vbNullString)
        
        lblMaterial.caption = sMaterial
        
        CargarAtributos
    End If
    
    Set Me.UonsSeleccionadas = m_oUonsSeleccionadas
End Sub

''' <summary>Carga el combo de operandos</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarComboOperandos()
    Dim i As Integer
    
    arOper = Array("=", ">", ">=", "<", "<=")
    
    sdbddOper.RemoveAll
    
    sdbddOper.AddItem ""
    For i = 0 To UBound(arOper)
        sdbddOper.AddItem arOper(i) & Chr(m_lSeparador) & arOper(i)
    Next
End Sub

''' <summary>Carga el grid de atributos con los atributos de material</summary>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarAtributos()
    Dim oatrib As CAtributo
    Dim oGmn As Object
    Dim bAddAtrib As Boolean
    
    VaciarAtributosMaterial
    
    If sGMN4Cod <> vbNullString Then
        Set oGmn = oFSGSRaiz.Generar_CGrupoMatNivel4
        oGmn.GMN1Cod = sGMN1Cod
        oGmn.GMN2Cod = sGMN2Cod
        oGmn.GMN3Cod = sGMN3Cod
        oGmn.Cod = sGMN4Cod
    ElseIf sGMN3Cod <> vbNullString Then
        Set oGmn = oFSGSRaiz.generar_CGrupoMatNivel3
        oGmn.GMN1Cod = sGMN1Cod
        oGmn.GMN2Cod = sGMN2Cod
        oGmn.Cod = sGMN3Cod
    ElseIf sGMN2Cod <> vbNullString Then
        Set oGmn = oFSGSRaiz.generar_CGrupoMatNivel2
        oGmn.GMN1Cod = sGMN1Cod
        oGmn.Cod = sGMN2Cod
    ElseIf sGMN1Cod <> vbNullString Then
        Set oGmn = oFSGSRaiz.generar_CGrupoMatNivel1
        oGmn.Cod = sGMN1Cod
    Else
        Set oGmn = oFSGSRaiz.Generar_CGrupoMatNivel4
    End If
    Set oAtribs = oGmn.DevolverAtribMatYArtMat(False, m_oUonsSeleccionadas)
    
    
    Set oGmn = Nothing
        
    If Not oAtribs Is Nothing Then
        If oAtribs.Count > 0 Then
            With sdbgAtributos
                For Each oatrib In oAtribs
                    .AddItem "0" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "1"
                Next
            End With
        End If
    End If
End Sub

Private Sub CargarGrid(ByVal Opcion As String)
Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim oArti As CArticulo

'sdbgMateriales.RemoveAll

Select Case Opcion
    
    Case "GMN1"
     
        For Each oGMN1 In oGrupsMN1
           
           sdbgMateriales.AddItem oGMN1.Cod & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & Chr(m_lSeparador) & oGMN1.Den
        Next
        
        Set oGrupsMN1 = Nothing
        
    Case "GMN2"
    
        For Each oGMN2 In oGrupsMN2
            sdbgMateriales.AddItem oGMN2.GMN1Cod & Chr(m_lSeparador) & oGMN2.Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oGMN2.Den
        Next
        Set oGrupsMN2 = Nothing
        
    Case "GMN3"
    
        For Each oGMN3 In oGrupsMN3
            sdbgMateriales.AddItem oGMN3.GMN1Cod & Chr(m_lSeparador) & oGMN3.GMN2Cod & Chr(m_lSeparador) & oGMN3.Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oGMN3.Den
        Next
        
        Set oGrupsMN3 = Nothing
        
    Case "GMN4"
        
        For Each oGMN4 In oGrupsMN4
            sdbgMateriales.AddItem oGMN4.GMN1Cod & Chr(m_lSeparador) & oGMN4.GMN2Cod & Chr(m_lSeparador) & oGMN4.GMN3Cod & Chr(m_lSeparador) & oGMN4.Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oGMN4.Den
        Next
        
        Set oGrupsMN4 = Nothing
        
    Case "ARTI"
        
    For Each oArti In oArticulos
        sdbgMateriales.AddItem oArti.GMN1Cod & Chr(m_lSeparador) & oArti.GMN2Cod & Chr(m_lSeparador) & oArti.GMN3Cod & Chr(m_lSeparador) & oArti.GMN4Cod & Chr(m_lSeparador) & oArti.Cod & Chr(m_lSeparador) & oArti.Den & Chr(m_lSeparador) & oArti.CodArticuloCentral & Chr(m_lSeparador) & oArti.isCentral & Chr(m_lSeparador) & oArti.NombreUON
    Next
End Select

Set oArticulos = Nothing

End Sub

Private Sub ConfigurarGrid()

    sdbgMateriales.RemoveAll
    sdbgMateriales.Columns(0).caption = gParametrosGenerales.gsABR_GMN1 & "."
    sdbgMateriales.Columns(1).caption = gParametrosGenerales.gsABR_GMN2 & "."
    sdbgMateriales.Columns(2).caption = gParametrosGenerales.gsABR_GMN3 & "."
    sdbgMateriales.Columns(3).caption = gParametrosGenerales.gsABR_GMN4 & "."
    
    Select Case gParametrosGenerales.giNEM
    
    Case 1
        sdbgMateriales.Columns(0).Visible = True
        sdbgMateriales.Columns(1).Visible = False
        sdbgMateriales.Columns(2).Visible = False
        sdbgMateriales.Columns(3).Visible = False
        
    Case 2
        sdbgMateriales.Columns(0).Visible = True
        sdbgMateriales.Columns(1).Visible = True
        sdbgMateriales.Columns(2).Visible = False
        sdbgMateriales.Columns(3).Visible = False
        
    Case 3
        sdbgMateriales.Columns(0).Visible = True
        sdbgMateriales.Columns(1).Visible = True
        sdbgMateriales.Columns(2).Visible = True
        sdbgMateriales.Columns(3).Visible = False
    
    Case 4
        
        sdbgMateriales.Columns(0).Visible = True
        sdbgMateriales.Columns(1).Visible = True
        sdbgMateriales.Columns(2).Visible = True
        sdbgMateriales.Columns(3).Visible = True
    End Select
    
    ' edu 20071010 incidencia 436
    ' Hacer invisible el checkbox en funci�n del par�metro.
    
    If gParametrosGenerales.gbArticulosGenericos = False Then
        chkArtiGen.Visible = False
    Else
        chkArtiGen.Visible = True
    End If
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>Coloca los controles en el formulario en funci�n del tama�o de este</summary>
''' <remarks>Llamada desde: Form_Resize</remarks>

Private Sub Arrange()
    If Me.WindowState <> vbMinimized Then
        If Me.Height < 6900 Then Me.Height = 6900
        If Me.Width < 9090 Then Me.Width = 9090
        
        sstabArticulos.Width = Me.ScaleWidth - (2 * cnEspacio)
        sstabArticulos.Height = Me.ScaleHeight - (2 * cnEspacio)
        fraDatosBusqueda.Width = sstabArticulos.Width - (2 * cnEspacio)
        fraDatosBusqueda.Height = sstabArticulos.Height - (4 * cnEspacio)
        sdbgAtributos.Width = fraDatosBusqueda.Width - (2 * cnEspacio)
        sdbgAtributos.Height = fraDatosBusqueda.Height - sdbgAtributos.Top - fraConcepto.Height - (2 * cnEspacio)
        fraConcepto.Top = fraDatosBusqueda.Height - fraConcepto.Height - cnEspacio
        fraAlmacen.Top = fraConcepto.Top
        fraRecepcion.Top = fraConcepto.Top
               
        sdbgAtributos.Columns("USAR").Width = 600
        sdbgAtributos.Columns("COD").Width = (sdbgAtributos.Width - 1200) * 0.22
        sdbgAtributos.Columns("DEN").Width = (sdbgAtributos.Width - 1200) * 0.42
        sdbgAtributos.Columns("OPER").Width = 600
        sdbgAtributos.Columns("VALOR").Width = (sdbgAtributos.Width - 1200) * 0.33
              
        fraArticulos.Width = sstabArticulos.Width - (2 * cnEspacio)
        fraArticulos.Height = sstabArticulos.Height - (4 * cnEspacio)
            
        sdbgMateriales.Width = sstabArticulos.Width - (2 * cnEspacio)
        sdbgMateriales.Columns(0).Width = sdbgMateriales.Width * 10 / 100
        sdbgMateriales.Columns(1).Width = sdbgMateriales.Width * 10 / 100
        sdbgMateriales.Columns(2).Width = sdbgMateriales.Width * 10 / 100
        sdbgMateriales.Columns(3).Width = sdbgMateriales.Width * 10 / 100
        sdbgMateriales.Columns(4).Width = sdbgMateriales.Width * 20 / 100
        sdbgMateriales.Columns(5).Width = sdbgMateriales.Width * 40 / 100
        sdbgMateriales.Height = fraArticulos.Height - cmdSeleccionar.Height - (2 * cnEspacio)
        cmdSeleccionar.Top = sdbgMateriales.Top + sdbgMateriales.Height + cnEspacio
        cmdCerrar.Top = cmdSeleccionar.Top
        cmdSeleccionar.Left = sdbgMateriales.Width / 2 - 1000
        cmdCerrar.Left = cmdSeleccionar.Left + cmdSeleccionar.Width + 300
    End If
End Sub

Private Sub Form_Terminate()
    Set m_oUonsSeleccionadas = Nothing
    Set oAtribs = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oGrupsMN1 = Nothing
    Set oGrupsMN2 = Nothing
    Set oGrupsMN3 = Nothing
    Set oGrupsMN4 = Nothing
    Set oArticulos = Nothing
    Set oAtribs = Nothing
    
    sGMN1Cod = vbNullString
    sGMN2Cod = vbNullString
    sGMN3Cod = vbNullString
    sGMN4Cod = vbNullString
    sGMNDen = vbNullString

End Sub

Private Sub sdbddOper_InitColumnProps()
    sdbddOper.DataFieldList = "Column 0"
    sdbddOper.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddOper_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddOper.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOper.Rows - 1
            bm = sdbddOper.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOper.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddOper_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer
    Dim i As Integer

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        
        ''' Comprobar la existencia en la lista
        For i = 0 To UBound(arOper)
            If arOper(i) = sdbgAtributos.Columns(sdbgAtributos.col).Text Then
                bExiste = True
                Exit For
            End If
        Next
        
        If Not bExiste Then
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.col).Text
            sdbgAtributos.Columns(sdbgAtributos.col).Text = ""
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer
    Dim oLista As CValoresPond
    Dim oElem As CValorPond

    sdbddValor.RemoveAll
    
    sdbddValor.AddItem ""

    iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

    Set oatrib = oFSGSRaiz.Generar_CAtributo
    Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)

    If Not oatrib Is Nothing Then
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            oatrib.CargarListaDeValores
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                sdbddValor.AddItem oElem.ValorLista & Chr(m_lSeparador) & oElem.ValorLista
            Next
            Set oLista = Nothing
        Else
            'Campo de tipo boolean:
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem msTrue & Chr(m_lSeparador) & msTrue
                sdbddValor.AddItem msFalse & Chr(m_lSeparador) & msFalse
            End If
        End If
    
        Set oatrib = Nothing
    End If
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

            Set oatrib = oFSGSRaiz.Generar_CAtributo
            Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)
            
            If oatrib Is Nothing Then Exit Sub
            
            oatrib.CargarListaDeValores
            
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                If UCase(msTrue) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "1"
                    bExiste = True
                End If
                If UCase(msFalse) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "0"
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            sdbgAtributos.Columns(sdbgAtributos.col).Text = ""
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.col).caption
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub

Private Sub sdbgatributos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    With sdbgAtributos
        Select Case .Columns(ColIndex).Name
            Case "VALOR"
                'Comprobar que el valor introducido se corresponde con el tipo de atributo
                If .Columns("VALOR").Text <> "" Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoString
                        Case TiposDeAtributos.TipoNumerico
                            If (Not IsNumeric(.Columns("VALOR").Value)) Then
                                MsgBox "El valor del campo Valor debe ser num�rico", vbInformation, "FULLSTEP"
                                Cancel = True
                            Else
                                If .Columns("OPER").Text <> "" And .Columns("OPER").Text = "=" Then
                                    If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                        Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                                    End If
                                End If
                            End If
                            
                            If .Columns("OPER").Text = "" Then
                                .Columns("OPER").Text = "="
                                If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                    Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                                End If
                            End If
                        Case TiposDeAtributos.TipoFecha
                            If (Not IsDate(.Columns("VALOR").Text) And .Columns("VALOR").Value <> "") Then
                                MsgBox "El valor del campo Valor debe ser una fecha.", vbInformation, "FULLSTEP"
                                Cancel = True
                            Else
                                If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                    If CDate(.Columns("MINIMO").Value) > CDate(.Columns("VALOR").Value) Or CDate(.Columns("MAXIMO").Value) < CDate(.Columns("VALOR").Value) Then
                                        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", .Columns("MINIMO").Value), "@Valor2", .Columns("MAXIMO").Value), vbInformation, "FULLSTEP"
                                        Cancel = True
                                    End If
                                End If
                            End If
                        Case TiposDeAtributos.TipoBoolean
                    End Select
                End If
            Case "OPER"
                'Comprobar si se est� intentando dejar en blanco la col. del operando teniendo un valor introducido
                If .Columns("OPER").Text = vbNullString And .Columns("VALOR").Text <> vbNullString Then
                    Cancel = True
                ElseIf .Columns("OPER").Text = "=" And .Columns("VALOR").Text <> vbNullString Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoNumerico
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                            End If
                    End Select
                End If
        End Select
    End With
End Sub

''' <summary>Comprueba que un valor est� entre un m�nimo y un m�ximo</summary>
''' <remarks>Llamada desde: sdbgatributos_BeforeColUpdate</remarks>
''' <param name="dblValor">Valor</param>
''' <param name="dblMin">M�nimo</param>
''' <param name="dblMax">M�ximo</param>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Function ComprobarValoresMaxMin(ByVal dblValor As Double, ByVal dblMin As Double, ByVal dblMax As Double) As Boolean
    ComprobarValoresMaxMin = True
    
    If dblMin > dblValor Or dblMax < dblValor Then
        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", dblMin), "@Valor2", dblMax), vbInformation, "FULLSTEP"
        ComprobarValoresMaxMin = False
    End If
End Function

Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bCargarCombo As Boolean
    
    'Combo de operandos
    If sdbgAtributos.Columns("IDTIPO").Value = TipoNumerico Then
        sdbgAtributos.Columns("OPER").DropDownHwnd = sdbddOper.hWnd
        sdbgAtributos.Columns("OPER").Locked = False
    Else
        sdbgAtributos.Columns("OPER").DropDownHwnd = 0
        sdbgAtributos.Columns("OPER").Locked = True
    End If
    
    'Combo de valores
    bCargarCombo = False
    If sdbgAtributos.col = sdbgAtributos.Columns("VALOR").Position Then
        If sdbgAtributos.Columns("INTRO").Value Then
            bCargarCombo = True
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TipoBoolean Then
                bCargarCombo = True
            End If
        End If
    End If
    
    If bCargarCombo Then
        sdbddValor.RemoveAll
        sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
        sdbddValor_DropDown
    Else
        sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
    End If
End Sub

Private Sub sdbgMateriales_DblClick()
    
    cmdSeleccionar_Click
    
End Sub

'''<summary>Carga los idiomas del formulario</summary>
'''<remarks>Tiempo m�ximo: >2 seg</remarks>

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRMAT_BUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    Me.caption = Ador(0).Value
    Ador.MoveNext
    lblCod.caption = Ador(0).Value
    Ador.MoveNext
    lblDen.caption = Ador(0).Value
    Ador.MoveNext
    chkArti.caption = Ador(0).Value
    Ador.MoveNext
    sdbgMateriales.Columns(4).caption = Ador(0).Value
    Ador.MoveNext
    sdbgMateriales.Columns(5).caption = Ador(0).Value
    Ador.MoveNext
    cmdSeleccionar.caption = Ador(0).Value
    Ador.MoveNext
    cmdCerrar.caption = Ador(0).Value
    Ador.MoveNext
    chkArtiGen.caption = Ador(0).Value
    Ador.MoveNext
    chkConcepto(0).caption = Ador(0).Value
    Ador.MoveNext
    chkConcepto(1).caption = Ador(0).Value
    Ador.MoveNext
    chkConcepto(2).caption = Ador(0).Value
    Ador.MoveNext
    chkAlmacen(0).caption = Ador(0).Value
    chkRecep(0).caption = Ador(0).Value
    Ador.MoveNext
    chkAlmacen(1).caption = Ador(0).Value
    Ador.MoveNext
    chkAlmacen(2).caption = Ador(0).Value
    chkRecep(2).caption = Ador(0).Value
    Ador.MoveNext
    chkRecep(1).caption = Ador(0).Value
    Ador.MoveNext
    fraConcepto.caption = Ador(0).Value
    lblConcepto.caption = Ador(0).Value
    Ador.MoveNext
    fraAlmacen.caption = Ador(0).Value
    lblAlmacenamiento.caption = Ador(0).Value
    Ador.MoveNext
    fraRecepcion.caption = Ador(0).Value
    lblRecepcion.caption = Ador(0).Value
    Ador.MoveNext
    lblMat.caption = Ador(0).Value
    Ador.MoveNext
    sstabArticulos.TabCaption(0) = Ador(0).Value
    Ador.MoveNext
    sstabArticulos.TabCaption(1) = Ador(0).Value
    Ador.MoveNext
    sdbgAtributos.Columns("COD").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAtributos.Columns("DEN").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAtributos.Columns("VALOR").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAtributos.Columns("OPER").caption = Ador(0).Value
    Ador.MoveNext
    msTrue = Ador(0).Value
    Ador.MoveNext
    msFalse = Ador(0).Value
    Ador.MoveNext
    msMsgMinMax = Ador(0).Value
    Ador.MoveNext
    lblBusqAtrib.caption = Ador(0).Value & ":"
    Ador.MoveNext
    Me.chkArtiCent.caption = Ador(0).Value
    Ador.MoveNext
    m_sVariasUnidades = Ador(0).Value
    Ador.MoveNext
    sdbgMateriales.Columns("UON_DEN").caption = Ador(0).Value
    lblUnidadOrg.caption = Ador(0).Value
    Ador.Close
    
    End If

    Set Ador = Nothing
    
    'pargen lit
    Dim oLiterales As CLiterales
    Set oLiterales = oGestorParametros.DevolverLiterales(48, 50, basPublic.gParametrosInstalacion.gIdioma)
    Me.sdbgMateriales.Columns("AGREGADO").caption = oLiterales.Item(1).Den 'Art de planta o agregado
    sdbgMateriales.Columns("ART").caption = oLiterales.Item(3).Den 'central
    Set oLiterales = Nothing


End Sub


Private Function BuscarMaterial(ByVal iNivel As Integer, Optional ByVal oRow1 As SSRow) As SSRow
    Dim oRow As SSRow
    Dim bEncontrado As Boolean
    Dim sCod As String
    
    If iNivel = 1 Then
        Set oRow = frmESTRMAT.ssMateriales.GetRow(ssChildRowFirst)
        If oRow.Cells("COD").Value = sdbgMateriales.Columns(0).Value Then
            bEncontrado = True
        Else
            Do While oRow.HasNextSibling
                Set oRow = oRow.GetSibling(ssSiblingRowNext)
                If oRow.Cells("COD").Value = sdbgMateriales.Columns(0).Value Then
                    bEncontrado = True
                    Exit Do
                End If
            Loop
        End If
    Else
        If oRow1.HasChild = True Then
            Select Case iNivel
                Case 2
                    sCod = sdbgMateriales.Columns(1).Value
                Case 3
                    sCod = sdbgMateriales.Columns(2).Value
                Case 4, 5
                    sCod = sdbgMateriales.Columns(3).Value
            End Select
            
            Set oRow = oRow1.GetChild(ssChildRowFirst)
            If oRow.Cells("COD").Value = sCod Then
                bEncontrado = True
            Else
                Do While oRow.HasNextSibling
                    Set oRow = oRow.GetSibling(ssSiblingRowNext)
                    If oRow.Cells("COD").Value = sCod Then
                        bEncontrado = True
                        Exit Do
                    End If
                Loop
            End If
        End If
    End If
    
    If bEncontrado = True Then
        Set BuscarMaterial = oRow
    Else
        Set BuscarMaterial = Nothing
    End If
End Function

Private Sub sdbgMateriales_RowLoaded(ByVal Bookmark As Variant)
    If sdbgMateriales.Columns("UON_DEN").Text = "Multiples*" Then
        sdbgMateriales.Columns("UON_DEN").Text = m_sVariasUnidades
    End If
    If sdbgMateriales.Columns("AGREGADO").Text = "True" Then
        sdbgMateriales.Columns("AGREGADO").CellStyleSet "Adjudica"
        sdbgMateriales.Columns("AGREGADO").Text = ""
    Else
        sdbgMateriales.Columns("AGREGADO").CellStyleSet ""
        sdbgMateriales.Columns("AGREGADO").Text = ""
    End If
End Sub

Private Sub sstabArticulos_Click(PreviousTab As Integer)
    If PreviousTab = 0 And sstabArticulos.Tab = 1 Then
        Screen.MousePointer = vbHourglass
        CargarArticulos
        Screen.MousePointer = vbNormal
    End If
End Sub

Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas

    Set UonsSeleccionadas = m_oUonsSeleccionadas

End Property

Public Property Set UonsSeleccionadas(oUonsSeleccionadas As CUnidadesOrganizativas)
    If Not oUonsSeleccionadas Is Nothing Then
        Set m_oUonsSeleccionadas = oUonsSeleccionadas
        Me.txtUonSeleccionada.caption = m_oUonsSeleccionadas.titulo
    End If
End Property

Public Function addAtributo(ByRef oAtributo As CAtributo) As CAtributo
    If Not oAtribs.existe(oAtributo.Id) Then
        Set addAtributo = oAtribs.addAtributo(oAtributo)
    End If
End Function
