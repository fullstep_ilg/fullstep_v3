VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstART 
   Caption         =   "Listado de art�culos (Opciones)"
   ClientHeight    =   5685
   ClientLeft      =   4935
   ClientTop       =   3540
   ClientWidth     =   9600
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstART.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5685
   ScaleWidth      =   9600
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   4800
      Top             =   0
   End
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleMode       =   0  'User
      ScaleWidth      =   9600
      TabIndex        =   10
      Top             =   5310
      Width           =   9600
      Begin VB.CommandButton cmdObtener 
         Caption         =   "&Obtener"
         Height          =   375
         Left            =   8040
         TabIndex        =   9
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5235
      Left            =   -45
      TabIndex        =   12
      Top             =   0
      Width           =   9600
      _ExtentX        =   16933
      _ExtentY        =   9234
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstART.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstART.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraTipoPer"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Opciones"
      TabPicture(2)   =   "frmLstART.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraTipoOrg"
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4785
         Left            =   150
         TabIndex        =   13
         Top             =   330
         Width           =   9315
         Begin VB.TextBox txtUonsSeleccionadas 
            BackColor       =   &H80000018&
            Enabled         =   0   'False
            Height          =   285
            Left            =   4320
            TabIndex        =   43
            TabStop         =   0   'False
            Top             =   720
            Width           =   3975
         End
         Begin VB.CommandButton cmdBuscarUon 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8700
            Picture         =   "frmLstART.frx":0D06
            Style           =   1  'Graphical
            TabIndex        =   42
            TabStop         =   0   'False
            Top             =   735
            Width           =   315
         End
         Begin VB.CommandButton cmdLimpiarUon 
            Height          =   285
            Left            =   8370
            Picture         =   "frmLstART.frx":0D72
            Style           =   1  'Graphical
            TabIndex        =   41
            Top             =   735
            Width           =   315
         End
         Begin VB.CheckBox chkSoloCentrales 
            Caption         =   "S�lo art�culos Centrales"
            Height          =   375
            Left            =   2640
            TabIndex        =   39
            Top             =   1080
            Width           =   2295
         End
         Begin VB.CommandButton cmdBuscaAtrib 
            Height          =   285
            Left            =   8700
            Picture         =   "frmLstART.frx":0E17
            Style           =   1  'Graphical
            TabIndex        =   31
            TabStop         =   0   'False
            Top             =   1440
            Width           =   315
         End
         Begin VB.Frame fraAlmacen 
            BorderStyle     =   0  'None
            Caption         =   "Almacenamiento"
            ForeColor       =   &H00000000&
            Height          =   1215
            Left            =   3360
            TabIndex        =   27
            Top             =   3480
            Width           =   2115
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "Opcional"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   30
               Top             =   870
               Width           =   1875
            End
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "No almacenar"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   29
               Top             =   570
               Width           =   1935
            End
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "Obligatorio"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   28
               Top             =   270
               Width           =   1485
            End
            Begin VB.Label lblAlmacenamiento 
               Caption         =   "DAlmacenamiento"
               Height          =   375
               Left            =   120
               TabIndex        =   37
               Top             =   0
               Width           =   1575
            End
         End
         Begin VB.Frame fraRecepcion 
            BorderStyle     =   0  'None
            Caption         =   "Recepci�n"
            ForeColor       =   &H00000000&
            Height          =   1215
            Left            =   6300
            TabIndex        =   23
            Top             =   3480
            Width           =   2115
            Begin VB.CheckBox chkRecep 
               Caption         =   "Opcional"
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   2
               Left            =   120
               TabIndex        =   26
               Top             =   870
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               Caption         =   "No recepcionar"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   25
               Top             =   570
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               Caption         =   "Obligatoria"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   24
               Top             =   270
               Width           =   1935
            End
            Begin VB.Label lblRecepcion 
               Caption         =   "DRecepci�n"
               Height          =   255
               Left            =   120
               TabIndex        =   38
               Top             =   0
               Width           =   1215
            End
         End
         Begin VB.CheckBox chkArtGen 
            Caption         =   "S�lo art�culos gen�ricos"
            Height          =   375
            Left            =   120
            TabIndex        =   22
            Top             =   1080
            Width           =   4455
         End
         Begin VB.Frame fraConcepto 
            BorderStyle     =   0  'None
            Caption         =   "Concepto"
            ForeColor       =   &H00000000&
            Height          =   1215
            Left            =   120
            TabIndex        =   18
            Top             =   3480
            Width           =   2115
            Begin VB.CheckBox chkConcepto 
               Caption         =   "Gasto/Inversi�n"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   21
               Top             =   870
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               Caption         =   "Inversi�n"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   20
               Top             =   570
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               Caption         =   "Gasto"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   19
               Top             =   240
               Width           =   1935
            End
            Begin VB.Label lblConcepto 
               Caption         =   "DConcepto"
               Height          =   255
               Left            =   120
               TabIndex        =   36
               Top             =   0
               Width           =   975
            End
         End
         Begin VB.CommandButton cmdBorrar 
            Height          =   285
            Left            =   8370
            Picture         =   "frmLstART.frx":0EA4
            Style           =   1  'Graphical
            TabIndex        =   17
            Top             =   315
            Width           =   315
         End
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8700
            Picture         =   "frmLstART.frx":0F49
            Style           =   1  'Graphical
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   315
            Width           =   315
         End
         Begin VB.TextBox txtEstMat 
            BackColor       =   &H80000018&
            Enabled         =   0   'False
            Height          =   285
            Left            =   4320
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   300
            Width           =   3975
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Listado completo"
            Height          =   195
            Left            =   135
            TabIndex        =   0
            Top             =   285
            Value           =   -1  'True
            Width           =   2190
         End
         Begin VB.OptionButton optEst 
            Caption         =   "Listar Rama:"
            Height          =   195
            Left            =   2655
            TabIndex        =   1
            Top             =   330
            Width           =   1455
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddOper 
            Height          =   915
            Left            =   2160
            TabIndex        =   32
            Top             =   2160
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmLstART.frx":0FB5
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
            Height          =   915
            Left            =   4440
            TabIndex        =   33
            Top             =   2280
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmLstART.frx":0FD1
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
            Height          =   1455
            Left            =   120
            TabIndex        =   34
            Top             =   1800
            Width           =   8895
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            RecordSelectors =   0   'False
            Col.Count       =   11
            stylesets.count =   3
            stylesets(0).Name=   "Yellow"
            stylesets(0).BackColor=   11862015
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmLstART.frx":0FED
            stylesets(1).Name=   "Normal"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   16777215
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmLstART.frx":1009
            stylesets(2).Name=   "Header"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmLstART.frx":1025
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            HeadStyleSet    =   "Header"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   11
            Columns(0).Width=   979
            Columns(0).Name =   "USAR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Style=   2
            Columns(1).Width=   3201
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   4419
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   1058
            Columns(3).Caption=   "OPER"
            Columns(3).Name =   "OPER"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3889
            Columns(4).Caption=   "VALOR"
            Columns(4).Name =   "VALOR"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "INTRO"
            Columns(5).Name =   "INTRO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "IDTIPO"
            Columns(6).Name =   "IDTIPO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "ID_ATRIB"
            Columns(7).Name =   "ID_ATRIB"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "MAXIMO"
            Columns(8).Name =   "MAXIMO"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "MINIMO"
            Columns(9).Name =   "MINIMO"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "VALOR_ATRIB"
            Columns(10).Name=   "VALOR_ATRIB"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            _ExtentX        =   15690
            _ExtentY        =   2566
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblUons 
            Caption         =   "DUnidades Org."
            Height          =   255
            Left            =   2640
            TabIndex        =   44
            Top             =   720
            Width           =   1695
         End
         Begin VB.Label lblBusqAtrib 
            Caption         =   "B�squeda por atributos"
            Height          =   255
            Left            =   120
            TabIndex        =   35
            Top             =   1440
            Width           =   1935
         End
      End
      Begin VB.Frame fraTipoPer 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2055
         Left            =   -74805
         TabIndex        =   16
         Top             =   465
         Width           =   5205
         Begin VB.OptionButton optOrdEst 
            Caption         =   "Posici�n en estructura de materiales+"
            Height          =   195
            Left            =   480
            TabIndex        =   4
            Top             =   1305
            Width           =   4275
         End
         Begin VB.OptionButton optOrdCod 
            Caption         =   "C�digo+"
            Height          =   195
            Left            =   480
            TabIndex        =   2
            Top             =   360
            Value           =   -1  'True
            Width           =   3915
         End
         Begin VB.OptionButton optOrdDen 
            Caption         =   "Denominaci�n+"
            Height          =   195
            Left            =   480
            TabIndex        =   3
            Top             =   825
            Width           =   3960
         End
      End
      Begin VB.Frame fraTipoOrg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2535
         Left            =   -74775
         TabIndex        =   14
         Top             =   450
         Width           =   5205
         Begin VB.CheckBox chkIncluirArtPlanta 
            Caption         =   "Incluir art�culos de planta"
            Height          =   375
            Left            =   240
            TabIndex        =   40
            Top             =   1680
            Width           =   2295
         End
         Begin VB.CheckBox chkArtPos 
            Caption         =   "Incluir posici�n en estructura materiales+"
            Height          =   195
            Left            =   270
            TabIndex        =   6
            Top             =   630
            Width           =   4440
         End
         Begin VB.CheckBox chkArtDet 
            Caption         =   "Incluir detalles art�culos+"
            Height          =   195
            Left            =   270
            TabIndex        =   5
            Top             =   270
            Width           =   4710
         End
         Begin VB.CheckBox chkAdjDet 
            Caption         =   "Incluir detalles adjudicaciones+"
            Height          =   195
            Left            =   630
            TabIndex        =   8
            Top             =   1350
            Width           =   3960
         End
         Begin VB.CheckBox chkArtAdj 
            Caption         =   "Incluir �ltimas adjudicaciones+"
            Height          =   195
            Left            =   270
            TabIndex        =   7
            Top             =   990
            Width           =   4515
         End
      End
   End
End
Attribute VB_Name = "frmLstART"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnEspacio As Long = 120

Private bRMat As Boolean ' Aplicar restricci�n de material a comprador
Private bRProve As Boolean 'Aplicar restricci�n de consulta de ultimas adjudicaciones
' variables para seleccion de una rama
Private sGMN1Cod As String
Private sGMN2Cod As String
Private sGMN3Cod As String
Private sGMN4Cod As String
'MULTILENGUAJE
Private sEspera(3) As String         'textos para formulario de espera
Private sTitulo As String
Private FormulaRpt(1 To 2, 1 To 17) As String      'formulas textos RPT
Private FormulaSubRpt(1 To 4, 1 To 8) As String   'formulas textos SUBRPT
Private sListadoSel(2) As String                  'formula Selecci�n rpt
Private m_arConcepto() As Boolean
Private m_arAlmacen() As Boolean
Private m_arRecep() As Boolean
Private arOper As Variant
Private msTrue As String
Private msFalse As String
Private msMsgMinMax As String
Private txtRecepcionImporte As String
Private txtRecepcionCantidad As String
Private txtTipoRecepcion As String
Private m_sArticulos As String
Private m_sUni As String
Private m_sGenerico As String
Private m_oUonsSeleccionadas As CUnidadesOrganizativas
Private m_bRestrMantUsu As Boolean
Private m_bRestrMantPerf As Boolean
Private m_oAtributos As CAtributos
'''<summary>Carga los idiomas del formulario</summary>
'''<remarks>Tiempo m�ximo: >2 seg</remarks>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTART, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
    
        chkAdjDet.caption = Ador(0).Value
        Ador.MoveNext
        chkArtAdj.caption = Ador(0).Value
        Ador.MoveNext
        chkArtDet.caption = Ador(0).Value
        Ador.MoveNext
        chkArtPos.caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        frmLstART.caption = Ador(0).Value
        Ador.MoveNext
        optEst.caption = Ador(0).Value
        Ador.MoveNext
        optOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        optOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        optOrdEst.caption = Ador(0).Value
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        Ador.MoveNext
        sTitulo = Ador(0).Value
        FormulaRpt(1, 1) = sTitulo
        Ador.MoveNext
        chkArtGen.caption = Ador(0).Value
        Ador.MoveNext
        fraConcepto.caption = Ador(0).Value
        lblConcepto.caption = Ador(0).Value
        Ador.MoveNext
        fraAlmacen.caption = Ador(0).Value
        lblAlmacenamiento.caption = Ador(0).Value
        Ador.MoveNext
        fraRecepcion.caption = Ador(0).Value
        lblRecepcion.caption = Ador(0).Value
        Ador.MoveNext
        chkConcepto(0).caption = Ador(0).Value
        Ador.MoveNext
        chkConcepto(1).caption = Ador(0).Value
        Ador.MoveNext
        chkConcepto(2).caption = Ador(0).Value
        Ador.MoveNext
        chkAlmacen(1).caption = Ador(0).Value
        chkRecep(1).caption = Ador(0).Value
        Ador.MoveNext
        chkAlmacen(0).caption = Ador(0).Value
        Ador.MoveNext
        chkAlmacen(2).caption = Ador(0).Value
        chkRecep(2).caption = Ador(0).Value
        Ador.MoveNext
        chkRecep(0).caption = Ador(0).Value
        Ador.MoveNext
        ' FORMULAS RPT
        For i = 2 To 7
            FormulaRpt(1, i) = Ador(0).Value
            If i = 7 Then
                m_sArticulos = Ador(0).Value
            End If
            Ador.MoveNext
        Next
        ' FORMULAS SUBrpt
        For i = 1 To 8
            FormulaSubRpt(1, i) = Ador(0).Value
            If i = 8 Then
                m_sUni = Ador(0).Value
            End If
            Ador.MoveNext
        Next
        sListadoSel(1) = Ador(0).Value
        Ador.MoveNext
        sListadoSel(2) = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 8) = Ador(0).Value
        m_sGenerico = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 9) = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 10) = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 11) = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 12) = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 13) = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 14) = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 15) = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 16) = Ador(0).Value
        Ador.MoveNext
        FormulaRpt(1, 17) = Ador(0).Value
        Ador.MoveNext
        ' FORMULAS SUBrpt Atributos
        For i = 1 To 8
            FormulaSubRpt(3, i) = Ador(0).Value
            Ador.MoveNext
        Next
        lblBusqAtrib.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbgAtributos.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("VALOR").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("OPER").caption = Ador(0).Value
        Ador.MoveNext
        msTrue = Ador(0).Value
        Ador.MoveNext
        msFalse = Ador(0).Value
        Ador.MoveNext
        msMsgMinMax = Ador(0).Value
        Ador.MoveNext
        txtTipoRecepcion = Ador(0).Value
        Ador.MoveNext
        txtRecepcionImporte = Ador(0).Value
        Ador.MoveNext
        txtRecepcionCantidad = Ador(0).Value
        Ador.MoveNext
        Me.chkSoloCentrales.caption = Ador(0).Value
        Ador.MoveNext
        Me.chkIncluirArtPlanta.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblUons.caption = Ador(0).Value
        Ador.Close
    
    End If
    FormulaRpt(1, 4) = FormulaRpt(1, 4) '& " " & Year(Date)
    Set Ador = Nothing

End Sub

Public Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonUsu)) Is Nothing) Then
        m_bRestrMantUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonPerf)) Is Nothing) Then
        m_bRestrMantPerf = True
    End If
End Sub

Public Sub PonerMatSeleccionado(sOrigen As String)
' Este procedimiento puede ser llamado desde  :
' Selecci�n de material (frmSELMAT),
' Estructura de materiales (frmESTRMAT) RAMAS
' Estructura de materiales (frmESTRMATART) TAB DE ARTICULOS

    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    
    If sOrigen = "frmESTRMAT" Then
        Set oGMN1Seleccionado = frmESTRMAT.g_oGMN1Seleccionado
        Set oGMN2Seleccionado = frmESTRMAT.g_oGMN2Seleccionado
        Set oGMN3Seleccionado = frmESTRMAT.g_oGMN3Seleccionado
        Set oGMN4Seleccionado = frmESTRMAT.g_oGMN4Seleccionado
    Else
        If sOrigen = "frmSELMAT" Then
            Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
            Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
            Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
            Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
        Else
         ' frmESTRMATART - Articulos de Estructura de materiales
         ' solamente existe el objeto oGMN4seleccionado
            sGMN1Cod = frmESTRMAT.g_oGMN4Seleccionado.GMN1Cod
            sGMN2Cod = frmESTRMAT.g_oGMN4Seleccionado.GMN2Cod
            sGMN3Cod = frmESTRMAT.g_oGMN4Seleccionado.GMN3Cod
            sGMN4Cod = frmESTRMAT.g_oGMN4Seleccionado.Cod
            txtEstMat = sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod & " - " & sGMN4Cod
            Exit Sub
        End If
    End If
    
    If Not oGMN1Seleccionado Is Nothing Then
         sGMN1Cod = oGMN1Seleccionado.Cod
         txtEstMat = sGMN1Cod
    Else
         sGMN1Cod = ""
    End If
           
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
    
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
    
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If
    
        
End Sub

Private Sub chkArtAdj_Click()

If chkArtAdj.Value = vbChecked Then
    chkAdjDet.Enabled = True
Else
    chkAdjDet.Value = vbUnchecked
    chkAdjDet.Enabled = False
End If

End Sub

Private Sub cmdBorrar_Click()
    txtEstMat.Text = ""
End Sub

Private Sub cmdLimpiarUon_Click()
    Me.txtUonsSeleccionadas.Text = ""
    m_oUonsSeleccionadas.clear
End Sub

Private Sub cmdBuscaAtrib_Click()
    Dim ofrmATRIB As frmAtribMod

    Set ofrmATRIB = New frmAtribMod

    ofrmATRIB.g_sOrigen = "frmLstART"

    ofrmATRIB.g_sGmn1 = sGMN1Cod
    ofrmATRIB.g_sGmn2 = sGMN2Cod
    ofrmATRIB.g_sGmn3 = sGMN3Cod
    ofrmATRIB.g_sGmn4 = sGMN4Cod

    ofrmATRIB.sstabGeneral.Tab = 0
    ofrmATRIB.g_GMN1RespetarCombo = True

    ofrmATRIB.sdbcGMN1_4Cod.Text = sGMN1Cod
        
    ofrmATRIB.sdbcGMN1_4Cod.Text = sGMN1Cod
    ofrmATRIB.sdbcGMN1_4Cod_Validate False
    ofrmATRIB.g_GMN1RespetarCombo = False
                
    ofrmATRIB.g_GMN2RespetarCombo = True
    ofrmATRIB.sdbcGMN2_4Cod.Text = sGMN2Cod
    ofrmATRIB.sdbcGMN2_4Cod_Validate False
    ofrmATRIB.g_GMN2RespetarCombo = False
                
    ofrmATRIB.g_GMN3RespetarCombo = True
    ofrmATRIB.sdbcGMN3_4Cod.Text = sGMN3Cod
    ofrmATRIB.sdbcGMN3_4Cod_Validate False
    ofrmATRIB.g_GMN3RespetarCombo = False
                
    ofrmATRIB.g_GMN4RespetarCombo = True
    ofrmATRIB.sdbcGMN4_4Cod.Text = sGMN4Cod
    ofrmATRIB.sdbcGMN4_4Cod_Validate False
    ofrmATRIB.g_GMN4RespetarCombo = False

    ofrmATRIB.cmdSeleccionar.Visible = True

    ofrmATRIB.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeMultiSelectRange
    ofrmATRIB.g_bSoloSeleccion = True

    ofrmATRIB.Show vbModal
End Sub

Private Sub cmdObtener_Click()
    Dim oReport As Object
    Dim SubListado As CRAXDRT.Report
    Dim oCRArticulos As CRArticulos
    Dim pv As Preview
    Dim oFos As FileSystemObject
    Dim sCodComp As String
    Dim sCodEqp As String
    Dim i As Integer
    Dim RepPath As String
    Dim sSeleccion As String
    Dim adoresDG As Ador.Recordset
    Dim n As Integer
    Dim oAtributos As CAtributos
    Dim iNumAtrib As Integer
        
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    If optOrdEst.Value = False Then
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptART.rpt"
        FormulaRpt(1, 3) = FormulaRpt(1, 3) & ":"
    Else
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptART_EST.rpt"
    End If
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    If bRMat Then
        sCodComp = oUsuarioSummit.comprador.Cod
        sCodEqp = oUsuarioSummit.comprador.codEqp
    End If
    
    'Preparo los checks de los conceptos en arrays
    ReDim m_arConcepto(0 To 2)
    ReDim m_arAlmacen(0 To 2)
    ReDim m_arRecep(0 To 2)
    For i = 0 To 2
        If chkConcepto(i).Value = vbChecked Then m_arConcepto(i) = True
        If chkAlmacen(i).Value = vbChecked Then m_arAlmacen(i) = True
        If chkRecep(i).Value = vbChecked Then m_arRecep(i) = True
    Next
    
    'Atributos
    Dim oAtributo As CAtributo
    
    If sdbgAtributos.Rows > 0 Then
        Set oAtributos = oFSGSRaiz.Generar_CAtributos
        
        With sdbgAtributos
            iNumAtrib = 0
            Do While iNumAtrib < .Rows
                .Bookmark = iNumAtrib
                If .Columns("USAR").Value Then
                    If .Columns("VALOR").Value <> "" Then
                        Select Case .Columns("IDTIPO").Value
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_text:=Replace(.Columns("VALOR").Value, "*", "%"))
                            Case TiposDeAtributos.TipoNumerico
                                Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_num:=.Columns("VALOR").Value, Formula:=.Columns("OPER").Value)
                            Case TiposDeAtributos.TipoFecha
                                Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_fec:=.Columns("VALOR").Value)
                            Case TiposDeAtributos.TipoBoolean
                                Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_bool:=.Columns("VALOR_ATRIB").Value)
                        End Select
                    Else
                        Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value)
                    End If
                    oAtributo.AmbitoAtributo = m_oAtributos.Item(.Columns("ID_ATRIB").Value).AmbitoAtributo
                End If
                iNumAtrib = iNumAtrib + 1
            Loop
            
            .Bookmark = 0
        End With
    End If
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    Set oCRArticulos = GenerarCRArticulos
    
    Screen.MousePointer = vbHourglass
        
    ' FORMULAS TEXTO RPT
    FormulaRpt(2, 1) = "txtTITULO"
    FormulaRpt(2, 2) = "txtSEL"
    FormulaRpt(2, 3) = "txtUNIDAD"
    FormulaRpt(2, 4) = "txtCANTIDAD"
    FormulaRpt(2, 5) = "txtPAG"
    FormulaRpt(2, 6) = "txtDE"
    FormulaRpt(2, 7) = "txtARTICULO"
    FormulaRpt(2, 8) = "txtGEN"
    FormulaRpt(2, 9) = "txtGasto"
    FormulaRpt(2, 10) = "txtInversion"
    FormulaRpt(2, 11) = "txtGastoInversion"
    FormulaRpt(2, 12) = "txtNoAlmacenable"
    FormulaRpt(2, 13) = "txtAlmacenamientoObligatorio"
    FormulaRpt(2, 14) = "txtAlmacenamientoOpcional"
    FormulaRpt(2, 15) = "txtNoRecepcionable"
    FormulaRpt(2, 16) = "txtRecepcionObligatoria"
    FormulaRpt(2, 17) = "txtRecepcionOpcional"
    
    
    ' edu 20071016 tarea 436

    For i = 1 To UBound(FormulaRpt, 2)
        n = crs_FormulaIndex(oReport, FormulaRpt(2, i))
        If n > 0 Then
            oReport.FormulaFields(n).Text = """" & FormulaRpt(1, i) & """"
        Else
        
            ' No existe ese campo.
            ' edu 7529
            ' quitado a peticion de Encarni.
            ' MsgBox "El report " & RepPath & " no contiene una referencia a la formula " & FormulaRpt(2, i)
            
        End If
        
    Next i

    oReport.FormulaFields(crs_FormulaIndex(oReport, "mostrarArtPlanta")).Text = """" & Me.chkIncluirArtPlanta.Value & """"
    
    If gParametrosGenerales.gbArticulosGenericos = False Then
        ' anular el parametro txtgen para que el report oculte el dato gen�rico
        oReport.FormulaFields(crs_FormulaIndex(oReport, FormulaRpt(2, 8))).Text = """" & "" & """"
    End If
    
    If optTodos.Value = True Then
        If chkArtGen Then
            sSeleccion = sSeleccion & " " & chkArtGen.caption 'S�lo art�culos gen�ricos
        Else
            sSeleccion = sListadoSel(1)                    'Todos los art�culos
        End If
    Else
        sSeleccion = sListadoSel(2) & " " & txtEstMat  'Rama de materiales:
        If chkArtGen Then
            sSeleccion = sSeleccion & " " & chkArtGen.caption 'S�lo art�culos gen�ricos
        End If
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTipoRecepcionLabel")).Text = """" & txtTipoRecepcion & """"
        
    Dim iPerfil As Integer
    If Not oUsuarioSummit.perfil Is Nothing Then
        iPerfil = oUsuarioSummit.perfil.Id
    Else
        iPerfil = 0
    End If
    
    Set adoresDG = oGestorInformes.ListadoArticulosEst(bRMat, optTodos, sCodEqp, sCodComp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, chkArtGen, m_arConcepto, m_arAlmacen, m_arRecep, oAtributos, IIf(chkSoloCentrales.Value = 1, True, False), m_bRestrMantUsu, m_bRestrMantPerf, iPerfil, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_oUonsSeleccionadas, False, False)
    
    If Not adoresDG Is Nothing Then
        oReport.Database.SetDataSource adoresDG
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    
    ' FORMULAS TEXTO SUBRPT
    FormulaSubRpt(2, 1) = "txtADJ"
    FormulaSubRpt(2, 2) = "txtCODPROVE"
    FormulaSubRpt(2, 3) = "txtDEN"
    FormulaSubRpt(2, 4) = "txtDEST"
    FormulaSubRpt(2, 5) = "txtFIN"
    FormulaSubRpt(2, 6) = "txtINI"
    FormulaSubRpt(2, 7) = "txtPRECIO"
    FormulaSubRpt(2, 8) = "txtUNI"

    
    
    If optOrdEst.Value = False Then
        Set SubListado = oReport.OpenSubreport("rptARTADJ")
        For i = 1 To UBound(FormulaSubRpt, 2)
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, FormulaSubRpt(2, i))).Text = """" & FormulaSubRpt(1, i) & """"
        Next i
        oCRArticulos.ListadoArticulos oReport, bRMat, sCodComp, sCodEqp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, optOrdEst, optOrdCod, optOrdDen, gParametrosGenerales.gsDEN_GMN1, gParametrosGenerales.gsDEN_GMN2, gParametrosGenerales.gsDEN_GMN3, gParametrosGenerales.gsDEN_GMN4, gParametrosGenerales.gsMONCEN, optTodos, chkArtDet, chkArtAdj, chkAdjDet, chkArtPos, m_arConcepto, m_arAlmacen, m_arRecep, IIf(Me.chkSoloCentrales.Value = 1, True, False), m_bRestrMantUsu, m_bRestrMantPerf, iPerfil, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_oUonsSeleccionadas
    Else
        ' rptARTEST
        Set SubListado = oReport.OpenSubreport("rptARTESTADJ")
        For i = 1 To UBound(FormulaSubRpt, 2)
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, FormulaSubRpt(2, i))).Text = """" & FormulaSubRpt(1, i) & """"
        Next i
        oCRArticulos.ListadoArticulosEst oReport, bRMat, sCodComp, sCodEqp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, optOrdEst, optOrdCod, optOrdDen, gParametrosGenerales.gsDEN_GMN1, gParametrosGenerales.gsDEN_GMN2, gParametrosGenerales.gsDEN_GMN3, gParametrosGenerales.gsDEN_GMN4, gParametrosGenerales.gsMONCEN, optTodos, chkArtDet, chkArtAdj, chkAdjDet, chkArtPos, chkArtGen, m_arConcepto, m_arAlmacen, m_arRecep, IIf(Me.chkSoloCentrales.Value = 1, True, False), m_bRestrMantUsu, m_bRestrMantPerf, iPerfil, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_oUonsSeleccionadas
    End If
    
    Set SubListado = Nothing
    
    'FORMULAS TEXTO SUBRPT ATRIBUTOS.
    FormulaSubRpt(4, 1) = "txtCODATRIB"
    FormulaSubRpt(4, 2) = "txtDENATRIB"
    FormulaSubRpt(4, 3) = "txtVALOR"
    FormulaSubRpt(4, 4) = "txtPROCESO"
    FormulaSubRpt(4, 5) = "txtPEDIDO"
    FormulaSubRpt(4, 6) = "txtINTERNO"
    FormulaSubRpt(4, 7) = "txtSi"
    FormulaSubRpt(4, 8) = "txtNo"
    
    
    If optOrdEst.Value = False Then
        Set SubListado = oReport.OpenSubreport("rtpARTATRIB")
        For i = 1 To UBound(FormulaSubRpt, 2)
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, FormulaSubRpt(4, i))).Text = """" & FormulaSubRpt(3, i) & """"
        Next i
        oCRArticulos.ListadoArticulos oReport, bRMat, sCodComp, sCodEqp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, optOrdEst, optOrdCod, optOrdDen, gParametrosGenerales.gsDEN_GMN1, gParametrosGenerales.gsDEN_GMN2, gParametrosGenerales.gsDEN_GMN3, gParametrosGenerales.gsDEN_GMN4, gParametrosGenerales.gsMONCEN, optTodos, chkArtDet, chkArtAdj, chkAdjDet, chkArtPos, m_arConcepto, m_arAlmacen, m_arRecep, IIf(Me.chkSoloCentrales.Value = 1, True, False), m_bRestrMantUsu, m_bRestrMantPerf, iPerfil, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_oUonsSeleccionadas
    Else
        ' rptATRIBEST
        Set SubListado = oReport.OpenSubreport("rptARTESTATRIB")
        For i = 1 To UBound(FormulaSubRpt, 2)
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, FormulaSubRpt(4, i))).Text = """" & FormulaSubRpt(3, i) & """"
        Next i
        oCRArticulos.ListadoArticulosEst oReport, bRMat, sCodComp, sCodEqp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, optOrdEst, optOrdCod, optOrdDen, gParametrosGenerales.gsDEN_GMN1, gParametrosGenerales.gsDEN_GMN2, gParametrosGenerales.gsDEN_GMN3, gParametrosGenerales.gsDEN_GMN4, gParametrosGenerales.gsMONCEN, optTodos, chkArtDet, chkArtAdj, chkAdjDet, chkArtPos, chkArtGen, m_arConcepto, m_arAlmacen, m_arRecep, IIf(Me.chkSoloCentrales.Value = 1, True, False), m_bRestrMantUsu, m_bRestrMantPerf, iPerfil, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_oUonsSeleccionadas
    End If
    Set SubListado = Nothing
    
    'FORMULAS TEXTO SUBRPT ARTESTART (Articulos de planta).
    ' FORMULAS TEXTO RPT
    
    
    ' rptARTESTART
    Set SubListado = oReport.OpenSubreport("rptARTESTART")
    'oReport.FormulaFields(crs_FormulaIndex(oReport, "mostrarSubPlanta")).Text = "1" 'Me.chkIncluirArtPlanta.Value
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArtPlanta")).Text = """" & gParametrosGenerales.gsArticulosPlanta & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArticulo")).Text = """" & m_sArticulos & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnidad")).Text = """" & m_sUni & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGEN")).Text = """" & m_sGenerico & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTipoRecepcion")).Text = """" & txtTipoRecepcion & """"
    'oCRArticulos.ListadoArticulosEst oReport, bRMat, sCodComp, sCodEqp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, optOrdEst, optOrdCod, optOrdDen, gParametrosGenerales.gsDEN_GMN1, gParametrosGenerales.gsDEN_GMN2, gParametrosGenerales.gsDEN_GMN3, gParametrosGenerales.gsDEN_GMN4, gParametrosGenerales.gsMONCEN, optTodos, chkArtDet, chkArtAdj, chkAdjDet, chkArtPos, chkArtGen, m_arConcepto, m_arAlmacen, m_arRecep
    
    Set SubListado = Nothing
    
    
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Unload frmESPERA
        Exit Sub
    End If
            
    Me.Hide
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.lblGeneral.caption = sEspera(1)  'Generando listado de art�culos
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2) 'seleccionando registros ...
    frmESPERA.lblDetalle = sEspera(3)  'Visualizando listado ...
    frmESPERA.Show
        
    Timer1.Enabled = True
    pv.caption = sTitulo 'Listado de art�culos
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me
    Set oCRArticulos = Nothing
    Set oReport = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdSelMat_Click()

    frmSELMAT.sOrigen = "frmLstART"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Show 1
    
End Sub

Private Sub cmdBuscarUon_Click()
    '''Cargar unidades organizativas en funci�n de las restricciones
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
    

    Dim frm As frmSELUO
    Set frm = New frmSELUO
    frm.multiselect = True
    frm.CheckChildren = False
    
    If Not m_oUonsSeleccionadas Is Nothing Then
        Set frm.UonsSeleccionadas = m_oUonsSeleccionadas
    End If
    
    oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.perfil.Id, , , False
    oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.perfil.Id, , , False
    oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.perfil.Id, , False

    frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
    frm.Show vbModal
    If frm.aceptar Then
        Me.txtUonsSeleccionadas.Text = frm.UonsSeleccionadas.titulo
        Set m_oUonsSeleccionadas = frm.UonsSeleccionadas
    End If
    
    Set frm = Nothing
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    CargarAtributos
End Sub

Private Sub Form_Load()

   ' Me.Width = 5715
   ' Me.Height = 3345
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    PonerFieldSeparator Me
    CargarComboOperandos
    Me.ConfigurarSeguridad
    
    ' Aplicar restricciones
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATRestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiAdjCon)) Is Nothing Then
        bRProve = True
        chkArtAdj.Visible = False
        chkAdjDet.Visible = False
    End If
    
    If FSEPConf Then
        
        chkArtAdj.Visible = False
        chkAdjDet.Visible = False
        chkArtAdj.Value = vbUnchecked
        chkAdjDet.Value = vbUnchecked
        chkArtDet.Top = chkArtDet.Top + 100
        chkArtPos.Top = chkArtPos.Top + 400
        
    End If
    
    chkArtGen.Visible = False
    
    If chkArtAdj.Value = vbUnchecked Then
        chkAdjDet.Enabled = False
    End If
    
    If optTodos.Value = True Then
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    End If
    
    ' edu 20071010 tarea 436 ocultar checkbox Generico cuando el par�metro vale falso

    If gParametrosGenerales.gbArticulosGenericos = False Then
        chkArtGen.Visible = False
        chkArtGen.Value = False
        '... y subir los otros checks para evitar el hueco
        chkArtDet.Top = chkArtDet.Top - 150
        chkArtPos.Top = chkArtPos.Top - 150
        chkArtAdj.Top = chkArtAdj.Top - 150
        chkAdjDet.Top = chkAdjDet.Top - 150
    Else
        chkArtGen.Visible = True
    End If
    
    Me.chkSoloCentrales.Visible = gParametrosGenerales.gbArticulosCentrales
    Me.chkIncluirArtPlanta.Visible = gParametrosGenerales.gbArticulosCentrales
    
    Set Me.UonsSeleccionadas = m_oUonsSeleccionadas
End Sub

''' <summary>Carga el combo de operandos</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarComboOperandos()
    Dim i As Integer
    
    arOper = Array("=", ">", ">=", "<", "<=")
    
    sdbddOper.RemoveAll
    
    sdbddOper.AddItem ""
    For i = 0 To UBound(arOper)
        sdbddOper.AddItem arOper(i) & Chr(m_lSeparador) & arOper(i)
    Next
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>Coloca los controles en el formulario en funci�n del tama�o de este</summary>
''' <remarks>Llamada desde: Form_Resize</remarks>

Private Sub Arrange()
    If Me.WindowState <> vbMinimized Then
        If Me.Height < 5220 Then Me.Height = 5220
        If Me.Width < 9720 Then Me.Width = 9720
        
        SSTab1.Width = Me.ScaleWidth - (cnEspacio / 2)
        SSTab1.Height = Me.ScaleHeight - Picture1.Height - (cnEspacio / 2)
        
        Picture1.Top = SSTab1.Top + SSTab1.Height + cnEspacio
        'Picture1.Width = Me.Width
        cmdObtener.Left = Picture1.Width - cmdObtener.Width - cnEspacio
        
        Frame2.Height = SSTab1.Height - (4 * cnEspacio)
        Frame2.Width = SSTab1.Width - (2 * cnEspacio)
        
        If chkArtGen.Visible Then
            lblBusqAtrib.Top = chkArtGen.Top + chkArtGen.Height + cnEspacio
        Else
            lblBusqAtrib.Top = optTodos.Top + optTodos.Height + (2 * cnEspacio)
        End If
        cmdBuscaAtrib.Top = lblBusqAtrib.Top - (cmdBuscaAtrib.Height - lblBusqAtrib.Height) - 40
        sdbgAtributos.Top = lblBusqAtrib.Top + lblBusqAtrib.Height
        
        sdbgAtributos.Width = Frame2.Width - (2 * cnEspacio)
        sdbgAtributos.Height = Frame2.Height - sdbgAtributos.Top - fraConcepto.Height - (2 * cnEspacio)
        fraConcepto.Top = Frame2.Height - fraConcepto.Height - cnEspacio
        fraAlmacen.Top = fraConcepto.Top
        fraRecepcion.Top = fraConcepto.Top

        sdbgAtributos.Columns("USAR").Width = 600
        sdbgAtributos.Columns("COD").Width = (sdbgAtributos.Width - 1200) * 0.22
        sdbgAtributos.Columns("DEN").Width = (sdbgAtributos.Width - 1200) * 0.42
        sdbgAtributos.Columns("OPER").Width = 600
        sdbgAtributos.Columns("VALOR").Width = (sdbgAtributos.Width - 1200) * 0.33
    End If
End Sub

Private Sub Form_Terminate()
    Set m_oUonsSeleccionadas = Nothing
End Sub

Private Sub optEst_Click()
    cmdSelMat.Enabled = True
    cmdBorrar.Enabled = True
End Sub

Private Sub optOrdCod_Click()
    chkArtPos.Enabled = True
End Sub

Private Sub optOrdDen_Click()
chkArtPos.Enabled = True
End Sub

Private Sub optOrdEst_Click()
    chkArtPos.Value = vbChecked
    chkArtPos.Enabled = False
End Sub

Private Sub optTodos_Click()
    txtEstMat = ""
    cmdSelMat.Enabled = False
    cmdBorrar.Enabled = False
End Sub

Private Sub sdbddOper_InitColumnProps()
    sdbddOper.DataFieldList = "Column 0"
    sdbddOper.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddOper_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddOper.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOper.Rows - 1
            bm = sdbddOper.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.Col).Value = Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOper.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddOper_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim i As Integer

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        
        ''' Comprobar la existencia en la lista
        For i = 0 To UBound(arOper)
            If arOper(i) = sdbgAtributos.Columns(sdbgAtributos.Col).Text Then
                bExiste = True
                Exit For
            End If
        Next
        
        If Not bExiste Then
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.Col).Text
            sdbgAtributos.Columns(sdbgAtributos.Col).Text = ""
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer
    Dim oLista As CValoresPond
    Dim oElem As CValorPond

    sdbddValor.RemoveAll
    
    sdbddValor.AddItem ""

    iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

    Set oatrib = oFSGSRaiz.Generar_CAtributo
    Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)

    If Not oatrib Is Nothing Then
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            oatrib.CargarListaDeValores
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                sdbddValor.AddItem oElem.ValorLista & Chr(m_lSeparador) & oElem.ValorLista
            Next
            Set oLista = Nothing
        Else
            'Campo de tipo boolean:
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem msTrue & Chr(m_lSeparador) & msTrue
                sdbddValor.AddItem msFalse & Chr(m_lSeparador) & msFalse
            End If
        End If
    
        Set oatrib = Nothing
    End If
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.Col).Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

            Set oatrib = oFSGSRaiz.Generar_CAtributo
            Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)
            
            If oatrib Is Nothing Then Exit Sub
            
            oatrib.CargarListaDeValores
            
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns(sdbgAtributos.Col).Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                If UCase(msTrue) = UCase(sdbgAtributos.Columns(sdbgAtributos.Col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "1"
                    bExiste = True
                End If
                If UCase(msFalse) = UCase(sdbgAtributos.Columns(sdbgAtributos.Col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "0"
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            sdbgAtributos.Columns(sdbgAtributos.Col).Text = ""
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.Col).caption
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub

Private Sub sdbgatributos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    With sdbgAtributos
        Select Case .Columns(ColIndex).Name
            Case "VALOR"
                'Comprobar que el valor introducido se corresponde con el tipo de atributo
                If .Columns("VALOR").Text <> "" Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoString
                        Case TiposDeAtributos.TipoNumerico
                            If (Not IsNumeric(.Columns("VALOR").Value)) Then
                                MsgBox "El valor del campo Valor debe ser num�rico", vbInformation, "FULLSTEP"
                                Cancel = True
                            Else
                                If .Columns("OPER").Text <> "" And .Columns("OPER").Text = "=" Then
                                    If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                        Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                                    End If
                                End If
                            End If
                            
                            If .Columns("OPER").Text = "" Then
                                .Columns("OPER").Text = "="
                                If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                    Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                                End If
                            End If
                        Case TiposDeAtributos.TipoFecha
                            If (Not IsDate(.Columns("VALOR").Text) And .Columns("VALOR").Value <> "") Then
                                MsgBox "El valor del campo Valor debe ser una fecha.", vbInformation, "FULLSTEP"
                                Cancel = True
                            Else
                                If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                    If CDate(.Columns("MINIMO").Value) > CDate(.Columns("VALOR").Value) Or CDate(.Columns("MAXIMO").Value) < CDate(.Columns("VALOR").Value) Then
                                        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", .Columns("MINIMO").Value), "@Valor2", .Columns("MAXIMO").Value), vbInformation, "FULLSTEP"
                                        Cancel = True
                                    End If
                                End If
                            End If
                        Case TiposDeAtributos.TipoBoolean
                    End Select
                End If
            Case "OPER"
                'Comprobar si se est� intentando dejar en blanco la col. del operando teniendo un valor introducido
                If .Columns("OPER").Text = vbNullString And .Columns("VALOR").Text <> vbNullString Then
                    Cancel = True
                ElseIf .Columns("OPER").Text = "=" And .Columns("VALOR").Text <> vbNullString Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoNumerico
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                            End If
                    End Select
                End If
        End Select
    End With
End Sub

''' <summary>Comprueba que un valor est� entre un m�nimo y un m�ximo</summary>
''' <remarks>Llamada desde: sdbgatributos_BeforeColUpdate</remarks>
''' <param name="dblValor">Valor</param>
''' <param name="dblMin">M�nimo</param>
''' <param name="dblMax">M�ximo</param>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Function ComprobarValoresMaxMin(ByVal dblValor As Double, ByVal dblMin As Double, ByVal dblMax As Double) As Boolean
    ComprobarValoresMaxMin = True
    
    If dblMin > dblValor Or dblMax < dblValor Then
        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", dblMin), "@Valor2", dblMax), vbInformation, "FULLSTEP"
        ComprobarValoresMaxMin = False
    End If
End Function

Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bCargarCombo As Boolean
    
    'Combo de operandos
    If sdbgAtributos.Columns("IDTIPO").Value = TipoNumerico Then
        sdbgAtributos.Columns("OPER").DropDownHwnd = sdbddOper.hWnd
        sdbgAtributos.Columns("OPER").Locked = False
    Else
        sdbgAtributos.Columns("OPER").DropDownHwnd = 0
        sdbgAtributos.Columns("OPER").Locked = True
    End If
    
    'Combo de valores
    bCargarCombo = False
    If sdbgAtributos.Col = sdbgAtributos.Columns("VALOR").Position Then
        If sdbgAtributos.Columns("INTRO").Value Then
            bCargarCombo = True
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TipoBoolean Then
                bCargarCombo = True
            End If
        End If
    End If
    
    If bCargarCombo Then
        sdbddValor.RemoveAll
        sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
        sdbddValor_DropDown
    Else
        sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
    End If
End Sub

Private Sub Timer1_Timer()
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
End Sub

''' <summary>Carga el grid de atributos con los atributos de material</summary>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Sub CargarAtributos()
    Dim oatrib As CAtributo
    Dim oGMN As CGrupoMatNivel4
    
    sdbgAtributos.RemoveAll
        
    Set oGMN = oFSGSRaiz.Generar_CGrupoMatNivel4
    oGMN.GMN1Cod = sGMN1Cod
    oGMN.GMN2Cod = sGMN2Cod
    oGMN.GMN3Cod = sGMN3Cod
    oGMN.Cod = sGMN4Cod
    
    Set m_oAtributos = Nothing
    Set m_oAtributos = oGMN.DevolverAtribMatYArtMat(oUons:=m_oUonsSeleccionadas)
    Set oGMN = Nothing
        
    If Not m_oAtributos Is Nothing Then
        If m_oAtributos.Count > 0 Then
            With sdbgAtributos
                For Each oatrib In m_oAtributos
                    .AddItem "0" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo
                Next
            End With
        End If
    End If
    
    Set oatrib = Nothing
End Sub

Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas

    Set UonsSeleccionadas = m_oUonsSeleccionadas

End Property

Public Property Set UonsSeleccionadas(oUonsSeleccionadas As CUnidadesOrganizativas)
    If Not oUonsSeleccionadas Is Nothing Then
        Set m_oUonsSeleccionadas = oUonsSeleccionadas
        Me.txtUonsSeleccionadas = m_oUonsSeleccionadas.titulo
    End If
End Property

Public Function addAtributo(ByRef oAtributo As CAtributo) As CAtributo
'    If Not oAtribs.Existe(oAtributo.Id) Then
'        Set addAtributo = oAtribs.addAtributo(oAtributo)
'    End If
End Function
