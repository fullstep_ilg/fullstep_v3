VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELPerWorkflow 
   BackColor       =   &H00808000&
   Caption         =   "A�adir aprovisionador"
   ClientHeight    =   5880
   ClientLeft      =   7155
   ClientTop       =   2805
   ClientWidth     =   5550
   Icon            =   "frmSELPerWorkflow.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5880
   ScaleWidth      =   5550
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   5400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPerWorkflow.frx":0CB2
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPerWorkflow.frx":107A
            Key             =   "Persona"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPerWorkflow.frx":1184
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPerWorkflow.frx":14D8
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPerWorkflow.frx":182C
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPerWorkflow.frx":1B80
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPerWorkflow.frx":1F14
            Key             =   "PerUsu"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPerWorkflow.frx":201E
            Key             =   "PersonaBaja"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPerWorkflow.frx":20A9
            Key             =   "PerUsuBaja"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   630
      Left            =   30
      ScaleHeight     =   630
      ScaleWidth      =   5445
      TabIndex        =   5
      Top             =   30
      Width           =   5445
      Begin VB.TextBox txtImporte 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   1860
         TabIndex        =   0
         Top             =   90
         Width           =   3405
      End
      Begin VB.Label lblImporte 
         BackColor       =   &H00808000&
         Caption         =   "Importe l�mite:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   150
         TabIndex        =   6
         Top             =   180
         Width           =   1305
      End
   End
   Begin VB.PictureBox picEstrOrg 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   4875
      Left            =   45
      ScaleHeight     =   4875
      ScaleWidth      =   5565
      TabIndex        =   4
      Top             =   570
      Width           =   5565
      Begin MSComctlLib.TreeView tvwEstrOrg 
         Height          =   4695
         Left            =   15
         TabIndex        =   1
         Top             =   105
         Width           =   5400
         _ExtentX        =   9525
         _ExtentY        =   8281
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   5550
      TabIndex        =   3
      Top             =   5460
      Width           =   5550
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "DAceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1680
         TabIndex        =   7
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2955
         TabIndex        =   2
         Top             =   30
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmSELPerWorkflow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Variables para interactuar con otros forms
Public sOrigen As String
Public oAprovSeleccionado As cAprovisionador
Public oAprovPadre As cAprovisionador
Public keyAprovisionadorSeleccionado As String
Public sCodPer As String
Public bRUO As Boolean
Public bRPerfUO As Boolean
Public bRDep As Boolean
Public dLimitePadre As Double
Public dLimiteHijos As Double
Public Categoria As Long 'Id de la categoria
Public NivelCategoria As Long 'Nivel de la categoria
Public idAprovisionador As Long

' Variable de control de flujo
Public Accion As accionessummit

'Variables de idiomas
Private sIdiAnyadirAp As String
Private sIdiModificarAp As String
Private sIdiSustituirAp As String
Private sIdiDetalle As String

'Variables del aproviosnador
Private sUON1 As String
Private sUON2 As String
Private sUON3 As String
Private sDenUON1 As String
Private sDenUON2 As String
Private sDenUON3 As String
Private sDep As String
Private sDenDep As String
Private sPer As String
Private sDenPer As String
Private m_sAprovisionadorExiste As String



Private Sub BuscarAprovisionador()
    Dim blnEncontrado As Boolean
    Dim oNodo As node
    
    blnEncontrado = False
    
    For Each oNodo In tvwestrorg.Nodes
        If UCase(oNodo.Tag) = UCase(keyAprovisionadorSeleccionado) Or UCase(oNodo.Tag) = UCase("A" & keyAprovisionadorSeleccionado) Or UCase(oNodo.Tag) = UCase("N" & keyAprovisionadorSeleccionado) Then
            blnEncontrado = True
            Exit For
        End If
    Next
    
    If blnEncontrado Then
        Set tvwestrorg.selectedItem = oNodo
        oNodo.EnsureVisible
        tvwestrorg.Enabled = False
    End If
    
    Set oNodo = Nothing
End Sub


''' <summary>Agrega o sustituye el aprovisionador a la categoria</summary>
Private Sub cmdAceptar_Click()
    Dim nodx As MSComctlLib.node
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    Dim iRes As Integer
    Dim oAprovisionadorModificado As cAprovisionador

    Set nodx = tvwestrorg.selectedItem
    
    If Not nodx Is Nothing Then
        
        If sOrigen = "frmCATSeguridad" Then
            'Validamos el importe
            If txtImporte.Text = "" Then
                oMensajes.FaltanDatos Left(lblImporte.caption, Len(lblImporte.caption) - 1)
                If Me.Visible Then txtImporte.SetFocus
                Exit Sub
            ElseIf Not IsNumeric(txtImporte.Text) Then
                oMensajes.NoValido Left(lblImporte.caption, Len(lblImporte.caption) - 1)
                If Me.Visible Then txtImporte.SetFocus
                Exit Sub
            End If
            
            Screen.MousePointer = vbHourglass
            Select Case Accion
            Case ACCCatSeguridadAnyaAprovi
                'A�ADIR APROVISIONADOR
                Set oAprovSeleccionado = oFSGSRaiz.Generar_CAprovisionador
                Select Case nodx.Image
                Case "Persona", "PerUsu"
                    'Aprovisionador de tipo Persona
                    EstablecerCodsAprovisionador nodx, "PER"
                    Set oAprovSeleccionado.Persona = oFSGSRaiz.Generar_CPersona
                    oAprovSeleccionado.Persona.Cod = sPer
                    oAprovSeleccionado.Persona.nombre = sDenPer
                    Set oAprovSeleccionado.Departamento = oFSGSRaiz.Generar_CDepartamento
                    oAprovSeleccionado.Departamento.Cod = sDep
                Case "UON1", "UON2", "UON3"
                    'Aprovisionador de tipo Unidad Organizativa
                    EstablecerCodsAprovisionador nodx, "UON"
                    Set oAprovSeleccionado.Departamento = Nothing
                    Set oAprovSeleccionado.Persona = Nothing
                Case "Departamento"
                    'Aprovisionador de tipo Departamento
                    EstablecerCodsAprovisionador nodx, "DEP"
                    Set oAprovSeleccionado.Departamento = oFSGSRaiz.Generar_CDepartamento
                    oAprovSeleccionado.Departamento.Cod = sDep
                    oAprovSeleccionado.Departamento.Den = sDenDep
                End Select
                oAprovSeleccionado.UON1 = sUON1
                oAprovSeleccionado.UON2 = sUON2
                oAprovSeleccionado.UON3 = sUON3
                oAprovSeleccionado.DenUON1 = sDenUON1
                oAprovSeleccionado.DenUON2 = sDenUON2
                oAprovSeleccionado.DenUON3 = sDenUON3
                oAprovSeleccionado.importe = CDbl(txtImporte)
                oAprovSeleccionado.Categoria = Categoria
                oAprovSeleccionado.Nivel = NivelCategoria
                
                'Se comprueba si el aprovisionador tendria acceso a EP(siempre que sea persona)
                If nodx.Image = "Persona" Or nodx.Image = "PerUsu" Then
                    If Left(nodx.Tag, 1) <> "A" Then
                        If frmCATSeguridad.bModifAcceso Then
                            If oAprovSeleccionado.TienePerfil = False Then
                                iRes = oMensajes.PreguntaDarAccesoFSEP(2)
                                If iRes = vbYes Then
                                    oAprovSeleccionado.DarAcceso = True
                                End If
                            Else
                                'El usuario no tiene acceso a FSEP en su perfil, se le tiene que dar acceso desde el perfil
                                oMensajes.UsuarioSinAccesoFSEP 2
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Else
                            oMensajes.UsuarioSinAccesoFSEP 2
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                    End If
                Else
                    'Mirar si alguno de loss usuarios del grupo no tiene acceso a EP
                    If oAprovSeleccionado.ExistenUsuariosSinAccesoEP() Then
                        'Mensaje diciendo que hay algun usuario que no tiene acceso
                        oMensajes.UsuarioSinAccesoFSEP 2
                    End If
                End If
                'Comprobamos que el aprovisionador que se pretende a�adir no existe ya en el listView
                If Not frmCATSeguridad.ExisteAprovisionadorEnLista(oAprovSeleccionado) Then
                    'Lo a�ado en bd
                    Set oIBaseDatos = oAprovSeleccionado
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    'Lo a�ado en el listview
                    frmCATSeguridad.A�adirAprovisionadorEnEstructura oAprovSeleccionado
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Set oIBaseDatos = Nothing
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                Else
                    'El aprovisionador ya existe en la lista
                    Screen.MousePointer = vbNormal
                    MsgBox m_sAprovisionadorExiste, vbExclamation
                    Exit Sub
                End If
            Case ACCCatSeguridadSusAprovi
                'SUSTITUIR APROVISIONADOR
                
                Set oAprovisionadorModificado = oFSGSRaiz.Generar_CAprovisionador
                'Le asigno el ID del aprovisionador que se va a sustituir
                oAprovisionadorModificado.Id = oAprovSeleccionado.Id
                Select Case nodx.Image
                Case "Persona", "PerUsu"
                    'Aprovisionador de tipo Persona
                    EstablecerCodsAprovisionador nodx, "PER"
                    Set oAprovisionadorModificado.Persona = oFSGSRaiz.Generar_CPersona
                    oAprovisionadorModificado.Persona.Cod = sPer
                    oAprovisionadorModificado.Persona.nombre = sDenPer
                    Set oAprovisionadorModificado.Departamento = oFSGSRaiz.Generar_CDepartamento
                    oAprovisionadorModificado.Departamento.Cod = sDep
                Case "UON1", "UON2", "UON3"
                    'Aprovisionador de tipo Unidad Organizativa
                    EstablecerCodsAprovisionador nodx, "UON"
                    Set oAprovisionadorModificado.Departamento = Nothing
                    Set oAprovisionadorModificado.Persona = Nothing
                Case "Departamento"
                    'Aprovisionador de tipo Departamento
                    EstablecerCodsAprovisionador nodx, "DEP"
                    Set oAprovisionadorModificado.Departamento = oFSGSRaiz.Generar_CDepartamento
                    oAprovisionadorModificado.Departamento.Cod = sDep
                    oAprovisionadorModificado.Departamento.Den = sDenDep
                End Select
                oAprovisionadorModificado.UON1 = sUON1
                oAprovisionadorModificado.UON2 = sUON2
                oAprovisionadorModificado.UON3 = sUON3
                oAprovisionadorModificado.DenUON1 = sDenUON1
                oAprovisionadorModificado.DenUON2 = sDenUON2
                oAprovisionadorModificado.DenUON3 = sDenUON3
                oAprovisionadorModificado.importe = CDbl(txtImporte)
                oAprovisionadorModificado.Categoria = Categoria
                oAprovisionadorModificado.Nivel = NivelCategoria
                    
                'Se comprueba que el aprovisionador no exista ya en la lista y si no existe se a�ade
                If Not frmCATSeguridad.ExisteAprovisionadorEnLista(oAprovisionadorModificado) Then
                    Set oIBaseDatos = oAprovisionadorModificado
                    'Finalizamos la edicion en Base de datos
                    teserror = oIBaseDatos.FinalizarEdicionModificando
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Set oIBaseDatos = Nothing
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                Else
                    'El aprovisionador ya existe en la lista
                    Screen.MousePointer = vbNormal
                    MsgBox m_sAprovisionadorExiste, vbExclamation
                    Exit Sub
                End If
                    
                'Asigno el aprovisionador modificado al aprovisionador seleccionado desde el formulario origen para modificarlo en el listView
                Set frmCATSeguridad.oAproviSeleccionado = oAprovisionadorModificado
                'Modificamos la lista de aprovisonadores para actualizar el provisionador
                frmCATSeguridad.ModificarAprovisionadorEnEstructura
            Case ACCCatSeguridadMODAprovi
                'MODIFICAR APROVISIONADOR
                
                Set oAprovisionadorModificado = oFSGSRaiz.Generar_CAprovisionador
                'Le asigno el ID del aprovisionador que se va a sustituir
                oAprovisionadorModificado.Id = oAprovSeleccionado.Id
                Select Case nodx.Image
                Case "Persona", "PerUsu"
                    'Aprovisionador de tipo Persona
                    EstablecerCodsAprovisionador nodx, "PER"
                    Set oAprovisionadorModificado.Persona = oFSGSRaiz.Generar_CPersona
                    oAprovisionadorModificado.Persona.Cod = sPer
                    oAprovisionadorModificado.Persona.nombre = sDenPer
                    Set oAprovisionadorModificado.Departamento = oFSGSRaiz.Generar_CDepartamento
                    oAprovisionadorModificado.Departamento.Cod = sDep
                Case "UON1", "UON2", "UON3"
                    'Aprovisionador de tipo Unidad Organizativa
                    EstablecerCodsAprovisionador nodx, "UON"
                    Set oAprovisionadorModificado.Departamento = Nothing
                    Set oAprovisionadorModificado.Persona = Nothing
                Case "Departamento"
                    'Aprovisionador de tipo Departamento
                    EstablecerCodsAprovisionador nodx, "DEP"
                    Set oAprovisionadorModificado.Departamento = oFSGSRaiz.Generar_CDepartamento
                    oAprovisionadorModificado.Departamento.Cod = sDep
                    oAprovisionadorModificado.Departamento.Den = sDenDep
                End Select
                oAprovisionadorModificado.UON1 = sUON1
                oAprovisionadorModificado.UON2 = sUON2
                oAprovisionadorModificado.UON3 = sUON3
                oAprovisionadorModificado.DenUON1 = sDenUON1
                oAprovisionadorModificado.DenUON2 = sDenUON2
                oAprovisionadorModificado.DenUON3 = sDenUON3
                oAprovisionadorModificado.importe = CDbl(txtImporte)
                oAprovisionadorModificado.Categoria = Categoria
                oAprovisionadorModificado.Nivel = NivelCategoria
                    
                
                Set oIBaseDatos = oAprovisionadorModificado
                'Finalizamos la edicion en Base de datos
                teserror = oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                'Asigno el aprovisionador modificado al aprovisionador seleccionado desde el formulario origen para modificarlo en el listView
                Set frmCATSeguridad.oAproviSeleccionado = oAprovisionadorModificado
                'Modificamos la lista de aprovisonadores para actualizar el provisionador
                frmCATSeguridad.ModificarAprovisionadorEnEstructura
            End Select
            
        RegistrarAccion Accion, "ID Aprov: " & oAprovSeleccionado.Id
                    
        End If
        
        Screen.MousePointer = vbNormal
   
    
    Unload Me
    
    End If
End Sub

Private Sub cmdCancelar_Click()
         
    Unload Me
    
End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELPERWORKFLOW, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        lblImporte = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiDetalle = Ador(0).Value
        Ador.MoveNext
        sIdiAnyadirAp = Ador(0).Value
        Ador.MoveNext
        sIdiModificarAp = Ador(0).Value
        Ador.MoveNext
        sIdiSustituirAp = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        m_sAprovisionadorExiste = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub





Private Sub Form_Load()
Dim nodx As node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

    Me.Width = 5670
    Me.Height = 6645
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    'Genera la estructura del arbol de las unidades organizativas
    GenerarEstructuraOrg False

    
    If Not oAprovSeleccionado Is Nothing Then
        'Si el aprovisionador es persona
        If Not oAprovSeleccionado.Persona Is Nothing Then
            If oAprovSeleccionado.Persona.BajaLog = 1 Then
                'Lo a�ade al �rbol con la image de persona de baja
                If Not IsNull(oAprovSeleccionado.Persona.UON3) Then
                    scod1 = oAprovSeleccionado.Persona.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oAprovSeleccionado.Persona.UON1))
                    scod2 = scod1 & oAprovSeleccionado.Persona.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oAprovSeleccionado.Persona.UON2))
                    scod3 = scod2 & oAprovSeleccionado.Persona.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oAprovSeleccionado.Persona.UON3))
                    scod4 = scod3 & oAprovSeleccionado.Persona.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oAprovSeleccionado.Persona.CodDep))
                    If Not IsNull(oAprovSeleccionado.Persona.Usuario) Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oAprovSeleccionado.Persona.Cod), CStr(oAprovSeleccionado.Persona.Cod) & " - " & oAprovSeleccionado.Persona.Apellidos & " " & oAprovSeleccionado.Persona.nombre, "PerUsuBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oAprovSeleccionado.Persona.Cod), CStr(oAprovSeleccionado.Persona.Cod) & " - " & oAprovSeleccionado.Persona.Apellidos & " " & oAprovSeleccionado.Persona.nombre, "PersonaBaja")
                    End If
                    nodx.Tag = "PER3" & CStr(oAprovSeleccionado.Persona.Cod)
                ElseIf Not IsNull(oAprovSeleccionado.Persona.UON2) Then
                    scod1 = oAprovSeleccionado.Persona.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oAprovSeleccionado.Persona.UON1))
                    scod2 = scod1 & oAprovSeleccionado.Persona.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oAprovSeleccionado.Persona.UON2))
                    scod3 = scod2 & oAprovSeleccionado.Persona.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oAprovSeleccionado.Persona.CodDep))
                    If Not IsNull(oAprovSeleccionado.Persona.Usuario) Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oAprovSeleccionado.Persona.Cod), CStr(oAprovSeleccionado.Persona.Cod) & " - " & oAprovSeleccionado.Persona.Apellidos & " " & oAprovSeleccionado.Persona.nombre, "PerUsuBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oAprovSeleccionado.Persona.Cod), CStr(oAprovSeleccionado.Persona.Cod) & " - " & oAprovSeleccionado.Persona.Apellidos & " " & oAprovSeleccionado.Persona.nombre, "PersonaBaja")
                    End If
                    nodx.Tag = "PER2" & CStr(oAprovSeleccionado.Persona.Cod)
                ElseIf Not IsNull(oAprovSeleccionado.Persona.UON1) Then
                    scod1 = oAprovSeleccionado.Persona.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oAprovSeleccionado.Persona.UON1))
                    scod2 = scod1 & oAprovSeleccionado.Persona.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oAprovSeleccionado.Persona.CodDep))
                    If Not IsNull(oAprovSeleccionado.Persona.Usuario) Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oAprovSeleccionado.Persona.Cod, oAprovSeleccionado.Persona.Cod & " - " & oAprovSeleccionado.Persona.Apellidos & " " & oAprovSeleccionado.Persona.nombre, "PerUsuBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oAprovSeleccionado.Persona.Cod, oAprovSeleccionado.Persona.Cod & " - " & oAprovSeleccionado.Persona.Apellidos & " " & oAprovSeleccionado.Persona.nombre, "PersonaBaja")
                    End If
                    nodx.Tag = "PER1" & CStr(oAprovSeleccionado.Persona.Cod)
                Else
                    scod1 = oAprovSeleccionado.Persona.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oAprovSeleccionado.Persona.CodDep))
                    If Not IsNull(oAprovSeleccionado.Persona.Usuario) Then
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oAprovSeleccionado.Persona.Cod), CStr(oAprovSeleccionado.Persona.Cod) & " " & oAprovSeleccionado.Persona.Apellidos & " " & oAprovSeleccionado.Persona.nombre, "PerUsuBaja")
                    Else
                        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oAprovSeleccionado.Persona.Cod), CStr(oAprovSeleccionado.Persona.Cod) & " " & oAprovSeleccionado.Persona.Apellidos & " " & oAprovSeleccionado.Persona.nombre, "PersonaBaja")
                    End If
                    nodx.Tag = "PER0" & CStr(oAprovSeleccionado.Persona.Cod)
                End If
                If oAprovSeleccionado.Persona.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
                
            Else
                Set nodx = tvwestrorg.Nodes.Item("PERS" & CStr(oAprovSeleccionado.Persona.Cod))
            End If
            nodx.EnsureVisible
            nodx.Selected = True
            Set nodx = Nothing
        End If
        
        
    End If

    Select Case Accion
        Case ACCCatSeguridadMODAprovi 'Solo pueden modificar el importe y mod destino
            picEstrOrg.Enabled = True
            PicDatos.Enabled = True
            caption = sIdiModificarAp
            BuscarAprovisionador
            txtImporte.Text = oAprovSeleccionado.importe
        Case ACCCatSeguridadAnyaAprovi 'Tiene que meter todo
            picEstrOrg.Enabled = True
            PicDatos.Enabled = True
            caption = sIdiAnyadirAp
            tvwestrorg.Enabled = True
        Case ACCCatSeguridadSusAprovi 'Sustituye el aprovisionador y puede cambiar el importe
            picEstrOrg.Enabled = True
            PicDatos.Enabled = True
            caption = sIdiAnyadirAp
            txtImporte.Text = oAprovSeleccionado.importe
            tvwestrorg.Enabled = True
        Case ACCCatSeguridadCon 'No hace nada
            caption = sIdiDetalle
            picEstrOrg.Enabled = False
            PicDatos.Enabled = False
            picNavigate.Visible = False
            tvwestrorg.Enabled = True
        End Select
    
    Exit Sub
    
End Sub

Private Sub Form_Resize()

If Me.Height < 1500 Then Exit Sub
If Me.Width < 800 Then Exit Sub
        
    picEstrOrg.Height = Me.Height - PicDatos.Height - picNavigate.Height - 485
    tvwestrorg.Height = picEstrOrg.Height - 50
    picEstrOrg.Width = Me.Width - 205
    tvwestrorg.Width = picEstrOrg.Width - 50

End Sub

Private Sub Form_Unload(Cancel As Integer)

Set oAprovSeleccionado = Nothing
Set oAprovPadre = Nothing
    
End Sub

''' <summary>Genera la estructura de la organizaci�n</summary>
''' <param name="bOrdenadoPorDen">Orden por denominaci�n</param>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    ' Departamentos asociados
    Dim oDepsAsocN0 As CDepAsociados
    Dim oDepsAsocN1 As CDepAsociados
    Dim oDepsAsocN2 As CDepAsociados
    Dim oDepsAsocN3 As CDepAsociados
    Dim oDepAsoc As CDepAsociado
    ' Unidades organizativas
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    ' Personas
    Dim oPersN0 As CPersonas
    Dim oPersN1 As CPersonas
    Dim oPersN2 As CPersonas
    Dim oPersN3 As CPersonas
    Dim oPer As CPersona
    ' Otras
    Dim nodx As node
    Dim varCodPersona As Variant
    Dim lIdPerfil As Long

    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
         
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or oUsuarioSummit.Tipo = TIpoDeUsuario.comprador) And (bRUO Or bRDep Or bRPerfUO) Then
        If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
        varCodPersona = oUsuarioSummit.Persona.Cod
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
        
        ' Cargamos las personas  de esos departamentos.
        For Each oDepAsoc In oDepsAsocN0
             oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
        Next
            
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False, , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, , , , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False, , , , , , , , , , , , , , , , bRPerfUO, lIdPerfil
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False, , , bRPerfUO, lIdPerfil
                ' Cargamos las personas  de esos departamentos.
        
                'Personas
                
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                   oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                    
                Next
        End Select
        
        
    Else
        
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
        oPersN0.CargarTodasLasPersonas , , , , 0, , True, , , False, , True
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True
                oPersN3.CargarTodasLasPersonas , , , , 3, , True, , , False, , True
                
        End Select
        
        
    End If
    
   '************************************************************
    'Generamos la estructura arborea
    tvwestrorg.Nodes.clear

    ' Unidades organizativas
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
            
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
            
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
            
    Next
    
    ' Departamentos
    
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) _
        And (bRUO Or bRDep Or bRPerfUO) Then
    
        For Each oDepAsoc In oDepsAsocN0
            
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            Next
        
        Next
                   
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            
            Next
        
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            
            Next
       
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            
            Next
        Next
    
    Else
    
        'Departamentos
            
            For Each oDepAsoc In oDepsAsocN0
            
                scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            
            Next
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            
        Next
                
            'Personas
            
            For Each oPer In oPersN0
                scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            
            Next
            
            For Each oPer In oPersN1
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
                
            Next
            
            For Each oPer In oPersN2
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            Next
            
            For Each oPer In oPersN3
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
                scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
                If oPer.AccesoFSEP Then
                    nodx.Tag = "A" & nodx.Tag
                Else
                    nodx.Tag = "N" & nodx.Tag
                End If
            Next
    End If
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
    
End Sub


Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If Len(node.Tag) < 5 Then
    DevolverCod = node.Tag
    Exit Function
End If
DevolverCod = Right(node.Tag, Len(node.Tag) - 5)

End Function
''' <summary>Asigna a las variables sDep,sUON1,sUON2,sUON3,sPer y las denominaciones los valores en funcion del aprovisionador elegido </summary>
''' <param name="Node">Nodo del aprovisionador seleccionado</param>
''' <param name="TipoAprovionador">Tipo de aprovisionador, si es departamento, persona o unidad organizativa</param>
Private Sub EstablecerCodsAprovisionador(ByVal node As MSComctlLib.node, ByVal TipoAprovisionador As String)

Select Case TipoAprovisionador
    Case "DEP"
        'Departamento
        Select Case Mid(node.Tag, 1, 4)
        'Recojo el codigo de departamento y las Uons de las que cuelga
        Case "DEP0"
            sDep = Replace(node.Tag, "DEP0", "")
            sUON1 = ""
            sUON2 = ""
            sUON3 = ""
        Case "DEP1"
            sDep = Replace(node.Tag, "DEP1", "")
            sUON1 = Replace(node.Parent.Tag, "UON1", "")
            sUON2 = ""
            sUON3 = ""
        Case "DEP2"
            sDep = Replace(node.Tag, "DEP2", "")
            sUON2 = Replace(node.Parent.Tag, "UON2", "")
            sUON1 = Replace(node.Parent.Parent.Tag, "UON1", "")
            sUON3 = ""
        Case "DEP3"
            sDep = Replace(node.Tag, "DEP3", "")
            sUON3 = Replace(node.Parent.Tag, "UON3", "")
            sUON2 = Replace(node.Parent.Parent.Tag, "UON2", "")
            sUON1 = Replace(node.Parent.Parent.Parent.Tag, "UON1", "")
        End Select
        sDenDep = Trim(Mid(node.Text, InStr(1, node.Text, "-") + 1, Len(node.Text)))
    Case "UON"
        'Unidad Organizativa
        Select Case Mid(node.Tag, 1, 4)
        'Recojo los codigos de las Uons y sus denominaciones
        Case "UON1"
            sUON1 = Replace(node.Tag, "UON1", "")
            sUON2 = ""
            sUON3 = ""
            sDenUON1 = Trim(Mid(node.Text, InStr(1, node.Text, "-") + 1, Len(node.Text)))
        Case "UON2"
            sUON2 = Replace(node.Tag, "UON2", "")
            sDenUON2 = Trim(Mid(node.Text, InStr(1, node.Text, "-") + 1, Len(node.Text)))
            sUON1 = Replace(node.Parent.Tag, "UON1", "")
            sDenUON1 = Trim(Mid(node.Parent.Text, InStr(1, node.Parent.Text, "-") + 1, Len(node.Parent.Text)))
            sUON3 = ""
        Case "UON3"
            sUON3 = Replace(node.Tag, "UON3", "")
            sDenUON3 = Trim(Mid(node.Text, InStr(1, node.Text, "-") + 1, Len(node.Text)))
            sUON2 = Replace(node.Parent.Tag, "UON2", "")
            sDenUON2 = Trim(Mid(node.Parent.Text, InStr(1, node.Parent.Text, "-") + 1, Len(node.Parent.Text)))
            sUON1 = Replace(node.Parent.Parent.Tag, "UON1", "")
            sDenUON1 = Trim(Mid(node.Parent.Parent.Text, InStr(1, node.Parent.Parent.Text, "-") + 1, Len(node.Parent.Parent.Text)))
        End Select
    Case "PER"
        'Persona
        Select Case Mid(node.Tag, 1, 5)
        'Recojo los codigos de las Uons y sus denominaciones
        Case "NPER0", "APER0"
            sPer = Replace(node.Tag, "NPER0", "")
            sPer = Replace(sPer, "APER0", "")
            Select Case Mid(node.Parent.Tag, 1, 4)
                'Se mira si la persona cuelga de un departamento
                Case "DEP0"
                    sDep = Replace(node.Parent.Tag, "DEP0", "")
            End Select
            sUON1 = ""
            sUON2 = ""
            sUON3 = ""
        Case "NPER1", "APER1"
            sPer = Replace(node.Tag, "NPER1", "")
            sPer = Replace(sPer, "APER1", "")
            Select Case Mid(node.Parent.Tag, 1, 4)
                'Se mira si la persona cuelga de un departamento o de una UON
                Case "UON1"
                    sUON1 = Replace(node.Parent.Parent.Tag, "UON1", "")
                Case "DEP1"
                    sDep = Replace(node.Parent.Tag, "DEP1", "")
                    sUON1 = Replace(node.Parent.Parent.Tag, "UON1", "")
            End Select
            sUON2 = ""
            sUON3 = ""
        Case "NPER2", "APER2"
            sPer = Replace(node.Tag, "NPER2", "")
            sPer = Replace(sPer, "APER2", "")
            Select Case Mid(node.Parent.Tag, 1, 4)
                'Se mira si la persona cuelga de un departamento o de una UON
                Case "UON2"
                    sUON2 = Replace(node.Parent.Tag, "UON2", "")
                    sUON1 = Replace(node.Parent.Parent.Tag, "UON1", "")
                Case "DEP2"
                    sDep = Replace(node.Parent.Tag, "DEP2", "")
                    sUON2 = Replace(node.Parent.Parent.Tag, "UON2", "")
                    sUON1 = Replace(node.Parent.Parent.Parent.Tag, "UON1", "")
            End Select
            sUON3 = ""
        Case "NPER3", "APER3"
            sPer = Replace(node.Tag, "NPER3", "")
            sPer = Replace(sPer, "APER3", "")
            Select Case Mid(node.Parent.Tag, 1, 4)
                'Se mira si la persona cuelga de un departamento o de una UON
                Case "UON3"
                    sUON3 = Replace(node.Parent.Tag, "UON3", "")
                    sUON2 = Replace(node.Parent.Parent.Tag, "UON2", "")
                    sUON1 = Replace(node.Parent.Parent.Parent.Tag, "UON1", "")
                Case "DEP3"
                    sDep = Replace(node.Parent.Tag, "DEP3", "")
                    sUON3 = Replace(node.Parent.Parent.Tag, "UON3", "")
                    sUON2 = Replace(node.Parent.Parent.Parent.Tag, "UON2", "")
                    sUON1 = Replace(node.Parent.Parent.Parent.Parent.Tag, "UON1", "")
            End Select
        End Select
        sDenPer = Trim(Mid(node.Text, InStr(1, node.Text, "-") + 1, Len(node.Text)))
End Select

End Sub


Private Sub txtImporte_Validate(Cancel As Boolean)
    
If txtImporte.Text <> "" Then
    
     If IsNumeric(txtImporte.Text) Then
        txtImporte = Format(txtImporte, "#,##0.##")
        If Not IsNumeric(Right(txtImporte, 1)) Then
            txtImporte = Left(txtImporte, Len(txtImporte) - 1)
        End If

   End If
End If
End Sub

