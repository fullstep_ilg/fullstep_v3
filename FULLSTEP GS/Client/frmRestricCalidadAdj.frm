VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmRestricCalidadAdj 
   Caption         =   "frmRestricCalidadAdj"
   ClientHeight    =   7335
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10080
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRestricCalidadAdj.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7335
   ScaleWidth      =   10080
   Begin VB.PictureBox PicPymes 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   0
      ScaleHeight     =   255
      ScaleWidth      =   4215
      TabIndex        =   3
      Top             =   120
      Width           =   4215
      Begin SSDataWidgets_B.SSDBCombo sdbcPYME 
         Height          =   285
         Left            =   720
         TabIndex        =   4
         Top             =   0
         Width           =   3435
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1773
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   4260
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6068
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblPYME 
         AutoSize        =   -1  'True
         Caption         =   "PYME:"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   0
         Width           =   450
      End
   End
   Begin VB.CommandButton cmdModoEdicion 
      Caption         =   "&Edici�n"
      Height          =   345
      Left            =   9000
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   6720
      Width           =   1005
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "&Refrescar"
      Height          =   345
      Left            =   120
      TabIndex        =   1
      Top             =   6720
      Width           =   1500
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgVariables 
      Height          =   6225
      Left            =   0
      TabIndex        =   0
      Top             =   480
      Width           =   9975
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   14
      stylesets.count =   6
      stylesets(0).Name=   "Nivel1"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8421504
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmRestricCalidadAdj.frx":014A
      stylesets(0).AlignmentText=   0
      stylesets(1).Name=   "Nivel2"
      stylesets(1).BackColor=   13619151
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmRestricCalidadAdj.frx":0166
      stylesets(1).AlignmentText=   0
      stylesets(2).Name=   "Nivel3"
      stylesets(2).BackColor=   13417139
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmRestricCalidadAdj.frx":0182
      stylesets(2).AlignmentText=   0
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmRestricCalidadAdj.frx":019E
      stylesets(4).Name=   "Nivel4"
      stylesets(4).BackColor=   12566463
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmRestricCalidadAdj.frx":01BA
      stylesets(4).AlignmentText=   0
      stylesets(5).Name=   "Nivel5"
      stylesets(5).BackColor=   11513775
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmRestricCalidadAdj.frx":01D6
      stylesets(5).AlignmentText=   0
      UseGroups       =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   3
      Groups(0).Width =   2778
      Groups(0).Caption=   "DVariable"
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   820
      Groups(0).Columns(0).Visible=   0   'False
      Groups(0).Columns(0).Caption=   "VARCAL"
      Groups(0).Columns(0).Name=   "VARCAL"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   1005
      Groups(0).Columns(1).Visible=   0   'False
      Groups(0).Columns(1).Caption=   "NIVEL"
      Groups(0).Columns(1).Name=   "NIVEL"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(2).Width=   2778
      Groups(0).Columns(2).Caption=   "Variable"
      Groups(0).Columns(2).Name=   "DEN"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(1).Width =   8493
      Groups(1).Caption=   "DRestriccion"
      Groups(1).Columns.Count=   3
      Groups(1).Columns(0).Width=   1138
      Groups(1).Columns(0).Caption=   "Restricci�n"
      Groups(1).Columns(0).Name=   "RESTRICCION"
      Groups(1).Columns(0).DataField=   "Column 3"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Style=   2
      Groups(1).Columns(1).Width=   1032
      Groups(1).Columns(1).Caption=   "Inferior"
      Groups(1).Columns(1).Name=   "R_INF"
      Groups(1).Columns(1).Alignment=   1
      Groups(1).Columns(1).CaptionAlignment=   2
      Groups(1).Columns(1).DataField=   "Column 4"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).NumberFormat=   "Standard"
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(2).Width=   6324
      Groups(1).Columns(2).Caption=   "Superior"
      Groups(1).Columns(2).Name=   "R_SUP"
      Groups(1).Columns(2).Alignment=   1
      Groups(1).Columns(2).CaptionAlignment=   2
      Groups(1).Columns(2).DataField=   "Column 5"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).NumberFormat=   "Standard"
      Groups(1).Columns(2).FieldLen=   256
      Groups(2).Width =   8493
      Groups(2).Caption=   "DAviso"
      Groups(2).Columns.Count=   8
      Groups(2).Columns(0).Width=   2593
      Groups(2).Columns(0).Caption=   "Aviso"
      Groups(2).Columns(0).Name=   "AVISO"
      Groups(2).Columns(0).DataField=   "Column 6"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(0).Style=   2
      Groups(2).Columns(1).Width=   1879
      Groups(2).Columns(1).Caption=   "Inferior"
      Groups(2).Columns(1).Name=   "A_INF"
      Groups(2).Columns(1).Alignment=   1
      Groups(2).Columns(1).CaptionAlignment=   2
      Groups(2).Columns(1).DataField=   "Column 7"
      Groups(2).Columns(1).DataType=   8
      Groups(2).Columns(1).NumberFormat=   "Standard"
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(2).Width=   4022
      Groups(2).Columns(2).Caption=   "Superior"
      Groups(2).Columns(2).Name=   "A_SUP"
      Groups(2).Columns(2).Alignment=   1
      Groups(2).Columns(2).CaptionAlignment=   2
      Groups(2).Columns(2).DataField=   "Column 8"
      Groups(2).Columns(2).DataType=   8
      Groups(2).Columns(2).NumberFormat=   "Standard"
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(3).Width=   900
      Groups(2).Columns(3).Visible=   0   'False
      Groups(2).Columns(3).Caption=   "EXISTE"
      Groups(2).Columns(3).Name=   "EXISTE"
      Groups(2).Columns(3).DataField=   "Column 9"
      Groups(2).Columns(3).DataType=   8
      Groups(2).Columns(3).FieldLen=   256
      Groups(2).Columns(4).Width=   4260
      Groups(2).Columns(4).Visible=   0   'False
      Groups(2).Columns(4).Caption=   "ID_VAR_CAL1"
      Groups(2).Columns(4).Name=   "ID_VAR_CAL1"
      Groups(2).Columns(4).DataField=   "Column 10"
      Groups(2).Columns(4).DataType=   8
      Groups(2).Columns(4).FieldLen=   256
      Groups(2).Columns(5).Width=   8493
      Groups(2).Columns(5).Visible=   0   'False
      Groups(2).Columns(5).Caption=   "ID_VAR_CAL2"
      Groups(2).Columns(5).Name=   "ID_VAR_CAL2"
      Groups(2).Columns(5).DataField=   "Column 11"
      Groups(2).Columns(5).DataType=   8
      Groups(2).Columns(5).FieldLen=   256
      Groups(2).Columns(6).Width=   8493
      Groups(2).Columns(6).Visible=   0   'False
      Groups(2).Columns(6).Caption=   "ID_VAR_CAL3"
      Groups(2).Columns(6).Name=   "ID_VAR_CAL3"
      Groups(2).Columns(6).DataField=   "Column 12"
      Groups(2).Columns(6).DataType=   8
      Groups(2).Columns(6).FieldLen=   256
      Groups(2).Columns(7).Width=   7382
      Groups(2).Columns(7).Visible=   0   'False
      Groups(2).Columns(7).Caption=   "ID_VAR_CAL4"
      Groups(2).Columns(7).Name=   "ID_VAR_CAL4"
      Groups(2).Columns(7).DataField=   "Column 13"
      Groups(2).Columns(7).DataType=   8
      Groups(2).Columns(7).FieldLen=   256
      _ExtentX        =   17595
      _ExtentY        =   10980
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmRestricCalidadAdj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bRespetarChange As Boolean
Private m_sTotal As String
Private m_sIdiConsulta As String
Private m_sIdiEdicion As String
Private m_oCalidad As CVariablesCalidad
Private bModoEdicion As Boolean
Private Const IntervaloRestricc As Integer = 189
Private Const IntervaloAviso As Integer = 190
Private Const IntervaloCoincide As Integer = 191

Private m_sCodTotalProveedor As String

Private Sub cmdModoEdicion_Click()
    If Not bModoEdicion Then
        sdbgVariables.AllowAddNew = False
        sdbgVariables.AllowUpdate = True
        sdbgVariables.AllowDelete = False
        
        sdbgVariables.Columns("R_SUP").NumberFormat = "#,##0.00#"
        sdbgVariables.Columns("R_INF").NumberFormat = "#,##0.00#"
        sdbgVariables.Columns("A_SUP").NumberFormat = "#,##0.00#"
        sdbgVariables.Columns("A_INF").NumberFormat = "#,##0.00#"
        
        Me.cmdRefresh.Enabled = False
        
        Me.cmdModoEdicion.caption = m_sIdiConsulta
        
        bModoEdicion = True
    Else
        If Me.sdbgVariables.DataChanged = True Then
            
            Me.sdbgVariables.Update
            
            If m_bRespetarChange Then
                Exit Sub
            End If
            
        End If
        
        sdbgVariables.AllowAddNew = False
        sdbgVariables.AllowUpdate = False
        sdbgVariables.AllowDelete = False
        
        sdbgVariables.Columns("R_SUP").NumberFormat = "Standard"
        sdbgVariables.Columns("R_INF").NumberFormat = "Standard"
        sdbgVariables.Columns("A_SUP").NumberFormat = "Standard"
        sdbgVariables.Columns("A_INF").NumberFormat = "Standard"
        
        Me.cmdRefresh.Enabled = True
        
        Me.cmdModoEdicion.caption = m_sIdiEdicion
                
        bModoEdicion = False
    End If
End Sub

Private Sub cmdRefresh_Click()
Dim lIdPyme As Long
    If gParametrosGenerales.gbPymes Then
        lIdPyme = Me.sdbcPYME.Columns("ID").Value
        CargarVariablesCalidad lIdPyme
    Else
        CargarVariablesCalidad
    End If
    
    
End Sub

Private Sub Form_Load()
    Me.Width = 10000
    Me.Height = 7500
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
   
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If gParametrosGenerales.gbPymes Then
        PicPymes.Visible = True
    
    Else
        PicPymes.Visible = False
        CargarVariablesCalidad
    End If
    
    sdbgVariables.AllowAddNew = False
    sdbgVariables.AllowUpdate = False
    sdbgVariables.AllowDelete = False
    
    bModoEdicion = False
       
End Sub

Private Sub Form_Resize()
    If Me.Width < 500 Then Exit Sub
    If Me.Height < 500 Then Exit Sub
    
    
    If gParametrosGenerales.gbPymes Then
        sdbgVariables.Height = Me.Height - 1640
        Me.sdbgVariables.Top = 600
    Else
        sdbgVariables.Height = Me.Height - 940
        sdbgVariables.Top = 0
    End If
    sdbgVariables.Width = Me.Width - 260
        
    sdbgVariables.Groups(0).Width = sdbgVariables.Width - (6 * 960) - 600
    sdbgVariables.Columns("DEN").Width = sdbgVariables.Groups(0).Width
    
    sdbgVariables.Groups(1).Width = 960 * 3
    sdbgVariables.Columns("RESTRICCION").Width = 960
    sdbgVariables.Columns("R_INF").Width = 960
    sdbgVariables.Columns("R_SUP").Width = 960
    
    sdbgVariables.Groups(2).Width = 960 * 3
    sdbgVariables.Columns("AVISO").Width = 960
    sdbgVariables.Columns("A_INF").Width = 960
    sdbgVariables.Columns("A_SUP").Width = 960
    
    
    Me.cmdRefresh.Top = Me.sdbgVariables.Top + Me.sdbgVariables.Height + 20
    
    Me.cmdModoEdicion.Top = Me.cmdRefresh.Top
    Me.cmdModoEdicion.Left = Me.sdbgVariables.Left + Me.sdbgVariables.Width - Me.cmdModoEdicion.Width
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Me.sdbgVariables.DataChanged = True Then
        
        Me.sdbgVariables.Update
        
        If m_bRespetarChange Then
            Cancel = True
        End If
        
    End If
End Sub

Private Sub sdbgVariables_BeforeUpdate(Cancel As Integer)
'MPG(17/03/2009)
'**********************************************************************
'Descripcion:= Modifica/A�ade (parametro m_bModificado) la restriccion y/o aviso de la variable de calidad
'Parametros entrada:=   ninguno
'Tiempo:= 0,21seg.
'***********************************************************************
    
    Dim sClave As String
    
    Dim sIdVarCal1 As String
    Dim sIdVarCal2 As String
    Dim sIdVarCal3 As String
    Dim sIdVarCal4 As String
    
    Dim oCalidad As CVariableCalidad
    
    sClave = sdbgVariables.Columns("NIVEL").Value & sdbgVariables.Columns("VARCAL").Value
    
    If sdbgVariables.Columns("NIVEL").Value > 1 Then
        sIdVarCal1 = "1" & sdbgVariables.Columns("ID_VAR_CAL1").Value
    End If
    If sdbgVariables.Columns("NIVEL").Value > 2 Then
        sIdVarCal2 = "2" & sdbgVariables.Columns("ID_VAR_CAL2").Value
    End If
    If sdbgVariables.Columns("NIVEL").Value > 3 Then
        sIdVarCal3 = "3" & sdbgVariables.Columns("ID_VAR_CAL3").Value
    End If
    If sdbgVariables.Columns("NIVEL").Value > 4 Then
        sIdVarCal4 = "4" & sdbgVariables.Columns("ID_VAR_CAL4").Value
    End If
    
    Select Case sdbgVariables.Columns("NIVEL").Value
    Case 0
        Set oCalidad = m_oCalidad.Item(sClave)
    Case 1
        Set oCalidad = m_oCalidad.Item(m_sCodTotalProveedor).VariblesCal.Item(sClave)
    Case 2
        Set oCalidad = m_oCalidad.Item(m_sCodTotalProveedor).VariblesCal.Item(sIdVarCal1).VariblesCal.Item(sClave)
    Case 3
        Set oCalidad = m_oCalidad.Item(m_sCodTotalProveedor).VariblesCal.Item(sIdVarCal1).VariblesCal.Item(sIdVarCal2).VariblesCal.Item(sClave)
    Case 4
        Set oCalidad = m_oCalidad.Item(m_sCodTotalProveedor).VariblesCal.Item(sIdVarCal1).VariblesCal.Item(sIdVarCal2).VariblesCal.Item(sIdVarCal3).VariblesCal.Item(sClave)
    Case 5
        Set oCalidad = m_oCalidad.Item(m_sCodTotalProveedor).VariblesCal.Item(sIdVarCal1).VariblesCal.Item(sIdVarCal2).VariblesCal.Item(sIdVarCal3).VariblesCal.Item(sIdVarCal4).VariblesCal.Item(sClave)
    End Select
    
    
    m_bRespetarChange = False
    
    '-----------RESTRICCION
    If sdbgVariables.Columns("R_INF").Value <> "" Then
        If Not IsNumeric(sdbgVariables.Columns("R_INF").Value) Then
            oMensajes.NoValida sdbgVariables.Columns("RESTRICCION").caption & " " & sdbgVariables.Columns("R_INF").caption
            Cancel = True
            If Me.Visible Then sdbgVariables.SetFocus
            Exit Sub
        End If
    End If
    If sdbgVariables.Columns("R_SUP").Value <> "" Then
        If Not IsNumeric(sdbgVariables.Columns("R_SUP").Value) Then
            oMensajes.NoValida sdbgVariables.Columns("RESTRICCION").caption & " " & sdbgVariables.Columns("R_SUP").caption
            Cancel = True
            If Me.Visible Then sdbgVariables.SetFocus
            Exit Sub
        End If
    End If
    
    If sdbgVariables.Columns("R_INF").Value <> "" And sdbgVariables.Columns("R_SUP").Value <> "" Then
        If StrToDblOrNull(sdbgVariables.Columns("R_INF").Value) > StrToDblOrNull(sdbgVariables.Columns("R_SUP").Value) Then
            oMensajes.NoValido IntervaloRestricc
            
            m_bRespetarChange = True
            
            Cancel = True
            Set oCalidad = Nothing
            Exit Sub
        End If
    End If
    
    '-----------AVISO
    If sdbgVariables.Columns("A_INF").Value <> "" Then
        If Not IsNumeric(sdbgVariables.Columns("A_INF").Value) Then
            oMensajes.NoValida sdbgVariables.Columns("AVISO").caption & " " & sdbgVariables.Columns("A_INF").caption
            Cancel = True
            If Me.Visible Then sdbgVariables.SetFocus
            Exit Sub
        End If
    End If
    If sdbgVariables.Columns("A_SUP").Value <> "" Then
        If Not IsNumeric(sdbgVariables.Columns("A_SUP").Value) Then
            oMensajes.NoValida sdbgVariables.Columns("AVISO").caption & " " & sdbgVariables.Columns("A_SUP").caption
            Cancel = True
            If Me.Visible Then sdbgVariables.SetFocus
            Exit Sub
        End If
    End If
    If sdbgVariables.Columns("A_INF").Value <> "" And sdbgVariables.Columns("A_SUP").Value <> "" Then
        If StrToDblOrNull(sdbgVariables.Columns("A_INF").Value) > StrToDblOrNull(sdbgVariables.Columns("A_SUP").Value) Then
            oMensajes.NoValido IntervaloAviso
            
            m_bRespetarChange = True
            
            Cancel = True
            Set oCalidad = Nothing
            Exit Sub
        End If
    End If
    
    '-----------RESTRICCION-----------AVISO
    Dim Ainf As Double
    Dim Asup As Double
    Dim Rinf As Double
    Dim Rsup As Double
    Dim bHayAviso As Boolean
    Dim bHayRestricc As Boolean
    Dim bHayAviso2 As Boolean
    Dim bHayRestricc2 As Boolean

    Ainf = -1000000000
    Asup = 1000000000
    Rinf = -1000000000
    Rsup = 1000000000
    
    bHayAviso = False
    bHayRestricc = False
    bHayAviso2 = True
    bHayRestricc2 = True

    If sdbgVariables.Columns("A_INF").Value <> "" Then
        Ainf = CDbl(sdbgVariables.Columns("A_INF").Value)
        bHayAviso = True
    Else
        bHayAviso2 = False
    End If
    If sdbgVariables.Columns("A_SUP").Value <> "" Then
        Asup = CDbl(sdbgVariables.Columns("A_SUP").Value)
        bHayAviso = True
    Else
        bHayAviso2 = False
    End If
    If sdbgVariables.Columns("R_INF").Value <> "" Then
        Rinf = CDbl(sdbgVariables.Columns("R_INF").Value)
        bHayRestricc = True
    Else
        bHayRestricc2 = False
    End If
    If sdbgVariables.Columns("R_SUP").Value <> "" Then
        Rsup = CDbl(sdbgVariables.Columns("R_SUP").Value)
        bHayRestricc = True
    Else
        bHayRestricc2 = False
    End If
    
    If bHayAviso And bHayRestricc Then
        If bHayAviso2 And bHayRestricc2 Then
            If (Rinf >= Ainf And Rinf <= Asup) Or (Ainf >= Rinf And Ainf <= Rsup) Then
                oMensajes.NoValido IntervaloCoincide
            
                m_bRespetarChange = True
            
                Cancel = True
                Set oCalidad = Nothing
                Exit Sub
            End If

            If (Rsup >= Ainf And Rsup <= Asup) Or (Asup >= Rinf And Asup <= Rsup) Then
                oMensajes.NoValido IntervaloCoincide
            
                m_bRespetarChange = True
            
                Cancel = True
                Set oCalidad = Nothing
                Exit Sub
            End If
        ElseIf bHayAviso2 And Not bHayRestricc2 Then
            If Rinf = -1000000000 Then
                If Rsup >= Ainf Then
                    oMensajes.NoValido IntervaloCoincide
                
                    m_bRespetarChange = True
                
                    Cancel = True
                    Set oCalidad = Nothing
                    Exit Sub
                End If
            Else
                If Rinf <= Asup Then
                    oMensajes.NoValido IntervaloCoincide
                
                    m_bRespetarChange = True
                
                    Cancel = True
                    Set oCalidad = Nothing
                    Exit Sub
                End If
            End If
            
        ElseIf Not bHayAviso2 And bHayRestricc2 Then
            If Ainf = -1000000000 Then
                If Asup >= Rinf Then
                    oMensajes.NoValido IntervaloCoincide
                
                    m_bRespetarChange = True
                
                    Cancel = True
                    Set oCalidad = Nothing
                    Exit Sub
                End If
            Else
                If Ainf <= Rsup Then
                    oMensajes.NoValido IntervaloCoincide
                
                    m_bRespetarChange = True
                
                    Cancel = True
                    Set oCalidad = Nothing
                    Exit Sub
                End If
            End If
        ElseIf Not bHayAviso2 And Not bHayRestricc2 Then
            If (Ainf = -1000000000 And Rinf = -1000000000) Or (Asup = 1000000000 And Rsup = 1000000000) Then
                oMensajes.NoValido IntervaloCoincide
            
                m_bRespetarChange = True
            
                Cancel = True
                Set oCalidad = Nothing
                Exit Sub
                
            ElseIf Ainf = -1000000000 Then
                If Asup >= Rinf Then
                    oMensajes.NoValido IntervaloCoincide
                
                    m_bRespetarChange = True
                
                    Cancel = True
                    Set oCalidad = Nothing
                    Exit Sub
                End If
            Else
                If Ainf <= Rsup Then
                    oMensajes.NoValido IntervaloCoincide
                
                    m_bRespetarChange = True
                
                    Cancel = True
                    Set oCalidad = Nothing
                    Exit Sub
                End If
            End If
        End If
    End If
    
    '-----------RESTRICCION
    If sdbgVariables.Columns("R_INF").Value <> "" Or sdbgVariables.Columns("R_SUP").Value <> "" Then
        sdbgVariables.Columns("EXISTE").Value = "1"
        
        If GridCheckToBoolean(sdbgVariables.Columns("RESTRICCION").Value) = False Then
            sdbgVariables.Columns("RESTRICCION").Value = "1"
        End If
    Else
        If GridCheckToBoolean(sdbgVariables.Columns("RESTRICCION").Value) = True Then
            sdbgVariables.Columns("RESTRICCION").Value = "0"
            
            sdbgVariables.Columns("EXISTE").Value = "1"
        End If
    End If
           
    If GridCheckToBoolean(sdbgVariables.Columns("RESTRICCION").Value) = False Then
        sdbgVariables.Columns("R_INF").Value = ""
        sdbgVariables.Columns("R_SUP").Value = ""
    End If
    
    oCalidad.RestriccionInf = StrToDblOrNull(sdbgVariables.Columns("R_INF").Value)
    oCalidad.RestriccionSup = StrToDblOrNull(sdbgVariables.Columns("R_SUP").Value)

    oCalidad.RestriccionSiNo = GridCheckToBoolean(sdbgVariables.Columns("RESTRICCION").Value)

    '-----------AVISO
    If sdbgVariables.Columns("A_INF").Value <> "" Or sdbgVariables.Columns("A_SUP").Value <> "" Then
        sdbgVariables.Columns("EXISTE").Value = "1"
        
        If GridCheckToBoolean(sdbgVariables.Columns("AVISO").Value) = False Then
            sdbgVariables.Columns("AVISO").Value = "1"
        End If
    Else
        If GridCheckToBoolean(sdbgVariables.Columns("AVISO").Value) = True Then
            sdbgVariables.Columns("AVISO").Value = "0"
            
            sdbgVariables.Columns("EXISTE").Value = "1"
        End If
    End If
    
    If GridCheckToBoolean(sdbgVariables.Columns("AVISO").Value) = False Then
        sdbgVariables.Columns("A_INF").Value = ""
        sdbgVariables.Columns("A_SUP").Value = ""
    End If
    
    oCalidad.AvisoInf = StrToDblOrNull(sdbgVariables.Columns("A_INF").Value)
    oCalidad.AvisoSup = StrToDblOrNull(sdbgVariables.Columns("A_SUP").Value)
    
    oCalidad.AvisoSiNo = GridCheckToBoolean(sdbgVariables.Columns("AVISO").Value)
    Dim lIdPyme As Long
    lIdPyme = 0
    
    If gParametrosGenerales.gbPymes Then
        lIdPyme = Me.sdbcPYME.Columns("ID").Value
    End If
    
    
    '
    oCalidad.ModificarRestriccionADJ lIdPyme
    
    '----
    oCalidad.modificado = GridCheckToBoolean(sdbgVariables.Columns("EXISTE").Value)
    
    Set oCalidad = Nothing
           
End Sub

Private Sub sdbgVariables_Change()
    If m_bRespetarChange = True Then Exit Sub
    
    If sdbgVariables.Columns(sdbgVariables.Col).Name = "RESTRICCION" Then
        If GridCheckToBoolean(sdbgVariables.Columns("RESTRICCION").Value) = False Then
            m_bRespetarChange = True
        
            sdbgVariables.Columns("R_INF").Value = ""
            sdbgVariables.Columns("R_SUP").Value = ""
        
            m_bRespetarChange = False
        End If
    ElseIf sdbgVariables.Columns(sdbgVariables.Col).Name = "AVISO" Then
        If GridCheckToBoolean(sdbgVariables.Columns("AVISO").Value) = False Then
            m_bRespetarChange = True
            
            sdbgVariables.Columns("A_INF").Value = ""
            sdbgVariables.Columns("A_SUP").Value = ""
            
            m_bRespetarChange = False
        End If
    End If
End Sub

Private Sub sdbgVariables_RowLoaded(ByVal Bookmark As Variant)
    Select Case sdbgVariables.Columns("NIVEL").Value
        Case 0, 1
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel1"
        Case 2
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel2"
        Case 3
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel3"
        Case 4
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel4"
        Case 5
            sdbgVariables.Columns("DEN").CellStyleSet "Nivel5"
    End Select
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAL_RESTRICCION_ADJ, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.caption = ador(0).Value
        
        ador.MoveNext
        Me.sdbgVariables.Groups(0).caption = ador(0).Value
        Me.sdbgVariables.Columns("DEN").caption = ador(0).Value
        ador.MoveNext
        Me.sdbgVariables.Groups(1).caption = ador(0).Value
        Me.sdbgVariables.Columns("RESTRICCION").caption = ador(0).Value
        ador.MoveNext
        Me.sdbgVariables.Groups(2).caption = ador(0).Value
        ador.MoveNext
        Me.sdbgVariables.Columns("R_INF").caption = ador(0).Value
        Me.sdbgVariables.Columns("A_INF").caption = ador(0).Value
        ador.MoveNext
        Me.sdbgVariables.Columns("R_SUP").caption = ador(0).Value
        Me.sdbgVariables.Columns("A_SUP").caption = ador(0).Value
        ador.MoveNext
        Me.sdbgVariables.Columns("AVISO").caption = ador(0).Value
        ador.MoveNext
        m_sTotal = ador(0).Value
        ador.MoveNext
        Me.cmdRefresh.caption = ador(0).Value
        ador.MoveNext
        m_sIdiEdicion = ador(0).Value
        Me.cmdModoEdicion.caption = m_sIdiEdicion
        ador.MoveNext
        m_sIdiConsulta = ador(0).Value
        ador.MoveNext
        lblPYME.caption = ador(0).Value & ":"
                
        ador.Close
    End If
End Sub

Private Sub CargarVariablesCalidad(Optional ByVal lIdPyme As Long = 0)
'MPG(17/03/2009)
'****************************************************************************
'Descripcion:= Procedure que carga las variables de calidad en la grid.
'Parametros:=  lIDPyme--> Id de la PYME, si esta trabajando en modo PYME
'Llamada:=  propio formulario
'Tiempo:=0,5
'****************************************************************************
   
    Dim oVarCal0 As CVariableCalidad
    Dim oVarCal1 As CVariableCalidad
    Dim oVarCal2 As CVariableCalidad
    Dim oVarCal3 As CVariableCalidad
    Dim oVarCal4 As CVariableCalidad
    Dim oVarCal5 As CVariableCalidad
    
    sdbgVariables.RemoveAll
    
    Set m_oCalidad = oFSGSRaiz.Generar_CVariablesCalidad
    m_oCalidad.CargarVariablesRestriccADJ , lIdPyme
    
    For Each oVarCal0 In m_oCalidad
        For Each oVarCal1 In oVarCal0.VariblesCal
            sdbgVariables.AddItem oVarCal1.Id & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal1.RestriccionSiNo) & Chr(m_lSeparador) & oVarCal1.RestriccionInf & Chr(m_lSeparador) & oVarCal1.RestriccionSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal1.AvisoSiNo) & Chr(m_lSeparador) & oVarCal1.AvisoInf & Chr(m_lSeparador) & oVarCal1.AvisoSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal1.modificado) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
            If Not oVarCal1.VariblesCal Is Nothing Then
                For Each oVarCal2 In oVarCal1.VariblesCal
                    sdbgVariables.AddItem oVarCal2.Id & Chr(m_lSeparador) & "2" & Chr(m_lSeparador) & "    " & oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal2.RestriccionSiNo) & Chr(m_lSeparador) & oVarCal2.RestriccionInf & Chr(m_lSeparador) & oVarCal2.RestriccionSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal2.AvisoSiNo) & Chr(m_lSeparador) & oVarCal2.AvisoInf & Chr(m_lSeparador) & oVarCal2.AvisoSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal2.modificado) & Chr(m_lSeparador) & oVarCal2.IdVarCal1 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                    If Not oVarCal2.VariblesCal Is Nothing Then
                        For Each oVarCal3 In oVarCal2.VariblesCal
                            sdbgVariables.AddItem oVarCal3.Id & Chr(m_lSeparador) & "3" & Chr(m_lSeparador) & "          " & oVarCal3.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal3.RestriccionSiNo) & Chr(m_lSeparador) & oVarCal3.RestriccionInf & Chr(m_lSeparador) & oVarCal3.RestriccionSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal3.AvisoSiNo) & Chr(m_lSeparador) & oVarCal3.AvisoInf & Chr(m_lSeparador) & oVarCal3.AvisoSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal3.modificado) & Chr(m_lSeparador) & oVarCal3.IdVarCal1 & Chr(m_lSeparador) & oVarCal3.IdVarCal2 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                            If Not oVarCal3.VariblesCal Is Nothing Then
                                For Each oVarCal4 In oVarCal3.VariblesCal
                                    sdbgVariables.AddItem oVarCal4.Id & Chr(m_lSeparador) & "4" & Chr(m_lSeparador) & "               " & oVarCal4.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal4.RestriccionSiNo) & Chr(m_lSeparador) & oVarCal4.RestriccionInf & Chr(m_lSeparador) & oVarCal4.RestriccionSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal4.AvisoSiNo) & Chr(m_lSeparador) & oVarCal4.AvisoInf & Chr(m_lSeparador) & oVarCal4.AvisoSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal4.modificado) & Chr(m_lSeparador) & oVarCal4.IdVarCal1 & Chr(m_lSeparador) & oVarCal4.IdVarCal2 & Chr(m_lSeparador) & oVarCal4.IdVarCal3 & Chr(m_lSeparador) & ""
                                    If Not oVarCal4.VariblesCal Is Nothing Then
                                        For Each oVarCal5 In oVarCal4.VariblesCal
                                            sdbgVariables.AddItem oVarCal5.Id & Chr(m_lSeparador) & "5" & Chr(m_lSeparador) & "                   " & oVarCal5.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal5.RestriccionSiNo) & Chr(m_lSeparador) & oVarCal5.RestriccionInf & Chr(m_lSeparador) & oVarCal5.RestriccionSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal5.AvisoSiNo) & Chr(m_lSeparador) & oVarCal5.AvisoInf & Chr(m_lSeparador) & oVarCal5.AvisoSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal5.modificado) & Chr(m_lSeparador) & oVarCal5.IdVarCal1 & Chr(m_lSeparador) & oVarCal5.IdVarCal2 & Chr(m_lSeparador) & oVarCal5.IdVarCal3 & Chr(m_lSeparador) & oVarCal5.IdVarCal4
                                        Next
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next
        sdbgVariables.AddItem oVarCal0.Id & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & m_sTotal & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal0.RestriccionSiNo) & Chr(m_lSeparador) & oVarCal0.RestriccionInf & Chr(m_lSeparador) & oVarCal0.RestriccionSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal0.AvisoSiNo) & Chr(m_lSeparador) & oVarCal0.AvisoInf & Chr(m_lSeparador) & oVarCal0.AvisoSup & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal0.modificado) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        m_sCodTotalProveedor = "0" & oVarCal0.Id
    Next
    
    sdbgVariables.Columns("DEN").Locked = True
End Sub


Private Sub sdbcPYME_CloseUp()
    If sdbcPYME.Value = "" Then Exit Sub
    CargarVariablesCalidad sdbcPYME.Columns("ID").Value
End Sub

Private Sub sdbcPYME_DropDown()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Carga la combo con todas las pymes que hay en la aplicacion
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,1seg
'************************************************************

Dim oPymes As cPymes
Dim oPyme As cPyme

    Screen.MousePointer = vbHourglass
    
    sdbcPYME.RemoveAll
    
    Set oPymes = oFSGSRaiz.Generar_CPymes
    oPymes.CargarTodasLasPymes , True
    
    For Each oPyme In oPymes
        sdbcPYME.AddItem oPyme.Id & Chr(m_lSeparador) & oPyme.Cod & Chr(m_lSeparador) & oPyme.Den
    Next
    
    Set oPymes = Nothing
    Set oPyme = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPYME_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcPYME.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPYME.Rows - 1
            bm = sdbcPYME.GetBookmark(i)
            If UCase(Text) = UCase(sdbcPYME.Columns("DEN").CellText(bm)) Then
                sdbcPYME.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcPYME_InitColumnProps()
    
    sdbcPYME.DataFieldList = "Column 0"
    sdbcPYME.DataFieldToDisplay = "Column 2"
    
End Sub



