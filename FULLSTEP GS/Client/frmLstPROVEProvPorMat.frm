VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPROVEProvPorMat 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de Proveedores por material (Opciones)"
   ClientHeight    =   2295
   ClientLeft      =   1950
   ClientTop       =   4020
   ClientWidth     =   5505
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPROVEProvPorMat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2295
   ScaleWidth      =   5505
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   5505
      TabIndex        =   6
      Top             =   1920
      Width           =   5505
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   4140
         TabIndex        =   4
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1890
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   5475
      _ExtentX        =   9657
      _ExtentY        =   3334
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selección"
      TabPicture(0)   =   "frmLstPROVEProvPorMat.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstPROVEProvPorMat.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1320
         Left            =   -74850
         TabIndex        =   10
         Top             =   375
         Width           =   5160
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominación"
            Height          =   195
            Left            =   435
            TabIndex        =   3
            Top             =   795
            Width           =   2595
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "Código"
            Height          =   195
            Left            =   435
            TabIndex        =   2
            Top             =   390
            Value           =   -1  'True
            Width           =   2595
         End
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1320
         Left            =   150
         TabIndex        =   7
         Top             =   375
         Width           =   5160
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   4290
            Picture         =   "frmLstPROVEProvPorMat.frx":0CEA
            Style           =   1  'Graphical
            TabIndex        =   11
            Top             =   765
            Width           =   345
         End
         Begin VB.OptionButton opTodos 
            Caption         =   "Listado Completo"
            Height          =   195
            Left            =   255
            TabIndex        =   0
            Top             =   375
            Value           =   -1  'True
            Width           =   2430
         End
         Begin VB.CommandButton cmdSelMat 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   4700
            Picture         =   "frmLstPROVEProvPorMat.frx":0D8F
            Style           =   1  'Graphical
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   765
            Width           =   345
         End
         Begin VB.OptionButton opSelMat 
            Caption         =   "Material:"
            Height          =   195
            Left            =   255
            TabIndex        =   1
            Top             =   810
            Width           =   1245
         End
         Begin VB.TextBox txtEstMat 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1530
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   780
            Width           =   2700
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   500
         Left            =   4605
         Top             =   -300
      End
   End
End
Attribute VB_Name = "frmLstPROVEProvPorMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de restricciones
Public bRMat As Boolean
Public bREqp As Boolean
Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String
'variables para los combos
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean

'Multilenguaje

Private sIdiDelMat As String
Private sIdiCompleto As String
Private sIdiTitulo As String
Private sIdiGenerando As String
Private sIdiSeleccionando As String
Private sIdiVisualizando As String
Private srIdiTitulo As String
Private srIdiSeleccion As String
Private srIdiProveedores As String
Private srIdiPag As String
Private srIdiDe As String


Private Sub cmdBorrar_Click()
    txtEstMat.Text = ""
End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = "frmLstPROVEProvPorMat"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Show 1

End Sub
Public Sub PonerMatSeleccionado()
        
    If Not frmSELMAT.oGMN1Seleccionado Is Nothing Then
         sGMN1Cod = frmSELMAT.oGMN1Seleccionado.Cod
         txtEstMat = sGMN1Cod
    Else
         sGMN1Cod = ""
    End If
        
    If Not frmSELMAT.oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = frmSELMAT.oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
        
    If Not frmSELMAT.oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = frmSELMAT.oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
        
    If Not frmSELMAT.oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = frmSELMAT.oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If

End Sub

Private Sub cmdObtener_Click()

    ObtenerListado
End Sub

Private Sub ObtenerListado()
    Dim literal As String
    Dim SelectionText As String
    Dim SubProveOrden As String
    Dim RecordSelFormula As String
    Dim SubProveSelFormula As String
    Dim oFos As FileSystemObject
    Dim NombresGMN(1 To 4) As String
    Dim Parametros(1 To 3) As String
    Dim oReport As Object
    Dim Srpt As CRAXDRT.Report
    Dim oCRProvePorMat As CRProvePorMat
    Dim pv As Preview
    
    ''' * Objetivo: Obtener un Listado de la estructura de la organización

    Set oCRProvePorMat = GenerarCRProvePorMat

    If txtEstMat = "" Then
        opTodos.Value = True
        opSelMat.Value = False
    Else
        opTodos.Value = False
        opSelMat.Value = True
    End If
    
    
    Dim i As Integer
    Dim j As Integer
    Dim FormulaText As String
    Dim RepPath As String
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPROVEPorMat.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
        
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    If Not opTodos Then
        SelectionText = sIdiDelMat & "  " & txtEstMat
    Else
        SelectionText = sIdiCompleto
    End If
    
    
    If bRMat Then
        gCodEqpUsuario = basOptimizacion.gCodEqpUsuario 'parametros(1)
        gCodCompradorUsuario = basOptimizacion.gCodCompradorUsuario 'parametros(2)
    Else
        gCodEqpUsuario = ""
        gCodCompradorUsuario = ""
    End If
            
    NombresGMN(1) = gParametrosGenerales.gsDEN_GMN1
    NombresGMN(2) = gParametrosGenerales.gsDEN_GMN2
    NombresGMN(3) = gParametrosGenerales.gsDEN_GMN3
    NombresGMN(4) = gParametrosGenerales.gsDEN_GMN4
        
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    For i = 1 To UBound(NombresGMN)
        FormulaText = oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN" & i & "NOM")).Text
        oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN" & i & "NOM")).Text = FormulaText & """" & NombresGMN(i) & """"
    Next i
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTITULO")).Text = """" & srIdiTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSELECCION")).Text = """" & srIdiSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    
    For i = 1 To 4
        Set Srpt = Nothing
        Set Srpt = oReport.OpenSubreport("rptSUBProveMat" & i & ".rpt")
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtPROVEEDORES")).Text = """" & srIdiProveedores & """"
    Next i

    Screen.MousePointer = vbHourglass
    
    oCRProvePorMat.ListadoProvePorMat oReport, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, bREqp, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, opOrdCod
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    
    Set pv = New Preview
    pv.Hide
    pv.caption = sIdiTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    
    
End Sub

Private Sub Form_Load()

    Me.Width = 5625
    Me.Height = 2700
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
    
    Timer1.Enabled = False
    txtEstMat.Enabled = False
    cmdSelMat.Enabled = False
    cmdBorrar.Enabled = False

    ' Configurar la seguridad
    ConfigurarSeguridad
 
 
End Sub

Private Sub ConfigurarSeguridad()

bRMat = False
bREqp = False
If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then

    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorGRUPRestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ProvePorGRUPRestEquipo)) Is Nothing) Then
        bREqp = True
    End If
End If
End Sub



Private Sub Form_Unload(Cancel As Integer)
    opTodos = True
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
End Sub

Private Sub opSelMat_Click()
    If opSelMat.Value = True Then
        cmdSelMat.Enabled = True
        cmdBorrar.Enabled = True
    Else
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    End If

End Sub


Private Sub opTodos_Click()
    If opTodos Then
        txtEstMat.Text = ""
        txtEstMat.Refresh
        sGMN1Cod = ""
        sGMN2Cod = ""
        sGMN3Cod = ""
        sGMN4Cod = ""
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    Else
        cmdSelMat.Enabled = True
        cmdBorrar.Enabled = True
    End If
End Sub

Private Sub Timer1_Timer()
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
End Sub

Private Sub txtestmat_Change()
    If txtEstMat.Text <> "" Then
        opSelMat.Value = True
    End If
End Sub



Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPROVE_PROVEPORMAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        opTodos.caption = Ador(0).Value
        sIdiCompleto = Ador(0).Value
        Ador.MoveNext
        opSelMat.caption = Ador(0).Value '5
        sIdiDelMat = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        'Ador.MoveNext
        'sIdiProve = Ador(0).Value
        'Ador.MoveNext
        'sIdiDelMat = Ador(0).Value '10
        'Ador.MoveNext
        'sIdiPorMat = Ador(0).Value
        Ador.MoveNext
        sIdiTitulo = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        'Idioma del report
        Ador.MoveNext
        srIdiTitulo = Ador(0).Value
        Ador.MoveNext
        srIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        srIdiProveedores = Ador(0).Value
        Ador.MoveNext
        srIdiPag = Ador(0).Value
        Ador.MoveNext
        srIdiDe = Ador(0).Value
        
        Ador.Close
    
    End If


   Set Ador = Nothing



End Sub





