VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFlujosRolesListaPersonas 
   BackColor       =   &H00808000&
   Caption         =   "DLista de personas asociadas al ROL:"
   ClientHeight    =   4230
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   8205
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujosRolesListaPersonas.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4230
   ScaleWidth      =   8205
   Begin MSComctlLib.ListView lstPersonas 
      Height          =   3570
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   7575
      _ExtentX        =   13361
      _ExtentY        =   6297
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   13229
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   2640
      ScaleHeight     =   420
      ScaleWidth      =   2625
      TabIndex        =   5
      Top             =   3720
      Width           =   2625
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   4
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   3
         Top             =   60
         Width           =   1050
      End
   End
   Begin VB.CommandButton cmdEliminarPer 
      Enabled         =   0   'False
      Height          =   312
      Left            =   7680
      Picture         =   "frmFlujosRolesListaPersonas.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   480
      Width           =   432
   End
   Begin VB.CommandButton cmdAnyadirPer 
      Height          =   312
      Left            =   7680
      Picture         =   "frmFlujosRolesListaPersonas.frx":0D44
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   60
      Width           =   432
   End
End
Attribute VB_Name = "frmFlujosRolesListaPersonas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Variables Publicas
Public oParticipantes As CPMParticipantes
Public sRol As String
Public m_bModifFlujo As Boolean
Public m_bRUON As Boolean
Public m_bRDep As Boolean

'Variables Privadas
Private msPersona As String



Private Sub cmdAceptar_Click()
    Dim oParticipante As CPMParticipante
    Dim Item As MSComctlLib.listItem
    
    Dim i As Integer
    i = 1
    Set oParticipantes = New CPMParticipantes
    For Each Item In lstPersonas.ListItems
        Set oParticipante = New CPMParticipante
        oParticipante.Per = Mid(Item.key, 2)
        oParticipante.Id = i
        oParticipantes.AddParticipante oParticipante
        i = i + 1
    Next
    Me.Hide 'Lo descargo en el origen
End Sub

Private Sub cmdAnyadirPer_Click()

    frmSOLSelPersona.g_sOrigen = "frmFlujosRolesListaPersonas"
    frmSOLSelPersona.bRDep = m_bRDep
    frmSOLSelPersona.bRUO = m_bRUON
    frmSOLSelPersona.Show vbModal
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdEliminarPer_Click()
    If oMensajes.PreguntaEliminar(lstPersonas.selectedItem.Text) = vbYes Then
        If lstPersonas.ListItems.Count = 1 Then
            cmdEliminarPer.Enabled = False
        End If
        lstPersonas.ListItems.Remove lstPersonas.selectedItem.Index
    End If
End Sub

Private Sub Form_Load()
    
    Me.Top = frmFlujosRoles.Top + (frmFlujosRoles.Height / 2) - (Me.Height / 2)
    If Me.Top < 0 Then
        Me.Top = 0
    End If
    Me.Left = frmFlujosRoles.Left + (frmFlujosRoles.Width / 2) - (Me.Width / 2)
    If Me.Left < 0 Then
        Me.Left = 0
    End If
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    MostrarPersonas
    
End Sub

Private Sub ConfigurarSeguridad()
        
    If Not m_bModifFlujo Then
        cmdAnyadirPer.Enabled = False
        cmdEliminarPer.Enabled = False
        
        picEdit.Visible = False
        Me.Height = Me.Height - picEdit.Height
    End If
    
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSROLESLISTAPERSONAS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value & " " & sRol '1
        Ador.MoveNext
        cmdAnyadirPer.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarPer.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '5
        Ador.MoveNext
        msPersona = Ador(0).Value
                                        
        Ador.Close
    End If
End Sub

Private Sub MostrarPersonas()
    Dim oParticipante As CPMParticipante
    Dim oPersona As CPersona
    
    lstPersonas.ListItems.clear
    If Not oParticipantes Is Nothing Then
        For Each oParticipante In oParticipantes
            Set oPersona = oFSGSRaiz.Generar_CPersona
            oPersona.Cod = oParticipante.Per
            oPersona.CargarTodosLosDatos
            
            lstPersonas.ListItems.Add , "P" & oPersona.Cod, oPersona.nombre & " " & oPersona.Apellidos & " (" & oPersona.mail & ")"
        Next
        Set oParticipante = Nothing
        Set oPersona = Nothing
    End If
End Sub

Private Sub Form_Resize()
    lstPersonas.Width = Me.Width - 750
    If picEdit.Visible Then
        lstPersonas.Height = Me.Height - 1065
    Else
        lstPersonas.Height = Me.Height - 645
    End If
        
    lstPersonas.ColumnHeaders(1).Width = lstPersonas.Width - 75
    
    picEdit.Top = Me.Height - 915
    picEdit.Left = Me.Width / 2 - (picEdit.Width / 2)
    
    cmdAnyadirPer.Left = Me.Width - 645
    cmdEliminarPer.Left = Me.Width - 645
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oParticipantes = Nothing
    sRol = ""
End Sub

Private Sub lstPersonas_Click()
    If Not lstPersonas.selectedItem Is Nothing Then
        cmdEliminarPer.Enabled = True
    Else
        cmdEliminarPer.Enabled = False
    End If
End Sub

