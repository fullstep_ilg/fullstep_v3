VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSolicitudComentarios 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DComentarios"
   ClientHeight    =   3600
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13170
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudComentarios.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3600
   ScaleWidth      =   13170
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdWorkflowGrafico 
      Height          =   285
      Left            =   4680
      Picture         =   "frmSolicitudComentarios.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "DFlujo de trabajo gr�fico"
      Top             =   60
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgComentarios2 
      Height          =   3105
      Left            =   120
      TabIndex        =   0
      Top             =   375
      Visible         =   0   'False
      Width           =   12945
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   11
      stylesets.count =   3
      stylesets(0).Name=   "conComent"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSolicitudComentarios.frx":0DC3
      stylesets(1).Name=   "Verde"
      stylesets(1).BackColor=   13172680
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSolicitudComentarios.frx":0E56
      stylesets(2).Name=   "Naranja"
      stylesets(2).BackColor=   8438015
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSolicitudComentarios.frx":0E72
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   11
      Columns(0).Width=   4948
      Columns(0).Caption=   "DEstado"
      Columns(0).Name =   "ESTADO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).ButtonsAlways=   -1  'True
      Columns(1).Width=   1455
      Columns(1).Caption=   "DEtapa"
      Columns(1).Name =   "ETAPA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Caption=   "DParticipante"
      Columns(2).Name =   "PARTICIPANTE"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3069
      Columns(3).Caption=   "DUsuario"
      Columns(3).Name =   "USUARIO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   1
      Columns(4).Width=   2884
      Columns(4).Caption=   "DAccion"
      Columns(4).Name =   "ACCION"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1852
      Columns(5).Caption=   "DFecha"
      Columns(5).Name =   "FECHA"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   2302
      Columns(6).Caption=   "DDestinatario"
      Columns(6).Name =   "DESTINATARIO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   1826
      Columns(7).Caption=   "DComentario"
      Columns(7).Name =   "COMENTARIO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   4
      Columns(7).ButtonsAlways=   -1  'True
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "USU"
      Columns(8).Name =   "USU"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "HAY_COMENT"
      Columns(9).Name =   "HAY_COMENT"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ID"
      Columns(10).Name=   "ID"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      _ExtentX        =   22834
      _ExtentY        =   5477
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblWorkflowGrafico 
      BackColor       =   &H00808000&
      Caption         =   "DPulse el bot�n para ver el flujo de trabajo en modo gr�fico"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "frmSolicitudComentarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oSolicitudSeleccionada As CInstancia

'Variables de idiomas:
Private m_sPendiente As String
Private m_sAprobada As String
Private m_sRechazada As String
Private m_sGenRechazada As String
Private m_sAnulada As String
Private m_sCerrada As String
Private m_sEnCurso As String
Private m_sIdiAccion(8) As String
Private m_sIdiProve As String
Private m_sPeticionario As String
Private m_sCompras As String

Private m_sEtapasParalelo As String

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass
    
    CargarRecursos
    
    PonerFieldSeparator Me
        
    sdbgComentarios2.Visible = True

    CargarComentarios
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOLIC_COMENT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value       '1 Comentarios
        Ador.MoveNext
                
        sdbgComentarios2.Columns("FECHA").caption = Ador(0).Value
        Ador.MoveNext
        sdbgComentarios2.Columns("USUARIO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgComentarios2.Columns("ESTADO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgComentarios2.Columns("COMENTARIO").caption = Ador(0).Value
        Ador.MoveNext
        
        m_sAnulada = Ador(0).Value  '6  Anulada
        Ador.MoveNext
        m_sAprobada = Ador(0).Value '7  Aprobada
        Ador.MoveNext
        m_sCerrada = Ador(0).Value  '8  Cerrada
        Ador.MoveNext
        m_sPendiente = Ador(0).Value '9  Pendiente
        Ador.MoveNext
        m_sRechazada = Ador(0).Value  '10  Rechazada
        Ador.MoveNext
        m_sEnCurso = Ador(0).Value '11 En curso de aprobaci�n
        
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        
        For i = 1 To 7
            Ador.MoveNext
            m_sIdiAccion(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        sdbgComentarios2.Columns("DESTINATARIO").caption = Ador(0).Value
        Ador.MoveNext
        m_sPeticionario = Ador(0).Value '23 Peticionario
        Ador.MoveNext
        m_sCompras = Ador(0).Value '24 COMPRAS
        Ador.MoveNext
        m_sIdiProve = Ador(0).Value '25 Proveedor
        Ador.MoveNext
        m_sIdiAccion(8) = Ador(0).Value '26 Reemisi�n

        Ador.MoveNext
        sdbgComentarios2.Columns("PARTICIPANTE").caption = Ador(0).Value '27 Participante
        Ador.MoveNext
        sdbgComentarios2.Columns("ETAPA").caption = Ador(0).Value '28 Etapa
        Ador.MoveNext
        sdbgComentarios2.Columns("ACCION").caption = Ador(0).Value '29 Acci�n Realizada
        Ador.MoveNext
        m_sEtapasParalelo = Ador(0).Value '30 Etapas Paralelo
        Ador.MoveNext
        lblWorkflowGrafico.caption = Ador(0).Value '31
        Ador.MoveNext
        cmdWorkflowGrafico.ToolTipText = Ador(0).Value '32
        Ador.MoveNext
        m_sGenRechazada = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub CargarComentarios()
    'Carga la grid con el hist�rico de los comentarios
    Dim Ador As Ador.Recordset
    Dim sestado As String
    Dim sUsu As String
    Dim sCadena As String
    Dim sAccion As String
    Dim sDest As String

    Set Ador = g_oSolicitudSeleccionada.DevolverHistorico(basPublic.gParametrosInstalacion.gIdioma)
        
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            Select Case Ador.Fields("ESTADO").Value
                Case EstadoSolicitud.Anulada
                    sestado = m_sAnulada
                Case EstadoSolicitud.Aprobada
                    sestado = m_sAprobada
                Case EstadoSolicitud.Cerrada
                    sestado = m_sCerrada
                Case EstadoSolicitud.Pendiente
                    sestado = m_sPendiente
                Case EstadoSolicitud.Rechazada
                    sestado = m_sRechazada
                Case EstadoSolicitud.GenRechazada
                    sestado = m_sGenRechazada
                Case Else
                    sestado = m_sEnCurso
            End Select

            sAccion = NullToStr(Ador.Fields("ACCION").Value)
            If sAccion = "" Then
                Select Case Ador.Fields("ID_OTRA_ACCION").Value
                    Case TipoAccionSolicitud.Alta
                        sAccion = m_sIdiAccion(1)
                    Case TipoAccionSolicitud.Anulacion
                        sAccion = m_sIdiAccion(7)
                    Case TipoAccionSolicitud.Aprobacion
                        sAccion = m_sIdiAccion(5)
                    Case TipoAccionSolicitud.Devolucion
                        sAccion = m_sIdiAccion(4)
                    Case TipoAccionSolicitud.Modificacion
                        sAccion = m_sIdiAccion(2)
                    Case TipoAccionSolicitud.Rechazo
                        sAccion = m_sIdiAccion(6)
                    Case TipoAccionSolicitud.Traslado
                        sAccion = m_sIdiAccion(3)
                    Case TipoAccionSolicitud.Reemision
                        sAccion = m_sIdiAccion(8)
                End Select
            End If

            If IsNull(Ador.Fields("PER").Value) Then
                sUsu = ""
            Else
                sUsu = Ador.Fields("PER").Value & "-" & NullToStr(Ador.Fields("USUARIO").Value)
            End If

            Dim bParalelo As Boolean
            bParalelo = False
            If Not IsNull(Ador.Fields("ETAPAS_PARALELO").Value) Then
                If CInt(Ador.Fields("ETAPAS_PARALELO").Value) > 1 Then
                    bParalelo = True
                End If
            End If
            If bParalelo Then
                sDest = m_sEtapasParalelo
            Else
                If IsNull(Ador.Fields("DEN_BLOQUE2").Value) Then
                    sDest = NullToStr(Ador.Fields("DESTINATARIO").Value)
                Else
                    sDest = NullToStr(Ador.Fields("DEN_BLOQUE2").Value)
                End If
            End If

            sCadena = sestado & Chr(m_lSeparador) & NullToStr(Ador.Fields("DEN_BLOQUE1").Value) & Chr(m_lSeparador) & NullToStr(Ador.Fields("ROL").Value) & Chr(m_lSeparador) & sUsu & Chr(m_lSeparador) & sAccion & Chr(m_lSeparador) & Format(Ador.Fields("FECHA").Value, "Short date") & Chr(m_lSeparador) & sDest & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & Ador.Fields("PER").Value
            If Not IsNull(Ador.Fields("COMENT").Value) Then
                If Trim(Ador.Fields("COMENT").Value) <> "" Then
                    sCadena = sCadena & Chr(m_lSeparador) & "1"
                Else
                    sCadena = sCadena & Chr(m_lSeparador) & "0"
                End If
            Else
                sCadena = sCadena & Chr(m_lSeparador) & "0"
            End If
            sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("ID").Value
            sdbgComentarios2.AddItem sCadena
            
            Ador.MoveNext
        Wend
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    ''' *  Descargar el formulario
    Set g_oSolicitudSeleccionada = Nothing
End Sub

Private Sub cmdWorkflowGrafico_Click()
    Dim oWorkflows As CWorkflows
    frmFlujos.m_bModifFlujo = False
    If IsNull(g_oSolicitudSeleccionada.DescrBreve) Then
        frmFlujos.m_sSolicitud = ""
    Else
        frmFlujos.m_sSolicitud = g_oSolicitudSeleccionada.DescrBreve
    End If
    
    frmFlujos.g_bDesdeInstancia = True
    frmFlujos.m_lIdFlujo = g_oSolicitudSeleccionada.WorkflowId
    frmFlujos.m_lIdInstancia = g_oSolicitudSeleccionada.Id
    Set oWorkflows = oFSGSRaiz.generar_CWorkflows
    frmFlujos.lblDenFlujo = oWorkflows.SacarDen(g_oSolicitudSeleccionada.WorkflowId, True)
    frmFlujos.Show vbModal
End Sub

Private Sub sdbgComentarios2_BtnClick()
    'Muestra el detalle de la persona
    If sdbgComentarios2.col < 0 Then Exit Sub
    Select Case sdbgComentarios2.Columns(sdbgComentarios2.col).Name
        Case "USUARIO"
            If sdbgComentarios2.Columns("USUARIO").Value = "" Then Exit Sub
            frmDetallePersona.g_sPersona = sdbgComentarios2.Columns("USU").Value
            frmDetallePersona.Show vbModal
        Case "COMENTARIO"
            If sdbgComentarios2.Columns("HAY_COMENT").Value = "0" Then Exit Sub
        
            'si tiene permisos para modificar los comentario podr� modificarlos
            If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICModificarComent)) Is Nothing) Then
                    frmSolicitudGestionar.g_bSoloConsulta = False
                Else
                    frmSolicitudGestionar.g_bSoloConsulta = True
                End If
            Else
                frmSolicitudGestionar.g_bSoloConsulta = False
            End If
    
            frmSolicitudGestionar.g_sOrigen = "frmSolicitudComentarios"
            Set frmSolicitudGestionar.g_oSolicitud = g_oSolicitudSeleccionada
            frmSolicitudGestionar.Show vbModal
            If sdbgComentarios2.Columns("HAY_COMENT").Value = "0" Then
                sdbgComentarios2.Columns("COMENTARIO").CellStyleSet "" ', sdbgComentarios2.Row
            Else
                sdbgComentarios2.Columns("COMENTARIO").CellStyleSet "conComent" ', sdbgComentarios2.Row
            End If
            If sdbgComentarios2.DataChanged Then
                sdbgComentarios2.Update
            End If
    End Select
End Sub

Private Sub sdbgComentarios2_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer

    If sdbgComentarios2.Columns("ETAPA").CellValue(Bookmark) = "" And sdbgComentarios2.Columns("DESTINATARIO").CellValue(Bookmark) = "" Then
        For i = 0 To sdbgComentarios2.Columns.Count - 1
            sdbgComentarios2.Columns(i).CellStyleSet ""
        Next i
    Else
        If sdbgComentarios2.Columns("ACCION").CellValue(Bookmark) <> "" And sdbgComentarios2.Columns("FECHA").CellValue(Bookmark) <> "" And sdbgComentarios2.Columns("DESTINATARIO").CellValue(Bookmark) <> "" Then
            For i = 0 To sdbgComentarios2.Columns.Count - 1
                sdbgComentarios2.Columns(i).CellStyleSet "Verde"
            Next i
        Else
            For i = 0 To sdbgComentarios2.Columns.Count - 1
                sdbgComentarios2.Columns(i).CellStyleSet "Naranja"
            Next i
    '        sdbgComentarios2.Columns("ID").Value = ""
        End If
    End If
    
    
    If sdbgComentarios2.Columns("HAY_COMENT").CellValue(Bookmark) = "1" Then
        sdbgComentarios2.Columns("COMENTARIO").CellStyleSet "conComent"
    Else
        sdbgComentarios2.Columns("COMENTARIO").CellStyleSet ""
    End If
End Sub
