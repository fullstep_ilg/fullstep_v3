VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroNegDetalle 
   BackColor       =   &H00808000&
   Caption         =   "Detalle"
   ClientHeight    =   2955
   ClientLeft      =   375
   ClientTop       =   3120
   ClientWidth     =   9135
   FillStyle       =   0  'Solid
   Icon            =   "frmInfAhorroNegDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2955
   ScaleWidth      =   9135
   Begin VB.PictureBox picgreen 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2595
      Left            =   120
      ScaleHeight     =   2595
      ScaleWidth      =   8895
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   60
      Width           =   8895
      Begin VB.PictureBox picTipoGrafico 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   420
         ScaleHeight     =   315
         ScaleWidth      =   2595
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   60
         Visible         =   0   'False
         Width           =   2595
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
            Height          =   285
            Left            =   0
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   0
            Width           =   1515
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Row.Count       =   5
            Row(0)          =   "Barras 2D"
            Row(1)          =   "Barras 3D"
            Row(2)          =   "Lineas 2D"
            Row(3)          =   "Lineas 3D"
            Row(4)          =   "Tarta"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).Caption=   "TIPO"
            Columns(0).Name =   "TIPO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.CommandButton cmdActualizar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroNegDetalle.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   480
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroNegDetalle.frx":0D3D
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   480
         Visible         =   0   'False
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgRes 
         Height          =   2295
         Left            =   420
         TabIndex        =   1
         Top             =   120
         Width           =   8235
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   17
         stylesets.count =   5
         stylesets(0).Name=   "White"
         stylesets(0).BackColor=   15400959
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmInfAhorroNegDetalle.frx":107F
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmInfAhorroNegDetalle.frx":109B
         stylesets(2).Name=   "Red"
         stylesets(2).BackColor=   4744445
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmInfAhorroNegDetalle.frx":10B7
         stylesets(3).Name=   "Blue"
         stylesets(3).BackColor=   16777152
         stylesets(3).Picture=   "frmInfAhorroNegDetalle.frx":10D3
         stylesets(4).Name=   "Green"
         stylesets(4).BackColor=   10409634
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmInfAhorroNegDetalle.frx":10EF
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   17
         Columns(0).Width=   1588
         Columns(0).Caption=   "Artículo"
         Columns(0).Name =   "ITEM"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasHeadForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).HeadBackColor=   12632256
         Columns(0).BackColor=   16776960
         Columns(1).Width=   3572
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16776960
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Procesos"
         Columns(2).Name =   "NUMPROCE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   12632256
         Columns(3).Width=   2566
         Columns(3).Caption=   "Presupuesto"
         Columns(3).Name =   "PRES"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "Standard"
         Columns(3).FieldLen=   256
         Columns(3).HasHeadForeColor=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).HeadBackColor=   12632256
         Columns(3).BackColor=   15400959
         Columns(4).Width=   2143
         Columns(4).Caption=   "Adjudicado"
         Columns(4).Name =   "ADJ"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).NumberFormat=   "Standard"
         Columns(4).FieldLen=   256
         Columns(4).HasHeadForeColor=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).HeadBackColor=   12632256
         Columns(4).BackColor=   15400959
         Columns(5).Width=   1984
         Columns(5).Caption=   "Ahorro"
         Columns(5).Name =   "AHO"
         Columns(5).Alignment=   1
         Columns(5).CaptionAlignment=   2
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).NumberFormat=   "Standard"
         Columns(5).FieldLen=   256
         Columns(5).HasHeadForeColor=   -1  'True
         Columns(5).HeadBackColor=   12632256
         Columns(6).Width=   1693
         Columns(6).Caption=   "%"
         Columns(6).Name =   "PORCEN"
         Columns(6).Alignment=   1
         Columns(6).CaptionAlignment=   2
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).NumberFormat=   "0.0#\%"
         Columns(6).FieldLen=   256
         Columns(6).HasHeadForeColor=   -1  'True
         Columns(6).HeadBackColor=   12632256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "UNI"
         Columns(7).Name =   "UNI"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "DENUNI"
         Columns(8).Name =   "DENUNI"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "DEST"
         Columns(9).Name =   "DEST"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "DENDEST"
         Columns(10).Name=   "DENDEST"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "PAG"
         Columns(11).Name=   "PAG"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(12).Width=   3200
         Columns(12).Visible=   0   'False
         Columns(12).Caption=   "DENPAG"
         Columns(12).Name=   "DENPAG"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "FECINI"
         Columns(13).Name=   "FECINI"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "FECFIN"
         Columns(14).Name=   "FECFIN"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "CODOCULTO"
         Columns(15).Name=   "CODOCULTO"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(16).Width=   3200
         Columns(16).Visible=   0   'False
         Columns(16).Caption=   "ASIG"
         Columns(16).Name=   "ASIG"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         _ExtentX        =   14526
         _ExtentY        =   4048
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSChart20Lib.MSChart MSChart1 
         Height          =   1995
         Left            =   420
         OleObjectBlob   =   "frmInfAhorroNegDetalle.frx":110B
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   420
         Width           =   8235
      End
      Begin VB.CommandButton cmdGrafico 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroNegDetalle.frx":2B31
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   120
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroNegDetalle.frx":2E73
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   315
      End
   End
   Begin VB.Label lblItemSinAsig 
      BackColor       =   &H00FFFFC0&
      Caption         =   "Items sin asignar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   5800
      TabIndex        =   10
      Top             =   2700
      Width           =   2900
   End
   Begin VB.Label lblItemAsig 
      BackColor       =   &H8000000E&
      Caption         =   "Items asignados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   2700
      TabIndex        =   9
      Top             =   2700
      Width           =   2900
   End
End
Attribute VB_Name = "frmInfAhorroNegDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Fecha As Date
Public Eqp As String
Public SoloDeAdjDir As Boolean
Public SoloEnReunion As Boolean
Public sGMN1 As String
Public sGMN2 As String
Public sGMN3 As String
Public sGMN4 As String
Public dequivalencia As Double
Public sDetalle As String

'Presupuestos
Public sPres1 As String
Public sPRES2 As String
Public sPRES3 As String
Public sPRES4 As String

'Variables utilizadas para el multilenguaje
Private sIdiTipoGrafico(5) As String
Private sIdiEqp As String
Private sIdiDetEqp As String
Private sIdiComprador As String
Private sIdiComp As String
Private sIdiDetComprador As String
Private sIdiItemAsig As String
Private sIdiItemSinAsig As String

Public g_sOrigen As String
Public sFecha As String
Public sMaterial As String


Private Sub cmdActualizar_Click()
    If cmdGrid.Visible Then
        MostrarGrafico sdbcTipoGrafico.Value
    End If
    
End Sub

Private Sub Form_Load()
    
    Me.Height = 3375
    Me.Width = 9255
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If g_sOrigen = "Concep3" Or g_sOrigen = "Concep4" Then
        lblItemAsig.Visible = True
        lblItemSinAsig.Visible = True
    Else
        lblItemAsig.Visible = False
        lblItemSinAsig.Visible = False
    End If
    
End Sub
Private Sub cmdGrafico_Click()
        
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
    
        Screen.MousePointer = vbHourglass
        picTipoGrafico.Visible = True
'        sdbcTipoGrafico.Value = "Barras 3D"
        sdbcTipoGrafico.Value = sIdiTipoGrafico(2)
        MostrarGrafico sdbcTipoGrafico.Value
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal

End Sub

Private Sub cmdGrid_Click()
        
        picTipoGrafico.Visible = False
        
        cmdGrafico.Visible = True
        cmdGrid.Visible = False
        sdbgRes.Visible = True
        MSChart1.Visible = False
End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    MSChart1.Visible = True
    Select Case Tipo
    
       Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar

                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
        
    
    
End Sub


Private Sub Form_Resize()
    
    If Me.Height < 1100 Then Exit Sub
    If Me.Width < 1100 Then Exit Sub
    
    picgreen.Width = Me.Width - 510
    picgreen.Height = Me.Height - 800
    sdbgRes.Width = picgreen.Width - 840
    sdbgRes.Height = picgreen.Height - 450
    
    If sdbgRes.Columns(2).Visible = False Then
        sdbgRes.Columns(0).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(1).Width = sdbgRes.Width * 0.25
        sdbgRes.Columns(3).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(4).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(5).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(6).Width = sdbgRes.Width * 0.15 - 470
    Else
        sdbgRes.Columns(0).Width = sdbgRes.Width * 0.1
        sdbgRes.Columns(1).Width = sdbgRes.Width * 0.2
        sdbgRes.Columns(2).Width = sdbgRes.Width * 0.1
        sdbgRes.Columns(3).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(4).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(5).Width = sdbgRes.Width * 0.15
        sdbgRes.Columns(6).Width = sdbgRes.Width * 0.15 - 470
    
    End If
    
    MSChart1.Width = sdbgRes.Width
    MSChart1.Height = sdbgRes.Height - 300
    
    lblItemSinAsig.Top = picgreen.Top + picgreen.Height
    lblItemAsig.Top = lblItemSinAsig.Top
    lblItemSinAsig.Left = sdbgRes.Left + sdbgRes.Width - lblItemSinAsig.Width
    lblItemAsig.Left = lblItemSinAsig.Left - lblItemAsig.Width - 200
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
    g_sOrigen = ""
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbgRes_DblClick()
Dim frm As frmInfAhorroNegDetalle
Dim frm2 As frmInfAhorroNegFechaDetalle
Dim ADORs As Ador.Recordset
    
    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Select Case sdbgRes.Columns(0).TagVariant
        
        Case sIdiEqp 'Equipo
            
                If MDI.ActiveForm.Name = "frmInfAhorroNegEqpReu" Then
                    'Equipos negociadores
                    Set ADORs = oGestorInformes.AhorroNegociadoEqpNegCompReunion(Me.Fecha, sdbgRes.Columns(0).Value)
                
                    If ADORs Is Nothing Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                    
                    Set frm = New frmInfAhorroNegDetalle
                    
                    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                    frm.caption = sIdiDetEqp & "      " & sdbgRes.Columns(0).Text
                    frm.sdbgRes.Columns(0).caption = sIdiComprador 'Comprador
                    frm.sdbgRes.Columns(0).TagVariant = sIdiComprador
                    frm.Fecha = Me.Fecha
                    frm.Eqp = sdbgRes.Columns(0).Text
                    frm.SoloDeAdjDir = Me.SoloDeAdjDir
                    frm.SoloEnReunion = Me.SoloEnReunion
                    frm.dequivalencia = dequivalencia
                    frm.sFecha = sFecha
                    
                    While Not ADORs.EOF
                        frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & Chr(m_lSeparador) & dequivalencia * ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & ADORs(5).Value
                        ADORs.MoveNext
                    Wend
                    
                Else
                    If MDI.ActiveForm.Name = "frmInfAhorroNegEqpDesde" Then
                        'Equipos negociadores en fecha
                        Set ADORs = oGestorInformes.AhorroNegociadoEqpNegCompradorFecha(Me.Fecha, , , , , , , sdbgRes.Columns(0).Value, Me.SoloEnReunion, Me.SoloDeAdjDir)
                    
                        If ADORs Is Nothing Then
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                        
                        Set frm = New frmInfAhorroNegDetalle
                        
                        sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                        frm.caption = sIdiDetEqp & "      " & sdbgRes.Columns(0).Text
                        frm.sdbgRes.Columns(0).caption = sIdiComp 'Comprador
                        frm.sdbgRes.Columns(0).TagVariant = sIdiComp
                        frm.Fecha = Me.Fecha
                        frm.Eqp = sdbgRes.Columns(0).Text
                        frm.dequivalencia = dequivalencia
                        frm.sFecha = sFecha
                        
                        While Not ADORs.EOF
                            frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & " , " & ADORs(2).Value & Chr(m_lSeparador) & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                            DoEvents
                            ADORs.MoveNext
                        Wend
                    Else
                        
                        'Equipos responsables en fecha
                        Set ADORs = oGestorInformes.AhorroNegociadoEqpResCompFecha(Me.Fecha, sdbgRes.Columns(0).Value, , , , , , Me.SoloDeAdjDir, Me.SoloEnReunion)
                    
                        If ADORs Is Nothing Then
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                        
                        Set frm = New frmInfAhorroNegDetalle
                        
                        sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                        
                        frm.sdbgRes.Columns("numproce").Visible = True
                        frm.caption = sIdiDetEqp & "      " & sdbgRes.Columns(0).Text
                        frm.sdbgRes.Columns(0).caption = sIdiComprador 'Comprador
                        frm.sdbgRes.Columns(0).TagVariant = sIdiComprador
                        frm.Fecha = Me.Fecha
                        frm.Eqp = sdbgRes.Columns(0).Text
                        frm.dequivalencia = dequivalencia
                        frm.sFecha = sFecha
                        While Not ADORs.EOF
                            frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                            ADORs.MoveNext
                        Wend
                        
                    
                    
                    End If
                
                 End If
                
                
                frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 400
                frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
                
                Screen.MousePointer = vbNormal
                
                frm.Show 1
                         
                sdbgRes.SelBookmarks.RemoveAll
                
                Set ADORs = Nothing
 
        Case sIdiComprador 'Comprador
                
                If MDI.ActiveForm.Name = "frmInfAhorroNegEqpReu" Or MDI.ActiveForm.Name = "frmInfAhorroNegEqpDesde" Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                    
                'Sacaremos el detalle de los procesos
                Set ADORs = oGestorInformes.AhorroNegociadoCompResFecha(Fecha, Me.Eqp, sdbgRes.Columns(0).Text, , , , , , , , , , , , Me.SoloDeAdjDir, Me.SoloEnReunion)
    
                Set frm2 = New frmInfAhorroNegFechaDetalle
                sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                
                frm2.caption = sIdiDetComprador & "      " & Me.sdbgRes.Columns(0).Text
                frm2.dequivalencia = dequivalencia
                frm2.sFecha = Me.Fecha
                
                If Not ADORs Is Nothing Then
                    While Not ADORs.EOF
                        frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                        ADORs.MoveNext
                    Wend
                End If
            
            frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm2.Show 1
            sdbgRes.SelBookmarks.RemoveAll
            
            Set ADORs = Nothing
    

        Case "GMN1"
            Set ADORs = oGestorInformes.AhorroNegociadoMatFecha(CDate(Me.Fecha), sdbgRes.Columns(0).Value, , , , , , , True, , , , Me.SoloEnReunion, Me.SoloDeAdjDir)
            If ADORs Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm = New frmInfAhorroNegDetalle
            frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsABR_GMN2
            frm.sdbgRes.Columns(0).TagVariant = "GMN2"
            frm.Fecha = Me.Fecha
            frm.SoloDeAdjDir = Me.SoloDeAdjDir
            frm.SoloEnReunion = Me.SoloEnReunion
            frm.sGMN1 = sdbgRes.Columns(0).Value
            frm.dequivalencia = dequivalencia
            frm.sDetalle = "GMN1"
            frm.sFecha = sFecha
            frm.sMaterial = sMaterial
            If Not ADORs Is Nothing Then
                While Not ADORs.EOF
                    frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & dequivalencia * ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                    ADORs.MoveNext
                Wend
            End If
            frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm.Show 1
            sdbgRes.SelBookmarks.RemoveAll
            
            Set ADORs = Nothing
    
        Case "GMN2"
            
            If frmInfAhorroNegMatDesde.sdbcFiltro.Text = frmInfAhorroNegMatDesde.sFechaFiltro Then
                Set ADORs = oGestorInformes.AhorroNegociadoMatFecha(Me.Fecha, Me.sGMN1, sdbgRes.Columns(0).Value, , , , , , True, , , , Me.SoloEnReunion, Me.SoloDeAdjDir)
            Else
                Set ADORs = oGestorInformes.AhorroNegociadoMatFecha("", Me.sGMN1, sdbgRes.Columns(0).Value, , , , , , True, , , , Me.SoloEnReunion, Me.SoloDeAdjDir, frmInfAhorroNegMatDesde.txtFecDesde.Text, frmInfAhorroNegMatDesde.txtFecHasta.Text)
            End If
            If ADORs Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm = New frmInfAhorroNegDetalle
            frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsABR_GMN3
            frm.sdbgRes.Columns(0).TagVariant = "GMN3"
            frm.Fecha = Me.Fecha
            frm.SoloDeAdjDir = Me.SoloDeAdjDir
            frm.SoloEnReunion = Me.SoloEnReunion
            frm.sGMN1 = Me.sGMN1
            frm.sGMN2 = sdbgRes.Columns(0).Value
            frm.dequivalencia = dequivalencia
            frm.sDetalle = "GMN2"
            frm.sFecha = sFecha
            frm.sMaterial = sMaterial
            While Not ADORs.EOF
                frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                ADORs.MoveNext
            Wend
            
            frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm.Show 1
            sdbgRes.SelBookmarks.RemoveAll
            
            Set ADORs = Nothing
            
        Case "GMN3"
                        
            If frmInfAhorroNegMatDesde.sdbcFiltro.Text = frmInfAhorroNegMatDesde.sFechaFiltro Then
                Set ADORs = oGestorInformes.AhorroNegociadoMatFecha(Me.Fecha, Me.sGMN1, Me.sGMN2, sdbgRes.Columns(0).Value, , , , , , True, , , Me.SoloEnReunion, Me.SoloDeAdjDir)
            Else
                Set ADORs = oGestorInformes.AhorroNegociadoMatFecha("", Me.sGMN1, Me.sGMN2, sdbgRes.Columns(0).Value, , , , , , True, , , Me.SoloEnReunion, Me.SoloDeAdjDir, frmInfAhorroNegMatDesde.txtFecDesde.Text, frmInfAhorroNegMatDesde.txtFecHasta.Text)
            End If
            If ADORs Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm = New frmInfAhorroNegDetalle
            frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsABR_GMN4
            frm.sdbgRes.Columns(0).TagVariant = "GMN4"
            frm.Fecha = Me.Fecha
            frm.SoloDeAdjDir = Me.SoloDeAdjDir
            frm.SoloEnReunion = Me.SoloEnReunion
            frm.sGMN1 = Me.sGMN1
            frm.sGMN2 = Me.sGMN2
            frm.sGMN3 = sdbgRes.Columns(0).Value
            frm.dequivalencia = dequivalencia
            frm.sDetalle = "GMN3"
            frm.sFecha = sFecha
            frm.sMaterial = sMaterial
            While Not ADORs.EOF
                frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                ADORs.MoveNext
            Wend
            
            frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm.Show 1
            sdbgRes.SelBookmarks.RemoveAll
            
            Set ADORs = Nothing
        
        Case "GMN4"
            
            If frmInfAhorroNegMatDesde.sdbcFiltro.Text = frmInfAhorroNegMatDesde.sFechaFiltro Then
                Set ADORs = oGestorInformes.AhorroNegociadoFecha(Me.Fecha, Me.sGMN1, Me.sGMN2, Me.sGMN3, sdbgRes.Columns(0).Value, , , , , , True, , , Me.SoloEnReunion, Me.SoloDeAdjDir)
            Else
                Set ADORs = oGestorInformes.AhorroNegociadoFecha("", Me.sGMN1, Me.sGMN2, Me.sGMN3, sdbgRes.Columns(0).Value, , , , , , True, , , Me.SoloEnReunion, Me.SoloDeAdjDir, frmInfAhorroNegMatDesde.txtFecDesde.Text, frmInfAhorroNegMatDesde.txtFecHasta.Text)
            End If
            
            If ADORs Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm2 = New frmInfAhorroNegFechaDetalle
            frm2.dequivalencia = dequivalencia
            frm2.sGMN1 = Me.sGMN1
            frm2.sGMN2 = Me.sGMN2
            frm2.sGMN3 = Me.sGMN3
            frm2.sGMN4 = sdbgRes.Columns(0).Value
            frm2.sDetalle = "GMN4"
            frm2.sFecha = sFecha
            
            While Not ADORs.EOF
                frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value & Chr(m_lSeparador) & ADORs("FECULTREU").Value
                ADORs.MoveNext
            Wend
            
            frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm2.Show 1
            sdbgRes.SelBookmarks.RemoveAll
            
            Set ADORs = Nothing
        
        Case basParametros.gParametrosGenerales.gsSingPres3
            If sPRES4 <> "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoFechaCon3(Me.Fecha, Me.sPres1, Me.sPRES2, Me.sPRES3, sdbgRes.Columns(0).Value, , , , , , True, , Me.SoloEnReunion, Me.SoloDeAdjDir)
                If ADORs Is Nothing Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                Set frm2 = New frmInfAhorroNegFechaDetalle
                frm2.g_sOrigen = "Concep3"
                frm2.dequivalencia = dequivalencia
                frm2.sFecha = sFecha
                While Not ADORs.EOF
                    frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                    ADORs.MoveNext
                Wend
                
                frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
                frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
                
                Screen.MousePointer = vbNormal
                
                frm2.Show 1
                
            Else
                If Me.sPres1 = "" Then sPres1 = sdbgRes.Columns(0).Value
                Set ADORs = oGestorInformes.AhorroNegociadoConcep3Fecha(Me.Fecha, Me.sPres1, Me.sPRES2, Me.sPRES3, Me.sPRES4, , , , True, , Me.SoloEnReunion, Me.SoloDeAdjDir)
                If ADORs Is Nothing Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                Set frm = New frmInfAhorroNegDetalle
                frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsSingPres3
                frm.sdbgRes.Columns(0).TagVariant = basParametros.gParametrosGenerales.gsSingPres3
                frm.Fecha = Me.Fecha
                frm.SoloDeAdjDir = Me.SoloDeAdjDir
                frm.SoloEnReunion = Me.SoloEnReunion
                
                frm.sPres1 = Me.sPres1
                frm.sFecha = sFecha
                If Me.sPRES2 = "" Then
                    frm.sPRES2 = ADORs(0).Value
                ElseIf Me.sPRES3 = "" Then
                    frm.sPres1 = sPres1
                    frm.sPRES2 = sPRES2
                    frm.sPRES3 = ADORs(0).Value
                Else
                    frm.sPres1 = Me.sPres1
                    frm.sPRES2 = Me.sPRES2
                    frm.sPRES3 = Me.sPRES3
                    frm.sPRES4 = ADORs(0).Value
                End If
                
                frm.dequivalencia = dequivalencia
                While Not ADORs.EOF
                    frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                    ADORs.MoveNext
                Wend
                
                frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
                frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
                
                Screen.MousePointer = vbNormal
                
                frm.Show 1
            End If
            
            sdbgRes.SelBookmarks.RemoveAll
            
            Set ADORs = Nothing
            
        Case basParametros.gParametrosGenerales.gsSingPres4
            If sPRES4 <> "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoFechaCon4(Me.Fecha, Me.sPres1, Me.sPRES2, Me.sPRES3, sdbgRes.Columns(0).Value, , , , , , True, , Me.SoloEnReunion, Me.SoloDeAdjDir)
                If ADORs Is Nothing Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                Set frm2 = New frmInfAhorroNegFechaDetalle
                frm2.g_sOrigen = "Concep4"
                frm2.dequivalencia = dequivalencia
                frm2.sFecha = sFecha
                While Not ADORs.EOF
                    frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                    ADORs.MoveNext
                Wend
                
                frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
                frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
                
                Screen.MousePointer = vbNormal
                
                frm2.Show 1
                
            Else
                If Me.sPres1 = "" Then sPres1 = sdbgRes.Columns(0).Value
                
                Set ADORs = oGestorInformes.AhorroNegociadoConcep4Fecha(Me.Fecha, Me.sPres1, Me.sPRES2, Me.sPRES3, Me.sPRES4, , , , True, , Me.SoloEnReunion, Me.SoloDeAdjDir)
                
                If ADORs Is Nothing Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                Set frm = New frmInfAhorroNegDetalle
                frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsSingPres4
                frm.sdbgRes.Columns(0).TagVariant = basParametros.gParametrosGenerales.gsSingPres4
                frm.Fecha = Me.Fecha
                frm.SoloDeAdjDir = Me.SoloDeAdjDir
                frm.SoloEnReunion = Me.SoloEnReunion
        
                frm.sPres1 = Me.sPres1
                frm.sFecha = sFecha
                If Me.sPRES2 = "" Then
                    frm.sPRES2 = ADORs(0).Value
                ElseIf Me.sPRES3 = "" Then
                    frm.sPres1 = sPres1
                    frm.sPRES2 = sPRES2
                    frm.sPRES3 = ADORs(0).Value
                Else
                    frm.sPres1 = Me.sPres1
                    frm.sPRES2 = Me.sPRES2
                    frm.sPRES3 = Me.sPRES3
                    frm.sPRES4 = ADORs(0).Value
                End If
                
                frm.dequivalencia = dequivalencia
                While Not ADORs.EOF
                    frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                    ADORs.MoveNext
                Wend
                
                frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
                frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
                
                Screen.MousePointer = vbNormal
                
                frm.Show 1
            End If
            
            sdbgRes.SelBookmarks.RemoveAll
            
            Set ADORs = Nothing
    End Select
        
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
    
    If g_sOrigen = "Concep3" Or g_sOrigen = "Concep4" Then
        If sdbgRes.Columns("ASIG").Value = "0" Then
            For i = 0 To 4
                sdbgRes.Columns(i).CellStyleSet "Blue"
            Next
        Else
            For i = 0 To 4
                sdbgRes.Columns(i).CellStyleSet "White"
            Next
        End If
    End If
    
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_NEGDETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
       ' Caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgRes.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        
        
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        
        sIdiEqp = Ador(0).Value
        Ador.MoveNext
        sIdiDetEqp = Ador(0).Value
        Ador.MoveNext
        sIdiComprador = Ador(0).Value
        Ador.MoveNext
        sIdiDetComprador = Ador(0).Value
        Ador.MoveNext
        sIdiItemAsig = Ador(0).Value
        Ador.MoveNext
        sIdiItemSinAsig = Ador(0).Value
       
        Ador.Close
    
    End If

    Set Ador = Nothing
    
    If g_sOrigen = "Concep3" Then
        lblItemAsig.caption = Replace(sIdiItemAsig, "CONCEP", LCase(gParametrosGenerales.gsSingPres3))
        lblItemSinAsig.caption = Replace(sIdiItemSinAsig, "CONCEP", LCase(gParametrosGenerales.gsSingPres3))
    End If
    If g_sOrigen = "Concep4" Then
        lblItemAsig.caption = Replace(sIdiItemAsig, "CONCEP", LCase(gParametrosGenerales.gsSingPres4))
        lblItemSinAsig.caption = Replace(sIdiItemSinAsig, "CONCEP", LCase(gParametrosGenerales.gsSingPres4))
    End If

End Sub






