VERSION 5.00
Begin VB.Form frmPAIFiltrar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pa�ses (Filtro)"
   ClientHeight    =   2550
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5400
   Icon            =   "frmPAIFiltrar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2550
   ScaleWidth      =   5400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   75
      TabIndex        =   6
      Top             =   60
      Width           =   5235
      Begin VB.CheckBox chkIgualCod 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   1140
         TabIndex        =   1
         Top             =   420
         Width           =   3450
      End
      Begin VB.OptionButton optCOD 
         BackColor       =   &H00808000&
         Caption         =   "Por c�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   0
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.TextBox txtCOD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   3
         TabIndex        =   0
         Top             =   360
         Width           =   795
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   75
      TabIndex        =   7
      Top             =   1080
      Width           =   5235
      Begin VB.OptionButton optDEN 
         BackColor       =   &H00808000&
         Caption         =   "Por denominaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   -60
         Width           =   1935
      End
      Begin VB.TextBox txtDEN 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   100
         TabIndex        =   2
         Top             =   360
         Width           =   2850
      End
      Begin VB.CheckBox chkIgualDen 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3180
         TabIndex        =   3
         Top             =   420
         Width           =   2040
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2760
      TabIndex        =   5
      Top             =   2130
      Width           =   1095
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1500
      TabIndex        =   4
      Top             =   2130
      Width           =   1095
   End
End
Attribute VB_Name = "frmPAIFiltrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmPaiFiltrar
''' *** Creacion: 28/12/1998 (Javier Arana)
''' *** Ultima revision: 14/01/1999 (Alfredo Magallon)

Option Explicit

Private Sub chkIgualCod_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    txtCod.SetFocus
    
End Sub

Private Sub Form_Load()

    ''' * Objetivo: Situar el formulario respecto al
    ''' * Objetivo: principal de paises
    
    On Error Resume Next
    
    Me.Left = frmPAI.Left + 500
    Me.Top = frmPAI.Top + 1000
    CargarRecursos
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPAI
    
    
        
End Sub

Private Sub optCOD_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    If optCOD.Value = True Then
        optDEN.Value = False
    End If
        
    txtCod.SetFocus
   
End Sub
Private Sub chkIgualDen_Click()

    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    txtDen.SetFocus
    
End Sub

Private Sub optDEN_Click()

    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    If optDEN.Value = True Then
        optCOD.Value = False
    End If
    
    txtDen.SetFocus
    
End Sub
Private Sub cmdAceptar_Click()

    ''' * Objetivo: Aplicar el filtro y descargar
    ''' * Objetivo: el formulario
    
    Screen.MousePointer = vbHourglass
    
    Set frmPAI.oPaises = Nothing
    Set frmPAI.oPaises = oFSGSRaiz.generar_cpaises

        
    'frmPAI.Caption = "Pa�ses (Consulta)"
    frmPAI.ponerCaption "", 3, False
    
    If optCOD.Value = True And txtCod <> "" Then
        
        frmPAI.sCriterioCodListado = txtCod
        frmPAI.sCriterioDenListado = ""
        frmPAI.bCriterioCoincidenciaTotal = chkIgualCod.Value
        
        If Not chkIgualCod.Value = vbChecked Then
        
            frmPAI.oPaises.CargarTodosLosPaises Trim(txtCod), , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Pai)
    '            frmPAI.Caption = "Paises (Consulta Codigo = " & Trim(txtCOD) & "*)"
            frmPAI.ponerCaption Trim(txtCod), 1, True
        
        Else
        
            frmPAI.oPaises.CargarTodosLosPaises Trim(txtCod), , True, , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Pai)
'            frmPAI.Caption = "Paises (Consulta Codigo = " & Trim(txtCOD) & ")"
            frmPAI.ponerCaption Trim(txtCod), 1, False
        
        End If
        
    Else
    
        If optDEN.Value = True And txtDen <> "" Then
        
            frmPAI.sCriterioCodListado = ""
            frmPAI.sCriterioDenListado = txtDen
            frmPAI.bCriterioCoincidenciaTotal = chkIgualDen.Value
            
            If Not chkIgualDen.Value = vbChecked Then
                
                frmPAI.oPaises.CargarTodosLosPaises , Trim(txtDen), , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Pai)
'                frmPAI.Caption = "Paises (Consulta Denominacion = " & Trim(txtDEN) & "*)"
                 frmPAI.ponerCaption Trim(txtDen), 2, True
               
            Else
                
                frmPAI.oPaises.CargarTodosLosPaises , Trim(txtDen), True, , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Pai)
'                frmPAI.Caption = "Paises (Consulta Denominacion = " & Trim(txtDEN) & ")"
                 frmPAI.ponerCaption Trim(txtDen), 2, False
                
            End If
                
        Else
        
            frmPAI.sCriterioCodListado = ""
            frmPAI.sCriterioDenListado = ""
            frmPAI.bCriterioCoincidenciaTotal = False
        
            frmPAI.oPaises.CargarTodosLosPaises , , , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Pai)
            
        End If
            
    End If
        
        
    frmPAI.sdbgPaises.ReBind
    
    MDI.MostrarFormulario frmPAI, True
    
    frmPAI.sdbgPaises.MoveFirst
    
    Screen.MousePointer = vbNormal
    
    Unload Me
        
    frmPAI.SetFocus
    
End Sub
Private Sub cmdCancelar_Click()

    ''' * Objetivo: Cerrar el formulario
    
    MDI.MostrarFormulario frmPAI, True
    
    Unload Me
    
    frmPAI.SetFocus
            
End Sub
Private Sub Form_Activate()

    ''' * Objetivo: Iniciar el formulario
    
    txtCod.SetFocus
    
End Sub
Private Sub txtCOD_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por codigo

    optCOD.Value = True
    optDEN.Value = False

End Sub
Private Sub txtDEN_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por denominacion

    optCOD.Value = False
    optDEN.Value = True

End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PAIFILTRAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    Caption = Ador(0).Value
    Ador.MoveNext
    optCOD.Caption = Ador(0).Value
    Ador.MoveNext
    chkIgualCod.Caption = Ador(0).Value
    chkIgualDen.Caption = Ador(0).Value
    Ador.MoveNext
    optDEN.Caption = Ador(0).Value
    Ador.MoveNext
    cmdAceptar.Caption = Ador(0).Value
    Ador.MoveNext
    cmdCancelar.Caption = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub



