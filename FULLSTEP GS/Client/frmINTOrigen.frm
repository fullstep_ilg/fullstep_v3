VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmINTOrigen 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "dOrigen"
   ClientHeight    =   3495
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7620
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmINTOrigen.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3495
   ScaleWidth      =   7620
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdFTP 
      Caption         =   "..."
      Height          =   285
      Left            =   2835
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   615
      Width           =   315
   End
   Begin VB.CommandButton cmdNTFSRecibosERP 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7200
      Picture         =   "frmINTOrigen.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2310
      Width           =   315
   End
   Begin VB.TextBox txtOrigenRecibosERP 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1980
      TabIndex        =   5
      Top             =   2310
      Width           =   5205
   End
   Begin VB.CommandButton cmdNTFSIntermedia 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7200
      Picture         =   "frmINTOrigen.frx":0D71
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1800
      Width           =   315
   End
   Begin VB.TextBox txtOrigenIntermedia 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1980
      TabIndex        =   3
      Top             =   1800
      Width           =   5205
   End
   Begin VB.CommandButton cmdNTFSEntidad 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7200
      Picture         =   "frmINTOrigen.frx":0E30
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1200
      Width           =   315
   End
   Begin VB.TextBox txtOrigenEntidad 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1980
      TabIndex        =   1
      Top             =   1200
      Width           =   5205
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4005
      TabIndex        =   8
      Top             =   2820
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2880
      TabIndex        =   7
      Top             =   2820
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcTipo 
      Height          =   285
      Left            =   1320
      TabIndex        =   0
      Top             =   600
      Width           =   1485
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2275
      Columns(0).Caption=   "Tipo"
      Columns(0).Name =   "DEN"
      Columns(0).CaptionAlignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "COD"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2619
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblOrigenRecibosERP 
      BackColor       =   &H00808000&
      Caption         =   "DRuta:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   960
      Left            =   120
      TabIndex        =   12
      Top             =   2310
      Width           =   1890
   End
   Begin VB.Label lblOrigenIntermedia 
      BackColor       =   &H00808000&
      Caption         =   "DRuta:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   120
      TabIndex        =   11
      Top             =   1800
      Width           =   1890
   End
   Begin VB.Label LblOrigen 
      BackColor       =   &H00808000&
      Caption         =   "DRuta:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   120
      TabIndex        =   10
      Top             =   1200
      Width           =   1890
   End
   Begin VB.Label lblTipo 
      BackColor       =   &H00808000&
      Caption         =   "DTipo :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   615
      Width           =   930
   End
End
Attribute VB_Name = "frmINTOrigen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
''' Objeto para utilizar el m�todo BrowseForFolder que visualiza una dialog box para seleccionar carpetas
Public m_oEntidadInt As CEntidadInt
Public m_oERPsIntOrigen As CERPsInt
Private m_iTipo As Integer
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean
Public m_bConsulta As Boolean
Public m_sEntidad As String
Public m_sCodERP As String
Public m_sTabla As String
Private m_oEntidadesIntegracion As CEntidadesInt
Private m_sTipoDestinoWCF As String
Private m_sConexionDestinoWCF As String
'''
''' Variables para idiomas
'''
Private m_sOrigenNTFS As String
Private m_sOrigenFTP As String
Private m_sOrigenWebSer As String
Private m_aTipoOrigenEntidad(1 To 3) As String
Private m_aTipoOrigenIntermedia(1 To 3) As String
Private m_aTipoOrigenRecibosERP(1 To 3) As String
Private m_sSelCarpeta As String
Private m_sConfFTP As String
Private m_sConfWEB As String

''' <summary>
''' Cargar las variables de pantalla multiidioma
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo:0 </remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ORIGEN_INTEGRACION, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        caption = m_sEntidad & ": " & Ador(0).Value
        Ador.MoveNext
        m_aTipoOrigenEntidad(1) = Ador(0).Value
        m_aTipoOrigenEntidad(2) = Ador(0).Value
        m_aTipoOrigenEntidad(3) = Ador(0).Value
        Ador.MoveNext
        m_aTipoOrigenIntermedia(1) = Ador(0).Value
        m_aTipoOrigenIntermedia(2) = Ador(0).Value
        m_aTipoOrigenIntermedia(3) = Ador(0).Value
        Ador.MoveNext
        m_aTipoOrigenRecibosERP(1) = Ador(0).Value
        m_aTipoOrigenRecibosERP(2) = Ador(0).Value
        m_aTipoOrigenRecibosERP(3) = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        m_sOrigenNTFS = Ador(0).Value
        Ador.MoveNext
        lblTipo.caption = Ador(0).Value
        Ador.MoveNext
        m_sOrigenFTP = Ador(0).Value
        Ador.MoveNext
        m_sConfFTP = Ador(0).Value
        Ador.MoveNext
        m_sOrigenWebSer = Ador(0).Value 'webservice
        Ador.MoveNext
        m_sConfWEB = Ador(0).Value
        Ador.MoveNext
        m_sTipoDestinoWCF = Ador(0).Value
        Ador.MoveNext
        m_sConexionDestinoWCF = Ador(0).Value
        Ador.Close
    End If

    Set Ador = Nothing

End Sub

''' <summary>
''' Responde a la pulsaci�n del bot�n Aceptar. Graba en bbdd los datos del origen.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1 </remarks>
Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit


    If sdbcTipo.Text = "" Then
       oMensajes.NoValido lblTipo.caption
       Exit Sub
    End If
    If m_iTipo = 0 Then
        oMensajes.NoValido lblTipo.caption
       Exit Sub
    End If

    'Solo se validan cuando el origen es NTFS
    If m_iTipo = 1 Then
        If m_oEntidadInt.sentido = SentidoIntegracion.salida Then
            If txtOrigenRecibosERP.Text = "" Then
                oMensajes.NoValido lblOrigenRecibosERP.caption
                Exit Sub
            End If
        Else
            If Not IsNull(m_oEntidadInt.sentido) Then
             If txtOrigenEntidad.Text = "" And Not m_oEntidadInt.TablaIntermedia Then
                oMensajes.NoValido LblOrigen.caption
                Exit Sub
             End If
             End If
        End If
        ' Si se marca el uso de tabla intermedia hay que validad el path
        If m_oEntidadInt.TablaIntermedia Then
            If txtOrigenIntermedia.Text = "" Then
                oMensajes.NoValido lblOrigenIntermedia.caption
                Exit Sub
            End If
        End If
    End If
 
 If Not m_bConsulta Then
    If m_iTipo = tipodestinointegracion.FTP Then
        If NullToStr(m_oEntidadInt.FTP_Ip) = "" Then
            oMensajes.FaltanDatos (m_sConfFTP)
            Exit Sub
        End If
    End If
    If m_iTipo = tipodestinointegracion.WEBSERVICE Then
        If NullToStr(m_oEntidadInt.WEB_Orig_Url) = "" Then
            oMensajes.FaltanDatos (m_sConfWEB)
            Exit Sub
        End If
    End If

    teserror = m_oEntidadInt.ModificarOrigen(m_iTipo, txtOrigenEntidad.Text, txtOrigenRecibosERP.Text, txtOrigenIntermedia.Text)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    frmCONFIntegracion.m_oEntidadesInt.CargarTodasLasEntidades (m_sCodERP)
 End If

 Unload Me
End Sub

Private Sub cmdCancelar_Click()
 Unload Me
End Sub

''' <summary>
''' Nos muestra la configuraci�n Ftp � WebService dependiendo del tipo seleccionado
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0 </remarks>
Private Sub cmdFTP_Click()
    If UCase(sdbcTipo.Value) = m_sOrigenWebSer Then
        frmINTWebService.m_tipodestino = WEBSERVICE
        frmINTWebService.m_bConsulta = m_bConsulta
        frmINTWebService.m_sEntidad = m_sEntidad
        frmINTWebService.m_sfrmLlamada = "frmINTOrigen"
        frmINTWebService.Left = Me.Left + 100
        frmINTWebService.Top = Me.Top + 1500
        Load frmINTWebService
        frmINTWebService.Show 1
    ElseIf sdbcTipo.Value = m_sTipoDestinoWCF Then
        frmINTWebService.m_tipodestino = WCF
        frmINTWebService.m_sfrmLlamada = "frmINTOrigen"
        frmINTWebService.caption = m_sConexionDestinoWCF & " " & m_sEntidad '''@@@BDD??
        frmINTWebService.m_bConsulta = m_bConsulta
        frmINTWebService.m_sEntidad = m_sEntidad
        
        frmINTWebService.Left = Me.Left + 100
        frmINTWebService.Top = Me.Top + 1500
        Load frmINTWebService
        
        frmINTWebService.Show 1
    Else
        
        frmINTFTP.m_bConsulta = m_bConsulta
        frmINTFTP.m_sEntidad = m_sEntidad
        frmINTFTP.m_sSistemaExterno = m_sCodERP
        frmINTFTP.m_sfrmLlamada = "frmINTOrigen"
        frmINTFTP.Left = Me.Left + 100
        frmINTFTP.Top = Me.Top + 1500
        Load frmINTFTP
        frmINTFTP.Show 1
        
    End If
End Sub

Private Sub cmdNTFSEntidad_Click()
Dim sBuffer As String
Dim sInicio As String

On Error GoTo Error

sInicio = txtOrigenEntidad.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, m_sSelCarpeta, sInicio)

If sBuffer <> "" Then
    txtOrigenEntidad.Text = sBuffer
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    txtOrigenEntidad.Text = ""



End Sub



Private Sub cmdNTFSIntermedia_Click()
Dim sBuffer As String
Dim sInicio As String

On Error GoTo Error

sInicio = txtOrigenIntermedia.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, m_sSelCarpeta, sInicio)

If sBuffer <> "" Then
    txtOrigenIntermedia.Text = sBuffer
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    txtOrigenIntermedia.Text = ""
End Sub

Private Sub cmdNTFSRecibosERP_Click()
Dim sBuffer As String
Dim sInicio As String

On Error GoTo Error

sInicio = txtOrigenRecibosERP.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, m_sSelCarpeta, sInicio)

If sBuffer <> "" Then
    txtOrigenRecibosERP.Text = sBuffer
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    txtOrigenRecibosERP.Text = ""
End Sub

''' <summary>
''' Carga e inicializa la pantalla
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0 </remarks>
Private Sub Form_Load()

    Dim oERPInt As CERPInt

    Me.Height = 4150 ''3500
    
    CargarRecursos
    
    ''MultiERP:Se cargan los diferentes ERP
    Set m_oERPsIntOrigen = Nothing
    Set m_oERPsIntOrigen = oFSGSRaiz.generar_CERPsInt
    m_oERPsIntOrigen.CargarTodosLosERPs
    
    sdbcTipo.AddItem m_sOrigenNTFS & Chr(9) & CStr(tipodestinointegracion.NTFS)
    sdbcTipo.AddItem m_sOrigenFTP & Chr(9) & CStr(tipodestinointegracion.FTP)
    sdbcTipo.AddItem m_sOrigenWebSer & Chr(9) & CStr(tipodestinointegracion.WEBSERVICE)
    sdbcTipo.AddItem m_sTipoDestinoWCF & Chr(9) & CStr(tipodestinointegracion.WCF)
    
    
    m_sTabla = m_oEntidadInt.Entidad
    ''Se cargan las rutas origen
    If Not IsNull(m_oEntidadInt.OrigenTipo) Then
        m_iTipo = m_oEntidadInt.OrigenTipo
    End If
    
    If m_iTipo = tipodestinointegracion.FTP Then
        txtOrigenEntidad = NullToStr(m_oEntidadInt.FTP_Origen)
        txtOrigenIntermedia = NullToStr(m_oEntidadInt.FTP_Intermedia)
        txtOrigenRecibosERP = NullToStr(m_oEntidadInt.FTP_RecibosERP)
    Else
        txtOrigenEntidad = NullToStr(m_oEntidadInt.OrigenEntidad)
        txtOrigenIntermedia = NullToStr(m_oEntidadInt.OrigenIntermedia)
        txtOrigenRecibosERP = NullToStr(m_oEntidadInt.OrigenRecibosERP)
    End If
    
    If m_oEntidadInt.OrigenTipo = tipodestinointegracion.FTP Or m_oEntidadInt.OrigenTipo = tipodestinointegracion.WEBSERVICE Or m_oEntidadInt.OrigenTipo = tipodestinointegracion.WCF Then
        cmdFTP.Visible = True
    Else
        cmdFTP.Visible = False
    End If

    If m_bConsulta Then ' En modo consulta todo protegido
        sdbcTipo.Enabled = False
        txtOrigenEntidad.Enabled = False
        txtOrigenRecibosERP.Enabled = False
        txtOrigenIntermedia.Enabled = False
        cmdNTFSEntidad.Enabled = False
        cmdNTFSRecibosERP.Enabled = False
        cmdNTFSIntermedia.Enabled = False
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
    Else
        sdbcTipo.Enabled = True
        txtOrigenEntidad.Enabled = True
        txtOrigenRecibosERP.Enabled = True
        If Not m_oEntidadInt.TablaIntermedia Then
            txtOrigenIntermedia.Enabled = False
            cmdNTFSIntermedia.Enabled = False
        Else
            txtOrigenIntermedia.Enabled = True
            cmdNTFSIntermedia.Enabled = True
        End If
        
        cmdNTFSEntidad.Enabled = True
        cmdNTFSRecibosERP.Enabled = True
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
    End If


    Select Case m_oEntidadInt.DestinoTipo ' la label toma una caption u otra en funci�n del tipo de destino
    Case tipodestinointegracion.NTFS
        LblOrigen.caption = m_aTipoOrigenEntidad(1)
        lblOrigenIntermedia.caption = m_aTipoOrigenIntermedia(1)
        lblOrigenRecibosERP.caption = m_aTipoOrigenRecibosERP(1) & " " & m_sCodERP & ":"
    Case tipodestinointegracion.WCF
        Me.LblOrigen.caption = "Url"
    Case Else
        LblOrigen.caption = m_aTipoOrigenEntidad(1)
        lblOrigenIntermedia.caption = m_aTipoOrigenIntermedia(1)
        lblOrigenRecibosERP.caption = m_aTipoOrigenRecibosERP(1) & " " & m_sCodERP & ":"
    End Select
    
    cmdNTFSEntidad.Visible = True
    cmdNTFSRecibosERP.Visible = True
    cmdNTFSIntermedia.Visible = True

    mostrarOcultarDestinos
End Sub

Private Sub Form_Unload(Cancel As Integer)

Set m_oEntidadInt = Nothing
Set m_oERPsIntOrigen = Nothing
Set m_oEntidadesIntegracion = Nothing

End Sub

Private Sub sdbcTipo_Change()

    If Not bRespetarCombo Then
    
        bCargarComboDesde = True
        'm_iTipo = 0
    End If
    

End Sub

''' <summary>
''' Tras seleccionar un tipo, establece a que rutas se tiene acceso
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0 </remarks>
Private Sub sdbcTipo_CloseUp()

    bRespetarCombo = True
    sdbcTipo.Text = sdbcTipo.Columns("DEN").Text
    bRespetarCombo = False
    
    m_iTipo = sdbcTipo.Columns("COD").Value
    
    cmdFTP.Visible = False
    
    If Not m_oEntidadInt.TablaIntermedia Then
        cmdNTFSIntermedia.Enabled = False
    Else
        cmdNTFSIntermedia.Enabled = True
    End If
    cmdNTFSEntidad.Enabled = True
    cmdNTFSRecibosERP.Enabled = True
    
    If m_iTipo = tipodestinointegracion.NTFS Then
        LblOrigen.caption = m_aTipoOrigenEntidad(1)
        lblOrigenIntermedia.caption = m_aTipoOrigenIntermedia(1)
        lblOrigenRecibosERP.caption = m_aTipoOrigenRecibosERP(1) & " " & m_sCodERP & ":"

    ElseIf m_iTipo = tipodestinointegracion.FTP Then
        LblOrigen.caption = m_aTipoOrigenEntidad(2)
        lblOrigenIntermedia.caption = m_aTipoOrigenIntermedia(2)
        lblOrigenRecibosERP.caption = m_aTipoOrigenRecibosERP(2) & " " & m_sCodERP & ":"
        cmdFTP.Visible = True
    ElseIf m_iTipo = tipodestinointegracion.WCF Then
        cmdFTP.Visible = True
    Else 'Web
        LblOrigen.caption = m_aTipoOrigenEntidad(3)
        lblOrigenIntermedia.caption = m_aTipoOrigenIntermedia(3)
        lblOrigenRecibosERP.caption = m_aTipoOrigenRecibosERP(3) & " " & m_sCodERP & ":"
        
        cmdFTP.Visible = True
    End If
        
    txtOrigenEntidad.Text = ""
    txtOrigenIntermedia.Text = ""
    txtOrigenRecibosERP.Text = ""
    
    bCargarComboDesde = False
    
    mostrarOcultarDestinos

End Sub

Private Sub sdbcTipo_InitColumnProps()
  
    sdbcTipo.DataFieldList = "Column 0"
    sdbcTipo.DataFieldToDisplay = "Column 0"

End Sub

Public Sub sdbcTipo_Validate(Cancel As Boolean)
    If sdbcTipo.Text = "" Then Exit Sub
   ' m_iTipo = sdbcTipo.Columns("COD").Value
    bCargarComboDesde = False
    
End Sub

Private Sub mostrarOcultarDestinos()
    If m_iTipo = tipodestinointegracion.WCF Then
       Me.LblOrigen.Visible = False
       Me.lblOrigenRecibosERP.Visible = False
       Me.lblOrigenIntermedia.Visible = False
       Me.txtOrigenEntidad.Visible = False
       Me.txtOrigenRecibosERP.Visible = False
       Me.txtOrigenIntermedia.Visible = False
       'Me.cmdFTP.Visible = False
       Me.cmdNTFSRecibosERP.Visible = False
       Me.cmdNTFSIntermedia.Visible = False
       Me.cmdNTFSEntidad.Visible = False
    Else
       Me.LblOrigen.Visible = True
       Me.lblOrigenRecibosERP.Visible = True
       Me.lblOrigenIntermedia.Visible = True
       Me.txtOrigenEntidad.Visible = True
       Me.txtOrigenRecibosERP.Visible = True
       Me.txtOrigenIntermedia.Visible = True
       Me.cmdFTP.Visible = True
       Me.cmdNTFSRecibosERP.Visible = True
       Me.cmdNTFSIntermedia.Visible = True
       Me.cmdNTFSEntidad.Visible = True
    End If
End Sub

