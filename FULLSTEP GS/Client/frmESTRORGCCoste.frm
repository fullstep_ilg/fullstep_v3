VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmESTRORGCCoste 
   Caption         =   "Gestionar Centro de Coste"
   ClientHeight    =   5985
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10410
   Icon            =   "frmESTRORGCCoste.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5985
   ScaleWidth      =   10410
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picNavigate2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   120
      ScaleHeight     =   435
      ScaleWidth      =   8385
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   5520
      Width           =   8385
      Begin VB.CommandButton cmdModificar 
         Caption         =   "D&Modificar"
         Height          =   345
         Left            =   0
         TabIndex        =   6
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "D&Aceptar"
         Height          =   345
         Left            =   3840
         TabIndex        =   5
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "D&Cancelar"
         Height          =   345
         Left            =   4920
         TabIndex        =   4
         Top             =   45
         Width           =   1005
      End
   End
   Begin MSComctlLib.TreeView tvwEstrPres 
      Height          =   4815
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   8493
      _Version        =   393217
      Style           =   7
      Checkboxes      =   -1  'True
      ImageList       =   "ImageList2"
      Appearance      =   1
   End
   Begin VB.ComboBox cboPartPres 
      Height          =   315
      ItemData        =   "frmESTRORGCCoste.frx":014A
      Left            =   1920
      List            =   "frmESTRORGCCoste.frx":014C
      TabIndex        =   0
      Text            =   "cboPartPres"
      Top             =   120
      Width           =   8415
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   9720
      Top             =   5520
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   -2147483643
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORGCCoste.frx":014E
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORGCCoste.frx":05A0
            Key             =   "PRES1"
            Object.Tag             =   "PRES1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORGCCoste.frx":09F2
            Key             =   "PRES2"
            Object.Tag             =   "PRES2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORGCCoste.frx":0E44
            Key             =   "PRES3"
            Object.Tag             =   "PRES3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORGCCoste.frx":1296
            Key             =   "PRES4"
            Object.Tag             =   "PRES4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORGCCoste.frx":16E8
            Key             =   "PRESBAJALOGICA"
            Object.Tag             =   "PRESBAJALOGICA"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORGCCoste.frx":20FA
            Key             =   "PRESSEL"
            Object.Tag             =   "PRESSEL"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblPartida 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Partida Presupuestaria:"
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   195
      Width           =   1665
   End
End
Attribute VB_Name = "frmESTRORGCCoste"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Variables privadas
Private m_oPartidas As cPresConceptos5Nivel0
Private oPres0 As cPresConcep5Nivel0
Private m_sPartida As String
Private oIBaseDatos As IBaseDatos

'Variables publicas
Public m_oPresupuestos As CPresConceptos5Nivel1 'Contendra toda la estructura de presupuestos
Public m_oPresupuestosNiv0 As cPresConceptos5Nivel0 'Contendra toda la estructura de presupuestos

'Variables para el arbol de unidades de negocio de QA
Private m_sEstructuraArbolPresupuestario As String '"C o M"    Modo del arbol de partida presupuestaria

Private m_SelNode As MSComctlLib.node
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String
Private m_sUON4 As String
Private m_sPres0Inicial As String
Dim arArboles() As String 'C�digo de los �rboles presentes en el combo
Dim m_bCambiosEnNodosSel As Boolean 'Boolean que nos dir� si se han modificado los nodos seleciconados
Dim m_sGuardarCambios As String
Dim m_sSeleccionePartida As String
Dim m_sConfirmarDesmarcarNodos As String

Private m_sConsultarCCoste As String
Private m_sGestionarCCoste As String
Private m_bSoloLectura As Boolean


''' <summary>
''' Procedimiento que, para la unidad organizativa seleccionada, obtiene si la hay, una partida presupuestaria con nodos asociados a dicha UO
''' recupera dichos nodos y los carga en el treeview del formulario
''' En caso de no haber partidas con nodos vinculados a la UO, carga la primera partida presupuestaria existente
''' </summary>
''' <remarks>Llamada desde la carga del formulario. Tiempo m�ximo < 1 seg.</remarks>
Private Sub Form_Load()

    Dim bPartidaYaSeleccionada As Boolean
    Dim i As Integer

    CargarRecursos
    
    CargarSeguridad

    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    cmdModificar.Visible = True

    'COMPROBAMOS SI EL NODO SELECCIONADO TIENE ALGUNA PARTIDA PRESUPUESTARIA CON NODOS SELECCIONADOS.
    'SI ES AS�, TOMAMOS EL C�DIGO DE LA PRIMERA QUE HAYA PARA MOSTRARLA
    If NoHayParametro(m_sEstructuraArbolPresupuestario) Then
        m_sEstructuraArbolPresupuestario = "C"
    End If
    
    'Con el nodo de la unidad organizativa, recuperamos sus datos presupuestarios
    Set m_SelNode = frmESTRORG.g_SelNode
    
    Set m_oPartidas = Nothing
    Set m_oPartidas = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    
    If Not m_SelNode Is Nothing Then
        
        Dim ILongCodUON1 As Long
        Dim ILongCodUON2 As Long
        Dim ILongCodUON3 As Long
        Dim ILongCodUON4 As Long
        
        ILongCodUON1 = basParametros.gLongitudesDeCodigos.giLongCodUON1
        ILongCodUON2 = basParametros.gLongitudesDeCodigos.giLongCodUON2
        ILongCodUON3 = basParametros.gLongitudesDeCodigos.giLongCodUON3
        ILongCodUON4 = basParametros.gLongitudesDeCodigos.giLongCodUON4
        
        m_sUON1 = Trim(Mid(m_SelNode.key, 5, ILongCodUON1))
        m_sUON2 = Trim(Mid(m_SelNode.key, 5 + ILongCodUON1, ILongCodUON2))
        m_sUON3 = Trim(Mid(m_SelNode.key, 5 + ILongCodUON1 + ILongCodUON2, ILongCodUON3))
        m_sUON4 = Trim(Mid(m_SelNode.key, 5 + ILongCodUON1 + ILongCodUON2 + ILongCodUON3, ILongCodUON4))
    
        Dim sDen As String
        Dim aPartidaPresup() As String
    
        'Al cargar el form, comprobamos si la UON tiene ya alguna partida presup asociada
        '(cogemos la primera)
        aPartidaPresup = m_oPartidas.obtenerPartidaPresup(m_sUON1, m_sUON2, m_sUON3, m_sUON4)
        
        m_sPres0Inicial = aPartidaPresup(0)
        sDen = aPartidaPresup(1)
        
    Else
        'ERROR ERROR ERROR
        'Si m_SelNode es nothing, significa que no vienes desde el men� contextual de un nodo del �rbol presupuestario
        'Entonces, �de d�nde vienes?
        m_sUON1 = ""
        m_sUON2 = ""
        m_sUON3 = ""
        m_sUON4 = ""
    End If
    
    'CARGAR COMBO CON LAS PARTIDAS PRESUPUESTARIAS
    Dim oPartida As cPresConcep5Nivel0
    
  
    Screen.MousePointer = vbHourglass
    
    cboPartPres.clear
    
    'Pero le pasamos un TRUE porque s�lo queremos las partidas presup con nodos activos (o sea, con al menos 1 nodo de nivel 1 que no sea baja l�gica)
    m_oPartidas.CargarPresupuestosConceptos5 , , , , , , , , , , False
     
    
    ReDim arArboles(m_oPartidas.Count)
    
    For Each oPartida In m_oPartidas
        cboPartPres.AddItem oPartida.Cod & " - " & oPartida.Den
        arArboles(cboPartPres.ListCount - 1) = oPartida.Cod
    Next
    
    'Si s�lo hay un partida
    If m_oPartidas.Count = 1 Then
        'Si s�lo hay una partida presupuestaria, ocultamos el combo
        tvwEstrPres.Height = tvwEstrPres.Height + (tvwEstrPres.Top - cboPartPres.Top)
        tvwEstrPres.Top = cboPartPres.Top
        cboPartPres.Visible = False
    End If
    
    Screen.MousePointer = vbNormal
          
    'Si hay partidas presupuestarias
    If m_oPartidas.Count > 0 Then
        
        bPartidaYaSeleccionada = False
        
        'Seleccionamos en el combo la partida que hemos obtenido antes
        If m_sPres0Inicial <> "" Then
            For i = 0 To cboPartPres.ListCount - 1
                If cboPartPres.List(i) = m_sPres0Inicial & " - " & sDen Or arArboles(i) = m_sPres0Inicial Then
                    'AL ASIGNAR EL INDEX, SE CARGA EL ARBOL DESDE EL cboPartPres_Click
                    cboPartPres.ListIndex = i
                    bPartidaYaSeleccionada = True
                End If
            Next
        End If
            
        If Not bPartidaYaSeleccionada Then
            'Si no hay ninguna partida presup seleccionada (porque la unidad organizativa tiene nodos seleccionados de la misma)
            'seleccionamos la primera partida
            'Al dar valor al ListIndex, ya directamente se va a cargar el Treeview
            'pues se activa el evento cboPartPres_click
            cboPartPres.ListIndex = 0
            
        End If
        
        'UBICACI�N DEL FORM
        Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        Me.Top = (MDI.ScaleHeight / 2 - Me.Height / 2) + 450
    End If

End Sub

''' <summary>
''' Procedimiento que carga los textos que aparecer�n en el formulario en el idioma que corresponda
''' </summary>
''' <remarks>Llamada desde form_load. Tiempo m�ximo < 0,01 seg.</remarks>
Private Sub CargarRecursos()
Dim rs As New ADODB.Recordset
    Set rs = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRORG_CCOSTE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not rs Is Nothing Then
        Me.caption = rs(0).Value '1 Gestionar Centro de Coste
        m_sGestionarCCoste = rs(0).Value '1 Gestionar Centro de Coste
        rs.MoveNext
        lblPartida.caption = rs(0).Value & ":" '2 Partida presupuestaria
        m_sPartida = rs(0).Value & ":"
        rs.MoveNext
        cmdCancelar.caption = rs(0).Value '3 Cancelar
        rs.MoveNext
        cmdModificar.caption = rs(0).Value '4 Modificar
        rs.MoveNext
        cmdAceptar.caption = rs(0).Value '5 Aceptar
        rs.MoveNext
        m_sGuardarCambios = rs(0).Value '6 Se han producido cambios en la selecci�n, �desea guardar los cambios?
        rs.MoveNext
        m_sSeleccionePartida = rs(0).Value '7 Por favor, seleccione una partida presupuestaria.
        rs.MoveNext
        m_sConfirmarDesmarcarNodos = rs(0).Value '8 Ha desmarcado elementos que tienen asociado un importe vigente. �Desea guardar los cambios?
        rs.MoveNext
        m_sConsultarCCoste = rs(0).Value '9 Consultar Centros de Coste
    End If
    
    rs.Close
End Sub


''' <summary>
''' Procedimiento que controla si el usuario tiene permiso para gestionar los centros de coste o no.
''' En caso de carecer de permiso, se ocultan los botones de edici�n
''' Del mismo modo, daremos valor a la variable m_bSoloLectura, que usaremos m�s tarde para devolver todas las partidas presupuestarias o s�lo las vinculadas a
''' una Unidad Organizativa concreta
''' </summary>
''' <remarks>Llamada desde Form_Load. Tiempo m�ximo < 0,01 seg.</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub CargarSeguridad()
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGGestionarCC)) Is Nothing) Then
        picNavigate2.Visible = False
        m_bSoloLectura = True
        Me.caption = m_sConsultarCCoste
    Else
        picNavigate2.Visible = True
        m_bSoloLectura = False
        Me.caption = m_sGestionarCCoste
    End If
        
    'INTEGRACION
    'Si la integraci�n es s�lo de entrada, no se puede editar
    If gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.CentroCoste) Then
        picNavigate2.Visible = False
        m_bSoloLectura = True
        Me.caption = m_sConsultarCCoste
    End If
End Sub

''' <summary>
''' Procedimiento que muestra la informaci�n del formulario en modo de s�lo lectura
''' </summary>
''' <remarks>Llamada desde cmdAceptar_Click, cmdCancelar_Click, cboPartPres_Click. Tiempo m�ximo 0,3 seg.</remarks>
Private Sub ModoConsulta()
    m_sEstructuraArbolPresupuestario = "C"
    
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    cmdModificar.Visible = True
                
    m_bCambiosEnNodosSel = False 'En modo consulta no hay cambios en nodos. por si acaso pasamos a false la vble
    
    If NoHayParametro(cboPartPres.ListIndex) Then
        iniciarCargaArbolPresup m_sPres0Inicial
    Else
        iniciarCargaArbolPresup arArboles(cboPartPres.ListIndex)
    End If
End Sub

''' <summary>
''' Procedimiento que muestra la informaci�n del formulario en modo modificable
''' </summary>
''' <remarks>Llamada desde cmdModificar_Click. Tiempo m�ximo 0,3 seg.</remarks>
Private Sub ModoEdicion()
    m_sEstructuraArbolPresupuestario = "M"

    cmdAceptar.Visible = True
    cmdCancelar.Visible = True
    cmdModificar.Visible = False
    
    If NoHayParametro(cboPartPres.ListIndex) Then
        iniciarCargaArbolPresup m_sPres0Inicial
    Else
        iniciarCargaArbolPresup arArboles(cboPartPres.ListIndex)
    End If
End Sub

''' <summary>
''' Procedimiento que inicia la carga de �rbol presupuestario. Llama a un procedimento que genera una clase CPresConcep5Nivel0 con el c�digo de Partida presupuestaria recibido y luego a otro que genera el �rbol
''' </summary>
''' <param name="sPres0">C�digo de la partida presupuestaria de la que se desea construir el �rbol</param>
''' <remarks>Llamada desde ModoConsulta, ModoEdicion. Tiempo m�ximo 0,02 seg.</remarks>
Private Sub iniciarCargaArbolPresup(ByVal sPres0 As String)
    PartidaSeleccionada sPres0
    
    GenerarEstructuraPresupuestos sPres0, False
End Sub

''' <summary>
''' Procedimiento que genera el �rbol presupuestario a partir de un c�digo de partida presupuestaria determinado
''' </summary>
''' <param name="sPres0">C�digo de la partida presupuestaria de la que se desea construir el �rbol</param>
''' <param name="bOrdenadoPorDen">Boolean que indica si se desea ordenar la estructura de presupuestos por descripci�n. False implica ordenar por c�digo</param>
''' <remarks>Llamada desde iniciarCargaArbolPresup. Tiempo m�ximo 0,03 seg.</remarks>
Private Sub GenerarEstructuraPresupuestos(ByVal sPres0 As String, ByVal bOrdenadoPorDen As Boolean)
    Dim nodo As MSComctlLib.node

    If m_sEstructuraArbolPresupuestario = "C" Or m_sEstructuraArbolPresupuestario = "" Then 'SOLO LECTURA -> FORMULARIO DE SOLO LECTURA
        tvwEstrPres.Checkboxes = False
    Else 'M, MODO EDICION
        tvwEstrPres.Checkboxes = False
    End If
    
    tvwEstrPres.Nodes.clear
    
    Set m_oPresupuestos = oFSGSRaiz.Generar_CPresConceptos5Nivel1
    m_oPresupuestos.GenerarEstructuraPresupSeleccionados m_sUON1, m_sUON2, m_sUON3, m_sUON4, basPublic.gParametrosInstalacion.gIdioma, m_bSoloLectura, bOrdenadoPorDen, sPres0
    
    'Generamos el arbol de presupuestos que se ve en el formulario
    GenerarArbolPresupuestos
    
    Exit Sub

Error:
    Set nodo = Nothing
    Resume Next
End Sub

''' <summary>
''' Procedimiento que, a partir de unos datos ya generados, construye un �rbol presupuestario con sus nodos
''' en un control treeview
''' </summary>
''' <remarks>Llamada desde GenerarEstructuraPresupuestos; Tiempo m�ximo 0,02 seg.</remarks>
Private Sub GenerarArbolPresupuestos()
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim opres1 As CPresConcep5Nivel1
    Dim oPRES2 As CPresConcep5Nivel2
    Dim oPRES3 As CPresConcep5Nivel3
    Dim oPRES4 As CPresConcep5Nivel4
    Dim nodx As MSComctlLib.node
    Dim i As Integer
    
    If m_sEstructuraArbolPresupuestario = "C" Or m_sEstructuraArbolPresupuestario = "" Then 'SOLO LECTURA -> FORMULARIO DE SOLO LECTURA
        
        tvwEstrPres.Checkboxes = False
    Else 'M, MODO EDICION
        
        tvwEstrPres.Checkboxes = True
    End If

    tvwEstrPres.Nodes.clear

    Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", oPres0.Den, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True
    
    If oPres0.Seleccionado Then
        If tvwEstrPres.Checkboxes Then
            nodx.Checked = True
        Else
            nodx.Image = "PRESSEL"
        End If
    End If
    

    For Each opres1 In m_oPresupuestos
        If Not (opres1.BajaLog) Then
            scod1 = opres1.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(opres1.Cod))
            Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, opres1.Cod & " - " & opres1.Den, "PRES1")
            nodx.Tag = "PRES1" & opres1.Cod
            If opres1.Seleccionado Then
                If tvwEstrPres.Checkboxes Then
                    nodx.Checked = True
                Else
                    nodx.Image = "PRESSEL"
                End If
            End If
        End If

        For Each oPRES2 In opres1.PresConceptos5Nivel2
            If Not (oPRES2.BajaLog) Then
                scod2 = oPRES2.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(oPRES2.Cod))
                Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                nodx.Tag = "PRES2" & oPRES2.Cod
                If oPRES2.Seleccionado Then
                    If tvwEstrPres.Checkboxes Then
                        nodx.Checked = True
                    Else
                        nodx.Image = "PRESSEL"
                    End If
                End If
            End If

                For Each oPRES3 In oPRES2.PresConceptos5Nivel3
                    If Not (oPRES3.BajaLog) Then
                        scod3 = oPRES3.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(oPRES3.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                        nodx.Tag = "PRES3" & oPRES3.Cod
                        If oPRES3.Seleccionado Then
                            If tvwEstrPres.Checkboxes Then
                                nodx.Checked = True
                            Else
                                nodx.Image = "PRESSEL"
                            End If
                        End If
                    End If

                    For Each oPRES4 In oPRES3.PresConceptos5Nivel4
                        If Not (oPRES4.BajaLog) Then
                            scod4 = oPRES4.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(oPRES4.Cod))
                            Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                            nodx.Tag = "PRES4" & oPRES4.Cod
                            If oPRES4.Seleccionado Then
                                If tvwEstrPres.Checkboxes Then
                                    nodx.Checked = True
                                Else
                                    nodx.Image = "PRESSEL"
                                End If
                            End If
                        End If
                    Next
                    Set oPRES4 = Nothing
                Next
                Set oPRES3 = Nothing
        Next
        Set oPRES2 = Nothing
    Next

    With tvwEstrPres
        For i = 1 To .Nodes.Count
                If .Checkboxes Then 'treeview de escritura
                    If .Nodes(i).Checked And .Nodes(i).Tag <> "Raiz " Then
                        .Nodes(i).EnsureVisible
                        .Nodes(i).Expanded = False
                    End If
                Else 'treeview de solo lectura
                    If .Nodes(i).Image = "PRESSEL" And .Nodes(i).Tag <> "Raiz " Then
                        .Nodes(i).EnsureVisible
                        .Nodes(i).Expanded = False
                    End If
                End If
        Next
    End With

    Set opres1 = Nothing
        
    Set m_oPresupuestos = Nothing
    
    Exit Sub
    
Error:
    Set nodx = Nothing
    Set m_oPresupuestos = Nothing
    Resume Next
End Sub

''' <summary>
''' Procedimiento que guarda los cambios realizados en los nodos presupuestarios vinculados a la Unidad Organizativa sobre la que estemos trabajando
''' y pasa la visualizaci�n del formulario a s�lo lectura
''' </summary>
''' <remarks>Llamada desde Bot�n Aceptar del presente formulario: frmESTRORGCCoste; Tiempo m�ximo < 1 seg</remarks>
Private Sub cmdAceptar_Click()
   
    Screen.MousePointer = vbHourglass
    
    AceptarCambiosNodosPresup
    
    ModoConsulta
        
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>
''' Procedimiento que pasa la visualizaci�n del formulario a modo s�lo lectura y descarta los cambios que se hayan podido hacer y no hayan sido guardados previamente
''' </summary>
''' <remarks>Llamada desde Boton Cancelar del formulario frmESTRORGCCoste; Tiempo m�ximo < 1 seg</remarks>
Private Sub cmdCancelar_Click()
    
    ModoConsulta
    
End Sub

''' <summary>
''' Procedimiento que pasa la visualizaci�n del formulario a modo modificaci�n
''' </summary>
''' <remarks>Llamada desde Boton Modificar del formulario frmESTRORGCCoste; Tiempo m�ximo < 1 seg</remarks>
Private Sub cmdModificar_Click()
    
    If cboPartPres.List(cboPartPres.ListIndex) = "" Then
        MsgBox (m_sSeleccionePartida)
        Exit Sub
    End If
    
    ModoEdicion
    
End Sub

''' <summary>
''' Procedimiento que crea una clase de partida presupuestaria (CPresConcep5Nivel0) al que, mediante el c�digo de la partida seleccionada
''' asigna todos los valores de dicha partida para poder trabajar con ella en el formulario.
''' </summary>
''' <param name="sPres0">C�digo de la partida presupuestaria seleccionada</param>
''' <remarks>Llamada desde iniciarCargaArbolPresup; Tiempo m�ximo < 1 seg.</remarks>
Private Sub PartidaSeleccionada(ByVal sPres0 As String)
    
    Set oPres0 = oFSGSRaiz.Generar_CPresConcep5Nivel0
    oPres0.Cod = sPres0
    oPres0.UON1 = m_sUON1
    oPres0.UON2 = m_sUON2
    oPres0.UON3 = m_sUON3
    oPres0.UON4 = m_sUON4
    Set oIBaseDatos = oPres0
    oIBaseDatos.IniciarEdicion
    Set oIBaseDatos = Nothing
    
End Sub


''' <summary>
''' Procedimiento que lanza la carga del �rbol de nodos presupuestarios a partir de la partida presup. seleccionada en el combo.
''' Controla as� mismo si, estando en modo edici�n, se han realizado cambios en los nodos seleccionados. Si es as�, solicita confirmaci�n para guardar los cambios.
''' </summary>
''' <remarks>Llamada desde la selecci�n de cualquiera de los elementos del combobox cboPartPres; Tiempo m�ximo < 1 seg.</remarks>
Private Sub cboPartPres_Click()
    
    If cboPartPres.List(cboPartPres.ListIndex) = "" Then
        tvwEstrPres.Nodes.clear
        Exit Sub
    End If
    
    If m_bCambiosEnNodosSel Then
        If Not oPres0 Is Nothing Then If cboPartPres.List(cboPartPres.ListIndex) = oPres0.Cod Then Exit Sub
            
        Dim Origen As String
        Origen = "cboPartPres"
        AceptarCambiosNodosPresup Origen
        
        m_bCambiosEnNodosSel = False
    End If
    
    m_sPres0Inicial = arArboles(cboPartPres.ListIndex)
    
    If cboPartPres = "" Then Exit Sub
    
    'Al de arbol presupuestario en el combo; volvemos a solo lectura
    m_sEstructuraArbolPresupuestario = "C"
    ModoConsulta

End Sub

''' <summary>
''' Procedimiento que marca como checkeados los nodos hijos de un nodo checkeado
''' </summary>
''' <param name="Nodx">Nodo checkeado</param>
''' <returns></returns>
''' <remarks>Llamada desde el chequeo un nodo en el arbol de nodos presupuestarios; Tiempo m�ximo < 1 seg.</remarks>
Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.node)
    
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node
    Dim nod4 As MSComctlLib.node

    Set nod1 = nodx.Child

    While Not nod1 Is Nothing
         
         nod1.Checked = True
         nod1.EnsureVisible
         nod1.Expanded = False
         Set nod2 = nod1.Child
         
         While Not nod2 Is Nothing
             nod2.Checked = True
             nod2.EnsureVisible
             nod2.Expanded = False
             Set nod3 = nod2.Child
             
             While Not nod3 Is Nothing
                 nod3.Checked = True
                 nod3.EnsureVisible
                 nod3.Expanded = False
                 Set nod4 = nod3.Child
                 
                 While Not nod4 Is Nothing
                     nod4.Checked = True
                     nod4.EnsureVisible
                     nod4.Expanded = False
                     Set nod4 = nod4.Next
                 Wend
                 Set nod3 = nod3.Next
             Wend
             Set nod2 = nod2.Next
         Wend
         Set nod1 = nod1.Next
    Wend
            
    DoEvents

End Sub

''' <summary>
''' Procedimiento que quita la marca de checkeados a los nodos hijos de un nodo checkeado
''' </summary>
''' <param name="Nodx">Nodo descheckeado</param>
''' <returns></returns>
''' <remarks>Llamada desde el deschequeo de un nodo en el arbol de nodos presupuestarios; Tiempo m�ximo < 1 seg.</remarks>

Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.node)
    
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

    Set nod1 = nodx.Child

    While Not nod1 Is Nothing
         
         nod1.Checked = False
         Set nod2 = nod1.Child
         
         While Not nod2 Is Nothing
             nod2.Checked = False
             Set nod3 = nod2.Child
             
             While Not nod3 Is Nothing
                 nod3.Checked = False
                 Set nod4 = nod3.Child
                 
                 While Not nod4 Is Nothing
                     nod4.Checked = False
                     Set nod4 = nod4.Next
                 Wend
                 Set nod3 = nod3.Next
             Wend
             Set nod2 = nod2.Next
         Wend
         Set nod1 = nod1.Next
    Wend
            
    DoEvents

End Sub

''' <summary>
''' Procedimiento que deschequea a los nodos padres del nodo deschequeado
''' </summary>
''' <param name="Nodx">Nodo deschequeado</param>
''' <returns></returns>
''' <remarks>Llamada desde el deschequeo de un nodo en el arbol de nodos presupuestarios; Tiempo m�ximo < 1 seg.</remarks>
Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.node)
    
    If nodx.Index = 1 Then
        'Es el nodo raiz
        nodx.Checked = False
    Else
        Set nodx = nodx.Parent
        While Not nodx Is Nothing And nodx.Index <> 1
            nodx.Checked = False
            Set nodx = nodx.Parent
        Wend
    End If
    DoEvents
    
End Sub

''' <summary>
''' Procedimiento que redimensiona los elementos del formulario cuando se cambian las dimensiones del mismo
''' </summary>
''' <remarks>Llamada desde la modificaci�n del tama�o del formulario; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Form_Resize()

    Arrange

End Sub

''' <summary>
''' Procedimiento que vac�a de contenido una serie de variables y objetos usados en el formulario
''' </summary>
''' <remarks>Llamada desde el evento Unload del formulario; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Form_Unload(Cancel As Integer)
Set oPres0 = Nothing
m_bCambiosEnNodosSel = False
m_sEstructuraArbolPresupuestario = ""
End Sub


''' <summary>
''' Procedimiento que al chequear un nodo, chequea �ste y sus hijos.
''' Si se deschequea, se deschequea el nodo y sus padres.
''' </summary>
''' <remarks>Llamada desde el chequeo de un nodo en el arbol de nodos presupuestarios; Tiempo m�ximo < 1 seg.</remarks>
Private Sub tvwEstrPres_NodeCheck(ByVal node As MSComctlLib.node)
    
    m_bCambiosEnNodosSel = True 'Cmabia el check de un nodo, con lo que ya suponemos que hay cambios y en base a esto preguntaremos si se quieresn conservar los cambios o no
       
    If node.Checked Then
        MarcarTodosLosHijos node
    Else
        QuitarMarcaTodosSusPadres node
        QuitarMarcaTodosLosHijos node
    End If
    
End Sub

''' <summary>
''' Guarda los nodos presupuestarios seleccionados en el arbol asociados a la unidad organizativa correspondiente
''' </summary>
''' <remarks>Llamada desde Boton Aceptar (cmdAceptar_Click) y desde la selecci�n de una partida presup en el combo (cboPartPres_Click); Tiempo m�ximo < 1 seg. </remarks>
Private Sub AceptarCambiosNodosPresup(Optional ByVal Origen As String)

If NoHayParametro(m_sUON1) And NoHayParametro(m_sUON2) And NoHayParametro(m_sUON3) And NoHayParametro(m_sUON4) Then
    'ERROR NO HAY DATOS DE LA UNIDAD ORGANIZATIVA
    Exit Sub
End If

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node
Dim nodx As MSComctlLib.node
Dim sPres0 As String
Dim sPres1 As String
Dim sPRES2 As String
Dim sPRES3 As String
Dim sPRES4 As String
Dim lLongPresNiv1 As Long
Dim lLongPresNiv2 As Long
Dim lLongPresNiv3 As Long
Dim lLongPresNiv4 As Long
Dim adoresNodosSeleccionados As New ADODB.Recordset
Dim nodosDesmarcados() As Variant
Dim nodosNuevos() As Variant
Dim nodosNuevosyDesmarcados() As Variant
Dim i As Long
Dim sMensajeAviso As String
Dim IRdoAviso As Integer
Dim hayImportes As Boolean

    adoresNodosSeleccionados.Fields.Append "PRES0", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv0
    adoresNodosSeleccionados.Fields.Append "PRES1", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv1
    adoresNodosSeleccionados.Fields.Append "PRES2", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv2
    adoresNodosSeleccionados.Fields.Append "PRES3", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv3
    adoresNodosSeleccionados.Fields.Append "PRES4", adVarChar, gLongitudesDeCodigos.giLongCodPres5Niv4
    adoresNodosSeleccionados.Open
    
    lLongPresNiv1 = basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1
    lLongPresNiv2 = basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2
    lLongPresNiv3 = basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3
    lLongPresNiv4 = basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4

    Set nodx = tvwEstrPres.Nodes(1)
    
    If nodx.key = "Raiz " Then
        If Not oPres0 Is Nothing Then
            sPres0 = oPres0.Cod
        Else
            sPres0 = arArboles(cboPartPres.ListIndex)
        End If
        
        If nodx.Checked Then
            adoresNodosSeleccionados.AddNew
            adoresNodosSeleccionados("PRES0") = sPres0
            adoresNodosSeleccionados("PRES1") = ""
            adoresNodosSeleccionados("PRES2") = ""
            adoresNodosSeleccionados("PRES3") = ""
            adoresNodosSeleccionados("PRES4") = ""
            adoresNodosSeleccionados.Update
        End If
    Else
        Exit Sub
    End If

    If nodx.Children > 0 Then
        Set nod1 = nodx.Child
        Do While Not nod1 Is Nothing
            
            sPres1 = Trim(Mid(nod1.key, 6, lLongPresNiv1))
            
            If nod1.Checked Then
                adoresNodosSeleccionados.AddNew
                adoresNodosSeleccionados("PRES0") = sPres0
                adoresNodosSeleccionados("PRES1") = sPres1
                adoresNodosSeleccionados("PRES2") = ""
                adoresNodosSeleccionados("PRES3") = ""
                adoresNodosSeleccionados("PRES4") = ""
                adoresNodosSeleccionados.Update
            End If
     
            If nod1.Children > 0 Then
                Set nod2 = nod1.Child
                Do While Not nod2 Is Nothing
                    
                    sPRES2 = Trim(Mid(nod2.key, (6 + lLongPresNiv1), lLongPresNiv2))
                    
                    If nod2.Checked Then
                        adoresNodosSeleccionados.AddNew
                        adoresNodosSeleccionados("PRES0") = sPres0
                        adoresNodosSeleccionados("PRES1") = sPres1
                        adoresNodosSeleccionados("PRES2") = sPRES2
                        adoresNodosSeleccionados("PRES3") = ""
                        adoresNodosSeleccionados("PRES4") = ""
                        adoresNodosSeleccionados.Update
                    End If

                    If nod2.Children > 0 Then
                        Set nod3 = nod2.Child
                        Do While Not nod3 Is Nothing
                            
                            sPRES3 = Trim(Mid(nod3.key, (6 + lLongPresNiv1 + lLongPresNiv2), lLongPresNiv3))
                            
                            If nod3.Checked Then
                                adoresNodosSeleccionados.AddNew
                                adoresNodosSeleccionados("PRES0") = sPres0
                                adoresNodosSeleccionados("PRES1") = sPres1
                                adoresNodosSeleccionados("PRES2") = sPRES2
                                adoresNodosSeleccionados("PRES3") = sPRES3
                                adoresNodosSeleccionados("PRES4") = ""
                                adoresNodosSeleccionados.Update
                            End If


                            If nod3.Children > 0 Then
                                Set nod4 = nod3.Child
                                Do While Not nod4 Is Nothing
                                    
                                    sPRES4 = Trim(Mid(nod4.key, (6 + lLongPresNiv1 + lLongPresNiv2 + lLongPresNiv3), lLongPresNiv4))
                                    
                                    If nod4.Checked Then
                                        adoresNodosSeleccionados.AddNew
                                        adoresNodosSeleccionados("PRES0") = sPres0
                                        adoresNodosSeleccionados("PRES1") = sPres1
                                        adoresNodosSeleccionados("PRES2") = sPRES2
                                        adoresNodosSeleccionados("PRES3") = sPRES3
                                        adoresNodosSeleccionados("PRES4") = sPRES4
                                        adoresNodosSeleccionados.Update
                                    End If
                                    
                                    Set nod4 = nod4.Next
                                Loop
                            End If
                            Set nod3 = nod3.Next
                        Loop
                    End If
                    Set nod2 = nod2.Next
                Loop
            End If
            Set nod1 = nod1.Next
        Loop
    End If
    
    If m_oPresupuestosNiv0 Is Nothing Then
        Set m_oPresupuestosNiv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    End If
    
    nodosNuevosyDesmarcados = m_oPresupuestosNiv0.obtenerNodosNuevosyDesmarcados(m_sUON1, m_sUON2, m_sUON3, m_sUON4, sPres0, adoresNodosSeleccionados)
    
    nodosNuevos = nodosNuevosyDesmarcados(1)
    nodosDesmarcados = nodosNuevosyDesmarcados(0)
    
'    'CREAR MENSAJE DE AVISO DE NODOS CON IMPORTE
    hayImportes = False
    If Not IsEmpty(nodosDesmarcados(0)) Then
        For i = 0 To UBound(nodosDesmarcados)
            If nodosDesmarcados(i)(5) = 1 Then
                hayImportes = True
                i = UBound(nodosDesmarcados)
            End If
        Next
        
        sMensajeAviso = m_sConfirmarDesmarcarNodos
    Else
        If Origen = "cboPartPres" Then
            sMensajeAviso = m_sGuardarCambios
            
        End If
    End If
    
    If hayImportes Or Origen = "cboPartPres" Then
        IRdoAviso = MsgBox(sMensajeAviso, vbYesNo + vbInformation + vbDefaultButton2)
    End If

    Select Case IRdoAviso
        Case vbNo
            'Exit Sub
        Case Else
            m_oPresupuestosNiv0.EliminarAniadirNodos m_sUON1, m_sUON2, m_sUON3, m_sUON4, nodosNuevos(), nodosDesmarcados(), True
    End Select
    
    Set m_oPresupuestos = Nothing

End Sub

''' <summary>
''' Procedimiento que el tamanyo y posicion de los controles del formulario cuando se cambian las dimensiones del mismo
''' </summary>
''' <remarks>Llamada desde el evento Resize del formulario: form_resize; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Arrange()
    
    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 6625 Then  'de tama�o de la ventana
            Me.Width = 6725       'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 4500 Then  'cuando no se maximiza ni
            Me.Height = 4570       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width
    
    
    If Height >= 1755 Then
        If Not cboPartPres.Visible Then
            tvwEstrPres.Height = Height - (1755 - 480)
        Else
            tvwEstrPres.Height = Height - 1755
        End If
        picNavigate2.Top = Height - 1050
    End If
    If Width >= 510 Then
        tvwEstrPres.Width = Width - 510
    End If
    If Width >= 2310 Then
        cboPartPres.Width = Width - 2310
    End If
    
End Sub


