VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CASelectorvaloresAtributo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'CASelectorValorAtributo
'edu
'T94
'20070118
'funcionalidad del selector de valores de atributo
Option Explicit

Public oMensajes As CMensajes

Private sIdiTrue As String
Private sIdiFalse As String
Private sIdiMayor As String
Private sIdiMenor As String
Private sIdiEntre As String

Private Sub CargarIdioma()
    sIdiTrue = oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63)
    sIdiFalse = oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64)
    sIdiMayor = oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 65)
    sIdiMenor = oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 66)
    sIdiEntre = oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 67)
End Sub



'devuelve el handler del control dropdown

Public Function Inicializar(ByRef ctl As Control, ByVal oatrib As CAtributo) As Long
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Select Case oatrib.TipoIntroduccion
        Case Introselec  'Seleccion
            ctl.RemoveAll
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If IsNull(oElem.ValorLista) = False Then
                     ctl.AddItem oElem.ValorLista
                End If
            Next
            If oatrib.Tipo = TipoNumerico Then
                ctl.Columns(0).Alignment = ssCaptionAlignmentRight
            Else
                ctl.Columns(0).Alignment = ssCaptionAlignmentLeft
            End If
            Inicializar = ctl.hWnd
            ctl.Enabled = True
        Case Else: 'Libre
            If oatrib.Tipo = TipoBoolean Then
                ctl.RemoveAll
                ctl.AddItem sIdiTrue
                ctl.AddItem sIdiFalse
                Inicializar = ctl.hWnd
                ctl.Enabled = True
            Else
                Inicializar = 0
                ctl.Enabled = False
            End If
    End Select

End Function

Public Sub CargarValores(ByRef ctl As Control, oatrib As CAtributo, celda As Object)
    Dim oLista As CValoresPond
    Dim oElem As CValorPond

    If celda.Locked Then
        ctl.Enabled = False
        Exit Sub
    End If

    ctl.RemoveAll
    
    If oatrib.TipoIntroduccion = Introselec Then
        Set oLista = oatrib.ListaPonderacion
        For Each oElem In oLista
            ctl.AddItem oElem.ValorLista
        Next
    Else
        If oatrib.Tipo = TipoBoolean Then
            ctl.AddItem sIdiTrue
            ctl.AddItem sIdiFalse
        End If
    End If

End Sub


Public Sub InitColumnProps(ctl As Control)
    ctl.DataFieldList = "Column 2"
    ctl.DataFieldToDisplay = "Column 2"
End Sub


Public Function PositionList(ByVal Text As String, ctl As Control) As String
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    PositionList = ""
    
    On Error Resume Next
   
    ctl.MoveFirst
    
    If Text <> "" Then
        For i = 0 To ctl.Rows - 1
            bm = ctl.GetBookmark(i)
            If UCase(Text) = UCase(Mid(ctl.Columns(0).CellText(bm), 1, Len(Text))) Then
                PositionList = Mid(ctl.Columns(0).CellText(bm), 1, Len(Text))
                ctl.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Function


Public Function ValidateList(ByRef Text As String, caption As String, oatrib As Object) As Integer
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    ''' Si es nulo, correcto
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond

    If Text = "" Then
        ValidateList = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista

        If oatrib Is Nothing Then
            ValidateList = False
            Exit Function
        End If

        If oatrib.TipoIntroduccion = Introselec Then
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If oatrib.Tipo = TipoBoolean Then
                If UCase(sIdiTrue) = UCase(Text) Then
                    bExiste = True
                End If
                If UCase(sIdiFalse) = UCase(Text) Then
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            Text = ""
            oMensajes.NoValido caption
            ValidateList = False
            Exit Function
        End If
        ValidateList = True
    End If
End Function

Private Sub Class_Initialize()
    CargarIdioma
End Sub
