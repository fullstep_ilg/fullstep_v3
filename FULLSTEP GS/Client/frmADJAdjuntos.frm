VERSION 5.00
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Object = "{DE8CE233-DD83-481D-844C-C07B96589D3A}#1.1#0"; "vbalSgrid6.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmADJAdjuntos 
   BackColor       =   &H00808000&
   Caption         =   "DProveedor"
   ClientHeight    =   5565
   ClientLeft      =   1740
   ClientTop       =   2385
   ClientWidth     =   7995
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmADJAdjuntos.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5565
   ScaleWidth      =   7995
   Begin vbAcceleratorSGrid6.vbalGrid vbalComentarios 
      Height          =   2625
      Left            =   90
      TabIndex        =   5
      Top             =   45
      Width           =   7845
      _ExtentX        =   13838
      _ExtentY        =   4630
      BackgroundPictureHeight=   0
      BackgroundPictureWidth=   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DisableIcons    =   -1  'True
      HighlightSelectedIcons=   0   'False
   End
   Begin VB.PictureBox picAdjuntos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   2890
      Left            =   60
      ScaleHeight     =   2895
      ScaleWidth      =   7905
      TabIndex        =   0
      Top             =   2675
      Width           =   7900
      Begin UltraGrid.SSUltraGrid sdbgAdjuntos 
         Height          =   2255
         Left            =   0
         TabIndex        =   4
         Top             =   240
         Width           =   7875
         _ExtentX        =   13891
         _ExtentY        =   3969
         _Version        =   131072
         GridFlags       =   17040384
         LayoutFlags     =   72613888
         Bands           =   "frmADJAdjuntos.frx":014A
         Override        =   "frmADJAdjuntos.frx":037E
         Appearance      =   "frmADJAdjuntos.frx":03D4
         Caption         =   "sdbgAdjuntos"
      End
      Begin VB.CommandButton cmdAbrirAdjuntos 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   7440
         Picture         =   "frmADJAdjuntos.frx":0410
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   2540
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdSalvarAdjuntos 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   6960
         Picture         =   "frmADJAdjuntos.frx":048C
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   2540
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.Label lblFicAdjuntos 
         BackColor       =   &H00808000&
         Caption         =   "DFicheros adjuntos:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   3
         Top             =   0
         Width           =   3440
      End
   End
   Begin MSComDlg.CommonDialog cmmdAdjun 
      Left            =   495
      Top             =   4545
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmADJAdjuntos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oOrigen As Form

Public g_iAnyo As Integer
Public g_sGmn1 As String
Public g_iProceCod As Long
Public g_sProveCod As String
Public g_iNumOfe As Integer
Public g_sCaption As String
Public g_lGrupo As Long
Public g_bAdjunVisible As Boolean

'Variables de idiomas:
Private m_sIdiProceso As String
Private m_sIdiObsAdjuntos As String
Private m_sIdiObsOferta As String
Private m_sIdiGuardar As String
Private m_sIdiTipoOrig As String
Private m_sIdiElArchivo As String
Private m_sIdiOferta As String
Private m_skb As String
Private m_sIdiAmbito As String
Private m_sIdiComent As String
Private m_sIdiSize As String
Private m_sIdiComentario As String

Private m_oAdjuntos As CAdjuntos
Private sayFileNames() As String
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean

Private m_sMsgError As String
Private Sub cmdAbrirAdjuntos_Click()
    Dim oAdjun As CAdjunto
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
    
    If sdbgAdjuntos.ActiveRow Is Nothing Then
        oMensajes.SeleccioneFichero
        Exit Sub
    Else
        If sdbgAdjuntos.Selected.Rows.Count = 0 Then sdbgAdjuntos.Selected.Rows.Add sdbgAdjuntos.ActiveRow
    End If
    
    Set oAdjun = m_oAdjuntos.Item(CStr(sdbgAdjuntos.ActiveRow.Cells("ID").Value))
    sFileName = FSGSLibrary.DevolverPathFichTemp
    sFileName = sFileName & oAdjun.nombre
    sFileTitle = sdbgAdjuntos.ActiveRow.Cells("AMBITO").Value
        
    If oAdjun.DataSize = 0 Then
        oMensajes.NoValido m_sIdiElArchivo & " " & oAdjun.nombre
        Set oAdjun = Nothing
        Exit Sub
    End If
    
    'Comprueba si existe el fichero y si existe se borra:
    Dim FOSFile As Scripting.FileSystemObject
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing
    
    teserror = oAdjun.ComenzarLecturaData
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oAdjun = Nothing
        Exit Sub
    End If
                
    oAdjun.LeerAdjuntoOferta oAdjun.Id, oAdjun.DataSize, sFileName
    
    sayFileNames(UBound(sayFileNames)) = sFileName
    ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
    
    'Lanzamos la aplicacion
    ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1

Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    Set oAdjun = Nothing
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number = 70 Then
        Resume Next
    ElseIf err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntos", "cmdAbrirAdjuntos_Click", err, Erl, Me)
        GoTo Cancelar
        Exit Sub
    End If
End Sub

Private Sub cmdSalvarAdjuntos_Click()
    Dim oAdjun As CAdjunto
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
    
    If sdbgAdjuntos.ActiveRow Is Nothing Then
        oMensajes.SeleccioneFichero
        Exit Sub
    Else
        If sdbgAdjuntos.Selected.Rows.Count = 0 Then sdbgAdjuntos.Selected.Rows.Add sdbgAdjuntos.ActiveRow
    End If
    
    Set oAdjun = m_oAdjuntos.Item(CStr(sdbgAdjuntos.ActiveRow.Cells("ID").Value))
    
    If oAdjun.DataSize = 0 Then
        oMensajes.NoValido m_sIdiElArchivo & " " & oAdjun.nombre
        Set oAdjun = Nothing
        Exit Sub
    End If
    
    cmmdAdjun.DialogTitle = m_sIdiGuardar
    cmmdAdjun.CancelError = True
    cmmdAdjun.Filter = m_sIdiTipoOrig & "|*.*"
    cmmdAdjun.filename = oAdjun.nombre
    cmmdAdjun.ShowSave

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle
    If sFileTitle = "" Then
        oMensajes.NoValido m_sIdiElArchivo
        Exit Sub
    End If

    teserror = oAdjun.ComenzarLecturaData
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oAdjun = Nothing
        Exit Sub
    End If
                
    oAdjun.LeerAdjuntoOferta oAdjun.Id, oAdjun.DataSize, sFileName

Cancelar:
    On Error Resume Next

    Set oAdjun = Nothing
    
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number = 70 Then
        Resume Next
    ElseIf err.Number <> 0 Then
        If err.Number <> 32755 Then '32755 = han pulsado cancel
            m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntos", "cmdSalvarAdjuntos_Click", err, Erl, Me)
        End If
        GoTo Cancelar
        Exit Sub
    End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntos", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Height = 5250
    Me.Width = 11625
    m_bDescargarFrm = False
    m_bActivado = False
    ReDim sayFileNames(0)
    
    CargarRecursos
    
    If g_bAdjunVisible = True Then
        picAdjuntos.Visible = True
                
        'Carga los adjuntos:
        MostrarAdjuntosOferta
        sdbgAdjuntos.Layout.Load App.Path & "\AdjuntosADJ.ugd", ssPersistenceTypeFile, ssPropCatBands + ssPropCatGroups + ssPropCatUnboundColumns + ssPropCatAppearanceCollection + ssPropCatGeneral
        
    Else
        picAdjuntos.Visible = False
    End If
        
    
    'Carga las observaciones:
'    sdbgComentarios.Layout.Load App.Path & "\comentariosADJ.ugd", ssPersistenceTypeFile, ssPropCatBands + ssPropCatGroups + ssPropCatGeneral + ssPropCatUnboundColumns + ssPropCatAppearanceCollection + ssPropCatGeneral
    MostrarObservacionesOferta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntos", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub Form_Resize()

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADJ_ADJUNTOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        lblFicAdjuntos.caption = Ador(0).Value              '1 archivos adjuntos
        Ador.MoveNext
        m_sIdiAmbito = Ador(0).Value   '2 �mbito
'        sdbgComentarios.Bands(0).Columns(0).Header.Caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiElArchivo = Ador.Fields.Item(0).Value   '3 Archivo
        Ador.MoveNext
        m_sIdiComentario = Ador(0).Value  '4 Comentario
        Ador.MoveNext
        m_sIdiProceso = Ador.Fields.Item(0).Value           '5 Proceso
        Ador.MoveNext
        m_sIdiTipoOrig = Ador.Fields.Item(0).Value          '6 Tipo original
        Ador.MoveNext
        m_sIdiGuardar = Ador.Fields.Item(0).Value           '7 Guardar archivo adjunto
        Ador.MoveNext
        m_sIdiObsOferta = Ador(0).Value                     '8 Observaciones
        Ador.MoveNext
        m_sIdiObsAdjuntos = Ador(0).Value                   '9 Adjuntos
        Ador.MoveNext
        m_sIdiOferta = Ador(0).Value                        '10 Oferta
        Ador.MoveNext
        m_sIdiSize = Ador(0).Value  '11 Tama�o
        Ador.MoveNext
        m_skb = Ador(0).Value   '12 KB
        Ador.MoveNext
        m_sIdiComent = Ador(0).Value  '13 Comentarios
        
        Ador.Close
    End If
    Set Ador = Nothing
    Me.caption = g_sCaption
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub Arrange()

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next
    'Redimensiona el formulario
    If Me.Width < 2000 Then Exit Sub
    If Me.Height < 2000 Then Exit Sub
    
    vbalComentarios.Width = Me.Width - 240
    vbalComentarios.ColumnWidth(1) = (vbalComentarios.Width - 240) / 15.3911342894394
    If g_bAdjunVisible = False Then
        vbalComentarios.Height = Me.Height - 650
    Else
        vbalComentarios.Height = Me.Height / 2 - 520
    End If
    
    picAdjuntos.Top = vbalComentarios.Top + vbalComentarios.Height + 60
    picAdjuntos.Height = Me.Height / 2 - 125
    picAdjuntos.Width = Me.Width - 200
    
    sdbgAdjuntos.Width = Me.Width - 240
    sdbgAdjuntos.Height = Me.Height / 2 - 727.5
    
    If sdbgAdjuntos.Bands.Count > 0 Then
        sdbgAdjuntos.Bands(0).Columns("AMBITO").Width = sdbgAdjuntos.Width * (25 / 100)
        sdbgAdjuntos.Bands(0).Columns("NOMBRE").Width = sdbgAdjuntos.Width * (20 / 100)
        sdbgAdjuntos.Bands(0).Columns("SIZE").Width = sdbgAdjuntos.Width * (9.3 / 100)
        sdbgAdjuntos.Bands(0).Columns("COMENT").Width = sdbgAdjuntos.Width * (40.6 / 100)
    End If
    
'    If sdbgComentarios.Bands.Count > 0 Then
'        sdbgComentarios.Bands(0).Columns("AMBITO").Width = sdbgComentarios.Width * (25 / 100)
'        sdbgComentarios.Bands(0).Columns("OBS").Width = sdbgComentarios.Width * (70 / 100)
'    End If
    
    cmdAbrirAdjuntos.Top = sdbgAdjuntos.Top + sdbgAdjuntos.Height + 45
    cmdSalvarAdjuntos.Top = cmdAbrirAdjuntos.Top
    cmdSalvarAdjuntos.Left = picAdjuntos.Width - 940
    cmdAbrirAdjuntos.Left = cmdSalvarAdjuntos.Left + cmdSalvarAdjuntos.Width + 60
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub

Public Sub MostrarAdjuntosOferta()
'**************************************************************************************
'*** Descripci�n: Muestra en el control List View los archivos adjuntos para una    ***
'***              oferta, junto a su �mbito y comentario en caso de que lo tenga.   ***
'***                                                                                ***
'*** Par�metros : ------                                                            ***
'***                                                                                ***
'*** Valor que devuelve: ------                                                     ***
'**************************************************************************************
    Dim oAdjunto As CAdjunto
    Dim sTextoAmbitoItem As String
    Dim Ador As ADODB.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oAdjuntos = oFSGSRaiz.Generar_CAdjuntos
    m_oAdjuntos.CargarTodosLosAdjuntosOferta g_iAnyo, g_sGmn1, g_iProceCod, g_sProveCod, g_iNumOfe, g_lGrupo
    
    Set Ador = New Ador.Recordset

    Ador.Fields.Append "AMBITO", adVarChar, 200
    Ador.Fields.Append "ID", adBigInt
    Ador.Fields.Append "NOMBRE", adVarChar, 200
    Ador.Fields.Append "SIZE", adVarChar, 200
    Ador.Fields.Append "COMENT", adVarChar, 8000
    Ador.Fields.Append "BTN", adVarChar, 10
    
    Ador.Open

    For Each oAdjunto In m_oAdjuntos
        Ador.AddNew
        
        If oAdjunto.PrecioItem Is Nothing And oAdjunto.Grupo Is Nothing Then
            Ador("AMBITO").Value = m_sIdiProceso
        ElseIf oAdjunto.PrecioItem Is Nothing And Not oAdjunto.Grupo Is Nothing Then
            Ador("AMBITO").Value = oAdjunto.Grupo.Codigo & " - " & oAdjunto.Grupo.Den
        ElseIf Not oAdjunto.PrecioItem Is Nothing Then
            sTextoAmbitoItem = oAdjunto.PrecioItem.ArticuloCod & " - " & oAdjunto.PrecioItem.Descr
            If Left$(sTextoAmbitoItem, 3) = " - " Then 'Si el art�culo no es codificado
                sTextoAmbitoItem = Right$(sTextoAmbitoItem, Len(sTextoAmbitoItem) - 3)
            End If
            Ador("AMBITO").Value = sTextoAmbitoItem
        End If
        Ador("ID").Value = oAdjunto.indice
        Ador("NOMBRE").Value = oAdjunto.nombre
        Ador("SIZE").Value = TamanyoAdjuntos(oAdjunto.DataSize / 1024) & " " & m_skb
        Ador("COMENT").Value = NullToStr(oAdjunto.Comentario)
        Ador("BTN").Value = "..."
    Next
    Set oAdjunto = Nothing
    
    Set sdbgAdjuntos.DataSource = Ador
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntos", "MostrarAdjuntosOferta", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DoEvents
    g_sCaption = ""
    g_lGrupo = 0
    g_bAdjunVisible = False
    g_iAnyo = 0
    g_sGmn1 = ""
    g_iProceCod = 0
    g_sProveCod = ""
    g_iNumOfe = 0

    Set m_oAdjuntos = Nothing
    
    DoEvents
    g_oOrigen.PopUpDescargarAdjuntos
    DoEvents
    Set g_oOrigen = Nothing
    
    Me.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntos", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Muestra en el control ultragrid las observaciones de los adjuntos para una oferta, junto a su �mbito y comentario en caso de que lo tenga
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Public Sub MostrarObservacionesOferta()
    Dim sTextoAmbitoItem As String
    Dim oOferta As COferta
    Dim oGrupo As CGrupo
    Dim oLinea As CPrecioItem
    Dim bMostrar As Boolean
    Dim Ador As ADODB.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set Ador = New Ador.Recordset

    Ador.Fields.Append "AMBITO", adVarChar, 350
    Ador.Fields.Append "OBS", adVarChar, 8000

    Ador.Open
    
    '�mbito proceso
    If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(CStr(g_sProveCod)) Is Nothing Then
        Set oOferta = g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(CStr(g_sProveCod))
        If Not IsNull(oOferta.obs) Then
            Ador.AddNew
            Ador("AMBITO").Value = m_sIdiOferta
            Ador("OBS").Value = oOferta.obs
        End If

        If Not IsNull(oOferta.ObsAdjun) Then
            Ador.AddNew
            Ador("AMBITO").Value = m_sIdiObsAdjuntos
            Ador("OBS").Value = oOferta.ObsAdjun
        End If
        Set oOferta = Nothing
    End If

    '�mbito grupo:
    For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.grupos
        bMostrar = True
        If g_oOrigen.sstabComparativa.selectedItem.Tag <> "General" And g_oOrigen.sstabComparativa.selectedItem.Tag <> "ALL" Then
            If g_oOrigen.sstabComparativa.selectedItem.Tag <> oGrupo.Codigo Then
                bMostrar = False
            End If
        End If

        If bMostrar = True Then
          If Not oGrupo.UltimasOfertas.Item(Trim(CStr(g_sProveCod))) Is Nothing Then
            Set oOferta = oGrupo.UltimasOfertas.Item(Trim(CStr(g_sProveCod)))
            
            If Not IsNull(oOferta.ObsAdjun) Then
                Ador.AddNew
                Ador("AMBITO").Value = oGrupo.Codigo & " - " & oGrupo.Den
                Ador("OBS").Value = oOferta.ObsAdjun
            End If

            '�mbito ITEM:
            If Not oOferta.Lineas Is Nothing Then
              For Each oLinea In oOferta.Lineas
                sTextoAmbitoItem = oGrupo.Items.Item(CStr(oLinea.Id)).ArticuloCod & " - " & oGrupo.Items.Item(CStr(oLinea.Id)).Descr
                If Left$(sTextoAmbitoItem, 3) = " - " Then 'Si el art�culo no es codificado
                    sTextoAmbitoItem = Right$(sTextoAmbitoItem, Len(sTextoAmbitoItem) - 3)
                End If

                If Not IsNull(oLinea.ObsAdjun) Then
                    Ador.AddNew
                    Ador("AMBITO").Value = sTextoAmbitoItem
                    Ador("OBS").Value = oLinea.ObsAdjun
                End If

                Select Case oLinea.Usar
                    Case 1
                        If Not IsNull(oLinea.Precio1Obs) And oLinea.Precio1Obs <> "" Then
                            Ador.AddNew
                            Ador("AMBITO").Value = sTextoAmbitoItem
                            Ador("OBS").Value = oLinea.Precio1Obs
                        End If
                    Case 2
                        If Not IsNull(oLinea.Precio2Obs) And oLinea.Precio2Obs <> "" Then
                            Ador.AddNew
                            Ador("AMBITO").Value = sTextoAmbitoItem
                            Ador("OBS").Value = oLinea.Precio2Obs
                        End If
                    Case 3
                        If Not IsNull(oLinea.Precio3Obs) And oLinea.Precio3Obs <> "" Then
                            Ador.AddNew
                            Ador("AMBITO").Value = sTextoAmbitoItem
                            Ador("OBS").Value = oLinea.Precio3Obs
                        End If
                End Select
              Next
            End If

            Set oOferta = Nothing
          End If
        End If
    Next
    
    'Set sdbgComentarios.DataSource = Ador
    vbalComentarios.Redraw = False
    vbalComentarios.BackColor = 12648447
    vbalComentarios.GridLines = True
    vbalComentarios.NoVerticalGridLines = True
    vbalComentarios.AddColumn m_sIdiComentario, , , , (vbalComentarios.Width - 240) / 15.3911342894394
    'vbalComentarios.StretchLastColumnToFit = True
    vbalComentarios.Header = False
    vbalComentarios.DefaultRowHeight = 20
    vbalComentarios.HighlightBackColor = 12648447
    vbalComentarios.HighlightForeColor = vbalComentarios.ForeColor
    'vbalComentarios.ColumnWidth(1) = Me.picAdjuntos.ScaleWidth
    If Ador.RecordCount > 0 Then
        vbalComentarios.Rows = Ador.RecordCount * 2
        Ador.MoveFirst
        Dim i As Integer
        i = 1
        While Not Ador.EOF
                vbalComentarios.CellDetails i, 1, Ador(0).Value, DT_WORDBREAK
                vbalComentarios.Cell(i, 1).ForeColor = vbBlue
                vbalComentarios.CellText(i + 1, 1) = Ador(1).Value
                vbalComentarios.CellTextAlign(i + 1, 1) = DT_WORDBREAK
    '            vbalComentarios.CellDetails i + 1, 1, Trim(Ador(1).Value), DT_WORDBREAK
                vbalComentarios.AutoHeightRow i + 1
                i = i + 2
                Ador.MoveNext
        Wend
        vbalComentarios.Redraw = True
    End If
    Ador.Close
    Set Ador = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntos", "MostrarObservacionesOferta", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub


Private Sub sdbgAdjuntos_ClickCellButton(ByVal Cell As UltraGrid.SSCell)
    'Accedemos a la pantalla de comentarios de la adjudicaci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If NullToStr(Cell.Row.Cells("COMENT").Value) = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    frmAdjComent.g_sOrigen = "frmADJAdjuntos"
    frmAdjComent.g_bRespetarCombo = True
    frmAdjComent.txtComentario.Text = NullToStr(Cell.Row.Cells("COMENT").Value)
    frmAdjComent.g_bRespetarCombo = False
    frmAdjComent.txtComentario.Locked = True
    frmAdjComent.txtComentario.BackColor = RGB(255, 255, 200)
    frmAdjComent.lstvwEsp.Visible = False
    frmAdjComent.picBotones.Visible = False
    frmAdjComent.caption = Me.caption
    
    Screen.MousePointer = vbNormal
    frmAdjComent.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntos", "sdbgAdjuntos_ClickCellButton", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbgAdjuntos_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bAdjunVisible = False Then Exit Sub
    
'    With Layout.Appearances.Add("Gris")
'        .BackColor = RGB(223, 223, 223)
'    End With
    'Para que se parezca a la vbaccelerator Sgrid
    sdbgAdjuntos.Appearance.BackColor = 12648447
    sdbgAdjuntos.Bands(0).Override.CellAppearance.BackColor = 12648447
    sdbgAdjuntos.Bands(0).Override.RowSelectors = UltraGrid.ssRowSelectorsOff
    sdbgAdjuntos.Bands(0).Override.BorderStyleRow = UltraGrid.ssBorderStyleNone
    sdbgAdjuntos.Bands(0).Override.CellAppearance.Font.Name = "Tahoma"
    sdbgAdjuntos.Bands(0).Override.CellAppearance.Font.Size = 8
    sdbgAdjuntos.Bands(0).Override.HeaderAppearance.Font.Size = 8
    sdbgAdjuntos.Bands(0).Override.HeaderAppearance.BackColor = vbWhite
    sdbgAdjuntos.Bands(0).Override.BorderStyleRow = UltraGrid.ssBorderStyleSolidLine
    sdbgAdjuntos.Appearance.BorderColor = &HC0C0C0
    sdbgAdjuntos.Bands(0).Override.TipStyleRowConnector = UltraGrid.ssTipStyleHide
    sdbgAdjuntos.Bands(0).Override.ActiveRowAppearance.BackColor = &H80000002
    sdbgAdjuntos.Bands(0).Override.ActiveRowAppearance.ForeColor = vbWhite
    sdbgAdjuntos.Bands(0).Override.RowSelectorAppearance.BackColor = vbWhite
    sdbgAdjuntos.Bands(0).Columns("AMBITO").Activation = UltraGrid.ssActivationActivateNoEdit
    sdbgAdjuntos.Bands(0).Columns("NOMBRE").Activation = UltraGrid.ssActivationActivateNoEdit
    sdbgAdjuntos.Bands(0).Columns("SIZE").Activation = UltraGrid.ssActivationActivateNoEdit
    sdbgAdjuntos.Bands(0).Columns("COMENT").Activation = UltraGrid.ssActivationActivateNoEdit
    
    sdbgAdjuntos.Bands(0).Columns("ID").Hidden = True
     
   ' sdbgAdjuntos.Bands(0).Columns("AMBITO").CellAppearance = "Gris"
    
    sdbgAdjuntos.Bands(0).Columns("AMBITO").Header.caption = m_sIdiAmbito
    sdbgAdjuntos.Bands(0).Columns("NOMBRE").Header.caption = m_sIdiElArchivo
    sdbgAdjuntos.Bands(0).Columns("SIZE").Header.caption = m_sIdiSize
    sdbgAdjuntos.Bands(0).Columns("COMENT").Header.caption = m_sIdiComent
    sdbgAdjuntos.Bands(0).Columns("BTN").Header.caption = " "
    
    sdbgAdjuntos.Bands(0).Columns("BTN").Style = UltraGrid.ssStyleButton
    sdbgAdjuntos.Bands(0).Columns("BTN").ButtonDisplayStyle = UltraGrid.ssButtonDisplayStyleAlways
    sdbgAdjuntos.Bands(0).Columns("BTN").Width = 260
    
    sdbgAdjuntos.Bands(0).Columns("SIZE").CellAppearance.TextAlign = ssAlignRight
     
    sdbgAdjuntos.Override.SelectTypeRow = ssSelectTypeSingle
    sdbgAdjuntos.Override.SelectTypeCol = ssSelectTypeNone
    
    sdbgAdjuntos.ActiveRow = sdbgAdjuntos.GetRow(ssChildRowFirst)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJAdjuntos", "sdbgAdjuntos_InitializeLayout", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

