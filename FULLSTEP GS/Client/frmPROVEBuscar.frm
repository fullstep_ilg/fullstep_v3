VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPROVEBuscar 
   BackColor       =   &H00808000&
   Caption         =   "Buscar proveedor"
   ClientHeight    =   7695
   ClientLeft      =   4470
   ClientTop       =   3180
   ClientWidth     =   7425
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROVEBuscar.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7695
   ScaleWidth      =   7425
   Begin TabDlg.SSTab sstabProve 
      Height          =   7515
      Left            =   180
      TabIndex        =   0
      Top             =   60
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   13256
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BackColor       =   8421376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos de busqueda"
      TabPicture(0)   =   "frmPROVEBuscar.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "stabGeneral"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Proveedores"
      TabPicture(1)   =   "frmPROVEBuscar.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sdbgProveERP"
      Tab(1).Control(1)=   "chkTrasladarProve"
      Tab(1).Control(2)=   "cmdCancelar"
      Tab(1).Control(3)=   "cmdAceptar"
      Tab(1).Control(4)=   "sdbgProveedores"
      Tab(1).ControlCount=   5
      Begin SSDataWidgets_B.SSDBGrid sdbgProveERP 
         Height          =   975
         Left            =   -74400
         TabIndex        =   95
         Top             =   1680
         Visible         =   0   'False
         Width           =   5790
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   3
         AllowColumnMoving=   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3201
         Columns(0).Caption=   "ERP"
         Columns(0).Name =   "ERP"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1826
         Columns(1).Caption=   "PROVE_ERP"
         Columns(1).Name =   "PROVE_ERP"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   5133
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   10213
         _ExtentY        =   1720
         _StockProps     =   79
         BackColor       =   12632256
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CheckBox chkTrasladarProve 
         Caption         =   "Trasladar proveedor a todas las lineas"
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   -74880
         TabIndex        =   91
         Top             =   480
         Visible         =   0   'False
         Width           =   6000
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cerrar"
         Height          =   345
         Left            =   -71640
         TabIndex        =   1
         Top             =   6300
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Seleccionar"
         Height          =   345
         Left            =   -72825
         TabIndex        =   2
         Top             =   6300
         Visible         =   0   'False
         Width           =   1005
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgProveedores 
         Height          =   5625
         Left            =   -74880
         TabIndex        =   4
         Top             =   600
         Width           =   6660
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   7
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPROVEBuscar.frx":0182
         stylesets(1).Name=   "Selected"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmPROVEBuscar.frx":019E
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         MaxSelectedRows =   0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   1773
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   3969
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Caption=   "DCodigo en ERP"
         Columns(2).Name =   "COD_ERP"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Style=   1
         Columns(3).Width=   1111
         Columns(3).Caption=   "CAL1"
         Columns(3).Name =   "CAL1"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   1111
         Columns(4).Caption=   "CAL2"
         Columns(4).Name =   "CAL2"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   1111
         Columns(5).Caption=   "CAL3"
         Columns(5).Name =   "CAL3"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   3201
         Columns(6).Caption=   "C�d.web"
         Columns(6).Name =   "CodWeb"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         _ExtentX        =   11747
         _ExtentY        =   9922
         _StockProps     =   79
         BackColor       =   12632256
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin TabDlg.SSTab stabGeneral 
         Height          =   7035
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   6810
         _ExtentX        =   12012
         _ExtentY        =   12409
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Filtro de datos generales"
         TabPicture(0)   =   "frmPROVEBuscar.frx":01BA
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "frCalif"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraSelComp"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraMat"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Frame1"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "fraERP"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Filtro avanzado "
         TabPicture(1)   =   "frmPROVEBuscar.frx":01D6
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraAutofactura"
         Tab(1).Control(1)=   "fraBusqAvanzada"
         Tab(1).ControlCount=   2
         Begin VB.Frame fraERP 
            Caption         =   "ERP"
            Height          =   675
            Left            =   120
            TabIndex        =   92
            Top             =   6240
            Visible         =   0   'False
            Width           =   6315
            Begin VB.TextBox txtERP 
               Height          =   285
               Left            =   1470
               MaxLength       =   100
               TabIndex        =   93
               Top             =   240
               Visible         =   0   'False
               Width           =   4375
            End
            Begin VB.Label lblCodERP 
               Caption         =   "C�digo:"
               Height          =   225
               Left            =   120
               TabIndex        =   94
               Top             =   240
               Width           =   1365
            End
         End
         Begin VB.Frame Frame1 
            Caption         =   "Datos generales"
            Height          =   1755
            Left            =   135
            TabIndex        =   78
            Top             =   420
            Width           =   6315
            Begin VB.TextBox txtDen 
               Height          =   285
               Left            =   1440
               MaxLength       =   100
               TabIndex        =   81
               Top             =   600
               Width           =   4755
            End
            Begin VB.TextBox txtCod 
               Height          =   285
               Left            =   4440
               MaxLength       =   20
               TabIndex        =   80
               Top             =   240
               Width           =   1770
            End
            Begin VB.TextBox txtVAT 
               Height          =   285
               Left            =   1440
               MaxLength       =   20
               TabIndex        =   79
               Top             =   240
               Width           =   1770
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
               Height          =   285
               Left            =   1470
               TabIndex        =   82
               Top             =   960
               Width           =   945
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1667
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
               Height          =   285
               Left            =   2430
               TabIndex        =   83
               Top             =   960
               Width           =   3765
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6641
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
               Height          =   285
               Left            =   1470
               TabIndex        =   84
               Top             =   1320
               Width           =   945
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1667
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
               Height          =   285
               Left            =   2430
               TabIndex        =   85
               Top             =   1320
               Width           =   3765
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6641
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label Label1 
               Caption         =   "Den:"
               Height          =   225
               Left            =   120
               TabIndex        =   90
               Top             =   600
               Width           =   1215
            End
            Begin VB.Label Label3 
               Caption         =   "C�digo:"
               Height          =   225
               Left            =   3600
               TabIndex        =   89
               Top             =   300
               Width           =   720
            End
            Begin VB.Label Label6 
               Caption         =   "Provincia:"
               Height          =   225
               Left            =   120
               TabIndex        =   88
               Top             =   1380
               Width           =   1320
            End
            Begin VB.Label Label5 
               Caption         =   "Pa�s:"
               Height          =   225
               Left            =   120
               TabIndex        =   87
               Top             =   1020
               Width           =   1320
            End
            Begin VB.Label Label7 
               Caption         =   "DDUNS/VAT:"
               Height          =   225
               Left            =   120
               TabIndex        =   86
               Top             =   300
               Width           =   1320
            End
         End
         Begin VB.Frame fraMat 
            Caption         =   "Material"
            Height          =   1725
            Left            =   135
            TabIndex        =   64
            Top             =   3690
            Width           =   6315
            Begin VB.CommandButton cmdSelMat 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   5895
               Picture         =   "frmPROVEBuscar.frx":01F2
               Style           =   1  'Graphical
               TabIndex        =   65
               Top             =   180
               Width           =   345
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
               Height          =   285
               Left            =   1470
               TabIndex        =   66
               Top             =   270
               Width           =   945
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   900
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1667
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
               Height          =   285
               Left            =   1470
               TabIndex        =   67
               Top             =   615
               Width           =   945
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   900
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1667
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
               Height          =   285
               Left            =   1470
               TabIndex        =   68
               Top             =   945
               Width           =   945
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   900
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1667
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
               Height          =   285
               Left            =   1470
               TabIndex        =   69
               Top             =   1290
               Width           =   945
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   900
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1667
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
               Height          =   285
               Left            =   2445
               TabIndex        =   70
               Top             =   615
               Width           =   3420
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4498
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1164
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6032
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
               Height          =   285
               Left            =   2445
               TabIndex        =   71
               Top             =   945
               Width           =   3420
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4498
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1164
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6032
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
               Height          =   285
               Left            =   2445
               TabIndex        =   72
               Top             =   1290
               Width           =   3420
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4498
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1164
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6032
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
               Height          =   285
               Left            =   2445
               TabIndex        =   73
               Top             =   270
               Width           =   3420
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   4498
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1164
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6032
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblGMN3_4 
               Caption         =   "Subfam.:"
               Height          =   225
               Left            =   120
               TabIndex        =   77
               Top             =   960
               Width           =   1365
            End
            Begin VB.Label lblGMN2_4 
               Caption         =   "Fam.:"
               Height          =   225
               Left            =   120
               TabIndex        =   76
               Top             =   660
               Width           =   1365
            End
            Begin VB.Label lblGMN1_4 
               Caption         =   "Comm:"
               Height          =   225
               Left            =   120
               TabIndex        =   75
               Top             =   300
               Width           =   1365
            End
            Begin VB.Label lblGMN4_4 
               Caption         =   "Gru.:"
               Height          =   225
               Left            =   120
               TabIndex        =   74
               Top             =   1320
               Width           =   1365
            End
         End
         Begin VB.Frame fraSelComp 
            Caption         =   "Equipo"
            Height          =   675
            Left            =   120
            TabIndex        =   58
            Top             =   5490
            Visible         =   0   'False
            Width           =   6315
            Begin VB.TextBox txtEqp 
               Height          =   285
               Left            =   1470
               Locked          =   -1  'True
               MaxLength       =   100
               TabIndex        =   59
               Top             =   240
               Visible         =   0   'False
               Width           =   4375
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
               Height          =   285
               Left            =   1470
               TabIndex        =   60
               Top             =   240
               Width           =   945
               DataFieldList   =   "Column 0"
               _Version        =   196617
               DataMode        =   2
               GroupHeaders    =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1296
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   5345
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1667
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               DataFieldToDisplay=   "Column 3"
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
               Height          =   285
               Left            =   2445
               TabIndex        =   61
               Top             =   240
               Width           =   3420
               DataFieldList   =   "Column 0"
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1640
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6032
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label Label4 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Label3"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1485
               TabIndex        =   63
               Top             =   240
               Visible         =   0   'False
               Width           =   4380
            End
            Begin VB.Label lblEqp 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Label3"
               Height          =   285
               Left            =   1470
               TabIndex        =   62
               Top             =   240
               Visible         =   0   'False
               Width           =   4065
            End
         End
         Begin VB.Frame frCalif 
            Caption         =   "Calificaciones"
            Height          =   1395
            Left            =   120
            TabIndex        =   42
            Top             =   2220
            Width           =   6315
            Begin VB.TextBox txtCal1 
               Height          =   285
               Left            =   5610
               TabIndex        =   45
               Top             =   240
               Width           =   570
            End
            Begin VB.TextBox txtCal2 
               Height          =   285
               Left            =   5610
               TabIndex        =   44
               Top             =   600
               Width           =   570
            End
            Begin VB.TextBox txtCal3 
               Height          =   285
               Left            =   5610
               TabIndex        =   43
               Top             =   960
               Width           =   570
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcCal1Den 
               Height          =   285
               Left            =   2445
               TabIndex        =   46
               Top             =   240
               Width           =   2835
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   4392
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1376
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   2249
               Columns(2).Caption=   "Valor inferior"
               Columns(2).Name =   "INF"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   2064
               Columns(3).Caption=   "Valor superior"
               Columns(3).Name =   "SUP"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   5001
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcCal1Cod 
               Height          =   285
               Left            =   1470
               TabIndex        =   47
               Top             =   240
               Width           =   960
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   1376
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4392
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   2249
               Columns(2).Caption=   "Valor inferior"
               Columns(2).Name =   "INF"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   2064
               Columns(3).Caption=   "Valor superior"
               Columns(3).Name =   "SUP"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   1693
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcCal2Den 
               Height          =   285
               Left            =   2445
               TabIndex        =   48
               Top             =   600
               Width           =   2835
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   4392
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1376
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   2249
               Columns(2).Caption=   "Valor inferior"
               Columns(2).Name =   "INF"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   2064
               Columns(3).Caption=   "Valor superior"
               Columns(3).Name =   "SUP"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   5001
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcCal2Cod 
               Height          =   285
               Left            =   1470
               TabIndex        =   49
               Top             =   600
               Width           =   960
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   1376
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4392
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   2249
               Columns(2).Caption=   "Valor inferior"
               Columns(2).Name =   "INF"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   2064
               Columns(3).Caption=   "Valor superior"
               Columns(3).Name =   "SUP"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   1693
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcCal3Den 
               Height          =   285
               Left            =   2445
               TabIndex        =   50
               Top             =   945
               Width           =   2835
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   4392
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   1376
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   2249
               Columns(2).Caption=   "Valor inferior"
               Columns(2).Name =   "INF"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   2064
               Columns(3).Caption=   "Valor superior"
               Columns(3).Name =   "SUP"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   5001
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcCal3Cod 
               Height          =   285
               Left            =   1470
               TabIndex        =   51
               Top             =   945
               Width           =   960
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   1376
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4392
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   2249
               Columns(2).Caption=   "Valor inferior"
               Columns(2).Name =   "INF"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   2064
               Columns(3).Caption=   "Valor superior"
               Columns(3).Name =   "SUP"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   1693
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblCalif1 
               Caption         =   "Calif1.:"
               Height          =   225
               Left            =   120
               TabIndex        =   57
               Top             =   300
               Width           =   1350
            End
            Begin VB.Label lblCalif2 
               Caption         =   "Calif2.:"
               Height          =   225
               Left            =   120
               TabIndex        =   56
               Top             =   660
               Width           =   1350
            End
            Begin VB.Label lblCalif3 
               Caption         =   "Calif3.:"
               Height          =   225
               Left            =   120
               TabIndex        =   55
               Top             =   1020
               Width           =   1350
            End
            Begin VB.Label lblSentCal1 
               Caption         =   ">="
               BeginProperty Font 
                  Name            =   "Microsoft Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   5370
               TabIndex        =   54
               Top             =   300
               Width           =   315
            End
            Begin VB.Label lblSentCal3 
               Caption         =   ">="
               BeginProperty Font 
                  Name            =   "Microsoft Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   5370
               TabIndex        =   53
               Top             =   1020
               Width           =   255
            End
            Begin VB.Label lblSentCal2 
               Caption         =   ">="
               BeginProperty Font 
                  Name            =   "Microsoft Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   5370
               TabIndex        =   52
               Top             =   660
               Width           =   315
            End
         End
         Begin VB.Frame fraAutofactura 
            Caption         =   "DAutofacturaci�n"
            Height          =   1275
            Left            =   -74820
            TabIndex        =   38
            Top             =   420
            Width           =   6315
            Begin VB.OptionButton optAutoFactura 
               Caption         =   "DMostrar proveedores que permitan autofacturac�n"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   41
               Top             =   240
               Width           =   5835
            End
            Begin VB.OptionButton optAutoFactura 
               Caption         =   "DMostrar proveedores que no permitan autofacturac�n"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   40
               Top             =   600
               Width           =   5835
            End
            Begin VB.OptionButton optAutoFactura 
               Caption         =   "DMostrar todos los proveedores"
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   39
               Top             =   960
               Value           =   -1  'True
               Width           =   5955
            End
         End
         Begin VB.Frame fraBusqAvanzada 
            Caption         =   "DProveedores relacionados"
            Height          =   3975
            Left            =   -74820
            TabIndex        =   20
            Top             =   1800
            Width           =   6315
            Begin VB.OptionButton optProveedores 
               Caption         =   "DMostrar todos los proveedores"
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   37
               Top             =   3600
               Value           =   -1  'True
               Width           =   3105
            End
            Begin VB.OptionButton optProveedores 
               Caption         =   "DMostrar proveedores subordinados"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   36
               Top             =   240
               Width           =   2985
            End
            Begin VB.OptionButton optProveedores 
               Caption         =   "DMostrar proveedores principales"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   35
               Top             =   1710
               Width           =   2865
            End
            Begin VB.Frame fraProveSub 
               BorderStyle     =   0  'None
               Caption         =   "Frame2"
               Height          =   1215
               Left            =   360
               TabIndex        =   29
               Top             =   480
               Width           =   5655
               Begin SSDataWidgets_B.SSDBCombo sdbcTipoRelacSub 
                  Height          =   285
                  Left            =   1560
                  TabIndex        =   30
                  Top             =   120
                  Width           =   3765
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   3201
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   7038
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "ID"
                  Columns(2).Name =   "ID"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   6641
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcProveSubCod 
                  Height          =   285
                  Left            =   1560
                  TabIndex        =   31
                  Top             =   840
                  Width           =   1020
                  ScrollBars      =   2
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "C�d."
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1799
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcProveSubDen 
                  Height          =   285
                  Left            =   2595
                  TabIndex        =   32
                  Top             =   840
                  Width           =   2730
                  ScrollBars      =   2
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   6165
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2117
                  Columns(1).Caption=   "C�d."
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4815
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblProveRelac 
                  Caption         =   "DTipo de relaci�n:"
                  Height          =   225
                  Index           =   0
                  Left            =   0
                  TabIndex        =   34
                  Top             =   120
                  Width           =   1320
               End
               Begin VB.Label lblProveRelac 
                  Caption         =   "DMostrar proveedores subordinados al siguiente proveedor:"
                  Height          =   225
                  Index           =   1
                  Left            =   0
                  TabIndex        =   33
                  Top             =   540
                  Width           =   4560
               End
            End
            Begin VB.Frame fraProvePri 
               BorderStyle     =   0  'None
               Caption         =   "Frame2"
               Height          =   1695
               Left            =   360
               TabIndex        =   21
               Top             =   1920
               Width           =   5775
               Begin VB.OptionButton optTRel 
                  Caption         =   "DCon tipo de relaci�n"
                  Height          =   255
                  Index           =   0
                  Left            =   120
                  TabIndex        =   23
                  Top             =   120
                  Value           =   -1  'True
                  Width           =   2595
               End
               Begin VB.OptionButton optTRel 
                  Caption         =   "DSin tipo de relaci�n"
                  Height          =   255
                  Index           =   1
                  Left            =   3120
                  TabIndex        =   22
                  Top             =   120
                  Width           =   2595
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcTipoRelacPri 
                  Height          =   285
                  Left            =   1560
                  TabIndex        =   24
                  Top             =   480
                  Width           =   3765
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   3201
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   7038
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "ID"
                  Columns(2).Name =   "ID"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   6641
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcProvePriCod 
                  Height          =   285
                  Left            =   1560
                  TabIndex        =   25
                  Top             =   1320
                  Width           =   1020
                  ScrollBars      =   2
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "C�d."
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1799
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcProvePriDen 
                  Height          =   285
                  Left            =   2595
                  TabIndex        =   26
                  Top             =   1320
                  Width           =   2730
                  ScrollBars      =   2
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   6165
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2117
                  Columns(1).Caption=   "C�d."
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4815
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblProveRelac 
                  Caption         =   "DMostrar proveedores que tienen al siguiente proveedor como subordinado:"
                  Height          =   225
                  Index           =   3
                  Left            =   0
                  TabIndex        =   28
                  Top             =   960
                  Width           =   5640
               End
               Begin VB.Label lblProveRelac 
                  Caption         =   "DTipo de relaci�n:"
                  Height          =   225
                  Index           =   2
                  Left            =   0
                  TabIndex        =   27
                  Top             =   480
                  Width           =   1320
               End
            End
         End
         Begin VB.PictureBox picEdit 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   -74985
            ScaleHeight     =   345
            ScaleWidth      =   6435
            TabIndex        =   17
            Top             =   4920
            Visible         =   0   'False
            Width           =   6435
            Begin VB.CommandButton Command3 
               Caption         =   "&Aceptar"
               Height          =   345
               Left            =   2205
               TabIndex        =   19
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton Command2 
               Caption         =   "Cancelar"
               Height          =   345
               Left            =   3420
               TabIndex        =   18
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picNavigate 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   -74865
            ScaleHeight     =   375
            ScaleWidth      =   6450
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   4920
            Width           =   6450
            Begin VB.CommandButton cmdRestaurar 
               Caption         =   "&Restaurar"
               Height          =   345
               Left            =   1125
               TabIndex        =   16
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdModificar 
               Caption         =   "&Modificar"
               Height          =   345
               Left            =   30
               TabIndex        =   15
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdListado 
               Caption         =   "&Listado"
               Height          =   345
               Left            =   2220
               TabIndex        =   14
               Top             =   0
               Visible         =   0   'False
               Width           =   1005
            End
         End
         Begin VB.Frame fraselProve 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   750
            Left            =   -74865
            TabIndex        =   6
            Top             =   405
            Width           =   6450
            Begin VB.CommandButton cmdBuscar 
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5985
               Picture         =   "frmPROVEBuscar.frx":025E
               Style           =   1  'Graphical
               TabIndex        =   7
               TabStop         =   0   'False
               Top             =   270
               Width           =   315
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
               Height          =   285
               Left            =   2160
               TabIndex        =   8
               Top             =   270
               Width           =   3795
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6694
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
               Height          =   285
               Left            =   945
               TabIndex        =   9
               Top             =   270
               Width           =   1215
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2143
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblProveCod 
               Caption         =   "C�digo:"
               Height          =   195
               Left            =   150
               TabIndex        =   10
               Top             =   300
               Width           =   720
            End
         End
         Begin MSComctlLib.ListView lstEqp 
            Height          =   3615
            Left            =   -74865
            TabIndex        =   11
            Top             =   1215
            Width           =   6450
            _ExtentX        =   11377
            _ExtentY        =   6376
            View            =   2
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            _Version        =   393217
            Icons           =   "ImageList1"
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "COD"
               Object.Tag             =   "COD"
               Text            =   "Cod"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "DEN"
               Object.Tag             =   "DEN"
               Text            =   "Denominaci�n"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComctlLib.ListView lstEqpMod 
            Height          =   3615
            Left            =   -74865
            TabIndex        =   12
            Top             =   1215
            Width           =   6450
            _ExtentX        =   11377
            _ExtentY        =   6376
            View            =   2
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            Checkboxes      =   -1  'True
            _Version        =   393217
            Icons           =   "ImageList1"
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "COD"
               Object.Tag             =   "COD"
               Text            =   "Cod"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "DEN"
               Object.Tag             =   "DEN"
               Text            =   "Denominaci�n"
               Object.Width           =   2540
            EndProperty
         End
      End
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   495
      Left            =   2760
      TabIndex        =   3
      Top             =   3000
      Width           =   1215
   End
End
Attribute VB_Name = "frmPROVEBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public oProveEncontrados As CProveedores
Public g_oProcesoSeleccionado As CProceso
Private DatosProve As TipoDatosProveedores

' Variables de restricciones
Public bRMat As Boolean
Public bREqp As Boolean

' Variables de idioma
Private sIdioma() As String

' Variables necesarias para carga con restricciones
Private oIProves As IProveedoresAsigAEquipo

' Variable para la combo de equipos
Private oEquipos As CEquipos

'Variables para los codigos de material
Public CodGMN1 As Variant
Public CodGMN2 As Variant
Public CodGMN3 As Variant
Public codGMN4 As Variant
Public codEqp As Variant

'Variables para las calificaciones
Private Calif1 As String
Private Calif2 As String
Private Calif3 As String

'Variables para contener las coleccciones de las combos
Private oPaises As CPaises
Private oPaisSeleccionado As CPais
Private oCalificaciones1 As CCalificaciones
Private oCalificaciones2 As CCalificaciones
Private oCalificaciones3 As CCalificaciones
Private oCalif1Seleccionada As CCalificacion
Private oCalif2Seleccionada As CCalificacion
Private oCalif3Seleccionada As CCalificacion

'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4

' Variables para el manejo de combos
Private PaiRespetarCombo As Boolean
Private PaiCargarComboDesde As Boolean
Private ProviRespetarCombo As Boolean
Private ProviCargarComboDesde As Boolean
Private Cal1RespetarCombo As Boolean
Private Cal1CargarComboDesde As Boolean
Private Cal2RespetarCombo As Boolean
Private Cal2CargarComboDesde As Boolean
Private Cal3RespetarCombo As Boolean
Private Cal3CargarComboDesde As Boolean
Private GMN1CargarComboDesde As Boolean
Private GMN1RespetarCombo As Boolean
Private GMN2CargarComboDesde As Boolean
Private GMN2RespetarCombo As Boolean
Private GMN3CargarComboDesde As Boolean
Private GMN3RespetarCombo As Boolean
Private GMN4CargarComboDesde As Boolean
Private GMN4RespetarCombo As Boolean
Private EqpCargarComboDesde As Boolean
Private EqpRespetarCombo As Boolean


'Variable para saber el origen
Public sOrigen As String

'Variables Combos Proveedores relacionados
Private bRespetarComboProveSub As Boolean
Private bRespetarComboProvePri As Boolean
Private bCargarComboSubDesde As Boolean
Private bCargarComboPriDesde As Boolean

Private oTiposRelac As CRelacTipos

Private m_oProveRelac As CProveedores

'Literales del recuadro de b�squeda avanzada para proveedores relacionados
Private sLiteralBusqAvanzada As String
Private sLiteralOcultar As String
Private sCodProveSelecc As String

Private sLiteralTipoRel As String
Private iEmpresa As Integer

Private Declare Function SetCursorPos Lib "user32" (ByVal X As Long, ByVal Y As Long) As Long

''' <summary>
''' Activacion del formulario. Redimensiona.
''' </summary>
''' <remarks>Llamada desde: Evento del formulario Tiempo m�ximo: 0,1</remarks>
Private Sub Form_Activate()
    Me.Visible = True

    Me.Height = 8150
    
    If (fraSelComp.Visible = False) Then
        Me.Height = Me.Height - 420
    End If
    If (fraERP.Visible = False) Then
        Me.Height = Me.Height - 520
    End If
End Sub

Private Sub Form_Resize()
    
Arrange
End Sub



''' <summary>Evento de selecci�n de una opci�n de b�squeda de proveedores relacionados</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub optProveedores_Click(Index As Integer)
    If Index = 0 Then
        fraProveSub.Enabled = True
        fraProvePri.Enabled = False
        sdbcTipoRelacSub.Enabled = True
        sdbcProveSubCod.Enabled = True
        sdbcProveSubDen.Enabled = True
        optTRel(0).Enabled = False
        optTRel(1).Enabled = False
        sdbcTipoRelacPri.Enabled = False
        sdbcProvePriCod.Enabled = False
        sdbcProvePriDen.Enabled = False
        
    ElseIf Index = 1 Then
        fraProveSub.Enabled = False
        fraProvePri.Enabled = True
        sdbcTipoRelacSub.Enabled = False
        sdbcProveSubCod.Enabled = False
        sdbcProveSubDen.Enabled = False
        optTRel(0).Enabled = True
        optTRel(1).Enabled = True
        sdbcTipoRelacPri.Enabled = True
        sdbcProvePriCod.Enabled = True
        sdbcProvePriDen.Enabled = True
    ElseIf Index = 2 Then
        fraProveSub.Enabled = False
        fraProvePri.Enabled = False
        sdbcTipoRelacSub.Enabled = False
        sdbcProveSubCod.Enabled = False
        sdbcProveSubDen.Enabled = False
        optTRel(0).Enabled = False
        optTRel(1).Enabled = False
        sdbcTipoRelacPri.Enabled = False
        sdbcProvePriCod.Enabled = False
        sdbcProvePriDen.Enabled = False
    End If
    
End Sub

Private Sub sdbcCal1Cod_Click()
    If Not sdbcCal1Cod.DroppedDown Then
        sdbcCal1Cod = ""
        sdbcCal1Den = ""
    End If
End Sub

Private Sub sdbcCal1Cod_CloseUp()

    If sdbcCal1Cod.Value = "..." Then
        sdbcCal1Cod.Text = ""
        Exit Sub
    End If
    
    Cal1RespetarCombo = True
    sdbcCal1Den.Text = sdbcCal1Cod.Columns(1).Text
    sdbcCal1Cod.Text = sdbcCal1Cod.Columns(0).Text
    Cal1RespetarCombo = False
    
    Calif1Seleccionada
    
    If Not oCalif1Seleccionada Is Nothing Then
        
        If gParametrosGenerales.gbSENT_ORD_CAL1 Then  'Orden descendente
          
            If txtCal1 = "" Then
                txtCal1 = oCalif1Seleccionada.ValorSup
            Else
                If val(txtCal1) < oCalif1Seleccionada.ValorInf Or val(txtCal1) > oCalif1Seleccionada.ValorSup Then
                    txtCal1 = oCalif1Seleccionada.ValorSup
                End If
            End If
      
        Else  'Orden ascendente
      
            If txtCal1 = "" Then
                txtCal1 = oCalif1Seleccionada.ValorInf
            Else
                If val(txtCal1) < oCalif1Seleccionada.ValorInf Or val(txtCal1) > oCalif1Seleccionada.ValorSup Then
                    txtCal1 = oCalif1Seleccionada.ValorInf
                End If
            End If
        
        End If
        
    End If
    
    Cal1CargarComboDesde = False
    
End Sub

Private Sub sdbcCal1Cod_DropDown()

    Dim oCal As CCalificacion
    Dim oCalificaciones1 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalificaciones1 = oFSGSRaiz.generar_CCalificaciones
   
    sdbcCal1Cod.RemoveAll

    If Cal1CargarComboDesde Then
        oCalificaciones1.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 1, Trim(sdbcCal1Cod.Text), , , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalificaciones1.CargarTodasLasCalificaciones 1, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalificaciones1
        
        sdbcCal1Cod.AddItem oCal.Cod & Chr(m_lSeparador) & oCal.Den & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
        
    Next

    If Cal1CargarComboDesde And Not oCalificaciones1.EOF Then
        sdbcCal1Cod.AddItem "..."
    End If

    Set oCalificaciones1 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal1Cod_InitColumnProps()
    sdbcCal1Cod.DataFieldList = "Column 0"
    sdbcCal1Cod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcCal1Den_Click()
    If Not sdbcCal1Den.DroppedDown Then
        sdbcCal1Cod = ""
        sdbcCal1Den = ""
    End If
End Sub

Private Sub sdbcCal1Den_CloseUp()

    If sdbcCal1Den.Value = "..." Then
        sdbcCal1Den.Text = ""
        Exit Sub
    End If
    
    Cal1RespetarCombo = True
    sdbcCal1Cod.Text = sdbcCal1Den.Columns(1).Text
    sdbcCal1Den.Text = sdbcCal1Den.Columns(0).Text
    Cal1RespetarCombo = False
    
    Calif1Seleccionada
    
    If oCalif1Seleccionada Is Nothing Then Exit Sub
    
    If gParametrosGenerales.gbSENT_ORD_CAL1 Then  'Orden descendente
        If txtCal1 = "" Then
            txtCal1 = oCalif1Seleccionada.ValorSup
        Else
            If val(txtCal1) < oCalif1Seleccionada.ValorInf Or val(txtCal1) > oCalif1Seleccionada.ValorSup Then
                txtCal1 = oCalif1Seleccionada.ValorSup
            End If
        End If
        
    Else   'Orden ascendente
        If txtCal1 = "" Then
            txtCal1 = oCalif1Seleccionada.ValorInf
        Else
            If val(txtCal1) < oCalif1Seleccionada.ValorInf Or val(txtCal1) > oCalif1Seleccionada.ValorSup Then
                txtCal1 = oCalif1Seleccionada.ValorInf
            End If
        End If
    End If
    
    Cal1CargarComboDesde = False
        
End Sub

Private Sub sdbcCal1Den_DropDown()

    Dim oCal As CCalificacion
    Dim oCalificaciones1 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalificaciones1 = oFSGSRaiz.generar_CCalificaciones
    
    sdbcCal1Den.RemoveAll

    If Cal1CargarComboDesde Then
        oCalificaciones1.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 1, , Trim(sdbcCal1Den.Text), , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalificaciones1.CargarTodasLasCalificaciones 1, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalificaciones1
        
        sdbcCal1Den.AddItem oCal.Den & Chr(m_lSeparador) & oCal.Cod & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
        
    Next

    If Cal1CargarComboDesde And Not oCalificaciones1.EOF Then
        sdbcCal1Den.AddItem "..."
    End If

    Set oCalificaciones1 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal1Den_InitColumnProps()
    sdbcCal1Den.DataFieldList = "Column 0"
    sdbcCal1Den.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCal1Cod_Change()
    
    If Not Cal1RespetarCombo Then
    
        Cal1RespetarCombo = True
        sdbcCal1Den.Text = ""
        Set oCalif1Seleccionada = Nothing
        
        Cal1RespetarCombo = False
        Cal1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcCal1Cod_PositionList(ByVal Text As String)
PositionList sdbcCal1Cod, Text
End Sub

Private Sub sdbcCal1Cod_Validate(Cancel As Boolean)

    Dim oCalificaciones1 As CCalificaciones
    Dim bExiste As Boolean
    
    Set oCalificaciones1 = oFSGSRaiz.generar_CCalificaciones
    
    If sdbcCal1Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    oCalificaciones1.CargarTodasLasCalificaciones 1, sdbcCal1Cod.Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCalificaciones1.Count = 0)
    
    If Not bExiste Then
        sdbcCal1Cod.Text = ""
        oMensajes.NoValido sIdioma(1)
    Else
        Cal1RespetarCombo = True
        sdbcCal1Den.Text = oCalificaciones1.Item(1).Den
        
        sdbcCal1Cod.Columns(0).Value = sdbcCal1Cod.Text
        sdbcCal1Cod.Columns(1).Value = sdbcCal1Den.Text
        
        Set oCalif1Seleccionada = oCalificaciones1.Item(1)
        If txtCal1 = "" Then
            txtCal1 = oCalif1Seleccionada.ValorInf
        Else
            If val(txtCal1) < oCalif1Seleccionada.ValorInf Or val(txtCal1) > oCalif1Seleccionada.ValorSup Then
                txtCal1 = oCalif1Seleccionada.ValorInf
            End If
        End If
        Cal1RespetarCombo = False
        Cal1CargarComboDesde = False
    End If
    
    Set oCalificaciones1 = Nothing

End Sub

Private Sub sdbcCal1Den_Change()
    
    If Not Cal1RespetarCombo Then
    
        Cal1RespetarCombo = True
        sdbcCal1Cod.Text = ""
        Set oCalif1Seleccionada = Nothing
        Cal1RespetarCombo = False
        Cal1CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCal1Den_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyDelete Then
        sdbcCal1Cod.DroppedDown = False
        sdbcCal1Cod.Text = ""
        sdbcCal1Cod.RemoveAll
        sdbcCal1Den.DroppedDown = False
        sdbcCal1Den.Text = ""
        sdbcCal1Den.RemoveAll
        Set oCalif1Seleccionada = Nothing
    End If
    
End Sub

Private Sub sdbcCal1Den_PositionList(ByVal Text As String)
PositionList sdbcCal1Den, Text
End Sub

Private Sub sdbcCal1Den_Validate(Cancel As Boolean)

    Dim oCalificaciones1 As CCalificaciones
    
    Dim bExiste As Boolean
    
    Set oCalificaciones1 = oFSGSRaiz.generar_CCalificaciones
    
    If sdbcCal1Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    oCalificaciones1.CargarTodasLasCalificaciones 1, , sdbcCal1Den.Text, True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCalificaciones1.Count = 0)
    
    If Not bExiste Then
        sdbcCal1Den.Text = ""
        oMensajes.NoValido sIdioma(1)
    Else
        Cal1RespetarCombo = True
        sdbcCal1Cod.Text = oCalificaciones1.Item(1).Cod
        
        sdbcCal1Den.Columns(0).Value = sdbcCal1Den.Text
        sdbcCal1Den.Columns(1).Value = sdbcCal1Cod.Text
        
        Set oCalif1Seleccionada = oCalificaciones1.Item(1)
        If txtCal1 = "" Then
            txtCal1 = oCalif1Seleccionada.ValorInf
        Else
            If val(txtCal1) < oCalif1Seleccionada.ValorInf Or val(txtCal1) > oCalif1Seleccionada.ValorSup Then
                txtCal1 = oCalif1Seleccionada.ValorInf
            End If
        End If
        Cal1RespetarCombo = False
        Cal1CargarComboDesde = False
    End If
    
    Set oCalificaciones1 = Nothing

End Sub

Private Sub sdbcCal2Cod_Change()
    
    If Not Cal2RespetarCombo Then
    
        Cal2RespetarCombo = True
        sdbcCal2Den.Text = ""
        Set oCalif2Seleccionada = Nothing
        
        Cal2RespetarCombo = False
        Cal2CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcCal2Cod_Click()
    
    If Not sdbcCal2Cod.DroppedDown Then
        sdbcCal2Cod = ""
    End If
    
End Sub

Private Sub sdbcCal2Cod_PositionList(ByVal Text As String)
PositionList sdbcCal2Cod, Text
End Sub

Private Sub sdbcCal2Cod_Validate(Cancel As Boolean)

    Dim oCalificaciones2 As CCalificaciones
    
    Dim bExiste As Boolean
    
    Set oCalificaciones2 = oFSGSRaiz.generar_CCalificaciones
    
    If sdbcCal2Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    oCalificaciones2.CargarTodasLasCalificaciones 2, sdbcCal2Cod.Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCalificaciones2.Count = 0)
    
    If Not bExiste Then
        sdbcCal2Cod.Text = ""
        oMensajes.NoValido sIdioma(1)
    Else
        Cal2RespetarCombo = True
        sdbcCal2Den.Text = oCalificaciones2.Item(1).Den
        
        sdbcCal2Cod.Columns(0).Value = sdbcCal2Cod.Text
        sdbcCal2Cod.Columns(1).Value = sdbcCal2Den.Text
        
        Set oCalif2Seleccionada = oCalificaciones2.Item(1)
        If txtCal2 = "" Then
            txtCal2 = oCalif2Seleccionada.ValorInf
        Else
            If val(txtCal2) < oCalif2Seleccionada.ValorInf Or val(txtCal2) > oCalif2Seleccionada.ValorSup Then
                txtCal2 = oCalif2Seleccionada.ValorInf
            End If
        End If
        Cal2RespetarCombo = False
        Cal2CargarComboDesde = False
    End If
    
    Set oCalificaciones2 = Nothing

End Sub

Private Sub sdbcCal2Den_Change()
    
    If Not Cal2RespetarCombo Then
    
        Cal2RespetarCombo = True
        sdbcCal2Cod.Text = ""
        Set oCalif2Seleccionada = Nothing
        Cal2RespetarCombo = False
        Cal2CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCal2Den_Click()
    If Not sdbcCal2Den.DroppedDown Then
        sdbcCal2Cod = ""
        sdbcCal2Den = ""
    End If
End Sub

Private Sub sdbcCal2Den_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyDelete Then
        sdbcCal2Cod.DroppedDown = False
        sdbcCal2Cod.Text = ""
        sdbcCal2Cod.RemoveAll
        sdbcCal2Den.DroppedDown = False
        sdbcCal2Den.Text = ""
        sdbcCal2Den.RemoveAll
        Set oCalif2Seleccionada = Nothing
    End If
    
End Sub

Private Sub sdbcCal2Den_PositionList(ByVal Text As String)
PositionList sdbcCal2Den, Text
End Sub

Private Sub sdbcCal2Den_Validate(Cancel As Boolean)

    Dim oCalificaciones2 As CCalificaciones
    
    Dim bExiste As Boolean
    
    Set oCalificaciones2 = oFSGSRaiz.generar_CCalificaciones
    
    If sdbcCal2Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    oCalificaciones2.CargarTodasLasCalificaciones 2, , sdbcCal2Den.Text, True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCalificaciones2.Count = 0)
    
    If Not bExiste Then
        sdbcCal2Den.Text = ""
        oMensajes.NoValido sIdioma(1)
    Else
        Cal2RespetarCombo = True
        sdbcCal2Cod.Text = oCalificaciones2.Item(1).Cod
        
        sdbcCal2Den.Columns(0).Value = sdbcCal2Den.Text
        sdbcCal2Den.Columns(1).Value = sdbcCal2Cod.Text
        
        Set oCalif2Seleccionada = oCalificaciones2.Item(1)
        If txtCal2 = "" Then
            txtCal2 = oCalif2Seleccionada.ValorInf
        Else
            If val(txtCal2) < oCalif2Seleccionada.ValorInf Or val(txtCal2) > oCalif2Seleccionada.ValorSup Then
                txtCal2 = oCalif2Seleccionada.ValorInf
            End If
        End If
        Cal2RespetarCombo = False
        Cal2CargarComboDesde = False
    End If
    
    Set oCalificaciones2 = Nothing

End Sub
Private Sub sdbcCal3Cod_Change()
    
    If Not Cal3RespetarCombo Then
    
        Cal3RespetarCombo = True
        sdbcCal3Den.Text = ""
        Set oCalif3Seleccionada = Nothing
        Cal3RespetarCombo = False
        Cal3CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcCal3Cod_PositionList(ByVal Text As String)
PositionList sdbcCal3Cod, Text
End Sub

Private Sub sdbcCal3Cod_Validate(Cancel As Boolean)

    Dim oCalificaciones3 As CCalificaciones
    
    Dim bExiste As Boolean
    
    Set oCalificaciones3 = oFSGSRaiz.generar_CCalificaciones
    
    If sdbcCal3Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
       
    Screen.MousePointer = vbHourglass
    oCalificaciones3.CargarTodasLasCalificaciones 3, sdbcCal3Cod.Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCalificaciones3.Count = 0)
    
    If Not bExiste Then
        sdbcCal3Cod.Text = ""
        oMensajes.NoValido sIdioma(1)
    Else
        Cal3RespetarCombo = True
        sdbcCal3Den.Text = oCalificaciones3.Item(1).Den
        
        sdbcCal3Cod.Columns(0).Value = sdbcCal3Cod.Text
        sdbcCal3Cod.Columns(1).Value = sdbcCal3Den.Text
        
        Set oCalif3Seleccionada = oCalificaciones3.Item(1)
        If txtCal3 = "" Then
            txtCal3 = oCalif3Seleccionada.ValorInf
        Else
            If val(txtCal3) < oCalif3Seleccionada.ValorInf Or val(txtCal3) > oCalif3Seleccionada.ValorSup Then
                txtCal3 = oCalif3Seleccionada.ValorInf
            End If
        End If
        Cal3RespetarCombo = False
        Cal3CargarComboDesde = False
    End If
    
    Set oCalificaciones3 = Nothing

End Sub

Private Sub sdbcCal3Den_Change()
    
    If Not Cal3RespetarCombo Then
    
        Cal3RespetarCombo = True
        sdbcCal3Cod.Text = ""
        Set oCalif3Seleccionada = Nothing
        Cal3RespetarCombo = False
        Cal3CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCal3Den_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyDelete Then
        sdbcCal3Cod.DroppedDown = False
        sdbcCal3Cod.Text = ""
        sdbcCal3Cod.RemoveAll
        sdbcCal3Den.DroppedDown = False
        sdbcCal3Den.Text = ""
        sdbcCal3Den.RemoveAll
        Set oCalif3Seleccionada = Nothing
    End If
    
End Sub

Private Sub sdbcCal3Den_PositionList(ByVal Text As String)
PositionList sdbcCal3Den, Text
End Sub

Private Sub sdbcCal3Den_Validate(Cancel As Boolean)

    Dim oCalificaciones3 As CCalificaciones
    
    Dim bExiste As Boolean
    
    Set oCalificaciones3 = oFSGSRaiz.generar_CCalificaciones
    
    If sdbcCal3Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    oCalificaciones3.CargarTodasLasCalificaciones 3, , sdbcCal3Den.Text, True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCalificaciones3.Count = 0)
    
    If Not bExiste Then
        sdbcCal3Den.Text = ""
        oMensajes.NoValido sIdioma(1)
    Else
        Cal3RespetarCombo = True
        sdbcCal3Cod.Text = oCalificaciones3.Item(1).Cod
        
        sdbcCal3Den.Columns(0).Value = sdbcCal3Den.Text
        sdbcCal3Den.Columns(1).Value = sdbcCal3Cod.Text
        
        Set oCalif3Seleccionada = oCalificaciones3.Item(1)
        If txtCal3 = "" Then
            txtCal3 = oCalif3Seleccionada.ValorInf
        Else
            If val(txtCal3) < oCalif3Seleccionada.ValorInf Or val(txtCal3) > oCalif3Seleccionada.ValorSup Then
                txtCal3 = oCalif3Seleccionada.ValorInf
            End If
        End If
        Cal3RespetarCombo = False
        Cal3CargarComboDesde = False
    End If
    
    Set oCalificaciones3 = Nothing

End Sub
Private Sub sdbcCal3Cod_CloseUp()

    If sdbcCal3Cod.Value = "..." Then
        sdbcCal3Cod.Text = ""
        Exit Sub
    End If
    
    Cal3RespetarCombo = True
    sdbcCal3Den.Text = sdbcCal3Cod.Columns(1).Text
    sdbcCal3Cod.Text = sdbcCal3Cod.Columns(0).Text
    Cal3RespetarCombo = False
    
    Calif3Seleccionada
    
    If oCalif3Seleccionada Is Nothing Then Exit Sub
    
    If gParametrosGenerales.gbSENT_ORD_CAL3 Then  'Orden descendente
        If txtCal3 = "" Then
            txtCal3 = oCalif3Seleccionada.ValorSup
        Else
            If val(txtCal3) < oCalif3Seleccionada.ValorInf Or val(txtCal3) > oCalif3Seleccionada.ValorSup Then
                txtCal3 = oCalif3Seleccionada.ValorSup
            End If
        End If
        
    Else  'Orden ascendente
        If txtCal3 = "" Then
            txtCal3 = oCalif3Seleccionada.ValorInf
        Else
            If val(txtCal3) < oCalif3Seleccionada.ValorInf Or val(txtCal3) > oCalif3Seleccionada.ValorSup Then
                txtCal3 = oCalif3Seleccionada.ValorInf
            End If
        End If
    End If
    
    
    Cal3CargarComboDesde = False
    
End Sub

Private Sub sdbcCal3Cod_DropDown()

    Dim oCal As CCalificacion
    Dim oCalificaciones3 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalificaciones3 = oFSGSRaiz.generar_CCalificaciones
    
    sdbcCal3Cod.RemoveAll

    If Cal3CargarComboDesde Then
        oCalificaciones3.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 3, Trim(sdbcCal3Cod.Text), , , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalificaciones3.CargarTodasLasCalificaciones 3, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalificaciones3
        
        sdbcCal3Cod.AddItem oCal.Cod & Chr(m_lSeparador) & oCal.Den & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
        
    Next

    If Cal3CargarComboDesde And Not oCalificaciones3.EOF Then
        sdbcCal3Cod.AddItem "..."
    End If

    Set oCalificaciones3 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal3Cod_InitColumnProps()
    sdbcCal3Cod.DataFieldList = "Column 0"
    sdbcCal3Cod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcCal3Den_CloseUp()

    If sdbcCal3Den.Value = "..." Then
        sdbcCal3Den.Text = ""
        Exit Sub
    End If
    
    Cal3RespetarCombo = True
    sdbcCal3Cod.Text = sdbcCal3Den.Columns(1).Text
    sdbcCal3Den.Text = sdbcCal3Den.Columns(0).Text
    Cal3RespetarCombo = False
    
    Calif3Seleccionada
    
    If oCalif3Seleccionada Is Nothing Then Exit Sub
    
    If gParametrosGenerales.gbSENT_ORD_CAL3 Then  'Orden descendente
        If txtCal3 = "" Then
            txtCal3 = oCalif3Seleccionada.ValorSup
        Else
            If val(txtCal3) < oCalif3Seleccionada.ValorInf Or val(txtCal3) > oCalif3Seleccionada.ValorSup Then
                txtCal3 = oCalif3Seleccionada.ValorSup
            End If
        End If
        
    Else  'Orden ascendente
        If txtCal3 = "" Then
            txtCal3 = oCalif3Seleccionada.ValorInf
        Else
            If val(txtCal3) < oCalif3Seleccionada.ValorInf Or val(txtCal3) > oCalif3Seleccionada.ValorSup Then
                txtCal3 = oCalif3Seleccionada.ValorInf
            End If
        End If
    End If

    
    Cal3CargarComboDesde = False
        
End Sub

Private Sub sdbcCal3Den_DropDown()

    Dim oCal As CCalificacion
    Dim oCalificaciones3 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalificaciones3 = oFSGSRaiz.generar_CCalificaciones
   
    sdbcCal3Den.RemoveAll

    If Cal3CargarComboDesde Then
        oCalificaciones3.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 3, , Trim(sdbcCal3Den.Text), , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalificaciones3.CargarTodasLasCalificaciones 3, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalificaciones3
        
        sdbcCal3Den.AddItem oCal.Den & Chr(m_lSeparador) & oCal.Cod & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
        
    Next

    If Cal3CargarComboDesde And Not oCalificaciones3.EOF Then
        sdbcCal3Den.AddItem "..."
    End If

    Set oCalificaciones3 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal3Den_InitColumnProps()
    sdbcCal3Den.DataFieldList = "Column 0"
    sdbcCal3Den.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcCal2Cod_CloseUp()

    If sdbcCal2Cod.Value = "..." Then
        sdbcCal2Cod.Text = ""
        Exit Sub
    End If
    
    Cal2RespetarCombo = True
    sdbcCal2Den.Text = sdbcCal2Cod.Columns(1).Text
    sdbcCal2Cod.Text = sdbcCal2Cod.Columns(0).Text
    Cal2RespetarCombo = False
    
    Calif2Seleccionada
    
    If oCalif2Seleccionada Is Nothing Then Exit Sub
    
    If gParametrosGenerales.gbSENT_ORD_CAL2 Then  'Orden descendente
        If txtCal2 = "" Then
            txtCal2 = oCalif2Seleccionada.ValorSup
        Else
            If val(txtCal2) < oCalif2Seleccionada.ValorInf Or val(txtCal2) > oCalif2Seleccionada.ValorSup Then
                txtCal2 = oCalif2Seleccionada.ValorSup
            End If
        End If

    Else  'Orden ascendente
    
        If txtCal2 = "" Then
            txtCal2 = oCalif2Seleccionada.ValorInf
        Else
            If val(txtCal2) < oCalif2Seleccionada.ValorInf Or val(txtCal2) > oCalif2Seleccionada.ValorSup Then
                txtCal2 = oCalif2Seleccionada.ValorInf
            End If
        End If
    End If
    
    Cal2CargarComboDesde = False
    
End Sub

Private Sub sdbcCal2Cod_DropDown()

    Dim oCal As CCalificacion
    Dim oCalificaciones2 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalificaciones2 = oFSGSRaiz.generar_CCalificaciones
    
    sdbcCal2Cod.RemoveAll

    If Cal2CargarComboDesde Then
        oCalificaciones2.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 2, Trim(sdbcCal2Cod.Text), , , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalificaciones2.CargarTodasLasCalificaciones 2, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalificaciones2
        
        sdbcCal2Cod.AddItem oCal.Cod & Chr(m_lSeparador) & oCal.Den & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
        
    Next

    If Cal2CargarComboDesde And Not oCalificaciones2.EOF Then
        sdbcCal2Cod.AddItem "..."
    End If

    Set oCalificaciones2 = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal2Cod_InitColumnProps()
    sdbcCal2Cod.DataFieldList = "Column 0"
    sdbcCal2Cod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcCal2Den_CloseUp()

    If sdbcCal2Den.Value = "..." Then
        sdbcCal2Den.Text = ""
        Exit Sub
    End If
    
    Cal2RespetarCombo = True
    sdbcCal2Cod.Text = sdbcCal2Den.Columns(1).Text
    sdbcCal2Den.Text = sdbcCal2Den.Columns(0).Text
    Cal2RespetarCombo = False
        
    Calif2Seleccionada
    
    If oCalif2Seleccionada Is Nothing Then Exit Sub
    
    If gParametrosGenerales.gbSENT_ORD_CAL2 Then  'Orden descendente
        If txtCal2 = "" Then
            txtCal2 = oCalif2Seleccionada.ValorSup
        Else
            If val(txtCal2) < oCalif2Seleccionada.ValorInf Or val(txtCal2) > oCalif2Seleccionada.ValorSup Then
                txtCal2 = oCalif2Seleccionada.ValorSup
            End If
        End If
        
    Else  'Orden descendente
    
        If txtCal2 = "" Then
            txtCal2 = oCalif2Seleccionada.ValorInf
        Else
            If val(txtCal2) < oCalif2Seleccionada.ValorInf Or val(txtCal2) > oCalif2Seleccionada.ValorSup Then
                txtCal2 = oCalif2Seleccionada.ValorInf
            End If
        End If
    End If
    
    
    Cal2CargarComboDesde = False
        
End Sub

Private Sub sdbcCal2Den_DropDown()

    Dim oCal As CCalificacion
    Dim oCalificaciones2 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalificaciones2 = oFSGSRaiz.generar_CCalificaciones
    
    sdbcCal2Den.RemoveAll

    If Cal2CargarComboDesde Then
        oCalificaciones2.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 2, , Trim(sdbcCal2Den.Text), , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalificaciones2.CargarTodasLasCalificaciones 2, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalificaciones2
        
        sdbcCal2Den.AddItem oCal.Den & Chr(m_lSeparador) & oCal.Cod & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
        
    Next

    If Cal2CargarComboDesde And Not oCalificaciones2.EOF Then
        sdbcCal2Den.AddItem "..."
    End If

    Set oCalificaciones2 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal2Den_InitColumnProps()
    sdbcCal2Den.DataFieldList = "Column 0"
    sdbcCal2Den.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>Realizar b�squeda</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub cmdAceptar_Click()
Dim aIdentificadores As Variant
Dim aBookmarks As Variant


    If sdbgProveedores.Columns(0).Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Select Case sOrigen
        
        Case "Prove"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmPROVE.CargarProveedorConBusqueda
                
        Case "ProveRelac"
                Dim i As Integer
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                ReDim aIdentificadores(sdbgProveedores.SelBookmarks.Count)
                ReDim aBookmarks(sdbgProveedores.SelBookmarks.Count)
                
                i = 0
                While i < sdbgProveedores.SelBookmarks.Count
                    sdbgProveedores.Bookmark = sdbgProveedores.SelBookmarks(i)
                    oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                    i = i + 1
                Wend
                sdbgProveedores.SelBookmarks.RemoveAll
                Screen.MousePointer = vbNormal
                frmPROVE.AnyadirProveRelacConBusqueda
                                
        Case "EqpPorProve"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmPROVEEqpPorProve.CargarProveedorConBusqueda
        
        Case "ProvePorEqp"
                
                If sdbgProveedores.SelBookmarks.Count = 0 Then
                    Set oProveEncontrados = Nothing
                    Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                    oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                End If
                frmPROVEProvePorEqp.CargarProveedoresConBusqueda
                
        Case "ProvePorMat"
                
                If sdbgProveedores.SelBookmarks.Count = 0 Then
                    Set oProveEncontrados = Nothing
                    Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                    oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                End If
                frmPROVEProvePorMat.CargarProveedoresConBusqueda
                
                
        Case "MatPorProve"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmPROVEMatPorProve.CargarProveedorConBusqueda
         
        Case "SelProveAnya"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSELPROVEAnya.CargarProveedorConBusqueda
         
       
        Case "frmAdmProvePortalresult"
                
                frmAdmPROVEPortalResult.sdbgProve.Columns("C�digo FSGS").Value = sdbgProveedores.Columns(0).Value
                        
        Case "LstMatPorProve"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmLstPROVEMatPorProve.CargarProveedorConBusqueda
                
        Case "LstPROVEEqpPorProve"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmLstPROVEEqpPorProve.CargarProveedorConBusqueda
                                
        Case "frmPROCEBuscar"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmPROCEBuscar.CargarProveBusqueda
                    
        Case "frmLstPROCEA2B2"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmListados.ofrmlstProce2.CargarProveBusqueda
                    
        Case "frmLstPROCEA2B3"
        
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmListados.ofrmLstProce3.CargarProveBusqueda
                        
        Case "frmLstPROCEA2B5"

                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmListados.ofrmlstProce5.CargarProveBusqueda
                
        Case "frmLstPROCEA2B4C1"

                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmLstOFEBuzon.CargarProveedorConBusqueda
        
        Case "frmLstPROCEA2B4C2"

                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmListados.ofrmLstProce4.CargarProveBusqueda
        
        Case "frmLstPROCEA6B3C1"
            
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmListados.ofrmLstProce6.CargarProveBusqueda
            
         Case "frmLstPROCEfrmSELPROVE"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSELPROVE.ofrmLstPROCE.CargarProveBusqueda
                    
        Case "frmLstPROCEfrmOFEPet"
        
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmOFEPet.ofrmLstPROCE.CargarProveBusqueda
                        
        Case "frmLstPROCEfrmOFERec"
            
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmOFERec.ofrmLstPROCE.CargarProveBusqueda
                
        Case "frmLstPROCEfrmOFEHistWeb"
            
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmOFEHistWeb.ofrmLstPROCE.CargarProveBusqueda
                
        Case "frmLstPROCEfrmPROCE"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmPROCE.g_ofrmLstPROCE.CargarProveBusqueda
    
        Case "frmOFEBuzon"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmOFEBuzon.CargarProveedorConBusqueda
    
        Case "frmLstOFEBuzon"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmLstOFEBuzon.CargarProveedorConBusqueda
        Case "frmSeguimiento"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSeguimiento.CargarProveedorConBusqueda
                
        Case "frmLstPedidos"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmLstPedidos.CargarProveedorConBusqueda
    
        Case "frmRecepGral"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmRecepGral.CargarProveedorConBusqueda
        
        Case "frmLstRecepciones"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmLstRecepciones.CargarProveedorConBusqueda
    
        Case "frmCatalogo"
    
'                If gParametrosGenerales.giINSTWEB = ConPortal Then
'                    oProveEncontrados.CargarDatosProveedor sdbgProveedores.Columns(0).Value
'                    If ((IsNull(oProveEncontrados.Item(CStr(sdbgProveedores.Columns(0).Value)).CodPortal) Or (oProveEncontrados.Item(CStr(sdbgProveedores.Columns(0).Value)).CodPortal = ""))) Then
'                        oMensajes.ProveCatalogProvePortal
'                        Screen.MousePointer = vbNormal
'                        Exit Sub
'                    End If
'                End If
                
                If gParametrosGenerales.giINSTWEB = conweb Then
                    oProveEncontrados.CargarDatosProveedor sdbgProveedores.Columns(0).Value, True
                    If oProveEncontrados.Item(CStr(sdbgProveedores.Columns(0).Value)).UsuarioWeb Is Nothing Then
                        oMensajes.ProveCatalogProveWeb
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmCatalogo.CargarProveedorConBusqueda
                    
        Case "frmCATALAnyaArt"
        
'                If gParametrosGenerales.giINSTWEB = ConPortal Then
'                    oProveEncontrados.CargarDatosProveedor sdbgProveedores.Columns(0).Value
'                    If ((IsNull(oProveEncontrados.Item(CStr(sdbgProveedores.Columns(0).Value)).CodPortal) Or (oProveEncontrados.Item(CStr(sdbgProveedores.Columns(0).Value)).CodPortal = ""))) Then
'                        oMensajes.ProveCatalogProvePortal
'                        Screen.MousePointer = vbNormal
'                        Exit Sub
'                    End If
'                End If
                
                If gParametrosGenerales.giINSTWEB = conweb Then
                    oProveEncontrados.CargarDatosProveedor sdbgProveedores.Columns(0).Value, True
                    If oProveEncontrados.Item(CStr(sdbgProveedores.Columns(0).Value)).UsuarioWeb Is Nothing Then
                        oMensajes.ProveCatalogProveWeb
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmCATALAnyaArt.CargarProveedorConBusqueda
                    
        Case "frmLstCatalogo"
        
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmLstCatalogo.CargarProveedorConBusqueda
    
        Case "frmDatoAmbitoProce"
                
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmDatoAmbitoProce.CargarProveedorConBusqueda
                
        Case "frmCatProvePedLibre"
                
                If sdbgProveedores.SelBookmarks.Count = 0 Then
                    sdbgProveedores.SelBookmarks.Add sdbgProveedores.Bookmark
                    Set oProveEncontrados = Nothing
                    Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                    oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                End If
                frmCatalogo.CargarProvesConBusquedaParaPedidoLibre
    
        Case "frmFormularios"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmFormularios.CargarProveedorConBusqueda
            
        Case "frmDesgloseValores"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmDesgloseValores.CargarProveedorConBusqueda
                
        Case "frmSolicitudDetalle"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSolicitudes.g_ofrmDetalleSolic.CargarProveedorConBusqueda
                
        Case "frmSolicitudDesglose"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.CargarProveedorConBusqueda
                
        Case "frmSolicitudDesgloseP"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.CargarProveedorConBusqueda
                
        Case "frmFlujosRoles"
                Screen.MousePointer = vbNormal
                Me.Hide 'Lo descargo en el origen
                Exit Sub
                
        Case "TiemposProveedor"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmLstTiemposProveedor.CargarProveedorConBusqueda
        Case "SeguimientoProAdj"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSeguimiento.CargarProveedorAdjConBusqueda
        Case "lstSeguimientoProAdj"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmLstPedidos.CargarProveedorAdjConBusqueda
                
        Case "frmSolicitudes"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSolicitudes.CargarProveedorConBusqueda
                
        Case "frmSOLAbrirFaltan"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSOLAbrirFaltan.CargarProveedorConBusqueda
                
        Case "frmSOLAbrirProc"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSOLAbrirProc.CargarProveedorConBusqueda
        Case "frmSelProveERP"
                Set oProveEncontrados = Nothing
                Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
                oProveEncontrados.Add sdbgProveedores.Columns(0).Value, sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                frmSelProveERP.CargarProveedorConBusqueda
    End Select
    
    Screen.MousePointer = vbNormal
    
    Unload Me
End Sub

''' <summary>Funci�n de carga</summary>
''' <remarks>Llamada desde: frmPROVEBuscar.sstabProve_Click; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub Cargar(Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones)
    Dim oICompAsignado As ICompProveAsignados
    Dim iNivelAsig As Integer
    Dim oIasig As IAsignaciones
    Dim oProveedor As CProveedor
    Dim oUON As IUon
    
    Dim iBusquedaAvanzada As Integer
    Dim sTipoRel As String
    Dim CodERP As String
    Dim bConTipoRel As Boolean
    Dim sCodProvePriSub As String
    Dim iAutoFactura As Integer
     
    Calif1 = ""
    Calif2 = ""
    Calif3 = ""
    
    If Trim(txtCal1) <> "" Then
        If Not gParametrosGenerales.gbSENT_ORD_CAL1 Then
            Calif1 = ">" & txtCal1
        Else
            Calif1 = "<" & txtCal1
        End If
    End If
    
    If Trim(txtCal2) <> "" Then
        If Not gParametrosGenerales.gbSENT_ORD_CAL2 Then
            Calif2 = ">" & txtCal2
        Else
            Calif2 = "<" & txtCal2
        End If
    End If
    
    If Trim(txtCal3) <> "" Then
        If Not gParametrosGenerales.gbSENT_ORD_CAL3 Then
            Calif3 = ">" & txtCal3
        Else
            Calif3 = "<" & txtCal3
        End If
    End If
    
    iBusquedaAvanzada = 0
    If optProveedores(0) Then
        iBusquedaAvanzada = 2
        If sdbcTipoRelacSub.Text <> "" Then
            sTipoRel = sdbcTipoRelacSub.Columns(2).Value
        End If
        If sdbcProveSubCod.Text <> "" Then
            sCodProvePriSub = sdbcProveSubCod.Text
        End If
    ElseIf optProveedores(1) Then
        iBusquedaAvanzada = 1
        bConTipoRel = optTRel(0)
        If sdbcTipoRelacPri.Text <> "" Then
            sTipoRel = sdbcTipoRelacPri.Columns(2).Value
        End If
        If sdbcProvePriCod.Text <> "" Then
            sCodProvePriSub = sdbcProvePriCod.Text
        End If
        
    End If
    
    If Me.optAutoFactura(0).Value = True Then
        iAutoFactura = 1
    ElseIf Me.optAutoFactura(1).Value = True Then
        iAutoFactura = 2
    Else
        iAutoFactura = 0
    End If
    
    If gParametrosGenerales.gbActivarCodProveErp Then
        If Trim(txtERP) <> "" Then
            CodERP = Trim(txtERP.Text)
        End If
        
        Set oUON = createUon(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario)
        iEmpresa = 0
    
        If Not oUON.Empresa Is Nothing Then iEmpresa = oUON.Empresa.Id
    End If
    
    If bRMat And Not bREqp Then
        
        'Si no ha seleccionado ning�n material
        If Trim(sdbcGMN1_4Cod) = "" And Trim(sdbcGMN2_4Cod) = "" And Trim(sdbcGMN3_4Cod) = "" And Trim(sdbcGMN4_4Cod) = "" Then
            oProveEncontrados.BuscarProveedoresConMatDelCompDesde 32000, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, Trim(txtCod), Trim(txtDen), Trim(sdbcPaiCod), , Trim(sdbcProviCod), Calif1, Calif2, Calif3, (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorDenProve), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval1), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval2), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval3), , True, , , , , , , , , , iAutoFactura, Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, CodERP, iEmpresa
            CargarProveedoresConBusqueda oProveEncontrados
            Exit Sub
        End If
        
        If sdbcGMN4_4Cod <> "" Then
            oProveEncontrados.BuscarTodosLosProveedoresDesde , codEqp, Trim(txtCod), Trim(txtDen), Trim(sdbcPaiCod), , Trim(sdbcProviCod), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), Calif1, Calif2, Calif3, (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorDenProve), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval1), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval2), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval3), , , True, , , , , , , , , , , Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, iAutoFactura, CodERP, iEmpresa
            CargarProveedoresConBusqueda oProveEncontrados
            Exit Sub
        Else
            If sdbcGMN3_4Cod <> "" Then
                                        
                Set oGMN3Seleccionado = Nothing
                Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
                oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
                oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
                oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
                 Set oICompAsignado = oGMN3Seleccionado
                iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                If iNivelAsig = 0 Or iNivelAsig > 3 Then
                    'El material que tiene asignado es de nivel 4
                    Exit Sub
                Else
                    oProveEncontrados.BuscarTodosLosProveedoresDesde , codEqp, Trim(txtCod), Trim(txtDen), Trim(sdbcPaiCod), , Trim(sdbcProviCod), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), Calif1, Calif2, Calif3, (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorDenProve), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval1), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval2), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval3), , , True, , , , , , , , , , , Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, iAutoFactura, CodERP, iEmpresa
                    CargarProveedoresConBusqueda oProveEncontrados
                    Exit Sub
                End If
                
            Else
                    
                If sdbcGMN2_4Cod <> "" Then
                    Set oGMN2Seleccionado = Nothing
                    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
                    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
                     Set oICompAsignado = oGMN2Seleccionado
                     iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                    If iNivelAsig = 0 Or iNivelAsig > 2 Then
                        Exit Sub
                    Else
                        oProveEncontrados.BuscarTodosLosProveedoresDesde , codEqp, Trim(txtCod), Trim(txtDen), Trim(sdbcPaiCod), , Trim(sdbcProviCod), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), Calif1, Calif2, Calif3, (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorDenProve), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval1), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval2), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval3), , , True, , , , , , , , , , , Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, iAutoFactura, CodERP, iEmpresa
                        CargarProveedoresConBusqueda oProveEncontrados
                        Exit Sub
                    End If
                    If Me.Visible Then txtCod.SetFocus
                    Exit Sub
                Else
                        
                    If oGMN1Seleccionado Is Nothing Then
                        Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                    End If
                    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
                    
                    Set oICompAsignado = oGMN1Seleccionado
                    iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                    
                    If iNivelAsig < 1 Or iNivelAsig > 2 Then
                        Exit Sub
                    Else
                        oProveEncontrados.BuscarTodosLosProveedoresDesde , codEqp, Trim(txtCod), Trim(txtDen), Trim(sdbcPaiCod), , Trim(sdbcProviCod), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), Calif1, Calif2, Calif3, (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorDenProve), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval1), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval2), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval3), , , True, , , , , , , , , , , Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, iAutoFactura, CodERP, iEmpresa
                        CargarProveedoresConBusqueda oProveEncontrados
                        Exit Sub
                    End If
                    
                End If
            End If
        End If
        
        If (gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador) Then
            oProveEncontrados.BuscarProveedoresConMatDelCompDesde 32000, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, Trim(txtCod), Trim(txtDen), Trim(sdbcPaiCod), , Trim(sdbcProviCod), Calif1, Calif2, Calif3, (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorDenProve), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval1), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval2), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval3), , True, , , , , , , , , , , Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, CodERP, iEmpresa
        Else
            oProveEncontrados.BuscarTodosLosProveedoresDesde , sdbcEqpCod, Trim(txtCod), Trim(txtDen), Trim(sdbcPaiCod), , Trim(sdbcProviCod), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), Calif1, Calif2, Calif3, (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorDenProve), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval1), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval2), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval3), , (sOrigen = "SelProveAnya") Or (sOrigen = "frmAdmProvePortalresult") Or (sOrigen = "frmOFEBuzon"), True, , , , , , , , , , , Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, iAutoFactura, CodERP, iEmpresa
        End If
        
        'Para caso de proveedores relacionados:
        ' Eliminar de la lista de proveedores encontrados aquellos que ya estuvieran
        ' relacionados con el proveedor principal
        If sOrigen = "ProveRelac" Then
            For Each oProveedor In m_oProveRelac
                oProveEncontrados.Remove (oProveedor.Cod)
            Next
        End If
        
        CargarProveedoresConBusqueda oProveEncontrados
        
    Else 'No hay restricci�n de material
        If (sOrigen = "frmLstPROCEfrmOFERec" Or sOrigen = "frmLstPROCEA2B4C2") And Not g_oProcesoSeleccionado Is Nothing Then
            Set oIasig = g_oProcesoSeleccionado
    
            If bRMat Then
                If IsEmpty(basOptimizacion.gCodEqpUsuario) Then
                    Set oProveEncontrados = oIasig.DevolverProveedoresDesde(Trim(txtCod), Trim(txtDen), , CriterioOrdenacion, , , , Trim(sdbcPaiCod), Trim(sdbcProviCod), Calif1, Calif2, Calif3, , , , iAutoFactura, Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, CodERP, iEmpresa)
                Else
                    Set oProveEncontrados = oIasig.DevolverProveedoresDesde(Trim(txtCod), Trim(txtDen), , CriterioOrdenacion, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcPaiCod), Trim(sdbcProviCod), Calif1, Calif2, Calif3, , , , iAutoFactura, Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, CodERP, iEmpresa)
                End If
            Else
                If IsEmpty(basOptimizacion.gCodEqpUsuario) Then
                    Set oProveEncontrados = oIasig.DevolverProveedoresDesde(Trim(txtCod), Trim(txtDen), , CriterioOrdenacion, , , , Trim(sdbcPaiCod), Trim(sdbcProviCod), Calif1, Calif2, Calif3, , , , iAutoFactura, Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, CodERP, iEmpresa)
                Else
                    Set oProveEncontrados = oIasig.DevolverProveedoresDesde(Trim(txtCod), Trim(txtDen), , CriterioOrdenacion, basOptimizacion.gCodEqpUsuario, , , Trim(sdbcPaiCod), Trim(sdbcProviCod), Calif1, Calif2, Calif3, , , , iAutoFactura, Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, CodERP, iEmpresa)
                End If
            End If
            Set oIasig = Nothing
        Else
            If bRMat Or (gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador) Then
                oProveEncontrados.BuscarProveedoresConMatDelCompDesde 32000, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, Trim(txtCod), Trim(txtDen), Trim(sdbcPaiCod), , Trim(sdbcProviCod), Calif1, Calif2, Calif3, (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorDenProve), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval1), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval2), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval3), , True, , , , , , , , , , iAutoFactura, Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, CodERP, iEmpresa
            Else
                oProveEncontrados.BuscarTodosLosProveedoresDesde , codEqp, Trim(txtCod), Trim(txtDen), Trim(sdbcPaiCod), , Trim(sdbcProviCod), Trim(CodGMN1), Trim(CodGMN2), Trim(CodGMN3), Trim(codGMN4), Calif1, Calif2, Calif3, (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorDenProve), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval1), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval2), (CriterioOrdenacion = TipoOrdenacionAsignaciones.OrdAsigPorval3), , (sOrigen = "SelProveAnya") Or (sOrigen = "frmAdmProvePortalresult") Or (sOrigen = "frmOFEBuzon"), True, , , , , , , , , , , Trim(txtVAT), iBusquedaAvanzada, sTipoRel, bConTipoRel, sCodProvePriSub, sCodProveSelecc, iAutoFactura, CodERP, iEmpresa
            End If
        End If
        'Para caso de proveedores relacionados:
        ' Eliminar de la lista de proveedores encontrados aquellos que ya estuvieran
        ' relacionados con el proveedor principal
        If sOrigen = "ProveRelac" Then
            For Each oProveedor In m_oProveRelac
                oProveEncontrados.Remove (oProveedor.Cod)
            Next
        End If
        CargarProveedoresConBusqueda oProveEncontrados
    End If
End Sub

Private Sub cmdCancelar_Click()
    
    Set oProveEncontrados = Nothing
    Unload Me
End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = "frmProveBuscar"
    frmSELMAT.bRComprador = bRMat
    'frmSELMAT.Hide
    frmSELMAT.Show 1
End Sub
Private Sub Calif1Seleccionada()
    
    If sdbcCal1Cod.Text = "" Then
        Set oCalif1Seleccionada = Nothing
        Exit Sub
    End If
    
    If oCalif1Seleccionada Is Nothing Then
        Set oCalif1Seleccionada = oFSGSRaiz.generar_CCalificacion
    End If
    
    oCalif1Seleccionada.Cod = sdbcCal1Cod.Text
    oCalif1Seleccionada.Den = sdbcCal1Den.Text
    
    On Error Resume Next
    
    If UCase(sdbcCal1Cod.Columns(0).Value) <> UCase(sdbcCal1Cod.Text) Then
        oCalif1Seleccionada.ValorInf = sdbcCal1Den.Columns(2).Value
        oCalif1Seleccionada.ValorSup = sdbcCal1Den.Columns(3).Value
    Else
        oCalif1Seleccionada.ValorInf = sdbcCal1Cod.Columns(2).Value
        oCalif1Seleccionada.ValorSup = sdbcCal1Cod.Columns(3).Value
    End If
    
    On Error GoTo 0

End Sub

Private Sub Calif2Seleccionada()
    
    If sdbcCal2Cod.Text = "" Then
        Set oCalif2Seleccionada = Nothing
        Exit Sub
    End If
    
    If oCalif2Seleccionada Is Nothing Then
        Set oCalif2Seleccionada = oFSGSRaiz.generar_CCalificacion
    End If
    
    oCalif2Seleccionada.Cod = sdbcCal2Cod.Text
    oCalif2Seleccionada.Den = sdbcCal2Den.Text
    
    On Error Resume Next
    
    If UCase(sdbcCal2Cod.Columns(0).Value) <> UCase(sdbcCal2Cod.Text) Then
        oCalif2Seleccionada.ValorInf = sdbcCal2Den.Columns(2).Value
        oCalif2Seleccionada.ValorSup = sdbcCal2Den.Columns(3).Value
    Else
        oCalif2Seleccionada.ValorInf = sdbcCal2Cod.Columns(2).Value
        oCalif2Seleccionada.ValorSup = sdbcCal2Cod.Columns(3).Value
    End If
    
    On Error GoTo 0

End Sub
Private Sub Calif3Seleccionada()
    
    If sdbcCal3Cod.Text = "" Then
        Set oCalif3Seleccionada = Nothing
        Exit Sub
    End If
    
    If oCalif3Seleccionada Is Nothing Then
        Set oCalif3Seleccionada = oFSGSRaiz.generar_CCalificacion
    End If
    
    oCalif3Seleccionada.Cod = sdbcCal3Cod.Text
    oCalif3Seleccionada.Den = sdbcCal3Den.Text
    
    On Error Resume Next
    
    If UCase(sdbcCal3Cod.Columns(0).Value) <> UCase(sdbcCal3Cod.Text) Then
        oCalif3Seleccionada.ValorInf = sdbcCal3Den.Columns(2).Value
        oCalif3Seleccionada.ValorSup = sdbcCal3Den.Columns(3).Value
    Else
        oCalif3Seleccionada.ValorInf = sdbcCal3Cod.Columns(2).Value
        oCalif3Seleccionada.ValorSup = sdbcCal3Cod.Columns(3).Value
    End If
    
    On Error GoTo 0

End Sub

''' <summary>Carga de los literales multiidioma</summary>
''' <remarks>Llamada desde:frmProveBuscar.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVE_BUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 4)
        For i = 1 To 4
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        frCalif.caption = sIdioma(1)
        cmdAceptar.caption = Ador(0).Value '5
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        fraMat.caption = Ador(0).Value
        Ador.MoveNext
        Frame1.caption = Ador(0).Value
        Ador.MoveNext
        fraSelComp.caption = Ador(0).Value
        Ador.MoveNext
        frmPROVEBuscar.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        Label3.caption = Ador(0).Value & ":" '12 C�digo
        lblCodERP.caption = Ador(0).Value & ":" '12 C�digo
        sdbgProveedores.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        Label5.caption = Ador(0).Value
        Ador.MoveNext
        Label6.caption = Ador(0).Value
        Ador.MoveNext
        sdbcCal1Cod.Columns(0).caption = Ador(0).Value '15 Cod
        sdbcCal1Den.Columns(1).caption = Ador(0).Value
        sdbcCal2Cod.Columns(0).caption = Ador(0).Value
        sdbcCal2Den.Columns(1).caption = Ador(0).Value
        sdbcCal3Cod.Columns(0).caption = Ador(0).Value
        sdbcCal3Den.Columns(1).caption = Ador(0).Value
        sdbcEqpCod.Columns(0).caption = Ador(0).Value
        sdbcEqpDen.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        sdbcPaiCod.Columns(0).caption = Ador(0).Value
        sdbcPaiDen.Columns(1).caption = Ador(0).Value
        sdbcProviCod.Columns(0).caption = Ador(0).Value
        sdbcProviDen.Columns(1).caption = Ador(0).Value

        Ador.MoveNext
        sdbcCal1Cod.Columns(1).caption = Ador(0).Value '16 Denominacion
        sdbcCal1Den.Columns(0).caption = Ador(0).Value
        sdbcCal2Cod.Columns(1).caption = Ador(0).Value
        sdbcCal2Den.Columns(0).caption = Ador(0).Value
        sdbcCal3Cod.Columns(1).caption = Ador(0).Value
        sdbcCal3Den.Columns(0).caption = Ador(0).Value
        sdbcEqpCod.Columns(1).caption = Ador(0).Value
        sdbcEqpDen.Columns(0).caption = Ador(0).Value
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        sdbcPaiCod.Columns(1).caption = Ador(0).Value
        sdbcPaiDen.Columns(0).caption = Ador(0).Value
        sdbcProviCod.Columns(1).caption = Ador(0).Value
        sdbcProviDen.Columns(0).caption = Ador(0).Value
        sdbgProveedores.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcCal1Cod.Columns(2).caption = Ador(0).Value
        sdbcCal1Den.Columns(2).caption = Ador(0).Value
        sdbcCal2Cod.Columns(2).caption = Ador(0).Value
        sdbcCal2Den.Columns(2).caption = Ador(0).Value
        sdbcCal3Cod.Columns(2).caption = Ador(0).Value
        sdbcCal3Den.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcCal1Cod.Columns(3).caption = Ador(0).Value
        sdbcCal1Den.Columns(3).caption = Ador(0).Value
        sdbcCal2Cod.Columns(3).caption = Ador(0).Value
        sdbcCal2Den.Columns(3).caption = Ador(0).Value
        sdbcCal3Cod.Columns(3).caption = Ador(0).Value
        sdbcCal3Den.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sstabProve.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabProve.TabCaption(1) = Ador(0).Value '20
        Ador.MoveNext
        'fraPremi.caption = ador(0).Value
        Ador.MoveNext
        'chkPremium.caption = ador(0).Value
        Ador.MoveNext
        'chkActivo.caption = ador(0).Value
        Ador.MoveNext
        'fraWeb.caption = ador(0).Value
        Ador.MoveNext
'        chkWeb.caption = ador(0).Value
        Ador.MoveNext
'        chkBloq.caption = ador(0).Value
        
        Ador.MoveNext
        sLiteralBusqAvanzada = Ador(0).Value  '27 B�squeda Avanzada
        Ador.MoveNext
        sLiteralOcultar = Ador(0).Value  '28 Ocultar
        Ador.MoveNext
        fraBusqAvanzada.caption = Ador(0).Value  '29 Proveedores relacionados
        Ador.MoveNext
        optProveedores(0).caption = Ador(0).Value  '30 Mostrar proveedores subordinados
        Ador.MoveNext
        lblProveRelac(0).caption = Ador(0).Value  '31 Tipo de relaci�n:
        sLiteralTipoRel = Ador(0).Value
        Ador.MoveNext
        lblProveRelac(1).caption = Ador(0).Value  '32 Mostrar proveedores subordinados al siguiente proveedor:
        lblProveRelac(2).caption = lblProveRelac(1).caption
        Ador.MoveNext
        optProveedores(1).caption = Ador(0).Value  '33 Mostrar proveedores principales
        Ador.MoveNext
        optTRel(0).caption = Ador(0).Value  '34 Con tipo de relaci�n
        Ador.MoveNext
        optTRel(1).caption = Ador(0).Value  '35 Sin tipo de relaci�n
        Ador.MoveNext
        lblProveRelac(3).caption = Ador(0).Value  '36 Mostrar proveedores que tienen al siguiente proveedor como subordinado:
        Ador.MoveNext
        optProveedores(2).caption = Ador(0).Value  '37 Mostrar todos los proveedores
        Me.optAutoFactura(2).caption = Ador(0).Value
        Ador.MoveNext
        Label7.caption = Ador(0).Value  '38 NIF:
        Ador.MoveNext
        stabGeneral.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        stabGeneral.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        Me.fraAutofactura.caption = Ador(0).Value
        Ador.MoveNext
        Me.optAutoFactura(0).caption = Ador(0).Value
        Ador.MoveNext
        Me.optAutoFactura(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgProveedores.Columns("COD_ERP").caption = Ador(0).Value '44 C�digo en ERP
        Ador.MoveNext
        sdbgProveERP.Columns("PROVE_ERP").caption = Ador(0).Value  '45 Proveedor en ERP
        Ador.MoveNext
        sdbgProveERP.Columns("DEN").caption = Ador(0).Value '46 Descripci�n
        Ador.MoveNext
        chkTrasladarProve.caption = Ador(0).Value
        
        Ador.Close
        
        lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
        lblGMN2_4.caption = gParametrosGenerales.gsABR_GMN2 & ":"
        lblGMN3_4.caption = gParametrosGenerales.gsABR_GMN3 & ":"
        lblGMN4_4.caption = gParametrosGenerales.gsABR_GMN4 & ":"
        
    End If
    
    Set Ador = Nothing
    
End Sub
        
''' <summary>
''' Carga del formulario de b�squeda
''' </summary>
''' <remarks>Llamada desde: frmProveBuscar; Tiempo m�ximo: 1 seg.</remarks>
''' <revision>JVS 29/08/2011</revision>
Private Sub Form_Load()

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
       
    CargarRecursos
    
    PonerFieldSeparator Me
    
    lblCalif1.caption = gParametrosGenerales.gsDEN_CAL1 & ":"
    lblCalif2.caption = gParametrosGenerales.gsDEN_CAL2 & ":"
    lblCalif3.caption = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If gParametrosGenerales.gbSENT_ORD_CAL1 Then
        lblSentCal1.caption = "<="
    Else
        lblSentCal1.caption = ">="
    End If
    
    If gParametrosGenerales.gbSENT_ORD_CAL2 Then
        lblSentCal2.caption = "<="
    Else
        lblSentCal2.caption = ">="
    End If
    
    If gParametrosGenerales.gbSENT_ORD_CAL3 Then
        lblSentCal3.caption = "<="
    Else
        lblSentCal3.caption = ">="
    End If
        
    If Not gParametrosGenerales.gbActivarCodProveErp Then
        sdbgProveedores.Columns("COD_ERP").Visible = False
    End If
    sdbgProveedores.Columns(3).caption = gParametrosGenerales.gsDEN_CAL1
    sdbgProveedores.Columns(4).caption = gParametrosGenerales.gsDEN_CAL2
    sdbgProveedores.Columns(5).caption = gParametrosGenerales.gsDEN_CAL3
    
    
    'Carga los tipos de relaci�n de proveedores
    Set oTiposRelac = oFSGSRaiz.Generar_CRelacTipos
    
    Set oProveEncontrados = oFSGSRaiz.generar_CProveedores
    Set oPaises = oFSGSRaiz.Generar_CPaises
    Set oCalificaciones1 = oFSGSRaiz.generar_CCalificaciones
    oCalificaciones1.CargarTodasLasCalificaciones 1, , , , , , , , basPublic.gParametrosInstalacion.gIdioma
    Set oCalificaciones2 = oFSGSRaiz.generar_CCalificaciones
    oCalificaciones2.CargarTodasLasCalificaciones 2, , , , , , , , basPublic.gParametrosInstalacion.gIdioma
    Set oCalificaciones3 = oFSGSRaiz.generar_CCalificaciones
    oCalificaciones3.CargarTodasLasCalificaciones 3, , , , , , , , basPublic.gParametrosInstalacion.gIdioma
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    

    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPROVE
    fraSelComp.Visible = False

    'Si hay restricci�n de equipo configuro el interfaz adecuadamente
    If bREqp Then
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        lblEqp.Visible = True
        lblEqp.caption = basOptimizacion.gCodEqpUsuario & " " & oUsuarioSummit.comprador.DenEqp
        sdbcEqpCod.Text = basOptimizacion.gCodEqpUsuario
        codEqp = basOptimizacion.gCodEqpUsuario
    End If

    If sOrigen = "SelProveAnya" Or sOrigen = "frmAdmProvePortalresult" Or sOrigen = "frmOFEBuzon" Or sOrigen = "frmLstOFEBuzon" Or sOrigen = "frmDatoAmbitoProce" Or sOrigen = "frmCatProvePedLibre" Or sOrigen = "ProveRelac" Then
        fraSelComp.Visible = True
    End If
    fraERP.Visible = False
    If gParametrosGenerales.gbActivarCodProveErp Then
        fraERP.Visible = True
        lblCodERP.Visible = True
        txtERP.Visible = True
    End If
        
    'Se modifica el SelectTypeRow de la grid dependiendo del origen
    If sOrigen = "SelProveAnya" Or sOrigen = "frmAdmProvePortalresult" Or sOrigen = "frmDatoAmbitoProce" Then
        sdbgProveedores.SelectTypeRow = ssSelectionTypeSingleSelect
    End If
    
    If Not basParametros.gParametrosGenerales.gbOblProveEqp Then
        fraSelComp.Visible = False
    End If
        
    fraProveSub.Enabled = False
    fraProvePri.Enabled = False
    
    sdbcTipoRelacSub.Enabled = False
    sdbcProveSubCod.Enabled = False
    sdbcProveSubDen.Enabled = False
    optTRel(0).Enabled = False
    optTRel(1).Enabled = False
    sdbcTipoRelacPri.Enabled = False
    sdbcProvePriCod.Enabled = False
    sdbcProvePriDen.Enabled = False
    If sOrigen = "ProveRelac" Then
        'Lista de proveedores ya relacionados
        Set m_oProveRelac = frmPROVE.g_oProveSeleccionado.ProveRel
        sCodProveSelecc = frmPROVE.g_oProveSeleccionado.Cod
    
        sdbgProveedores.SelectTypeRow = ssSelectionTypeMultiSelectRange
        sdbgProveedores.MaxSelectedRows = 100
        
        Me.cmdAceptar.Visible = True
        Me.cmdCancelar.Visible = True
    Else
        sdbgProveedores.SelectTypeRow = ssSelectionTypeSingleSelect
        sdbgProveedores.MaxSelectedRows = 0
    End If

    sdbgProveedores.Columns("CodWeb").Visible = False
    
    If Not NoHayParametro(CodGMN1) Then
        sdbcGMN1_4Cod.Text = CodGMN1
        sdbcGMN1_4Cod_Validate (False)
    End If
    If Not NoHayParametro(CodGMN2) Then
        sdbcGMN2_4Cod.Text = CodGMN2
        sdbcGMN2_4Cod_Validate (False)
    End If
    If Not NoHayParametro(CodGMN3) Then
        sdbcGMN3_4Cod.Text = CodGMN3
        sdbcGMN3_4Cod_Validate (False)
    End If
    If Not NoHayParametro(codGMN4) Then
        sdbcGMN4_4Cod.Text = codGMN4
        sdbcGMN4_4Cod_Validate (False)
    End If
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
            
    Me.fraAutofactura.Visible = False
    If (gParametrosGenerales.gsAccesoFSIM = AccesoFSIM) Then
        Me.fraAutofactura.Visible = True
    End If
End Sub

''' <summary>Evento descarga del formulario, donde se borran los objetos</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub Form_Unload(Cancel As Integer)
    
    Calif1 = ""
    Calif2 = ""
    Calif3 = ""
    
    sOrigen = ""
    
    Set oTiposRelac = Nothing
    Set oProveEncontrados = Nothing
    Set oPaises = Nothing
    Set oCalificaciones1 = Nothing
    Set oCalificaciones2 = Nothing
    Set oCalificaciones3 = Nothing
    Set oEquipos = Nothing
    Set oIProves = Nothing
    Set oGruposMN1 = Nothing
    Set oGruposMN2 = Nothing
    Set oGruposMN3 = Nothing
    Set oGruposMN4 = Nothing
    Set g_oProcesoSeleccionado = Nothing
    
    CodGMN1 = ""
    CodGMN2 = ""
    CodGMN3 = ""
    codGMN4 = ""
    codEqp = ""
    sdbgProveedores.SelBookmarks.RemoveAll

End Sub

Private Sub sdbcPaiCod_Change()
    
    If Not PaiRespetarCombo Then
    
        PaiRespetarCombo = True
        sdbcPaiDen.Text = ""
        PaiRespetarCombo = False
        
        PaiCargarComboDesde = True
        Set oPaisSeleccionado = Nothing
        
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
    
End Sub

Private Sub sdbcPaiCod_CloseUp()
    
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    PaiRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    PaiRespetarCombo = False
    
    Set oPaisSeleccionado = oPaises.Item(sdbcPaiCod.Columns(0).Text)
        
    PaiCargarComboDesde = False
        
End Sub

Private Sub sdbcPaiCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oPaises = Nothing
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    sdbcPaiCod.RemoveAll
    
    If PaiCargarComboDesde Then
        oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcPaiCod.Text), , , False
    Else
        oPaises.CargarTodosLosPaises , , , , , False
    End If
    
    Codigos = oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If PaiCargarComboDesde And Not oPaises.EOF Then
        sdbcPaiCod.AddItem "..."
    End If

    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPaiCod_InitColumnProps()

    sdbcPaiCod.DataFieldList = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcPaiCod_PositionList(ByVal Text As String)
PositionList sdbcPaiCod, Text
End Sub
Private Sub sdbcPaiCod_Validate(Cancel As Boolean)

    Dim oPaises As CPaises
    Dim bExiste As Boolean
    
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    If sdbcPaiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
     oPaises.CargarTodosLosPaises sdbcPaiCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiCod.Text = ""
    Else
        PaiRespetarCombo = True
        sdbcPaiDen.Text = oPaises.Item(1).Den
        
        sdbcPaiCod.Columns(0).Value = sdbcPaiCod.Text
        sdbcPaiCod.Columns(1).Value = sdbcPaiDen.Text
        
        PaiRespetarCombo = False
        Set oPaisSeleccionado = oPaises.Item(1)
        PaiCargarComboDesde = False

    End If
    
    Set oPaises = Nothing
    
    
End Sub
Private Sub sdbcPaiDen_Change()
    
    If Not PaiRespetarCombo Then
    
        PaiRespetarCombo = True
        sdbcPaiCod.Text = ""
        PaiRespetarCombo = False
        PaiCargarComboDesde = True
        Set oPaisSeleccionado = Nothing

    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
        
End Sub
Private Sub sdbcPaiDen_CloseUp()
    
    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    PaiRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    PaiRespetarCombo = False
    
    Set oPaisSeleccionado = oPaises.Item(sdbcPaiDen.Columns(1).Text)
    
    PaiCargarComboDesde = False
        
End Sub

Private Sub sdbcPaiDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oPaises = Nothing
    Set oPaises = oFSGSRaiz.Generar_CPaises
   
    sdbcPaiDen.RemoveAll
    
    If PaiCargarComboDesde Then
        oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPaiDen.Text), True, False
    Else
        oPaises.CargarTodosLosPaises , , , True, , False
    End If
    
    Codigos = oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If PaiCargarComboDesde And Not oPaises.EOF Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiCod.Refresh

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcPaiDen_InitColumnProps()

    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcPaiDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcPaiCod.DroppedDown = False
        sdbcPaiCod.Text = ""
        sdbcPaiCod.RemoveAll
        sdbcPaiDen.DroppedDown = False
        sdbcPaiDen.Text = ""
        sdbcPaiDen.RemoveAll
        Set oPaisSeleccionado = Nothing
    End If
End Sub
Private Sub sdbcPaiDen_PositionList(ByVal Text As String)
PositionList sdbcPaiDen, Text
End Sub

Private Sub sdbcPaiDen_Validate(Cancel As Boolean)

    Dim oPaises As CPaises
    Dim bExiste As Boolean
    
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    If sdbcPaiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    oPaises.CargarTodosLosPaises , sdbcPaiDen.Text, True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiDen.Text = ""
    Else
        PaiRespetarCombo = True
        sdbcPaiCod.Text = oPaises.Item(1).Cod
        
        sdbcPaiDen.Columns(0).Value = sdbcPaiDen.Text
        sdbcPaiDen.Columns(1).Value = sdbcPaiCod.Text
        
        PaiRespetarCombo = False
        Set oPaisSeleccionado = oPaises.Item(1)
        PaiCargarComboDesde = False
        
    End If
    
    Set oPaises = Nothing

End Sub

Public Sub CargarProveedoresConBusqueda(ByVal oProves As CProveedores)
Dim i As Integer

sdbgProveedores.RemoveAll

If oProves Is Nothing Then Exit Sub
    
DatosProve = oProves.DevolverLosDatos
    
    For i = 0 To UBound(DatosProve.Cod) - 1
   
        sdbgProveedores.AddItem DatosProve.Cod(i) & Chr(m_lSeparador) & DatosProve.Den(i) & Chr(m_lSeparador) & DatosProve.CodERP(i) & Chr(m_lSeparador) & DatosProve.Cal1(i) & Chr(m_lSeparador) & DatosProve.Cal2(i) & Chr(m_lSeparador) & DatosProve.Cal3(i) & Chr(m_lSeparador) & DatosProve.AccesoWeb(i)
    
    Next

    If Not oProves.EOF Then
        sdbgProveedores.AddItem Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "....."
    End If

Set oProves = Nothing

End Sub
Private Sub sdbcProviCod_Change()
    
    If Not ProviRespetarCombo Then
    
        ProviRespetarCombo = True
        sdbcProviDen.Text = ""
        ProviRespetarCombo = False
        
        ProviCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcProviCod_CloseUp()
    
    If sdbcProviCod.Value = "..." Then
        sdbcProviCod.Text = ""
        Exit Sub
    End If
    
    ProviRespetarCombo = True
    sdbcProviDen.Text = sdbcProviCod.Columns(1).Text
    sdbcProviCod.Text = sdbcProviCod.Columns(0).Text
    ProviRespetarCombo = False
    
    ProviCargarComboDesde = False
        
End Sub

Private Sub sdbcProviCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProviCod.RemoveAll
        
    If Not oPaisSeleccionado Is Nothing Then
    
        
        If ProviCargarComboDesde Then
            oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProviCod.Text), , , False
        Else
            oPaisSeleccionado.CargarTodasLasProvincias , , , , False
        End If
        
        Codigos = oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
        
        If ProviCargarComboDesde And Not oPaisSeleccionado.Provincias.EOF Then
            sdbcProviCod.AddItem "..."
        End If

    End If
    
    sdbcProviCod.SelStart = 0
    sdbcProviCod.SelLength = Len(sdbcProviCod.Text)
    sdbcProviCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProviCod_InitColumnProps()

    sdbcProviCod.DataFieldList = "Column 0"
    sdbcProviCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProviCod_PositionList(ByVal Text As String)
PositionList sdbcProviCod, Text
End Sub
Private Sub sdbcProviCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProviCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    If Not oPaisSeleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        oPaisSeleccionado.CargarTodasLasProvincias sdbcProviCod.Text, , True, , False
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oPaisSeleccionado.Provincias.Count = 0)
        
    Else
    
        bExiste = False
        
    End If
    
        
    If Not bExiste Then
        sdbcProviCod.Text = ""
    Else
        ProviRespetarCombo = True
        sdbcProviDen.Text = oPaisSeleccionado.Provincias.Item(1).Den
        
        sdbcProviCod.Columns(0).Value = sdbcProviCod.Text
        sdbcProviCod.Columns(1).Value = sdbcProviDen.Text
        
        ProviRespetarCombo = False
        ProviCargarComboDesde = False
    End If
    
End Sub
Private Sub sdbcProviDen_Change()
    
    If Not ProviRespetarCombo Then
    
        ProviRespetarCombo = True
        sdbcProviCod.Text = ""
        ProviRespetarCombo = False
        ProviCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcProviDen_CloseUp()
    
    If sdbcProviDen.Value = "..." Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If
    
    ProviRespetarCombo = True
    sdbcProviCod.Text = sdbcProviDen.Columns(1).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(0).Text
    ProviRespetarCombo = False
    
    ProviCargarComboDesde = False
        
End Sub
Private Sub sdbcProviDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProviDen.RemoveAll
    
    If Not oPaisSeleccionado Is Nothing Then
    
        
        If ProviCargarComboDesde Then
            oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProviDen.Text), True, False
        Else
            oPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If
        
        Codigos = oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
        
        If ProviCargarComboDesde And Not oPaisSeleccionado.Provincias.EOF Then
            sdbcProviDen.AddItem "..."
        End If

    End If
    
    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
    sdbcProviCod.Refresh

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcProviDen_InitColumnProps()

    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcProviDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcProviCod.DroppedDown = False
        sdbcProviCod.Text = ""
        sdbcProviCod.RemoveAll
        sdbcProviDen.DroppedDown = False
        sdbcProviDen.Text = ""
        sdbcProviDen.RemoveAll
    End If
End Sub


Private Sub sdbcProviDen_PositionList(ByVal Text As String)
PositionList sdbcProviDen, Text
End Sub

Private Sub sdbcProviDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProviDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    If Not oPaisSeleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        oPaisSeleccionado.CargarTodasLasProvincias , sdbcProviDen.Text, True, , False
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oPaisSeleccionado.Provincias.Count = 0)
        
    Else
    
        bExiste = False
        
    End If
    
        
    If Not bExiste Then
        sdbcProviDen.Text = ""
    Else
        ProviRespetarCombo = True
        Screen.MousePointer = vbHourglass
        sdbcProviCod.Text = oPaisSeleccionado.Provincias.Item(1).Cod
        sdbcProviDen.Columns(0).Value = sdbcProviDen.Text
        sdbcProviDen.Columns(1).Value = sdbcProviCod.Text
        
        Screen.MousePointer = vbNormal
        ProviRespetarCombo = False
        ProviCargarComboDesde = False
    End If

End Sub

Private Sub sdbgProveedores_BtnClick()
    If sdbgProveedores.col < 0 Then Exit Sub
    
    Select Case sdbgProveedores.Columns(sdbgProveedores.col).Name
    
        Case "COD_ERP"
            CargarCodigosERP sdbgProveedores.Columns("COD").Value
            
            sdbgProveERP.Left = sdbgProveedores.Columns("COD_ERP").Left - 3000
            sdbgProveERP.Top = (sdbgProveedores.Row * 240) + 1100
            SetCursorPos sdbgProveedores.ActiveCell.Left, sdbgProveedores.ActiveCell.Top + 30
            sdbgProveERP.Visible = True
            
    End Select
        
End Sub

Private Sub sdbgProveedores_Click()
'Static iAnyadir As Integer
'Dim iEliminar As Integer
'Dim Cod As String
'
'If sOrigen = "ProvePorEqp" Or sOrigen = "ProvePorMat" Or sOrigen = "frmCatProvePedLibre" Then
'
'    sdbgProveedores.SelBookmarks.Add sdbgProveedores.Bookmark
'
'    If iAnyadir = sdbgProveedores.SelBookmarks.Count Then
'        Cod = sdbgProveedores.Columns("COD").Value
'        For iEliminar = 0 To sdbgProveedores.SelBookmarks.Count - 1
'            sdbgProveedores.Bookmark = sdbgProveedores.SelBookmarks(iEliminar)
'            If Cod = sdbgProveedores.Columns("COD").Value Then
'                sdbgProveedores.SelBookmarks.Remove iEliminar
'                sdbgProveedores.ActiveRowStyleSet = "Normal"
'                iAnyadir = sdbgProveedores.SelBookmarks.Count
'                Exit Sub
'            End If
'        Next
'    End If
'
'    iAnyadir = sdbgProveedores.SelBookmarks.Count
'    sdbgProveedores.ActiveRowStyleSet = "Selected"
'
'Else
'    sdbgProveedores.SelBookmarks.Remove 0
'    sdbgProveedores.ActiveRowStyleSet = "Selected"
'End If

End Sub

''' <summary>
''' Al hacer dbl click se limpia la selecci�n. La selecci�n pasa a ser lo q acabas de hacer dbl click
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgProveedores_DblClick()
    sdbgProveedores.SelBookmarks.RemoveAll
    sdbgProveedores.SelBookmarks.Add sdbgProveedores.Bookmark

    cmdAceptar_Click
End Sub

''' <summary>Evento de pulsaci�n sobre cabecera en la grid</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub sdbgProveedores_HeadClick(ByVal ColIndex As Integer)
    Dim iOrden As Integer
    
    Select Case ColIndex
        Case 0
            iOrden = TipoOrdenacionAsignaciones.OrdAsigPorCodProve
        Case 1
            iOrden = TipoOrdenacionAsignaciones.OrdAsigPorDenProve
        Case 3
            iOrden = TipoOrdenacionAsignaciones.OrdAsigPorval1
        Case 4
            iOrden = TipoOrdenacionAsignaciones.OrdAsigPorval2
        Case 5
            iOrden = TipoOrdenacionAsignaciones.OrdAsigPorval3
    End Select
    
    Cargar iOrden

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgProveedores_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    sdbgProveERP.Visible = False
End Sub



''' <summary>
''' Para poder ver el tab de proveedores debes de tener algo seleccionado
''' </summary>
''' <param name="PreviousTab">tab del q vienes</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sstabProve_Click(PreviousTab As Integer)
    Screen.MousePointer = vbNormal
    
    If PreviousTab = 0 And sstabProve.Tab = 1 Then
        If Not HayAlgunFiltro Then
            sstabProve.Tab = 0
            
            oMensajes.ProveSeleccioneAlgo
        Else
            Screen.MousePointer = vbHourglass
            Cargar
            Screen.MousePointer = vbNormal
        End If
    End If
End Sub



''' <summary>
''' Para poder ver el tab de proveedores debes de tener algo seleccionado
''' </summary>
''' <returns>Si hay algo seleccionado o no</returns>
''' <remarks>Llamada desde: xxx ; Tiempo m�ximo: 0</remarks>
Private Function HayAlgunFiltro() As Boolean
    'Datos Generales --------------------------------
    If Trim(Me.txtVAT.Text) <> "" Then
        HayAlgunFiltro = True
        Exit Function
    End If
    If Trim(Me.txtCod.Text) <> "" Then
        HayAlgunFiltro = True
        Exit Function
    End If
    If Trim(Me.txtDen.Text) <> "" Then
        HayAlgunFiltro = True
        Exit Function
    End If
    If Trim(Me.sdbcPaiCod.Value) <> "" Then
        HayAlgunFiltro = True
        Exit Function
    End If
    'Si no hay pais no hay provincia
    
    'Calificaciones --------------------------------
    If Trim(txtCal1.Text) <> "" Then
'        If Not IsNumeric(txtCal1) Then Para esto esta txtCal1_Change
'            oMensajes.NoValido sIdioma(2)
'        Else
            HayAlgunFiltro = True
            Exit Function
'        End If
    End If
    If Trim(txtCal2.Text) <> "" Then
'        If Not IsNumeric(txtCal2) Then Para esto esta txtCal2_Change
'            oMensajes.NoValido sIdioma(3)
'        Else
            HayAlgunFiltro = True
            Exit Function
'        End If
    End If
    If Trim(txtCal3.Text) <> "" Then
'        If Not IsNumeric(txtCal3) Then Para esto esta txtCal3_Change
'            oMensajes.NoValido sIdioma(4)
'        Else
            HayAlgunFiltro = True
            Exit Function
'        End If
    End If
    
    'Material --------------------------------
    If Me.sdbcGMN1_4Cod.Value <> "" Then
        HayAlgunFiltro = True
        Exit Function
    End If
    'si no hay gmn1 no hay gmn2,3,4
    
    'Equipo --------------------------------
    If Me.fraSelComp.Visible Then
        If Trim(Me.sdbcEqpCod.Text) <> "" Then 'editable o no (Si hay restricci�n de equipo) .Text contiene al equipo
            HayAlgunFiltro = True
            Exit Function
        End If
    End If
    
    'Autofacturacion
    If Me.fraAutofactura.Visible Then
        If Me.optAutoFactura(2).Value = False Then
            'Filtra o bien pq permite autofacturacion o bien pq no permite autofacturacion
            HayAlgunFiltro = True
            Exit Function
        End If
    End If
    
    'Relacionados
    If Me.optProveedores(0).Value = True Then
        'Filtra por subordinados
        If sdbcTipoRelacSub.Text <> "" Or sdbcProveSubCod.Text <> "" Then
            HayAlgunFiltro = True
            Exit Function
        End If
    ElseIf Me.optProveedores(1).Value = True Then
        'Filtra por principales
        If sdbcTipoRelacPri.Text <> "" Or sdbcProvePriCod.Text <> "" Then
            HayAlgunFiltro = True
            Exit Function
        End If
    End If
    'ERP
    If Trim(Me.txtERP.Text) <> "" Then
        HayAlgunFiltro = True
        Exit Function
    End If
End Function



Private Sub txtCal1_Change()

If txtCal1 = "" Then
    'sdbcCal1Cod.Text = ""
    'sdbcCal1Den.Text = ""
    Exit Sub
End If

If Not IsNumeric(txtCal1) Then
    oMensajes.NoValido sIdioma(2)
    txtCal1 = ""
    If Me.Visible Then txtCal1.SetFocus
    Exit Sub
End If
    
Set oCalif1Seleccionada = BuscarCalificacion(txtCal1, oCalificaciones1)

If oCalif1Seleccionada Is Nothing Then
    sdbcCal1Cod.Text = ""
    sdbcCal1Den.Text = ""
Else
    sdbcCal1Cod.Text = oCalif1Seleccionada.Cod
    sdbcCal1Cod_Validate False
End If
    
End Sub

Private Function BuscarCalificacion(ByVal valor As Variant, ByVal oCalificaciones As CCalificaciones) As CCalificacion
Dim iIndice As Integer
Dim iLimSup As Integer

    iIndice = 1
    
    If IsNull(valor) Then
        Set BuscarCalificacion = Nothing
    End If
    
    iLimSup = oCalificaciones.Count
       
       
    While iIndice <= iLimSup
        
        If valor >= oCalificaciones.Item(iIndice).ValorInf And valor <= oCalificaciones.Item(iIndice).ValorSup Then
            Set BuscarCalificacion = oCalificaciones.Item(iIndice)
            Exit Function
        End If
        iIndice = iIndice + 1
    Wend
    
    Set BuscarCalificacion = Nothing

End Function

Private Sub txtCal2_Change()
    
If txtCal2 = "" Then
    'Set oCalif2Seleccionada = Nothing
    'sdbcCal2Cod.Text = ""
    'sdbcCal2Den.Text = ""
    Exit Sub
End If

If Not IsNumeric(txtCal2) Then
    oMensajes.NoValido sIdioma(3)
    txtCal2 = ""
    If Me.Visible Then txtCal2.SetFocus
    Exit Sub
End If
    
Set oCalif2Seleccionada = BuscarCalificacion(txtCal2, oCalificaciones2)

If oCalif2Seleccionada Is Nothing Then
    sdbcCal2Cod.Text = ""
    sdbcCal2Den.Text = ""
Else
    sdbcCal2Cod.Text = oCalif2Seleccionada.Cod
    sdbcCal2Cod_Validate False
End If

End Sub

Private Sub txtCal3_Change()
    
If txtCal3 = "" Then
    'Set oCalif3Seleccionada = Nothing
    'sdbcCal3Cod.Text = ""
    'sdbcCal3Den.Text = ""
    Exit Sub
End If

If Not IsNumeric(txtCal3) Then
    oMensajes.NoValido sIdioma(4)
    txtCal3 = ""
    If Me.Visible Then txtCal3.SetFocus
    Exit Sub
End If
    
Set oCalif3Seleccionada = BuscarCalificacion(txtCal3, oCalificaciones3)

If oCalif3Seleccionada Is Nothing Then
    sdbcCal3Cod.Text = ""
    sdbcCal3Den.Text = ""
Else
    sdbcCal3Cod.Text = oCalif3Seleccionada.Cod
    sdbcCal3Cod_Validate False
End If

End Sub

Private Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    CodGMN1 = oGMN1Seleccionado.Cod
    sdbcGMN2_4Cod = ""
    sdbcGMN2_4Den = ""
    sdbcGMN3_4Cod = ""
    sdbcGMN3_4Den = ""
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod.Text
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    CodGMN1 = oGMN2Seleccionado.GMN1Cod
    CodGMN2 = oGMN2Seleccionado.Cod
    sdbcGMN3_4Cod = ""
    
   
End Sub
Private Sub GMN3Seleccionado()

    Set oGMN3Seleccionado = Nothing
    
    If sdbcGMN3_4Cod.Text = "" Or sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    CodGMN1 = oGMN3Seleccionado.GMN1Cod
    CodGMN2 = oGMN3Seleccionado.GMN2Cod
    CodGMN3 = oGMN3Seleccionado.Cod

     sdbcGMN4_4Cod = ""
End Sub

Private Sub GMN4Seleccionado()
    
    Set oGMN4Seleccionado = Nothing
    
    If sdbcGMN4_4Cod.Text = "" Or sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
    CodGMN1 = oGMN4Seleccionado.GMN1Cod
    CodGMN2 = oGMN4Seleccionado.GMN2Cod
    CodGMN3 = oGMN4Seleccionado.GMN3Cod
    codGMN4 = oGMN4Seleccionado.Cod

    
End Sub
Public Sub PonerMatSeleccionadoEnCombos()

    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4

    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
        
    End If
    
           
End Sub




Private Sub sdbcEqpCod_PositionList(ByVal Text As String)
PositionList sdbcEqpCod, Text
End Sub
Private Sub sdbcEqpCod_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpCod.Text = ""
        sdbcEqpCod.Text = ""
    Else
        EqpRespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        
        sdbcEqpCod.Columns(0).Value = sdbcEqpCod.Text
        sdbcEqpCod.Columns(1).Value = sdbcEqpDen.Text
        
        EqpRespetarCombo = False
        EqpCargarComboDesde = False
    End If
    
    Set oEquipos = Nothing

End Sub

Private Sub sdbcEqpDen_PositionList(ByVal Text As String)
PositionList sdbcEqpDen, Text
End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
    Else
        EqpRespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        
        sdbcEqpDen.Columns(0).Value = sdbcEqpDen.Text
        sdbcEqpDen.Columns(1).Value = sdbcEqpCod.Text
        
        EqpRespetarCombo = False
    End If
    
    Set oEquipos = Nothing


End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    If bRMat Then
        
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
          If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
         Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False)
    Else
         oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
        GMN1RespetarCombo = False
        GMN1Seleccionado
        GMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing

End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN1_4Den_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
          Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, , False)
    Else
         oGMN1s.CargarTodosLosGruposMat , sdbcGMN1_4Den.Text, True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Den.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1_4Den.Columns(0).Value = sdbcGMN1_4Den.Text
        sdbcGMN1_4Den.Columns(1).Value = sdbcGMN1_4Cod.Text
        GMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    
    Set oGMN1s = Nothing

End Sub


Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Public Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
               Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False)
        Else
             oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sdbcGMN2_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
             Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, , Trim(sdbcGMN2_4Den), True, , False)
        Else
             oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4Den.Text, True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Den.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2s.Item(1).Cod
        sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
        sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing

End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Public Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
             Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False)
        Else
              oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
    Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not GMN3RespetarCombo Then

        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Den.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
          Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , True, False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , True, False)
        End If
        
    Else
        
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , True
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , True, False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
             Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, , Trim(sdbcGMN3_4Den), True, , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4Den.Text, True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Den.Text = ""
    Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3s.Item(1).Cod
        sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
        sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Public Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
             Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False)
        Else
             oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub

Private Sub sdbcGMN4_4Den_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, , Trim(sdbcGMN4_4Den), , True, False)
        Else
             oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4Den.Text, True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Den.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4s.Item(1).Cod
        sdbcGMN4_4Den.Columns(0).Value = sdbcGMN4_4Den.Text
        sdbcGMN4_4Den.Columns(1).Value = sdbcGMN4_4Cod.Text
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub
Private Sub sdbcGMN1_4Cod_Change()

    If Not GMN1RespetarCombo Then
    
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub
Private Sub sdbcGMN1_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Den.RemoveAll

    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        
        GMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN2_4Cod.RemoveAll

    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
         Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False)
        End If
    Else
        
         If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not GMN2RespetarCombo Then

        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If

    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer
    
    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
         Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , True)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , True)
        End If
    
    Else
        
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , True
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , True, False
        End If
            
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh

    Screen.MousePointer = vbNormal
End Sub



Private Sub sdbcGMN3_4Cod_Change()
    
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Cod.RemoveAll

    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
         Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False)
        End If
    Else
        
         If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not GMN1RespetarCombo Then

        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
    If Not GMN4RespetarCombo Then
    
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Cod.RemoveAll

    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
         Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False)
        End If
    Else
        
       If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEqpCod_Change()

    If Not EqpRespetarCombo Then
    
        EqpRespetarCombo = True
        sdbcEqpDen.Text = ""
        EqpRespetarCombo = False
        EqpCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN4_4Den_Change()
    
    If Not GMN4RespetarCombo Then

        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Den.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
       Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , True, False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , True, False)
        End If
    Else
        
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , True
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , True, False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcEqpCod_CloseUp()

    If sdbcEqpCod.Value = "..." Then
        sdbcEqpCod.Text = ""
        Exit Sub
    End If
    
    EqpRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    EqpRespetarCombo = False
    
    EqpCargarComboDesde = False
    
End Sub

Private Sub sdbcEqpCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcEqpCod.RemoveAll

    If EqpCargarComboDesde Then
        oEquipos.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , , False
    Else
        oEquipos.CargarTodosLosEquipos , , , , False
    End If
    
    Codigos = oEquipos.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If EqpCargarComboDesde And Not oEquipos.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEqpCod_InitColumnProps()
    
    sdbcEqpCod.DataField = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcEqpDen_Change()

    If Not EqpRespetarCombo Then

        EqpRespetarCombo = True
        sdbcEqpCod.Text = ""
        EqpRespetarCombo = False
        EqpCargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEqpDen_CloseUp()
   
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    EqpRespetarCombo = True
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    EqpRespetarCombo = False
    
    EqpCargarComboDesde = False
    
End Sub

Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcEqpDen.RemoveAll

    If EqpCargarComboDesde Then
        oEquipos.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), , False
    Else
        oEquipos.CargarTodosLosEquipos , , , , False
    End If
    
    Codigos = oEquipos.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If EqpCargarComboDesde And Not oEquipos.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

''' <summary>Reposicionamiento</summary>
''' <remarks>Llamada desde: frmPROVEBuscar.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub Arrange()

    Dim iV As Integer
    Dim vWidth As Integer

    vWidth = 7365

    Dim vsdbgProveedoresH As Integer
    Dim vsstabProveH As Integer
    Dim vcmdAceptarT As Integer
    Dim vcmdAceptarL As Integer
    Dim vcmdCancelarL As Integer
    Dim vsstabProveW As Integer
    Dim vframe1W As Integer
    Dim vfrCalifW As Integer
    Dim vfraSelCompW As Integer
    Dim vfraERPW As Integer
    Dim vfraMatW As Integer
    Dim vsdbgProveedoresW As Integer
    Dim vfraAutoFacturaW As Integer
    
    On Error Resume Next
    
    vframe1W = 6315
    vfrCalifW = 6315
    vfraSelCompW = 6315
    vfraERPW = 6315
    vfraMatW = 6315
    vfraAutoFacturaW = 6315
    vsdbgProveedoresW = 6585
    vsstabProveW = 6855
    
    vcmdCancelarL = 3255
    vcmdAceptarL = 2115
        
    'Material invisible a menos q sOrigen sea ...
    'Equipo invisible a menos q sOrigen sea ...
    'Equipo invisible si Not basParametros.gParametrosGenerales.gbOblProveEqp
    'Autofactura invisible a menos q tenga FSFA
    If (fraSelComp.Visible = False) Then
        If (Me.fraAutofactura.Visible = False) Then
            'altura la da dat gen + calif + material
            vsdbgProveedoresH = 5085 + Me.fraERP.Height
            vsstabProveH = 6075 + Me.fraERP.Height
            vcmdAceptarT = 5640
        Else
            'altura la da autofactura + avanzados
            vsdbgProveedoresH = 5310
            vsstabProveH = 6375
            vcmdAceptarT = 5880
        End If
        If (Me.fraERP.Visible = False) Then
            'altura la da dat gen + calif + material (sin equipo ni erp)
            vsdbgProveedoresH = 5500
            vsstabProveH = 6500
            vcmdAceptarT = 5640
        ElseIf (Me.fraERP.Visible = True) Then
            'altura la da dat gen + calif + material + erp (sin equipo)
            vsdbgProveedoresH = 5085 + Me.fraERP.Height + 100
            vsstabProveH = 6075 + Me.fraERP.Height + 100
            vcmdAceptarT = 5640
        End If
    Else 'ambos visibles
        If (Me.fraERP.Visible = True) Then
            'altura la da dat gen + calif + material + equipo + erp
            vsdbgProveedoresH = 5745 + Me.fraERP.Height
            vsstabProveH = 6795 + Me.fraERP.Height
            vcmdAceptarT = 6300
        Else
            'altura la da dat gen + calif + material + equipo
            vsdbgProveedoresH = 5765
            vsstabProveH = 6815
            vcmdAceptarT = 6300
        End If
        
    End If
    
    If (Me.fraAutofactura.Visible = False) Then
        fraBusqAvanzada.Top = 420
    Else
        fraBusqAvanzada.Top = 1740
    End If
    
    iV = Me.Width - vWidth
    
    frCalif.Top = 2160
    fraMat.Top = 3630
    fraSelComp.Top = 5430
    If fraSelComp.Visible = False Then
        fraERP.Top = 5430
    Else
        fraERP.Top = 5430 + fraSelComp.Height 'Le sumo la altura del frame de equipo
    End If
    
    Me.stabGeneral.Height = vsstabProveH - Me.fraSelComp.Height + 150
    
    sstabProve.Height = vsstabProveH
    sstabProve.Width = vsstabProveW + iV
        
    stabGeneral.Width = stabGeneral.Width + iV
    Frame1.Width = vframe1W + iV

    frCalif.Width = vfrCalifW + iV
    fraSelComp.Width = vfraSelCompW + iV
    fraMat.Width = vfraMatW + iV
    Me.fraAutofactura.Width = vfraAutoFacturaW + iV
    fraBusqAvanzada.Width = Frame1.Width
    sdbgProveedores.Width = vsdbgProveedoresW + iV
    fraERP.Width = vfraERPW + iV
    
    sdbgProveedores.Height = vsdbgProveedoresH
    sdbgProveedores.Columns(0).Width = sdbgProveedores.Width * 0.16
    sdbgProveedores.Columns(1).Width = sdbgProveedores.Width * 0.36
    sdbgProveedores.Columns(2).Width = sdbgProveedores.Width * 0.18
    sdbgProveedores.Columns(3).Width = sdbgProveedores.Width * 0.14
    sdbgProveedores.Columns(4).Width = sdbgProveedores.Width * 0.14
    sdbgProveedores.Columns(5).Width = sdbgProveedores.Width * 0.16
    sdbgProveedores.Columns(6).Width = sdbgProveedores.Width * 0.16

    cmdCancelar.Left = vcmdCancelarL + iV
    cmdAceptar.Left = vcmdAceptarL + iV
    cmdCancelar.Top = vcmdAceptarT
    cmdAceptar.Top = vcmdAceptarT
    cmdAceptar.Left = (sdbgProveedores.Width / 2) - (cmdAceptar.Width / 2) - 300
    cmdCancelar.Left = (sdbgProveedores.Width / 2) + 300
    
    If Me.chkTrasladarProve.Visible Then
        Me.sdbgProveedores.Height = Me.sdbgProveedores.Height - Me.chkTrasladarProve.Height - 10
        Me.sdbgProveedores.Top = Me.chkTrasladarProve.Top + Me.chkTrasladarProve.Height + 10
    End If

End Sub


''' <summary>Evento de cambio del c�digo de proveedor principal </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_Change()
    
   If Not bRespetarComboProvePri Then
    
        bRespetarComboProvePri = True
        sdbcProvePriDen.Text = ""
        bRespetarComboProvePri = False
        
        bCargarComboPriDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre el c�digo de proveedor principal </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_Click()
     
     If Not sdbcProvePriCod.DroppedDown Then
        sdbcProvePriCod = ""
        sdbcProvePriDen = ""
        bCargarComboPriDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">c�digo de proveedor seleccionado</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_PositionList(ByVal Text As String)
PositionList sdbcProvePriCod, Text
End Sub

''' <summary>
''' Validacion del codigo del proveedor
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oProves As CProveedores
    
    If sdbcProvePriCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProvePriCod.Text), , True, , False
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProvePriCod.Text))
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcProvePriCod.Text = ""
    Else
        bRespetarComboProvePri = True
        sdbcProvePriCod.Text = oProves.Item(1).Cod
        sdbcProvePriDen.Text = oProves.Item(1).Den
        
        sdbcProvePriCod.Columns(0).Text = sdbcProvePriCod.Text
        sdbcProvePriCod.Columns(1).Text = sdbcProvePriDen.Text
            
        bRespetarComboProvePri = False
    End If
    bCargarComboPriDesde = True
End Sub

''' <summary>Evento de cambio de denominaci�n de proveedor principal </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_Change()
    
    If Not bRespetarComboProvePri Then
    
        bRespetarComboProvePri = True
        sdbcProvePriCod.Text = ""
        bRespetarComboProvePri = False
        bCargarComboPriDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre la denominaci�n de proveedor principal </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_Click()
    
    If Not sdbcProvePriDen.DroppedDown Then
        sdbcProvePriCod = ""
        sdbcProvePriDen = ""
        bCargarComboPriDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">c�digo de proveedor seleccionado</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_PositionList(ByVal Text As String)
PositionList sdbcProvePriDen, Text
End Sub

''' <summary>
''' Validacion de la denominaci�n del proveedor principal
''' </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oProves As CProveedores
    
    If sdbcProvePriDen.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProvePriDen.Text), True, , False
                    
    bExiste = Not (oProves.Count = 0)
    
    If Not bExiste Then
        sdbcProvePriDen.Text = ""
    Else
        bRespetarComboProvePri = True
        sdbcProvePriCod.Text = oProves.Item(1).Cod
        sdbcProvePriDen.Text = oProves.Item(1).Den
        sdbcProvePriDen.Columns(1).Text = sdbcProvePriCod.Text
        sdbcProvePriDen.Columns(0).Text = sdbcProvePriDen.Text
        
        bRespetarComboProvePri = False
    End If
    Screen.MousePointer = vbNormal
    bCargarComboPriDesde = False
End Sub

''' <summary>Evento closeup del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_CloseUp()

    
    If sdbcProvePriDen.Value = "..." Then
        sdbcProvePriDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProvePriDen.Value = "" Then Exit Sub
    
    bRespetarComboProvePri = True
    sdbcProvePriCod.Text = sdbcProvePriDen.Columns(1).Text
    sdbcProvePriDen.Text = sdbcProvePriDen.Columns(0).Text
    bRespetarComboProvePri = False
    bCargarComboPriDesde = False
    DoEvents
            
End Sub

''' <summary>
''' DropDown del combo de denominacion de proveedores, carga la lista
''' </summary>
'''
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el combo de denominacion de proveedor; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oProves As CProveedores
    
    sdbcProvePriDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
         
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboPriDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProvePriDen.Text), , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboPriDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProvePriDen.Text), , , , , , , , , , , True
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProvePriDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProvePriDen.AddItem "..."
    End If

    sdbcProvePriDen.SelStart = 0
    sdbcProvePriDen.SelLength = Len(sdbcProvePriDen.Text)
    sdbcProvePriCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboPriDesde = False
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriDen_InitColumnProps()
    sdbcProvePriDen.DataFieldList = "Column 0"
    sdbcProvePriDen.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>Evento closeup en el combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_CloseUp()
    
    If sdbcProvePriCod.Value = "..." Then
        sdbcProvePriCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProvePriCod.Value = "" Then Exit Sub
    
    bRespetarComboProvePri = True
    sdbcProvePriDen.Text = sdbcProvePriCod.Columns(1).Text
    sdbcProvePriCod.Text = sdbcProvePriCod.Columns(0).Text
    bRespetarComboProvePri = False
    bCargarComboPriDesde = False
    DoEvents
        
End Sub

''' <summary>
''' DropDown del combo de c�digo de proveedores principales, carga la lista
''' </summary>
'''
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el combo de denominacion de proveedor; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oProves As CProveedores
    
    sdbcProvePriCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboPriDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProvePriCod.Text), , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboPriDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProvePriCod.Text)
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProvePriCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProvePriCod.AddItem "..."
    End If

    sdbcProvePriCod.SelStart = 0
    sdbcProvePriCod.SelLength = Len(sdbcProvePriCod.Text)
    sdbcProvePriCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboPriDesde = False
    
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProvePriCod_InitColumnProps()
    sdbcProvePriCod.DataFieldList = "Column 0"
    sdbcProvePriCod.DataFieldToDisplay = "Column 0"
End Sub



''' <summary>Evento de cambio de c�digo de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_Change()
    
   If Not bRespetarComboProveSub Then
    
        bRespetarComboProveSub = True
        sdbcProveSubDen.Text = ""
        bRespetarComboProveSub = False
        
        bCargarComboSubDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre el c�digo de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_Click()
     
     If Not sdbcProveSubCod.DroppedDown Then
        sdbcProveSubCod = ""
        sdbcProveSubDen = ""
        bCargarComboSubDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">c�digo de proveedor seleccionado</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_PositionList(ByVal Text As String)
PositionList sdbcProveSubCod, Text
End Sub

''' <summary>
''' Validacion del codigo del proveedor
''' </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oProves As CProveedores
    
    If sdbcProveSubCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveSubCod.Text), , True, , False
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveSubCod.Text))
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcProveSubCod.Text = ""
    Else
        bRespetarComboProveSub = True
        sdbcProveSubCod.Text = oProves.Item(1).Cod
        sdbcProveSubDen.Text = oProves.Item(1).Den
        
        sdbcProveSubCod.Columns(0).Text = sdbcProveSubCod.Text
        sdbcProveSubCod.Columns(1).Text = sdbcProveSubDen.Text
            
        bRespetarComboProveSub = False
    End If
    bCargarComboSubDesde = True
End Sub

'' <summary>Evento de cambio de denominaci�n de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_Change()
    
    If Not bRespetarComboProveSub Then
    
        bRespetarComboProveSub = True
        sdbcProveSubCod.Text = ""
        bRespetarComboProveSub = False
        bCargarComboSubDesde = True
    End If
    
End Sub

''' <summary>Evento de click sobre la denominaci�n de proveedor relacionado subordinado </summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_Click()
    
    If Not sdbcProveSubDen.DroppedDown Then
        sdbcProveSubCod = ""
        sdbcProveSubDen = ""
        bCargarComboSubDesde = False
    End If
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">denominaci�n de proveedor seleccionada</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_PositionList(ByVal Text As String)
PositionList sdbcProveSubDen, Text
End Sub

''' <summary>Evento de validaci�n en el combo</summary>
''' <param name="Cancel">Cancela el cambio de columna o fila</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oProves As CProveedores
    
    If sdbcProveSubDen.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProveSubDen.Text), True, , False
                    
    bExiste = Not (oProves.Count = 0)
    
    If Not bExiste Then
        sdbcProveSubDen.Text = ""
    Else
        bRespetarComboProveSub = True
        sdbcProveSubCod.Text = oProves.Item(1).Cod
        sdbcProveSubDen.Text = oProves.Item(1).Den
        sdbcProveSubDen.Columns(1).Text = sdbcProveSubCod.Text
        sdbcProveSubDen.Columns(0).Text = sdbcProveSubDen.Text
        
        bRespetarComboProveSub = False
    End If
    Screen.MousePointer = vbNormal
    bCargarComboSubDesde = False
End Sub

''' <summary>Evento closeup en el combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_CloseUp()

    
    If sdbcProveSubDen.Value = "..." Then
        sdbcProveSubDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveSubDen.Value = "" Then Exit Sub
    
    bRespetarComboProveSub = True
    sdbcProveSubCod.Text = sdbcProveSubDen.Columns(1).Text
    sdbcProveSubDen.Text = sdbcProveSubDen.Columns(0).Text
    bRespetarComboProveSub = False
    bCargarComboSubDesde = False
    DoEvents
            
End Sub

''' <summary>
''' DropDown del combo de denominacion de proveedores, carga la lista
''' </summary>
'''
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Evento que salta al hacer click en el combo de denominacion de proveedor; Tiempo m�ximo: 0</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oProves As CProveedores
    
    sdbcProveSubDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
         
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboSubDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProveSubDen.Text), , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True, , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboSubDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , Trim(sdbcProveSubDen.Text), , , , , , , , , , , True
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , True
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveSubDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProveSubDen.AddItem "..."
    End If

    sdbcProveSubDen.SelStart = 0
    sdbcProveSubDen.SelLength = Len(sdbcProveSubDen.Text)
    sdbcProveSubCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboSubDesde = False
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubDen_InitColumnProps()
    sdbcProveSubDen.DataFieldList = "Column 0"
    sdbcProveSubDen.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>Evento closeup del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_CloseUp()
    
    If sdbcProveSubCod.Value = "..." Then
        sdbcProveSubCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveSubCod.Value = "" Then Exit Sub
    
    bRespetarComboProveSub = True
    sdbcProveSubDen.Text = sdbcProveSubCod.Columns(1).Text
    sdbcProveSubCod.Text = sdbcProveSubCod.Columns(0).Text
    bRespetarComboProveSub = False
    bCargarComboSubDesde = False
    DoEvents
        
End Sub

''' <summary>Evento dropdown del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sEqp As String
    Dim oProves As CProveedores
    
    
    sdbcProveSubCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
        If bCargarComboSubDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProveSubCod.Text), , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, , , , , , , , , , , , , , , , , , , , , , , , , basOptimizacion.gPYMEUsuario, basOptimizacion.gUON1Usuario
        End If
    Else
    
        If bCargarComboSubDesde Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp, Trim(sdbcProveSubCod.Text)
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, sEqp
        End If
        
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveSubCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveSubCod.AddItem "..."
    End If

    sdbcProveSubCod.SelStart = 0
    sdbcProveSubCod.SelLength = Len(sdbcProveSubCod.Text)
    sdbcProveSubCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboSubDesde = False
    
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcProveSubCod_InitColumnProps()
    sdbcProveSubCod.DataFieldList = "Column 0"
    sdbcProveSubCod.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>Evento closeup del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_CloseUp()
    If sdbcTipoRelacSub.Value = "..." Or sdbcTipoRelacSub.Text = "" Then
        sdbcTipoRelacSub.Text = ""
        Exit Sub
    End If
    
    
End Sub

''' <summary>Evento dropdown del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_DropDown()
    
    Dim oTipoRelac As CRelacTipo
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoRelacSub.RemoveAll
    
    oTiposRelac.CargarTodosLosRelacTipos True
    
    For Each oTipoRelac In oTiposRelac
        sdbcTipoRelacSub.AddItem oTipoRelac.Cod & Chr(m_lSeparador) & oTipoRelac.Descripciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den & Chr(m_lSeparador) & oTipoRelac.Id
    Next
        
    sdbcTipoRelacSub.Columns(2).Visible = False
    sdbcTipoRelacSub.SelStart = 0
    sdbcTipoRelacSub.SelLength = Len(sdbcTipoRelacSub.Text)
    sdbcTipoRelacSub.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_InitColumnProps()
    sdbcTipoRelacSub.DataFieldList = "Column 1"
    sdbcTipoRelacSub.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">denominaci�n de proveedor seleccionada</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_PositionList(ByVal Text As String)
PositionList sdbcTipoRelacSub, Text, 1
End Sub

''' <summary>Evento de validaci�n en el combo</summary>
''' <param name="Cancel">Cancela el cambio de columna o fila</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacSub_Validate(Cancel As Boolean)
    
    If sdbcTipoRelacSub.Text = "" Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    oTiposRelac.CargarTodosLosRelacTipos True

    If oTiposRelac.Count = 0 Then
        sdbcTipoRelacSub.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdioma(1)
    End If

    Screen.MousePointer = vbNormal
End Sub

''' <summary>Evento closeup del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_CloseUp()
    If sdbcTipoRelacPri.Value = "..." Or sdbcTipoRelacPri.Text = "" Then
        sdbcTipoRelacPri.Text = ""
        Exit Sub
    End If
   
   
End Sub

''' <summary>Evento dropdown del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_DropDown()
    
    Dim oTipoRelac As CRelacTipo
    
    Screen.MousePointer = vbHourglass
    
    sdbcTipoRelacPri.RemoveAll
    
    oTiposRelac.CargarTodosLosRelacTipos True
    
    For Each oTipoRelac In oTiposRelac
        sdbcTipoRelacPri.AddItem oTipoRelac.Cod & Chr(m_lSeparador) & oTipoRelac.Descripciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den & Chr(m_lSeparador) & oTipoRelac.Id
    Next
        
    sdbcTipoRelacPri.SelStart = 0
    sdbcTipoRelacPri.SelLength = Len(sdbcTipoRelacPri.Text)
    sdbcTipoRelacPri.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>Inicializaci�n de las propiedades de las columnas del combo</summary>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_InitColumnProps()
    sdbcTipoRelacPri.DataFieldList = "Column 1"
    sdbcTipoRelacPri.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>Evento de posicionamiento en el combo seg�n la selecci�n</summary>
''' <param name="Text">denominaci�n de proveedor seleccionada</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_PositionList(ByVal Text As String)
PositionList sdbcTipoRelacPri, Text, 1
End Sub

''' <summary>Evento de validaci�n en el combo</summary>
''' <param name="Cancel">Cancela el cambio de columna o fila</param>
''' <remarks>Llamada desde:frmProveBuscar; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 30/08/2011</revision>
Private Sub sdbcTipoRelacPri_Validate(Cancel As Boolean)
    
    If sdbcTipoRelacPri.Text = "" Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    oTiposRelac.CargarTodosLosRelacTipos True

    If oTiposRelac.Count = 0 Then
        sdbcTipoRelacPri.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sLiteralTipoRel
    End If

    Screen.MousePointer = vbNormal
End Sub


Public Property Get seleccionarTodos() As Boolean
    seleccionarTodos = IIf(Me.chkTrasladarProve.Value = 1, True, False)
End Property

Private Sub CargarCodigosERP(ByVal Cod As String)
    Dim AdorERP As Ador.Recordset
    Dim oProve As CProveedores
    Set oProve = Nothing
    Set oProve = oFSGSRaiz.generar_CProveedores
    
    Set AdorERP = oProve.DevolverCodERPProve(Cod)
    
    sdbgProveERP.RemoveAll
    If AdorERP.EOF Then
        AdorERP.Close
        Set AdorERP = Nothing
        Exit Sub
    Else
        While Not AdorERP.EOF
            sdbgProveERP.AddItem NullToStr(AdorERP.Fields("DEN").Value) & Chr(m_lSeparador) & NullToStr(AdorERP.Fields("COD_ERP").Value) & Chr(m_lSeparador) & NullToStr(AdorERP.Fields("DEN_ERP").Value)
            AdorERP.MoveNext
        Wend
        AdorERP.Close
        Set AdorERP = Nothing
        Exit Sub
    End If
End Sub
