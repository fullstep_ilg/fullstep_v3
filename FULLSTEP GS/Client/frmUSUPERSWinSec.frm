VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmUSUPERSWinSec 
   BackColor       =   &H00808000&
   Caption         =   "A�adir usuario windows"
   ClientHeight    =   10050
   ClientLeft      =   1485
   ClientTop       =   1770
   ClientWidth     =   7890
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmUSUPERSWinSec.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   10050
   ScaleWidth      =   7890
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   2715
      Left            =   120
      TabIndex        =   7
      Top             =   1890
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   4789
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   0
      ScaleHeight     =   465
      ScaleWidth      =   7890
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   9585
      Width           =   7890
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   2985
         TabIndex        =   8
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   4125
         TabIndex        =   9
         Top             =   75
         Width           =   1005
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1845
      Left            =   120
      ScaleHeight     =   1845
      ScaleWidth      =   7710
      TabIndex        =   10
      Top             =   30
      Width           =   7710
      Begin VB.CheckBox chkAdmin 
         BackColor       =   &H00808000&
         Caption         =   "DAdministrador"
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   0
         TabIndex        =   19
         Top             =   0
         Width           =   2055
      End
      Begin VB.CommandButton cmdRuta 
         Caption         =   "..."
         Height          =   285
         Left            =   6810
         TabIndex        =   18
         Top             =   1080
         Width           =   330
      End
      Begin VB.CheckBox chkChangeIni 
         BackColor       =   &H00808000&
         Caption         =   "El usuario debe cambiar la contrase�a en el pr�ximo inicio de sesi�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   465
         Left            =   3630
         TabIndex        =   3
         Top             =   270
         Width           =   4125
      End
      Begin VB.CheckBox chkDeshab 
         BackColor       =   &H00808000&
         Caption         =   "Cuenta deshabilitada"
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   3630
         TabIndex        =   4
         Top             =   690
         Width           =   2055
      End
      Begin VB.CheckBox chkBloq 
         BackColor       =   &H00808000&
         Caption         =   "Cuenta bloqueada"
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   5745
         TabIndex        =   5
         Top             =   690
         Width           =   2100
      End
      Begin VB.TextBox txtPWDConf 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1110
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   1080
         Width           =   2355
      End
      Begin VB.TextBox txtPWD 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1110
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   735
         Width           =   2355
      End
      Begin VB.TextBox txtCod 
         Height          =   285
         Left            =   1110
         MaxLength       =   50
         TabIndex        =   0
         Top             =   390
         Width           =   1680
      End
      Begin VB.Label lblPwd 
         BackStyle       =   0  'Transparent
         Caption         =   "Contrase�a:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   0
         TabIndex        =   17
         Top             =   795
         Width           =   900
      End
      Begin VB.Label lblCodigo 
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   16
         Top             =   420
         Width           =   975
      End
      Begin VB.Label lblNombre 
         BackColor       =   &H00808000&
         Caption         =   "Persona:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   15
         Top             =   1485
         Width           =   900
      End
      Begin VB.Label lblPwdConf 
         BackStyle       =   0  'Transparent
         Caption         =   "Confirmaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   0
         TabIndex        =   14
         Top             =   1140
         Width           =   975
      End
      Begin VB.Label lblRuta 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "Ruta por defecto para gesti�n de archivos:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3630
         TabIndex        =   13
         Top             =   1140
         Width           =   3105
      End
      Begin VB.Label lblPersona 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1110
         TabIndex        =   6
         Top             =   1440
         Width           =   5100
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3780
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUPERSWinSec.frx":0CB2
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUPERSWinSec.frx":107A
            Key             =   "Persona"
            Object.Tag             =   "Persona"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUPERSWinSec.frx":118E
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUPERSWinSec.frx":14E2
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUPERSWinSec.frx":1836
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUPERSWinSec.frx":1B8A
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUPERSWinSec.frx":1F1E
            Key             =   "COMP"
            Object.Tag             =   "COMP"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUPERSWinSec.frx":2028
            Key             =   "PERSASIG"
            Object.Tag             =   "PERSASIG"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUPERSWinSec.frx":234A
            Key             =   "COMPASIG"
            Object.Tag             =   "COMPASIG"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picCom 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4815
      Left            =   120
      ScaleHeight     =   4815
      ScaleWidth      =   7695
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   4725
      Width           =   7695
      Begin VB.PictureBox picEquipo 
         Appearance      =   0  'Flat
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   435
         Left            =   0
         ScaleHeight     =   435
         ScaleWidth      =   7485
         TabIndex        =   42
         Top             =   270
         Width           =   7485
         Begin SSDataWidgets_B.SSDBCombo sdbcEquipo 
            Height          =   285
            Left            =   2220
            TabIndex        =   43
            Top             =   0
            Width           =   4695
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   2037
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "CodEqp"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6271
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DenEqp"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "CodDenEqp"
            Columns(2).Name =   "CodDenEqp"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   8281
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 2"
         End
         Begin VB.Label lblEquipo 
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   2220
            TabIndex        =   45
            Top             =   0
            Visible         =   0   'False
            Width           =   4695
         End
         Begin VB.Label lblEqp 
            BackColor       =   &H00808000&
            Caption         =   "DPertenece a equipo:"
            ForeColor       =   &H00FFFFFF&
            Height          =   345
            Left            =   0
            TabIndex        =   44
            Top             =   30
            Width           =   2115
         End
      End
      Begin VB.PictureBox picPerfil 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   0
         ScaleHeight     =   435
         ScaleWidth      =   7485
         TabIndex        =   20
         Top             =   660
         Width           =   7485
         Begin SSDataWidgets_B.SSDBCombo sdbcPerfil 
            Height          =   285
            Left            =   2220
            TabIndex        =   21
            Top             =   90
            Width           =   4695
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   12
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DC�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "DDenominaci�n"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FSGS"
            Columns(3).Name =   "FSGS"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   11
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FSPM"
            Columns(4).Name =   "FSPM"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   11
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "FSQA"
            Columns(5).Name =   "FSQA"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   11
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "FSEP"
            Columns(6).Name =   "FSEP"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   11
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "FSSM"
            Columns(7).Name =   "FSSM"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   11
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "FSCM"
            Columns(8).Name =   "FSCM"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   11
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "FSIM"
            Columns(9).Name =   "FSIM"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   11
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "FSIS"
            Columns(10).Name=   "FSIS"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   11
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "FSBI"
            Columns(11).Name=   "FSBI"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   11
            Columns(11).FieldLen=   256
            _ExtentX        =   8281
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 2"
         End
         Begin VB.Label lblPerf 
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   2220
            TabIndex        =   40
            Top             =   90
            Visible         =   0   'False
            Width           =   4695
         End
         Begin VB.Label lblPerfil 
            BackColor       =   &H00808000&
            Caption         =   "DPerfil de seguridad:"
            ForeColor       =   &H00FFFFFF&
            Height          =   345
            Left            =   0
            TabIndex        =   22
            Top             =   120
            Width           =   2115
         End
      End
      Begin VB.PictureBox picAccesos 
         Appearance      =   0  'Flat
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3525
         Left            =   0
         ScaleHeight     =   3525
         ScaleWidth      =   7665
         TabIndex        =   23
         Top             =   1230
         Width           =   7665
         Begin VB.PictureBox picFSBI 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   2220
            ScaleHeight     =   405
            ScaleMode       =   0  'User
            ScaleWidth      =   5265
            TabIndex        =   46
            Top             =   3240
            Width           =   5265
            Begin VB.CheckBox chkFSBI 
               BackColor       =   &H00808000&
               Caption         =   "DAcceso a sistema FSBI (Inteligencia de negocio)"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   0
               TabIndex        =   47
               Top             =   0
               Width           =   4845
            End
         End
         Begin VB.PictureBox picFSSM 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   2220
            ScaleHeight     =   405
            ScaleWidth      =   5115
            TabIndex        =   38
            Top             =   1650
            Width           =   5115
            Begin VB.CheckBox chkFSSM 
               BackColor       =   &H00808000&
               Caption         =   "DAcceso a sistema FSSM (Presupuestos)"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   0
               TabIndex        =   39
               Top             =   0
               Width           =   3500
            End
         End
         Begin VB.PictureBox picFSEP 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   2220
            ScaleHeight     =   405
            ScaleWidth      =   5325
            TabIndex        =   36
            Top             =   825
            Width           =   5325
            Begin VB.CheckBox chkFSEP 
               BackColor       =   &H00808000&
               Caption         =   "DAcceso al Electronic Procurement (FSEP)"
               ForeColor       =   &H00FFFFFF&
               Height          =   225
               Left            =   0
               TabIndex        =   37
               Top             =   0
               Width           =   5115
            End
         End
         Begin VB.PictureBox picFSPM 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   2220
            ScaleHeight     =   405
            ScaleWidth      =   5055
            TabIndex        =   34
            Top             =   0
            Width           =   5055
            Begin VB.CheckBox chkFSPM 
               BackColor       =   &H00808000&
               Caption         =   "DAcceso a sistema FSPM (Solicitudes)"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   0
               TabIndex        =   35
               Top             =   0
               Width           =   3500
            End
         End
         Begin VB.PictureBox picFSIS 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   2220
            ScaleHeight     =   405
            ScaleMode       =   0  'User
            ScaleWidth      =   5265
            TabIndex        =   32
            Top             =   2850
            Width           =   5265
            Begin VB.CheckBox chkFSIS 
               BackColor       =   &H00808000&
               Caption         =   "DAcceso a sistema FSIS (Integraci�n)"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   0
               TabIndex        =   33
               Top             =   0
               Width           =   4845
            End
         End
         Begin VB.PictureBox picFSCM 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   2220
            ScaleHeight     =   405
            ScaleWidth      =   5115
            TabIndex        =   30
            Top             =   1230
            Width           =   5115
            Begin VB.CheckBox chkFSCM 
               BackColor       =   &H00808000&
               Caption         =   "DAcceso a sistema FSCM (Contratos)"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   0
               TabIndex        =   31
               Top             =   0
               Width           =   3500
            End
         End
         Begin VB.PictureBox picFSIM 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   2220
            ScaleHeight     =   405
            ScaleWidth      =   5205
            TabIndex        =   28
            Top             =   2055
            Width           =   5205
            Begin VB.CheckBox chkFSIM 
               BackColor       =   &H00808000&
               Caption         =   "DAcceso a sistema FSIM (Facturaci�n)"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   0
               TabIndex        =   29
               Top             =   0
               Width           =   3250
            End
         End
         Begin VB.PictureBox picFSQA 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   2220
            ScaleHeight     =   405
            ScaleWidth      =   5025
            TabIndex        =   26
            Top             =   2445
            Width           =   5025
            Begin VB.CheckBox chkFSQA 
               BackColor       =   &H00808000&
               Caption         =   "DAcceso a sistema FSQA (Calidad)"
               ForeColor       =   &H00FFFFFF&
               Height          =   255
               Left            =   0
               TabIndex        =   27
               Top             =   0
               Width           =   3250
            End
         End
         Begin VB.PictureBox picFSGS 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   2220
            ScaleHeight     =   405
            ScaleWidth      =   5235
            TabIndex        =   24
            Top             =   420
            Width           =   5235
            Begin VB.CheckBox chkFSGS 
               BackColor       =   &H00808000&
               Caption         =   "DAcceso a sistema FSGS (Compras)"
               ForeColor       =   &H00FFFFFF&
               Height          =   225
               Left            =   0
               TabIndex        =   25
               Top             =   0
               Width           =   5115
            End
         End
         Begin VB.Label lblAcceso 
            BackColor       =   &H00808000&
            Caption         =   "DAcceso a los m�dulos:"
            ForeColor       =   &H00FFFFFF&
            Height          =   345
            Left            =   0
            TabIndex        =   41
            Top             =   0
            Width           =   2115
         End
      End
      Begin VB.Line Line3 
         BorderColor     =   &H80000005&
         X1              =   0
         X2              =   7695
         Y1              =   105
         Y2              =   105
      End
   End
End
Attribute VB_Name = "frmUSUPERSWinSec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnPicHeight As Integer = 400

Public g_oUsuario As CUsuario
Public g_iAccion As Integer

Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String
Private m_sDep As String
Private m_vRutaLocal As Variant
Private m_vRutaUnidad As Variant
Private m_vUnidad As Variant
Private m_oPer As CPersona  'Aqui voy a mantener la persona elegida en el arbol.
Private m_oIBaseDatos As IBaseDatos
Private m_sNombre As String
Private m_sIdioma() As String
Private m_sConfPwd As String
Private m_bRespetarCombo As Boolean
Private m_bNoModificarTieneSobres As Boolean
Private m_bChanging As Boolean
Private m_bResizing As Boolean

Private Sub chkAdmin_Click()
    If chkAdmin.Value = vbChecked Then
        AsignarPerfilAdmin
        
        picPerfil.Enabled = False
        sdbcPerfil.Backcolor = &HC0C0C0
        lblPerf.Backcolor = &HC0C0C0
    Else
        sdbcPerfil.Text = ""
        sdbcPerfil.Value = Null
        CargarAccesos False, False, False, False, False, False, False, False, False, False
        
        picPerfil.Enabled = True
        sdbcPerfil.Backcolor = &HFFFFFF
        lblPerf.Backcolor = &HFFFFFF
    End If
End Sub

''' <summary>Asigna el perfil de Administrador</summary>
''' <remarks>Llamada desde:frmLstUSUPERSWinSec.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 18/12/2012</revision>
Private Sub AsignarPerfilAdmin()
    Dim oPerfiles As CPerfiles
    
    Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfiles(, , , , , , True)
    If Not oPerfiles Is Nothing Then
        If oPerfiles.Count > 0 Then
            AsignarPerfil oPerfiles.Item(1), True
        End If
    End If
End Sub

''' <summary>Asigna el perfil de Administrador</summary>
''' <param name="oPerfil">Objeto con los datos del perfil</param>
''' <remarks>Llamada desde: MostrarDatos, AsignarPerfilAdmin</remarks>
''' <revision>LTG 18/12/2012</revision>
Private Sub AsignarPerfil(ByVal oPerfil As CPerfil, ByVal bEdit As Boolean)
    If Not oPerfil Is Nothing Then
        picAccesos.Enabled = False
        
        With oPerfil
            lblPerf.caption = .Den
            
            sdbcPerfil.Value = .Id
            sdbcPerfil.Text = .Den
            sdbcPerfil.Columns("COD").Value = .Cod
            sdbcPerfil.Columns("DEN").Value = .Den
            sdbcPerfil.Columns("FSGS").Value = .FSGS
            sdbcPerfil.Columns("FSPM").Value = .FSPM
            sdbcPerfil.Columns("FSQA").Value = .FSQA
            sdbcPerfil.Columns("FSEP").Value = .FSEP
            sdbcPerfil.Columns("FSSM").Value = .FSSM
            sdbcPerfil.Columns("FSCM").Value = .FSCM
            sdbcPerfil.Columns("FSIS").Value = .FSIS
            sdbcPerfil.Columns("FSIM").Value = .FSIM
            sdbcPerfil.Columns("FSBI").Value = .FSBI
            
            CargarAccesos .FSGS, .FSPM, .FSQA, .FSEP, .FSSM, .FSCM, .FSIS, .FSIM, .FSBI, bEdit
        End With
    End If
End Sub

Private Sub cmdAceptar_Click()
    Dim oUsuario As CUsuario
    Dim nodx As node
    Dim teserror As TipoErrorSummit
    Dim dfechahoracrypt As Date
    Dim irespuesta As Integer
    Dim sPassUsu As String
    Dim sAux As String
    Dim i As Integer
    Dim iEstadoBaja As Integer
    Dim bCambiarPwd As Boolean
    Dim oPerf As CPerfil
    
    teserror.NumError = TESnoerror
    
    ' Validar datos
    If Trim(txtCod) = "" Then
        oMensajes.NoValido m_sIdioma(1)
        If Me.Visible Then txtCod.SetFocus
        Exit Sub
    End If
    
    'Compruebo si hay seguridad de windows (y LDAP) o no para comprobar contrase�a
    If Not (gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurityWeb = Windows) _
    And Not (gParametrosGenerales.giWinSecurity = LDAP Or gParametrosGenerales.giWinSecurityWeb = LDAP) Then
        If Trim(txtPWD) = "" Then
            oMensajes.NoValido m_sIdioma(2)
            If Me.Visible Then txtPWD.SetFocus
            Exit Sub
        End If
        If Trim(txtPWDConf) = "" Then
            oMensajes.NoValido lblPwdConf.caption
            If Me.Visible Then txtPWDConf.SetFocus
            Exit Sub
        End If
        If txtPWD.Text <> txtPWDConf.Text Then
            oMensajes.NoValida m_sConfPwd
            If Me.Visible Then txtPWDConf.SetFocus
            Exit Sub
        End If
    ElseIf (gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurityWeb = Windows) _
        Or (gParametrosGenerales.giWinSecurity = LDAP Or gParametrosGenerales.giWinSecurityWeb = LDAP) Then
        If txtPWD.Text <> txtPWDConf.Text Then
            oMensajes.NoValida m_sConfPwd
            If Me.Visible Then txtPWDConf.SetFocus
            Exit Sub
        End If
    End If
    
    Set nodx = tvwestrorg.selectedItem
    
    If nodx Is Nothing Then
        oMensajes.FaltanDatos m_sIdioma(3)
        If Me.Visible Then tvwestrorg.SetFocus
        Exit Sub
    End If
    
    If Left(nodx.Tag, 3) <> "PER" Then
        oMensajes.FaltanDatos m_sIdioma(3)
        If Me.Visible Then tvwestrorg.SetFocus
        Exit Sub
    End If
    
    m_sNombre = DevolverCod(nodx)
    If m_sNombre = "Error" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Select Case frmUSUARIOS.Accion
    
        Case ACCUsuAnya
                
            If m_oPer Is Nothing Then
                oMensajes.FaltanDatos m_sIdioma(3)
                If Me.Visible Then tvwestrorg.SetFocus
                Exit Sub
            End If
    
            Set oUsuario = oFSGSRaiz.generar_cusuario
            
            dfechahoracrypt = Now
            
            'Compruebo que no hay autenticaci�n de windows (o de LDAP) para que recoja las contrase�as
            If Not ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) _
            And (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP)) Then
                If Trim(txtPWD.Text) <> Trim(txtPWDConf.Text) Then
                    oMensajes.NoValida m_sConfPwd
                    If Me.Visible Then txtPWDConf.SetFocus
                    Exit Sub
                End If
                        
                Screen.MousePointer = vbHourglass
                
'                'Compruebo que hay autenticaci�n de windows (o de LDAP) en Gs o WEB, o que no la hay para coger la contrase�a y encriptarla
'                If ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) Or (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP) And txtPWD.Text <> "") _
'                Or ((gParametrosGenerales.giWinSecurity <> Windows And gParametrosGenerales.giWinSecurity <> LDAP) And (gParametrosGenerales.giWinSecurityWeb <> Windows And gParametrosGenerales.giWinSecurityWeb <> LDAP)) Then
                    For i = 1 To Len(txtCod.Text)
                        If InStr(1, FSGSLibrary.sAuxMatrixCodigoValido, Mid(txtCod.Text, i, 1)) = 0 Then
                            oMensajes.CodUsuarioNoValido (Mid(txtCod.Text, i, 1))
                            Screen.MousePointer = vbNormal
                            If Me.Visible Then txtCod.SetFocus
                            Exit Sub
                        End If
                    Next
                    
                    ''' Principio de encriptaci�n de la contrase�a
                    sAux = Trim(txtCod.Text) & Format(dfechahoracrypt, "DD\/MM\/YYYY HH\:NN\:SS")
                    For i = 1 To Len(sAux)
                        If InStr(1, FSGSLibrary.sAuxMatrix, Mid(sAux, i, 1)) = 0 Then
                            oMensajes.CodUsuarioNoValido (Mid(sAux, i, 1))
                            Screen.MousePointer = vbNormal
                            If Me.Visible Then txtCod.SetFocus
                            Exit Sub
                        End If
                    Next
                    
                    sPassUsu = FSGSLibrary.EncriptarAES(Trim(txtCod.Text), Trim(txtPWD.Text), True, dfechahoracrypt, 1, TipoDeUsuario.Persona)
                
                    'Datos
                    oUsuario.Pwd = sPassUsu
                    oUsuario.fecpwd = dfechahoracrypt
                    oUsuario.PWDDes = Trim(txtPWD.Text)
'                End If
            End If
            
            oUsuario.FecUsu = dfechahoracrypt
            If chkAdmin <> vbUnchecked Then
                oUsuario.ADM = FSGSLibrary.EncriptarAES(Trim(txtCod.Text), Trim(txtCod.Text), True, dfechahoracrypt, 1, TipoDeUsuario.Administrador)
            Else
                oUsuario.ADM = FSGSLibrary.EncriptarAES(Trim(txtCod.Text), Trim(txtCod.Text), True, dfechahoracrypt, 1, TipoDeUsuario.Persona)
            End If
        
            'Datos
            oUsuario.Cod = Trim(txtCod)
            oUsuario.Bloq = 0
            If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
                oUsuario.CuentaBloqueada = False
                If chkChangeIni.Value = vbChecked Then
                    oUsuario.CuentaDebeCambiarPwd = True
                Else
                    oUsuario.CuentaDebeCambiarPwd = False
                End If
                oUsuario.CuentaNoCambiarPwd = True
                oUsuario.CuentaNuncaExpira = True
                If chkDeshab.Value = vbChecked Then
                    oUsuario.CuentaDeshabilitada = True
                Else
                    oUsuario.CuentaDeshabilitada = False
                End If
            End If
            
            'Perfil
            If sdbcPerfil.Text <> "" Then
                Set oPerf = oFSGSRaiz.Generar_CPerfil
                oPerf.Id = CLng(sdbcPerfil.Value)
                oPerf.Cod = sdbcPerfil.Columns("COD").Value
                oPerf.Den = sdbcPerfil.Columns("DEN").Value
                oPerf.FSBI = chkFSBI.Value
                oPerf.FSCM = chkFSCM.Value
                oPerf.FSEP = chkFSEP.Value
                oPerf.FSGS = chkFSGS.Value
                oPerf.FSIM = chkFSIM.Value
                oPerf.FSIS = chkFSIS.Value
                oPerf.FSPM = chkFSPM.Value
                oPerf.FSQA = chkFSQA.Value
                oPerf.FSSM = chkFSSM.Value
                Set oUsuario.Perfil = oPerf
            Else
                Set oUsuario.Perfil = Nothing
                oUsuario.AccesoFSEP = chkFSEP.Value
                oUsuario.AccesoFSGS = chkFSGS.Value
                oUsuario.AccesoFSWS = chkFSPM.Value
                oUsuario.AccesoFSQA = chkFSQA.Value
                oUsuario.AccesoFSINT = chkFSIS.Value
                oUsuario.AccesoFSContratos = chkFSCM.Value
                oUsuario.AccesoFSIM = chkFSIM.Value
                oUsuario.AccesoFSSM = chkFSSM.Value
                oUsuario.AccesoFSBI = chkFSBI.Value
            End If
            
            Set oUsuario.Persona = m_oPer
            
            'Equipo
            If sdbcEquipo.Text <> "" Then
                If IsNull(m_oPer.codEqp) Then 'A�adir FC
                    irespuesta = oMensajes.A�adirFunComp
                    If irespuesta = vbNo Then
                        Screen.MousePointer = vbNormal
                        Set oUsuario = Nothing
                        Exit Sub
                    End If
                Else
                    If oUsuario.Persona.codEqp <> sdbcEquipo.Columns(0).Value Then
                        irespuesta = oMensajes.ModifFunComp
                        If irespuesta = vbNo Then
                            Screen.MousePointer = vbNormal
                            Set oUsuario = Nothing
                            Exit Sub
                        End If
                    End If
                End If
                oUsuario.Persona.codEqp = sdbcEquipo.Columns(0).Value
                oUsuario.Tipo = 2
            Else
                If Not IsNull(m_oPer.codEqp) Then
                    irespuesta = oMensajes.PreguntaEliminarFunComp
                    If irespuesta = vbYes Then
                        oUsuario.Persona.codEqp = Null
                        oUsuario.Tipo = 3
                    Else
                        oUsuario.Tipo = 2
                        oUsuario.AccesoFSGS = True
                    End If
                Else
                    oUsuario.Tipo = 3
                End If
            End If
            
            If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
                If IsEmpty(m_vRutaLocal) Then
                    If gParametrosGenerales.gsTSHomeFolderRutaLocal <> "" Then
                        m_vUnidad = Null
                        m_vRutaUnidad = Null
                        m_vRutaLocal = gParametrosGenerales.gsTSHomeFolderRutaLocal
                    ElseIf gParametrosGenerales.gsTSHomeFolderRutaUnidad <> "" Then
                        m_vRutaLocal = Null
                        m_vUnidad = gParametrosGenerales.gsTSHomeFolderUnidad
                        m_vRutaUnidad = gParametrosGenerales.gsTSHomeFolderRutaUnidad
                    End If
                End If
                oUsuario.TSRutaLocal = m_vRutaLocal
                oUsuario.TSUnidad = m_vUnidad
                oUsuario.TSRutaUnidad = m_vRutaUnidad
            Else
                oUsuario.TSRutaLocal = Null
                oUsuario.TSUnidad = Null
                oUsuario.TSRutaUnidad = Null
            End If
            
            ''Si la persona ya estuvo relacionada a un usuario hay que recuperar ese
            iEstadoBaja = oUsuario.ComprobarUsuarioBaja(m_oPer.Cod)
            If iEstadoBaja = 1 Then
                oMensajes.CodigoUsarioBaja
                Screen.MousePointer = vbNormal
                Set oUsuario = Nothing
                Exit Sub
            ElseIf iEstadoBaja = 2 Then
                If oMensajes.RecuperarUsuarioBaja(oUsuario.Cod) = vbNo Then
                    Screen.MousePointer = vbNormal
                    Set oUsuario = Nothing
                    Exit Sub
                Else
                    Set oUsuario.Persona = m_oPer
                    
                    'Si se recupera el usuario encriptar la contrase�a con el c�digo del usuario recuperado
                    sPassUsu = FSGSLibrary.EncriptarAES(Trim(oUsuario.Cod), Trim(txtPWD.Text), True, dfechahoracrypt, 1, TipoDeUsuario.Persona)
                    'Datos
                    oUsuario.Pwd = sPassUsu
                    If chkAdmin <> vbUnchecked Then
                        oUsuario.ADM = FSGSLibrary.EncriptarAES(Trim(oUsuario.Cod), Trim(oUsuario.Cod), True, dfechahoracrypt, 1, TipoDeUsuario.Administrador)
                    Else
                        oUsuario.ADM = FSGSLibrary.EncriptarAES(Trim(oUsuario.Cod), Trim(oUsuario.Cod), True, dfechahoracrypt, 1, TipoDeUsuario.Persona)
                    End If
                    
                    teserror = oUsuario.RecuperarBaja(m_oPer.Cod)
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        TratarError teserror
                        Exit Sub
                    End If
                    
                    oUsuario.CargarDatosUsuario
    
                    Set frmUSUARIOS.g_oUsuario = oUsuario
    
                    Screen.MousePointer = vbNormal
                    
                    Set nodx = Nothing
                    Unload Me
                    Exit Sub
                End If
            End If
            
            ' Utilizo el interface de BD para guardar el usuario
            Set m_oIBaseDatos = oUsuario
            m_bChanging = True
            teserror = m_oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                m_bChanging = False
                Exit Sub
            End If
            m_bChanging = False
            Set frmUSUARIOS.g_oUsuario = oUsuario
            
        Case ACCUsuMod
                    
            Screen.MousePointer = vbHourglass
            
            If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
                If frmUSUARIOS.g_oUsuario.PWDDes <> txtPWD.Text Then
                    bCambiarPwd = True
                End If
                
                'Compruebo que no hay autenticaci�n de windows (ni de LDAP) para que recoja las contrase�as
                If Not ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) _
                And (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP)) Then
                    If txtPWD.Text <> txtPWDConf.Text Then
                        oMensajes.NoValida m_sConfPwd
                        If Me.Visible Then txtPWDConf.SetFocus
                        Exit Sub
                    End If
                End If
                
                ''' Principio de encriptaci�n de la contrase�a
                'Compruebo que hay autenticaci�n de windows (o de LDAP) en Gs o WEB, o que no la hay para coger la contrase�a y encriptarla
                If ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) Or (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP) And txtPWD.Text <> "") _
                    Or ((gParametrosGenerales.giWinSecurity <> Windows And gParametrosGenerales.giWinSecurity <> LDAP) And (gParametrosGenerales.giWinSecurityWeb <> Windows And gParametrosGenerales.giWinSecurityWeb <> LDAP)) Then
                    For i = 1 To Len(txtCod.Text)
                        If InStr(1, FSGSLibrary.sAuxMatrixCodigoValido, Mid(txtCod.Text, i, 1)) = 0 Then
                            oMensajes.CodUsuarioNoValido (Mid(txtCod.Text, i, 1))
                            Screen.MousePointer = vbNormal
                            If Me.Visible Then txtCod.SetFocus
                            Exit Sub
                        End If
                    Next
        
                    dfechahoracrypt = Now
                    
                    sAux = txtCod.Text & Format(dfechahoracrypt, "DD\/MM\/YYYY HH\:NN\:SS")
                    For i = 1 To Len(sAux)
                        If InStr(1, FSGSLibrary.sAuxMatrix, Mid(sAux, i, 1)) = 0 Then
                            oMensajes.CodUsuarioNoValido (Mid(sAux, i, 1))
                            Screen.MousePointer = vbNormal
                            If Me.Visible Then txtCod.SetFocus
                            Exit Sub
                        End If
                    Next
                    
                    sPassUsu = FSGSLibrary.EncriptarAES(txtCod.Text, txtPWD.Text, True, dfechahoracrypt, 1, TipoDeUsuario.Persona)
                Else
                    g_oUsuario.Pwd = ""
                    'g_oUsuario.fecpwd = ""
                    g_oUsuario.PWDDes = ""
                End If
        
                If chkChangeIni.Value = vbChecked Then
                    g_oUsuario.CuentaDebeCambiarPwd = True
                Else
                    g_oUsuario.CuentaDebeCambiarPwd = False
                End If
                g_oUsuario.CuentaNoCambiarPwd = True
                g_oUsuario.CuentaNuncaExpira = True
                If chkDeshab.Value = vbChecked Then
                    g_oUsuario.CuentaDeshabilitada = True
                Else
                    g_oUsuario.CuentaDeshabilitada = False
                End If
                
                If chkBloq.Value = vbChecked Then
                    g_oUsuario.CuentaBloqueada = True
                Else
                    g_oUsuario.CuentaBloqueada = False
                End If
            Else
                'La contrase�a no aparece si la validaci�n es Windows o LDAP, tanto en GS como en Web
                If Not ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) _
                And (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP)) Then
                     If frmUSUARIOS.g_oUsuario.PWDDes <> txtPWD.Text Then
                        bCambiarPwd = True
                    End If
                End If
                
                For i = 1 To Len(txtCod.Text)
                    If InStr(1, FSGSLibrary.sAuxMatrixCodigoValido, Mid(txtCod.Text, i, 1)) = 0 Then
                        oMensajes.CodUsuarioNoValido (Mid(txtCod.Text, i, 1))
                        Screen.MousePointer = vbNormal
                        If Me.Visible Then txtCod.SetFocus
                        Exit Sub
                    End If
                Next
                              
                sAux = txtCod.Text & Format(dfechahoracrypt, "DD\/MM\/YYYY HH\:NN\:SS")
                For i = 1 To Len(sAux)
                    If InStr(1, FSGSLibrary.sAuxMatrix, Mid(sAux, i, 1)) = 0 Then
                        oMensajes.CodUsuarioNoValido (Mid(sAux, i, 1))
                        Screen.MousePointer = vbNormal
                        If Me.Visible Then txtCod.SetFocus
                        Exit Sub
                    End If
                Next
            
                If chkBloq.Value = vbChecked Then
                    g_oUsuario.Bloq = gParametrosGenerales.giLOGPREBLOQ
                Else
                    g_oUsuario.Bloq = 0
                End If
            End If
            
            If chkAdmin <> vbUnchecked Then
                g_oUsuario.ADM = FSGSLibrary.EncriptarAES(Trim(txtCod.Text), Trim(txtCod.Text), True, g_oUsuario.FecUsu, 1, TipoDeUsuario.Administrador)
            Else
                g_oUsuario.ADM = FSGSLibrary.EncriptarAES(Trim(txtCod.Text), Trim(txtCod.Text), True, g_oUsuario.FecUsu, 1, TipoDeUsuario.Persona)
            End If
            
            'Perfil
            If sdbcPerfil.Text <> "" Then
                Set oPerf = oFSGSRaiz.Generar_CPerfil
                oPerf.Id = CLng(sdbcPerfil.Value)
                oPerf.Cod = sdbcPerfil.Columns("COD").Value
                oPerf.Den = sdbcPerfil.Columns("DEN").Value
                oPerf.FSBI = chkFSBI.Value
                oPerf.FSCM = chkFSCM.Value
                oPerf.FSEP = chkFSEP.Value
                oPerf.FSGS = chkFSGS.Value
                oPerf.FSIM = chkFSIM.Value
                oPerf.FSIS = chkFSIS.Value
                oPerf.FSPM = chkFSPM.Value
                oPerf.FSQA = chkFSQA.Value
                oPerf.FSSM = chkFSSM.Value
                Set g_oUsuario.Perfil = oPerf
            Else
                Set g_oUsuario.Perfil = Nothing
                g_oUsuario.AccesoFSEP = chkFSEP.Value
                g_oUsuario.AccesoFSGS = chkFSGS.Value
                g_oUsuario.AccesoFSWS = chkFSPM.Value
                g_oUsuario.AccesoFSQA = chkFSQA.Value
                g_oUsuario.AccesoFSINT = chkFSIS.Value
                g_oUsuario.AccesoFSContratos = chkFSCM.Value
                g_oUsuario.AccesoFSIM = chkFSIM.Value
                g_oUsuario.AccesoFSSM = chkFSSM.Value
                g_oUsuario.AccesoFSBI = chkFSBI.Value
            End If
            
            'Cambiamos la persona asociada al usuario
            If Not m_oPer Is Nothing Then
                If m_oPer.Cod <> frmUSUARIOS.g_oUsuario.Persona.Cod Then
                    Set g_oUsuario.Persona = m_oPer
                End If
            End If
            If sdbcEquipo.Text <> "" Then
                If IsNull(g_oUsuario.Persona.codEqp) Then 'A�adir FC
                    irespuesta = oMensajes.A�adirFunComp
                    If irespuesta = vbNo Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                Else
                    If g_oUsuario.Persona.codEqp <> sdbcEquipo.Columns(0).Value Then
                        irespuesta = oMensajes.ModifFunComp
                        If irespuesta = vbNo Then
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                    End If
                End If
                g_oUsuario.Persona.codEqp = sdbcEquipo.Columns(0).Value
                g_oUsuario.Tipo = 2
            Else
                If Not IsNull(g_oUsuario.Persona.codEqp) Then
                    irespuesta = oMensajes.PreguntaEliminarFunComp
                    If irespuesta = vbYes Then
                        g_oUsuario.Persona.codEqp = Null
                        g_oUsuario.Tipo = 3
                    Else
                        g_oUsuario.Tipo = 2
                        g_oUsuario.AccesoFSGS = True
                    End If
                Else
                    g_oUsuario.Tipo = 3
                End If
            End If
            If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
                If IsEmpty(m_vRutaLocal) Then
                    If gParametrosGenerales.gsTSHomeFolderRutaLocal <> "" Then
                        m_vUnidad = Null
                        m_vRutaUnidad = Null
                        m_vRutaLocal = gParametrosGenerales.gsTSHomeFolderRutaLocal
                    ElseIf gParametrosGenerales.gsTSHomeFolderRutaUnidad <> "" Then
                        m_vRutaLocal = Null
                        m_vUnidad = gParametrosGenerales.gsTSHomeFolderUnidad
                        m_vRutaUnidad = gParametrosGenerales.gsTSHomeFolderRutaUnidad
                    End If
                End If
                g_oUsuario.TSRutaLocal = m_vRutaLocal
                g_oUsuario.TSUnidad = m_vUnidad
                g_oUsuario.TSRutaUnidad = m_vRutaUnidad
            Else
                g_oUsuario.TSRutaLocal = Null
                g_oUsuario.TSUnidad = Null
                g_oUsuario.TSRutaUnidad = Null
            End If
            
            g_oUsuario.PWDOld = g_oUsuario.PWDDes
            If Trim(txtPWD.Text) <> "" Then
                g_oUsuario.PWDDes = txtPWD.Text
            End If
                
            Set m_oIBaseDatos = g_oUsuario
            m_bChanging = True
            teserror = m_oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                m_bChanging = False
                Exit Sub
            End If
            m_bChanging = False
    End Select
    
    Screen.MousePointer = vbNormal
    m_bChanging = False
    Set nodx = Nothing
    Set oPerf = Nothing
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Departamentos asociados
Dim oDepsAsocN0 As CDepAsociados
Dim oDepsAsocN1 As CDepAsociados
Dim oDepsAsocN2 As CDepAsociados
Dim oDepsAsocN3 As CDepAsociados
Dim oDepAsoc As CDepAsociado

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3

' Personas
Dim oPersN0 As CPersonas
Dim oPersN1 As CPersonas
Dim oPersN2 As CPersonas
Dim oPersN3 As CPersonas
Dim oPer As CPersona

' Otras
Dim nodx As node
    
    Screen.MousePointer = vbHourglass
    
    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
    
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
       
    oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
    
    oPersN0.CargarTodasLasPersonas , , , , 0, True, , , , False, , True
    'Cargamos toda la estrucutura organizativa
    Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, True, , , , False, , True
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False
                oPersN2.CargarTodasLasPersonas , , , , 2, True, , , , False, , True
                oPersN1.CargarTodasLasPersonas , , , , 1, True, , , , False, , True
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, True, , , , False, , True
                oPersN2.CargarTodasLasPersonas , , , , 2, True, , , , False, , True
                oPersN3.CargarTodasLasPersonas , , , , 3, True, , , , False, , True
                
        End Select
        
        
   '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
            
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
            
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
            
    Next
    
        'Departamentos
            
            For Each oDepAsoc In oDepsAsocN0
            
                scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            
            Next
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            
        Next
                
            'Personas
            
            For Each oPer In oPersN0
                    scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                    
                    If frmUSUARIOS.tvwUsu.selectedItem.Tag <> "Raiz" Then
                        'Modificacion
                        If IsNull(oPer.Usuario) Or ((oPer.Cod = frmUSUARIOS.g_oUsuario.Persona.Cod) And (oPer.Apellidos = frmUSUARIOS.g_oUsuario.Persona.Apellidos)) Then
                            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                            nodx.Tag = "PER0" & CStr(oPer.Cod)
                            If oPer.codEqp <> "" Then
                                nodx.Image = "COMP"
                            End If
                        End If
                    Else 'A�adir
                        If IsNull(oPer.Usuario) Then
                            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                            nodx.Tag = "PER0" & CStr(oPer.Cod)
                            If oPer.codEqp <> "" Then
                                nodx.Image = "COMP"
                            End If
                        End If
                    End If
                  
            Next
            
            For Each oPer In oPersN1
                    scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                    scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                    If frmUSUARIOS.tvwUsu.selectedItem.Tag <> "Raiz" Then
                        If IsNull(oPer.Usuario) Or ((oPer.Cod = frmUSUARIOS.g_oUsuario.Persona.Cod) And (oPer.Apellidos = frmUSUARIOS.g_oUsuario.Persona.Apellidos)) Then
                                    
                            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                            nodx.Tag = "PER1" & CStr(oPer.Cod)
                
                            If oPer.codEqp <> "" Then
                                nodx.Image = "COMP"
                            End If
                        End If
                    Else
                        If IsNull(oPer.Usuario) Then
                            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                            nodx.Tag = "PER1" & CStr(oPer.Cod)
                
                            If oPer.codEqp <> "" Then
                                nodx.Image = "COMP"
                            End If
                        End If
                    End If
            Next
            
            For Each oPer In oPersN2
                    scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                    scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                    scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                    
                    If frmUSUARIOS.tvwUsu.selectedItem.Tag <> "Raiz" Then
                    
                        If IsNull(oPer.Usuario) Or ((oPer.Cod = frmUSUARIOS.g_oUsuario.Persona.Cod) And (oPer.Apellidos = frmUSUARIOS.g_oUsuario.Persona.Apellidos)) Then
                                    
                            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                            nodx.Tag = "PER2" & CStr(oPer.Cod)
            
                            If oPer.codEqp <> "" Then
                                nodx.Image = "COMP"
                            End If
                        End If
                    Else
                        If IsNull(oPer.Usuario) Then
                            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                            nodx.Tag = "PER2" & CStr(oPer.Cod)
            
                            If oPer.codEqp <> "" Then
                                nodx.Image = "COMP"
                            End If
                        End If
                    End If
            Next
            
            For Each oPer In oPersN3
                    scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                    scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                    scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
                    scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                    
                    If frmUSUARIOS.tvwUsu.selectedItem.Tag <> "Raiz" Then
                    
                        If IsNull(oPer.Usuario) Or ((oPer.Cod = frmUSUARIOS.g_oUsuario.Persona.Cod) And (oPer.Apellidos = frmUSUARIOS.g_oUsuario.Persona.Apellidos)) Then
                    
                            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                            nodx.Tag = "PER3" & CStr(oPer.Cod)
            
                            If oPer.codEqp <> "" Then
                                nodx.Image = "COMP"
                            End If
                        End If
                    Else
                        If IsNull(oPer.Usuario) Then
                            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                            nodx.Tag = "PER3" & CStr(oPer.Cod)
            
                            If oPer.codEqp <> "" Then
                                nodx.Image = "COMP"
                            End If
                        End If
                    End If
            Next
    
    
    Screen.MousePointer = vbNormal
    
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
    
End Sub



Private Sub Form_Activate()

On Error GoTo Error
    
    Select Case frmUSUARIOS.Accion
        
        Case ACCUsuAnya
                    
            picCom.Enabled = True
            picDatos.Enabled = True
            txtCod.Enabled = True
            If Me.Visible Then txtCod.SetFocus
            
        Case ACCUsuMod
            If m_bNoModificarTieneSobres Then
                picFSGS.Enabled = False
            Else
                picFSGS.Enabled = True
            End If
            picDatos.Enabled = True
            txtCod.Enabled = False
            If Me.Visible Then txtPWD.SetFocus
        
        Case ACCusudet
            picCom.Enabled = False
            picDatos.Enabled = False
    End Select
    
    Exit Sub
    
Error:
    If err.Number = 5 Then
        Resume Next
    End If
End Sub

''' <summary>Carga de los literales multiidioma</summary>
''' <remarks>Llamada desde:frmLstUSUPERSWinSec.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>NGO 27/12/2011</revision>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_USU_PERS_WINSEC, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim m_sIdioma(1 To 4)
        For i = 1 To 4
            m_sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        lblCodigo.caption = m_sIdioma(1) & ":"
        sdbcEquipo.Columns(0).caption = m_sIdioma(1)
        sdbcPerfil.Columns(1).caption = m_sIdioma(1)
        lblPwd.caption = m_sIdioma(2) & ":"
        lblNombre.caption = m_sIdioma(3) & ":"
        lblPwdConf.caption = m_sIdioma(4)
        
        chkBloq.caption = Ador(0).Value '5
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        'lblEqui.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        Ador.MoveNext
        sdbcEquipo.Columns(1).caption = Ador(0).Value
        sdbcPerfil.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        chkFSEP.caption = Ador(0).Value  '12 Acceso a sistema FSEP (Aprovisionamiento)
        Ador.MoveNext
        m_sConfPwd = Ador(0).Value  '13 Confirmaci�n de la contrase�a
        Ador.MoveNext
        chkChangeIni.caption = Ador(0).Value  '14 El usuario debe cambiar la contrase�a en el pr�ximo inicio de sesi�n
        Ador.MoveNext
        lblRuta.caption = Ador(0).Value  '15 Ruta por defecto para gesti�n de archivos:
        Ador.MoveNext
        'chkNoExpira.Caption = Ador(0).Value  '16 La contrase�a nunca expira
        Ador.MoveNext
        chkDeshab.caption = Ador(0).Value  '17 Cuenta deshabilitada
        Ador.MoveNext
        '18 Ruta por defecto para gesti�n de archivos
        Ador.MoveNext
        '19 Ruta local
        Ador.MoveNext
        '20 Conectar unidad:
        Ador.MoveNext
        '21 a
        Ador.MoveNext
        chkFSPM.caption = Ador(0).Value  '22 Acceso a sistema FSWS (Solicitudes)
        Ador.MoveNext
        chkFSGS.caption = Ador(0).Value '23 Acceso a sistema FSGS (Compras)
        Ador.MoveNext
        lblEqp.caption = Ador(0).Value  '24 Pertenece a equipo:
        Ador.MoveNext
        chkFSQA.caption = Ador(0).Value  '25 Acceso a sistema FSQA (Calidad)
        Ador.MoveNext
        chkFSSM.caption = Ador(0).Value '25 Acceso a sistema FSSM (Presupuestos)
        Ador.MoveNext
        chkFSIS.caption = Ador(0).Value  '27 Acceso a sistema FSIS (Integraci�n)
        Ador.MoveNext
        chkFSCM.caption = Ador(0).Value  '28 Acceso a sistema FSCM (Contratos)
        Ador.MoveNext
        chkFSIM.caption = Ador(0).Value  '29 Acceso a sistema FSIM (Facturaci�n)
        Ador.MoveNext
        chkAdmin.caption = Ador(0).Value  '30 Administrador
        Ador.MoveNext
        lblAcceso.caption = Ador(0).Value & ":"  '31 Acceso a los m�dulos
        Ador.MoveNext
        lblPerfil.caption = Ador(0).Value & ":"   '32 Perfil de seguridad
        Ador.MoveNext
        chkFSBI.caption = Ador(0).Value    '33 Acceso a sistema FSBI
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

''' <summary>
''' Evento que se genera al cargar el formulario, cargando los datos y haciendo comprobaciones para mostrar o no parte de los controles
''' </summary>
''' <remarks>
''' Llamadas desde: Autom�tica, siempre que se cargue el formulario
''' Tiempo m�ximo: 0,1 seg
''' </remarks>
''' <revision>NGO 27/12/2011</revision>

Private Sub Form_Load()
    Dim nodx As node
    
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodUSU
        
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Set g_oUsuario = frmUSUARIOS.g_oUsuario
    g_iAccion = frmUSUARIOS.Accion
    
    If g_iAccion = ACCUsuMod Then
        g_oUsuario.PWDOld = g_oUsuario.PWDDes
    End If
    
    MostrarDatos
    ConfigurarFormulario
    
    m_bNoModificarTieneSobres = False
    
    Select Case frmUSUARIOS.Accion
        Case ACCUsuAnya
            If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
                chkChangeIni.Value = vbChecked
                chkDeshab.Value = vbUnchecked
                chkBloq.Value = vbUnchecked
                chkBloq.Enabled = False
            End If
            
            'Generar la estructura de la organizacion en el treeview
            tvwestrorg.Enabled = True
            GenerarEstructuraOrg False
    
        Case ACCUsuMod
            'Si el usuario tiene sobres pendientes de abrir no se cambia
            If gParametrosGenerales.gbAdminPublica Then
                If frmUSUARIOS.g_oUsuario.ExistenSobresCerradosDeUsuario Then
                    m_bNoModificarTieneSobres = True
                    m_sUON1 = NullToStr(frmUSUARIOS.g_oUsuario.Persona.UON1)
                    m_sUON2 = NullToStr(frmUSUARIOS.g_oUsuario.Persona.UON2)
                    m_sUON3 = NullToStr(frmUSUARIOS.g_oUsuario.Persona.UON3)
                    m_sDep = NullToStr(frmUSUARIOS.g_oUsuario.Persona.CodDep)
                    GenerarRamaDetalle
                Else
                    ' Generar la estructura de la organizacion en el treeview
                    GenerarEstructuraOrg False
                    Set nodx = tvwestrorg.Nodes.Item("PERS" & CStr(frmUSUARIOS.g_oUsuario.Persona.Cod))
                    nodx.Selected = True
                    nodx.EnsureVisible
                    If nodx.Image = "COMP" Then
                        nodx.Image = "COMPASIG"
                    Else
                        nodx.Image = "PERSASIG"
                    End If
                    tvwestrorg.Enabled = True
                End If
            Else
                ' Generar la estructura de la organizacion en el treeview
                GenerarEstructuraOrg False
                Set nodx = tvwestrorg.Nodes.Item("PERS" & CStr(frmUSUARIOS.g_oUsuario.Persona.Cod))
                nodx.Selected = True
                nodx.EnsureVisible
                If nodx.Image = "COMP" Then
                    nodx.Image = "COMPASIG"
                Else
                    nodx.Image = "PERSASIG"
                End If
                tvwestrorg.Enabled = True
            End If
        Case ACCusudet
            GenerarRamaDetalle
    End Select
    
    Set m_oPer = Nothing
    Set nodx = Nothing
End Sub

''' <summary>Muestra los datos en pantalla</summary>
''' <remarks>Llamadas desde: Form_Load</remarks>
''' <revision>LTG 18/12/2012</revision>

Private Sub MostrarDatos()
    Dim bFSEP As Boolean
    Dim bFSGS As Boolean
    Dim bFSPM As Boolean
    Dim bFSQA As Boolean
    Dim bFSIS As Boolean
    Dim bFSCM As Boolean
    Dim bFSIM As Boolean
    Dim bFSSM As Boolean
    Dim bFSBI As Boolean
    
    bFSEP = False
    bFSGS = False
    bFSPM = False
    bFSQA = False
    bFSIS = False
    bFSCM = False
    bFSIM = False
    bFSSM = False
    bFSBI = False
    
    If g_iAccion = ACCUsuAnya Then
        If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
            If gParametrosGenerales.gsTSHomeFolderRutaLocal <> "" Then
                m_vUnidad = Null
                m_vRutaUnidad = Null
                m_vRutaLocal = gParametrosGenerales.gsTSHomeFolderRutaLocal
            ElseIf gParametrosGenerales.gsTSHomeFolderRutaUnidad <> "" Then
                m_vRutaLocal = Null
                m_vUnidad = gParametrosGenerales.gsTSHomeFolderUnidad
                m_vRutaUnidad = gParametrosGenerales.gsTSHomeFolderRutaUnidad
            End If
        End If
    ElseIf g_iAccion = ACCUsuMod Then
        chkAdmin.Value = IIf(g_oUsuario.EsAdmin, vbChecked, vbUnchecked)
        txtPWD = g_oUsuario.PWDDes
        txtPWDConf = g_oUsuario.PWDDes
        
        If Not frmUSUARIOS.g_oUsuario.Perfil Is Nothing Then
            bFSGS = g_oUsuario.Perfil.FSGS
        Else
            bFSGS = g_oUsuario.AccesoFSGS
        End If
        
        If basParametros.gParametrosGenerales.giWinSecurity = UsuariosLocales And bFSGS Then
            txtCod.Text = g_oUsuario.Cod
            lblPersona.caption = g_oUsuario.Persona.nombre & " " & g_oUsuario.Persona.Apellidos
            
            'DatosTS
            If Not IsNull(g_oUsuario.TSRutaLocal) Then
                m_vUnidad = Null
                m_vRutaUnidad = Null
                m_vRutaLocal = g_oUsuario.TSRutaLocal
            ElseIf Not IsNull(g_oUsuario.TSRutaUnidad) Then
                m_vRutaLocal = Null
                m_vUnidad = g_oUsuario.TSUnidad
                m_vRutaUnidad = g_oUsuario.TSRutaUnidad
            ElseIf gParametrosGenerales.gsTSHomeFolderRutaLocal <> "" Then
                m_vUnidad = Null
                m_vRutaUnidad = Null
                m_vRutaLocal = gParametrosGenerales.gsTSHomeFolderRutaLocal
            ElseIf gParametrosGenerales.gsTSHomeFolderRutaUnidad <> "" Then
                m_vRutaLocal = Null
                m_vUnidad = gParametrosGenerales.gsTSHomeFolderUnidad
                m_vRutaUnidad = gParametrosGenerales.gsTSHomeFolderRutaUnidad
            End If
            
            'Propiedades de windows
            If g_oUsuario.CuentaDebeCambiarPwd Then
                chkChangeIni.Value = vbChecked
            Else
                chkChangeIni.Value = vbUnchecked
            End If
            If g_oUsuario.CuentaDeshabilitada Then
                chkDeshab.Value = vbChecked
            Else
                chkDeshab.Value = vbUnchecked
            End If
            If g_oUsuario.CuentaBloqueada Then
                chkBloq.Value = vbChecked
                chkBloq.Enabled = True
            Else
                chkBloq.Value = vbUnchecked
                chkBloq.Enabled = False
            End If
        Else
            txtCod.Text = g_oUsuario.Cod
            lblPersona.caption = g_oUsuario.Persona.nombre & " " & g_oUsuario.Persona.Apellidos
                        
            If gParametrosGenerales.giLOGPREBLOQ > 0 Then
                If g_oUsuario.Bloq >= gParametrosGenerales.giLOGPREBLOQ Then
                    chkBloq.Value = vbChecked
                    chkBloq.Enabled = True
                Else
                    chkBloq.Value = vbUnchecked
                    chkBloq.Enabled = False
                End If
                chkBloq.Visible = True
            Else
                chkBloq.Value = vbUnchecked
                chkBloq.Visible = False
            End If
        End If
        
        If Not IsNull(g_oUsuario.Persona.codEqp) Then
            sdbcEquipo.Value = g_oUsuario.Persona.codEqp & " " & g_oUsuario.Persona.DenEqp
            sdbcEquipo.Text = g_oUsuario.Persona.codEqp & " " & g_oUsuario.Persona.DenEqp
            sdbcEquipo.Columns(0).Value = g_oUsuario.Persona.codEqp
        Else
            sdbcEquipo.Value = ""
            sdbcEquipo.Text = ""
        End If
        
        'Perfil
        sdbcPerfil.Visible = True
        lblPerf.Visible = False
        AsignarPerfil g_oUsuario.Perfil, g_oUsuario.EsAdmin
            
        If Not frmUSUARIOS.g_oUsuario.Perfil Is Nothing Then
            bFSEP = g_oUsuario.Perfil.FSEP
            bFSPM = g_oUsuario.Perfil.FSPM
            bFSQA = g_oUsuario.Perfil.FSQA
            bFSIS = g_oUsuario.Perfil.FSIS
            bFSCM = g_oUsuario.Perfil.FSCM
            bFSIM = g_oUsuario.Perfil.FSIM
            bFSSM = g_oUsuario.Perfil.FSSM
            bFSBI = g_oUsuario.Perfil.FSBI
        Else
            bFSEP = g_oUsuario.AccesoFSEP
            bFSPM = g_oUsuario.AccesoFSWS
            bFSQA = g_oUsuario.AccesoFSQA
            bFSIS = g_oUsuario.AccesoFSINT
            bFSCM = g_oUsuario.AccesoFSContratos
            bFSIM = g_oUsuario.AccesoFSIM
            bFSSM = g_oUsuario.AccesoFSSM
            bFSBI = g_oUsuario.AccesoFSBI
        End If
    ElseIf g_iAccion = ACCusudet Then
        chkAdmin.Value = IIf(g_oUsuario.EsAdmin, vbChecked, vbUnchecked)
        
        If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
            m_sUON1 = NullToStr(g_oUsuario.Persona.UON1)
            m_sUON2 = NullToStr(g_oUsuario.Persona.UON2)
            m_sUON3 = NullToStr(g_oUsuario.Persona.UON3)
            m_sDep = NullToStr(g_oUsuario.Persona.CodDep)
    
            picEdit.Visible = False
            picDatos.Enabled = False
            txtCod = g_oUsuario.Cod
            txtPWD.Text = "**********"
            lblPersona.caption = g_oUsuario.Persona.nombre & " " & g_oUsuario.Persona.Apellidos
            
            'DatosTS
            If Not IsNull(g_oUsuario.TSRutaLocal) Then
                m_vUnidad = Null
                m_vRutaUnidad = Null
                m_vRutaLocal = g_oUsuario.TSRutaLocal
            ElseIf Not IsNull(g_oUsuario.TSRutaUnidad) Then
                m_vRutaLocal = Null
                m_vUnidad = g_oUsuario.TSUnidad
                m_vRutaUnidad = g_oUsuario.TSRutaUnidad
            ElseIf gParametrosGenerales.gsTSHomeFolderRutaLocal <> "" Then
                m_vUnidad = Null
                m_vRutaUnidad = Null
                m_vRutaLocal = gParametrosGenerales.gsTSHomeFolderRutaLocal
            ElseIf gParametrosGenerales.gsTSHomeFolderRutaUnidad <> "" Then
                m_vRutaLocal = Null
                m_vUnidad = gParametrosGenerales.gsTSHomeFolderUnidad
                m_vRutaUnidad = gParametrosGenerales.gsTSHomeFolderRutaUnidad
            End If
        
            'Propiedades de windows
            If g_oUsuario.CuentaDebeCambiarPwd Then
                chkChangeIni.Value = vbChecked
            Else
                chkChangeIni.Value = vbUnchecked
            End If
            If g_oUsuario.CuentaDeshabilitada Then
                chkDeshab.Value = vbChecked
            Else
                chkDeshab.Value = vbUnchecked
            End If
            If g_oUsuario.CuentaBloqueada Then 'ZZ Mirar como quedan las otras cuando esta chekeada
                chkBloq.Value = vbChecked
                chkBloq.Enabled = True
            Else
                chkBloq.Value = vbUnchecked
                chkBloq.Enabled = False
            End If
        Else
            m_sUON1 = NullToStr(g_oUsuario.Persona.UON1)
            m_sUON2 = NullToStr(g_oUsuario.Persona.UON2)
            m_sUON3 = NullToStr(g_oUsuario.Persona.UON3)
            m_sDep = NullToStr(g_oUsuario.Persona.CodDep)
    
            picEdit.Visible = False
            picDatos.Enabled = False
            
            txtCod = g_oUsuario.Cod
            txtPWD.Text = "**********"
            lblPersona.caption = g_oUsuario.Persona.nombre & " " & g_oUsuario.Persona.Apellidos
            
            If gParametrosGenerales.giLOGPREBLOQ > 0 Then
                chkBloq.Visible = True
                If g_oUsuario.Bloq >= gParametrosGenerales.giLOGPREBLOQ Then
                    chkBloq = vbChecked
                    chkBloq.Enabled = True
                Else
                    chkBloq = vbUnchecked
                    chkBloq.Enabled = False
                End If
            Else
                chkBloq = vbUnchecked
                chkBloq.Visible = False
            End If
        End If
        
        If (FSEPConf = 0) Then
            lblEquipo.Visible = True
            sdbcEquipo.Visible = False
            If g_oUsuario.Persona.codEqp <> "" Then
               lblEquipo = g_oUsuario.Persona.codEqp & " " & g_oUsuario.Persona.DenEqp
            Else
               lblEquipo = ""
            End If
        End If
        
        'Perfil
        sdbcPerfil.Visible = False
        lblPerf.Visible = True
        AsignarPerfil g_oUsuario.Perfil, False
        
        If Not frmUSUARIOS.g_oUsuario.Perfil Is Nothing Then
            bFSEP = g_oUsuario.Perfil.FSEP
            bFSGS = g_oUsuario.Perfil.FSGS
            bFSPM = g_oUsuario.Perfil.FSPM
            bFSQA = g_oUsuario.Perfil.FSQA
            bFSIS = g_oUsuario.Perfil.FSIS
            bFSCM = g_oUsuario.Perfil.FSCM
            bFSIM = g_oUsuario.Perfil.FSIM
            bFSSM = g_oUsuario.Perfil.FSSM
            bFSBI = g_oUsuario.Perfil.FSBI
        Else
            bFSEP = g_oUsuario.AccesoFSEP
            bFSGS = g_oUsuario.AccesoFSGS
            bFSPM = g_oUsuario.AccesoFSWS
            bFSQA = g_oUsuario.AccesoFSQA
            bFSIS = g_oUsuario.AccesoFSINT
            bFSCM = g_oUsuario.AccesoFSContratos
            bFSIM = g_oUsuario.AccesoFSIM
            bFSSM = g_oUsuario.AccesoFSSM
            bFSBI = g_oUsuario.AccesoFSBI
        End If
    End If
        
    chkFSEP.Value = IIf(bFSEP, vbChecked, vbUnchecked)
    chkFSGS.Value = IIf(bFSGS, vbChecked, vbUnchecked)
    chkFSPM.Value = IIf(bFSPM, vbChecked, vbUnchecked)
    chkFSQA.Value = IIf(bFSQA, vbChecked, vbUnchecked)
    chkFSIS.Value = IIf(bFSIS, vbChecked, vbUnchecked)
    chkFSCM.Value = IIf(bFSCM, vbChecked, vbUnchecked)
    chkFSIM.Value = IIf(bFSIM, vbChecked, vbUnchecked)
    chkFSSM.Value = IIf(bFSSM, vbChecked, vbUnchecked)
    chkFSBI.Value = IIf(bFSBI, vbChecked, vbUnchecked)
End Sub

''' <summary>Configura los controles del formulario en funci�n de gParametrosGenerales.giWinSecurity y los m�dulos habilitados</summary>
''' <remarks>Llamadas desde: Form_Load</remarks>
''' <revision>LTG 12/12/2012</revision>

Private Sub ConfigurarFormulario()
    If FSEPConf Then
        picFSGS.Visible = False
    Else
        'Si no hay acceso al m�dulo FSEP no se ver� el picture correspondiente:
        If Not gParametrosGenerales.gbPedidosAprov Then
            picFSEP.Visible = False
            chkFSEP.Value = vbUnchecked
        End If
        'Si no hay acceso al m�dulo FSWS no se ver� el picture correspondiente:
        If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.SinAcceso Then
            chkFSPM.Value = vbUnchecked
            picFSPM.Visible = False
        End If
        If gParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso Then
            picFSQA.Visible = False
            chkFSQA.Value = vbUnchecked
        End If
        If Not gParametrosGenerales.gbIntegracion Then
            picFSIS.Visible = False
            chkFSIS.Value = vbUnchecked
        End If
        'Si no hay acceso al m�dulo Contratos no se ver� el picture correspondiente:
        If gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.SinAcceso Then
            picFSCM.Visible = False
            chkFSCM.Value = vbUnchecked
        End If
        'Si no hay acceso al m�dulo FSIM no se ver� el picture correspondiente:
        If gParametrosGenerales.gsAccesoFSIM = TIPOACCESOFSIM.SinAcceso Then
            picFSIM.Visible = False
            chkFSIM.Value = vbUnchecked
        End If
        'Si no hay acceso al m�dulo FSSM no se ver� el picture correspondiente:
        If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.SinAcceso Then
            picFSSM.Visible = False
            chkFSSM.Value = vbUnchecked
        End If
        'Si no hay acceso al m�dulo FSBI no se ver� el picture correspondiente:
        If gParametrosGenerales.gsAccesoFSBI = TipoAccesoFSBI.SinAcceso Then
            picFSBI.Visible = False
            chkFSBI.Value = vbUnchecked
        End If
    End If
    
    If gParametrosGenerales.giINSTWEB = SinWeb Then
        picFSEP.Visible = False
    End If
    
    If gParametrosGenerales.giWinSecurity <> UsuariosLocales Then
        If gParametrosGenerales.giLOGPREBLOQ = 0 Then
            chkBloq.Visible = False
        Else
            chkBloq.Visible = True
        End If
    
        picDatos.Visible = True
    End If
    
    If gParametrosGenerales.giWinSecurity <> UsuariosLocales Then
        chkChangeIni.Visible = False
        chkDeshab.Visible = False
        lblRuta.Visible = False
        cmdRuta.Visible = False
        
        chkBloq.Top = txtCod.Top
        chkBloq.Left = chkChangeIni.Left
    End If
    
    'Si hay autenticacion windows (ni LDAP) en GS y WEB no hace falta contrase�a
    If ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) _
    And (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP)) Then
        lblPwd.Visible = False
        lblPwdConf.Visible = False
        txtPWD.Visible = False
        txtPWDConf.Visible = False
    End If
    
    If g_iAccion = ACCusudet Then
        If NullToStr(g_oUsuario.Persona.codEqp) = "" Then
            picEquipo.Visible = False
        Else
            picEquipo.Visible = True
        End If
        
        If g_oUsuario.Perfil Is Nothing Then
            picPerfil.Visible = False
        Else
            picPerfil.Visible = True
        End If
    Else
        picEquipo.Visible = True
        
        picPerfil.Visible = True
        If Not g_oUsuario Is Nothing Then
            If g_oUsuario.Perfil Is Nothing Then
                picAccesos.Enabled = True
            Else
                picAccesos.Enabled = False
            End If
        Else
            picAccesos.Enabled = True
        End If
    End If
End Sub

''' <summary>
''' Evento que se genera siempre que se redimiensiona el formulario, haciendo comprobaciones sobre los parametros de seguridad para redimensionar en base a los controles que se muestran
''' </summary>
''' <remarks>
''' Llamadas desde: Autom�tica, siempre que se redimensione el formulario
''' Tiempo m�ximo: 0 seg
''' </remarks>
''' <revision>NGO 27/12/2011</revision>
Private Sub Form_Resize()
    Dim colPicVisibles As Collection
    Dim oPic As PictureBox
    Dim lngTop
    
    On Error Resume Next
    
    If m_bResizing Then Exit Sub
    
    m_bResizing = True
    
    If Me.Height < 9400 Then
        Me.Height = 9400
        Exit Sub
    End If
    If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
        If Me.Width < 7600 Then
            Me.Width = 7600
            Exit Sub
        End If
    Else
        If Me.Width < 7250 Then
            Me.Width = 7250
            Exit Sub
        End If
    End If
    
    tvwestrorg.Width = Me.Width - 325
    picDatos.Width = Me.Width - 325
    chkChangeIni.Width = Me.Width - chkChangeIni.Left - 1200
    
    'Si hay autenticacion windows en GS y WEB hay que recolocar los controles
    If ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) _
    And (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP)) Then
        If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
            chkChangeIni.Left = lblCodigo.Left
            chkChangeIni.Width = 6000
            txtCod.Top = lblPwdConf.Top
            lblCodigo.Top = lblPwdConf.Top
            chkDeshab.Left = lblPwd.Left
            chkBloq.Left = chkDeshab.Left + chkDeshab.Width + 100
        Else
            lblNombre.Top = lblPwd.Top
            lblPersona.Top = txtPWD.Top
            picDatos.Height = lblPersona.Top + lblPersona.Height + 100
        End If
    End If
    
    If gParametrosGenerales.giWinSecurity <> UsuariosLocales Then chkBloq.Left = chkDeshab.Left
    
    tvwestrorg.Width = Me.Width - 360
    tvwestrorg.Top = lblNombre.Top + lblNombre.Height + 100
    tvwestrorg.Left = picDatos.Left
    
    If Not picEquipo.Visible Then
        picAccesos.Top = picPerfil.Top + 100
        picPerfil.Top = picEquipo.Top
    End If
    If Not picPerfil.Visible Then picAccesos.Top = picPerfil.Top + 100
    
    Set colPicVisibles = New Collection
    If picFSPM.Visible Then colPicVisibles.Add picFSPM
    If picFSGS.Visible Then colPicVisibles.Add picFSGS
    If picFSEP.Visible Then colPicVisibles.Add picFSEP
    If picFSCM.Visible Then colPicVisibles.Add picFSCM
    If picFSSM.Visible Then colPicVisibles.Add picFSSM
    If picFSIM.Visible Then colPicVisibles.Add picFSIM
    If picFSQA.Visible Then colPicVisibles.Add picFSQA
    If picFSIS.Visible Then colPicVisibles.Add picFSIS
    If picFSBI.Visible Then colPicVisibles.Add picFSBI
    
    lngTop = 0
    For Each oPic In colPicVisibles
        oPic.Top = lngTop
        lngTop = lngTop + cnPicHeight
    Next
    Set colPicVisibles = Nothing
    picAccesos.Height = lngTop
    picCom.Height = picAccesos.Top + picAccesos.Height
    
    If picEdit.Visible Then
        tvwestrorg.Height = Me.Height - tvwestrorg.Top - picCom.Height - picEdit.Height - 520
    Else
        tvwestrorg.Height = Me.Height - tvwestrorg.Top - picCom.Height - 520
    End If
    
    picCom.Top = tvwestrorg.Top + tvwestrorg.Height
    picEdit.Top = picCom.Top + picCom.Height

    cmdAceptar.Left = tvwestrorg.Width / 2 - cmdAceptar.Width - 50
    cmdCancelar.Left = tvwestrorg.Width / 2 + 50
        
    cmdRuta.Left = picDatos.Left + lblRuta.Left + lblRuta.Width + 250
    
    picCom.Width = tvwestrorg.Width + 100
    picFSEP.Width = picCom.Width
    picFSGS.Width = picCom.Width
    picFSPM.Width = picCom.Width
    picFSQA.Width = picCom.Width
    picFSIS.Width = picCom.Width
    picFSCM.Width = picCom.Width
    picFSIM.Width = picCom.Width
    picFSBI.Width = picCom.Width
    
    Line3.X2 = tvwestrorg.Width
    
    If picEdit.Visible Then
        Me.Height = picEdit.Top + picEdit.Height + 400
    Else
        Me.Height = picEdit.Top + 400
    End If

    m_bResizing = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If m_bChanging = True Then Exit Sub

    If frmUSUARIOS.Accion = ACCusudet Then
        picEdit.Visible = True
        picCom.Enabled = True
        picDatos.Enabled = True
    End If

    m_sUON1 = ""
    m_sUON2 = ""
    m_sUON3 = ""
    m_sDep = ""
    Set m_oPer = Nothing
    Set m_oIBaseDatos = Nothing
    m_sNombre = ""
    m_sConfPwd = ""
    m_bNoModificarTieneSobres = False

End Sub

Private Sub sdbcEquipo_Change()
    Dim nodx As node
    
    If Not m_bRespetarCombo Then
        Set nodx = tvwestrorg.selectedItem
        If nodx Is Nothing Then Exit Sub
            If sdbcEquipo.Text = "" Then
                If nodx.Image = "COMP" Then nodx.Image = "Persona"
                If nodx.Image = "COMPASIG" Then nodx.Image = "PERSASIG"
            End If
        Set nodx = Nothing
    End If
End Sub

Private Sub sdbcEquipo_CloseUp()
    Dim nodx As node

    Set nodx = tvwestrorg.selectedItem
    If Not nodx Is Nothing Then
        EstablecerImagenNodo nodx
        Set nodx = Nothing
    End If
End Sub

Private Sub EstablecerImagenNodo(ByVal oNodo As node)
    Select Case frmUSUARIOS.Accion
        Case ACCUsuMod, ACCUsuAnya
            'Si es una persona se preuntar� si se quiere a�adir una funci�n compradora
            If (oNodo.Image = "Persona" Or oNodo.Image = "PERSASIG") And sdbcEquipo.Text <> "" Then
                If oNodo.Image = "Persona" Then oNodo.Image = "COMP"
                If oNodo.Image = "PERSASIG" Then oNodo.Image = "COMPASIG"
                Exit Sub
            End If
            
            'Si ya es compradora se preguntar� si se quiere cambiar la funci�n compradora
            If (oNodo.Image = "COMP" Or oNodo.Image = "COMPASIG") And sdbcEquipo.Text = "" Then
                If oNodo.Image = "COMP" Then oNodo.Image = "Persona"
                If oNodo.Image = "COMPASIG" Then oNodo.Image = "PERSASIG"
            End If
        Case Else
    End Select
End Sub


Private Sub sdbcEquipo_DropDown()
    Dim oEqps As CEquipos
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim nodx As node
    
    Set nodx = tvwestrorg.selectedItem
    
    If nodx Is Nothing Then Exit Sub
    
    'Desplegamos los equipos si hay una persona seleccionada
    If Left(nodx.Tag, 3) = "PER" Then
        Screen.MousePointer = vbHourglass
        Set oEqps = oFSGSRaiz.Generar_CEquipos
        
        sdbcEquipo.RemoveAll
        
        oEqps.CargarTodosLosEquipos , , False, False, False
        
        Codigos = oEqps.DevolverLosCodigos
    
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcEquipo.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i) & " " & Codigos.Den(i)
        Next
        
        sdbcEquipo.SelStart = 0
        sdbcEquipo.SelLength = Len(sdbcEquipo.Text)
        sdbcEquipo.Refresh
    
        Set oEqps = Nothing
        
        Screen.MousePointer = vbNormal
    Else
        sdbcEquipo.RemoveAll
    End If
    
Set nodx = Nothing

End Sub

Private Sub sdbcEquipo_InitColumnProps()
    sdbcEquipo.DataFieldList = "Column 0"
End Sub


Private Sub sdbcEquipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEquipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEquipo.Rows - 1
            bm = sdbcEquipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEquipo.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEquipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

''' <summary>Carga en pantalla los accesos indicados</summary>
''' <param name="bFSGS">Acceso a GS</param>
''' <param name="bFSPM">Acceso a PM</param>
''' <param name="bFSQA">Acceso a QA</param>
''' <param name="bFSEP">Acceso a EP</param>
''' <param name="bFSSM">Acceso a SM</param>
''' <param name="bFSCM">Acceso a CM</param>
''' <param name="bFSIS">Acceso a IS</param>
''' <param name="bFSIM">Acceso a IM</param>
''' <param name="bFSBI">Acceso a BI</param>
''' <param name="bEdit">Controles editables</param>
''' <remarks>Llamada desde: sdbcPerfil_Change</remarks>
''' <revision>LTG 17/12/2012</revision>

Private Sub CargarAccesos(ByVal bFSGS As Boolean, ByVal bFSPM As Boolean, ByVal bFSQA As Boolean, ByVal bFSEP As Boolean, ByVal bFSSM As Boolean, _
        ByVal bFSCM As Boolean, ByVal bFSIS As Boolean, ByVal bFSIM As Boolean, ByVal bFSBI As Boolean, ByVal bEdit As Boolean)
    If bFSGS Then
        If bEdit Then
            chkFSGS.Value = vbGrayed
        Else
            chkFSGS.Value = vbChecked
        End If
    Else
        chkFSGS.Value = vbUnchecked
    End If
    If bFSPM Then
        If bEdit Then
            chkFSPM.Value = vbGrayed
        Else
            chkFSPM.Value = vbChecked
        End If
    Else
        chkFSPM.Value = vbUnchecked
    End If
    If bFSQA Then
        If bEdit Then
            chkFSQA.Value = vbGrayed
        Else
            chkFSQA.Value = vbChecked
        End If
    Else
        chkFSQA.Value = vbUnchecked
    End If
    If bFSEP Then
        If bEdit Then
            chkFSEP.Value = vbGrayed
        Else
            chkFSEP.Value = vbChecked
        End If
    Else
        chkFSEP.Value = vbUnchecked
    End If
    If bFSSM Then
        If bEdit Then
            chkFSSM.Value = vbGrayed
        Else
            chkFSSM.Value = vbChecked
        End If
    Else
        chkFSSM.Value = vbUnchecked
    End If
    If bFSCM Then
        If bEdit Then
            chkFSCM.Value = vbGrayed
        Else
            chkFSCM.Value = vbChecked
        End If
    Else
        chkFSCM.Value = vbUnchecked
    End If
    If bFSIM Then
        If bEdit Then
            chkFSIM.Value = vbGrayed
        Else
            chkFSIM.Value = vbChecked
        End If
    Else
        chkFSIM.Value = vbUnchecked
    End If
    If bFSIS Then
        If bEdit Then
            chkFSIS.Value = vbGrayed
        Else
            chkFSIS.Value = vbChecked
        End If
    Else
        chkFSIS.Value = vbUnchecked
    End If
    If bFSBI Then
        If bEdit Then
            chkFSBI.Value = vbGrayed
        Else
            chkFSBI.Value = vbChecked
        End If
    Else
        chkFSBI.Value = vbUnchecked
    End If
End Sub

Private Sub sdbcEquipo_Validate(Cancel As Boolean)
    Dim oEquipos As CEquipos
    Dim nodx As node

    Set nodx = tvwestrorg.selectedItem
    If nodx Is Nothing Then
        sdbcEquipo.Text = ""
        Exit Sub
    End If
    
    With sdbcEquipo
        If Trim(.Text) = "" Then Exit Sub
        
        Set oEquipos = oFSGSRaiz.Generar_CEquipos
        oEquipos.CargarTodosLosEquiposDesde 1, .Value
        
        If oEquipos.Count = 0 Then
            .Text = ""
            .Value = ""
            Exit Sub
        Else
            .Value = oEquipos.Item(1).Cod
            .Text = oEquipos.Item(1).Cod & " " & oEquipos.Item(1).Den
            
            .Columns("CodEqp").Value = oEquipos.Item(1).Cod
            .Columns("DenEqp").Value = oEquipos.Item(1).Den
            .Columns("CoddenEqp").Value = oEquipos.Item(1).Cod & " " & oEquipos.Item(1).Den
        End If
        
        EstablecerImagenNodo nodx
        
        Set oEquipos = Nothing
    End With
    
    Set nodx = Nothing
End Sub

Private Sub sdbcPerfil_Change()
    If sdbcPerfil.Text = "" Then
        picAccesos.Enabled = True
        CargarAccesos False, False, False, False, False, False, False, False, False, True
    Else
        picAccesos.Enabled = False
    End If
End Sub

Private Sub sdbcPerfil_CloseUp()
    Dim nodx As node

    Set nodx = tvwestrorg.selectedItem
    
    If nodx Is Nothing Then Exit Sub
    
    If sdbcPerfil.Text <> "" Then
        picAccesos.Enabled = False
        CargarAccesos sdbcPerfil.Columns("FSGS").Value, sdbcPerfil.Columns("FSPM").Value, sdbcPerfil.Columns("FSQA").Value, sdbcPerfil.Columns("FSEP").Value, _
            sdbcPerfil.Columns("FSSM").Value, sdbcPerfil.Columns("FSCM").Value, sdbcPerfil.Columns("FSIS").Value, sdbcPerfil.Columns("FSIM").Value, _
            sdbcPerfil.Columns("FSBI").Value, False
    Else
        picAccesos.Enabled = True
        CargarAccesos False, False, False, False, False, False, False, False, False, False
    End If
    
    Set nodx = Nothing
End Sub

Private Sub sdbcPerfil_DropDown()
    Dim oPerfiles As CPerfiles
    Dim oPerfil As CPerfil
    Dim nodx As node
    
    Set nodx = tvwestrorg.selectedItem
    If nodx Is Nothing Then Exit Sub
    
    With sdbcPerfil
        .RemoveAll
        
        'Desplegamos los perfiles si hay una persona seleccionada
        If Left(nodx.Tag, 3) = "PER" Then
            Screen.MousePointer = vbHourglass
                    
            Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfiles(, , , , False, , False)
        
            For Each oPerfil In oPerfiles
                .AddItem oPerfil.Id & Chr(m_lSeparador) & oPerfil.Cod & Chr(m_lSeparador) & oPerfil.Den & Chr(m_lSeparador) & oPerfil.FSGS & _
                    Chr(m_lSeparador) & oPerfil.FSPM & Chr(m_lSeparador) & oPerfil.FSQA & Chr(m_lSeparador) & oPerfil.FSEP & Chr(m_lSeparador) & oPerfil.FSSM & _
                    Chr(m_lSeparador) & oPerfil.FSCM & Chr(m_lSeparador) & oPerfil.FSIM & Chr(m_lSeparador) & oPerfil.FSIS & Chr(m_lSeparador) & oPerfil.FSBI
            Next
            Set oPerfil = Nothing
            Set oPerfiles = Nothing
            
            .SelStart = 0
            .SelLength = Len(.Text)
            .Refresh
            
            Screen.MousePointer = vbNormal
        End If
    End With
    
    Set nodx = Nothing
End Sub

Private Sub sdbcPerfil_InitColumnProps()
    sdbcPerfil.DataFieldList = "Column 0"
End Sub

Private Sub sdbcPerfil_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPerfil.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPerfil.Rows - 1
            bm = sdbcPerfil.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPerfil.Columns("COD").CellText(bm), 1, Len(Text))) Then
                sdbcPerfil.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcPerfil_Validate(Cancel As Boolean)
    Dim oPerfiles As CPerfiles
    
    With sdbcPerfil
        If Trim(.Text) = "" Then Exit Sub
        
        Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfiles(.Columns("COD").Value, , True, , , , False)
    
        If oPerfiles.Count = 0 Then
            .Text = ""
            Exit Sub
        Else
            .Value = oPerfiles.Item(1).Id
            .Text = oPerfiles.Item(1).Den
            .Columns("COD").Value = oPerfiles.Item(1).Cod
            .Columns("FSGS").Value = oPerfiles.Item(1).FSGS
            .Columns("FSPM").Value = oPerfiles.Item(1).FSPM
            .Columns("FSQA").Value = oPerfiles.Item(1).FSQA
            .Columns("FSEP").Value = oPerfiles.Item(1).FSEP
            .Columns("FSSM").Value = oPerfiles.Item(1).FSSM
            .Columns("FSCM").Value = oPerfiles.Item(1).FSCM
            .Columns("FSIS").Value = oPerfiles.Item(1).FSIS
            .Columns("FSIM").Value = oPerfiles.Item(1).FSIM
            .Columns("FSBI").Value = oPerfiles.Item(1).FSBI
            
            CargarAccesos .Columns("FSGS").Value, .Columns("FSPM").Value, .Columns("FSQA").Value, .Columns("FSEP").Value, _
                .Columns("FSSM").Value, .Columns("FSCM").Value, .Columns("FSIS").Value, .Columns("FSIM").Value, _
                .Columns("FSBI").Value, False
        End If
        
        Set oPerfiles = Nothing
    End With
End Sub

Private Sub tvwestrorg_NodeClick(ByVal node As MSComctlLib.node)
Dim teserror As TipoErrorSummit
Dim nodx As node
    
If Not frmUSUARIOS.Accion = ACCusudet Then
    If m_bNoModificarTieneSobres Then Exit Sub
    
    Set nodx = tvwestrorg.selectedItem
    
    If Left(node.Tag, 3) = "PER" Then
        sdbcEquipo.RemoveAll
        m_bRespetarCombo = True
        sdbcEquipo.Text = ""
        m_bRespetarCombo = False
        Me.picCom.Enabled = True
        Screen.MousePointer = vbHourglass
        Set m_oPer = oFSGSRaiz.Generar_CPersona
        m_oPer.Cod = DevolverCod(nodx)
        'Leemos los datos de la persona
        Set m_oIBaseDatos = m_oPer
        teserror = m_oIBaseDatos.IniciarEdicion
        m_oIBaseDatos.CancelarEdicion
        Set m_oIBaseDatos = Nothing
        lblPersona.caption = m_oPer.nombre & " " & m_oPer.Apellidos
        If IsNull(m_oPer.codEqp) Or IsEmpty(m_oPer.codEqp) Then
            m_bRespetarCombo = True
            sdbcEquipo.Text = ""
            'Comentar cuando se cambie CPersona_IniciarEdicion
            sdbcEquipo.Columns("CODEQP").Value = ""
            sdbcEquipo.Columns("DENEQP").Value = ""
            m_bRespetarCombo = False
        Else
            m_bRespetarCombo = True
            sdbcEquipo.Text = m_oPer.codEqp & " " & m_oPer.DenEqp
            'Comentar cuando se cambie CPersona_IniciarEdicion
            sdbcEquipo.Columns("CODEQP").Value = m_oPer.codEqp
            sdbcEquipo.Columns("DENEQP").Value = m_oPer.DenEqp
            chkFSGS.Value = vbChecked
            m_bRespetarCombo = False
        End If
        
        Screen.MousePointer = vbNormal
    
    Else
        m_bRespetarCombo = True
        lblPersona.caption = ""
        sdbcEquipo.Text = ""
        picCom.Enabled = False
        Set m_oPer = Nothing
        m_bRespetarCombo = False
    End If
    
    Set nodx = Nothing
End If
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

    Select Case Left(node.Tag, 3)
    
    Case "PER"
        
            DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    Case Else
             DevolverCod = "Error"
    End Select

End Function

Private Sub GenerarRamaDetalle()
'A�ade el comprador y se posiciona en �l
    Dim nodx As node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim iNivel As Integer
    
    Dim oUON1 As CUnidadesOrgNivel1
    Dim oUON2 As CUnidadesOrgNivel2
    Dim oUON3 As CUnidadesOrgNivel3
    Dim oDep As CDepartamentos
    
    
    ' Unidades organizativas
    iNivel = 0
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
      
    If m_sUON1 <> "" Then
        Set oUON1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
        oUON1.CargarTodasLasUnidadesOrgNivel1 m_sUON1, m_sUON2, m_sUON3, m_sDep, True, , m_sUON1, , True
        
        iNivel = 1
        scod1 = m_sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(m_sUON1))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, m_sUON1 & " - " & oUON1.Item(1).Den, "UON1")
        nodx.Tag = "UON1" & CStr(m_sUON1)
    End If
    
    If m_sUON2 <> "" Then
        Set oUON2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
        oUON2.CargarTodasLasUnidadesOrgNivel2 m_sUON1, m_sUON2, m_sUON3, m_sDep, True, , m_sUON2, , True
        
        iNivel = 2
        scod1 = m_sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(m_sUON1))
        scod2 = scod1 & m_sUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(m_sUON2))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, m_sUON2 & " - " & oUON2.Item(1).Den, "UON2")
        nodx.Tag = "UON2" & CStr(m_sUON2)
    End If
    
    If m_sUON3 <> "" Then
        Set oUON3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
        oUON3.CargarTodasLasUnidadesOrgNivel3 m_sUON1, m_sUON2, m_sUON3, m_sDep, True, , m_sUON3, , True
        
        iNivel = 3
        scod1 = m_sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(m_sUON1))
        scod2 = scod1 & m_sUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(m_sUON2))
        scod3 = scod2 & m_sUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(m_sUON3))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, m_sUON3 & " - " & oUON3.Item(1).Den, "UON3")
        nodx.Tag = "UON3" & CStr(m_sUON3)
    End If
    
    'Departamento
    If m_sDep <> "" Then
        Set oDep = oFSGSRaiz.Generar_CDepartamentos
        oDep.CargarTodosLosDepartamentos m_sDep, , True
        
        If scod3 <> "" Then
            scod4 = scod3 & m_sDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(m_sDep))
            Set nodx = tvwestrorg.Nodes.Add("UON" & iNivel & scod3, tvwChild, "DEP" & scod4, m_sDep & " - " & oDep.Item(1).Den, "Departamento")
        ElseIf scod2 <> "" Then
            scod4 = scod2 & m_sDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(m_sDep))
            Set nodx = tvwestrorg.Nodes.Add("UON" & iNivel & scod2, tvwChild, "DEP" & scod4, m_sDep & " - " & oDep.Item(1).Den, "Departamento")
        ElseIf scod1 <> "" Then
            scod4 = scod1 & m_sDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(m_sDep))
            Set nodx = tvwestrorg.Nodes.Add("UON" & iNivel & scod1, tvwChild, "DEP" & scod4, m_sDep & " - " & oDep.Item(1).Den, "Departamento")
            Else
            scod4 = m_sDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(m_sDep))
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEP" & scod4, m_sDep & " - " & oDep.Item(1).Den, "Departamento")
            
        End If
        nodx.Tag = "DEP" & CStr(m_sDep)
    
        'a�ade el comprador al �rbol
        Set nodx = tvwestrorg.Nodes.Add("DEP" & scod4, tvwChild, "PERS" & CStr(frmUSUARIOS.g_oUsuario.Persona.Cod), CStr(frmUSUARIOS.g_oUsuario.Persona.Cod) & " - " & frmUSUARIOS.g_oUsuario.Persona.Apellidos & " " & frmUSUARIOS.g_oUsuario.Persona.nombre, "Persona")
        nodx.Tag = "PER" & CStr(frmUSUARIOS.g_oUsuario.Persona.Cod)
        If frmUSUARIOS.g_oUsuario.Tipo = comprador Then
            nodx.Image = "COMP"
        End If
        nodx.Selected = True
    End If
        
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oDep = Nothing
End Sub

Private Sub cmdRuta_Click()
    Dim vDatos As Variant
    vDatos = MostrarFormUSUPERSWinsecRutas(oGestorIdiomas, gParametrosInstalacion.gIdioma, oMensajes, m_vRutaLocal, m_vRutaUnidad, m_vUnidad, picDatos.Enabled)
    
    m_vRutaLocal = vDatos(0)
    m_vRutaUnidad = vDatos(1)
    m_vUnidad = vDatos(2)
End Sub

