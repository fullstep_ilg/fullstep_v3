VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAdmPROVEPortalBuscar 
   Caption         =   "DResultado de busqueda de actividades"
   ClientHeight    =   5265
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7590
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAdmPROVEPortalBuscar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5265
   ScaleWidth      =   7590
   Begin VB.CommandButton cmdlistado 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7125
      Picture         =   "frmAdmPROVEPortalBuscar.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   60
      Width           =   315
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   0
      Top             =   0
   End
   Begin SSDataWidgets_B.SSDBGrid sdbcActividades 
      Height          =   4815
      Left            =   0
      TabIndex        =   1
      Top             =   435
      Width           =   7575
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      Row.Count       =   4
      Col.Count       =   7
      Row(0).Col(0)   =   "Impresoras (001-002-003)"
      Row(0).Col(1)   =   "3"
      Row(1).Col(0)   =   "Imprimibles y cartones de plástico (001-003)"
      Row(1).Col(1)   =   "3"
      Row(2).Col(0)   =   "Impermeables, ropa de monte y zapatillas deportivas (003-004-005-006-007)"
      Row(2).Col(1)   =   "4"
      Row(3).Col(0)   =   "Imperdibles (006-007-007-007)"
      Row(3).Col(1)   =   "1"
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmAdmPROVEPortalBuscar.frx":0D37
      DividerType     =   0
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      HeadStyleSet    =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   10901
      Columns(0).Caption=   "Actividades"
      Columns(0).Name =   "NOMACT"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HeadBackColor=   16777215
      Columns(1).Width=   2011
      Columns(1).Caption=   "Proveedores"
      Columns(1).Name =   "NUMPROVE"
      Columns(1).Alignment=   2
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   1
      Columns(1).ButtonsAlways=   -1  'True
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HeadBackColor=   16777215
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ACT1"
      Columns(2).Name =   "ACT1"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ACT2"
      Columns(3).Name =   "ACT2"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "ACT3"
      Columns(4).Name =   "ACT3"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ACT4"
      Columns(5).Name =   "ACT4"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ACT5"
      Columns(6).Name =   "ACT5"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   13361
      _ExtentY        =   8493
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCriterio 
      BackColor       =   &H00C0E0FF&
      Caption         =   "DCriterio de busqueda:"
      Height          =   435
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   1980
   End
   Begin VB.Label lblCaracteres 
      BackColor       =   &H00C0E0FF&
      Caption         =   "DImpr"
      Height          =   435
      Left            =   1980
      TabIndex        =   2
      Top             =   0
      Width           =   5715
   End
End
Attribute VB_Name = "frmAdmPROVEPortalBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
        ' creado el 29/01/2002
        ' formulario con todas las actividades que comienzan con los caracteres
        ' introducidos desde el formulario de administración de proveedores del portal
        

Option Explicit

'variables publicas
Public g_ArItem As Ador.Recordset
Public g_sOrigen As String
Public g_oGrupoMatNivel4 As CGrupoMatNivel4
'variables privadas
Private m_oProveedores As CProveedores
Private m_sSeleccion As String
Private m_stxtTitulo As String
Private m_stxtSeleccion As String
Private m_stxtPag As String
Private m_stxtDe As String
Private m_stxtActividades As String
Private m_stxtNumProve As String

Private Sub cmdlistado_Click()
Dim RecordSortFields(1 To 1) As String
Dim oReport As CRAXDRT.Report
Dim pv As Preview
Dim oFos As FileSystemObject
Dim RepPath As String
Dim SelectionText(1 To 2, 1 To 1) As String

      
    If crs_Connected = False Then
        Exit Sub
    End If
    
    m_sSeleccion = lblCaracteres.caption
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptActivProvPortal.rpt"
        
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
   
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    Screen.MousePointer = vbHourglass
    
    ' FORMULA FIELDS A NIVEL DE REPORT

    SelectionText(2, 1) = "TITULO"
    SelectionText(1, 1) = m_stxtTitulo
    
    ' Formula Fields
   
    oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionText(2, 1))).Text = """" & SelectionText(1, 1) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & m_sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & m_stxtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtActividades")).Text = """" & m_stxtActividades & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNumProv")).Text = """" & m_stxtNumProve & """"
    
    oReport.Database.SetDataSource g_ArItem
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    DoEvents
    frmESPERA.Show
    frmESPERA.lblGeneral.caption = frmAdmPROVEPortalBuscar.caption
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""

    
    Set pv = New Preview
    pv.Hide
    pv.caption = m_stxtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    Timer1.Enabled = True
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Set oReport = Nothing
    Unload Me
    DoEvents
    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Load()

    Me.Height = 5655
    Me.Width = 7680
    
    CargarRecursos
    
    PonerFieldSeparator Me
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADMPROVPORBUS, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then

        m_stxtTitulo = Ador(0).Value
        Me.caption = Ador(0).Value     '1 - [ comunicando con el portal ] -
        Ador.MoveNext
        lblCriterio.caption = Ador(0).Value  '2
        m_stxtSeleccion = Ador(0).Value
        Ador.MoveNext
        sdbcActividades.Columns(0).caption = Ador(0).Value  '3  Actividades
        m_stxtActividades = Ador(0).Value
        Ador.MoveNext
        sdbcActividades.Columns(1).caption = Ador(0).Value  '4
        m_stxtNumProve = Ador(0).Value
        Ador.MoveNext
        m_stxtDe = Ador(0).Value
        Ador.MoveNext
        m_stxtPag = Ador(0).Value
        
        Ador.MoveNext
        If gParametrosGenerales.gbSincronizacionMat = True Then
            Me.caption = Ador(0).Value   '7  Resultado de busqueda de materiales
            Ador.MoveNext
            sdbcActividades.Columns(0).caption = Ador(0).Value  '8  Materiales
        End If
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Resize()
    If Height < 1500 Then Exit Sub
    If Width < 3000 Then Exit Sub
    
    If frmAdmPROVEPortalBuscar.Height >= 900 Then
        sdbcActividades.Height = frmAdmPROVEPortalBuscar.Height - 900
    End If
    If frmAdmPROVEPortalBuscar.Width >= 300 Then
        sdbcActividades.Width = frmAdmPROVEPortalBuscar.Width - 300
    End If
    
    sdbcActividades.Width = frmAdmPROVEPortalBuscar.Width - 100
    sdbcActividades.Height = frmAdmPROVEPortalBuscar.Height - 830
    
    sdbcActividades.Columns("NOMACT").Width = 0.8 * sdbcActividades.Width
    sdbcActividades.Columns("NUMPROVE").Width = 0.17 * sdbcActividades.Width
    
    lblCaracteres.Width = sdbcActividades.Width - lblCriterio.Width
    lblCaracteres.Left = lblCriterio.Width
    
    cmdlistado.Left = sdbcActividades.Width - cmdlistado.Width - 50

End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_sOrigen = ""
    Set g_oGrupoMatNivel4 = Nothing
    Set m_oProveedores = Nothing
    Me.Visible = False

End Sub

Private Sub sdbcActividades_BtnClick()
Dim oProve As CProveedor
Dim aut As Integer

    Set m_oProveedores = Nothing
    Set m_oProveedores = oFSGSRaiz.generar_CProveedores
    Screen.MousePointer = vbHourglass
    frmAdmPROVEPortalResult.g_vACN1Seleccionada = StrToNull(sdbcActividades.Columns("ACT1").Value)
    frmAdmPROVEPortalResult.g_vACN2Seleccionada = StrToNull(sdbcActividades.Columns("ACT2").Value)
    frmAdmPROVEPortalResult.g_vACN3Seleccionada = StrToNull(sdbcActividades.Columns("ACT3").Value)
    frmAdmPROVEPortalResult.g_vACN4Seleccionada = StrToNull(sdbcActividades.Columns("ACT4").Value)
    frmAdmPROVEPortalResult.g_vACN5Seleccionada = StrToNull(sdbcActividades.Columns("ACT5").Value)
    frmAdmPROVEPortalResult.g_sNombre = ""
    frmAdmPROVEPortalResult.g_sPais = ""
    frmAdmPROVEPortalResult.g_sProvi = ""
    frmAdmPROVEPortalResult.g_bSoloFS = False
    frmAdmPROVEPortalResult.g_bSoloAut = False
    frmAdmPROVEPortalResult.g_bPremium = False
    
    frmAdmPROVEPortalResult.g_sOrigen = g_sOrigen
    frmAdmPROVEPortalResult.g_sOrigenCarga = "BuscarAct"
    
    If g_sOrigen = "frmSELPROVEAnya" Then
        Set frmAdmPROVEPortalResult.g_oGrupoMatNivel4 = g_oGrupoMatNivel4
    End If
    
    frmEsperaPortal.Show
    DoEvents
    m_oProveedores.CargarProveedoresPortalConActividad StrToNull(sdbcActividades.Columns("ACT1").Value), StrToNull(sdbcActividades.Columns("ACT2").Value), StrToNull(sdbcActividades.Columns("ACT3").Value), StrToNull(sdbcActividades.Columns("ACT4").Value), StrToNull(sdbcActividades.Columns("ACT5").Value)
    Unload frmEsperaPortal
    frmAdmPROVEPortalResult.sdbgProve.RemoveAll
    For Each oProve In m_oProveedores
        With oProve

            If .EstadoEnPortal = fsgsserver.TESPCActivo Then
                aut = 1
            Else
                aut = 0
            End If
            frmAdmPROVEPortalResult.sdbgProve.AddItem .CodPortal & Chr(m_lSeparador) & .Den & Chr(m_lSeparador) & Chr(m_lSeparador) & .CodPais & " " & .DenProvi & Chr(m_lSeparador) & Chr(m_lSeparador) & .Cod & Chr(m_lSeparador) & aut & Chr(m_lSeparador) & .IDPortal & Chr(m_lSeparador) & .EsPremium & Chr(m_lSeparador) & .Cod

        End With
    Next
    
    Screen.MousePointer = vbNormal
    frmAdmPROVEPortalResult.Hide
    frmAdmPROVEPortalResult.Show

End Sub

Private Sub Timer1_Timer()
    If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
End Sub
