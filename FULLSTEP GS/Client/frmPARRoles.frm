VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPARRoles 
   Caption         =   "Relaciones proceso / personas involucradas (Consulta)"
   ClientHeight    =   4800
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6540
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPARRoles.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4800
   ScaleWidth      =   6540
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   6540
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   4245
      Width           =   6540
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2340
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         Height          =   345
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   1200
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "&Filtrar"
         Height          =   345
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   1200
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Left            =   2340
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdCodigo 
         Caption         =   "&C�digo"
         Height          =   345
         Left            =   3480
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   5460
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
   End
   Begin VB.PictureBox picIdiomas 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   60
      ScaleHeight     =   435
      ScaleWidth      =   6150
      TabIndex        =   10
      Top             =   105
      Width           =   6150
      Begin SSDataWidgets_B.SSDBCombo sdbcIdi 
         Height          =   285
         Left            =   1395
         TabIndex        =   11
         Top             =   135
         Width           =   2895
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   5106
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "ID"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "OFFSET"
         Columns(2).Name =   "OFFSET"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.Label lblIdiMat 
         Caption         =   "DIdioma"
         Height          =   285
         Left            =   270
         TabIndex        =   12
         Top             =   165
         Width           =   700
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRoles 
      Height          =   3315
      Left            =   0
      TabIndex        =   9
      Top             =   720
      Width           =   6435
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   1
      GroupHeaders    =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPARRoles.frx":014A
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      BalloonHelp     =   0   'False
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   1323
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777152
      Columns(1).Width=   5980
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   100
      Columns(2).Width=   1429
      Columns(2).Caption=   "Conv."
      Columns(2).Name =   "CONV"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   2
      Columns(3).Width=   1588
      Columns(3).Caption=   "Invitado"
      Columns(3).Name =   "OBS"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      _ExtentX        =   11351
      _ExtentY        =   5847
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPARRoles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmPARRoles
''' *** Creacion: 10/3/1999 (Javier Arana)

Option Explicit

Public Accion As accionessummit
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean

'' Colecci�n de Idiomas
Private oIdiomas As CIdiomas
Private oIdioma As CIdioma
''' Coleccion de Roles
Public oRoles As CRoles
''' Rol en edicion
Private oRolEnEdicion As CRol
Private oIBAseDatosEnEdicion As IBaseDatos
''' Listado de Roles
Public bOrdenListadoDen As Boolean
''' Propiedades de seguridad
Private bModif  As Boolean
''' Control de errores
Private bModError As Boolean
Private bAnyaError As Boolean
Private bValError As Boolean
''' Variables de control
Private bCambioGeneral As Boolean
Private bModoEdicion As Boolean
''' Posicion para UnboundReadData
Private p As Long
'Vable. del form para eliminar las filas en el UnboundDeleteRow
Private iDifBoomark As Integer
'Multilenguaje
Private sIdiTitulos(1 To 4) As String
Private sIdiRolesCod As String
Private sIdiCodigo As String
Private sIdiConsulta As String
Private sIdiEdicion As String
Private sIdiLaRoles As String
Private sIdiDenominacion As String
Private sIdiOrdenando As String
Private sIdiCod As String
Private sMensajes(4) As String

Private Sub ConfigurarSeguridad()
    ''' * Objetivo: Configurar el formulario segun los permisos
    ''' * Objetivo: del usuario
    bModif = True
    cmdModoEdicion.Visible = True
End Sub

Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 5830 Then   'de tama�o de la ventana
            Me.Width = 6030        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 2615 Then  'cuando no se maximiza ni
            Me.Height = 2715       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width
            
    
    If Height >= 1900 Then sdbgRoles.Height = Height - 1900
    If Width >= 250 Then sdbgRoles.Width = Width - 250
    
    sdbgRoles.Columns(0).Width = sdbgRoles.Width * 10 / 100
    sdbgRoles.Columns(1).Width = sdbgRoles.Width * 50 / 100 - 570
    sdbgRoles.Columns(2).Width = sdbgRoles.Width * 20 / 100
    sdbgRoles.Columns(3).Width = sdbgRoles.Width * 20 / 100  ' redimensiono el grid para meter al invitado Kepa Mu�oz 20/01/2006
        
    cmdModoEdicion.Left = sdbgRoles.Left + sdbgRoles.Width - cmdModoEdicion.Width
    
End Sub
Private Sub cmdA�adir_Click()
    
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgRoles.Scroll 0, sdbgRoles.Rows - sdbgRoles.Row
    
    If sdbgRoles.VisibleRows > 0 Then
        
        If sdbgRoles.VisibleRows > sdbgRoles.Rows Then
            sdbgRoles.Row = sdbgRoles.Rows
        Else
            sdbgRoles.Row = sdbgRoles.Rows - (sdbgRoles.Rows - sdbgRoles.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgRoles.SetFocus
    
End Sub
Private Sub cmdFiltrar_Click()

    ''' * Objetivo: Activar el formulario de filtrar.
    frmPARRolesFiltrar.WindowState = vbNormal
    frmPARRolesFiltrar.Show 1
    If frmPARRoles.caption = "" Then frmPARRoles.caption = sIdiTitulos(3)
    
End Sub
Private Sub cmdCodigo_Click()
    
    ''' * Objetivo: Cambiar de codigo la Rol actual
    
    Dim teserror As TipoErrorSummit
    
    ''' Resaltar la Rol actual
    
    If sdbgRoles.Rows = 0 Then Exit Sub
    
    sdbgRoles.SelBookmarks.Add sdbgRoles.Bookmark
    
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    
    Set oRolEnEdicion = oRoles.Item(CStr(sdbgRoles.Bookmark))
    Set oIBAseDatosEnEdicion = oRolEnEdicion
  
    
    teserror = oIBAseDatosEnEdicion.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If Me.Visible Then sdbgRoles.SetFocus
        Exit Sub
    End If
        
    oIBAseDatosEnEdicion.CancelarEdicion
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    frmMODCOD.caption = sIdiRolesCod
    frmMODCOD.Left = frmPARRoles.Left + 500
    frmMODCOD.Top = frmPARRoles.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodROL
    frmMODCOD.txtCodAct.Text = oRolEnEdicion.Cod
    Set frmMODCOD.fOrigen = frmPARRoles
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    DoEvents
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
        oMensajes.NoValido sIdiCodigo
        Set oIBAseDatosEnEdicion = Nothing
        Set oRolEnEdicion = Nothing
        Exit Sub
    End If
        
    If UCase(g_sCodigoNuevo) = UCase(oRolEnEdicion.Cod) Then
        oMensajes.NoValido sIdiCodigo
        Set oIBAseDatosEnEdicion = Nothing
        Set oRolEnEdicion = Nothing
        Exit Sub
    End If
            
    bCambioGeneral = False
    
    If UCase(oRolEnEdicion.Cod) = UCase(gParametrosGenerales.gsROLDEF) Then
        bCambioGeneral = True
    End If
    
    ''' Cambiar el codigo
    
    Screen.MousePointer = vbHourglass
    teserror = oIBAseDatosEnEdicion.CambiarCodigo(g_sCodigoNuevo)
    Screen.MousePointer = vbNormal
             
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    ''' Actualizar los datos
    
    If bCambioGeneral Then
          gParametrosGenerales.gsROLDEF = g_sCodigoNuevo
    End If
    
    sdbgRoles.MoveFirst
    sdbgRoles.Refresh
    sdbgRoles.Bookmark = sdbgRoles.SelBookmarks(0)
    sdbgRoles.SelBookmarks.RemoveAll
        
    Set oIBAseDatosEnEdicion = Nothing
    Set oRolEnEdicion = Nothing
    
End Sub
Private Sub cmdDeshacer_Click()

    ''' * Objetivo: Deshacer la edicion en la Rol actual
    
    sdbgRoles.CancelUpdate
    sdbgRoles.DataChanged = False
    
    If Not oRolEnEdicion Is Nothing Then
        oIBAseDatosEnEdicion.CancelarEdicion
        Set oIBAseDatosEnEdicion = Nothing
        Set oRolEnEdicion = Nothing
    End If
    
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    cmdCodigo.Enabled = True
    
    Accion = ACCRolCon
        
End Sub
Private Sub cmdEliminar_Click()

Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim aAux As Variant
Dim aAux2 As Variant
Dim iIndice As Integer
Dim inum As Integer
Dim i As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit
  
    If sdbgRoles.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    iDifBoomark = 0
    
    Select Case sdbgRoles.SelBookmarks.Count
        Case 0
            Screen.MousePointer = vbNormal
            Exit Sub
        Case 1
            irespuesta = oMensajes.PreguntaEliminar(sIdiLaRoles & " " & sdbgRoles.Columns(0).Value & " (" & sdbgRoles.Columns(1).Value & ")")
        Case Is > 1
            irespuesta = oMensajes.PreguntaEliminarRoles(sMensajes(4))
    End Select
    
    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ReDim aIdentificadores(sdbgRoles.SelBookmarks.Count)
    ReDim aBookmarks(sdbgRoles.SelBookmarks.Count)
    
    i = 0
    While i < sdbgRoles.SelBookmarks.Count
        aIdentificadores(i + 1) = oRoles.Item(CStr(sdbgRoles.SelBookmarks(i))).Cod
        aBookmarks(i + 1) = sdbgRoles.SelBookmarks(i)
        sdbgRoles.Bookmark = sdbgRoles.SelBookmarks(i)
        i = i + 1
    Wend
    
    udtTeserror = oRoles.EliminarRolesDeBaseDatos(aIdentificadores)
    If udtTeserror.NumError <> TESnoerror Then
      If udtTeserror.NumError = TESImposibleEliminar Then
          iIndice = 1
          aAux = udtTeserror.Arg1
          inum = UBound(udtTeserror.Arg1, 2)
          
          For i = 0 To inum
              udtTeserror.Arg1(2, i) = oRoles.Item(CStr(sdbgRoles.SelBookmarks(aAux(2, i) - iIndice))).Den
              sdbgRoles.SelBookmarks.Remove (aAux(2, i) - iIndice)
              iIndice = iIndice + 1
          Next
            
          aAux2 = udtTeserror.Arg1
          
          oMensajes.ImposibleEliminacionMultiple 313, aAux2
          If sdbgRoles.SelBookmarks.Count > 0 Then
              sdbgRoles.DeleteSelected
          End If
      Else
          TratarError udtTeserror
          Screen.MousePointer = vbNormal
          Exit Sub
      End If
    Else
        sdbgRoles.DeleteSelected
        DoEvents
    End If
        
    sdbgRoles.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal

    
End Sub
Private Sub cmdListado_Click()
    AbrirLstParametros "frmPARRoles", bOrdenListadoDen
End Sub
Private Sub cmdModoEdicion_Click()
    
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
        sdbcIdi.Enabled = False
        sdbgRoles.AllowAddNew = True
        sdbgRoles.AllowUpdate = True
        sdbgRoles.AllowDelete = False
        
        frmPARRoles.caption = sIdiTitulos(4)
        cmdModoEdicion.caption = sIdiConsulta
        
        cmdFiltrar.Visible = False
        cmdRestaurar.Visible = False
        cmdListado.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
            
        bModoEdicion = True
        
        Accion = ACCRolCon
    
        If oUsuarioSummit.Tipo = TipoDeUsuario.Administrador Then
            cmdCodigo.Visible = True
        Else
            cmdCodigo.Visible = False
        End If
        
    Else
                
        If sdbgRoles.DataChanged = True Then
        
            v = sdbgRoles.ActiveCell.Value
            If Me.Visible Then sdbgRoles.SetFocus
            If (v <> "") Then
                sdbgRoles.ActiveCell.Value = v
            End If
            
            'bValError = False
            'bAnyaError = False
            'bModError = False
                           
            If (actualizarYSalir() = True) Then
                Exit Sub
            End If
        End If
        
        sdbcIdi.Enabled = True

        sdbgRoles.AllowAddNew = False
        sdbgRoles.AllowUpdate = False
        sdbgRoles.AllowDelete = False
                
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdCodigo.Visible = False
        cmdFiltrar.Visible = True
        cmdRestaurar.Visible = True
        cmdListado.Visible = True
        
        If oUsuarioSummit.Tipo <> TipoDeUsuario.Administrador Then
            cmdCodigo.Visible = False
        End If
    
        frmPARRoles.caption = sIdiTitulos(3)
        cmdModoEdicion.caption = sIdiEdicion
        
        cmdRestaurar_Click
        
        bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgRoles.SetFocus
    
End Sub

Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    actualizarYSalir = False
    If sdbgRoles.DataChanged = True Then
        bValError = False
        bAnyaError = False
        bModError = False
        If sdbgRoles.Row = 0 Then
            If sdbgRoles.Rows = 1 Then
                sdbgRoles.Update
                If bValError Or bAnyaError Or bModError Then
                    actualizarYSalir = True
                    Exit Function
                End If
            Else
                sdbgRoles.MoveNext
                DoEvents
                If bValError Or bAnyaError Or bModError Then
                    actualizarYSalir = True
                    Exit Function
                Else
                    sdbgRoles.MovePrevious
                End If
            End If
        Else
            sdbgRoles.MovePrevious
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgRoles.MoveNext
            End If
        End If
    End If
End Function

Private Sub cmdRestaurar_Click()

    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
        
    Screen.MousePointer = vbHourglass
    
    Me.caption = sIdiTitulos(3)
    
    Set oRoles = Nothing
    
    Set oRoles = oFSGSRaiz.generar_CRoles
  
    oRoles.CargarTodosLosRoles , , , , , True, sdbcIdi.Columns(1).Value
    
    sdbgRoles.ReBind
    
    If Me.Visible Then sdbgRoles.SetFocus
    
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub Form_Load()

    ''' * Objetivo: Cargar las Roles e iniciar
    ''' * Objetivo: el formulario
    
    Me.Width = 6630
    Me.Height = 4785
    
    CargarRecursos
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
     Me.Left = 0
    End If

    
    Set oRoles = oFSGSRaiz.generar_CRoles
    
    Screen.MousePointer = vbHourglass
    oRoles.CargarTodosLosRoles , , , , , True, basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    Show
    
    bModoEdicion = False
    
    Accion = ACCRolCon
        
    bOrdenListadoDen = False
    
    ConfigurarSeguridad
    
    'Carga el combo con los idiomas
    
    Set oIdiomas = oGestorParametros.DevolverIdiomas
    Dim indice As Integer
    Dim bm As Variant
    indice = 0
    For Each oIdioma In oIdiomas
        sdbcIdi.AddItem oIdioma.Den & Chr(9) & oIdioma.Cod & Chr(9) & oIdioma.OFFSET
        If oIdioma.Cod = basPublic.gParametrosInstalacion.gIdioma Then
            bm = sdbcIdi.GetBookmark(indice)
        End If
        indice = indice + 1
    Next
        
    'mostrar por defecto el idioma de la instalaci�n
    
    sdbcIdi.Bookmark = bm
    sdbcIdi.Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
     
End Sub
Private Sub Form_Resize()
    
    ''' * Objetivo: Adecuar los controles
    Arrange
    
End Sub

''' * Objetivo: Descargar el formulario si no
''' * Objetivo: hay cambios pendientes
Private Sub Form_Unload(Cancel As Integer)
    Dim v As Variant
    
    If sdbgRoles.DataChanged Then
        v = sdbgRoles.ActiveCell.Value
        If Me.Visible Then sdbgRoles.SetFocus
        sdbgRoles.ActiveCell.Value = v
        
        If actualizarYSalir() Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    Set oRoles = Nothing
    Set oRolEnEdicion = Nothing
    Set oIBAseDatosEnEdicion = Nothing
    Me.Visible = False
End Sub

Private Sub sdbcIdi_CloseUp()
    Dim sCodigo As String
    Dim sDenominacion As String
    Dim lCodigo As Integer
    Dim lDenominacion As Integer
    Dim sHeadCaption As String

    If caption <> sIdiTitulos(3) Then
    
        If InStr(1, caption, sIdiTitulos(1), vbTextCompare) <> 0 Then
        
            ''' Datos filtrados por codigo
            
            If InStr(1, caption, "*", vbTextCompare) <> 0 Then
                
                lCodigo = Len(caption) - Len(sIdiTitulos(1))
                sCodigo = Mid(caption, Len(sIdiTitulos(1)) + 1, lCodigo - 1)
                Screen.MousePointer = vbHourglass
                oRoles.CargarTodosLosRoles Trim(sCodigo), , , , , True, sdbcIdi.Columns(1).Value
                Screen.MousePointer = vbNormal
                
            Else
            
                lCodigo = Len(caption) - Len(sIdiTitulos(1))
                sCodigo = Mid(caption, Len(sIdiTitulos(1)) + 1, lCodigo)
                oRoles.CargarTodosLosRoles Trim(sCodigo), , True, , , True, sdbcIdi.Columns(1).Value
                
           End If
           
        Else
            
            ''' Datos filtrados por denominacion
            
            lDenominacion = Len(caption) - Len(sIdiTitulos(2))
            sDenominacion = Mid(caption, Len(sIdiTitulos(2)) + 1, lDenominacion)
            
            If InStr(1, sDenominacion, "*", vbTextCompare) <> 0 Then
            
                sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
                oRoles.CargarTodosLosRoles , Trim(sDenominacion), , , , True, sdbcIdi.Columns(1).Value
                            
            Else
                
                sDenominacion = Left(sDenominacion, Len(sDenominacion))
                oRoles.CargarTodosLosRoles , Trim(sDenominacion), True, , , True, sdbcIdi.Columns(1).Value
            
            End If
                
        End If
   
    Else
   
        Screen.MousePointer = vbHourglass
        oRoles.CargarTodosLosRoles , , , , , True, sdbcIdi.Columns(1).Value
        Screen.MousePointer = vbNormal
    
    End If
    
   
    sdbgRoles.ReBind
    sdbgRoles.AllowAddNew = True
    sdbgRoles.AllowUpdate = True
    sdbgRoles.AllowDelete = True

    If cmdModoEdicion.caption <> sIdiConsulta Then
        sdbgRoles.AllowAddNew = False
        sdbgRoles.AllowUpdate = False
        sdbgRoles.AllowDelete = False
    End If

End Sub

Private Sub sdbgRoles_AfterDelete(RtnDispErrMsg As Integer)

    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    RtnDispErrMsg = 0
    If Me.Visible Then sdbgRoles.SetFocus
    sdbgRoles.Bookmark = sdbgRoles.RowBookmark(sdbgRoles.Row)
    
End Sub
Private Sub sdbgRoles_AfterInsert(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub
Private Sub sdbgRoles_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
    If bAnyaError = False And bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub
Private Sub sdbgRoles_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    
    DispPromptMsg = 0

End Sub
Private Sub sdbgRoles_BeforeUpdate(Cancel As Integer)

    ''' * Objetivo: Validar los datos
    
    bValError = False
    Cancel = False
    
    If Trim(sdbgRoles.Columns(0).Value) = "" Then
        oMensajes.NoValido sIdiCodigo
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgRoles.Columns(1).Value) = "" Then
        oMensajes.NoValida sIdiDenominacion
        Cancel = True
        GoTo Salir
    End If
    
Salir:
        
    bValError = Cancel
    If Me.Visible Then sdbgRoles.SetFocus
        
End Sub
Private Sub sdbgRoles_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
        cmdCodigo.Enabled = False
    
    End If
    
    If Accion = ACCRolCon And Not sdbgRoles.IsAddRow Then
    
        Set oRolEnEdicion = Nothing
        Set oRolEnEdicion = oRoles.Item(CStr(sdbgRoles.Bookmark))
      
        Set oIBAseDatosEnEdicion = oRolEnEdicion
        
        teserror = oIBAseDatosEnEdicion.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
            
            TratarError teserror
            sdbgRoles.DataChanged = False
            sdbgRoles.Columns("COD").Value = oRolEnEdicion.Cod
            sdbgRoles.Columns("DEN").Value = oRolEnEdicion.Den
            sdbgRoles.Columns("CONV").Value = BooleanToSQLBinary(oRolEnEdicion.Conv)
            sdbgRoles.Columns("OBS").Value = BooleanToSQLBinary(oRolEnEdicion.Invi) ' a�ado el dato de invitado Kepa Mu�oz 20/01/2006
            teserror.NumError = TESnoerror
            
        End If
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgRoles.SetFocus
        Else
            Accion = ACCRolMod
        End If
    End If
End Sub
Private Sub sdbgRoles_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    
    Dim sCodigo As String
    Dim sDenominacion As String
    Dim lCodigo As Integer
    Dim lDenominacion As Integer
    Dim sHeadCaption As String
    
    If bModoEdicion Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgRoles.Columns(ColIndex).caption
    sdbgRoles.Columns(ColIndex).caption = sIdiOrdenando
    
    ''' Volvemos a cargar las Roles, ordenadas segun la columna
    ''' en cuyo encabezado se ha hecho 'clic'.
    
    Set oRoles = Nothing
    Set oRoles = oFSGSRaiz.generar_CRoles
   
    If caption <> sIdiTitulos(3) Then
    
        If InStr(1, caption, sIdiTitulos(1), vbTextCompare) <> 0 Then
        
            ''' Datos filtrados por codigo
            
            If InStr(1, caption, "*", vbTextCompare) <> 0 Then
                
                lCodigo = Len(caption) - Len(sIdiTitulos(1))
                sCodigo = Mid(caption, Len(sIdiTitulos(1)) + 1, lCodigo - 1)
            
                Select Case ColIndex
                Case 0
                    oRoles.CargarTodosLosRoles Trim(sCodigo), , , , , True, sdbcIdi.Columns(1).Value
                Case 1
                    oRoles.CargarTodosLosRoles Trim(sCodigo), , , True, , True, sdbcIdi.Columns(1).Value
                Case 2
                    oRoles.CargarTodosLosRoles Trim(sCodigo), , , , True, True, sdbcIdi.Columns(1).Value
                End Select
                
            Else
            
                lCodigo = Len(caption) - Len(sIdiTitulos(1))
                sCodigo = Mid(caption, Len(sIdiTitulos(1)) + 1, lCodigo)
            
                Select Case ColIndex
                Case 0
                    oRoles.CargarTodosLosRoles Trim(sCodigo), , True, , , True, sdbcIdi.Columns(1).Value
                Case 1
                    oRoles.CargarTodosLosRoles Trim(sCodigo), , True, True, , True, sdbcIdi.Columns(1).Value
                Case 2
                    oRoles.CargarTodosLosRoles Trim(sCodigo), , True, , True, True, sdbcIdi.Columns(1).Value
                End Select
                
           End If
           
        Else
            
            ''' Datos filtrados por denominacion
            
            lDenominacion = Len(caption) - Len(sIdiTitulos(2))
            sDenominacion = Mid(caption, Len(sIdiTitulos(2)) + 1, lDenominacion)
            
            If InStr(1, sDenominacion, "*", vbTextCompare) <> 0 Then
            
                sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
                
                Select Case ColIndex
                Case 0
                    oRoles.CargarTodosLosRoles , Trim(sDenominacion), , , , True, sdbcIdi.Columns(1).Value
                Case 1
                    oRoles.CargarTodosLosRoles , Trim(sDenominacion), , True, , True, sdbcIdi.Columns(1).Value
                Case 2
                    oRoles.CargarTodosLosRoles , Trim(sDenominacion), , , True, True, sdbcIdi.Columns(1).Value
                End Select
                            
            Else
                
                sDenominacion = Left(sDenominacion, Len(sDenominacion))
                
                Select Case ColIndex
                Case 0
                    oRoles.CargarTodosLosRoles , Trim(sDenominacion), True, , , True, sdbcIdi.Columns(1).Value
                Case 1
                    oRoles.CargarTodosLosRoles , Trim(sDenominacion), True, True, , True, sdbcIdi.Columns(1).Value
                Case 2
                    oRoles.CargarTodosLosRoles , Trim(sDenominacion), True, , True, True, sdbcIdi.Columns(1).Value
                End Select
                    
            End If
                
        End If
                
    Else
    
        ''' Datos no filtrados
        
        Select Case ColIndex
        Case 0
            oRoles.CargarTodosLosRoles , , , , , True, sdbcIdi.Columns(1).Value
        Case 1
            oRoles.CargarTodosLosRoles , , , True, , True, sdbcIdi.Columns(1).Value
        Case 2
            oRoles.CargarTodosLosRoles , , , , True, True, sdbcIdi.Columns(1).Value
        End Select
    
    End If
        
    Select Case ColIndex
    Case 0
        bOrdenListadoDen = False
    Case 1
        bOrdenListadoDen = True
    Case 2
        bOrdenListadoDen = False
    End Select
    
    sdbgRoles.ReBind
    sdbgRoles.Columns(ColIndex).caption = sHeadCaption

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbgRoles_InitColumnProps()
    
    sdbgRoles.Columns(0).FieldLen = basParametros.gLongitudesDeCodigos.giLongCodROL
    
End Sub

Private Sub sdbgRoles_KeyDown(KeyCode As Integer, Shift As Integer)
'***********************************************************************
'*** Captura la tecla Supr para poder eliminar desde el teclado  *******
'***********************************************************************

    If KeyCode = vbKeyDelete Then
        If bModoEdicion And sdbgRoles.SelBookmarks.Count > 0 Then
            cmdEliminar_Click
        End If
    End If

End Sub

Private Sub sdbgRoles_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgRoles.DataChanged = False Then
            
            sdbgRoles.CancelUpdate
            sdbgRoles.DataChanged = False
            
            If Not oRolEnEdicion Is Nothing Then
                oIBAseDatosEnEdicion.CancelarEdicion
                Set oIBAseDatosEnEdicion = Nothing
                Set oRolEnEdicion = Nothing
            End If
           
            If Not sdbgRoles.IsAddRow Then
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
            Else
                cmdA�adir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = False
            End If
            
            Accion = ACCRolCon
            
        Else
        
            If sdbgRoles.IsAddRow Then
           
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
           
                Accion = ACCRolCon
            
            End If
            
        End If
        
    End If
    
End Sub

Private Sub sdbgRoles_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgRoles.IsAddRow Then
        sdbgRoles.Columns(0).Locked = True
        If sdbgRoles.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
            cmdCodigo.Enabled = True
        End If
    Else
        If sdbgRoles.DataChanged = True Then cmdDeshacer.Enabled = True
        sdbgRoles.Columns(0).Locked = False
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgRoles.Row) Then
                sdbgRoles.col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdCodigo.Enabled = False
        
    End If
        
End Sub
Private Sub sdbgRoles_UnboundAddData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, NewRowBookmark As Variant)

    ''' * Objetivo: Hemos anyadido una fila al grid,
    ''' * Objetivo: ahora hay que anyadirla a la
    ''' * Objetivo: coleccion
    ''' * Recibe: Buffer con los datos a anyadir y bookmark de la fila
        
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    
    bAnyaError = False
    
    ''' Anyadir a la coleccion en todos los idiomas
    ' a�ado la columna OBS para el invitado Kepa Mu�oz 20/01/2006
    Set oIdiomas = oGestorParametros.DevolverIdiomas
    For Each oIdioma In oIdiomas
        If oIdioma.Cod = sdbcIdi.Columns(1).Value Then
            oRoles.Add sdbgRoles.Columns(0).Value, sdbgRoles.Columns(1).Value, GridCheckToBoolean(sdbgRoles.Columns("CONV").Value), GridCheckToBoolean(sdbgRoles.Columns("OBS").Value), sdbcIdi.Columns(1).Value, oRoles.Count
        Else
            oRoles.Add sdbgRoles.Columns(0).Value, "", GridCheckToBoolean(sdbgRoles.Columns("CONV").Value), GridCheckToBoolean(sdbgRoles.Columns("OBS").Value), oIdioma.Cod, oRoles.Count
        End If
    
        ''' Anyadir a la base de datos
        
        Set oRolEnEdicion = oRoles.Item(CStr(oRoles.Count - 1))
        
        Set oIBAseDatosEnEdicion = oRolEnEdicion
        teserror = oIBAseDatosEnEdicion.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
             
            v = sdbgRoles.ActiveCell.Value
            oRoles.Remove (CStr(oRoles.Count - 1))
            TratarError teserror
            If Me.Visible Then sdbgRoles.SetFocus
            bAnyaError = True
            RowBuf.RowCount = 0
            sdbgRoles.ActiveCell.Value = v
            Set oIBAseDatosEnEdicion = Nothing
            Set oRolEnEdicion = Nothing
            Exit Sub
             
        Else
            ''' Registro de acciones
            If gParametrosGenerales.gbActivLog Then
                oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCRolAnya, "Cod:" & oRolEnEdicion.Cod
            End If
            Accion = ACCRolCon
        End If
    
        If oIdioma.Cod <> sdbcIdi.Columns(1).Value Then
            oRoles.Remove (oRoles.Count)
        End If
    Next
    
    Set oIBAseDatosEnEdicion = Nothing
    Set oRolEnEdicion = Nothing
        
End Sub
Private Sub sdbgRoles_UnboundDeleteRow(vBookmark As Variant)

     ' * Objetivo: Hemos eliminado una fila al grid,
   ' * Objetivo: ahora hay que eliminarla de la
   ' * Objetivo: coleccion
   ' * Recibe: Bookmark de la fila
    
    Dim udtTeserror As TipoErrorSummit
    Dim bEliminado As Boolean
    Dim lIndFor As Long, lIndPro As Long
        
    
    lIndPro = val(vBookmark) - iDifBoomark
    iDifBoomark = iDifBoomark + 1
    If gParametrosGenerales.gbActivLog Then
       oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCRolEli, sIdiCod & oRoles.Item(CStr(lIndPro)).Cod
    End If
            
    ' Eliminar de la coleccion
    
    For lIndFor = lIndPro To oRoles.Count - 2
       oRoles.Remove (CStr(lIndFor))
       Set oRolEnEdicion = oRoles.Item(CStr(lIndFor + 1))
       oRoles.Add oRolEnEdicion.Cod, oRolEnEdicion.Den, oRolEnEdicion.Conv, oRolEnEdicion.Invi, oRolEnEdicion.idioma, lIndFor
       Set oRolEnEdicion = Nothing
       Next lIndFor
        
       oRoles.Remove (CStr(lIndFor))
       Accion = ACCRolCon

    
End Sub
Private Sub sdbgRoles_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Integer
    Dim i As Integer
    Dim j As Integer
    
    Dim oRol As CRol
        
    Dim iNumRoles As Integer

    If oRoles Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If

    iNumRoles = oRoles.Count
    
    If IsNull(StartLocation) Then       'If the grid is empty then
        If ReadPriorRows Then               'If moving backwards through grid then
            p = iNumRoles - 1                             'pointer = # of last grid row
        Else                                        'else
            p = 0                                       'pointer = # of first grid row
        End If
    Else                                        'If the grid already has data in it then
        p = StartLocation                       'pointer = location just before or after the row where data will be added
    
        If ReadPriorRows Then               'If moving backwards through grid then
                p = p - 1                               'move pointer back one row
        Else                                        'else
                p = p + 1                               'move pointer ahead one row
        End If
    End If
    
    'The pointer (p) now points to the row of the grid where you will start adding data.
    
    For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
        
        If p < 0 Or p > iNumRoles - 1 Then Exit For           'If the pointer is outside the grid then stop this
    
        Set oRol = Nothing
        Set oRol = oRoles.Item(CStr(p))
    
        For j = 0 To 3 ' modificado por Kepa Mu�oz para a�adir la columna invitado 20/01/2006
      
            Select Case j
                    
                    Case 0:
                            RowBuf.Value(i, 0) = oRol.Cod
                    Case 1:
                            RowBuf.Value(i, 1) = oRol.Den
                    Case 2:
                            RowBuf.Value(i, 2) = oRol.Conv
                    Case 3:
                            RowBuf.Value(i, 3) = oRol.Invi
                                        
            End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
        Next j
    
        RowBuf.Bookmark(i) = p                              'set the value of the bookmark for the current row in the rowbuffer
    
        If ReadPriorRows Then                               'move the pointer forward or backward, depending
            p = p - 1                                           'on which way it's supposed to move
        Else
            p = p + 1
        End If
            r = r + 1                                               'increment the number of rows read
        Next i
    
    RowBuf.RowCount = r                                         'set the size of the row buffer to the number of rows read

    Set oRol = Nothing
    
End Sub
Private Sub sdbgRoles_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
    
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim v As Variant
    
    bModError = False
    
    ''' Modificamos en la base de datos
    
    With oRolEnEdicion
        .Cod = sdbgRoles.Columns(0).Value
        .Den = sdbgRoles.Columns(1).Value
        .Conv = GridCheckToBoolean(sdbgRoles.Columns("CONV").Value)
        .Invi = GridCheckToBoolean(sdbgRoles.Columns("OBS").Value) ' a�ado el invitado Kepa Mu�oz 20/01/2006
    End With
    
    teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando
       
    If teserror.NumError <> TESnoerror Then
    
        v = sdbgRoles.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgRoles.SetFocus
        bModError = True
        RowBuf.RowCount = 0
        sdbgRoles.ActiveCell.Value = v
        
    Else
        ''' Registro de acciones
        
        If gParametrosGenerales.gbActivLog Then
            oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCRolAnya, "Cod:" & oRolEnEdicion.Cod
        End If
        
        Accion = ACCRolCon
        
        Set oIBAseDatosEnEdicion = Nothing
        Set oRolEnEdicion = Nothing
    End If
End Sub



Public Sub ponerCaption(Texto, indice, comodin)
    
caption = sIdiTitulos(indice) & Texto & IIf(comodin, "*", "")
  

End Sub

    
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PARROLES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    sIdiTitulos(1) = Ador(0).Value & " "
    Ador.MoveNext
    sIdiTitulos(2) = Ador(0).Value & " "
    Ador.MoveNext
    sIdiTitulos(3) = Ador(0).Value
    caption = sIdiTitulos(3)
    Ador.MoveNext
    sIdiTitulos(4) = Ador(0).Value
    Ador.MoveNext
    sdbgRoles.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbgRoles.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    sdbgRoles.Columns(2).caption = Ador(0).Value
    Ador.MoveNext
    
    cmdA�adir.caption = Ador(0).Value
    Ador.MoveNext
    cmdCodigo.caption = Ador(0).Value
    Ador.MoveNext
    cmdDeshacer.caption = Ador(0).Value
    Ador.MoveNext
    cmdEliminar.caption = Ador(0).Value
    Ador.MoveNext
    cmdFiltrar.caption = Ador(0).Value
    Ador.MoveNext
    cmdListado.caption = Ador(0).Value
    Ador.MoveNext
    cmdModoEdicion.caption = Ador(0).Value
    Ador.MoveNext
    cmdRestaurar.caption = Ador(0).Value
    Ador.MoveNext
    
    
    sIdiRolesCod = Ador(0).Value
    Ador.MoveNext
    sIdiCodigo = Ador(0).Value
    Ador.MoveNext
    sIdiConsulta = Ador(0).Value
    Ador.MoveNext
    sIdiEdicion = Ador(0).Value
    Ador.MoveNext
    sIdiLaRoles = Ador(0).Value
    Ador.MoveNext
    sIdiDenominacion = Ador(0).Value
    Ador.MoveNext
    sIdiOrdenando = Ador(0).Value
    Ador.MoveNext
    lblIdiMat.caption = Ador(0).Value & ":"
    Ador.MoveNext
    sMensajes(4) = Ador(0).Value
    Ador.MoveNext
    sdbgRoles.Columns("OBS").caption = Ador(0).Value ' a�ado el caption para "Invitado" Kepa Mu�oz 20/01/2006

    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub





