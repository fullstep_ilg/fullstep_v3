VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPlantillaDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   8055
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7815
   Icon            =   "frmPlantillaDetalle.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7326.622
   ScaleMode       =   0  'User
   ScaleWidth      =   7845.058
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraPedidoDirecto 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   120
      TabIndex        =   15
      Top             =   2040
      Width           =   7575
      Begin VB.CheckBox chkParaPedidoDef 
         BackColor       =   &H00808000&
         Caption         =   "Plantilla por defecto"
         ForeColor       =   &H8000000E&
         Height          =   315
         Left            =   3360
         TabIndex        =   17
         Top             =   120
         Width           =   3255
      End
      Begin VB.CheckBox chkParaPedido 
         BackColor       =   &H00808000&
         Caption         =   "Plantilla para emitir pedido directo"
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   120
         Width           =   2895
      End
   End
   Begin VB.PictureBox picTapa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   210
      Left            =   120
      Picture         =   "frmPlantillaDetalle.frx":0CB2
      ScaleHeight     =   210
      ScaleWidth      =   195
      TabIndex        =   14
      Top             =   2880
      Width           =   200
   End
   Begin VB.Frame fraSoloEstosMateriales 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   240
      TabIndex        =   12
      Top             =   2400
      Width           =   7815
      Begin VB.CheckBox chkSoloEstosMateriales 
         BackColor       =   &H00808000&
         Caption         =   "La plantilla s�lo ser� v�lida para los siguientes materiales"
         ForeColor       =   &H8000000E&
         Height          =   195
         Left            =   0
         TabIndex        =   13
         Top             =   120
         Width           =   5000
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1965
      Left            =   120
      ScaleHeight     =   1965
      ScaleWidth      =   7695
      TabIndex        =   5
      Top             =   60
      Width           =   7695
      Begin VB.CommandButton cmdFecha 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7230
         Picture         =   "frmPlantillaDetalle.frx":0EAC
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   60
         Width           =   315
      End
      Begin VB.TextBox txtDescr 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   1140
         MaxLength       =   500
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   510
         Width           =   6375
      End
      Begin VB.TextBox txtFecha 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5760
         TabIndex        =   3
         Top             =   30
         Width           =   1395
      End
      Begin VB.TextBox txtNombre 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1140
         MaxLength       =   100
         TabIndex        =   1
         Top             =   30
         Width           =   3315
      End
      Begin VB.Label lblDescr 
         BackColor       =   &H00808000&
         Caption         =   "Descripci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   0
         TabIndex        =   9
         Top             =   570
         Width           =   975
      End
      Begin VB.Label lblFecha 
         BackColor       =   &H00808000&
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   4740
         TabIndex        =   8
         Top             =   90
         Width           =   975
      End
      Begin VB.Label lblNom 
         BackColor       =   &H00808000&
         Caption         =   "Nombre:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   0
         TabIndex        =   7
         Top             =   30
         Width           =   795
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2775
      TabIndex        =   4
      Top             =   7410
      Width           =   1035
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3975
      TabIndex        =   0
      Top             =   7410
      Width           =   1035
   End
   Begin MSComctlLib.TreeView tvwEstrMatMod 
      Height          =   4140
      Left            =   240
      TabIndex        =   10
      Top             =   3120
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   7303
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageListMod"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList3 
      Left            =   0
      Top             =   7440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":1436
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":14E6
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":193A
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":19FB
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":1ABB
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":1B6B
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":1C35
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":1CEE
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":1D9E
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwEstrMat 
      Height          =   4140
      Left            =   0
      TabIndex        =   11
      Top             =   3000
      Visible         =   0   'False
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   7303
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList3"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageListMod 
      Left            =   240
      Top             =   7320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":1E5E
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":1F0E
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":2260
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":26B4
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":2764
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":2814
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":28CD
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":2984
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillaDetalle.frx":2A34
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPlantillaDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private vez As Integer

Private m_sFecha As String
Private m_sDescr As String
Private m_sNombre As String

Private m_oIBaseDatos As IBaseDatos

' edu PM 98

Private oCEstructura As CAEstructuraMat
Private sIdiMat As String

'Variable para no ejecutar el node check si se ha disparado el mousedown

Private bMouseDown As Boolean
Private SelNode As MSComctlLib.node
Private bRMatAsig As Boolean

'permisos sobre asignacion de materiales

Private m_bRestringirSoloMaterialesComprador As Boolean
Private m_bRestringirAsignacionAtributosMateriales As Boolean
Private sIdiMarcaruno As String

Public bOcultarMateriales As Boolean

Private Sub chkParaPedido_Click()

    If chkParaPedido.Value = vbChecked Then
        chkParaPedidoDef.Enabled = True
    Else
        chkParaPedidoDef.Value = vbUnchecked
        chkParaPedidoDef.Enabled = False
    End If
            
End Sub

Private Sub chkSoloEstosMateriales_Click()
    If chkSoloEstosMateriales.Enabled = False Then
        Exit Sub
    End If
    If chkSoloEstosMateriales.Value = vbChecked Then
        'ConfigurarMateriales True
    Else
        ' eliminar todas las marcas del arbol
        If oCEstructura Is Nothing Then
            Set oCEstructura = New CAEstructuraMat
        End If
        oCEstructura.Desmarcar tvwEstrMatMod, False
    End If

End Sub

' fin de pm 98

' fin de declaraciones

Private Sub cmdAceptar_Click()
Dim udtTeserror As TipoErrorSummit
Dim oPlantilla As CPlantilla
   
    If Trim(txtNombre.Text) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sNombre
        If Me.Visible Then txtNombre.SetFocus
        Exit Sub
    End If
    
    If Trim(txtFecha.Text) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sFecha
        If Me.Visible Then txtFecha.SetFocus
        Exit Sub
    Else
        If Not IsDate(txtFecha.Text) Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido m_sFecha
            If Me.Visible Then txtFecha.SetFocus
            Exit Sub
        End If
    End If
    
    If fraSoloEstosMateriales.Visible = True Then
        'edu Incidencia 7213 de T98
        ' si hay restriccion de material exigir al menos un material marcado
        ' solo en caso de alta.
        If chkSoloEstosMateriales = vbChecked Then
            If oCEstructura.ContarMarcas(tvwEstrMatMod) <= 0 Then
                oMensajes.NoValido sIdiMarcaruno '"Se debe marcar al menos un material"
                If Me.Visible Then txtNombre.SetFocus
                Exit Sub
            End If
        End If
    End If
    

    Select Case frmPlantillasProce.g_Accion
    
        Case ACCPlantillasMod
               
            frmPlantillasProce.g_oPlantillaSeleccionada.Descripcion = Trim(txtDescr.Text)
            frmPlantillasProce.g_oPlantillaSeleccionada.nombre = Trim(txtNombre.Text)
            frmPlantillasProce.g_oPlantillaSeleccionada.Fecha = Trim(txtFecha.Text)
            frmPlantillasProce.g_oPlantillaSeleccionada.ParaPedido = (Me.chkParaPedido.Value = vbChecked)
            frmPlantillasProce.g_oPlantillaSeleccionada.ParaPedidoDef = (Me.chkParaPedidoDef.Value = vbChecked)
            
            Set m_oIBaseDatos = frmPlantillasProce.g_oPlantillaSeleccionada
            udtTeserror = m_oIBaseDatos.FinalizarEdicionModificando
            If udtTeserror.NumError = TESnoerror Then
                ModificarPlantilla
                'edu tarea 98
                'a peticion de encarni, la modificacion de materiales se traslada al formulario frmPlantillasProce
                'y en este se trata exclusivamente el alta y la modificacion del titulo
                'oCEstructura.GuardarMateriales tvwEstrMatMod, oUsuarioSummit.comprador, frmPlantillasProce.g_oPlantillaSeleccionada, "MODIFICACION"
                RegistrarAccion ACCPlantillasMod, "Id:" & frmPlantillasProce.g_oPlantillaSeleccionada.Id
                'frmPlantillasProce.CargarEstructuraMateriales
                
                Unload Me
            Else
                Screen.MousePointer = vbNormal
                TratarError udtTeserror
                Exit Sub
            End If

            frmPlantillasProce.lstvwplantillas_ItemClick frmPlantillasProce.lstvwplantillas.selectedItem
        
        Case ACCPlantillasAnya
        
            Set frmPlantillasProce.g_oPlantillaSeleccionada = oFSGSRaiz.Generar_CPlantilla
            frmPlantillasProce.g_oPlantillaSeleccionada.Descripcion = Trim(txtDescr.Text)
            frmPlantillasProce.g_oPlantillaSeleccionada.nombre = Trim(txtNombre.Text)
            frmPlantillasProce.g_oPlantillaSeleccionada.Fecha = Trim(txtFecha.Text)
            If ADMIN_OBLIGATORIO Then
                frmPlantillasProce.g_oPlantillaSeleccionada.AdminPub = True
            Else
                If basParametros.gParametrosGenerales.gbAdminPublica = True Then
                    If basParametros.gParametrosGenerales.gCPProcesoAdminPub = True Then
                        frmPlantillasProce.g_oPlantillaSeleccionada.AdminPub = True
                    Else
                        frmPlantillasProce.g_oPlantillaSeleccionada.AdminPub = False
                    End If
                Else
                    frmPlantillasProce.g_oPlantillaSeleccionada.AdminPub = False
                End If
            End If


            frmPlantillasProce.g_oPlantillaSeleccionada.esp = Null
            
            frmPlantillasProce.g_oPlantillaSeleccionada.ParaPedido = (Me.chkParaPedido.Value = vbChecked)
            frmPlantillasProce.g_oPlantillaSeleccionada.ParaPedidoDef = (Me.chkParaPedidoDef.Value = vbChecked)
            
            'Si se marca la plantilla como plantilla para pedido "por defecto"
            'comprovamos si tiene restricciones de material
            'ya que solo puede haber una plantilla por defecto sin material
            frmPlantillasProce.g_oPlantillaSeleccionada.ActuPedidoDef = False
            If frmPlantillasProce.g_oPlantillaSeleccionada.ParaPedidoDef = True Then
            
                If chkSoloEstosMateriales = vbUnchecked Then
                                 
                    For Each oPlantilla In frmPlantillasProce.g_oPlantillas
                
                        If oPlantilla.ExisteRestriccionMaterial = False And oPlantilla.ParaPedidoDef = True Then
                    
                            frmPlantillasProce.g_oPlantillaSeleccionada.ActuPedidoDef = True
                                        
                            'Como solo puede haber una plantilla "por defecto" sin materiales
                            'una vez encuentro la plantilla y la desmarco "por defecto"
                            'salgo del bucle
                            Exit For
                    
                        End If
                    Next
                End If
            End If
            
            udtTeserror = frmPlantillasProce.g_oPlantillaSeleccionada.Insertar(frmPlantillasProce.g_oPlantillaSeleccionada, oCEstructura.AsignarMateriales(tvwEstrMatMod, frmPlantillasProce.g_oPlantillaSeleccionada))
            
            If udtTeserror.NumError = TESnoerror Then
            
                If frmPlantillasProce.g_oPlantillaSeleccionada.ActuPedidoDef = True Then
                    oPlantilla.ParaPedidoDef = False
                End If
                
                RegistrarAccion ACCPlantillasAnya, "Id:" & frmPlantillasProce.g_oPlantillaSeleccionada.Id

                With frmPlantillasProce.g_oPlantillaSeleccionada
                    frmPlantillasProce.g_lNumID = .Id
                    frmPlantillasProce.g_oPlantillas.Add .Id, .nombre, .Fecha, NullToStr(.Descripcion), .Destino, .FormaPago, .FechaSuministro, .ProveedorAct, .DistribUON, .PresAnualesTipo1, .PresAnualesTipo2, .PresTipo1, .PresTipo2, .EspecProce, .EspecGrupo, .EspecItem, .FechaAct, , , .Solicitud, .AdminPub, .esp, bSubasta:=.ModoSubasta, iSubastaEspera:=IIf(IsNull(.MinutosEspera), 0, .MinutosEspera), bUnSoloPedido:=.DefUnSoloPedido, _
                                                         bParaPedido:=.ParaPedido, bParaPedidoDef:=.ParaPedidoDef, iSubTipo:=.Subtipo, iSubModo:=.SubModo, vSubDuracion:=.SubDuracion, bSubPublicar:=.SubPublicar, bSubNotifEventos:=.SubNotifEventos, vSubMinJaponesa:=.SubMinJaponesa, bSubVerDesdePrimPuja:=.SubVerDesdePrimPuja, iSubBajMinGanTipo:=.SubBajMinGanTipo, vSubBajMinGanProcVal:=.SubBajMinGanProcVal, _
                                                         vSubBajMinGanGrupoVal:=.SubBajMinGanGrupoVal, vSubBajMinGanItemVal:=.SubBajMinGanItemVal, bSubBajMinProve:=.SubBajMinProve, iSubBajMinProveTipo:=.SubBajMinProveTipo, vSubBajMinProveProcVal:=.SubBajMinProveProcVal, vSubBajMinProveGrupoVal:=.SubBajMinProveGrupoVal, _
                                                         vSubBajMinProveItemVal:=.SubBajMinProveItemVal
                    frmPlantillasProce.CargarGridPlantillas True
                End With
            Else
                Screen.MousePointer = vbNormal
                TratarError udtTeserror
                Exit Sub
            End If

    End Select

    Screen.MousePointer = vbNormal

    frmPlantillasProce.g_Accion = ACCPlantillasCon

    Unload Me

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdFecha_Click()
    AbrirFormCalendar Me, txtFecha
End Sub



Private Sub ConfigurarMateriales()
    
    Screen.MousePointer = vbHourglass

    If m_bRestringirSoloMaterialesComprador Then
        chkSoloEstosMateriales.Value = vbChecked
        fraSoloEstosMateriales.Enabled = False
    Else
        chkSoloEstosMateriales.Value = vbUnchecked
        fraSoloEstosMateriales.Enabled = True
    End If
        
    Select Case frmPlantillasProce.g_Accion
    
    Case ACCPlantillasCon ' consulta
    
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        picDatos.Enabled = False
        Me.Height = Me.Height - 300

    Case ACCPlantillasAnya ' alta

        Select Case m_bRestringirSoloMaterialesComprador

        Case True
            Select Case oUsuarioSummit.comprador Is Nothing
            Case False
                oCEstructura.GenerarEstructuraDeComprador oUsuarioSummit.comprador, tvwEstrMatMod
            Case True
                'Segun prototipo: Si el usuario que est� creando la plantilla tiene restriccion
                oCEstructura.GenerarEstructuraMateriales , tvwEstrMatMod
            End Select
        Case False
            chkSoloEstosMateriales.Value = False
            fraSoloEstosMateriales.Enabled = True 'False Por incidencia 7489
            Select Case oUsuarioSummit.comprador Is Nothing
            Case False
                ' Usuario sin restricci�n y con comprador : Derivar material del comprador y permitir seleccionar cualquier art�culo.
                'Por incidencia 7546, que no aparezcan marcas ya que el usuario no tiene restriccion.
                oCEstructura.GenerarEstructuraMateriales , tvwEstrMatMod
            Case True
                'Usuario sin restricci�n y sin comprador : Permitir seleccionar cualquier art�culo.
                oCEstructura.GenerarEstructuraMateriales , tvwEstrMatMod
            End Select
        End Select

        oCEstructura.MemorizarEstructura tvwEstrMatMod

    Case ACCPlantillasMod ' modificacion
        
        'edu pm98
        'a peticion de Encarni, se elimina la modificacion de materiales de plantilla de este formulario a frmplantillasproce
        
        'ocultar estructura de materiales
        
        OcultarEstructuraMateriales

    End Select

    Screen.MousePointer = vbNormal
End Sub


Private Sub OcultarEstructuraMateriales()
        tvwEstrMat.Visible = False
        tvwEstrMatMod.Visible = False
        picTapa.Visible = False

        fraSoloEstosMateriales.Visible = False
        Me.Height = Me.Height - tvwEstrMat.Height - chkSoloEstosMateriales.Height - 500
        cmdAceptar.Top = cmdAceptar.Top - tvwEstrMat.Height - chkSoloEstosMateriales.Height - 200
        cmdCancelar.Top = cmdAceptar.Top
        
        picTapa.Top = tvwEstrMatMod.Top + 60
        picTapa.Left = tvwEstrMatMod.Top + 60
End Sub

Private Sub CargarRecursos()
Dim Adores As Ador.Recordset

    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_PLANTILLAS_DETALLE, basPublic.gParametrosInstalacion.gIdioma)
        
        If Not Adores Is Nothing Then
        
        lblNom.caption = Adores(0).Value
        m_sNombre = Adores(0).Value
        Adores.MoveNext
        lblDescr.caption = Adores(0).Value
        m_sDescr = Adores(0).Value
        Adores.MoveNext
        lblFecha.caption = Adores(0).Value
        m_sFecha = Adores(0).Value
        Adores.MoveNext
        cmdAceptar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        
        ' edu T98
        Adores.MoveNext
        sIdiMat = Adores(0).Value ' 6
        
        Adores.MoveNext
        sIdiMarcaruno = Adores(0).Value ' 7
        Adores.MoveNext
        chkSoloEstosMateriales.caption = Adores(0).Value
        Adores.MoveNext
        chkParaPedido.caption = Adores(0).Value
        Adores.MoveNext
        chkParaPedidoDef.caption = Adores(0).Value
        Adores.Close
        
        End If

    Set Adores = Nothing

End Sub
Private Sub ModificarPlantilla()
Dim Item As MSComctlLib.listItem

    Set Item = frmPlantillasProce.lstvwplantillas.selectedItem
    Item.Text = txtNombre.Text
    Set Item = Nothing

End Sub

Private Sub Form_Activate()

    'edu pm 98. se debe llamar a esta funcion en el evento Activate, ya que si la llamamos en el Load
    'al no ser visible el treeview las marcas de check no se asignan
    
    If vez <> 1 And bOcultarMateriales <> True Then
        vez = 1
        ConfigurarMateriales
    End If
    
    If bOcultarMateriales Then
        frmPlantillaDetalle.chkParaPedido.Enabled = False
        frmPlantillaDetalle.chkParaPedidoDef.Enabled = False
        OcultarEstructuraMateriales
    End If


End Sub

Private Sub Form_Load()

    CargarRecursos

    ' edu PM 98 : configurar el formulario en funcion de los permisos de materiales y instanciar CAEstructuraMat que permitir� gestionar la estructura de materiales a trav�s del arbol.
    
    ConfigurarSeguridad
    
    Set oCEstructura = New CAEstructuraMat
    oCEstructura.sTituloArbol = sIdiMat
    
    picTapa.Top = tvwEstrMatMod.Top + 60
    picTapa.Left = tvwEstrMatMod.Left + 60

    If m_bRestringirSoloMaterialesComprador Then
        oCEstructura.CrearArbolPosiblesGMN oUsuarioSummit.comprador
    End If
    
    chkParaPedidoDef.Enabled = False
        
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmPlantillasProce.g_Accion = ACCPlantillasCon
    Set m_oIBaseDatos = Nothing
    vez = 0
End Sub

' edu tarea 98

Private Sub ConfigurarSeguridad()

    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
        'edu. por email de encarni, testear si el usuario es comprador antes de aplicar restriccion
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANTILLASRestringirSoloMaterialesComprador)) Is Nothing) Then
                 m_bRestringirSoloMaterialesComprador = True
            End If
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANTILLASRestringirAsignacionAtributosMateriales)) Is Nothing) Then
                 m_bRestringirAsignacionAtributosMateriales = True
            End If
        End If
    End If

End Sub



Private Sub tvwEstrMatMod_KeyUp(KeyCode As Integer, Shift As Integer)
    
    If bMouseDown Then
        bMouseDown = False
        oCEstructura.MarcarNodo SelNode, m_bRestringirSoloMaterialesComprador
    End If
    
End Sub

Private Sub tvwEstrMatMod_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.node
Dim tvhti As TVHITTESTINFO

                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrMatMod.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrMatMod.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento
                             
                bMouseDown = True
                Set SelNode = nod
                             
                End If
            End If
        End If
    End If
    
End Sub

Private Sub tvwEstrMatMod_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

    If bMouseDown Then
        bMouseDown = False
        oCEstructura.MarcarNodo SelNode, m_bRestringirSoloMaterialesComprador
        'edu Nuevo requisito:
        ' si se desmarcan todos los nodos desmarcar el check de restriccion
        If SelNode.Checked = False Then
            If oCEstructura.ContarMarcas(tvwEstrMatMod) <= 0 Then
                'edu 2008-02-20: excepcion al requisito anterior:
                'EXCEPTO si el usuario tiene restriccion de material.
                If Not m_bRestringirSoloMaterialesComprador Then
                    CheckMateriales vbUnchecked
                End If
            End If
        Else
            'edu 2008-02-26: Nuevo requisito:
            ' si se marcan todos los nodos marcar el check de restriccion
            If Not m_bRestringirSoloMaterialesComprador Then
                CheckMateriales vbChecked
            End If
        End If
        DoEvents
    End If

End Sub


Private Sub tvwEstrMatMod_NodeCheck(ByVal node As MSComctlLib.node)

    If bMouseDown Then
        Exit Sub
    End If
    
    bMouseDown = True
    Set SelNode = node

End Sub

' marcar automaticamente el check de materiales vitando que se dispare el evento check.

Sub CheckMateriales(modo As Variant)

    If chkSoloEstosMateriales.Value = modo Then
        Exit Sub
    End If
    If chkSoloEstosMateriales.Enabled = True Then
        chkSoloEstosMateriales.Enabled = False
        chkSoloEstosMateriales.Value = modo
        chkSoloEstosMateriales.Enabled = True
    Else
        chkSoloEstosMateriales.Value = modo
    End If
    
    

End Sub
