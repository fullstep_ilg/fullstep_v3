VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPROVEMatPorProve 
   Caption         =   "Material por proveedor"
   ClientHeight    =   5385
   ClientLeft      =   0
   ClientTop       =   1485
   ClientWidth     =   5700
   Icon            =   "frmPROVEMatPorProve.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5385
   ScaleWidth      =   5700
   Begin TabDlg.SSTab sstabEstrMat 
      Height          =   4125
      Left            =   0
      TabIndex        =   12
      Top             =   750
      Width           =   5685
      _ExtentX        =   10028
      _ExtentY        =   7276
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Material asignado"
      TabPicture(0)   =   "frmPROVEMatPorProve.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "tvwEstrMatMod"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Picture2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "tvwEstrMat"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Art�culos homologados"
      TabPicture(1)   =   "frmPROVEMatPorProve.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Picture1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin MSComctlLib.TreeView tvwEstrMat 
         Height          =   3615
         Left            =   90
         TabIndex        =   13
         Top             =   390
         Width           =   5475
         _ExtentX        =   9657
         _ExtentY        =   6376
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         SingleSel       =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox Picture2 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   210
         Left            =   180
         Picture         =   "frmPROVEMatPorProve.frx":0182
         ScaleHeight     =   210
         ScaleWidth      =   195
         TabIndex        =   17
         Top             =   450
         Width           =   200
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3645
         Left            =   -74910
         ScaleHeight     =   3645
         ScaleWidth      =   5535
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   390
         Width           =   5535
         Begin SSDataWidgets_B.SSDBGrid sdbgArticulos 
            Height          =   3465
            Left            =   60
            TabIndex        =   16
            Top             =   60
            Width           =   5385
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            Col.Count       =   3
            stylesets.count =   1
            stylesets(0).Name=   "ActiveRow"
            stylesets(0).ForeColor=   16777215
            stylesets(0).BackColor=   -2147483647
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPROVEMatPorProve.frx":037C
            stylesets(0).AlignmentText=   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            PictureButton   =   "frmPROVEMatPorProve.frx":0398
            BalloonHelp     =   0   'False
            MaxSelectedRows =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   2408
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   3
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777152
            Columns(1).Width=   4895
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   50
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777152
            Columns(2).Width=   953
            Columns(2).Caption=   "Hom."
            Columns(2).Name =   "HOM"
            Columns(2).CaptionAlignment=   2
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   2
            _ExtentX        =   9499
            _ExtentY        =   6112
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin MSComctlLib.TreeView tvwEstrMatMod 
         Height          =   3615
         Left            =   120
         TabIndex        =   14
         Top             =   390
         Visible         =   0   'False
         Width           =   5445
         _ExtentX        =   9604
         _ExtentY        =   6376
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   450
      Left            =   0
      ScaleHeight     =   450
      ScaleWidth      =   5700
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   4935
      Width           =   5700
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   105
         TabIndex        =   9
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2325
         TabIndex        =   8
         Top             =   45
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1215
         TabIndex        =   7
         Top             =   45
         Width           =   1005
      End
   End
   Begin VB.Frame fraSelComp 
      Height          =   735
      Left            =   60
      TabIndex        =   4
      Top             =   0
      Width           =   5580
      Begin VB.CommandButton cmdBuscar 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5130
         Picture         =   "frmPROVEMatPorProve.frx":04AA
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
         Height          =   285
         Left            =   2115
         TabIndex        =   10
         Top             =   240
         Width           =   2985
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 1"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 0"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
         Height          =   285
         Left            =   900
         TabIndex        =   0
         Top             =   240
         Width           =   1215
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2143
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "Proveedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   60
         TabIndex        =   5
         Top             =   300
         Width           =   870
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   495
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0537
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":05E7
            Key             =   "GMDes"
            Object.Tag             =   "GMDes"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0679
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0ACD
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0B8F
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0C51
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0D01
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0DC9
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0E80
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0F30
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   510
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   5700
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   4425
      Visible         =   0   'False
      Width           =   5700
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2880
         TabIndex        =   3
         Top             =   105
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1740
         TabIndex        =   2
         Top             =   105
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmPROVEMatPorProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variable para no ejecutar el node check si se ha disparado el mousedown
Private bMouseDown As Boolean
Private SelNode As MSComctlLib.node

'Variable para la gesti�n de homologaci�n de art�culos
Private oIArtHom As IArticulosHomologados
Private oArticulos As CArticulos
'Variables del idioma
Private sIdioma() As String

'Variables del resize
Private iAltura As Integer
Private iAnchura As Integer

' Variables de seguridad
Private bREqp As Boolean
Private bRMatAsig As Boolean
Private bModif As Boolean

Private oIMaterial As IMaterialAsignado
Public oGruposMN1 As CGruposMatNivel1
Public oGruposMN2 As CGruposMatNivel2
Public oGruposMN3 As CGruposMatNivel3
Public oGruposMN4 As CGruposMatNivel4

Private oGruposMN1Seleccionados As CGruposMatNivel1
Private oGruposMN2Seleccionados As CGruposMatNivel2
Private oGruposMN3Seleccionados As CGruposMatNivel3
Private oGruposMN4Seleccionados As CGruposMatNivel4
Private oGruposMN1DesSeleccionados As CGruposMatNivel1
Private oGruposMN2DesSeleccionados As CGruposMatNivel2
Private oGruposMN3DesSeleccionados As CGruposMatNivel3
Private oGruposMN4DesSeleccionados As CGruposMatNivel4
Private oGrupsMN1Comp As CGruposMatNivel1
Private oGrupsMN2Comp As CGruposMatNivel2
Private oGrupsMN3Comp As CGruposMatNivel3
Private oGrupsMN4Comp As CGruposMatNivel4
Private oProveSeleccionado As CProveedor
Private oProves As CProveedores
Private oIBaseDatos As IBaseDatos
Private RespetarComboProve As Boolean

'Variable que tiene el Asunto para la Notificaci�n de Asignaci�n de Materiales de QA a Proveedores
Private sNotificarAsigMatQASubject As String



Private Sub CargarEstructuraMateriales()

If Not oProveSeleccionado Is Nothing Then
    
    tvwEstrMat.Scroll = False
    
    GenerarEstructuraDeMateriales
    
    If oProveSeleccionado.EsPremium And gParametrosGenerales.gbPremium Then
        cmdModificar.Enabled = False
    Else
        If bModif Then
            cmdModificar.Enabled = True
        Else
            cmdModificar.Enabled = False
        End If
    End If
    cmdRestaurar.Enabled = True
End If

If tvwEstrMat.Nodes.Count = 0 Then Exit Sub

ConfigurarInterfazSeguridad tvwEstrMat.Nodes("Raiz")

On Error Resume Next
tvwEstrMat.Nodes("raiz").Root.Selected = True
If Me.Visible Then tvwEstrMat.SetFocus
tvwEstrMat.Scroll = True
On Error GoTo 0
    

End Sub

Private Sub cmdAceptar_Click()
    Dim GMN1 As String
    Dim GMN2 As String
    Dim GMN3 As String
    Dim GMN4 As String
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node
    Dim nod4 As MSComctlLib.node
    Dim nodx As MSComctlLib.node
    Dim nodxAux As MSComctlLib.node
    Dim teserror As TipoErrorSummit
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim sCod As String
    Dim i As Long
    Dim vbm As Variant
    'Variables de m�todo que tendr�n las colecciones de Materiales a Notificar:
    Dim oGruposMN1SelNotif As CGruposMatNivel1 'Este ser� igual que oGruposMN1Seleccionados
    Dim oGruposMN2SelNotif As CGruposMatNivel2 'Este ser� igual que oGruposMN2Seleccionados
    Dim oGruposMN3SelNotif As CGruposMatNivel3 'Este ser� igual que oGruposMN3Seleccionados
    Dim oGruposMN4SelNotif As CGruposMatNivel4 'Este ser� igual que oGruposMN4Seleccionados
    Dim oGruposMN1DesSelNotif As CGruposMatNivel1
    Dim oGruposMN2DesSelNotif As CGruposMatNivel2
    Dim oGruposMN3DesSelNotif As CGruposMatNivel3
    Dim oGruposMN4DesSelNotif As CGruposMatNivel4
    'Variables que me ayudaran a volver a recorrer el arbol
    Dim nod2Aux As MSComctlLib.node
    Dim nod3Aux As MSComctlLib.node
    Dim nod4Aux As MSComctlLib.node
    'Varibale que me indicar� si algun descendiente de un nodo est� seleccionado
    Dim bCheckedEncontrado
    'Variable que indicar� que el padre del GMN ya est� en una colecci�n, por lo que no tiene que a�adirse a la suya
    Dim bGMN1DesSeleccionado As Boolean
    Dim bGMN2DesSeleccionado As Boolean
    Dim bGMN3DesSeleccionado As Boolean
    bGMN1DesSeleccionado = False
    bGMN2DesSeleccionado = False
    bGMN3DesSeleccionado = False
    
    Set oGruposMN1DesSelNotif = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2DesSelNotif = oFSGSRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3DesSelNotif = oFSGSRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4DesSelNotif = oFSGSRaiz.Generar_CGruposMatNivel4
    
    teserror.NumError = TESnoerror
    
    Screen.MousePointer = vbHourglass
    
    LockWindowUpdate Me.hWnd
    
    If SSTabEstrMat.Tab = 0 Then
        Set nodx = tvwEstrMatMod.Nodes(1)
        If nodx.Children = 0 Then
             LockWindowUpdate 0&
             Screen.MousePointer = vbNormal
             Exit Sub
        End If
           
        Set nod1 = nodx.Child
           
        While Not nod1 Is Nothing
            scod1 = DevolverCod(nod1) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(nod1)))
            ' No asignado
            If Not nod1.Checked Then
                If Not oGruposMN1.Item(scod1) Is Nothing Then
                    'ES POSIBLE QUE EST� EN LA COLECCI�N POR QUE TEN�A ALGUN HIJO MARCADO.
                    Set nodxAux = tvwEstrMat.Nodes("GMN1" & scod1)
                    If nodxAux.Image = "GMN1A" Then
                        oGruposMN1DesSeleccionados.Add DevolverCod(nod1), ""

                        bCheckedEncontrado = False
                        Set nod2Aux = nod1.Child
                        Do While Not nod2Aux Is Nothing And Not bCheckedEncontrado
                            bCheckedEncontrado = nod2Aux.Checked
                            If bCheckedEncontrado Then
                                Exit Do
                            Else
                                Set nod3Aux = nod2Aux.Child
                                Do While Not nod3Aux Is Nothing And Not bCheckedEncontrado
                                    bCheckedEncontrado = nod3Aux.Checked
                                    If bCheckedEncontrado Then
                                        Exit Do
                                    Else
                                        Set nod4Aux = nod3Aux.Child
                                        Do While Not nod4Aux Is Nothing And Not bCheckedEncontrado
                                            bCheckedEncontrado = nod4Aux.Checked
                                            If bCheckedEncontrado Then
                                                Exit Do
                                            End If
                                            Set nod4Aux = nod4Aux.Next
                                        Loop
                                    End If
                                    Set nod3Aux = nod3Aux.Next
                                Loop
                            End If
                            Set nod2Aux = nod2Aux.Next
                        Loop
                        'Tengo que recorrer TODA la descendencia de este nodo.
                        'Si encuentro un solo descendiente que est� SELECCIONADO, rompo el Do While y NO meto el nodo
                        'Si no encuentro ningun descendiente seleccionado, meto el nodo y marco para que no se metan sus descendientes
                        If Not bCheckedEncontrado Then
                            oGruposMN1DesSelNotif.Add DevolverCod(nod1), ""
                            bGMN1DesSeleccionado = True
                        End If
                    End If
                    Set nodxAux = Nothing
                End If
                
                Set nod2 = nod1.Child
                
                While Not nod2 Is Nothing
                    scod2 = DevolverCod(nod2) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(nod2)))
                    ' No asignado
                    If Not nod2.Checked Then
                        If Not oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                            'ES POSIBLE QUE EST� EN LA COLECCI�N POR QUE TEN�A ALGUN HIJO MARCADO.
                            Set nodxAux = tvwEstrMat.Nodes("GMN2" & scod1 & scod2)
                            If nodxAux.Image = "GMN2A" Then
                                oGruposMN2DesSeleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                                If Not bGMN1DesSeleccionado Then
                                    bCheckedEncontrado = False
                                    Set nod3Aux = nod2.Child
                                    Do While Not nod3Aux Is Nothing And Not bCheckedEncontrado
                                        bCheckedEncontrado = nod3Aux.Checked
                                        If bCheckedEncontrado Then
                                            Exit Do
                                        Else
                                            Set nod4Aux = nod3Aux.Child
                                            Do While Not nod4Aux Is Nothing And Not bCheckedEncontrado
                                                bCheckedEncontrado = nod4Aux.Checked
                                                If bCheckedEncontrado Then
                                                    Exit Do
                                                End If
                                                Set nod4Aux = nod4Aux.Next
                                            Loop
                                        End If
                                        Set nod3Aux = nod3Aux.Next
                                    Loop
                                    If Not bCheckedEncontrado Then
                                        oGruposMN2DesSelNotif.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                                        bGMN2DesSeleccionado = True
                                    End If
                                End If
                            End If
                            Set nodxAux = Nothing
                        End If
                
                        Set nod3 = nod2.Child
                        While Not nod3 Is Nothing
                            scod3 = DevolverCod(nod3) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(nod3)))
                            If Not nod3.Checked Then
                                If Not oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                    'ES POSIBLE QUE EST� EN LA COLECCI�N POR QUE TEN�A ALGUN HIJO MARCADO.
                                    Set nodxAux = tvwEstrMat.Nodes("GMN3" & scod1 & scod2 & scod3)
                                    If nodxAux.Image = "GMN3A" Then
                                        oGruposMN3DesSeleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""

                                        If Not bGMN1DesSeleccionado And Not bGMN2DesSeleccionado Then
                                            bCheckedEncontrado = False
                                            Set nod4Aux = nod3.Child
                                            Do While Not nod4Aux Is Nothing And Not bCheckedEncontrado
                                                bCheckedEncontrado = nod4Aux.Checked
                                                If bCheckedEncontrado Then
                                                    Exit Do
                                                End If
                                                Set nod4Aux = nod4Aux.Next
                                            Loop
                                            If Not bCheckedEncontrado Then
                                                oGruposMN3DesSelNotif.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                                bGMN3DesSeleccionado = True
                                            End If
                                        End If
                                    End If
                                    Set nodxAux = Nothing
                                End If
                                Set nod4 = nod3.Child
                                While Not nod4 Is Nothing
                                    scod4 = DevolverCod(nod4) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(DevolverCod(nod4)))
                                    If Not nod4.Checked Then
                                        If Not oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                            'ES POSIBLE QUE EST� EN LA COLECCI�N POR QUE TEN�A ALGUN HIJO MARCADO.
                                            Set nodxAux = tvwEstrMat.Nodes("GMN4" & scod1 & scod2 & scod3 & scod4)
                                            If nodxAux.Image = "GMN4A" Then
                                                oGruposMN4DesSeleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""

                                                If Not bGMN1DesSeleccionado And Not bGMN2DesSeleccionado And Not bGMN3DesSeleccionado Then
                                                    oGruposMN4DesSelNotif.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                                End If
                                            End If
                                            Set nodxAux = Nothing
                                        End If
                                    Else
                                        If oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                            oGruposMN4Seleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                        Else 'ES POSIBLE QUE EST� EN LA COLECCI�N POR QUE TEN�A ALGUN HIJO MARCADO.
                                            Set nodxAux = tvwEstrMat.Nodes("GMN4" & scod1 & scod2 & scod3 & scod4)
                                            If nodxAux.Image = "GMN4" Then
                                                oGruposMN4Seleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                            End If
                                            Set nodxAux = Nothing
                                        End If
                                    End If
                                    
                                    Set nod4 = nod4.Next
                                Wend
                            Else
                                ' Si esta checked GMN3
                                If oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                    'No estaba asignado antes.
                                    oGruposMN3Seleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                Else 'ES POSIBLE QUE EST� EN LA COLECCI�N POR QUE TEN�A ALGUN HIJO MARCADO.
                                    Set nodxAux = tvwEstrMat.Nodes("GMN3" & scod1 & scod2 & scod3)
                                    If nodxAux.Image = "GMN3" Then
                                        oGruposMN3Seleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                    End If
                                    Set nodxAux = Nothing
                                
                                End If
                            End If
                            
                            Set nod3 = nod3.Next

                            bGMN3DesSeleccionado = False
                        Wend
                    Else
                        ' Si esta checked GMN2
                        If oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                            'No estaba asignado antes.
                            oGruposMN2Seleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                        Else 'ES POSIBLE QUE EST� EN LA COLECCI�N POR QUE TEN�A ALGUN HIJO MARCADO.
                            Set nodxAux = tvwEstrMat.Nodes("GMN2" & scod1 & scod2)
                            If nodxAux.Image = "GMN2" Then
                                oGruposMN2Seleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                            End If
                            Set nodxAux = Nothing
                        
                        End If
                    End If
                    
                    Set nod2 = nod2.Next

                    bGMN2DesSeleccionado = False
                Wend
            Else
            ' Si esta checked GMN1
                If oGruposMN1.Item(scod1) Is Nothing Then
                    'No estaba asignado antes.
                    oGruposMN1Seleccionados.Add DevolverCod(nod1), ""
                Else 'ES POSIBLE QUE EST� EN LA COLECCI�N POR QUE TEN�A ALGUN HIJO MARCADO.
                    Set nodxAux = tvwEstrMat.Nodes("GMN1" & scod1)
                    If nodxAux.Image = "GMN1" Then
                        oGruposMN1Seleccionados.Add DevolverCod(nod1), ""
                    End If
                    Set nodxAux = Nothing
                End If
            End If
            Set nod1 = nod1.Next
            bGMN1DesSeleccionado = False
        Wend
    
        'Vaciar variables auxiliares
        Set nod2Aux = Nothing
        Set nod3Aux = Nothing
        Set nod4Aux = Nothing
                
        Set oIMaterial = oProveSeleccionado
         
        Set oIMaterial.GruposMN1 = oGruposMN1DesSeleccionados
        Set oIMaterial.GruposMN2 = oGruposMN2DesSeleccionados
        Set oIMaterial.GruposMN3 = oGruposMN3DesSeleccionados
        Set oIMaterial.GruposMN4 = oGruposMN4DesSeleccionados
                  
        If Not oGruposMN4DesSeleccionados Is Nothing Then
            If Not ComprobarAtributosEnArticulos Then
                Set oIMaterial.GruposMN1 = Nothing
                Set oIMaterial.GruposMN2 = Nothing
                Set oIMaterial.GruposMN3 = Nothing
                Set oIMaterial.GruposMN4 = Nothing
                Set oGruposMN1DesSeleccionados = Nothing
                Set oGruposMN2DesSeleccionados = Nothing
                Set oGruposMN3DesSeleccionados = Nothing
                Set oGruposMN4DesSeleccionados = Nothing
                Set oGruposMN1DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
                Set oGruposMN2DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
                Set oGruposMN3DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
                Set oGruposMN4DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
                LockWindowUpdate 0&
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
          
        teserror = oIMaterial.DesAsignarMaterial
            
        If teserror.NumError <> TESnoerror Then
            LockWindowUpdate 0&
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oIMaterial.GruposMN1 = Nothing
            Set oIMaterial.GruposMN2 = Nothing
            Set oIMaterial.GruposMN3 = Nothing
            Set oIMaterial.GruposMN4 = Nothing
            Set oGruposMN1DesSeleccionados = Nothing
            Set oGruposMN2DesSeleccionados = Nothing
            Set oGruposMN3DesSeleccionados = Nothing
            Set oGruposMN4DesSeleccionados = Nothing
            Set oGruposMN1DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
            Set oGruposMN2DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
            Set oGruposMN3DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
            Set oGruposMN4DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
            Exit Sub
        End If
          
        Set oIMaterial.GruposMN1 = oGruposMN1Seleccionados
        Set oIMaterial.GruposMN2 = oGruposMN2Seleccionados
        Set oIMaterial.GruposMN3 = oGruposMN3Seleccionados
        Set oIMaterial.GruposMN4 = oGruposMN4Seleccionados
        
        teserror = oIMaterial.AsignarMaterial
            
        If teserror.NumError <> TESnoerror Then
            LockWindowUpdate 0&
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oIMaterial.GruposMN1 = Nothing
            Set oIMaterial.GruposMN2 = Nothing
            Set oIMaterial.GruposMN3 = Nothing
            Set oIMaterial.GruposMN4 = Nothing
            Set oGruposMN1Seleccionados = Nothing
            Set oGruposMN2Seleccionados = Nothing
            Set oGruposMN3Seleccionados = Nothing
            Set oGruposMN4Seleccionados = Nothing
            Set oGruposMN1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
            Set oGruposMN2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
            Set oGruposMN3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
            Set oGruposMN4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
            Exit Sub
        End If
             
        If basParametros.gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
            'Hay que notificar a los usuarios de Gestion de Material de QA la [des| ]asignaci�n de materiales de QA a Proveedores
            'Crear las Estructuras de Memoria:
            Dim oProvesSel As CProveedores
            Set oProvesSel = oFSGSRaiz.generar_CProveedores
            oProvesSel.Add oProveSeleccionado.Cod, oProveSeleccionado.Den
            
            Set oGruposMN1SelNotif = oGruposMN1Seleccionados
            Set oGruposMN2SelNotif = oGruposMN2Seleccionados
            Set oGruposMN3SelNotif = oGruposMN3Seleccionados
            Set oGruposMN4SelNotif = oGruposMN4Seleccionados
            
            If oGruposMN1SelNotif.Count = 0 Then
                Set oGruposMN1SelNotif = Nothing
            End If
            If oGruposMN2SelNotif.Count = 0 Then
                Set oGruposMN2SelNotif = Nothing
            End If
            If oGruposMN3SelNotif.Count = 0 Then
                Set oGruposMN3SelNotif = Nothing
            End If
            If oGruposMN4SelNotif.Count = 0 Then
                Set oGruposMN4SelNotif = Nothing
            End If
            If oGruposMN1DesSelNotif.Count = 0 Then
                Set oGruposMN1DesSelNotif = Nothing
            End If
            If oGruposMN2DesSelNotif.Count = 0 Then
                Set oGruposMN2DesSelNotif = Nothing
            End If
            If oGruposMN3DesSelNotif.Count = 0 Then
                Set oGruposMN3DesSelNotif = Nothing
            End If
            If oGruposMN4DesSelNotif.Count = 0 Then
                Set oGruposMN4DesSelNotif = Nothing
            End If
                   
            teserror = NotificarAsigMatQA(sNotificarAsigMatQASubject, oProvesSel, oGruposMN1SelNotif, oGruposMN2SelNotif, oGruposMN3SelNotif, oGruposMN4SelNotif, oProvesSel, oGruposMN1DesSelNotif, oGruposMN2DesSelNotif, oGruposMN3DesSelNotif, oGruposMN4DesSelNotif)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                'Exit Sub
            End If
            Set oGruposMN1SelNotif = Nothing
            Set oGruposMN2SelNotif = Nothing
            Set oGruposMN3SelNotif = Nothing
            Set oGruposMN4SelNotif = Nothing
            Set oGruposMN1DesSelNotif = Nothing
            Set oGruposMN2DesSelNotif = Nothing
            Set oGruposMN3DesSelNotif = Nothing
            Set oGruposMN4DesSelNotif = Nothing
        End If
          
        RegistrarAccion accionessummit.ACCMatPorProveMod, "Cod:" & Trim(sdbcProveCod.Text)
        
        fraSelComp.Enabled = True
        tvwEstrMatMod.Visible = False
        tvwEstrMat.Visible = True
        picNavigate.Visible = True
        picEdit.Visible = False
        
        Set oGruposMN1Seleccionados = Nothing
        Set oGruposMN2Seleccionados = Nothing
        Set oGruposMN3Seleccionados = Nothing
        Set oGruposMN4Seleccionados = Nothing
        Set oGruposMN1DesSeleccionados = Nothing
        Set oGruposMN2DesSeleccionados = Nothing
        Set oGruposMN3DesSeleccionados = Nothing
        Set oGruposMN4DesSeleccionados = Nothing
                
        cmdRestaurar_Click
        SSTabEstrMat.TabVisible(0) = True
    Else
        'Estamos modificando la homologaci�n de art�culos
        
        GMN1 = DevolverCod(tvwEstrMat.selectedItem.Parent.Parent.Parent)
        GMN2 = DevolverCod(tvwEstrMat.selectedItem.Parent.Parent)
        GMN3 = DevolverCod(tvwEstrMat.selectedItem.Parent)
        GMN4 = DevolverCod(tvwEstrMat.selectedItem)
        
        sCod = GMN1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(GMN1))
        sCod = sCod & GMN2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(GMN2))
        sCod = sCod & GMN3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(GMN3))
        sCod = sCod & GMN4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(GMN4))
        
        For i = 0 To sdbgArticulos.Rows - 1
            vbm = sdbgArticulos.AddItemBookmark(i)
            oArticulos.Item(sCod & sdbgArticulos.Columns("COD").CellValue(vbm)).Homologado = Abs((sdbgArticulos.Columns("HOM").CellValue(vbm)) <> "" And (sdbgArticulos.Columns("HOM").CellValue(vbm)) <> "0")
        Next i
                
        teserror = oIArtHom.ActualizarArticulosHomologadosDelProveedor(sdbcProveCod, oArticulos, GMN1, GMN2, GMN3, GMN4)
        
        If teserror.NumError <> TESnoerror Then
            LockWindowUpdate 0&
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If
            
        RegistrarAccion accionessummit.ACCMatPorProveModHom, "Cod:" & Trim(sdbcProveCod.Text)
          
        fraSelComp.Enabled = True
        picNavigate.Visible = True
        picEdit.Visible = False
        
        sdbgArticulos.AllowUpdate = False
        SSTabEstrMat.TabEnabled(0) = True
        sdbgArticulos.MoveFirst
    End If
    
    LockWindowUpdate 0&
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdBuscar_Click()

    frmPROVEBuscar.sOrigen = "MatPorProve"
    
    If bREqp Then
        frmPROVEBuscar.codEqp = basOptimizacion.gCodEqpUsuario
    End If
    
    frmPROVEBuscar.Hide
    frmPROVEBuscar.Show 1
    If Me.Visible Then tvwEstrMat.SetFocus
    
End Sub

Private Sub cmdCancelar_Click()
    LockWindowUpdate Me.hWnd
    
    If SSTabEstrMat.Tab = 0 Then
        
        SSTabEstrMat.TabEnabled(1) = True
        fraSelComp.Enabled = True
        tvwEstrMatMod.Visible = False
        tvwEstrMat.Visible = True
        picNavigate.Visible = True
        picEdit.Visible = False
        
        Set oGruposMN1Seleccionados = Nothing
        Set oGruposMN2Seleccionados = Nothing
        Set oGruposMN3Seleccionados = Nothing
        Set oGruposMN4Seleccionados = Nothing
        Set oGruposMN1DesSeleccionados = Nothing
        Set oGruposMN2DesSeleccionados = Nothing
        Set oGruposMN3DesSeleccionados = Nothing
        Set oGruposMN4DesSeleccionados = Nothing
    Else
        
        Set oArticulos = Nothing
        fraSelComp.Enabled = True
        picNavigate.Visible = True
        picEdit.Visible = False
        sdbgArticulos.AllowUpdate = False
        cmdRestaurar_Click
        SSTabEstrMat.TabEnabled(0) = True
    End If
    
    LockWindowUpdate 0&

End Sub

Private Sub cmdlistado_Click()

    If sdbcProveCod.Text = "" And sdbcProveCod.Text = "" Then
        frmLstPROVEMatPorProve.optTodos.Value = True
    Else
        If Not sdbcProveCod.Text = "" And Not sdbcProveCod.Text = "" Then
            frmLstPROVEMatPorProve.optProve = True
            frmLstPROVEMatPorProve.RespetarComboProve = True
            frmLstPROVEMatPorProve.sdbcProveCod.Text = sdbcProveCod.Text
            frmLstPROVEMatPorProve.sdbcProveDen.Text = sdbcProveDen.Text
        End If
    End If
    
    frmLstPROVEMatPorProve.Show vbModal
End Sub

Private Sub cmdModificar_Click()

Dim oIBaseDatos As IBaseDatos
        
    'Comprobamos que otro usuario no haya eliminado el proveedor
    
    Set oIBaseDatos = oProveSeleccionado
    
    If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
        
        oMensajes.DatoEliminado sIdioma(1)
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        sdbcProveCod.Text = ""
        sdbcProveDen.Text = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen.RemoveAll
        tvwEstrMat.Nodes.clear
        Set oProveSeleccionado = Nothing
        
        Exit Sub
    End If

    LockWindowUpdate Me.hWnd
    
    fraSelComp.Enabled = False
    picNavigate.Visible = False
    picEdit.Visible = True
    
    If SSTabEstrMat.Tab = 0 Then
        
        SSTabEstrMat.TabVisible(1) = False
        
        'Vamos a modificar la asignaci�n de material
        
        tvwEstrMat.Visible = False
        tvwEstrMatMod.Visible = True
        
        Screen.MousePointer = vbHourglass
        tvwEstrMatMod.Scroll = False
        GenerarEstructuraMatAsignable
        If tvwEstrMatMod.Nodes.Count > 0 Then
            tvwEstrMatMod.Nodes("Raiz").Root.Selected = True
            tvwEstrMatMod.Scroll = True
        End If
        Screen.MousePointer = vbNormal
        
        Set oGruposMN1DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
        Set oGruposMN2DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
        Set oGruposMN3DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
        Set oGruposMN4DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
        
        Set oGruposMN1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
        Set oGruposMN2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
        Set oGruposMN3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
        Set oGruposMN4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
        
    Else
        'Vamos a modificar la homologaci�n de art�culos
        
        SSTabEstrMat.TabEnabled(0) = False
        sdbgArticulos.AllowUpdate = True
        
    End If
    
    LockWindowUpdate 0&
End Sub

Private Sub cmdRestaurar_Click()

    Screen.MousePointer = vbHourglass
    
    If SSTabEstrMat.Tab = 0 Then
        LockWindowUpdate Me.hWnd
        CargarEstructuraMateriales
        LockWindowUpdate 0&
    Else
        SSTabEstrMat_Click 0
    End If
    
    Screen.MousePointer = vbNormal
End Sub



Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVE_MATPORPROVE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 2)
        sIdioma(1) = Ador(0).Value '1
        Label1.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sIdioma(2) = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        frmPROVEMatPorProve.caption = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns(0).caption = Ador(0).Value '9 C�digo
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        sdbgArticulos.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns(1).caption = Ador(0).Value '10 Denominaci�n
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        sdbgArticulos.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        SSTabEstrMat.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTabEstrMat.TabCaption(1) = Ador(0).Value
#If VERSION >= 31000 Then
        Ador.MoveNext
        sNotificarAsigMatQASubject = Ador(0).Value
#End If
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub


Private Sub Form_Load()

iAltura = 5790
iAnchura = 5820

Me.Height = 5790
Me.Width = 5820
If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If

CargarRecursos

PonerFieldSeparator Me

SSTabEstrMat.TabVisible(1) = False

cmdModificar.Enabled = False
cmdRestaurar.Enabled = False

ConfigurarSeguridad

Set oProves = oFSGSRaiz.generar_CProveedores

        
End Sub

Private Sub Form_Resize()
    
    
    If Me.Height < 2500 Then Exit Sub
    If Me.Width < 1000 Then Exit Sub
    
    SSTabEstrMat.Height = Me.Height - 1700
    SSTabEstrMat.Width = Me.Width - 200
        
    tvwEstrMat.Width = SSTabEstrMat.Width - 200
    tvwEstrMat.Height = SSTabEstrMat.Height - 500
    tvwEstrMatMod.Width = tvwEstrMat.Width
    tvwEstrMatMod.Height = tvwEstrMat.Height
    Picture1.Width = tvwEstrMat.Width
    Picture1.Height = tvwEstrMat.Height
    sdbgArticulos.Height = Picture1.Height - 100
    sdbgArticulos.Width = Picture1.Width - 100
    sdbgArticulos.Columns("COD").Width = sdbgArticulos.Width * 0.25
    sdbgArticulos.Columns("DEN").Width = sdbgArticulos.Width * 0.55
    sdbgArticulos.Columns("HOM").Width = sdbgArticulos.Width * 0.2 - 560
    cmdAceptar.Left = (tvwEstrMat.Width / 2) - (cmdAceptar.Width / 2) - 300
    cmdCancelar.Left = (tvwEstrMat.Width / 2) + 300


End Sub

Private Sub Form_Unload(Cancel As Integer)

'Unload frmSELMaterial

Set oGruposMN1 = Nothing
Set oGruposMN2 = Nothing
Set oGruposMN3 = Nothing
Set oGruposMN4 = Nothing
'Set oEquipoSeleccionado = Nothing
Set oProves = Nothing
Set oProveSeleccionado = Nothing
Me.Visible = False

End Sub




Private Sub GenerarEstructuraDeMateriales()

Dim oGMN1 As CGrupoMatNivel1
Dim nodx As MSComctlLib.node
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oIMaterialAsignado As IMaterialAsignado

If oProveSeleccionado Is Nothing Then Exit Sub

tvwEstrMat.Nodes.clear
Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(2), "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True

On Error GoTo Error

Set oIMaterialAsignado = oProveSeleccionado

With oIMaterialAsignado


'************** GruposMN1 ***********
Set oGruposMN1 = .DevolverGruposMN1Asignados(, , , False, False, True)
    
        For Each oGMN1 In oGruposMN1
    
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
            If oGMN1.GruposMatNivel2 Is Nothing Then
                nodx.Image = "GMN1A"
            End If
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod) ' Con la X1X indicamos que el GMN1 ha sido asignado.
            
        Next
        
    If gParametrosGenerales.giNEM <= 1 Then
        Exit Sub
    End If
    
    
    '************** GruposMN2 ***********
    Set oGruposMN2 = .DevolverGruposMN2Asignados(, , , False, False, True)
    
        For Each oGMN2 In oGruposMN2
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
            Set nodx = tvwEstrMat.Nodes("GMN1" & scod1)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN2.GMN1Cod) & " - " & oGMN2.GMN1Den, "GMN1")
            End If
            nodx.Tag = "GMN1" & CStr(oGMN2.GMN1Cod)
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
            
            If oGMN2.GruposMatNivel3 Is Nothing Then
                nodx.Image = "GMN2A"
            End If
            nodx.Tag = "GMN2" & CStr(oGMN2.Cod) ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
            If nodx.Parent.Image = "GMN1" Then
                nodx.Parent.Expanded = True
            End If
        Next
    
    
    If gParametrosGenerales.giNEM <= 2 Then
        Exit Sub
    End If
    
    
    '************** GruposMN3 ***********
    Set oGruposMN3 = oIMaterialAsignado.DevolverGruposMN3Asignados(, , , False, False, True)
    
    If Not oGruposMN3 Is Nothing Then
        For Each oGMN3 In oGruposMN3
            scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
            scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
            Set nodx = tvwEstrMat.Nodes("GMN1" & scod1)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN3.GMN1Cod) & " - " & oGMN3.GMN1Den, "GMN1")
            End If
            nodx.Tag = "GMN1" & CStr(oGMN3.GMN1Cod)  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            
            Set nodx = tvwEstrMat.Nodes("GMN2" & scod2)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN3.GMN2Cod) & " - " & oGMN3.GMN2Den, "GMN2")
            End If
            nodx.Tag = "GMN2" & CStr(oGMN3.GMN2Cod)   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            
            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
            If oGMN3.GruposMatNivel4 Is Nothing Then
                nodx.Image = "GMN3A"
            End If
            nodx.Tag = "GMN3" & CStr(oGMN3.Cod) ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
            If nodx.Parent.Image = "GMN2" Then
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        Next
    
    End If
    
    If gParametrosGenerales.giNEM <= 3 Then
        Exit Sub
    End If
    
    '************** GruposMN4 ***********
    Set oGruposMN4 = oIMaterialAsignado.DevolverGruposMN4Asignados(, , , False, False, True)
    
    If Not oGruposMN4 Is Nothing Then
        
        For Each oGMN4 In oGruposMN4

            scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
            scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
            scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
            scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
            Set nodx = Nothing
            Set nodx = tvwEstrMat.Nodes("GMN1" & scod1)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN4.GMN1Cod) & " - " & oGMN4.GMN1Den, "GMN1")
            End If
            nodx.Tag = "GMN1" & CStr(oGMN4.GMN1Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            
            Set nodx = tvwEstrMat.Nodes("GMN2" & scod2)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN4.GMN2Cod) & " - " & oGMN4.GMN2Den, "GMN2")
            End If
            nodx.Tag = "GMN2" & CStr(oGMN4.GMN2Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            
            Set nodx = tvwEstrMat.Nodes("GMN3" & scod3)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN4.GMN3Cod) & " - " & oGMN4.GMN3Den, "GMN3")
            End If
            nodx.Tag = "GMN3" & CStr(oGMN4.GMN3Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            
            Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4A")
            nodx.Tag = "GMN4" & CStr(oGMN4.Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            
            If (nodx.Parent.Image) = "GMN3" Then
                nodx.Parent.Parent.Parent.Expanded = True
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        
        Next
    
    End If
 
 End With
 
Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
End Sub

Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> Administrador Then

    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVEModificar)) Is Nothing) Then
        bModif = True
        cmdModificar.Visible = True
    Else
        cmdModificar.Visible = False
        cmdListado.Left = cmdRestaurar.Left
        cmdRestaurar.Left = cmdModificar.Left
    End If
    
    If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVERestEquipo)) Is Nothing) And basParametros.gParametrosGenerales.gbOblProveEqp Then
        bREqp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVERestMatComp)) Is Nothing) Then
        bRMatAsig = True
    End If
Else
    
    bModif = True
    cmdModificar.Visible = True
End If
cmdListado.Visible = True
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

Select Case Left(node.Tag, 4)

Case "GMN1"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "GMN2"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "GMN3"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
   
Case "GMN4"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
   
End Select

End Function
Private Sub ConfigurarInterfazSeguridad(ByVal nodx As MSComctlLib.node)
    
    If nodx Is Nothing Then Exit Sub
    
    Select Case Left(nodx.Tag, 4)
        
        Case "GMN1"
                    If basParametros.gParametrosGenerales.giNEM = 1 Then
                        SSTabEstrMat.TabVisible(1) = True
                    Else
                        SSTabEstrMat.TabVisible(1) = False
                    End If
        Case "GMN2"
                    If basParametros.gParametrosGenerales.giNEM = 2 Then
                        SSTabEstrMat.TabVisible(1) = True
                    Else
                        SSTabEstrMat.TabVisible(1) = False
                    End If
        Case "GMN3"
                    If basParametros.gParametrosGenerales.giNEM = 3 Then
                        SSTabEstrMat.TabVisible(1) = True
                    Else
                        SSTabEstrMat.TabVisible(1) = False
                    End If
        Case "GMN4"
                    If basParametros.gParametrosGenerales.giNEM = 4 Then
                        SSTabEstrMat.TabVisible(1) = True
                    Else
                        SSTabEstrMat.TabVisible(1) = False
                    End If
        Case Else
                    SSTabEstrMat.TabVisible(1) = False
    End Select
    
    
End Sub



Private Sub sdbcProveCod_Change()
    
    If Not RespetarComboProve Then
    
        RespetarComboProve = True
        sdbcProveDen.Text = ""
        tvwEstrMat.Nodes.clear
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        RespetarComboProve = False
               
    End If
    
End Sub

Private Sub sdbcProveCod_Click()
     
     If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveCod.Rows - 1
            bm = sdbcProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIMAsig As IMaterialAsignado
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el proveedor
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
    
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod.Text), , , , , Trim(oGruposMN1.Item(1).Cod) ', , ,, , , , OrdPorDen, OrdPorCalif1, OrdPorCalif2, OrdPorcalif3, False
    Else
        If bREqp Then
            oProves.BuscarProveedoresConEqpDelCompDesde 1, basOptimizacion.gCodEqpUsuario, sdbcProveCod, , True, , , , , , , , , , , False
        Else
            oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
        End If
    End If
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Text
        
        RespetarComboProve = False
        ProveedorSeleccionado
    End If

End Sub

Private Sub sdbcProveDen_Change()
    
    If Not RespetarComboProve Then
    
        RespetarComboProve = True
        sdbcProveCod.Text = ""
        tvwEstrMat.Nodes.clear
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        RespetarComboProve = False
        
    End If
    
End Sub

Private Sub sdbcProveDen_Click()
    
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcProveDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProveDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el proveedor

    If bREqp Then
        oProves.BuscarProveedoresConEqpDelCompDesde 1, basOptimizacion.gCodEqpUsuario, , sdbcProveDen, True, , , , , , , , , , , False
    Else
        oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProveDen.Text), True, , False
    End If

    bExiste = Not (oProves.Count = 0)
    
    If Not bExiste Then
        sdbcProveDen.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        
        sdbcProveDen.Columns(0).Value = sdbcProveDen.Text
        sdbcProveDen.Columns(1).Value = sdbcProveCod.Text
        
        RespetarComboProve = False
        ProveedorSeleccionado
    End If

End Sub

Private Sub sdbgArticulos_Change()
    
    sdbgArticulos.Update
    
End Sub

Private Sub sdbgArticulos_HeadClick(ByVal ColIndex As Integer)
Dim oArt As CArticulo

    sdbgArticulos.RemoveAll
    
    Select Case Left(tvwEstrMat.selectedItem.Tag, 4)
            
            Case "GMN1"
            
            Case "GMN2"
            
            Case "GMN3"
            
            Case "GMN4"
                    
                    
                    Set oArticulos = oIArtHom.DevolverArticulosDelProveedor(sdbcProveCod, DevolverCod(tvwEstrMat.selectedItem.Parent.Parent.Parent), DevolverCod(tvwEstrMat.selectedItem.Parent.Parent), DevolverCod(tvwEstrMat.selectedItem.Parent), DevolverCod(tvwEstrMat.selectedItem), (ColIndex = 1), (ColIndex = 2))
        End Select
        
        For Each oArt In oArticulos
            
            sdbgArticulos.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den & Chr(m_lSeparador) & oArt.Homologado
        
        Next
        
End Sub

Private Sub SSTabEstrMat_Click(PreviousTab As Integer)
Dim oArt As CArticulo

    If SSTabEstrMat.Tab = 1 Then
        If oProveSeleccionado Is Nothing Then
            SSTabEstrMat.Tab = 0
            If Me.Visible Then tvwEstrMat.SetFocus
            Exit Sub
        End If
        
        sdbgArticulos.RemoveAll
        
        Set oIArtHom = oProveSeleccionado
        
        Select Case Left(tvwEstrMat.selectedItem.Tag, 4)
            
            Case "GMN1"
            
            Case "GMN2"
            
            Case "GMN3"
            
            Case "GMN4"
                    
                    Screen.MousePointer = vbHourglass
                    Set oArticulos = oIArtHom.DevolverArticulosDelProveedor(sdbcProveCod, DevolverCod(tvwEstrMat.selectedItem.Parent.Parent.Parent), DevolverCod(tvwEstrMat.selectedItem.Parent.Parent), DevolverCod(tvwEstrMat.selectedItem.Parent), DevolverCod(tvwEstrMat.selectedItem))
                    Screen.MousePointer = vbNormal
                    
                    For Each oArt In oArticulos
            
                        sdbgArticulos.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den & Chr(m_lSeparador) & oArt.Homologado
        
                    Next
                    If bModif Then
                        cmdModificar.Enabled = True
                    Else
                        cmdModificar.Enabled = False
                    End If

        End Select
        
        
    Else
        If Me.Visible Then tvwEstrMat.SetFocus
        If oProveSeleccionado.EsPremium And gParametrosGenerales.gbPremium Then
            cmdModificar.Enabled = False
        Else
            If bModif Then
                cmdModificar.Enabled = True
            Else
                cmdModificar.Enabled = False
            End If
        End If
        
    End If
End Sub

Private Sub tvwEstrMat_Collapse(ByVal node As MSComctlLib.node)
     ConfigurarInterfazSeguridad node
End Sub

Private Sub tvwEstrMat_GotFocus()
   
   ConfigurarInterfazSeguridad tvwEstrMat.selectedItem
   
End Sub

Private Sub tvwEstrMat_NodeClick(ByVal node As MSComctlLib.node)
    
    ConfigurarInterfazSeguridad node
    
End Sub

Private Sub GenerarEstructuraMatAsignable()
Dim oIMaterialProve As IMaterialAsignado
Dim oGrupsMN1 As CGruposMatNivel1
Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

On Error GoTo Error

tvwEstrMatMod.Nodes.clear

'Set oGrupsMN1 = oIMaterialAsignado.DevolverEstrMatAsignable

    Set nodx = tvwEstrMatMod.Nodes.Add(, , "Raiz", sIdioma(2), "Raiz")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
If Not bRMatAsig Then

    Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
   
    oGrupsMN1.GenerarEstructuraMateriales False

    
    Select Case gParametrosGenerales.giNEM
    
    Case 1
            For Each oGMN1 In oGrupsMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
                nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            Next
            
    Case 2
        
        For Each oGMN1 In oGrupsMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
            Next
        Next
            
            
    Case 3
        
        For Each oGMN1 In oGrupsMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
                For Each oGMN3 In oGMN2.GruposMatNivel3
                    scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                    Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
                    nodx.Tag = "GMN3" & CStr(oGMN3.Cod)
                Next
            Next
        Next
        
        
    Case 4
        For Each oGMN1 In oGrupsMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            
            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
                    
                    For Each oGMN3 In oGMN2.GruposMatNivel3
                        
                        scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                        Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
                        nodx.Tag = "GMN3" & CStr(oGMN3.Cod)
                                
                                For Each oGMN4 In oGMN3.GruposMatNivel4
                                    scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                    Set nodx = tvwEstrMatMod.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4")
                                    nodx.Tag = "GMN4" & CStr(oGMN4.Cod)
                                Next
                    Next
            Next
        Next
        
        
    End Select
        
        Set oGrupsMN1 = Nothing
    
Else
    ' Carga del material asignado al Prove
    
    Set oIMaterialProve = oUsuarioSummit.comprador
   
    With oIMaterialProve


        '************** GruposMN1 ***********
        Set oGrupsMN1Comp = .DevolverGruposMN1Asignados(, , , , False)
            
        For Each oGMN1 In oGrupsMN1Comp
    
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod) ' Con la X1X indicamos que el GMN1 ha sido asignado.
            
        Next
        
        If gParametrosGenerales.giNEM <= 1 Then
            Exit Sub
        End If
    
    
        '************** GruposMN2 ***********
        Set oGrupsMN2Comp = .DevolverGruposMN2Asignados(, , , , False)
    
        For Each oGMN2 In oGrupsMN2Comp
            
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN2.GMN1Cod) & " - " & oGMN2.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN2.GMN1Cod)
            Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
            nodx.Tag = "GMN2" & CStr(oGMN2.Cod) ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
        
        Next
    
    
        If gParametrosGenerales.giNEM <= 2 Then
            Exit Sub
        End If
        
    
        '************** GruposMN3 ***********
        Set oGrupsMN3Comp = oIMaterialProve.DevolverGruposMN3Asignados(, , , , False)
        
        If Not oGrupsMN3Comp Is Nothing Then
            
            For Each oGMN3 In oGrupsMN3Comp
                
                scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
                scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN3.GMN1Cod) & " - " & oGMN3.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & CStr(oGMN3.GMN1Cod)  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN3.GMN2Cod) & " - " & oGMN3.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN3.GMN2Cod)   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
                nodx.Tag = "GMN3" & CStr(oGMN3.Cod) ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
                
            Next
    
        End If
    
        If gParametrosGenerales.giNEM <= 3 Then
            Exit Sub
        End If
    
        '************** GruposMN4 ***********
        Set oGrupsMN4Comp = oIMaterialProve.DevolverGruposMN4Asignados(, , , , False)
        
        If Not oGrupsMN4Comp Is Nothing Then
            
            For Each oGMN4 In oGrupsMN4Comp
    
                scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN4.GMN1Cod) & " - " & oGMN4.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & CStr(oGMN4.GMN1Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN4.GMN2Cod) & " - " & oGMN4.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN4.GMN2Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN4.GMN3Cod) & " - " & oGMN4.GMN3Den, "GMN3")
                nodx.Tag = "GMN3" & CStr(oGMN4.GMN3Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4")
                nodx.Tag = "GMN4" & CStr(oGMN4.Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                
                
            Next
        
        End If
     
     End With
     

End If

Set oGrupsMN1 = Nothing
    
' Ahora de esta estructura debemos marcar los materiales que ya estan seleccionados
For Each nodx In tvwEstrMat.Nodes
    If Right(nodx.Image, 1) = "A" Then
        tvwEstrMatMod.Nodes(nodx.key).Checked = True
        If nodx.Visible Then
            tvwEstrMatMod.Nodes(nodx.key).EnsureVisible
        End If
        DoEvents
    End If
Next


Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
    
End Sub

Private Sub tvwEstrMatMod_KeyUp(KeyCode As Integer, Shift As Integer)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String

If bMouseDown Then
    
    bMouseDown = False
    
    If SelNode.Tag = "Raiz" Then
        SelNode.Checked = Not SelNode.Checked
        DoEvents
        Exit Sub
    End If
    
    If bRMatAsig Then
        Select Case Left(SelNode.Tag, 4)
        
            Case "GMN1"
                
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                
            Case "GMN2"
                scod1 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent)))
                If oGrupsMN1Comp.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                End If
                
            Case "GMN3"
                
                scod1 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent)))
                scod2 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent)))
                
                If oGrupsMN2Comp.Item(scod1 & scod2) Is Nothing _
                And oGrupsMN1Comp.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                End If
                
            Case "GMN4"
                
                scod1 = DevolverCod(SelNode.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent.Parent)))
                scod2 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent.Parent)))
                scod3 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode.Parent)))
                
                If oGrupsMN3Comp.Item(scod1 & scod2 & scod3) Is Nothing _
                And oGrupsMN2Comp.Item(scod1 & scod2) Is Nothing _
                And oGrupsMN1Comp.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                End If
        
        End Select
        
    End If
    
    If SelNode.Checked Then
            
            MarcarTodosLosHijos SelNode
            
    Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
        
    End If

End If

End Sub

Private Sub tvwEstrMatMod_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.node
Dim tvhti As TVHITTESTINFO

                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrMatMod.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrMatMod.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento
                             
                bMouseDown = True
                Set SelNode = nod
                             
                End If
            End If
        End If
    End If
    

End Sub

Private Sub tvwEstrMatMod_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String

If bMouseDown Then
    
    bMouseDown = False
    
    If SelNode.Tag = "Raiz" Then
        SelNode.Checked = Not SelNode.Checked
        DoEvents
        Exit Sub
    End If
    
    If bRMatAsig Then
        Select Case Left(SelNode.Tag, 4)
        
            Case "GMN1"
                
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                
            Case "GMN2"
                scod1 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent)))
                If oGrupsMN1Comp.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                End If
                
            Case "GMN3"
                
                scod1 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent)))
                scod2 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent)))
                
                If oGrupsMN2Comp.Item(scod1 & scod2) Is Nothing _
                And oGrupsMN1Comp.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                End If
                
            Case "GMN4"
                
                scod1 = DevolverCod(SelNode.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent.Parent)))
                scod2 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent.Parent)))
                scod3 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode.Parent)))
                
                If oGrupsMN3Comp.Item(scod1 & scod2 & scod3) Is Nothing _
                And oGrupsMN2Comp.Item(scod1 & scod2) Is Nothing _
                And oGrupsMN1Comp.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                End If
        
        End Select
        
    End If
    
    If SelNode.Checked Then
            
            MarcarTodosLosHijos SelNode
            
    Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
        
    End If

End If

End Sub

Private Sub tvwEstrMatMod_NodeCheck(ByVal node As MSComctlLib.node)
If bMouseDown Then
    Exit Sub
End If

bMouseDown = True
Set SelNode = node

End Sub
Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.node)

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = True
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = True
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = True
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = True
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub
Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.node)

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = False
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = False
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = False
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = False
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = False
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub

Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.node)
    
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        nodx.Checked = False
        Set nodx = nodx.Parent
    Wend
    DoEvents
End Sub

Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    ProveedorSeleccionado
    
End Sub

Private Sub sdbcProveCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Long
    Dim oIMAsig As IMaterialAsignado
    
    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll
    
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)

        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , Trim(oGruposMN1.Item(1).Cod) ', , ,, , , , OrdPorDen, OrdPorCalif1, OrdPorCalif2, OrdPorcalif3, False
    Else
        If bREqp Then
            oProves.BuscarProveedoresConEqpDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, sdbcProveCod, sdbcProveDen, , , , , , , , , , , , False
        Else
            oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProveCod.Text), , , , False
        End If
    End If
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    'If Not oProves.EOF Then
    If oProves.Count = gParametrosInstalacion.giCargaMaximaCombos Then
        sdbcProveCod.AddItem "..."
    End If

    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub



Private Sub sdbcProveDen_CloseUp()

    
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    ProveedorSeleccionado
    
End Sub

Private Sub sdbcProveDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Long
    Dim oIMAsig As IMaterialAsignado
    
    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)

        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , Trim(oGruposMN1.Item(1).Cod)
    Else
        If bREqp Then
            oProves.BuscarProveedoresConEqpDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, , sdbcProveDen, , , , , , , , True, , , False
        Else
            oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveDen, , True, False
        End If
    End If
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    'If Not oProves.EOF Then
    If oProves.Count = gParametrosInstalacion.giCargaMaximaCombos Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub ProveedorSeleccionado()
    
    oProves.CargarDatosProveedor (sdbcProveCod.Text)
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
    
    If Not oProveSeleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        LockWindowUpdate Me.hWnd
        CargarEstructuraMateriales
        If oProveSeleccionado.EsPremium And gParametrosGenerales.gbPremium Then
            cmdModificar.Enabled = False
        Else
            If bModif Then
                cmdModificar.Enabled = True
            Else
                cmdModificar.Enabled = False
            End If
        End If
        cmdRestaurar.Enabled = True
        LockWindowUpdate 0&
    Else
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
    End If
    
    Screen.MousePointer = vbNormal
End Sub
Public Sub CargarProveedorConBusqueda()

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
   
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveCod_Validate False
'    ProveedorSeleccionado
    
End Sub

Private Function ComprobarAtributosEnArticulos() As Boolean
Dim bExisten As Boolean
Dim irespuesta As Integer

      bExisten = oIMaterial.ExistenValoresDeAtributos
      If bExisten Then
        irespuesta = oMensajes.PreguntaDesasignarMaterialProve(0)
        If irespuesta = vbYes Then
            ComprobarAtributosEnArticulos = True
        Else
            ComprobarAtributosEnArticulos = False
        End If
      Else
        ComprobarAtributosEnArticulos = True
      End If
      
     
End Function
