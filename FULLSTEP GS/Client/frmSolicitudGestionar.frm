VERSION 5.00
Begin VB.Form frmSolicitudGestionar 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DAccion"
   ClientHeight    =   3180
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5745
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudGestionar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3180
   ScaleWidth      =   5745
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   1630
      TabIndex        =   2
      Top             =   2760
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2950
      TabIndex        =   1
      Top             =   2760
      Width           =   1095
   End
   Begin VB.TextBox txtMotivo 
      Height          =   2200
      Left            =   120
      MaxLength       =   500
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   400
      Width           =   5535
   End
   Begin VB.Label lblMotivo 
      BackColor       =   &H00808000&
      Caption         =   "DIndique el motivo de ...:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   5535
   End
End
Attribute VB_Name = "frmSolicitudGestionar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oSolicitud As CInstancia
Public g_iAccion As Integer
Public g_sOrigen As String
Public g_sOrigen2 As String
Public g_bSoloConsulta As Boolean

'Variables para los idiomas:
Private m_sMotivoAnular As String
Private m_sMotivoCerrar As String
Private m_sMotivoReabrir As String
Private m_sMotivoRechazar As String
Private m_sCaptionAnular As String
Private m_sCaptionCerrar As String
Private m_sCaptionReabrir As String
Private m_sCaptionRechazar As String
Private m_sComentario As String
Private m_sCaptionMonitorizar As String
Private m_sMotivoMonitorizar As String

Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String

Private Sub cmdAceptar_Click()
''' <summary>
''' Evento del bot�n de Aceptar. Dependiendo de la acci�n a realizar llama al procedimiento correspondiente.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Tiempo m�ximo:0,1</remarks>

Dim teserror As TipoErrorSummit

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_sOrigen = "frmSolicitudComentarios" Then
        Screen.MousePointer = vbHourglass
        
        teserror = g_oSolicitud.GuardarComentario(frmSolicitudComentarios.sdbgComentarios2.Columns("ID").Value, StrToNull(txtMotivo.Text))
        If Trim(txtMotivo.Text) = "" Then
            frmSolicitudComentarios.sdbgComentarios2.Columns("HAY_COMENT").Value = "0"
        Else
            frmSolicitudComentarios.sdbgComentarios2.Columns("HAY_COMENT").Value = "1"
        End If

        Screen.MousePointer = vbNormal
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Exit Sub
        Else
            Unload Me
        End If
        
    Else
        cmdAceptar.Enabled = False
        'Realiza la acci�n correspondiente seg�n desde donde se le haya llamado
        Select Case g_iAccion
            Case 1:
                Anular
            Case 2:
                Cerrar
            Case 3:
                Reabrir
            Case 4:
                Rechazar
            Case 5:
                Monitorizar
        End Select
        
        cmdAceptar.Enabled = True
        Screen.MousePointer = vbNormal
        Unload Me
        
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If


End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not m_bActivado Then
        m_bActivado = True
   End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Load()
''' <summary>
''' Load del formulario: carga textos y visibilidad
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Tiempo m�ximo:0,1</remarks>

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    m_bActivado = False
    cmdAceptar.Enabled = True
    
    CargarRecursos
    
    If g_sOrigen = "frmSolicitudComentarios" Then
        'Va a modificar el comentario de el hist�rico de estados:
        lblMotivo.Visible = False
        txtMotivo.Top = Me.lblMotivo.Top
        txtMotivo.Height = 2500
        Me.caption = m_sComentario

        txtMotivo.Text = g_oSolicitud.DevolverComentario(frmSolicitudComentarios.sdbgComentarios2.Columns("ID").Value)

        If g_bSoloConsulta = True Then
            Me.cmdAceptar.Visible = False
            Me.cmdCancelar.Visible = False
            Me.txtMotivo.Height = txtMotivo.Height + cmdAceptar.Height
            Me.txtMotivo.Locked = True
        End If
        
    Else
        Select Case g_iAccion
            Case 1:
                'Anular
                Me.caption = m_sCaptionAnular
                lblMotivo.caption = m_sMotivoAnular
                
            Case 2:
                'Cerrar
                Me.caption = m_sCaptionCerrar
                lblMotivo.caption = m_sMotivoCerrar
                
            Case 3:
                'Reabrir
                Me.caption = m_sCaptionReabrir
                lblMotivo.caption = m_sMotivoReabrir
                
            Case 4:
                'Rechazar
                Me.caption = m_sCaptionRechazar
                lblMotivo.caption = m_sMotivoRechazar
            Case 5:
                'Activar monitorizaci�n
                Me.caption = m_sCaptionMonitorizar
                lblMotivo.caption = m_sMotivoMonitorizar
        End Select
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarRecursos()
''' <summary>
''' Carga los textos de BD.
''' </summary>
''' <returns>Nada</returns>
''' <remarks> Form_load(); Tiempo m�ximo:0,1</remarks>

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOLIC_GESTIONAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sCaptionRechazar = Ador(0).Value       'Rechazar
        Ador.MoveNext
        m_sMotivoRechazar = Ador(0).Value 'Indique el motivo del rechazo
        Ador.MoveNext
        
        cmdAceptar.caption = Ador(0).Value  'Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value  'Cancelar
        Ador.MoveNext
        
        m_sCaptionAnular = Ador(0).Value       'anular
        Ador.MoveNext
        m_sMotivoAnular = Ador(0).Value 'Indique el motivo de la anulaci�n
        Ador.MoveNext
        
        m_sCaptionCerrar = Ador(0).Value       'Cerrar
        Ador.MoveNext
        m_sMotivoCerrar = Ador(0).Value 'Indique el motivo del cierre
        Ador.MoveNext
        
        m_sCaptionReabrir = Ador(0).Value       'Reabrir
        Ador.MoveNext
        m_sMotivoReabrir = Ador(0).Value 'Indique el motivo de la reapertura
        
        Ador.MoveNext
        m_sComentario = Ador(0).Value
        
        Ador.MoveNext
        m_sCaptionMonitorizar = Ador(0).Value  '12 Monitorizaci�n de solicitudes
        Ador.MoveNext
        m_sMotivoMonitorizar = Ador(0).Value   '13 Puede usted introducir un comentario si as� lo desea

        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub
''' <summary>
''' Anular solicitud
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub Anular()
    Dim teserror As TipoErrorSummit
    Dim oEmails As CEmailSolicitudes
    Dim arMensaje As Variant
    Dim iRes As Integer
    Dim bSesionIniciada  As Boolean
    
    'Anula la solicitud que est� seleccionada en la grid
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    'Env�a el email al peticionario
     Set oEmails = oFSGSRaiz.Generar_CEmailSolicitudes
    arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 3)
    
    If arMensaje(0) = "" Then
    ElseIf arMensaje(2) = "1010" Then 'No encontr� la ruta o plantilla
        iRes = oMensajes.PreguntaRutaSolicitudesNoEncontrada
        If iRes = vbNo Then
            Set oEmails = Nothing
            Screen.MousePointer = vbNormal
            cmdAceptar.Enabled = True
            Unload Me
            Exit Sub
        Else
            arMensaje(2) = ""
        End If
    End If
        
    teserror = g_oSolicitud.AnularSolicitud(basOptimizacion.gCodPersonaUsuario, txtMotivo.Text)
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
    Else
        arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 3)
        
        If arMensaje(2) = "1010" Then 'No encontr� la ruta o plantilla
            arMensaje(2) = ""
        End If
    
        If Not bSesionIniciada Or oIdsMail Is Nothing Then
            Set oIdsMail = IniciarSesionMail
            bSesionIniciada = True
        End If
        'doevents
        teserror = ComponerMensaje(arMensaje(0), arMensaje(1), arMensaje(2), , , arMensaje(4), , , arMensaje(5), _
                                    lIdInstancia:=g_oSolicitud.Id, _
                                    entidadNotificacion:=entidadNotificacion.Solicitud, _
                                    tipoNotificacion:=AnulacionSolicitudPet, _
                                    sToName:=g_oSolicitud.Peticionario.nombre & " " & g_oSolicitud.Peticionario.Apellidos)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        End If
    
        If bSesionIniciada Then
            FinalizarSesionMail
        End If
        
        Set oEmails = Nothing

        'Actualiza la grid y la colecci�n del formulario de solicitudes
        Select Case g_sOrigen2
            Case "frmSolicitudes"   's�lo si el formulario origen es el de solicitudes
                frmSolicitudes.ActualizarEstadoEnGrid g_oSolicitud.Id, EstadoSolicitud.Anulada
                If g_sOrigen = "frmSolicitudDetalle" Then
                    frmSolicitudes.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Anulada
                End If
                
            Case "frmPROCE"
                frmPROCE.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Anulada
                
            Case "frmCatalogo"
                frmCatalogo.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Anulada
                
            Case "frmPedidos"
                frmPEDIDOS.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Anulada
                
            Case "frmSeguimiento"
                frmSeguimiento.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Anulada
        End Select
        
        
        ''' Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCSolicAnular, "Id:" & g_oSolicitud.Id

    End If

    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
    If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "Anular", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
''' <summary>
''' Cerrar solicitud
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub Cerrar()
    Dim teserror As TipoErrorSummit
    Dim oEmails As CEmailSolicitudes
    Dim arMensaje As Variant
    Dim iRes As Integer
    Dim bSesionIniciada  As Boolean

    'Anula la solicitud que est� seleccionada en la grid

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    'Crea el email para al peticionario, se hace primero por si no hay plantilla de un mensaje antes de cerrar
    Set oEmails = oFSGSRaiz.Generar_CEmailSolicitudes
    arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 4)

    If arMensaje(0) = "" Then
    ElseIf arMensaje(2) = "1010" Then 'No encontr� la ruta o plantilla
        iRes = oMensajes.PreguntaRutaSolicitudesNoEncontrada
        If iRes = vbNo Then
            Set oEmails = Nothing
            Screen.MousePointer = vbNormal
            cmdAceptar.Enabled = True
            Unload Me
            Exit Sub
        Else
            arMensaje(2) = ""
        End If
    End If
        
    teserror = g_oSolicitud.Cerrar(basOptimizacion.gCodPersonaUsuario, txtMotivo.Text)
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
    Else
        arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 4)
        If arMensaje(2) = "1010" Then
            arMensaje(2) = ""
        End If
        
        If Not bSesionIniciada Or oIdsMail Is Nothing Then
            Set oIdsMail = IniciarSesionMail
            bSesionIniciada = True
        End If
        'doevents
        teserror = ComponerMensaje(arMensaje(0), arMensaje(1), arMensaje(2), , , arMensaje(4), , , arMensaje(5), _
                                            lIdInstancia:=g_oSolicitud.Id, _
                                            entidadNotificacion:=entidadNotificacion.Solicitud, _
                                            tipoNotificacion:=CierreSolicitudPet, _
                                            sToName:=g_oSolicitud.Peticionario.nombre & " " & g_oSolicitud.Peticionario.Apellidos)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        End If
    
        If bSesionIniciada Then
            FinalizarSesionMail
        End If
        
        Set oEmails = Nothing

        'Actualiza la grid y la colecci�n del formulario de solicitudes
        Select Case g_sOrigen2
            Case "frmSolicitudes"   's�lo si el formulario origen es el de solicitudes
                frmSolicitudes.ActualizarEstadoEnGrid g_oSolicitud.Id, EstadoSolicitud.Cerrada
                If g_sOrigen = "frmSolicitudDetalle" Then
                    frmSolicitudes.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Cerrada
                End If
                
            Case "frmPROCE"
                frmPROCE.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Cerrada
                
            Case "frmCatalogo"
                frmCatalogo.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Cerrada
                
            Case "frmPedidos"
                frmPEDIDOS.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Cerrada
                
            Case "frmSeguimiento"
                frmSeguimiento.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Cerrada
        End Select
    
        
        ''' Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCSolicCerrar, "Id:" & g_oSolicitud.Id
        
    End If
    

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "Cerrar", err, Erl, , m_bActivado)
      Exit Sub
   End If


End Sub
''' <summary>
''' Reabrir solicitud
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub Reabrir()
    Dim teserror As TipoErrorSummit
    Dim oEmails As CEmailSolicitudes
    Dim arMensaje As Variant
    Dim iRes As Integer
    Dim bSesionIniciada  As Boolean
    
    'Anula la solicitud que est� seleccionada en la grid
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    'Env�a el email al peticionario
     Set oEmails = oFSGSRaiz.Generar_CEmailSolicitudes
    arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 5)
    
    If arMensaje(0) = "" Then
    ElseIf arMensaje(2) = "1010" Then 'No encontr� la ruta o plantilla
        iRes = oMensajes.PreguntaRutaSolicitudesNoEncontrada
        If iRes = vbNo Then
            Set oEmails = Nothing
            Screen.MousePointer = vbNormal
            cmdAceptar.Enabled = True
            Unload Me
            Exit Sub
        Else
            arMensaje(2) = ""
        End If
    End If
    
    teserror = g_oSolicitud.ReabrirSolicitud(basOptimizacion.gCodPersonaUsuario, txtMotivo.Text)
        
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
    Else
        arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 5)
        If arMensaje(2) = "1010" Then
            arMensaje(2) = ""
        End If
    
        If Not bSesionIniciada Or oIdsMail Is Nothing Then
            Set oIdsMail = IniciarSesionMail
            bSesionIniciada = True
        End If
        'doevents
        teserror = ComponerMensaje(arMensaje(0), arMensaje(1), arMensaje(2), , , arMensaje(4), , , arMensaje(5), _
                                    lIdInstancia:=g_oSolicitud.Id, _
                                    entidadNotificacion:=entidadNotificacion.Solicitud, _
                                    tipoNotificacion:=ReaperturaSolicitudPet, _
                                    sToName:=g_oSolicitud.Peticionario.nombre & " " & g_oSolicitud.Peticionario.Apellidos)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        End If
    
        If bSesionIniciada Then
            FinalizarSesionMail
        End If
        
        Set oEmails = Nothing


        'Actualiza la grid y la colecci�n del formulario de solicitudes
        'Pero s�lo si el formulario origen es el de solicitudes
        Select Case g_sOrigen2
            Case "frmSolicitudes"
                frmSolicitudes.ActualizarEstadoEnGrid g_oSolicitud.Id, EstadoSolicitud.Aprobada
                If g_sOrigen = "frmSolicitudDetalle" Then
                    frmSolicitudes.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada
                End If
                    
            Case "frmPROCE"
                frmPROCE.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada
                
            Case "frmCatalogo"
                frmCatalogo.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada
                
            Case "frmPedidos"
                frmPEDIDOS.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada
                
            Case "frmSeguimiento"
                frmSeguimiento.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Aprobada
        End Select
        
        
        ''' Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCSolicReabrir, "Id:" & g_oSolicitud.Id
        
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "Reabrir", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
''' <summary>
''' Rechazar solicitud
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub Rechazar()
    Dim teserror As TipoErrorSummit
    Dim oEmails As CEmailSolicitudes
    Dim arMensaje As Variant
    Dim iRes As Integer
    Dim bSesionIniciada  As Boolean
    
    'Anula la solicitud que est� seleccionada en la grid
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    'Env�a el email al peticionario
    Set oEmails = oFSGSRaiz.Generar_CEmailSolicitudes
    arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 2)

    If arMensaje(0) = "" Then
    ElseIf arMensaje(2) = "1010" Then 'No encontr� la ruta o plantilla
        iRes = oMensajes.PreguntaRutaSolicitudesNoEncontrada
        If iRes = vbNo Then
            Set oEmails = Nothing
            Screen.MousePointer = vbNormal
            cmdAceptar.Enabled = True
            Unload Me
            Exit Sub
        Else
            arMensaje(2) = ""
        End If
    End If
    
    teserror = g_oSolicitud.RechazarSolicitud(basOptimizacion.gCodPersonaUsuario, txtMotivo.Text)
        
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        
    Else

        'Actualiza la grid y la colecci�n del formulario de solicitudes
        'Pero s�lo si el formulario origen es el de solicitudes
        Select Case g_sOrigen2
            Case "frmSolicitudes"
                frmSolicitudes.ActualizarEstadoEnGrid g_oSolicitud.Id, EstadoSolicitud.Rechazada
                If g_sOrigen = "frmSolicitudDetalle" Then
                    frmSolicitudes.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Rechazada
                End If
                
            Case "frmPROCE"
                frmPROCE.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Rechazada
                
            Case "frmCatalogo"
                frmCatalogo.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Rechazada
                
            Case "frmPedidos"
                frmPEDIDOS.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Rechazada
                
            Case "frmSeguimiento"
                frmSeguimiento.g_ofrmDetalleSolic.ActualizarEstadoSolicitud EstadoSolicitud.Rechazada
        End Select
        
        
        '''Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCSolicRechazar, "Id:" & g_oSolicitud.Id
        
        arMensaje = oEmails.Mensaje_Solicitud(g_oSolicitud.Id, basPublic.gParametrosInstalacion.gsPathSolicitudes, 2)
        If arMensaje(2) = "1010" Then
            arMensaje(2) = ""
        End If
            
        If Not bSesionIniciada Or oIdsMail Is Nothing Then
            Set oIdsMail = IniciarSesionMail
            bSesionIniciada = True
        End If
        'doevents
        teserror = ComponerMensaje(arMensaje(0), arMensaje(1), arMensaje(2), , , arMensaje(4), , , arMensaje(5), _
                        lIdInstancia:=g_oSolicitud.Id, _
                        entidadNotificacion:=entidadNotificacion.Solicitud, _
                        tipoNotificacion:=RechazoSolicitudPet, _
                        sToName:=g_oSolicitud.Peticionario.nombre & " " & g_oSolicitud.Peticionario.Apellidos)
                        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        End If
    
        If bSesionIniciada Then
            FinalizarSesionMail
        End If
        
        Set oEmails = Nothing
        
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "Rechazar", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>Llama a la funci�n de BD que activa la monitorizaci�n.</summary>
''' <remarks>Private Sub cmdAceptar_Click(); Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 10/12/2012</revision>

Private Sub Monitorizar()
    Dim teserror As TipoErrorSummit
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
            
    teserror = g_oSolicitud.Monitorizar(True, basOptimizacion.gCodPersonaUsuario, txtMotivo.Text)
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
    Else
        frmSolicitudes.sdbgSolicitudes.Columns("MONITORIZ").Value = True
        frmSolicitudes.sdbgSolicitudes.Columns("TEXTOMONITORIZ").Value = Replace(txtMotivo.Text, Chr(13) & Chr(10), " ")
        frmSolicitudes.sdbgSolicitudes.Columns("ALERTA_MONITORIZACION").Visible = True
        frmSolicitudes.sdbgSolicitudes.Scroll -1, frmSolicitudes.sdbgSolicitudes.Row
        frmSolicitudes.sdbgSolicitudes.Update

    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "Monitorizar", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_iAccion = 0
    Set g_oSolicitud = Nothing
    g_sOrigen = ""
    g_sOrigen2 = ""
    g_bSoloConsulta = False
    m_sMotivoAnular = ""
    m_sMotivoCerrar = ""
    m_sMotivoReabrir = ""
    m_sMotivoRechazar = ""
    m_sCaptionAnular = ""
    m_sCaptionCerrar = ""
    m_sCaptionReabrir = ""
    m_sCaptionRechazar = ""
    m_sComentario = ""
    m_sCaptionMonitorizar = ""
    m_sMotivoMonitorizar = ""
    m_bActivado = False
    m_sMsgError = ""
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSolicitudGestionar", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
