VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmVARCalAnya 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DA�adir variable de calidad"
   ClientHeight    =   1725
   ClientLeft      =   615
   ClientTop       =   5505
   ClientWidth     =   6210
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVARCalAnya.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1725
   ScaleWidth      =   6210
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtDen 
      Height          =   285
      Left            =   1520
      TabIndex        =   1
      Top             =   680
      Width           =   4500
   End
   Begin VB.TextBox txtCod 
      Height          =   285
      Left            =   1520
      MaxLength       =   5
      TabIndex        =   0
      Top             =   200
      Width           =   2000
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1920
      TabIndex        =   3
      Top             =   1200
      Width           =   975
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3120
      TabIndex        =   4
      Top             =   1200
      Width           =   975
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgDenominaciones 
      Height          =   615
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Visible         =   0   'False
      Width           =   6000
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   3
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2514
      Columns(0).Caption=   "IDI"
      Columns(0).Name =   "IDI"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   14671839
      Columns(1).Width=   7408
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD_IDI"
      Columns(2).Name =   "COD_IDI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   10583
      _ExtentY        =   1085
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblDen 
      BackColor       =   &H00808000&
      Caption         =   "DNombre:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   720
      Width           =   1500
   End
   Begin VB.Label lblCod 
      BackColor       =   &H00808000&
      Caption         =   "DIdentificador:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   240
      Width           =   1500
   End
End
Attribute VB_Name = "frmVARCalAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Raiz As CRaiz
Public oMensajes  As CMensajes
Public g_lMaxID1 As Long
Public g_bAnyadir As Boolean
Public g_oVarCal1 As CVariableCalidad
Public m_bMultiIdioma As Boolean
Public g_oIdiomas As CIdiomas
Public g_oVarsCalidad1Mod As CVariablesCalidad
Public g_VarCalID As Long
Public g_sCadena As String
Public g_lLongCodVarCal As Long
Public g_Aceptar As Boolean

Private m_sCod As String
Private m_sDen As String

Private Sub cmdAceptar_Click()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n: A�ade un nuevo Tab con la nueva Variable de calidad de Nivel 1
'                 con todos los idiomas
'*** Par�metros de entrada: ninguno
'*** Llamada desde: propio formulario
'*** Tiempo m�ximo: 0,04seg
'************************************************************
'Dim inicio As Double, final As Double, tiempoTranscurrido As Double
'inicio = Timer

Dim oVarCal As CVariableCalidad
Dim lId As Long
Dim i As Byte
Dim vbm As Variant
Dim sCod, sDen As String
Dim sCadena, sCadenaIni As String
Dim bDatosModificados As Boolean

    If sdbgDenominaciones.DataChanged Then
        sdbgDenominaciones.Update
    End If
    Dim oDenominaciones As CMultiidiomas
    Set oDenominaciones = Raiz.Generar_CMultiidiomas
    If Trim(txtCod.Text) = "" Then
        oMensajes.NoValido m_sCod
        Exit Sub
    End If
    'Solo con letras y n�meros
    If Not NombreDeVariableValido(txtCod.Text) Then
        oMensajes.NoValido 176
        Exit Sub
    End If
    
    For i = 1 To g_oIdiomas.Count
        vbm = sdbgDenominaciones.AddItemBookmark(i - 1)
        sCod = sdbgDenominaciones.Columns(2).CellValue(vbm)
        sDen = sdbgDenominaciones.Columns(1).CellValue(vbm)
        oDenominaciones.Add sCod, sDen
        If sDen = "" Then
                oMensajes.NoValido m_sDen & " (" & g_oIdiomas.Item(sCod).Den & ")"
                Exit Sub
        Else
            If Not g_bAnyadir Then
                If sDen <> g_oVarCal1.Denominaciones.Item(sCod).Den Then
                    bDatosModificados = True
                End If
            End If
            
            If sCod = Idioma Then
                sCadenaIni = sDen
            Else
                If sCadena <> "" Then sCadena = sCadena & " / "
                sCadena = sCadena & sDen
            End If
        End If
    Next i
    sCadena = sCadenaIni & " / " & sCadena

    'Comprobar que el c�digo no exista ya para las variables de nivel 1:
    Screen.MousePointer = vbHourglass
    If g_bAnyadir Then
        lId = g_lMaxID1
        g_lMaxID1 = lId + 1
        
        'A�ade la variable de calidad al tab y a la colecci�n de la pantalla de variables de calidad:
        Set oVarCal = g_oVarsCalidad1Mod.Add(lId, 1, Trim(txtCod.Text), oDenominaciones)
        Set oVarCal.Denominaciones = oDenominaciones
        oVarCal.modificado = True
        Set oVarCal.VariblesCal = Raiz.Generar_CVariablesCalidad
        g_VarCalID = oVarCal.Id
        
    Else
        If g_oVarCal1.Cod <> Trim(txtCod.Text) Then
            g_oVarCal1.Cod = Trim(txtCod.Text)
            g_oVarCal1.modificado = True
        End If
            
        If bDatosModificados Then
            
            Set g_oVarCal1.Denominaciones = oDenominaciones
            g_oVarCal1.modificado = True
        End If
    End If
    
    g_sCadena = sCadena
    Set g_oVarCal1 = Nothing
    Set oVarCal = Nothing
    Set oDenominaciones = Nothing
    Screen.MousePointer = vbNormal
    g_Aceptar = True
    Unload Me

End Sub

Private Sub cmdCancelar_Click()
    g_Aceptar = False
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Height = 2040
    Me.Width = 6210
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    'If m_bMultiIdioma = True Then
    If g_oIdiomas.Count > 1 Then
        'Es multiidioma,muestra la grid:
        sdbgDenominaciones.Visible = True
        lblDen.Visible = False
        txtDen.Visible = False
        CargarIdiomas
        
        If Not g_bAnyadir Then
            txtCod.Text = g_oVarCal1.Cod
        End If
        
        'Ajusta la pantalla para que no haya que hacer scroll
        sdbgDenominaciones.Width = 5980
        sdbgDenominaciones.Height = (sdbgDenominaciones.Rows + 1.2) * sdbgDenominaciones.RowHeight
        cmdAceptar.Top = sdbgDenominaciones.Top + sdbgDenominaciones.Height + 85
        cmdCancelar.Top = cmdAceptar.Top
        Me.Height = cmdAceptar.Top + cmdAceptar.Height + 550
        
    Else
        'Se muestra solo el textbox
        sdbgDenominaciones.Visible = False
        lblDen.Visible = True
        txtDen.Visible = True
        
        If Not g_bAnyadir Then
            txtCod.Text = g_oVarCal1.Cod
            txtDen.Text = g_oVarCal1.Denominaciones.Item(Idioma).Den
        End If
    
    End If
            
    txtCod.MaxLength = g_lLongCodVarCal

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIDAD_ANYA, Idioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value  '1 Variables de calidad
        Ador.MoveNext
        lblCod.Caption = Ador(0).Value & ":"  '2 Identificador:
        m_sCod = Ador(0).Value
        Ador.MoveNext
        lblDen.Caption = Ador(0).Value & ":"  '3 Nombre:
        m_sDen = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value  '4 &Aceptar
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value  '5 &Cancelar
        Ador.MoveNext
        If g_oIdiomas.Count > 1 Then
            Me.sdbgDenominaciones.Columns("IDI").Caption = Ador(0).Value  '6 "dIdioma"
            Me.sdbgDenominaciones.Columns("DEN").Caption = m_sDen
        
        End If
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub


Private Sub CargarIdiomas()
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n: carga la grid con los idiomas que hay en la plataforma
'*** Par�metros de entrada: ninguno

'*** Llamada desde: form_load
'*** Tiempo m�ximo: 0,1
'************************************************************
'Dim inicio As Double, final As Double, tiempoTranscurrido As Double
'inicio = Timer
            
Dim oIdioma As CIdioma
Dim sValor As String


    'Carga las denominaciones del grupo de datos generales en todos los idiomas:
    Set oIdioma = g_oIdiomas.Item(Idioma)
    If Not g_bAnyadir Then
        sValor = g_oVarCal1.Denominaciones.Item(Idioma).Den
    Else
        sValor = ""
    End If
    sdbgDenominaciones.AddItem oIdioma.Den & Chr(m_lSeparador) & sValor & Chr(m_lSeparador) & oIdioma.Cod


    For Each oIdioma In g_oIdiomas
        If oIdioma.Cod <> Idioma Then
            If Not g_bAnyadir Then
                sValor = g_oVarCal1.Denominaciones.Item(oIdioma.Cod).Den
            Else
                sValor = ""
            End If
    
            sdbgDenominaciones.AddItem oIdioma.Den & Chr(m_lSeparador) & sValor & Chr(m_lSeparador) & oIdioma.Cod
        End If

    Next
    sdbgDenominaciones.MoveFirst
    
    Set oIdioma = Nothing
    
'    final = Timer
'    tiempoTranscurrido = (final - inicio)

End Sub
