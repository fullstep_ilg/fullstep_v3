VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmVarCalSubtipo 
   Caption         =   "Form1"
   ClientHeight    =   5355
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9780
   Icon            =   "frmVarCalSubtipo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5355
   ScaleWidth      =   9780
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Height          =   4500
      Left            =   90
      ScaleHeight     =   4440
      ScaleWidth      =   9225
      TabIndex        =   0
      Top             =   360
      Width           =   9285
      Begin VB.PictureBox picNumerico 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   4395
         Left            =   0
         ScaleHeight     =   4395
         ScaleWidth      =   9285
         TabIndex        =   1
         Top             =   0
         Width           =   9285
         Begin VB.PictureBox Picture3 
            Appearance      =   0  'Flat
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   1215
            Left            =   315
            ScaleHeight     =   1170
            ScaleMode       =   0  'User
            ScaleWidth      =   7815
            TabIndex        =   18
            Top             =   3000
            Width           =   7815
            Begin VB.OptionButton optOpcionConf 
               BackColor       =   &H00808000&
               Caption         =   "DNo calcular"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   2
               Left            =   225
               TabIndex        =   21
               Top             =   900
               Width           =   3240
            End
            Begin VB.OptionButton optOpcionConf 
               BackColor       =   &H00808000&
               Caption         =   "DEvaluar f�rmula"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   0
               Left            =   225
               TabIndex        =   20
               Top             =   300
               Width           =   3120
            End
            Begin VB.OptionButton optOpcionConf 
               BackColor       =   &H00808000&
               Caption         =   "DMedia ponderada seg�n:"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   1
               Left            =   225
               TabIndex        =   19
               Top             =   600
               Width           =   3100
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcVarPond 
               Height          =   285
               Left            =   3500
               TabIndex        =   22
               Top             =   645
               Width           =   3900
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "COD"
               Columns(0).Alignment=   1
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6800
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6879
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblOpcionConf 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DSelecci�n del modo de c�lculo para unidades de negocio de nivel superior:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   23
               Top             =   0
               Width           =   5385
            End
         End
         Begin VB.PictureBox Picture2 
            Appearance      =   0  'Flat
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   3000
            ScaleHeight     =   375
            ScaleWidth      =   5655
            TabIndex        =   13
            Top             =   150
            Width           =   5655
            Begin VB.OptionButton optPeriodo 
               BackColor       =   &H00808000&
               Caption         =   "-a�o en curso"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   1
               Left            =   3120
               TabIndex        =   17
               Top             =   0
               Width           =   2535
            End
            Begin VB.OptionButton optPeriodo 
               BackColor       =   &H00808000&
               Height          =   375
               Index           =   0
               Left            =   0
               TabIndex        =   16
               Top             =   0
               Value           =   -1  'True
               Width           =   250
            End
            Begin VB.TextBox txtPeriodo 
               Alignment       =   1  'Right Justify
               Height          =   315
               Left            =   250
               TabIndex        =   14
               Top             =   0
               Width           =   1020
            End
            Begin VB.Label lblPeriodo 
               BackColor       =   &H00808000&
               Caption         =   "meses"
               ForeColor       =   &H00FFFFFF&
               Height          =   285
               Index           =   1
               Left            =   1400
               TabIndex        =   15
               Top             =   50
               Width           =   1560
            End
         End
         Begin VB.TextBox txtFormula 
            Height          =   315
            Left            =   3060
            TabIndex        =   3
            Top             =   2430
            Width           =   4230
         End
         Begin VB.CommandButton cmdAyuda 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   7.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   7365
            Picture         =   "frmVarCalSubtipo.frx":014A
            Style           =   1  'Graphical
            TabIndex        =   2
            Top             =   2445
            Width           =   350
         End
         Begin VB.Label lblFormula 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "Ponderacion de la subvariable:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   315
            TabIndex        =   10
            Top             =   2070
            Width           =   2205
         End
         Begin VB.Label lblPeriodo 
            BackColor       =   &H00808000&
            Caption         =   "Periodo a considerar:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   0
            Left            =   315
            TabIndex        =   9
            Top             =   240
            Width           =   2250
         End
         Begin VB.Label lblVariables 
            BackColor       =   &H00808000&
            Caption         =   "Variables:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   315
            TabIndex        =   8
            Top             =   750
            Width           =   2310
         End
         Begin VB.Label lblVarX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X1 = No conformidades abiertas"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   0
            Left            =   3060
            TabIndex        =   7
            Top             =   750
            Width           =   2265
         End
         Begin VB.Label lblVarX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X2 = No conformidades cerradas dentro de plazo"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   1
            Left            =   3060
            TabIndex        =   6
            Top             =   1065
            Width           =   3465
         End
         Begin VB.Label lblVarX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X3 = No conformidades cerradas fuera de plazo"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   2
            Left            =   3060
            TabIndex        =   5
            Top             =   1365
            Width           =   3375
         End
         Begin VB.Label lblVarX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X4 = No conformidades con cierre negativo"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   3
            Left            =   3060
            TabIndex        =   4
            Top             =   1680
            Width           =   3075
         End
      End
   End
   Begin VB.Label lblTipo 
      Caption         =   "Subvariable de tipo: "
      Height          =   240
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   3300
   End
   Begin VB.Label lblCambios 
      Caption         =   "Para guardar los cambios efectuados cierre esta ventana y pulse Guardar cambios en la ventada principal de Variables de calidad."
      ForeColor       =   &H000000FF&
      Height          =   240
      Left            =   105
      TabIndex        =   11
      Top             =   5000
      Width           =   9735
   End
End
Attribute VB_Name = "frmVarCalSubtipo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables publicas
Public g_oVariableCalidad As CVariableCalidad
Public g_uvdSubTipo As CalidadSubtipo
Public GestorIdiomas As CGestorIdiomas
Public Mensajes As CMensajes
Public FrmVARCalidad As Object
Public FrmVARCalCompuestaNivel5 As Object
Public FrmVARCalCompuestaNivel4 As Object
Public FrmVARCalCompuestaNivel3 As Object
Public FrmVARCalNivel2 As Object
Public Idioma As String

Private m_sTitulo As String
Private m_sTiposVar(0 To 2) As String
Private m_sFormula As String
Private m_sIdiErrorFormula(11) As String

Private Sub cmdAyuda_Click()
    MostrarFormSOLAyudaCalculosLocal GestorIdiomas, Idioma
End Sub

Private Sub Form_Load()
    Me.Width = 9900
    Me.Height = 5500 '4290
        
    CargarRecursos
    CargarVariablesHermanas
    Me.Caption = m_sTitulo & " " & g_oVariableCalidad.Denominaciones.Item(Idioma).Den
    Dim indice As Byte
    Select Case g_uvdSubTipo
        Case CalPPM
            indice = 0
        Case CalCargoProveedores
            indice = 1
        Case Else
            indice = 2
    End Select
    lblTipo.Caption = lblTipo.Caption & " " & m_sTiposVar(indice)
    
    If NullToStr(g_oVariableCalidad.NCPeriodo) = "" Then
        txtPeriodo.Text = NullToStr(g_oVariableCalidad.NCPeriodo)
        optPeriodo(0).Value = True
    Else
        If CInt(g_oVariableCalidad.NCPeriodo) = 0 Then
            txtPeriodo.Text = ""
            optPeriodo(1).Value = True
        Else
            txtPeriodo.Text = CInt(g_oVariableCalidad.NCPeriodo)
            optPeriodo(0).Value = True
        End If
    End If
    
    optOpcionConf(g_oVariableCalidad.Opcion_Conf).Value = True
    If g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
        sdbcVarPond.Columns("ID").Value = g_oVariableCalidad.IdVar_Pond
    End If
    
    txtFormula.Text = NullToStr(g_oVariableCalidad.Formula)
    lblCambios.Visible = False
    
    picNumerico.Enabled = Not g_oVariableCalidad.BajaLog
End Sub

''' <summary>
''' Carga los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Dim iDen(3) As Integer

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIDAD_SUB, Idioma)
    
    If Not Ador Is Nothing Then
        m_sTitulo = Ador(0).Value  ' 1 Puntuaci�n de la subvariable:
        Ador.MoveNext
        lblTipo.Caption = Ador(0).Value '2 Subvariable de tipo:
        Ador.MoveNext
        lblPeriodo(0).Caption = Ador(0).Value '3 Periodo a considerar:
        Ador.MoveNext
        lblPeriodo(1).Caption = Ador(0).Value '4 meses
        Ador.MoveNext
        lblVariables.Caption = Ador(0).Value '5 Variables:
        Ador.MoveNext
        lblFormula.Caption = Ador(0).Value '6 Ponderaci�n de la subvariable:
        Ador.MoveNext
        m_sTiposVar(0) = Ador(0).Value '7 PPM
        Ador.MoveNext
        m_sTiposVar(1) = Ador(0).Value '8 Cargo a proveedores
        Ador.MoveNext
        m_sTiposVar(2) = Ador(0).Value '9 Tasa de servicios
        Ador.MoveNext
        If g_uvdSubTipo = CalPPM Then
            lblVarX(0) = Ador(0).Value ' 10 "X1 = Piezas defectuosas"
            iDen(0) = 10
            iDen(1) = 11
            Ador.MoveNext
            lblVarX(1) = Ador(0).Value ' 11 "X2 = Piezas servidas"
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
        
        ElseIf g_uvdSubTipo = CalCargoProveedores Then
            iDen(0) = 12
            iDen(1) = 13
            Ador.MoveNext
            Ador.MoveNext
            lblVarX(0) = Ador(0).Value  ' 12 "X1 = Importe repercutido"
            Ador.MoveNext
            lblVarX(1) = Ador(0).Value  ' 13 "X2 = Facturaci�n"
            Ador.MoveNext
            Ador.MoveNext
        
        ElseIf g_uvdSubTipo = CalTasaServicios Then
            iDen(0) = 14
            iDen(1) = 15
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            lblVarX(0) = Ador(0).Value  ' 14 "X1 = Env�os correctos"
            Ador.MoveNext
            lblVarX(1) = Ador(0).Value  ' 15 "X2 = Total env�os"
        End If
        Ador.MoveNext
        lblVarX(2) = Ador(0).Value  ' 16 "X3 = Objetivo"
        Ador.MoveNext
        lblVarX(3) = Ador(0).Value  ' 17 "X4 = Suelo"
        iDen(2) = 16
        iDen(3) = 17
        Ador.MoveNext
        lblCambios.Caption = Ador(0).Value  '18 Para guardar los cambios efectuados cierre esta ventana y pulse Guardar cambios en la ventada principal de Variables de calidad.
        Ador.MoveNext
        m_sFormula = Ador(0).Value '19
        
        For i = 1 To 11
            Ador.MoveNext
            m_sIdiErrorFormula(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        optPeriodo(1).Caption = Ador(0).Value ' a�o en curso
        
        Ador.MoveNext ' 32 Valoraci�n de proveedor
        Ador.MoveNext
        lblOpcionConf.Caption = Ador(0).Value ' Selecci�n del modo de c�lculo para unidades de negocio de nivel superior:
        Ador.MoveNext
        optOpcionConf(0).Caption = Ador(0).Value ' Evaluar f�rmula
        Ador.MoveNext
        optOpcionConf(1).Caption = Ador(0).Value ' Media ponderada seg�n:
        Ador.MoveNext
        optOpcionConf(2).Caption = Ador(0).Value ' No calcular
        
        Ador.Close
    End If
    Set Ador = Nothing
End Sub

Private Sub Arrange()
    Dim dblTres As Double

    On Error Resume Next
    If Not lblCambios.Visible Then
        Picture1.Height = Me.Height - 915
    Else
        Picture1.Height = Me.Height - 1300
    End If
    dblTres = Me.Width / 3
    lblTipo.Width = dblTres - 100
    Picture1.Width = Me.Width - 405
    lblCambios.Top = Picture1.Top + Picture1.Height + 90
    
    picNumerico.Height = Picture1.Height
    picNumerico.Width = Picture1.Width
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ValidarPonderacion Then
        Cancel = True
        Exit Sub
    End If
    
    Set g_oVariableCalidad = Nothing
End Sub

''' <summary>Validamos si son correctos los datos introducidos</summary>
''' <remarks>Llamada desde: Form_Unload</remarks>

Private Function ValidarPonderacion() As Boolean
    Dim bGuardar As Boolean
    Dim udtNCCal As TVariablesCalPonderacion
        
    bGuardar = False
    ValidarPonderacion = False
    
    'Si ha cambiado la ponderaci�n la guardo
    If g_oVariableCalidad.BajaLog Then
        ValidarPonderacion = True
        Exit Function
    End If
    If txtFormula.Text = "" Then
        Mensajes.NoValido m_sFormula
        Exit Function
    End If
    If Not ValidarFormula(txtFormula.Text, False) Then
        Mensajes.NoValido m_sFormula
        Exit Function
    End If
    If g_oVariableCalidad.EsMultimaterial Then
        If g_oVariableCalidad.TieneObjetivosSuelosEnFormula(txtFormula.Text) Then
            Mensajes.VarMultiMatNoObjSuelo
            Exit Function
        End If
    End If
    If optPeriodo(0).Value Then
        If txtPeriodo.Text = "" Then
            Mensajes.NoValido Left(lblPeriodo(0).Caption, Len(lblPeriodo(0).Caption) - 1)
            Exit Function
        End If
        If Not IsNumeric(txtPeriodo.Text) Then
            Mensajes.NoValido Left(lblPeriodo(0).Caption, Len(lblPeriodo(0).Caption) - 1)
            Exit Function
        ElseIf CInt(txtPeriodo.Text) = 0 Then
            Mensajes.NoValido Left(lblPeriodo(0).Caption, Len(lblPeriodo(0).Caption) - 1)
            Exit Function
        End If
        txtPeriodo.Text = CInt(txtPeriodo.Text)
    End If
                
    If g_oVariableCalidad.TipoPonderacion <> udtNCCal Then
        bGuardar = True
    ElseIf txtFormula.Text <> NullToStr(g_oVariableCalidad.Formula) Then
        bGuardar = True
    Else
        If optPeriodo(0).Value Then
            If CInt(txtPeriodo.Text) <> NullToDbl0(g_oVariableCalidad.NCPeriodo) Then
                bGuardar = True
            End If
        Else
            bGuardar = True
        End If
    End If
    
    If optOpcionConf(0).Value And g_oVariableCalidad.Opcion_Conf <> VarCalOpcionConf.EvaluarFormula Then
        bGuardar = True
    ElseIf optOpcionConf(1).Value Then
        If sdbcVarPond.Text = "" Then
            Mensajes.NoValido Left(lblOpcionConf.Caption, Len(lblOpcionConf.Caption) - 1)
            Exit Function
        End If
        If sdbcVarPond.Text <> NullToStr(g_oVariableCalidad.IdVar_Pond) Then
            bGuardar = True
        End If
    Else
        If optOpcionConf(2).Value And g_oVariableCalidad.Opcion_Conf <> VarCalOpcionConf.NoCalcular Then bGuardar = True
    End If
        
    If bGuardar Then
        ValidarPonderacion = GuardarPondNC
    Else
        ValidarPonderacion = True
    End If
End Function

'Si se han realizado cambios mostraremos el mensaje de guardar en la pantalla padre
Private Sub HayCambios()
    If Not lblCambios.Visible Then
        VisualizarGuardar
        If Me.WindowState = 2 Then
            Me.Picture1.Height = Me.Picture1.Height - 420
            Me.picNumerico.Height = picNumerico.Height - 420
            Me.lblCambios.Top = Me.lblCambios.Top - 400
        Else
            Me.Height = Me.Height + 420
        End If
    End If
End Sub
Private Function GuardarPondNC() As Boolean
    On Error GoTo Error
    
    'Guardo la punt cuando se guarde todo en el form principal de variables
    g_oVariableCalidad.modificado = True
    
    If NullToStr(g_oVariableCalidad.Formula) <> txtFormula.Text Then
        g_oVariableCalidad.IDFormula = Null
    End If
    If optPeriodo(0).Value Then
        g_oVariableCalidad.NCPeriodo = CInt(txtPeriodo.Text)
    ElseIf optPeriodo(1).Value Then
        g_oVariableCalidad.NCPeriodo = 0
    End If
    g_oVariableCalidad.Formula = txtFormula.Text
    
    If optOpcionConf(VarCalOpcionConf.EvaluarFormula).Value = True Then
        g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.EvaluarFormula
        g_oVariableCalidad.IdVar_Pond = ""
    ElseIf optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True Then
        g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana
        g_oVariableCalidad.IdVar_Pond = sdbcVarPond.Columns(0).Value
    Else
        g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.NoCalcular
        g_oVariableCalidad.IdVar_Pond = ""
    End If
        
    Set g_oVariableCalidad.CamposPond = Nothing
    Set g_oVariableCalidad.INTListaPond = Nothing

    GuardarPondNC = True
    Exit Function
Error:
    GuardarPondNC = False
End Function

Private Function ValidarFormula(ByVal sFormula As String, ByVal bNoMens As Boolean) As Boolean
    Dim sVariables() As String
    Dim lErrCode As Integer
    Dim lIndex As Integer
    Dim iEq As USPExpression
    Dim sCaracter As String
    Dim i As Integer
        
    If sFormula <> "" Then
        Set iEq = New USPExpression
        
        ReDim sVariables(4)
        For i = 1 To 4
            sVariables(i) = "X" & i
        Next
    
        lIndex = iEq.Parse(UCase(sFormula), sVariables, lErrCode)
    
        If lErrCode <> USPEX_NO_ERROR Then
            ' Parsing error handler
            If bNoMens = False Then
                Select Case lErrCode
                    Case USPEX_DIVISION_BY_ZERO
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(1))
                    Case USPEX_EMPTY_EXPRESSION
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(2))
                    Case USPEX_MISSING_OPERATOR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(3))
                    Case USPEX_SYNTAX_ERROR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(4))
                    Case USPEX_UNKNOWN_FUNCTION
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(5))
                    Case USPEX_UNKNOWN_OPERATOR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(6))
                    Case USPEX_WRONG_PARAMS_NUMBER
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(7))
                    Case USPEX_UNKNOWN_IDENTIFIER
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(8))
                    Case USPEX_UNKNOWN_VAR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(9))
                    Case USPEX_VARIABLES_NOTUNIQUE
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(10))
                    Case Else
                        sCaracter = Mid(sFormula, lIndex)
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(11) & vbCrLf & sCaracter)
                End Select
            End If
            Set iEq = Nothing
            ValidarFormula = False
            Exit Function
        End If
        Set iEq = Nothing
    Else
        ValidarFormula = False
        Exit Function
    End If
    
    ValidarFormula = True
End Function

Private Sub optPeriodo_Click(Index As Integer)
    HayCambios
End Sub

Private Sub optOpcionConf_Click(Index As Integer)
    If Index <> VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
        sdbcVarPond.Text = ""
    End If
    HayCambios
End Sub

Private Sub sdbcVarPond_Change()
    HayCambios
End Sub

Private Sub sdbcVarPond_Click()
    HayCambios
    optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True
End Sub

Private Sub txtFormula_Change()
    HayCambios
End Sub

Private Sub txtPeriodo_Change()
    HayCambios
End Sub

Private Sub VisualizarGuardar()
    FrmVARCalidad.VisualizarGuardar
    lblCambios.Visible = True
    
    Select Case g_oVariableCalidad.Nivel
        Case 5
            FrmVARCalCompuestaNivel5.lblCambios.Visible = True
            FrmVARCalCompuestaNivel4.lblCambios.Visible = True
            FrmVARCalCompuestaNivel3.lblCambios.Visible = True
        Case 4
            FrmVARCalCompuestaNivel4.lblCambios.Visible = True
            FrmVARCalCompuestaNivel3.lblCambios.Visible = True
        Case 3
            FrmVARCalCompuestaNivel3.lblCambios.Visible = True
    End Select
End Sub

Private Sub sdbcVarPond_InitColumnProps()
    sdbcVarPond.DataFieldList = "Column 0"
    sdbcVarPond.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcVarPond_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcVarPond.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcVarPond.Rows - 1
            bm = sdbcVarPond.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcVarPond.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcVarPond.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub CargarVariablesHermanas()
    Screen.MousePointer = vbHourglass
    Dim oVarCal As CVariableCalidad
    Dim g_oVariablesCalidadHermanas As CVariablesCalidad
    sdbcVarPond.RemoveAll
    
    Select Case g_oVariableCalidad.Nivel
        Case 5
            Set g_oVariablesCalidadHermanas = FrmVARCalCompuestaNivel5.g_oVarCal4Mod.VariblesCal
        Case 4
            Set g_oVariablesCalidadHermanas = FrmVARCalCompuestaNivel4.g_oVarCal3Mod.VariblesCal
        Case 3
            Set g_oVariablesCalidadHermanas = FrmVARCalCompuestaNivel3.g_oVarCal2Mod.VariblesCal
        Case 2
            Set g_oVariablesCalidadHermanas = FrmVARCalidad.g_oVarsCalidad1Mod.Item(FrmVARCalidad.ssTabVar.SelectedItem.Index).VariblesCal
    End Select
    
    For Each oVarCal In g_oVariablesCalidadHermanas
        If oVarCal.Id <> g_oVariableCalidad.Id And oVarCal.BajaLog = False Then
            sdbcVarPond.AddItem oVarCal.Id & Chr(9) & oVarCal.Cod & " - " & oVarCal.Denominaciones.Item(Idioma).Den
            If oVarCal.Id = g_oVariableCalidad.IdVar_Pond Then
                sdbcVarPond.Text = oVarCal.Cod
            End If
        End If
    Next
    
    Screen.MousePointer = vbNormal
End Sub

