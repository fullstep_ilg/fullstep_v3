VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form frmAhorrosPend 
   Caption         =   "Procesos pendientes de traspasar ahorros"
   ClientHeight    =   4695
   ClientLeft      =   2370
   ClientTop       =   3210
   ClientWidth     =   8580
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAhorrosPend.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4695
   ScaleWidth      =   8580
   Begin RichTextLib.RichTextBox txtProce 
      Height          =   4695
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8565
      _ExtentX        =   15108
      _ExtentY        =   8281
      _Version        =   393217
      BackColor       =   -2147483633
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      TextRTF         =   $"frmAhorrosPend.frx":0CB2
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAhorrosPend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Raiz As CRaiz
Public Mensajes As CMensajes
Public Idioma As String
Public Origen As String
Public Errores As Variant

Private m_oProcesos As CProcesos
Private m_lNumPendientes As Long
Private m_sErrorCal As String
Private m_sPendientes As String
Private m_sFecCierre As String
Private m_sCausa As String
Private m_sErrorProve As String
Private m_lWid As Double
Private m_vLongitudesDeCodigos As LongitudesDeCodigos

Public Property Get gLongitudesDeCodigos() As LongitudesDeCodigos
    gLongitudesDeCodigos = m_vLongitudesDeCodigos
End Property

Public Property Let gLongitudesDeCodigos(ByRef vParametrosGenerales As LongitudesDeCodigos)
    m_vLongitudesDeCodigos = vParametrosGenerales
End Property

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_ESPERA_PARCIAL, Idioma)
    
    If Not Ador Is Nothing Then
        m_sErrorCal = Ador(0).Value
        Ador.MoveNext
        m_sPendientes = Ador(0).Value
        Ador.MoveNext
        m_sFecCierre = Ador(0).Value
        Ador.MoveNext
        m_sCausa = Ador(0).Value
        Ador.MoveNext
        m_sErrorProve = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass

    m_lWid = Me.Width
    CargarRecursos
    
    If Origen = "PENDIENTES" Then
        Me.Caption = m_sPendientes
        Set m_oProcesos = Raiz.generar_CProcesos
        m_lNumPendientes = m_oProcesos.CargarProcesosPendientesDeCalcularAhorros(TipoOrdenacionProcesos.OrdPorFechaUltimaReunion)
    End If
    MostrarTexto
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub MostrarTexto()
    Dim I As Integer
    Dim sTexto As String
    Dim sCausa As String
    Dim oproce As CProceso
    Dim inum As Integer
    Dim sPrincipio As String
    Dim sFinal As String
    Dim iNumChars As Integer
    Dim iSobran As Integer
    Dim teserror As TipoErrorSummit
    
    m_lNumPendientes = 0
    inum = 0
    
    sTexto = ""
    iNumChars = (txtProce.Width / 114.2)
    
    Select Case Origen
        Case "PENDIENTES"
            For Each oproce In m_oProcesos
                inum = inum + 1
                sPrincipio = CStr(oproce.Anyo) & "/" & Space(m_vLongitudesDeCodigos.giLongCodGMN1 - Len(oproce.GMN1Cod)) & oproce.GMN1Cod & "/" & Space(5 - Len(CStr(oproce.Cod))) & CStr(oproce.Cod) & "  "
                sFinal = "  " & m_sFecCierre & " " & oproce.FechaUltimaReunion & vbCrLf
                iSobran = iNumChars - Len(sPrincipio) - Len(sFinal)
                If Len(oproce.Den) < iSobran Then
                    sTexto = sTexto & sPrincipio & oproce.Den & Space(iSobran - Len(oproce.Den)) & sFinal
                Else
                    sTexto = sTexto & sPrincipio & Left(oproce.Den, iSobran) & sFinal
                End If
            Next
        
        Case "ERRORCALCULO"
            Me.Caption = m_sErrorCal
            For I = 0 To UBound(Errores, 2) - 1
                teserror = Errores(1, I)
                inum = inum + 1
                Select Case teserror.NumError
                    Case TESDatoEliminado
                        sCausa = Mensajes.CargarTextoMensaje(142) & " " & Mensajes.CargarTexto(OTROS, 44)
                    Case TESProcesoCambiadoDeEstado
                        If teserror.Arg1 < 11 Then
                            sCausa = Mensajes.CargarTextoMensaje(582)
                        ElseIf teserror.Arg1 = 20 Then
                            sCausa = Mensajes.CargarTextoMensaje(583)
                        End If
                    Case TESProcesoAhorroCalculado
                        sCausa = Mensajes.CargarTextoMensaje(585)
                    Case TESErrorAhorrosCalculando
                        sCausa = Mensajes.CargarTexto(OTROS, 82)
                    Case Else
                        sCausa = Mensajes.CargarTextoMensaje(179)
                End Select
                sPrincipio = Errores(0, I) & "  " & m_sCausa & "  "
                iSobran = iNumChars - Len(sPrincipio) - 1
                If Len(sCausa) < iSobran Then
                    sTexto = sTexto & sPrincipio & sCausa & Space(iSobran - Len(sCausa)) & vbCrLf
                Else
                    sTexto = sTexto & sPrincipio & Left(sCausa, iSobran) & vbCrLf
                End If
            Next
        Case "ERRORCALIDAD"
            Me.Caption = m_sErrorProve
            For I = 0 To UBound(Errores, 2) - 1
                teserror = Errores(1, I)
                inum = inum + 1
                sCausa = TratarErrorSinMensaje(teserror, Mensajes)
                sPrincipio = Errores(0, I) & "  " & m_sCausa & "  "
                iSobran = iNumChars - Len(sPrincipio) - 1
                If Len(sCausa) < iSobran Then
                    sTexto = sTexto & sPrincipio & sCausa & Space(iSobran - Len(sCausa)) & vbCrLf
                Else
                    sTexto = sTexto & sPrincipio & Left(sCausa, iSobran) & vbCrLf
                End If
            Next
    End Select
    
    txtProce.Text = sTexto
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    If Me.Height < 500 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    txtProce.Height = Me.Height - 405
    txtProce.Width = Me.Width - 135
    
    If m_lWid <> Me.Width Then
        MostrarTexto
        m_lWid = Me.Width
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oProcesos = Nothing
End Sub
