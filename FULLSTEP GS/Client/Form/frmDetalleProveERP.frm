VERSION 5.00
Begin VB.Form frmDetalleProveERP 
   BackColor       =   &H00808000&
   Caption         =   "DDetalle proveedor ERP"
   ClientHeight    =   3330
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   5460
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetalleProveERP.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3330
   ScaleWidth      =   5460
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtCampo4 
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   2750
      Width           =   3300
   End
   Begin VB.TextBox txtCampo3 
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   2250
      Width           =   3300
   End
   Begin VB.TextBox txtCampo2 
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   1750
      Width           =   3300
   End
   Begin VB.TextBox txtCampo1 
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   1250
      Width           =   3300
   End
   Begin VB.TextBox txtDescripcion 
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   750
      Width           =   3300
   End
   Begin VB.TextBox txtCodigo 
      Height          =   285
      Left            =   1800
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   250
      Width           =   3300
   End
   Begin VB.Label lblCampo4 
      BackStyle       =   0  'Transparent
      Caption         =   "Campo4"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   90
      TabIndex        =   5
      Top             =   2750
      Width           =   1400
   End
   Begin VB.Label lblCampo3 
      BackStyle       =   0  'Transparent
      Caption         =   "Campo3"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   90
      TabIndex        =   4
      Top             =   2250
      Width           =   1400
   End
   Begin VB.Label lblCampo2 
      BackStyle       =   0  'Transparent
      Caption         =   "Campo2"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   90
      TabIndex        =   3
      Top             =   1750
      Width           =   1400
   End
   Begin VB.Label lblCampo1 
      BackStyle       =   0  'Transparent
      Caption         =   "Campo1"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   90
      TabIndex        =   2
      Top             =   1250
      Width           =   1400
   End
   Begin VB.Label lblDescripcion 
      BackStyle       =   0  'Transparent
      Caption         =   "DDescripcion"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   90
      TabIndex        =   1
      Top             =   750
      Width           =   1400
   End
   Begin VB.Label lblCodigo 
      BackStyle       =   0  'Transparent
      Caption         =   "CC�digo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   90
      TabIndex        =   0
      Top             =   250
      Width           =   1400
   End
End
Attribute VB_Name = "frmDetalleProveERP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ProveERP As CProveERP
Public GestorIdiomas As CGestorIdiomas
Private m_vParametrosGenerales As ParametrosGenerales
Private m_vParametrosInstalacion As ParametrosInstalacion


Private Sub Form_Load()
    CargarRecursos
    RellenarTextos
End Sub

Private Sub CargarRecursos()

Dim Ador As ADODB.Recordset

    On Error Resume Next

    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_DETALLE_PROVE_ERP, ParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value                  '(MOD=506, ID=1) Detalle proveedor ERP
        Ador.MoveNext
        lblCodigo.Caption = Ador(0).Value           '(MOD=506, ID=2) C�digo:
        Ador.MoveNext
        lblDescripcion.Caption = Ador(0).Value      '(MOD=506, ID=3) Descripci�n:

        Ador.Close
    End If

    Set Ador = Nothing

End Sub

''' <summary> Muestra los datos del proveedor en el ERP </summary>
''' <remarks> Llamada desde Form_Load </remarks>
Private Sub RellenarTextos()


    txtCodigo.Text = ProveERP.Cod
    txtDescripcion.Text = ProveERP.Den

    'Si est� activado se muestran los labels correspondientes, sino no
    
    'CAMPO1
    If ParametrosGenerales.gbCampo1ERPAct = True Then
        lblCampo1.Caption = ParametrosGenerales.gsCampo1ERP & ":"
        txtCampo1.Text = NullToStr(ProveERP.Campo1)
    Else
        lblCampo1.Visible = False
        txtCampo1.Visible = False
    End If
    
    
    'CAMPO2
    If ParametrosGenerales.gbCampo2ERPAct = True Then
        lblCampo2.Caption = ParametrosGenerales.gsCampo2ERP & ":"
        txtCampo2.Text = NullToStr(ProveERP.Campo2)
        
        'Reordenar
        If Not ParametrosGenerales.gbCampo1ERPAct Then
            lblCampo2.Top = 1250
            txtCampo2.Top = 1250
        End If
        
    Else
        lblCampo2.Visible = False
        txtCampo2.Visible = False
    End If
    
    
    'CAMPO3
    If ParametrosGenerales.gbCampo3ERPAct = True Then
        lblCampo3.Caption = ParametrosGenerales.gsCampo3ERP & ":"
        txtCampo3.Text = NullToStr(ProveERP.Campo3)
        
        'Reordenar
        If ParametrosGenerales.gbCampo2ERPAct Then
            lblCampo3.Top = lblCampo2.Top + 500
            txtCampo3.Top = txtCampo2.Top + 500
        Else
            If ParametrosGenerales.gbCampo1ERPAct Then
                lblCampo3.Top = lblCampo1.Top + 500
                txtCampo3.Top = txtCampo1.Top + 500
            Else
                lblCampo3.Top = 1250
                txtCampo3.Top = 1250
            End If
        End If
        
    Else
        lblCampo3.Visible = False
        txtCampo3.Visible = False
    End If


    'CAMPO4
    If ParametrosGenerales.gbCampo4ERPAct = True Then
        lblCampo4.Caption = ParametrosGenerales.gsCampo4ERP & ":"
        txtCampo4.Text = NullToStr(ProveERP.Campo4)
        
        'Reordenar
        If ParametrosGenerales.gbCampo3ERPAct Then
            lblCampo4.Top = lblCampo3.Top + 500
            txtCampo4.Top = txtCampo3.Top + 500
        Else
            If ParametrosGenerales.gbCampo2ERPAct Then
                lblCampo4.Top = lblCampo2.Top + 500
                txtCampo4.Top = txtCampo2.Top + 500
            Else
                If ParametrosGenerales.gbCampo1ERPAct Then
                    lblCampo4.Top = lblCampo1.Top + 500
                    txtCampo4.Top = txtCampo1.Top + 500
                Else
                    lblCampo4.Top = 1250
                    txtCampo4.Top = 1250
                End If
            End If
        End If
        
    Else
        lblCampo4.Visible = False
        txtCampo4.Visible = False
    End If
 
    Screen.MousePointer = vbNormal

End Sub

Public Property Get ParametrosGenerales() As ParametrosGenerales
    ParametrosGenerales = m_vParametrosGenerales
End Property

Public Property Let ParametrosGenerales(ByRef vParametrosGenerales As ParametrosGenerales)
    m_vParametrosGenerales = vParametrosGenerales
End Property

Public Property Get ParametrosInstalacion() As ParametrosInstalacion
    ParametrosInstalacion = m_vParametrosInstalacion
End Property

Public Property Let ParametrosInstalacion(ByRef vParametrosInstalacion As ParametrosInstalacion)
    m_vParametrosInstalacion = vParametrosInstalacion
End Property
