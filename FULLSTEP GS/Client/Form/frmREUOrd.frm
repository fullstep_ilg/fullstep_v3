VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmREUOrd 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ordenar reuni�n"
   ClientHeight    =   1485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4725
   Icon            =   "frmREUOrd.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1485
   ScaleWidth      =   4725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1170
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1080
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2370
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1080
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcCrit1 
      Height          =   285
      Left            =   2190
      TabIndex        =   1
      Top             =   180
      Width           =   2415
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   -2147483640
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   5239
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcCrit2 
      Height          =   285
      Left            =   2190
      TabIndex        =   3
      Top             =   660
      Width           =   2415
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   -2147483640
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   5239
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4260
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Criterio secundario:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   300
      TabIndex        =   2
      Top             =   720
      Width           =   1935
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Criterio principal:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   300
      TabIndex        =   0
      Top             =   240
      Width           =   1935
   End
End
Attribute VB_Name = "frmREUOrd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private sIdioma() As String
Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Idiomas As CIdiomas
Public ValoresOrdenacion As Variant

Private m_ParamGen As ParametrosGenerales
Public Property Get ParamGen() As ParametrosGenerales
    ParamGen = m_ParamGen
End Property
Public Property Let ParamGen(ByRef vParametrosGenerales As ParametrosGenerales)
    m_ParamGen = vParametrosGenerales
End Property

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    
    Dim Criterio1 As TipoOrdenacionReuniones
    Dim Criterio2 As TipoOrdenacionReuniones

    Screen.MousePointer = vbHourglass
    Select Case sdbcCrit1
        Case m_ParamGen.gsDEN_GMN1
                Criterio1 = TipoOrdenacionReuniones.OrdPorMatPrimerNivel
        Case sIdioma(1)
                Criterio1 = TipoOrdenacionReuniones.OrdPorPersonas
        Case sIdioma(2)
                Criterio1 = TipoOrdenacionReuniones.OrdPorDeptos
        Case sIdioma(3)
                Criterio1 = TipoOrdenacionReuniones.OrdPorResponsable
        Case sIdioma(4)
                Criterio1 = TipoOrdenacionReuniones.OrdPorTipoReu
    End Select

    Select Case sdbcCrit2
        Case m_ParamGen.gsDEN_GMN1
                Criterio2 = TipoOrdenacionReuniones.OrdPorMatPrimerNivel
        Case sIdioma(1)
                Criterio2 = TipoOrdenacionReuniones.OrdPorPersonas
        Case sIdioma(2)
                Criterio2 = TipoOrdenacionReuniones.OrdPorDeptos
        Case sIdioma(3)
                Criterio2 = TipoOrdenacionReuniones.OrdPorResponsable
        Case sIdioma(4)
                Criterio2 = TipoOrdenacionReuniones.OrdPorTipoReu
    End Select
    
    ReDim ValoresOrdenacion(0 To 4)
    
    ValoresOrdenacion(0) = True
    ValoresOrdenacion(1) = Criterio1
    ValoresOrdenacion(2) = Criterio2
    ValoresOrdenacion(3) = sdbcCrit1
    ValoresOrdenacion(4) = sdbcCrit2
'    teserror = frmREU.oReuSeleccionada.Ordenar(Criterio1, Criterio2)
'    If teserror.NumError <> TESnoerror Then
'        Screen.MousePointer = vbNormal
'        basErrores.TratarError teserror
'        Exit Sub
'    End If
'
'    RegistrarAccion ACCPlaReuMod, "Reunion:" & frmREU.oReuSeleccionada.Fecha & " Orden1:" & sdbcCrit1 & " Orden2:" & sdbcCrit2
'
'    frmREU.oReuSeleccionada.FinalizarBloqueoEnBD
'    frmREU.Accion = ACCGestReuCon
'    frmREU.cmdRestaurarGest_Click
'    Screen.MousePointer = vbNormal
'
'    Unload Me
    Aceptar = True
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
'    frmREU.oReuSeleccionada.FinalizarBloqueoEnBD
'    frmREU.Accion = ACCGestReuCon
'    Unload Me
    ReDim ValoresOrdenacion(0)
    ValoresOrdenacion(0) = False
    Me.Hide
End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_REUORD, Idioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 4)
        For i = 1 To 4
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        cmdAceptar.Caption = Ador(0).Value '5
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Caption = Ador(0).Value '7
        Ador.MoveNext
        Label1.Caption = Ador(0).Value
        Ador.MoveNext
        Label2.Caption = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Load()
'    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
'    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    CargarRecursos
    
    sdbcCrit1.AddItem m_ParamGen.gsDEN_GMN1
    sdbcCrit1.AddItem sIdioma(1)
    sdbcCrit1.AddItem sIdioma(2)
    sdbcCrit1.AddItem sIdioma(3)
    sdbcCrit1.AddItem sIdioma(4)
    
    sdbcCrit2.AddItem m_ParamGen.gsDEN_GMN1
    sdbcCrit2.AddItem sIdioma(1)
    sdbcCrit2.AddItem sIdioma(2)
    sdbcCrit2.AddItem sIdioma(3)
    sdbcCrit2.AddItem sIdioma(4)
    
    Select Case m_ParamGen.giCRITEORDREU1
        Case TipoOrdenacionReuniones.OrdPorMatPrimerNivel
                sdbcCrit1 = m_ParamGen.gsDEN_GMN1
        Case TipoOrdenacionReuniones.OrdPorPersonas
                sdbcCrit1 = sIdioma(1)
        Case TipoOrdenacionReuniones.OrdPorDeptos
                sdbcCrit1 = sIdioma(2)
        Case TipoOrdenacionReuniones.OrdPorResponsable
                sdbcCrit1 = sIdioma(3)
        Case TipoOrdenacionReuniones.OrdPorTipoReu
                sdbcCrit1 = sIdioma(4)
    End Select
    
    Select Case m_ParamGen.giCRITEORDREU2
        Case TipoOrdenacionReuniones.OrdPorMatPrimerNivel
                sdbcCrit2 = m_ParamGen.gsDEN_GMN1
        Case TipoOrdenacionReuniones.OrdPorPersonas
                sdbcCrit2 = sIdioma(1)
        Case TipoOrdenacionReuniones.OrdPorDeptos
                sdbcCrit2 = sIdioma(2)
        Case TipoOrdenacionReuniones.OrdPorResponsable
                sdbcCrit2 = sIdioma(3)
        Case TipoOrdenacionReuniones.OrdPorTipoReu
                sdbcCrit2 = sIdioma(4)
    End Select
End Sub

Private Sub sdbcCrit1_InitColumnProps()
    sdbcCrit1.DataFieldList = "Column 0"
    sdbcCrit1.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCrit2_InitColumnProps()
    sdbcCrit2.DataFieldList = "Column 0"
    sdbcCrit2.DataFieldToDisplay = "Column 0"
End Sub
