VERSION 5.00
Begin VB.Form frmUSUPWDCamb 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cambio de contrase�a"
   ClientHeight    =   2670
   ClientLeft      =   3480
   ClientTop       =   6750
   ClientWidth     =   4590
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmUSUPWDCamb.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2670
   ScaleWidth      =   4590
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picButtons 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   4590
      TabIndex        =   10
      Top             =   2175
      Width           =   4590
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   2370
         TabIndex        =   3
         Top             =   90
         Width           =   1215
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   1110
         TabIndex        =   2
         Top             =   90
         Width           =   1155
      End
   End
   Begin VB.PictureBox picNoADM 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2085
      Left            =   60
      ScaleHeight     =   2025
      ScaleWidth      =   4395
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   60
      Width           =   4455
      Begin VB.TextBox txtContrActual 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         Locked          =   -1  'True
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   480
         Width           =   2100
      End
      Begin VB.TextBox txtContrNuevaConf 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   1590
         Width           =   2100
      End
      Begin VB.TextBox txtContrNueva 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   0
         Top             =   1170
         Width           =   2100
      End
      Begin VB.TextBox txtUsuActual 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         Locked          =   -1  'True
         MaxLength       =   20
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   60
         Width           =   2100
      End
      Begin VB.Label lblPwdAct 
         BackColor       =   &H00808000&
         Caption         =   "Contrase�a actual:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   540
         Width           =   2085
      End
      Begin VB.Label lblConf1 
         BackColor       =   &H00808000&
         Caption         =   "Confirmaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1650
         Width           =   2085
      End
      Begin VB.Label lblPwdNue 
         BackColor       =   &H00808000&
         Caption         =   "Nueva contrase�a:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   1230
         Width           =   2085
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         BorderWidth     =   2
         X1              =   120
         X2              =   4260
         Y1              =   990
         Y2              =   990
      End
      Begin VB.Label lblUsuAct 
         BackColor       =   &H00808000&
         Caption         =   "Usuario actual:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   120
         Width           =   2085
      End
   End
End
Attribute VB_Name = "frmUSUPWDCamb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public UsuarioSummit As CUsuario
Public Mensajes As CMensajes
Public AceptarClicked As Boolean

Private sConfUsu As String
Private sPwdAct As String
Private sConfPwd As String
Private sUsuario As String
Private oParamGen As ParametrosGenerales
'Multilenguaje
Private sIdiTitulo As String

Public Property Let ParamGen(ByVal vNewValue As Variant)
    oParamGen = vNewValue
End Property

''' <summary>
''' Evento que se genera al hacer click sobre el bot�n de aceptar del formulario de cambio de contrase�a, comprobando qe cumple con los requisitos y que no es erronea
''' </summary>
''' <remarks>
''' Llamadas desde: Autom�tica, siempre que se haga click sobre el bot�n de aceptar del formulario
''' Tiempo m�ximo: 0,3 seg
''' </remarks>
Private Sub cmdAceptar_Click()
    Dim sPwdNuevo As String
    Dim fecpwd As Date
           
    'Compruebo si hay Autenticaci�n windows en GS o WEB
    ' Adem�s hay que comprobar que tampoco haya autenticaci�n LDAP
    If Not ((oParamGen.giWinSecurity = Windows Or oParamGen.giWinSecurity = LDAP) _
    And (oParamGen.giWinSecurityWeb = Windows Or oParamGen.giWinSecurity = LDAP)) Then
        If txtContrNueva.Text = txtContrActual.Text Then
            MsgBox Mensajes.CargarTextoMensaje(556), vbCritical, "FULLSTEP"
            Exit Sub
        End If
        If txtContrNueva.Text <> txtContrNuevaConf.Text Then
            MsgBox Mensajes.CargarTextoMensaje(5) & " " & sConfPwd, vbExclamation, "FULLSTEP"
            Exit Sub
        End If
        sPwdNuevo = txtContrNueva.Text
    Else
        sPwdNuevo = ""
    End If
    
    fecpwd = Now
    
    Dim oWebSvc As FSGSLibrary.CWebService
    Set oWebSvc = New FSGSLibrary.CWebService
    If oWebSvc.LlamarWebServiceCambioPWD(oParamGen, 0, 0, UsuarioSummit.Cod, UsuarioSummit.PWDDes, UsuarioSummit.Cod, sPwdNuevo, UsuarioSummit.fecpwd, Mensajes, fecpwd) Then
        UsuarioSummit.PWDDes = txtContrNueva.Text
        UsuarioSummit.fecpwd = fecpwd
        Dim sPassEncript As String
        sPassEncript = FSGSLibrary.EncriptarAES(UsuarioSummit.Cod, UsuarioSummit.PWDDes, True, UsuarioSummit.fecpwd, 1, TIpoDeUsuario.Persona)
        UsuarioSummit.Pwd = sPassEncript
        UsuarioSummit.CuentaDebeCambiarPwd = False
        
        AceptarClicked = True
        Me.Hide
    Else
        txtContrNuevaConf.Text = ""
        txtContrNueva.Text = ""
    End If
    Set oWebSvc = Nothing
End Sub

Private Sub cmdCancelar_Click()
    AceptarClicked = False
    Me.Hide
End Sub

Private Sub Form_Load()
    CargarRecursos
            
    Me.Height = 3075
    txtUsuActual.Text = UsuarioSummit.Cod
    txtContrActual.Text = UsuarioSummit.PWDDes

    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_USUCAMB, Idioma)
    
    If Not Ador Is Nothing Then
        lblPwdAct.Caption = Ador(0).Value
        Ador.MoveNext
        lblPwdNue.Caption = Ador(0).Value
        Ador.MoveNext
        lblConf1.Caption = Ador(0).Value
        Ador.MoveNext
        lblUsuAct.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sUsuario = Ador(0).Value
        Ador.MoveNext
        sConfUsu = Ador(0).Value
        Ador.MoveNext
        sConfPwd = Ador(0).Value
        Ador.MoveNext
        sPwdAct = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiTitulo = Ador(0).Value

        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

