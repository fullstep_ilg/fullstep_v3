VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPETHIST 
   BackColor       =   &H80000004&
   Caption         =   "DHist�rico de notificaciones:"
   ClientHeight    =   3000
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9150
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPETHIST.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3000
   ScaleWidth      =   9150
   StartUpPosition =   2  'CenterScreen
   Begin SSDataWidgets_B.SSDBGrid sdbgPeticiones 
      Height          =   2895
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9120
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   4
      DividerType     =   0
      DividerStyle    =   0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      BalloonHelp     =   0   'False
      MaxSelectedRows =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   1799
      Columns(0).Caption=   "Fecha"
      Columns(0).Name =   "FECHA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5530
      Columns(1).Caption=   "Contacto"
      Columns(1).Name =   "CONTACTO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   5530
      Columns(2).Caption=   "Tipo"
      Columns(2).Name =   "TIPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1958
      Columns(3).Caption=   "V�a"
      Columns(3).Name =   "VIA"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   16087
      _ExtentY        =   5106
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPETHIST"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Raiz As cRaiz
Public Mensajes As CMensajes

Private m_bDescargarFrm As Boolean
Private sIdiVia(5) As String
Private sIdiTipo(5) As String
Private m_bActivado As Boolean
Private m_bUnload As Boolean
Private m_sMsgError As String

Private Sub Form_Load()
    If Not Raiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    m_bActivado = False
    m_bUnload = False
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Me.Width = 8500
    Me.Height = 4620
    
    DoEvents
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
   If Err.Number <> 0 Then
      m_bDescargarFrm = Raiz.TratarError("Formulario", "frmPETHIST", "Form_Load", Err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    
    ''' * Objetivo: Adecuar los controles
    With sdbgPeticiones
        .Width = Me.Width - 120
        .Height = Me.Height - 400
        .Columns("FECHA").Width = (sdbgPeticiones.Width - 570) * 0.2
        .Columns("CONTACTO").Width = (sdbgPeticiones.Width - 570) * 0.35
        .Columns("TIPO").Width = (sdbgPeticiones.Width - 570) * 0.35
        .Columns("VIA").Width = (sdbgPeticiones.Width - 570) * 0.1
    End With
End Sub

Private Sub CargarRecursos()
    Dim ADOR As ADOR.Recordset
    Dim i As Integer

    On Error Resume Next
    
    Set ADOR = GestorIdiomas.DevolverTextosDelModulo(FRM_PETHIST, Idioma)
    
    If Not ADOR Is Nothing Then
        Me.Caption = ADOR(0).Value
        ADOR.MoveNext
        Me.sdbgPeticiones.Columns("FECHA").Caption = ADOR(0).Value
        ADOR.MoveNext
        Me.sdbgPeticiones.Columns("CONTACTO").Caption = ADOR(0).Value
        ADOR.MoveNext
        Me.sdbgPeticiones.Columns("TIPO").Caption = ADOR(0).Value
        ADOR.MoveNext
        Me.sdbgPeticiones.Columns("VIA").Caption = ADOR(0).Value
        ADOR.MoveNext
        sIdiTipo(0) = ADOR(0).Value
        ADOR.MoveNext
        sIdiTipo(1) = ADOR(0).Value
        ADOR.MoveNext
        sIdiTipo(2) = ADOR(0).Value
        ADOR.MoveNext
        sIdiTipo(3) = ADOR(0).Value
        ADOR.MoveNext
        sIdiVia(0) = ADOR(0).Value
        ADOR.MoveNext
        sIdiVia(1) = ADOR(0).Value
        ADOR.MoveNext
        sIdiVia(2) = ADOR(0).Value
        ADOR.MoveNext
        sIdiTipo(4) = ADOR(0).Value
        
        ADOR.Close
    End If
    
    Set ADOR = Nothing
End Sub

Public Sub AnyadirPeticion(ByVal Fecha As Variant, ByVal Contacto As Variant, ByVal Tipo As tipoNotificacion, ByVal via As Variant)
    Dim sTipo As String
    Dim sVia As String

    If Not Raiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case Tipo
        Case 0:
            sTipo = sIdiTipo(0)
        Case 1:
            sTipo = sIdiTipo(1)
        Case 2:
            sTipo = sIdiTipo(4)
        Case 3:
            sTipo = sIdiTipo(2)
        Case 4:
            sTipo = sIdiTipo(3)
    End Select
            
    Select Case via
        Case 0:
            sVia = sIdiVia(0)
        Case 1:
            sVia = sIdiVia(1)
        Case 2:
            sVia = sIdiVia(2)
    End Select
        
    sdbgPeticiones.AddItem Fecha & Chr(m_lSeparador) & Contacto & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & sVia

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
   If Err.Number <> 0 Then
      m_bDescargarFrm = Raiz.TratarError("Formulario", "frmPETHIST", "AnyadirPeticion", Err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not Raiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        Mensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub
    End If

    Exit Sub
ERROR:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmPETHIST", "Form_Unload", Err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


