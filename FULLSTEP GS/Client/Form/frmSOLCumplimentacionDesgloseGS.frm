VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSOLCumplimentacionDesgloseGS 
   Caption         =   "Form1"
   ClientHeight    =   5055
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   8850
   Icon            =   "frmSOLCumplimentacionDesgloseGS.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5055
   ScaleWidth      =   8850
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   4695
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8610
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   8
      stylesets.count =   5
      stylesets(0).Name=   "Bloqueo"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSOLCumplimentacionDesgloseGS.frx":0CB2
      stylesets(0).AlignmentPicture=   4
      stylesets(1).Name=   "Calculado"
      stylesets(1).BackColor=   16766421
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSOLCumplimentacionDesgloseGS.frx":0E33
      stylesets(1).AlignmentPicture=   1
      stylesets(2).Name=   "Gris"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSOLCumplimentacionDesgloseGS.frx":0E4F
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmSOLCumplimentacionDesgloseGS.frx":0E6B
      stylesets(4).Name=   "Amarillo"
      stylesets(4).BackColor=   12648447
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmSOLCumplimentacionDesgloseGS.frx":0E87
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   106
      Columns.Count   =   8
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(0).HeadStyleSet=   "Bloqueo"
      Columns(1).Width=   1931
      Columns(1).Name =   "ATRIBUTO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777160
      Columns(2).Width=   6429
      Columns(2).Caption=   "DDato"
      Columns(2).Name =   "DATO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "TIPO"
      Columns(3).Name =   "TIPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   2
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "CAMPO_GS"
      Columns(4).Name =   "CAMPO_GS"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   2
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "SUBTIPO"
      Columns(5).Name =   "SUBTIPO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   2
      Columns(5).FieldLen=   256
      Columns(6).Width=   1455
      Columns(6).Caption=   "DEsc."
      Columns(6).Name =   "ESCRITURA"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   11
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(7).Width=   1376
      Columns(7).Caption=   "DObl."
      Columns(7).Name =   "OBLIGATORIO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   11
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      _ExtentX        =   15187
      _ExtentY        =   8281
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSOLCumplimentacionDesgloseGS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables privadas
Private m_oCumplimentaciones  As CSolCumplimentacionesGs
Private m_bErrorCumplimentaciones As Boolean

'Variables p�blicas
Public g_lCampoPadre As Long
Public g_sCampoPadre As String
Public g_lSolicitudId As Long

'Clases de Client
Public Raiz As CRaiz
Public Mensajes As CMensajes

'Variables de idiomas
Public Idioma As String
Public GestorIdiomas As CGestorIdiomas

Private m_ParamGen As ParametrosGenerales

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_SOL_CUMPLIMENTACION_GS, Idioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        sdbgCampos.Columns("DATO").Caption = Ador(0).Value
        Ador.MoveNext
        sdbgCampos.Columns("ESCRITURA").Caption = Ador(0).Value
        Ador.MoveNext
        sdbgCampos.Columns("OBLIGATORIO").Caption = Ador(0).Value
        Ador.MoveNext
        Me.Caption = Me.Caption & " " & Ador(0).Value & " " & g_sCampoPadre
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()

    PonerFieldSeparator Me

    CargarRecursos

    Set m_oCumplimentaciones = Raiz.Generar_CSolCumplimentacionesGs
    
    'Las columnas todas locked excepto Escritura/Obligatorio
    
    CargarCumplimentacionGS
    
End Sub

Private Sub CargarCumplimentacionGS()
    Dim oCumplimentacion As CSolCumplimentacionGs
    
    sdbgCampos.RemoveAll
        
    m_oCumplimentaciones.CargarCumplimentacion g_lSolicitudId, False, , g_lCampoPadre
    If Not m_oCumplimentaciones Is Nothing Then
        For Each oCumplimentacion In m_oCumplimentaciones
            sdbgCampos.AddItem oCumplimentacion.IdCampo & Chr(m_lSeparador) & oCumplimentacion.CodAtrib & Chr(m_lSeparador) & oCumplimentacion.Campo.Denominaciones.Item(Idioma).Den _
                & Chr(m_lSeparador) & oCumplimentacion.Campo.TipoPredef & Chr(m_lSeparador) & oCumplimentacion.Campo.CampoGS & Chr(m_lSeparador) & oCumplimentacion.Campo.Tipo & Chr(m_lSeparador) _
                & IIf(oCumplimentacion.ESCRITURA, 1, 0) & Chr(m_lSeparador) & IIf(oCumplimentacion.Obligatorio, 1, 0)
        Next
    End If
End Sub

Private Sub Form_Resize()
    sdbgCampos.Width = Me.Width - 520
    sdbgCampos.Columns("DATO").Width = sdbgCampos.Width - sdbgCampos.Columns("ATRIBUTO").Width - sdbgCampos.Columns("ESCRITURA").Width - sdbgCampos.Columns("OBLIGATORIO").Width - 580
    sdbgCampos.Height = Me.Height - 900
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set m_oCumplimentaciones = Nothing
    m_bErrorCumplimentaciones = False
    
    g_lCampoPadre = 0
    g_sCampoPadre = ""
    g_lSolicitudId = 0
End Sub

Private Sub sdbgCampos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim oCumplimentacion As CSolCumplimentacionGs
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit

    If sdbgCampos.Columns("ID").Value <> "" And sdbgCampos.Columns("ID").Value <> "0" Then
        Set oCumplimentacion = m_oCumplimentaciones.Item(CStr(sdbgCampos.Columns("ID").Value))
        If Not oCumplimentacion Is Nothing Then
            If oCumplimentacion.ESCRITURA = CBool(sdbgCampos.Columns("ESCRITURA").Value) And _
            oCumplimentacion.Obligatorio = CBool(sdbgCampos.Columns("OBLIGATORIO").Value) Then
                'No hay cambios com lo cual no grabamos nada
                Exit Sub
            End If
            
            oCumplimentacion.ESCRITURA = CBool(sdbgCampos.Columns("ESCRITURA").Value)
            oCumplimentacion.Obligatorio = CBool(sdbgCampos.Columns("OBLIGATORIO").Value)
            
            Set oIBaseDatos = oCumplimentacion
            teserror = oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                If teserror.NumError = TESDatoEliminado Then teserror.Arg1 = sdbgCampos.Columns("DATO").Value
                TratarError teserror, Mensajes, m_ParamGen
                sdbgCampos.CancelUpdate
                oCumplimentacion.ESCRITURA = CBool(sdbgCampos.Columns("ESCRITURA").Value)
                oCumplimentacion.Obligatorio = CBool(sdbgCampos.Columns("OBLIGATORIO").Value)
                If Me.Visible Then sdbgCampos.SetFocus
                Set oCumplimentacion = Nothing
                Set oIBaseDatos = Nothing
                m_bErrorCumplimentaciones = True
                Exit Sub
            End If
            
            Set oCumplimentacion = Nothing
            Set oIBaseDatos = Nothing
            m_bErrorCumplimentaciones = False
        End If
    End If
End Sub

Private Sub sdbgCampos_Change()
    If sdbgCampos.Col < 0 Then Exit Sub
    
    With sdbgCampos
        Select Case .Columns(.Col).Name
        Case "ESCRITURA"
            If .Columns("CAMPO_GS").Value = TipoCampoSC.PrecioUnitarioAdj Or _
            .Columns("CAMPO_GS").Value = TipoCampoSC.ProveedorAdj Or _
            .Columns("CAMPO_GS").Value = TipoCampoSC.CantidadAdj Or _
            .Columns("CAMPO_GS").Value = TipoCampoSC.TotalLineaAdj Or _
            .Columns("CAMPO_GS").Value = TipoCampoSC.TotalLineaPreadj Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.NumSolicitERP Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.ImporteSolicitudesVinculadas Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.DesgloseActividad Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.Moneda Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.Activo Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.CentroCoste Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.Factura Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.AnyoImputacion Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.PartidaPresupuestaria Or _
            .Columns("CAMPO_GS").Value = TipoCampoGS.CodComprador Or _
            .Columns("TIPO").Value = TipoCampoPredefinido.Calculado Or _
            .Columns("TIPO").Value = TipoCampoPredefinido.Servicio Or _
            .Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                .Columns("ESCRITURA").Value = False
                .DataChanged = False
                Exit Sub
            End If
                        
            If Not .Columns("ESCRITURA").Value Then
                .Columns("OBLIGATORIO").Value = False
            End If
                
        Case "OBLIGATORIO"
            If Not .Columns("OBLIGATORIO").Value Then
            Else
                If Not .Columns("ESCRITURA").Value Then
                    .Columns("OBLIGATORIO").Value = False
                    .DataChanged = False
                    Exit Sub
                End If
            End If
        End Select
    
        If .DataChanged Then .Update
    End With
End Sub

Private Sub sdbgCampos_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgCampos.DataChanged Then
                sdbgCampos.Update
                If m_bErrorCumplimentaciones Then
                    Exit Sub
                End If
            End If
    End Select
End Sub

Private Sub sdbgCampos_LostFocus()
    If sdbgCampos.DataChanged Then
        sdbgCampos.Update
    End If
End Sub

Private Sub sdbgCampos_RowLoaded(ByVal Bookmark As Variant)
    If sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
        sdbgCampos.Columns("DATO").CellStyleSet "Amarillo"
    ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
        sdbgCampos.Columns("DATO").CellStyleSet "Calculado"
    Else
        sdbgCampos.Columns("DATO").CellStyleSet ""
    End If
    
    'Si el campo es de Desglose Vinculado
    If sdbgCampos.Columns("CAMPO_GS").CellValue(Bookmark) = idsficticios.DesgloseVinculado Then
        sdbgCampos.Columns("ESCRITURA").CellStyleSet "Gris"
    End If
End Sub

Public Property Get ParamGenerales() As ParametrosGenerales
    ParamGenerales = m_ParamGen
End Property

Public Property Let ParamGenerales(ByRef ParamGen As ParametrosGenerales)
    m_ParamGen = ParamGen
End Property


