VERSION 5.00
Begin VB.Form frmFormCampoDescr 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3660
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7470
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormCampoDescr.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3660
   ScaleWidth      =   7470
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtObs 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   3105
      Left            =   60
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   90
      Width           =   7350
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   345
      Left            =   2490
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   3270
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   3690
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3270
      Visible         =   0   'False
      Width           =   1005
   End
End
Attribute VB_Name = "frmFormCampoDescr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public g_bModif As Boolean
Public g_lMaxLength As Integer
Public Ok As Boolean

Private Sub cmdAceptar_Click()
    Ok = True
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.Width = 7560
    
    If g_bModif Then
        txtObs.Locked = False
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        Me.Height = 4140
        If g_lMaxLength > 0 Then txtObs.MaxLength = g_lMaxLength
    Else
        txtObs.Locked = True
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        Me.Height = 3740
    End If
    
    CargarRecursos
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = GestorIdiomas.DevolverTextosDelModulo(FRM_ADJ_COMENT, Idioma)
    
    If Not ador Is Nothing Then
        ador.MoveNext
        cmdAceptar.Caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.Caption = ador(0).Value
        
        ador.Close
    End If
    
    Set ador = Nothing
End Sub
