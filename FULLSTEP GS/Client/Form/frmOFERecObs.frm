VERSION 5.00
Begin VB.Form frmOFERecObs 
   BackColor       =   &H00808000&
   Caption         =   "Comentario de precio"
   ClientHeight    =   3630
   ClientLeft      =   3600
   ClientTop       =   2670
   ClientWidth     =   7545
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmOFERecObs.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   7545
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   3780
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3240
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   345
      Left            =   2580
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   3240
      Width           =   1005
   End
   Begin VB.TextBox txtObs 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   3105
      Left            =   90
      MaxLength       =   2000
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   30
      Width           =   7350
   End
End
Attribute VB_Name = "frmOFERecObs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Public g_bModif As Boolean
Public AceptarClicked As Boolean
Public g_bInvitado As Boolean

Private Sub cmdAceptar_Click()
    AceptarClicked = True
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    AceptarClicked = False
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.Width = 7665
    
    If g_bModif Then
        txtObs.Locked = False
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        Me.Height = 4020
    Else
        txtObs.Locked = True
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        Me.Height = 3600
    End If
End Sub

Public Function HabilitarBotonesInvitado()
    ' funci�n para ocultar/mostrar los botones si se es invitado

    If g_bInvitado Then
       cmdAceptar.Visible = False
       txtObs.Enabled = False
    Else
       cmdAceptar.Visible = True
       txtObs.Enabled = True
    End If
End Function

