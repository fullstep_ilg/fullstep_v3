VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmVARCalMaterial 
   BackColor       =   &H00808000&
   Caption         =   "Materiales por comprador"
   ClientHeight    =   8850
   ClientLeft      =   5790
   ClientTop       =   2265
   ClientWidth     =   8355
   Icon            =   "frmVARCalMaterial.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8850
   ScaleWidth      =   8355
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picMatQa 
      Align           =   1  'Align Top
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   450
      Left            =   0
      ScaleHeight     =   450
      ScaleWidth      =   8355
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   0
      Width           =   8355
      Begin VB.Label lblMaterialQA 
         BackColor       =   &H00808000&
         Caption         =   "Material de QA asociado:"
         ForeColor       =   &H8000000E&
         Height          =   240
         Left            =   120
         TabIndex        =   7
         Top             =   120
         Width           =   8130
      End
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   210
      Left            =   45
      Picture         =   "frmVARCalMaterial.frx":014A
      ScaleHeight     =   210
      ScaleWidth      =   195
      TabIndex        =   4
      Top             =   1590
      Visible         =   0   'False
      Width           =   195
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6960
      Top             =   3720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalMaterial.frx":0344
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalMaterial.frx":03F4
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalMaterial.frx":0848
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalMaterial.frx":0909
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalMaterial.frx":09C9
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalMaterial.frx":0A79
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalMaterial.frx":0B43
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalMaterial.frx":0BFC
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalMaterial.frx":0CAC
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   550
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   8355
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   8295
      Width           =   8355
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1485
         TabIndex        =   3
         Top             =   100
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2640
         TabIndex        =   2
         Top             =   100
         Width           =   1005
      End
   End
   Begin MSComctlLib.TreeView tvwEstrMatMod 
      Height          =   4725
      Left            =   105
      TabIndex        =   0
      Top             =   3600
      Width           =   8100
      _ExtentX        =   14288
      _ExtentY        =   8334
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwMaterialQA 
      Height          =   3045
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   8100
      _ExtentX        =   14288
      _ExtentY        =   5371
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmVARCalMaterial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Raiz As CRaiz
Public Idioma As String
Public g_oVariableCalidad As CVariableCalidad
Public g_bExiste As Boolean
Public Mensajes As CMensajes
Public GestorSeguridad As CGestorSeguridad
Public UsuCod As String

Private m_vLongitudesDeCodigos As LongitudesDeCodigos
Private m_oParametrosGenerales As ParametrosGenerales

'Variable para no ejecutar el node check si se ha disparado el mousedown
Private bMouseDown As Boolean
Private SelNode As MSComctlLib.Node
Private m_bCargarTree As Boolean

Private m_oMaterialesQA As CMaterialesQA
Private m_oVarCalMaterialesQA As CMaterialesQA

Private m_sMaterialQA As String
Private m_sMaterialGS As String
Private m_bDescargarFrm As Boolean
Private m_bActivado As Boolean

Public Property Get gLongitudesDeCodigos() As LongitudesDeCodigos
    gLongitudesDeCodigos = m_vLongitudesDeCodigos
End Property

Public Property Let gLongitudesDeCodigos(ByRef vParametrosGenerales As LongitudesDeCodigos)
    m_vLongitudesDeCodigos = vParametrosGenerales
End Property

Public Property Get ParametrosGenerales() As ParametrosGenerales
    ParametrosGenerales = m_oParametrosGenerales
End Property

Public Property Let ParametrosGenerales(ByRef vNewValue As ParametrosGenerales)
    m_oParametrosGenerales = vNewValue
End Property

Private Sub cmdAceptar_Click()
    Dim oIMaterial As IMaterialAsignado
    Dim teserror As TipoErrorSummit
    Dim bSeguir As Boolean
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    teserror.NumError = TESnoerror
    
    Screen.MousePointer = vbHourglass
    
    'Si es una var. multimaterial comprobar que no tiene en su f�rmula objetivos y/o suelos
    bSeguir = True
    If m_oVarCalMaterialesQA.Count = 0 Or m_oVarCalMaterialesQA.Count > 1 Then
        If g_oVariableCalidad.TieneObjetivosSuelosEnFormula Then
            Mensajes.NoPermitidoVarMultimatConObjSueloEnFormula
            bSeguir = False
        End If
    End If
    
    If bSeguir Then
        Set g_oVariableCalidad.MaterialesQA = m_oVarCalMaterialesQA
        If g_oVariableCalidad.MaterialesQA.Count = 0 Then
            g_oVariableCalidad.MaterialQA = Null
        Else
            g_oVariableCalidad.MaterialQA = 1
        End If
      
        If g_bExiste Then
            Set oIMaterial = g_oVariableCalidad
            teserror = oIMaterial.AsignarMaterial
              
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror, Mensajes, m_oParametrosGenerales
                Exit Sub
            End If
                    
            RegistrarAccion AccionesSummit.ACCVarCalAsigMat, "VarID: " & CStr(g_oVariableCalidad.Id) & " Nivel: " & CStr(g_oVariableCalidad.Nivel), UsuCod, m_oParametrosGenerales.gbACTIVLOG, GestorSeguridad
        End If
        
        Me.Hide
    End If
    
    Screen.MousePointer = vbNormal
  
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmVARCalMaterial", "cmdAceptar_Click", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub cmdCancelar_Click()
  Me.Hide
End Sub

Private Sub Form_Activate()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Me.Hide
        Exit Sub
    End If
    
    If Not m_bActivado Then m_bActivado = True

    If m_bCargarTree Then
        GenerarEstructuraMaterialesQA
        GenerarEstructuraMatAsignable
        MaterialSeleccionado
        DoEvents
        m_bCargarTree = False
    End If
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmVARCalMaterial", "Form_Activate", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    m_bDescargarFrm = False
    m_bActivado = False
    m_bCargarTree = True
    
    CargarRecursos
        
    If Not g_oVariableCalidad.Denominaciones Is Nothing Then
        If Not g_oVariableCalidad.Denominaciones.Item(Idioma) Is Nothing Then
            Me.Caption = Me.Caption & " " & g_oVariableCalidad.Denominaciones.Item(Idioma).Den
        End If
    End If
    
    'Cargar materiales de QA
    Set m_oMaterialesQA = Raiz.Generar_CMaterialesQA
    m_oMaterialesQA.CargarTodosLosMateriales , IIf(g_oVariableCalidad.Subtipo = CalidadSubtipo.CalCertificado, g_oVariableCalidad.Origen, 0)
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmVARCalMaterial", "Form_Load", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub Form_Resize()
    If Me.WindowState = vbMinimized Then Exit Sub
    If Me.Width < 5500 Then Me.Width = 5500
    If Me.Height < 4500 Then Me.Height = 4500
    
    tvwMaterialQA.Width = Me.ScaleWidth - (2 * tvwMaterialQA.Left)
    tvwMaterialQA.Height = (Me.ScaleHeight - picMatQa.Height - picEdit.Height - 100) / 3
    
    tvwEstrMatMod.Top = tvwMaterialQA.Top + tvwMaterialQA.Height + 100
    tvwEstrMatMod.Height = 2 * (Me.ScaleHeight - picMatQa.Height - picEdit.Height - 100) / 3
    tvwEstrMatMod.Width = Me.ScaleWidth - (2 * tvwEstrMatMod.Left)
    
    cmdAceptar.Left = (Me.ScaleWidth / 2) - (cmdAceptar.Width / 2) - 300
    cmdCancelar.Left = (Me.ScaleWidth / 2) + 300
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oMaterialesQA = Nothing
End Sub

''' <summary>Genera la estructura del �rbol de materiales de GS correspondiente a los materiales de QA asignados</summary>
''' <remarks>Llamada desde: Form_Activate, tvwMaterialQA_NodeCheck</remarks>

Private Sub GenerarEstructuraMatAsignable()
    Dim oMatQA As CMaterialQA
    Dim oGMN1s As CGruposMatNivel1
    Dim oGMN2s As CGruposMatNivel2
    Dim oGMN3s As CGruposMatNivel3
    Dim oGMN4s As CGruposMatNivel4
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim nodx As MSComctlLib.Node
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    LockWindowUpdate Me.hwnd
    
    Screen.MousePointer = vbHourglass
    
    With tvwEstrMatMod
        .Nodes.Clear
    
        Set nodx = .Nodes.Add(, , "Raiz", m_sMaterialGS, "Raiz")
        nodx.Tag = "Raiz"
        nodx.Expanded = True
    
        'Marcar los materiales de GS correspondientes a todas las variables de calidad relacionadas con la variable
        'Recorro los materiales de QA
        '****
        'Estas colecciones s�lo las creo para crear el �rbol
        'Se van a mostrar s�lo los materiales de Gs que se correspondan con los materiales de QA seleccionados, no todos
        'Para saber si un nodo se corresponde (con check en el �rbol) o s�lo se ha creado para mostrar sus hijos utilizo la prop. Indice
        '****
        Set oGMN1s = Raiz.Generar_CGruposMatNivel1
        Set oGMN2s = Raiz.Generar_CGruposMatNivel2
        Set oGMN3s = Raiz.Generar_CGruposMatNivel3
        Set oGMN4s = Raiz.Generar_CGruposMatNivel4
        'Arrays para ordenaci�n
        Dim arGMN1() As String
        Dim arGMN2() As String
        Dim arGMN3() As String
        Dim arGMN4() As String
        For Each oMatQA In m_oVarCalMaterialesQA
            For Each oGMN1 In oMatQA.Col_GMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                If oGMN1s.Item(scod1) Is Nothing Then
                    oGMN1s.Add oGMN1.Cod, oGMN1.Den
                    ReDim Preserve arGMN1(0 To oGMN1s.Count - 1)
                    arGMN1(oGMN1s.Count - 1) = scod1
                End If
                oGMN1s.Item(scod1).Indice = 1
            Next
        
            For Each oGMN2 In oMatQA.Col_GMN2
                scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
                If oGMN1s.Item(scod1) Is Nothing Then
                    oGMN1s.Add oGMN2.GMN1Cod, oGMN2.GMN1Den
                    ReDim Preserve arGMN1(0 To oGMN1s.Count - 1)
                    arGMN1(oGMN1s.Count - 1) = scod1
                End If
                
                scod2 = oGMN2.Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                If oGMN2s.Item(scod1 & scod2) Is Nothing Then
                    oGMN2s.Add oGMN2.GMN1Cod, oGMN2.GMN1Den, oGMN2.Cod, oGMN2.Den
                    ReDim Preserve arGMN2(0 To oGMN2s.Count - 1)
                    arGMN2(oGMN2s.Count - 1) = scod1 & scod2
                End If
                oGMN2s.Item(scod1 & scod2).Indice = 1
            Next
        
            For Each oGMN3 In oMatQA.Col_GMN3
                scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
                If oGMN1s.Item(scod1) Is Nothing Then
                    oGMN1s.Add oGMN3.GMN1Cod, oGMN3.GMN1Den
                    ReDim Preserve arGMN1(0 To oGMN1s.Count - 1)
                    arGMN1(oGMN1s.Count - 1) = scod1
                End If
                
                scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
                If oGMN2s.Item(scod1 & scod2) Is Nothing Then
                    oGMN2s.Add oGMN3.GMN1Cod, oGMN3.GMN1Den, oGMN3.GMN2Cod, oGMN3.GMN2Den
                    ReDim Preserve arGMN2(0 To oGMN2s.Count - 1)
                    arGMN2(oGMN2s.Count - 1) = scod1 & scod2
                End If

                scod3 = oGMN3.Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                If oGMN3s.Item(scod1 & scod2 & scod3) Is Nothing Then
                    oGMN3s.Add oGMN3.GMN1Cod, oGMN3.GMN2Cod, oGMN3.Cod, oGMN3.GMN1Den, oGMN3.GMN2Den, oGMN3.Den
                    ReDim Preserve arGMN3(0 To oGMN3s.Count - 1)
                    arGMN3(oGMN3s.Count - 1) = scod1 & scod2 & scod3
                End If
                oGMN3s.Item(scod1 & scod2 & scod3).Indice = 1
            Next
        
            For Each oGMN4 In oMatQA.Col_GMN4
                scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                If oGMN1s.Item(scod1) Is Nothing Then
                    oGMN1s.Add oGMN4.GMN1Cod, oGMN4.GMN1Den
                    ReDim Preserve arGMN1(0 To oGMN1s.Count - 1)
                    arGMN1(oGMN1s.Count - 1) = scod1
                End If

                scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                If oGMN2s.Item(scod1 & scod2) Is Nothing Then
                    oGMN2s.Add oGMN4.GMN1Cod, oGMN4.GMN1Den, oGMN4.GMN2Cod, oGMN4.GMN2Den
                    ReDim Preserve arGMN2(0 To oGMN2s.Count - 1)
                    arGMN2(oGMN2s.Count - 1) = scod1 & scod2
                End If

                scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                If oGMN3s.Item(scod1 & scod2 & scod3) Is Nothing Then
                    oGMN3s.Add oGMN4.GMN1Cod, oGMN4.GMN2Cod, oGMN4.GMN3Cod, oGMN4.GMN1Den, oGMN4.GMN2Den, oGMN4.GMN3Den
                    ReDim Preserve arGMN3(0 To oGMN3s.Count - 1)
                    arGMN3(oGMN3s.Count - 1) = scod1 & scod2 & scod3
                End If

                scod4 = oGMN4.Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                If oGMN4s.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                    oGMN4s.Add oGMN4.GMN1Cod, oGMN4.GMN2Cod, oGMN4.GMN3Cod, oGMN4.GMN1Den, oGMN4.GMN2Den, oGMN4.GMN3Den, oGMN4.Cod, oGMN4.Den
                    ReDim Preserve arGMN4(0 To oGMN4s.Count - 1)
                    arGMN4(oGMN4s.Count - 1) = scod1 & scod2 & scod3 & scod4
                End If
                oGMN4s.Item(scod1 & scod2 & scod3 & scod4).Indice = 1
            Next
        Next

        'Crear el �rbol
        Dim sImagen As String
        Dim i As Integer
        'Nivel 1
        If oGMN1s.Count > 0 Then
            OrdenarArray arGMN1
            For i = 0 To UBound(arGMN1)
                Set oGMN1 = oGMN1s.Item(arGMN1(i))
                sImagen = IIf(oGMN1.Indice = 1, "GMN1A", "GMN1")
                Set nodx = .Nodes.Add("Raiz", tvwChild, "GMN1" & arGMN1(i), CStr(oGMN1.Cod) & " - " & oGMN1.Den, sImagen)
            Next
        End If
        'Nivel 2
        If oGMN2s.Count > 0 Then
            OrdenarArray arGMN2
            For i = 0 To UBound(arGMN2)
                Set oGMN2 = oGMN2s.Item(arGMN2(i))
                sImagen = IIf(oGMN2.Indice = 1, "GMN2A", "GMN2")
                Set nodx = .Nodes.Add("GMN1" & Left(arGMN2(i), gLongitudesDeCodigos.giLongCodGMN1), tvwChild, "GMN2" & arGMN2(i), CStr(oGMN2.Cod) & " - " & oGMN2.Den, sImagen)
                If oGMN1s.Item(Left(arGMN2(i), gLongitudesDeCodigos.giLongCodGMN1)).Indice = 0 Then nodx.EnsureVisible
            Next
        End If
        'Nivel 3
        If oGMN3s.Count > 0 Then
            OrdenarArray arGMN3
            For i = 0 To UBound(arGMN3)
                Set oGMN3 = oGMN3s.Item(arGMN3(i))
                sImagen = IIf(oGMN3.Indice = 1, "GMN3A", "GMN3")
                Set nodx = .Nodes.Add("GMN2" & Left(arGMN3(i), gLongitudesDeCodigos.giLongCodGMN1 + gLongitudesDeCodigos.giLongCodGMN2), tvwChild, "GMN3" & arGMN3(i), CStr(oGMN3.Cod) & " - " & oGMN3.Den, sImagen)
                If oGMN2s.Item(Left(arGMN3(i), gLongitudesDeCodigos.giLongCodGMN1 + gLongitudesDeCodigos.giLongCodGMN2)).Indice = 0 Then nodx.EnsureVisible
            Next
        End If
        'Nivel 4
        If oGMN4s.Count > 0 Then
            OrdenarArray arGMN4
            For i = 0 To UBound(arGMN4)
                Set oGMN4 = oGMN4s.Item(arGMN4(i))
                sImagen = IIf(oGMN4.Indice = 1, "GMN4A", "GMN4")
                Set nodx = .Nodes.Add("GMN3" & Left(arGMN4(i), gLongitudesDeCodigos.giLongCodGMN1 + gLongitudesDeCodigos.giLongCodGMN2 + gLongitudesDeCodigos.giLongCodGMN3), tvwChild, "GMN4" & arGMN4(i), CStr(oGMN4.Cod) & " - " & oGMN4.Den, sImagen)
                If oGMN3s.Item(Left(arGMN4(i), gLongitudesDeCodigos.giLongCodGMN1 + gLongitudesDeCodigos.giLongCodGMN2 + gLongitudesDeCodigos.giLongCodGMN3)).Indice = 0 Then nodx.EnsureVisible
            Next
        End If
    End With
    
Salir:
    Set nodx = Nothing
    Set oMatQA = Nothing
    Set oGMN1s = Nothing
    Set oGMN2s = Nothing
    Set oGMN3s = Nothing
    Set oGMN4s = Nothing
    Set oGMN1 = Nothing
    Set oGMN2 = Nothing
    Set oGMN3 = Nothing
    Set oGMN4 = Nothing
    Screen.MousePointer = vbNormal
    LockWindowUpdate 0&
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmVARCalMaterial", "GenerarEstructuraMatAsignable", Err, Erl, , m_bActivado)
    End If
    Resume Salir
End Sub

''' <summary>Ordena el array pasado como par�metro</summary>
''' <param name="arArray">Array</param>
''' <remarks>Llamada desde: GenerarEstructuraMatAsignable</remarks>

Private Sub OrdenarArray(ByRef arArray As Variant)
    Dim i As Integer
    Dim j As Integer
    Dim sItem As String
    
    If Not IsEmpty(arArray) Then
        For i = 0 To UBound(arArray)
            For j = 0 To i
                If StrComp(arArray(j), arArray(i)) > 0 Then
                    'Intercambiar
                    sItem = arArray(j)
                    arArray(j) = arArray(i)
                    arArray(i) = sItem
                End If
            Next
        Next
    End If
End Sub

''' <summary>Genera la estructura del �rbol de materiales de QA</summary>
''' <remarks>Llamada desde: Form_Activate</remarks>

Private Sub GenerarEstructuraMaterialesQA()
    Dim oMatQA As CMaterialQA
    Dim scod1 As String
    Dim nodx As MSComctlLib.Node
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    With tvwMaterialQA
        .Nodes.Clear
        
        Set nodx = .Nodes.Add(, , "Raiz", m_sMaterialQA, "Raiz")
        nodx.Tag = "Raiz"
        nodx.Expanded = True
                        
        If g_bExiste Then
            g_oVariableCalidad.CargarMaterialesQA
        Else
            If g_oVariableCalidad.MaterialesQA Is Nothing Then Set g_oVariableCalidad.MaterialesQA = Raiz.Generar_CMaterialesQA
        End If
        Set m_oVarCalMaterialesQA = g_oVariableCalidad.MaterialesQA.Clone
        
        'Cargar el �rbol de materiales de QA
        For Each oMatQA In m_oMaterialesQA.Materiales
            scod1 = CStr(oMatQA.Id)
            Set nodx = .Nodes.Add("Raiz", tvwChild, "MQA" & scod1, oMatQA.Den, "GMN1")
            If Not m_oVarCalMaterialesQA.Item(CStr(oMatQA.Id)) Is Nothing Then
                nodx.EnsureVisible
                nodx.Checked = True
            End If
            nodx.Tag = oMatQA.Den
        Next
    End With
    
    Screen.MousePointer = vbNormal
    
    Exit Sub
Error:
    Set nodx = Nothing
    Resume Next
End Sub

Private Sub tvwEstrMatMod_NodeCheck(ByVal Node As MSComctlLib.Node)
    Set SelNode = Node
    Node.Checked = Not (Node.Checked)
    DoEvents
End Sub

''' <summary>Selecciona el primer nodo del �rbol de materiales de QA</summary>
''' <remarks>Llamada desde: tvwMaterialQA_NodeCheck, Form_Activate</remarks>

Private Sub MaterialSeleccionado()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    With tvwEstrMatMod
        .Nodes("Raiz").Root.Selected = True
        If .Nodes.Count = 0 Then Exit Sub
    End With
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmVARCalMaterial", "MaterialSeleccionado", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Carga los recursos del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next

    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CAL_MATERIAL, Idioma)

    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        m_sMaterialQA = Ador(0).Value
        Ador.MoveNext
        m_sMaterialGS = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        lblMaterialQA.Caption = Ador(0).Value & ":"
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Private Sub tvwMaterialQA_NodeCheck(ByVal Node As MSComctlLib.Node)
    Dim oNodoHijo As MSComctlLib.Node
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Node.Tag = "Raiz" Then
        Set oNodoHijo = Node.Child
        While Not oNodoHijo Is Nothing
            oNodoHijo.Checked = Node.Checked
            MatQAChequeado oNodoHijo
            Set oNodoHijo = oNodoHijo.Next
        Wend
    Else
        MatQAChequeado Node
        
        'Chequear/Deschequear el raiz
        Dim bCheck As Boolean
        bCheck = True
        Set oNodoHijo = tvwMaterialQA.Nodes("Raiz").Child
        Do While Not oNodoHijo Is Nothing
            If Not oNodoHijo.Checked Then
                bCheck = False
                Exit Do
            End If
            Set oNodoHijo = oNodoHijo.Next
        Loop
        tvwMaterialQA.Nodes("Raiz").Checked = bCheck
    End If
    
    GenerarEstructuraMatAsignable
    MaterialSeleccionado

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmVARCalMaterial", "tvwMaterialQA_NodeCheck", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Acciones a llevar a cabo cunado se chequea/deschequea un material de QA</summary>
''' <param name="Node">Nodo del �rbol de materiales de QA<param>
''' <remarks>Llamada desde: tvwMaterialQA_NodeCheck</remarks>

Private Sub MatQAChequeado(ByVal Node As MSComctlLib.Node)
    Dim sCod As String
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sCod = Right(Node.Key, Len(Node.Key) - 3)
    If Node.Checked Then
        If m_oVarCalMaterialesQA.Item(sCod) Is Nothing Then m_oVarCalMaterialesQA.Add m_oMaterialesQA.Item(sCod).Id, m_oMaterialesQA.Item(sCod).Den, m_oMaterialesQA.Item(sCod).Baja, Idioma
    Else
        If Not m_oVarCalMaterialesQA.Item(sCod) Is Nothing Then m_oVarCalMaterialesQA.Remove sCod
    End If
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmVARCalMaterial", "MatQAChequeado", Err, Erl, , m_bActivado)
    End If
End Sub
