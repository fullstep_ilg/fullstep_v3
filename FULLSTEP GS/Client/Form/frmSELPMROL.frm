VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSELPMROL 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DSelección de rol"
   ClientHeight    =   1260
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5580
   Icon            =   "frmSELPMROL.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1260
   ScaleWidth      =   5580
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   1440
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   0
      Top             =   840
      Width           =   3825
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   2
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   1
         Top             =   60
         Width           =   1050
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcRoles 
      Height          =   285
      Left            =   1440
      TabIndex        =   3
      Top             =   300
      Width           =   4000
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7064
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "PERMITIR_TRASLADO"
      Columns(2).Name =   "PERMITIR_TRASLADO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   7056
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblRol 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominación:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   360
      Width           =   1380
   End
End
Attribute VB_Name = "frmSELPMROL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public lIdFlujo As Long
Public lIdRol As Long
Public bPermitirTraslados As Boolean
Public sOrigen As String
Public Datos As Variant
Public Ok As Boolean
Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Raiz As CRaiz

Private oRoles As CPMRoles
Private oRol As CPMRol

Private Sub cmdAceptar_Click()
    ReDim Datos(0 To 1)
    Datos(0) = lIdRol
    Datos(1) = bPermitirTraslados
    Ok = True
    Me.Hide 'Lo descargo en el origen
End Sub

Private Sub cmdCancelar_Click()
    Ok = False
    lIdRol = 0
    Me.Hide
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass
    CargarRecursos
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
   Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = GestorIdiomas.DevolverTextosDelModulo(FRM_SELPMROL, Idioma)
    
    If Not ador Is Nothing Then
            
        Me.Caption = ador(0).Value '1
        ador.MoveNext
        lblRol.Caption = ador(0).Value
        ador.MoveNext
        cmdAceptar.Caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.Caption = ador(0).Value
                
        ador.Close
    End If
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: carga los roles en la combo, con el id, la descripción y si permite o no traslados
'Llamada desde: Form_Load
Private Sub CargarRoles()
    Screen.MousePointer = vbHourglass
    
    Set oRoles = Raiz.Generar_CPMRoles
    oRoles.CargarRolesWorkflow lIdFlujo
        
    sdbcRoles.RemoveAll
    For Each oRol In oRoles
        sdbcRoles.AddItem oRol.Id & Chr(9) & oRol.Den & Chr(9) & oRol.PermitirTraslados
    Next
    Set oRol = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oRoles = Nothing
    Set oRol = Nothing
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: al cerrar el combo guarda el id del rol seleccionado y si permite o no traslados
Private Sub sdbcRoles_CloseUp()
    If sdbcRoles.Value <> "" Then
        lIdRol = CLng(sdbcRoles.Value)
        bPermitirTraslados = CBool(sdbcRoles.Columns(2).Value)
    End If
End Sub

Private Sub sdbcRoles_DropDown()
    CargarRoles
End Sub

Private Sub sdbcRoles_InitColumnProps()
    sdbcRoles.DataFieldList = "Column 0"
    sdbcRoles.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcRoles_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcRoles.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcRoles.Rows - 1
            bm = sdbcRoles.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcRoles.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcRoles.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

