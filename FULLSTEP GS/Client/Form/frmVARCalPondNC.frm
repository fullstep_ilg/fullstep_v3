VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmVARCalPondNC 
   Caption         =   "DVariables de calidad"
   ClientHeight    =   7815
   ClientLeft      =   1005
   ClientTop       =   2610
   ClientWidth     =   10050
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVARCalPondNC.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7815
   ScaleWidth      =   10050
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Height          =   6855
      Left            =   135
      ScaleHeight     =   6795
      ScaleWidth      =   9705
      TabIndex        =   3
      Top             =   405
      Width           =   9765
      Begin VB.PictureBox picNumerico 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   6810
         Left            =   0
         ScaleHeight     =   6810
         ScaleWidth      =   9780
         TabIndex        =   4
         Top             =   0
         Width           =   9780
         Begin VB.PictureBox Picture3 
            Appearance      =   0  'Flat
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   1350
            Left            =   315
            ScaleHeight     =   1350
            ScaleWidth      =   7575
            TabIndex        =   27
            Top             =   5160
            Width           =   7575
            Begin VB.OptionButton optOpcionConf 
               BackColor       =   &H00808000&
               Caption         =   "DMedia ponderada seg�n:"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   1
               Left            =   225
               TabIndex        =   30
               Top             =   600
               Width           =   3240
            End
            Begin VB.OptionButton optOpcionConf 
               BackColor       =   &H00808000&
               Caption         =   "DEvaluar f�rmula"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   0
               Left            =   225
               TabIndex        =   29
               Top             =   300
               Width           =   3120
            End
            Begin VB.OptionButton optOpcionConf 
               BackColor       =   &H00808000&
               Caption         =   "DNo calcular"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   2
               Left            =   225
               TabIndex        =   28
               Top             =   900
               Width           =   3240
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcVarPond 
               Height          =   285
               Left            =   3500
               TabIndex        =   31
               Top             =   645
               Width           =   3900
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "COD"
               Columns(0).Alignment=   1
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6800
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   6879
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblOpcionConf 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DSelecci�n del modo de c�lculo para unidades de negocio de nivel superior:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   32
               Top             =   0
               Width           =   7185
            End
         End
         Begin VB.PictureBox Picture2 
            Appearance      =   0  'Flat
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   495
            Left            =   3000
            ScaleHeight     =   495
            ScaleWidth      =   6615
            TabIndex        =   22
            Top             =   50
            Width           =   6615
            Begin VB.OptionButton optPeriodo 
               BackColor       =   &H00808000&
               Caption         =   "Da�o en curso"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   1
               Left            =   3400
               TabIndex        =   26
               Top             =   120
               Width           =   2535
            End
            Begin VB.OptionButton optPeriodo 
               BackColor       =   &H00808000&
               Height          =   375
               Index           =   0
               Left            =   50
               TabIndex        =   24
               Top             =   100
               Value           =   -1  'True
               Width           =   255
            End
            Begin VB.TextBox txtPeriodo 
               Alignment       =   1  'Right Justify
               Height          =   315
               Left            =   400
               TabIndex        =   23
               Top             =   120
               Width           =   1020
            End
            Begin VB.Label lblPeriodo 
               BackColor       =   &H00808000&
               Caption         =   "meses"
               ForeColor       =   &H00FFFFFF&
               Height          =   285
               Index           =   1
               Left            =   1500
               TabIndex        =   25
               Top             =   200
               Width           =   1560
            End
         End
         Begin VB.CommandButton cmdAyuda 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   7.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   7365
            Picture         =   "frmVARCalPondNC.frx":014A
            Style           =   1  'Graphical
            TabIndex        =   10
            Top             =   4485
            Width           =   350
         End
         Begin VB.TextBox txtFormula 
            Height          =   315
            Left            =   3060
            TabIndex        =   9
            Top             =   4470
            Width           =   4230
         End
         Begin VB.OptionButton optPond 
            BackColor       =   &H00808000&
            Caption         =   "Suma de los pesos de las no conformidades"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   2
            Left            =   3060
            TabIndex        =   7
            Top             =   3735
            Width           =   3915
         End
         Begin VB.OptionButton optPond 
            BackColor       =   &H00808000&
            Caption         =   "Media de los pesos de las no conformidades"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   1
            Left            =   3060
            TabIndex        =   6
            Top             =   3450
            Width           =   3915
         End
         Begin VB.OptionButton optPond 
            BackColor       =   &H00808000&
            Caption         =   "N�mero de no conformidades"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   0
            Left            =   3060
            TabIndex        =   5
            Top             =   3165
            Width           =   3915
         End
         Begin VB.Label lblNoConfX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X7 = Suelo"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   6
            Left            =   3060
            TabIndex        =   21
            Top             =   2580
            Width           =   780
         End
         Begin VB.Label lblNoConfX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X6 = Objetivo"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   5
            Left            =   3060
            TabIndex        =   20
            Top             =   2280
            Width           =   1005
         End
         Begin VB.Label lblNoConfX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X5 = Facturaci�n Proveedor ultimos 12 meses"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   4
            Left            =   3060
            TabIndex        =   19
            Top             =   1980
            Width           =   3285
         End
         Begin VB.Label lblNoConfX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X4 = No conformidades con cierre negativo"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   3
            Left            =   3060
            TabIndex        =   18
            Top             =   1680
            Width           =   3105
         End
         Begin VB.Label lblNoConfX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X3 = No conformidades cerradas fuera de plazo"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   2
            Left            =   3060
            TabIndex        =   17
            Top             =   1365
            Width           =   3435
         End
         Begin VB.Label lblNoConfX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X2 = No conformidades cerradas dentro de plazo"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   1
            Left            =   3060
            TabIndex        =   16
            Top             =   1065
            Width           =   3525
         End
         Begin VB.Label lblNoConfX 
            AutoSize        =   -1  'True
            BackColor       =   &H00808000&
            Caption         =   "X1 = No conformidades abiertas"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   0
            Left            =   3060
            TabIndex        =   15
            Top             =   750
            Width           =   2310
         End
         Begin VB.Label lblCalculo 
            BackColor       =   &H00808000&
            Caption         =   "C�lculo de Xi:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   315
            TabIndex        =   14
            Top             =   3120
            Width           =   2520
         End
         Begin VB.Label lblVariables 
            BackColor       =   &H00808000&
            Caption         =   "Variables:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   315
            TabIndex        =   13
            Top             =   750
            Width           =   2310
         End
         Begin VB.Label lblPeriodo 
            BackColor       =   &H00808000&
            Caption         =   "Periodo a considerar:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Index           =   0
            Left            =   315
            TabIndex        =   12
            Top             =   240
            Width           =   2250
         End
         Begin VB.Label lblFormula 
            BackColor       =   &H00808000&
            Caption         =   "Ponderacion de la subvariable:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   315
            TabIndex        =   11
            Top             =   4110
            Width           =   4560
         End
      End
   End
   Begin VB.Label lblCambios 
      Caption         =   "Para guardar los cambios efectuados cierre esta ventana y pulse Guardar cambios en la ventada principal de Variables de calidad."
      ForeColor       =   &H000000FF&
      Height          =   240
      Left            =   240
      TabIndex        =   8
      Top             =   7520
      Width           =   9735
   End
   Begin VB.Label lblForm 
      Caption         =   "Formulario asociado:"
      Height          =   240
      Left            =   6750
      TabIndex        =   2
      Top             =   45
      Width           =   3255
   End
   Begin VB.Label lblOrigen 
      Caption         =   "Origen de datos: "
      Height          =   240
      Left            =   3420
      TabIndex        =   1
      Top             =   45
      Width           =   3255
   End
   Begin VB.Label lblTipo 
      Caption         =   "Subvariable de tipo: "
      Height          =   240
      Left            =   45
      TabIndex        =   0
      Top             =   45
      Width           =   3300
   End
End
Attribute VB_Name = "frmVARCalPondNC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables publicas
Public g_oVariableCalidad As CVariableCalidad
Public g_oVarCalidadOriginal As CVariableCalidad
Public g_lFormulario As Long
Public GestorIdiomas As CGestorIdiomas
Public Mensajes As CMensajes
Public FrmVARCalidad As Object
Public FrmVARCalCompuestaNivel5 As Object
Public FrmVARCalCompuestaNivel4 As Object
Public FrmVARCalCompuestaNivel3 As Object
Public Idioma As String

'Variables de gesti�n
Private m_bRespetarCarga As Boolean

'Variables de idiomas
Private m_sSinPond As String
Private m_sContinua As String
Private m_sDiscreta As String
Private m_sCaducidad As String
Private m_sFormula As String
Private m_sManual As String
Private m_sAutomatico As String
Private m_sMensaje(0 To 8) As String
Private m_sLabel(0 To 2) As String
Private m_sTiposVar(0 To 2) As String
Private m_sSiNo(0 To 5) As String
Private m_sIdiErrorFormula(11) As String
Private m_sCaption As String

Private Sub cmdAyuda_Click()
    MostrarFormSOLAyudaCalculosLocal GestorIdiomas, Idioma
End Sub

Private Sub Form_Load()
    Me.Width = 10170
    Me.Height = 7750 ' 6390
        
    CargarRecursos
    PuntuacionCargar
    CargarVariablesHermanas
            
    lblCambios.Visible = False
    Me.Caption = m_sCaption & " " & g_oVariableCalidad.Denominaciones.Item(Idioma).Den
    picNumerico.Enabled = Not g_oVariableCalidad.BajaLog
End Sub

Private Sub PuntuacionCargar()
    m_bRespetarCarga = True
    lblOrigen.Caption = m_sLabel(1) & " " & g_oVariableCalidad.OrigenCod
    If NullToStr(g_oVariableCalidad.FormularioCOD) = "" Then g_oVariableCalidad.CargarNombreFormulario
    lblForm.Caption = m_sLabel(2) & " " & g_oVariableCalidad.FormularioCOD
    lblTipo.Caption = m_sLabel(0) & " " & m_sTiposVar(2)
    
    
    If NullToStr(g_oVariableCalidad.NCPeriodo) = "" Then
        txtPeriodo.Text = NullToStr(g_oVariableCalidad.NCPeriodo)
        optPeriodo(0).Value = True
    Else
        If CInt(g_oVariableCalidad.NCPeriodo) = 0 Then
            txtPeriodo.Text = ""
            optPeriodo(1).Value = True
        Else
            txtPeriodo.Text = CInt(g_oVariableCalidad.NCPeriodo)
            optPeriodo(0).Value = True
        End If
    End If
    
    Select Case g_oVariableCalidad.TipoPonderacion
        Case PondNCNumero
            optPond(0).Value = True
        Case PondNCMediaPesos
            optPond(1).Value = True
        Case PondNCSumaPesos
            optPond(2).Value = True
    End Select
    txtFormula.Text = NullToStr(g_oVariableCalidad.Formula)
        
    optOpcionConf(g_oVariableCalidad.Opcion_Conf).Value = True
    If g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
        sdbcVarPond.Columns("ID").Value = g_oVariableCalidad.IdVar_Pond
    End If
        
    m_bRespetarCarga = False
End Sub

''' <summary>
''' Carga los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Dim iDen(6) As Integer

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIDAD_PUNT, Idioma)
    
    If Not Ador Is Nothing Then
        m_sSinPond = Ador(0).Value
        Ador.MoveNext
        m_sContinua = Ador(0).Value
        optPond(1).Caption = m_sContinua
        Ador.MoveNext
        m_sDiscreta = Ador(0).Value
        Ador.MoveNext
        m_sCaducidad = Ador(0).Value
        Ador.MoveNext
        m_sFormula = Ador(0).Value '5
        optPond(2).Caption = m_sFormula
        Ador.MoveNext
        m_sManual = Ador(0).Value
        Ador.MoveNext
        m_sAutomatico = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sMensaje(0) = Ador(0).Value 'El intervalo ya existe
        Ador.MoveNext
        m_sMensaje(1) = Ador(0).Value 'el campo desde debe ser inferior al campo hasta
        Ador.MoveNext
        m_sMensaje(2) = Ador(0).Value 'debe ser num�rico
        Ador.MoveNext
        m_sMensaje(3) = Ador(0).Value 'debe ser una fecha
        Ador.MoveNext
        m_sMensaje(4) = Ador(0).Value 'el valor ya existe
        Ador.MoveNext
        m_sMensaje(5) = Ador(0).Value 'El intervalo introducido contiene uno de los ya existentes
        Ador.MoveNext
        m_sMensaje(6) = Ador(0).Value 'Modifique el �ltimo intervalo introducido
        Ador.MoveNext
        Ador.MoveNext
        m_sMensaje(7) = Ador(0).Value 'Tenga en cuenta el intervalo libre introducido
        Ador.MoveNext
        m_sMensaje(8) = Ador(0).Value 'Introduzca un m�ximo inferior o igual al introducido en el intervalo libre del campo
        Ador.MoveNext
        m_sLabel(0) = Ador(0).Value
        Ador.MoveNext
        m_sTiposVar(0) = Ador(0).Value '24 Certificado
        Ador.MoveNext
        m_sLabel(1) = Ador(0).Value
        Ador.MoveNext
        m_sLabel(2) = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        '30
        Ador.MoveNext
        m_sSiNo(0) = Ador(0).Value 'Valor = S�
        Ador.MoveNext
        m_sSiNo(1) = Ador(0).Value 'Valor = No
        Ador.MoveNext
        m_sSiNo(2) = Ador(0).Value 'Vigente
        Ador.MoveNext
        m_sSiNo(3) = Ador(0).Value 'Caducada
        Ador.MoveNext
        m_sSiNo(4) = Ador(0).Value 'Contiene archivo
        Ador.MoveNext
        m_sSiNo(5) = Ador(0).Value 'No contiene archivo
        Ador.MoveNext
        For i = 1 To 11
            Ador.MoveNext
            m_sIdiErrorFormula(i) = Ador(0).Value
        Next i
        Ador.MoveNext
        lblCambios.Caption = Ador(0).Value
        Ador.MoveNext
        m_sTiposVar(1) = Ador(0).Value '50 Integracion
        Ador.MoveNext
        Ador.MoveNext
        lblFormula.Caption = Ador(0).Value '52
        Ador.MoveNext
        lblPeriodo(0).Caption = Ador(0).Value
        Ador.MoveNext
        m_sTiposVar(2) = Ador(0).Value
        Ador.MoveNext
        lblPeriodo(1).Caption = Ador(0).Value '55
        Ador.MoveNext
        lblVariables.Caption = Ador(0).Value
        Ador.MoveNext
        lblNoConfX(0).Caption = Ador(0).Value
        Ador.MoveNext
        iDen(0) = 57
        lblNoConfX(1).Caption = Ador(0).Value '57 X1 = No conformidades abiertas
        iDen(1) = 58
        Ador.MoveNext
        lblNoConfX(2).Caption = Ador(0).Value
        iDen(2) = 59
        Ador.MoveNext
        iDen(3) = 60
        lblNoConfX(3).Caption = Ador(0).Value
        Ador.MoveNext
        lblCalculo.Caption = Ador(0).Value
        Ador.MoveNext
        optPond(0).Caption = Ador(0).Value
        Ador.MoveNext
        optPond(1).Caption = Ador(0).Value
        Ador.MoveNext
        optPond(2).Caption = Ador(0).Value
        Ador.MoveNext
        m_sCaption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        iDen(4) = 67
        lblNoConfX(4).Caption = Ador(0).Value
        Ador.MoveNext
        iDen(5) = 68
        lblNoConfX(5).Caption = Ador(0).Value
        Ador.MoveNext
        iDen(6) = 69
        lblNoConfX(6).Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        optPeriodo(1).Caption = Ador(0).Value ' a�o en curso
        
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        
        Ador.MoveNext
        lblOpcionConf.Caption = Ador(0).Value ' Selecci�n del modo de c�lculo para unidades de negocio de nivel superior:
        Ador.MoveNext
        optOpcionConf(0).Caption = Ador(0).Value ' Evaluar f�rmula
        Ador.MoveNext
        optOpcionConf(1).Caption = Ador(0).Value ' Media ponderada seg�n:
        Ador.MoveNext
        optOpcionConf(2).Caption = Ador(0).Value ' No calcular
        
        Ador.Close
    End If
    Set Ador = Nothing
End Sub

Private Sub Arrange()
    Dim dblTres As Double

    On Error Resume Next
    
    If Not lblCambios.Visible Then
        Picture1.Height = Me.Height - 915
    Else
        Picture1.Height = Me.Height - 1400 '1300
    End If
    dblTres = Me.Width / 3
    lblTipo.Width = dblTres - 100
    lblOrigen.Width = dblTres - 100
    lblForm.Width = dblTres - 100
    lblOrigen.Left = lblTipo.Left + lblTipo.Width + 100
    lblForm.Left = lblOrigen.Left + lblOrigen.Width + 100
    Picture1.Width = Me.Width - 405
    lblCambios.Top = Picture1.Top + Picture1.Height + 90
    
    picNumerico.Height = Picture1.Height
    picNumerico.Width = Picture1.Width

End Sub
Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ValidarPonderacion Then
        Cancel = True
        Exit Sub
    End If
    
    Set g_oVariableCalidad = Nothing
    Set g_oVarCalidadOriginal = Nothing
End Sub

''' <summary>Comprobamos si los datos introducidos son correctos. De ser asi los guardo.</summary>
''' <remarks>Llamada desde: Form_Unload</remarks>

Private Function ValidarPonderacion() As Boolean
    Dim bGuardar As Boolean
    Dim udtNCCal As TVariablesCalPonderacion
    
    ValidarPonderacion = False
    
    bGuardar = False
    'Si ha cambiado la ponderaci�n la guardo
    If txtFormula.Text = "" Then
        Mensajes.NoValido m_sFormula
        Exit Function
    End If
    If Not ValidarFormula(txtFormula.Text, False) Then
        Mensajes.NoValido m_sFormula
        Exit Function
    End If
    If g_oVariableCalidad.EsMultimaterial Then
        If g_oVariableCalidad.TieneObjetivosSuelosEnFormula(txtFormula.Text) Then
            Mensajes.VarMultiMatNoObjSuelo
            Exit Function
        End If
    End If
    If optPeriodo(0).Value Then
        If txtPeriodo.Text = "" Then
            Mensajes.NoValido Left(lblPeriodo(0).Caption, Len(lblPeriodo(0).Caption) - 1)
            Exit Function
        End If
        If Not IsNumeric(txtPeriodo.Text) Then
            Mensajes.NoValido Left(lblPeriodo(0).Caption, Len(lblPeriodo(0).Caption) - 1)
            Exit Function
        ElseIf CInt(txtPeriodo.Text) = 0 Then
            Mensajes.NoValido Left(lblPeriodo(0).Caption, Len(lblPeriodo(0).Caption) - 1)
            Exit Function
        End If
        txtPeriodo.Text = CInt(txtPeriodo.Text)
    End If
    
    If optOpcionConf(VarCalOpcionConf.EvaluarFormula).Value = True And g_oVariableCalidad.Opcion_Conf <> VarCalOpcionConf.EvaluarFormula Then
        bGuardar = True
    ElseIf optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True Then
        If sdbcVarPond.Text = "" Then
            Mensajes.NoValido Left(lblOpcionConf.Caption, Len(lblOpcionConf.Caption) - 1)
            Exit Function
        End If
        If sdbcVarPond.Text <> NullToStr(g_oVariableCalidad.IdVar_Pond) Then
            bGuardar = True
        End If
    Else
        If optOpcionConf(VarCalOpcionConf.NoCalcular).Value = True And g_oVariableCalidad.Opcion_Conf <> VarCalOpcionConf.NoCalcular Then bGuardar = True
    End If
    
    If optPond(0).Value Then
        udtNCCal = PondNCNumero
    ElseIf optPond(1).Value Then
        udtNCCal = PondNCMediaPesos
    ElseIf optPond(2).Value Then
        udtNCCal = PondNCSumaPesos
    Else
        optPond(1).Value = True
        udtNCCal = PondNCMediaPesos
    End If
            
    If g_oVariableCalidad.TipoPonderacion <> udtNCCal Then
        bGuardar = True
    ElseIf txtFormula.Text <> NullToStr(g_oVariableCalidad.Formula) Then
        bGuardar = True
    Else
        If optPeriodo(0).Value Then
            If CInt(txtPeriodo.Text) <> NullToDbl0(g_oVariableCalidad.NCPeriodo) Then
                bGuardar = True
            End If
        Else
            bGuardar = True
        End If
    End If
        
    If bGuardar Then
        ValidarPonderacion = GuardarPondNC
    Else
        ValidarPonderacion = True
    End If
End Function

'Si ha habido cambios mostramos el mensaje de guardar en la pantalla padre
Private Sub HayCambios()
    If Not lblCambios.Visible Then
        VisualizarGuardar
        If Me.WindowState = 2 Then
            Me.Picture1.Height = Me.Picture1.Height - 420
            Me.picNumerico.Height = picNumerico.Height - 420
            Me.lblCambios.Top = Me.lblCambios.Top - 400
        Else
            Me.Height = Me.Height + 420
        End If
    End If
End Sub

'Guardamos la ponderaci�n
Private Function GuardarPondNC() As Boolean
    On Error GoTo Error
    
    'Guardo la punt cuando se guarde todo en el form principal de variables
    g_oVariableCalidad.modificado = True
    
    If optPond(0).Value Then
        g_oVariableCalidad.TipoPonderacion = PondNCNumero
    ElseIf optPond(1).Value Then
        g_oVariableCalidad.TipoPonderacion = PondNCMediaPesos
    ElseIf optPond(2).Value Then
        g_oVariableCalidad.TipoPonderacion = PondNCSumaPesos
    Else
        g_oVariableCalidad.TipoPonderacion = PondNCMediaPesos
    End If
    If NullToStr(g_oVariableCalidad.Formula) <> txtFormula.Text Then
        g_oVariableCalidad.IDFormula = Null
    End If
    If optPeriodo(0).Value Then
        g_oVariableCalidad.NCPeriodo = CInt(txtPeriodo.Text)
    ElseIf optPeriodo(1).Value Then
        g_oVariableCalidad.NCPeriodo = 0
    End If
    g_oVariableCalidad.Formula = txtFormula.Text
    
    If optOpcionConf(VarCalOpcionConf.EvaluarFormula).Value = True Then
        g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.EvaluarFormula
        g_oVariableCalidad.IdVar_Pond = ""
    ElseIf optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True Then
        g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana
        g_oVariableCalidad.IdVar_Pond = sdbcVarPond.Columns(0).Value
    Else
        g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.NoCalcular
        g_oVariableCalidad.IdVar_Pond = ""
    End If
    
    Set g_oVariableCalidad.CamposPond = Nothing
    Set g_oVariableCalidad.INTListaPond = Nothing

    GuardarPondNC = True
    Exit Function
Error:
    GuardarPondNC = False
End Function

'Validamos en la medida de lo posible la formula introducida para evitar algunos errores.
Private Function ValidarFormula(ByVal sFormula As String, ByVal bNoMens As Boolean) As Boolean
    Dim sVariables() As String
    Dim lErrCode As Integer
    Dim lIndex As Integer
    Dim iEq As USPExpression
    Dim sCaracter As String
    Dim i As Integer
    
    If sFormula <> "" Then
        Set iEq = New USPExpression
        
        ReDim sVariables(7)
        For i = 1 To 7
            sVariables(i) = "X" & i
        Next
    
        lIndex = iEq.Parse(UCase(sFormula), sVariables, lErrCode)
    
        If lErrCode <> USPEX_NO_ERROR Then
            ' Parsing error handler
            If bNoMens = False Then
                Select Case lErrCode
                    Case USPEX_DIVISION_BY_ZERO
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(1))
                    Case USPEX_EMPTY_EXPRESSION
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(2))
                    Case USPEX_MISSING_OPERATOR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(3))
                    Case USPEX_SYNTAX_ERROR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(4))
                    Case USPEX_UNKNOWN_FUNCTION
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(5))
                    Case USPEX_UNKNOWN_OPERATOR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(6))
                    Case USPEX_WRONG_PARAMS_NUMBER
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(7))
                    Case USPEX_UNKNOWN_IDENTIFIER
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(8))
                    Case USPEX_UNKNOWN_VAR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(9))
                    Case USPEX_VARIABLES_NOTUNIQUE
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(10))
                    Case Else
                        sCaracter = Mid(sFormula, lIndex)
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(11) & vbCrLf & sCaracter)
                End Select
            End If
            Set iEq = Nothing
            ValidarFormula = False
            Exit Function
        End If
        Set iEq = Nothing
    Else
        ValidarFormula = False
        Exit Function
    End If
    
    ValidarFormula = True
End Function

Private Sub optPeriodo_Click(Index As Integer)
    HayCambios
End Sub

Private Sub optPond_Click(Index As Integer)
    HayCambios
End Sub

Private Sub optOpcionConf_Click(Index As Integer)
    If Index <> VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
        sdbcVarPond.Text = ""
    End If
    HayCambios
End Sub

Private Sub sdbcVarPond_Change()
    HayCambios
End Sub

Private Sub sdbcVarPond_Click()
    HayCambios
    optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True
End Sub

Private Sub txtFormula_Change()
    HayCambios
End Sub

Private Sub txtPeriodo_Change()
    HayCambios
End Sub

Private Sub VisualizarGuardar()
    FrmVARCalidad.VisualizarGuardar
    lblCambios.Visible = True
    
    Select Case g_oVariableCalidad.Nivel
        Case 5
            FrmVARCalCompuestaNivel5.lblCambios.Visible = True
            FrmVARCalCompuestaNivel4.lblCambios.Visible = True
            FrmVARCalCompuestaNivel3.lblCambios.Visible = True
        Case 4
            FrmVARCalCompuestaNivel4.lblCambios.Visible = True
            FrmVARCalCompuestaNivel3.lblCambios.Visible = True
        Case 3
            FrmVARCalCompuestaNivel3.lblCambios.Visible = True
    End Select
End Sub

Private Sub sdbcVarPond_InitColumnProps()
    sdbcVarPond.DataFieldList = "Column 0"
    sdbcVarPond.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcVarPond_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcVarPond.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcVarPond.Rows - 1
            bm = sdbcVarPond.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcVarPond.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcVarPond.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub CargarVariablesHermanas()
    Screen.MousePointer = vbHourglass
    Dim oVarCal As CVariableCalidad
    Dim g_oVariablesCalidadHermanas As CVariablesCalidad
    sdbcVarPond.RemoveAll
    
    Select Case g_oVariableCalidad.Nivel
        Case 5
            Set g_oVariablesCalidadHermanas = FrmVARCalCompuestaNivel5.g_oVarCal4Mod.VariblesCal
        Case 4
            Set g_oVariablesCalidadHermanas = FrmVARCalCompuestaNivel4.g_oVarCal3Mod.VariblesCal
        Case 3
            Set g_oVariablesCalidadHermanas = FrmVARCalCompuestaNivel3.g_oVarCal2Mod.VariblesCal
        Case 2
            Set g_oVariablesCalidadHermanas = FrmVARCalidad.g_oVarsCalidad1Mod.Item(FrmVARCalidad.ssTabVar.SelectedItem.Index).VariblesCal
    End Select
    
    For Each oVarCal In g_oVariablesCalidadHermanas
        If oVarCal.Id <> g_oVariableCalidad.Id And oVarCal.BajaLog = False Then
            sdbcVarPond.AddItem oVarCal.Id & Chr(9) & oVarCal.Cod & " - " & oVarCal.Denominaciones.Item(Idioma).Den
            If oVarCal.Id = g_oVariableCalidad.IdVar_Pond Then
                sdbcVarPond.Text = oVarCal.Cod
            End If
        End If
    Next
    
    Screen.MousePointer = vbNormal
End Sub
