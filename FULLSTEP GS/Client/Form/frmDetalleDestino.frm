VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmDetalleDestino 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DDetalle Destino"
   ClientHeight    =   5460
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   5010
   Icon            =   "frmDetalleDestino.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5460
   ScaleWidth      =   5010
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2445
      TabIndex        =   17
      Top             =   5040
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1290
      TabIndex        =   16
      Top             =   5040
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.TextBox txtEmail 
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   4560
      Width           =   2460
   End
   Begin VB.TextBox txtFAX 
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   4080
      Width           =   1890
   End
   Begin VB.TextBox txtTfno 
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   3585
      Width           =   1890
   End
   Begin VB.TextBox txtPais 
      Height          =   285
      Left            =   2160
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   2580
      Width           =   2460
   End
   Begin VB.TextBox txtProvincia 
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   3090
      Width           =   2460
   End
   Begin VB.TextBox txtPoblacion 
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   2115
      Width           =   2460
   End
   Begin VB.TextBox txtCP 
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   1635
      Width           =   1170
   End
   Begin VB.TextBox txtDireccion 
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   1140
      Width           =   2460
   End
   Begin VB.TextBox txtDescripcion 
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   660
      Width           =   2460
   End
   Begin VB.TextBox txtCodigo 
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   180
      Width           =   1170
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcPais 
      Height          =   285
      Left            =   1290
      TabIndex        =   9
      Top             =   2610
      Width           =   780
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   -2147483640
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1217
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5054
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   1376
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProvi 
      Height          =   285
      Left            =   1290
      TabIndex        =   11
      Top             =   3090
      Width           =   780
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   -2147483640
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1217
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5054
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   1376
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblEmail 
      BackStyle       =   0  'Transparent
      Caption         =   "DE-Mail:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   270
      TabIndex        =   23
      Top             =   4620
      Width           =   1395
   End
   Begin VB.Label lblFAX 
      BackStyle       =   0  'Transparent
      Caption         =   "DFAX:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   270
      TabIndex        =   22
      Top             =   4140
      Width           =   1395
   End
   Begin VB.Label lblTfno 
      BackStyle       =   0  'Transparent
      Caption         =   "DTel�fono:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   270
      TabIndex        =   21
      Top             =   3660
      Width           =   1395
   End
   Begin VB.Label lblPais 
      BackStyle       =   0  'Transparent
      Caption         =   "DPa�s:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   270
      TabIndex        =   20
      Top             =   2670
      Width           =   1395
   End
   Begin VB.Label lblProvincia 
      BackStyle       =   0  'Transparent
      Caption         =   "DProvincia:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   270
      TabIndex        =   19
      Top             =   3150
      Width           =   1395
   End
   Begin VB.Label lblPoblacion 
      BackStyle       =   0  'Transparent
      Caption         =   "DPoblacii�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   270
      TabIndex        =   18
      Top             =   2175
      Width           =   1395
   End
   Begin VB.Label lblCP 
      BackStyle       =   0  'Transparent
      Caption         =   "CCP:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   270
      TabIndex        =   7
      Top             =   1695
      Width           =   1395
   End
   Begin VB.Label lblDireccion 
      BackStyle       =   0  'Transparent
      Caption         =   "DDirecci�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   270
      TabIndex        =   4
      Top             =   1215
      Width           =   1395
   End
   Begin VB.Label lblDescripcion 
      BackStyle       =   0  'Transparent
      Caption         =   "DDenominaci�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   270
      TabIndex        =   2
      Top             =   735
      Width           =   1395
   End
   Begin VB.Label lblCodigo 
      BackStyle       =   0  'Transparent
      Caption         =   "CC�digo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   270
      TabIndex        =   1
      Top             =   255
      Width           =   1395
   End
End
Attribute VB_Name = "frmDetalleDestino"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Raiz As CRaiz
Public GestorIdiomas As CGestorIdiomas
Public sID_Dest As String
Public sIdioma_Gen As String
Public b_Modificar As Boolean
Public oLinea As CLineaPedido
Public m_sCargaMaxCb As String
Private m_oPaises As CPaises
Private m_oProvincias As CProvincias

Private Sub CargarRecursos()

Dim Ador As ADODB.Recordset

    On Error Resume Next

    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_DETALLEDESTINO, sIdioma_Gen)

    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value                      '(MOD=507, ID=1) Detalle Destino
        Ador.MoveNext
        lblCodigo.Caption = Ador(0).Value & ":"         '(MOD=507, ID=2) C�digo:
        sdbcPais.Columns("COD").Caption = Ador(0).Value
        sdbcProvi.Columns("COD").Caption = Ador(0).Value
        Ador.MoveNext
        lblDescripcion.Caption = Ador(0).Value & ":"    '(MOD=507, ID=3) Descripci�n:
        sdbcPais.Columns("DEN").Caption = Ador(0).Value
        sdbcProvi.Columns("DEN").Caption = Ador(0).Value
        Ador.MoveNext
        lblDireccion.Caption = Ador(0).Value & ":"      '(MOD=507, ID=4) Direcci�n:
        Ador.MoveNext
        lblCP.Caption = Ador(0).Value & ":"             '(MOD=507, ID=5) C.P.:
        Ador.MoveNext
        lblPoblacion.Caption = Ador(0).Value & ":"      '(MOD=507, ID=6) Poblaci�n:
        Ador.MoveNext
        lblProvincia.Caption = Ador(0).Value & ":"      '(MOD=507, ID=7) Provincia:
        Ador.MoveNext
        lblPais.Caption = Ador(0).Value & ":"           '(MOD=507, ID=8) Pa�s:
        Ador.MoveNext
        lblTfno.Caption = Ador(0).Value & ":"           '(MOD=507, ID=9) Tel�fono:
        Ador.MoveNext
        lblFAX.Caption = Ador(0).Value & ":"            '(MOD=507, ID=10) FAX:
        Ador.MoveNext
        lblEmail.Caption = Ador(0).Value & ":"          '(MOD=507, ID=11) E-mail:
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value              '(MOD=507, ID=12) Aceptar
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value                '(MOD=507, ID=13) Cancelar
        Ador.Close
    End If

    Set Ador = Nothing

End Sub

Sub ConfigurarSeguridad()

cmdAceptar.Visible = b_Modificar
cmdCancelar.Visible = b_Modificar
sdbcPais.Visible = b_Modificar
sdbcProvi.Visible = b_Modificar
txtDireccion.Locked = Not b_Modificar
txtDescripcion.Locked = Not b_Modificar
txtCP.Locked = Not b_Modificar
txtPoblacion.Locked = Not b_Modificar
'txtProvincia.Locked = Not b_Modificar
'txtPais.Locked = Not b_Modificar
txtTfno.Locked = Not b_Modificar
txtFAX.Locked = Not b_Modificar
txtEmail.Locked = Not b_Modificar
If b_Modificar Then
    Me.Top = cmdAceptar.Top + cmdAceptar.Height + 100
End If

End Sub


Private Sub ValidarProvincia()
Dim oPais As CPais
Dim ADORs As ADODB.Recordset
If sdbcProvi.Text <> "" Then
    Set oPais = m_oPaises.DevolverPais(UCase(sdbcPais.Text))
    oPais.CargarTodasLasProvincias sdbcProvi.Text, , True
    If oPais.Provincias.Item(1) Is Nothing Then
        txtProvincia.Text = ""
        sdbcProvi.Text = ""
    Else
        sdbcProvi.Text = oPais.Provincias.Item(1).Cod
        txtProvincia.Text = oPais.Provincias.Item(1).Den
    End If
End If
End Sub

Private Sub cmdAceptar_Click()

If Not oLinea Is Nothing Then
    oLinea.Dest_Den = txtDescripcion.Text
    oLinea.Dest_Dir = txtDireccion.Text
    oLinea.Dest_pai = sdbcPais.Text
    oLinea.Dest_Provi = sdbcProvi.Text
    oLinea.Dest_pob = txtPoblacion.Text
    oLinea.Dest_cp = txtCP.Text
    oLinea.Dest_Tfno = txtTfno.Text
    oLinea.Dest_Fax = txtFAX.Text
    oLinea.Dest_Email = txtEmail.Text
    oLinea.Dest_Modificado = True
End If

Unload Me
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim oDestinos As CDestinos
Dim oPaises As CPaises
Dim oPais As CPais
Dim oProvincia As CProvincia
Dim ADORs As Recordset

Screen.MousePointer = vbHourglass

CargarRecursos
Set oDestinos = Raiz.Generar_CDestinos
Set oPaises = Raiz.Generar_CPaises
Set ADORs = oDestinos.DevolverTodosLosDestinos(sID_Dest, , , , , , , , , , , , True)
If oLinea Is Nothing Then
    'Cargamos el destino concreto
    Do While Not ADORs.EOF
        txtCodigo.Text = ADORs("DESTINOCOD").Value & ""
        txtDescripcion.Text = ADORs("DEN_" & sIdioma_Gen).Value & ""
        txtDireccion.Text = ADORs("DESTINODIR").Value & ""
        txtCP.Text = ADORs("DESTINOCP").Value & ""
        txtPoblacion.Text = ADORs("DESTINOPOB").Value & ""
        If ADORs("DESTINOPAI").Value & "" <> "" Then
            oPaises.CargarTodosLosPaises ADORs("DESTINOPAI").Value & "", , False, , , True
            For Each oPais In oPaises
                txtPais.Text = oPais.Den
            Next
        End If
        Set oPais = Raiz.generar_CPais
        oPais.Cod = ADORs("DESTINOPAI").Value & ""
        If ADORs("DESTINOPROVI").Value & "" <> "" Then
            oPais.CargarTodasLasProvincias ADORs("DESTINOPROVI").Value & "", , True
            txtProvincia.Text = oPais.Provincias.Item(1).Den
        End If
        txtTfno.Text = ADORs("TFNO").Value & ""
        txtFAX.Text = ADORs("FAX").Value & ""
        txtEmail.Text = ADORs("EMAIL").Value & ""
        ADORs.MoveNext
    Loop
Else
    Set m_oPaises = Raiz.Generar_CPaises
    
    m_oPaises.CargarTodosLosPaises , , , , , False
    txtCodigo.Text = oLinea.CodDestino
    If oLinea.Dest_Den = "" Then
        If Not (ADORs.BOF And ADORs.EOF) Then
            txtDescripcion.Text = ADORs("DEN_" & sIdioma_Gen).Value & ""
        End If
    Else
        txtDescripcion.Text = oLinea.Dest_Den
    End If
    txtDireccion.Text = oLinea.Dest_Dir
    txtCP.Text = oLinea.Dest_cp
    txtPoblacion.Text = oLinea.Dest_pob
    sdbcPais.Text = oLinea.Dest_pai
    sdbcProvi.Text = oLinea.Dest_Provi
    If oLinea.Dest_pai & "" <> "" Then
        oPaises.CargarTodosLosPaises oLinea.Dest_pai & "", , False, , , True
        For Each oPais In oPaises
            txtPais.Text = oPais.Den
        Next
    End If
    Set oPais = Raiz.generar_CPais
    oPais.Cod = oLinea.Dest_pai & ""
    If oLinea.Dest_Provi & "" <> "" Then
        oPais.CargarTodasLasProvincias oLinea.Dest_Provi, , True
        txtProvincia.Text = oPais.Provincias.Item(1).Den
    End If
    txtTfno.Text = oLinea.Dest_Tfno
    txtFAX.Text = oLinea.Dest_Fax
    txtEmail.Text = oLinea.Dest_Email
End If

ADORs.Close
Set ADORs = Nothing
Set oDestinos = Nothing
Set oPais = Nothing
Set oPaises = Nothing

ConfigurarSeguridad

Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcPais_CloseUp()
ValidarPais
End Sub

Private Sub sdbcPais_DropDown()
Dim oPai As CPais
''' * Objetivo: Abrir el combo de Paises de la forma adecuada
Screen.MousePointer = vbHourglass

If sdbcPais.Text <> "" Then
    m_oPaises.CargarTodosLosPaisesDesde m_sCargaMaxCb, CStr(sdbcPais.Text), , , False
Else
    m_oPaises.CargarTodosLosPaises , , , , , False
End If

sdbcPais.RemoveAll

For Each oPai In m_oPaises
    sdbcPais.AddItem oPai.Cod & Chr(m_lSeparador) & oPai.Den
Next

If sdbcPais.Rows = 0 Then
    sdbcPais.AddItem ""
End If
Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcPais_InitColumnProps()
sdbcPais.DataFieldList = "Column 0"
sdbcPais.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcPais_LostFocus()
ValidarPais
End Sub

Private Sub sdbcPais_Validate(Cancel As Boolean)

ValidarPais

End Sub


Private Sub ValidarPais()
Dim oPais As CPais
Set oPais = m_oPaises.DevolverPais(UCase(sdbcPais.Text))
If oPais Is Nothing Then
    txtPais.Text = ""
    sdbcPais.Text = ""
Else
    sdbcPais.Text = oPais.Cod
    txtPais.Text = oPais.Den
End If

End Sub

Private Sub sdbcProvi_CloseUp()
ValidarProvincia
End Sub

Private Sub sdbcProvi_DropDown()
Dim oPais As CPais
Dim oProvi As CProvincia

If sdbcPais.Text = "" Then
    sdbcProvi.RemoveAll
    sdbcProvi.AddItem ""
    sdbcProvi.Refresh
    Screen.MousePointer = vbNormal
    Exit Sub
End If

Screen.MousePointer = vbHourglass

If m_oPaises.Item(sdbcPais.Text) Is Nothing Then
    If sdbcPais.Text <> "" Then
        m_oPaises.CargarTodosLosPaisesDesde m_sCargaMaxCb, sdbcPais.Text, , , False
    Else
        m_oPaises.CargarTodosLosPaises , , , , , False
    End If
End If

Set oPais = m_oPaises.Item(sdbcPais.Text)
  
If sdbcProvi.Text <> "" Then
    oPais.CargarTodasLasProvinciasDesde m_sCargaMaxCb, sdbcProvi.Text, , , False
Else
    oPais.CargarTodasLasProvincias
End If

Set m_oProvincias = oPais.Provincias

Set oPais = Nothing

sdbcProvi.RemoveAll

For Each oProvi In m_oProvincias
    sdbcProvi.AddItem oProvi.Cod & Chr(m_lSeparador) & oProvi.Den
Next

If sdbcProvi.Rows = 0 Then
    sdbcProvi.AddItem ""
End If

Screen.MousePointer = vbNormal
End Sub



Private Sub sdbcProvi_InitColumnProps()
sdbcProvi.DataFieldList = "Column 0"
sdbcProvi.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProvi_LostFocus()
ValidarProvincia
End Sub

Private Sub sdbcProvi_Validate(Cancel As Boolean)
ValidarProvincia
End Sub


