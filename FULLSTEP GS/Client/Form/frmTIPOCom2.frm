VERSION 5.00
Begin VB.Form frmTIPOCom2 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tipo de comunicación "
   ClientHeight    =   2565
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3660
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmTIPOCom2.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2565
   ScaleWidth      =   3660
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton optAviso 
      BackColor       =   &H00808000&
      Caption         =   "Aviso de despublicación"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   720
      TabIndex        =   5
      Top             =   695
      Width           =   2310
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   1875
      TabIndex        =   4
      Top             =   2100
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   735
      TabIndex        =   3
      Top             =   2100
      Width           =   1005
   End
   Begin VB.OptionButton optAdj 
      BackColor       =   &H00808000&
      Caption         =   "Comunicación de adjudicación"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   720
      TabIndex        =   2
      Top             =   1605
      Width           =   2600
   End
   Begin VB.OptionButton optObj 
      BackColor       =   &H00808000&
      Caption         =   "Comunicación de objetivos"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   720
      TabIndex        =   1
      Top             =   1150
      Width           =   2600
   End
   Begin VB.OptionButton optOfe 
      BackColor       =   &H00808000&
      Caption         =   "Petición de ofertas"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   720
      TabIndex        =   0
      Top             =   240
      Value           =   -1  'True
      Width           =   2600
   End
End
Attribute VB_Name = "frmTIPOCom2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public OpcionSeleccionada As Integer

Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset
    Dim i As Integer
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_TIPOCOM2, Idioma)
    
    If Not Ador Is Nothing Then
        Caption = Ador(0).Value
        Ador.MoveNext
        optOfe.Caption = Ador(0).Value
        Ador.MoveNext
        optObj.Caption = Ador(0).Value
        Ador.MoveNext
        optAdj.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optAviso.Caption = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub cmdAceptar_Click()
    If optOfe Then
        OpcionSeleccionada = 1
    End If
    If optObj Then
        OpcionSeleccionada = 2
    End If
    If optAdj Then
        OpcionSeleccionada = 3
    End If
    If optAviso Then
        OpcionSeleccionada = 4
    End If
    
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    CargarRecursos
    optObj.Value = True
End Sub


