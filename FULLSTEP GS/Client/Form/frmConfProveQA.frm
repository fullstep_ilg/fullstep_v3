VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfProveQA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Configuración de campos"
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   11100
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmConfProveQA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5820
   ScaleWidth      =   11100
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picConf 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5800
      Left            =   0
      ScaleHeight     =   5805
      ScaleWidth      =   11100
      TabIndex        =   0
      Top             =   0
      Width           =   11100
      Begin VB.PictureBox picBotones 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   0
         ScaleHeight     =   735
         ScaleWidth      =   11100
         TabIndex        =   16
         Top             =   5040
         Width           =   11100
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "D&Cancelar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   6000
            TabIndex        =   18
            Top             =   120
            Width           =   1200
         End
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "D&Aceptar"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   4200
            TabIndex        =   17
            Top             =   120
            Width           =   1200
         End
      End
      Begin VB.PictureBox picCalif 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   0
         ScaleHeight     =   495
         ScaleWidth      =   11100
         TabIndex        =   12
         Top             =   4560
         Width           =   11100
         Begin VB.CheckBox chkMostrarValores 
            BackColor       =   &H00808000&
            Caption         =   "DMostrar valores numéricos de calificaciones"
            Enabled         =   0   'False
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   6315
            TabIndex        =   14
            Top             =   120
            Width           =   4605
         End
         Begin VB.CheckBox chkMostrarCalif 
            BackColor       =   &H00808000&
            Caption         =   "DMostrar calificaciones de proveedores"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   2970
            TabIndex        =   13
            Top             =   120
            Width           =   4000
         End
         Begin VB.Label lblUno 
            BackColor       =   &H00808000&
            Caption         =   "DCalificaciones de proveedores:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   120
            TabIndex        =   15
            Top             =   120
            Width           =   2700
         End
      End
      Begin VB.PictureBox picVarCalidad 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   4455
         Left            =   0
         ScaleHeight     =   4455
         ScaleWidth      =   11055
         TabIndex        =   1
         Top             =   0
         Width           =   11055
         Begin VB.CheckBox chkMostrarValorPuntuacion 
            BackColor       =   &H00808000&
            Caption         =   "DMostrar valor puntuación variable de calidad"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   5715
            TabIndex        =   8
            Top             =   4035
            Width           =   4575
         End
         Begin VB.CheckBox chkMostrarCalifPuntuacion 
            BackColor       =   &H00808000&
            Caption         =   "DMostrar calificación puntuación variable de calidad"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   120
            TabIndex        =   7
            Top             =   4035
            Value           =   1  'Checked
            Width           =   5085
         End
         Begin VB.CheckBox chkMostrarVarsMaterialProceso 
            BackColor       =   &H00808000&
            Caption         =   "DCargar sólo variables material del proceso"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   435
            Width           =   5175
         End
         Begin VB.PictureBox picVarCal 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   120
            ScaleHeight     =   3000
            ScaleWidth      =   5205
            TabIndex        =   4
            Top             =   840
            Width           =   5200
            Begin MSComctlLib.TreeView tvwVarCal 
               Height          =   3000
               Left            =   0
               TabIndex        =   5
               Top             =   0
               Width           =   5205
               _ExtentX        =   9181
               _ExtentY        =   5292
               _Version        =   393217
               LabelEdit       =   1
               Style           =   7
               Checkboxes      =   -1  'True
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.PictureBox picUndNeg 
            BorderStyle     =   0  'None
            Height          =   3000
            Left            =   5715
            ScaleHeight     =   3000
            ScaleWidth      =   5205
            TabIndex        =   2
            Top             =   840
            Width           =   5200
            Begin MSComctlLib.TreeView tvwUndNeg 
               Height          =   3000
               Left            =   0
               TabIndex        =   3
               Top             =   0
               Width           =   5200
               _ExtentX        =   9181
               _ExtentY        =   5292
               _Version        =   393217
               LabelEdit       =   1
               Style           =   7
               Checkboxes      =   -1  'True
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Label lblSelecUnidades 
            BackColor       =   &H00808000&
            Caption         =   "DSeleccione las unidades de negocio donde quiere ver la puntuación:"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   5715
            TabIndex        =   11
            Top             =   435
            Width           =   5235
         End
         Begin VB.Label lblQAUnidades 
            BackColor       =   &H00808000&
            Caption         =   "DQA. Unidades de negocio:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   5715
            TabIndex        =   10
            Top             =   120
            Width           =   3135
         End
         Begin VB.Label lblQAVariables 
            BackColor       =   &H00808000&
            Caption         =   "DQA. Variables de calidad:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   120
            TabIndex        =   9
            Top             =   120
            Width           =   3015
         End
         Begin VB.Line lSep 
            BorderColor     =   &H00FFFFFF&
            X1              =   120
            X2              =   11030
            Y1              =   4440
            Y2              =   4440
         End
      End
   End
End
Attribute VB_Name = "frmConfProveQA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Private Const WM_HSCROLL = &H114
Private Const WM_VSCROLL = &H115
Private Const SB_LEFT = 6
Private Const SB_TOP = 6

Public g_bAltaNueva As Boolean
Public g_oConfProve As CConfProveQA
Public g_bHacerProcesoSeleccionado As Boolean
Public g_oVarsCalidad As CVariablesCalidad
Public m_oVistaCalSeleccionada As CConfVistaCalGlobal
Public g_oUnidadesQa As CUnidadesNegQA
Public g_oUnqaEnProve As CUnidadesNegQA 'coleccion unidades con puntos. Rellenar grids/arboles visibilidad
Public g_sUsuCod As String
Public g_oMensajes As CMensajes
Public g_TipoUsuario As TipoDeUsuario
Public g_sIdioma As String
Public GestorIdiomas As CGestorIdiomas

Private m_oParametrosGenerales As ParametrosGenerales
Private m_oVarCal0 As CVariableCalidad
Private m_oVarCal1 As CVariableCalidad
Private m_oVarCal2 As CVariableCalidad
Private m_oVarCal3 As CVariableCalidad
Private m_oVarCal4 As CVariableCalidad
Private m_oVarCal5 As CVariableCalidad
Private m_lIdNivel0 As Long
Private m_bGrpGenerarEstrucUnQaCambio As Boolean
Private m_bGrpGenerarEstrucUnQaLimpiado As Boolean
Private m_bGrpGenerarEstrucUnQa1ra As Boolean
Private m_bDeshabilidar_Chk_ChangeTodas As Boolean
Private m_variablesCalChequeadas As String
Private m_unidadesOrgChequeadas As String
Private m_puntuacionCalificacion As Long
Private m_calificacionesProveedores As Long

Const POS_ID = 0
Const POS_ID_VARCAL1 = 1
Const POS_ID_VARCAL2 = 2
Const POS_ID_VARCAL3 = 3
Const POS_ID_VARCAL4 = 4
Const POS_NIVEL = 5
Const POS_RESTRICCION = 6
Const POS_R_INF = 7
Const POS_R_SUP = 8
Const POS_AVISO = 9
Const POS_A_INF = 10
Const POS_A_SUP = 11
Const POS_UNQA = 12
Const POS_UNQA_HIJOS = 13
Const POS_UNQA_NIVEL = 14
Const POS_TIPO = 15
Const POS_SUBTIPO = 16

Public Property Get ParamGenerales() As ParametrosGenerales
    ParamGenerales = m_oParametrosGenerales
End Property

Public Property Let ParamGenerales(ByRef ParamGen As ParametrosGenerales)
    m_oParametrosGenerales = ParamGen
End Property

Private Function obtenerCodigoNodo(ByVal node As MSComctlLib.node) As String
    Dim sDatos_Variable() As String
    Dim sCodigo As String
    
    sDatos_Variable = Split(node.Tag, "#")

    Select Case sDatos_Variable(POS_NIVEL)
        Case 0
            sCodigo = CStr(0) & "-" & sDatos_Variable(POS_ID)
        Case 1
            sCodigo = CStr(1) & "-" & sDatos_Variable(POS_ID)
        Case 2
            sCodigo = CStr(2) & "-" & sDatos_Variable(POS_ID)
        Case 3
            sCodigo = CStr(3) & "-" & sDatos_Variable(POS_ID)
        Case 4
            sCodigo = CStr(4) & "-" & sDatos_Variable(POS_ID)
        Case 5
            sCodigo = CStr(5) & "-" & sDatos_Variable(POS_ID)
    End Select
        
    obtenerCodigoNodo = sCodigo
End Function

Private Function obtenerCodigoNodo2(ByVal node As MSComctlLib.node) As String
    Dim sDatos_Variable() As String
    
    sDatos_Variable = Split(node.Tag, "#")
    obtenerCodigoNodo2 = sDatos_Variable(POS_ID)
End Function

Private Sub chkMostrarCalif_Click()
    If chkMostrarCalif Then
        chkMostrarValores.Enabled = True
    Else
        chkMostrarValores.Enabled = False
        chkMostrarValores.Value = 0
    End If
End Sub

Private Sub chkMostrarVarsMaterialProceso_Click()
    GenerarEstructuraVarCal
End Sub

Private Sub cmdAceptar_Click()
    Dim variablesCalChequeadas As String
    Dim unidadesOrgChequeadas As String
    Dim puntuacionCalificacion As Long
    Dim calificacionesProveedores As Long
    Dim tsError As TipoErrorSummit
    
    variablesCalChequeadas = ObtenerVariablesChequeadas
    unidadesOrgChequeadas = ObtenerUnidadesChequeadas
    
    If chkMostrarCalif And chkMostrarValores Then
        calificacionesProveedores = 2
    ElseIf chkMostrarCalif Then
        calificacionesProveedores = 1
    Else
        calificacionesProveedores = 0
    End If
    
    If chkMostrarCalifPuntuacion And chkMostrarValorPuntuacion Then
        puntuacionCalificacion = 3
    ElseIf chkMostrarCalifPuntuacion Then
        puntuacionCalificacion = 1
    ElseIf chkMostrarValorPuntuacion Then
        puntuacionCalificacion = 2
    Else
        puntuacionCalificacion = 0
    End If
    
    If (g_bAltaNueva) Then
        tsError = g_oConfProve.AnyadirConfUsuario(g_sUsuCod, variablesCalChequeadas, unidadesOrgChequeadas, puntuacionCalificacion, calificacionesProveedores)
        g_bAltaNueva = False
    Else
        tsError = g_oConfProve.ModificarConfUsuario(g_sUsuCod, variablesCalChequeadas, unidadesOrgChequeadas, puntuacionCalificacion, calificacionesProveedores)
    End If
    
    If tsError.NumError <> TESnoerror Then
        TratarError tsError, g_oMensajes, m_oParametrosGenerales
    End If
    
    m_variablesCalChequeadas = variablesCalChequeadas
    m_unidadesOrgChequeadas = unidadesOrgChequeadas
    m_puntuacionCalificacion = puntuacionCalificacion
    m_calificacionesProveedores = calificacionesProveedores
    
    g_oConfProve.variablesCalChequeadas = m_variablesCalChequeadas
    g_oConfProve.unidadesOrgChequeadas = m_unidadesOrgChequeadas
    g_oConfProve.puntuacionCalificacion = m_puntuacionCalificacion
    g_oConfProve.calificacionesProveedores = m_calificacionesProveedores
        
    g_bHacerProcesoSeleccionado = True
    
    Me.Hide
End Sub

Private Function ObtenerUnidadesChequeadas() As String
    Dim codNodo As String
    Dim nodx As MSComctlLib.node
    Dim listaUnidadesSeleccionadas As String
    
    listaUnidadesSeleccionadas = ""
    
    For Each nodx In Me.tvwUndNeg.Nodes
        If nodx.Checked Then
            codNodo = obtenerCodigoNodo2(nodx)
            listaUnidadesSeleccionadas = listaUnidadesSeleccionadas + "," + codNodo
        End If
    Next
        
    If (listaUnidadesSeleccionadas <> "") Then
        ObtenerUnidadesChequeadas = Right(listaUnidadesSeleccionadas, Len(listaUnidadesSeleccionadas) - 1)
    Else
        ObtenerUnidadesChequeadas = listaUnidadesSeleccionadas
    End If
End Function

Private Function ObtenerVariablesChequeadas() As String
    Dim codNodo As String
    Dim nodx As MSComctlLib.node
    Dim listaVariablesSeleccionadas As String
    
    listaVariablesSeleccionadas = ""
    
    For Each nodx In Me.tvwVarCal.Nodes
        If nodx.Checked Then
            codNodo = obtenerCodigoNodo(nodx)
            listaVariablesSeleccionadas = listaVariablesSeleccionadas + "," + codNodo
        End If
    Next
        
    If (listaVariablesSeleccionadas <> "") Then
        ObtenerVariablesChequeadas = Right(listaVariablesSeleccionadas, Len(listaVariablesSeleccionadas) - 1)
    Else
        ObtenerVariablesChequeadas = listaVariablesSeleccionadas
    End If
End Function

Private Sub cmdCancelar_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    CargarRecursos
    
    If Not g_bAltaNueva Then
        'Usuario con configuracion definida
        m_variablesCalChequeadas = g_oConfProve.variablesCalChequeadas()
        m_unidadesOrgChequeadas = g_oConfProve.unidadesOrgChequeadas()
        m_puntuacionCalificacion = g_oConfProve.puntuacionCalificacion()
        m_calificacionesProveedores = g_oConfProve.calificacionesProveedores()
        
        Select Case m_puntuacionCalificacion
            Case 0
                chkMostrarCalifPuntuacion.Value = 0
                chkMostrarValorPuntuacion.Value = 0
            Case 1
                chkMostrarCalifPuntuacion.Value = 1
                chkMostrarValorPuntuacion.Value = 0
            Case 2
                chkMostrarCalifPuntuacion.Value = 0
                chkMostrarValorPuntuacion.Value = 1
            Case 3
                chkMostrarCalifPuntuacion.Value = 1
                chkMostrarValorPuntuacion.Value = 1
        End Select
        
        Select Case m_calificacionesProveedores
            Case 0
                chkMostrarCalif.Value = 0
                chkMostrarValores.Value = 0
            Case 1
                chkMostrarCalif.Value = 1
                chkMostrarValores.Value = 0
                chkMostrarValores.Enabled = True
            Case 2
                chkMostrarCalif.Value = 1
                chkMostrarValores.Value = 1
                chkMostrarValores.Enabled = True
        End Select
    End If
    
    If m_oParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
        GenerarEstructuraVarCal
        GrpGenerarEstructuraUnidadesNegocio
    End If
End Sub

Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_CONFPROVEQA, g_sIdioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        lblQAVariables.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblQAUnidades.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        chkMostrarVarsMaterialProceso.Caption = Ador(0).Value
        Ador.MoveNext
        lblSelecUnidades.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        chkMostrarCalifPuntuacion.Caption = Ador(0).Value
        Ador.MoveNext
        chkMostrarValorPuntuacion.Caption = Ador(0).Value
        Ador.MoveNext
        lblUno.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        chkMostrarCalif.Caption = Ador(0).Value
        Ador.MoveNext
        chkMostrarValores.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Public Function estaVarCalEnConfiguracion(ByVal VarCal As String) As Boolean
    estaVarCalEnConfiguracion = IIf(Not (InStr(m_variablesCalChequeadas, VarCal) = 0), True, False)
End Function

Public Function estaUnOrgEnConfiguracion(ByVal UnOrg As String) As Boolean
    estaUnOrgEnConfiguracion = IIf(Not (InStr(m_unidadesOrgChequeadas, UnOrg) = 0), True, False)
End Function

Private Sub GenerarEstructuraVarCal()
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    Dim nodx As MSComctlLib.node
    Dim bVisibleVista As Boolean
    Dim bOcultoPorMat As Boolean
    Dim bTodosCheck As Boolean
    
    tvwVarCal.Nodes.Clear
    
    If Not g_oVarsCalidad Is Nothing Then
        For Each m_oVarCal1 In g_oVarsCalidad
                
            If Me.chkMostrarVarsMaterialProceso Or (m_oVarCal1.Nivel <> 1 And g_bAltaNueva) Then
                bVisibleVista = True
            Else
                bVisibleVista = False
            End If
            
            bOcultoPorMat = False
            
            If (m_oVarCal1.UsuConsultar Or g_TipoUsuario = TipoDeUsuario.Administrador) Then
                If m_oVarCal1.Nivel = 0 Then
                    Set nodx = tvwVarCal.Nodes.Add(, , "NIV0" & CStr(m_oVarCal1.Id), m_oVarCal1.Denominaciones.Item(g_sIdioma).Den)
                    
                    nodx.Tag = m_oVarCal1.Id & "#####0"
                    
                    scod0 = CStr(0) & "-" & CStr(m_oVarCal1.Id)
                    
                    If Me.chkMostrarVarsMaterialProceso Then bVisibleVista = True
                    
                    If bVisibleVista Then
                        nodx.Checked = True
                    Else
                        If (estaVarCalEnConfiguracion(scod0)) Then
                            nodx.Checked = True
                        Else
                            nodx.Checked = False
                        End If
                        
                        bTodosCheck = False
                    End If
                    
                    nodx.EnsureVisible
                    nodx.Expanded = True
                Else
                    'La de nivel 0 siempre tiene el usuario permiso de consulta-> las de nivel 1 siempre tienen un padre
                    scod1 = CStr(1) & "-" & CStr(m_oVarCal1.Id)
                    
                    If Me.chkMostrarVarsMaterialProceso And Not m_oVarCal1.MaterialProceso Then
                        bOcultoPorMat = True
                    End If
                                             
                    If bOcultoPorMat = False Then
                        Set nodx = tvwVarCal.Nodes.Add("NIV0" & CStr(m_lIdNivel0), tvwChild, "NIV1" & CStr(m_oVarCal1.Id), m_oVarCal1.Denominaciones.Item(g_sIdioma).Den)
                    
                        nodx.Tag = m_oVarCal1.Id & "#####1"
                                                
                        If bVisibleVista Then
                            nodx.Checked = True
                        Else
                            If (estaVarCalEnConfiguracion(scod1)) Then
                                nodx.Checked = True
                            Else
                                nodx.Checked = False
                            End If
                            bTodosCheck = False
                        End If
                        
                        nodx.EnsureVisible
                        nodx.Expanded = True
                    End If
                End If
            End If
                                            
            If m_oVarCal1.Nivel > 0 Then
                If Not m_oVarCal1.VariblesCal Is Nothing Then
                    If (m_oVarCal1.UsuConsultar Or g_TipoUsuario = TipoDeUsuario.Administrador) _
                     And bOcultoPorMat = False Then
                        For Each m_oVarCal2 In m_oVarCal1.VariblesCal
                            bOcultoPorMat = False
                                                    
                            scod2 = CStr(2) & "-" & CStr(m_oVarCal2.Id)
                            
                            If (m_oVarCal2.UsuConsultar Or g_TipoUsuario = TipoDeUsuario.Administrador) Then
                                If Me.chkMostrarVarsMaterialProceso And Not m_oVarCal2.MaterialProceso Then
                                    bOcultoPorMat = True
                                End If
                                                                
                                If bOcultoPorMat = False Then
                                    Set nodx = tvwVarCal.Nodes.Add("NIV1" & CStr(m_oVarCal1.Id), tvwChild, "NIV2" & CStr(m_oVarCal2.Id), m_oVarCal2.Denominaciones.Item(g_sIdioma).Den)
                                    
                                    nodx.Tag = m_oVarCal2.Id & "#" & m_oVarCal1.Id & "####2"
                                    
                                    If bVisibleVista Then
                                        nodx.Checked = True
                                    Else
                                        If (estaVarCalEnConfiguracion(scod2)) Then
                                            nodx.Checked = True
                                        Else
                                            nodx.Checked = False
                                        End If
                                        bTodosCheck = False
                                    End If
                                    
                                    nodx.EnsureVisible
                                    nodx.Expanded = True
                                End If
                                                    
                                If Not m_oVarCal2.VariblesCal Is Nothing And bOcultoPorMat = False Then
                                    For Each m_oVarCal3 In m_oVarCal2.VariblesCal
                                        bOcultoPorMat = False
                                                                        
                                        scod3 = CStr(3) & "-" & CStr(m_oVarCal3.Id)
                                        
                                        If (m_oVarCal3.UsuConsultar Or g_TipoUsuario = TipoDeUsuario.Administrador) Then
                                            If Me.chkMostrarVarsMaterialProceso And Not m_oVarCal3.MaterialProceso Then
                                                bOcultoPorMat = True
                                            End If
                                                                                    
                                            If bOcultoPorMat = False Then
                                                Set nodx = tvwVarCal.Nodes.Add("NIV2" & CStr(m_oVarCal2.Id), tvwChild, "NIV3" & CStr(m_oVarCal3.Id), m_oVarCal3.Denominaciones.Item(g_sIdioma).Den)
                                                
                                                nodx.Tag = m_oVarCal3.Id & "#" & m_oVarCal1.Id & "#" & m_oVarCal2.Id & "###3"
                                                                                            
                                                If bVisibleVista Then
                                                    nodx.Checked = True
                                                Else
                                                    If (estaVarCalEnConfiguracion(scod3)) Then
                                                        nodx.Checked = True
                                                    Else
                                                        nodx.Checked = False
                                                    End If
                                                    bTodosCheck = False
                                                End If
                                                
                                                nodx.EnsureVisible
                                                nodx.Expanded = True
                                            End If
                                
                                            If Not m_oVarCal3.VariblesCal Is Nothing And bOcultoPorMat = False Then
                                                For Each m_oVarCal4 In m_oVarCal3.VariblesCal
            
                                                    bOcultoPorMat = False
                                                    
                                                    scod4 = CStr(4) & "-" & CStr(m_oVarCal4.Id)
                                                    
                                                    If (m_oVarCal4.UsuConsultar Or g_TipoUsuario = TipoDeUsuario.Administrador) Then
                                                        If Me.chkMostrarVarsMaterialProceso And Not m_oVarCal4.MaterialProceso Then
                                                            bOcultoPorMat = True
                                                        End If
                                                            
                                                        If bOcultoPorMat = False Then
                                                            Set nodx = tvwVarCal.Nodes.Add("NIV3" & CStr(m_oVarCal3.Id), tvwChild, "NIV4" & CStr(m_oVarCal4.Id), m_oVarCal4.Denominaciones.Item(g_sIdioma).Den)
                                                            
                                                            nodx.Tag = m_oVarCal4.Id & "#" & m_oVarCal1.Id & "#" & m_oVarCal2.Id & "#" & m_oVarCal3.Id & "##4"
                                                            
                                                            If bVisibleVista Then
                                                                nodx.Checked = True
                                                            Else
                                                                If (estaVarCalEnConfiguracion(scod4)) Then
                                                                    nodx.Checked = True
                                                                Else
                                                                    nodx.Checked = False
                                                                End If

                                                                bTodosCheck = False
                                                            End If
                                                            
                                                            nodx.EnsureVisible
                                                            nodx.Expanded = True
                                                        End If
                                                                
                                                        If Not m_oVarCal4.VariblesCal Is Nothing And bOcultoPorMat = False Then
                                                            For Each m_oVarCal5 In m_oVarCal4.VariblesCal
                                                                bOcultoPorMat = False
                                                                
                                                                scod5 = CStr(5) & "-" & CStr(m_oVarCal5.Id)
                                                               
                                                                If (m_oVarCal5.UsuConsultar Or g_TipoUsuario = TipoDeUsuario.Administrador) Then
                                                                    If Me.chkMostrarVarsMaterialProceso And Not m_oVarCal5.MaterialProceso Then
                                                                        bOcultoPorMat = True
                                                                    End If
                                                                            
                                                                    If bOcultoPorMat = False Then
                                                                        Set nodx = tvwVarCal.Nodes.Add("NIV4" & CStr(m_oVarCal4.Id), tvwChild, "NIV5" & CStr(m_oVarCal5.Id), m_oVarCal5.Denominaciones.Item(g_sIdioma).Den)
                                                                    
                                                                        nodx.Tag = m_oVarCal5.Id & "#" & m_oVarCal1.Id & "#" & m_oVarCal2.Id & "#" & m_oVarCal3.Id & "#" & m_oVarCal4.Id & "#5"
                                                                    
                                                                        If bVisibleVista Then
                                                                            nodx.Checked = True
                                                                        Else
                                                                            If (estaVarCalEnConfiguracion(scod5)) Then
                                                                                nodx.Checked = True
                                                                            Else
                                                                                nodx.Checked = False
                                                                            End If

                                                                            bTodosCheck = False
                                                                        End If
                                                                        
                                                                        nodx.EnsureVisible
                                                                        nodx.Expanded = True
                                                                    End If
                                                                End If 'If (m_oVarCal5.UsuConsultar
                                                            Next 'For Each m_oVarCal5
                                                        End If 'If Not m_oVarCal4.VariblesCal
                                                    End If 'If (m_oVarCal4.UsuConsultar
                                                Next 'For Each m_oVarCal4
                                            End If 'If Not m_oVarCal3.VariblesCal
                                        End If 'If (m_oVarCal3.UsuConsultar
                                    Next 'For Each m_oVarCal3
                                End If 'If Not m_oVarCal2.VariblesCal
                            End If 'If (m_oVarCal2.UsuConsultar
                        Next 'For Each m_oVarCal2
                    End If 'If (m_oVarCal1.UsuConsultar
                End If 'If Not m_oVarCal1.VariblesCal
            End If 'If m_oVarCal1.Nivel > 0

        Next
               
        'Scroll arriba y a la izq.
        SendMessage tvwVarCal.hwnd, WM_HSCROLL, SB_LEFT, 0
        SendMessage tvwVarCal.hwnd, WM_VSCROLL, SB_TOP, 0
    End If
End Sub

Private Sub GrpGenerarEstructuraUnidadesNegocio()
    Dim oNegocio As CUnidadNegQA
    Dim oNegocio2 As CUnidadNegQA
    Dim oNegocio3 As CUnidadNegQA
    
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    
    Dim nodx As MSComctlLib.node
    Dim bNiv0 As Boolean
    
    Dim bTodosCheck As Boolean
    Dim bAlgunaUnqa As Boolean
    
    Dim pruebaHayHijosSelec As Boolean

    Screen.MousePointer = vbHourglass
    
    Me.tvwUndNeg.Visible = True
        
    tvwUndNeg.Nodes.Clear
    
    bNiv0 = False
    
    bTodosCheck = True
    
    If m_bGrpGenerarEstrucUnQaCambio Then
        m_bGrpGenerarEstrucUnQaLimpiado = False
    End If
        
    For Each oNegocio In g_oUnidadesQa.Unidades
        If Not bNiv0 Then
            bNiv0 = True
            
            scod0 = CStr(oNegocio.Id)
            Set nodx = tvwUndNeg.Nodes.Add(, , "UN0" & scod0, CStr(oNegocio.Cod) & " - " & oNegocio.Den)
            nodx.Tag = scod0
            nodx.EnsureVisible
            nodx.Expanded = True
            If (estaUnOrgEnConfiguracion(scod0) Or g_bAltaNueva) Then
                nodx.Checked = True
            End If

        Else
            scod1 = CStr(oNegocio.Id)
            
            bAlgunaUnqa = AlgunaUnqa(scod1)
            
            If oNegocio.Seleccionada Then
                bAlgunaUnqa = True
            End If
                       
            If oNegocio.tieneAlgunaUNQASeleccionada Then
                Set nodx = tvwUndNeg.Nodes.Add("UN0" & scod0, tvwChild, "UN1" & scod1, CStr(oNegocio.Cod) & " - " & oNegocio.Den)
                nodx.Tag = scod1
                nodx.EnsureVisible
                nodx.Expanded = True
                
                If Not m_bGrpGenerarEstrucUnQa1ra Or m_bGrpGenerarEstrucUnQaCambio Then
                    nodx.Checked = False
                    
                    bTodosCheck = False
                Else
                    nodx.Checked = UnqaEstaEnVista(oNegocio.Id, 0, m_lIdNivel0, 0, 0, 0, 0)
                    
                    If nodx.Checked = False Then bTodosCheck = False
                End If

                If (estaUnOrgEnConfiguracion(scod1)) Then
                    nodx.Checked = True
                End If
        
                For Each oNegocio2 In oNegocio.UnidadesNegQA.Unidades
                    scod2 = CStr(oNegocio2.Id)
                        
                    bAlgunaUnqa = AlgunaUnqa(scod2)
                    
                    If oNegocio2.tieneAlgunaUNQASeleccionada Then
                        
                        Set nodx = tvwUndNeg.Nodes.Add("UN1" & scod1, tvwChild, "UN2" & scod2, CStr(oNegocio2.Cod) & " - " & oNegocio2.Den)
                        
                        nodx.Tag = scod2
                        nodx.EnsureVisible
                        nodx.Expanded = True
            
                        If Not m_bGrpGenerarEstrucUnQa1ra Or m_bGrpGenerarEstrucUnQaCambio Then
                            nodx.Checked = False
                            
                            bTodosCheck = False
                        Else
                            nodx.Checked = UnqaEstaEnVista(oNegocio2.Id, 0, m_lIdNivel0, 0, 0, 0, 0)
                            
                            If nodx.Checked = False Then bTodosCheck = False
                        End If
                        
                        If (estaUnOrgEnConfiguracion(scod2)) Then
                            nodx.Checked = True
                        End If
                                    
                        For Each oNegocio3 In oNegocio2.UnidadesNegQA.Unidades
                            scod3 = CStr(oNegocio3.Id)
                            
                            bAlgunaUnqa = AlgunaUnqa(scod3)
                            
                            If oNegocio3.tieneAlgunaUNQASeleccionada Then
                            
                                Set nodx = tvwUndNeg.Nodes.Add("UN2" & scod2, tvwChild, "UN3" & scod3, CStr(oNegocio3.Cod) & " - " & oNegocio3.Den)
                                nodx.Tag = scod3
                                nodx.EnsureVisible
                                nodx.Expanded = True
                                
                                If Not m_bGrpGenerarEstrucUnQa1ra Or m_bGrpGenerarEstrucUnQaCambio Then
                                    nodx.Checked = False
                                    
                                    bTodosCheck = False
                                Else
                                    nodx.Checked = UnqaEstaEnVista(oNegocio3.Id, 0, m_lIdNivel0, 0, 0, 0, 0)
                                    
                                    If nodx.Checked = False Then bTodosCheck = False
                                End If
                                If (estaUnOrgEnConfiguracion(scod3)) Then
                                    nodx.Checked = True
                                End If
                            End If 'If bAlgunaUnqa Then
                        Next 'For Each oNegocio3
                    End If 'If bAlgunaUnqa Then
                Next 'For Each oNegocio2
            End If 'If bAlgunaUnqa Then
        End If 'If Not bNiv0 Then
    Next 'For Each oNegocio
        
    m_bDeshabilidar_Chk_ChangeTodas = True
    
    'Scroll arriba y a la izq.
    SendMessage tvwUndNeg.hwnd, WM_HSCROLL, SB_LEFT, 0
    SendMessage tvwUndNeg.hwnd, WM_VSCROLL, SB_TOP, 0
    
    m_bDeshabilidar_Chk_ChangeTodas = False
        
    m_bGrpGenerarEstrucUnQaCambio = False
            
    Set oNegocio = Nothing
    Set oNegocio2 = Nothing
    Set oNegocio3 = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Function AlgunaUnqa(ByVal IdUnqa As Long) As Boolean
    AlgunaUnqa = IIf(Not (g_oUnqaEnProve.Item(CStr(IdUnqa)) Is Nothing), True, False)
End Function

Private Function UnqaEstaEnVista(ByVal Unqa As Long, ByVal Nivel As Integer, ByVal Id As Long, ByVal VC1 As Long, _
        ByVal VC2 As Long, ByVal VC3 As Long, ByVal VC4 As Long) As Boolean
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    Select Case Nivel
        Case 0
            scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(Id)
            UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).UnQas, "," & Unqa & ",") > 0)
        Case 1
            scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(Id)
            UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).UnQas, "," & Unqa & ",") > 0)
        Case 2
            scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(VC1)
            scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(Id)
            UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).UnQas, "," & Unqa & ",") > 0)
        Case 3
            scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(VC1)
            scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(VC2)
            scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(Id)
            UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).UnQas, "," & Unqa & ",") > 0)
        Case 4
            scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(VC1)
            scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(VC2)
            scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(VC3)
            scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(Id)
            UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).UnQas, "," & Unqa & ",") > 0)
        Case 5
            scod0 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(1) & CStr(VC1)
            scod2 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(2) & CStr(VC2)
            scod3 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(3) & CStr(VC3)
            scod4 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(4) & CStr(VC4)
            scod5 = CStr(m_oVistaCalSeleccionada.Vista) & CStr(5) & CStr(Id)
            UnqaEstaEnVista = (InStr(m_oVistaCalSeleccionada.ConfVistasCalGlobalVar.Item(scod0).VariblesCalVista.Item(scod1).VariblesCalVista.Item(scod2).VariblesCalVista.Item(scod3).VariblesCalVista.Item(scod4).VariblesCalVista.Item(scod5).UnQas, "," & Unqa & ",") > 0)
    End Select
End Function

Private Sub Form_Resize()
    picConf.Width = Me.Width
    picConf.Height = Me.Height
        
    picVarCalidad.Visible = (m_oParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso)
    If picVarCalidad.Visible Then
        picCalif.Top = picVarCalidad.Top + picVarCalidad.Height
    Else
        picCalif.Top = picVarCalidad.Top
    End If
    picBotones.Top = picCalif.Top + picCalif.Height
    
    Me.Height = picBotones.Top + picBotones.Height + 400
End Sub

Private Sub tvwUndNeg_NodeCheck(ByVal node As MSComctlLib.node)
    Dim sDatos_Variable() As String
    
    sDatos_Variable = Split(node.Tag, "#")
      
    Me.tvwUndNeg.SelectedItem = node
End Sub


Private Sub tvwUndNeg_NodeClick(ByVal node As MSComctlLib.node)
    node.Checked = Not node.Checked
    
    tvwUndNeg_NodeCheck node
End Sub

Private Sub tvwVarCal_NodeClick(ByVal node As MSComctlLib.node)
    node.Checked = Not node.Checked
    
    tvwVarCal_NodeCheck node
End Sub


Private Sub tvwVarCal_NodeCheck(ByVal node As MSComctlLib.node)
    Dim sDatos_Variable() As String
    Dim scod0 As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    sDatos_Variable = Split(node.Tag, "#")

    Select Case sDatos_Variable(POS_NIVEL)
        Case 0
            scod0 = CStr(0) & sDatos_Variable(POS_ID)
    
        Case 1
            scod0 = CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(1) & sDatos_Variable(POS_ID)
    
        Case 2
            scod0 = CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
            scod2 = CStr(2) & sDatos_Variable(POS_ID)
    
        Case 3
            scod0 = CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
            scod2 = CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
            scod3 = CStr(3) & sDatos_Variable(POS_ID)
    
        Case 4
            scod0 = CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
            scod2 = CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
            scod3 = CStr(3) & sDatos_Variable(POS_ID_VARCAL3)
            scod4 = CStr(4) & sDatos_Variable(POS_ID)
    
        Case 5
            scod0 = CStr(0) & CStr(m_lIdNivel0)
            scod1 = CStr(1) & sDatos_Variable(POS_ID_VARCAL1)
            scod2 = CStr(2) & sDatos_Variable(POS_ID_VARCAL2)
            scod3 = CStr(3) & sDatos_Variable(POS_ID_VARCAL3)
            scod4 = CStr(4) & sDatos_Variable(POS_ID_VARCAL4)
            scod5 = CStr(5) & sDatos_Variable(POS_ID)
    End Select
    
    Me.tvwVarCal.SelectedItem = node
End Sub
