VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAsistentesReunion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asistentes de la reuni�n"
   ClientHeight    =   4875
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3405
   Icon            =   "frmAsistentesReunion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4875
   ScaleWidth      =   3405
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ListView lstAsistentes 
      Height          =   4815
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   3345
      _ExtentX        =   5900
      _ExtentY        =   8493
      View            =   1
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      OLEDragMode     =   1
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483625
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      OLEDragMode     =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   17
      ImageHeight     =   17
      MaskColor       =   16777215
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentesReunion.frx":0442
            Key             =   "CONECTADO"
            Object.Tag             =   "CONECTADO"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAsistentesReunion.frx":07E2
            Key             =   "DESCONECTADO"
            Object.Tag             =   "DESCONECTADO"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmAsistentesReunion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public ReuSeleccionada As CReunion

Private Sub Form_Load()
    CargarRecursos
    VerAsistentes
End Sub

Public Sub VerAsistentes()
    Dim adoAsistentes As ador.Recordset
        
    Set adoAsistentes = ReuSeleccionada.DevolverAsistentesReunion
    
    lstAsistentes.Icons = ImageList1
    
    lstAsistentes.ListItems.Clear
    While Not adoAsistentes.EOF
        If adoAsistentes("CONECTADO").Value = 1 Then

            lstAsistentes.ListItems.Add , , IIf(IsNull(Trim(adoAsistentes("CODIGO").Value)), "Administrador (adm", Trim(adoAsistentes("NOMBRE").Value) & " " & Trim(adoAsistentes("APELLIDO").Value) & " (" & Trim(adoAsistentes("CODIGO").Value)) & ")", , ImageList1.ListImages.Item("CONECTADO").Index
        Else

            lstAsistentes.ListItems.Add , , IIf(IsNull(Trim(adoAsistentes("CODIGO").Value)), "Administrador (adm", Trim(adoAsistentes("NOMBRE").Value) & " " & Trim(adoAsistentes("APELLIDO").Value) & " (" & Trim(adoAsistentes("CODIGO").Value)) & ")", , ImageList1.ListImages.Item("DESCONECTADO").Index
        End If
        adoAsistentes.MoveNext
    Wend
    adoAsistentes.Close
    Set adoAsistentes = Nothing

End Sub
Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = GestorIdiomas.DevolverTextosDelModulo(FRM_ASISTENTESREUNION, Idioma) 'oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
    
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.Close
    End If
    
    Set ador = Nothing
End Sub

