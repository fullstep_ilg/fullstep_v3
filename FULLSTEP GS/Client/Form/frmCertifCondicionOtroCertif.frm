VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCertifCondicionOtroCertif 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DCondiciones para bifurcaci�n a etapa:"
   ClientHeight    =   3840
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   13380
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCertifCondicionOtroCertif.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3840
   ScaleWidth      =   13380
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin SSDataWidgets_B.SSDBDropDown sdbddCampo 
      Height          =   555
      Left            =   1800
      TabIndex        =   11
      Top             =   1200
      Width           =   3495
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "TIPO"
      Columns(0).Name =   "TIPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   6165
      Columns(2).Caption=   "CAMPO"
      Columns(2).Name =   "CAMPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   6174
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddCampoValor 
      Height          =   555
      Left            =   7920
      TabIndex        =   10
      Top             =   1680
      Width           =   3495
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "TIPO"
      Columns(0).Name =   "TIPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   6165
      Columns(2).Caption=   "CAMPO_VALOR"
      Columns(2).Name =   "CAMPO_VALOR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   6174
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddGrupoValor 
      Height          =   555
      Left            =   6840
      TabIndex        =   3
      Top             =   1200
      Width           =   3500
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6165
      Columns(1).Caption=   "GRUPO"
      Columns(1).Name =   "GRUPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6174
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValorBooleano 
      Height          =   555
      Left            =   5640
      TabIndex        =   2
      Top             =   960
      Width           =   1215
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns(0).Width=   3201
      Columns(0).Caption=   "VALOR"
      Columns(0).Name =   "VALOR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   2143
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddOperador 
      Height          =   555
      Left            =   3600
      TabIndex        =   1
      Top             =   1560
      Width           =   3500
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   6165
      Columns(0).Caption=   "OPERADOR"
      Columns(0).Name =   "OPERADOR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   6174
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddGrupo 
      Height          =   555
      Left            =   1200
      TabIndex        =   0
      Top             =   960
      Width           =   3500
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6165
      Columns(1).Caption=   "GRUPO"
      Columns(1).Name =   "GRUPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin VB.TextBox txtFormula 
      Height          =   285
      Left            =   960
      MaxLength       =   1000
      TabIndex        =   7
      Top             =   3420
      Width           =   11145
   End
   Begin VB.CommandButton cmdAyuda 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   312
      Left            =   12210
      Picture         =   "frmCertifCondicionOtroCertif.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   3420
      Width           =   432
   End
   Begin VB.CommandButton cmdAnyadirCondicion 
      Height          =   312
      Left            =   12780
      Picture         =   "frmCertifCondicionOtroCertif.frx":0EE3
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   120
      Width           =   432
   End
   Begin VB.CommandButton cmdEliminarCondicion 
      Height          =   312
      Left            =   12780
      Picture         =   "frmCertifCondicionOtroCertif.frx":0F65
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   480
      Width           =   432
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCondiciones 
      Height          =   3255
      Left            =   60
      TabIndex        =   9
      Top             =   60
      Width           =   12615
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   16
      stylesets.count =   6
      stylesets(0).Name=   "No"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCertifCondicionOtroCertif.frx":0FF7
      stylesets(0).AlignmentPicture=   2
      stylesets(1).Name=   "S�"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCertifCondicionOtroCertif.frx":1013
      stylesets(1).AlignmentPicture=   2
      stylesets(2).Name=   "Gris"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmCertifCondicionOtroCertif.frx":102F
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmCertifCondicionOtroCertif.frx":104B
      stylesets(4).Name=   "ActiveRow"
      stylesets(4).ForeColor=   16777215
      stylesets(4).BackColor=   8388608
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmCertifCondicionOtroCertif.frx":1067
      stylesets(5).Name=   "Amarillo"
      stylesets(5).BackColor=   12648447
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmCertifCondicionOtroCertif.frx":1083
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowAddNew     =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      Columns.Count   =   16
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   1826
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   5
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID_GRUPO"
      Columns(2).Name =   "ID_GRUPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   3
      Columns(2).FieldLen=   256
      Columns(3).Width=   2064
      Columns(3).Caption=   "DGrupo"
      Columns(3).Name =   "GRUPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   3
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "ID_CAMPO"
      Columns(4).Name =   "ID_CAMPO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   3
      Columns(4).FieldLen=   256
      Columns(5).Width=   3519
      Columns(5).Caption=   "DCampo"
      Columns(5).Name =   "CAMPO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   3
      Columns(6).Width=   1640
      Columns(6).Caption=   "DOperador"
      Columns(6).Name =   "OPERADOR"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Style=   3
      Columns(7).Width=   1561
      Columns(7).Caption=   "DDirecto"
      Columns(7).Name =   "DIRECTO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   11
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(8).Width=   5080
      Columns(8).Caption=   "DValor"
      Columns(8).Name =   "VALOR"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "TIPO_VALOR"
      Columns(9).Name =   "TIPO_VALOR"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   2
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ID_GRUPO_VALOR"
      Columns(10).Name=   "ID_GRUPO_VALOR"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   3
      Columns(10).FieldLen=   256
      Columns(11).Width=   2461
      Columns(11).Caption=   "DGrupo"
      Columns(11).Name=   "GRUPO_VALOR"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).Style=   3
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "ID_CAMPO_VALOR"
      Columns(12).Name=   "ID_CAMPO_VALOR"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   3
      Columns(12).FieldLen=   256
      Columns(13).Width=   3016
      Columns(13).Caption=   "CAMPO_VALOR"
      Columns(13).Name=   "CAMPO_VALOR"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "FECACT"
      Columns(14).Name=   "FECACT"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   7
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "TIPO_CAMPO"
      Columns(15).Name=   "TIPO_CAMPO"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   2
      Columns(15).FieldLen=   256
      _ExtentX        =   22251
      _ExtentY        =   5741
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblCondicion 
      BackColor       =   &H00808000&
      Caption         =   "Condici�n"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   60
      TabIndex        =   6
      Top             =   3420
      Width           =   1035
   End
End
Attribute VB_Name = "frmCertifCondicionOtroCertif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public GestorSeguridad As CGestorSeguridad
Public Raiz As CRaiz
Public Idioma As String
Public Mensajes As CMensajes
Public UsuCod As String
Public Solicitud As CSolicitud
Public FormCondiciones As Long

'Variables Publicas
Public sDestino As String
Public sCodPrecondicion As String

'Variable Privadas
Private m_ParamGen As ParametrosGenerales
Private m_arCamposGenericos(2 To 7) As String
Private m_sCampoGenerico As String
Private m_bError As Boolean
Private m_sMensajesCondicion(1 To 6) As String  'literales para basmensajes
Private m_sErrorFormula(1 To 11) As String
Private m_sValorBooleano(1) As String ' No y Si
Private m_vLongitudesDeCodigos As LongitudesDeCodigos
Private m_bHayCambiosCondiciones As Boolean

Public Property Get HayCambiosCondiciones() As Boolean
    HayCambiosCondiciones = m_bHayCambiosCondiciones
End Property

Public Property Get LongitudesDeCodigos() As LongitudesDeCodigos
    LongitudesDeCodigos = m_vLongitudesDeCodigos
End Property
Public Property Let LongitudesDeCodigos(ByRef vParametrosGenerales As LongitudesDeCodigos)
    m_vLongitudesDeCodigos = vParametrosGenerales
End Property

Public Property Get ParamGen() As ParametrosGenerales
    ParamGen = m_ParamGen
End Property
Public Property Let ParamGen(ByRef vParametrosGenerales As ParametrosGenerales)
    m_ParamGen = vParametrosGenerales
End Property

Private Sub cmdAnyadirCondicion_Click()
    With sdbgCondiciones
        .Scroll 0, .Rows - .Row
        
        If .VisibleRows > 0 Then
            If .VisibleRows >= .Rows Then
                If .VisibleRows = .Rows Then
                    .Row = .Rows - 1
                Else
                    .Row = .Rows
                End If
            Else
                .Row = .Rows - (.Rows - .VisibleRows) - 1
            End If
        End If
                
        .Columns("COD").Value = "X" & Solicitud.CondicionesSolicitud.NuevoCod
        .Columns("GRUPO").CellStyleSet "Normal", .Row
        .Columns("GRUPO").Locked = False
        .Columns("GRUPO").DropDownHwnd = sdbddGrupo.hwnd
        .Columns("VALOR").Locked = True
        .Columns("VALOR").CellStyleSet "Gris", .Row
        .Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hwnd
        .Columns("GRUPO_VALOR").Locked = False
        .Columns("GRUPO_VALOR").CellStyleSet "Normal", .Row
        .Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hwnd
        .Columns("CAMPO_VALOR").Locked = False
        .Columns("CAMPO_VALOR").CellStyleSet "Normal", .Row
    
        .Col = 4
        If Me.Visible Then .SetFocus
        .DoClick
    End With
End Sub

Private Sub cmdAyuda_Click()
    'si hay cambios los guarda
    If sdbgCondiciones.DataChanged Then
        sdbgCondiciones.Update
        If m_bError Then Exit Sub
    End If
    
    MostrarFormSOLAyudaCalculosLocal GestorIdiomas, Idioma
End Sub

Private Sub cmdEliminarCondicion_Click()
    Dim irespuesta As Integer

    With sdbgCondiciones
        If .IsAddRow Then
            If .DataChanged Then .CancelUpdate
        Else
            If .Rows = 0 Then Exit Sub
            
            Screen.MousePointer = vbHourglass
            
            If .SelBookmarks.Count = 1 Then
                irespuesta = Mensajes.PreguntaEliminar(.Columns("COD").Value)
                
                If irespuesta = vbYes Then
                    If .IsAddRow Then
                        If .DataChanged Then .CancelUpdate
                    Else
                        Solicitud.CondicionesSolicitud.Remove CStr(.Columns("COD").Value)
                                                
                        If .AddItemRowIndex(.Bookmark) > -1 Then
                            .RemoveItem (.AddItemRowIndex(.Bookmark))
                        Else
                            .RemoveItem (0)
                        End If
                        If IsEmpty(.GetBookmark(0)) Then
                            .Bookmark = .GetBookmark(-1)
                        Else
                            .Bookmark = .GetBookmark(0)
                        End If
                    End If
                End If
            End If
                
            .SelBookmarks.RemoveAll
        
            Screen.MousePointer = vbNormal
        End If
    End With
End Sub

Private Sub Form_Load()
    CargarRecursos
          
    ConfigurarSeguridad
    PonerFieldSeparator Me
    CargarComboValorBooleano
    CargarCondiciones
    
    txtFormula.Text = NullToStr(Solicitud.CondicionesFormula)
End Sub

Private Sub CargarComboValorBooleano()
    sdbddValorBooleano.RemoveAll
    
    sdbddValorBooleano.AddItem m_sValorBooleano(Abs(CInt(True)))
    sdbddValorBooleano.AddItem m_sValorBooleano(Abs(CInt(False)))
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim I As Integer
    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_SOLICITUDCONDICIONES, Idioma)
    
    If Not Ador Is Nothing Then
        With Ador
            Me.Caption = Ador(0).Value
            .MoveNext
            sdbgCondiciones.Columns("GRUPO").Caption = Ador(0).Value
            sdbgCondiciones.Columns("GRUPO_VALOR").Caption = Ador(0).Value
            m_sMensajesCondicion(1) = Ador(0).Value
            .MoveNext
            sdbgCondiciones.Columns("CAMPO").Caption = Ador(0).Value
            sdbgCondiciones.Columns("CAMPO_VALOR").Caption = Ador(0).Value
            m_sMensajesCondicion(2) = Ador(0).Value
            .MoveNext
            sdbgCondiciones.Columns("OPERADOR").Caption = Ador(0).Value
            m_sMensajesCondicion(3) = Ador(0).Value
            .MoveNext
            sdbgCondiciones.Columns("DIRECTO").Caption = Ador(0).Value  '5
            m_sMensajesCondicion(4) = Ador(0).Value
            .MoveNext
            sdbgCondiciones.Columns("VALOR").Caption = Ador(0).Value
            m_sMensajesCondicion(5) = Ador(0).Value
            .MoveNext
            cmdAnyadirCondicion.ToolTipText = Ador(0).Value
            .MoveNext
            cmdEliminarCondicion.ToolTipText = Ador(0).Value
            .MoveNext
            lblCondicion.Caption = Ador(0).Value
            .MoveNext
            cmdAyuda.ToolTipText = Ador(0).Value '10
            .MoveNext
            m_arCamposGenericos(2) = Ador(0).Value
            .MoveNext
            m_arCamposGenericos(3) = Ador(0).Value
            .MoveNext
            m_arCamposGenericos(4) = Ador(0).Value
            .MoveNext
            m_arCamposGenericos(5) = Ador(0).Value
            .MoveNext
            m_arCamposGenericos(6) = Ador(0).Value ' 15
            .MoveNext
            m_arCamposGenericos(7) = Ador(0).Value
            .MoveNext
            m_sMensajesCondicion(6) = Ador(0).Value
            .MoveNext
            m_sValorBooleano(0) = Ador(0).Value
            .MoveNext
            m_sValorBooleano(1) = Ador(0).Value
            For I = 1 To 11
                .MoveNext
                m_sErrorFormula(I) = Ador(0).Value
            Next I
            .MoveNext
            m_sCampoGenerico = Ador(0).Value
            
            Ador.Close
        End With
    End If
End Sub

''' <summary>Configura la seguridad del grid</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub ConfigurarSeguridad()
    With sdbgCondiciones
        sdbddGrupo.AddItem 0 & Chr(m_lSeparador) & ""
        .Columns("GRUPO").DropDownHwnd = sdbddGrupo.hwnd
        sdbddCampo.AddItem 0 & Chr(m_lSeparador) & ""
        .Columns("CAMPO").DropDownHwnd = sdbddCampo.hwnd
        sdbddOperador.AddItem ""
        .Columns("OPERADOR").DropDownHwnd = sdbddOperador.hwnd
        sdbddValorBooleano.AddItem ""
        sdbddGrupoValor.AddItem 0 & Chr(m_lSeparador) & ""
        .Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hwnd
        sdbddCampoValor.AddItem 0 & Chr(m_lSeparador) & ""
        .Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hwnd
    End With
End Sub

''' <summary>Carga las condiciones en el grid</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub CargarCondiciones()
    Dim oCondicion As CCondicionSolicitud
    Dim sGrupo As String
    Dim sCampo As String
    Dim sOperador As String
    Dim bDirecto As Boolean
    Dim sValor As String
    Dim lIdGrupoValor As Long
    Dim sGrupoValor As String
    Dim sCampoValor As String
    
    sdbgCondiciones.RemoveAll
    
    For Each oCondicion In Solicitud.CondicionesSolicitud
        sCampo = oCondicion.CampoForm.Denominaciones.Item(Idioma).Den
        sGrupo = oCondicion.CampoForm.Grupo.Denominaciones.Item(Idioma).Den
        
        sOperador = oCondicion.Operador
        
        If oCondicion.TipoValor = TipoValorCondicionEnlace.CampoDeFormulario Then
            bDirecto = False
            sValor = ""
            lIdGrupoValor = oCondicion.CampoValorForm.Grupo.Id
            sCampoValor = oCondicion.CampoValorForm.Denominaciones.Item(Idioma).Den
            sGrupoValor = oCondicion.CampoValorForm.Grupo.Denominaciones.Item(Idioma).Den
        Else
            bDirecto = True
            If Not oCondicion.CampoForm Is Nothing Then
                If oCondicion.CampoForm.Tipo = TipoBoolean Then
                    If oCondicion.valor <> 1 And oCondicion.valor <> 0 And oCondicion.valor <> True And oCondicion.valor <> False Then
                        sValor = oCondicion.valor
                    Else
                        sValor = m_sValorBooleano(Abs(CInt(oCondicion.valor)))
                    End If
                Else
                    sValor = oCondicion.valor
                End If
            Else
                sValor = oCondicion.valor
            End If
            
            sGrupoValor = ""
            lIdGrupoValor = 0
            sCampoValor = ""
        End If
        
        sdbgCondiciones.AddItem oCondicion.Id & Chr(m_lSeparador) & oCondicion.Cod & Chr(m_lSeparador) & oCondicion.CampoForm.Grupo.Id & Chr(m_lSeparador) & sGrupo & _
            Chr(m_lSeparador) & oCondicion.Campo & Chr(m_lSeparador) & sCampo & Chr(m_lSeparador) & oCondicion.Operador & Chr(m_lSeparador) & bDirecto & Chr(m_lSeparador) & sValor & Chr(m_lSeparador) & oCondicion.TipoValor & _
            Chr(m_lSeparador) & lIdGrupoValor & Chr(m_lSeparador) & sGrupoValor & Chr(m_lSeparador) & oCondicion.CampoValor & Chr(m_lSeparador) & sCampoValor & Chr(m_lSeparador) & _
            oCondicion.FECACT & Chr(m_lSeparador) & oCondicion.tipoCampo & Chr(m_lSeparador) & oCondicion.IncluyeCampoImporte
    Next
    
    Set oCondicion = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If sdbgCondiciones.DataChanged Then
        m_bError = False
        sdbgCondiciones.Update
        If m_bError Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    Cancel = CInt(Not ValidarFormula)
    Solicitud.CondicionesFormula = txtFormula.Text
End Sub

Private Sub sdbddCampo_CloseUp()
    With sdbgCondiciones
        If .Columns("CAMPO").Value = "" Then Exit Sub
        
        .Columns("TIPO_CAMPO").Value = sdbddCampo.Columns(0).Value
        .Columns("ID_CAMPO").Value = sdbddCampo.Columns(1).Value
        .Columns("CAMPO").Value = sdbddCampo.Columns(2).Value
                
        .Columns("DIRECTO").Value = False
        .Columns("DIRECTO").Locked = False
        .Columns("DIRECTO").CellStyleSet "Normal", .Row
        .Columns("TIPO_VALOR").Value = ""
        .Columns("VALOR").Value = ""
        .Columns("VALOR").Locked = True
        .Columns("VALOR").CellStyleSet "Gris", .Row
        .Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hwnd
        .Columns("GRUPO_VALOR").Locked = False
        .Columns("GRUPO_VALOR").CellStyleSet "Normal", .Row
        .Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hwnd
        .Columns("CAMPO_VALOR").Locked = False
        .Columns("CAMPO_VALOR").CellStyleSet "Normal", .Row
    End With
End Sub

Private Sub sdbddCampo_DropDown()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim bAdd As Boolean
    
    Screen.MousePointer = vbHourglass
    sdbddCampo.RemoveAll

    With sdbgCondiciones
        If .Columns("ID_GRUPO").Value <> "" Then
            If CLng(.Columns("ID_GRUPO").Value) > 0 Then
                Set oGrupo = Raiz.Generar_CFormGrupo
                oGrupo.Id = .Columns("ID_GRUPO").Value
                Set oGrupo.Formulario = Raiz.Generar_CFormulario 'Lo pongo para enga�ar al objeto y que cargue los datos de FORM_CAMPO
                oGrupo.CargarTodosLosCampos
                For Each oCampo In oGrupo.Campos
                    If oCampo.Tipo <> TipoDesglose And oCampo.Tipo <> TipoArchivo And oCampo.CampoPadre Is Nothing Then
                        bAdd = True
                        If sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value <> "" Then
                            bAdd = (CLng(sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value) <> oCampo.Id)
                        End If
                        
                        If bAdd Then sdbddCampo.AddItem oCampo.Tipo & Chr(m_lSeparador) & oCampo.Id & Chr(m_lSeparador) & oCampo.Denominaciones.Item(Idioma).Den
                    End If
                Next
                
                If sdbddCampo.Rows = 0 Then sdbddCampo.AddItem 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & ""
            End If
        End If
    End With

    Screen.MousePointer = vbNormal
    
    Set oGrupo = Nothing
    Set oCampo = Nothing
End Sub

Private Sub sdbddCampo_InitColumnProps()
    sdbddCampo.DataFieldList = "Column 0, Column 1"
    sdbddCampo.DataFieldToDisplay = "Column 2"
End Sub

Private Sub sdbddCampo_PositionList(ByVal Text As String)
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next

    With sdbddCampo
        .MoveFirst
    
        If .Columns("CAMPO").Value <> "" Then
            For I = 0 To .Rows - 1
                bm = .GetBookmark(I)
                If UCase(sdbgCondiciones.Columns("CAMPO").Value) = UCase(Mid(.Columns(2).CellText(bm), 1, Len(sdbgCondiciones.Columns("CAMPO").Value))) Then
                    sdbgCondiciones.Columns("CAMPO").Value = Mid(.Columns(2).CellText(bm), 1, Len(sdbgCondiciones.Columns("CAMPO").Value))
                    .Bookmark = bm
                    Exit For
                End If
            Next I
        End If
    End With
End Sub

Private Sub sdbddCampoValor_CloseUp()
    With sdbgCondiciones
        If .Columns("CAMPO_VALOR").Value = "" Then Exit Sub
    
        .Columns("TIPO_VALOR").Value = sdbddCampoValor.Columns(0).Value
        .Columns("ID_CAMPO_VALOR").Value = sdbddCampoValor.Columns(1).Value
        .Columns("CAMPO_VALOR").Value = sdbddCampoValor.Columns(2).Value
    End With
End Sub

Private Sub sdbddCampoValor_DropDown()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim bAdd As Boolean
    
    Screen.MousePointer = vbHourglass
    
    With sdbddCampoValor
        .RemoveAll
        
        If sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value <> "" Then
            If CLng(sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value) > 0 Then
                Set oGrupo = Raiz.Generar_CFormGrupo
                oGrupo.Id = sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value
                Set oGrupo.Formulario = Raiz.Generar_CFormulario 'Lo pongo para enga�ar al objeto y que cargue los datos de FORM_CAMPO
                oGrupo.CargarTodosLosCampos
                For Each oCampo In oGrupo.Campos
                    If oCampo.Tipo <> TipoDesglose And oCampo.Tipo <> TipoArchivo And oCampo.CampoPadre Is Nothing Then
                        bAdd = True
                        If sdbgCondiciones.Columns("ID_CAMPO").Value <> "" Then
                            bAdd = (CLng(sdbgCondiciones.Columns("ID_CAMPO").Value) <> oCampo.Id)
                        End If
                        
                        If bAdd Then .AddItem TipoValorCondicionEnlace.CampoDeFormulario & Chr(m_lSeparador) & oCampo.Id & Chr(m_lSeparador) & oCampo.Denominaciones.Item(Idioma).Den
                    End If
                Next
            End If
        End If
        
        If .Rows = 0 Then .AddItem 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & ""
    End With
    
    Screen.MousePointer = vbNormal
    
    Set oGrupo = Nothing
    Set oCampo = Nothing
End Sub

Private Sub sdbddCampoValor_InitColumnProps()
    sdbddCampoValor.DataFieldList = "Column 2"
End Sub

Private Sub sdbddCampoValor_PositionList(ByVal Text As String)
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next

    With sdbddCampoValor
        .MoveFirst
    
        If sdbgCondiciones.Columns("CAMPO_VALOR").Value <> "" Then
            For I = 0 To .Rows - 1
                bm = .GetBookmark(I)
                If UCase(sdbgCondiciones.Columns("CAMPO_VALOR").Value) = UCase(Mid(.Columns(2).CellText(bm), 1, Len(sdbgCondiciones.Columns("CAMPO_VALOR").Value))) Then
                    sdbgCondiciones.Columns("CAMPO_VALOR").Value = Mid(.Columns(2).CellText(bm), 1, Len(sdbgCondiciones.Columns("CAMPO_VALOR").Value))
                    .Bookmark = bm
                    Exit For
                End If
            Next I
        End If
    End With
End Sub

Private Sub sdbddGrupo_CloseUp()
    With sdbgCondiciones
        If .Columns("GRUPO").Value = "" Then Exit Sub
        
        .Columns("ID_GRUPO").Value = sdbddGrupo.Columns(0).Value
        .Columns("GRUPO").Value = sdbddGrupo.Columns(1).Value
        
        .Columns("DIRECTO").Value = False
        .Columns("DIRECTO").Locked = False
        .Columns("DIRECTO").CellStyleSet "Normal", sdbgCondiciones.Row
        .Columns("TIPO_VALOR").Value = ""
        .Columns("VALOR").Value = ""
        .Columns("VALOR").Locked = True
        .Columns("VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
        .Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hwnd
        .Columns("GRUPO_VALOR").Locked = False
        .Columns("GRUPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
        .Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hwnd
        .Columns("CAMPO_VALOR").Locked = False
        .Columns("CAMPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
    End With
End Sub

Private Sub sdbddGrupo_DropDown()
    Dim oFormulario As CFormulario
    Dim oGrupo As CFormGrupo

    Screen.MousePointer = vbHourglass
    
    With sdbddGrupo
        .RemoveAll
        
        Set oFormulario = Raiz.Generar_CFormulario
        oFormulario.Id = FormCondiciones
        oFormulario.CargarTodosLosGrupos
            
        For Each oGrupo In oFormulario.Grupos
            .AddItem oGrupo.Id & Chr(m_lSeparador) & oGrupo.Denominaciones.Item(Idioma).Den
        Next
    End With
    
    Screen.MousePointer = vbNormal
    
    Set oFormulario = Nothing
    Set oGrupo = Nothing
End Sub

Private Sub sdbddGrupo_InitColumnProps()
    sdbddGrupo.DataFieldList = "Column 0"
    sdbddGrupo.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddGrupo_PositionList(ByVal Text As String)
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next

    With sdbddGrupo
        .MoveFirst
    
        If sdbgCondiciones.Columns("GRUPO").Value <> "" Then
            For I = 0 To .Rows - 1
                bm = .GetBookmark(I)
                If UCase(sdbgCondiciones.Columns("GRUPO").Value) = UCase(Mid(.Columns(1).CellText(bm), 1, Len(sdbgCondiciones.Columns("GRUPO").Value))) Then
                    sdbgCondiciones.Columns("GRUPO").Value = Mid(.Columns(1).CellText(bm), 1, Len(sdbgCondiciones.Columns("GRUPO").Value))
                    .Bookmark = bm
                    Exit For
                End If
            Next I
        End If
    End With
End Sub

Private Sub sdbddGrupoValor_CloseUp()
    With sdbgCondiciones
        If .Columns("GRUPO_VALOR").Value = "" Then Exit Sub
    
        sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value = sdbddGrupoValor.Columns(0).Value
        sdbgCondiciones.Columns("GRUPO_VALOR").Value = sdbddGrupoValor.Columns(1).Value
    End With
End Sub

Private Sub sdbddGrupoValor_DropDown()
    Dim oFormulario As CFormulario
    Dim oGrupo As CFormGrupo

    Screen.MousePointer = vbHourglass
    
    With sdbddGrupoValor
        .RemoveAll
        
        Set oFormulario = Raiz.Generar_CFormulario
        oFormulario.Id = FormCondiciones
        oFormulario.CargarTodosLosGrupos
    
        For Each oGrupo In oFormulario.Grupos
            .AddItem oGrupo.Id & Chr(m_lSeparador) & oGrupo.Denominaciones.Item(Idioma).Den
        Next
    End With
    
    Screen.MousePointer = vbNormal
    
    Set oFormulario = Nothing
    Set oGrupo = Nothing
End Sub

Private Sub sdbddGrupoValor_InitColumnProps()
    sdbddGrupoValor.DataFieldList = "Column 0"
    sdbddGrupoValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddGrupoValor_PositionList(ByVal Text As String)
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next

    With sdbddGrupoValor
        .MoveFirst
    
        If sdbgCondiciones.Columns("GRUPO_VALOR").Value <> "" Then
            For I = 0 To .Rows - 1
                bm = .GetBookmark(I)
                If UCase(sdbgCondiciones.Columns("GRUPO_VALOR").Value) = UCase(Mid(.Columns(1).CellText(bm), 1, Len(sdbgCondiciones.Columns("GRUPO_VALOR").Value))) Then
                    sdbgCondiciones.Columns("GRUPO_VALOR").Value = Mid(.Columns(1).CellText(bm), 1, Len(sdbgCondiciones.Columns("GRUPO_VALOR").Value))
                    .Bookmark = bm
                    Exit For
                End If
            Next I
        End If
    End With
End Sub

Private Sub sdbddOperador_CloseUp()
    If sdbgCondiciones.Columns("OPERADOR").Value = "" Then Exit Sub
    sdbgCondiciones.Columns("OPERADOR").Value = sdbddOperador.Columns(0).Value
End Sub

Private Sub sdbddOperador_DropDown()
    Dim oCampo As CFormItem
    Dim iTipoCampo As TiposDeAtributos
    
    Screen.MousePointer = vbHourglass
    
    With sdbddOperador
        .RemoveAll
        
        If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
            If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) > 0 Then
                If sdbgCondiciones.Columns("ID_CAMPO").Value <> "" Then
                    If CLng(sdbgCondiciones.Columns("ID_CAMPO").Value) > 0 Then
                        Set oCampo = Raiz.Generar_CFormCampo
                        oCampo.Id = sdbgCondiciones.Columns("ID_CAMPO").Value
                        oCampo.CargarDatosFormCampo
                        iTipoCampo = oCampo.Tipo
                    End If
                End If
                
                Select Case iTipoCampo
                    Case TiposDeAtributos.TipoBoolean
                        .AddItem "="
                    Case TiposDeAtributos.TipoNumerico, TiposDeAtributos.TipoFecha
                        .AddItem ">"
                        .AddItem "<"
                        .AddItem ">="
                        .AddItem "<="
                        .AddItem "="
                        .AddItem "<>"
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                        .AddItem "="
                        .AddItem "Like"
                        .AddItem "<>"
                End Select
            End If
        End If
        
        If .Rows = 0 Then
            .AddItem ""
        End If
    End With
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddOperador_InitColumnProps()
    sdbddOperador.DataFieldList = "Column 0"
    sdbddOperador.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddOperador_PositionList(ByVal Text As String)
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next

    With sdbddOperador
        .MoveFirst
    
        If sdbgCondiciones.Columns("OPERADOR").Value <> "" Then
            For I = 0 To .Rows - 1
                bm = .GetBookmark(I)
                If UCase(sdbgCondiciones.Columns("OPERADOR").Value) = UCase(Mid(.Columns(0).CellText(bm), 1, Len(sdbgCondiciones.Columns("OPERADOR").Value))) Then
                    sdbgCondiciones.Columns("OPERADOR").Value = Mid(.Columns(0).CellText(bm), 1, Len(sdbgCondiciones.Columns("OPERADOR").Value))
                    .Bookmark = bm
                    Exit For
                End If
            Next I
        End If
    End With
End Sub

Private Sub sdbddValorBooleano_CloseUp()
    If sdbgCondiciones.Columns("VALOR").Value = "" Then Exit Sub
    sdbgCondiciones.Columns("VALOR").Value = sdbddValorBooleano.Columns(0).Value
End Sub

Private Sub sdbddValorBooleano_InitColumnProps()
    sdbddValorBooleano.DataFieldList = "Column 0"
    sdbddValorBooleano.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddValorBooleano_PositionList(ByVal Text As String)
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next

    With sdbddValorBooleano
        .MoveFirst
    
        If sdbgCondiciones.Columns("VALOR").Value <> "" Then
            For I = 0 To .Rows - 1
                bm = .GetBookmark(I)
                If UCase(sdbgCondiciones.Columns("VALUE").Value) = UCase(Mid(.Columns(0).CellText(bm), 1, Len(sdbgCondiciones.Columns("VALOR").Value))) Then
                    sdbgCondiciones.Columns("VALOR").Value = Mid(.Columns(0).CellText(bm), 1, Len(sdbgCondiciones.Columns("VALOR").Value))
                    .Bookmark = bm
                    Exit For
                End If
            Next I
        End If
    End With
End Sub

Private Sub sdbgCondiciones_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    
    m_bHayCambiosCondiciones = True
    
    With sdbgCondiciones
        If .Rows = 0 Then Exit Sub
        
        If IsEmpty(.GetBookmark(0)) Then
            .Bookmark = .GetBookmark(-1)
        Else
            .Bookmark = .GetBookmark(0)
        End If
        If Me.Visible Then .SetFocus
    End With
End Sub

Private Sub sdbgCondiciones_AfterInsert(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0

    m_bHayCambiosCondiciones = True

    With sdbgCondiciones
        If IsEmpty(.GetBookmark(0)) Then
            .Bookmark = .GetBookmark(-1)
        Else
            .Bookmark = .GetBookmark(0)
        End If
    End With
End Sub

Private Sub sdbgCondiciones_AfterUpdate(RtnDispErrMsg As Integer)
    m_bHayCambiosCondiciones = True
End Sub

Private Sub sdbgCondiciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

'*****************************************************************************
'*** Descripci�n:  Function que comprueba que la condici�n de enlace es correcta antes de actualizar la BD
'*** Parametros entrada:= Cancel
'*** Llamada desde: Al actualizar sdbgCondiciones y perder el foco
'*** Tiempo m�ximo: 0,20 seg
'************************************************************
Private Sub sdbgCondiciones_BeforeUpdate(Cancel As Integer)
    Dim sValor As String
    Dim sUON1 As String
    Dim sUON2 As String
    Dim sUON3 As String
    Dim oCondicion As CCondicionSolicitud
    Dim bm As Variant
    
    With sdbgCondiciones
        .Columns("VALOR").Mask = ""
        .Columns("VALOR").PromptInclude = False
        
        'Comprobar datos Obligatorios
        If .Columns("COD").Value = "" Then
            m_bError = True
            Mensajes.NoValido "ID"
            GoTo Salir
        ElseIf IsNumeric(.Columns("COD").Value) Then
            m_bError = True
            Mensajes.NoValido "ID"
            GoTo Salir
        End If
        If .Columns("ID_CAMPO").Value = "" Or .Columns("ID_CAMPO").Value = "0" Then
            m_bError = True
            Mensajes.NoValido m_sMensajesCondicion(2)
            GoTo Salir
        End If
        If .Columns("OPERADOR").Value = "" Then
            m_bError = True
            Mensajes.NoValido m_sMensajesCondicion(3)
            GoTo Salir
        End If
        'Establecer TIPO_VALOR por si no se ha tocado:
        If .Columns("TIPO_VALOR").Value = "" Or .Columns("TIPO_VALOR").Value = "0" Then
            If .Columns("DIRECTO").Value Then
                .Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.ValorEstatico
            Else
                .Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.CampoDeFormulario
            End If
        End If
        If CInt(.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.CampoDeFormulario And _
                (.Columns("ID_CAMPO_VALOR").Value = "" Or .Columns("ID_CAMPO_VALOR").Value = "0") Then
            m_bError = True
            Mensajes.NoValido m_sMensajesCondicion(2)
            GoTo Salir
        End If
            
        'Comprobar que los tipos de datos coinciden
        Dim iTipoCampo As TiposDeAtributos
        Dim iTipoCampoValor As TiposDeAtributos
        Dim oCampo As CFormItem
        Dim oCampoValor As CFormItem
        Dim vValorConvertido As Variant
        Dim bObligatorioComprobarTipo As Boolean
        bObligatorioComprobarTipo = False
        iTipoCampo = CInt(.Columns("TIPO_CAMPO").Value)
        If CInt(.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.CampoDeFormulario Then
            If .Columns("ID_CAMPO_VALOR").Value <> "" Then
                If CLng(.Columns("ID_CAMPO_VALOR").Value) > 0 Then
                    Set oCampoValor = Raiz.Generar_CFormCampo
                    oCampoValor.Id = .Columns("ID_CAMPO_VALOR").Value
                    oCampoValor.CargarDatosFormCampo
                    oCampoValor.Grupo.CargarDatosFormGrupo
                    iTipoCampoValor = oCampoValor.Tipo
                End If
            End If
        End If
        
        sValor = sdbgCondiciones.Columns("VALOR").Value
        
        If sValor <> "" Or bObligatorioComprobarTipo Then
            On Error GoTo ERROR
            Select Case iTipoCampo
                Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                    If iTipoCampoValor > 0 Then
                        If iTipoCampoValor <> TipoString And iTipoCampoValor <> TipoTextoCorto And iTipoCampoValor <> TipoTextoMedio And iTipoCampoValor <> TipoTextoLargo Then
                            Err.Raise 13 'Type mismatch
                        End If
                    Else
                       vValorConvertido = CStr(.Columns("VALOR").Value)
                    End If
                Case TiposDeAtributos.TipoNumerico
                    If iTipoCampoValor > 0 Then
                        If Not iTipoCampoValor = TipoNumerico Then
                            Err.Raise 13 'Type mismatch
                        End If
                    Else
                       vValorConvertido = CDbl(.Columns("VALOR").Value)
                    End If
                Case TiposDeAtributos.TipoFecha
                    If iTipoCampoValor > 0 Then
                        If Not iTipoCampoValor = TipoFecha Then
                            Err.Raise 13 'Type mismatch
                        End If
                    Else
                       vValorConvertido = CDate(.Columns("VALOR").Value)
                    End If
                Case TiposDeAtributos.TipoBoolean
                    If iTipoCampoValor > 0 Then
                        If Not iTipoCampoValor = TipoBoolean Then
                            Err.Raise 13 'Type mismatch
                        End If
                    Else
                        If .Columns("VALOR").Value <> m_sValorBooleano(0) And .Columns("VALOR").Value <> m_sValorBooleano(1) Then
                            Err.Raise 13 'Type mismatch
                        Else
                            If .Columns("VALOR").Value = m_sValorBooleano(0) Then
                                sValor = "0"
                            ElseIf .Columns("VALOR").Value = m_sValorBooleano(1) Then
                                sValor = "1"
                            End If
                        End If
                    End If
            End Select
        End If
            
        'Guardar Datos
         If .IsAddRow Then
            Set oCondicion = Raiz.Generar_CCondicionSolicitud
            oCondicion.Cod = Trim(.Columns("COD").Value)
            oCondicion.tipoCampo = CInt(.Columns("TIPO_CAMPO").Value)
            oCondicion.Campo = CLng(.Columns("ID_CAMPO").Value)
            oCondicion.Operador = .Columns("OPERADOR").Value
            oCondicion.TipoValor = CInt(.Columns("TIPO_VALOR").Value)
            oCondicion.CampoValor = CLng(.Columns("ID_CAMPO_VALOR").Value)
            oCondicion.valor = sValor
            oCondicion.cargarCampo
            oCondicion.CampoForm.Grupo.CargarDatosFormGrupo

            If CInt(.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.CampoDeFormulario Then
                Set oCondicion.CampoValorForm = oCampoValor
            Else
                Set oCondicion.CampoValorForm = Nothing
            End If
            
            Solicitud.CondicionesSolicitud.AddCondicion oCondicion
        Else
            Set oCondicion = Solicitud.CondicionesSolicitud.Item(Trim(.Columns("COD").Value))
            If Not oCondicion Is Nothing Then
                oCondicion.Cod = Trim(.Columns("COD").Value)
                oCondicion.tipoCampo = CInt(.Columns("TIPO_CAMPO").Value)
                oCondicion.Campo = CLng(.Columns("ID_CAMPO").Value)
                oCondicion.Operador = .Columns("OPERADOR").Value
                oCondicion.TipoValor = CInt(.Columns("TIPO_VALOR").Value)
                oCondicion.CampoValor = CLng(.Columns("ID_CAMPO_VALOR").Value)
                oCondicion.valor = sValor
                
                If CInt(.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.CampoDeFormulario Then
                    If Not oCondicion.CampoValorForm Is Nothing Then
                        oCondicion.CampoValorForm.Id = CLng(.Columns("ID_CAMPO_VALOR").Value)
                    Else
                        Set oCondicion.CampoValorForm = oCampoValor
                    End If
                Else
                    Set oCondicion.CampoValorForm = Nothing
                End If
            Else
                'Has cambiado de codigo. Borrar de la colecci�n y meter uno nuevo.
                bm = .GetBookmark(0) 'Linea en edici�n
                Solicitud.CondicionesSolicitud.Remove .Columns("COD").CellText(bm)
                
                Set oCondicion = Raiz.Generar_CCondicionSolicitud
                oCondicion.Cod = Trim(.Columns("COD").Value)
                oCondicion.tipoCampo = CInt(.Columns("TIPO_CAMPO").Value)
                oCondicion.Campo = CLng(.Columns("ID_CAMPO").Value)
                oCondicion.Operador = .Columns("OPERADOR").Value
                oCondicion.TipoValor = CInt(.Columns("TIPO_VALOR").Value)
                oCondicion.CampoValor = CLng(.Columns("ID_CAMPO_VALOR").Value)
                oCondicion.valor = sValor
                oCondicion.cargarCampo
                oCondicion.CampoForm.Grupo.CargarDatosFormGrupo
    
                If CInt(.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.CampoDeFormulario Then
                    Set oCondicion.CampoValorForm = oCampoValor
                Else
                    Set oCondicion.CampoValorForm = Nothing
                End If
                
                Solicitud.CondicionesSolicitud.AddCondicion oCondicion
                
                'resulta q borra el texto del grupo pq pierde el value
                If Not (sdbgCondiciones.Columns("GRUPO").Value = CStr(sdbgCondiciones.Columns("ID_GRUPO").Value)) Then
                    sdbgCondiciones.Columns("GRUPO").Value = sdbgCondiciones.Columns("ID_GRUPO").Value
                    
                    sdbddGrupo_PositionList sdbgCondiciones.Columns("GRUPO").Text
                End If
            End If
        End If
    End With

    Exit Sub
    
Salir:
    Set oCampoValor = Nothing
    If Me.Visible Then sdbgCondiciones.SetFocus
    Cancel = True
    Exit Sub
ERROR:
    If Err.Number = 13 Then 'Type mismatch
        Cancel = True
        Mensajes.NoValido m_sMensajesCondicion(6)
        If Me.Visible Then sdbgCondiciones.SetFocus
        m_bError = True
        Exit Sub
    End If
End Sub

Private Sub sdbgCondiciones_Change()
    With sdbgCondiciones
        If .Col >= 0 Then
            Select Case .Columns(.Col).Name
                Case "DIRECTO"
                    If .Columns("DIRECTO").Value Then
                        .Columns("VALOR").Locked = False
                        .Columns("VALOR").CellStyleSet "Normal", .Row
                        .Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.ValorEstatico
                        .Columns("ID_GRUPO_VALOR").Value = 0
                        .Columns("GRUPO_VALOR").Value = ""
                        .Columns("GRUPO_VALOR").Locked = True
                        .Columns("GRUPO_VALOR").CellStyleSet "Gris", .Row
                        .Columns("GRUPO_VALOR").DropDownHwnd = 0
                        .Columns("ID_CAMPO_VALOR").Value = 0
                        .Columns("CAMPO_VALOR").Value = ""
                        .Columns("CAMPO_VALOR").Locked = True
                        .Columns("CAMPO_VALOR").CellStyleSet "Gris", .Row
                        .Columns("CAMPO_VALOR").DropDownHwnd = 0
                    Else
                        .Columns("VALOR").Value = ""
                        .Columns("VALOR").Locked = True
                        .Columns("VALOR").CellStyleSet "Gris", .Row
                        .Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.CampoDeFormulario
                        .Columns("GRUPO_VALOR").Locked = False
                        .Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hwnd
                        .Columns("GRUPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
                        .Columns("CAMPO_VALOR").Locked = False
                        .Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hwnd
                        .Columns("CAMPO_VALOR").CellStyleSet "Normal", .Row
                    End If
                Case "GRUPO"
                    .Columns("ID_CAMPO").Value = 0
                    .Columns("CAMPO").Value = ""
                    .Columns("OPERADOR").Value = ""
                Case "CAMPO"
                    If .Columns("CAMPO").Value = "" Then
                        .Columns("TIPO_CAMPO").Value = 0
                        .Columns("GRUPO").Locked = False
                        .Columns("GRUPO").DropDownHwnd = sdbddGrupo.hwnd
                        .Columns("GRUPO").CellStyleSet "Normal", .Row
                    End If
                Case "GRUPO_VALOR"
                    .Columns("ID_CAMPO_VALOR").Value = 0
                    .Columns("CAMPO_VALOR").Value = ""
            End Select
        End If
    End With
End Sub

Private Sub sdbgCondiciones_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete And sdbgCondiciones.Col = -1 Then
        cmdEliminarCondicion_Click
    End If
End Sub

Private Sub sdbgCondiciones_KeyPress(KeyAscii As Integer)
    With sdbgCondiciones
        Select Case KeyAscii
            Case vbKeyReturn
                If .DataChanged Then
                    .Update
                    If m_bError Then Exit Sub
                End If
            Case vbKeyBack
                If .Col >= 0 Then
                    Select Case .Columns(.Col).Name
                        Case "GRUPO"
                            .Columns("TIPO_CAMPO").Value = 0
                            .Columns("ID_GRUPO").Value = 0
                            .Columns("GRUPO").Value = " "
                            .Columns("ID_CAMPO").Value = 0
                            .Columns("CAMPO").Value = ""
                            .Columns("OPERADOR").Value = ""
                            .Col = .Columns("COD").Position
                        Case "CAMPO"
                            .Columns("ID_CAMPO").Value = 0
                            .Columns("CAMPO").Value = ""
                            .Columns("TIPO_CAMPO").Value = 0
                            .Columns("GRUPO").Locked = False
                            .Columns("GRUPO").DropDownHwnd = sdbddGrupo.hwnd
                            .Columns("GRUPO").CellStyleSet "Normal", .Row
                            .Col = .Columns("GRUPO").Position
                        Case "OPERADOR"
                            .Columns("OPERADOR").Value = ""
                        Case "GRUPO_VALOR"
                            .Columns("ID_GRUPO_VALOR").Value = 0
                            .Columns("GRUPO_VALOR").Value = ""
                            .Columns("ID_CAMPO_VALOR").Value = 0
                            .Columns("CAMPO_VALOR").Value = ""
                            .Col = .Columns("OPERADOR").Position
                        Case "CAMPO_VALOR"
                            .Columns("ID_CAMPO_VALOR").Value = 0
                            .Columns("CAMPO_VALOR").Value = ""
                            .Col = .Columns("GRUPO_VALOR").Position
                    End Select
                End If
        End Select
    End With
End Sub

Private Sub sdbgCondiciones_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bMalaColumna As Boolean
    Dim iIndexMensaje As Integer
    Dim sColumnaARellenar As String
    
    With sdbgCondiciones
        If .IsAddRow Then
            cmdAnyadirCondicion.Enabled = False
        Else
            cmdAnyadirCondicion.Enabled = True
        End If
    
        If .Col < 0 Then Exit Sub
        
        If .IsAddRow And Not IsNull(LastRow) Then 'Cambiamos de fila para a�adir
            If .IsAddRow Then
                .Columns("COD").Value = "X" & Solicitud.CondicionesSolicitud.NuevoCod
            End If
            .Columns("GRUPO").CellStyleSet "Normal", .Row
            .Columns("GRUPO").Locked = False
            .Columns("GRUPO").DropDownHwnd = sdbddGrupo.hwnd
            .Columns("VALOR").Locked = True
            .Columns("VALOR").CellStyleSet "Gris", .Row
            .Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hwnd
            .Columns("GRUPO_VALOR").Locked = False
            .Columns("GRUPO_VALOR").CellStyleSet "Normal", .Row
            .Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hwnd
            .Columns("CAMPO_VALOR").Locked = False
            .Columns("CAMPO_VALOR").CellStyleSet "Normal", .Row
        End If
        If Not .IsAddRow Or IsNull(LastRow) Then 'Si no estamos a�adiendo o Si estamos a�adiendo pero cambiamos de columna
            Select Case .Columns(.Col).Name
                Case "COD"
                    If .IsAddRow Then
                        .Columns("COD").Value = "X" & Solicitud.CondicionesSolicitud.NuevoCod
                    End If
                Case "GRUPO"
                    If .Columns("TIPO_CAMPO").Value <> "" Then
                        .Columns("GRUPO").CellStyleSet "Normal", .Row
                        .Columns("GRUPO").Locked = False
                        .Columns("GRUPO").DropDownHwnd = sdbddGrupo.hwnd
                    End If
                Case "DIRECTO"
                    .Columns("DIRECTO").Locked = False
                    .Columns("DIRECTO").CellStyleSet "Normal", .Row
                Case "VALOR"
                    Dim sValor_aux As String
                    .Columns("VALOR").Mask = ""
                    .Columns("VALOR").PromptInclude = False
                    sValor_aux = .Columns("VALOR").Value
                    .Columns("VALOR").Value = ""
                    .Columns("VALOR").Value = sValor_aux
                    If .Columns("TIPO_VALOR").Value <> "" Then
                        If CInt(.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.CampoDeFormulario Then
                            .Columns("VALOR").CellStyleSet "Gris", .Row
                            .Columns("VALOR").Locked = True
                        Else
                            .Columns("VALOR").CellStyleSet "Normal", .Row
                            .Columns("VALOR").Locked = False
                            If .Columns("TIPO_CAMPO").Value <> "" Then
                                If CInt(.Columns("TIPO_CAMPO").Value) = TipoBoolean Then
                                    .Columns("VALOR").Style = ssStyleComboBox
                                    .Columns("VALOR").DropDownHwnd = sdbddValorBooleano.hwnd
                                Else
                                    .Columns("VALOR").Style = ssStyleEdit
                                    .Columns("VALOR").DropDownHwnd = 0
                                End If
                            End If
                        End If
                    End If
                Case "GRUPO_VALOR", "CAMPO_VALOR"
                    If .Columns("TIPO_VALOR").Value <> "" Then
                        If CInt(.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.CampoDeFormulario Then
                            .Columns("GRUPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
                            .Columns("GRUPO_VALOR").Locked = False
                            .Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hwnd
                            .Columns("CAMPO_VALOR").CellStyleSet "Normal", .Row
                            .Columns("CAMPO_VALOR").Locked = False
                            .Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hwnd
                        Else
                            .Columns("GRUPO_VALOR").CellStyleSet "Gris", .Row
                            .Columns("GRUPO_VALOR").Locked = True
                            .Columns("GRUPO_VALOR").DropDownHwnd = 0
                            .Columns("CAMPO_VALOR").CellStyleSet "Gris", .Row
                            .Columns("CAMPO_VALOR").Locked = True
                            .Columns("CAMPO_VALOR").DropDownHwnd = 0
                        End If
                    End If
            End Select
        End If
    
        bMalaColumna = False
        Select Case .Columns(.Col).Name
            Case "CAMPO"
                If Not .Columns("CAMPO").Locked Then
                    bMalaColumna = True
                    iIndexMensaje = 1
                    sColumnaARellenar = "GRUPO"
                    If .Columns("ID_GRUPO").Value <> "" Then bMalaColumna = False
                End If
                
            Case "OPERADOR"
                bMalaColumna = True
                iIndexMensaje = 2
                sColumnaARellenar = "CAMPO"
                If .Columns("TIPO_CAMPO").Value <> "" Then
                    If CInt(.Columns("TIPO_CAMPO").Value) > 0 Then bMalaColumna = False
                End If
                
            Case "CAMPO_VALOR"
                If Not .Columns("CAMPO_VALOR").Locked Then
                    bMalaColumna = True
                    iIndexMensaje = 1
                    sColumnaARellenar = "GRUPO_VALOR"
                    If .Columns("ID_GRUPO_VALOR").Value <> "" Then bMalaColumna = False
                End If
        End Select
        If bMalaColumna Then
            Mensajes.NoValido m_sMensajesCondicion(iIndexMensaje)
            .Col = .Columns(sColumnaARellenar).Position
        End If
    End With
End Sub

Private Sub sdbgCondiciones_RowLoaded(ByVal Bookmark As Variant)
    With sdbgCondiciones
        If CInt(.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ValorEstatico Then
            .Columns("ID_GRUPO_VALOR").Value = 0
            .Columns("GRUPO_VALOR").Value = ""
            .Columns("GRUPO_VALOR").Locked = True
            .Columns("GRUPO_VALOR").CellStyleSet "Gris" ', sdbgCondiciones.Row
            .Columns("GRUPO_VALOR").DropDownHwnd = 0
            .Columns("ID_CAMPO_VALOR").Value = 0
            .Columns("CAMPO_VALOR").Value = ""
            .Columns("CAMPO_VALOR").Locked = True
            .Columns("CAMPO_VALOR").CellStyleSet "Gris" ', sdbgCondiciones.Row
            .Columns("CAMPO_VALOR").DropDownHwnd = 0
        Else
            .Columns("VALOR").Value = ""
            .Columns("VALOR").Locked = True
            .Columns("VALOR").CellStyleSet "Gris"
        End If
    End With
End Sub

Private Sub txtFormula_GotFocus()
    'si hay cambios los guarda
    If sdbgCondiciones.DataChanged Then sdbgCondiciones.Update
End Sub

Private Function ValidarFormula() As Boolean
    Dim sVariables() As String
    Dim lErrCode As Integer
    Dim lIndex As Integer
    Dim iEq As USPExpression
    Dim oCondicion As CCondicionEnlace
    Dim I As Integer
    Dim sCaracter As String
    
    Set iEq = New USPExpression
        
    With sdbgCondiciones
        If .Rows > 0 Then
            ReDim sVariables(sdbgCondiciones.Rows)
                   
            For I = 0 To .Rows - 1
                sVariables(I) = .Columns("COD").CellValue(.AddItemBookmark(I))
            Next
        Else
            ReDim sVariables(0)
        End If
        
        'Si hay alguna linea para las condiciones que comprueba si los ha introducido correctamente
        lErrCode = USPEX_NO_ERROR
        If .Rows > 0 Then
            If UBound(sVariables) > 0 Or txtFormula.Text <> "" Then
                lIndex = iEq.Parse(txtFormula.Text, sVariables, lErrCode)
            End If
        Else
            If txtFormula.Text <> "" Then lIndex = iEq.Parse(txtFormula.Text, sVariables, lErrCode)
        End If
        
        If lErrCode <> USPEX_NO_ERROR Then
            ' Parsing error handler
            Select Case lErrCode
                Case USPEX_DIVISION_BY_ZERO
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(1))
                Case USPEX_EMPTY_EXPRESSION
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(2))
                Case USPEX_MISSING_OPERATOR
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(3))
                Case USPEX_SYNTAX_ERROR
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(4))
                Case USPEX_UNKNOWN_FUNCTION
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(5))
                Case USPEX_UNKNOWN_OPERATOR
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(6))
                Case USPEX_WRONG_PARAMS_NUMBER
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(7))
                Case USPEX_UNKNOWN_IDENTIFIER
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(8))
                Case USPEX_UNKNOWN_VAR
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(9))
                Case USPEX_VARIABLES_NOTUNIQUE
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(10))
                Case Else
                    sCaracter = Mid(txtFormula.Text, lIndex)
                    Mensajes.FormulaIncorrecta (m_sErrorFormula(11) & vbCrLf & sCaracter)
            End Select
        
            If Me.Visible Then txtFormula.SetFocus
        
            Set iEq = Nothing
            ValidarFormula = False
            Exit Function
        End If
    End With
    
    Set iEq = Nothing
    ValidarFormula = True
End Function

Private Sub txtFormula_KeyDown(KeyCode As Integer, Shift As Integer)
   If sdbgCondiciones.Rows = 0 Then
        Mensajes.IntroducirIdentificador
        txtFormula.Text = ""
        If Me.Visible Then sdbgCondiciones.SetFocus
    End If
End Sub
