VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEsperaVirtual 
   BackColor       =   &H8000000E&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Calculo de ahorros"
   ClientHeight    =   1695
   ClientLeft      =   4410
   ClientTop       =   5175
   ClientWidth     =   6165
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1695
   ScaleWidth      =   6165
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      BackColor       =   &H8000000E&
      BorderStyle     =   0  'None
      Height          =   600
      Left            =   30
      ScaleHeight     =   600
      ScaleWidth      =   6135
      TabIndex        =   1
      Top             =   600
      Width           =   6135
      Begin MSComctlLib.ProgressBar ProgressBar 
         Height          =   315
         Left            =   195
         TabIndex        =   2
         Top             =   150
         Width           =   5745
         _ExtentX        =   10134
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   1
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   2400
      MousePointer    =   1  'Arrow
      TabIndex        =   0
      Top             =   1260
      Width           =   1005
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H8000000E&
      Caption         =   "Calculando proceso:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   570
      TabIndex        =   3
      Top             =   150
      Width           =   5415
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   0
      Picture         =   "frmEsperaVirtual.frx":0000
      Top             =   120
      Width           =   480
   End
End
Attribute VB_Name = "frmEsperaVirtual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Raiz As CRaiz
Public GestorIdiomas As CGestorIdiomas
Public Mensajes As CMensajes
Public Idioma As String
Public g_oProcesos As CProcesos
Public g_lNum As Long

Private m_vProcesoErrores As Variant
Private m_sLabel As String
Private m_bCancel As Boolean
Private m_bCalculando As Boolean
Private m_vLongitudesDeCodigos As LongitudesDeCodigos

Public Property Get LongitudesDeCodigos() As LongitudesDeCodigos
    LongitudesDeCodigos = m_vLongitudesDeCodigos
End Property

Public Property Let LongitudesDeCodigos(ByRef vLongitudesDeCodigos As LongitudesDeCodigos)
    m_vLongitudesDeCodigos = vLongitudesDeCodigos
End Property

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_ESPERA_VIRTUAL, Idioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        m_sLabel = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub cmdCancelar_Click()
    m_bCancel = True
End Sub

Private Sub Form_Load()
    CargarRecursos
    
    ReDim m_vProcesoErrores(2, 0)
    m_bCancel = False
    m_bCalculando = False
End Sub

Private Sub Calcular()
    Dim oproce As CProceso
    Dim teserror As TipoErrorSummit
    Dim bError As Boolean
    
    ProgressBar.Max = g_lNum
    ProgressBar.Visible = True
    Me.Refresh
    Screen.MousePointer = vbHourglass
    bError = False
    For Each oproce In g_oProcesos
        If ProgressBar.Value < g_lNum Then ProgressBar.Value = ProgressBar.Value + 1
        ProgressBar.Refresh
        Label1 = m_sLabel & " " & CStr(oproce.Anyo) & "/" & oproce.GMN1Cod & "/" & CStr(oproce.Cod)
        DoEvents
        teserror = oproce.CalcularAhorrosProce
        DoEvents
        If teserror.NumError <> TESnoerror Then
            bError = True
            AnotarError oproce.Anyo, oproce.GMN1Cod, oproce.Cod, teserror
        End If
        DoEvents
        If m_bCancel Then Exit For
    Next
    Screen.MousePointer = vbNormal
    If m_bCancel Then Mensajes.MensajeOKOnly 584
    If bError Then MostrarErrores
    Set g_oProcesos = Nothing
End Sub

Private Sub Form_Activate()
    If Not m_bCalculando Then
        m_bCalculando = True
        Calcular
        Me.Hide
    End If
End Sub

Private Sub AnotarError(ByVal iAnyo As Integer, ByVal sGMN1Cod As String, ByVal lCod As Long, udtError As TipoErrorSummit)
    Dim i As Integer

    i = UBound(m_vProcesoErrores, 2)
    m_vProcesoErrores(0, i) = CStr(iAnyo) & "/" & Space(m_vLongitudesDeCodigos.giLongCodGMN1 - Len(sGMN1Cod)) & sGMN1Cod & "/" & Space(5 - Len(CStr(lCod))) & CStr(lCod)
    m_vProcesoErrores(1, i) = udtError
    ReDim Preserve m_vProcesoErrores(2, i + 1)
End Sub
Private Sub MostrarErrores()
    MostrarFormAhorrosPendLocal GestorIdiomas, Idioma, "ERRORCALCULO", Raiz, m_vLongitudesDeCodigos, Mensajes, m_vProcesoErrores
End Sub
