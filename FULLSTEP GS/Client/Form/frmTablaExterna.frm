VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmTablaExterna 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3285
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7425
   Icon            =   "frmTablaExterna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3285
   ScaleWidth      =   7425
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   960
      Top             =   2760
   End
   Begin VB.CommandButton cmdAceptar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2205
      TabIndex        =   0
      Top             =   2880
      Width           =   1125
   End
   Begin VB.CommandButton cmdCancelar 
      BackColor       =   &H00C9D2D6&
      Cancel          =   -1  'True
      Caption         =   "Cerrar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3600
      TabIndex        =   1
      Top             =   2880
      Width           =   1125
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgBuscar 
      Height          =   2685
      Left            =   0
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   0
      Width           =   7335
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   6
      stylesets.count =   2
      stylesets(0).Name=   "ActiveRowBlue"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8388608
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTablaExterna.frx":014A
      stylesets(1).Name=   "Amarillo"
      stylesets(1).BackColor=   65535
      stylesets(1).Picture=   "frmTablaExterna.frx":0166
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Caption=   "CAMPO1"
      Columns(0).Name =   "CAMPO1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "CAMPO2"
      Columns(1).Name =   "CAMPO2"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "CAMPO3"
      Columns(2).Name =   "CAMPO3"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "CAMPO4"
      Columns(3).Name =   "CAMPO4"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "CAMPO5"
      Columns(4).Name =   "CAMPO5"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ART"
      Columns(5).Name =   "ART"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      TabNavigation   =   1
      _ExtentX        =   12938
      _ExtentY        =   4736
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmTablaExterna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Raiz As CRaiz
Public oMensajes  As CMensajes
Public g_strPK As String

Public g_IdPK    As Variant         'valor si ya ha elegido algo (para que salga resaltada la linea)
Public g_sOrigen As String          'Origen
Public g_IdTabla As Integer         'Id de la Tabla
Public g_sPK As String              '
Public g_sIdART As String           'IdArt si existe
Public g_sNombreTabla As String     'Nombre de la Tabla
Public g_sDenoTabla As String       'Descripcion de la Tabla

'---------------------------------------
Private m_blnTablaconArticulo As Boolean
Private m_ador_ART As Ador.Recordset
Private m_oTablaExterna As CTablaExterna
Private m_sSi As String
Private m_sNo As String
Private Sub cmdAceptar_Click()
On Error GoTo Error

    Screen.MousePointer = vbHourglass
    If sdbgBuscar.Rows = 0 Then
        g_strPK = ""
    Else
        g_strPK = CStr(sdbgBuscar.Columns(g_sPK).Value)
    End If
    Screen.MousePointer = vbNormal
    Unload Me
Exit Sub

Error:
    Screen.MousePointer = vbNormal
    oMensajes.DatosModificados
End Sub

Private Sub cmdCancelar_Click()
    g_strPK = "vbFormControlMenu"
    Unload Me
End Sub

Private Sub Form_Load()

    '------------------------------------------------------------------------
    Me.Caption = g_sDenoTabla
        
    
    Set m_oTablaExterna = Raiz.Generar_CTablaExterna
    m_blnTablaconArticulo = m_oTablaExterna.SacarTablaTieneART(g_IdTabla)
    
    CargarRecursos

    'If m_blnTablaconArticulo Then
    '    'Osea tiene ART y esa columna la ocultamos
    'Else
    '    'Osea NO tiene ART mostramos todas las columnas
    'End If
    
    PonerFieldSeparator Me
    
End Sub
'Osea tiene ART y esa columna la ocultamos
Private Sub CargarGrid_Sin_ART()
Dim strNombreCampoART As String
Dim ador1 As Ador.Recordset
Dim strFila As String
Dim strCampo1 As String, strCampo2 As String, strCampo3 As String, strCampo4 As String, strCampo5 As String

'Screen.MousePointer = vbHourglass
sdbgBuscar.RemoveAll

'If m_blnTablaconArticulo Then Exit Sub
strNombreCampoART = UCase(m_oTablaExterna.SacarCampoART(g_IdTabla))
'prs As Recordset, psNombreTabla As String
'_______________________________________________________________________
If Not m_ador_ART.EOF Then
    strCampo1 = UCase("" & m_ador_ART.Fields(0).Value)
    strCampo2 = UCase("" & m_ador_ART.Fields(1).Value)
    strCampo3 = UCase("" & m_ador_ART.Fields(2).Value)
    strCampo4 = UCase("" & m_ador_ART.Fields(3).Value)
    strCampo5 = UCase("" & m_ador_ART.Fields(4).Value)
End If
'�����������������������������������������������������������������������
Set ador1 = m_oTablaExterna.SacarRegistrosTABLAART(m_ador_ART, g_sNombreTabla, g_sIdART, strNombreCampoART)
Do While Not ador1.EOF
    If sdbgBuscar.Columns(0).Visible Then
        strFila = vntBooleanIdioma(CStr("" & ador1.Fields(0).Value), strCampo1)
    Else
        strFila = ""
    End If
    If sdbgBuscar.Columns(1).Visible Then
        strFila = strFila & Chr(m_lSeparador) & vntBooleanIdioma(CStr("" & ador1.Fields(1).Value), strCampo2)
    Else
        strFila = strFila & Chr(m_lSeparador) & ""
    End If
    If sdbgBuscar.Columns(2).Visible Then
        strFila = strFila & Chr(m_lSeparador) & vntBooleanIdioma(CStr("" & ador1.Fields(2).Value), strCampo3)
    Else
        strFila = strFila & Chr(m_lSeparador) & ""
    End If
    If sdbgBuscar.Columns(3).Visible Then
        strFila = strFila & Chr(m_lSeparador) & vntBooleanIdioma(CStr("" & ador1.Fields(3).Value), strCampo4)
    Else
        strFila = strFila & Chr(m_lSeparador) & ""
    End If
    If sdbgBuscar.Columns(4).Visible Then
        strFila = strFila & Chr(m_lSeparador) & vntBooleanIdioma(CStr("" & ador1.Fields(4).Value), strCampo5)
    Else
        strFila = strFila & Chr(m_lSeparador) & ""
    End If

    sdbgBuscar.AddItem strFila

    ador1.MoveNext
Loop
ador1.Close
Set ador1 = Nothing
'Screen.MousePointer = vbNormal
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'UnloadMode
'=========
'vbFormCode  Unload method invoked from code.
'vbAppWindows Current Windows session ending.
'vbFormMDIForm  MDI child form is closing because the MDI form is closing.
'vbFormControlMenu User has chosen Close command from the Control-menu box on a form.
'vbAppTaskManager Windows Task Manager is closing the application.

    If UnloadMode = vbFormControlMenu Then
        g_strPK = "vbFormControlMenu"
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_sOrigen = ""
    g_IdTabla = 0
    g_sIdART = ""
    g_sNombreTabla = ""
    g_sDenoTabla = ""
    Set m_oTablaExterna = Nothing
End Sub
Private Sub sdbgBuscar_DblClick()
    cmdAceptar_Click
End Sub


'aqui pondremos el visible y el width de las columnas y el caption
Private Sub sdbgBuscar_InitColumnProps()
Dim ador1 As Ador.Recordset
Dim max1 As Long, max2 As Long, max3 As Long, max4 As Long, max5 As Long
Dim max1_c As Long, max2_c As Long, max3_c As Long, max4_c As Long, max5_c As Long
Dim sCaption As String
Dim strCampoART As String

If m_blnTablaconArticulo Then

    strCampoART = UCase(m_oTablaExterna.SacarCampoART(g_IdTabla))
    Set ador1 = m_oTablaExterna.SacarCampos_SIN_ART(g_IdTabla, strCampoART)
    Set m_ador_ART = ador1.Clone

    If Not ador1 Is Nothing Then
        If Not ador1.EOF Then
            If IsNull(ador1.Fields(0).Value) Then
                sdbgBuscar.Columns(0).Visible = False
            ElseIf ador1.Fields(0).Value = "" Then
                sdbgBuscar.Columns(0).Visible = False
            Else
                max1 = IIf(g_sIdART = "", 0, m_oTablaExterna.SacarLongitudRegistroMayor(g_sNombreTabla, ador1.Fields(0).Value))
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields(0).Value)
                max1_c = Len(sCaption)
                If max1 > max1_c Then
                    max1 = max1 * 100 'Si el texto es mayor que la cabecera
                Else
                    max1 = max1_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max1 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max1 = 450
                End If
                With sdbgBuscar.Columns(0)
                    .Name = "" & ador1.Fields(0).Value
                    .Visible = True
                    .Width = max1
                    .Caption = sCaption
                    '.Visible = True: .Width = max1: .caption = sCaption
                End With
            End If
            If IsNull(ador1.Fields(1).Value) Then
                sdbgBuscar.Columns(1).Visible = False
            ElseIf ador1.Fields(1).Value = "" Then
                sdbgBuscar.Columns(1).Visible = False
            Else
                max2 = IIf(g_sIdART = "", 0, m_oTablaExterna.SacarLongitudRegistroMayor(g_sNombreTabla, ador1.Fields(1).Value))
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields(1).Value)
                max2_c = Len(sCaption)
                If max2 > max2_c Then
                    max2 = max2 * 100 'Si el texto es mayor que la cabecera
                Else
                    max2 = max2_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max2 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max2 = 450
                End If
                With sdbgBuscar.Columns(1)
                    .Name = "" & ador1.Fields(1).Value
                    .Visible = True
                    .Width = max2
                    .Caption = sCaption
                    '.Visible = True: .Width = max2: .caption = sCaption
                End With
            End If
            If IsNull(ador1.Fields(2).Value) Then
                sdbgBuscar.Columns(2).Visible = False
            ElseIf ador1.Fields(2).Value = "" Then
                sdbgBuscar.Columns(2).Visible = False
            Else
                max3 = IIf(g_sIdART = "", 0, m_oTablaExterna.SacarLongitudRegistroMayor(g_sNombreTabla, ador1.Fields(2).Value))
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields(2).Value)
                max3_c = Len(sCaption)
                If max3 > max3_c Then
                    max3 = max3 * 100 'Si el texto es mayor que la cabecera
                Else
                    max3 = max3_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max3 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max3 = 450
                End If
                With sdbgBuscar.Columns(2)
                    .Name = "" & ador1.Fields(2).Value
                    .Visible = True
                    .Width = max3
                    .Caption = sCaption
                    '.Visible = True: .Width = max3: .caption = sCaption
                End With
            End If
            If IsNull(ador1.Fields(3).Value) Then
                sdbgBuscar.Columns(3).Visible = False
            ElseIf ador1.Fields(3).Value = "" Then
                sdbgBuscar.Columns(3).Visible = False
            Else
                max4 = IIf(g_sIdART = "", 0, m_oTablaExterna.SacarLongitudRegistroMayor(g_sNombreTabla, ador1.Fields(3).Value))
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields(3).Value)
                max4_c = Len(sCaption)
                If max4 > max4_c Then
                    max4 = max4 * 100 'Si el texto es mayor que la cabecera
                Else
                    max4 = max4_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max4 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max4 = 450
                End If
                With sdbgBuscar.Columns(3)
                    .Name = "" & ador1.Fields(3).Value
                    .Visible = True
                    .Width = max4
                    .Caption = sCaption
                    '.Visible = True: .Width = max4: .caption = sCaption
                End With
            End If
            If IsNull(ador1.Fields(4).Value) Then
                sdbgBuscar.Columns(4).Visible = False
            ElseIf ador1.Fields(4).Value = "" Then
                sdbgBuscar.Columns(4).Visible = False
            Else
                max5 = IIf(g_sIdART = "", 0, m_oTablaExterna.SacarLongitudRegistroMayor(g_sNombreTabla, ador1.Fields(4).Value))
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields(4).Value)
                max5_c = Len(sCaption)
                If max5 > max5_c Then
                    max5 = max5 * 100 'Si el texto es mayor que la cabecera
                Else
                    max5 = max5_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max5 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max5 = 450
                End If
                With sdbgBuscar.Columns(4)
                    .Name = "" & ador1.Fields(4).Value
                    .Visible = True
                    .Width = max5
                    .Caption = sCaption
                    '.Visible = True: .Width = max5: .caption = sCaption
                End With
            End If
        End If
    End If

    CargarGrid_Sin_ART

Else

    Dim sNombreTabla As String
    Set ador1 = m_oTablaExterna.SacarCampos(g_IdTabla)
    'Obtenemos el nombre de la tabla y el de los campos de la tabla y cojemos sus descripciones
    If Not ador1 Is Nothing Then
        If Not ador1.EOF Then
            sNombreTabla = NullToStr(ador1.Fields("NOMBRE").Value)
            If IsNull(ador1.Fields("CAMPO1").Value) Then
                sdbgBuscar.Columns(0).Visible = False
            Else
                max1 = m_oTablaExterna.SacarLongitudRegistroMayor(sNombreTabla, ador1.Fields("CAMPO1").Value)
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields("CAMPO1").Value)
                max1_c = Len(sCaption)
                If max1 > max1_c Then
                    max1 = max1 * 100 'Si el texto es mayor que la cabecera
                Else
                    max1 = max1_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max1 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max1 = 450
                End If
                With sdbgBuscar.Columns(0)
                    .Name = "" & ador1.Fields("CAMPO1").Value
                    .Visible = True
                    .Width = max1
                    .Caption = sCaption
                    '.Visible = True: .Width = max1: .caption = sCaption
                End With
            End If
            If IsNull(ador1.Fields("CAMPO2").Value) Then
                sdbgBuscar.Columns(1).Visible = False
            Else
                max2 = m_oTablaExterna.SacarLongitudRegistroMayor(sNombreTabla, ador1.Fields("CAMPO2").Value)
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields("CAMPO2").Value)
                max2_c = Len(sCaption)
                If max2 > max2_c Then
                    max2 = max2 * 100 'Si el texto es mayor que la cabecera
                Else
                    max2 = max2_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max2 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max2 = 450
                End If
                With sdbgBuscar.Columns(1)
                    .Name = "" & ador1.Fields("CAMPO2").Value
                    .Visible = True
                    .Width = max2
                    .Caption = sCaption
                    '.Visible = True: .Width = max2: .caption = sCaption
                End With
            End If
            If IsNull(ador1.Fields("CAMPO3").Value) Then
                sdbgBuscar.Columns(2).Visible = False
            Else
                max3 = m_oTablaExterna.SacarLongitudRegistroMayor(sNombreTabla, ador1.Fields("CAMPO3").Value)
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields("CAMPO3").Value)
                max3_c = Len(sCaption)
                If max3 > max3_c Then '
                    max3 = max3 * 100 'Si el texto es mayor que la cabecera
                Else
                    max3 = max3_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max3 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max3 = 450
                End If
                With sdbgBuscar.Columns(2)
                    .Name = "" & ador1.Fields("CAMPO3").Value
                    .Visible = True
                    .Width = max3
                    .Caption = sCaption
                    '.Visible = True: .Width = max3: .caption = sCaption
                End With
            End If
            If IsNull(ador1.Fields("CAMPO4").Value) Then
                sdbgBuscar.Columns(3).Visible = False
            Else
                max4 = m_oTablaExterna.SacarLongitudRegistroMayor(sNombreTabla, ador1.Fields("CAMPO4").Value)
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields("CAMPO4").Value)
                max4_c = Len(sCaption)
                If max4 > max4_c Then '
                    max4 = max4 * 100 'Si el texto es mayor que la cabecera
                Else
                    max4 = max4_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max4 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max4 = 450
                End If
                With sdbgBuscar.Columns(3)
                    .Name = "" & ador1.Fields("CAMPO4").Value
                    .Visible = True
                    .Width = max4
                    .Caption = sCaption
                    '.Visible = True: .Width = max4: .caption = sCaption
                End With
            End If
            If IsNull(ador1.Fields("CAMPO5").Value) Then
                sdbgBuscar.Columns(4).Visible = False
            Else
                max5 = m_oTablaExterna.SacarLongitudRegistroMayor(sNombreTabla, ador1.Fields("CAMPO5").Value)
                sCaption = m_oTablaExterna.SacarDescripcion(g_sNombreTabla, ador1.Fields("CAMPO5").Value)
                max5_c = Len(sCaption)
                If max5 > max5_c Then '
                    max5 = max5 * 100 'Si el texto es mayor que la cabecera
                Else
                    max5 = max5_c * 150 'Si la cabecera es mayor que el texto
                End If
                If max5 < 450 Then 'Marcamos un tama�o m�nimo de 450 px
                    max5 = 450
                End If
                With sdbgBuscar.Columns(4)
                    .Name = "" & ador1.Fields("CAMPO5").Value
                    .Visible = True
                    .Width = max5
                    .Caption = sCaption
                    '.Visible = True: .Width = max5: .caption = sCaption
                End With
            End If
        End If
    End If

    CargarGrid

End If

ador1.Close
Set ador1 = Nothing
End Sub

Private Sub CargarGrid()
Dim ador1 As Ador.Recordset
Dim ador2 As Ador.Recordset
Dim sNombreTabla As String
Dim sCampo1 As String, sCampo2 As String, sCampo3 As String, sCampo4 As String, sCampo5 As String
Dim strFila As String

'Screen.MousePointer = vbHourglass
sdbgBuscar.RemoveAll

Set ador1 = m_oTablaExterna.SacarCampos(g_IdTabla)
'Obtenemos el nombre de la tabla y el de los campos de la tabla y cojemos sus descripciones
If Not ador1 Is Nothing Then
    If Not ador1.EOF Then
        'NOMBRE
        sNombreTabla = NullToStr(ador1.Fields("NOMBRE").Value)
        '"DENOMINACION"
        '"CAMPO1"
        sCampo1 = NullToStr(ador1.Fields("CAMPO1").Value)
        '"CAMPO2"
        sCampo2 = NullToStr(ador1.Fields("CAMPO2").Value)
        '"CAMPO3"
        sCampo3 = NullToStr(ador1.Fields("CAMPO3").Value)
        '"CAMPO4"
        sCampo4 = NullToStr(ador1.Fields("CAMPO4").Value)
        '"CAMPO5"
        sCampo5 = NullToStr(ador1.Fields("CAMPO5").Value)
        
        Set ador2 = m_oTablaExterna.SacarCamposTabla(sNombreTabla)
        Do While Not ador2.EOF
            If sdbgBuscar.Columns(0).Visible Then
                strFila = vntBooleanIdioma(CStr("" & ador2.Fields(0).Value), sCampo1)
            Else
                strFila = ""
            End If
            If sdbgBuscar.Columns(1).Visible Then
                strFila = strFila & Chr(m_lSeparador) & vntBooleanIdioma(CStr("" & ador2.Fields(1).Value), sCampo2)
            Else
                strFila = strFila & Chr(m_lSeparador) & ""
            End If
            If sdbgBuscar.Columns(2).Visible Then
                strFila = strFila & Chr(m_lSeparador) & vntBooleanIdioma(CStr("" & ador2.Fields(2).Value), sCampo3)
            Else
                strFila = strFila & Chr(m_lSeparador) & ""
            End If
            If sdbgBuscar.Columns(3).Visible Then
                strFila = strFila & Chr(m_lSeparador) & vntBooleanIdioma(CStr("" & ador2.Fields(3).Value), sCampo4)
            Else
                strFila = strFila & Chr(m_lSeparador) & ""
            End If
            If sdbgBuscar.Columns(4).Visible Then
                strFila = strFila & Chr(m_lSeparador) & vntBooleanIdioma(CStr("" & ador2.Fields(4).Value), sCampo5)
            Else
                strFila = strFila & Chr(m_lSeparador) & ""
            End If

            sdbgBuscar.AddItem strFila

            ador2.MoveNext
        Loop
    End If
End If
ador1.Close
Set ador1 = Nothing
ador2.Close
Set ador2 = Nothing
'Screen.MousePointer = vbNormal
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_TABLAEXTERNA, Idioma)

    If Not Ador Is Nothing Then
        m_sSi = Ador(0).Value
        Ador.MoveNext
        m_sNo = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.Close
    End If
    Set Ador = Nothing
    'Nota: El caption de la ventana se le pasa desde el frm que la llama
End Sub



Private Sub ResaltarLinea()
Dim i As Integer
Dim vbm As Variant

For i = 0 To sdbgBuscar.Rows - 1
    vbm = sdbgBuscar.AddItemBookmark(i)
    If sdbgBuscar.Columns(g_sPK).CellValue(vbm) = CStr(g_IdPK) Then
        sdbgBuscar.Row = i
        sdbgBuscar.ActiveRowStyleSet = "ActiveRowBlue"
        Exit For
    End If
Next
End Sub

Private Sub Timer1_Timer()
    '_______________________________________________________________________
    'Si ya esta una linea elegida la resaltamos con el StyleSet
    If Not IsEmpty(g_IdPK) Then
        ResaltarLinea
        Timer1.Enabled = False
    End If
    '�����������������������������������������������������������������������
End Sub
Private Function vntBooleanIdioma(pDato, pCampo As String) As Variant
    'me aseguro de q la columna es de tipo bit  y luego comparo por true o false
    If m_oTablaExterna.blnColumnaEsTipoBit(g_sNombreTabla, pCampo) Then
        If UCase(pDato) = "TRUE" Then
            vntBooleanIdioma = m_sSi
        ElseIf UCase(pDato) = "FALSE" Then
            vntBooleanIdioma = m_sNo
        End If
    Else
        vntBooleanIdioma = pDato
    End If
End Function
