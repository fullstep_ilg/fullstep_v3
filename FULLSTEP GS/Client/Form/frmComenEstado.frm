VERSION 5.00
Begin VB.Form frmComenEstado 
   BackColor       =   &H00808000&
   Caption         =   "DComentario del estado actual"
   ClientHeight    =   3420
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8835
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmComenEstado.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3420
   ScaleWidth      =   8835
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtComentario 
      Height          =   2835
      Left            =   75
      MaxLength       =   2000
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   45
      Width           =   8640
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   3300
      TabIndex        =   0
      Top             =   3000
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   4500
      TabIndex        =   2
      Top             =   3000
      Width           =   1005
   End
End
Attribute VB_Name = "frmComenEstado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmComenEstado
''' *** Creacion: 13/08/2001


Option Explicit
Public GestorIdiomas As CGestorIdiomas
Public Comentario As String
Private ComentarioIni As String
Public Idioma As String
Public sOrigen As String
''' INTEGRACION
Public bNoModificar As Boolean


Private Sub ConfigurarSeguridad()
cmdAceptar.Visible = Not bNoModificar
cmdCancelar.Visible = Not bNoModificar
txtComentario.Locked = bNoModificar
End Sub

''' <summary>
''' Devuelve le Comentario
''' </summary>
''' <returns>Comentario</returns>
''' <remarks>Llamada desde: Evento del boton Aceptar Tiempo m�ximo: 0,1</remarks>

Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos

Comentario = txtComentario.Text
Unload Me
 
End Sub

Private Sub cmdCancelar_Click()

Comentario = ComentarioIni
Unload Me
        
End Sub

''' <summary>
''' Activacion del formulario
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento del formulario Tiempo m�ximo: 0,1</remarks>

Private Sub Form_Activate()

ComentarioIni = Comentario
txtComentario.Text = Comentario
ConfigurarSeguridad

End Sub

Private Sub Form_Load()

    Me.Height = 4820
    Me.Width = 7480

    CargarRecursos
    txtComentario.Text = ""
    
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Adodb.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_COMENESTADO, Idioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        If sOrigen = "frmPEDIDOS" Or sOrigen = "frmPedidosEmision2" Or sOrigen = "frmSeguimientoLinea" Then
            Me.Caption = Ador(0).Value
        End If
        
        Ador.MoveNext
        If sOrigen = "frmSeguimiento" Then
            Me.Caption = Ador(0).Value
        End If
        
        Ador.MoveNext
        If sOrigen = "EstadoOrden" Or sOrigen = "EstadoLinea" Then
            Me.Caption = Ador(0).Value
        End If
        Ador.MoveNext
        If sOrigen = "frmSeguimientoInt" Then   '7
            Me.Caption = Ador(0).Value
        End If
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Resize()
  If Me.Height > 2900 And Me.Width > 500 Then
      txtComentario.Height = Me.Height - 945
      txtComentario.Width = Me.Width - 320
      cmdAceptar.Top = txtComentario.Height + 150
      cmdCancelar.Top = cmdAceptar.Top
      cmdAceptar.Left = txtComentario.Width / 2 - cmdAceptar.Width - 50
      cmdCancelar.Left = txtComentario.Width / 2 + 50
  End If
  
End Sub

