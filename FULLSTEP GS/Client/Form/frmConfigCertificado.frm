VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmConfigCertificado 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DConfiguraci�n del certificado"
   ClientHeight    =   13185
   ClientLeft      =   975
   ClientTop       =   2700
   ClientWidth     =   9945
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmConfigCertificado.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   13185
   ScaleWidth      =   9945
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   1440
      TabIndex        =   1
      Top             =   10380
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   10380
      Width           =   1215
   End
   Begin VB.PictureBox picTarea 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   10185
      Left            =   0
      ScaleHeight     =   10185
      ScaleWidth      =   9915
      TabIndex        =   2
      Top             =   0
      Width           =   9912
      Begin VB.PictureBox picCondOtroCertif 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   120
         ScaleHeight     =   315
         ScaleWidth      =   9855
         TabIndex        =   70
         Top             =   3600
         Width           =   9855
         Begin VB.CommandButton cmdCondiciones 
            Caption         =   "DCondiciones"
            Height          =   315
            Left            =   8400
            TabIndex        =   14
            Top             =   0
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.CheckBox chkCondOtroCertif 
            Caption         =   "DQue cumpla condiciones en base a otro Tipo de certificado:"
            Height          =   195
            Left            =   0
            TabIndex        =   12
            Top             =   45
            Width           =   4935
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCertificados 
            Height          =   285
            Left            =   5040
            TabIndex        =   13
            Top             =   15
            Visible         =   0   'False
            Width           =   3135
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "COD_DEN"
            Columns(2).Name =   "COD_DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "ID"
            Columns(3).Name =   "ID"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FORMULARIO"
            Columns(4).Name =   "FORMULARIO"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   5530
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.Frame frRegExp 
         BorderStyle     =   0  'None
         Height          =   2000
         Left            =   0
         TabIndex        =   67
         Top             =   1500
         Visible         =   0   'False
         Width           =   9855
         Begin SSDataWidgets_B.SSDBDropDown sdbddValoresOperador 
            Height          =   300
            Left            =   4320
            TabIndex        =   68
            Top             =   1500
            Width           =   2415
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   106
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "OPERADOR"
            Columns(1).Name =   "OPERADOR"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4260
            _ExtentY        =   529
            _StockProps     =   77
         End
         Begin VB.CommandButton cmdValidar 
            Caption         =   "D&Validar"
            Height          =   315
            Left            =   1440
            TabIndex        =   11
            Top             =   1500
            Width           =   1215
         End
         Begin VB.CommandButton cmdEliminar 
            Caption         =   "D&Eliminar"
            Height          =   315
            Left            =   120
            TabIndex        =   10
            Top             =   1500
            Width           =   1215
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgRegExp 
            Height          =   1200
            Left            =   120
            TabIndex        =   9
            Top             =   240
            Width           =   9650
            _Version        =   196617
            DataMode        =   2
            BorderStyle     =   0
            Col.Count       =   8
            stylesets.count =   4
            stylesets(0).Name=   "ExpRegOK"
            stylesets(0).BackColor=   65280
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmConfigCertificado.frx":0CB2
            stylesets(1).Name=   "Normal"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmConfigCertificado.frx":0CCE
            stylesets(2).Name=   "ExpRegKO"
            stylesets(2).ForeColor=   16777215
            stylesets(2).BackColor=   255
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmConfigCertificado.frx":0CEA
            stylesets(3).Name=   "Disabled"
            stylesets(3).BackColor=   15790320
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmConfigCertificado.frx":0D06
            AllowAddNew     =   -1  'True
            AllowColumnSwapping=   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   291
            Columns.Count   =   8
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "CONTROL_ROW"
            Columns(0).Name =   "CONTROL_ROW"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   11
            Columns(0).FieldLen=   256
            Columns(0).HeadStyleSet=   "Disabled"
            Columns(1).Width=   847
            Columns(1).Caption=   "OPERADOR"
            Columns(1).Name =   "OPERADOR"
            Columns(1).Alignment=   2
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).Style=   3
            Columns(1).HasHeadForeColor=   -1  'True
            Columns(1).HeadForeColor=   -2147483633
            Columns(2).Width=   2990
            Columns(2).Caption=   "CAMPO_PROVE"
            Columns(2).Name =   "CAMPO_PROVE"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   3889
            Columns(3).Caption=   "EXPREG_DESCR"
            Columns(3).Name =   "EXPREG_DESCR"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   4842
            Columns(4).Caption=   "EXPREG"
            Columns(4).Name =   "EXPREG"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3519
            Columns(5).Caption=   "VALOR_VALIDACION"
            Columns(5).Name =   "VALOR_VALIDACION"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "FILA_CORRECTA"
            Columns(6).Name =   "FILA_CORRECTA"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   11
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "VALIDACION"
            Columns(7).Name =   "VALIDACION"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   11
            Columns(7).FieldLen=   256
            _ExtentX        =   17022
            _ExtentY        =   2117
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValoresCampoProve 
            Height          =   300
            Left            =   6840
            TabIndex        =   69
            Top             =   1500
            Width           =   2895
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   106
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5106
            _ExtentY        =   529
            _StockProps     =   77
         End
      End
      Begin VB.CheckBox chkCertifExpReg 
         Caption         =   "DQue cumplan con un patr�n de validaci�n:"
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   1300
         Width           =   5175
      End
      Begin VB.Frame frInicial 
         BorderStyle     =   0  'None
         Height          =   6210
         Left            =   0
         TabIndex        =   26
         Top             =   3975
         Width           =   9950
         Begin VB.CheckBox ChkPermitirFueraPlazo 
            Caption         =   "DPermitir env�ar fuera del plazo de cumplimentaci�n"
            Height          =   255
            Left            =   3840
            TabIndex        =   21
            Top             =   1920
            Width           =   5895
         End
         Begin VB.CheckBox ChkActivarEnvioConjunto 
            Caption         =   "DActivar env�o conjunto de certificados"
            Height          =   255
            Left            =   240
            TabIndex        =   20
            Top             =   1920
            Width           =   3135
         End
         Begin VB.Frame fraCumplimentacion 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   90
            TabIndex        =   28
            Top             =   1250
            Width           =   6012
            Begin VB.OptionButton optObligatoria 
               Caption         =   "DCumplimentaci�n obligatoria"
               Height          =   255
               Left            =   120
               TabIndex        =   17
               Top             =   60
               Width           =   2508
            End
            Begin VB.OptionButton optOpcional 
               Caption         =   "DCumplimentaci�n opcional"
               Height          =   255
               Left            =   2880
               TabIndex        =   18
               Top             =   60
               Width           =   2388
            End
         End
         Begin VB.CheckBox chkSolicitMail 
            Caption         =   "DNotificar la solicitud v�a email"
            Height          =   195
            Left            =   120
            TabIndex        =   15
            Top             =   0
            Width           =   3615
         End
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   0
            TabIndex        =   27
            Top             =   2640
            Width           =   9912
            Begin VB.OptionButton optExpirSin 
               Caption         =   "DSin expiraci�n"
               Height          =   255
               Left            =   8280
               TabIndex        =   24
               Top             =   30
               Width           =   1548
            End
            Begin VB.OptionButton optExpirTipo 
               Caption         =   "DConfigurar expiraci�n por tipo de proveedor"
               Height          =   255
               Left            =   4320
               TabIndex        =   23
               Top             =   30
               Width           =   3708
            End
            Begin VB.OptionButton optExpirTodos 
               Caption         =   "DConfigurar expiraci�n para todos los proveedores"
               Height          =   255
               Left            =   120
               TabIndex        =   22
               Top             =   30
               Width           =   4428
            End
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCumplimentador 
            Height          =   285
            Left            =   3840
            TabIndex        =   16
            Top             =   900
            Width           =   2655
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4678
            _ExtentY        =   508
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPlazoCumpl 
            Height          =   285
            Left            =   3840
            TabIndex        =   19
            Top             =   1650
            Width           =   2655
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4678
            _ExtentY        =   508
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin TabDlg.SSTab SSTabExpir 
            Height          =   2900
            Left            =   120
            TabIndex        =   25
            Top             =   3200
            Width           =   9735
            _ExtentX        =   17171
            _ExtentY        =   5106
            _Version        =   393216
            TabHeight       =   420
            TabCaption(0)   =   "DPotenciales"
            TabPicture(0)   =   "frmConfigCertificado.frx":0D22
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "fraPlazoExpirP"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "fraExpirP"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).ControlCount=   2
            TabCaption(1)   =   "DReales"
            TabPicture(1)   =   "frmConfigCertificado.frx":0D3E
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "fraExpirR"
            Tab(1).Control(1)=   "fraPlazoExpirR"
            Tab(1).ControlCount=   2
            TabCaption(2)   =   "DTodos los proveedores"
            TabPicture(2)   =   "frmConfigCertificado.frx":0D5A
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "fraExpir"
            Tab(2).Control(1)=   "fraPlazoExpir"
            Tab(2).ControlCount=   2
            Begin VB.Frame fraExpir 
               Caption         =   "DFecha de expiraci�n en base a: "
               Height          =   1600
               Left            =   -74880
               TabIndex        =   54
               Top             =   480
               Width           =   9492
               Begin VB.OptionButton optFecForm 
                  Caption         =   "DCampo del formulario"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   57
                  Top             =   1100
                  Width           =   2028
               End
               Begin VB.OptionButton optFecCumpl 
                  Caption         =   "DFecha de cumplimentaci�n"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   56
                  Top             =   700
                  Width           =   4428
               End
               Begin VB.OptionButton optFecSolic 
                  Caption         =   "DFecha de solicitud"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   55
                  Top             =   300
                  Width           =   4428
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcGrupo 
                  Height          =   288
                  Left            =   3000
                  TabIndex        =   58
                  Top             =   1100
                  Width           =   2652
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4678
                  _ExtentY        =   508
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCampo 
                  Height          =   288
                  Left            =   6680
                  TabIndex        =   59
                  Top             =   1100
                  Width           =   2652
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4678
                  _ExtentY        =   508
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblSeccion3C 
                  Caption         =   "DCampo:"
                  Height          =   252
                  Left            =   5880
                  TabIndex        =   61
                  Top             =   1100
                  Width           =   852
               End
               Begin VB.Label lblSeccion3B 
                  Caption         =   "DGrupo:"
                  Height          =   252
                  Left            =   2280
                  TabIndex        =   60
                  Top             =   1100
                  Width           =   852
               End
            End
            Begin VB.Frame fraPlazoExpir 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   492
               Left            =   -74880
               TabIndex        =   51
               Top             =   2100
               Width           =   9492
               Begin SSDataWidgets_B.SSDBCombo sdbcPlazoExpir 
                  Height          =   288
                  Left            =   3000
                  TabIndex        =   52
                  Top             =   120
                  Width           =   2652
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4678
                  _ExtentY        =   508
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblSeccion3D 
                  Caption         =   "DPlazo de expiraci�n:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   252
                  Left            =   120
                  TabIndex        =   53
                  Top             =   120
                  Width           =   1812
               End
            End
            Begin VB.Frame fraExpirR 
               Caption         =   "DFecha de expiraci�n en base a: "
               Height          =   1600
               Left            =   -74880
               TabIndex        =   43
               Top             =   480
               Width           =   9492
               Begin VB.OptionButton optFecSolicR 
                  Caption         =   "DFecha de solicitud"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   46
                  Top             =   300
                  Width           =   4428
               End
               Begin VB.OptionButton optFecCumplR 
                  Caption         =   "DFecha de cumplimentaci�n"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   45
                  Top             =   700
                  Width           =   4428
               End
               Begin VB.OptionButton optFecFormR 
                  Caption         =   "DCampo del formulario"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   44
                  Top             =   1100
                  Width           =   2028
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcGrupoR 
                  Height          =   288
                  Left            =   3000
                  TabIndex        =   47
                  Top             =   1100
                  Width           =   2652
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4678
                  _ExtentY        =   508
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCampoR 
                  Height          =   288
                  Left            =   6680
                  TabIndex        =   48
                  Top             =   1100
                  Width           =   2652
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4678
                  _ExtentY        =   508
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblSeccion3BR 
                  Caption         =   "DGrupo:"
                  Height          =   252
                  Left            =   2280
                  TabIndex        =   50
                  Top             =   1100
                  Width           =   852
               End
               Begin VB.Label lblSeccion3CR 
                  Caption         =   "DCampo:"
                  Height          =   252
                  Left            =   5880
                  TabIndex        =   49
                  Top             =   1100
                  Width           =   852
               End
            End
            Begin VB.Frame fraPlazoExpirR 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   492
               Left            =   -74880
               TabIndex        =   40
               Top             =   2100
               Width           =   9492
               Begin SSDataWidgets_B.SSDBCombo sdbcPlazoExpirR 
                  Height          =   288
                  Left            =   3000
                  TabIndex        =   41
                  Top             =   120
                  Width           =   2652
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4678
                  _ExtentY        =   508
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblSeccion3DR 
                  Caption         =   "DPlazo de expiraci�n:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   252
                  Left            =   120
                  TabIndex        =   42
                  Top             =   120
                  Width           =   1812
               End
            End
            Begin VB.Frame fraExpirP 
               Caption         =   "DFecha de expiraci�n en base a: "
               Height          =   1600
               Left            =   120
               TabIndex        =   32
               Top             =   480
               Width           =   9492
               Begin VB.OptionButton optFecFormP 
                  Caption         =   "DCampo del formulario"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   35
                  Top             =   1100
                  Width           =   2028
               End
               Begin VB.OptionButton optFecCumplP 
                  Caption         =   "DFecha de cumplimentaci�n"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   34
                  Top             =   700
                  Width           =   4428
               End
               Begin VB.OptionButton optFecSolicP 
                  Caption         =   "DFecha de solicitud"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   33
                  Top             =   300
                  Width           =   4428
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcGrupoP 
                  Height          =   288
                  Left            =   3000
                  TabIndex        =   36
                  Top             =   1100
                  Width           =   2652
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4678
                  _ExtentY        =   508
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCampoP 
                  Height          =   288
                  Left            =   6680
                  TabIndex        =   37
                  Top             =   1100
                  Width           =   2652
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4678
                  _ExtentY        =   508
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblSeccion3CP 
                  Caption         =   "DCampo:"
                  Height          =   252
                  Left            =   5880
                  TabIndex        =   39
                  Top             =   1100
                  Width           =   852
               End
               Begin VB.Label lblSeccion3BP 
                  Caption         =   "DGrupo:"
                  Height          =   252
                  Left            =   2280
                  TabIndex        =   38
                  Top             =   1100
                  Width           =   852
               End
            End
            Begin VB.Frame fraPlazoExpirP 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   492
               Left            =   120
               TabIndex        =   29
               Top             =   2100
               Width           =   9492
               Begin SSDataWidgets_B.SSDBCombo sdbcPlazoExpirP 
                  Height          =   288
                  Left            =   3000
                  TabIndex        =   30
                  Top             =   120
                  Width           =   2652
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4678
                  _ExtentY        =   508
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblSeccion3DP 
                  Caption         =   "DPlazo de expiraci�n:"
                  Height          =   252
                  Left            =   120
                  TabIndex        =   31
                  Top             =   120
                  Width           =   1812
               End
            End
         End
         Begin VB.Label lblSeccion3 
            Caption         =   "D�C�mo establecer la fecha de expiraci�n del certificado?"
            Height          =   255
            Left            =   120
            TabIndex        =   66
            Top             =   2400
            Width           =   5175
         End
         Begin VB.Label lblSeccion2C 
            Caption         =   "Dde la fecha de solicitud"
            Height          =   255
            Left            =   6720
            TabIndex        =   65
            Top             =   1650
            Width           =   3135
         End
         Begin VB.Label lblSeccion2B 
            Caption         =   "DPlazo de cumplimentaci�n:"
            Height          =   255
            Left            =   240
            TabIndex        =   64
            Top             =   1650
            Width           =   3135
         End
         Begin VB.Label lblSeccion2A 
            Caption         =   "DCuando lo env�e el:"
            Height          =   255
            Left            =   240
            TabIndex        =   63
            Top             =   900
            Width           =   3135
         End
         Begin VB.Label lblSeccion2 
            Caption         =   "D�Cu�ndo considerar que el certificado est� cumplimentado?"
            Height          =   255
            Left            =   120
            TabIndex        =   62
            Top             =   570
            Width           =   5175
         End
         Begin VB.Line Line 
            Index           =   0
            X1              =   90
            X2              =   9850
            Y1              =   2300
            Y2              =   2300
         End
         Begin VB.Line Line 
            Index           =   1
            X1              =   105
            X2              =   9865
            Y1              =   390
            Y2              =   390
         End
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcSolicit 
         Height          =   288
         Left            =   3840
         TabIndex        =   5
         Top             =   480
         Width           =   2652
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4678
         _ExtentY        =   508
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoProveedor 
         Height          =   285
         Left            =   3840
         TabIndex        =   7
         Top             =   870
         Width           =   2655
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4678
         _ExtentY        =   508
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblTipoProveedor 
         Caption         =   "DTipo Proveedor"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   900
         Width           =   3135
      End
      Begin VB.Label lblSeccion1A 
         Caption         =   "DSolicitar a los proveedores de forma:"
         Height          =   252
         Left            =   120
         TabIndex        =   4
         Top             =   480
         Width           =   3132
      End
      Begin VB.Label lblSeccion1 
         Caption         =   "D�C�mo solicitar el certificado a los proveedores?"
         Height          =   252
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   5172
      End
   End
End
Attribute VB_Name = "frmConfigCertificado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public GestorIdiomas As CGestorIdiomas
Public GestorSeguridad As CGestorSeguridad
Public Idioma As String
Public Raiz As CRaiz
Public oMensajes As CMensajes
Public UsuCod As String
Public g_oSolicitud As CSolicitud

'Variables de idiomas
Private m_sManual As String
Private m_sAutomatica As String
Private m_sCumplProveedor As String
Private m_sCumplPeticionario As String
Private m_sCumplAmbos As String
Private m_sIdioma(1 To 8) As String
Private m_bHacerValidaciones As Boolean
Private m_bGuardando As Boolean
Private m_sNoHayExpReg As String
Private m_sErrorExpReg As String
Private m_sTodos As String
Private m_sPotencial As String
Private m_sReal As String
Private m_bCambiosRealizados_ExpReg As Boolean
Private m_bModificar As Boolean
Private m_ParamGen As ParametrosGenerales
Private m_vLongitudesDeCodigos As LongitudesDeCodigos
Private m_bHayCambiosCondiciones As Boolean

Public Property Get ParamGen() As ParametrosGenerales
    ParamGen = m_ParamGen
End Property
Public Property Let ParamGen(ByRef vParametrosGenerales As ParametrosGenerales)
    m_ParamGen = vParametrosGenerales
End Property

Public Property Get LongitudesDeCodigos() As LongitudesDeCodigos
    LongitudesDeCodigos = m_vLongitudesDeCodigos
End Property
Public Property Let LongitudesDeCodigos(ByRef vParametrosGenerales As LongitudesDeCodigos)
    m_vLongitudesDeCodigos = vParametrosGenerales
End Property

Private Sub ChkActivarEnvioConjunto_Click()
    chkCondOtroCertif.Enabled = Not (ChkActivarEnvioConjunto.Value = 1)
End Sub

Private Sub chkCertifExpReg_Click()
    If chkCertifExpReg.Value = 1 Then chkCondOtroCertif.Value = 0
    RedimensionarForm
End Sub

Private Sub chkCondOtroCertif_Click()
    If chkCondOtroCertif.Value = 1 Then chkCertifExpReg.Value = 0
    sdbcCertificados.Visible = (chkCondOtroCertif.Value = 1)
    cmdCondiciones.Visible = (chkCondOtroCertif.Value = 1 And sdbcCertificados.Value <> "")
    ChkActivarEnvioConjunto.Enabled = Not (chkCondOtroCertif.Value = 1)
End Sub

Private Sub cmdCondiciones_Click()
    Dim oFrm As frmCertifCondicionOtroCertif
    
    Set oFrm = New frmCertifCondicionOtroCertif
    With oFrm
        Set .GestorIdiomas = GestorIdiomas
        Set .Raiz = Raiz
        Set .Mensajes = oMensajes
        Set .GestorSeguridad = GestorSeguridad
        Set .Solicitud = g_oSolicitud
        .UsuCod = UsuCod
        .Idioma = Idioma
        .FormCondiciones = sdbcCertificados.Columns("FORMULARIO").Value
        .LongitudesDeCodigos = m_vLongitudesDeCodigos
        .ParamGen = m_ParamGen
        
        .Show vbModal
        
        m_bHayCambiosCondiciones = .HayCambiosCondiciones
    End With
                 
    Unload oFrm
    Set oFrm = Nothing
End Sub

Private Sub cmdEliminar_Click()
    If Not sdbgRegExp.SelBookmarks.Count = 0 Then
        If sdbgRegExp.Rows = 1 Then
            oMensajes.mensajeGenericoOkOnly m_sNoHayExpReg
        Else
            sdbgRegExp.DeleteSelected
        End If
    End If
End Sub

Private Sub cmdValidar_Click()
    Dim vbm As Variant
    Dim i As Integer
    Dim objRegExp As RegExp
    
    Set objRegExp = New RegExp
    With objRegExp
        .IgnoreCase = True
        .Global = True
    End With
    m_bHacerValidaciones = False
    sdbgRegExp.Update
    For i = 0 To sdbgRegExp.Rows - 1
        sdbgRegExp.SetFocus
        vbm = sdbgRegExp.RowBookmark(i)
        
        objRegExp.Pattern = sdbgRegExp.Columns("EXPREG").CellValue(vbm)
        sdbgRegExp.Row = i
        If sdbgRegExp.Columns("VALOR_VALIDACION").CellText(vbm) = "" Or IsEmpty(vbm) Then
            sdbgRegExp.Columns("VALIDACION").Value = Nothing
        Else
            If objRegExp.Test(sdbgRegExp.Columns("VALOR_VALIDACION").CellValue(vbm)) Then
                sdbgRegExp.Columns("VALIDACION").Value = True
                sdbgRegExp.Columns("VALOR_VALIDACION").CellStyleSet "ExpRegOK", i
                sdbgRegExp.Update
            Else
                sdbgRegExp.Columns("VALIDACION").Value = False
                sdbgRegExp.Columns("VALOR_VALIDACION").CellStyleSet "ExpRegKO", i
                sdbgRegExp.Update
            End If
        End If
    Next
    m_bHacerValidaciones = True
End Sub

Private Sub optFecCumpl_Click()
    sdbcGrupo.Enabled = False
    sdbcCampo.Enabled = False
End Sub

Private Sub optFecCumplP_Click()
    sdbcGrupoP.Enabled = False
    sdbcCampoP.Enabled = False
    sdbcGrupoR.Enabled = False
    sdbcCampoR.Enabled = False
    optFecCumplR.Value = True
End Sub

Private Sub optFecCumplR_Click()
    sdbcGrupoR.Enabled = False
    sdbcCampoR.Enabled = False
    sdbcGrupoP.Enabled = False
    sdbcCampoP.Enabled = False
    optFecCumplP.Value = True
End Sub

Private Sub optFecForm_Click()
    sdbcGrupo.Enabled = True
    sdbcCampo.Enabled = True
End Sub

Private Sub optFecFormP_Click()
    sdbcGrupoP.Enabled = True
    sdbcCampoP.Enabled = True
    sdbcGrupoR.Enabled = True
    sdbcCampoR.Enabled = True
    optFecFormR.Value = True
End Sub

Private Sub optFecFormR_Click()
    sdbcGrupoR.Enabled = True
    sdbcCampoR.Enabled = True
    sdbcGrupoP.Enabled = True
    sdbcCampoP.Enabled = True
    optFecFormP.Value = True
End Sub

Private Sub optFecSolic_Click()
    sdbcGrupo.Enabled = False
    sdbcCampo.Enabled = False
End Sub

Private Sub optFecSolicP_Click()
    sdbcGrupoP.Enabled = False
    sdbcCampoP.Enabled = False
    sdbcGrupoR.Enabled = False
    sdbcCampoR.Enabled = False
    optFecSolicR.Value = True
End Sub

Private Sub optFecSolicR_Click()
    sdbcGrupoR.Enabled = False
    sdbcCampoR.Enabled = False
    sdbcGrupoP.Enabled = False
    sdbcCampoP.Enabled = False
    optFecSolicP.Value = True
End Sub


Private Sub optObligatoria_Click()
    'Mostrar el plazo de cumplimiento
    lblSeccion2B.Visible = True
    sdbcPlazoCumpl.Visible = True
    lblSeccion2C.Visible = True
End Sub

Private Sub optOpcional_Click()
    'Ocultar el plazo de cumplimiento
    lblSeccion2B.Visible = False
    sdbcPlazoCumpl.Visible = False
    lblSeccion2C.Visible = False
End Sub

Private Sub sdbcCertificados_Change()
    cmdCondiciones.Visible = False
End Sub

Private Sub sdbcCertificados_CloseUp()
    g_oSolicitud.SolicitudCondiciones = sdbcCertificados.Columns("ID").Value
    g_oSolicitud.CargarCondicionesSolicitud sdbcCertificados.Columns("ID").Value
    cmdCondiciones.Visible = (Trim(sdbcCertificados.Text) <> "")
End Sub

Private Sub sdbcCertificados_DropDown()
    Dim oSolicitudes As CSolicitudes
    Dim oSolicitud As CSolicitud
    
    sdbcCertificados.RemoveAll
    
    Set oSolicitudes = Raiz.Generar_CSolicitudes
    oSolicitudes.CargarCertificados Idioma
    If oSolicitudes.Count > 0 Then
        oSolicitudes.Remove CStr(g_oSolicitud.Id)
        For Each oSolicitud In oSolicitudes
            sdbcCertificados.AddItem oSolicitud.Codigo & Chr(m_lSeparador) & oSolicitud.Denominaciones.Item(Idioma).Den & Chr(m_lSeparador) & oSolicitud.Codigo & " - " & oSolicitud.Denominaciones.Item(Idioma).Den & _
                Chr(m_lSeparador) & oSolicitud.Id & Chr(m_lSeparador) & oSolicitud.Formulario.Id
        Next
    End If
    
    Set oSolicitud = Nothing
    Set oSolicitudes = Nothing
End Sub

Private Sub sdbcCertificados_InitColumnProps()
    sdbcCertificados.DataFieldList = "Column 0"
    sdbcCertificados.DataFieldToDisplay = "Column 2"
End Sub

Private Sub sdbcCertificados_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    
    With sdbcCertificados
        .MoveFirst
        If Text <> "" Then
            For i = 0 To .Rows - 1
                bm = .GetBookmark(i)
                If UCase(Text) = UCase(Mid(.Columns("COD").CellText(bm), 1, Len(Text))) Then
                    .Bookmark = bm
                    Exit For
                End If
            Next i
        End If
    End With
End Sub

Private Sub sdbcCertificados_Validate(Cancel As Boolean)
    Dim oSolicitudes As CSolicitudes
    Dim lPos As Long
    Dim sCod As String
    
    With sdbcCertificados
        lPos = InStr(1, .Text, " - ")
        If lPos > 0 Then
            sCod = Left(.Text, lPos - 1)
        Else
            sCod = .Text
        End If
        If Trim(sCod) <> "" Then
            If UCase(sCod) <> UCase(.Columns("COD").Value) Then
                Set oSolicitudes = Raiz.Generar_CSolicitudes
                oSolicitudes.CargarCertificados Idioma, sCod
            
                If oSolicitudes.Count = 0 Then
                    .Value = ""
                    Exit Sub
                Else
                    If UCase(oSolicitudes.Item(1).Codigo) <> UCase(sCod) Then
                        .Value = ""
                        Exit Sub
                    Else
                        .Columns("COD").Value = oSolicitudes.Item(1).Codigo
                        .Columns("DEN").Value = oSolicitudes.Item(1).Denominaciones.Item(Idioma).Den
                        .Columns("COD_DEN").Value = oSolicitudes.Item(1).Codigo & " - " & oSolicitudes.Item(1).Denominaciones.Item(Idioma).Den
                    End If
                End If
                
                Set oSolicitudes = Nothing
            End If
            
            .Value = .Columns("COD").Value
            .Text = .Columns("COD_DEN").Value
        End If
        
        cmdCondiciones.Visible = (Trim(.Text) <> "")
    End With
End Sub

'Combo Modo Solicitud
Private Sub sdbcSolicit_CloseUp()
    sdbcSolicit.Text = sdbcSolicit.Columns(1).Text
End Sub

Private Sub sdbcSolicit_DropDown()
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    sdbcSolicit.RemoveAll
  
    sdbcSolicit.AddItem 0 & Chr(m_lSeparador) & m_sManual
    sdbcSolicit.AddItem 1 & Chr(m_lSeparador) & m_sAutomatica
    
    sdbcSolicit.SelStart = 0
    sdbcSolicit.SelLength = Len(sdbcSolicit.Text)
    sdbcSolicit.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcSolicit_InitColumnProps()
    sdbcSolicit.DataFieldList = "Column 1"
    sdbcSolicit.DataFieldToDisplay = "Column 1"
End Sub
Private Sub sdbcSolicit_PositionList(ByVal Text As String)
    PositionList sdbcSolicit, Text
End Sub

Private Sub PositionList(ByRef sdbcControl As Object, ByVal Text As String, Optional ByVal vCol As Variant = 0)
Dim i As Long
Dim bm As Variant
On Error Resume Next
With sdbcControl
    .MoveFirst
    If Text <> "" Then
        For i = 0 To .Rows - 1
            bm = .GetBookmark(i)
            If UCase(Text) = UCase(Mid(.Columns(vCol).CellText(bm), 1, Len(Text))) Then
                .Bookmark = bm
                Exit For
            End If
        Next i
    End If
End With
End Sub


'Combo Cumplimentador
Private Sub sdbcCumplimentador_CloseUp()
    sdbcCumplimentador.Text = sdbcCumplimentador.Columns(1).Text
End Sub

Private Sub sdbcCumplimentador_DropDown()
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    sdbcCumplimentador.RemoveAll
  
    sdbcCumplimentador.AddItem TCumplimentador.proveedor & Chr(m_lSeparador) & m_sCumplProveedor
    sdbcCumplimentador.AddItem TCumplimentador.Peticionario & Chr(m_lSeparador) & m_sCumplPeticionario
    sdbcCumplimentador.AddItem TCumplimentador.AmbosCumplimentadores & Chr(m_lSeparador) & m_sCumplAmbos
    
    sdbcCumplimentador.SelStart = 0
    sdbcCumplimentador.SelLength = Len(sdbcCumplimentador.Text)
    sdbcCumplimentador.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCumplimentador_InitColumnProps()
    sdbcCumplimentador.DataFieldList = "Column 1"
    sdbcCumplimentador.DataFieldToDisplay = "Column 1"
End Sub
Private Sub sdbcCumplimentador_PositionList(ByVal Text As String)
PositionList sdbcCumplimentador, Text
End Sub

'Combo Plazo Cumplimiento
Private Sub sdbcPlazoCumpl_CloseUp()
    sdbcPlazoCumpl.Text = sdbcPlazoCumpl.Columns(1).Text
End Sub

''' <summary>
''' Carga el combo de "Plazo Cumplimentacion"
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcPlazoCumpl_DropDown()
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    sdbcPlazoCumpl.RemoveAll
    
    For i = 0 To 16
        If i = 0 Then
            sdbcPlazoCumpl.AddItem CStr(i) & Chr(m_lSeparador) & m_sIdioma(1)
        ElseIf i = 1 Then
            sdbcPlazoCumpl.AddItem CStr(i) & Chr(m_lSeparador) & "1 " & m_sIdioma(2)
        ElseIf i <= 6 Then
            sdbcPlazoCumpl.AddItem CStr(i) & Chr(m_lSeparador) & i & " " & m_sIdioma(3)
        ElseIf i = 7 Then
            sdbcPlazoCumpl.AddItem CStr(i) & Chr(m_lSeparador) & "1 " & m_sIdioma(4)
        ElseIf i >= 8 And i <= 9 Then
            sdbcPlazoCumpl.AddItem CStr(7 * (i - 6)) & Chr(m_lSeparador) & CStr(i - 6) & " " & m_sIdioma(5)
        ElseIf i = 10 Then
            sdbcPlazoCumpl.AddItem "30" & Chr(m_lSeparador) & "1 " & m_sIdioma(6)
        ElseIf i >= 11 And i <= 15 Then
            sdbcPlazoCumpl.AddItem CStr(30 * (i - 9)) & Chr(m_lSeparador) & CStr(i - 9) & " " & m_sIdioma(7)
        ElseIf i = 16 Then
            sdbcPlazoCumpl.AddItem "365" & Chr(m_lSeparador) & "1 " & m_sIdioma(8)
        End If
    Next i
    
    sdbcPlazoCumpl.SelStart = 0
    sdbcPlazoCumpl.SelLength = Len(sdbcPlazoCumpl.Text)
    sdbcPlazoCumpl.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPlazoCumpl_InitColumnProps()
    sdbcPlazoCumpl.DataFieldList = "Column 1"
    sdbcPlazoCumpl.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcPlazoCumpl_PositionList(ByVal Text As String)
PositionList sdbcPlazoCumpl, Text
End Sub

'Combo Plazo Expiraci�n (todos)
Private Sub sdbcPlazoExpir_CloseUp()
    sdbcPlazoExpir.Text = sdbcPlazoExpir.Columns(1).Text
End Sub

''' <summary>
''' Carga el combo de "Plazo Expiracion"
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcPlazoExpir_DropDown()
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    sdbcPlazoExpir.RemoveAll
    
    sdbcPlazoExpir.AddItem 0 & Chr(m_lSeparador) & m_sIdioma(1)
    sdbcPlazoExpir.AddItem 1 & Chr(m_lSeparador) & "1 " & m_sIdioma(2)
    For i = 2 To 6
        sdbcPlazoExpir.AddItem i & Chr(m_lSeparador) & i & " " & m_sIdioma(3)
    Next
    sdbcPlazoExpir.AddItem 7 & Chr(m_lSeparador) & "1 " & m_sIdioma(4)
    For i = 2 To 3
        sdbcPlazoExpir.AddItem i + 6 & Chr(m_lSeparador) & i & " " & m_sIdioma(5)
    Next
    sdbcPlazoExpir.AddItem 10 & Chr(m_lSeparador) & "1 " & m_sIdioma(6)
    For i = 2 To 6
        sdbcPlazoExpir.AddItem i + 9 & Chr(m_lSeparador) & i & " " & m_sIdioma(7)
    Next
    sdbcPlazoExpir.AddItem 16 & Chr(m_lSeparador) & "1 " & m_sIdioma(8)
    sdbcPlazoExpir.AddItem 17 & Chr(m_lSeparador) & 18 & " " & m_sIdioma(7)
    sdbcPlazoExpir.AddItem 18 & Chr(m_lSeparador) & 24 & " " & m_sIdioma(7)

    
    sdbcPlazoExpir.SelStart = 0
    sdbcPlazoExpir.SelLength = Len(sdbcPlazoExpir.Text)
    sdbcPlazoExpir.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPlazoExpir_InitColumnProps()
    sdbcPlazoExpir.DataFieldList = "Column 1"
    sdbcPlazoExpir.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcPlazoExpir_PositionList(ByVal Text As String)
PositionList sdbcPlazoExpir, Text
End Sub

'Combo Plazo Expiraci�n (reales)
Private Sub sdbcPlazoExpirR_CloseUp()
    sdbcPlazoExpirR.Text = sdbcPlazoExpirR.Columns(1).Text
End Sub

''' <summary>
''' Carga el combo de "Plazo Expiracion Reales"
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcPlazoExpirR_DropDown()
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    sdbcPlazoExpirR.RemoveAll
    
    sdbcPlazoExpirR.AddItem 0 & Chr(m_lSeparador) & m_sIdioma(1)
    sdbcPlazoExpirR.AddItem 1 & Chr(m_lSeparador) & "1 " & m_sIdioma(2)
    For i = 2 To 6
        sdbcPlazoExpirR.AddItem i & Chr(m_lSeparador) & i & " " & m_sIdioma(3)
    Next
    sdbcPlazoExpirR.AddItem 7 & Chr(m_lSeparador) & "1 " & m_sIdioma(4)
    For i = 2 To 3
        sdbcPlazoExpirR.AddItem i + 6 & Chr(m_lSeparador) & i & " " & m_sIdioma(5)
    Next
    sdbcPlazoExpirR.AddItem 10 & Chr(m_lSeparador) & "1 " & m_sIdioma(6)
    For i = 2 To 6
        sdbcPlazoExpirR.AddItem i + 9 & Chr(m_lSeparador) & i & " " & m_sIdioma(7)
    Next
    sdbcPlazoExpirR.AddItem 16 & Chr(m_lSeparador) & "1 " & m_sIdioma(8)
    sdbcPlazoExpirR.AddItem 17 & Chr(m_lSeparador) & 18 & " " & m_sIdioma(7)
    sdbcPlazoExpirR.AddItem 18 & Chr(m_lSeparador) & 24 & " " & m_sIdioma(7)

    
    sdbcPlazoExpirR.SelStart = 0
    sdbcPlazoExpirR.SelLength = Len(sdbcPlazoExpirR.Text)
    sdbcPlazoExpirR.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPlazoExpirR_InitColumnProps()
    sdbcPlazoExpirR.DataFieldList = "Column 1"
    sdbcPlazoExpirR.DataFieldToDisplay = "Column 1"
End Sub
Private Sub sdbcPlazoExpirR_PositionList(ByVal Text As String)
PositionList sdbcPlazoExpirR, Text
End Sub

'Combo Plazo Expiraci�n (reales)
Private Sub sdbcPlazoExpirP_CloseUp()
    sdbcPlazoExpirP.Text = sdbcPlazoExpirP.Columns(1).Text
End Sub

''' <summary>
''' Carga el combo de "Plazo Expiracion Potenciales"
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcPlazoExpirP_DropDown()
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    sdbcPlazoExpirP.RemoveAll
    
    sdbcPlazoExpirP.AddItem 0 & Chr(m_lSeparador) & m_sIdioma(1)
    sdbcPlazoExpirP.AddItem 1 & Chr(m_lSeparador) & "1 " & m_sIdioma(2)
    For i = 2 To 6
        sdbcPlazoExpirP.AddItem i & Chr(m_lSeparador) & i & " " & m_sIdioma(3)
    Next
    sdbcPlazoExpirP.AddItem 7 & Chr(m_lSeparador) & "1 " & m_sIdioma(4)
    For i = 2 To 3
        sdbcPlazoExpirP.AddItem i + 6 & Chr(m_lSeparador) & i & " " & m_sIdioma(5)
    Next
    sdbcPlazoExpirP.AddItem 10 & Chr(m_lSeparador) & "1 " & m_sIdioma(6)
    For i = 2 To 6
        sdbcPlazoExpirP.AddItem i + 9 & Chr(m_lSeparador) & i & " " & m_sIdioma(7)
    Next
    sdbcPlazoExpirP.AddItem 16 & Chr(m_lSeparador) & "1 " & m_sIdioma(8)
    sdbcPlazoExpirP.AddItem 17 & Chr(m_lSeparador) & 18 & " " & m_sIdioma(7)
    sdbcPlazoExpirP.AddItem 18 & Chr(m_lSeparador) & 24 & " " & m_sIdioma(7)

    
    sdbcPlazoExpirP.SelStart = 0
    sdbcPlazoExpirP.SelLength = Len(sdbcPlazoExpirP.Text)
    sdbcPlazoExpirP.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPlazoExpirP_InitColumnProps()
    sdbcPlazoExpirP.DataFieldList = "Column 1"
    sdbcPlazoExpirP.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcPlazoExpirP_PositionList(ByVal Text As String)
PositionList sdbcPlazoExpirP, Text
End Sub

'Potenciales combo grupo
Private Sub sdbcGrupoP_CloseUp()
    sdbcGrupoP.Text = sdbcGrupoP.Columns(1).Text
    sdbcGrupoR.Columns(0).Value = sdbcGrupoP.Columns(0).Value
    sdbcGrupoR.Text = sdbcGrupoP.Columns(1).Text
    sdbcCampoP.Text = ""
    sdbcCampoR.Text = ""
End Sub

Private Sub sdbcGrupoP_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    'Cargar combo
    Codigos = g_oSolicitud.CargarGruposFormCertificado(Idioma)
    sdbcGrupoP.RemoveAll
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGrupoP.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    sdbcGrupoP.SelStart = 0
    sdbcGrupoP.SelLength = Len(sdbcGrupoP.Text)
    sdbcGrupoP.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGrupoP_InitColumnProps()
    sdbcGrupoP.DataFieldList = "Column 1"
    sdbcGrupoP.DataFieldToDisplay = "Column 1"
    sdbcGrupoP.Columns(0).Visible = False
End Sub

Private Sub sdbcGrupoP_PositionList(ByVal Text As String)
PositionList sdbcGrupoP, Text
End Sub

'Potenciales combo campo
Private Sub sdbcCampoP_CloseUp()
    sdbcCampoP.Text = sdbcCampoP.Columns(1).Text
    sdbcCampoR.Columns(0).Value = sdbcCampoP.Columns(0).Value
    sdbcCampoR.Text = sdbcCampoP.Columns(1).Text
End Sub

Private Sub sdbcCampoP_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    Codigos = g_oSolicitud.CargarCamposFechaFormCertificado(Idioma, sdbcGrupoP.Columns(0).Text)
    
    sdbcCampoP.RemoveAll
   
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcCampoP.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    sdbcCampoP.SelStart = 0
    sdbcCampoP.SelLength = Len(sdbcCampoP.Text)
    sdbcCampoP.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCampoP_InitColumnProps()
    sdbcCampoP.DataFieldList = "Column 1"
    sdbcCampoP.DataFieldToDisplay = "Column 1"
    sdbcCampoP.Columns(0).Visible = False
End Sub

Private Sub sdbcCampoP_PositionList(ByVal Text As String)
PositionList sdbcCampoP, Text
End Sub

'Reales combo grupo
Private Sub sdbcGrupoR_CloseUp()
    sdbcGrupoR.Text = sdbcGrupoR.Columns(1).Text
    sdbcGrupoP.Columns(0).Value = sdbcGrupoR.Columns(0).Value
    sdbcGrupoP.Text = sdbcGrupoR.Columns(1).Text
    sdbcCampoR.Text = ""
    sdbcCampoP.Text = ""
End Sub

Private Sub sdbcGrupoR_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    Codigos = g_oSolicitud.CargarGruposFormCertificado(Idioma)
    
    sdbcGrupoR.RemoveAll
   
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGrupoR.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    sdbcGrupoR.SelStart = 0
    sdbcGrupoR.SelLength = Len(sdbcGrupoR.Text)
    sdbcGrupoR.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGrupoR_InitColumnProps()
    sdbcGrupoR.DataFieldList = "Column 1"
    sdbcGrupoR.DataFieldToDisplay = "Column 1"
    sdbcGrupoR.Columns(0).Visible = False
End Sub

Private Sub sdbcGrupoR_PositionList(ByVal Text As String)
PositionList sdbcGrupoR, Text
End Sub

'Reales combo campo
Private Sub sdbcCampoR_CloseUp()
    sdbcCampoR.Text = sdbcCampoR.Columns(1).Text
    sdbcCampoP.Columns(0).Value = sdbcCampoR.Columns(0).Value
    sdbcCampoP.Text = sdbcCampoR.Columns(1).Text
End Sub

Private Sub sdbcCampoR_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    Codigos = g_oSolicitud.CargarCamposFechaFormCertificado(Idioma, sdbcGrupoR.Columns(0).Text)
    
    sdbcCampoR.RemoveAll
   
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcCampoR.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    sdbcCampoR.SelStart = 0
    sdbcCampoR.SelLength = Len(sdbcCampoR.Text)
    sdbcCampoR.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCampoR_InitColumnProps()
    sdbcCampoR.DataFieldList = "Column 1"
    sdbcCampoR.DataFieldToDisplay = "Column 1"
    sdbcCampoR.Columns(0).Visible = False
End Sub

Private Sub sdbcCampoR_PositionList(ByVal Text As String)
PositionList sdbcCampoR, Text
End Sub

'Todos combo grupo
Private Sub sdbcGrupo_CloseUp()
    sdbcGrupo.Text = sdbcGrupo.Columns(1).Text
    sdbcCampo.Text = ""
End Sub

Private Sub sdbcGrupo_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    Codigos = g_oSolicitud.CargarGruposFormCertificado(Idioma)
    
    sdbcGrupo.RemoveAll
   
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGrupo.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    sdbcGrupo.SelStart = 0
    sdbcGrupo.SelLength = Len(sdbcGrupo.Text)
    sdbcGrupo.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGrupo_InitColumnProps()
    sdbcGrupo.DataFieldList = "Column 1"
    sdbcGrupo.DataFieldToDisplay = "Column 1"
    sdbcGrupo.Columns(0).Visible = False
End Sub

Private Sub sdbcGrupo_PositionList(ByVal Text As String)
PositionList sdbcGrupo, Text
End Sub

'Potenciales combo campo
Private Sub sdbcCampo_CloseUp()
    sdbcCampo.Text = sdbcCampo.Columns(1).Text
End Sub

Private Sub sdbcCampo_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    Codigos = g_oSolicitud.CargarCamposFechaFormCertificado(Idioma, sdbcGrupo.Columns(0).Text)
    
    sdbcCampo.RemoveAll
   
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcCampo.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    sdbcCampo.SelStart = 0
    sdbcCampo.SelLength = Len(sdbcCampo.Text)
    sdbcCampo.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCampo_InitColumnProps()
    sdbcCampo.DataFieldList = "Column 1"
    sdbcCampo.DataFieldToDisplay = "Column 1"
    sdbcCampo.Columns(0).Visible = False
End Sub

Private Sub sdbcCampo_PositionList(ByVal Text As String)
PositionList sdbcCampo, Text
End Sub

''' <summary>
''' Graba la configuraci�n
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim vbm As Variant
    Dim i As Integer
    Dim IdCampo As Long
    If chkCertifExpReg.Value Then
        If sdbgRegExp.Rows = 0 Then
            oMensajes.mensajeGenericoOkOnly m_sNoHayExpReg
            Exit Sub
        Else
            m_bGuardando = True
            sdbgRegExp.Update
            m_bGuardando = False
            For i = 0 To sdbgRegExp.Rows - 1
                vbm = sdbgRegExp.RowBookmark(i)
                
                If IsEmpty(sdbgRegExp.Columns("FILA_CORRECTA").CellValue(vbm)) Then
                    oMensajes.mensajeGenericoOkOnly m_sErrorExpReg
                    Exit Sub
                End If
            Next
        End If
    End If


    If sdbcSolicit.Columns(0).Value = 0 Then
        'Modo Solicitud: Manual
        g_oSolicitud.ModoSolicitud = TModoSolicitud.SolicManual
    ElseIf sdbcSolicit.Columns(0).Value = 1 Then
        'Modo Solicitud: Autom�tica
        g_oSolicitud.ModoSolicitud = TModoSolicitud.SolicAutomatica
    End If
    
    'Tipo Proveedor Certif
    Select Case sdbcTipoProveedor.Columns(0).Value
    Case 0 'Todos
        g_oSolicitud.TipoProveedorCertif = TTipoProveedorCertif.SolicTodos
    Case 1 'Potenciales
        g_oSolicitud.TipoProveedorCertif = TTipoProveedorCertif.SolicPotenciales
    Case 2 'Reales
        g_oSolicitud.TipoProveedorCertif = TTipoProveedorCertif.SolicReales
    End Select
    
    'Notificaci�n email: true / false
    g_oSolicitud.Notificacion = chkSolicitMail.Value
    If chkCondOtroCertif.Value = 1 Then
        g_oSolicitud.SolicitudCondiciones = sdbcCertificados.Columns("ID").Value
    Else
        g_oSolicitud.SolicitudCondiciones = 0
    End If
    
    'Envio conjunto de certificados: true / false
    g_oSolicitud.EnvioConjunto = ChkActivarEnvioConjunto.Value
    
    g_oSolicitud.PermitirFueraPlazo = ChkPermitirFueraPlazo.Value
    
    If sdbcCumplimentador.Columns(0).Value = TCumplimentador.proveedor Then
        'Cumplimentador: Proveedor 2
        g_oSolicitud.Cumplimentador = TCumplimentador.proveedor
    ElseIf sdbcCumplimentador.Columns(0).Value = TCumplimentador.Peticionario Then
        'Cumplimentador: Peticionario 1
        g_oSolicitud.Cumplimentador = TCumplimentador.Peticionario
    ElseIf sdbcCumplimentador.Columns(0).Value = TCumplimentador.AmbosCumplimentadores Then
        'cumplimentador : ambos (0)
        g_oSolicitud.Cumplimentador = TCumplimentador.AmbosCumplimentadores
    End If
    
    'Solicitud Opcional / Obligatoria
    g_oSolicitud.Opcional = optOpcional.Value
    
    'Plazo de cumplimentaci�n
    g_oSolicitud.PlazoCumplimentacion = sdbcPlazoCumpl.Columns(0).Value
    
    'Opci�n sin expiraci�n
    If optExpirSin.Value Then
        g_oSolicitud.ModoExpiracion = TModoExpir.SinExpiracion
        g_oSolicitud.PlazoExpiracionPotenciales = -1
        g_oSolicitud.PlazoExpiracion = -1
    End If
    
    'Opci�n expiraci�n todos los proveedores
    If optExpirTodos.Value Then
        If optFecSolic.Value Then
             g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFSolic
        Else
            If optFecCumpl.Value Then
                g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFCumpl
            Else
                g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFExpir
                If sdbcCampo.Columns(0).Value = "" Or sdbcCampo.Columns(0).Value = 0 Then
                    oMensajes.CampoNoSeleccionado
                    Screen.MousePointer = vbNormal
                    cmdAceptar.Enabled = True
                    Exit Sub
                End If
                IdCampo = StrToDbl0(sdbcCampo.Columns(0).Value)
            End If
        End If
        g_oSolicitud.PlazoExpiracionPotenciales = -1
        g_oSolicitud.PlazoExpiracion = sdbcPlazoExpir.Columns(0).Value
    End If
    
    'Opci�n expiraci�n proveedores reales y potenciales
    If optExpirTipo.Value Then
        If optFecSolicR.Value Then
            g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFSolic
        Else
            If optFecCumplR.Value Then
                g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFCumpl
            Else
                g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFExpir
                If IsNumeric(sdbcGrupoR.Columns(0).Value) And IsNumeric(sdbcCampoR.Columns(0).Value) Then
                    IdCampo = sdbcCampoR.Columns(0).Value
                End If
            End If
        End If
        g_oSolicitud.PlazoExpiracionPotenciales = sdbcPlazoExpirP.Columns(0).Value
        g_oSolicitud.PlazoExpiracion = sdbcPlazoExpirR.Columns(0).Value
    End If
    If IdCampo > 0 Then
        g_oSolicitud.IdCampoFechaExpiracion = IdCampo
    End If
    g_oSolicitud.ExpReg = chkCertifExpReg.Value
    If m_bCambiosRealizados_ExpReg And chkCertifExpReg.Value Then
        Dim oExpRegs As CExpRegs
        Set oExpRegs = Raiz.Generar_CExpRegs
        For i = 0 To sdbgRegExp.Rows - 1
            vbm = sdbgRegExp.AddItemBookmark(i)
            If Not IsEmpty(vbm) And Not sdbgRegExp.Columns("CAMPO_PROVE").CellValue(vbm) = "" Then
                oExpRegs.Add g_oSolicitud.Id, sdbgRegExp.Columns("CAMPO_PROVE").CellValue(vbm), sdbgRegExp.Columns("EXPREG_DESCR").CellValue(vbm), _
                sdbgRegExp.Columns("EXPREG").CellValue(vbm), IIf(sdbgRegExp.Columns("OPERADOR").CellValue(vbm) = "", 0, sdbgRegExp.Columns("OPERADOR").CellValue(vbm))
            End If
        Next
        Set g_oSolicitud.ExpRegs = oExpRegs
    Else
        Set g_oSolicitud.ExpRegs = Nothing
    End If
    
    Screen.MousePointer = vbHourglass
    GuardarDatosConfiguracion
    Screen.MousePointer = vbNormal
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
   
    PonerFieldSeparator Me
    CargarRecursos
    
    Screen.MousePointer = vbHourglass
    CargarDatosConfiguracion
    FormatearCombos
    sdbgRegExp.RowHeight = 300
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' carga los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Me.Top = 500
    Me.Left = 3000
    
    On Error Resume Next
      
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_CONFIGCERTIFICADO, Idioma)
    
    If Not Ador Is Nothing Then
        With Ador
            '25088
            Me.Caption = Ador(0).Value & " " & CStr(g_oSolicitud.Denominaciones.Item(Idioma).Den) '1 Configuraci�n del certificado
            .MoveNext
            lblSeccion1.Caption = Ador(0).Value '2 �C�mo solicitar el certificado a los proveedores?
            .MoveNext
            lblSeccion1A.Caption = Ador(0).Value '3 Solicitar a los proveedores de forma:
            .MoveNext
            chkSolicitMail.Caption = Ador(0).Value '4 Notificar la solicitud v�a email
            .MoveNext
            lblSeccion2.Caption = Ador(0).Value '5 �Cu�ndo considerar que el certificado est� cumplimentado?
            .MoveNext
            lblSeccion2A.Caption = Ador(0).Value '6 Cuando lo env�e el:
            .MoveNext
            optObligatoria.Caption = Ador(0).Value '7 Cumplimentaci�n obligatoria
            .MoveNext
            optOpcional.Caption = Ador(0).Value '8 Cumplimentaci�n opcional
            .MoveNext
            lblSeccion2B.Caption = Ador(0).Value '9 Plazo de cumplimentaci�n:
            .MoveNext
            lblSeccion2C.Caption = Ador(0).Value '10 de la fecha de solicitud
            .MoveNext
            lblSeccion3.Caption = Ador(0).Value '11 �C�mo establecer la fecha de expiraci�n del certificado?
            .MoveNext
            optExpirTodos.Caption = Ador(0).Value '12 Configurar expiraci�n para todos los proveedores
            .MoveNext
            optExpirTipo.Caption = Ador(0).Value '13 Configurar expiraci�n por tipo de proveedor
            .MoveNext
            optExpirSin.Caption = Ador(0).Value '14 Sin expiraci�n
            .MoveNext
            m_sTodos = Ador(0).Value
            SSTabExpir.TabCaption(2) = Ador(0).Value '15 Todos los proveedores
            .MoveNext
            m_sPotencial = Ador(0).Value
            SSTabExpir.TabCaption(0) = Ador(0).Value '16 Potenciales
            .MoveNext
            m_sReal = Ador(0).Value
            SSTabExpir.TabCaption(1) = Ador(0).Value '17 Reales
            .MoveNext
            fraExpirP.Caption = Ador(0).Value '18 Fecha de expiraci�n en base a:
            fraExpirR.Caption = Ador(0).Value
            fraExpir.Caption = Ador(0).Value
            .MoveNext
            optFecSolicP.Caption = Ador(0).Value '19 Fecha de solicitud
            optFecSolicR.Caption = Ador(0).Value
            optFecSolic.Caption = Ador(0).Value
            .MoveNext
            .MoveNext
            optFecCumplP.Caption = Ador(0).Value '21 Fecha de cumplimentaci�n
            optFecCumplR.Caption = Ador(0).Value
            optFecCumpl.Caption = Ador(0).Value
            .MoveNext
            optFecFormP.Caption = Ador(0).Value '22 Campo del formulario
            optFecFormR.Caption = Ador(0).Value
            optFecForm.Caption = Ador(0).Value
            .MoveNext
            lblSeccion3BP.Caption = Ador(0).Value '23 Grupo:
            lblSeccion3BR.Caption = Ador(0).Value
            lblSeccion3B.Caption = Ador(0).Value
            .MoveNext
            lblSeccion3CP.Caption = Ador(0).Value '24 Campo:
            lblSeccion3CR.Caption = Ador(0).Value
            lblSeccion3C.Caption = Ador(0).Value
            .MoveNext
            lblSeccion3DP.Caption = Ador(0).Value '25 Plazo de expiraci�n:
            lblSeccion3DR.Caption = Ador(0).Value
            lblSeccion3D.Caption = Ador(0).Value
            .MoveNext
            m_sManual = Ador(0).Value '26 Manual
            .MoveNext
            m_sAutomatica = Ador(0).Value '27 Autom�tica
            .MoveNext
            m_sCumplProveedor = Ador(0).Value '28 Proveedor
            .MoveNext
            m_sCumplPeticionario = Ador(0).Value '29 Peticionario
            .MoveNext
            For i = 1 To 8
                m_sIdioma(i) = Ador(0).Value ' ctes. de Plazo de cumplimentaci�n
                .MoveNext
            Next
            cmdAceptar.Caption = Ador(0).Value '00 &Aceptar
            Ador.MoveNext
            cmdCancelar.Caption = Ador(0).Value '01 &Cancelar
            Ador.MoveNext
            Me.lblTipoProveedor.Caption = Ador(0).Value
            Ador.MoveNext
            m_sCumplAmbos = Ador(0).Value
            
            sdbgRegExp.Columns("OPERADOR").Caption = ""
            .MoveNext
            sdbgRegExp.Columns("CAMPO_PROVE").Caption = Ador(0).Value
            .MoveNext
            sdbgRegExp.Columns("EXPREG_DESCR").Caption = Ador(0).Value
            .MoveNext
            sdbgRegExp.Columns("EXPREG").Caption = Ador(0).Value
            .MoveNext
            sdbgRegExp.Columns("VALOR_VALIDACION").Caption = Ador(0).Value
            .MoveNext
            sdbgRegExp.Columns("OPERADOR").Caption = Ador(0).Value
            
            .MoveNext
            sdbddValoresCampoProve.AddItem "COD" & Chr(m_lSeparador) & Ador(0).Value 'C�digo
            .MoveNext
            sdbddValoresCampoProve.AddItem "CP" & Chr(m_lSeparador) & Ador(0).Value 'C�digo postal
            .MoveNext
            sdbddValoresCampoProve.AddItem "DEN" & Chr(m_lSeparador) & Ador(0).Value 'Denominaci�n
            .MoveNext
            sdbddValoresCampoProve.AddItem "DIR" & Chr(m_lSeparador) & Ador(0).Value 'Direcci�n
            .MoveNext
            sdbddValoresCampoProve.AddItem "PAG" & Chr(m_lSeparador) & Ador(0).Value 'Forma de pago
            .MoveNext
            sdbddValoresCampoProve.AddItem "MON" & Chr(m_lSeparador) & Ador(0).Value 'Moneda
            .MoveNext
            sdbddValoresCampoProve.AddItem "NIF" & Chr(m_lSeparador) & Ador(0).Value 'NIF
            .MoveNext
            sdbddValoresCampoProve.AddItem "PAI" & Chr(m_lSeparador) & Ador(0).Value 'Pa�s
            .MoveNext
            sdbddValoresCampoProve.AddItem "POB" & Chr(m_lSeparador) & Ador(0).Value 'Poblaci�n
            .MoveNext
            sdbddValoresCampoProve.AddItem "PROVI" & Chr(m_lSeparador) & Ador(0).Value 'Provincia
            .MoveNext
            sdbddValoresCampoProve.AddItem "URLPROVE" & Chr(m_lSeparador) & Ador(0).Value 'URL
            .MoveNext
            sdbddValoresCampoProve.AddItem "VIA_PAG" & Chr(m_lSeparador) & Ador(0).Value 'V�a de pago
            sdbddValoresCampoProve.Columns(0).Visible = False
            sdbddValoresCampoProve.DataFieldList = "Column 0"
            sdbddValoresCampoProve.DataFieldToDisplay = "Column 1"
            sdbgRegExp.Columns("CAMPO_PROVE").DropDownHwnd = sdbddValoresCampoProve.hwnd
            
            .MoveNext
            sdbddValoresOperador.AddItem 1 & Chr(m_lSeparador) & Ador(0).Value 'Y
            .MoveNext
            sdbddValoresOperador.AddItem 2 & Chr(m_lSeparador) & Ador(0).Value 'O
            sdbddValoresCampoProve.Columns(0).Visible = False
            sdbddValoresOperador.DataFieldList = "Column 0"
            sdbddValoresOperador.DataFieldToDisplay = "Column 1"
            sdbgRegExp.Columns("OPERADOR").DropDownHwnd = sdbddValoresOperador.hwnd
            .MoveNext
            chkCertifExpReg.Caption = Ador(0).Value
            .MoveNext
            cmdEliminar.Caption = Ador(0).Value
            .MoveNext
            cmdValidar.Caption = Ador(0).Value
            .MoveNext
            m_sNoHayExpReg = Ador(0).Value
            .MoveNext
            m_sErrorExpReg = Ador(0).Value 'Algun patron de validacion introducido no es correcto
            .MoveNext
            chkCondOtroCertif.Caption = Ador(0).Value & ":"   '78
            .MoveNext
            cmdCondiciones.Caption = Ador(0).Value  '79
            .MoveNext
            ChkActivarEnvioConjunto.Caption = Ador(0).Value  '80 Env�o conjunto de certificados
            .MoveNext
            ChkPermitirFueraPlazo.Caption = Ador(0).Value '81 Permitir env�ar fuera de plazo de cumplimentaci�n
            .Close
        End With
    End If
    Set Ador = Nothing
End Sub

''' <summary>
''' Carga de los datos de configuraci�n
''' </summary>
''' <remarks>Llamada desde: frmConfigCertificado.Form_Load
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 26/04/2012</revision>
Private Sub CargarDatosConfiguracion()
    g_oSolicitud.CargarConfiguracionCertificado Idioma
    
    'Inicializo los controles y cargo los combos
    sdbcGrupo.Enabled = False
    sdbcCampo.Enabled = False
    sdbcGrupoR.Enabled = False
    sdbcCampoR.Enabled = False
    sdbcGrupoP.Enabled = False
    sdbcCampoP.Enabled = False
    
    'Modo solicitud
    sdbcSolicit.Columns(0).Value = g_oSolicitud.ModoSolicitud
    If g_oSolicitud.ModoSolicitud = TModoSolicitud.SolicManual Then
        sdbcSolicit.Text = m_sManual
    ElseIf g_oSolicitud.ModoSolicitud = TModoSolicitud.SolicAutomatica Then
        sdbcSolicit.Text = m_sAutomatica
    End If
    
    'Tipo Proveedor
    sdbcTipoProveedor.Columns(0).Value = NullToDbl0(g_oSolicitud.TipoProveedorCertif)
    Select Case sdbcTipoProveedor.Columns(0).Value
    Case 0 'Todos
        sdbcTipoProveedor.Text = m_sTodos
    Case 1 'Potenciales
        sdbcTipoProveedor.Text = m_sPotencial
    Case 2 'Reales
        sdbcTipoProveedor.Text = m_sReal
    End Select
    
    'Notificaci�n solicitud
    If g_oSolicitud.Notificacion Then
        chkSolicitMail.Value = 1
    Else
        chkSolicitMail.Value = 0
    End If
    
    'Envio conjunto de certificados
    If g_oSolicitud.EnvioConjunto Then
        ChkActivarEnvioConjunto.Value = 1
    Else
        ChkActivarEnvioConjunto.Value = 0
    End If
    
    'Permitir env�ar fuera de palzo de cumplimentaci�n
    If g_oSolicitud.PermitirFueraPlazo Then
        ChkPermitirFueraPlazo.Value = 1
    Else
        ChkPermitirFueraPlazo.Value = 0
    End If
    
    'Cumplimentador
    sdbcCumplimentador.Columns(0).Value = g_oSolicitud.Cumplimentador
    If g_oSolicitud.Cumplimentador = TCumplimentador.proveedor Then
        sdbcCumplimentador.Text = m_sCumplProveedor
    ElseIf g_oSolicitud.Cumplimentador = TCumplimentador.Peticionario Then
        sdbcCumplimentador.Text = m_sCumplPeticionario
    ElseIf g_oSolicitud.Cumplimentador = AmbosCumplimentadores Then
        sdbcCumplimentador.Text = m_sCumplAmbos
    End If
    
    'Cumplimentaci�n obligatoria / opcional
    optObligatoria = Not g_oSolicitud.Opcional
    optOpcional = g_oSolicitud.Opcional
    If optOpcional.Value Then
        'Ocultar el plazo de cumplimiento
        lblSeccion2B.Visible = False
        sdbcPlazoCumpl.Visible = False
        lblSeccion2C.Visible = False
    End If
    'Plazo de cumplimentaci�n
    sdbcPlazoCumpl.Columns(0).Value = g_oSolicitud.PlazoCumplimentacion
    Select Case g_oSolicitud.PlazoCumplimentacion
        Case 0
            sdbcPlazoCumpl.Text = m_sIdioma(1)
        Case 1
            sdbcPlazoCumpl.Text = "1 " & m_sIdioma(2)
        Case 2
            sdbcPlazoCumpl.Text = "2 " & m_sIdioma(3)
        Case 3
            sdbcPlazoCumpl.Text = "3 " & m_sIdioma(3)
        Case 4
            sdbcPlazoCumpl.Text = "4 " & m_sIdioma(3)
        Case 5
            sdbcPlazoCumpl.Text = "5 " & m_sIdioma(3)
        Case 6
            sdbcPlazoCumpl.Text = "6 " & m_sIdioma(3)
        Case 7
            sdbcPlazoCumpl.Text = "1 " & m_sIdioma(4)
        Case 14
            sdbcPlazoCumpl.Text = "2 " & m_sIdioma(5)
        Case 21
            sdbcPlazoCumpl.Text = "3 " & m_sIdioma(5)
        Case 30
            sdbcPlazoCumpl.Text = "1 " & m_sIdioma(6)
        Case 60
            sdbcPlazoCumpl.Text = "2 " & m_sIdioma(7)
        Case 90
            sdbcPlazoCumpl.Text = "3 " & m_sIdioma(7)
        Case 120
            sdbcPlazoCumpl.Text = "4 " & m_sIdioma(7)
        Case 150
            sdbcPlazoCumpl.Text = "5 " & m_sIdioma(7)
        Case 180
            sdbcPlazoCumpl.Text = "6 " & m_sIdioma(7)
        Case 365
            sdbcPlazoCumpl.Text = "1 " & m_sIdioma(8)
        Case Else '25244 / -1 Default
            sdbcPlazoCumpl.Text = m_sIdioma(1)
        
    End Select
    
    ' Modo expiraci�n
    If g_oSolicitud.ModoExpiracion = TModoExpir.SinExpiracion Then
        'Sin expiraci�n
        optExpirSin.Value = True
        OpcionSinExpiracion
    Else
        If g_oSolicitud.PlazoExpiracionPotenciales = -1 Then
            'Todos los proveedores
            optExpirTodos.Value = True
            OpcionExpiracionTodos
        Else
            'Proveedores reales y potenciales
            optExpirTipo.Value = True
            OpcionExpiracionTipo
            
        End If
    End If
    
    If g_oSolicitud.ExpReg Then
        Dim iExpReg As CExpReg
        For Each iExpReg In g_oSolicitud.ExpRegs
            sdbgRegExp.AddItem IIf(iExpReg.Operador = 0, True, False) & Chr(m_lSeparador) & iExpReg.Operador & Chr(m_lSeparador) & _
            iExpReg.CampoProve & Chr(m_lSeparador) & iExpReg.ExpRegDescr & Chr(m_lSeparador) & iExpReg.ExpReg & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & True
        Next
        chkCertifExpReg.Value = 1
    Else
        chkCertifExpReg.Value = 0
        
        If NullToDbl0(g_oSolicitud.SolicitudCondiciones) <> 0 Then
            chkCondOtroCertif.Value = 1
            
            Dim oSolicitud As CSolicitud
            Set oSolicitud = Raiz.Generar_CSolicitud
            oSolicitud.Id = g_oSolicitud.SolicitudCondiciones
            oSolicitud.CargarDatosSolicitud
            
            With sdbcCertificados
                .Value = g_oSolicitud.SolicitudCondiciones
                .Text = oSolicitud.Codigo & " - " & oSolicitud.Denominaciones.Item(Idioma).Den
                .Columns("COD").Value = oSolicitud.Codigo
                .Columns("DEN").Value = oSolicitud.Denominaciones.Item(Idioma).Den
                .Columns("COD_DEN").Value = oSolicitud.Codigo & " - " & oSolicitud.Denominaciones.Item(Idioma).Den
                .Columns("ID").Value = oSolicitud.Id
                .Columns("FORMULARIO").Value = oSolicitud.Formulario.Id
            End With
            cmdCondiciones.Visible = True
            
            Set oSolicitud = Nothing
        End If
    End If
End Sub


''' <summary>
''' Guarda los datos de configuraci�n
''' </summary>
''' <remarks>Llamada desde: frmConfigCertificado.cmd_Aceptar
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 26/04/2012</revision>
Private Sub GuardarDatosConfiguracion()
    g_oSolicitud.GuardarConfiguracionCertificado m_bHayCambiosCondiciones
End Sub

Private Sub Form_Unload(Cancel As Integer)
    m_bModificar = True
End Sub

Private Sub optExpirSin_Click()
    OpcionSinExpiracion
End Sub

Private Sub optExpirTipo_Click()
    OpcionExpiracionTipo
End Sub

Private Sub optExpirTodos_Click()
    OpcionExpiracionTodos
End Sub

Private Sub MostrarOcultarTabPotenciales(ByVal Mostrar As Boolean)
    fraExpirP.Visible = Mostrar
    fraPlazoExpirP.Visible = Mostrar
End Sub

Private Sub MostrarOcultarTabReales(ByVal Mostrar As Boolean)
    fraExpirR.Visible = Mostrar
    fraPlazoExpirR.Visible = Mostrar
End Sub

Private Sub MostrarOcultarTabTodos(ByVal Mostrar As Boolean)
    fraExpir.Visible = Mostrar
    fraPlazoExpir.Visible = Mostrar
End Sub

Private Sub OpcionSinExpiracion()
    SSTabExpir.Visible = False
    RedimensionarForm
End Sub

Private Sub OpcionExpiracionTodos()
    Dim Mostrar As Boolean
    SSTabExpir.Visible = True
    
    Mostrar = False
    MostrarOcultarTabPotenciales (Mostrar)
    SSTabExpir.TabVisible(0) = Mostrar
    MostrarOcultarTabReales (Mostrar)
    SSTabExpir.TabVisible(1) = Mostrar
    Mostrar = True
    MostrarOcultarTabTodos (Mostrar)
    SSTabExpir.TabVisible(2) = Mostrar
    
    SSTabExpir.Tab = 2
    
    If g_oSolicitud.PlazoExpiracionPotenciales = -1 Then
        If Not IsNumeric(sdbcPlazoExpir.Columns(0).Value) Then
            'Plazo de expiraci�n
            sdbcPlazoExpir.Columns(0).Value = g_oSolicitud.PlazoExpiracion
            Select Case g_oSolicitud.PlazoExpiracion
                Case 0
                    sdbcPlazoExpir.Text = m_sIdioma(1)
                Case 1
                    sdbcPlazoExpir.Text = "1 " & m_sIdioma(2)
                Case 2
                    sdbcPlazoExpir.Text = "2 " & m_sIdioma(3)
                Case 3
                    sdbcPlazoExpir.Text = "3 " & m_sIdioma(3)
                Case 4
                    sdbcPlazoExpir.Text = "4 " & m_sIdioma(3)
                Case 5
                    sdbcPlazoExpir.Text = "5 " & m_sIdioma(3)
                Case 6
                    sdbcPlazoExpir.Text = "6 " & m_sIdioma(3)
                Case 7
                    sdbcPlazoExpir.Text = "1 " & m_sIdioma(4)
                Case 8
                    sdbcPlazoExpir.Text = "2 " & m_sIdioma(5)
                Case 9
                    sdbcPlazoExpir.Text = "3 " & m_sIdioma(5)
                Case 10
                    sdbcPlazoExpir.Text = "1 " & m_sIdioma(6)
                Case 11
                    sdbcPlazoExpir.Text = "2 " & m_sIdioma(7)
                Case 12
                    sdbcPlazoExpir.Text = "3 " & m_sIdioma(7)
                Case 13
                    sdbcPlazoExpir.Text = "4 " & m_sIdioma(7)
                Case 14
                    sdbcPlazoExpir.Text = "5 " & m_sIdioma(7)
                Case 15
                    sdbcPlazoExpir.Text = "6 " & m_sIdioma(7)
                Case 16
                    sdbcPlazoExpir.Text = "1 " & m_sIdioma(8)
                Case 17
                    sdbcPlazoExpir.Text = "18 " & m_sIdioma(7)
                Case 18
                    sdbcPlazoExpir.Text = "24 " & m_sIdioma(7)
                Case Else
                    sdbcPlazoExpir.Columns(0).Value = 0
                    sdbcPlazoExpir.Text = m_sIdioma(1)
            End Select
        End If
        
        'Fecha expiraci�n en base a...
        If g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFSolic Then
            optFecSolic.Value = True
            optFecSolicR.Value = True
            optFecSolicP.Value = True
        ElseIf g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFCumpl Then
            optFecCumpl.Value = True
            optFecCumplR.Value = True
            optFecCumplP.Value = True
        ElseIf g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFExpir Then
            optFecForm.Value = True
            sdbcGrupo.Enabled = True
            sdbcCampo.Enabled = True
            optFecFormR.Value = True
            sdbcGrupoR.Enabled = True
            sdbcCampoR.Enabled = True
            optFecFormP.Value = True
            sdbcGrupoP.Enabled = True
            sdbcCampoP.Enabled = True
            If Not IsNumeric(sdbcGrupo.Columns(0).Value) Then
                sdbcGrupo.Columns(0).Value = g_oSolicitud.IdGrupoFechaExpiracion
                sdbcGrupo.Text = g_oSolicitud.DenGrupoFechaExpiracion
            End If
            If Not IsNumeric(sdbcCampo.Columns(0).Value) Then
                sdbcCampo.Columns(0).Value = g_oSolicitud.IdCampoFechaExpiracion
                sdbcCampo.Text = g_oSolicitud.DenCampoFechaExpiracion
            End If
            If Not IsNumeric(sdbcGrupoR.Columns(0).Value) Then
                sdbcGrupoR.Columns(0).Value = g_oSolicitud.IdGrupoFechaExpiracion
                sdbcGrupoR.Text = g_oSolicitud.DenGrupoFechaExpiracion
            End If
            If Not IsNumeric(sdbcCampoR.Columns(0).Value) Then
                sdbcCampoR.Columns(0).Value = g_oSolicitud.IdCampoFechaExpiracion
                sdbcCampoR.Text = g_oSolicitud.DenCampoFechaExpiracion
            End If
            If Not IsNumeric(sdbcGrupoP.Columns(0).Value) Then
                sdbcGrupoP.Columns(0).Value = g_oSolicitud.IdGrupoFechaExpiracion
                sdbcGrupoP.Text = g_oSolicitud.DenGrupoFechaExpiracion
            End If
            If Not IsNumeric(sdbcCampoP.Columns(0).Value) Then
                sdbcCampoP.Columns(0).Value = g_oSolicitud.IdCampoFechaExpiracion
                sdbcCampoP.Text = g_oSolicitud.DenCampoFechaExpiracion
            End If
        Else
            g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFSolic
            optFecSolic.Value = True
            optFecSolicR.Value = True
            optFecSolicP.Value = True
        End If
    Else
        If Not IsNumeric(sdbcPlazoExpir.Columns(0).Value) Then
            sdbcPlazoExpir.Columns(0).Value = 0
            sdbcPlazoExpir.Text = m_sIdioma(1)
        End If
    End If

    RedimensionarForm
End Sub


Private Sub OpcionExpiracionTipo()
    Dim Mostrar As Boolean
    SSTabExpir.Visible = True
    
    Mostrar = True
    MostrarOcultarTabPotenciales (Mostrar)
    SSTabExpir.TabVisible(0) = Mostrar
    MostrarOcultarTabReales (Mostrar)
    SSTabExpir.TabVisible(1) = Mostrar
    Mostrar = False
    MostrarOcultarTabTodos (Mostrar)
    SSTabExpir.TabVisible(2) = Mostrar
    
    SSTabExpir.Tab = 0
    
    
    If g_oSolicitud.PlazoExpiracionPotenciales <> -1 Then
        If Not IsNumeric(sdbcPlazoExpirR.Columns(0).Value) Then
            sdbcPlazoExpirR.Columns(0).Value = g_oSolicitud.PlazoExpiracion
            'Plazo de expiraci�n (Reales)
            Select Case g_oSolicitud.PlazoExpiracion
                Case 0
                    sdbcPlazoExpirR.Text = m_sIdioma(1)
                Case 1
                    sdbcPlazoExpirR.Text = "1 " & m_sIdioma(2)
                Case 2
                    sdbcPlazoExpirR.Text = "2 " & m_sIdioma(3)
                Case 3
                    sdbcPlazoExpirR.Text = "3 " & m_sIdioma(3)
                Case 4
                    sdbcPlazoExpirR.Text = "4 " & m_sIdioma(3)
                Case 5
                    sdbcPlazoExpirR.Text = "5 " & m_sIdioma(3)
                Case 6
                    sdbcPlazoExpirR.Text = "6 " & m_sIdioma(3)
                Case 7
                    sdbcPlazoExpirR.Text = "1 " & m_sIdioma(4)
                Case 8
                    sdbcPlazoExpirR.Text = "2 " & m_sIdioma(5)
                Case 9
                    sdbcPlazoExpirR.Text = "3 " & m_sIdioma(5)
                Case 10
                    sdbcPlazoExpirR.Text = "1 " & m_sIdioma(6)
                Case 11
                    sdbcPlazoExpirR.Text = "2 " & m_sIdioma(7)
                Case 12
                    sdbcPlazoExpirR.Text = "3 " & m_sIdioma(7)
                Case 13
                    sdbcPlazoExpirR.Text = "4 " & m_sIdioma(7)
                Case 14
                    sdbcPlazoExpirR.Text = "5 " & m_sIdioma(7)
                Case 15
                    sdbcPlazoExpirR.Text = "6 " & m_sIdioma(7)
                Case 16
                    sdbcPlazoExpirR.Text = "1 " & m_sIdioma(8)
                Case 17
                    sdbcPlazoExpirR.Text = "18 " & m_sIdioma(7)
                Case 18
                    sdbcPlazoExpirR.Text = "24 " & m_sIdioma(7)
                Case Else
                    sdbcPlazoExpirR.Columns(0).Value = 0
                    sdbcPlazoExpirR.Text = m_sIdioma(1)
            End Select
        End If
    
        'Plazo de expiraci�n (Potenciales)
        If Not IsNumeric(sdbcPlazoExpirP.Columns(0).Value) Then
            sdbcPlazoExpirP.Columns(0).Value = g_oSolicitud.PlazoExpiracionPotenciales
            Select Case g_oSolicitud.PlazoExpiracionPotenciales
                Case 0
                    sdbcPlazoExpirP.Text = m_sIdioma(1)
                Case 1
                    sdbcPlazoExpirP.Text = "1 " & m_sIdioma(2)
                Case 2
                    sdbcPlazoExpirP.Text = "2 " & m_sIdioma(3)
                Case 3
                    sdbcPlazoExpirP.Text = "3 " & m_sIdioma(3)
                Case 4
                    sdbcPlazoExpirP.Text = "4 " & m_sIdioma(3)
                Case 5
                    sdbcPlazoExpirP.Text = "5 " & m_sIdioma(3)
                Case 6
                    sdbcPlazoExpirP.Text = "6 " & m_sIdioma(3)
                Case 7
                    sdbcPlazoExpirP.Text = "1 " & m_sIdioma(4)
                Case 8
                    sdbcPlazoExpirP.Text = "2 " & m_sIdioma(5)
                Case 9
                    sdbcPlazoExpirP.Text = "3 " & m_sIdioma(5)
                Case 10
                    sdbcPlazoExpirP.Text = "1 " & m_sIdioma(6)
                Case 11
                    sdbcPlazoExpirP.Text = "2 " & m_sIdioma(7)
                Case 12
                    sdbcPlazoExpirP.Text = "3 " & m_sIdioma(7)
                Case 13
                    sdbcPlazoExpirP.Text = "4 " & m_sIdioma(7)
                Case 14
                    sdbcPlazoExpirP.Text = "5 " & m_sIdioma(7)
                Case 15
                    sdbcPlazoExpirP.Text = "6 " & m_sIdioma(7)
                Case 16
                    sdbcPlazoExpirP.Text = "1 " & m_sIdioma(8)
                Case 17
                    sdbcPlazoExpirP.Text = "18 " & m_sIdioma(7)
                Case 18
                    sdbcPlazoExpirP.Text = "24 " & m_sIdioma(7)
                Case Else
                    sdbcPlazoExpirP.Columns(0).Value = 0
                    sdbcPlazoExpirP.Text = m_sIdioma(1)
            End Select
        End If
    'Fecha expiraci�n en base a... (Potenciales y Reales)
        If g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFSolic Then
            optFecSolicR.Value = True
            optFecSolicP.Value = True
            optFecSolic.Value = True
        ElseIf g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFCumpl Then
            optFecCumplR.Value = True
            optFecCumplP.Value = True
            optFecCumpl.Value = True
        ElseIf g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFExpir Then
            optFecFormR.Value = True
            sdbcGrupoR.Enabled = True
            sdbcCampoR.Enabled = True
            optFecFormP.Value = True
            sdbcGrupoP.Enabled = True
            sdbcCampoP.Enabled = True
            optFecForm.Value = True
            sdbcGrupo.Enabled = True
            sdbcCampo.Enabled = True
            If Not IsNumeric(sdbcGrupoP.Columns(0).Value) Then
                sdbcGrupoP.Columns(0).Value = g_oSolicitud.IdGrupoFechaExpiracion
                sdbcGrupoP.Text = g_oSolicitud.DenGrupoFechaExpiracion
            End If
            If Not IsNumeric(sdbcCampoP.Columns(0).Value) Then
                sdbcCampoP.Columns(0).Value = g_oSolicitud.IdCampoFechaExpiracion
                sdbcCampoP.Text = g_oSolicitud.DenCampoFechaExpiracion
            End If
            If Not IsNumeric(sdbcGrupoR.Columns(0).Value) Then
                sdbcGrupoR.Columns(0).Value = g_oSolicitud.IdGrupoFechaExpiracion
                sdbcGrupoR.Text = g_oSolicitud.DenGrupoFechaExpiracion
            End If
            If Not IsNumeric(sdbcCampoR.Columns(0).Value) Then
                sdbcCampoR.Columns(0).Value = g_oSolicitud.IdCampoFechaExpiracion
                sdbcCampoR.Text = g_oSolicitud.DenCampoFechaExpiracion
            End If
            If Not IsNumeric(sdbcGrupo.Columns(0).Value) Then
                sdbcGrupo.Columns(0).Value = g_oSolicitud.IdGrupoFechaExpiracion
                sdbcGrupo.Text = g_oSolicitud.DenGrupoFechaExpiracion
            End If
            If Not IsNumeric(sdbcCampo.Columns(0).Value) Then
                sdbcCampo.Columns(0).Value = g_oSolicitud.IdCampoFechaExpiracion
                sdbcCampo.Text = g_oSolicitud.DenCampoFechaExpiracion
            End If
        Else
            g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFSolic
            optFecSolic.Value = True
            optFecSolicR.Value = True
            optFecSolicP.Value = True
        End If
    Else
        If Not IsNumeric(sdbcPlazoExpirR.Columns(0).Value) Then
            sdbcPlazoExpirR.Columns(0).Value = 0
            sdbcPlazoExpirR.Text = m_sIdioma(1)
        End If
        If Not IsNumeric(sdbcPlazoExpirP.Columns(0).Value) Then
            sdbcPlazoExpirP.Columns(0).Value = 0
            sdbcPlazoExpirP.Text = m_sIdioma(1)
        End If
            g_oSolicitud.ModoExpiracion = TModoExpir.ExpiracionFSolic
            optFecSolic.Value = True
            optFecSolicR.Value = True
            optFecSolicP.Value = True
    End If
    RedimensionarForm

End Sub

''' <summary>Redimensionar</summary>
''' <param name="SinExpir">Sin Expiracion</param>
''' <remarks>Llamada desde: OpcionSinExpiracion      OpcionExpiracionTodos      OpcionExpiracionTipo; Tiempo m�ximo: 0,2</remarks>

Private Sub RedimensionarForm()
    If chkCertifExpReg.Value = 1 Then
        frRegExp.Visible = True
        With sdbgRegExp
            If .Rows = 0 Then
                .SetFocus
                .Columns("CONTROL_ROW").Value = True
                .Columns("OPERADOR").CellStyleSet "Disabled"
                m_bHacerValidaciones = False
                .Update
                .Col = sdbgRegExp.Columns("CAMPO_PROVE").Position
            End If
        End With
        m_bHacerValidaciones = True
        sdbgRegExp.Refresh
        
        picCondOtroCertif.Top = frRegExp.Top + frRegExp.Height + 50
    Else
        frRegExp.Visible = False
        picCondOtroCertif.Top = frRegExp.Top + 50
    End If
    frInicial.Top = picCondOtroCertif.Top + 300
    If optExpirSin.Value Then
        frInicial.Height = SSTabExpir.Top + 100
    Else
        frInicial.Height = SSTabExpir.Top + SSTabExpir.Height + 100
    End If
    picTarea.Height = frInicial.Top + frInicial.Height
    cmdAceptar.Top = picTarea.Top + picTarea.Height
    cmdCancelar.Top = cmdAceptar.Top
    Me.Height = cmdAceptar.Top + cmdAceptar.Height + 700
End Sub

''' <summary>Ocultar las columnas de codigos en los combos, solo mostrar columnas de datos</summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>

Private Sub FormatearCombos()
    sdbcSolicit.Columns(0).Visible = False
    sdbcTipoProveedor.Columns(0).Visible = False
    sdbcCumplimentador.Columns(0).Visible = False
    sdbcPlazoCumpl.Columns(0).Visible = False
    sdbcPlazoExpir.Columns(0).Visible = False
    sdbcPlazoExpirP.Columns(0).Visible = False
    sdbcPlazoExpirR.Columns(0).Visible = False
    'sdbcValoresCampoProve.Columns(0).Visible = False
End Sub
''' <summary>
''' Tras cerrar el combo muestra el texto correcto
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcTipoProveedor_CloseUp()
    sdbcTipoProveedor.Text = sdbcTipoProveedor.Columns(1).Text
End Sub
''' <summary>
''' Cargar el combo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcTipoProveedor_DropDown()
    Screen.MousePointer = vbHourglass
    
    'Cargar combo
    sdbcTipoProveedor.RemoveAll
    
    sdbcTipoProveedor.AddItem 0 & Chr(m_lSeparador) & m_sTodos 'Todos-> null
    sdbcTipoProveedor.AddItem 1 & Chr(m_lSeparador) & m_sPotencial 'Potencial->1
    sdbcTipoProveedor.AddItem 2 & Chr(m_lSeparador) & m_sReal 'Real->2
    
    sdbcTipoProveedor.SelStart = 0
    sdbcTipoProveedor.SelLength = Len(sdbcTipoProveedor.Text)
    sdbcTipoProveedor.Refresh

    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Inicializar el combo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcTipoProveedor_InitColumnProps()
    sdbcTipoProveedor.DataFieldList = "Column 1"
    sdbcTipoProveedor.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">lo q hayas escrito</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcTipoProveedor_PositionList(ByVal Text As String)
PositionList sdbcTipoProveedor, Text
End Sub

Private Sub sdbgRegExp_AfterDelete(RtnDispErrMsg As Integer)
    sdbgRegExp.MoveFirst
    sdbgRegExp.SetFocus
    sdbgRegExp.Columns("CONTROL_ROW").Value = True
    sdbgRegExp.Columns("OPERADOR").Value = ""
    sdbgRegExp.Columns("OPERADOR").CellStyleSet "Disabled"
    sdbgRegExp.Update
    sdbgRegExp.Col = sdbgRegExp.Columns("CAMPO_PROVE").Position
End Sub

Private Sub sdbgRegExp_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    If sdbgRegExp.Columns("CONTROL_ROW").Value Then
        sdbgRegExp.Columns("OPERADOR").CellStyleSet "Disabled"
    End If
    If ColIndex = 5 And sdbgRegExp.Columns(5).Value = "" Then
        sdbgRegExp.Columns("VALOR_VALIDACION").CellStyleSet "Normal"
    End If
End Sub

Private Sub sdbgRegExp_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgRegExp_BeforeRowColChange(Cancel As Integer)
    If Not (m_bGuardando And sdbgRegExp.Columns("OPERADOR").Value = "") Then
        If m_bHacerValidaciones Then
            Cancel = Not ValidarExpReg(sdbgRegExp.Col)
            If sdbgRegExp.Col = sdbgRegExp.Columns("VALOR_VALIDACION").Position Then
                Dim objRegExp As RegExp
           
                Set objRegExp = New RegExp
                With objRegExp
                    .IgnoreCase = True
                    .Global = True
                End With
                objRegExp.Pattern = sdbgRegExp.Columns("EXPREG").Value
                If sdbgRegExp.Columns("VALOR_VALIDACION").Value = "" Then
                    sdbgRegExp.Columns("VALIDACION").Value = Nothing
                Else
                    m_bHacerValidaciones = False
                    If objRegExp.Test(sdbgRegExp.Columns("VALOR_VALIDACION").Value) Then
                        sdbgRegExp.Columns("VALIDACION").Value = True
                        sdbgRegExp.Columns("VALOR_VALIDACION").CellStyleSet "ExpRegOK"
                        sdbgRegExp.Update
                    Else
                        sdbgRegExp.Columns("VALIDACION").Value = False
                        sdbgRegExp.Columns("VALOR_VALIDACION").CellStyleSet "ExpRegKO"
                        sdbgRegExp.Update
                    End If
                    m_bHacerValidaciones = True
                End If
            End If
        End If
    End If
End Sub

Private Sub sdbgRegExp_BeforeUpdate(Cancel As Integer)
    If Not (m_bGuardando And sdbgRegExp.Columns("OPERADOR").Value = "") Then
        If Not sdbgRegExp.Columns("CONTROL_ROW").Value And sdbgRegExp.Columns("OPERADOR").Value = "" Then
            If m_bHacerValidaciones Then
                oMensajes.NoValido sdbgRegExp.Columns("OPERADOR").Caption
                Cancel = True
            End If
            Exit Sub
        End If
        If sdbgRegExp.Columns("CAMPO_PROVE").Value = "" Then
            If m_bHacerValidaciones Then
                oMensajes.NoValido sdbgRegExp.Columns("CAMPO_PROVE").Caption
                Cancel = True
            End If
            Exit Sub
        End If
        If sdbgRegExp.Columns("EXPREG").Value = "" Then
            If m_bHacerValidaciones Then
                oMensajes.NoValido sdbgRegExp.Columns("EXPREG").Caption
                Cancel = True
            End If
            Exit Sub
        End If
    End If
    sdbgRegExp.Columns("FILA_CORRECTA").Value = 1
    m_bCambiosRealizados_ExpReg = True
End Sub

Private Sub sdbgRegExp_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    sdbgRegExp.Columns("OPERADOR").Locked = False
    If sdbgRegExp.Columns("CONTROL_ROW").Value And sdbgRegExp.Col = sdbgRegExp.Columns("OPERADOR").Position Then
        sdbgRegExp.Columns("OPERADOR").Locked = True
        sdbgRegExp.Col = sdbgRegExp.Columns("CAMPO_PROVE").Position
    End If
    If IsEmpty(sdbgRegExp.Columns("CONTROL_ROW").Value) Then
        sdbgRegExp.Columns("CONTROL_ROW").Value = False
    End If
End Sub

Private Sub sdbgRegExp_RowLoaded(ByVal Bookmark As Variant)
    If sdbgRegExp.Columns("CONTROL_ROW").Value Then
        sdbgRegExp.Columns("OPERADOR").CellStyleSet "Disabled"
    End If

    If sdbgRegExp.Columns("VALOR_VALIDACION").Text = "" Or IsEmpty(sdbgRegExp.Columns("VALIDACION").Value) Then
        sdbgRegExp.Columns("VALOR_VALIDACION").CellStyleSet "Normal"
    ElseIf sdbgRegExp.Columns("VALIDACION").Value Then
        sdbgRegExp.Columns("VALOR_VALIDACION").CellStyleSet "ExpRegOK"
    Else
        sdbgRegExp.Columns("VALOR_VALIDACION").CellStyleSet "ExpRegKO"
    End If
End Sub

Private Function ValidarExpReg(Optional ByVal inum As Integer = 0) As Boolean
    ValidarExpReg = False
    If inum = 0 Or inum = 1 Then
        If (IsEmpty(sdbgRegExp.Columns("CONTROL_ROW").Value) Or Not sdbgRegExp.Columns("CONTROL_ROW").Value) And sdbgRegExp.Columns("OPERADOR").Value = "" Then
            oMensajes.NoValido sdbgRegExp.Columns("OPERADOR").Caption
            Exit Function
        End If
    End If
    If inum = 0 Or inum = 2 Then
        If sdbgRegExp.Columns("CAMPO_PROVE").Value = "" Then
            oMensajes.NoValido sdbgRegExp.Columns("CAMPO_PROVE").Caption
            Exit Function
        End If
    End If
    If inum = 0 Or inum = 4 Then
        If sdbgRegExp.Columns("EXPREG").Value = "" Then
            oMensajes.NoValido sdbgRegExp.Columns("EXPREG").Caption
            Exit Function
        End If
    End If
    ValidarExpReg = True
End Function
