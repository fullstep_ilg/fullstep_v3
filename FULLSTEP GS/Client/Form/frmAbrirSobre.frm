VERSION 5.00
Object = "{57A1F96E-5A81-4063-8193-6E7BB254EDBD}#1.0#0"; "DXAnimatedGIF.ocx"
Begin VB.Form frmAbrirSobre 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FULLSTEP"
   ClientHeight    =   2820
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8415
   Icon            =   "frmAbrirSobre.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2820
   ScaleWidth      =   8415
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrCrono 
      Left            =   600
      Top             =   690
   End
   Begin DXAnimatedGIF.DXGif DXGif1 
      Height          =   1050
      Left            =   450
      TabIndex        =   6
      Top             =   420
      Width           =   930
      _ExtentX        =   1640
      _ExtentY        =   1852
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   4290
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   2370
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   3090
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2370
      Width           =   1005
   End
   Begin VB.Label lblFechaApertura 
      Caption         =   "Fecha de apertura: 30/01/2003 15:20:58"
      Height          =   270
      Left            =   5295
      TabIndex        =   3
      Top             =   135
      Width           =   3045
   End
   Begin VB.Label lblSobre 
      Alignment       =   2  'Center
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   2835
      TabIndex        =   2
      Top             =   195
      Width           =   2715
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      Caption         =   "Apertura de sobre"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2805
      TabIndex        =   1
      Top             =   975
      Width           =   2775
   End
   Begin VB.Label lblAdvertencia 
      Caption         =   "Atenci�n! Una vez realice la apertura del sobre ya no podr� volver a cerrarlo."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   450
      Left            =   165
      TabIndex        =   0
      Top             =   1905
      Width           =   8220
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   16000
      Y1              =   1785
      Y2              =   1785
   End
End
Attribute VB_Name = "frmAbrirSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Raiz As cRaiz
Public AceptarClicked As Boolean
Public Sobre As Integer

Private sIdiFechaApertura As String
Private sIdiSobre As String
Private m_dtHoraServidor As Date
Private m_dtHoraLocalInicial As Date

Private Sub cmdAceptar_Click()
    AceptarClicked = True
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    AceptarClicked = False
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.DXGif1.FileName = App.Path & "\email.gif"
    
    m_dtHoraServidor = Raiz.DevolverFechaServidor
    m_dtHoraLocalInicial = Now
    
    CargarRecursos
    Me.tmrCrono.Interval = 1000
    Me.lblSobre = sIdiSobre & Sobre
End Sub

Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset
    
    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_ABRIRSOBRE, Idioma)
    
    If Not Ador Is Nothing Then
        Me.lblTitulo.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiFechaApertura = Ador(0).Value
        Ador.MoveNext
        Me.lblAdvertencia.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiSobre = Ador(0).Value
        Ador.Close
        Set Ador = Nothing
    End If
End Sub

Private Sub tmrCrono_Timer()
    Dim dif As Variant
    
    dif = DateDiff("s", m_dtHoraLocalInicial, Now)
    Me.lblFechaApertura = sIdiFechaApertura & ": " & DateAdd("s", dif, m_dtHoraServidor)
End Sub
