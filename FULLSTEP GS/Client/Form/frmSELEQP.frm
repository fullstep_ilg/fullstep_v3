VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSELEQP 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DSelección de equipo"
   ClientHeight    =   1290
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4635
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELEQP.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1290
   ScaleWidth      =   4635
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   960
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   4
      Top             =   840
      Width           =   3825
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   3
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   2
         Top             =   60
         Width           =   1050
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcEqp 
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Top             =   300
      Width           =   3000
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6800
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5292
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblEqp 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominación:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   1380
   End
End
Attribute VB_Name = "frmSELEQP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public GestorIdiomas As CGestorIdiomas
Public Raiz As CRaiz
Public Idioma As String
Public sEqp As String
Public bModif As Boolean

Private oEquipos As CEquipos
Private oEquipo As CEquipo

Private Sub cmdAceptar_Click()
    Me.Hide 'Lo descargo en el origen
End Sub

Private Sub cmdCancelar_Click()
    sEqp = ""
    Me.Hide
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass
    
    CargarRecursos
    ConfigurarSeguridad
    CargarEquipos
    PreseleccionarEquipo
    Screen.MousePointer = vbNormal
End Sub

Private Sub ConfigurarSeguridad()
    If Not bModif Then
        sdbcEqp.Enabled = False
        picEdit.Visible = False
        Me.Height = Me.Height - picEdit.Visible
    End If
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_SELEQP, Idioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value '1
        Ador.MoveNext
        lblEqp.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
                
        Ador.Close
    End If
End Sub

Private Sub CargarEquipos()
    Screen.MousePointer = vbHourglass
    
    Set oEquipos = Raiz.Generar_CEquipos
    oEquipos.CargarTodosLosEquipos , , , True
        
    sdbcEqp.RemoveAll
    For Each oEquipo In oEquipos
        sdbcEqp.AddItem oEquipo.Cod & Chr(9) & oEquipo.Den
    Next
    Set oEquipo = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub PreseleccionarEquipo()
    If sEqp <> "" Then
        sdbcEqp.Value = sEqp
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oEquipos = Nothing
    Set oEquipo = Nothing
End Sub

Private Sub sdbcEqp_CloseUp()
    sEqp = sdbcEqp.Value
End Sub

Private Sub sdbcEqp_DropDown()
    CargarEquipos
End Sub

Private Sub sdbcEqp_InitColumnProps()
    sdbcEqp.DataFieldList = "Column 0"
    sdbcEqp.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcEqp_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqp.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqp.Rows - 1
            bm = sdbcEqp.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqp.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcEqp.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
