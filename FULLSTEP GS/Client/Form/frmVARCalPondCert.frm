VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmVARCalPondCert 
   Caption         =   "DVariables de calidad"
   ClientHeight    =   6090
   ClientLeft      =   180
   ClientTop       =   2580
   ClientWidth     =   10275
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVARCalPondCert.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6090
   ScaleWidth      =   10275
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picConfig 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   4425
      Left            =   0
      ScaleHeight     =   4425
      ScaleWidth      =   10275
      TabIndex        =   9
      Top             =   585
      Width           =   10275
      Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
         Height          =   3570
         Left            =   165
         TabIndex        =   1
         Top             =   360
         Width           =   4440
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   5
         stylesets.count =   2
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmVARCalPondCert.frx":014A
         stylesets(0).AlignmentText=   0
         stylesets(0).AlignmentPicture=   1
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmVARCalPondCert.frx":01B1
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   820
         Columns(1).Caption=   "Id."
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   5
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   4524
         Columns(2).Caption=   "Campo"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1535
         Columns(3).Caption=   "Ayuda"
         Columns(3).Name =   "AYU"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   4
         Columns(3).ButtonsAlways=   -1  'True
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "PONDTIPO"
         Columns(4).Name =   "PONDTIPO"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         _ExtentX        =   7832
         _ExtentY        =   6297
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox Picture1 
         Height          =   4095
         Left            =   4800
         ScaleHeight     =   4035
         ScaleWidth      =   5355
         TabIndex        =   10
         Top             =   0
         Width           =   5415
         Begin VB.PictureBox picNumerico 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   4050
            Left            =   0
            ScaleHeight     =   4050
            ScaleWidth      =   5340
            TabIndex        =   11
            Top             =   0
            Width           =   5340
            Begin VB.Frame fraPond 
               BackColor       =   &H00808000&
               Height          =   2280
               Index           =   3
               Left            =   180
               TabIndex        =   35
               Tag             =   "Discreta"
               Top             =   1560
               Visible         =   0   'False
               Width           =   4995
               Begin SSDataWidgets_B.SSDBGrid sdbgDiscreta 
                  Height          =   1695
                  Left            =   225
                  TabIndex        =   36
                  Top             =   300
                  Width           =   4605
                  ScrollBars      =   2
                  _Version        =   196617
                  DataMode        =   2
                  Row.Count       =   3
                  Col.Count       =   3
                  Row(0).Col(0)   =   "1/1/2002"
                  Row(0).Col(1)   =   "1"
                  Row(1).Col(0)   =   "1/1/2003"
                  Row(1).Col(1)   =   "2"
                  Row(2).Col(0)   =   "1/1/2004"
                  Row(2).Col(1)   =   "3"
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   1
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   3942
                  Columns(0).Caption=   "Valor"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).Locked=   -1  'True
                  Columns(1).Width=   3200
                  Columns(1).Caption=   " => Puntos"
                  Columns(1).Name =   "PUNTOS"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "INDI"
                  Columns(2).Name =   "INDI"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   8123
                  _ExtentY        =   2990
                  _StockProps     =   79
                  ForeColor       =   0
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
            End
            Begin VB.Frame fraPond 
               BackColor       =   &H00808000&
               Height          =   2220
               Index           =   4
               Left            =   180
               TabIndex        =   33
               Tag             =   "Continua"
               Top             =   1605
               Visible         =   0   'False
               Width           =   4965
               Begin SSDataWidgets_B.SSDBGrid sdbgContinua 
                  Height          =   1830
                  Left            =   180
                  TabIndex        =   34
                  Top             =   225
                  Width           =   4635
                  ScrollBars      =   2
                  _Version        =   196617
                  DataMode        =   2
                  Row.Count       =   3
                  Col.Count       =   16
                  Row(0).Col(0)   =   "0"
                  Row(0).Col(1)   =   "10"
                  Row(0).Col(2)   =   "1"
                  Row(1).Col(0)   =   "11"
                  Row(1).Col(1)   =   "20"
                  Row(1).Col(2)   =   "2"
                  Row(2).Col(0)   =   "20"
                  Row(2).Col(1)   =   "999999"
                  Row(2).Col(2)   =   "10"
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmVARCalPondCert.frx":01CD
                  AllowAddNew     =   -1  'True
                  AllowDelete     =   -1  'True
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   1
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  ExtraHeight     =   26
                  Columns.Count   =   4
                  Columns(0).Width=   2752
                  Columns(0).Caption=   "Desde"
                  Columns(0).Name =   "DESDE"
                  Columns(0).AllowSizing=   0   'False
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2143
                  Columns(1).Caption=   "Hasta"
                  Columns(1).Name =   "HASTA"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   2196
                  Columns(2).Caption=   "=> Puntos"
                  Columns(2).Name =   "PUNTOS"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   3200
                  Columns(3).Visible=   0   'False
                  Columns(3).Caption=   "INDI"
                  Columns(3).Name =   "INDI"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  _ExtentX        =   8176
                  _ExtentY        =   3228
                  _StockProps     =   79
                  ForeColor       =   0
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
            End
            Begin VB.Frame fraPond 
               BackColor       =   &H00808000&
               Height          =   1875
               Index           =   5
               Left            =   180
               TabIndex        =   26
               Tag             =   "SiNO"
               Top             =   1665
               Visible         =   0   'False
               Width           =   4980
               Begin VB.TextBox txtPuntuacion1 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Left            =   1860
                  TabIndex        =   28
                  Top             =   600
                  Width           =   855
               End
               Begin VB.TextBox txtPuntuacion2 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Left            =   1860
                  TabIndex        =   27
                  Top             =   1020
                  Width           =   855
               End
               Begin VB.Label lblSi 
                  BackColor       =   &H00808000&
                  Caption         =   "DValor = S�"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   255
                  Left            =   285
                  TabIndex        =   32
                  Top             =   660
                  Width           =   1455
               End
               Begin VB.Label lblPuntos1 
                  BackColor       =   &H00808000&
                  Caption         =   "DPuntos"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   255
                  Left            =   2820
                  TabIndex        =   31
                  Top             =   660
                  Width           =   840
               End
               Begin VB.Label lblNo 
                  BackColor       =   &H00808000&
                  Caption         =   "DValor = No"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   255
                  Left            =   285
                  TabIndex        =   30
                  Top             =   1080
                  Width           =   1530
               End
               Begin VB.Label lblPuntos2 
                  BackColor       =   &H00808000&
                  Caption         =   "DPuntos"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   255
                  Left            =   2820
                  TabIndex        =   29
                  Top             =   1080
                  Width           =   885
               End
            End
            Begin VB.Frame fraPond 
               BackColor       =   &H00808000&
               Height          =   975
               Index           =   1
               Left            =   180
               TabIndex        =   24
               Tag             =   "Manual"
               Top             =   1800
               Visible         =   0   'False
               Width           =   4935
               Begin VB.Label lblManual 
                  BackColor       =   &H00808000&
                  Caption         =   "DLos valores de ponderaci�n deber�n introducirse manualmente por el usuario"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   435
                  Left            =   300
                  TabIndex        =   25
                  Top             =   360
                  Width           =   4275
               End
            End
            Begin VB.Frame fraPond 
               BackColor       =   &H00808000&
               Height          =   1350
               Index           =   2
               Left            =   180
               TabIndex        =   20
               Tag             =   "Formula"
               Top             =   1710
               Visible         =   0   'False
               Width           =   4980
               Begin VB.CommandButton cmdAyuda 
                  BeginProperty Font 
                     Name            =   "Tahoma"
                     Size            =   7.5
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   1
                  Left            =   4455
                  Picture         =   "frmVARCalPondCert.frx":01E9
                  Style           =   1  'Graphical
                  TabIndex        =   22
                  Top             =   690
                  Width           =   350
               End
               Begin VB.TextBox txtFormula 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Left            =   180
                  TabIndex        =   21
                  Top             =   675
                  Width           =   4230
               End
               Begin VB.Label lblFormula 
                  BackColor       =   &H00808000&
                  Caption         =   "Redacte la f�rmula considerando el campo como X"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   285
                  Left            =   225
                  TabIndex        =   23
                  Top             =   315
                  Width           =   4560
               End
            End
            Begin VB.Frame fraPond 
               BackColor       =   &H00808000&
               Height          =   1035
               Index           =   0
               Left            =   220
               TabIndex        =   18
               Tag             =   "Sin pond"
               Top             =   1800
               Visible         =   0   'False
               Width           =   4935
               Begin VB.Label lblSinPond 
                  BackColor       =   &H00808000&
                  Caption         =   "DAtributo sin ponderaci�n"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   195
                  Left            =   585
                  TabIndex        =   19
                  Top             =   450
                  Width           =   3795
               End
            End
            Begin VB.OptionButton optPond 
               BackColor       =   &H00808000&
               Caption         =   "DSin ponderaci�n"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   0
               Left            =   240
               TabIndex        =   16
               Top             =   795
               Value           =   -1  'True
               Width           =   1665
            End
            Begin VB.OptionButton optPond 
               BackColor       =   &H00808000&
               Caption         =   "DManual"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   4
               Left            =   4140
               TabIndex        =   15
               Top             =   1155
               Width           =   1665
            End
            Begin VB.OptionButton optPond 
               BackColor       =   &H00808000&
               Caption         =   "DF�rmula"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               HelpContextID   =   3
               Index           =   3
               Left            =   2265
               TabIndex        =   14
               Top             =   1155
               Width           =   1665
            End
            Begin VB.OptionButton optPond 
               BackColor       =   &H00808000&
               Caption         =   "DEscala discreta"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   1
               Left            =   2265
               TabIndex        =   13
               Top             =   795
               Width           =   1665
            End
            Begin VB.OptionButton optPond 
               BackColor       =   &H00808000&
               Caption         =   "DEscala continua"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   2
               Left            =   240
               TabIndex        =   12
               Top             =   1155
               Width           =   1665
            End
            Begin VB.Label Label1 
               Alignment       =   2  'Center
               BackColor       =   &H00808000&
               Caption         =   "Label1"
               ForeColor       =   &H00FFFFFF&
               Height          =   405
               Left            =   45
               TabIndex        =   17
               Top             =   135
               Width           =   5190
            End
            Begin VB.Line Line1 
               BorderColor     =   &H00FFFFFF&
               X1              =   0
               X2              =   5310
               Y1              =   570
               Y2              =   570
            End
         End
      End
      Begin MSComctlLib.TabStrip ssTabVar 
         Height          =   4020
         Left            =   120
         TabIndex        =   37
         Top             =   0
         Width           =   4560
         _ExtentX        =   8043
         _ExtentY        =   7091
         _Version        =   393216
         BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
            NumTabs         =   1
            BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               ImageVarType    =   2
            EndProperty
         EndProperty
      End
      Begin VB.Label lblCambios 
         AutoSize        =   -1  'True
         Caption         =   "Para guardar los cambios efectuados cierre esta ventana y pulse Guadar cambios en la ventada principal de Variables de calidad."
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   120
         TabIndex        =   38
         Top             =   4200
         Width           =   9255
      End
   End
   Begin VB.PictureBox picSolicitud 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   585
      Left            =   0
      ScaleHeight     =   585
      ScaleWidth      =   10275
      TabIndex        =   5
      Top             =   0
      Width           =   10275
      Begin VB.Label lblTipo 
         Caption         =   "Subvariable de tipo: "
         Height          =   240
         Left            =   135
         TabIndex        =   8
         Top             =   120
         Width           =   3300
      End
      Begin VB.Line Line2 
         X1              =   0
         X2              =   10620
         Y1              =   435
         Y2              =   435
      End
      Begin VB.Label lblOrigen 
         Caption         =   "Origen de datos: "
         Height          =   240
         Left            =   3510
         TabIndex        =   7
         Top             =   120
         Width           =   3255
      End
      Begin VB.Label lblForm 
         Caption         =   "Formulario asociado:"
         Height          =   240
         Left            =   6840
         TabIndex        =   6
         Top             =   120
         Width           =   3255
      End
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   465
      Left            =   0
      ScaleHeight     =   465
      ScaleWidth      =   10275
      TabIndex        =   0
      Top             =   5625
      Width           =   10275
      Begin VB.CommandButton cmdAyuda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   7890
         Picture         =   "frmVARCalPondCert.frx":041A
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   90
         Width           =   350
      End
      Begin VB.TextBox txtFormulaVar 
         Height          =   300
         Left            =   1845
         TabIndex        =   2
         Top             =   90
         Width           =   6000
      End
      Begin VB.Label lblFormulaVar 
         Caption         =   "F�rmula de la variable:"
         Height          =   195
         Left            =   135
         TabIndex        =   4
         Top             =   135
         Width           =   1725
      End
   End
End
Attribute VB_Name = "frmVARCalPondCert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables publicas
Public g_oVariableCalidad As CVariableCalidad
Public g_oVarCalidadOriginal As CVariableCalidad
Public GestorIdiomas As CGestorIdiomas
Public GestorSeguridad As CGestorSeguridad
Public GestorParametros As CGestorParametros
Public FrmVARCalidad As Object
Public FrmVARCalCompuestaNivel5 As Object
Public FrmVARCalCompuestaNivel4 As Object
Public FrmVARCalCompuestaNivel3 As Object
Public Raiz As CRaiz
Public Mensajes As CMensajes
Public g_lFormulario As Long
Public Idioma As String
Public UsuCod As String

Private m_oPonderaciones As CVarPonderaciones
Private m_oGrupos As CFormGrupos
Private m_oCampoSeleccionado As CVarPonderacion

'Variables de gesti�n
Private m_bErrorC  As Boolean
Private m_bErrorD As Boolean
Private m_bRespetarCarga As Boolean

'Variables de idiomas
Private m_sSinPond As String
Private m_sContinua As String
Private m_sDiscreta As String
Private m_sCaducidad As String
Private m_sFormula As String
Private m_sManual As String
Private m_sAutomatico As String
Private m_sMensaje(0 To 8) As String
Private m_sLabel(0 To 2) As String
Private m_sTiposVar(0 To 1) As String
Private m_sSiNo(0 To 5) As String
Private m_sIdiErrorFormula(12) As String
Private m_sCaption As String
Private m_dFecExpiracion As Dictionary
Private m_dFecCumplimentacion As Dictionary
Private m_sDentroPlazo As String
Private m_sFueraPlazo As String
Private m_oIdiomas As CIdiomas

Private m_ParamGen As ParametrosGenerales
Public Property Get ParamGen() As ParametrosGenerales
    ParamGen = m_ParamGen
End Property
Public Property Let ParamGen(ByRef vParametrosGenerales As ParametrosGenerales)
    m_ParamGen = vParametrosGenerales
End Property

Private Sub cmdAyuda_Click(Index As Integer)
    'Para forzar un BeforeRowColChange
    If sdbgCampos.Row = 0 Then
        sdbgCampos.MoveNext
    Else
        sdbgCampos.MoveFirst
    End If
    
    MostrarFormSOLAyudaCalculosLocal GestorIdiomas, Idioma
End Sub

Private Sub Form_Load()
    CargarRecursos
    PonerFieldSeparator Me
    PuntuacionCargar
    
    lblCambios.Visible = False
    Me.Caption = m_sCaption & " " & g_oVariableCalidad.Denominaciones.Item(Idioma).Den
    picNumerico.Enabled = Not g_oVariableCalidad.BajaLog
End Sub

Private Sub CargarGruposEnTab()
    Dim iTab As Integer
    Dim oGrupo As CFormGrupo

    ssTabVar.Tabs.Clear
    iTab = 1
    For Each oGrupo In m_oGrupos
        ssTabVar.Tabs.Add iTab, "A" & oGrupo.Id, oGrupo.Denominaciones.Item(CStr(Idioma)).Den  'si no es multiidioma ser� la misma para todos
        ssTabVar.Tabs(iTab).Tag = CStr(oGrupo.Id)
        iTab = iTab + 1
    Next
End Sub

'Descripci�n: Carga en g_oVariableCalidad toda la informaci�n q se va a mostrar por pantalla
'Param. Entrada: -
'Param. Salida: -
'Llamada: Form_Load
    
Private Sub PuntuacionCargar()
    lblOrigen.Caption = m_sLabel(1) & ": " & g_oVariableCalidad.OrigenCod
    lblTipo.Caption = m_sLabel(0) & " " & m_sTiposVar(0)
    
    Set g_oVariableCalidad.CamposPond = Nothing
    g_oVariableCalidad.CargarPonderacionCertificado m_dFecExpiracion, m_dFecCumplimentacion
    
    g_oVariableCalidad.CargarNombreFormulario
    lblForm.Caption = m_sLabel(2) & " " & g_oVariableCalidad.FormularioCOD
    
    DatosCertificado
End Sub

Private Sub DatosCertificado()
    Dim oPond As CVarPonderacion
    Dim lGrupo As Long
    Dim i As Integer
    
    Set m_oPonderaciones = g_oVariableCalidad.CamposPond
    Set m_oGrupos = Raiz.Generar_CFormGrupos
    lGrupo = 0
    i = 0
    sdbgCampos.RemoveAll
    
    m_bRespetarCarga = True
    
    txtFormulaVar.Text = NullToStr(g_oVariableCalidad.Formula)
    
    For Each oPond In m_oPonderaciones
        With oPond
            If Not .Grupo Is Nothing Then
                If lGrupo <> .Grupo.Id Then
                    If m_oGrupos.Item(CStr(.Grupo.Id)) Is Nothing Then
                        m_oGrupos.Add .Grupo.Id, .Grupo.Denominaciones, , , , , .Grupo.Orden
                    End If
                    lGrupo = .Grupo.Id
                    i = i + 1
                End If
            End If
            If i = 0 Or i = 1 Then
                sdbgCampos.AddItem .Campo.Id & Chr(m_lSeparador) & .PondCod & Chr(m_lSeparador) & .Campo.Denominaciones.Item(Idioma).Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & .PondTipo
            End If
        End With
        CargarGruposEnTab
     Next
     If sdbgCampos.Rows > 0 Then
        sdbgCampos.Row = 0
        CampoSeleccionado
     End If
     m_bRespetarCarga = False
End Sub

''' <summary>
''' Carga el frame derecho (Tipos de ponderacion) segun el tipo del campo seleccionado
''' Actualmente la ponderacion de manual se ocultara SIEMPRE
''' </summary>
''' <remarks>Llamada desde=DatosCertificado() // ; Tiempo m�ximo:0,35seg.</remarks>
Private Sub CampoSeleccionado()
    Dim oValorLista As CValorPond
    Dim oValor As CCampoValorLista
    Dim i As Integer
    
    Set m_oCampoSeleccionado = Nothing
    Set m_oCampoSeleccionado = m_oPonderaciones.Item(CStr(sdbgCampos.Columns("ID").Value))
    Label1.Caption = sdbgCampos.Columns("DEN").Value
    
    For i = 0 To 5
        fraPond(i).Visible = False
    Next
    For i = 0 To 4
        optPond(i).Visible = False
        optPond(i).Enabled = True
    Next
    
    With m_oCampoSeleccionado
        Select Case .Campo.Tipo
        Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
            optPond(0).Caption = m_sSinPond
            optPond(1).Caption = m_sDiscreta
            optPond(0).Visible = True
            optPond(1).Visible = True
                    
            If .Campo.TipoIntroduccion = IntroLibre Then
                optPond(1).Enabled = False
            End If
            
            Select Case .PondTipo
                Case EscalaDiscreta
                    optPond(1).Value = True
                    fraPond(3).Visible = True
                    sdbgDiscreta.RemoveAll
                    If Not .PondLista Is Nothing Then
                        For Each oValorLista In .PondLista
                            sdbgDiscreta.AddItem oValorLista.ValorLista & Chr(m_lSeparador) & oValorLista.ValorPond & Chr(m_lSeparador) & oValorLista.indice
                        Next
                    Else
                        For Each oValor In .Campo.ValoresLista
                            sdbgDiscreta.AddItem oValor.valorNum & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oValor.indice + 1
                        Next
                    End If
                Case Else
                    optPond(0).Value = True
                    fraPond(0).Visible = True
                
            End Select
        Case TipoNumerico
            optPond(0).Caption = m_sSinPond
            optPond(1).Caption = m_sDiscreta
            optPond(2).Caption = m_sContinua
            optPond(3).Caption = m_sFormula
            'optPond(4).caption = m_sManual
            For i = 0 To 4
                optPond(i).Visible = True
            Next
            optPond(4).Visible = False
            If .Campo.TipoIntroduccion = IntroLibre Then
                optPond(1).Enabled = False
            End If
            Select Case .PondTipo
            Case EscalaDiscreta
                optPond(1).Value = True
                fraPond(3).Visible = True
                sdbgDiscreta.RemoveAll
                If Not .PondLista Is Nothing Then
                    For Each oValorLista In .PondLista
                        sdbgDiscreta.AddItem oValorLista.ValorLista & Chr(m_lSeparador) & oValorLista.ValorPond & Chr(m_lSeparador) & oValorLista.indice
                    Next
                Else
                    For Each oValor In .Campo.ValoresLista
                        sdbgDiscreta.AddItem oValor.valorNum & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oValor.indice + 1
                    Next
                End If
                
            Case EscalaContinua
                optPond(2).Value = True
                fraPond(4).Visible = True
                sdbgContinua.RemoveAll
                For Each oValorLista In .PondLista
                    sdbgContinua.AddItem oValorLista.ValorDesde & Chr(m_lSeparador) & oValorLista.ValorHasta & Chr(m_lSeparador) & oValorLista.ValorPond & Chr(m_lSeparador) & oValorLista.indice
                Next
    
            Case Formula
                optPond(3).Value = True
                fraPond(2).Visible = True
                txtFormula.Text = NullToStr(.PondFormula)
            
            Case Manual
                optPond(4).Value = True
                fraPond(1).Visible = True
                
            Case Else
                optPond(0).Value = True
                fraPond(0).Visible = True
                
            End Select
        Case TipoFecha
            If m_oCampoSeleccionado.Campo.Id = VarCalPCertCampoGeneral.FecCumplimentacion Then
                optPond(0).Caption = m_sSinPond
                optPond(1).Caption = m_sCaducidad
            
                optPond(0).Visible = True
                optPond(1).Visible = True
                optPond(2).Visible = False
                     
                lblSi.Caption = m_sDentroPlazo
                lblNo.Caption = m_sFueraPlazo
                        
                Select Case .PondTipo
                    Case Automatico
                        optPond(1).Value = True
                        fraPond(5).Visible = True
                        txtPuntuacion1.Text = NullToStr(.PondSi)
                        txtPuntuacion2.Text = NullToStr(.PondNo)
                    Case Else
                        optPond(0).Value = True
                        fraPond(0).Visible = True
                End Select
            Else
                optPond(0).Caption = m_sSinPond
                optPond(1).Caption = m_sDiscreta
                optPond(2).Caption = m_sContinua
                optPond(3).Caption = m_sCaducidad
                'optPond(4).caption = m_sManual
                For i = 0 To 4
                    optPond(i).Visible = True
                Next
                optPond(3).Visible = True
                optPond(4).Visible = False
                If .Campo.TipoIntroduccion = IntroLibre Then
                    optPond(1).Enabled = False
                End If
                
                Select Case .PondTipo
                    Case EscalaDiscreta
                        optPond(1).Value = True
                        fraPond(3).Visible = True
                        sdbgDiscreta.RemoveAll
                        If Not .PondLista Is Nothing Then
                            For Each oValorLista In .PondLista
                                sdbgDiscreta.AddItem oValorLista.ValorLista & Chr(m_lSeparador) & oValorLista.ValorPond & Chr(m_lSeparador) & oValorLista.indice
                            Next
                        Else
                            For Each oValor In .Campo.ValoresLista
                                sdbgDiscreta.AddItem oValor.valorFec & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oValor.indice + 1
                            Next
                        End If
                    Case EscalaContinua
                        optPond(2).Value = True
                        fraPond(4).Visible = True
                        sdbgContinua.RemoveAll
                        For Each oValorLista In .PondLista
                            sdbgContinua.AddItem oValorLista.ValorDesde & Chr(m_lSeparador) & oValorLista.ValorHasta & Chr(m_lSeparador) & oValorLista.ValorPond & Chr(m_lSeparador) & oValorLista.indice
                        Next
                    Case PorCaducidad
                        optPond(3).Value = True
                        fraPond(5).Visible = True
                        lblSi.Caption = m_sSiNo(2)
                        lblNo.Caption = m_sSiNo(3)
                        txtPuntuacion1.Text = NullToStr(.PondSi)
                        txtPuntuacion2.Text = NullToStr(.PondNo)
                    Case Manual
                        optPond(4).Value = True
                        fraPond(1).Visible = True
                    Case Else
                        optPond(0).Value = True
                        fraPond(0).Visible = True
                End Select
            End If
        
        Case TipoBoolean
            optPond(0).Caption = m_sSinPond
            optPond(1).Caption = m_sAutomatico
            'optPond(2).caption = m_sManual
            For i = 0 To 2
                optPond(i).Visible = True
            Next
            optPond(2).Visible = False
            Select Case .PondTipo
            Case Automatico
                optPond(1).Value = True
                fraPond(5).Visible = True
                lblSi.Caption = m_sSiNo(0)
                lblNo.Caption = m_sSiNo(1)
                txtPuntuacion1.Text = NullToStr(.PondSi)
                txtPuntuacion2.Text = NullToStr(.PondNo)
            Case Manual
                optPond(2).Value = True
                fraPond(1).Visible = True
            Case Else
                optPond(0).Value = True
                fraPond(0).Visible = True
            End Select
            
        Case TipoArchivo
            optPond(0).Caption = m_sSinPond
            optPond(1).Caption = m_sAutomatico
            'optPond(2).caption = m_sManual
            For i = 0 To 2
                optPond(i).Visible = True
            Next
            optPond(2).Visible = False
            Select Case .PondTipo
            Case Automatico
                optPond(1).Value = True
                fraPond(5).Visible = True
                lblSi.Caption = m_sSiNo(4)
                lblNo.Caption = m_sSiNo(5)
                txtPuntuacion1.Text = NullToStr(.PondSi)
                txtPuntuacion2.Text = NullToStr(.PondNo)
                
            Case Manual
                optPond(2).Value = True
                fraPond(1).Visible = True
            Case Else
                optPond(0).Value = True
                fraPond(0).Visible = True
            End Select
        End Select
    End With
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Dim oIdioma As CIdioma
        
    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIDAD_PUNT, Idioma)
    
    If Not Ador Is Nothing Then
        m_sSinPond = Ador(0).Value
        Ador.MoveNext
        m_sContinua = Ador(0).Value
        Ador.MoveNext
        m_sDiscreta = Ador(0).Value
        Ador.MoveNext
        m_sCaducidad = Ador(0).Value
        Ador.MoveNext
        m_sFormula = Ador(0).Value '5
        Ador.MoveNext
        m_sManual = Ador(0).Value
        Ador.MoveNext
        m_sAutomatico = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("DESDE").Caption = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("HASTA").Caption = Ador(0).Value
        Ador.MoveNext
        sdbgDiscreta.Columns("VALOR").Caption = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("PUNTOS").Caption = Ador(0).Value '11
        sdbgDiscreta.Columns("PUNTOS").Caption = Ador(0).Value
        lblPuntos1.Caption = Ador(0).Value
        lblPuntos2.Caption = Ador(0).Value
        Ador.MoveNext
        lblManual.Caption = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(0) = Ador(0).Value 'El intervalo ya existe
        Ador.MoveNext
        m_sMensaje(1) = Ador(0).Value 'el campo desde debe ser inferior al campo hasta
        Ador.MoveNext
        m_sMensaje(2) = Ador(0).Value 'debe ser num�rico
        Ador.MoveNext
        m_sMensaje(3) = Ador(0).Value 'debe ser una fecha
        Ador.MoveNext
        m_sMensaje(4) = Ador(0).Value 'el valor ya existe
        Ador.MoveNext
        m_sMensaje(5) = Ador(0).Value 'El intervalo introducido contiene uno de los ya existentes
        Ador.MoveNext
        m_sMensaje(6) = Ador(0).Value 'Modifique el �ltimo intervalo introducido
        Ador.MoveNext
        lblSinPond.Caption = Ador(0).Value 'Campo sin ponderaci�n 20
        Ador.MoveNext
        m_sMensaje(7) = Ador(0).Value 'Tenga en cuenta el intervalo libre introducido
        Ador.MoveNext
        m_sMensaje(8) = Ador(0).Value 'Introduzca un m�ximo inferior o igual al introducido en el intervalo libre del campo
        Ador.MoveNext
        m_sLabel(0) = Ador(0).Value
        Ador.MoveNext
        m_sTiposVar(0) = Ador(0).Value '24 Certificado
        Ador.MoveNext
        m_sLabel(1) = Ador(0).Value
        Ador.MoveNext
        m_sLabel(2) = Ador(0).Value
        Ador.MoveNext
        sdbgCampos.Columns("DEN").Caption = Ador(0).Value
        Ador.MoveNext
        sdbgCampos.Columns("AYU").Caption = Ador(0).Value
        Ador.MoveNext
        sdbgCampos.Columns("COD").Caption = Ador(0).Value
        Ador.MoveNext
        lblFormulaVar.Caption = Ador(0).Value '30
        Ador.MoveNext
        m_sSiNo(0) = Ador(0).Value 'Valor = S�
        Ador.MoveNext
        m_sSiNo(1) = Ador(0).Value 'Valor = No
        Ador.MoveNext
        m_sSiNo(2) = Ador(0).Value 'Vigente
        Ador.MoveNext
        m_sSiNo(3) = Ador(0).Value 'Caducada
        Ador.MoveNext
        m_sSiNo(4) = Ador(0).Value 'Contiene archivo
        Ador.MoveNext
        m_sSiNo(5) = Ador(0).Value 'No contiene archivo
        Ador.MoveNext
        lblFormula.Caption = Ador(0).Value '37
        For i = 1 To 11
            Ador.MoveNext
            m_sIdiErrorFormula(i) = Ador(0).Value
        Next i
        Ador.MoveNext
        lblCambios.Caption = Ador(0).Value '49
        For i = 1 To 16
            Ador.MoveNext
        Next
        m_sCaption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sIdiErrorFormula(12) = Ador(0).Value '70 La formula contiene una variable sin ponderaci�n.
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sTiposVar(1) = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sDentroPlazo = Ador(0).Value
        Ador.MoveNext
        m_sFueraPlazo = Ador(0).Value
        
        Ador.Close
        
        'Fecha expiraci�n y fecha cumplimentaci�n
        Set m_oIdiomas = GestorParametros.DevolverIdiomas(, , True)
        Dim arIdiomas() As String
        ReDim arIdiomas(0 To m_oIdiomas.Count - 1)
        Set m_dFecExpiracion = New Dictionary
        Set m_dFecCumplimentacion = New Dictionary
        
        i = 0
        For Each oIdioma In m_oIdiomas
            m_dFecExpiracion.Add oIdioma.Cod, ""
            m_dFecCumplimentacion.Add oIdioma.Cod, ""
            
            arIdiomas(i) = oIdioma.Cod
            i = i + 1
        Next
        
        Set Ador = GestorIdiomas.DevolverTextosIdiomasDelModulo(FRM_VAR_CALIDAD_PUNT, arIdiomas, 75)
        For Each oIdioma In m_oIdiomas
            m_dFecExpiracion.Item(oIdioma.Cod) = Ador("TEXT_" & oIdioma.Cod)
        Next
        Ador.Close
        
        Set Ador = GestorIdiomas.DevolverTextosIdiomasDelModulo(FRM_VAR_CALIDAD_PUNT, arIdiomas, 76)
        For Each oIdioma In m_oIdiomas
            m_dFecCumplimentacion.Item(oIdioma.Cod) = Ador("TEXT_" & oIdioma.Cod)
        Next
        Ador.Close
    End If

    Set Ador = Nothing
    Set oIdioma = Nothing
End Sub

Private Sub Arrange()
    Dim dblTres As Double

    On Error Resume Next
        
    If Me.Width < 10200 Then Me.Width = 10200
    If Me.Height < 6000 Then Me.Height = 6000
    
    dblTres = picSolicitud.Width / 3
    lblTipo.Width = dblTres - 100
    lblOrigen.Width = dblTres - 100
    lblForm.Width = dblTres - 100
    lblOrigen.Left = lblTipo.Left + lblTipo.Width + 100
    lblForm.Left = lblOrigen.Left + lblOrigen.Width + 100
    
    Line2.X2 = Me.Width + 90
    
    picConfig.Height = Me.ScaleHeight - picSolicitud.Height - picEdit.Height
    If Not lblCambios.Visible Then
        ssTabVar.Height = picConfig.Height
        picNumerico.Height = picConfig.Height
        Picture1.Height = picConfig.Height
    Else
        ssTabVar.Height = picConfig.Height - 300
        picNumerico.Height = picConfig.Height - 300
    
        Picture1.Height = picConfig.Height - 300
    End If
    Picture1.Left = ssTabVar.Left + ssTabVar.Width + 100
    ssTabVar.Width = picConfig.Width - Picture1.Width - 300
    sdbgCampos.Width = ssTabVar.Width - 200
    lblCambios.Top = Picture1.Top + Picture1.Height + 100
    picNumerico.Height = Picture1.Height
    sdbgCampos.Height = ssTabVar.Height - 500
    sdbgCampos.Columns("ID").Width = sdbgCampos.Width * 0.1
    sdbgCampos.Columns("DEN").Width = sdbgCampos.Width * 0.6
    sdbgCampos.Columns("AYU").Width = sdbgCampos.Width * 0.25 - 200
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>
''' Antes de salir del formulario se Comprueba que la formular introducida sea correcta
''' </summary>
''' <param name="cancel">Por si queremos cancelar el seguimiento</param>
''' <remarks>Llamada desde; Tiempo m�ximo=0,2seg.</remarks>
Private Sub Form_Unload(Cancel As Integer)
    'Para forzar un BeforeRowColChange
    If sdbgCampos.Row = 0 Then
        sdbgCampos.MoveNext
    Else
        sdbgCampos.MoveFirst
    End If
        
    If txtFormulaVar.Text = "" Then
        Mensajes.NoValido Left(lblFormulaVar.Caption, Len(lblFormulaVar.Caption) - 1)
        Cancel = True
        Exit Sub
    End If
    
    If ValidarFormula(True, txtFormulaVar.Text, False) Then
        ActualizarVariable
    Else
        Cancel = True
        Exit Sub
    End If
        
    Set g_oVariableCalidad = Nothing
    Set g_oVarCalidadOriginal = Nothing
    Set m_dFecExpiracion = Nothing
    Set m_dFecCumplimentacion = Nothing
End Sub

Private Sub ActualizarVariable()
    g_oVariableCalidad.Formula = StrToNull(txtFormulaVar.Text)
    If Not g_oVarCalidadOriginal Is Nothing Then
        If NullToStr(g_oVariableCalidad.Formula) <> NullToStr(g_oVarCalidadOriginal.Formula) Then
            If Not g_oVariableCalidad.modificado Then g_oVariableCalidad.modificado = True
        Else
            txtFormulaVar.ForeColor = vbBlack
        End If
    End If
    
    If g_oVariableCalidad.modificado Then VisualizarGuardar
End Sub

Private Sub optPond_Click(Index As Integer)
    Dim oValorLista As CValorPond
    Dim oValor As CCampoValorLista
    Dim vPond As Variant
    Dim i As Integer
    
    If m_bRespetarCarga Then Exit Sub
    
    VisualizarGuardar
    Arrange
    
    For i = 0 To 5
        fraPond(i).Visible = False
    Next
    
    Select Case optPond(Index).Caption
    Case m_sSinPond
        fraPond(0).Visible = True
    Case m_sContinua
        fraPond(4).Visible = True
        sdbgContinua.RemoveAll
        If Not m_oCampoSeleccionado.PondLista Is Nothing Then
            'Si el valorlista no es nulo quiere decir que hay una discreta y la continua no tiene datos
            If Not m_oCampoSeleccionado.PondLista.Item(1) Is Nothing Then
            If IsNull(m_oCampoSeleccionado.PondLista.Item(1).ValorLista) Then
                For Each oValorLista In m_oCampoSeleccionado.PondLista
                    sdbgContinua.AddItem oValorLista.ValorDesde & Chr(m_lSeparador) & oValorLista.ValorHasta & Chr(m_lSeparador) & oValorLista.ValorPond & Chr(m_lSeparador) & oValorLista.indice
                Next
            End If
            End If
        End If
    Case m_sDiscreta
        fraPond(3).Visible = True
        sdbgDiscreta.RemoveAll
        With m_oCampoSeleccionado
            If Not .PondLista Is Nothing Then
                For Each oValor In .Campo.ValoresLista
                    If Not .PondLista.Item(oValor.indice + 1) Is Nothing Then
                        If .Campo.Tipo = TipoNumerico Then
                            If VarToDec0(.PondLista.Item(oValor.indice + 1).ValorLista) = VarToDec0(oValor.valorNum) Then
                                vPond = VarToDec0(.PondLista.Item(oValor.indice + 1).ValorPond)
                            Else
                                vPond = Null
                            End If
                        Else
                            If .PondLista.Item(oValor.indice + 1).ValorLista = oValor.valorFec Then
                                vPond = VarToDec0(.PondLista.Item(oValor.indice + 1).ValorPond)
                            Else
                                vPond = Null
                            End If
                        End If
                    Else
                        vPond = Null
                    End If
                    If .Campo.Tipo = TipoNumerico Then
                        sdbgDiscreta.AddItem oValor.valorNum & Chr(m_lSeparador) & NullToStr(vPond) & Chr(m_lSeparador) & oValor.indice + 1
                    Else
                        sdbgDiscreta.AddItem oValor.valorFec & Chr(m_lSeparador) & NullToStr(vPond) & Chr(m_lSeparador) & oValor.indice + 1
                    End If
                Next
            Else
                 For Each oValor In .Campo.ValoresLista
                    Select Case .Campo.Tipo
                        Case TipoNumerico
                            sdbgDiscreta.AddItem oValor.valorNum & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oValor.indice + 1
                        Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
                            sdbgDiscreta.AddItem oValor.valorText.Item(Idioma).Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oValor.indice + 1
                        Case Else
                            sdbgDiscreta.AddItem oValor.valorFec & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oValor.indice + 1
                    End Select
                Next
            End If
        End With
    Case m_sCaducidad
        fraPond(5).Visible = True
        If m_oCampoSeleccionado.Campo.Id = VarCalPCertCampoGeneral.FecCumplimentacion Then
            lblSi.Caption = m_sDentroPlazo
            lblNo.Caption = m_sFueraPlazo
        Else
            lblSi.Caption = m_sSiNo(2)
            lblNo.Caption = m_sSiNo(3)
        End If
        txtPuntuacion1.Text = NullToStr(m_oCampoSeleccionado.PondSi)
        txtPuntuacion2.Text = NullToStr(m_oCampoSeleccionado.PondNo)
        
    Case m_sFormula
        fraPond(2).Visible = True
        txtFormula.Text = NullToStr(m_oCampoSeleccionado.PondFormula)
    Case m_sManual
        fraPond(1).Visible = True
    Case m_sAutomatico
        fraPond(5).Visible = True
        txtPuntuacion1.Text = NullToStr(m_oCampoSeleccionado.PondSi)
        txtPuntuacion2.Text = NullToStr(m_oCampoSeleccionado.PondNo)
        If m_oCampoSeleccionado.Campo.Tipo = TipoArchivo Then
            lblSi.Caption = m_sSiNo(4)
            lblNo.Caption = m_sSiNo(5)
        ElseIf m_oCampoSeleccionado.Campo.Tipo = TipoBoolean Then
            lblSi.Caption = m_sSiNo(0)
            lblNo.Caption = m_sSiNo(1)
        End If
    End Select
End Sub

Private Sub sdbgCampos_BeforeRowColChange(Cancel As Integer)
    Dim bGuardar As Boolean
    Dim iTipoPond As TAtributoPonderacion
    Dim i As Integer
    Dim vbm As Variant
    Dim lInd As Integer
    
    If m_bRespetarCarga Then Exit Sub
    
    bGuardar = False
    'Si ha cambiado la ponderaci�n la guardo
    If Not m_oCampoSeleccionado Is Nothing Then
        If sdbgContinua.DataChanged Then
            sdbgContinua.Update
            If m_bErrorC Then
                Cancel = True
                Exit Sub
            End If
        End If
        If sdbgDiscreta.DataChanged Then
            sdbgDiscreta.Update
            If m_bErrorD Then
                Cancel = True
                Exit Sub
            End If
        End If
        
        Select Case m_oCampoSeleccionado.Campo.Tipo
        Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
            If optPond(0).Value Then
                iTipoPond = SinPonderacion
            ElseIf optPond(1).Value Then
                If m_oCampoSeleccionado.Campo.TipoIntroduccion = IntroLibre Then
                    iTipoPond = Manual
                Else
                    iTipoPond = EscalaDiscreta
                    
                    For i = 0 To sdbgDiscreta.Rows - 1
                        vbm = sdbgDiscreta.AddItemBookmark(i)
                        lInd = i + 1
                        If sdbgDiscreta.Columns("PUNTOS").CellValue(vbm) = "" Then
                            Mensajes.NoValido sdbgDiscreta.Columns("PUNTOS").Caption
                            Cancel = True
                            Exit Sub
                        End If
                    Next
                End If
            Else
                iTipoPond = SinPonderacion
            End If
            
            If m_oCampoSeleccionado.PondTipo <> iTipoPond Then
                bGuardar = True
            Else
                Select Case m_oCampoSeleccionado.PondTipo
                    Case EscalaDiscreta
                        If sdbgDiscreta.Rows <> m_oCampoSeleccionado.PondLista.Count Then
                            bGuardar = True
                        Else
                            For i = 0 To sdbgDiscreta.Rows - 1
                                vbm = sdbgDiscreta.AddItemBookmark(i)
                                lInd = i + 1
                                If VarToDec0(sdbgDiscreta.Columns("PUNTOS").CellValue(vbm)) <> VarToDec0(m_oCampoSeleccionado.PondLista.Item(CStr(lInd)).ValorPond) Then
                                    bGuardar = True
                                    Exit For
                                End If
                            Next
                        End If
                End Select
            End If
        Case TipoNumerico
            If optPond(0).Value Then
                iTipoPond = SinPonderacion
            ElseIf optPond(1).Value Then
                iTipoPond = EscalaDiscreta
                For i = 0 To sdbgDiscreta.Rows - 1
                    vbm = sdbgDiscreta.AddItemBookmark(i)
                    lInd = i + 1
                    If sdbgDiscreta.Columns("PUNTOS").CellValue(vbm) = "" Then
                        Mensajes.NoValido sdbgDiscreta.Columns("PUNTOS").Caption
                        Cancel = True
                        Exit Sub
                    End If
                Next
            ElseIf optPond(2).Value Then
                iTipoPond = EscalaContinua
                If sdbgContinua.Rows = 0 Then
                    Mensajes.NoValido m_sContinua
                    Cancel = True
                    Exit Sub
                End If
            ElseIf optPond(3).Value Then
                iTipoPond = Formula
                If txtFormula.Text = "" Then
                    Mensajes.NoValido m_sFormula
                    Cancel = True
                    Exit Sub
                End If
            ElseIf optPond(4).Value Then
                iTipoPond = Manual
            Else
                iTipoPond = SinPonderacion
            End If
            
            If m_oCampoSeleccionado.PondTipo <> iTipoPond Then
                bGuardar = True
            Else
                Select Case m_oCampoSeleccionado.PondTipo
                Case EscalaDiscreta
                    If sdbgDiscreta.Rows <> m_oCampoSeleccionado.PondLista.Count Then
                        bGuardar = True
                    Else
                        For i = 0 To sdbgDiscreta.Rows - 1
                            vbm = sdbgDiscreta.AddItemBookmark(i)
                            lInd = i + 1
                            If VarToDec0(sdbgDiscreta.Columns("PUNTOS").CellValue(vbm)) <> VarToDec0(m_oCampoSeleccionado.PondLista.Item(CStr(lInd)).ValorPond) Then
                                bGuardar = True
                                Exit For
                            End If
                        Next
                    End If
                Case EscalaContinua
                    If sdbgContinua.Rows <> m_oCampoSeleccionado.PondLista.Count Then
                        bGuardar = True
                    Else
                        For i = 0 To sdbgContinua.Rows - 1
                            vbm = sdbgContinua.AddItemBookmark(i)
                            lInd = i + 1
                            If Not m_oCampoSeleccionado.PondLista.Item(CStr(lInd)) Is Nothing Then
                                If VarToDec0(sdbgContinua.Columns("DESDE").CellValue(vbm)) <> VarToDec0(m_oCampoSeleccionado.PondLista.Item(CStr(lInd)).ValorDesde) Then
                                    bGuardar = True
                                    Exit For
                                End If
                                If VarToDec0(sdbgContinua.Columns("HASTA").CellValue(vbm)) <> VarToDec0(m_oCampoSeleccionado.PondLista.Item(CStr(lInd)).ValorHasta) Then
                                    bGuardar = True
                                    Exit For
                                End If
                                If VarToDec0(sdbgContinua.Columns("PUNTOS").CellValue(vbm)) <> VarToDec0(m_oCampoSeleccionado.PondLista.Item(CStr(lInd)).ValorPond) Then
                                    bGuardar = True
                                    Exit For
                                End If
                            Else
                                bGuardar = True
                                Exit For
                            End If
                        Next
                    End If
                Case Formula
                    If ValidarFormula(False, txtFormula.Text, False) Then
                        If txtFormula.Text <> m_oCampoSeleccionado.PondFormula Then
                            bGuardar = True
                        End If
                    Else
                        Cancel = True
                        Exit Sub
                    End If
            
                End Select
            End If
    
        Case TipoFecha
            If m_oCampoSeleccionado.Campo.Id = VarCalPCertCampoGeneral.FecCumplimentacion Then
                If optPond(0).Value Then
                    iTipoPond = SinPonderacion
                ElseIf optPond(1).Value Then
                    iTipoPond = Automatico
                End If
                If m_oCampoSeleccionado.PondTipo <> iTipoPond Then
                    bGuardar = True
                Else
                    bGuardar = ((VarToDec0(txtPuntuacion1.Text) <> VarToDec0(m_oCampoSeleccionado.PondSi)) Or (VarToDec0(txtPuntuacion2.Text) <> VarToDec0(m_oCampoSeleccionado.PondNo)))
                End If
            Else
                If optPond(0).Value Then
                    iTipoPond = SinPonderacion
                ElseIf optPond(1).Value Then
                    iTipoPond = EscalaDiscreta
                    For i = 0 To sdbgDiscreta.Rows - 1
                        vbm = sdbgDiscreta.AddItemBookmark(i)
                        lInd = i + 1
                        If sdbgDiscreta.Columns("PUNTOS").CellValue(vbm) = "" Then
                            Mensajes.NoValido sdbgDiscreta.Columns("PUNTOS").Caption
                            Cancel = True
                            Exit Sub
                        End If
                    Next
                ElseIf optPond(2).Value Then
                    iTipoPond = EscalaContinua
                    If sdbgContinua.Rows = 0 Then
                        Mensajes.NoValido m_sContinua
                        Cancel = True
                        Exit Sub
                    End If
                ElseIf optPond(3).Value Then
                    iTipoPond = PorCaducidad
                    If Not IsNumeric(txtPuntuacion1.Text) Or Not IsNumeric(txtPuntuacion2.Text) Then
                        Mensajes.NoValido sdbgDiscreta.Columns("PUNTOS").Caption
                        Cancel = True
                        Exit Sub
                    End If
                ElseIf optPond(4).Value Then
                    iTipoPond = Manual
                Else
                    iTipoPond = SinPonderacion
                End If
                
                If m_oCampoSeleccionado.PondTipo <> iTipoPond Then
                    bGuardar = True
                Else
                    Select Case m_oCampoSeleccionado.PondTipo
                    Case EscalaDiscreta
                        If sdbgDiscreta.Rows <> m_oCampoSeleccionado.PondLista.Count Then
                            bGuardar = True
                        Else
                            For i = 0 To sdbgDiscreta.Rows - 1
                                vbm = sdbgDiscreta.AddItemBookmark(i)
                                lInd = i + 1
                                If VarToDec0(sdbgDiscreta.Columns("PUNTOS").CellValue(vbm)) <> VarToDec0(m_oCampoSeleccionado.PondLista.Item(CStr(lInd)).ValorPond) Then
                                    bGuardar = True
                                    Exit For
                                End If
                            Next
                        End If
                        
                    Case EscalaContinua
                        If sdbgContinua.Rows <> m_oCampoSeleccionado.PondLista.Count Then
                            bGuardar = True
                        Else
                            For i = 0 To sdbgContinua.Rows - 1
                                vbm = sdbgContinua.AddItemBookmark(i)
                                lInd = i + 1
                                If Not m_oCampoSeleccionado.PondLista.Item(CStr(lInd)) Is Nothing Then
                                    If CDate(sdbgContinua.Columns("DESDE").CellValue(vbm)) <> CDate(m_oCampoSeleccionado.PondLista.Item(CStr(lInd)).ValorDesde) Then
                                        bGuardar = True
                                        Exit For
                                    End If
                                    If CDate(sdbgContinua.Columns("HASTA").CellValue(vbm)) <> CDate(m_oCampoSeleccionado.PondLista.Item(CStr(lInd)).ValorHasta) Then
                                        bGuardar = True
                                        Exit For
                                    End If
                                    If VarToDec0(sdbgContinua.Columns("PUNTOS").CellValue(vbm)) <> VarToDec0(m_oCampoSeleccionado.PondLista.Item(CStr(lInd)).ValorPond) Then
                                        bGuardar = True
                                        Exit For
                                    End If
                                Else
                                    bGuardar = True
                                    Exit For
                                End If
                            Next
                        End If
                        
                    Case PorCaducidad
                        If VarToDec0(txtPuntuacion1.Text) <> VarToDec0(m_oCampoSeleccionado.PondSi) Then
                            bGuardar = True
                        End If
                        If VarToDec0(txtPuntuacion2.Text) <> VarToDec0(m_oCampoSeleccionado.PondNo) Then
                            bGuardar = True
                        End If
                
                    End Select
                End If
            End If
        Case TipoBoolean
            If optPond(0).Value Then
                iTipoPond = SinPonderacion
            ElseIf optPond(1).Value Then
                iTipoPond = Automatico
                If Not IsNumeric(txtPuntuacion1.Text) Or Not IsNumeric(txtPuntuacion2.Text) Then
                    Mensajes.NoValido sdbgDiscreta.Columns("PUNTOS").Caption
                    Cancel = True
                    Exit Sub
                End If
            ElseIf optPond(2).Value Then
                iTipoPond = Manual
            Else
                iTipoPond = SinPonderacion
            End If
            If m_oCampoSeleccionado.PondTipo <> iTipoPond Then
                bGuardar = True
            Else
                If m_oCampoSeleccionado.PondTipo = Automatico Then
                    If VarToDec0(txtPuntuacion1.Text) <> VarToDec0(m_oCampoSeleccionado.PondSi) Then
                        bGuardar = True
                    End If
                    If VarToDec0(txtPuntuacion2.Text) <> VarToDec0(m_oCampoSeleccionado.PondNo) Then
                        bGuardar = True
                    End If
                End If
            End If
    
        Case TipoArchivo
            If optPond(0).Value Then
                iTipoPond = SinPonderacion
            ElseIf optPond(1).Value Then
                iTipoPond = Automatico
                If Not IsNumeric(txtPuntuacion1.Text) Or Not IsNumeric(txtPuntuacion2.Text) Then
                    Mensajes.NoValido sdbgDiscreta.Columns("PUNTOS").Caption
                    Cancel = True
                    Exit Sub
                End If
            ElseIf optPond(2).Value Then
                iTipoPond = Manual
            Else
                iTipoPond = SinPonderacion
            End If
            If m_oCampoSeleccionado.PondTipo <> iTipoPond Then
                bGuardar = True
            Else
                If m_oCampoSeleccionado.PondTipo = Automatico Then
                    If VarToDec0(txtPuntuacion1.Text) <> VarToDec0(m_oCampoSeleccionado.PondSi) Then
                        bGuardar = True
                    End If
                    If VarToDec0(txtPuntuacion2.Text) <> VarToDec0(m_oCampoSeleccionado.PondNo) Then
                        bGuardar = True
                    End If
                End If
            End If
        End Select
        
        If bGuardar Then
            If Not GuardarCampo Then
               Cancel = True
            Else
                sdbgCampos.Columns("PONDTIPO").Value = m_oCampoSeleccionado.PondTipo
            End If
        End If
    End If
End Sub

'''Guarda dependiendo del tipo de campo la ponderaci�n que tendra en el calculo de puntuaciones
Private Function GuardarCampo() As Boolean
    Dim vbm As Variant
    Dim i As Integer
    
    On Error GoTo ERROR
    
    txtFormulaVar.ForeColor = vbRed
    
    g_oVariableCalidad.modificado = True
    VisualizarGuardar
    Arrange
    
    Select Case m_oCampoSeleccionado.Campo.Tipo
    Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
        m_oCampoSeleccionado.PondFormula = Null
        Set m_oCampoSeleccionado.PondLista = Nothing
        m_oCampoSeleccionado.PondSi = Null
        m_oCampoSeleccionado.PondNo = Null
        If optPond(1).Value Then
             If m_oCampoSeleccionado.Campo.TipoIntroduccion = Introselec Then
                m_oCampoSeleccionado.PondTipo = EscalaDiscreta
                
                Set m_oCampoSeleccionado.PondLista = Raiz.Generar_CValoresPond
                m_oCampoSeleccionado.PondSi = Null
                m_oCampoSeleccionado.PondNo = Null
                With m_oCampoSeleccionado.PondLista
                    For i = 0 To sdbgDiscreta.Rows - 1
                        vbm = sdbgDiscreta.AddItemBookmark(i)
                        .Add , , , , , i + 1, sdbgDiscreta.Columns("VALOR").CellValue(vbm), sdbgDiscreta.Columns("PUNTOS").CellValue(vbm), , , , i + 1
                    Next
                End With
            Else
                m_oCampoSeleccionado.PondTipo = Manual
            End If
        Else
            m_oCampoSeleccionado.PondTipo = SinPonderacion
        End If

    Case TipoNumerico
        If optPond(1).Value Then
            m_oCampoSeleccionado.PondTipo = EscalaDiscreta
            m_oCampoSeleccionado.PondFormula = Null
            Set m_oCampoSeleccionado.PondLista = Nothing
            Set m_oCampoSeleccionado.PondLista = Raiz.Generar_CValoresPond
            m_oCampoSeleccionado.PondSi = Null
            m_oCampoSeleccionado.PondNo = Null
            With m_oCampoSeleccionado.PondLista
                For i = 0 To sdbgDiscreta.Rows - 1
                    vbm = sdbgDiscreta.AddItemBookmark(i)
                    .Add , , , , , i + 1, sdbgDiscreta.Columns("VALOR").CellValue(vbm), sdbgDiscreta.Columns("PUNTOS").CellValue(vbm), , , , i + 1
                Next
            End With
        ElseIf optPond(2).Value Then
            m_oCampoSeleccionado.PondTipo = EscalaContinua
            m_oCampoSeleccionado.PondFormula = Null
            Set m_oCampoSeleccionado.PondLista = Nothing
            Set m_oCampoSeleccionado.PondLista = Raiz.Generar_CValoresPond
            m_oCampoSeleccionado.PondSi = Null
            m_oCampoSeleccionado.PondNo = Null
            With m_oCampoSeleccionado.PondLista
                For i = 0 To sdbgContinua.Rows - 1
                    vbm = sdbgContinua.AddItemBookmark(i)
                    .Add , , , , , i + 1, , sdbgContinua.Columns("PUNTOS").CellValue(vbm), sdbgContinua.Columns("DESDE").CellValue(vbm), sdbgContinua.Columns("HASTA").CellValue(vbm), , i + 1
                Next
            End With
            
        ElseIf optPond(3).Value Then
            m_oCampoSeleccionado.PondTipo = Formula
            m_oCampoSeleccionado.PondFormula = txtFormula.Text
            Set m_oCampoSeleccionado.PondLista = Nothing
            m_oCampoSeleccionado.PondSi = Null
            m_oCampoSeleccionado.PondNo = Null

        ElseIf optPond(4).Value Then
            m_oCampoSeleccionado.PondTipo = Manual
            m_oCampoSeleccionado.PondFormula = Null
            Set m_oCampoSeleccionado.PondLista = Nothing
            m_oCampoSeleccionado.PondSi = Null
            m_oCampoSeleccionado.PondNo = Null
        Else
            m_oCampoSeleccionado.PondTipo = SinPonderacion
            m_oCampoSeleccionado.PondFormula = Null
            Set m_oCampoSeleccionado.PondLista = Nothing
            m_oCampoSeleccionado.PondSi = Null
            m_oCampoSeleccionado.PondNo = Null
        End If

    Case TipoFecha
        If m_oCampoSeleccionado.Campo.Id = VarCalPCertCampoGeneral.FecCumplimentacion Then
            If optPond(1).Value Then
                m_oCampoSeleccionado.PondTipo = Automatico
            Else
                m_oCampoSeleccionado.PondTipo = SinPonderacion
            End If
            m_oCampoSeleccionado.PondFormula = Null
            Set m_oCampoSeleccionado.PondLista = Nothing
            m_oCampoSeleccionado.PondSi = Null
            m_oCampoSeleccionado.PondNo = Null
            m_oCampoSeleccionado.PondSi = StrToNull(txtPuntuacion1.Text)
            m_oCampoSeleccionado.PondNo = StrToNull(txtPuntuacion2.Text)
        Else
            If optPond(1).Value Then
                m_oCampoSeleccionado.PondTipo = EscalaDiscreta
                m_oCampoSeleccionado.PondFormula = Null
                Set m_oCampoSeleccionado.PondLista = Nothing
                Set m_oCampoSeleccionado.PondLista = Raiz.Generar_CValoresPond
                m_oCampoSeleccionado.PondSi = Null
                m_oCampoSeleccionado.PondNo = Null
                With m_oCampoSeleccionado.PondLista
                    For i = 0 To sdbgDiscreta.Rows - 1
                        vbm = sdbgDiscreta.AddItemBookmark(i)
                        .Add , , , , , i + 1, sdbgDiscreta.Columns("VALOR").CellValue(vbm), sdbgDiscreta.Columns("PUNTOS").CellValue(vbm), , , , i + 1
                    Next
                End With
                
            ElseIf optPond(2).Value Then
                m_oCampoSeleccionado.PondTipo = EscalaContinua
                m_oCampoSeleccionado.PondFormula = Null
                Set m_oCampoSeleccionado.PondLista = Nothing
                Set m_oCampoSeleccionado.PondLista = Raiz.Generar_CValoresPond
                m_oCampoSeleccionado.PondSi = Null
                m_oCampoSeleccionado.PondNo = Null
                With m_oCampoSeleccionado.PondLista
                    For i = 0 To sdbgContinua.Rows - 1
                        vbm = sdbgContinua.AddItemBookmark(i)
                        .Add , , , , , i + 1, , sdbgContinua.Columns("PUNTOS").CellValue(vbm), sdbgContinua.Columns("DESDE").CellValue(vbm), sdbgContinua.Columns("HASTA").CellValue(vbm), , i + 1
                    Next
                End With
                
            ElseIf optPond(3).Value Then
                m_oCampoSeleccionado.PondTipo = PorCaducidad
                m_oCampoSeleccionado.PondFormula = Null
                Set m_oCampoSeleccionado.PondLista = Nothing
                m_oCampoSeleccionado.PondSi = StrToNull(txtPuntuacion1.Text)
                m_oCampoSeleccionado.PondNo = StrToNull(txtPuntuacion2.Text)
            ElseIf optPond(4).Value Then
                m_oCampoSeleccionado.PondTipo = Manual
                m_oCampoSeleccionado.PondFormula = Null
                Set m_oCampoSeleccionado.PondLista = Nothing
                m_oCampoSeleccionado.PondSi = Null
                m_oCampoSeleccionado.PondNo = Null
            Else
                m_oCampoSeleccionado.PondTipo = SinPonderacion
                m_oCampoSeleccionado.PondFormula = Null
                Set m_oCampoSeleccionado.PondLista = Nothing
                m_oCampoSeleccionado.PondSi = Null
                m_oCampoSeleccionado.PondNo = Null
            End If
        End If
    
    Case TipoBoolean
        m_oCampoSeleccionado.PondFormula = Null
        Set m_oCampoSeleccionado.PondLista = Nothing
        m_oCampoSeleccionado.PondSi = Null
        m_oCampoSeleccionado.PondNo = Null
        If optPond(1).Value Then
            m_oCampoSeleccionado.PondTipo = Automatico
            m_oCampoSeleccionado.PondSi = StrToNull(txtPuntuacion1.Text)
            m_oCampoSeleccionado.PondNo = StrToNull(txtPuntuacion2.Text)
        ElseIf optPond(2).Value Then
            m_oCampoSeleccionado.PondTipo = Manual
        Else
            m_oCampoSeleccionado.PondTipo = SinPonderacion
        End If

    Case TipoArchivo
        m_oCampoSeleccionado.PondFormula = Null
        Set m_oCampoSeleccionado.PondLista = Nothing
        m_oCampoSeleccionado.PondSi = Null
        m_oCampoSeleccionado.PondNo = Null
        If optPond(1).Value Then
            m_oCampoSeleccionado.PondTipo = Automatico
            m_oCampoSeleccionado.PondSi = StrToNull(txtPuntuacion1.Text)
            m_oCampoSeleccionado.PondNo = StrToNull(txtPuntuacion2.Text)
        ElseIf optPond(2).Value Then
            m_oCampoSeleccionado.PondTipo = Manual
        Else
            m_oCampoSeleccionado.PondTipo = SinPonderacion
        End If
    End Select
    GuardarCampo = True

ERROR:
    GuardarCampo = False
End Function
Private Sub sdbgCampos_BtnClick()
    If sdbgCampos.Rows = 0 Then Exit Sub
    
    If sdbgCampos.Columns(sdbgCampos.Col).Name = "AYU" Then
        'Muestra el formulario con la ayuda:
        MostrarFormFormCampoAyudaLocal GestorIdiomas, Idioma, Raiz, Mensajes, GestorSeguridad, UsuCod, m_ParamGen, FormCampoAyudaTipoAyuda.CampoInstancia, False, _
            m_oPonderaciones.Item(CStr(sdbgCampos.Columns("ID").Value)).Campo
    End If
End Sub

Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If Not IsNull(LastRow) Then
        If sdbgCampos.AddItemRowIndex(LastRow) <> sdbgCampos.AddItemRowIndex(sdbgCampos.Bookmark) Then
            CampoSeleccionado
        End If
    Else
        If sdbgCampos.Row >= 0 Then
            CampoSeleccionado
        End If
    End If
End Sub

Private Sub sdbgCampos_RowLoaded(ByVal Bookmark As Variant)
    If sdbgCampos.Columns("PONDTIPO").Value <> 0 Then
        sdbgCampos.Columns("DEN").CellStyleSet "Ponderacion"
    Else
        sdbgCampos.Columns("DEN").CellStyleSet ""
    End If
End Sub

Private Sub sdbgContinua_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
End Sub

Private Sub sdbgContinua_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgContinua_BeforeUpdate(Cancel As Integer)
    Dim vValorDesde As Variant
    Dim vValorHasta As Variant
    Dim irespuesta As Integer
    Dim i As Integer
    
    m_bErrorC = False
    If sdbgContinua.Columns("DESDE").Text = "" Then
        If sdbgContinua.Columns("HASTA").Text = "" And sdbgContinua.Columns("PUNTOS").Text = "" Then
            sdbgContinua.CancelUpdate
            If m_oCampoSeleccionado.Campo.Tipo = TipoFecha Then
                Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & " " & m_sMensaje(3)
            Else
                Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & " " & m_sMensaje(2)
            End If
            If Me.Visible Then sdbgContinua.SetFocus
            Cancel = True
            m_bErrorC = True
        End If
    Else
        If m_oCampoSeleccionado.Campo.Tipo = TiposDeAtributos.TipoFecha Then
            If Not IsDate(sdbgContinua.Columns("DESDE").Text) Then
                Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & " " & m_sMensaje(3)
                If Me.Visible Then sdbgContinua.SetFocus
                Cancel = True
                m_bErrorC = True
            Else
                If Not IsDate(sdbgContinua.Columns("HASTA").Text) Then
                    Mensajes.NoValido sdbgContinua.Columns("HASTA").Caption & " " & m_sMensaje(3)
                    If Me.Visible Then sdbgContinua.SetFocus
                    Cancel = True
                    m_bErrorC = True
                Else
                    If IsDate(m_oCampoSeleccionado.Campo.Minimo) Then
                        If IsDate(m_oCampoSeleccionado.Campo.Maximo) Then
                            If CDate(m_oCampoSeleccionado.Campo.Minimo) > CDate(sdbgContinua.Columns("DESDE").Value) Or CDate(m_oCampoSeleccionado.Campo.Maximo) < CDate(sdbgContinua.Columns("DESDE").Value) Then
                              Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                              If Me.Visible Then sdbgContinua.SetFocus
                              Cancel = True
                              m_bErrorC = True
                              Exit Sub
                            ElseIf CDate(m_oCampoSeleccionado.Campo.Maximo) < CDate(sdbgContinua.Columns("HASTA").Value) Or CDate(m_oCampoSeleccionado.Campo.Minimo) > CDate(sdbgContinua.Columns("HASTA").Value) Then
                                Mensajes.NoValido sdbgContinua.Columns("HASTA").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                                If Me.Visible Then sdbgContinua.SetFocus
                                Cancel = True
                                m_bErrorC = True
                                Exit Sub
                            End If
                        Else
                            If CDate(m_oCampoSeleccionado.Campo.Minimo) > CDate(sdbgContinua.Columns("DESDE").Value) Then
                              Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                              If Me.Visible Then sdbgContinua.SetFocus
                              Cancel = True
                              m_bErrorC = True
                              Exit Sub
                            ElseIf CDate(m_oCampoSeleccionado.Campo.Minimo) > CDate(sdbgContinua.Columns("HASTA").Value) Then
                                Mensajes.NoValido sdbgContinua.Columns("HASTA").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                                If Me.Visible Then sdbgContinua.SetFocus
                                Cancel = True
                                m_bErrorC = True
                                Exit Sub
                            End If
                        End If
                    Else
                        If m_oCampoSeleccionado.Campo.Maximo <> "" And IsDate(m_oCampoSeleccionado.Campo.Maximo) Then
                            If CDate(m_oCampoSeleccionado.Campo.Maximo) < CDate(sdbgContinua.Columns("DESDE").Value) Then
                              Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                              If Me.Visible Then sdbgContinua.SetFocus
                              Cancel = True
                              m_bErrorC = True
                              Exit Sub
                            ElseIf CDate(m_oCampoSeleccionado.Campo.Maximo) < CDate(sdbgContinua.Columns("HASTA").Value) Then
                              Mensajes.NoValido sdbgContinua.Columns("HASTA").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                              If Me.Visible Then sdbgContinua.SetFocus
                              Cancel = True
                              m_bErrorC = True
                              Exit Sub
                            End If
                        End If
                    End If
                    If Not IsNumeric(sdbgContinua.Columns("PUNTOS").Value) Then
                        Mensajes.NoValida sdbgContinua.Columns("PUNTOS").Caption & " " & m_sMensaje(2)
                        If Me.Visible Then sdbgContinua.SetFocus
                        Cancel = True
                        m_bErrorC = True
                    End If
                End If
            End If
        Else
            If m_oCampoSeleccionado.Campo.Tipo = TipoNumerico Then
                If Not IsNumeric(sdbgContinua.Columns("DESDE").Text) Then
                    Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & " " & m_sMensaje(2)
                    If Me.Visible Then sdbgContinua.SetFocus
                    Cancel = True
                    m_bErrorC = True
                Else
                    If Not IsNumeric(sdbgContinua.Columns("HASTA").Text) Then
                        Mensajes.NoValido sdbgContinua.Columns("HASTA").Caption & " " & m_sMensaje(2)
                        If Me.Visible Then sdbgContinua.SetFocus
                        Cancel = True
                        m_bErrorC = True
                    Else
                        If m_oCampoSeleccionado.Campo.Minimo <> "" Then
                            If m_oCampoSeleccionado.Campo.Maximo <> "" Then
                                If VarToDec0(m_oCampoSeleccionado.Campo.Minimo) > VarToDec0(sdbgContinua.Columns("DESDE").Value) Or VarToDec0(m_oCampoSeleccionado.Campo.Maximo) < VarToDec0(sdbgContinua.Columns("DESDE").Value) Then
                                    Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                                    If Me.Visible Then sdbgContinua.SetFocus
                                    Cancel = True
                                    m_bErrorC = True
                                    Exit Sub
                                End If
                            Else
                                If VarToDec0(m_oCampoSeleccionado.Campo.Minimo) > (VarToDec0(sdbgContinua.Columns("DESDE").Value)) Then
                                    Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                                    If Me.Visible Then sdbgContinua.SetFocus
                                    Cancel = True
                                    m_bErrorC = True
                                    Exit Sub
                                End If
                            End If
                        End If
                        If m_oCampoSeleccionado.Campo.Maximo <> "" Then
                            If m_oCampoSeleccionado.Campo.Minimo <> "" Then
                                If VarToDec0(m_oCampoSeleccionado.Campo.Maximo) < VarToDec0(sdbgContinua.Columns("HASTA").Value) Or VarToDec0(m_oCampoSeleccionado.Campo.Minimo) > VarToDec0(sdbgContinua.Columns("HASTA").Value) Then
                                    Mensajes.NoValido sdbgContinua.Columns("HASTA").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                                    If Me.Visible Then sdbgContinua.SetFocus
                                    Cancel = True
                                    m_bErrorC = True
                                    Exit Sub
                                End If
                            Else
                                If VarToDec0(m_oCampoSeleccionado.Campo.Maximo) < VarToDec0(sdbgContinua.Columns("HASTA").Value) Then
                                    Mensajes.NoValido sdbgContinua.Columns("HASTA").Caption & vbLf & m_sMensaje(7) & ": (" & m_oCampoSeleccionado.Campo.Minimo & " - " & m_oCampoSeleccionado.Campo.Maximo & ")"
                                    If Me.Visible Then sdbgContinua.SetFocus
                                    Cancel = True
                                    m_bErrorC = True
                                    Exit Sub
                                End If
                            End If
                        End If
                    
                        If Not IsNumeric(sdbgContinua.Columns("PUNTOS").Value) Then
                            Mensajes.NoValida sdbgContinua.Columns("PUNTOS").Caption & " " & m_sMensaje(2)
                            If Me.Visible Then sdbgContinua.SetFocus
                            Cancel = True
                            m_bErrorC = True
                        End If
                    End If
                End If
            End If
        End If
        
        If Not m_bErrorC Then
            If m_oCampoSeleccionado.Campo.Tipo = TiposDeAtributos.TipoFecha Then
                If IsDate(sdbgContinua.Columns("DESDE").Text) And IsDate(sdbgContinua.Columns("HASTA").Text) Then
                    If CDate(sdbgContinua.Columns("DESDE").Text) > CDate(sdbgContinua.Columns("HASTA").Text) Then
                        Mensajes.NoValida m_sMensaje(1) 'sdbgContinua.Columns("DESDE").Caption
                        If Me.Visible Then sdbgContinua.SetFocus
                        Cancel = True
                        m_bErrorC = True
                    End If
                End If
            Else
                If VarToDec0(sdbgContinua.Columns("DESDE").Text) > VarToDec0(sdbgContinua.Columns("HASTA").Text) Then
                    Mensajes.NoValida m_sMensaje(1) 'sdbgContinua.Columns("DESDE").Caption
                    If Me.Visible Then sdbgContinua.SetFocus
                    Cancel = True
                    m_bErrorC = True
                End If
            End If
            If Not m_bErrorC Then
                If m_oCampoSeleccionado.Campo.Tipo = TipoNumerico Then
                    vValorDesde = VarToDec0(sdbgContinua.Columns("DESDE").Text)
                    vValorHasta = VarToDec0(sdbgContinua.Columns("HASTA").Text)
                Else
                    vValorDesde = CDate(sdbgContinua.Columns("DESDE").Text)
                    vValorHasta = CDate(sdbgContinua.Columns("HASTA").Text)
                End If
                For i = 0 To sdbgContinua.Rows - 1
                If sdbgContinua.AddItemRowIndex(sdbgContinua.Bookmark) <> i Then
                    If m_oCampoSeleccionado.Campo.Tipo = TiposDeAtributos.TipoFecha Then
                        If CDate(vValorDesde) >= CDate(sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i))) And CDate(vValorDesde) < CDate(sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i))) Then
                            'el intervalo definido ya existe
                            Mensajes.NoValido m_sMensaje(0)
                            If Me.Visible Then sdbgContinua.SetFocus
                            Cancel = True
                            m_bErrorC = True
                            Exit Sub
                        End If
                        If CDate(vValorHasta) > CDate(sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i))) And CDate(vValorHasta) < CDate(sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i))) Then
                            'el intervalo definido ya existe
                            Mensajes.NoValida m_sMensaje(0)
                            If Me.Visible Then sdbgContinua.SetFocus
                            Cancel = True
                            m_bErrorC = True
                            Exit Sub
                        End If
                        If CDate(vValorDesde) < CDate(sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i))) And CDate(vValorHasta) > CDate(sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i))) Then
                            'el intervalo introducido contiene uno de los ya existe
                            irespuesta = Mensajes.PreguntaEliminarIntervalo(m_sMensaje(5) & ": " & vbCrLf & sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i)) & " - " & sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i)))
                            If irespuesta = vbNo Then
                                MsgBox m_sMensaje(6), vbInformation, "FULLSTEP"
                                If Me.Visible Then sdbgContinua.SetFocus
                                Cancel = True
                                m_bErrorC = True
                                Exit Sub
                            Else
                                sdbgContinua.RemoveItem i
                                Exit Sub
                            End If
                        End If
                    Else
                        If VarToDec0(vValorDesde) >= VarToDec0(sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i))) And VarToDec0(vValorDesde) < VarToDec0(sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i))) Then
                            'el intervalo definido ya existe
                            Mensajes.NoValido m_sMensaje(0)
                            If Me.Visible Then sdbgContinua.SetFocus
                            Cancel = True
                            m_bErrorC = True
                            Exit Sub
                        End If
                        If VarToDec0(vValorHasta) > VarToDec0(sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i))) And VarToDec0(vValorHasta) < VarToDec0(sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i))) Then
                            'el intervalo definido ya existe
                            Mensajes.NoValida m_sMensaje(0)
                            If Me.Visible Then sdbgContinua.SetFocus
                            Cancel = True
                            m_bErrorC = True
                            Exit Sub
                        End If
                        If VarToDec0(vValorDesde) < VarToDec0(sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i))) And VarToDec0(vValorHasta) > VarToDec0(sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i))) Then
                            'el intervalo introducido contiene uno de los ya existe
                            irespuesta = Mensajes.PreguntaEliminarIntervalo(m_sMensaje(5) & ": " & vbCrLf & sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i)) & " - " & sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i)))
                            If irespuesta = vbNo Then
                                MsgBox m_sMensaje(6), vbInformation, "FULLSTEP"
                                If Me.Visible Then sdbgContinua.SetFocus
                                Cancel = True
                                m_bErrorC = True
                                Exit Sub
                            Else
                                sdbgContinua.RemoveItem i
                                Exit Sub
                            End If
                        End If
                    End If
                End If
                Next
            End If
        End If
    End If
End Sub

Private Sub sdbgContinua_Change()
    VisualizarGuardar
    Arrange
End Sub

Private Sub sdbgDiscreta_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgDiscreta_BeforeUpdate(Cancel As Integer)
    m_bErrorD = False
    If Not IsNumeric(sdbgDiscreta.Columns("PUNTOS").Text) And sdbgDiscreta.Columns("PUNTOS").Value <> "" Then
        Mensajes.NoValida sdbgDiscreta.Columns("PUNTOS").Caption & " " & m_sMensaje(2)
        If Me.Visible Then sdbgDiscreta.SetFocus
        Cancel = True
        m_bErrorD = True
    End If
End Sub


''' <summary>
''' Valida la formula introducida
''' </summary>
''' <param name="bCert">Si se trata de un certificado</param>
''' <param name="sFormula">Formula</param>
''' <param name="bNoMens">Para que no muestre los mensajes de error en la formula
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde=Form_Unload // sdbgCampos_BeforeRowColChange; Tiempo m�ximo:0,2seg.</remarks>
Private Function ValidarFormula(ByVal bCert As Boolean, ByVal sFormula As String, ByVal bNoMens As Boolean) As Boolean
    Dim sVariables() As String
    Dim lErrCode As Integer
    Dim lIndex As Integer
    Dim iEq As USPExpression
    Dim sCaracter As String
    Dim i As Integer
    Dim oVarPond As CVarPonderacion
    Dim bEnFormula As Boolean
    Dim bCorrecto As Boolean
    
    bCorrecto = True
    If sFormula <> "" Then
        Set iEq = New USPExpression
        
        If bCert Then
            If sdbgCampos.Rows > 0 Then
                ReDim sVariables(m_oPonderaciones.Count)
                i = 0
                For Each oVarPond In m_oPonderaciones
                    sVariables(i) = oVarPond.PondCod
                    'Comprobar que si la variable no tiene ponderacion no este en la formula
                    bEnFormula = g_oVariableCalidad.ComprobarVariableEnFormula(sFormula, oVarPond.PondCod)
                    If bEnFormula And oVarPond.PondTipo = SinPonderacion Then
                        bCorrecto = False
                        Exit For
                    End If
                    i = i + 1
                Next
                
            Else
                ReDim sVariables(0)
            End If
        Else
            ReDim sVariables(1)
            sVariables(1) = "X"
        End If
        If bCorrecto Then
            lIndex = iEq.Parse(UCase(sFormula), sVariables, lErrCode)
        
            If lErrCode <> USPEX_NO_ERROR Then
                ' Parsing error handler
                If bNoMens = False Then
                    Select Case lErrCode
                        Case USPEX_DIVISION_BY_ZERO
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(1))
                        Case USPEX_EMPTY_EXPRESSION
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(2))
                        Case USPEX_MISSING_OPERATOR
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(3))
                        Case USPEX_SYNTAX_ERROR
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(4))
                        Case USPEX_UNKNOWN_FUNCTION
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(5))
                        Case USPEX_UNKNOWN_OPERATOR
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(6))
                        Case USPEX_WRONG_PARAMS_NUMBER
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(7))
                        Case USPEX_UNKNOWN_IDENTIFIER
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(8))
                        Case USPEX_UNKNOWN_VAR
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(9))
                        Case USPEX_VARIABLES_NOTUNIQUE
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(10))
                        Case Else
                            sCaracter = Mid(sFormula, lIndex)
                            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(11) & vbCrLf & sCaracter)
                    End Select
                End If
                Set iEq = Nothing
                ValidarFormula = False
                Exit Function
            End If
        Else
            Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(12))
            ValidarFormula = False
            Exit Function
        End If
        Set iEq = Nothing
    Else
        ValidarFormula = False
        Exit Function
    End If
        
    ValidarFormula = True
End Function

Private Sub sdbgDiscreta_Change()
    VisualizarGuardar
    Arrange
End Sub

Private Sub ssTabVar_Click()
    Dim i As Integer
    Dim oGrupo As CFormGrupo
    Dim oPond As CVarPonderacion
    
    'Para forzar un BeforeRowColChange
    If sdbgCampos.Row = 0 Then
       sdbgCampos.MoveNext
    Else
       sdbgCampos.MoveFirst
    End If
    
    Set oGrupo = m_oGrupos.Item(CStr(ssTabVar.SelectedItem.Tag))
    i = 0
    sdbgCampos.RemoveAll
    
    m_bRespetarCarga = True
    
    
    For Each oPond In m_oPonderaciones
       With oPond
            If Not .Grupo Is Nothing Then   'Las condiciones CU y EX no tienen grupo, son generales del certificado y se cargan en el primero
                If .Grupo.Id = oGrupo.Id Then
                    sdbgCampos.AddItem .Campo.Id & Chr(m_lSeparador) & .PondCod & Chr(m_lSeparador) & .Campo.Denominaciones.Item(Idioma).Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & .PondTipo
                End If
            End If
       End With
    Next
    If sdbgCampos.Rows > 0 Then
       sdbgCampos.Row = 0
       CampoSeleccionado
    End If
    m_bRespetarCarga = False
End Sub

Private Sub txtFormula_Change()
    VisualizarGuardar
    Arrange
End Sub

Private Sub txtFormulaVar_Change()
    If m_bRespetarCarga Then Exit Sub
    txtFormulaVar.ForeColor = vbRed
    VisualizarGuardar
    
    Arrange
End Sub

Private Sub txtPuntuacion1_Change()
    VisualizarGuardar
    Arrange
End Sub

Private Sub txtPuntuacion2_Change()
    VisualizarGuardar
    Arrange
End Sub

Private Sub VisualizarGuardar()
    lblCambios.Visible = True
    
    FrmVARCalidad.VisualizarGuardar
    Select Case g_oVariableCalidad.Nivel
        Case 5
            FrmVARCalCompuestaNivel5.lblCambios.Visible = True
            FrmVARCalCompuestaNivel4.lblCambios.Visible = True
            FrmVARCalCompuestaNivel3.lblCambios.Visible = True
        Case 4
            FrmVARCalCompuestaNivel4.lblCambios.Visible = True
            FrmVARCalCompuestaNivel3.lblCambios.Visible = True
        Case 3
            FrmVARCalCompuestaNivel3.lblCambios.Visible = True
    End Select
End Sub



