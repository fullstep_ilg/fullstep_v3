VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Begin VB.Form frmWeb 
   Caption         =   "Form1"
   ClientHeight    =   9630
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   8940
   Icon            =   "frmWeb.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9630
   ScaleWidth      =   8940
   StartUpPosition =   3  'Windows Default
   Begin SHDocVwCtl.WebBrowser WebBrowser 
      Height          =   5175
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   8265
      ExtentX         =   14579
      ExtentY         =   9128
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
End
Attribute VB_Name = "frmWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public UsuarioSummit As CUsuario
Public m_sPage As String
Public m_sProve As String
Public m_sDenProve As String
Public m_sIDVar As String
Public m_sNivel As String
Public m_sUNQA As String
Public m_sSubTipo As String
Public sOrigen As String
Public m_sURL As String
Public RutasFSNWeb As String
Public URLSesSrv As String

Private Sub Form_Activate()
    Dim strSessionId As String
    Dim sURL As String
    
    Select Case UCase(sOrigen)
        Case "FRMCOMPARATIVAQA"
            Me.BorderStyle = 1
            With WebBrowser
                .Top = 0
                .Left = 0
                .Width = Me.Width - 100
                .Height = Me.Height - 500
                ' Se crea el id de sesion de fullstep web
                strSessionId = DevolverSessionIdEstablecido(UsuarioSummit, URLSesSrv)
                sURL = RutasFSNWeb & "/App_Pages/QA/Puntuacion/" & m_sPage
                sURL = sURL & "?desdeGS=1&SessionId=" & strSessionId & "&codProv=" & m_sProve & "&DenProv=" & m_sDenProve & "&Variable=" & m_sIDVar & _
                              "&Nivel=" & m_sNivel & "&Key=" & m_sUNQA & "&SubTipo=" & m_sSubTipo
                .Navigate2 sURL, 4
            End With
        Case Else
            If m_sURL <> "" Then
                Me.Caption = m_sURL
                Me.WindowState = 2
                With WebBrowser
                    .Top = 0
                    .Left = 0
                    .Width = Me.Width - 100
                    .Height = Me.Height - 500
                    .Navigate2 m_sURL, 4
                End With
            End If
    End Select
End Sub

Private Sub Form_Load()
    WebBrowser.Navigate2 "about:blank"
End Sub

Private Sub Form_Resize()
    With WebBrowser
        .Top = 0
        .Left = 0
        If Me.Width > 500 Then
            .Width = Me.Width - 100
        End If
        If Me.Height > 1000 Then
            .Height = Me.Height - 500
        End If
    End With
End Sub


