VERSION 5.00
Begin VB.Form frmDetPersona 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   4290
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3765
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetPersona.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4290
   ScaleWidth      =   3765
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4590
      Left            =   1395
      ScaleHeight     =   4590
      ScaleWidth      =   2400
      TabIndex        =   0
      Top             =   0
      Width           =   2400
      Begin VB.TextBox txtEquipo 
         Height          =   285
         Left            =   60
         TabIndex        =   18
         Top             =   3855
         Width           =   2220
      End
      Begin VB.TextBox txtFax 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   20
         TabIndex        =   8
         Top             =   2910
         Width           =   1740
      End
      Begin VB.TextBox txtTfno 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   20
         TabIndex        =   7
         Top             =   1975
         Width           =   1755
      End
      Begin VB.TextBox txtApel 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   100
         TabIndex        =   6
         Top             =   1055
         Width           =   2220
      End
      Begin VB.TextBox txtNom 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   50
         TabIndex        =   5
         Top             =   595
         Width           =   2220
      End
      Begin VB.TextBox txtCargo 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   50
         TabIndex        =   4
         Top             =   1515
         Width           =   2220
      End
      Begin VB.TextBox txtCod 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         TabIndex        =   3
         Top             =   135
         Width           =   1110
      End
      Begin VB.TextBox txtTfno2 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   20
         TabIndex        =   2
         Top             =   2460
         Width           =   1755
      End
      Begin VB.TextBox txtMail 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   100
         TabIndex        =   1
         Top             =   3375
         Width           =   2220
      End
   End
   Begin VB.Timer timPer 
      Interval        =   60000
      Left            =   3060
      Top             =   2805
   End
   Begin VB.Label lblCargo 
      BackColor       =   &H00808000&
      Caption         =   "Cargo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   105
      TabIndex        =   17
      Top             =   1575
      Width           =   1050
   End
   Begin VB.Label lblNom 
      BackColor       =   &H00808000&
      Caption         =   "Nombre:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   105
      TabIndex        =   16
      Top             =   675
      Width           =   1050
   End
   Begin VB.Label lblApel 
      BackColor       =   &H00808000&
      Caption         =   "Apellidos:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   105
      TabIndex        =   15
      Top             =   1095
      Width           =   1050
   End
   Begin VB.Label lblTfno 
      BackColor       =   &H00808000&
      Caption         =   "Tfno. 1:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   105
      TabIndex        =   14
      Top             =   2055
      Width           =   1050
   End
   Begin VB.Label lblFax 
      BackColor       =   &H00808000&
      Caption         =   "Fax:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   105
      TabIndex        =   13
      Top             =   2955
      Width           =   1050
   End
   Begin VB.Label lblMail 
      BackColor       =   &H00808000&
      Caption         =   "E-mail:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   105
      TabIndex        =   12
      Top             =   3435
      Width           =   1050
   End
   Begin VB.Label lblCod 
      BackColor       =   &H00808000&
      Caption         =   "C�digo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   105
      TabIndex        =   11
      Top             =   195
      Width           =   1050
   End
   Begin VB.Label lblTfno2 
      BackColor       =   &H00808000&
      Caption         =   "Tfno. 2:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   105
      TabIndex        =   10
      Top             =   2475
      Width           =   1050
   End
   Begin VB.Label lblEquipo 
      BackColor       =   &H00808000&
      Caption         =   "Equipo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   9
      Top             =   3915
      Width           =   765
   End
End
Attribute VB_Name = "frmDetPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private sIdiDetalle As String

Public sOrigen As String
Public FSEPConf As Boolean
Public Raiz As CRaiz
Public MDIScaleWidth As Single
Public MDIScaleHeight As Single
Public giLongCodPER As Integer
Public sCodResponsable As String
Public sIdioma As String
Public GestorIdiomas As CGestorIdiomas
Private Sub Form_Activate()
    Caption = sIdiDetalle

    If FSEPConf Then
        txtEquipo.Visible = False
        lblEquipo.Visible = False
        Me.Height = Me.Height - 200
        picDatos.Height = picDatos.Height - 200
    Else
        Me.Height = 4665
    End If

End Sub

Private Sub Form_Load()
    Dim oCompradores As CCompradores
    
    Me.Left = MDIScaleWidth / 2 - Me.Width / 2
    Me.Top = MDIScaleHeight / 2 - Me.Height / 2
        
    txtCod.MaxLength = giLongCodPER
    CargarRecursos
    
    If sOrigen = "frmDetalleProceso" Then
        
        Set oCompradores = Raiz.generar_CCompradores
        oCompradores.CargarTodosLosCompradores sCodResponsable, , , True

        txtCod = oCompradores.Item(1).Cod
        txtNom = NullToStr(oCompradores.Item(1).nombre)
        txtApel = oCompradores.Item(1).Apel
        txtCargo = NullToStr(oCompradores.Item(1).Cargo)
        txtTfno = NullToStr(oCompradores.Item(1).Tfno)
        txtTfno2 = NullToStr(oCompradores.Item(1).Tfno2)
        txtFax = NullToStr(oCompradores.Item(1).Fax)
        txtMail = NullToStr(oCompradores.Item(1).mail)
        
        If Not IsNull((oCompradores.Item(1).CodEqp)) Then
            txtEquipo.Text = oCompradores.Item(1).CodEqp
            If Not IsNull(oCompradores.Item(1).DenEqp) And oCompradores.Item(1).DenEqp <> "" Then
                txtEquipo.Text = txtEquipo.Text & " - " & oCompradores.Item(1).DenEqp
            End If
        End If
        
        Set oCompradores = Nothing
    End If

End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_DET_PERSONA, sIdioma)
    
    If Not Ador Is Nothing Then
        lblCod.Caption = Ador(0).Value
        Ador.MoveNext
        lblNom.Caption = Ador(0).Value
        Ador.MoveNext
        lblApel.Caption = Ador(0).Value
        Ador.MoveNext
        lblCargo.Caption = Ador(0).Value
        Ador.MoveNext
        lblTfno.Caption = Ador(0).Value
        Ador.MoveNext
        lblTfno2.Caption = Ador(0).Value
        Ador.MoveNext
        lblFax.Caption = Ador(0).Value
        Ador.MoveNext
        lblMail.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiDetalle = Ador(0).Value
        Ador.MoveNext
        lblEquipo.Caption = Ador(0).Value
        
        Ador.Close
    
    End If

    Set Ador = Nothing

End Sub






