VERSION 5.00
Begin VB.Form frmDESTFiltrar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Destinos (Filtro)+"
   ClientHeight    =   2580
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5400
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDESTFiltrar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2580
   ScaleWidth      =   5400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   60
      TabIndex        =   8
      Top             =   1140
      Width           =   5235
      Begin VB.CheckBox chkIgualDen 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total+"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3420
         TabIndex        =   3
         Top             =   420
         Width           =   1680
      End
      Begin VB.TextBox txtDEN 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   240
         MaxLength       =   100
         TabIndex        =   2
         Top             =   360
         Width           =   3075
      End
      Begin VB.OptionButton optDEN 
         BackColor       =   &H00808000&
         Caption         =   "Denominaci�n+"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Top             =   -60
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   60
      TabIndex        =   6
      Top             =   120
      Width           =   5235
      Begin VB.CheckBox chkIgualCod 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total+"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   1140
         TabIndex        =   1
         Top             =   420
         Width           =   3570
      End
      Begin VB.TextBox txtCOD 
         Height          =   285
         Left            =   240
         MaxLength       =   3
         TabIndex        =   0
         Top             =   360
         Width           =   795
      End
      Begin VB.OptionButton optCOD 
         BackColor       =   &H00808000&
         Caption         =   "C�digo+"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   150
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   15
         Value           =   -1  'True
         Width           =   1215
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar+"
      Height          =   345
      Left            =   2655
      TabIndex        =   5
      Top             =   2160
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar+"
      Default         =   -1  'True
      Height          =   345
      Left            =   1440
      TabIndex        =   4
      Top             =   2160
      Width           =   1005
   End
End
Attribute VB_Name = "frmDESTFiltrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmDESTFiltrar
''' *** Creacion: 28/12/1998 (Javier Arana)
''' *** Ultima revision: 14/01/1999 (Alfredo Magallon)

Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public LongCodDEST As Integer
Public Idiomas As CIdiomas
Public Filtros As Variant

Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_DESTFILTRAR, Idioma)
    
    If Not Ador Is Nothing Then
        chkIgualCod.Caption = Ador(0).Value
        chkIgualDen.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        frmDESTFiltrar.Caption = Ador(0).Value
        Ador.MoveNext
        optCOD.Caption = Ador(0).Value
        Ador.MoveNext
        optDEN.Caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Load()
    On Error Resume Next
    
    CargarRecursos
    
    txtCOD.MaxLength = LongCodDEST
End Sub

Private Sub optCOD_Click()
    If optCOD.Value Then optDEN.Value = False
    If Me.Visible Then txtCOD.SetFocus
End Sub

Private Sub chkIgualDen_Click()
    If Me.Visible Then txtDEN.SetFocus
End Sub

Private Sub chkIgualCod_Click()
    If Me.Visible Then txtCOD.SetFocus
End Sub

Private Sub optDEN_Click()
    If optDEN.Value Then optCOD.Value = False
    If Me.Visible Then txtDEN.SetFocus
End Sub

Private Sub cmdAceptar_Click()
    ReDim Filtros(0 To 5)
    
    Filtros(0) = optCOD.Value
    Filtros(1) = txtCOD.Text
    Filtros(2) = (chkIgualCod.Value = vbChecked)
    Filtros(3) = optDEN.Value
    Filtros(4) = txtDEN.Text
    Filtros(5) = (chkIgualDen.Value = vbChecked)
    
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    Me.Hide
End Sub

Private Sub Form_Activate()
    If Me.Visible Then txtCOD.SetFocus
End Sub

Private Sub txtCOD_GotFocus()
    optCOD.Value = True
    optDEN.Value = False
End Sub

Private Sub txtDEN_GotFocus()
    optCOD.Value = False
    optDEN.Value = True
End Sub

