VERSION 5.00
Begin VB.Form frmESTRMATDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalle"
   ClientHeight    =   2595
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6360
   Icon            =   "frmESTRMATDetalle.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2595
   ScaleWidth      =   6360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   1200
      ScaleHeight     =   420
      ScaleWidth      =   3105
      TabIndex        =   2
      Top             =   2040
      Width           =   3105
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   510
         TabIndex        =   4
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1650
         TabIndex        =   3
         Top             =   60
         Width           =   1005
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1770
      Left            =   120
      ScaleHeight     =   1770
      ScaleWidth      =   5955
      TabIndex        =   5
      Top             =   240
      Width           =   5955
      Begin VB.TextBox txtCantModificable 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2760
         TabIndex        =   13
         Top             =   1320
         Width           =   1230
      End
      Begin VB.TextBox txtRecepcion 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2760
         TabIndex        =   8
         Top             =   840
         Width           =   3015
      End
      Begin VB.PictureBox PicIntegracion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   5040
         Picture         =   "frmESTRMATDetalle.frx":0CB2
         ScaleHeight     =   300
         ScaleWidth      =   330
         TabIndex        =   6
         Top             =   -45
         Width           =   325
      End
      Begin VB.TextBox txtCod 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2760
         TabIndex        =   0
         Top             =   -30
         Width           =   1230
      End
      Begin VB.TextBox txtDen 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2760
         MaxLength       =   100
         TabIndex        =   1
         Top             =   390
         Width           =   3045
      End
      Begin VB.Label lblCantModificable 
         BackStyle       =   0  'Transparent
         Caption         =   "dCantidad pedida modificable:"
         ForeColor       =   &H8000000E&
         Height          =   285
         Left            =   0
         TabIndex        =   12
         Top             =   1320
         Width           =   2535
      End
      Begin VB.Label lblCod 
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   0
         TabIndex        =   11
         Top             =   0
         Width           =   660
      End
      Begin VB.Label lblDen 
         BackStyle       =   0  'Transparent
         Caption         =   "Denominaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   0
         TabIndex        =   10
         Top             =   420
         Width           =   1125
      End
      Begin VB.Label lblRecepcion 
         BackStyle       =   0  'Transparent
         Caption         =   "dTipo de Recepcion:"
         ForeColor       =   &H8000000E&
         Height          =   285
         Left            =   0
         TabIndex        =   9
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label lblEstInt 
         Height          =   300
         Left            =   5400
         TabIndex        =   7
         Top             =   -45
         Width           =   300
      End
   End
   Begin VB.Timer timDet 
      Interval        =   60000
      Left            =   4920
      Top             =   2160
   End
End
Attribute VB_Name = "frmESTRMATDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public FormCaption As String
Public Cod As String
Public Den As String
Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public TextRecepcion As String
Public EstIntegracion As Variant
Public Accion As AccionesSummit
Public CantEditable As Boolean

Private m_vLongitudesDeCodigos As LongitudesDeCodigos
Private m_oParametrosIntegracion As ParametrosIntegracion
'Multilenguaje
Private sIdiCod As String
Private sIdiDen As String
Private sIdiRecepcion As String
Private sIdiSi As String
Private sIdiNo As String

Public Property Get LongitudesDeCodigos() As LongitudesDeCodigos
    LongitudesDeCodigos = m_vLongitudesDeCodigos
End Property

Public Property Let LongitudesDeCodigos(ByRef vLongitudesDeCodigos As LongitudesDeCodigos)
    m_vLongitudesDeCodigos = vLongitudesDeCodigos
End Property

Public Property Get ParametrosIntegracion() As ParametrosIntegracion
    ParametrosIntegracion = m_oParametrosIntegracion
End Property

Public Property Let ParametrosIntegracion(ByRef vNewValue As ParametrosIntegracion)
    m_oParametrosIntegracion = vNewValue
End Property

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    Select Case Accion
        Case ACCGMN1Eli, ACCGMN2Eli, ACCGMN3Eli, ACCGMN4Eli
            picDatos.Enabled = False
        Case ACCGMN1Mod, ACCGMN2Mod, ACCGMN3Mod, ACCGMN4Mod
            txtCod.Enabled = False
            If Me.Visible Then txtDen.SetFocus
        Case ACCGMN1Det, ACCGMN1Det, ACCGMN3Det, ACCGMN4Det, ACCGMN2Det
            picDatos.Enabled = False
            picEdit.Visible = False
            Height = Height - 400
        Case Else
            If Me.Visible Then txtCod.SetFocus
    End Select
End Sub

Private Sub Form_Load()
    CargarRecursos
    
    If m_oParametrosIntegracion.gaExportar(EntidadIntegracion.Materiales) Then
        If IsNull(EstIntegracion) Or (EstIntegracion = EstadoIntegracion.recibidocorrecto) Then
            lblEstInt.BackColor = RGB(212, 208, 200)
         Else
            lblEstInt.BackColor = RGB(255, 0, 0)
        End If
    End If
        
    Select Case Accion
        Case ACCGMN1Anya
                txtCod.MaxLength = m_vLongitudesDeCodigos.giLongCodGMN1
        Case ACCGMN2Anya
                txtCod.MaxLength = m_vLongitudesDeCodigos.giLongCodGMN2
        Case ACCGMN3Anya
                txtCod.MaxLength = m_vLongitudesDeCodigos.giLongCodGMN3
        Case ACCGMN4Anya
                txtCod.MaxLength = m_vLongitudesDeCodigos.giLongCodGMN4
    End Select
    
    If m_oParametrosIntegracion.gaExportar(EntidadIntegracion.Materiales) Then
        PicIntegracion.Visible = True
        lblEstInt.Visible = True
    Else
        PicIntegracion.Visible = False
        lblEstInt.Visible = False
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Accion = ACCGMCon
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
        
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_ESTRMAT_DETALLE, Idioma)
    If Not Ador Is Nothing Then
        Caption = Ador(0).Value
        Ador.MoveNext
        lblCod.Caption = Ador(0).Value
        Ador.MoveNext
        lblDen.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiCod = Ador(0).Value
        Ador.MoveNext
        sIdiDen = Ador(0).Value
        Ador.MoveNext
        lblRecepcion.Caption = Ador(0).Value
        Ador.MoveNext
        lblCantModificable.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        sIdiSi = Ador(0).Value
        Ador.MoveNext
        sIdiNo = Ador(0).Value
        
        Ador.Close
    End If

    Me.Caption = FormCaption
    txtCod.Text = Cod
    txtDen.Text = Den
    txtRecepcion.Text = TextRecepcion
    txtCantModificable.Text = IIf(CantEditable, sIdiSi, sIdiNo)

    Set Ador = Nothing
End Sub
