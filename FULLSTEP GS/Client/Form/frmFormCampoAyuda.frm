VERSION 5.00
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Begin VB.Form frmFormCampoAyuda 
   BackColor       =   &H00808000&
   Caption         =   "DTexto de ayuda..."
   ClientHeight    =   3870
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7545
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormCampoAyuda.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3870
   ScaleWidth      =   7545
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   380
      Left            =   2280
      ScaleHeight     =   375
      ScaleWidth      =   2535
      TabIndex        =   2
      Top             =   3480
      Width           =   2535
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   1200
         TabIndex        =   4
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   0
         TabIndex        =   3
         Top             =   0
         Width           =   1005
      End
   End
   Begin VB.TextBox txtAyuda 
      Height          =   3315
      Left            =   120
      MaxLength       =   2000
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   7350
   End
   Begin UltraGrid.SSUltraGrid ssAyuda 
      Height          =   3315
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   7350
      _ExtentX        =   12965
      _ExtentY        =   5847
      _Version        =   131072
      GridFlags       =   17040384
      LayoutFlags     =   68157444
      ScrollBars      =   2
      Override        =   "frmFormCampoAyuda.frx":0CB2
      Caption         =   "ssAyuda"
   End
End
Attribute VB_Name = "frmFormCampoAyuda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Idiomas As CIdiomas
Public CampoSeleccionado As CFormItem
Public CampoPredef As CCampoPredef
Public Raiz As CRaiz
Public Mensajes As CMensajes
Public GestorSeguridad As CGestorSeguridad
Public UsuCod As String
Public TipoAyuda As FormCampoAyudaTipoAyuda
Public Modif As Boolean

'Variables de idiomas
Private m_sIdiForm As String
Private m_sIdiCampo As String
Private m_sIdioma As String
Private m_sIdiAyuda As String
Private m_TesError As TipoErrorSummit
Private m_oParametrosGenerales As ParametrosGenerales

Public Property Get TesError() As TipoErrorSummit
    TesError = m_TesError
End Property

Public Property Let TesError(ByRef vNewValue As TipoErrorSummit)
    m_TesError = vNewValue
End Property

Public Property Get ParametrosGenerales() As ParametrosGenerales
    ParametrosGenerales = m_oParametrosGenerales
End Property

Public Property Let ParametrosGenerales(ByRef vNewValue As ParametrosGenerales)
    m_oParametrosGenerales = vNewValue
End Property

Private Sub cmdAceptar_Click()
    Dim oIdioma As CIdioma
    Dim oAyudas As CMultiidiomas
    Dim oRow As SSRow

    Screen.MousePointer = vbHourglass
    
    Set oAyudas = Raiz.Generar_CMultiidiomas
    
    If txtAyuda.Visible = True Then
        For Each oIdioma In Idiomas
            oAyudas.Add oIdioma.Cod, Trim(txtAyuda.Text)
        Next
        Set oIdioma = Nothing
    Else
        ssAyuda.ActiveRow = ssAyuda.GetRow(ssChildRowFirst)
        
        Set oRow = ssAyuda.ActiveRow
        oAyudas.Add oRow.Cells("COD_IDI").Value, NullToStr(Trim(oRow.Cells("AYUDA").Value))
        While oRow.HasNextSibling
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
            oAyudas.Add oRow.Cells("COD_IDI").Value, NullToStr(Trim(oRow.Cells("AYUDA").Value))
        Wend
    End If
        
    If Not CampoSeleccionado Is Nothing Then
        'Guarda en BD la ayuda introducida para el campo:
        Set CampoSeleccionado.Ayudas = oAyudas
        TesError = CampoSeleccionado.ModificarAyuda
        
        Screen.MousePointer = vbNormal
        
        Set oAyudas = Nothing
        Set oRow = Nothing
            
        If TesError.NumError <> TESnoerror Then
            TratarError TesError, Mensajes, m_oParametrosGenerales
            Exit Sub
        Else
            RegistrarAccion ACCFormItemModif, "Id:" & CampoSeleccionado.Id, UsuCod, m_oParametrosGenerales.gbACTIVLOG, GestorSeguridad
            Unload Me
        End If
    Else
        'Es un campo predefinido:
        Set CampoPredef.Ayudas = oAyudas
        TesError = CampoPredef.ModificarAyuda
        
        Screen.MousePointer = vbNormal
        
        Set oAyudas = Nothing
        Set oRow = Nothing
            
        If TesError.NumError <> TESnoerror Then
            TratarError TesError, Mensajes, m_oParametrosGenerales
            Exit Sub
        Else
            RegistrarAccion ACCTipoSolicitudItemModif, "Id:" & CampoPredef.Id, UsuCod, m_oParametrosGenerales.gbACTIVLOG, GestorSeguridad
            Unload Me
        End If
    End If
End Sub

Private Sub cmdCancelar_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.Height = 4350
    Me.Width = 7635

    CargarRecursos
    
    If Not Modif Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        txtAyuda.Locked = True
    End If
    
    If Not CampoSeleccionado Is Nothing Then
        'If g_sOrigen = "frmSolicitudDetalle" Or g_sOrigen = "frmDesgloseInstancia" Or g_sOrigen = "frmVarCalPondCert" Then
        If TipoAyuda = CampoInstancia Then
            'Es la ayuda de un campo de instancia
            ssAyuda.Visible = False
            txtAyuda.Visible = True
            If Not CampoSeleccionado.Ayudas Is Nothing Then
                If Not CampoSeleccionado.Ayudas.Item(Idioma) Is Nothing Then
                    txtAyuda.Text = NullToStr(CampoSeleccionado.Ayudas.Item(Idioma).Den)
                End If
            End If
            Me.Caption = Me.Caption & " " & m_sIdiCampo & " " & CampoSeleccionado.Denominaciones.Item(CStr(Idioma)).Den
        Else
            'Es la ayuda de un campo del formulario
            If CampoSeleccionado.Grupo.Formulario.Multiidioma Then
                ssAyuda.Visible = True
                txtAyuda.Visible = False
                MostrarAyuda
            Else
                ssAyuda.Visible = False
                txtAyuda.Visible = True
                If Not CampoSeleccionado.Ayudas Is Nothing Then
                    If Not CampoSeleccionado.Ayudas.Item(Idioma) Is Nothing Then
                        txtAyuda.Text = NullToStr(CampoSeleccionado.Ayudas.Item(Idioma).Den)
                    End If
                End If
            End If
            Me.Caption = Me.Caption & " " & m_sIdiForm & " " & CampoSeleccionado.Grupo.Formulario.Den & " " & m_sIdiCampo & " " & CampoSeleccionado.Denominaciones.Item(CStr(Idioma)).Den
        End If
        
    Else
        'Es la ayuda de un campo predefinido
        'If g_sOrigen = "frmPARTipoSolicit" Then
        If TipoAyuda = CampoPredefinido Then
            ssAyuda.Visible = True
            txtAyuda.Visible = False
            MostrarAyuda
        Else
            ssAyuda.Visible = False
            txtAyuda.Visible = True
            If Not CampoPredef.Ayudas Is Nothing Then
                If Not CampoPredef.Ayudas.Item(Idioma) Is Nothing Then
                    txtAyuda.Text = NullToStr(CampoPredef.Ayudas.Item(Idioma).Den)
                End If
            End If
        End If
        
        Me.Caption = Me.Caption & " " & m_sIdiCampo & " " & CampoPredef.Denominaciones.Item(CStr(Idioma)).Den
        
    End If
End Sub

Private Sub Form_Resize()
    If Me.Height < 1100 Then Exit Sub
    If Me.Width < 600 Then Exit Sub
    
    If Modif Then
        txtAyuda.Height = Me.Height - 1065
    Else
        txtAyuda.Height = Me.Height - 1065 + picEdit.Height
    End If
    txtAyuda.Width = Me.Width - 315
    
    ssAyuda.Height = txtAyuda.Height
    ssAyuda.Width = txtAyuda.Width
    
    If ssAyuda.Bands.Count > 0 Then
        ssAyuda.Bands(0).Columns("IDIOMA").Width = ssAyuda.Width * 0.17
        ssAyuda.Bands(0).Columns("AYUDA").Width = ssAyuda.Width * 0.75
    End If
    
    picEdit.Top = txtAyuda.Top + txtAyuda.Height + 85
    picEdit.Left = Me.Width / 3
End Sub

Private Sub Form_Unload(Cancel As Integer)
    DoEvents
    Set CampoSeleccionado = Nothing
    Set CampoPredef = Nothing
    Set Idiomas = Nothing
    
    DoEvents
    Set ssAyuda.DataSource = Nothing
    DoEvents
    
    Me.Visible = False
End Sub

Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_FORMCAMPO_AYUDA, Idioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value  '1 Texto de ayuda /
        Ador.MoveNext
        m_sIdiForm = Ador(0).Value & ":"  '2 Formulario
        Ador.MoveNext
        m_sIdiCampo = Ador(0).Value & ":"  '3 Campo
        Ador.MoveNext
        m_sIdioma = Ador(0).Value  '4 Idioma
        Ador.MoveNext
        m_sIdiAyuda = Ador(0).Value   '5 Texto de ayuda
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value   '6 &Aceptar
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value  '7 &Cancelar
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub ssAyuda_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
    If ssAyuda.Visible = False Then Exit Sub
    
    ssAyuda.Bands(0).Columns("COD_IDI").Hidden = True
    
    With Layout.Appearances.Add("Gris")
        .BackColor = RGB(223, 223, 223)
    End With
    ssAyuda.Bands(0).Columns("IDIOMA").CellAppearance = "Gris"
    
    ssAyuda.Bands(0).Columns("IDIOMA").Activation = ssActivationActivateNoEdit
    If Modif Then
        ssAyuda.Bands(0).Columns("AYUDA").Activation = ssActivationAllowEdit
    Else
        ssAyuda.Bands(0).Columns("AYUDA").Activation = ssActivationActivateNoEdit
    End If
    
    ssAyuda.Override.CellMultiLine = ssCellMultiLineTrue
    ssAyuda.Override.RowSizing = ssRowSizingAutoFree
    ssAyuda.Override.RowSizingAutoMaxLines = 0
    ssAyuda.Bands(0).Columns("AYUDA").AutoSizeEdit = ssAutoSizeEditTrue
     
    ssAyuda.Bands(0).Columns("IDIOMA").Header.Caption = m_sIdioma
    ssAyuda.Bands(0).Columns("AYUDA").Header.Caption = m_sIdiAyuda
    
    ssAyuda.ActiveRow = ssAyuda.GetRow(ssChildRowFirst)
        
End Sub

'**************************************************************************************
'*** Descripción: Muestra en el control ultragrid las ayudas de los campos de un    ***
'***              formulario                                                        ***
'**************************************************************************************

Private Sub MostrarAyuda()
    Dim Ador As Adodb.Recordset
    Dim oIdioma As CIdioma
    Dim stm  As Adodb.Stream
    Dim RS As Adodb.Recordset
    Dim bMostrar As Boolean
    
    Set Ador = New Adodb.Recordset

    Ador.Fields.Append "COD_IDI", adVarChar, 200
    Ador.Fields.Append "IDIOMA", adVarChar, 200
    Ador.Fields.Append "AYUDA", adVarChar, 2000, adFldMayBeNull

    Ador.Open


    If Not CampoSeleccionado Is Nothing Then
        bMostrar = True
    Else
        'Es un campo predefinido:
        'If g_sOrigen = "frmPARTipoSolicit" Then
        If TipoAyuda = CampoPredefinido Then
            bMostrar = True
        End If
    End If
    
    If bMostrar = True Then
        For Each oIdioma In Idiomas
            Ador.AddNew
            Ador("COD_IDI").Value = oIdioma.Cod
            Ador("IDIOMA").Value = oIdioma.Den
            If Not CampoSeleccionado Is Nothing Then
                If Not CampoSeleccionado.Ayudas Is Nothing Then
                    If CampoSeleccionado.Ayudas.Count > 0 Then
                        If Not CampoSeleccionado.Ayudas.Item(CStr(oIdioma.Cod)) Is Nothing Then
                            Ador("AYUDA").Value = NullToStr(CampoSeleccionado.Ayudas.Item(CStr(oIdioma.Cod)).Den)
                        Else
                            Ador("AYUDA").Value = ""
                        End If
                    Else
                        Ador("AYUDA").Value = ""
                    End If
                Else
                    Ador("AYUDA").Value = ""
                End If
            Else
                'Es un campo predefinido
                If Not CampoPredef.Ayudas Is Nothing Then
                    If CampoPredef.Ayudas.Count > 0 Then
                        If Not CampoPredef.Ayudas.Item(CStr(oIdioma.Cod)) Is Nothing Then
                            Ador("AYUDA").Value = NullToStr(CampoPredef.Ayudas.Item(CStr(oIdioma.Cod)).Den)
                        Else
                            Ador("AYUDA").Value = ""
                        End If
                    Else
                        Ador("AYUDA").Value = ""
                    End If
                Else
                    Ador("AYUDA").Value = ""
                End If
            End If
        Next
    End If

    Set stm = New Adodb.Stream

    Ador.Save stm, adPersistXML
    Ador.Close
    Set Ador = Nothing
    
    Set RS = New Adodb.Recordset
    RS.Open stm
    RS.ActiveConnection = Nothing
    
    stm.Close
    Set stm = Nothing
    Set ssAyuda.DataSource = RS
    Set RS = Nothing
End Sub

