VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{1E673DEF-C82F-4FBB-BAB9-77B0634E8F4D}#1.0#0"; "ProceSelector.ocx"
Begin VB.Form frmCopiarGrupo 
   BackColor       =   &H00808000&
   Caption         =   "DCopiar grupo"
   ClientHeight    =   2940
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   11325
   Icon            =   "frmCopiarGrupo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2940
   ScaleWidth      =   11325
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picNuevoCod 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   120
      ScaleHeight     =   615
      ScaleWidth      =   8175
      TabIndex        =   16
      Top             =   1560
      Visible         =   0   'False
      Width           =   8175
      Begin VB.TextBox txtNuevoCod 
         Height          =   285
         Left            =   0
         TabIndex        =   9
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label lblCodExistente 
         BackColor       =   &H00808000&
         Caption         =   "DEl c�digo del grupo ya existe en el proceso, indique otro c�digo para el grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   300
         Left            =   0
         TabIndex        =   17
         Top             =   0
         Width           =   8025
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   4440
      TabIndex        =   10
      Top             =   2520
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   5640
      TabIndex        =   11
      Top             =   2520
      Width           =   1005
   End
   Begin VB.Frame fraSelProce 
      BackColor       =   &H00808000&
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11055
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   10605
         Picture         =   "frmCopiarGrupo.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   2145
         TabIndex        =   2
         Top             =   240
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   555
         TabIndex        =   1
         Top             =   240
         Width           =   960
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1693
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
         Height          =   285
         Left            =   4380
         TabIndex        =   4
         Top             =   240
         Width           =   1305
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1958
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8599
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "INVI"
         Columns(2).Name =   "INVI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   2302
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SelectorDeProcesos.ProceSelector ProceSelector 
         Height          =   315
         Left            =   3240
         TabIndex        =   3
         Top             =   240
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
         Height          =   285
         Left            =   5685
         TabIndex        =   5
         Top             =   240
         Width           =   4860
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   7858
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1826
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "INVI"
         Columns(2).Name =   "INVI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8572
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblProceso 
         BackColor       =   &H00808000&
         Caption         =   "DProceso:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   180
         Left            =   3630
         TabIndex        =   14
         Top             =   285
         Width           =   705
      End
      Begin VB.Label lblAnyo 
         BackColor       =   &H00808000&
         Caption         =   "DA�o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   120
         TabIndex        =   13
         Top             =   285
         Width           =   435
      End
      Begin VB.Label lblGMN1 
         BackColor       =   &H00808000&
         Caption         =   "DCmd:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   1650
         TabIndex        =   12
         Top             =   285
         Width           =   450
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcGrupoCod 
      Height          =   285
      Left            =   2790
      TabIndex        =   7
      Top             =   1080
      Width           =   1305
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1958
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   8599
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   2302
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcGrupoDen 
      Height          =   285
      Left            =   4095
      TabIndex        =   8
      Top             =   1080
      Width           =   4860
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   7858
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1826
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "INVI"
      Columns(2).Name =   "INVI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   8572
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblSelGrupo 
      BackColor       =   &H00808000&
      Caption         =   "DSeleccione el grupo a copiar:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   120
      TabIndex        =   15
      Top             =   1125
      Width           =   2625
   End
End
Attribute VB_Name = "frmCopiarGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public CargaMaximaCombos As Integer
Public MaxProceCod As Long
Public Raiz As cRaiz
Public Mensajes As CMensajes
Public LongCodGMN1 As Integer
Public AbrGMN1 As String
Public FrmProceBusq As Object
Public ProcesoDestino As cProceso
Public CopiarOk As Boolean
Public LongCodGrupo As Integer

Private m_DatosUsu As DatosUsuario
Private m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_bRespetarCombo As Boolean
Private m_bCargarComboDesde As Boolean
Private m_ParamGen As ParametrosGenerales
Private m_arIdioma(2) As String
Private m_oProceso As cProceso
Private m_bCopiarDestino As Boolean
Private m_bCopiarFormaPago As Boolean
Private m_bCopiarFechasSum As Boolean
Private m_bCopiarProveAct As Boolean
Private m_bCopiarDistribucion As Boolean
Private m_bCopiarPres1 As Boolean
Private m_bCopiarPres2 As Boolean
Private m_bCopiarPres3 As Boolean
Private m_bCopiarPres4 As Boolean
Private m_bCopiarEspProce As Boolean
Private m_bCopiarEspGrupo As Boolean
Private m_bCopiarEspItem As Boolean
Private m_bCambioAmbitoDatos As Boolean

Public Property Get CopiarDestino() As Boolean
    CopiarDestino = m_bCopiarDestino
End Property

Public Property Get CopiarFormaPago() As Boolean
    CopiarFormaPago = m_bCopiarFormaPago
End Property

Public Property Get CopiarFechasSum() As Boolean
    CopiarFechasSum = m_bCopiarFechasSum
End Property

Public Property Get CopiarProveAct() As Boolean
    CopiarProveAct = m_bCopiarProveAct
End Property

Public Property Get CopiarDistribucion() As Boolean
    CopiarDistribucion = m_bCopiarDistribucion
End Property

Public Property Get CopiarPres1() As Boolean
    CopiarPres1 = m_bCopiarPres1
End Property

Public Property Get CopiarPres2() As Boolean
    CopiarPres2 = m_bCopiarPres2
End Property

Public Property Get CopiarPres3() As Boolean
    CopiarPres3 = m_bCopiarPres3
End Property

Public Property Get CopiarPres4() As Boolean
    CopiarPres4 = m_bCopiarPres4
End Property

Public Property Get CopiarEspProce() As Boolean
    CopiarEspProce = m_bCopiarEspProce
End Property

Public Property Get CopiarEspGrupo() As Boolean
    CopiarEspGrupo = m_bCopiarEspGrupo
End Property

Public Property Get CopiarEspItem() As Boolean
    CopiarEspItem = m_bCopiarEspItem
End Property

Public Property Get CambioAmbitoDatos() As Boolean
    CambioAmbitoDatos = m_bCambioAmbitoDatos
End Property

Public Property Get DatosUsu() As DatosUsuario
    DatosUsu = m_DatosUsu
End Property

Public Property Let DatosUsu(ByRef DatosUsu As DatosUsuario)
    m_DatosUsu = DatosUsu
End Property

Public Property Get ParamGen() As ParametrosGenerales
    ParamGen = m_ParamGen
End Property

Public Property Let ParamGen(ByRef vParametrosGenerales As ParametrosGenerales)
    m_ParamGen = vParametrosGenerales
End Property

Public Property Get Anyo() As Integer
    Anyo = m_oProceso.Anyo
End Property

Public Property Get GMN1() As String
    GMN1 = m_oProceso.GMN1Cod
End Property

Public Property Get ProceCod() As Long
    ProceCod = m_oProceso.Cod
End Property

Public Property Get GrupoId() As Long
    GrupoId = CLng(sdbcGrupoCod.Columns(2).Value)
End Property

Public Property Get GrupoNuevoCod() As String
    GrupoNuevoCod = txtNuevoCod.Text
End Property

Private Sub cmdAceptar_Click()
    If ValidarGrupo Then
        CopiarOk = True
        Me.Hide
    End If
End Sub

''' <summary>Valida la selecci�n</summary>
''' <remarks>Llamada desde: cmdAceptar_Click</remarks>

Private Function ValidarGrupo() As Boolean
    Dim bOk As Boolean
    Dim bEncontrado As Boolean
    Dim bMismoProceso As Boolean
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
    
    'Comprobar el c�digo del grupo
    If sdbcGrupoCod.Text <> "" Then
        Dim oGrupo As CGrupo
        For Each oGrupo In ProcesoDestino.Grupos
            If UCase(oGrupo.Codigo) = UCase(sdbcGrupoCod.Text) Then
                bEncontrado = True
                Exit For
            End If
        Next
        Set oGrupo = Nothing
        
        PedirCodigoNuevoGrupo bEncontrado
        If bEncontrado Then
            If txtNuevoCod.Text <> "" Then
                If Not NombreDeGrupoValido(txtNuevoCod.Text) Then
                    Mensajes.CodigoGrupoNoValido m_arIdioma(0)
                Else
                    'Comprobar que el nuevo c�digo no existe ya en el proceso destino
                    bEncontrado = False
                    For Each oGrupo In ProcesoDestino.Grupos
                        If UCase(oGrupo.Codigo) = UCase(txtNuevoCod.Text) Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    
                    If bEncontrado Then
                        Mensajes.NoValido m_arIdioma(0)
                    Else
                        bOk = True
                    End If
                End If
            End If
        Else
            bOk = True
        End If
    Else
        PedirCodigoNuevoGrupo False
        bOk = False
    End If
        
    If bOk Then
        bMismoProceso = (m_oProceso.Anyo = ProcesoDestino.Anyo And m_oProceso.GMN1Cod = ProcesoDestino.GMN1Cod And m_oProceso.Cod = ProcesoDestino.Cod)
        
        'Comprobar el �mbito de datos
        Dim bDestProcAItem, bFPagoProcAItem, bFSumProcAItem, bProvActProcAItem, bPres1ProcAItem, bPres2ProcAItem, bPres3ProcAItem, bPres4ProcAItem As Boolean
        Dim bDestGrupoAItem, bFPagoGrupoAItem, bFSumGrupoAItem, bProvActGrupoAItem, bDistGrupoAItem, bPres1GrupoAItem, bPres2GrupoAItem, bPres3GrupoAItem, bPres4GrupoAItem As Boolean
        Dim bDestGrupo, bFPagoGrupo, bFSumGrupo, bProvActGrupo, bPres1Grupo, bPres2Grupo, bPres3Grupo, bPres4Grupo As Boolean
        Dim bEspProc, bEspGrupo, bEspItem As Boolean
        Dim bComprobarDest, bComprobarFPago, bComprobarFSum, bComprobarProvAct, bComprobarPres1, bComprobarPres2, bComprobarPres3, bComprobarPres4, bComprobarDist As Boolean
        
        m_oProceso.CargarDatosGeneralesProceso
                
        'Distribuci�n UO (Obligatorio)
        bComprobarDist = (Not bMismoProceso And ProcesoDestino.DefDistribUON = EnProceso)
        bDistGrupoAItem = (m_oProceso.DefDistribUON = EnItem And ProcesoDestino.DefDistribUON = EnGrupo)
        'Destino
        bComprobarDest = (Not bMismoProceso And ProcesoDestino.DefDestino = EnProceso)
        bDestProcAItem = (m_oProceso.DefDestino = EnItem And ProcesoDestino.DefDestino = EnProceso)
        bDestGrupoAItem = (m_oProceso.DefDestino = EnItem And ProcesoDestino.DefDestino = EnGrupo)
        bDestGrupo = (Not bMismoProceso And m_oProceso.DefDestino <> EnItem And ProcesoDestino.DefDestino = EnProceso)
        'Forma de pago
        bComprobarFPago = (Not bMismoProceso And ProcesoDestino.DefFormaPago = EnProceso)
        bFPagoProcAItem = (m_oProceso.DefFormaPago = EnItem And ProcesoDestino.DefFormaPago = EnProceso)
        bFPagoGrupoAItem = (m_oProceso.DefFormaPago = EnItem And ProcesoDestino.DefFormaPago = EnGrupo)
        bFPagoGrupo = (Not bMismoProceso And m_oProceso.DefFormaPago <> EnItem And ProcesoDestino.DefFormaPago = EnProceso)
        'Fechas suministro
        bComprobarFSum = (Not bMismoProceso And ProcesoDestino.DefFechasSum = EnProceso)
        bFSumProcAItem = (m_oProceso.DefFechasSum = EnItem And ProcesoDestino.DefFechasSum = EnProceso)
        bFSumGrupoAItem = (m_oProceso.DefFechasSum = EnItem And ProcesoDestino.DefFechasSum = EnGrupo)
        bFSumGrupo = (Not bMismoProceso And m_oProceso.DefFechasSum <> EnItem And ProcesoDestino.DefFechasSum = EnProceso)
        'Proveedor actual
        bComprobarProvAct = (Not bMismoProceso And ProcesoDestino.DefProveActual = EnProceso)
        bProvActProcAItem = (Not ProcesoDestino.AdminPublica And (m_oProceso.DefProveActual = EnItem And ProcesoDestino.DefProveActual = EnProceso))
        bProvActGrupoAItem = (Not ProcesoDestino.AdminPublica And (m_oProceso.DefProveActual = EnItem And ProcesoDestino.DefProveActual = EnGrupo))
        bProvActGrupo = (Not bMismoProceso And Not ProcesoDestino.AdminPublica And (m_oProceso.DefProveActual <> EnItem And ProcesoDestino.DefProveActual = EnProceso))
        'Presupuestos
        bComprobarPres1 = (m_ParamGen.gbUsarPres1 And Not bMismoProceso And ProcesoDestino.DefPresAnualTipo1 = EnProceso)
        bPres1ProcAItem = (m_ParamGen.gbUsarPres1 And (m_oProceso.DefPresAnualTipo1 = EnItem And (ProcesoDestino.DefPresAnualTipo1 = EnProceso)))
        bPres1GrupoAItem = (m_ParamGen.gbUsarPres1 And (m_oProceso.DefPresAnualTipo1 = EnItem And (ProcesoDestino.DefPresAnualTipo1 = EnGrupo)))
        bPres1Grupo = (Not bMismoProceso And m_ParamGen.gbUsarPres1 And (m_oProceso.DefPresAnualTipo1 <> EnItem And ProcesoDestino.DefPresAnualTipo1 = EnProceso))
        bComprobarPres2 = (m_ParamGen.gbUsarPres2 And Not bMismoProceso And ProcesoDestino.DefPresAnualTipo2 = EnProceso)
        bPres2ProcAItem = (m_ParamGen.gbUsarPres2 And (m_oProceso.DefPresAnualTipo2 = EnItem And (ProcesoDestino.DefPresAnualTipo2 = EnProceso)))
        bPres2GrupoAItem = (m_ParamGen.gbUsarPres2 And (m_oProceso.DefPresAnualTipo2 = EnItem And (ProcesoDestino.DefPresAnualTipo2 = EnGrupo)))
        bPres2Grupo = (Not bMismoProceso And m_ParamGen.gbUsarPres2 And (m_oProceso.DefPresAnualTipo2 <> EnItem And ProcesoDestino.DefPresAnualTipo2 = EnProceso))
        bComprobarPres3 = (m_ParamGen.gbUsarPres3 And Not bMismoProceso And ProcesoDestino.DefPresTipo1 = EnProceso)
        bPres3ProcAItem = (m_ParamGen.gbUsarPres3 And (m_oProceso.DefPresTipo1 = EnItem And (ProcesoDestino.DefPresTipo1 = EnProceso)))
        bPres3GrupoAItem = (m_ParamGen.gbUsarPres3 And (m_oProceso.DefPresTipo1 = EnItem And (ProcesoDestino.DefPresTipo1 = EnGrupo)))
        bPres3Grupo = (Not bMismoProceso And m_ParamGen.gbUsarPres3 And (m_oProceso.DefPresTipo1 <> EnItem And ProcesoDestino.DefPresTipo1 = EnProceso))
        bComprobarPres4 = (m_ParamGen.gbUsarPres4 And Not bMismoProceso And ProcesoDestino.DefPresTipo2 = EnProceso)
        bPres4ProcAItem = (m_ParamGen.gbUsarPres4 And (m_oProceso.DefPresTipo2 = EnItem And (ProcesoDestino.DefPresTipo2 = EnProceso)))
        bPres4GrupoAItem = (m_ParamGen.gbUsarPres4 And (m_oProceso.DefPresTipo2 = EnItem And (ProcesoDestino.DefPresTipo2 = EnGrupo)))
        bPres4Grupo = (Not bMismoProceso And m_ParamGen.gbUsarPres4 And (m_oProceso.DefPresTipo2 <> EnItem And ProcesoDestino.DefPresTipo2 = EnProceso))
        'Especificaciones
        If m_oProceso.DefEspecificaciones Then bEspProc = m_oProceso.TieneEspecificaciones
        If m_oProceso.DefEspGrupos Then bEspGrupo = m_oProceso.Grupos.Item(sdbcGrupoCod.Text).TieneEspecificaciones
        If m_oProceso.DefEspItems Then bEspItem = m_oProceso.Grupos.Item(sdbcGrupoCod.Text).ItemsTienenEspecificaciones
                      
        
        'Comprobaci�n obligatoria: Si proceso origen y destino no son el mismo y la distribuci�n en destino es a nivel de proceso la distribuci�n debe coincidir
        If bComprobarDist Then
            bOk = ProcesoDestino.ComprobarDistribucion(m_oProceso, m_oProceso.Grupos.Item(sdbcGrupoCod.Text).Id)
            If Not bOk Then Mensajes.DistribucionProcesosNoCoincidentes
        End If
        
        If bOk Then
            m_bCopiarDestino = True
            m_bCopiarFormaPago = True
            m_bCopiarFechasSum = True
            m_bCopiarProveAct = (ProcesoDestino.DefProveActual <> NoDefinido)
            m_bCopiarPres1 = (m_ParamGen.gbUsarPres1 And ProcesoDestino.DefPresAnualTipo1 <> NoDefinido)
            m_bCopiarPres2 = (m_ParamGen.gbUsarPres2 And ProcesoDestino.DefPresAnualTipo2 <> NoDefinido)
            m_bCopiarPres3 = (m_ParamGen.gbUsarPres3 And ProcesoDestino.DefPresTipo1 <> NoDefinido)
            m_bCopiarPres4 = (m_ParamGen.gbUsarPres4 And ProcesoDestino.DefPresTipo2 <> NoDefinido)
            'Otras comprobaciones (si el dato es el mismo en el origen y en el destino no se hace ning�n traslado)
            If (bComprobarDest Or bComprobarFPago Or bComprobarFSum Or bComprobarProvAct Or bComprobarPres1 Or bComprobarPres2 Or bComprobarPres3 Or bComprobarPres4) Then
                'Cargar los items del grupo origen si es necesario
                If (m_oProceso.DefDestino = TipoDefinicionDatoProceso.EnItem Or m_oProceso.DefFormaPago = TipoDefinicionDatoProceso.EnItem Or _
                    m_oProceso.DefFechasSum = TipoDefinicionDatoProceso.EnItem Or m_oProceso.DefProveActual = TipoDefinicionDatoProceso.EnItem Or _
                    m_oProceso.DefPresAnualTipo1 = TipoDefinicionDatoProceso.EnItem Or m_oProceso.DefPresAnualTipo2 = TipoDefinicionDatoProceso.EnItem Or _
                    m_oProceso.DefPresTipo1 = TipoDefinicionDatoProceso.EnItem Or m_oProceso.DefPresTipo2 = TipoDefinicionDatoProceso.EnItem) Then
                    m_oProceso.Grupos.Item(sdbcGrupoCod.Text).CargarTodosLosItems OrdItemPorCodArt
                End If
                
                Dim bCheck As Boolean
                If bComprobarDest Then
                    bCheck = ProcesoDestino.ComprobarDestino(m_oProceso, sdbcGrupoCod.Text)
                    bDestGrupo = (bDestGrupo And Not bCheck)
                    bDestProcAItem = (bDestProcAItem And Not bCheck)
                    m_bCopiarDestino = False
                End If
                If bComprobarFPago Then
                    bCheck = ProcesoDestino.ComprobarFormaPago(m_oProceso, sdbcGrupoCod.Text)
                    bFPagoGrupo = (bFPagoGrupo And Not bCheck)
                    bFPagoProcAItem = (bFPagoProcAItem And Not bCheck)
                    m_bCopiarFormaPago = False
                End If
                If bComprobarFSum Then
                    bCheck = ProcesoDestino.ComprobarFechasSuministro(m_oProceso, sdbcGrupoCod.Text)
                    bFSumGrupo = (bFSumGrupo And Not bCheck)
                    bFSumProcAItem = (bFSumProcAItem And Not bCheck)
                    m_bCopiarFechasSum = False
                End If
                If bComprobarProvAct Then
                    bCheck = ProcesoDestino.ComprobarProveedorActual(m_oProceso, sdbcGrupoCod.Text)
                    bProvActGrupo = (bProvActGrupo And Not bCheck)
                    bProvActProcAItem = (bProvActProcAItem And Not bCheck)
                    m_bCopiarProveAct = False
                End If
                If bComprobarPres1 Then
                    bCheck = ProcesoDestino.ComprobarValoresPresupuestos(1, m_oProceso, sdbcGrupoCod.Text)
                    bPres1Grupo = (bPres1Grupo And Not bCheck)
                    bPres1ProcAItem = (bPres1ProcAItem And Not bCheck)
                    m_bCopiarPres1 = False
                End If
                If bComprobarPres2 Then
                    bCheck = ProcesoDestino.ComprobarValoresPresupuestos(2, m_oProceso, sdbcGrupoCod.Text)
                    bPres2Grupo = (bPres2Grupo And Not bCheck)
                    bPres2ProcAItem = (bPres2ProcAItem And Not bCheck)
                    m_bCopiarPres2 = False
                End If
                If bComprobarPres3 Then
                    bCheck = ProcesoDestino.ComprobarValoresPresupuestos(3, m_oProceso, sdbcGrupoCod.Text)
                    bPres3Grupo = (bPres3Grupo And Not bCheck)
                    bPres3ProcAItem = (bPres3ProcAItem And Not bCheck)
                    m_bCopiarPres3 = False
                End If
                If bComprobarPres4 Then
                    bCheck = ProcesoDestino.ComprobarValoresPresupuestos(4, m_oProceso, sdbcGrupoCod.Text)
                    bPres4Grupo = (bPres4Grupo And Not bCheck)
                    bPres4ProcAItem = (bPres4ProcAItem And Not bCheck)
                    m_bCopiarPres4 = False
                End If
            End If
            m_bCopiarEspProce = Not bMismoProceso
            m_bCopiarEspGrupo = True
            m_bCopiarEspItem = True
            m_bCopiarDistribucion = True
            m_bCambioAmbitoDatos = False
            
            'Cambios obligatorios: �mbito grupo a �tem
            If bDestGrupoAItem Then bOk = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(Mensajes.CargarTexto(116, 103), True) = vbYes)
            If bOk And bFPagoGrupoAItem Then bOk = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(Mensajes.CargarTexto(116, 110), True) = vbYes)
            If bOk And bFSumGrupoAItem Then bOk = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(Mensajes.CargarTexto(116, 141), True) = vbYes)
            If bOk And bDistGrupoAItem Then bOk = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(Mensajes.CargarTexto(116, 139), True) = vbYes)
            If m_ParamGen.gbOBLPP And bOk And bPres1GrupoAItem Then bOk = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(ParamGen.gsPlurPres1, True) = vbYes)
            If m_ParamGen.gbOBLPC And bOk And bPres2GrupoAItem Then bOk = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(ParamGen.gsPlurPres2, True) = vbYes)
            If m_ParamGen.gbOBLPres3 And bOk And bPres3GrupoAItem Then bOk = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(ParamGen.gsPlurPres3, True) = vbYes)
            If m_ParamGen.gbOBLPres4 And bOk And bPres4GrupoAItem Then bOk = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(ParamGen.gsPlurPres4, True) = vbYes)
                          
            If bOk Then
                'Cambios de proceso a grupo
                If bDestGrupo Then m_bCopiarDestino = (Mensajes.PreguntarCambiarAmbitoDatosAGrupo(Mensajes.CargarTexto(116, 103)) = vbYes)
                If bFPagoGrupo Then m_bCopiarFormaPago = (Mensajes.PreguntarCambiarAmbitoDatosAGrupo(Mensajes.CargarTexto(116, 110)) = vbYes)
                If bFSumGrupo Then m_bCopiarFechasSum = (Mensajes.PreguntarCambiarAmbitoDatosAGrupo(Mensajes.CargarTexto(116, 141)) = vbYes)
                If bProvActGrupo Then m_bCopiarProveAct = (Mensajes.PreguntarCambiarAmbitoDatosAGrupo(Mensajes.CargarTexto(116, 178)) = vbYes)
                If bPres1Grupo Then m_bCopiarPres1 = (Mensajes.PreguntarCambiarAmbitoDatosAGrupo(ParamGen.gsPlurPres1) = vbYes)
                If bPres2Grupo Then m_bCopiarPres2 = (Mensajes.PreguntarCambiarAmbitoDatosAGrupo(ParamGen.gsPlurPres2) = vbYes)
                If bPres3Grupo Then m_bCopiarPres3 = (Mensajes.PreguntarCambiarAmbitoDatosAGrupo(ParamGen.gsPlurPres3) = vbYes)
                If bPres4Grupo Then m_bCopiarPres4 = (Mensajes.PreguntarCambiarAmbitoDatosAGrupo(ParamGen.gsPlurPres4) = vbYes)
                
                'Cambios de proceso a item
                If bDestProcAItem Then m_bCopiarDestino = (Mensajes.PreguntarCambiarAmbitoDatosProcesoAItem(Mensajes.CargarTexto(116, 103)) = vbYes)
                If bFPagoProcAItem Then m_bCopiarFormaPago = (Mensajes.PreguntarCambiarAmbitoDatosProcesoAItem(Mensajes.CargarTexto(116, 110)) = vbYes)
                If bFSumProcAItem Then m_bCopiarFechasSum = (Mensajes.PreguntarCambiarAmbitoDatosProcesoAItem(Mensajes.CargarTexto(116, 141)) = vbYes)
                If bProvActProcAItem Then m_bCopiarProveAct = (Mensajes.PreguntarCambiarAmbitoDatosProcesoAItem(Mensajes.CargarTexto(116, 178)) = vbYes)
                If bPres1ProcAItem Then m_bCopiarPres1 = (Mensajes.PreguntarCambiarAmbitoDatosProcesoAItem(ParamGen.gsPlurPres1) = vbYes)
                If bPres2ProcAItem Then m_bCopiarPres2 = (Mensajes.PreguntarCambiarAmbitoDatosProcesoAItem(ParamGen.gsPlurPres2) = vbYes)
                If bPres3ProcAItem Then m_bCopiarPres3 = (Mensajes.PreguntarCambiarAmbitoDatosProcesoAItem(ParamGen.gsPlurPres3) = vbYes)
                If bPres4ProcAItem Then m_bCopiarPres4 = (Mensajes.PreguntarCambiarAmbitoDatosProcesoAItem(ParamGen.gsPlurPres4) = vbYes)
                            
                'Cambios de grupo a item
                If bProvActGrupoAItem Then m_bCopiarProveAct = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(Mensajes.CargarTexto(116, 178), False) = vbYes)
                If bDestGrupoAItem Then m_bCopiarDestino = bOk
                If bFPagoGrupoAItem Then m_bCopiarFormaPago = bOk
                If bFSumGrupoAItem Then m_bCopiarFechasSum = bOk
                If bDistGrupoAItem Then m_bCopiarDistribucion = bOk
                If m_ParamGen.gbOBLPP Then
                    If Not bComprobarPres1 Then m_bCopiarPres1 = bOk
                Else
                    If bPres1GrupoAItem Then m_bCopiarPres1 = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(ParamGen.gsPlurPres1, False) = vbYes)
                End If
                If m_ParamGen.gbOBLPC Then
                    If Not bComprobarPres2 Then m_bCopiarPres2 = bOk
                Else
                    If bPres2GrupoAItem Then m_bCopiarPres2 = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(ParamGen.gsPlurPres2, False) = vbYes)
                End If
                If m_ParamGen.gbOBLPres3 Then
                    If Not bComprobarPres3 Then m_bCopiarPres3 = bOk
                Else
                    If bPres3GrupoAItem Then m_bCopiarPres3 = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(ParamGen.gsPlurPres3, False) = vbYes)
                End If
                If m_ParamGen.gbOBLPres4 Then
                    If Not bComprobarPres4 Then m_bCopiarPres4 = bOk
                Else
                    If bPres4GrupoAItem Then m_bCopiarPres4 = (Mensajes.PreguntarCambiarAmbitoDatosGrupoAItem(ParamGen.gsPlurPres4, False) = vbYes)
                End If
                
                'Especificaciones
                If bEspProc And Not bMismoProceso Then m_bCopiarEspProce = (Mensajes.PreguntarTrasladarEspProcesoAGrupo(ProcesoDestino.DefEspGrupos) = vbYes)
                If bEspGrupo Then m_bCopiarEspGrupo = (Mensajes.PreguntarTrasladarEspGrupoAGrupo(ProcesoDestino.DefEspGrupos) = vbYes)
                If bEspItem Then m_bCopiarEspItem = (Mensajes.PreguntarTrasladarEspAItem(ProcesoDestino.DefEspItems) = vbYes)
                
                'Si ha habido alg�n cambio de �mbito de datos hay que refrescar todo el �rbol del proceso
                m_bCambioAmbitoDatos = (bDestGrupo Or bFPagoGrupo Or bFSumGrupo Or bProvActGrupo Or bPres1Grupo Or bPres2Grupo Or bPres3Grupo Or bPres4Grupo Or _
                                        bDestProcAItem Or bFPagoProcAItem Or bFSumProcAItem Or bProvActProcAItem Or bPres1ProcAItem Or bPres2ProcAItem Or bPres3ProcAItem Or bPres4ProcAItem Or _
                                        bDestGrupoAItem Or bFPagoGrupoAItem Or bFSumGrupoAItem Or bProvActGrupoAItem Or bDistGrupoAItem Or bPres1GrupoAItem Or bPres2GrupoAItem Or bPres3GrupoAItem Or bPres4GrupoAItem Or _
                                        bEspProc Or bEspGrupo)
            End If
        End If
    End If
    
Salir:
    ValidarGrupo = bOk
    Set oGrupo = Nothing
    Exit Function
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "ValidarGrupo", Err, Erl, , m_bActivado)
    End If
    Resume Salir
End Function

''' <summary>Muestra la caja de texto para introducir el c�digo para el nuevo grupo</summary>
''' <remarks>Llamada desde: ValidarGrupo</remarks>

Private Sub PedirCodigoNuevoGrupo(ByVal bVisible As Boolean)
    picNuevoCod.Visible = bVisible
    Arrange
    If bVisible Then txtNuevoCod.SetFocus
End Sub

Private Sub cmdCancelar_Click()
    CopiarOk = False
    Me.Hide
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then Arrange
End Sub

''' <summary>Ubica los controles en el formulario</summary>
''' <remarks>Llamada desde: Form_Resize, PedirCodigoNuevoGrupo</remarks>

Private Sub Arrange()
    If Me.Width < 11565 Then Me.Width = 11565
    If picNuevoCod.Visible Then
        If Me.Height < 3510 Then Me.Height = 3510
    Else
        If Me.Height < 3865 Then Me.Height = 2865
    End If
        
    cmdAceptar.Top = Me.ScaleHeight - cmdAceptar.Height - 100
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = (Me.ScaleWidth / 2) - cmdAceptar.Width - 75
    cmdCancelar.Left = cmdAceptar.Left + cmdAceptar.Width + 75
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oProceso = Nothing
End Sub

Private Sub sdbcAnyo_Click()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcAnyo_Click", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGMN1_4Cod_Click()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not sdbcGMN1_4Cod.DroppedDown Then sdbcGMN1_4Cod = ""
   
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGMN1_4Cod_Click", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoCod_Change()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
        
    With sdbcGrupoDen
        If Not m_bRespetarCombo Then
            m_bRespetarCombo = True
            
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
            
            m_bRespetarCombo = False
            m_bCargarComboDesde = True
        End If
    End With
    
    txtNuevoCod.Text = ""
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoCod_Change", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoCod_Click()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not sdbcGrupoCod.DroppedDown Then
        With sdbcGrupoCod
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
        End With
        
        With sdbcGrupoDen
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
        End With
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoCod_Click", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoCod_CloseUp()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    With sdbcGrupoCod
        If .Value = "..." Or Trim(.Value) = "" Then
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
            Exit Sub
        End If
        
        If .Value = "" Then Exit Sub
        
        m_bRespetarCombo = True
        sdbcGrupoDen.Text = sdbcGrupoCod.Columns(1).Text
        sdbcGrupoCod.Text = sdbcGrupoCod.Columns(0).Text
        m_bRespetarCombo = False
            
        m_bCargarComboDesde = False
        
        txtNuevoCod.Text = ""
    End With
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoCod_CloseUp", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoCod_DropDown()
    Dim oGrupo As CGrupo
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcGrupoCod.RemoveAll
    
    If Not m_oProceso Is Nothing Then
        Screen.MousePointer = vbHourglass
            
        m_oProceso.CargarGrupos
        
        For Each oGrupo In m_oProceso.Grupos
            sdbcGrupoCod.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den & Chr(m_lSeparador) & oGrupo.Id
        Next
    
        sdbcGrupoCod.SelStart = 0
        sdbcGrupoCod.SelLength = Len(sdbcGrupoCod.Text)
        sdbcGrupoCod.Refresh
    End If
    
Salir:
    Screen.MousePointer = vbNormal
    Set oGrupo = Nothing
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoCod_DropDown", Err, Erl, Me, m_bActivado)
    End If
    Resume Salir
End Sub

Private Sub sdbcGrupoCod_InitColumnProps()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcGrupoCod.DataFieldList = "Column 0"
    sdbcGrupoCod.DataFieldToDisplay = "Column 0"

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoCod_InitColumnProps", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    On Error Resume Next
    
    sdbcGrupoCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGrupoCod.Rows - 1
            bm = sdbcGrupoCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGrupoCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGrupoCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoCod_PositionList", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoCod_Validate(Cancel As Boolean)
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Trim(sdbcGrupoCod.Text = "") Then Exit Sub
    
    Screen.MousePointer = vbHourglass
        
    m_oProceso.CargarGrupos
        
    If m_oProceso.Grupos.Item(sdbcGrupoCod.Text) Is Nothing Then
        sdbcGrupoCod.Text = ""
        sdbcGrupoCod.Columns(0).Value = ""
        sdbcGrupoCod.Columns(1).Value = ""
        sdbcGrupoCod.Columns(2).Value = ""
        sdbcGrupoCod.RemoveAll
        Screen.MousePointer = vbNormal
        Mensajes.NoValido m_arIdioma(0)
    Else
        m_bRespetarCombo = True
        sdbcGrupoDen.Text = m_oProceso.Grupos.Item(sdbcGrupoCod.Text).Den
        
        sdbcGrupoCod.Columns("COD").Text = m_oProceso.Grupos.Item(sdbcGrupoCod.Text).Codigo
        sdbcGrupoCod.Columns("DEN").Text = m_oProceso.Grupos.Item(sdbcGrupoCod.Text).Den
        sdbcGrupoCod.Columns("ID").Text = m_oProceso.Grupos.Item(sdbcGrupoCod.Text).Id
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = False
    End If
   
Salir:
    Screen.MousePointer = vbNormal
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoCod_Validate", Err, Erl, Me, m_bActivado)
    End If
    Resume Salir
End Sub

Private Sub sdbcGrupoDen_Change()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        
        With sdbcGrupoCod
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
        End With
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoDen_Change", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoDen_Click()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not sdbcGrupoDen.DroppedDown Then
        With sdbcGrupoCod
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
        End With
        
        With sdbcGrupoDen
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
        End With
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoDen_Click", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoDen_CloseUp()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    With sdbcGrupoDen
        If .Value = "" Then Exit Sub
        
        m_bRespetarCombo = True
        sdbcGrupoCod.Text = .Columns(1).Text
        .Text = .Columns(0).Text
        m_bRespetarCombo = False
        m_bCargarComboDesde = False
    End With
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoDen_CloseUp", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoDen_DropDown()
    Dim oGrupo As CGrupo
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcGrupoDen.RemoveAll
    
    If Not m_oProceso Is Nothing Then
        Screen.MousePointer = vbHourglass
            
        m_oProceso.CargarGrupos
        
        For Each oGrupo In m_oProceso.Grupos
            sdbcGrupoDen.AddItem oGrupo.Den & Chr(m_lSeparador) & oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Id
        Next
    
        sdbcGrupoDen.SelStart = 0
        sdbcGrupoDen.SelLength = Len(sdbcGrupoDen.Text)
        sdbcGrupoDen.Refresh
    End If
    
Salir:
    Screen.MousePointer = vbNormal
    Set oGrupo = Nothing
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoDen_DropDown", Err, Erl, Me, m_bActivado)
    End If
    Resume Salir
End Sub

Private Sub sdbcGrupoDen_InitColumnProps()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcGrupoDen.DataFieldList = "Column 0"
    sdbcGrupoDen.DataFieldToDisplay = "Column 0"

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoDen_InitColumnProps", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupoDen_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    On Error Resume Next
    
    sdbcGrupoDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGrupoDen.Rows - 1
            bm = sdbcGrupoDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGrupoDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGrupoDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGrupoDen_PositionList", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceCod_Change()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
    
        With sdbcProceDen
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .RemoveAll
        End With
        
        With sdbcGrupoCod
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
        End With
        
        With sdbcGrupoDen
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
        End With
        
        txtNuevoCod.Text = ""
        
        Set m_oProceso = Nothing
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceCod_Change", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceCod_Click()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceCod_Click", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceCod_CloseUp()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    With sdbcProceCod
        If .Value = "..." Or Trim(.Value) = "" Then
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .RemoveAll
            Exit Sub
        End If
        
        sdbcGrupoCod.Text = ""
        sdbcGrupoCod.Columns(0).Value = ""
        sdbcGrupoCod.Columns(1).Value = ""
        sdbcGrupoCod.Columns(2).Value = ""
        sdbcGrupoCod.RemoveAll
        
        txtNuevoCod.Text = ""
        
        If .Value = "" Then Exit Sub
    End With
    
    m_bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    m_bRespetarCombo = False
    m_bCargarComboDesde = False
    
    ProcesoSeleccionado
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceCod_CloseUp", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceCod_DropDown()
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim oProceso As cProceso
    Dim oProcesos As CProcesos
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcProceCod.RemoveAll
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
       
    Set m_oProceso = Nothing
    
    EstablecerRangoEstados udtEstDesde, udtEstHasta
    
    Set oProcesos = Raiz.Generar_CProcesos
    If m_bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde CargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , Val(sdbcProceCod.Value), , False, False, m_DatosUsu.CodEqp, m_DatosUsu.CodPersona, m_DatosUsu.RMat, m_DatosUsu.RComp, m_DatosUsu.RCompResp, m_DatosUsu.REqp, m_DatosUsu.RUsuAper, m_DatosUsu.RUsuUON, m_DatosUsu.RUsuDep, m_DatosUsu.Cod, m_DatosUsu.UON1, m_DatosUsu.UON2, m_DatosUsu.UON3, m_DatosUsu.CodDep, , , , True, m_DatosUsu.SoloInvitado, , m_DatosUsu.RPerfUON, m_DatosUsu.Perfil
    Else
        oProcesos.CargarTodosLosProcesosDesde CargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, m_DatosUsu.CodEqp, m_DatosUsu.CodPersona, m_DatosUsu.RMat, m_DatosUsu.RComp, m_DatosUsu.RCompResp, m_DatosUsu.REqp, m_DatosUsu.RUsuAper, m_DatosUsu.RUsuUON, m_DatosUsu.RUsuDep, m_DatosUsu.Cod, m_DatosUsu.UON1, m_DatosUsu.UON2, m_DatosUsu.UON3, m_DatosUsu.CodDep, , , , True, m_DatosUsu.SoloInvitado, , m_DatosUsu.RPerfUON, m_DatosUsu.Perfil
    End If
    
    For Each oProceso In oProcesos
        sdbcProceCod.AddItem oProceso.Cod & Chr(m_lSeparador) & oProceso.Den & Chr(m_lSeparador) & BooleanToSQLBinary(oProceso.Invitado)
    Next
            
    'If m_bCargarComboDesde And Not oProcesos.EOF Then
    If Not oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
Salir:
    Screen.MousePointer = vbNormal
    Set oProcesos = Nothing
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceCod_DropDown", Err, Erl, Me, m_bActivado)
    End If
    Resume Salir
End Sub

Private Sub sdbcProceCod_InitColumnProps()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceCod_InitColumnProps", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    On Error Resume Next
    
    sdbcProceCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceCod.Rows - 1
            bm = sdbcProceCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceCod_PositionList", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Public Sub sdbcProceCod_Validate(Cancel As Boolean)
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim oProcesos As CProcesos
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Trim(sdbcProceCod.Text = "") Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Set m_oProceso = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcProceCod.Value > MaxProceCod Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        Mensajes.NoValido m_arIdioma(1)
        Set m_oProceso = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If Not IsNumeric(sdbcProceCod) Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        Mensajes.NoValido m_arIdioma(1)
        Set m_oProceso = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        m_bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        m_bRespetarCombo = False
        If m_oProceso Is Nothing Then ProcesoSeleccionado
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceDen.Columns(1).Text Then
        m_bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
        m_bRespetarCombo = False
        If m_oProceso Is Nothing Then ProcesoSeleccionado
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proceso
    Screen.MousePointer = vbHourglass
    
    EstablecerRangoEstados udtEstDesde, udtEstHasta
    
    Set oProcesos = Raiz.Generar_CProcesos
    oProcesos.CargarTodosLosProcesosDesde 1, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , Val(sdbcProceCod), , True, False, m_DatosUsu.CodEqp, m_DatosUsu.CodPersona, m_DatosUsu.RMat, m_DatosUsu.RComp, m_DatosUsu.RCompResp, m_DatosUsu.REqp, m_DatosUsu.RUsuAper, m_DatosUsu.RUsuUON, m_DatosUsu.RUsuDep, m_DatosUsu.Cod, m_DatosUsu.UON1, m_DatosUsu.UON2, m_DatosUsu.UON3, m_DatosUsu.CodDep, , , , True, m_DatosUsu.SoloInvitado, , m_DatosUsu.RPerfUON, m_DatosUsu.Perfil
        
    If oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        Mensajes.NoValido m_arIdioma(1)
        Set m_oProceso = Nothing
    Else
        m_bRespetarCombo = True
        sdbcProceDen.Text = oProcesos.Item(1).Den
        
        sdbcProceCod.Columns("COD").Text = sdbcProceCod.Text
        sdbcProceCod.Columns("DEN").Text = sdbcProceDen.Text
        sdbcProceCod.Columns("INVI").Text = BooleanToSQLBinary(oProcesos.Item(1).Invitado)
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = False
        
        ProcesoSeleccionado
    End If
   
Salir:
    Screen.MousePointer = vbNormal
    Set oProcesos = Nothing
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceCod_Validate", Err, Erl, Me, m_bActivado)
    End If
    Resume Salir
End Sub

''' <summary>Establece los rangos de estado para la b�squeda de procesos</summary>
''' <remarks>Llamada desde: sdbcProceCod_Validate</remarks>

Private Sub EstablecerRangoEstados(ByRef udtEstDesde As TipoEstadoProceso, ByRef udtEstHasta As TipoEstadoProceso)
    Select Case ProceSelector.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstDesde = sinitems
            udtEstHasta = ConItemsSinValidar
        Case PSSeleccion.PSAbiertos
            udtEstDesde = sinitems
            udtEstHasta = PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = ParcialmenteCerrado
            udtEstHasta = ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstDesde = conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = sinitems
            udtEstHasta = TipoEstadoProceso.Cerrado
    End Select
End Sub

Private Sub sdbcGMN1_4Cod_Change()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not m_bRespetarCombo Then
        m_bCargarComboDesde = True
        
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGMN1_4Cod_Change", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    m_bRespetarCombo = False
        
    m_bCargarComboDesde = False
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGMN1_4Cod_CloseUp", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer
    Dim oGruposMN1 As CGruposMatNivel1
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = Raiz.Generar_CGruposMatNivel1
    
    If m_bCargarComboDesde Then
        If m_DatosUsu.RMat Then
            Set oIMAsig = m_DatosUsu.Comprador
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(CargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , m_DatosUsu.RUsuAper, m_DatosUsu.RCompResp, m_DatosUsu.CodPersona, True, sdbcAnyo.Value)
        Else
            Set oGruposMN1 = Raiz.Generar_CGruposMatNivel1
            oGruposMN1.CargarTodosLosGruposMatDesde CargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If m_DatosUsu.RMat Then
            Set oIMAsig = m_DatosUsu.Comprador
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(CargaMaximaCombos, , , , False, , m_DatosUsu.RUsuAper, m_DatosUsu.RCompResp, m_DatosUsu.CodPersona, True, sdbcAnyo.Value)
        Else
            Set oGruposMN1 = Raiz.Generar_CGruposMatNivel1
            oGruposMN1.CargarTodosLosGruposMatDesde CargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    'If m_bCargarComboDesde And Not oGruposMN1.EOF Then
    If Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh

Salir:
    Screen.MousePointer = vbNormal
    Set oGruposMN1 = Nothing
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGMN1_4Cod_DropDown", Err, Erl, Me, m_bActivado)
    End If
    Resume Salir
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGMN1_4Cod_InitColumnProps", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGMN1_4Cod_PositionList", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    Dim oGruposMN1 As CGruposMatNivel1
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Or sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    
    If m_DatosUsu.RMat Then
        Set oIMAsig = m_DatosUsu.Comprador
        
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False, , m_DatosUsu.RUsuAper, m_DatosUsu.RCompResp, m_DatosUsu.CodPersona, True, sdbcAnyo.Value)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, LongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            Mensajes.NoValido m_arIdioma(2)
        Else
            m_bCargarComboDesde = False
        End If
    Else
        Set oGMN1 = Raiz.generar_CGrupoMatNivel1
      
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            Mensajes.NoValido m_arIdioma(2)
        Else
            m_bCargarComboDesde = False
        End If
    End If

Salir:
    Screen.MousePointer = vbNormal
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcGMN1_4Cod_Validate", Err, Erl, Me, m_bActivado)
    End If
    Resume Salir
End Sub

Private Sub sdbcProceDen_Change()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        
        With sdbcProceCod
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .RemoveAll
        End With
        
        With sdbcGrupoCod
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .Columns(2).Value = ""
            .RemoveAll
        End With
        
        Set m_oProceso = Nothing
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceDen_Change", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceDen_Click()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    If Not sdbcProceDen.DroppedDown Then
        With sdbcProceCod
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .RemoveAll
        End With
        
        With sdbcProceDen
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .RemoveAll
        End With
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceDen_Click", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceDen_CloseUp()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    With sdbcProceDen
        If .Value = "....." Or Trim(.Value) = "" Then
            .Text = ""
            .Columns(0).Value = ""
            .Columns(1).Value = ""
            .RemoveAll
            Exit Sub
        End If
        
        If .Value = "" Then Exit Sub
        
        m_bRespetarCombo = True
        sdbcProceCod.Text = .Columns(1).Text
        .Text = .Columns(0).Text
        m_bRespetarCombo = False
        m_bCargarComboDesde = False
    End With
    
    ProcesoSeleccionado

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceDen_CloseUp", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceDen_DropDown()
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim oProceso As cProceso
    Dim oProcesos As CProcesos
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcProceDen.RemoveAll
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set m_oProceso = Nothing
            
    EstablecerRangoEstados udtEstDesde, udtEstHasta
    
    Set oProcesos = Raiz.Generar_CProcesos
    If m_bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde CargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , sdbcProceDen, False, False, m_DatosUsu.CodEqp, m_DatosUsu.CodPersona, m_DatosUsu.RMat, m_DatosUsu.RComp, m_DatosUsu.RCompResp, m_DatosUsu.REqp, m_DatosUsu.RUsuAper, m_DatosUsu.RUsuUON, m_DatosUsu.RUsuDep, m_DatosUsu.Cod, m_DatosUsu.UON1, m_DatosUsu.UON2, m_DatosUsu.UON3, m_DatosUsu.CodDep, , , , True, m_DatosUsu.SoloInvitado, , m_DatosUsu.RPerfUON, m_DatosUsu.Perfil
    Else
        oProcesos.CargarTodosLosProcesosDesde CargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, m_DatosUsu.CodEqp, m_DatosUsu.CodPersona, m_DatosUsu.RMat, m_DatosUsu.RComp, m_DatosUsu.RCompResp, m_DatosUsu.REqp, m_DatosUsu.RUsuAper, m_DatosUsu.RUsuUON, m_DatosUsu.RUsuDep, m_DatosUsu.Cod, m_DatosUsu.UON1, m_DatosUsu.UON2, m_DatosUsu.UON3, m_DatosUsu.CodDep, , , , True, m_DatosUsu.SoloInvitado, , m_DatosUsu.RPerfUON, m_DatosUsu.Perfil
    End If
        
    For Each oProceso In oProcesos
        sdbcProceDen.AddItem oProceso.Den & Chr(m_lSeparador) & oProceso.Cod & Chr(m_lSeparador) & BooleanToSQLBinary(oProceso.Invitado)
    Next
    
    If Not oProcesos.EOF Then
        sdbcProceDen.AddItem "....."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh

Salir:
    Screen.MousePointer = vbNormal
    Set oProcesos = Nothing
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceDen_DropDown", Err, Erl, Me, m_bActivado)
    End If
    Resume Salir
End Sub

Private Sub sdbcProceDen_InitColumnProps()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceDen_InitColumnProps", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcProceDen_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    On Error Resume Next
    
    sdbcProceDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProceDen.Rows - 1
            bm = sdbcProceDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProceDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProceDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "sdbcProceDen_PositionList", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub ProceSelector1_Click(ByVal Opcion As SelectorDeProcesos.PSSeleccion)
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "ProceSelector1_Click", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub cmdBuscar_Click()
    Dim oProcesos As CProcesos
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    With FrmProceBusq
        .bRDest = m_DatosUsu.RDest
        .bRMat = m_DatosUsu.RMat
        .bRAsig = m_DatosUsu.RComp
        .bRCompResponsable = m_DatosUsu.RCompResp
        .bREqpAsig = m_DatosUsu.REqp
        .bRUsuAper = m_DatosUsu.RUsuAper
        .bRUsuUON = m_DatosUsu.RUsuUON
        .bRPerfUON = m_DatosUsu.RPerfUON
        .bRUsuDep = m_DatosUsu.RUsuDep
        .bRestProvMatComp = m_DatosUsu.RProvMatComp
        .bRestProvEquComp = False
        .sOrigen = "frmCopiarGrupo"
        .sdbcAnyo.Value = sdbcAnyo.Value
        .sdbcGMN1Proce_Cod.Value = sdbcGMN1_4Cod.Value
        .sdbcGMN1Proce_Cod_Validate False
        
        .Show 1
        
        Set oProcesos = .oProceEncontrados
        Set .oProceEncontrados = Nothing
    End With
                                
    Unload FrmProceBusq

    If Not oProcesos Is Nothing Then
        If oProcesos.Count > 0 Then
            If Not oProcesos.Item(1) Is Nothing Then
                sdbcAnyo.Value = oProcesos.Item(1).Anyo
                sdbcGMN1_4Cod.Value = oProcesos.Item(1).GMN1Cod
                sdbcGMN1_4Cod.Columns(0).Value = ""
                sdbcGMN1_4Cod_Validate False
                
                If Not oProcesos.Item(1) Is Nothing Then
                    If oProcesos.Item(1).Estado >= TipoEstadoProceso.sinitems And oProcesos.Item(1).Estado <= TipoEstadoProceso.ConItemsSinValidar Then
                        ProceSelector.Seleccion = 0
                    Else
                        If oProcesos.Item(1).Estado >= TipoEstadoProceso.validado And oProcesos.Item(1).Estado <= TipoEstadoProceso.PreadjYConObjNotificados Then
                            ProceSelector.Seleccion = 1
                        Else
                            If oProcesos.Item(1).Estado = TipoEstadoProceso.ParcialmenteCerrado Then
                                ProceSelector.Seleccion = 7
                            Else
                                ProceSelector.Seleccion = 2
                            End If
                        End If
                   End If
                End If
                
                m_bRespetarCombo = True
                sdbcProceCod.Value = oProcesos.Item(1).Cod
                sdbcProceDen.Value = oProcesos.Item(1).Den
                sdbcProceCod_Validate False
                m_bRespetarCombo = False
                m_bCargarComboDesde = False
            End If
        End If
    End If

Salir:
    Set oProcesos = Nothing
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "cmdBuscar_Click", Err, Erl, Me, m_bActivado)
    End If
    Resume Salir
End Sub

Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset
    
    On Error Resume Next

    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_COPIARGRUPO, m_DatosUsu.Idioma)

    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.Caption = Ador(0).Value & ":"
        lblGMN1.Caption = AbrGMN1 & ":"
        Ador.MoveNext
        lblProceso.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        m_arIdioma(0) = Ador(0).Value
        Ador.MoveNext
        m_arIdioma(1) = Ador(0).Value
        Ador.MoveNext
        m_arIdioma(2) = Ador(0).Value
        Ador.MoveNext
        ProceSelector.DenParaPendientes = Ador(0).Value
        Ador.MoveNext
        ProceSelector.DenParaAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector.DenParaCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector.DenParaTodos = Ador(0).Value
        Ador.MoveNext
        ProceSelector.AbrevParaPendientes = Ador(0).Value
        Ador.MoveNext
        ProceSelector.AbrevParaAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector.AbrevParaCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector.AbrevParaTodos = Ador(0).Value
        Ador.MoveNext
        ProceSelector.DenParaParcialCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector.AbrevParaParcialCerrados = Ador(0).Value
        Ador.MoveNext
        lblSelGrupo.Caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblCodExistente.Caption = Ador(0).Value & ":"
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Private Sub Form_Activate()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If

    If Not m_bActivado Then m_bActivado = True

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "Form_Activate", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    m_bDescargarFrm = False
    m_bActivado = False
    
    PonerFieldSeparator Me
    txtNuevoCod.MaxLength = LongCodGrupo
    CargarRecursos
    CargarAnyos
    
    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "Form_Load", Err, Erl, Me, m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Carga el combo de a�os</summary>
''' <remarks>Llamada desde Form_Load</remarks>

Private Sub CargarAnyos()
    Dim iAnyoActual As Integer
    Dim iInd As Integer
    
    If Not Raiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    iAnyoActual = Year(Date)
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        sdbcAnyo.AddItem iInd
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7

    Exit Sub
Error:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Form", "frmCopiarGrupo", "CargarAnyos", Err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Establece las propiedades del objeto de proceso seleccionado</summary>
''' <remarks>Llamada desde Form_Load</remarks>

Private Sub ProcesoSeleccionado()
    Set m_oProceso = Raiz.Generar_CProceso
    
    m_oProceso.Anyo = sdbcAnyo.Value
    m_oProceso.GMN1Cod = sdbcGMN1_4Cod.Value
    m_oProceso.Cod = sdbcProceCod.Value
End Sub
