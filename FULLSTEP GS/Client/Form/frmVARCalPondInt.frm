VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmVARCalPondInt 
   Caption         =   "DVariables de calidad"
   ClientHeight    =   6510
   ClientLeft      =   180
   ClientTop       =   2580
   ClientWidth     =   5880
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVARCalPondInt.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6510
   ScaleWidth      =   5880
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Height          =   5500
      Left            =   45
      ScaleHeight     =   5445
      ScaleWidth      =   5715
      TabIndex        =   6
      Top             =   405
      Width           =   5775
      Begin VB.PictureBox picNumerico 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   5450
         Left            =   0
         ScaleHeight     =   5445
         ScaleWidth      =   5730
         TabIndex        =   7
         Top             =   0
         Width           =   5730
         Begin VB.PictureBox PictureCalc 
            Appearance      =   0  'Flat
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   1350
            Left            =   100
            ScaleHeight     =   1350
            ScaleWidth      =   7335
            TabIndex        =   15
            Top             =   3600
            Width           =   7335
            Begin VB.OptionButton optOpcionConf 
               BackColor       =   &H00808000&
               Caption         =   "DNo calcular"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   2
               Left            =   50
               TabIndex        =   18
               Top             =   900
               Width           =   2800
            End
            Begin VB.OptionButton optOpcionConf 
               BackColor       =   &H00808000&
               Caption         =   "DEvaluar f�rmula"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   0
               Left            =   50
               TabIndex        =   17
               Top             =   300
               Width           =   2800
            End
            Begin VB.OptionButton optOpcionConf 
               BackColor       =   &H00808000&
               Caption         =   "DMedia ponderada seg�n:"
               ForeColor       =   &H00FFFFFF&
               Height          =   375
               Index           =   1
               Left            =   50
               TabIndex        =   16
               Top             =   600
               Width           =   2800
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcVarPond 
               Height          =   285
               Left            =   3000
               TabIndex        =   19
               Top             =   645
               Width           =   2300
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "COD"
               Columns(0).Alignment=   1
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6800
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4057
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblOpcionConf 
               AutoSize        =   -1  'True
               BackColor       =   &H00808000&
               Caption         =   "DSelecci�n del modo de c�lculo para unidades de negocio de nivel superior:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   -1  'True
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   0
               TabIndex        =   20
               Top             =   0
               Width           =   7185
            End
         End
         Begin VB.OptionButton optPond 
            BackColor       =   &H00808000&
            Caption         =   "DEscala continua"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Index           =   1
            Left            =   630
            TabIndex        =   0
            Top             =   675
            Width           =   1845
         End
         Begin VB.OptionButton optPond 
            BackColor       =   &H00808000&
            Caption         =   "DF�rmula"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            HelpContextID   =   3
            Index           =   2
            Left            =   3375
            TabIndex        =   1
            Top             =   690
            Width           =   1665
         End
         Begin VB.Frame fraPond 
            BackColor       =   &H00808000&
            Height          =   1350
            Index           =   2
            Left            =   330
            TabIndex        =   10
            Tag             =   "Formula"
            Top             =   1380
            Visible         =   0   'False
            Width           =   4980
            Begin VB.TextBox txtFormula 
               Height          =   315
               Left            =   180
               TabIndex        =   3
               Top             =   675
               Width           =   4230
            End
            Begin VB.CommandButton cmdAyuda 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   7.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   4455
               Picture         =   "frmVARCalPondInt.frx":014A
               Style           =   1  'Graphical
               TabIndex        =   4
               Top             =   690
               Width           =   350
            End
            Begin VB.Label lblFormula 
               BackColor       =   &H00808000&
               Caption         =   "Redacte la f�rmula considerando el campo como X"
               ForeColor       =   &H00FFFFFF&
               Height          =   285
               Left            =   225
               TabIndex        =   11
               Top             =   315
               Width           =   4560
            End
         End
         Begin VB.Frame fraPond 
            BackColor       =   &H00808000&
            Height          =   1035
            Index           =   0
            Left            =   345
            TabIndex        =   8
            Tag             =   "Sin pond"
            Top             =   1680
            Visible         =   0   'False
            Width           =   4935
            Begin VB.Label lblSinPond 
               BackColor       =   &H00808000&
               Caption         =   "DAtributo sin ponderaci�n"
               ForeColor       =   &H00FFFFFF&
               Height          =   195
               Left            =   585
               TabIndex        =   9
               Top             =   450
               Width           =   3795
            End
         End
         Begin VB.Frame fraPond 
            BackColor       =   &H00808000&
            Height          =   2430
            Index           =   1
            Left            =   330
            TabIndex        =   14
            Tag             =   "Continua"
            Top             =   960
            Visible         =   0   'False
            Width           =   4965
            Begin SSDataWidgets_B.SSDBGrid sdbgContinua 
               Height          =   2040
               Left            =   180
               TabIndex        =   2
               Top             =   225
               Width           =   4635
               ScrollBars      =   2
               _Version        =   196617
               DataMode        =   2
               Row.Count       =   3
               Col.Count       =   16
               Row(0).Col(0)   =   "0"
               Row(0).Col(1)   =   "10"
               Row(0).Col(2)   =   "1"
               Row(1).Col(0)   =   "11"
               Row(1).Col(1)   =   "20"
               Row(1).Col(2)   =   "2"
               Row(2).Col(0)   =   "20"
               Row(2).Col(1)   =   "999999"
               Row(2).Col(2)   =   "10"
               stylesets.count =   1
               stylesets(0).Name=   "Normal"
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmVARCalPondInt.frx":037B
               AllowAddNew     =   -1  'True
               AllowDelete     =   -1  'True
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               ExtraHeight     =   26
               Columns.Count   =   4
               Columns(0).Width=   2752
               Columns(0).Caption=   "Desde"
               Columns(0).Name =   "DESDE"
               Columns(0).AllowSizing=   0   'False
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2143
               Columns(1).Caption=   "Hasta"
               Columns(1).Name =   "HASTA"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   2196
               Columns(2).Caption=   "=> Puntos"
               Columns(2).Name =   "PUNTOS"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "INDI"
               Columns(3).Name =   "INDI"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   8176
               _ExtentY        =   3598
               _StockProps     =   79
               ForeColor       =   0
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00FFFFFF&
            X1              =   0
            X2              =   5730
            Y1              =   450
            Y2              =   450
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackColor       =   &H00808000&
            Caption         =   "Label1"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   12
            Top             =   135
            Width           =   5640
         End
      End
   End
   Begin VB.Label lblCambios 
      Caption         =   "Para guardar los cambios efectuados cierre esta ventana y pulse Guadar cambios en la ventada principal de Variables de calidad."
      ForeColor       =   &H000000FF&
      Height          =   555
      Left            =   90
      TabIndex        =   13
      Top             =   6000
      Width           =   5745
   End
   Begin VB.Label lblTipo 
      Caption         =   "Subvariable de tipo: "
      Height          =   240
      Left            =   45
      TabIndex        =   5
      Top             =   45
      Width           =   5730
   End
End
Attribute VB_Name = "frmVARCalPondInt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables publicas
Public g_oVariableCalidad As CVariableCalidad
Public g_oVarCalidadOriginal As CVariableCalidad
Public g_lFormulario As Long
Public GestorIdiomas As CGestorIdiomas
Public Mensajes As CMensajes
Public Raiz As CRaiz
Public FrmVARCalidad As Object
Public FrmVARCalCompuestaNivel5 As Object
Public FrmVARCalCompuestaNivel4 As Object
Public FrmVARCalCompuestaNivel3 As Object
Public Idioma As String

'Variables de gesti�n
Private m_bErrorC  As Boolean
Private m_bRespetarCarga As Boolean

'Variables de idiomas
Private m_sSinPond As String
Private m_sContinua As String
Private m_sDiscreta As String
Private m_sCaducidad As String
Private m_sFormula As String
Private m_sManual As String
Private m_sAutomatico As String
Private m_sMensaje(0 To 8) As String
Private m_sLabel(0 To 2) As String
Private m_sTiposVar(0 To 1) As String
Private m_sSiNo(0 To 5) As String
Private m_sIdiErrorFormula(11) As String
Private m_sCaption As String

Private Sub cmdAyuda_Click()
    MostrarFormSOLAyudaCalculosLocal GestorIdiomas, Idioma
End Sub

Private Sub Form_Load()
    Me.Width = 6000
    Me.Height = 6500 '4965
            
    CargarRecursos
    PuntuacionCargar
    CargarVariablesHermanas
    
    lblCambios.Visible = False
    Me.Caption = m_sCaption & " " & g_oVariableCalidad.Denominaciones.Item(Idioma).Den
    picNumerico.Enabled = Not g_oVariableCalidad.BajaLog
End Sub

Private Sub PuntuacionCargar()
    Dim oValorLista As CValorPond
    
    m_bRespetarCarga = True
    Label1.Caption = g_oVariableCalidad.Den
    lblTipo.Caption = m_sLabel(0) & " " & m_sTiposVar(1)

    If g_oVariableCalidad.TipoPonderacion = PondIntEscContinua Then
        If g_oVariableCalidad.INTListaPond Is Nothing Then
            g_oVariableCalidad.CargarPonderacionIntegracion
        End If
        optPond(1).Value = True
        fraPond(1).Visible = True
        sdbgContinua.RemoveAll
        For Each oValorLista In g_oVariableCalidad.INTListaPond
            sdbgContinua.AddItem oValorLista.ValorDesde & Chr(9) & oValorLista.ValorHasta & Chr(9) & oValorLista.ValorPond & Chr(9) & oValorLista.indice
        Next
    Else
        optPond(2).Value = True
        fraPond(2).Visible = True
        txtFormula.Text = NullToStr(g_oVariableCalidad.Formula)
    End If
        
    optOpcionConf(g_oVariableCalidad.Opcion_Conf).Value = True
    If g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
        sdbcVarPond.Columns("ID").Value = g_oVariableCalidad.IdVar_Pond
    End If
    
    m_bRespetarCarga = False
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIDAD_PUNT, Idioma)
    
    If Not Ador Is Nothing Then
        m_sSinPond = Ador(0).Value
        Ador.MoveNext
        m_sContinua = Ador(0).Value
        optPond(1).Caption = m_sContinua
        Ador.MoveNext
        m_sDiscreta = Ador(0).Value
        Ador.MoveNext
        m_sCaducidad = Ador(0).Value
        Ador.MoveNext
        m_sFormula = Ador(0).Value '5
        optPond(2).Caption = m_sFormula
        Ador.MoveNext
        m_sManual = Ador(0).Value
        Ador.MoveNext
        m_sAutomatico = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("DESDE").Caption = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("HASTA").Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sdbgContinua.Columns("PUNTOS").Caption = Ador(0).Value '11
        Ador.MoveNext
        Ador.MoveNext
        m_sMensaje(0) = Ador(0).Value 'El intervalo ya existe
        Ador.MoveNext
        m_sMensaje(1) = Ador(0).Value 'el campo desde debe ser inferior al campo hasta
        Ador.MoveNext
        m_sMensaje(2) = Ador(0).Value 'debe ser num�rico
        Ador.MoveNext
        m_sMensaje(3) = Ador(0).Value 'debe ser una fecha
        Ador.MoveNext
        m_sMensaje(4) = Ador(0).Value 'el valor ya existe
        Ador.MoveNext
        m_sMensaje(5) = Ador(0).Value 'El intervalo introducido contiene uno de los ya existentes
        Ador.MoveNext
        m_sMensaje(6) = Ador(0).Value 'Modifique el �ltimo intervalo introducido
        Ador.MoveNext
        lblSinPond.Caption = Ador(0).Value 'Campo sin ponderaci�n 20
        Ador.MoveNext
        m_sMensaje(7) = Ador(0).Value 'Tenga en cuenta el intervalo libre introducido
        Ador.MoveNext
        m_sMensaje(8) = Ador(0).Value 'Introduzca un m�ximo inferior o igual al introducido en el intervalo libre del campo
        Ador.MoveNext
        m_sLabel(0) = Ador(0).Value
        Ador.MoveNext
        m_sTiposVar(0) = Ador(0).Value '24 Certificado
        Ador.MoveNext
        m_sLabel(1) = Ador(0).Value
        Ador.MoveNext
        m_sLabel(2) = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        '30
        Ador.MoveNext
        m_sSiNo(0) = Ador(0).Value 'Valor = S�
        Ador.MoveNext
        m_sSiNo(1) = Ador(0).Value 'Valor = No
        Ador.MoveNext
        m_sSiNo(2) = Ador(0).Value 'Vigente
        Ador.MoveNext
        m_sSiNo(3) = Ador(0).Value 'Caducada
        Ador.MoveNext
        m_sSiNo(4) = Ador(0).Value 'Contiene archivo
        Ador.MoveNext
        m_sSiNo(5) = Ador(0).Value 'No contiene archivo
        Ador.MoveNext
        For i = 1 To 11
            Ador.MoveNext
            m_sIdiErrorFormula(i) = Ador(0).Value
        Next i
        Ador.MoveNext
        lblCambios.Caption = Ador(0).Value
        Ador.MoveNext
        m_sTiposVar(1) = Ador(0).Value '50 Integracion
        Ador.MoveNext
        lblFormula.Caption = Ador(0).Value '51
        For i = 1 To 14
            Ador.MoveNext
        Next
        m_sCaption = Ador(0).Value '65
        
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        
        Ador.MoveNext
        lblOpcionConf.Caption = Ador(0).Value ' Selecci�n del modo de c�lculo para unidades de negocio de nivel superior:
        Ador.MoveNext
        optOpcionConf(0).Caption = Ador(0).Value ' Evaluar f�rmula
        Ador.MoveNext
        optOpcionConf(1).Caption = Ador(0).Value ' Media ponderada seg�n:
        Ador.MoveNext
        optOpcionConf(2).Caption = Ador(0).Value ' No calcular
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Private Sub Arrange()
    On Error Resume Next
    
    If Not lblCambios.Visible Then
        Picture1.Height = Me.Height - 915
    Else
        Picture1.Height = Me.Height - 1500 '- 1575
    End If

    lblCambios.Top = Picture1.Top + Picture1.Height + 50
    Picture1.Width = Me.Width - 270
    picNumerico.Height = Picture1.Height
    picNumerico.Width = Picture1.Width
    'fraPond(1).Height = Picture1.Height - fraPond(1).Top - 300
    'sdbgContinua.Height = fraPond(1).Height - 400
    'fraPond(1).Width = Picture1.Width - (fraPond(1).Left * 2)
    'sdbgContinua.Width = fraPond(1).Width - 400
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ValidarPonderacion Then
        Cancel = True
        Exit Sub
    End If
    
    Set g_oVariableCalidad = Nothing
    Set g_oVarCalidadOriginal = Nothing
End Sub

Private Sub optPond_Click(Index As Integer)
    Dim oValorLista As CValorPond
    
    If m_bRespetarCarga Then Exit Sub
    
    If Index = 1 Then
        fraPond(1).Visible = True
        fraPond(2).Visible = False
        sdbgContinua.RemoveAll
        If Not g_oVariableCalidad.INTListaPond Is Nothing Then
            For Each oValorLista In g_oVariableCalidad.INTListaPond
                sdbgContinua.AddItem oValorLista.ValorDesde & Chr(9) & oValorLista.ValorHasta & Chr(9) & oValorLista.ValorPond & Chr(9) & oValorLista.indice
            Next
        End If
    Else
        fraPond(2).Visible = True
        fraPond(1).Visible = False
        txtFormula.Text = NullToStr(g_oVariableCalidad.Formula)
    End If
    
    VisualizarGuardar
    Arrange
End Sub

Private Function ValidarPonderacion() As Boolean
    Dim bGuardar As Boolean
    Dim iTipoPond As TVariablesCalPonderacion
    Dim i As Integer
    Dim vbm As Variant
    Dim lInd As Integer
    
    bGuardar = False
    'Si ha cambiado la ponderaci�n la guardo
    If sdbgContinua.DataChanged Then
        sdbgContinua.Update
        If m_bErrorC Then
            ValidarPonderacion = False
            Exit Function
        End If
    End If
    
    If optPond(1).Value Then
        iTipoPond = PondIntEscContinua
        If sdbgContinua.Rows = 0 Then
            Mensajes.NoValido m_sContinua
            ValidarPonderacion = False
            Exit Function
        End If
    
    ElseIf optPond(2).Value Then
        iTipoPond = PondIntFormula
        If txtFormula.Text = "" Then
            Mensajes.NoValido m_sFormula
            ValidarPonderacion = False
            Exit Function
        End If
    End If
        
    If g_oVariableCalidad.TipoPonderacion <> iTipoPond Then
        bGuardar = True
    Else
        If iTipoPond = PondIntEscContinua Then
            If sdbgContinua.Rows <> g_oVariableCalidad.INTListaPond.Count Then
                bGuardar = True
            Else
                For i = 0 To sdbgContinua.Rows - 1
                    vbm = sdbgContinua.AddItemBookmark(i)
                    lInd = i + 1
                    If Not g_oVariableCalidad.INTListaPond.Item(CStr(lInd)) Is Nothing Then
                        If VarToDec0(sdbgContinua.Columns("DESDE").CellValue(vbm)) <> VarToDec0(g_oVariableCalidad.INTListaPond.Item(CStr(lInd)).ValorDesde) Then
                            bGuardar = True
                            Exit For
                        End If
                        If VarToDec0(sdbgContinua.Columns("HASTA").CellValue(vbm)) <> VarToDec0(g_oVariableCalidad.INTListaPond.Item(CStr(lInd)).ValorHasta) Then
                            bGuardar = True
                            Exit For
                        End If
                        If VarToDec0(sdbgContinua.Columns("PUNTOS").CellValue(vbm)) <> VarToDec0(g_oVariableCalidad.INTListaPond.Item(CStr(lInd)).ValorPond) Then
                            bGuardar = True
                            Exit For
                        End If
                    Else
                        bGuardar = True
                        Exit For
                    End If
                Next
            End If
        ElseIf iTipoPond = PondIntFormula Then
            If ValidarFormula(txtFormula.Text, False) Then
                If txtFormula.Text <> g_oVariableCalidad.Formula Then
                    bGuardar = True
                End If
            Else
                ValidarPonderacion = False
                Exit Function
            End If
    
        End If
    End If
    
    If optOpcionConf(0).Value And g_oVariableCalidad.Opcion_Conf <> VarCalOpcionConf.EvaluarFormula Then
        bGuardar = True
    ElseIf optOpcionConf(1).Value Then
        If sdbcVarPond.Text = "" Then
            Mensajes.NoValido Left(lblOpcionConf.Caption, Len(lblOpcionConf.Caption) - 1)
            Exit Function
        End If
        If sdbcVarPond.Text <> NullToStr(g_oVariableCalidad.IdVar_Pond) Then
            bGuardar = True
        End If
    Else
        If optOpcionConf(2).Value And g_oVariableCalidad.Opcion_Conf <> VarCalOpcionConf.NoCalcular Then bGuardar = True
    End If
    
    If bGuardar Then
        If Not GuardarPondInt Then
           ValidarPonderacion = False
        Else
           ValidarPonderacion = True
        End If
    Else
        ValidarPonderacion = True
    End If
End Function

Private Function GuardarPondInt() As Boolean
    Dim vbm As Variant
    Dim i As Integer
    
    On Error GoTo Error
    'Guardo la punt cuando se guarde todo en el form principal de variables
    g_oVariableCalidad.modificado = True
    VisualizarGuardar
    Arrange
    
    If optPond(1).Value Then
        g_oVariableCalidad.TipoPonderacion = PondIntEscContinua
        g_oVariableCalidad.Formula = Null
        g_oVariableCalidad.IDFormula = Null
        Set g_oVariableCalidad.CamposPond = Nothing
        g_oVariableCalidad.NCPeriodo = Null
        Set g_oVariableCalidad.INTListaPond = Nothing
        Set g_oVariableCalidad.INTListaPond = Raiz.Generar_CValoresPond
        
        With g_oVariableCalidad.INTListaPond
            For i = 0 To sdbgContinua.Rows - 1
                vbm = sdbgContinua.AddItemBookmark(i)
                .Add , , , , , i + 1, , sdbgContinua.Columns("PUNTOS").CellValue(vbm), sdbgContinua.Columns("DESDE").CellValue(vbm), sdbgContinua.Columns("HASTA").CellValue(vbm), , i + 1
            Next
        End With
    Else
        g_oVariableCalidad.TipoPonderacion = PondIntFormula
        If NullToStr(g_oVariableCalidad.Formula) <> txtFormula.Text Then
            g_oVariableCalidad.IDFormula = Null
        End If
        g_oVariableCalidad.Formula = txtFormula.Text
        Set g_oVariableCalidad.CamposPond = Nothing
        g_oVariableCalidad.NCPeriodo = Null
        Set g_oVariableCalidad.INTListaPond = Nothing
    End If
    
    If optOpcionConf(VarCalOpcionConf.EvaluarFormula).Value = True Then
        g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.EvaluarFormula
        g_oVariableCalidad.IdVar_Pond = ""
    ElseIf optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True Then
        g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana
        g_oVariableCalidad.IdVar_Pond = sdbcVarPond.Columns(0).Value
    Else
        g_oVariableCalidad.Opcion_Conf = VarCalOpcionConf.NoCalcular
        g_oVariableCalidad.IdVar_Pond = ""
    End If
    
    GuardarPondInt = True
    Exit Function
Error:
    GuardarPondInt = False
End Function

Private Sub sdbgContinua_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
End Sub

Private Sub sdbgContinua_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
 DispPromptMsg = 0
End Sub

Private Sub sdbgContinua_BeforeUpdate(Cancel As Integer)
    Dim vValorDesde As Variant
    Dim vValorHasta As Variant
    Dim irespuesta As Integer
    Dim i As Integer
    
    m_bErrorC = False
    If sdbgContinua.Columns("DESDE").Text = "" Then
        If sdbgContinua.Columns("HASTA").Text = "" And sdbgContinua.Columns("PUNTOS").Text = "" Then
            sdbgContinua.CancelUpdate
            Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & " " & m_sMensaje(2)
            If Me.Visible Then sdbgContinua.SetFocus
            Cancel = True
            m_bErrorC = True
        End If
    Else
        If Not IsNumeric(sdbgContinua.Columns("DESDE").Text) Then
            Mensajes.NoValido sdbgContinua.Columns("DESDE").Caption & " " & m_sMensaje(2)
            If Me.Visible Then sdbgContinua.SetFocus
            Cancel = True
            m_bErrorC = True
        Else
            If Not IsNumeric(sdbgContinua.Columns("HASTA").Text) Then
                Mensajes.NoValido sdbgContinua.Columns("HASTA").Caption & " " & m_sMensaje(2)
                If Me.Visible Then sdbgContinua.SetFocus
                Cancel = True
                m_bErrorC = True
            Else
                If Not IsNumeric(sdbgContinua.Columns("PUNTOS").Value) Then
                    Mensajes.NoValida sdbgContinua.Columns("PUNTOS").Caption & " " & m_sMensaje(2)
                    If Me.Visible Then sdbgContinua.SetFocus
                    Cancel = True
                    m_bErrorC = True
                End If
            End If
        End If
        
        If Not m_bErrorC Then
            If VarToDec0(sdbgContinua.Columns("DESDE").Text) > VarToDec0(sdbgContinua.Columns("HASTA").Text) Then
                Mensajes.NoValida m_sMensaje(1) 'sdbgContinua.Columns("DESDE").Caption
                If Me.Visible Then sdbgContinua.SetFocus
                Cancel = True
                m_bErrorC = True
            End If
            If Not m_bErrorC Then
                vValorDesde = VarToDec0(sdbgContinua.Columns("DESDE").Text)
                vValorHasta = VarToDec0(sdbgContinua.Columns("HASTA").Text)
                For i = 0 To sdbgContinua.Rows - 1
                    If sdbgContinua.AddItemRowIndex(sdbgContinua.Bookmark) <> i Then
                        If VarToDec0(vValorDesde) >= VarToDec0(sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i))) And VarToDec0(vValorDesde) < VarToDec0(sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i))) Then
                            'el intervalo definido ya existe
                            Mensajes.NoValido m_sMensaje(0)
                            If Me.Visible Then sdbgContinua.SetFocus
                            Cancel = True
                            m_bErrorC = True
                            Exit Sub
                        End If
                        If VarToDec0(vValorHasta) > VarToDec0(sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i))) And VarToDec0(vValorHasta) < VarToDec0(sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i))) Then
                            'el intervalo definido ya existe
                            Mensajes.NoValida m_sMensaje(0)
                            If Me.Visible Then sdbgContinua.SetFocus
                            Cancel = True
                            m_bErrorC = True
                            Exit Sub
                        End If
                        If VarToDec0(vValorDesde) < VarToDec0(sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i))) And VarToDec0(vValorHasta) > VarToDec0(sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i))) Then
                            'el intervalo introducido contiene uno de los ya existe
                            irespuesta = Mensajes.PreguntaEliminarIntervalo(m_sMensaje(5) & ": " & vbCrLf & sdbgContinua.Columns("DESDE").CellValue(sdbgContinua.AddItemBookmark(i)) & " - " & sdbgContinua.Columns("HASTA").CellValue(sdbgContinua.AddItemBookmark(i)))
                            If irespuesta = vbNo Then
                                MsgBox m_sMensaje(6), vbInformation, "FULLSTEP"
                                If Me.Visible Then sdbgContinua.SetFocus
                                Cancel = True
                                m_bErrorC = True
                                Exit Sub
                            Else
                                sdbgContinua.RemoveItem i
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        End If
    End If
   
End Sub

Private Function ValidarFormula(ByVal sFormula As String, ByVal bNoMens As Boolean) As Boolean
    Dim sVariables() As String
    Dim lErrCode As Integer
    Dim lIndex As Integer
    Dim iEq As USPExpression
    Dim sCaracter As String
    
        
    If sFormula <> "" Then
        Set iEq = New USPExpression
        
        ReDim sVariables(1)
        sVariables(1) = "X"
    
        lIndex = iEq.Parse(UCase(sFormula), sVariables, lErrCode)
    
        If lErrCode <> USPEX_NO_ERROR Then
            ' Parsing error handler
            If bNoMens = False Then
                Select Case lErrCode
                    Case USPEX_DIVISION_BY_ZERO
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(1))
                    Case USPEX_EMPTY_EXPRESSION
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(2))
                    Case USPEX_MISSING_OPERATOR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(3))
                    Case USPEX_SYNTAX_ERROR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(4))
                    Case USPEX_UNKNOWN_FUNCTION
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(5))
                    Case USPEX_UNKNOWN_OPERATOR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(6))
                    Case USPEX_WRONG_PARAMS_NUMBER
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(7))
                    Case USPEX_UNKNOWN_IDENTIFIER
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(8))
                    Case USPEX_UNKNOWN_VAR
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(9))
                    Case USPEX_VARIABLES_NOTUNIQUE
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(10))
                    Case Else
                        sCaracter = Mid(sFormula, lIndex)
                        Mensajes.FormulaIncorrecta (m_sIdiErrorFormula(11) & vbCrLf & sCaracter)
                End Select
            End If
            Set iEq = Nothing
            ValidarFormula = False
            Exit Function
        End If
        Set iEq = Nothing
    Else
        ValidarFormula = False
        Exit Function
    End If
    
    ValidarFormula = True
End Function

Private Sub sdbgContinua_Change()
    VisualizarGuardar
    Arrange
End Sub

Private Sub txtFormula_Change()
    VisualizarGuardar
    Arrange
End Sub

Private Sub optOpcionConf_Click(Index As Integer)
    If Index <> VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
        sdbcVarPond.Text = ""
    End If
    VisualizarGuardar
    Arrange
End Sub

Private Sub sdbcVarPond_Change()
    VisualizarGuardar
    Arrange
End Sub

Private Sub sdbcVarPond_Click()
    VisualizarGuardar
    Arrange
    optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True
End Sub

Private Sub VisualizarGuardar()
    FrmVARCalidad.VisualizarGuardar
    lblCambios.Visible = True
    
    Select Case g_oVariableCalidad.Nivel
    Case 5
        FrmVARCalCompuestaNivel5.lblCambios.Visible = True
        FrmVARCalCompuestaNivel4.lblCambios.Visible = True
        FrmVARCalCompuestaNivel3.lblCambios.Visible = True
    Case 4
        FrmVARCalCompuestaNivel4.lblCambios.Visible = True
        FrmVARCalCompuestaNivel3.lblCambios.Visible = True
    Case 3
        FrmVARCalCompuestaNivel3.lblCambios.Visible = True
    End Select
End Sub

Private Sub sdbcVarPond_InitColumnProps()
    sdbcVarPond.DataFieldList = "Column 0"
    sdbcVarPond.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcVarPond_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcVarPond.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcVarPond.Rows - 1
            bm = sdbcVarPond.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcVarPond.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcVarPond.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub CargarVariablesHermanas()
    Screen.MousePointer = vbHourglass
    Dim oVarCal As CVariableCalidad
    Dim g_oVariablesCalidadHermanas As CVariablesCalidad
    sdbcVarPond.RemoveAll
    
    Select Case g_oVariableCalidad.Nivel
        Case 5
            Set g_oVariablesCalidadHermanas = FrmVARCalCompuestaNivel5.g_oVarCal4Mod.VariblesCal
        Case 4
            Set g_oVariablesCalidadHermanas = FrmVARCalCompuestaNivel4.g_oVarCal3Mod.VariblesCal
        Case 3
            Set g_oVariablesCalidadHermanas = FrmVARCalCompuestaNivel3.g_oVarCal2Mod.VariblesCal
        Case 2
            Set g_oVariablesCalidadHermanas = FrmVARCalidad.g_oVarsCalidad1Mod.Item(FrmVARCalidad.ssTabVar.SelectedItem.Index).VariblesCal
    End Select
    
    For Each oVarCal In g_oVariablesCalidadHermanas
        If oVarCal.Id <> g_oVariableCalidad.Id And oVarCal.BajaLog = False Then
            sdbcVarPond.AddItem oVarCal.Id & Chr(9) & oVarCal.Cod & " - " & oVarCal.Denominaciones.Item(Idioma).Den
            If oVarCal.Id = g_oVariableCalidad.IdVar_Pond Then
                sdbcVarPond.Text = oVarCal.Cod
            End If
        End If
    Next
    
    Screen.MousePointer = vbNormal
End Sub

