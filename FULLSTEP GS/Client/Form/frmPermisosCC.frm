VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPermisosCC 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DCierre parcial del proceso:"
   ClientHeight    =   4185
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8145
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPermisosCC.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4185
   ScaleWidth      =   8145
   Begin SSDataWidgets_B.SSDBGrid sdbgPermisosCC 
      Height          =   3135
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   7995
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   3
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPermisosCC.frx":014A
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1402
      Columns(0).Name =   "CHECK"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   3200
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "CODIGO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   8864
      Columns(2).Caption=   "Denominacion"
      Columns(2).Name =   "DENOMINACION"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      _ExtentX        =   14102
      _ExtentY        =   5530
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   8145
      TabIndex        =   0
      Top             =   3690
      Width           =   8145
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "D&Cancelar"
         Height          =   345
         Left            =   3623
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
   End
   Begin VB.Label lblMensaje 
      BackStyle       =   0  'Transparent
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   7935
   End
End
Attribute VB_Name = "frmPermisosCC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Public g_Usuario As String                              '
Public g_oPres5Asignados As cPresConceptos5Nivel0       '
Public g_bModoConsulta As Boolean                       '
'*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Private m_oArbolesPres5 As cPresConceptos5Nivel0
Private m_perfil As String
Private m_usuario As String
Private m_mensaje As String

Public MDIScaleWidth As Single
Public MDIScaleHeight As Single
Public sIdioma As String
Public GestorIdiomas As CGestorIdiomas
Public Raiz As CRaiz

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_PERMISOSCC, sIdioma)  '407
    
    If Not Ador Is Nothing Then
        
        m_perfil = Ador(0).Value  '1 Perfil
        Ador.MoveNext
        lblMensaje.Caption = Ador(0).Value  '2 Seleccione los �rboles de partidas de control presupuestario que podr� gestionar el perfil:
        Ador.MoveNext
        'sdbgPermisosCC.caption = ador(0).Value '3 Arboles de partidas de control de aprovisionamiento
        Ador.MoveNext
        'sdbgPermisosCC.Columns(0).caption = ador(0).Value = ""
        sdbgPermisosCC.Columns(1).Caption = Ador(0).Value  'C�digo
        Ador.MoveNext
        sdbgPermisosCC.Columns(2).Caption = Ador(0).Value  'Denominaci�n
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value     'Cerrar
        Ador.MoveNext
        m_usuario = Ador(0).Value     'Usuario
        Ador.MoveNext
        m_mensaje = Ador(0).Value     'Al eliminar el permiso al perfil se eliminar�n los permisos sobre los �rboles de control de aprovisonamiento. �Desea continuar?
        Ador.Close
    End If
    
    Set Ador = Nothing
        
End Sub
Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
CargarRecursos
PonerFieldSeparator Me
Me.Left = MDIScaleWidth / 2 - Me.Width / 2
Me.Top = MDIScaleHeight / 2 - Me.Height / 2
Me.Caption = m_usuario & ": " & g_Usuario

If g_bModoConsulta Then
    sdbgPermisosCC.Columns("CHECK").Visible = False
Else
    sdbgPermisosCC.Columns("CHECK").Visible = True
End If

Set m_oArbolesPres5 = Raiz.Generar_CPresConceptos5Nivel0
CargarGrid
    
End Sub


Private Sub CargarGrid()
Dim oPres5Niv0 As cPresConcep5Nivel0
Dim bEsta As Boolean

    sdbgPermisosCC.RemoveAll
    
    If g_bModoConsulta Then
        For Each oPres5Niv0 In g_oPres5Asignados
            'y lo incluimos en el grid
            sdbgPermisosCC.AddItem "0" & Chr(m_lSeparador) & oPres5Niv0.Cod & Chr(m_lSeparador) & oPres5Niv0.Den
        Next
    Else
        
        m_oArbolesPres5.CargarPresupuestosConceptos5
        For Each oPres5Niv0 In m_oArbolesPres5
            'y lo incluimos en el grid
            If g_oPres5Asignados Is Nothing Then
                bEsta = False
            Else
                If g_oPres5Asignados.Item(CStr(oPres5Niv0.Cod)) Is Nothing Then
                    bEsta = False
                Else
                    bEsta = True
                End If
            End If
            sdbgPermisosCC.AddItem BooleanToSQLBinary(bEsta) & Chr(m_lSeparador) & oPres5Niv0.Cod & Chr(m_lSeparador) & oPres5Niv0.Den
        Next
    End If

End Sub

Public Sub Form_Unload(Cancel As Integer)
    g_Usuario = ""
    Set m_oArbolesPres5 = Nothing
    Set g_oPres5Asignados = Nothing
    m_perfil = ""
    m_usuario = ""
    m_mensaje = ""
    g_bModoConsulta = False
End Sub

Private Sub sdbgPermisosCC_Change()
    
    If sdbgPermisosCC.Col = 0 Then
        If sdbgPermisosCC.Columns("CHECK").Value = "0" Then
            If Not g_oPres5Asignados.Item(CStr(sdbgPermisosCC.Columns("CODIGO").Value)) Is Nothing Then
                g_oPres5Asignados.Remove CStr(sdbgPermisosCC.Columns("CODIGO").Value)
            End If
        Else
            If g_oPres5Asignados.Item(CStr(sdbgPermisosCC.Columns("CODIGO").Value)) Is Nothing Then
                g_oPres5Asignados.Add sdbgPermisosCC.Columns("CODIGO").Value, sdbgPermisosCC.Columns("DENOMINACION").Value, ""
            End If
        End If
    End If
    sdbgPermisosCC.Update
End Sub

Private Sub sdbgPermisosCC_InitColumnProps()
    If g_bModoConsulta Then
        sdbgPermisosCC.Columns("CHECK").Width = 0
        sdbgPermisosCC.Columns("CODIGO").Width = 1815
        sdbgPermisosCC.Columns("DENOMINACION").Width = 5850
    Else
        sdbgPermisosCC.Columns("CHECK").Width = 800
        sdbgPermisosCC.Columns("CODIGO").Width = 1815
        sdbgPermisosCC.Columns("DENOMINACION").Width = 5050
    End If
End Sub
