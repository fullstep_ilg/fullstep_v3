VERSION 5.00
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Begin VB.Form frmAdjProve 
   BackColor       =   &H00808000&
   Caption         =   "Adjudicaciones proveedor"
   ClientHeight    =   4680
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12885
   Icon            =   "frmAdjProve.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4680
   ScaleWidth      =   12885
   StartUpPosition =   2  'CenterScreen
   Begin UltraGrid.SSUltraGrid sdbgAdjProve 
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   12615
      _ExtentX        =   22251
      _ExtentY        =   7646
      _Version        =   131072
      GridFlags       =   17040384
      LayoutFlags     =   72351744
      Override        =   "frmAdjProve.frx":014A
      Appearance      =   "frmAdjProve.frx":01A0
      Caption         =   "sdbgAdjProve"
   End
End
Attribute VB_Name = "frmAdjProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private msProveCod As String
Private msProveDen As String
Private miAnyo As Integer
Private msGMN1 As String
Private miProce As Long
Private msMoneda As String
Private mdblCambio As Double
Private mvTextos As Variant
Private msIdioma As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Public m_oFSGSRaiz As CRaiz
Public m_sIdioma As String
Public m_sIdiomaPortal As String
Public m_gsDEN_GMN1 As String
Public m_gsDEN_GMN2 As String
Public m_gsDEN_GMN3 As String
Public m_gsDEN_GMN4 As String

Public Property Let ProveCod(ByVal sNewValue As String)
    msProveCod = sNewValue
End Property
Public Property Let ProveDen(ByVal sNewValue As String)
    msProveDen = sNewValue
End Property
Public Property Let Anyo(ByVal iNewValue As Integer)
    miAnyo = iNewValue
End Property
Public Property Let GMN1(ByVal sNewValue As String)
    msGMN1 = sNewValue
End Property
Public Property Let Proce(ByVal iNewValue As Long)
    miProce = iNewValue
End Property
Public Property Let Moneda(ByVal sNewValue As String)
    msMoneda = sNewValue
End Property
Public Property Let Cambio(ByVal dblNewValue As Double)
    mdblCambio = dblNewValue
End Property
Public Property Let Textos(ByVal vNewValue As Variant)
    mvTextos = vNewValue
End Property
Private Sub Form_Activate()
If Not m_oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If Err.Number <> 0 Then
        m_bDescargarFrm = m_oFSGSRaiz.TratarError("Form", "frmAdjProve", "Form_Activate", Err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
Private Sub Form_Load()
    Dim oAdjudicaciones As CAdjudicaciones
If Not m_oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
m_bDescargarFrm = False
    If Not IsEmpty(m_sIdiomaPortal) Then
        msIdioma = m_sIdiomaPortal
    Else
        msIdioma = m_sIdioma
    End If
    
    'Cargar datos
    Set oAdjudicaciones = m_oFSGSRaiz.Generar_CAdjudicaciones
    Set sdbgAdjProve.DataSource = oAdjudicaciones.DevolverAdjudicacionesProveedor(msProveCod, msIdioma, msMoneda, mdblCambio)
    sdbgAdjProve.CollapseAll
    
    'Configurar grid
    ConfigurarGrid
    
    CargarRecursos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If Err.Number <> 0 Then
        m_bDescargarFrm = m_oFSGSRaiz.TratarError("Form", "frmAdjProve", "Form_Load", Err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not Me.WindowState = vbMinimized Then
        sdbgAdjProve.Left = 0
        sdbgAdjProve.Top = 0
        sdbgAdjProve.Width = Me.Width - 100
        sdbgAdjProve.Height = Me.Height - 400
        
        'Redimensionar las columnas del grid
        sdbgAdjProve.Bands(0).Columns("GMN1").Width = (sdbgAdjProve.Width - 900) * 0.1
        sdbgAdjProve.Bands(0).Columns("GMN2").Width = (sdbgAdjProve.Width - 900) * 0.1
        sdbgAdjProve.Bands(0).Columns("GMN3").Width = (sdbgAdjProve.Width - 900) * 0.1
        sdbgAdjProve.Bands(0).Columns("GMN4").Width = (sdbgAdjProve.Width - 900) * 0.1
        sdbgAdjProve.Bands(0).Columns("DEN_" & msIdioma).Width = (sdbgAdjProve.Width - 900) * 0.6
        
        sdbgAdjProve.Bands(1).Columns("ART").Width = (sdbgAdjProve.Width - 1200) * 0.12
        sdbgAdjProve.Bands(1).Columns("DESCR").Width = (sdbgAdjProve.Width - 1200) * 0.27
        sdbgAdjProve.Bands(1).Columns("DEST").Width = (sdbgAdjProve.Width - 1200) * 0.07
        sdbgAdjProve.Bands(1).Columns("UNI").Width = (sdbgAdjProve.Width - 1200) * 0.07
        sdbgAdjProve.Bands(1).Columns("PREC_VALIDO").Width = (sdbgAdjProve.Width - 1200) * 0.1
        sdbgAdjProve.Bands(1).Columns("CANT_ADJ").Width = (sdbgAdjProve.Width - 1200) * 0.1
        sdbgAdjProve.Bands(1).Columns("PORCEN").Width = (sdbgAdjProve.Width - 1200) * 0.07
        sdbgAdjProve.Bands(1).Columns("FECINI").Width = (sdbgAdjProve.Width - 1200) * 0.1
        sdbgAdjProve.Bands(1).Columns("FECFIN").Width = (sdbgAdjProve.Width - 1200) * 0.1
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub
''' <summary>Configura la apariencia del grid</summary>
''' <remarks>Llamada desde: CargarAdjudicacionesProveedor; Tiempo m�ximo: 0</remarks>
Private Sub ConfigurarGrid()
    Dim oGrupo As SSGroup
    Dim oCol As SSColumn
    
If Not m_oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAdjProve.Bands(0).Columns("PROVE").Hidden = True
    
    sdbgAdjProve.Bands(1).Columns("GMN1").Hidden = True
    sdbgAdjProve.Bands(1).Columns("GMN2").Hidden = True
    sdbgAdjProve.Bands(1).Columns("GMN3").Hidden = True
    sdbgAdjProve.Bands(1).Columns("GMN4").Hidden = True
    
    sdbgAdjProve.Bands(1).Columns("PREC_VALIDO").Format = "#,##0.00"
    sdbgAdjProve.Bands(1).Columns("CANT_ADJ").Format = "#,##0.00"
    sdbgAdjProve.Bands(1).Columns("PORCEN").Format = "#,##0.00"
    
    Set oGrupo = sdbgAdjProve.Bands(1).Groups.Add("Articulos", 0, "Art�culos")
    For Each oCol In sdbgAdjProve.Bands(1).Columns
        oGrupo.Columns.Add oCol
    Next
    
    sdbgAdjProve.Bands(0).Override.CellAppearance.BackColor = &H80000018
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If Err.Number <> 0 Then
        m_bDescargarFrm = m_oFSGSRaiz.TratarError("Form", "frmAdjProve", "ConfigurarGrid", Err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
''' <summary>Carga los textos del grid</summary>
''' <remarks>Llamada desde: CargarAdjudicacionesProveedor; Tiempo m�ximo: 0</remarks>
Private Sub CargarRecursos()
If Not m_oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.Caption = mvTextos(0) & ": " & msProveCod & "-" & msProveDen
    
    sdbgAdjProve.Bands(0).Columns("GMN1").Header.Caption = m_gsDEN_GMN1
    sdbgAdjProve.Bands(0).Columns("GMN2").Header.Caption = m_gsDEN_GMN2
    sdbgAdjProve.Bands(0).Columns("GMN3").Header.Caption = m_gsDEN_GMN3
    sdbgAdjProve.Bands(0).Columns("GMN4").Header.Caption = m_gsDEN_GMN4
    sdbgAdjProve.Bands(0).Columns("DEN_" & msIdioma).Header.Caption = mvTextos(1)
    
    sdbgAdjProve.Bands(1).Columns("ART").Header.Caption = mvTextos(2)
    sdbgAdjProve.Bands(1).Columns("DESCR").Header.Caption = mvTextos(1)
    sdbgAdjProve.Bands(1).Columns("DEST").Header.Caption = mvTextos(3)
    sdbgAdjProve.Bands(1).Columns("UNI").Header.Caption = mvTextos(4)
    sdbgAdjProve.Bands(1).Columns("PREC_VALIDO").Header.Caption = mvTextos(8) & " " & msMoneda
    sdbgAdjProve.Bands(1).Columns("CANT_ADJ").Header.Caption = mvTextos(5)
    sdbgAdjProve.Bands(1).Columns("PORCEN").Header.Caption = "%"
    sdbgAdjProve.Bands(1).Columns("FECINI").Header.Caption = mvTextos(6)
    sdbgAdjProve.Bands(1).Columns("FECFIN").Header.Caption = mvTextos(7)
    
    sdbgAdjProve.Bands(1).Groups(0).Header.Caption = mvTextos(9)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If Err.Number <> 0 Then
        m_bDescargarFrm = m_oFSGSRaiz.TratarError("Form", "frmAdjProve", "CargarRecursos", Err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

