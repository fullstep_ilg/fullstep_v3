VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CtiposDeDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum FormCampoAyudaTipoAyuda
    CampoInstancia = 0
    CampoFormulario = 1
    CampoPredefinido = 2
End Enum

Public Type DatosUsuario
    Idioma As String
    Perfil As Long
    Comprador As CComprador
    CodEqp As Variant
    CodPersona As Variant
    RDest As Boolean
    RMat As Boolean
    RComp As Boolean
    RCompResp As Boolean
    REqp As Boolean
    RUsuAper As Boolean
    RUsuUON As Boolean
    RUsuDep As Boolean
    RPerfUON As Boolean
    RProvMatComp As Boolean
    Cod As Variant
    UON1 As Variant
    UON2 As Variant
    UON3 As Variant
    CodDep As Variant
    SoloInvitado As Boolean
End Type
