VERSION 5.00
Begin VB.Form frmDetalleCampoExterno 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DDetalle de campo"
   ClientHeight    =   990
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5790
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetalleCampoExterno.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   990
   ScaleWidth      =   5790
   StartUpPosition =   1  'CenterOwner
   Begin VB.Label lblTipoCampo 
      BackColor       =   &H00808000&
      Caption         =   "Label2"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   1700
      TabIndex        =   3
      Top             =   600
      Width           =   3700
   End
   Begin VB.Label lblPredef 
      BackColor       =   &H00808000&
      Caption         =   "Label1"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   1700
      TabIndex        =   2
      Top             =   160
      Width           =   3700
   End
   Begin VB.Label lblLitTipo 
      BackColor       =   &H00808000&
      Caption         =   "DTipo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1400
   End
   Begin VB.Label lblLitPredef 
      BackColor       =   &H00808000&
      Caption         =   "DPredefinido:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   160
      Width           =   1400
   End
End
Attribute VB_Name = "frmDetalleCampoExterno"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Tipo As TiposDeAtributos
Public DenoTabla As String

Private m_sIdiTipo(5 To 8) As String
Private m_slblPredef As String


Private Sub Form_Load()
    Me.Height = 1485
    Me.Width = 5625
    
    CargarRecursos
    Me.Caption = Me.Caption & " " & DenoTabla
    
    lblPredef.Caption = m_slblPredef & " (" & DenoTabla & ")"
    'lblTipoCampo.caption = m_sIdiTipo(8)
     
     Select Case Tipo
         Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
            lblTipoCampo.Caption = m_sIdiTipo(8)
         Case TipoNumerico
            lblTipoCampo.Caption = m_sIdiTipo(5)
         Case TipoFecha
            lblTipoCampo.Caption = m_sIdiTipo(6)
         Case TipoBoolean
            lblTipoCampo.Caption = m_sIdiTipo(7)
     End Select
End Sub

Private Sub CargarRecursos()
    Dim i As Integer
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = GestorIdiomas.DevolverTextosDelModulo(FRM_DETALLE_CAMPO_EXTERNO, Idioma)
    
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value    '1 Detalle de campo externo:
        ador.MoveNext
        lblLitPredef.Caption = ador(0).Value '2 Externo:
        ador.MoveNext
        m_slblPredef = ador(0).Value '3 Campo externo
        ador.MoveNext
        lblLitTipo = ador(0).Value '4 Tipo
   
        For i = 5 To 8
            ador.MoveNext
            m_sIdiTipo(i) = ador(0).Value
        Next i
        
        ador.Close
    End If
    
    Set ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    DenoTabla = ""
End Sub









