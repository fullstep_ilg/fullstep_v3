VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Begin VB.Form frmREUAgenda 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Agenda+"
   ClientHeight    =   5580
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   9915
   Icon            =   "frmREUAgenda.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5580
   ScaleWidth      =   9915
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   415
      Index           =   1
      Left            =   0
      Picture         =   "frmREUAgenda.frx":0CB2
      ScaleHeight     =   420
      ScaleWidth      =   420
      TabIndex        =   2
      Top             =   0
      Width           =   415
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   415
      Index           =   0
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   420
      TabIndex        =   1
      Top             =   0
      Width           =   415
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   1000
      Top             =   480
   End
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   6000
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10005
      ExtentX         =   17648
      ExtentY         =   10583
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
End
Attribute VB_Name = "frmREUAgenda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public UsuarioSummit As CUsuario
Public Year As Integer
Public Month As Integer
Public Day As Integer
Public Hour As Integer
Public Min As Integer
Public Seg As Integer
Public RutasFSNWeb As String
Public URLSesSrv As String

Private Sub Form_Load()
    Dim strSessionId As String
    strSessionId = DevolverSessionIdEstablecido(UsuarioSummit, URLSesSrv)
    
    Dim sURL As String
    sURL = RutasFSNWeb & "/App_Pages/GS/Reuniones/SelectorPlantilla.aspx"
    sURL = sURL & "?desdeGS=1&SessionId=" & strSessionId & "&tipoDocumento=1&a=" & Year & "&m=" & Month & "&d=" & Day & "&h=" & Hour & "&mn=" & Min & "&s=" & Seg
    Picture1(0).Width = 10005
    Picture1(0).Height = 6000
    Picture1(1).Left = (Picture1(0).Width - Picture1(1).Width) / 2
    Picture1(1).Top = (Picture1(0).Height - Picture1(1).Height) / 2
    WebBrowser1.Navigate2 sURL, 4
    Timer1.Enabled = True
End Sub

Private Sub Timer1_Timer()
    If WebBrowser1.ReadyState = READYSTATE_COMPLETE Then
        Picture1(0).Visible = False
        Picture1(1).Visible = False
        Timer1.Enabled = False
    End If
End Sub

Private Sub WebBrowser1_WindowClosing(ByVal IsChildWindow As Boolean, Cancel As Boolean)
    Me.Hide
End Sub


