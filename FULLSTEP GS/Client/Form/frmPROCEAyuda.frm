VERSION 5.00
Begin VB.Form frmPROCEAyuda 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ayuda para la b�squeda"
   ClientHeight    =   2955
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5475
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROCEAyuda.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2955
   ScaleWidth      =   5475
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   2002
      TabIndex        =   0
      Top             =   2565
      Width           =   1470
   End
   Begin VB.Line Line7 
      X1              =   5355
      X2              =   5355
      Y1              =   150
      Y2              =   2355
   End
   Begin VB.Line Line6 
      X1              =   1050
      X2              =   1050
      Y1              =   165
      Y2              =   2340
   End
   Begin VB.Line Line5 
      X1              =   75
      X2              =   75
      Y1              =   135
      Y2              =   2355
   End
   Begin VB.Line Line4 
      X1              =   75
      X2              =   5355
      Y1              =   2340
      Y2              =   2340
   End
   Begin VB.Line Line3 
      X1              =   75
      X2              =   5340
      Y1              =   1560
      Y2              =   1560
   End
   Begin VB.Line Line2 
      X1              =   75
      X2              =   5340
      Y1              =   780
      Y2              =   780
   End
   Begin VB.Line Line1 
      X1              =   75
      X2              =   5355
      Y1              =   150
      Y2              =   150
   End
   Begin VB.Label Label7 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   75
      TabIndex        =   7
      Top             =   150
      Width           =   5295
   End
   Begin VB.Label Label6 
      Caption         =   "Mostrar� todos los resultados que finalicen con la cadena DIV"
      Height          =   630
      Left            =   1215
      TabIndex        =   6
      Top             =   1725
      Width           =   4155
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "*XXX"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   90
      TabIndex        =   5
      Top             =   1725
      Width           =   1125
   End
   Begin VB.Label Label4 
      Caption         =   "Mostrar� todos los resultados que contengan la cadena DIV"
      Height          =   750
      Left            =   1215
      TabIndex        =   4
      Top             =   975
      Width           =   4155
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "*XXX*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Left            =   75
      TabIndex        =   3
      Top             =   975
      Width           =   1155
   End
   Begin VB.Label Label2 
      Caption         =   "Mostrar� todos los resultados que empiecen por DIV"
      Height          =   660
      Left            =   1215
      TabIndex        =   2
      Top             =   315
      Width           =   4155
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "XXX*"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   660
      Left            =   75
      TabIndex        =   1
      Top             =   315
      Width           =   1155
   End
End
Attribute VB_Name = "frmPROCEAyuda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    CargarRecursos
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next

    Set ador = GestorIdiomas.DevolverTextosDelModulo(FRM_PROCEAYUDA, Idioma)

    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Label2.Caption = ador(0).Value
        ador.MoveNext
        Label4.Caption = ador(0).Value
        ador.MoveNext
        Label6.Caption = ador(0).Value
        ador.MoveNext
        cmdCerrar.Caption = ador(0).Value
    End If
    ador.Close
    Set ador = Nothing
End Sub

