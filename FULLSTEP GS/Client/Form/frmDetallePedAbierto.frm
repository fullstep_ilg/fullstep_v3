VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmDetallePedAbierto 
   Caption         =   "DInformaci�n de pedidos contra pedidos abierto"
   ClientHeight    =   5010
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   14760
   Icon            =   "frmDetallePedAbierto.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5010
   ScaleWidth      =   14760
   StartUpPosition =   2  'CenterScreen
   Begin SSDataWidgets_B.SSDBGrid sdbgPedidos 
      Height          =   4785
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   14460
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      GroupHeaders    =   0   'False
      Col.Count       =   21
      stylesets.count =   6
      stylesets(0).Name=   "GreyImg"
      stylesets(0).BackColor=   12632256
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmDetallePedAbierto.frx":0CB2
      stylesets(0).AlignmentPicture=   2
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmDetallePedAbierto.frx":0EF0
      stylesets(2).Name=   "NoSelected"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   16777215
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmDetallePedAbierto.frx":0F0C
      stylesets(3).Name=   "Selected"
      stylesets(3).ForeColor=   16777215
      stylesets(3).BackColor=   8388608
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmDetallePedAbierto.frx":0F28
      stylesets(4).Name=   "Grey"
      stylesets(4).BackColor=   12632256
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmDetallePedAbierto.frx":0F44
      stylesets(5).Name=   "NoHom"
      stylesets(5).ForeColor=   16777215
      stylesets(5).BackColor=   12632256
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmDetallePedAbierto.frx":0F60
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      CaptionAlignment=   0
      Columns.Count   =   21
      Columns(0).Width=   1429
      Columns(0).Caption=   "DA�o"
      Columns(0).Name =   "ANYO"
      Columns(0).Alignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   11
      Columns(0).FieldLen=   256
      Columns(1).Width=   1402
      Columns(1).Caption=   "DPedido"
      Columns(1).Name =   "PEDIDO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1482
      Columns(2).Caption=   "DOrden"
      Columns(2).Name =   "ORDEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   2646
      Columns(3).Caption=   "DPedido ERP"
      Columns(3).Name =   "NUM_PED_ERP"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3572
      Columns(4).Caption=   "DAprovisionador"
      Columns(4).Name =   "APROV"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   2408
      Columns(5).Caption=   "DFec. Emisi�n"
      Columns(5).Name =   "FEC_EMISION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   2408
      Columns(6).Caption=   "DEstado actual"
      Columns(6).Name =   "EST"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).Style=   3
      Columns(7).Width=   3200
      Columns(7).Caption=   "DFec. Inicio"
      Columns(7).Name =   "FEC_INI"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Caption=   "DFec. Fin"
      Columns(8).Name =   "FEC_FIN"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   1826
      Columns(9).Caption=   "DN� L�nea"
      Columns(9).Name =   "NUM_LIN"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   2117
      Columns(10).Caption=   "DEstado l�nea"
      Columns(10).Name=   "EST_LIN"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3096
      Columns(11).Caption=   "DCantidad Pedida"
      Columns(11).Name=   "CANT_PED"
      Columns(11).Alignment=   1
      Columns(11).CaptionAlignment=   0
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).NumberFormat=   "#,##0.00####################"
      Columns(11).FieldLen=   256
      Columns(11).Locked=   -1  'True
      Columns(12).Width=   3200
      Columns(12).Caption=   "DCantidad Pedida"
      Columns(12).Name=   "CANT_PED_PA"
      Columns(12).Alignment=   1
      Columns(12).CaptionAlignment=   0
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Caption=   "DImporte pedido"
      Columns(13).Name=   "IMPORTE_PED"
      Columns(13).Alignment=   1
      Columns(13).CaptionAlignment=   0
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Caption=   "DImporte Pedido"
      Columns(14).Name=   "IMP_PED_PA"
      Columns(14).Alignment=   1
      Columns(14).CaptionAlignment=   0
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   1535
      Columns(15).Caption=   "DU.P."
      Columns(15).Name=   "UNI"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   5
      Columns(15).NumberFormat=   "#,##0.0####################"
      Columns(15).FieldLen=   256
      Columns(15).Locked=   -1  'True
      Columns(16).Width=   3200
      Columns(16).Caption=   "DMoneda"
      Columns(16).Name=   "MON"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      Columns(17).Width=   3200
      Columns(17).Caption=   "PRES1"
      Columns(17).Name=   "PRES1"
      Columns(17).DataField=   "Column 17"
      Columns(17).DataType=   8
      Columns(17).FieldLen=   256
      Columns(18).Width=   3200
      Columns(18).Caption=   "PRES2"
      Columns(18).Name=   "PRES2"
      Columns(18).DataField=   "Column 18"
      Columns(18).DataType=   8
      Columns(18).FieldLen=   256
      Columns(19).Width=   3200
      Columns(19).Caption=   "PRES3"
      Columns(19).Name=   "PRES3"
      Columns(19).DataField=   "Column 19"
      Columns(19).DataType=   8
      Columns(19).FieldLen=   256
      Columns(20).Width=   3200
      Columns(20).Caption=   "PRES4"
      Columns(20).Name=   "PRES4"
      Columns(20).DataField=   "Column 20"
      Columns(20).DataType=   8
      Columns(20).FieldLen=   256
      _ExtentX        =   25506
      _ExtentY        =   8440
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDetallePedAbierto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public m_oPedidos As COrdenesEntrega
Public m_oPedidoAbierto As COrdenEntrega
Public Idioma As String
Public TipoPedAbierto As TipoEmisionPedido

Private m_ParamGen As ParametrosGenerales
Private sIdiEst(1 To 11) As String
Private sIdiEstPA(4) As String
Private m_bModoPedidoContraAbierto As Boolean

Public Property Get Pedidos() As COrdenesEntrega
    Set Pedidos = m_oPedidos
End Property

Public Property Set Pedidos(ByRef oPedidos As COrdenesEntrega)
    Set m_oPedidos = oPedidos
    m_bModoPedidoContraAbierto = True
End Property

Public Property Get PedidoAbierto() As COrdenEntrega
    Set PedidoAbierto = m_oPedidoAbierto
End Property

Public Property Set PedidoAbierto(ByRef oPedidoAbierto As COrdenEntrega)
    Set m_oPedidoAbierto = oPedidoAbierto
    m_bModoPedidoContraAbierto = False
End Property

Public Property Get ParamGenerales() As ParametrosGenerales
    ParamGenerales = m_ParamGen
End Property

Public Property Let ParamGenerales(ByRef ParamGen As ParametrosGenerales)
    m_ParamGen = ParamGen
End Property

Private Sub Form_Load()
    PonerFieldSeparator Me
    CargarRecursos
    ConfigurarGrid
    CargarGrid
End Sub

''' <summary>Configuraci�n de las columnas del grid</summary>
''' <remarks>Llamada desde: Form_load</remarks>

Private Sub ConfigurarGrid()
    With sdbgPedidos
        If m_bModoPedidoContraAbierto Then
            .Columns("FEC_INI").Visible = False
            .Columns("FEC_FIN").Visible = False
            .Columns("CANT_PED_PA").Visible = False
            .Columns("IMP_PED_PA").Visible = False
            .Columns("EST_LIN").Visible = False
            
            If m_ParamGen.gbUsarPres1 And m_ParamGen.gbUsarPedPres1 Then
                .Columns("PRES1").Visible = True
            Else
                .Columns("PRES1").Visible = False
            End If
            If m_ParamGen.gbUsarPres2 And m_ParamGen.gbUsarPedPres2 Then
                .Columns("PRES2").Visible = True
            Else
                .Columns("PRES2").Visible = False
            End If
            If m_ParamGen.gbUsarPres3 And m_ParamGen.gbUsarPedPres3 Then
                .Columns("PRES3").Visible = True
            Else
                .Columns("PRES3").Visible = False
            End If
            If m_ParamGen.gbUsarPres4 And m_ParamGen.gbUsarPedPres4 Then
                .Columns("PRES4").Visible = True
            Else
                .Columns("PRES4").Visible = False
            End If
            .Columns("CANT_PED").Visible = (TipoPedAbierto = PedidoAbiertoCantidad)
            .Columns("IMPORTE_PED").Visible = (TipoPedAbierto = PedidoAbiertoImporte)
            .Columns("UNI").Visible = (TipoPedAbierto = PedidoAbiertoCantidad)
            .Columns("MON").Visible = (TipoPedAbierto = PedidoAbiertoImporte)
        Else
            .Columns("PRES1").Visible = False
            .Columns("PRES2").Visible = False
            .Columns("PRES3").Visible = False
            .Columns("PRES4").Visible = False
            .Columns("EST_LIN").Visible = True
            
            Select Case m_oPedidoAbierto.PedidoAbiertoTipo
                Case TipoEmisionPedido.PedidoAbiertoCantidad
                    .Columns("IMP_PED_PA").Visible = False
                    .Columns("IMPORTE_PED").Visible = False
                    .Columns("MON").Visible = False
                Case TipoEmisionPedido.PedidoAbiertoImporte
                    .Columns("CANT_PED_PA").Visible = False
                    .Columns("CANT_PED").Visible = False
                    .Columns("UNI").Visible = False
            End Select
        End If
        .Columns("NUM_PED_ERP").Visible = m_ParamGen.gbPedidosERP
    End With
End Sub

''' <summary>Carga los idiomas del formulario</summary>
''' <remarks>Llamada desde: Form_load</remarks>

Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset
    
    On Error Resume Next

    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_DETPEDDESDEPA, Idioma)

    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        With sdbgPedidos
            .Columns("ANYO").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("PEDIDO").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("ORDEN").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("APROV").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("FEC_EMISION").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("EST").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("NUM_LIN").Caption = Ador(0).Value
            Ador.MoveNext
            If m_bModoPedidoContraAbierto Then
                .Columns("CANT_PED").Caption = Ador(0).Value
            Else
                .Columns("CANT_PED_PA").Caption = Ador(0).Value
            End If
            Ador.MoveNext
            .Columns("UNI").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("CCOSTE").Caption = Ador(0).Value
            Ador.MoveNext
            sIdiEst(1) = Ador(0).Value
            Ador.MoveNext
            sIdiEst(2) = Ador(0).Value
            Ador.MoveNext
            sIdiEst(3) = Ador(0).Value
            Ador.MoveNext
            sIdiEst(4) = Ador(0).Value
            Ador.MoveNext
            sIdiEst(5) = Ador(0).Value
            Ador.MoveNext
            sIdiEst(6) = Ador(0).Value
            Ador.MoveNext
            sIdiEst(7) = Ador(0).Value
            Ador.MoveNext
            sIdiEst(8) = Ador(0).Value
            Ador.MoveNext
            sIdiEst(9) = Ador(0).Value
            Ador.MoveNext
            sIdiEst(10) = Ador(0).Value
            Ador.MoveNext
            If m_bModoPedidoContraAbierto Then
                .Columns("IMPORTE_PED").Caption = Ador(0).Value
            Else
                .Columns("IMP_PED_PA").Caption = Ador(0).Value
            End If
            Ador.MoveNext
            .Columns("MON").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("FEC_INI").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("FEC_FIN").Caption = Ador(0).Value
            Ador.MoveNext
            If Not m_bModoPedidoContraAbierto Then .Columns("CANT_PED").Caption = Ador(0).Value
            Ador.MoveNext
            If Not m_bModoPedidoContraAbierto Then .Columns("IMPORTE_PED").Caption = Ador(0).Value
            Ador.MoveNext
            sIdiEstPA(0) = Ador(0).Value
            Ador.MoveNext
            sIdiEstPA(1) = Ador(0).Value
            Ador.MoveNext
            sIdiEstPA(2) = Ador(0).Value
            Ador.MoveNext
            sIdiEstPA(3) = Ador(0).Value
            Ador.MoveNext
            .Columns("EST_LIN").Caption = Ador(0).Value
            Ador.MoveNext
            .Columns("NUM_PED_ERP").Caption = Ador(0).Value
            Ador.MoveNext 'borrado
            sIdiEst(11) = Ador(0).Value
            sIdiEstPA(4) = Ador(0).Value
            
            .Columns("PRES1").Caption = m_ParamGen.gsSingPres1
            .Columns("PRES2").Caption = m_ParamGen.gsSingPres2
            .Columns("PRES3").Caption = m_ParamGen.gsSingPres3
            .Columns("PRES4").Caption = m_ParamGen.gsSingPres4
        End With
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Private Sub CargarGrid()
    Dim oOrden As COrdenEntrega
    
    If m_bModoPedidoContraAbierto Then
        If Not Pedidos Is Nothing Then
            If Pedidos.Count > 0 Then
                For Each oOrden In Pedidos
                    AgregarLineaGrid oOrden
                Next
                
                Set oOrden = Nothing
            End If
        End If
    Else
        AgregarLineaGrid m_oPedidoAbierto
    End If
End Sub

Private Sub AgregarLineaGrid(ByRef oOrden As COrdenEntrega)
    Dim oLinea As CLineaPedido
    Dim sAprov As String
    Dim sLinea As String
    Dim dImpPedido As Double
    Dim dImpPedidoPA As Double
    
    If Not oOrden.Persona Is Nothing Then
        If Not IsNull(oOrden.Persona.nombre) Then sAprov = oOrden.Persona.nombre & " "
        If Not IsNull(oOrden.Persona.Apellidos) Then sAprov = sAprov & oOrden.Persona.Apellidos
    End If
                        
    For Each oLinea In oOrden.LineasPedido
        dImpPedido = (oLinea.importePedido * IIf(IsNull(oOrden.Cambio), 1, oOrden.Cambio))
        dImpPedidoPA = (oLinea.ImportePedidoPA * IIf(IsNull(oOrden.Cambio), 1, oOrden.Cambio))
        
        sLinea = oLinea.AnyoPedido & Chr(m_lSeparador) & oLinea.Pedido & Chr(m_lSeparador) & oOrden.Numero & Chr(m_lSeparador) & NullToStr(oOrden.NumPedidoERP) & Chr(m_lSeparador) & sAprov & Chr(m_lSeparador) & oOrden.FechaEmision
        sLinea = sLinea & Chr(m_lSeparador) & DenEstado(oOrden.Estado, oOrden.EstaDeBajaLogica)
        If m_bModoPedidoContraAbierto Then
            sLinea = sLinea & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        Else
            sLinea = sLinea & Chr(m_lSeparador) & oOrden.PedidoAbiertoFechaInicio & Chr(m_lSeparador) & oOrden.PedidoAbiertoFechaFin
        End If
        sLinea = sLinea & Chr(m_lSeparador) & Format(oLinea.Num, "000")
        If m_bModoPedidoContraAbierto Then
            sLinea = sLinea & Chr(m_lSeparador) & ""
        Else
            sLinea = sLinea & Chr(m_lSeparador) & DenEstado(IIf(NullToDbl0(oLinea.Estado) = 0, oOrden.Estado, oLinea.Estado), False)
        End If
        sLinea = sLinea & Chr(m_lSeparador) & oLinea.CantidadPedida
        If m_bModoPedidoContraAbierto Then
            sLinea = sLinea & Chr(m_lSeparador) & ""
        Else
            sLinea = sLinea & Chr(m_lSeparador) & oLinea.CantidadPedidaPA
        End If
        sLinea = sLinea & Chr(m_lSeparador) & FormateoNumerico(dImpPedido, "#,##0.00")
        If m_bModoPedidoContraAbierto Then
            sLinea = sLinea & Chr(m_lSeparador) & ""
        Else
            sLinea = sLinea & Chr(m_lSeparador) & FormateoNumerico(dImpPedidoPA, "#,##0.00")
        End If
        sLinea = sLinea & Chr(m_lSeparador) & oLinea.UnidadPedido & Chr(m_lSeparador) & oOrden.Moneda
        
        If m_bModoPedidoContraAbierto Then
            'Presupuestos
            If oOrden.Tipo <> TipoPedido.Aprovisionamiento Then
                If m_ParamGen.gbUsarPres1 And m_ParamGen.gbUsarPedPres1 Then
                    sLinea = sLinea & Chr(m_lSeparador) & StringDePresupuestos(oLinea.Presupuestos1, 1)
                Else
                    sLinea = sLinea & Chr(m_lSeparador) & ""
                End If
                
                If m_ParamGen.gbUsarPres2 And m_ParamGen.gbUsarPedPres2 Then
                    sLinea = sLinea & Chr(m_lSeparador) & StringDePresupuestos(oLinea.Presupuestos2, 2)
                Else
                    sLinea = sLinea & Chr(m_lSeparador) & ""
                End If
                
                If m_ParamGen.gbUsarPres3 And m_ParamGen.gbUsarPedPres3 Then
                    sLinea = sLinea & Chr(m_lSeparador) & StringDePresupuestos(oLinea.Presupuestos3, 3)
                Else
                    sLinea = sLinea & Chr(m_lSeparador) & ""
                End If
                
                If m_ParamGen.gbUsarPres4 And m_ParamGen.gbUsarPedPres4 Then
                    sLinea = sLinea & Chr(m_lSeparador) & StringDePresupuestos(oLinea.Presupuestos4, 4)
                Else
                    sLinea = sLinea & Chr(m_lSeparador) & ""
                End If
            Else
                sLinea = sLinea & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
            End If
        Else
            sLinea = sLinea & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        End If
        
        sdbgPedidos.AddItem sLinea
    Next
    
    Set oLinea = Nothing
End Sub

Public Function StringDePresupuestos(ByVal oPresupuestos As Object, ByVal iTipo As Integer) As String
    Dim oPres As Object
    Dim sPresup As String
    
    If oPresupuestos Is Nothing Then
        StringDePresupuestos = ""
        Exit Function
    End If
    
    For Each oPres In oPresupuestos
        If sPresup <> "" Then sPresup = sPresup & "; "
        If iTipo < 3 Then
            sPresup = sPresup & oPres.Anyo & "-"
        End If
        sPresup = sPresup & oPres.CodPRES1
        If oPres.CodPRES2 <> "" Then
            sPresup = sPresup & "-" & oPres.CodPRES2
            If oPres.CodPRES3 <> "" Then
                sPresup = sPresup & "-" & oPres.CodPRES3
                If oPres.Cod <> "" Then
                    sPresup = sPresup & "-" & oPres.Cod
                End If
            End If
        End If
    Next

    StringDePresupuestos = sPresup
End Function

Private Function DenEstado(ByVal Estado As TipoEstadoLineaPedido, ByVal EstaDeBaja As Boolean) As String
    If m_bModoPedidoContraAbierto Then
        If EstaDeBaja Then
            DenEstado = sIdiEst(11)
        Else
            Select Case Estado
                Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                    DenEstado = sIdiEst(4)
                Case TipoEstadoOrdenEntrega.Anulado
                    DenEstado = sIdiEst(8)
                Case TipoEstadoOrdenEntrega.DenegadoParcialAprob
                    DenEstado = sIdiEst(2)
                Case TipoEstadoOrdenEntrega.DenegadoTotalAprobador
                    DenEstado = sIdiEst(10)
                Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                    DenEstado = sIdiEst(3)
                Case TipoEstadoOrdenEntrega.EnCamino
                    DenEstado = sIdiEst(5)
                Case TipoEstadoOrdenEntrega.EnRecepcion
                    DenEstado = sIdiEst(6)
                Case TipoEstadoOrdenEntrega.PendienteDeAprobacion
                    DenEstado = sIdiEst(1)
                Case TipoEstadoOrdenEntrega.RechazadoPorProveedor
                    DenEstado = sIdiEst(9)
                Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                    DenEstado = sIdiEst(7)
            End Select
        End If
    Else
        If EstaDeBaja Then
            DenEstado = sIdiEstPA(4)
        Else
            Select Case Estado
                Case TipoEstadoPedidoAbierto.PANoVigente
                    DenEstado = sIdiEstPA(0)
                Case TipoEstadoPedidoAbierto.PAAbierto
                    DenEstado = sIdiEstPA(1)
                Case TipoEstadoPedidoAbierto.PACerrado
                    DenEstado = sIdiEstPA(2)
                Case TipoEstadoPedidoAbierto.PAAnulado
                    DenEstado = sIdiEstPA(3)
            End Select
        End If
    End If
End Function

Private Sub Form_Resize()
    If Me.WindowState = vbMinimized Then Exit Sub

    If Me.Height < 1665 Then Me.Height = 1665
    If Me.Width < 3645 Then Me.Width = 3645
    
    With sdbgPedidos
        .Left = 120
        .Top = 120
        .Width = Me.ScaleWidth - 240
        .Height = Me.ScaleHeight - 240
                
        If m_bModoPedidoContraAbierto Then
            .Columns("ANYO").Width = .Width * 6 / 100
            .Columns("PEDIDO").Width = .Width * 7 / 100
            .Columns("ORDEN").Width = .Width * 7 / 100
            .Columns("APROV").Width = .Width * 15 / 100
            .Columns("FEC_EMISION").Width = .Width * 9 / 100
            .Columns("FEC_INI").Width = .Width * 9 / 100
            .Columns("FEC_FIN").Width = .Width * 9 / 100
            .Columns("EST").Width = .Width * 14 / 100
            .Columns("MON").Width = .Width * 7 / 100
        Else
            .Columns("ANYO").Width = .Width * 5 / 100
            .Columns("PEDIDO").Width = .Width * 5 / 100
            .Columns("ORDEN").Width = .Width * 5 / 100
            .Columns("APROV").Width = .Width * 12 / 100
            .Columns("FEC_EMISION").Width = .Width * 8 / 100
            .Columns("FEC_INI").Width = .Width * 8 / 100
            .Columns("FEC_FIN").Width = .Width * 8 / 100
            .Columns("EST").Width = .Width * 9 / 100
            .Columns("EST_LIN").Width = .Width * 9 / 100
            .Columns("MON").Width = .Width * 5 / 100
        End If
        .Columns("NUM_PED_ERP").Width = .Width * 10 / 100
        .Columns("NUM_LIN").Width = .Width * 6 / 100
        .Columns("CANT_PED").Width = .Width * 10 / 100
        .Columns("CANT_PED_PA").Width = .Width * 10 / 100
        .Columns("IMPORTE_PED").Width = .Width * 10 / 100
        .Columns("IMP_PED_PA").Width = .Width * 10 / 100
        .Columns("UNI").Width = .Width * 5 / 100
        .Columns("PRES1").Width = .Width * 16 / 100
        .Columns("PRES2").Width = .Width * 16 / 100
        .Columns("PRES3").Width = .Width * 16 / 100
        .Columns("PRES4").Width = .Width * 16 / 100
    End With
End Sub
