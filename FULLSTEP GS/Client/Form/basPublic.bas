Attribute VB_Name = "basPublic"
Option Explicit

Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

''' <summary>Muestra frmSOLAyudaCalculos</summary>
''' <param name="oGestorIdiomas">Gestor de Idiomas</param>
''' <param name="sIdioma">Idioma</param>
''' <remarks> Llamada desde: frmRESREU</remarks>

Public Sub MostrarFormSOLAyudaCalculosLocal(ByRef oGestorIdiomas As CGestorIdiomas, ByVal sIdioma As String)
    Dim oFrm As frmSOLAyudaCalculos
    
    Set oFrm = New frmSOLAyudaCalculos
    With oFrm
        Set .GestorIdiomas = oGestorIdiomas
        oFrm.Idioma = sIdioma
        
        .Show vbModal
    End With
        
    Set oFrm = Nothing
End Sub

''' <summary>Muestra frmVARCalMaterial</summary>
''' <param name="oGestorIdiomas">Gestor idiomas</param>
''' <param name="sIdioma">Idioma</param>
''' <param name="sOrigen">Origen</param>
''' <param name="oFSGSRaiz">Objeto Raiz</param>
''' <param name="oLongitudesDeCodigos">Longitudes de c�digo</param>
''' <param name="oMensajes">Objeto Mensajes</param>
''' <remarks> Llamada desde: frmActualizarAhorros</remarks>

Public Sub MostrarFormAhorrosPendLocal(ByRef oGestorIdiomas As CGestorIdiomas, ByVal sIdioma As String, ByVal sOrigen As String, ByRef oFSGSRaiz As CRaiz, ByRef oLongitudesDeCodigos As LongitudesDeCodigos, _
        ByRef oMensajes As CMensajes, Optional ByRef oErrores As Variant)
    Dim oFrm As frmAhorrosPend
    
    Set oFrm = New frmAhorrosPend
    With oFrm
        Set .GestorIdiomas = oGestorIdiomas
        Set .Raiz = oFSGSRaiz
        Set .Mensajes = oMensajes
        If Not IsMissing(oErrores) Then .Errores = oErrores
        .gLongitudesDeCodigos = oLongitudesDeCodigos
        .Idioma = sIdioma
        .Origen = sOrigen
        
        .Show vbModal
    End With
                 
    Unload oFrm
    Set oFrm = Nothing
End Sub

''' <summary>Muestra frmESTRORGBuscar</summary>
''' <param name="oGestorIdiomas">Gestor de Idiomas</param>
''' <param name="Idioma">Idioma</param>
''' <param name="Idiomas">Idiomas</param>
''' <returns>Array con las opciones de filtrado</returns>
''' <remarks> Llamada desde: frmVARCalPondCert</remarks>

Public Sub MostrarFormFormCampoAyudaLocal(ByRef oGestorIdiomas As CGestorIdiomas, ByVal Idioma As String, ByRef oFSGSRaiz As CRaiz, ByRef oMensajes As CMensajes, ByRef oGestorSeguridad As CGestorSeguridad, _
        ByVal UsuCod As String, ByRef oParametrosGenerales As ParametrosGenerales, ByVal TipoAyuda As FormCampoAyudaTipoAyuda, ByVal bModif As Boolean, Optional ByRef oCampoSeleccionado As CFormItem, _
        Optional ByRef oIdiomas As CIdiomas, Optional ByRef oCampoPredef As CCampoPredef)
    Dim oFrm As frmFormCampoAyuda
    
    Set oFrm = New frmFormCampoAyuda
    With oFrm
        .Idioma = Idioma
        Set .GestorIdiomas = oGestorIdiomas
        Set .Raiz = oFSGSRaiz
        Set .Idiomas = oIdiomas
        Set .CampoSeleccionado = oCampoSeleccionado
        Set .CampoPredef = oCampoPredef
        Set .Mensajes = oMensajes
        Set .GestorSeguridad = oGestorSeguridad
        .UsuCod = UsuCod
        .ParametrosGenerales = oParametrosGenerales
        .TipoAyuda = TipoAyuda
        .Modif = bModif
    
        .Show vbModal
    End With
    
    Unload oFrm
    Set oFrm = Nothing
End Sub
