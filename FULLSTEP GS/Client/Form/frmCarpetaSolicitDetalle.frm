VERSION 5.00
Begin VB.Form frmCarpetaSolicitDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalle"
   ClientHeight    =   1320
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4665
   Icon            =   "frmCarpetaSolicitDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1320
   ScaleWidth      =   4665
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   960
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   5
      Top             =   900
      Width           =   3825
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   1
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   2
         Top             =   60
         Width           =   1050
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1290
      Left            =   1365
      ScaleHeight     =   1290
      ScaleWidth      =   3915
      TabIndex        =   4
      Top             =   0
      Width           =   3915
      Begin VB.TextBox txtDen 
         Height          =   285
         Left            =   60
         MaxLength       =   500
         TabIndex        =   0
         Top             =   240
         Width           =   3165
      End
   End
   Begin VB.Label lblDen 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominación:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   60
      TabIndex        =   3
      Top             =   300
      Width           =   1380
   End
End
Attribute VB_Name = "frmCarpetaSolicitDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String

Private Sub cmdAceptar_Click()
    If Me.Visible Then txtDen.SetFocus
    If Me.txtDen <> "" Then
        Me.Hide 'Lo descargo en el Formulario que lo ha llamado
    End If
End Sub

Private Sub cmdCancelar_Click()
    txtDen.Text = ""
    If Me.Visible Then txtDen.SetFocus
    Me.Hide 'Lo descargo en el Formulario que lo ha llamado
End Sub

Private Sub Form_Load()
    If Me.Top < 0 Then
        Me.Top = 0
    End If
    If Me.Left < 0 Then
        Me.Left = 0
    End If
    
    CargarRecursos
End Sub

Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_CARPETASOLICITDETALLE, Idioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value '1
        Ador.MoveNext
        lblDen.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
                
        Ador.Close
    End If
End Sub

