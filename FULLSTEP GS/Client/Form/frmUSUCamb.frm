VERSION 5.00
Begin VB.Form frmUSUCamb 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cambio de contrase�a"
   ClientHeight    =   4815
   ClientLeft      =   4515
   ClientTop       =   3570
   ClientWidth     =   4560
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmUSUCamb.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4815
   ScaleWidth      =   4560
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox PicNoLogin 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   850
      Left            =   60
      ScaleHeight     =   855
      ScaleWidth      =   4455
      TabIndex        =   10
      Top             =   0
      Width           =   4455
      Begin VB.Label Label1 
         BackColor       =   &H00808000&
         Caption         =   "DPor favor, modifique la contrase�a:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   12
         Top             =   480
         Width           =   4215
      End
      Begin VB.Label Label2 
         BackColor       =   &H00808000&
         Caption         =   "DSegun la pol�tica de seguridad definida, su contrase�a ha expirado."
         ForeColor       =   &H00FFFFFF&
         Height          =   405
         Left            =   0
         TabIndex        =   11
         Top             =   0
         Width           =   4215
      End
   End
   Begin VB.PictureBox picButtons 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   4560
      TabIndex        =   8
      Top             =   4320
      Width           =   4560
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   2370
         TabIndex        =   3
         Top             =   90
         Width           =   1215
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   1110
         TabIndex        =   2
         Top             =   90
         Width           =   1155
      End
   End
   Begin VB.PictureBox picNoADM 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1650
      Left            =   60
      ScaleHeight     =   1590
      ScaleWidth      =   4395
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   900
      Width           =   4455
      Begin VB.TextBox txtContrNuevaConf 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   1080
         Width           =   2100
      End
      Begin VB.TextBox txtContrNueva 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   0
         Top             =   660
         Width           =   2100
      End
      Begin VB.TextBox txtContrActual 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         Locked          =   -1  'True
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   60
         Width           =   2100
      End
      Begin VB.Label lblConf1 
         BackColor       =   &H00808000&
         Caption         =   "Confirmaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1140
         Width           =   2085
      End
      Begin VB.Label lblPwdNue 
         BackColor       =   &H00808000&
         Caption         =   "Nueva contrase�a:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   2085
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         BorderWidth     =   2
         X1              =   120
         X2              =   4260
         Y1              =   480
         Y2              =   480
      End
      Begin VB.Label lblPwdAct 
         BackColor       =   &H00808000&
         Caption         =   "Contrase�a actual:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   120
         Width           =   2085
      End
   End
End
Attribute VB_Name = "frmUSUCamb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public VieneDeMain As Boolean
Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public UsuarioSummit As CUsuario
Public Mensajes As CMensajes
Public AceptarClicked As Boolean

Private iAdm As Integer
Private sConfUsu As String
Private sPwdAct As String
Private sConfPwd As String
Private sUsuario As String
Private oParamGen As ParametrosGenerales
'Multilenguaje
Private sIdiTitulo As String

Public Property Let ParamGen(ByVal vNewValue As Variant)
    oParamGen = vNewValue
End Property

''' <summary>
''' Evento que se genera al hacer click sobre el bot�n de aceptar del formulario de cambio de contrase�a, comprobando qe cumple con los requisitos y que no es erronea
''' </summary>
''' <remarks>
''' Llamadas desde: Autom�tica, siempre que se haga click sobre el bot�n de aceptar del formulario
''' Tiempo m�ximo: 0,3 seg
''' </remarks>
Private Sub cmdAceptar_Click()
    Dim sUsuActual As String
    Dim sPwdActual As String
    Dim sUsuNuevo As String
    Dim sPwdNuevo As String
    Dim fecpwd As Date
        
    If VieneDeMain And iAdm <> 1 Then
         If txtContrNuevaConf.Text = txtContrNueva.Text And txtContrActual.Text = txtContrNueva.Text Then
            MsgBox Mensajes.CargarTextoMensaje(556), vbExclamation, "FULLSTEP" 'La nueva contrase�a es igual a la actual,
            txtContrNuevaConf.Text = ""
            txtContrNueva.Text = ""
            Exit Sub
         End If
         If txtContrNueva.Text <> txtContrNuevaConf.Text Then
            MsgBox Mensajes.CargarTextoMensaje(5) & " " & sConfPwd, vbExclamation, "FULLSTEP" 'Las contrase�as son distintas
            txtContrNuevaConf.Text = ""
            Exit Sub
        End If
        
        sUsuActual = UsuarioSummit.Cod
        sPwdActual = txtContrActual.Text
        sUsuNuevo = UsuarioSummit.Cod
        sPwdNuevo = txtContrNueva.Text
    Else
        ''' Comprobaci�n de igualdad
        'Compruebo si hay Autenticaci�n windows (o LDAP) en GS o WEB
        If Not ((oParamGen.giWinSecurity = Windows Or oParamGen.giWinSecurity = LDAP) _
        And (oParamGen.giWinSecurityWeb = Windows Or oParamGen.giWinSecurityWeb = LDAP)) Then
            If txtContrActual.Text = txtContrNueva.Text Then
                Mensajes.MensajeOKOnly 556 'La nueva contrase�a es igual a la actual,
                Exit Sub
            End If
            If txtContrNueva.Text <> txtContrNuevaConf.Text Then
                Mensajes.NoValida sConfPwd
                Exit Sub
            End If
            If ((oParamGen.giWinSecurity = Windows Or oParamGen.giWinSecurity = LDAP) _
            Or (oParamGen.giWinSecurityWeb = Windows Or oParamGen.giWinSecurity = LDAP)) _
            And Trim(txtContrNueva.Text) <> "" And Trim(txtContrNuevaConf.Text) <> "" Then
                If Trim(txtContrNueva) = "" Then
                    Mensajes.NoValido Replace(lblPwdNue.Caption, ":", "", , , vbTextCompare)
                    If Me.Visible Then txtContrNueva.SetFocus
                    Exit Sub
                End If
                If Trim(txtContrNuevaConf) = "" Then
                    Mensajes.NoValido lblPwdNue.Caption
                    If Me.Visible Then txtContrNuevaConf.SetFocus
                    Exit Sub
                End If
            End If
            sPwdNuevo = txtContrNueva.Text
        Else
            sPwdNuevo = ""
        End If
        
        sUsuActual = UsuarioSummit.Cod
        sPwdActual = txtContrActual.Text
        sUsuNuevo = UsuarioSummit.Cod
    End If
    
    fecpwd = Now
    
    Screen.MousePointer = vbHourglass
    
    Dim oWebSvc As FSGSLibrary.CWebService
    Set oWebSvc = New FSGSLibrary.CWebService
    If oWebSvc.LlamarWebServiceCambioPWD(oParamGen, 0, 0, sUsuActual, sPwdActual, sUsuNuevo, sPwdNuevo, UsuarioSummit.fecpwd, Mensajes, fecpwd) Then
        UsuarioSummit.PWDDes = sPwdNuevo
        UsuarioSummit.fecpwd = fecpwd
        Dim sPassEncript As String
        sPassEncript = FSGSLibrary.EncriptarAES(UsuarioSummit.Cod, UsuarioSummit.PWDDes, True, UsuarioSummit.fecpwd, 1, TIpoDeUsuario.Persona)
        UsuarioSummit.Pwd = sPassEncript
        
        AceptarClicked = True
        Me.Hide
    Else
        txtContrNuevaConf.Text = ""
        txtContrNueva.Text = ""
    End If
    Set oWebSvc = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Evento que se genera al hacer click sobre el bot�n de cancelar del formulario de cambio de contrase�a, comprobando de donde viene la apertura del formulario para cerrar el formulario o la sesion en caso de venir del inicio
''' </summary>
''' <remarks>
''' Llamadas desde: Autom�tica, siempre que se haga click sobre el bot�n de cancelar del formulario
''' Tiempo m�ximo: 0 seg
''' </remarks>
Private Sub cmdCancelar_Click()
    AceptarClicked = False
    Me.Hide
End Sub

Private Sub Form_Load()
    If Not VieneDeMain Then
        picNoADM.Top = 60
        PicNoLogin.Visible = False
    Else
        picNoADM.Top = 900
        PicNoLogin.Visible = True
    End If
    
    CargarRecursos
        
    UsuarioSummit.PWDOld = UsuarioSummit.PWDDes
    iAdm = 0
    Me.picNoADM.Visible = True
    If VieneDeMain Then
        Me.Height = 3600
    Else
        Me.Height = 2790
    End If
    txtContrActual.Text = UsuarioSummit.PWDDes
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Carga los textos de la p�gina en los controles
''' </summary>
''' <remarks>
''' Llamadas desde: Page_Load
''' Tiempo m�ximo: 0,1 seg
''' </remarks>
Private Sub CargarRecursos()
    Dim Ador As Adodb.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_USUCAMB, Idioma)
    
    If Not Ador Is Nothing Then
        lblPwdAct.Caption = Ador(0).Value
        Ador.MoveNext
        lblPwdNue.Caption = Ador(0).Value
        Ador.MoveNext
        lblConf1.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        sUsuario = Ador(0).Value
        Ador.MoveNext
        sConfUsu = Ador(0).Value
        Ador.MoveNext
        sConfPwd = Ador(0).Value
        Ador.MoveNext
        sPwdAct = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiTitulo = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Label2.Caption = Ador(0).Value
        Ador.MoveNext
        Label1.Caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub
