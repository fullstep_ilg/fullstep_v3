VERSION 5.00
Begin VB.Form frmFormularioAnya 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DAnyadir formulario"
   ClientHeight    =   1380
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5310
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormularioAnya.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1380
   ScaleWidth      =   5310
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtCod 
      Height          =   285
      Left            =   1600
      MaxLength       =   50
      TabIndex        =   0
      Top             =   200
      Width           =   3150
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2720
      TabIndex        =   3
      Top             =   940
      Width           =   975
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1520
      TabIndex        =   2
      Top             =   940
      Width           =   975
   End
   Begin VB.CheckBox chkMultiidioma 
      BackColor       =   &H00808000&
      Caption         =   "Multiidioma"
      ForeColor       =   &H00FFFFFF&
      Height          =   192
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   1692
   End
   Begin VB.Label lblNom 
      BackColor       =   &H00808000&
      Caption         =   "C�digo:"
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   240
      TabIndex        =   4
      Top             =   280
      Width           =   1050
   End
End
Attribute VB_Name = "frmFormularioAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Mensajes As CMensajes
Public Raiz As CRaiz
Public Accion As AccionesSummit
Public FormSeleccionado As CFormulario
Public Idiomas As CIdiomas
Public IdiomaInst As String

Private m_sCodigo As String
Private m_sCaptionModif As String
Private m_sCaptionAnya As String
Private m_sDatGen As String
Private m_TesError As TipoErrorSummit

Public Property Get TesError() As TipoErrorSummit
    TesError = m_TesError
End Property

Public Property Let TesError(ByRef vNewValue As TipoErrorSummit)
    m_TesError = vNewValue
End Property

Private Sub Form_Activate()
    If Accion = ACCFormularioAnyadir Then
        chkMultiidioma.Visible = True
        If Me.Visible Then txtCod.SetFocus
    Else
        chkMultiidioma.Visible = False
        cmdAceptar.Top = cmdAceptar.Top - 200
        cmdCancelar.Top = cmdAceptar.Top
        Me.Height = Me.Height - 200
    End If
End Sub

Private Sub Form_Load()
    CargarRecursos
    
    If Accion = ACCFormularioAnyadir Then
        Me.Caption = m_sCaptionAnya
    Else
        Me.Caption = m_sCaptionModif
        txtCod.Text = FormSeleccionado.Den
    End If
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_FORM_ANYA, Idioma)
    
    If Not Ador Is Nothing Then
        m_sCaptionAnya = Ador(0).Value   '1 A�adir formulario
        Ador.MoveNext
        lblNom.Caption = Ador(0).Value & ":"   '2 C�digo:
        m_sCodigo = Ador(0).Value
        Ador.MoveNext
        chkMultiidioma.Caption = Ador(0).Value '3 Multiidioma
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value  '4 &Aceptar
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value '5 &Cancelar
        Ador.MoveNext
        m_sCaptionModif = Ador(0).Value ' 6 Modificar c�digo de formulario
        Ador.MoveNext
        m_sDatGen = Ador(0).Value  '7 Datos generales
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub cmdAceptar_Click()
    Dim oIBaseDatos As IBaseDatos
    Dim Ador As Ador.Recordset
    Dim oForm As CFormulario
    Dim oIdioma As CIdioma
    Dim oMultiidi As CMultiidiomas
    
    If Trim(txtCod.Text) = "" Then
        Mensajes.NoValido m_sCodigo
        If Me.Visible Then txtCod.SetFocus
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Select Case Accion
        Case ACCFormularioModificar
            FormSeleccionado.Den = Trim(txtCod.Text)
            
            Set oIBaseDatos = FormSeleccionado
            m_TesError = oIBaseDatos.FinalizarEdicionModificando
        
        Case ACCFormularioAnyadir
            Set oForm = Raiz.Generar_CFormulario
            oForm.Den = Trim(txtCod.Text)
            If chkMultiidioma.Value = vbChecked Then
                oForm.Multiidioma = True
            Else
                oForm.Multiidioma = False
            End If
            
            Set oForm.Grupos = Raiz.Generar_CFormGrupos
            
            'Carga las denominaciones del grupo de datos generales en todos los idiomas:
            Set oMultiidi = Raiz.Generar_CMultiidiomas
            For Each oIdioma In Idiomas
                If oIdioma.Cod = IdiomaInst Then
                    oMultiidi.Add oIdioma.Cod, m_sDatGen
                Else
                    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_FORM_ANYA, oIdioma.Cod, 7)
                    If Not Ador Is Nothing Then
                        oMultiidi.Add oIdioma.Cod, Ador(0).Value
                    End If
                    Ador.Close
                End If
            Next
            Set Ador = Nothing
            
            oForm.Grupos.Add 1, oMultiidi, , FormSeleccionado
            Set oIBaseDatos = oForm
            m_TesError = oIBaseDatos.AnyadirABaseDatos
            Set m_TesError.Arg1 = oForm
            
            Set oForm = Nothing
    End Select
    
    m_TesError.Arg2 = True
    
    Set oIBaseDatos = Nothing
    Set oMultiidi = Nothing
    
    Screen.MousePointer = vbNormal
    
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    m_TesError.Arg2 = False
    Me.Hide
End Sub



