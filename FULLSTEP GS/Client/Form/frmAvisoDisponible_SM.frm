VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAvisoDisponible_SM 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DControl de disponible"
   ClientHeight    =   3795
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   12420
   ControlBox      =   0   'False
   Icon            =   "frmAvisoDisponible_SM.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3795
   ScaleWidth      =   12420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSi 
      Caption         =   "D&S�"
      Default         =   -1  'True
      Height          =   315
      Left            =   4800
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   3360
      Width           =   1005
   End
   Begin VB.CommandButton cmdNo 
      Cancel          =   -1  'True
      Caption         =   "D&No"
      Height          =   315
      Left            =   6000
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   3360
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPartidas 
      Height          =   2445
      Left            =   150
      TabIndex        =   2
      Top             =   780
      Width           =   12075
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   4
      stylesets.count =   5
      stylesets(0).Name=   "IntOK"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmAvisoDisponible_SM.frx":000C
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmAvisoDisponible_SM.frx":0028
      stylesets(2).Name=   "ActiveRow"
      stylesets(2).ForeColor=   16777215
      stylesets(2).BackColor=   -2147483647
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmAvisoDisponible_SM.frx":0044
      stylesets(2).AlignmentText=   0
      stylesets(3).Name=   "IntHeader"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmAvisoDisponible_SM.frx":0060
      stylesets(3).AlignmentPicture=   0
      stylesets(4).Name=   "IntKO"
      stylesets(4).BackColor=   255
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmAvisoDisponible_SM.frx":01B8
      AllowUpdate     =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      MaxSelectedRows =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   8520
      Columns(0).Caption=   "PARTIDA"
      Columns(0).Name =   "PARTIDA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "PRESUPUESTADO"
      Columns(1).Name =   "PRESUPUESTADO"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3836
      Columns(2).Caption=   "DISPONIBLE"
      Columns(2).Name =   "DISPONIBLE"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   4710
      Columns(3).Caption=   "SOLICITADO"
      Columns(3).Name =   "SOLICITADO"
      Columns(3).Alignment=   1
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   21299
      _ExtentY        =   4313
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image Imagen1 
      Height          =   480
      Left            =   270
      Picture         =   "frmAvisoDisponible_SM.frx":01D4
      Top             =   120
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image Imagen2 
      Height          =   555
      Left            =   240
      Picture         =   "frmAvisoDisponible_SM.frx":02CA
      Top             =   30
      Visible         =   0   'False
      Width           =   585
   End
   Begin VB.Label Label 
      Caption         =   "D�Desea continuar?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   1020
      TabIndex        =   4
      Top             =   420
      Width           =   11145
   End
   Begin VB.Label Label 
      Caption         =   "DAtenci�n!!!!!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Index           =   0
      Left            =   1020
      TabIndex        =   3
      Top             =   180
      Width           =   11145
   End
End
Attribute VB_Name = "frmAvisoDisponible_SM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Idioma As String
Public g_bOK As Boolean
Public g_oColKO As Collection
Public GestorIdiomas As CGestorIdiomas
Public g_iTipoAviso As Integer
Private Sub CargarRecursos()
Dim Ador As Adodb.Recordset
Dim s As String
On Error Resume Next

Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_AVISODISPONIBLE, Idioma)
 
If Not Ador Is Nothing Then
    Me.Caption = Ador(0).Value '1
    Ador.MoveNext
    s = Ador(0).Value
    Ador.MoveNext
    If g_iTipoAviso = 1 Then
        'Para el texto del aviso
        Label(0).Caption = s & "."
        Label(1).Caption = Ador(0).Value
        Ador.MoveNext
    Else
        'Para el texto del bloqueo
        Ador.MoveNext
        Label(0).Caption = Ador(0).Value & ". " & s & ":"
    End If
    Ador.MoveNext
    cmdSi.Caption = Ador(0).Value
    Ador.MoveNext
    If g_iTipoAviso = 1 Then
        cmdNo.Caption = Ador(0).Value
        Ador.MoveNext
    Else
        Ador.MoveNext
        cmdNo.Caption = Ador(0).Value
    End If
    Ador.MoveNext
    sdbgPartidas.Columns("PARTIDA").Caption = Ador(0).Value
    Ador.MoveNext
    sdbgPartidas.Columns("PRESUPUESTADO").Caption = Ador(0).Value
    Ador.MoveNext
    sdbgPartidas.Columns("DISPONIBLE").Caption = Ador(0).Value
    Ador.MoveNext
    sdbgPartidas.Columns("SOLICITADO").Caption = Ador(0).Value
    Ador.Close
End If
End Sub


Private Sub cmdNo_Click()
Unload Me
End Sub

Private Sub cmdSi_Click()
g_bOK = True
Unload Me
End Sub


Private Sub Form_Load()
Dim i As Integer
Dim sLinea As String
'Cargamos la lista con la colecci�n
g_bOK = False
PonerFieldSeparator Me
CargarRecursos
sdbgPartidas.RemoveAll
For i = 1 To g_oColKO.Count
    sLinea = Replace(g_oColKO(i), "#", Chr(m_lSeparador))
    sdbgPartidas.AddItem sLinea
Next i

Select Case g_iTipoAviso
    Case 1
        Imagen1.Visible = True
    Case 2
        Imagen2.Visible = True
        Label(1).Visible = False
        cmdSi.Visible = False
        cmdNo.Left = cmdSi.Left + (cmdSi.Width)
End Select

Screen.MousePointer = vbNormal
End Sub


