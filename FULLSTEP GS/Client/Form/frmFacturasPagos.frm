VERSION 5.00
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Begin VB.Form frmFacturasPagos 
   Caption         =   "Form1"
   ClientHeight    =   5835
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10230
   Icon            =   "frmFacturasPagos.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5835
   ScaleWidth      =   10230
   StartUpPosition =   1  'CenterOwner
   Begin UltraGrid.SSUltraGrid ssFacturas 
      Height          =   6915
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10845
      _ExtentX        =   19129
      _ExtentY        =   12197
      _Version        =   131072
      GridFlags       =   17040384
      Images          =   "frmFacturasPagos.frx":014A
      LayoutFlags     =   67108864
      Caption         =   "ssFacturas"
   End
End
Attribute VB_Name = "frmFacturasPagos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Raiz As CRaiz

Public g_lLineaPedido As Long
Public g_sArticulo As String
Public g_bGridDesplegado As Boolean
Public g_lIdOrden As Long
Public g_sOrden As String

Private m_sIdiFacturas(6) As String
Private m_sIdiPagos(3) As String

Private Sub Form_Load()
    CargarRecursos
    CargarGridFacturas
    
    If g_bGridDesplegado Then
        ssFacturas.ExpandAll
    Else
        ssFacturas.CollapseAll
    End If
    
End Sub

''' <summary>
''' Evento que salta cndo se redimensiona el formulario
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub Form_Resize()
    ssFacturas.Height = Me.ScaleHeight
    ssFacturas.Width = Me.ScaleWidth
    ssFacturas.Bands(0).Columns("ANYO").Width = ssFacturas.Width * 0.06
    ssFacturas.Bands(0).Columns("NUM").Width = ssFacturas.Width * 0.16
    ssFacturas.Bands(0).Columns("ESTADODEN").Width = ssFacturas.Width * 0.3
    ssFacturas.Bands(0).Columns("FECHA").Width = ssFacturas.Width * 0.1
    ssFacturas.Bands(0).Columns("IMPORTE").Width = ssFacturas.Width * 0.2
    ssFacturas.Bands(0).Columns("MONEDA").Width = ssFacturas.Width * 0.08
End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_lLineaPedido = 0
    g_sArticulo = ""
End Sub

''' <summary>
''' Proceso que se encarga de cargar los idiomas del formulario en el idioma del usuario.
''' </summary>
''' <remarks>Tiempo m�ximo=0,1seg.</remarks>
Private Sub CargarRecursos()
Dim rs As Ador.Recordset

    On Error Resume Next
    
    Set rs = GestorIdiomas.DevolverTextosDelModulo(FRM_FACTURASPAGOS, Idioma)
    If Not rs Is Nothing Then
        Me.Caption = rs(0).Value & ": " & g_sArticulo '1 Facturas del art�culo
        rs.MoveNext
        m_sIdiFacturas(1) = rs(0).Value 'A�o
        rs.MoveNext
        m_sIdiFacturas(2) = rs(0).Value 'Numero factura
        rs.MoveNext
        m_sIdiPagos(1) = rs(0).Value 'Numero factura
        rs.MoveNext
        m_sIdiFacturas(3) = rs(0).Value 'Estado
        m_sIdiPagos(2) = rs(0).Value 'Estado
        rs.MoveNext
        m_sIdiFacturas(4) = rs(0).Value 'Fecha
        m_sIdiPagos(3) = rs(0).Value
   
        rs.MoveNext
        If g_lLineaPedido = 0 Then
            Me.Caption = rs(0).Value & ": " & g_sOrden '1 Facturas del art�culo
        End If
        
        rs.MoveNext
        m_sIdiFacturas(5) = rs(0).Value 'Importe
        rs.MoveNext
        m_sIdiFacturas(6) = rs(0).Value 'Importe
   
        rs.Close
    End If

    
End Sub

''' <summary>
''' Garga el grid con los datos de las facturas y sus pagos. (Grid jer�rquico )
''' </summary>
''' <remarks>Tiempo m�ximo=0,1seg.</remarks>
Sub CargarGridFacturas()

    Dim oFacturas As CFacturas
    Set oFacturas = Raiz.Generar_CFacturas
    If g_lLineaPedido = 0 Then
        Set ssFacturas.DataSource = oFacturas.DevolverFacturasYPagosOrden(g_lIdOrden, Idioma)
    Else
        Set ssFacturas.DataSource = oFacturas.DevolverFacturasYPagos(g_lLineaPedido, Idioma)
    End If
    Set oFacturas = Nothing

End Sub

''' <summary>
''' Configura la grid de facturas.
''' </summary>
''' <param name="Context">las del evento</param>
''' <param name="Layout">las del evento</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub ssFacturas_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
    ssFacturas.Bands(0).Columns("FACID").Hidden = True
    If Not g_lLineaPedido = 0 Then
        ssFacturas.Bands(0).Columns("LINEA_PEDIDO").Hidden = True
    End If
    ssFacturas.Bands(0).Columns("ANYO").Header.VisiblePosition = 1
    ssFacturas.Bands(0).Columns("NUM").Header.VisiblePosition = 2
    ssFacturas.Bands(0).Columns("ESTADODEN").Header.VisiblePosition = 3
    ssFacturas.Bands(0).Columns("FECHA").Header.VisiblePosition = 4
    ssFacturas.Bands(0).Columns("IMPORTE").Header.VisiblePosition = 5
    ssFacturas.Bands(0).Columns("MONEDA").Header.VisiblePosition = 6

    ssFacturas.Bands(0).Columns("ANYO").Header.Caption = m_sIdiFacturas(1) ' "A�o"
    ssFacturas.Bands(0).Columns("NUM").Header.Caption = m_sIdiFacturas(2) '"N�mero factura"
    ssFacturas.Bands(0).Columns("ESTADODEN").Header.Caption = m_sIdiFacturas(3) '"Estado"
    ssFacturas.Bands(0).Columns("FECHA").Header.Caption = m_sIdiFacturas(4) '"Fecha"
    ssFacturas.Bands(0).Columns("IMPORTE").Header.Caption = m_sIdiFacturas(5) '"Importe"
    ssFacturas.Bands(0).Columns("IMPORTE").Format = "#,##0.00"
    ssFacturas.Bands(0).Columns("MONEDA").Header.Caption = m_sIdiFacturas(6) '"Moneda"
    
    ssFacturas.Bands(0).Columns("ANYO").Activation = ssActivationActivateNoEdit
    ssFacturas.Bands(0).Columns("NUM").Activation = ssActivationActivateNoEdit
    ssFacturas.Bands(0).Columns("ESTADODEN").Activation = ssActivationActivateNoEdit
    ssFacturas.Bands(0).Columns("FECHA").Activation = ssActivationActivateNoEdit
    ssFacturas.Bands(0).Columns("IMPORTE").Activation = ssActivationActivateNoEdit
    ssFacturas.Bands(0).Columns("MONEDA").Activation = ssActivationActivateNoEdit
        
    ssFacturas.Bands(1).Columns("ID_FACTURA").Hidden = True
    ssFacturas.Bands(1).Columns("LINEA_PEDIDO").Hidden = True
    ssFacturas.Bands(1).Columns("NUM").Header.VisiblePosition = 1
    ssFacturas.Bands(1).Columns("DEN").Header.VisiblePosition = 2
    ssFacturas.Bands(1).Columns("FECHA").Header.VisiblePosition = 3

    ssFacturas.Bands(1).Columns("NUM").Header.Caption = m_sIdiPagos(1)  '"N�mero pago"
    ssFacturas.Bands(1).Columns("DEN").Header.Caption = m_sIdiPagos(2)  '"Estado"
    ssFacturas.Bands(1).Columns("FECHA").Header.Caption = m_sIdiPagos(3) '"Fecha"
    
    ssFacturas.Bands(1).Columns("NUM").Activation = ssActivationActivateNoEdit
    ssFacturas.Bands(1).Columns("DEN").Activation = ssActivationActivateNoEdit
    ssFacturas.Bands(1).Columns("FECHA").Activation = ssActivationActivateNoEdit
    
    
End Sub
