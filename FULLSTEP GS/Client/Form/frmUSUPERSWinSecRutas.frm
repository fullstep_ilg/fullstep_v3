VERSION 5.00
Begin VB.Form frmUSUPERSWinSecRutas 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ruta por defecto para gesti�n de archivos"
   ClientHeight    =   2175
   ClientLeft      =   1785
   ClientTop       =   4680
   ClientWidth     =   6960
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmUSUPERSWinSecRutas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2175
   ScaleWidth      =   6960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2445
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1830
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   3615
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1830
      Width           =   1005
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   1860
      Left            =   0
      TabIndex        =   2
      Top             =   -60
      Width           =   6945
      Begin VB.OptionButton optRutaTS 
         BackColor       =   &H00808000&
         Caption         =   "Ruta local"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   225
         TabIndex        =   7
         Top             =   225
         Width           =   3390
      End
      Begin VB.OptionButton optRutaTS 
         BackColor       =   &H00808000&
         Caption         =   "Conectar unidad:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   1
         Left            =   225
         TabIndex        =   6
         Top             =   945
         Value           =   -1  'True
         Width           =   3570
      End
      Begin VB.TextBox txtRuta 
         Height          =   315
         Index           =   0
         Left            =   675
         MaxLength       =   255
         TabIndex        =   5
         Top             =   540
         Width           =   6120
      End
      Begin VB.TextBox txtRuta 
         Height          =   315
         Index           =   1
         Left            =   1755
         MaxLength       =   255
         TabIndex        =   4
         Top             =   1305
         Width           =   5040
      End
      Begin VB.ComboBox cmbUnidad 
         Height          =   315
         Left            =   675
         TabIndex        =   3
         Text            =   "Z:"
         Top             =   1305
         Width           =   735
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackColor       =   &H00808000&
         Caption         =   "a"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1350
         TabIndex        =   8
         Top             =   1395
         Width           =   420
      End
   End
End
Attribute VB_Name = "frmUSUPERSWinSecRutas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const Matrix = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

Public GestorIdiomas As CGestorIdiomas
Public Mensajes As CMensajes
Public g_vRutaLocal As Variant
Public g_vUnidad As Variant
Public g_vRutaUnidad As Variant
Public g_bEdicion As Boolean
Public Idioma As String
Public OK As Boolean
Public Datos As Variant

Private strMatrix(26) As String

Private Sub RellenarComboUnidad()
    Dim i As Integer
    Dim Str1 As String
    Dim Str2 As String

    cmbUnidad.Clear
    cmbUnidad.AddItem ""
    
    Str1 = Matrix
    
    For i = 1 To Len(Matrix)
        Str2 = Left(Str1, 1)   'First Character
        Str1 = Right(Str1, (Len(Str1) - 1))   'All but First Character
        strMatrix(i) = Str2 & ":"  'Makes up each row of the Array
        cmbUnidad.AddItem Str2 & ":", i
    Next i
End Sub

Public Function BuscarUnidad(ByVal sLetra As String) As Integer
    Dim i As Integer
    Dim iRes As Integer
    
    iRes = -1
    For i = 1 To UBound(strMatrix)
        If sLetra = strMatrix(i) Then
            iRes = i
            Exit For
        End If
    Next
    BuscarUnidad = iRes
End Function

Private Sub Form_Load()
    On Error Resume Next
        
    CargarRecursos
    RellenarComboUnidad

    If Not IsNull(g_vRutaLocal) Then
        optRutaTS(0).Value = True
        txtRuta(0).Text = g_vRutaLocal
    ElseIf Not IsNull(g_vRutaUnidad) Then
        optRutaTS(1).Value = True
        cmbUnidad.ListIndex = BuscarUnidad(g_vUnidad & ":")
        txtRuta(1).Text = g_vRutaUnidad
    End If
    
    If g_bEdicion Then
        Frame2.Enabled = True
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        Height = 2670
    Else
        Frame2.Enabled = False
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        Height = 2300
    End If
End Sub

Private Sub cmdAceptar_Click()
    Dim oFos As FileSystemObject
    
    If Not optRutaTS(0).Value And Not optRutaTS(1).Value Then
        Mensajes.MensajeOKOnly 712
        Exit Sub
    End If
    
    If optRutaTS(0).Value = True Then
        Set oFos = New FileSystemObject
            
        g_vRutaLocal = txtRuta(0).Text
        g_vRutaUnidad = Null
        g_vUnidad = Null
    ElseIf optRutaTS(1).Value = True Then
        If txtRuta(1).Text = "" Then
            Mensajes.MensajeOKOnly 712
            Exit Sub
        End If
        If cmbUnidad.ListIndex = -1 Then
            Mensajes.MensajeOKOnly 712
            Exit Sub
        End If
        g_vUnidad = Left(strMatrix(cmbUnidad.ListIndex), 1)
        g_vRutaUnidad = txtRuta(1).Text
        g_vRutaLocal = Null
    End If
    
    ReDim Datos(0 To 2)
    Datos(0) = g_vRutaLocal
    Datos(1) = g_vRutaUnidad
    Datos(2) = g_vUnidad
    OK = True
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    OK = False
    Me.Hide
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_USU_PERS_WINSEC, Idioma)
    
    If Not Ador Is Nothing Then
        For i = 1 To 17
            If i = 6 Then cmdAceptar.Caption = Ador(0).Value
            If i = 7 Then cmdCancelar.Caption = Ador(0).Value
            Ador.MoveNext
        Next
        Caption = Ador(0).Value
        Ador.MoveNext
        optRutaTS(0).Caption = Ador(0).Value
        Ador.MoveNext
        optRutaTS(1).Caption = Ador(0).Value
        Ador.MoveNext
        Label6.Caption = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Private Sub optRutaTS_Click(Index As Integer)
    If Index = 0 Then
        txtRuta(0).Locked = False
        cmbUnidad.ListIndex = -1
        cmbUnidad.Locked = True
        txtRuta(1).Text = ""
        txtRuta(1).Locked = True
    Else
        cmbUnidad.Locked = False
        txtRuta(1).Locked = False
        txtRuta(0).Locked = True
        txtRuta(0).Text = ""
    End If
End Sub
