VERSION 5.00
Begin VB.Form frmCatalogoEsp 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   7485
   ClientLeft      =   1290
   ClientTop       =   2775
   ClientWidth     =   9735
   Icon            =   "frmCatalogoEsp.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7485
   ScaleWidth      =   9735
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   3630
      ScaleHeight     =   420
      ScaleWidth      =   2535
      TabIndex        =   15
      Top             =   6960
      Width           =   2535
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Height          =   315
         Left            =   195
         TabIndex        =   7
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   8
         Top             =   60
         Width           =   1050
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   6900
      Index           =   2
      Left            =   30
      TabIndex        =   11
      Top             =   -30
      Width           =   4455
      Begin VB.TextBox txtArticuloEsp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000012&
         Height          =   6345
         Index           =   2
         Left            =   60
         MaxLength       =   2000
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   0
         Top             =   420
         Width           =   4260
      End
      Begin VB.Label lblArticuloEsp 
         BackColor       =   &H00808000&
         Caption         =   "Especificaciones de la l�nea de cat�logo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   14
         Top             =   180
         Width           =   4185
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   3450
      Index           =   1
      Left            =   4620
      TabIndex        =   10
      Top             =   3420
      Width           =   5025
      Begin VB.TextBox txtArticuloEsp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000012&
         Height          =   2865
         Index           =   1
         Left            =   660
         MaxLength       =   2000
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   6
         Top             =   420
         Width           =   4260
      End
      Begin VB.CommandButton cmdPasarEspALinea 
         Height          =   315
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoEsp.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1650
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarEspDesdeLinea 
         Height          =   315
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoEsp.frx":100B
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1290
         Width           =   495
      End
      Begin VB.Label lblArticuloEsp 
         BackColor       =   &H00808000&
         Caption         =   "Especificaciones del proveedor/art�culo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   195
         Index           =   1
         Left            =   720
         TabIndex        =   13
         Top             =   180
         Width           =   4095
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   3450
      Index           =   0
      Left            =   4620
      TabIndex        =   9
      Top             =   -30
      Width           =   5025
      Begin VB.CommandButton cmdPasarEspDesdeLinea 
         Height          =   315
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoEsp.frx":1365
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   1410
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarEspALinea 
         Height          =   315
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoEsp.frx":16BF
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   1770
         Width           =   495
      End
      Begin VB.TextBox txtArticuloEsp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000012&
         Height          =   2865
         Index           =   0
         Left            =   660
         MaxLength       =   800
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   3
         Top             =   420
         Width           =   4260
      End
      Begin VB.Label lblArticuloEsp 
         BackColor       =   &H00808000&
         Caption         =   "Especificaciones del art�culo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   195
         Index           =   0
         Left            =   720
         TabIndex        =   12
         Top             =   180
         Width           =   3795
      End
   End
End
Attribute VB_Name = "frmCatalogoEsp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public GestorIdiomas As CGestorIdiomas
Public Raiz As CRaiz
Public Idioma As String
Public Mensajes As CMensajes
Public GestorSeguridad As CGestorSeguridad
Public oUsuarioSummit As CUsuario

Public gvarCodUsuario As Variant
Public gTipoDeUsuario As Variant
Public gCodEqpUsuario As Variant
Public gCodCompradorUsuario As Variant

Public g_oLinea         As CLineaCatalogo
Public g_oArticulo      As CArticulo
Public g_oArticuloProve As CArticulo
Public g_oProveedor     As CProveedor
Public g_oIBaseDatos    As IBaseDatos

Private m_oArticulos    As CArticulos
Private m_oEsp          As CEspecificacion

Public g_bCancelarEsp   As Boolean
Public g_sComentario    As String
Public g_bRespetarCombo As Boolean
Public g_bSoloRuta      As Boolean

'Restricciones
Private m_bModifArt      As Boolean
Private m_bRMatArt       As Boolean
Private m_bModifArtProve As Boolean
Private m_bRMatArtProve  As Boolean
Private m_bModifLinea    As Boolean

Private m_iPasado        As Integer
Private Accion           As accionessummit
'Idiomas
Private m_sIdiArticulo      As String
Private m_sIdiProveedor     As String
Private m_sIdiDialogTitle   As String
Private m_sIdiAllFiles      As String
Private m_sIdiArchivo       As String
Private m_sIdiGuardarEsp    As String
Private m_sIdiTipoOrig      As String
Private m_sIdiNombre        As String
Private m_skb As String
Private m_sCaption As String

Private m_bGrabar As Boolean

Private m_ParamGen As ParametrosGenerales
Public Property Get ParamGenerales() As ParametrosGenerales
    ParamGenerales = m_ParamGen
End Property

Public Property Let ParamGenerales(ByRef ParamGen As ParametrosGenerales)
    m_ParamGen = ParamGen
End Property

''' <summary>
''' Graba los cambios
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()

    m_bGrabar = True

    If g_oLinea.ArtCod_Interno <> "" Then
        Accion = ACCArtadjMod
        txtArticuloEsp_Validate 0, False
    End If
    If g_oLinea.ArtCod_Interno <> "" Then
        Accion = ACCMatPorProveAdjMod
        txtArticuloEsp_Validate 1, False
    End If
    Accion = ACCCatAdjudAdjunMod
    txtArticuloEsp_Validate 2, False
    
    Unload Me
End Sub

''' <summary>
''' Descarta los cambios
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdCancelar_Click()
    Unload Me
End Sub

''' <summary>
''' Pasar Esp A Linea de cat�logo
''' </summary>
''' <param name="Index">Q control se ha cliquado</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdPasarEspALinea_Click(Index As Integer)
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit
        
    g_bRespetarCombo = True
    
    Select Case Index
    Case 0
        If ComprobarPasarEspec(txtArticuloEsp(2).Text, txtArticuloEsp(0).Text) Then txtArticuloEsp(2).Text = PasarEspec(txtArticuloEsp(2).Text, txtArticuloEsp(0).Text)
    Case 1
        If ComprobarPasarEspec(txtArticuloEsp(2).Text, txtArticuloEsp(1).Text) Then txtArticuloEsp(2).Text = PasarEspec(txtArticuloEsp(2).Text, txtArticuloEsp(1).Text)
    End Select
    
    g_bRespetarCombo = False
    Accion = ACCCatAdjudAdjunMod
End Sub

''' <summary>Comprueba si hay que a�adir una especificaci�n a otra. Comprueba si ya existe/summary>
''' <param name="sEspecDest">Especificaci�n destino</param>
''' <param name="sEspecOrg">Especificaci�n a a�adir</param>
''' <returns>Booleano indicando si se puede hacer el paso</returns>
''' <remarks>Llamada desde: cmdPasarEspALinea_Click</remarks>

Private Function ComprobarPasarEspec(ByVal sEspecDest As String, ByVal sEspecOrg) As Boolean
    Dim arEspecDest() As String
    Dim arEspecOrg() As String
    Dim i As Integer
    Dim k As Integer
    Dim bEncontrado As Boolean
    
    ComprobarPasarEspec = True
    
    If Len(sEspecOrg) = 0 Then ComprobarPasarEspec = False
    
    If Len(sEspecDest) > 0 And Len(sEspecOrg) > 0 Then
        arEspecDest = Split(sEspecDest, vbCrLf)
        arEspecOrg = Split(sEspecOrg, vbCrLf)
                
        For i = 0 To UBound(arEspecOrg)
            bEncontrado = False
            
            For k = 0 To UBound(arEspecDest)
                If arEspecOrg(i) = arEspecDest(k) Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            
            If Not bEncontrado Then Exit For
        Next
        
        If bEncontrado Then ComprobarPasarEspec = False
    End If
End Function

''' <summary>A�ade una especificaci�n a otra. A�ade unicamente las especificaciones que no existan ya</summary>
''' <param name="sEspecDest">Especificaci�n destino</param>
''' <param name="sEspecOrg">Especificaci�n a a�adir</param>
''' <returns>Booleano indicando si se puede hacer el paso</returns>
''' <remarks>Llamada desde: cmdPasarEspALinea_Click</remarks>

Private Function PasarEspec(ByVal sEspecDest As String, ByVal sEspecOrg As String) As String
    Dim arEspecDest() As String
    Dim arEspecOrg() As String
    Dim i As Integer
    Dim k As Integer
    Dim bEncontrado As Boolean
    Dim sDest As String
    
    sDest = sEspecDest
    
    If Len(sEspecOrg) > 0 Then
        If Len(sEspecDest) > 0 And Len(sEspecOrg) > 0 Then
            arEspecDest = Split(sEspecDest, vbCrLf)
            arEspecOrg = Split(sEspecOrg, vbCrLf)
                    
            For i = 0 To UBound(arEspecOrg)
                bEncontrado = False
                
                For k = 0 To UBound(arEspecDest)
                    If arEspecOrg(i) = arEspecDest(k) Then
                        bEncontrado = True
                        Exit For
                    End If
                Next
                
                If Not bEncontrado Then
                    sDest = sDest & IIf(sDest = "", "", vbCrLf) & arEspecOrg(i)
                End If
            Next
        Else
            sDest = sEspecOrg
        End If
    End If
    
    PasarEspec = sDest
End Function

''' <summary>
''' Pasar Esp desde Linea de cat�logo a Maestro/Prove
''' </summary>
''' <param name="Index">Q control se ha cliquado</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdPasarEspDesdeLinea_Click(Index As Integer)

If txtArticuloEsp(2).Text <> "" Then
    Select Case Index
    Case 0
        If ComprobarPasarEspec(txtArticuloEsp(0).Text, txtArticuloEsp(2).Text) Then txtArticuloEsp(0).Text = PasarEspec(txtArticuloEsp(0).Text, txtArticuloEsp(2).Text)
        Accion = ACCArtadjMod

    Case 1
        If ComprobarPasarEspec(txtArticuloEsp(1).Text, txtArticuloEsp(2).Text) Then txtArticuloEsp(1).Text = PasarEspec(txtArticuloEsp(1).Text, txtArticuloEsp(2).Text)
        Accion = ACCMatPorProveAdjMod

    End Select
End If
End Sub

''' <summary>
''' Carga la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
              
    If g_oLinea Is Nothing Then Unload Me
    
    If g_oLinea.ArtCod_Interno = "" Then
        Frame1(0).Visible = False
        Frame1(1).Visible = False
        Me.Width = 8475
    End If
    
    CargarEsp
    
    CargarRecursos
    
    If g_oLinea.ArtCod_Interno = "" Then
        Me.Caption = m_sCaption & " - " & m_sIdiProveedor & ": " & g_oProveedor.Cod & _
             " / " & m_sIdiArticulo & ": " & g_oLinea.ArtDen
    Else
        Me.Caption = m_sCaption & " - " & m_sIdiProveedor & ": " & g_oProveedor.Cod & _
             " / " & m_sIdiArticulo & ": " & g_oArticulo.Cod
    End If
    ConfigurarSeguridad

    m_iPasado = 0
End Sub
Private Sub CargarEsp()
Dim oProves As CProveedores

    If g_oLinea Is Nothing Then Unload Me

    Set oProves = Raiz.generar_CProveedores
    oProves.CargarTodosLosProveedoresDesde3 1, g_oLinea.ProveCod, , True
    If oProves.Count = 0 Then
        Mensajes.DatoEliminado m_sIdiProveedor
        Set oProves = Nothing
        Set g_oProveedor = Nothing
    End If
    Set g_oProveedor = oProves.Item(1)

    If g_oLinea.ArtCod_Interno <> "" Then
        'Carga las especificaciones de todos
        Set g_oArticulo = Raiz.generar_CArticulo
        g_oArticulo.GMN1Cod = g_oLinea.GMN1Cod
        g_oArticulo.GMN2Cod = g_oLinea.GMN2Cod
        g_oArticulo.GMN3Cod = g_oLinea.GMN3Cod
        g_oArticulo.GMN4Cod = g_oLinea.GMN4Cod
        g_oArticulo.Cod = g_oLinea.ArtCod_Interno
        g_oArticulo.Den = g_oLinea.ArtDen
        g_oArticulo.CodigoUnidad = g_oLinea.CodigoUnidadArticulo
        If Not IsNull(g_oLinea.Concepto) Then
            g_oArticulo.Concepto = g_oLinea.Concepto
        End If
        If Not IsNull(g_oLinea.Almacenable) Then
            g_oArticulo.Almacenable = g_oLinea.Almacenable
        End If
        If Not IsNull(g_oLinea.Recepcionable) Then
            g_oArticulo.Recepcionable = g_oLinea.Recepcionable
        End If
        
        
        g_oArticulo.CargarTodasLasEspecificaciones , , True
        Set g_oIBaseDatos = g_oArticulo
        g_oIBaseDatos.IniciarEdicion
        Set g_oIBaseDatos = Nothing
        
        g_bRespetarCombo = True
        txtArticuloEsp(0).Text = NullToStr(g_oArticulo.esp)
        g_bRespetarCombo = False
            
        Set g_oArticuloProve = Raiz.generar_CArticulo
        g_oArticuloProve.GMN1Cod = g_oLinea.GMN1Cod
        g_oArticuloProve.GMN2Cod = g_oLinea.GMN2Cod
        g_oArticuloProve.GMN3Cod = g_oLinea.GMN3Cod
        g_oArticuloProve.GMN4Cod = g_oLinea.GMN4Cod
        g_oArticuloProve.Cod = g_oLinea.ArtCod_Interno
        g_oArticuloProve.Den = g_oLinea.ArtDen
        g_oArticuloProve.CodigoUnidad = g_oLinea.CodigoUnidadArticulo
        If Not IsNull(g_oLinea.Concepto) Then
            g_oArticuloProve.Concepto = g_oLinea.Concepto
        End If
        If Not IsNull(g_oLinea.Almacenable) Then
            g_oArticuloProve.Almacenable = g_oLinea.Almacenable
        End If
        If Not IsNull(g_oLinea.Recepcionable) Then
            g_oArticuloProve.Recepcionable = g_oLinea.Recepcionable
        End If
        g_oArticuloProve.CargarTodasLasEspecificaciones , , True, g_oLinea.ProveCod
        If g_oArticuloProve.especificaciones Is Nothing Then
            cmdPasarEspALinea(1).Enabled = False
            cmdPasarEspDesdeLinea(1).Enabled = False
            txtArticuloEsp(1).Locked = True
        Else
            Set g_oIBaseDatos = g_oArticuloProve
            g_oIBaseDatos.IniciarEdicion
            Set g_oIBaseDatos = Nothing
            cmdPasarEspALinea(1).Enabled = True
            cmdPasarEspDesdeLinea(1).Enabled = True
            txtArticuloEsp(1).Locked = False
            
            g_bRespetarCombo = True
            txtArticuloEsp(1).Text = NullToStr(g_oArticuloProve.esp)
            g_bRespetarCombo = False
        End If
    End If
    
    g_oLinea.CargarTodasLasEspecificaciones , , True
    g_bRespetarCombo = True
    txtArticuloEsp(2).Text = NullToStr(g_oLinea.esp)
    
    g_bRespetarCombo = False

    If g_oProveedor Is Nothing Then
        Mensajes.NoValido m_sIdiProveedor
        Unload Me
    End If
End Sub

''' <summary>
''' Redimensiona la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Resize()
    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 1500 Then Exit Sub
    
    On Error Resume Next
    
    If g_oLinea.ArtCod_Interno <> "" Then
        Frame1(2).Height = (Me.Height - Me.picEdit.Height - 50) - 550
        Frame1(2).Width = Me.Width * 0.5 - 100
        
        Frame1(0).Left = Frame1(2).Width + 165
        Frame1(0).Height = (Me.Height - Me.picEdit.Height - 50) / 2 - 275
        Frame1(0).Width = Me.Width * 0.5 - 200
        
        Frame1(1).Left = Frame1(2).Width + 165
        Frame1(1).Top = Frame1(0).Height - 30
        Frame1(1).Height = Frame1(0).Height
        Frame1(1).Width = Frame1(0).Width
    Else
        Frame1(2).Height = (Me.Height - Me.picEdit.Height - 50) - 500
        Frame1(2).Width = Me.Width - 300
    End If
    
    Me.picEdit.Top = Me.Frame1(2).Top + Frame1(2).Height + 20
    Me.picEdit.Left = (Frame1(2).Width + 165) - 1000
    
    txtArticuloEsp(0).Width = Frame1(0).Width - 850
    txtArticuloEsp(1).Width = txtArticuloEsp(0).Width
    txtArticuloEsp(2).Width = Frame1(2).Width - 195
    txtArticuloEsp(2).Height = Frame1(2).Height - lblArticuloEsp(2).Height - 350
    txtArticuloEsp(0).Height = Frame1(0).Height - lblArticuloEsp(0).Height - 350 'dblAux / 2 - 405
    txtArticuloEsp(1).Height = Frame1(1).Height - lblArticuloEsp(1).Height - 350
    cmdPasarEspDesdeLinea(0).Left = cmdPasarEspALinea(0).Left
    txtArticuloEsp(0).Left = cmdPasarEspALinea(0).Width + cmdPasarEspALinea(0).Left + 105
    cmdPasarEspALinea(1).Left = cmdPasarEspALinea(0).Left
    cmdPasarEspDesdeLinea(1).Left = cmdPasarEspALinea(0).Left
    txtArticuloEsp(1).Left = cmdPasarEspALinea(1).Width + cmdPasarEspALinea(1).Left + 105
End Sub


''' <summary>
''' Descarga la pantalla
''' </summary>
''' <param name="Cancel">Cancelar la acci�n</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
    m_bGrabar = False
End Sub

Private Sub txtArticuloEsp_Change(Index As Integer)

Select Case Index
Case 0
    If Not g_bRespetarCombo Then
        If Accion <> ACCArtadjMod Then
            Accion = ACCArtadjMod
        End If
    End If
Case 1
    If Not g_bRespetarCombo Then
        If Accion <> ACCMatPorProveAdjMod Then
            Accion = ACCMatPorProveAdjMod
        End If
    End If
Case 2
    If Not g_bRespetarCombo Then
        If Accion <> ACCCatAdjudAdjunMod Then
            Accion = ACCCatAdjudAdjunMod
            m_iPasado = 0
        End If
    End If
End Select

End Sub

''' <summary>
''' Graba los cambios al aceptar
''' </summary>
''' <param name="Index">Q control a validar</param>
''' <param name="Cancel">Cancelar la acci�n</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub txtArticuloEsp_Validate(Index As Integer, Cancel As Boolean)
Dim teserror As TipoErrorSummit

If Not m_bGrabar Then Exit Sub

Select Case Index
Case 0
    If g_oLinea.ArtCod_Interno = "" Then Exit Sub
    
    
        If StrComp(NullToStr(g_oArticulo.esp), txtArticuloEsp(0).Text, vbTextCompare) <> 0 Then
                
            g_oArticulo.esp = StrToNull(txtArticuloEsp(0).Text)
            If txtArticuloEsp(0).Text <> "" Then
                g_oArticulo.EspAdj = 1
            Else
                If g_oArticulo.especificaciones.Count = 0 Then
                    g_oArticulo.EspAdj = 0
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            ''usuario para la integraci�n
            g_oArticulo.Usuario = gvarCodUsuario
            teserror = g_oArticulo.ModificarEspecificacion
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror, Mensajes, ParamGenerales
                Cancel = True
                Accion = ACCCatAdjudAdjunCon
                Exit Sub
            End If
            
            'Registrar accion
            RegistrarAccion Accion, " Gmn1:" & g_oArticulo.GMN1Cod & " Gmn2:" & g_oArticulo.GMN2Cod & " Gmn3:" & g_oArticulo.GMN3Cod & " Gmn4:" & g_oArticulo.GMN4Cod & " Art:" & g_oArticulo.Cod & " Esp:" & Left(g_oArticulo.esp, 50), gvarCodUsuario, m_ParamGen.gbACTIVLOG, GestorSeguridad
            Accion = ACCCatAdjudAdjunCon
            Screen.MousePointer = vbNormal
        
        End If
    
Case 1

    If g_oLinea.ArtCod_Interno = "" Then Exit Sub
    
    
        If StrComp(NullToStr(g_oArticuloProve.esp), txtArticuloEsp(1).Text, vbTextCompare) <> 0 Then
                
            g_oArticuloProve.esp = StrToNull(txtArticuloEsp(1).Text)
            If txtArticuloEsp(1).Text <> "" Then
                g_oArticuloProve.EspAdj = 1
            Else
                If g_oArticuloProve.especificaciones.Count = 0 Then
                    g_oArticuloProve.EspAdj = 0
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            teserror = g_oArticuloProve.ModificarEspecificacion(g_oProveedor.Cod)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror, Mensajes, m_ParamGen
                Cancel = True
                Accion = ACCCatAdjudAdjunCon
                Exit Sub
            End If
            
            'Registrar accion
            RegistrarAccion Accion, "Prove:" & g_oProveedor.Cod & " Gmn1:" & g_oArticulo.GMN1Cod & " Gmn2:" & g_oArticulo.GMN2Cod & " Gmn3:" & g_oArticulo.GMN3Cod & " Gmn4:" & g_oArticulo.GMN4Cod & " Art:" & g_oArticulo.Cod & " Esp:" & Left(g_oArticuloProve.esp, 50), gvarCodUsuario, m_ParamGen.gbACTIVLOG, GestorSeguridad
            Accion = ACCCatAdjudAdjunCon
            Screen.MousePointer = vbNormal
        
        End If
    
Case 2

    
        If StrComp(NullToStr(g_oLinea.esp), txtArticuloEsp(2).Text, vbTextCompare) <> 0 Then
                
            g_oLinea.esp = StrToNull(txtArticuloEsp(2).Text)
            If txtArticuloEsp(2).Text <> "" Then
                g_oLinea.EspAdj = 1
            Else
                If g_oLinea.especificaciones.Count = 0 Then
                    g_oLinea.EspAdj = 0
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            teserror = g_oLinea.ModificarEspecificacion
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror, Mensajes, m_ParamGen
                Cancel = True
                Accion = ACCCatAdjudAdjunCon
                Exit Sub
            End If
            
            'Registrar accion
            RegistrarAccion Accion, "Linea:" & g_oLinea.Id & " Esp:" & Left(g_oLinea.esp, 50), gvarCodUsuario, m_ParamGen.gbACTIVLOG, GestorSeguridad
            Accion = ACCCatAdjudAdjunCon
            Screen.MousePointer = vbNormal
        
        End If
    
End Select

End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_ESP, Idioma)

    If Not Ador Is Nothing Then
        lblArticuloEsp(0).Caption = Ador(0).Value
        Ador.MoveNext
        lblArticuloEsp(1).Caption = Ador(0).Value
        Ador.MoveNext
        lblArticuloEsp(2).Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        m_sIdiArticulo = Ador(0).Value
        Ador.MoveNext
        m_sIdiProveedor = Ador(0).Value
        Ador.MoveNext
        m_sIdiDialogTitle = Ador(0).Value
        Ador.MoveNext
        m_sIdiAllFiles = Ador(0).Value
        Ador.MoveNext
        m_sIdiArchivo = Ador(0).Value
        Ador.MoveNext
        m_sIdiGuardarEsp = Ador(0).Value
        Ador.MoveNext
        m_sIdiTipoOrig = Ador(0).Value
        Ador.MoveNext
        m_sIdiNombre = Ador(0).Value
        
        Ador.MoveNext
        
        Ador.MoveNext
        
        Ador.MoveNext
        
        Ador.MoveNext
        
        Ador.MoveNext
        m_skb = Ador(0).Value
        Ador.MoveNext
        m_sCaption = Ador(0).Value '18 Especificaciones
        Ador.Close
    End If
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Public Sub ConfigurarSeguridad()
    Dim oIMaterialAsignado As IMaterialAsignado
    Dim oArticulos As CArticulos
    Dim oArt As CArticulo
    
    m_bModifArt = True
    m_bModifArtProve = True
    m_bModifLinea = True
    
    'Articulo
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiModificar)) Is Nothing) Then
        m_bModifArt = False
    Else
        If gTipoDeUsuario = Comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATRestMatComp)) Is Nothing) Then
            'El usu tiene restricci�n de material, miramos si el articulo pertenece a su material, sino no puede modificarlo
            If g_oLinea.ArtCod_Interno <> "" Then
                Set oIMaterialAsignado = oUsuarioSummit.Comprador
                Set oArticulos = oIMaterialAsignado.DevolverArticulos(g_oArticulo.Cod, , True)
                m_bModifArt = False
                m_bRMatArt = True
                For Each oArt In oArticulos
                    If oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod And _
                        oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod Then
                        m_bModifArt = True
                        m_bRMatArt = False
                        Exit For
                    End If
                Next
            Else
                m_bModifArt = False
                m_bRMatArt = True
            End If
        End If
    End If
    'Articulo/proveedor
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtGestion)) Is Nothing) Then
        m_bModifArtProve = False
    Else
        If gTipoDeUsuario = Comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtRestMAtComp)) Is Nothing) Then
            If g_oLinea.ArtCod_Interno <> "" Then
                m_bModifArtProve = False
                m_bRMatArtProve = True
                'El usu tiene restricci�n de material, miramos si el articulo pertenece a su material/prove
                Set oIMaterialAsignado = g_oProveedor
                Set oIMaterialAsignado.ARTICULOS = Raiz.Generar_CArticulos
                oIMaterialAsignado.ARTICULOS.Add g_oArticulo.GMN1Cod, g_oArticulo.GMN2Cod, g_oArticulo.GMN3Cod, g_oArticulo.GMN4Cod, g_oArticulo.Cod, g_oArticulo.Den, , , 1
                Set oArticulos = oIMaterialAsignado.DevolverArticulos(gCodEqpUsuario, gCodCompradorUsuario, True, False, False)
                If oArticulos.Count > 0 Then m_bModifArtProve = True: m_bRMatArtProve = False
            End If
        End If
    End If
    'Linea
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGAdjModificar)) Is Nothing) Then
        m_bModifLinea = False
    End If

    If Not m_bModifArt Then
        cmdPasarEspDesdeLinea(0).Visible = False
        txtArticuloEsp(0).Locked = True
    End If
    
    If Not m_bModifArtProve Then
        cmdPasarEspDesdeLinea(1).Visible = False
        txtArticuloEsp(1).Locked = True
    End If
    
    If Not m_bModifLinea Then
        cmdPasarEspALinea(0).Visible = False
        cmdPasarEspALinea(1).Visible = False
        txtArticuloEsp(2).Locked = True
    End If

End Sub
