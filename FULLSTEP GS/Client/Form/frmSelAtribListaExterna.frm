VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSelAtribListaExterna 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DSeleccionar atributo de lista externa"
   ClientHeight    =   6030
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   8820
   Icon            =   "frmSelAtribListaExterna.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6030
   ScaleWidth      =   8820
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSeleccionar 
      Caption         =   "D&Seleccionar"
      Height          =   345
      Left            =   3060
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   5610
      Width           =   1005
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "D&Cerrar"
      Height          =   345
      Left            =   4260
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   5610
      Width           =   1005
   End
   Begin VB.Frame Frame 
      BackColor       =   &H00808000&
      ForeColor       =   &H00404000&
      Height          =   975
      Index           =   0
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   8625
      Begin VB.CommandButton cmdBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8220
         Picture         =   "frmSelAtribListaExterna.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Busqueda"
         Top             =   510
         Width           =   315
      End
      Begin VB.TextBox txtDen 
         Height          =   285
         Left            =   2790
         TabIndex        =   4
         Top             =   510
         Width           =   5340
      End
      Begin VB.TextBox txtCodigo 
         Height          =   285
         Left            =   180
         TabIndex        =   3
         Top             =   510
         Width           =   2520
      End
      Begin VB.Label Label 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DDenominaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   2820
         TabIndex        =   2
         Top             =   240
         Width           =   1185
      End
      Begin VB.Label Label 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DC�digo:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   0
         Left            =   210
         TabIndex        =   1
         Top             =   240
         Width           =   660
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
      Height          =   4365
      Left            =   90
      TabIndex        =   6
      Top             =   1140
      Width           =   8670
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   2
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSelAtribListaExterna.frx":0FF4
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      CaptionAlignment=   0
      Columns.Count   =   2
      Columns(0).Width=   4895
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   9340
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   15293
      _ExtentY        =   7699
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSelAtribListaExterna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public GestorIdiomas As CGestorIdiomas
Public Idioma As String
Public Raiz As CRaiz
'Variable que usaremos para saber qu� estamos buscando y qu� devolver� integraci�n
Public g_lIdAtrib As Long
'Variable C�digo de atributo
Public g_sCodAtrib As String
'Variable para formar el caption del formulario
Public g_sDenAtrib As String
'Distribuci�n
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String
'Usuario
Public g_sUsuCod As String
'Valor de la lista que se ha seleccionado
Public g_sValor As String
'Empresa
Public g_sCodEmp As String
'Id de la cabecera bien sea Orden Entrega o bien Id de la Instancia
Public g_lIdCab As Long
'Num de la linea de pedido o de la linea de la instancia
Public g_lLineaNum As Long
'Colecci�n de atributos de cabecera bien del pedido o bien de la solicitud
Public g_oAtribCab As Object
'Colecci�n de atributos de l�nea bien del pedido o bien de la solicitud
Public g_oAtribLin As Object
'Org. Compras/centro
Public g_sOrgCompras As String
Public g_sCentro As String

Private Sub CargarRecursos()
Dim Ador As ADODB.Recordset
Dim I As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
On Error Resume Next

Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_SELATRIBLISTAEXTERNA, Idioma)
If Not Ador Is Nothing Then
    Me.Caption = g_sCodAtrib & " - " & g_sDenAtrib & ". " & Ador(0).Value
    Ador.MoveNext
    Label(0).Caption = Ador(0).Value
    sdbgAtributos.Columns("COD").Caption = Ador(0).Value
    Ador.MoveNext
    Label(1).Caption = Ador(0).Value
    sdbgAtributos.Columns("DEN").Caption = Ador(0).Value
    Ador.MoveNext
    cmdSeleccionar.Caption = Ador(0).Value
    Ador.MoveNext
    cmdCerrar.Caption = Ador(0).Value
    Ador.Close
End If

Set Ador = Nothing
End Sub



'***********************************************ESQUEMA**************************************************************************************************************
'<ORDEN_ENTREGA>
'    <IDORDEN></IDORDEN>
'    <EMPRESA></EMPRESA>
'    <ORGCOMPRAS></ORGCOMPRAS>
'    <USU></USU>
'    <ATRIBUTOS>
'            <ATRIBUTO>
'                <ID></ID>
'                <TIPO></TIPO>
'                <VALOR></VALOR>  Campos num�ricos con separador de decimales �.� . Campos booleanos con 1/0. Campos fecha con formato AAAAMMDD
'            </ATRIBUTO>
'    </ATRIBUTOS>
'    <LINEAS>
'            <LINEA>
'                <NUM></NUM> N�mero de l�nea
'                <CENTRO></CENTRO>
'                <ATRIBUTOS_LINEA>
'                    <ATRIBUTO_LINEA>
'                        <ID></ID>
'                        <TIPO></TIPO>
'                        <VALOR></VALOR>  Campos num�ricos con separador de decimales �.� . Campos booleanos con 1/0. Campos fecha con formato AAAAMMDD
'                    </ATRIBUTO_LINEA >
'                </ATRIBUTOS_LINEA >
'            </LINEA>
'    </LINEAS>
'</ORDEN_ENTREGA >
'***********************************************ESQUEMA**************************************************************************************************************

Private Function GenerarXMLListaPedido() As String
Dim sLinea As String
Dim oAtribCab As CAtributoOfertado
Dim oAtribLin As CAtributoOfertado
Dim sValor As String

sLinea = "<ORDEN_ENTREGA>" & vbCrLf
sLinea = sLinea & "<IDORDEN>" & g_lIdCab & "</IDORDEN>" & vbCrLf
sLinea = sLinea & "<EMPRESA>" & g_sCodEmp & "</EMPRESA>" & vbCrLf
sLinea = sLinea & "<ORGCOMPRAS>" & g_sOrgCompras & "</ORGCOMPRAS>" & vbCrLf
sLinea = sLinea & "<USU>" & g_sUsuCod & "</USU>" & vbCrLf
sLinea = sLinea & "<ATRIBUTOS>" & vbCrLf
For Each oAtribCab In g_oAtribCab
    sLinea = sLinea & "<ATRIBUTO>" & vbCrLf
    sValor = SacarValorAtrib(oAtribCab)
    oAtribCab.Tipo = oAtribCab.objeto.Tipo
    sValor = SacarValorAtrib(oAtribCab)
    sLinea = sLinea & "<ID>" & oAtribCab.IdAtribProce & "</ID>" & vbCrLf
    sLinea = sLinea & "<TIPO>" & oAtribCab.Tipo & "</TIPO>" & vbCrLf
    sLinea = sLinea & "<VALOR>" & sValor & "</VALOR>" & vbCrLf
    sLinea = sLinea & "</ATRIBUTO>" & vbCrLf
Next
sLinea = sLinea & "</ATRIBUTOS>" & vbCrLf
If Not g_oAtribLin Is Nothing Then
    sLinea = sLinea & "<LINEAS>" & vbCrLf
    sLinea = sLinea & "<LINEA>" & vbCrLf
    sLinea = sLinea & "<NUM>" & g_lLineaNum & "</NUM>" & vbCrLf
    sLinea = sLinea & "<CENTRO>" & g_sCentro & "</CENTRO>" & vbCrLf
    sLinea = sLinea & "<ATRIBUTOS_LINEA>" & vbCrLf
    For Each oAtribLin In g_oAtribLin
        sLinea = sLinea & "<ATRIBUTO_LINEA>" & vbCrLf
        oAtribLin.Tipo = oAtribLin.objeto.Tipo
        sValor = SacarValorAtrib(oAtribLin)
        sLinea = sLinea & "<ID>" & oAtribLin.IdAtribProce & "</ID>" & vbCrLf
        sLinea = sLinea & "<TIPO>" & oAtribLin.Tipo & "</TIPO>" & vbCrLf
        sLinea = sLinea & "<VALOR>" & sValor & "</VALOR>" & vbCrLf
        sLinea = sLinea & "</ATRIBUTO_LINEA>" & vbCrLf
    Next
    sLinea = sLinea & "</ATRIBUTOS_LINEA>" & vbCrLf
    sLinea = sLinea & "</LINEA>" & vbCrLf
    sLinea = sLinea & "</LINEAS>" & vbCrLf
End If
sLinea = sLinea & "</ORDEN_ENTREGA>"
GenerarXMLListaPedido = sLinea
End Function

'**********************************************ESQUEMA***************************************************************************************************
'<PROCE>
'    <IDSOLICITUD></IDSOLICITUD> Id de la solicitud
'    <UON1_PROCE></UON1_PROCE >
'    <UON2_PROCE ></UON2_PROCE >
'    <UON3_PROCE ></UON3_PROCE >
'    <USU></USU>
'    <ATRIBUTOS>
'        <ATRIBUTO>
'            <ID></ID>
'            <TIPO></TIPO>
'            <VALOR></VALOR>  Campos num�ricos con separador de decimales "." . Campos booleanos con 1/0. Campos fecha con formato AAAAMMDD
'        </ATRIBUTO>
'    </ATRIBUTOS>
'    <LINEAS>
'        <LINEA>
'            <UON1></UON1>
'            <IDSOLICITUD></IDSOLICITUD> Id de la solicitud
'            <UON1></UON1>
'            <UON2></UON2>
'            <UON3></UON3>
'            <NUM></NUM> N�mero de l�nea / Id del item del proceso
'            <ATRIBUTOS_LINEA>
'                <ATRIBUTO_LINEA>
'                    <ID></ID>
'                    <TIPO></TIPO>
'                    <VALOR></VALOR>  Campos num�ricos con separador de decimales "." . Campos booleanos con 1/0. Campos fecha con formato AAAAMMDD
'                </ATRIBUTO_LINEA >
'            </ATRIBUTOS_LINEA>
'        </LINEA>
'    </LINEAS>
'</PROCE>
'**********************************************ESQUEMA***************************************************************************************************
Private Function GenerarXMLListaProceso() As String
Dim sLinea As String
Dim oAtribCab As CAtributo
Dim oAtribLin As CAtributo
Dim sValor As String

sLinea = "<PROCE>" & vbCrLf
sLinea = sLinea & "<IDSOLICITUD>" & g_lIdCab & "</IDSOLICITUD>" & vbCrLf
sLinea = sLinea & "<UON1_PROCE>" & g_sUON1 & "</UON1_PROCE>" & vbCrLf
sLinea = sLinea & "<UON2_PROCE>" & g_sUON2 & "</UON2_PROCE>" & vbCrLf
sLinea = sLinea & "<UON3_PROCE>" & g_sUON3 & "</UON3_PROCE>" & vbCrLf
sLinea = sLinea & "<USU>" & g_sUsuCod & "</USU>" & vbCrLf
sLinea = sLinea & "<ATRIBUTOS>" & vbCrLf
For Each oAtribCab In g_oAtribCab
    sLinea = sLinea & "<ATRIBUTO>" & vbCrLf
    sValor = SacarValorAtrib(oAtribCab)
    sLinea = sLinea & "<ID>" & oAtribCab.Id & "</ID>" & vbCrLf
    sLinea = sLinea & "<TIPO>" & oAtribCab.Tipo & "</TIPO>" & vbCrLf
    sLinea = sLinea & "<VALOR>" & sValor & "</VALOR>" & vbCrLf
    sLinea = sLinea & "</ATRIBUTO>" & vbCrLf
Next
sLinea = sLinea & "</ATRIBUTOS>" & vbCrLf
If Not g_oAtribLin Is Nothing Then
    sLinea = sLinea & "<LINEAS>" & vbCrLf
    sLinea = sLinea & "<LINEA>" & vbCrLf
    sLinea = sLinea & "<IDSOLICITUD>" & g_lIdCab & "</IDSOLICITUD>" & vbCrLf
    sLinea = sLinea & "<UON1>" & g_sUON1 & "</UON1>" & vbCrLf
    sLinea = sLinea & "<UON2>" & g_sUON2 & "</UON2>" & vbCrLf
    sLinea = sLinea & "<UON3>" & g_sUON3 & "</UON3>" & vbCrLf
    sLinea = sLinea & "<NUM>" & g_lLineaNum & "</NUM>" & vbCrLf
    sLinea = sLinea & "<ATRIBUTOS_LINEA>" & vbCrLf
    For Each oAtribLin In g_oAtribLin
        sLinea = sLinea & "<ATRIBUTO_LINEA>" & vbCrLf
        sValor = SacarValorAtrib(oAtribLin)
        sLinea = sLinea & "<ID>" & oAtribLin.Id & "</ID>" & vbCrLf
        sLinea = sLinea & "<TIPO>" & oAtribLin.Tipo & "</TIPO>" & vbCrLf
        sLinea = sLinea & "<VALOR>" & sValor & "</VALOR>" & vbCrLf
        sLinea = sLinea & "</ATRIBUTO_LINEA>" & vbCrLf
    Next
    sLinea = sLinea & "</ATRIBUTOS_LINEA>" & vbCrLf
    sLinea = sLinea & "</LINEA>" & vbCrLf
    sLinea = sLinea & "</LINEAS>" & vbCrLf
End If
sLinea = sLinea & "</PROCE>"
GenerarXMLListaProceso = sLinea
End Function

Private Function SacarValorAtrib(ByVal oAtrib As Object) As String
Dim sValor As String
Select Case oAtrib.Tipo
    Case TiposDeAtributos.TipoBoolean
        If oAtrib.ValorBool = 0 Then
            sValor = 0
        Else
            sValor = 1
        End If
    Case TiposDeAtributos.TipoFecha
        sValor = Format(Year(oAtrib.ValorFec), "0000") & Format(Month(oAtrib.ValorFec), "00") & Format(Day(oAtrib.ValorFec), "00")
    Case TiposDeAtributos.TipoNumerico
        sValor = NullToDbl0(oAtrib.ValorNum)
    Case Else
        sValor = NullToStr(oAtrib.ValorText)
End Select

SacarValorAtrib = sValor
End Function

Private Sub cmdBuscar_Click()
    Dim Mapper As Object
    Dim adoRecordset As ADODB.Recordset
    Dim auxERP As FSGSServer.CERPInt
    Dim sMapper As String
    Dim oOrgsCompra As COrganizacionesCompras
    Dim sOrgCompras As String
    Dim vEmp As Variant
    Dim iNumError As Integer
    Dim sError As String
    Dim sCod As String
    Dim sDen As String
    Dim sXML As String
    Dim iEntidadIntegracion As Integer
    
    'Primero borramos todas las filas que hay dibujadas
    sdbgAtributos.RemoveAll
    
    If g_sCodEmp = "" Then
        'Sacamos la Organizaci�n de compras para sacar la mapper
        Set oOrgsCompra = Raiz.Generar_COrganizacionesCompras
        Set adoRecordset = oOrgsCompra.ObtenerUON(g_sUON1, g_sUON2, g_sUON3)
        If Not adoRecordset Is Nothing Then
            sOrgCompras = NullToStr(adoRecordset("ORGCOMPRAS").Value)
        End If
        Set adoRecordset = Nothing
    End If
    vEmp = g_sCodEmp
    
    
    If g_sUON1 = "" Then
        sXML = GenerarXMLListaPedido
        iEntidadIntegracion = EntidadIntegracion.PED_directo
    Else
        sXML = GenerarXMLListaProceso
        iEntidadIntegracion = EntidadIntegracion.SolicitudPM
    End If
    Set auxERP = Raiz.Generar_CERPInt
    
    
    Dim iServiceBindingType As Integer
    Dim iServiceSecurityMode As Integer
    Dim iClientCredentialType As Integer
    Dim iProxyCredentialType As Integer
    Dim sUserName As String
    Dim sUserPassword As String
    Dim strRutaServicio As String
    Dim bWCF As Boolean
    Dim oFSIS As CFSISService
    
    bWCF = False
    
    bWCF = auxERP.ObtenerDatosConexionWS(iEntidadIntegracion, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sUserName, sUserPassword, strRutaServicio)
    
    If Not bWCF Then
        sMapper = auxERP.ObtenerMapper(sOrgCompras, vEmp)
        On Error Resume Next
        Set Mapper = CreateObject(sMapper & ".cListadoAtributos")
        On Error GoTo 0
        If Not Mapper Is Nothing Then
            If txtCodigo.Text <> "" Then
                sCod = txtCodigo.Text
                If InStr(sCod, "*") = 0 Then sCod = "*" & sCod & "*"
                sCod = StrToSQLNULL(Replace(sCod, "*", "%"))
            End If
            If txtDen.Text <> "" Then
                sDen = txtDen.Text
                If InStr(sDen, "*") = 0 Then sDen = "*" & sDen & "*"
                sDen = StrToSQLNULL(Replace(sDen, "*", "%"))
            End If
            Screen.MousePointer = vbHourglass
            Set adoRecordset = Mapper.DevolverListaObjetosGS(iNumError, sError, Val(g_sCodEmp), g_lIdAtrib, g_sUsuCod, sCod, sDen, g_sUON1 & IIf(g_sUON2 <> "", "#" & g_sUON2, "") & IIf(g_sUON3 <> "", "#" & g_sUON3, ""), sXML)
            If Not adoRecordset Is Nothing Then
                If iNumError = 0 Then
                    Do While Not adoRecordset.EOF
                        sdbgAtributos.AddItem adoRecordset("CODBUSC").Value & Chr(m_lSeparador) & adoRecordset("DENBUSC").Value
                        adoRecordset.MoveNext
                    Loop
                End If
            End If
            Screen.MousePointer = vbNormal
        End If
        Set Mapper = Nothing
    Else
        
        If oFSIS Is Nothing Then
           Set oFSIS = New CFSISService
        End If
        
        Dim result As ADODB.Recordset
        
        If txtCodigo.Text <> "" Then
            sCod = txtCodigo.Text
            If InStr(sCod, "*") = 0 Then sCod = "*" & sCod & "*"
            sCod = StrToSQLNULL(Replace(sCod, "*", "%"))
        End If
        If txtDen.Text <> "" Then
            sDen = txtDen.Text
            If InStr(sDen, "*") = 0 Then sDen = "*" & sDen & "*"
            sDen = StrToSQLNULL(Replace(sDen, "*", "%"))
        End If
        
        Screen.MousePointer = vbHourglass
       Set adoRecordset = oFSIS.DevolverListaObjetosGS(g_sCodEmp, g_lIdAtrib, sCod, sDen, g_sUsuCod, g_sUON1 & IIf(g_sUON2 <> "", "#" & g_sUON2, "") & IIf(g_sUON3 <> "", "#" & g_sUON3, ""), sXML, _
                                                        iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sUserName, sUserPassword, strRutaServicio)
        
        If Not adoRecordset Is Nothing Then
            adoRecordset.MoveFirst
            Do While Not adoRecordset.EOF
                sdbgAtributos.AddItem adoRecordset("CODBUSC").Value & Chr(m_lSeparador) & adoRecordset("DENBUSC").Value
                adoRecordset.MoveNext
            Loop
        End If
        Screen.MousePointer = vbNormal
    End If
    
    Set adoRecordset = Nothing
    Set auxERP = Nothing
End Sub

Private Sub cmdCerrar_Click()
Unload Me
End Sub

Private Sub cmdSeleccionar_Click()
If sdbgAtributos.Rows = 0 Then Exit Sub
g_sValor = sdbgAtributos.Columns("COD").Value & " - " & sdbgAtributos.Columns("DEN").Value
Unload Me
End Sub

Private Sub Form_Load()
PonerFieldSeparator Me
CargarRecursos
End Sub

Private Sub sdbgAtributos_DblClick()
With sdbgAtributos
    If .Rows = 0 Then Exit Sub
    cmdSeleccionar_Click
End With
End Sub

