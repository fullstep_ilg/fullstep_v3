VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCATALAnyaArt 
   BackColor       =   &H00808000&
   Caption         =   "Cat�logo de aprovisionamiento - A�adir art�culos"
   ClientHeight    =   6735
   ClientLeft      =   2325
   ClientTop       =   1065
   ClientWidth     =   7770
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATALAnyaArt.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6735
   ScaleWidth      =   7770
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   345
      Left            =   4845
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   6315
      Width           =   1185
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&A�adir"
      Height          =   345
      Left            =   3345
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   6315
      Width           =   1305
   End
   Begin VB.CommandButton cmdSeleccionar 
      Caption         =   "&Seleccionar todos"
      Height          =   345
      Left            =   1665
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   6315
      Width           =   1545
   End
   Begin VB.Frame fraProveMat 
      BackColor       =   &H00808000&
      ForeColor       =   &H80000005&
      Height          =   2010
      Left            =   90
      TabIndex        =   11
      Top             =   -30
      Width           =   7620
      Begin VB.CheckBox chkVerNoCat 
         BackColor       =   &H00808000&
         Caption         =   "Ver solo art�culos que no est�n en el cat�logo"
         ForeColor       =   &H80000005&
         Height          =   375
         Left            =   180
         TabIndex        =   6
         Top             =   1470
         Value           =   1  'Checked
         Width           =   6975
      End
      Begin VB.TextBox txtEstMat 
         BackColor       =   &H80000018&
         Enabled         =   0   'False
         Height          =   285
         Left            =   180
         TabIndex        =   3
         Top             =   1065
         Width           =   6150
      End
      Begin VB.CommandButton cmdBuscarProve 
         Height          =   285
         Left            =   6390
         Picture         =   "frmCATALAnyaArt.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   450
         Width           =   315
      End
      Begin VB.CommandButton cmdSelMat 
         Height          =   315
         Left            =   6720
         Picture         =   "frmCATALAnyaArt.frx":0D40
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   1065
         Width           =   345
      End
      Begin VB.CommandButton cmdBorrar 
         Height          =   315
         Left            =   6345
         Picture         =   "frmCATALAnyaArt.frx":0DAC
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1065
         Width           =   345
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
         Height          =   285
         Left            =   180
         TabIndex        =   0
         Top             =   450
         Width           =   1620
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "C�d."
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2857
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
         Height          =   285
         Left            =   1815
         TabIndex        =   1
         Top             =   450
         Width           =   4530
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "C�d."
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7990
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblProve 
         BackColor       =   &H00808000&
         Caption         =   "Proveedor:"
         ForeColor       =   &H80000005&
         Height          =   255
         Left            =   210
         TabIndex        =   13
         Top             =   210
         Width           =   1650
      End
      Begin VB.Label lblMat 
         BackStyle       =   0  'Transparent
         Caption         =   "Material:"
         ForeColor       =   &H80000005&
         Height          =   240
         Left            =   225
         TabIndex        =   12
         Top             =   825
         Width           =   1635
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCatAproArt 
      Height          =   4155
      Left            =   90
      TabIndex        =   7
      Top             =   2070
      Width           =   7650
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   12
      stylesets.count =   2
      stylesets(0).Name=   "Tahoma"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCATALAnyaArt.frx":0E52
      stylesets(1).Name=   "NoHom"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   12632256
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCATALAnyaArt.frx":0E6E
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      HeadStyleSet    =   "Tahoma"
      StyleSet        =   "Tahoma"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      CaptionAlignment=   0
      Columns.Count   =   12
      Columns(0).Width=   794
      Columns(0).Caption=   "Hom"
      Columns(0).Name =   "HOM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   11
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   7699
      Columns(1).Caption=   "Art�culo"
      Columns(1).Name =   "ART"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   2328
      Columns(2).Caption=   "Consumo"
      Columns(2).Name =   "CONSUM"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   5
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2064
      Columns(3).Caption=   "Unidad def."
      Columns(3).Name =   "UNI"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasForeColor=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "ARTCOD"
      Columns(4).Name =   "ARTCOD"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "GMN1"
      Columns(5).Name =   "GMN1"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "GMN2"
      Columns(6).Name =   "GMN2"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "GMN3"
      Columns(7).Name =   "GMN3"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "GMN4"
      Columns(8).Name =   "GMN4"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ARTDEN"
      Columns(9).Name =   "ARTDEN"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "GENERICO"
      Columns(10).Name=   "GENERICO"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "TIPORECEPCION"
      Columns(11).Name=   "TIPORECEPCION"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      _ExtentX        =   13494
      _ExtentY        =   7320
      _StockProps     =   79
      Caption         =   "Seleccione los art�culos que desee y haga 'clic' en 'A�adir' para continuar..."
      ForeColor       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCATALAnyaArt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private CargarComboDesde As Boolean
Private RespetarCombo As Boolean
Public bRMat As Boolean
Private sEquipo As String
Private sComprador As String

'Variables para materiales
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4
Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String
Public vCat1 As Variant
Public vCat2 As Variant
Public vCat3 As Variant
Public vCat4 As Variant
Public vCat5 As Variant

Private oProves As CProveedores
Private oProveSeleccionado As CProveedor
Public oArticulos As CArticulos

Private sIdiProve As String
Private sProveCod As String

Public bValidar As Boolean

Private Sub Arrange()

    On Error Resume Next
    
    If Me.Height < 3000 Then Exit Sub
    If Me.Width < 3000 Then Exit Sub
    fraProveMat.Width = Me.Width - 270
    sdbgCatAproArt.Width = fraProveMat.Width + 30
    sdbgCatAproArt.Height = Me.Height - 2985
    
    
    If Not gParametrosGenerales.gbOblPedidosHom Then
        sdbgCatAproArt.Columns("HOM").Visible = False
        sdbgCatAproArt.Columns("ART").Width = sdbgCatAproArt.Width * 0.6
        sdbgCatAproArt.Columns("CONSUM").Width = sdbgCatAproArt.Width * 0.2
        sdbgCatAproArt.Columns("UNI").Width = sdbgCatAproArt.Width * 0.155
    Else
        sdbgCatAproArt.Columns("HOM").Width = sdbgCatAproArt.Width * 0.08
        sdbgCatAproArt.Columns("ART").Width = sdbgCatAproArt.Width * 0.5
        sdbgCatAproArt.Columns("CONSUM").Width = sdbgCatAproArt.Width * 0.2
        sdbgCatAproArt.Columns("UNI").Width = sdbgCatAproArt.Width * 0.15
    End If
    
    cmdSeleccionar.Top = sdbgCatAproArt.Top + sdbgCatAproArt.Height + 100
    cmdSeleccionar.Left = Me.Width / 3 - 700
    cmdAceptar.Top = cmdSeleccionar.Top
    cmdAceptar.Left = cmdSeleccionar.Left + cmdSeleccionar.Width + 180
    cmdCancelar.Top = cmdSeleccionar.Top
    cmdCancelar.Left = cmdAceptar.Left + cmdAceptar.Width + 180

End Sub
Private Sub CargarGridArticulos()
    Dim i As Long

    Screen.MousePointer = vbHourglass
    
    sdbgCatAproArt.RemoveAll
    
    If chkVerNoCat.Value = vbChecked Then
        oArticulos.CargarTodosLosArticulosPorNivelDeMaterialYProveedor sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcProveCod.Text), 1, True, , , True, sEquipo, sComprador
    Else
        oArticulos.CargarTodosLosArticulosPorNivelDeMaterialYProveedor sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcProveCod.Text), 1, True, , , False, sEquipo, sComprador
    End If
    
    sdbgCatAproArt.AllowAddNew = True
   
    For i = 1 To oArticulos.Count
        sdbgCatAproArt.AddItem oArticulos.Item(i).Homologado & Chr(m_lSeparador) & oArticulos.Item(i).Cod & " - " & oArticulos.Item(i).Den & Chr(m_lSeparador) & oArticulos.Item(i).Cantidad & Chr(m_lSeparador) & oArticulos.Item(i).CodigoUnidad _
        & Chr(m_lSeparador) & oArticulos.Item(i).Cod & Chr(m_lSeparador) & oArticulos.Item(i).GMN1Cod & Chr(m_lSeparador) & oArticulos.Item(i).GMN2Cod & Chr(m_lSeparador) & oArticulos.Item(i).GMN3Cod & Chr(m_lSeparador) & oArticulos.Item(i).GMN4Cod & Chr(m_lSeparador) & oArticulos.Item(i).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oArticulos.Item(i).Generico) & Chr(m_lSeparador) & oArticulos.Item(i).tipoRecepcion
    Next
    sdbgCatAproArt.AllowAddNew = False
    
    Screen.MousePointer = vbNormal
   
End Sub

Public Sub PonerMatProveSeleccionado()
         
    If sGMN1Cod <> "" Then
        txtEstMat = sGMN1Cod
    End If
    
    If sGMN2Cod <> "" Then
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    End If
    
    If sGMN3Cod <> "" Then
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    End If
    
    If sGMN4Cod <> "" Then
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    End If

    CargarGridArticulos

    bValidar = False

End Sub

Public Sub PonerMatSeleccionado()
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
        
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
        txtEstMat = sGMN1Cod
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If
    
    CargarGridArticulos

End Sub

Private Sub ProveedorSeleccionado()
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
    If oProveSeleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    txtEstMat = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    
'    If gParametrosGenerales.giINSTWEB = ConPortal Then
'        oProves.CargarDatosProveedor sdbcProveCod.Text
'        If ((IsNull(oProves.Item(CStr(sdbcProveCod.Text)).CodPortal) Or (oProves.Item(CStr(sdbcProveCod.Text)).CodPortal = ""))) Then
'            oMensajes.ProveCatalogProvePortal
'            Screen.MousePointer = vbNormal
'            sdbcProveCod.Text = ""
'            Set oProveSeleccionado = Nothing
'            Exit Sub
'        End If
'    End If
                
    If gParametrosGenerales.giINSTWEB = conweb Then
        oProves.CargarDatosProveedor sdbcProveCod.Text, True
        If oProves.Item(CStr(sdbcProveCod.Text)).UsuarioWeb Is Nothing Then
            oMensajes.ProveCatalogProveWeb
            Screen.MousePointer = vbNormal
            sdbcProveCod.Text = ""
            Set oProveSeleccionado = Nothing
            Exit Sub
        End If
    End If
                
    Screen.MousePointer = vbNormal

End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALANYA_ART, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        lblProve.caption = Ador(0).Value
        sIdiProve = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns("COD").caption = Ador(0).Value
        sdbcProveDen.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns("DEN").caption = Ador(0).Value
        sdbcProveDen.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        lblMat.caption = Ador(0).Value
        Ador.MoveNext
        sdbgCatAproArt.caption = Ador(0).Value
        Ador.MoveNext
        sdbgCatAproArt.Columns("HOM").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCatAproArt.Columns("ART").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCatAproArt.Columns("CONSUM").caption = Ador(0).Value
        Ador.MoveNext
        sdbgCatAproArt.Columns("UNI").caption = Ador(0).Value
        Ador.MoveNext
        cmdSeleccionar.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        chkVerNoCat.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing

End Sub

Private Sub chkVerNoCat_Click()

If txtEstMat.Text <> "" Then
    CargarGridArticulos
End If
End Sub

Private Sub cmdAceptar_Click()
Dim i, j As Long
Dim oLineaNueva As CLineaCatalogo
Dim oLineasNuevas As CLineasCatalogo
Dim bAdjuntarArt As Boolean
Dim bAdjuntarProve As Boolean

    If oProveSeleccionado Is Nothing Then
        oMensajes.NoValido 15
        Exit Sub
    End If
    If sdbgCatAproArt.Rows = 0 Then Exit Sub
    
    If ((Not IsNull(sdbgCatAproArt.Bookmark)) And (sdbgCatAproArt.SelBookmarks.Count = 0)) Then
        sdbgCatAproArt.SelBookmarks.Add sdbgCatAproArt.Bookmark
    End If
    
    Select Case gParametrosGenerales.giCatalogoEspecArticulo
        Case 1
            bAdjuntarArt = True
        Case 2
            i = oMensajes.PreguntaAnyadirEspecificaciones(TipoEspecificacion.EspArticulo)
            If i = vbYes Then
                bAdjuntarArt = True
            Else
                bAdjuntarArt = False
            End If
        Case 3
            bAdjuntarArt = False
    End Select
    Select Case gParametrosGenerales.giCatalogoEspecProveArt
        Case 1
            bAdjuntarProve = True
        Case 2
            i = oMensajes.PreguntaAnyadirEspecificaciones(TipoEspecificacion.EspArticuloProveedor)
            If i = vbYes Then
                bAdjuntarProve = True
            Else
                bAdjuntarProve = False
            End If
        Case 3
            bAdjuntarProve = False
    End Select
    
    Screen.MousePointer = vbHourglass
    frmCatalogo.sdbgAdjudicaciones.AllowAddNew = True
    
    Set oLineasNuevas = oFSGSRaiz.Generar_CLineasCatalogo 'nuevos art�culos a a�adir
    Set oLineaNueva = oFSGSRaiz.Generar_CLineaCatalogo
    For i = 0 To sdbgCatAproArt.SelBookmarks.Count - 1
        With oLineaNueva
            .ArtCod_Interno = sdbgCatAproArt.Columns("ARTCOD").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .ArtDen = sdbgCatAproArt.Columns("ARTDEN").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .ArtGenerico = BooleanToSQLBinary(GridCheckToBoolean(sdbgCatAproArt.Columns("GENERICO").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))))
            .Homologado = sdbgCatAproArt.Columns("HOM").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .UnidadCompra = sdbgCatAproArt.Columns("UNI").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .CantidadAdj = sdbgCatAproArt.Columns("CONSUM").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .UnidadPedido = sdbgCatAproArt.Columns("UNI").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .tipoRecepcion = sdbgCatAproArt.Columns("TIPORECEPCION").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .DestinoUsuario = True
            .Cat1 = vCat1
            .Cat2 = vCat2
            .Cat3 = vCat3
            .Cat4 = vCat4
            .Cat5 = vCat5
            .Anyo = Null
            .ProceCod = Null
            .Item = Null
            'A�adido el 23-2-2006
            #If VERSION >= 30600 Then
               .mon = oProveSeleccionado.CodMon
            #End If
            .Destino = gParametrosInstalacion.gsDestino
            If bAdjuntarArt Then
                If bAdjuntarProve Then
                   .EspAdj = 3
                Else
                    .EspAdj = 1
                End If
            Else
                If bAdjuntarProve Then
                    .EspAdj = 2
                Else
                    .EspAdj = 0
                End If
            End If
            .PorcenDesvio = gParametrosGenerales.gsDesvioRecepcion
    
            If (gParametrosGenerales.gbOblPedidosHom And sdbcProveCod.Text <> "" And .Homologado = False) Then
                oMensajes.ArticuloNoHomologado
                frmCatalogo.sdbgAdjudicaciones.AllowAddNew = False
                sdbgCatAproArt.SelBookmarks.RemoveAll
                sdbgCatAproArt.MoveFirst
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            j = frmCatalogo.oLineasAAnyadir.Count
            frmCatalogo.oLineasAAnyadir.Add Id:=j + 1, Prove:=StrToNull(sdbcProveCod.Text), ArtCod_Interno:=.ArtCod_Interno, ArtDen:=.ArtDen, Homologado:=.Homologado, UnidadCompra:=.UnidadCompra, CantAdj:=.CantidadAdj, UnidadPedido:=.UnidadPedido, FactorConversion:=1, bDestUsu:=.DestinoUsuario, vHayEsp:=.EspAdj, Destino:=.Destino, Mon_linea:=.mon, Generico:=.ArtGenerico, Cat1:=vCat1, Cat2:=vCat2, Cat3:=vCat3, Cat4:=vCat4, Cat5:=vCat5, PorcenDesvio:=gParametrosGenerales.gsDesvioRecepcion
            frmCatalogo.oLineasAAnyadir.Item(CStr(j + 1)).EspAdj = .EspAdj
            oLineasNuevas.Add Id:=j + 1, Prove:=StrToNull(sdbcProveCod.Text), ArtCod_Interno:=.ArtCod_Interno, ArtDen:=.ArtDen, Homologado:=.Homologado, UnidadCompra:=.UnidadCompra, CantAdj:=.CantidadAdj, UnidadPedido:=.UnidadPedido, FactorConversion:=1, bDestUsu:=.DestinoUsuario, vHayEsp:=.EspAdj, Destino:=.Destino, Mon_linea:=.mon, Generico:=.ArtGenerico, Cat1:=vCat1, Cat2:=vCat2, Cat3:=vCat3, Cat4:=vCat4, Cat5:=vCat5, PorcenDesvio:=gParametrosGenerales.gsDesvioRecepcion
            oLineasNuevas.Item(CStr(j + 1)).EspAdj = .EspAdj
            oLineasNuevas.Item(CStr(j + 1)).tipoRecepcion = .tipoRecepcion
        End With
        
    Next
        
    Dim sLinea As String
    For Each oLineaNueva In oLineasNuevas
        sLinea = "0" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.ArtCod_Interno & Chr(m_lSeparador) & oLineaNueva.ArtDen & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oLineaNueva.ProveCod)
        sLinea = sLinea & Chr(m_lSeparador) & NullToStr(oLineaNueva.Destino) & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oLineaNueva.UnidadCompra) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.mon
        sLinea = sLinea & Chr(m_lSeparador) & NullToStr(oLineaNueva.UnidadPedido) & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.GMN1Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oLineaNueva.ArtCod_Interno & Chr(m_lSeparador) & oLineaNueva.GMN2Cod & Chr(m_lSeparador) & oLineaNueva.GMN3Cod & Chr(m_lSeparador) & oLineaNueva.GMN4Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.PorcenDesvio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.EspAdj & Chr(m_lSeparador) & CStr(oLineaNueva.ArtGenerico)
        sLinea = sLinea & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.tipoRecepcion
        frmCatalogo.sdbgAdjudicaciones.AddItem sLinea
    Next
    
    frmCatalogo.sdbgAdjudicaciones.MoveLast
    
    sdbgCatAproArt.DeleteSelected
    Set oLineaNueva = Nothing
    Set oLineasNuevas = Nothing
    Screen.MousePointer = vbNormal

    
End Sub

Private Sub cmdBorrar_Click()
    txtEstMat.Text = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
End Sub

Private Sub cmdBuscarProve_Click()
    frmPROVEBuscar.sOrigen = "frmCATALAnyaArt"
    frmPROVEBuscar.bRMat = bRMat
    frmPROVEBuscar.Show 1
End Sub

Public Sub CargarProveedorConBusqueda()
    
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    RespetarCombo = True
    sdbcProveCod.Text = oProves.Item(1).Cod
    sdbcProveDen.Text = oProves.Item(1).Den
    RespetarCombo = False
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdSeleccionar_Click()
Dim i, j As Long
Dim oLineaNueva As CLineaCatalogo
Dim oLineasNuevas As CLineasCatalogo
Dim bAdjuntarArt As Boolean
Dim bAdjuntarProve As Boolean
Dim sLinea As String

    If oProveSeleccionado Is Nothing Then
        oMensajes.NoValido 15
        Exit Sub
    End If
    
    If sdbgCatAproArt.Rows = 0 Then Exit Sub
    
    sdbgCatAproArt.SelBookmarks.RemoveAll
       
    For i = 0 To (sdbgCatAproArt.Rows - 1)
        sdbgCatAproArt.SelBookmarks.Add i
    Next
    
    frmCatalogo.sdbgAdjudicaciones.AllowAddNew = True
    
    Select Case gParametrosGenerales.giCatalogoEspecArticulo
        Case 1
            bAdjuntarArt = True
        Case 2
            i = oMensajes.PreguntaAnyadirEspecificaciones(TipoEspecificacion.EspArticulo)
            If i = vbYes Then
                bAdjuntarArt = True
            Else
                bAdjuntarArt = False
            End If
        Case 3
            bAdjuntarArt = False
    End Select
    Select Case gParametrosGenerales.giCatalogoEspecProveArt
        Case 1
            bAdjuntarProve = True
        Case 2
            i = oMensajes.PreguntaAnyadirEspecificaciones(TipoEspecificacion.EspArticuloProveedor)
            If i = vbYes Then
                bAdjuntarProve = True
            Else
                bAdjuntarProve = False
            End If
        Case 3
            bAdjuntarProve = False
    End Select

    Screen.MousePointer = vbHourglass
    
    Set oLineasNuevas = oFSGSRaiz.Generar_CLineasCatalogo 'nuevos art�culos a a�adir

    For i = 0 To (sdbgCatAproArt.SelBookmarks.Count - 1)
        Set oLineaNueva = oFSGSRaiz.Generar_CLineaCatalogo
        With oLineaNueva
            .ArtCod_Interno = sdbgCatAproArt.Columns("ARTCOD").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .ArtDen = sdbgCatAproArt.Columns("ARTDEN").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .ArtGenerico = BooleanToSQLBinary(GridCheckToBoolean(sdbgCatAproArt.Columns("GENERICO").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))))
            .GMN1Cod = sdbgCatAproArt.Columns("GMN1").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .GMN2Cod = sdbgCatAproArt.Columns("GMN2").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .GMN3Cod = sdbgCatAproArt.Columns("GMN3").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .GMN4Cod = sdbgCatAproArt.Columns("GMN4").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .Homologado = GridCheckToBoolean(sdbgCatAproArt.Columns("HOM").CellValue(sdbgCatAproArt.SelBookmarks.Item(i)))
            .UnidadCompra = sdbgCatAproArt.Columns("UNI").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .UnidadPedido = sdbgCatAproArt.Columns("UNI").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .CantidadAdj = sdbgCatAproArt.Columns("CONSUM").CellValue(sdbgCatAproArt.SelBookmarks.Item(i))
            .DestinoUsuario = True
            .Anyo = Null
            .ProceCod = Null
            .Item = Null
            .mon = oProveSeleccionado.CodMon
            .Destino = gParametrosInstalacion.gsDestino
            If bAdjuntarArt Then
                If bAdjuntarProve Then
                    .EspAdj = 3
                Else
                    .EspAdj = 1
                End If
            Else
                If bAdjuntarProve Then
                    .EspAdj = 2
                Else
                    .EspAdj = 0
                End If
            End If
            
            If (gParametrosGenerales.gbOblPedidosHom And sdbcProveCod.Text <> "" And .Homologado = False) Then
                oMensajes.ArticuloNoHomologado
                frmCatalogo.sdbgAdjudicaciones.AllowAddNew = False
                sdbgCatAproArt.SelBookmarks.RemoveAll
                sdbgCatAproArt.MoveFirst
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
                    
            j = frmCatalogo.oLineasAAnyadir.Count
            frmCatalogo.oLineasAAnyadir.Add Id:=(j + 1), Prove:=StrToNull(sdbcProveCod.Text), ArtCod_Interno:=.ArtCod_Interno, ArtDen:=.ArtDen, Homologado:=.Homologado, UnidadCompra:=.UnidadCompra, CantAdj:=.CantidadAdj, UnidadPedido:=.UnidadPedido, FactorConversion:=1, vHayEsp:=.EspAdj, bDestUsu:=.DestinoUsuario, Destino:=.Destino, Mon_linea:=.mon, Generico:=.ArtGenerico, Cat1:=vCat1, Cat2:=vCat2, Cat3:=vCat3, Cat4:=vCat4, Cat5:=vCat5
            oLineasNuevas.Add Id:=j + 1, Prove:=StrToNull(sdbcProveCod.Text), ArtCod_Interno:=.ArtCod_Interno, ArtDen:=.ArtDen, Homologado:=.Homologado, UnidadCompra:=.UnidadCompra, CantAdj:=.CantidadAdj, UnidadPedido:=.UnidadPedido, FactorConversion:=1, bDestUsu:=.DestinoUsuario, vHayEsp:=.EspAdj, Destino:=.Destino, Mon_linea:=.mon, Generico:=.ArtGenerico, Cat1:=vCat1, Cat2:=vCat2, Cat3:=vCat3, Cat4:=vCat4, Cat5:=vCat5
            frmCatalogo.oLineasAAnyadir.Item(CStr(j + 1)).EspAdj = .EspAdj
            oLineasNuevas.Item(CStr(j + 1)).EspAdj = .EspAdj
        End With
    Next
           
    For Each oLineaNueva In oLineasNuevas
        sLinea = "0" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.ArtCod_Interno & Chr(m_lSeparador) & oLineaNueva.ArtDen & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oLineaNueva.ProveCod)
        sLinea = sLinea & Chr(m_lSeparador) & NullToStr(oLineaNueva.Destino) & Chr(m_lSeparador) & BooleanToSQLBinary(oLineaNueva.DestinoUsuario) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oLineaNueva.UnidadCompra) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.mon & Chr(m_lSeparador) & NullToStr(oLineaNueva.UnidadPedido) & Chr(m_lSeparador) & 1 & Chr(m_lSeparador) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.GMN1Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oLineaNueva.ArtCod_Interno & Chr(m_lSeparador) & oLineaNueva.GMN2Cod & Chr(m_lSeparador) & oLineaNueva.GMN3Cod & Chr(m_lSeparador) & oLineaNueva.GMN4Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oLineaNueva.EspAdj & Chr(m_lSeparador) & CStr(oLineaNueva.ArtGenerico)
        frmCatalogo.sdbgAdjudicaciones.AddItem sLinea
    Next
    
    frmCatalogo.sdbgAdjudicaciones.MoveLast
    
    sdbgCatAproArt.RemoveAll
    Set oLineaNueva = Nothing
    Set oLineasNuevas = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdSelMat_Click()
    
    bValidar = True
    
    If sdbcProveCod.Text <> "" Then
        Set frmSELMATProve.oProveedor = oProveSeleccionado
        frmSELMATProve.sOrigen = "frmCATALAnyaArt"
        frmSELMATProve.bRMat = bRMat
        frmSELMATProve.Show 1
    Else
        frmSELMAT.sOrigen = "frmCATALAnyaArt"
        frmSELMAT.bRComprador = bRMat
        frmSELMAT.Show 1
    End If

End Sub


Private Sub Form_Load()
    Me.Top = 1500
    Me.Left = 1500
    
    Arrange
    
    CargarRecursos

    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores

    Set oArticulos = Nothing
    Set oArticulos = oFSGSRaiz.Generar_CArticulos

    If Not gParametrosGenerales.gbOblPedidosHom Then
        sdbgCatAproArt.Columns("HOM").Visible = False
        sdbgCatAproArt.Columns("ART").Width = sdbgCatAproArt.Columns("ART").Width + 100
        sdbgCatAproArt.Columns("CONSUM").Width = sdbgCatAproArt.Columns("CONSUM").Width + 150
    End If

    bValidar = True
    
    sEquipo = ""
    sComprador = ""
    If bRMat Then
        sEquipo = basOptimizacion.gCodEqpUsuario
        sComprador = basOptimizacion.gCodCompradorUsuario
    End If
    
    PonerFieldSeparator Me
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmCatalogo.sdbgAdjudicaciones.AllowAddNew = False
End Sub

Private Sub sdbcProveCod_Change()
    
    bValidar = True
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcProveDen.Text = ""
        RespetarCombo = False
        txtEstMat = ""
        sGMN1Cod = ""
        sGMN2Cod = ""
        sGMN3Cod = ""
        sGMN4Cod = ""
        
        CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcProveCod_Click()
    
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If

End Sub


Private Sub sdbcProveCod_CloseUp()
    
    bValidar = True
    
    If sdbcProveCod.Value = "" Or sdbcProveCod.Value = "..." Then
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Value
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Value
    RespetarCombo = False
    
    ProveedorSeleccionado

End Sub


Private Sub sdbcProveCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Long

Screen.MousePointer = vbHourglass

sdbcProveCod.RemoveAll

If oProves Is Nothing Then
    Set oProves = oFSGSRaiz.generar_CProveedores
End If

'Restricci�n de material en proveedores
If bRMat Then
    oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
End If

'Sin restricciones
If Not bRMat Then
    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod.Text)
End If

Codigos = oProves.DevolverLosCodigos

For i = 0 To UBound(Codigos.Cod) - 1
    sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
Next

If Not oProves.EOF Then
    sdbcProveCod.AddItem "..." & Chr(m_lSeparador) & "....."
End If

sdbcProveCod.SelStart = 0
sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
sdbcProveCod.Refresh

Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProveCod_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcProveCod.DroppedDown = False
        sdbcProveCod.Text = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen.DroppedDown = False
        sdbcProveDen.Text = ""
        sdbcProveDen.RemoveAll
    End If
End Sub


Private Sub sdbcProveCod_PositionList(ByVal Text As String)
Dim i As Long
Dim bm As Variant

    On Error Resume Next
    
    sdbcProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveCod.Rows - 1
            bm = sdbcProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean

    If Not bValidar Then Exit Sub
    
    If Trim(sdbcProveCod.Text) = "" Then Exit Sub

    ''' Solo continuamos si existe el proveedor

    Screen.MousePointer = vbHourglass

    'Restricci�n de material en proveedores
    If bRMat Then
        oProves.BuscarProveedoresConMatDelCompDesde 1, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
    End If

    'Sin restricciones
    If Not bRMat Then
        oProves.BuscarTodosLosProveedoresDesde 1, , Trim(sdbcProveCod.Text)
    End If

    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))

    Screen.MousePointer = vbNormal

    If Not bExiste Then
        sdbcProveCod.Text = ""
        oMensajes.NoValido sIdiProve
        Screen.MousePointer = Normal
    Else
        RespetarCombo = True
        sdbcProveDen = oProves.Item(1).Den
        RespetarCombo = False
        sProveCod = sdbcProveCod
        ProveedorSeleccionado
    End If

End Sub


Private Sub sdbcProveDen_Change()
     
     bValidar = True
     
     If Not RespetarCombo Then
        RespetarCombo = True
        sdbcProveCod.Text = ""
        RespetarCombo = False
        txtEstMat = ""
        sGMN1Cod = ""
        sGMN2Cod = ""
        sGMN3Cod = ""
        sGMN4Cod = ""
        
        CargarComboDesde = True
        sdbcProveCod = ""
    End If
End Sub

Private Sub sdbcProveDen_Click()
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub


Private Sub sdbcProveDen_CloseUp()
    
    bValidar = True
    
    If sdbcProveDen.Value = "" Or sdbcProveDen.Value = "....." Then
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Value
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Value
    sProveCod = sdbcProveCod
    RespetarCombo = False

    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)

    ProveedorSeleccionado

End Sub


Private Sub sdbcProveDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Long

Screen.MousePointer = vbHourglass

sdbcProveDen.RemoveAll

If oProves Is Nothing Then
    Set oProves = oFSGSRaiz.generar_CProveedores
End If

'Restricci�n de material en proveedores
If bRMat Then
    oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcProveDen), , , , , , , True
End If

'Sin restricciones
If Not bRMat Then
    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , Trim(sdbcProveDen), , , , , , , , , , , True
End If

Codigos = oProves.DevolverLosCodigos

For i = 0 To UBound(Codigos.Cod) - 1
   
    sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
Next

If Not oProves.EOF Then
    sdbcProveDen.AddItem "..." & Chr(m_lSeparador) & "....."
End If

Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProveDen_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbgCatAproArt_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgCatAproArt_HeadClick(ByVal ColIndex As Integer)
Dim i As Long

    Screen.MousePointer = vbHourglass

    sdbgCatAproArt.RemoveAll

    If chkVerNoCat.Value = vbChecked Then
        oArticulos.CargarTodosLosArticulosPorNivelDeMaterialYProveedor sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcProveCod.Text), ColIndex, True, , , True, sEquipo, sComprador
    Else
        oArticulos.CargarTodosLosArticulosPorNivelDeMaterialYProveedor sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcProveCod.Text), ColIndex, True, , , False, sEquipo, sComprador
    End If

    sdbgCatAproArt.AllowUpdate = True
    For i = 1 To oArticulos.Count
        sdbgCatAproArt.AddItem oArticulos.Item(i).Homologado & Chr(m_lSeparador) & oArticulos.Item(i).Cod & " - " & oArticulos.Item(i).Den & Chr(m_lSeparador) & oArticulos.Item(i).Cantidad & Chr(m_lSeparador) & oArticulos.Item(i).CodigoUnidad _
        & Chr(m_lSeparador) & oArticulos.Item(i).Cod & Chr(m_lSeparador) & oArticulos.Item(i).GMN1Cod & Chr(m_lSeparador) & oArticulos.Item(i).GMN2Cod & Chr(m_lSeparador) & oArticulos.Item(i).GMN3Cod & Chr(m_lSeparador) & oArticulos.Item(i).GMN4Cod & Chr(m_lSeparador) & oArticulos.Item(i).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oArticulos.Item(i).Generico) & Chr(m_lSeparador) & oArticulos.Item(i).tipoRecepcion
    Next
    sdbgCatAproArt.AllowUpdate = False

    Screen.MousePointer = vbNormal

End Sub



Private Sub sdbgCatAproArt_RowLoaded(ByVal Bookmark As Variant)
    If ((CBool(sdbgCatAproArt.Columns("HOM").Value = 0)) And (sdbcProveCod.Text <> "")) Then
        sdbgCatAproArt.Columns("HOM").CellStyleSet "NoHom"
        sdbgCatAproArt.Columns("ART").CellStyleSet "NoHom"
        sdbgCatAproArt.Columns("CONSUM").CellStyleSet "NoHom"
        sdbgCatAproArt.Columns("UNI").CellStyleSet "NoHom"
    End If
End Sub


Private Sub txtestmat_Change()
    sdbgCatAproArt.RemoveAll
End Sub
