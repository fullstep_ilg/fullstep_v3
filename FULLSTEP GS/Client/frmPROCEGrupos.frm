VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPROCEGrupos 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DSeleccione los grupos"
   ClientHeight    =   2595
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROCEGrupos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2595
   ScaleWidth      =   4680
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ListView lstGrupos 
      Height          =   2085
      Left            =   75
      TabIndex        =   3
      Top             =   75
      Width           =   4500
      _ExtentX        =   7938
      _ExtentY        =   3678
      View            =   2
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "Ce&rrar"
      Height          =   315
      Left            =   1940
      TabIndex        =   2
      Top             =   2240
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2340
      TabIndex        =   1
      Top             =   2240
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1200
      TabIndex        =   0
      Top             =   2240
      Width           =   1005
   End
End
Attribute VB_Name = "frmPROCEGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_lIndice As Long
Public g_bModoEdicion As Boolean
Public g_iConfigAnterior As Integer
Public g_iDefEspGrupo As Integer  'para las especificaciones

Private m_bCargando As Boolean
Private m_bCargarLista As Boolean

Private oGrupoAnteriores As CGrupos

Public bRestProvMatComp As Boolean
Private m_bAceptar As Boolean
Private m_bCancelar As Boolean
Private m_bCerrar As Boolean
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String

Private Sub cmdAceptar_Click()
    'Almacena en BD:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    GuardarAmbitoDatosGrupos
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Click del boton cancelar
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento del sistema Tiempo m�ximo: 0,3</remarks>
Private Sub cmdCancelar_Click()
    Dim oGrupo As CGrupo
    
    'Restaura la configuraci�n como estaba antes:
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_lIndice
        Case 0 'DEST
            frmPROCE.g_oProcesoSeleccionado.DefDestino = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefDestino = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefDestino
                oGrupo.DestCod = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DestCod
                oGrupo.DestDen = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DestDen
            Next
        
        Case 1 ' PAGO
            frmPROCE.g_oProcesoSeleccionado.DefFormaPago = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefFormaPago = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefFormaPago
                oGrupo.PagCod = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).PagCod
                oGrupo.PagDen = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).PagDen
            Next
            
        Case 2 'FECHAS
            frmPROCE.g_oProcesoSeleccionado.DefFechasSum = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefFechasSum = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefFechasSum
                oGrupo.FechaFinSuministro = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).FechaFinSuministro
                oGrupo.FechaInicioSuministro = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).FechaInicioSuministro
            Next
            
        Case 3 'PROVE
            frmPROCE.g_oProcesoSeleccionado.DefProveActual = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefProveActual = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefProveActual
                oGrupo.ProveActual = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).ProveActual
                oGrupo.ProveActDen = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).ProveActDen
            Next
            
        Case 4 'ESP
            frmPROCE.g_oProcesoSeleccionado.DefEspGrupos = SQLBinaryToBoolean(g_iDefEspGrupo)
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefEspecificaciones = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefEspecificaciones
            Next
            frmPROCE.RestaurarConfig True
            m_bCancelar = True
            Unload Me
            Exit Sub
            
        Case 5 'DIST UON
            frmPROCE.g_oProcesoSeleccionado.DefDistribUON = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefDistribUON = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefDistribUON
                oGrupo.HayDistribucionUON = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayDistribucionUON
                Set oGrupo.DistsNivel1 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DistsNivel1
                Set oGrupo.DistsNivel2 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DistsNivel2
                Set oGrupo.DistsNivel3 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DistsNivel3
                        
            Next
            
        Case 6 'PRES ANUAL 1
            frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefPresAnualTipo1 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresAnualTipo1
                oGrupo.HayPresAnualTipo1 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayPresAnualTipo1
                Set oGrupo.Pres1Nivel4 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).Pres1Nivel4
            Next
            
        Case 7 'PRES ANUAL 2
            frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefPresAnualTipo2 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresAnualTipo2
                oGrupo.HayPresAnualTipo2 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayPresAnualTipo2
                Set oGrupo.Pres2Nivel4 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).Pres2Nivel4
            Next
            
        Case 8 'PRES 1
            frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefPresTipo1 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresTipo1
                oGrupo.HayPresTipo1 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayPresTipo1
                Set oGrupo.Pres3Nivel4 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).Pres3Nivel4
            Next
            
        Case 9 'PRES 2
            frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefPresTipo2 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresTipo2
                oGrupo.HayPresTipo2 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayPresTipo2
                Set oGrupo.Pres4Nivel4 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).Pres4Nivel4
            Next
            
            
        Case 10  'SOLICITUD DE COMPRAS
            frmPROCE.g_oProcesoSeleccionado.DefSolicitud = g_iConfigAnterior
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                oGrupo.DefSolicitud = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefSolicitud
                oGrupo.SolicitudId = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).SolicitudId
                oGrupo.SolicitudDen = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).SolicitudDen
            Next
            
    End Select
        
    frmPROCE.RestaurarConfig
    m_bCancelar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Click del boton cerrar
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:Evento del sistema Tiempo m�ximo: 0,3</remarks>

Private Sub cmdCerrar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCerrar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "cmdCerrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()
    'La carga hay que hacerla as�,porque sino el width de la columna del listview se pone por defecto muy peque�o,
    'y no se puede cambiar.Para que funcione debe rellenarse el listview cuando est� ya cargado el form.
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
    End If
    If m_bCargarLista = True Then
        DoEvents
        m_bCargando = True
        MostrarGrupos
        m_bCargando = False
        m_bCargarLista = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Load del formulario
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento del sistema Tiempo m�ximo: 0,3</remarks>

Private Sub Form_Load()
    Dim oGrupo As CGrupo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    oFSGSRaiz.pg_sFrmCargado UCase(Me.Name), True
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    m_bAceptar = False
    m_bCancelar = False
    m_bCerrar = False
    
    
    If g_bModoEdicion = True Then
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        cmdCerrar.Visible = False
        
        'Almacena en una clase los valores que tenemos al comienzo,por si se cancela:
        Set oGrupoAnteriores = oFSGSRaiz.Generar_CGrupos
    
        Select Case g_lIndice
            Case 0 'DEST
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefDestino = oGrupo.DefDestino
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DestCod = oGrupo.DestCod
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DestDen = oGrupo.DestDen
                Next
            
            Case 1 ' PAGO
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefFormaPago = oGrupo.DefFormaPago
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).PagCod = oGrupo.PagCod
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).PagDen = oGrupo.PagDen
                Next
                
            Case 2 'FECHAS
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefFechasSum = oGrupo.DefFechasSum
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).FechaFinSuministro = oGrupo.FechaFinSuministro
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).FechaInicioSuministro = oGrupo.FechaInicioSuministro
                Next
                
            Case 3 'PROVE
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefProveActual = oGrupo.DefProveActual
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).ProveActual = oGrupo.ProveActual
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).ProveActDen = oGrupo.ProveActDen
                Next
                
            Case 4 'ESP
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefEspecificaciones = oGrupo.DefEspecificaciones
                Next
            
            Case 5 'DIST UON
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefDistribUON = oGrupo.DefDistribUON
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayDistribucionUON = oGrupo.HayDistribucionUON
                    Set oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DistsNivel1 = oGrupo.DistsNivel1
                    Set oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DistsNivel2 = oGrupo.DistsNivel2
                    Set oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DistsNivel3 = oGrupo.DistsNivel3
                            
                Next
                
            Case 6 'PRES ANUAL 1
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresAnualTipo1 = oGrupo.DefPresAnualTipo1
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayPresAnualTipo1 = oGrupo.HayPresAnualTipo1
                    Set oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).Pres1Nivel4 = oGrupo.Pres1Nivel4
                Next
                
            Case 7 'PRES ANUAL 2
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresAnualTipo2 = oGrupo.DefPresAnualTipo2
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayPresAnualTipo2 = oGrupo.HayPresAnualTipo2
                    Set oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).Pres2Nivel4 = oGrupo.Pres2Nivel4
                Next
                
            Case 8 'PRES 1
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresTipo1 = oGrupo.DefPresTipo1
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayPresTipo1 = oGrupo.HayPresTipo1
                    Set oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).Pres3Nivel4 = oGrupo.Pres3Nivel4
                Next
                
            Case 9 'PRES 2
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresTipo2 = oGrupo.DefPresTipo2
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).HayPresTipo2 = oGrupo.HayPresTipo2
                    Set oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).Pres4Nivel4 = oGrupo.Pres4Nivel4
                Next
                
                
            Case 10  'SOLICITUD DE COMPRAS
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefSolicitud = oGrupo.DefSolicitud
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).SolicitudId = oGrupo.SolicitudId
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).SolicitudDen = oGrupo.SolicitudDen
                Next
                
        End Select
    
    Else
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        cmdCerrar.Visible = True
    End If
    
    m_bCargarLista = True
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCE_GRUPOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    Me.caption = Ador(0).Value
    Ador.MoveNext
    cmdAceptar.caption = Ador(0).Value
    Ador.MoveNext
    cmdCancelar.caption = Ador(0).Value
    Ador.MoveNext
    cmdCerrar.caption = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------


End Sub


Private Sub MostrarGrupos()
    Dim oGrupo As CGrupo
    Dim i As Integer
    Dim bChequeado As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lstGrupos.ListItems.clear
    
    i = 1
    
    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
        
         Select Case g_lIndice
            Case 0 'DEST
                bChequeado = oGrupo.DefDestino
            
            Case 1 ' PAGO
                bChequeado = oGrupo.DefFormaPago
                
            Case 2 'FECHAS
                bChequeado = oGrupo.DefFechasSum
            
            Case 3 'PROVE
                bChequeado = oGrupo.DefProveActual
                
            Case 4 'ESP
                bChequeado = oGrupo.DefEspecificaciones
            
            Case 5 'DIST UON
                bChequeado = oGrupo.DefDistribUON
            
            Case 6 'PRES ANUAL 1
                bChequeado = oGrupo.DefPresAnualTipo1
                
            Case 7 'PRES ANUAL 2
                bChequeado = oGrupo.DefPresAnualTipo2
                
            Case 8 'PRES 1
                bChequeado = oGrupo.DefPresTipo1
            
            Case 9 'PRES 2
                bChequeado = oGrupo.DefPresTipo2
                
            Case 10  'SOLICITUD DE COMPRAS
                bChequeado = oGrupo.DefSolicitud
                
        End Select
        
        'A�ado a la lista:
        lstGrupos.ListItems.Add , "GR" & CStr(i), oGrupo.Codigo & " - " & oGrupo.Den
        lstGrupos.ListItems(i).Checked = bChequeado
        lstGrupos.ListItems.Item("GR" & CStr(i)).Tag = oGrupo.Codigo
        
        i = i + 1
    Next
    
    Set oGrupo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "MostrarGrupos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub



''' <summary>
''' Descarga del formulario
''' Se comprueba en el que haya seleccionado algun grupo de la lista para que no se pueda ir por la X del formulario sin marcar
''' </summary>
''' <param name="Cancel">Cancelacion de la descarga del formulario</param>
''' <returns></returns>
''' <remarks>Llamada desde: cmdAceptar_Click,cmdCancelar_click,cmdCerrar_click Tiempo m�ximo: 0,3</remarks>

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    oFSGSRaiz.pg_sFrmCargado UCase(Me.Name), False
    If Not m_bAceptar And Not m_bCancelar And Not m_bCerrar Then
        If g_bModoEdicion = True Then
            cmdCancelar_Click
        Else
            cmdCerrar_Click
        End If
    Else

        If g_bModoEdicion = True Then
            Set oGrupoAnteriores = Nothing
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Selecciona un grupo para el q va a estar definido el campo configurable en frmProce
''' </summary>
''' <param name="Item">grupo</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub lstGrupos_ItemCheck(ByVal Item As MSComctlLib.listItem)
Dim oGrupo As CGrupo
Dim oGru As CGrupo
Dim bRestaurar As Boolean
Dim dblAbierto As Double
Dim lSolic As Long

    'si est� en modo consulta que no deje modificar el listbox de grupos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bModoEdicion = False And m_bCargando = False Then
        Item.Checked = Not Item.Checked
        Exit Sub
    End If
    
    Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(Item.Tag))
    If oGrupo Is Nothing Then Exit Sub

    Select Case g_lIndice

        Case 0 'Destino
                If Item.Checked Then
                    frmDatoAmbitoProce.g_iLstGrupoItem = Item.Index
                    frmDatoAmbitoProce.g_iCol = 3
                    frmDatoAmbitoProce.g_sOrigen = "DEST"
                    frmDatoAmbitoProce.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefDestino Then
                        'Y el ambito anterior no era el grupoy no hay ningun grupo ya configurado
                        'restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefDestino = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefDestino = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefDestino = False
                End If

        Case 1 'Forma de pago
                If Item.Checked Then
                    frmDatoAmbitoProce.g_iLstGrupoItem = Item.Index
                    frmDatoAmbitoProce.g_iCol = 3
                    frmDatoAmbitoProce.g_sOrigen = "PAGO"
                    frmDatoAmbitoProce.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefFormaPago Then
                        'Y el ambito anterior no era el grupo restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefFormaPago = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefFormaPago = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefFormaPago = False
                End If

        Case 2 'Fechas de suministro
                If Item.Checked Then
                    frmDatoAmbitoProce.g_iLstGrupoItem = Item.Index
                    frmDatoAmbitoProce.g_iCol = 3
                    frmDatoAmbitoProce.g_sOrigen = "FECHAS"
                    frmDatoAmbitoProce.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefFechasSum Then
                        'Y el ambito anterior no era el grupo restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefFechasSum = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefFechasSum = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefFechasSum = False
                End If

        Case 3 'Proveedor actual
                If Item.Checked Then
                    frmDatoAmbitoProce.g_iLstGrupoItem = Item.Index
                    frmDatoAmbitoProce.g_iCol = 3
                    frmDatoAmbitoProce.g_sOrigen = "PROVE"
                    frmDatoAmbitoProce.bRestProvMatComp = bRestProvMatComp
                    frmDatoAmbitoProce.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefProveActual Then
                        'Y el ambito anterior no era el grupo restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefProveActual = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefProveActual = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefProveActual = False
                End If

        Case 4 'Especificaciones
                If Item.Checked Then
                    oGrupo.DefEspecificaciones = True
                Else
                    oGrupo.DefEspecificaciones = False
                End If

        Case 5 'Dist UON
                If Item.Checked Then
                    frmDistUON.g_sOrigen = "PROCEM"
                    frmDistUON.g_iLstGrupoItem = Item.Index
                    frmDistUON.g_iCol = 3
                    frmDistUON.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefDistribUON Then
                        'Y el ambito anterior no era el grupo restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefDistribUON = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefDistribUON = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefDistribUON = False
                End If

        Case 6 'Presupuesto anual tipo1
                Set frmPROCE.g_oGrupoSeleccionado = oGrupo
                If Item.Checked Then
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo1
                    frmPRESAsig.g_iAmbitoPresup = 2
                    frmPRESAsig.g_iTipoPres = 1
                    frmPRESAsig.g_sOrigen = "frmPROCEModifConfig"
                    frmPRESAsig.g_iLstGrupoItem = Item.Index
                    frmPRESAsig.g_iCol = 3
                    frmPRESAsig.g_bModif = True
                    dblAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                    'Si el proceso no tiene items s�lo se muestran los porcentajes
                    frmPRESAsig.g_dblAbierto = dblAbierto
                    If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 Then
                        frmPRESAsig.g_dblAsignado = dblAbierto
                        frmPRESAsig.g_bHayPres = True
                    Else
                        frmPRESAsig.g_dblAsignado = 0
                        frmPRESAsig.g_bHayPres = False
                    End If
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefPresAnualTipo1 Then
                        'Y el ambito anterior no era el grupo restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefPresAnualTipo1 = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefPresAnualTipo1 = False
                End If

        Case 7 'Presupuesto anual tipo2
                Set frmPROCE.g_oGrupoSeleccionado = oGrupo
                If Item.Checked Then
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo2
                    frmPRESAsig.g_iAmbitoPresup = 2
                    frmPRESAsig.g_iTipoPres = 2
                    frmPRESAsig.g_sOrigen = "frmPROCEModifConfig"
                    frmPRESAsig.g_iLstGrupoItem = Item.Index
                    frmPRESAsig.g_iCol = 3
                    frmPRESAsig.g_bModif = True
                    dblAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                    frmPRESAsig.g_dblAbierto = dblAbierto
                    If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                        frmPRESAsig.g_dblAsignado = dblAbierto
                        frmPRESAsig.g_bHayPres = True
                    Else
                        frmPRESAsig.g_dblAsignado = 0
                        frmPRESAsig.g_bHayPres = False
                    End If
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefPresAnualTipo2 Then
                        'Y el ambito anterior no era el grupo restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefPresAnualTipo2 = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefPresAnualTipo2 = False
                End If

        Case 8 'Presupuesto tipo1
                Set frmPROCE.g_oGrupoSeleccionado = oGrupo
                If Item.Checked Then
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo1
                    frmPRESAsig.g_iAmbitoPresup = 2
                    frmPRESAsig.g_iTipoPres = 3
                    frmPRESAsig.g_sOrigen = "frmPROCEModifConfig"
                    frmPRESAsig.g_iLstGrupoItem = Item.Index
                    frmPRESAsig.g_iCol = 3
                    frmPRESAsig.g_bModif = True
                    dblAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                    frmPRESAsig.g_dblAbierto = dblAbierto
                    If frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                        frmPRESAsig.g_dblAsignado = dblAbierto
                        frmPRESAsig.g_bHayPres = True
                    Else
                        frmPRESAsig.g_dblAsignado = 0
                        frmPRESAsig.g_bHayPres = False
                    End If
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefPresTipo1 Then
                        'Y el ambito anterior no era el grupo restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefPresTipo1 = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefPresTipo1 = False
                End If

        Case 9 'Presupuesto tipo2
                Set frmPROCE.g_oGrupoSeleccionado = oGrupo
                If Item.Checked Then
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo2
                    frmPRESAsig.g_iAmbitoPresup = 2
                    frmPRESAsig.g_iTipoPres = 4
                    frmPRESAsig.g_sOrigen = "frmPROCEModifConfig"
                    frmPRESAsig.g_iLstGrupoItem = Item.Index
                    frmPRESAsig.g_iCol = 3
                    frmPRESAsig.g_bModif = True
                    dblAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                    frmPRESAsig.g_dblAbierto = dblAbierto
                    If frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                        frmPRESAsig.g_dblAsignado = dblAbierto
                        frmPRESAsig.g_bHayPres = True
                    Else
                        frmPRESAsig.g_dblAsignado = 0
                        frmPRESAsig.g_bHayPres = False
                    End If
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefPresTipo2 Then
                        'Y el ambito anterior no era el grupo restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefPresTipo2 = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefPresTipo2 = False
                End If

        Case 10  'Solicitud de compras

                If (frmPROCE.g_oProcesoSeleccionado.Estado >= validado) And (Not g_iConfigAnterior = NoDefinido) Then
                    If g_iConfigAnterior = EnItem Then
                        If Not frmPROCE.MirarSiCambioPosible(EnGrupo, lSolic, oGrupo.Id) Then
                            oMensajes.MensajeOKOnly 1389, Exclamation
                            Item.Checked = False
                            Exit Sub
                        End If
                        
                        If lSolic > -1 Then
                            oGrupo.SolicitudId = lSolic
                            
                            Dim oFormItem As CFormItem
                            Set oFormItem = oFSGSRaiz.Generar_CFormCampo
                            oGrupo.SolicitudDen = oFormItem.DevolverTitulo(lSolic)
                            Set oFormItem = Nothing
                        Else
                            oGrupo.SolicitudId = Null
                        End If
                    ElseIf NullToDbl0(frmPROCE.g_oProcesoSeleccionado.SolicitudId) > 0 Then
                        oGrupo.SolicitudId = frmPROCE.g_oProcesoSeleccionado.SolicitudId
                    End If
                    
                    'Tarea 3369
                    oGrupo.DefSolicitud = True
                                    
                    frmPROCE.g_oProcesoSeleccionado.DefSolicitud = EnGrupo
                    
                    Exit Sub
                End If

                If Item.Checked Then
                    frmSolicitudBuscar.g_iLstGrupoItem = Item.Index
                    frmSolicitudBuscar.g_iCol = 3
                    frmSolicitudBuscar.g_sOrigen = "frmPROCEModifConfig"
                    frmSolicitudBuscar.Show 1
                    'Si se ha cancelado en DatoAmbitoProce
                    If Not oGrupo.DefSolicitud Then
                        'Y el ambito anterior no era el grupo restaurar la config
                        If g_iConfigAnterior <> 2 Then
                            bRestaurar = True
                            For Each oGru In frmPROCE.g_oProcesoSeleccionado.Grupos
                                If oGru.DefSolicitud = True Then
                                    bRestaurar = False
                                    Exit For
                                End If
                            Next
                            If bRestaurar Then
                                frmPROCE.RestaurarConfig
                                frmPROCE.g_oProcesoSeleccionado.DefSolicitud = g_iConfigAnterior
                            End If
                        End If
                        Item.Checked = False
                    End If
                Else
                    oGrupo.DefSolicitud = False
                End If
    End Select

    Set oGrupo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "lstGrupos_ItemCheck", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
'*****************************************************************************************
' Si se est� configurando a nivel de grupo un dato obligatorio y no se ha seleccionado
' ning�n grupo no se cierra el form.
' Si se est� configurando a nivel de grupo un dato no obligatorio y no se ha seleccionado
' ning�n grupo el ambito del dato no cambia, se queda como estaba.
' Si el dato es "Especificaciones" y no se ha seleccionado ning�n grupo no se configura
' a nivel de grupo.
'*****************************************************************************************
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdAceptar_Click Tiempo m�ximo: 0,3</remarks>
Public Sub GuardarAmbitoDatosGrupos()
    Dim oGrupo As CGrupo
    Dim irespuesta As Integer
    Dim bSalir As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmPROCE.sdbgDatos.Col = "-1" Then Exit Sub
    
    bSalir = False
    
    Select Case g_lIndice
        Case 0 'DEST
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If oGrupo.DefDestino = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                If bSalir = True Then
                    m_bAceptar = True
                    frmPROCE.ModificarAmbitoDatosGrupos g_lIndice
                    Unload Me
                End If

        Case 1 'PAG
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If oGrupo.DefFormaPago = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                If bSalir = True Then
                    m_bAceptar = True
                    frmPROCE.ModificarAmbitoDatosGrupos g_lIndice
                    Unload Me
                End If
        
        Case 2 'FECSUM
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If oGrupo.DefFechasSum = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                If bSalir = True Then
                    m_bAceptar = True
                    frmPROCE.ModificarAmbitoDatosGrupos g_lIndice
                    Unload Me
                End If
                
        Case 3 'PROVE (no obligatorio)
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If oGrupo.DefProveActual = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                
                If bSalir = False Then
                    If g_iConfigAnterior = 2 Then
                        irespuesta = oMensajes.ModifProcesoConfigDeGrupoAItem
                        If irespuesta = vbNo Then Exit Sub
                    Else
                        irespuesta = oMensajes.PreguntaModificarAmbitoDatoAGrupo()
                        If irespuesta = vbNo Then Exit Sub
                    End If
                End If
                m_bAceptar = True
                frmPROCE.ModificarAmbitoDatosGrupos g_lIndice, bSalir
                Unload Me
        
        Case 4 'ESP (no obligatorio)
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If oGrupo.DefEspecificaciones = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If oGrupo.DefEspecificaciones = False Then
                        If TieneEspecificaciones(oGrupo) Then
                            irespuesta = oMensajes.PreguntaPerderEspecificaciones(9)
                            If irespuesta = vbNo Then bSalir = False
                            If bSalir Then
                                oGrupo.esp = Null
                                oGrupo.especificaciones = Nothing
                            End If
                            Exit For
                        End If
                    End If
                Next
                If bSalir Then
                    m_bAceptar = True
                    frmPROCE.ModificarAmbitoDatosGrupos g_lIndice
                    Unload Me
                End If
        
        Case 5 'DIST UON
                bSalir = True
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If Not oGrupo.DefDistribUON Then
                        bSalir = False
                        Exit For
                    End If
                Next
                
                If Not bSalir Then Exit Sub
                    
                m_bAceptar = True
                frmPROCE.ModificarAmbitoDatosGrupos g_lIndice, bSalir
                Unload Me
                
        Case 6 'PRES ANU1 (no obligatorio)
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefPresAnualTipo1 = True Then
                            bSalir = True
                            Exit For
                        End If
                    Next
                    
                    If bSalir = False Then
                        If gParametrosGenerales.gbOBLPP Then Exit Sub
                        If g_iConfigAnterior = 2 Then
                            irespuesta = oMensajes.ModifProcesoConfigDeGrupoAItem
                            If irespuesta = vbNo Then Exit Sub
                        Else
                            irespuesta = oMensajes.PreguntaModificarAmbitoDatoAGrupo()
                            If irespuesta = vbNo Then Exit Sub
                        End If
                    End If
                    m_bAceptar = True
                    frmPROCE.ModificarAmbitoDatosGrupos g_lIndice, bSalir
                    Unload Me
                
        Case 7 'PRES ANU2 (no obligatorio)
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefPresAnualTipo2 = True Then
                            bSalir = True
                            Exit For
                        End If
                    Next
                    
                    If bSalir = False Then
                        If gParametrosGenerales.gbOBLPC Then Exit Sub
                        If g_iConfigAnterior = 2 Then
                            irespuesta = oMensajes.ModifProcesoConfigDeGrupoAItem
                            If irespuesta = vbNo Then Exit Sub
                        Else
                            irespuesta = oMensajes.PreguntaModificarAmbitoDatoAGrupo()
                            If irespuesta = vbNo Then Exit Sub
                        End If
                    End If
                    m_bAceptar = True
                    frmPROCE.ModificarAmbitoDatosGrupos g_lIndice, bSalir
                    Unload Me
                    
        Case 8 'PRES1 (no obligatorio)
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefPresTipo1 = True Then
                            bSalir = True
                            Exit For
                        End If
                    Next
                    
                    If bSalir = False Then
                        If gParametrosGenerales.gbOBLPres3 Then Exit Sub
                        If g_iConfigAnterior = 2 Then
                            irespuesta = oMensajes.ModifProcesoConfigDeGrupoAItem
                            If irespuesta = vbNo Then Exit Sub
                        Else
                            irespuesta = oMensajes.PreguntaModificarAmbitoDatoAGrupo()
                            If irespuesta = vbNo Then Exit Sub
                        End If
                    End If
                    m_bAceptar = True
                    frmPROCE.ModificarAmbitoDatosGrupos g_lIndice, bSalir
                    Unload Me
                    
        Case 9 'PRES2 (no obligatorio)
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefPresTipo2 = True Then
                            bSalir = True
                            Exit For
                        End If
                    Next
                    
                    If bSalir = False Then
                        If gParametrosGenerales.gbOBLPres4 Then Exit Sub
                        If g_iConfigAnterior = 2 Then
                            irespuesta = oMensajes.ModifProcesoConfigDeGrupoAItem
                            If irespuesta = vbNo Then Exit Sub
                        Else
                            irespuesta = oMensajes.PreguntaModificarAmbitoDatoAGrupo()
                            If irespuesta = vbNo Then Exit Sub
                        End If
                    End If
                    m_bAceptar = True
                    frmPROCE.ModificarAmbitoDatosGrupos g_lIndice, bSalir
                    Unload Me
        
        '''Solicitud de compra
        Case 10:
                For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                    If oGrupo.DefSolicitud = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                
                If bSalir = True Then
                    m_bAceptar = True
                    frmPROCE.ModificarAmbitoDatosGrupos g_lIndice
                    Unload Me
                End If
                    
                
    End Select
    
    Set oGrupo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "GuardarAmbitoDatosGrupos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Function TieneEspecificaciones(ByVal oGrupo As CGrupo) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oGrupo.esp <> "" Then
        TieneEspecificaciones = True
        Exit Function
    End If
    If oGrupo.especificaciones Is Nothing Then
        oGrupo.CargarTodasLasEspecificaciones
    ElseIf oGrupo.especificaciones.Count = 0 Then
        oGrupo.CargarTodasLasEspecificaciones
    End If
    If oGrupo.especificaciones.Count > 0 Then
        TieneEspecificaciones = True
        Exit Function
    End If
    If oGrupo.AtributosEspecificacion Is Nothing Then
        oGrupo.CargarTodosLosAtributosEspecificacion
    ElseIf oGrupo.AtributosEspecificacion.Count = 0 Then
        oGrupo.CargarTodosLosAtributosEspecificacion
    End If
    If oGrupo.AtributosEspecificacion.Count > 0 Then
        TieneEspecificaciones = True
        Exit Function
    End If
    TieneEspecificaciones = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEGrupos", "TieneEspecificaciones", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

