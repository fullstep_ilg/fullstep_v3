
Notificación de Asignación/Desasignación de Materiales de Calidad a Proveedores


<!--INICIO BLOQUE_PROVEEDOR-->
PROVEEDOR:	@COD_PROVE - @DEN_PROVE
-------------------------------------------------------------------------------------------
<!--INICIO BLOQUE_ASIGNADOS-->
Materiales Asignados:
	<!--INICIO BLOQUE_MATERIAL_ASIG-->
	@DEN_QA	(@GMN1@GMN2@GMN3@GMN4@DEN_GS)
	<!--FIN BLOQUE_MATERIAL_ASIG-->

<!--FIN BLOQUE_ASIGNADOS-->
<!--INICIO BLOQUE_DESASIGNADOS-->
Materiales DesAsignados:
	<!--INICIO BLOQUE_MATERIAL_DESASIG-->
	@DEN_QA	(@GMN1@GMN2@GMN3@GMN4@DEN_GS)
	<!--FIN BLOQUE_MATERIAL_DESASIG-->

<!--FIN BLOQUE_DESASIGNADOS-->

<!--FIN BLOQUE_PROVEEDOR-->
	