VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroNegConcep3Desde 
   Caption         =   "Informe de ahorros negociados por concepto 3  en un per�odo"
   ClientHeight    =   5820
   ClientLeft      =   1065
   ClientTop       =   2325
   ClientWidth     =   10695
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfAhorroNegConcep3Desde.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5820
   ScaleMode       =   0  'User
   ScaleWidth      =   10092.87
   Begin VB.CommandButton cmdImprimir 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8685
      Picture         =   "frmInfAhorroNegConcep3Desde.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   240
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.CommandButton cmdActualizar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8295
      Picture         =   "frmInfAhorroNegConcep3Desde.frx":0DB4
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   240
      Width           =   315
   End
   Begin VB.Frame fraSel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   15
      TabIndex        =   16
      Top             =   0
      Width           =   9030
      Begin VB.CommandButton cmdGrafico 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7905
         Picture         =   "frmInfAhorroNegConcep3Desde.frx":0E3F
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7905
         Picture         =   "frmInfAhorroNegConcep3Desde.frx":1181
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   240
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.PictureBox picTipoGrafico 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   4335
         ScaleHeight     =   435
         ScaleWidth      =   3525
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   150
         Visible         =   0   'False
         Width           =   3525
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
            Height          =   285
            Left            =   720
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   90
            Width           =   1815
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Row.Count       =   5
            Row(0)          =   "Barras 2D"
            Row(1)          =   "Barras 3D"
            Row(2)          =   "Lineas 2D"
            Row(3)          =   "Lineas 3D"
            Row(4)          =   "Tarta"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).Caption=   "TIPO"
            Columns(0).Name =   "TIPO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   3201
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.CommandButton cmdCalFecApeDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         Picture         =   "frmInfAhorroNegConcep3Desde.frx":12CB
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   660
         TabIndex        =   0
         Top             =   240
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalFecApeHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3780
         Picture         =   "frmInfAhorroNegConcep3Desde.frx":1855
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   2640
         TabIndex        =   2
         Top             =   240
         Width           =   1110
      End
      Begin VB.OptionButton optDir 
         Caption         =   "Adj. directa"
         Height          =   195
         Left            =   4320
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   390
         Width           =   1350
      End
      Begin VB.OptionButton optTodos 
         Caption         =   "Todos"
         Height          =   195
         Left            =   5715
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   390
         Width           =   975
      End
      Begin VB.OptionButton optReu 
         Caption         =   "Adj. en reuni�n"
         Height          =   195
         Left            =   4320
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   150
         Value           =   -1  'True
         Width           =   2340
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMon 
         Height          =   285
         Left            =   6750
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   240
         Width           =   1035
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1164
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1879
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "EQUIV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "SSDBCombo1"
         BackColor       =   -2147483643
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Hasta:"
         Height          =   225
         Left            =   2160
         TabIndex        =   19
         Top             =   300
         Width           =   450
      End
      Begin VB.Label lblFecDesde 
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   300
         Width           =   555
      End
   End
   Begin VB.Frame fraDesde 
      Caption         =   "Mostrar resultados desde presupuesto"
      Height          =   735
      Left            =   15
      TabIndex        =   13
      Top             =   720
      Width           =   10650
      Begin VB.CommandButton cmdSelPres 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7230
         Picture         =   "frmInfAhorroNegConcep3Desde.frx":1DDF
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   240
         Width           =   345
      End
      Begin VB.CommandButton cmdBorrar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6850
         Picture         =   "frmInfAhorroNegConcep3Desde.frx":1E4B
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   240
         Width           =   345
      End
      Begin VB.Label lblPres 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   660
         TabIndex        =   14
         Top             =   260
         Width           =   5490
      End
   End
   Begin VB.PictureBox picLegend 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   805
      Left            =   9075
      Picture         =   "frmInfAhorroNegConcep3Desde.frx":1EF0
      ScaleHeight     =   810
      ScaleWidth      =   1740
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   30
      Visible         =   0   'False
      Width           =   1733
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   0
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   5460
      Width           =   10650
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegConcep3Desde.frx":5BF2
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegConcep3Desde.frx":5C0E
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegConcep3Desde.frx":5C2A
      DividerType     =   1
      DividerStyle    =   2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   7
      Columns(0).Width=   2275
      Columns(0).Caption=   "CAPTION"
      Columns(0).Name =   "CAPTION"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "PRESCAPTION"
      Columns(1).Name =   "PRESCAPTION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "PRES"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,#"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "ADJCAPTION"
      Columns(3).Name =   "ADJCAPTION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "ADJ"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,#"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2090
      Columns(5).Caption=   "AHOCAPTION"
      Columns(5).Name =   "AHOCAPTION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "AHO"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "#,#"
      Columns(6).FieldLen=   256
      _ExtentX        =   18785
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   3915
      Left            =   15
      TabIndex        =   11
      Top             =   1530
      Width           =   10635
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   9
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegConcep3Desde.frx":5C46
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4744445
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegConcep3Desde.frx":5C62
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegConcep3Desde.frx":5C7E
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Fecha"
      Columns(0).Name =   "FEC"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   3200
      Columns(1).Caption=   "Fecha"
      Columns(1).Name =   "FECHACORTA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16776960
      Columns(2).Width=   2884
      Columns(2).Caption=   "Referencia"
      Columns(2).Name =   "REF"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1640
      Columns(3).Caption=   "Procesos"
      Columns(3).Name =   "NUMPROCE"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   12632256
      Columns(4).Width=   2963
      Columns(4).Caption=   "Presupuesto"
      Columns(4).Name =   "PRES"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   15400959
      Columns(5).Width=   2963
      Columns(5).Caption=   "Adjudicado"
      Columns(5).Name =   "ADJ"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "Standard"
      Columns(5).FieldLen=   256
      Columns(5).HasBackColor=   -1  'True
      Columns(5).BackColor=   15400959
      Columns(6).Width=   2514
      Columns(6).Caption=   "Ahorro"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "Standard"
      Columns(6).FieldLen=   256
      Columns(7).Width=   1455
      Columns(7).Caption=   "%"
      Columns(7).Name =   "PORCEN"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   2
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).NumberFormat=   "0.0#\%"
      Columns(7).FieldLen=   256
      Columns(8).Width=   1191
      Columns(8).Caption=   "Detalle"
      Columns(8).Name =   "DETALLE"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Style=   4
      Columns(8).ButtonsAlways=   -1  'True
      _ExtentX        =   18759
      _ExtentY        =   6906
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   3915
      Left            =   15
      OleObjectBlob   =   "frmInfAhorroNegConcep3Desde.frx":5C9A
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   1530
      Width           =   9855
   End
   Begin VB.PictureBox picLegend2 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   720
      Left            =   9075
      Picture         =   "frmInfAhorroNegConcep3Desde.frx":76C0
      ScaleHeight     =   720
      ScaleWidth      =   1635
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   30
      Visible         =   0   'False
      Width           =   1635
   End
End
Attribute VB_Name = "frmInfAhorroNegConcep3Desde"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Formulario de listados
Public ofrmLstAhorroNeg As frmLstINFAhorrosNeg

'Variables para func. combos
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

'Monedas
Private oMonedas As CMonedas
Private dequivalencia As Double
Private oMon As CMoneda

'Recordset
Private ADORs As Ador.Recordset

'Variables para el presupuesto 3
Public g_sPRES1 As String
Public g_sPRES2 As String
Public g_sPRES3 As String
Public g_sPRES4 As String

'Variables utilizadas para el multilenguaje
Private sIdiTipoGrafico(5) As String
Private sIdiFecha As String
Private sIdiMonCent As String
Private sIdiTotal As String
Private sIdiPresup As String
Private sIdiAdj As String
Private sIdiAhor As String
Private sIdiDetResult As String
Private m_sIdiDetalle As String
'Variables para las UO
Private m_sLblUO As String
Private bRUO As Boolean
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String

Private Sub cmdActualizar_Click()
            
    If txtFecDesde = "" Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then txtFecDesde.SetFocus
        Exit Sub
    End If
    
    If Not IsDate(txtFecDesde) Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then txtFecDesde.SetFocus
        Exit Sub
    End If
    
    If txtFecHasta <> "" Then
        If Not IsDate(txtFecHasta) Then
            oMensajes.NoValido sIdiFecha
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
        
        If CDate(txtFecDesde) > CDate(txtFecHasta) Then
            oMensajes.FechaDesdeMayorFechaHasta
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    End If
    
    If bRUO Then
        If basOptimizacion.gUON1Usuario <> "" And lblPres.caption = "" Then
            ColocarUODeUsuario
        End If
    End If
    
    Set ADORs = Nothing
    
    Screen.MousePointer = vbHourglass
    
    
    Set ADORs = Nothing
    
    If txtFecHasta <> "" Then
        Set ADORs = oGestorInformes.AhorroNegociadoConcep3DesdeHasta(CDate(txtFecDesde), CDate(txtFecHasta), g_sPRES1, g_sPRES2, g_sPRES3, g_sPRES4, optReu, optDir, , , , , g_sUON1, g_sUON2, g_sUON3)

    Else
        Set ADORs = oGestorInformes.AhorroNegociadoConcep3DesdeHasta(CDate(txtFecDesde), , g_sPRES1, g_sPRES2, g_sPRES3, g_sPRES4, optReu, optDir, , , , , g_sUON1, g_sUON2, g_sUON3)

    End If

    Screen.MousePointer = vbNormal

    If ADORs Is Nothing Then Exit Sub

    CargarGrid
    
    If cmdGrid.Visible = True Then
        MSChart1.Visible = True
        MostrarGrafico sdbcTipoGrafico.Value
    End If
    
End Sub


Private Sub cmdBorrar_Click()
    lblPres.caption = ""
    g_sPRES1 = ""
    g_sPRES2 = ""
    g_sPRES3 = ""
    g_sPRES4 = ""
    g_sUON1 = ""
    g_sUON2 = ""
    g_sUON3 = ""
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub cmdGrafico_Click()
    
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
    
        Screen.MousePointer = vbHourglass
        sdbcTipoGrafico = sIdiTipoGrafico(2)
        MostrarGrafico sIdiTipoGrafico(2)
        picTipoGrafico.Visible = True
        sdbcMon.Visible = False
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdGrid_Click()
        
        picTipoGrafico.Visible = False
        sdbcMon.Visible = True
        picLegend.Visible = False
        picLegend2.Visible = False
        
        cmdGrafico.Visible = True
        cmdGrid.Visible = False
        sdbgRes.Visible = True
        MSChart1.Visible = False
    
End Sub

Private Sub cmdImprimir_Click()
Set ofrmLstAhorroNeg = New frmLstINFAhorrosNeg
    ofrmLstAhorroNeg.sOrigen = "frmInfAhorroNegConcep3Desde"
    ofrmLstAhorroNeg.WindowState = vbNormal
    
    If txtFecDesde <> "" Then
        ofrmLstAhorroNeg.txtFecDesde.Text = txtFecDesde.Text
    End If
    
    If txtFecHasta <> "" Then
        ofrmLstAhorroNeg.txtFecHasta.Text = txtFecHasta.Text
    End If
    ofrmLstAhorroNeg.bRUO = bRUO
    If optReu Then ofrmLstAhorroNeg.optReu.Value = True
    If optDir Then ofrmLstAhorroNeg.optDir.Value = True
    If optTodos Then ofrmLstAhorroNeg.optTodos.Value = True

    ofrmLstAhorroNeg.sdbcMon.Text = sdbcMon.Text
    ofrmLstAhorroNeg.sdbcMon_Validate False
    
    ofrmLstAhorroNeg.txtEstMat.Text = lblPres.caption
    ofrmLstAhorroNeg.m_sUON1 = g_sUON1
    ofrmLstAhorroNeg.m_sUON2 = g_sUON2
    ofrmLstAhorroNeg.m_sUON3 = g_sUON3
'    ofrmLstAhorroNeg.PonerPresSeleccionado3 g_sPRES1, g_sPRES2, g_sPRES3, g_sPRES4, g_sUON1, g_sUON2, g_sUON3
    ofrmLstAhorroNeg.Show 1


End Sub

Private Sub cmdSelPres_Click()
      
    frmSELPresUO.sOrigen = "frmInfAhorroNegConcep3Desde"
    frmSELPresUO.bRUO = bRUO
    frmSELPresUO.g_iTipoPres = 3
    frmSELPresUO.bMostrarBajas = True
    frmSELPresUO.Show 1
    
End Sub

Private Sub Form_Activate()
    
    sdbgRes.SelBookmarks.RemoveAll
End Sub

Private Sub Form_Load()
    
    Me.Height = 6180
    Me.Width = 10815
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    CargarRecursos
   
    PonerFieldSeparator Me
   
    g_sUON1 = ""
    g_sUON2 = ""
    g_sUON3 = ""
    ConfigurarSeguridad
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        dequivalencia = oMonedas.Item(1).Equiv
    End If
    
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & ""
    
    g_sPRES1 = ""
    g_sPRES2 = ""
    g_sPRES3 = ""
    g_sPRES4 = ""

End Sub

Private Sub Form_Resize()
    
    If Me.Width > 800 Then
        
        sdbgRes.Width = Me.Width - 800
        
        sdbgRes.Columns(1).Width = (sdbgRes.Width - 570) * 0.1
        sdbgRes.Columns(2).Width = (sdbgRes.Width - 570) * 0.16
        sdbgRes.Columns(3).Width = (sdbgRes.Width - 570) * 0.1
        sdbgRes.Columns(4).Width = (sdbgRes.Width - 570) * 0.16
        sdbgRes.Columns(5).Width = (sdbgRes.Width - 570) * 0.16
        sdbgRes.Columns(6).Width = (sdbgRes.Width - 570) * 0.16
        sdbgRes.Columns(7).Width = (sdbgRes.Width - 570) * 0.08
        sdbgRes.Columns(8).Width = (sdbgRes.Width - 570) * 0.08
        
        
        
        sdbgTotales.Top = Me.Height - 780
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Columns(0).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(1).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(2).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(3).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(4).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(5).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(6).Width = sdbgTotales.Width / 8
        
        MSChart1.Width = Me.Width - 800
        
        Me.fraDesde.Width = sdbgRes.Width
    End If
    
    If Me.Height > 2500 Then
        sdbgRes.Height = Me.Height - 2400
        MSChart1.Height = Me.Height - 2400
    End If
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set ADORs = Nothing
    Set oMonedas = Nothing
    Set oMon = Nothing
    Me.Visible = False
    
End Sub


Private Sub CargarGrid()
        
    Dim dpres As Double
    Dim dadj As Double
    Dim sFecha As String
    dpres = 0
    dadj = 0
    
    sdbgRes.RemoveAll
    sdbgTotales.RemoveAll
    
    While Not ADORs.EOF
        
        If ADORs("ADJDIR").Value = 0 Then
            sFecha = Format(ADORs(0).Value, "short date") & " " & Format(ADORs(0).Value, "short time")
        Else
            sFecha = Format(ADORs(0).Value, "short date")
        End If
        
        sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & sFecha & Chr(m_lSeparador) & ADORs.Fields("REF").Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & dequivalencia * ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & ADORs(5).Value
        dpres = dpres + dequivalencia * ADORs(2).Value
        dadj = dadj + dequivalencia * ADORs(3).Value
        ADORs.MoveNext
    Wend
    ADORs.Close
    Set ADORs = Nothing
    
   sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & dpres & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & dadj & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & dpres - dadj

End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    Select Case Tipo
    
'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                picLegend.Visible = True
                picLegend2.Visible = False
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                picLegend.Visible = False
                picLegend2.Visible = True
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                picLegend.Visible = True
                picLegend2.Visible = False
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
           
End Sub


Private Sub lblPres_Change()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub optDir_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optReu_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optTodos_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub


Private Sub BorrarDatosTotales()
    sdbgTotales.RemoveAll
    sdbgTotales.Columns(6).CellStyleSet "Normal"
    sdbgTotales.Refresh
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbgRes_BtnClick()
Dim frm2 As frmInfAhorroNegFechaDetalle

    If sdbgRes.Rows = 0 Then Exit Sub
    
    Set ADORs = Nothing
    
    Screen.MousePointer = vbHourglass
        
    Set ADORs = oGestorInformes.AhorroNegociadoFechaCon3(sdbgRes.Columns(0).Value, g_sPRES1, g_sPRES2, g_sPRES3, g_sPRES4, , , , , , True, , optReu, optDir, g_sUON1, g_sUON2, g_sUON3)
    
    If ADORs Is Nothing Then Exit Sub
    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
    Set frm2 = New frmInfAhorroNegFechaDetalle
    frm2.g_sOrigen = "Concep3Desde"
    frm2.dequivalencia = dequivalencia
    frm2.sFecha = sdbgRes.Columns(0).Value
    frm2.caption = m_sIdiDetalle & " " & gParametrosGenerales.gsSingPres3 & ": " & sdbgRes.Columns(0).Value
    While Not ADORs.EOF
        frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
        ADORs.MoveNext
    Wend
    ADORs.Close
    Set ADORs = Nothing
    
    frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
    frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
    
    Screen.MousePointer = vbNormal
    
    frm2.Show 1
    sdbgRes.SelBookmarks.RemoveAll
    
End Sub

Private Sub sdbgRes_DblClick()
Dim frm As frmInfAhorroNegPresDetalle

    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    
    Set ADORs = Nothing
    
    Screen.MousePointer = vbHourglass
    
    If g_sPRES4 <> "" Then
    
    Else
        Set ADORs = oGestorInformes.AhorroNegociadoConcep3Fecha(CDate(sdbgRes.Columns(0).Value), g_sPRES1, g_sPRES2, g_sPRES3, , , , True, , optReu, optDir, g_sUON1, g_sUON2, g_sUON3)
        If ADORs Is Nothing Then Exit Sub
    End If
    
    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
    Set frm = New frmInfAhorroNegPresDetalle
    frm.g_sPRES1 = g_sPRES1
    frm.g_sPRES2 = g_sPRES2
    frm.g_sPRES3 = g_sPRES3
    frm.g_sPRES4 = g_sPRES4
    frm.g_sUON1 = g_sUON1
    frm.g_sUON2 = g_sUON2
    frm.g_sUON3 = g_sUON3
    frm.g_sOrigen = "Concep3Desde"
    
    frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsSingPres3
    frm.g_dEquivalencia = dequivalencia
    
    frm.caption = sIdiDetResult & "      " & sdbgRes.Columns(0).Value
    frm.g_dFecha = CDate(sdbgRes.Columns(0).Value)
    frm.g_bSoloDeAdjDir = optDir
    frm.g_bSoloEnReunion = optReu
    
    If Not ADORs Is Nothing Then
        While Not ADORs.EOF
            frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
            ADORs.MoveNext
        Wend
        ADORs.Close
    End If
    Set ADORs = Nothing
            
    frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 1000
    frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
    Screen.MousePointer = vbNormal
    frm.Show 1
    
    sdbgRes.SelBookmarks.RemoveAll

End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
        
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If

End Sub

Private Sub sdbgTotales_RowLoaded(ByVal Bookmark As Variant)
If sdbgTotales.Columns(6).Value = "" Then Exit Sub
    
    If sdbgTotales.Columns(6).Value < 0 Then
        sdbgTotales.Columns(6).CellStyleSet "Red"
    Else
        If sdbgTotales.Columns(6).Value > 0 Then
            sdbgTotales.Columns(6).CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
        
End Sub

Private Sub sdbcMon_DropDown()
    
    sdbcMon.RemoveAll
    
    oMonedas.CargarTodasLasMonedas , , , , , False, True
      
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcMon.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMon.Rows - 1
            bm = sdbcMon.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMon.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcMon.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
    End If
    
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset
Dim sCaption1 As String
Dim sCaption2 As String

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRONEG_PRES34_DESDE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sCaption1 = Ador(0).Value '1
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value
        Ador.MoveNext
        optReu.caption = Ador(0).Value
        Ador.MoveNext
        optDir.caption = Ador(0).Value '5
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        
        'Tipo de gr�fico
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        
        sCaption2 = Ador(0).Value
        fraDesde = gParametrosGenerales.gsSingPres3
        Ador.MoveNext
        
        Ador.MoveNext
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(5).caption = Ador(0).Value
        sIdiAdj = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(6).caption = Ador(0).Value
        
        sIdiAhor = Ador(0).Value 'Ahorro
        Ador.MoveNext
        sIdiFecha = Ador(0).Value
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value 'Moneda central
        Ador.MoveNext
        sIdiTotal = Ador(0).Value 'Total
        Ador.MoveNext
        sIdiPresup = Ador(0).Value 'Presupuestos
        Ador.MoveNext
        sIdiDetResult = Ador(0).Value 'Detalle de resultado con fecha
        Ador.MoveNext
        m_sIdiDetalle = Ador(0).Value 'Detalle
        sdbgRes.Columns(8).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value

        Ador.Close
    
        Me.caption = sCaption1 & " " & gParametrosGenerales.gsSingPres3 & " " & sCaption2
    End If

    picLegend.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGMATDESDE_PICLEGEND + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)
    picLegend2.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGMATDESDE_PICLEGEND2 + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)

    Set Ador = Nothing

End Sub


Private Sub txtFecDesde_Change()
    MSChart1.Visible = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub txtFecHasta_Change()
    MSChart1.Visible = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Public Sub MostrarPresSeleccionado()
    
    g_sPRES1 = frmSELPresUO.g_sPRES1
    g_sPRES2 = frmSELPresUO.g_sPRES2
    g_sPRES3 = frmSELPresUO.g_sPRES3
    g_sPRES4 = frmSELPresUO.g_sPRES4
    g_sUON1 = frmSELPresUO.g_sUON1
    g_sUON2 = frmSELPresUO.g_sUON2
    g_sUON3 = frmSELPresUO.g_sUON3
    
    lblPres = ""
    If g_sUON1 <> "" Then
        lblPres = "(" & g_sUON1
        If g_sUON2 <> "" Then
            lblPres = lblPres & " - " & g_sUON2
            If g_sUON3 <> "" Then
                lblPres = lblPres & " - " & g_sUON3 & ") "
            Else
                lblPres = lblPres & ") "
            End If
        Else
            lblPres = lblPres & ") "
        End If
    End If
    
    If g_sPRES4 <> "" Then
        lblPres = lblPres & g_sPRES1 & " - " & g_sPRES2 & " - " & g_sPRES3 & " - " & g_sPRES4 & " " & frmSELPresUO.g_sDenPres
    ElseIf g_sPRES3 <> "" Then
        lblPres = lblPres & g_sPRES1 & " - " & g_sPRES2 & " - " & g_sPRES3 & " " & frmSELPresUO.g_sDenPres
    ElseIf g_sPRES2 <> "" Then
        lblPres = lblPres & g_sPRES1 & " - " & g_sPRES2 & " " & frmSELPresUO.g_sDenPres
    ElseIf g_sPRES1 <> "" Then
        lblPres = lblPres & g_sPRES1 & " " & frmSELPresUO.g_sDenPres
    End If
        
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
    
End Sub
Private Sub ColocarUODeUsuario()
    g_sUON1 = NullToStr(basOptimizacion.gUON1Usuario)
    g_sUON2 = NullToStr(basOptimizacion.gUON2Usuario)
    g_sUON3 = NullToStr(basOptimizacion.gUON3Usuario)
    m_sLblUO = ""
    If g_sUON1 <> "" Then
        m_sLblUO = "(" & g_sUON1
        If g_sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & g_sUON2
            If g_sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & g_sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If
    lblPres = m_sLblUO

End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    bRUO = False
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegCon3RestUO)) Is Nothing) Then
        bRUO = True
        ColocarUODeUsuario
    End If
End Sub
