VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstINFAhorrosApl 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de ahorros aplicados"
   ClientHeight    =   4095
   ClientLeft      =   705
   ClientTop       =   1455
   ClientWidth     =   8430
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstINFAhorrosApl.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4095
   ScaleWidth      =   8430
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   8430
      TabIndex        =   1
      Top             =   3720
      Width           =   8430
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   7050
         TabIndex        =   0
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3675
      Left            =   30
      TabIndex        =   2
      Top             =   0
      Width           =   8385
      _ExtentX        =   14790
      _ExtentY        =   6482
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstINFAhorrosApl.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraAnyos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraMon"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraTipo"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "fraAnyoPres"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstINFAhorrosApl.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame6"
      Tab(1).Control(1)=   "Frame3"
      Tab(1).Control(2)=   "Frame5"
      Tab(1).ControlCount=   3
      Begin VB.Frame fraAnyoPres 
         Caption         =   "Frame1"
         Height          =   855
         Left            =   180
         TabIndex        =   30
         Top             =   2220
         Width           =   4875
         Begin SSDataWidgets_B.SSDBCombo sdbcADesdePres 
            Height          =   285
            Left            =   960
            TabIndex        =   31
            Top             =   330
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAHastaPres 
            Height          =   285
            Left            =   3435
            TabIndex        =   32
            Top             =   330
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin VB.Label lblDesdePres 
            Caption         =   "Desde:"
            Height          =   195
            Left            =   210
            TabIndex        =   34
            Top             =   360
            Width           =   780
         End
         Begin VB.Label lblHastaPres 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   2670
            TabIndex        =   33
            Top             =   360
            Width           =   690
         End
      End
      Begin VB.Frame Frame6 
         Height          =   750
         Left            =   -74850
         TabIndex        =   27
         Top             =   1260
         Width           =   8070
         Begin VB.CheckBox chkHist 
            Caption         =   "Mostrar detalle"
            Height          =   195
            Left            =   345
            TabIndex        =   29
            Top             =   315
            Width           =   2190
         End
         Begin VB.ComboBox CmbDetalle 
            Height          =   315
            ItemData        =   "frmLstINFAhorrosApl.frx":0CEA
            Left            =   2925
            List            =   "frmLstINFAhorrosApl.frx":0CEC
            Style           =   2  'Dropdown List
            TabIndex        =   28
            Top             =   255
            Width           =   1050
         End
      End
      Begin VB.Frame fraTipo 
         Caption         =   "Filtro por material"
         Height          =   855
         Left            =   180
         TabIndex        =   23
         Top             =   1320
         Width           =   8070
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   7110
            Picture         =   "frmLstINFAhorrosApl.frx":0CEE
            Style           =   1  'Graphical
            TabIndex        =   26
            Top             =   360
            Width           =   345
         End
         Begin VB.TextBox txtEst 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   375
            Width           =   6765
         End
         Begin VB.CommandButton cmdSel 
            Height          =   315
            Left            =   7545
            Picture         =   "frmLstINFAhorrosApl.frx":0D93
            Style           =   1  'Graphical
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   360
            Width           =   345
         End
      End
      Begin VB.Frame Frame3 
         Height          =   705
         Left            =   -74850
         TabIndex        =   22
         Top             =   510
         Width           =   8070
         Begin VB.OptionButton optReu 
            Caption         =   "Adj. en reuni�n"
            Height          =   195
            Left            =   360
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   315
            Width           =   2445
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Todos"
            Height          =   195
            Left            =   5715
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   315
            Value           =   -1  'True
            Width           =   1800
         End
         Begin VB.OptionButton optDir 
            Caption         =   "Adj. directa"
            Height          =   195
            Left            =   3030
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   315
            Width           =   2340
         End
      End
      Begin VB.Frame Frame5 
         Height          =   1125
         Left            =   -74850
         TabIndex        =   20
         Top             =   2070
         Width           =   8070
         Begin VB.ComboBox cmbNivel 
            Height          =   315
            ItemData        =   "frmLstINFAhorrosApl.frx":0DFF
            Left            =   2760
            List            =   "frmLstINFAhorrosApl.frx":0E01
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   270
            Width           =   1365
         End
         Begin VB.OptionButton opDatGraf 
            Caption         =   "Ver datos y gr�fico"
            Height          =   195
            Left            =   5715
            TabIndex        =   14
            Top             =   735
            Width           =   2100
         End
         Begin VB.OptionButton opVerGraf 
            Caption         =   "Ver solo gr�fico"
            Height          =   195
            Left            =   3045
            TabIndex        =   13
            Top             =   735
            Width           =   2145
         End
         Begin VB.OptionButton opVerDat 
            Caption         =   "Ver solo datos"
            Height          =   195
            Left            =   375
            TabIndex        =   12
            Top             =   735
            Value           =   -1  'True
            Width           =   2445
         End
         Begin VB.Label lblNivel 
            Alignment       =   1  'Right Justify
            Caption         =   "Desglosar listado al nivel:"
            Height          =   195
            Left            =   120
            TabIndex        =   21
            Top             =   330
            Width           =   2340
         End
      End
      Begin VB.Frame fraMon 
         Height          =   855
         Left            =   5220
         TabIndex        =   18
         Top             =   2220
         Width           =   3030
         Begin SSDataWidgets_B.SSDBCombo sdbcMon 
            Height          =   285
            Left            =   1140
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   330
            Width           =   1140
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1164
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3651
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   1879
            Columns(2).Caption=   "Equivalencia"
            Columns(2).Name =   "EQUIV"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   2011
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblMon 
            Caption         =   "Moneda:"
            Height          =   240
            Left            =   360
            TabIndex        =   19
            Top             =   360
            Width           =   915
         End
      End
      Begin VB.Frame fraAnyos 
         Height          =   840
         Left            =   180
         TabIndex        =   15
         Top             =   450
         Width           =   8070
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyoDesde 
            Height          =   285
            Left            =   1110
            TabIndex        =   3
            Top             =   300
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMesDesde 
            Height          =   285
            Left            =   2190
            TabIndex        =   4
            Top             =   300
            Width           =   1470
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2593
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyoHasta 
            Height          =   285
            Left            =   4845
            TabIndex        =   5
            Top             =   300
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMesHasta 
            Height          =   285
            Left            =   5925
            TabIndex        =   6
            Top             =   300
            Width           =   1470
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2593
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin VB.Label lblAnyoHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   4080
            TabIndex        =   17
            Top             =   330
            Width           =   690
         End
         Begin VB.Label lblAnyoDesde 
            Caption         =   "Desde:"
            Height          =   195
            Left            =   360
            TabIndex        =   16
            Top             =   330
            Width           =   780
         End
      End
      Begin VB.Timer Timer1 
         Interval        =   2000
         Left            =   7440
         Top             =   -90
      End
   End
End
Attribute VB_Name = "frmLstINFAhorrosApl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private sSeleccion  As String
Private bRespetarCombo As Boolean
Private bOculto As Boolean
' Variables de restricciones
Public bRMat As Boolean      ' material asociado
Public bRUO As Boolean         ' equipo comprador
Private oICompAsignado As ICompProveAsignados 'control de seleccion de material asociado
'Vbles. para la selecci�n de material
Private sGMN1Cod As String
Private sGMN2Cod As String
Private sGMN3Cod As String
Private sGMN4Cod As String
'Vbles. para la selecci�n de proyecto
Public sProy1 As String
Public sProy2 As String
Public sProy3 As String
Public sProy4 As String
'Vbles. para la selecci�n de partidas contables
Public sParCon1 As String
Public sParCon2 As String
Public sParCon3 As String
Public sParCon4 As String
'Vbles. para la selecci�n de unidades organizativas
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String
Private m_sLblUO As String
Private m_iNivelUO As Integer

'Vbles. para la selecci�n de concepto3
Public sConcep3_1 As String
Public sConcep3_2 As String
Public sConcep3_3 As String
Public sConcep3_4 As String
'Vbles. para la selecci�n de concepto4
Public sConcep4_1 As String
Public sConcep4_2 As String
Public sConcep4_3 As String
Public sConcep4_4 As String

'Desglose
Private iNivel As Integer

' Origen llamada a formulario
Public sOrigen As String

'Variables para func. combos
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean
Private sMonedaOrig As String
Private sMoneda As String
Private oMonedas As CMonedas
Private dequivalencia As Double
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4

'Variables para el multilenguaje
Private sIdiAnual As String
Private sIdiMensual As String
Private sIdiTitGeneral As String
Private sIdiTitMat As String
Private sIdiTit  As String
Private sIdiTitUO  As String
Private sIdiGenerando As String
Private sIdiSeleccionando As String
Private sIdiVisualizando  As String
Private sIdiMeses(12) As String
Private sIdiMat As String
Private sIdiMon As String
Private sIdiAnyoInic As String
Private sIdiMonCent As String
Private sIdiOpc As String
Private sIdiUO As String
Private sIdiSeleccion As String
Private sIdiAdjDir As String
Private sIdiReu As String
Private sIdiTodos As String
Private sIdiEquivalencia As String
Private sIdiPeriodoDesde As String
Private sIdiPeriodoHasta As String
Private srIdiPresupGen As String
Private srIdiPorcentajeAhorro As String
Private srIdiFechaGen As String
Private srIdiMaterialMat As String
Private srIdiPorcentajeAhorroMat As String
Private srIdiPresupTotal As String
Private srIdiPorcentajeObj As String
Private srIdiConsumido As String
Private srIdiAdjudicado As String
Private srIdiAhorro As String
Private srIdiUOUO As String
Private srIdiPag As String
Private srIdiDe As String
Private srIdiLPresupuestado As String
Private srIdiLPositivo As String
Private srIdiLNegativo As String
Private sIdiAnyo  As String

Public Sub MostrarParConSeleccionada()
Dim iNivel As Integer
Dim i As Integer
    
    sParCon1 = frmSELPresAnuUON.g_sPRES1
    sParCon2 = frmSELPresAnuUON.g_sPRES2
    sParCon3 = frmSELPresAnuUON.g_sPRES3
    sParCon4 = frmSELPresAnuUON.g_sPRES4
    sUON1 = frmSELPresAnuUON.g_sUON1
    sUON2 = frmSELPresAnuUON.g_sUON2
    sUON3 = frmSELPresAnuUON.g_sUON3
    
    m_sLblUO = ""
    If sUON1 <> "" Then
        m_sLblUO = "(" & sUON1
        If sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & sUON2
            If sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If
    
    iNivel = 1
    If sParCon4 <> "" Then
        txtEst = m_sLblUO & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " - " & sParCon4 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 4
    ElseIf sParCon3 <> "" Then
        txtEst = m_sLblUO & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 3
    ElseIf sParCon2 <> "" Then
        txtEst = m_sLblUO & sParCon1 & " - " & sParCon2 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 2
    ElseIf sParCon1 <> "" Then
        txtEst = m_sLblUO & sParCon1 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 1
    Else
        txtEst = m_sLblUO
    End If

    
    cmbNivel.clear
       
    For i = gParametrosGenerales.giNEPC To iNivel Step -1
        If i = 0 Then Exit For
        cmbNivel.AddItem i
    Next i
    
    cmbNivel.ListIndex = 0
        
    If iNivel = 1 Then
        If sParCon1 <> "" Then
            cmbNivel.Text = iNivel + 1
        Else
            cmbNivel.Text = "1"
        End If
    Else
        If iNivel = 4 Then
            cmbNivel.Text = "4"
        Else
            cmbNivel.Text = iNivel + 1
        End If
    End If
End Sub
Public Sub PonerMatSeleccionado(Optional sOrigen As String)
Dim iNivel As Integer
Dim i As Integer

    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    If sOrigen = "frmInfAhorroApliMat" Then
        Set oGMN1Seleccionado = frmInfAhorroApliMat.oGMN1Seleccionado
        Set oGMN2Seleccionado = frmInfAhorroApliMat.oGMN2Seleccionado
        Set oGMN3Seleccionado = frmInfAhorroApliMat.oGMN3Seleccionado
        Set oGMN4Seleccionado = frmInfAhorroApliMat.oGMN4Seleccionado
    Else
        Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
        Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
        Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
        Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    End If
        
    iNivel = 1
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
        txtEst = sGMN1Cod
        iNivel = 1
    Else
        sGMN1Cod = ""
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        txtEst = txtEst & " - " & sGMN2Cod
        iNivel = iNivel + 1
    Else
        sGMN2Cod = ""
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        txtEst = txtEst & " - " & sGMN3Cod
        iNivel = iNivel + 1
    Else
        sGMN3Cod = ""
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        txtEst = txtEst & " - " & sGMN4Cod
        iNivel = iNivel + 1
    Else
        sGMN4Cod = ""
    End If
    'cargamos en combo de nivel en funcion del nivel de material seleccionado
    cmbNivel.clear
    For i = gParametrosGenerales.giNEM To iNivel Step -1
        cmbNivel.AddItem i
    Next i
    cmbNivel.ListIndex = 0
    
    If iNivel = 1 Then
        If Not oGMN1Seleccionado Is Nothing Then
            cmbNivel.Text = iNivel + 1
        Else
            cmbNivel.Text = "1"
        End If
    Else
        If iNivel = 4 Then
            cmbNivel.Text = "4"
        Else
            cmbNivel.Text = iNivel + 1
        End If
    End If
End Sub
Public Sub MostrarProySeleccionado()
Dim iNivel As Integer
Dim i As Integer

    
    sProy1 = frmSELPresAnuUON.g_sPRES1
    sProy2 = frmSELPresAnuUON.g_sPRES2
    sProy3 = frmSELPresAnuUON.g_sPRES3
    sProy4 = frmSELPresAnuUON.g_sPRES4
    sUON1 = frmSELPresAnuUON.g_sUON1
    sUON2 = frmSELPresAnuUON.g_sUON2
    sUON3 = frmSELPresAnuUON.g_sUON3
    
    m_sLblUO = ""
    If sUON1 <> "" Then
        m_sLblUO = "(" & sUON1
        If sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & sUON2
            If sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If
    
    iNivel = 1
    If sProy4 <> "" Then
        txtEst = m_sLblUO & sProy1 & " - " & sProy2 & " - " & sProy3 & " - " & sProy4 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 4
    ElseIf sProy3 <> "" Then
        txtEst = m_sLblUO & sProy1 & " - " & sProy2 & " - " & sProy3 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 3
    ElseIf sProy2 <> "" Then
        txtEst = m_sLblUO & sProy1 & " - " & sProy2 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 2
    ElseIf sProy1 <> "" Then
        txtEst = m_sLblUO & sProy1 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 1
    Else
        txtEst = m_sLblUO
    End If
    
    cmbNivel.clear
    
    For i = gParametrosGenerales.giNEPP To iNivel Step -1
        If i = 0 Then Exit For
        cmbNivel.AddItem i
    Next i
    
    cmbNivel.ListIndex = 0
    If iNivel = 1 Then
        If sProy1 <> "" Then
            cmbNivel.Text = iNivel + 1
        Else
            cmbNivel.Text = "1"
        End If
    Else
        If iNivel = 4 Then
            cmbNivel.Text = "4"
        Else
            cmbNivel.Text = iNivel + 1
        End If
    End If
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad(sOrigen As String)
    Dim bPonerUO As Boolean
    
    Select Case sOrigen
        
        Case "A4B2C2", "frmInfAhorroApliMat"
            'Material
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplMatRestMat)) Is Nothing) And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
                bRMat = True
            End If
            
        Case "A4B2C5", "frmInfAhorroApliUO"
            'Unidades organizativas
            If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) Then
            
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplUORestPer)) Is Nothing) Then
                    bRUO = True
                End If
                          
            End If
        Case "A4B2C3", "frmPROY" ' Proyectos
            If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) Then
            
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplProyRestUO)) Is Nothing) Then
                    bRUO = True
                    bPonerUO = True
                End If
                          
            End If
            
        Case "A4B2C4", "frmPARCON" ' Partidas contables
            If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) Then
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplPartRestUO)) Is Nothing) Then
                    bRUO = True
                    bPonerUO = True
                End If
            End If
        Case "A4B2C6", "frmInfAhorroApliConcep3" 'concepto 3
            If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) Then
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplCon3RestUO)) Is Nothing) Then
                    bRUO = True
                    bPonerUO = True
                End If
            End If
    
        Case "A4B2C7", "frmInfAhorroApliConcep4" 'concepto 4
            If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) Then
                If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplCon4RestUO)) Is Nothing) Then
                    bRUO = True
                    bPonerUO = True
                End If
            End If
    End Select
    
    If bRUO Then
        m_iNivelUO = 0
        If CStr(basOptimizacion.gUON3Usuario) <> "" Then
            m_iNivelUO = 3
        ElseIf CStr(basOptimizacion.gUON2Usuario) <> "" Then
            m_iNivelUO = 2
        ElseIf CStr(basOptimizacion.gUON1Usuario) <> "" Then
            m_iNivelUO = 1
        End If
        If bPonerUO Then
            sUON1 = NullToStr(basOptimizacion.gUON1Usuario)
            sUON2 = NullToStr(basOptimizacion.gUON2Usuario)
            sUON3 = NullToStr(basOptimizacion.gUON3Usuario)
            m_sLblUO = ""
            If sUON1 <> "" Then
                m_sLblUO = "(" & sUON1
                If sUON2 <> "" Then
                    m_sLblUO = m_sLblUO & " - " & sUON2
                    If sUON3 <> "" Then
                        m_sLblUO = m_sLblUO & " - " & sUON3 & ") "
                    Else
                        m_sLblUO = m_sLblUO & ") "
                    End If
                Else
                    m_sLblUO = m_sLblUO & ") "
                End If
            End If
            Me.txtEst.Text = m_sLblUO
        
        End If
    End If

End Sub

Private Function ComprobarArchivoRpt(ByVal sArchivo As String) As Boolean
Dim oFos As FileSystemObject
        
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(sArchivo) Then
        ComprobarArchivoRpt = False
    Else
        ComprobarArchivoRpt = True
    End If
    Set oFos = Nothing

End Function
Private Sub ObtenerInformeAhorrosApl()
Dim iAnyoIni As Integer
Dim iAnyoFin As Integer
Dim iMesIni As Integer
Dim iMesFin As Integer
Dim bDetalleAnyo As Boolean
Dim sGraf As String
Dim FormulaMon As String

If crs_Connected = False Then
    Exit Sub
End If

iAnyoIni = val(sdbcAnyoDesde)
iMesIni = sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1
iAnyoFin = val(sdbcAnyoHasta)
iMesFin = sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1

' no estan implementados los graficos para detalle anual y mensual excepto para
' el listado GENERAL
If sOrigen = "frmInfAhorroApliGen" Or sOrigen = "A4B2C1" Then
    If CmbDetalle.Text = sIdiAnual Then
        bDetalleAnyo = True
    End If
    If opVerDat.Value Then
        sGraf = "0"
    Else
        If opVerGraf.Alignment Then
            sGraf = "1"
        Else
            sGraf = "2"
        End If
    End If
Else
'esto para los dem�s
    If chkHist.Value = vbChecked Then
        sGraf = "0"
        If CmbDetalle.Text = sIdiAnual Then
            bDetalleAnyo = True
        End If
    Else
        If opVerDat.Value Then
            sGraf = "0"
        Else
            If opVerGraf.Value Then
                sGraf = "1"
            Else
                sGraf = "2"
            End If
        End If
    End If
End If

sSeleccion = sIdiSeleccion & ": "
If optDir.Value Then
    sSeleccion = sSeleccion & sIdiAdjDir
Else
    If optReu.Value Then
        sSeleccion = sSeleccion & sIdiReu
    Else
        sSeleccion = sSeleccion & sIdiTodos
    End If
End If

FormulaMon = ""
If sMonedaOrig = sMoneda Then
    FormulaMon = sIdiMon & ": " & sMoneda
Else
    FormulaMon = sIdiMon & ": " & sMoneda & ", " & sIdiEquivalencia & ": " & CStr(dequivalencia) & " (" & sIdiMonCent & ": " & sMonedaOrig & ")"
End If

sSeleccion = sSeleccion & ", " & sIdiPeriodoDesde & " " & CStr(iMesIni) & "/" & CStr(iAnyoIni) & ", " & sIdiPeriodoHasta & " " & CStr(iMesFin) & "/" & CStr(iAnyoFin)

If gParametrosInstalacion.gsRPTPATH = "" Then
    If gParametrosGenerales.gsRPTPATH = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Exit Sub
    Else
        gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
        g_GuardarParametrosIns = True
    End If
End If

Select Case sOrigen
Case "frmInfAhorroApliGen", "A4B2C1"
    ObtenerInformeAhorrosGen iAnyoIni, iMesIni, iAnyoFin, iMesFin, bDetalleAnyo, sGraf, FormulaMon
Case "A4B2C2", "frmInfAhorroApliMat"  ' Material
    ObtenerInformeAhorrosMat iAnyoIni, iMesIni, iAnyoFin, iMesFin, bDetalleAnyo, sGraf, FormulaMon
Case "A4B2C3", "frmPROY"  ' Proyectos
    ObtenerInformeAhorrosPres1 iAnyoIni, iMesIni, iAnyoFin, iMesFin, bDetalleAnyo, sGraf, FormulaMon
Case "A4B2C4", "frmPARCON"  ' Partidas contables
    ObtenerInformeAhorrosPres2 iAnyoIni, iMesIni, iAnyoFin, iMesFin, bDetalleAnyo, sGraf, FormulaMon
Case "A4B2C5", "frmInfAhorroApliUO"  ' unidades organizativas
    ObtenerInformeAhorrosUO iAnyoIni, iMesIni, iAnyoFin, iMesFin, bDetalleAnyo, sGraf, FormulaMon
Case "A4B2C6", "frmInfAhorroApliConcep3"  'Concepto 3
    ObtenerInformeAhorrosPres3 iAnyoIni, iMesIni, iAnyoFin, iMesFin, bDetalleAnyo, sGraf, FormulaMon
Case "A4B2C7", "frmInfAhorroApliConcep4"  'Concepto 3
    ObtenerInformeAhorrosPres4 iAnyoIni, iMesIni, iAnyoFin, iMesFin, bDetalleAnyo, sGraf, FormulaMon
End Select
End Sub
Private Sub ObtenerInformeAhorrosGen(ByVal iAnyoIni As Integer, ByVal iMesIni As Integer, ByVal iAnyoFin As Integer, ByVal iMesFin As Integer, ByVal bDetalleAnyo As Boolean, ByVal sGraf As String, ByVal FormulaMon As String)
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sTitulo As String
    Dim RepPath As String
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
'    Dim SelectionTextRpt(1 To 2, 1 To 20) As String
    Dim SelectionTextRpt() As String
    Dim i As Integer
    
    Set oCRInformes = GenerarCRInformes
    
    Screen.MousePointer = vbHourglass
    sTitulo = sIdiTitGeneral
                
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplGen.rpt"
    If Not ComprobarArchivoRpt(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Exit Sub
    End If
            
    '*********** FORMULA FIELDS REPORT
    ReDim SelectionTextRpt(1 To 2, 1 To 17) As String

    SelectionTextRpt(2, 1) = "SEL"
    SelectionTextRpt(1, 1) = sSeleccion
    SelectionTextRpt(2, 2) = "MONEDA"
    SelectionTextRpt(1, 2) = FormulaMon
    SelectionTextRpt(2, 3) = "MOSTRAR_ANYO"
    SelectionTextRpt(2, 4) = "MOSTRAR_MES"
    If chkHist = 0 Then
        SelectionTextRpt(1, 3) = "N"
        SelectionTextRpt(1, 4) = "N"
    Else
        If bDetalleAnyo Then
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "N"
        Else
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "S"
        End If
    End If
    SelectionTextRpt(2, 5) = "VGRAF"
    SelectionTextRpt(1, 5) = sGraf
    SelectionTextRpt(2, 6) = "txtTITULO"
    SelectionTextRpt(1, 6) = sTitulo
    SelectionTextRpt(2, 7) = "txtPRESUPUESTO"
    SelectionTextRpt(1, 7) = srIdiPresupGen
    SelectionTextRpt(2, 8) = "txtPORCENTAHORRO"
    SelectionTextRpt(1, 8) = srIdiPorcentajeAhorro
    SelectionTextRpt(2, 9) = "txtFECHA"
    SelectionTextRpt(1, 9) = srIdiFechaGen
    SelectionTextRpt(2, 10) = "txtAHORRO"
    SelectionTextRpt(1, 10) = srIdiAhorro
    SelectionTextRpt(2, 11) = "txtADJUDICADO"
    SelectionTextRpt(1, 11) = srIdiAdjudicado
    SelectionTextRpt(2, 12) = "txtPAG"
    SelectionTextRpt(1, 12) = srIdiPag
    SelectionTextRpt(2, 13) = "txtDE"
    SelectionTextRpt(1, 13) = srIdiDe
    SelectionTextRpt(2, 14) = "txtLADJUDICADO"
    SelectionTextRpt(1, 14) = srIdiAdjudicado
    SelectionTextRpt(2, 15) = "txtLPRESUPUESTADO"
    SelectionTextRpt(1, 15) = srIdiLPresupuestado
    SelectionTextRpt(2, 16) = "txtLPOSITIVO"
    SelectionTextRpt(1, 16) = srIdiLPositivo
    SelectionTextRpt(2, 17) = "txtLNEGATIVO"
    SelectionTextRpt(1, 17) = srIdiLNegativo
        
        
    'SE HABRE EL REPORT
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next

    oCRInformes.ListadoAhorroAplGen oReport, iAnyoIni, iMesIni, iAnyoFin, iMesFin, dequivalencia, optDir.Value, optReu.Value

    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub

Private Sub ObtenerInformeAhorrosMat(ByVal iAnyoIni As Integer, ByVal iMesIni As Integer, ByVal iAnyoFin As Integer, ByVal iMesFin As Integer, ByVal bDetalleAnyo As Boolean, ByVal sGraf As String, ByVal FormulaMon As String)
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sTitulo As String
    Dim RepPath As String
    Dim OcultarNiveles As String
    Dim sSelMaterial As String
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    '    Dim SelectionTextRpt(1 To 2, 1 To 20) As String
    Dim SelectionTextRpt() As String
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass

    Set oCRInformes = GenerarCRInformes

    sTitulo = sIdiTitMat
    Select Case cmbNivel.Text
    Case 1
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplMat1.rpt"
    Case 2
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplMat2.rpt"
    Case 3
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplMat3.rpt"
    Case 4
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplMat4.rpt"
    End Select
    If Not ComprobarArchivoRpt(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Exit Sub
    End If
                        
    OcultarNiveles = ""
    If sGMN4Cod <> "" Then
        OcultarNiveles = "4"
        sSelMaterial = sIdiMat & ": " & sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod & " - " & sGMN4Cod
    Else
        If sGMN3Cod <> "" Then
            OcultarNiveles = "3"
            sSelMaterial = sIdiMat & ": " & sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod
        Else
            If sGMN2Cod <> "" Then
                OcultarNiveles = "2"
                sSelMaterial = sIdiMat & ": " & sGMN1Cod & " - " & sGMN2Cod
            Else
                If sGMN1Cod <> "" Then
                    OcultarNiveles = "1"
                    sSelMaterial = sIdiMat & ": " & sGMN1Cod
                End If
            End If
        End If
    End If
            
    '*********** FORMULA FIELDS REPORT
    ReDim SelectionTextRpt(1 To 2, 1 To 20) As String
    
    SelectionTextRpt(2, 1) = "SEL"
    SelectionTextRpt(1, 1) = sSeleccion
    SelectionTextRpt(2, 2) = "MONEDA"
    SelectionTextRpt(1, 2) = FormulaMon
    
    SelectionTextRpt(2, 3) = "MOSTRAR_ANYO"
    SelectionTextRpt(2, 4) = "MOSTRAR_MES"

    If chkHist.Value = 0 Then
        SelectionTextRpt(1, 3) = "N"
        SelectionTextRpt(1, 4) = "N"
    Else
        If bDetalleAnyo Then
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "N"
        Else
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "S"
        End If
    End If
    SelectionTextRpt(2, 5) = "OCULTAR_NIVEL"
    SelectionTextRpt(1, 5) = OcultarNiveles
    SelectionTextRpt(2, 6) = "SELMATERIAL"
    SelectionTextRpt(1, 6) = sSelMaterial
    SelectionTextRpt(2, 7) = "VGRAF"
    SelectionTextRpt(1, 7) = sGraf
    SelectionTextRpt(2, 8) = "txtMATERIAL"
    SelectionTextRpt(1, 8) = srIdiMaterialMat
    SelectionTextRpt(2, 9) = "txtPRESUPTOTAL"
    SelectionTextRpt(1, 9) = srIdiPresupTotal
    SelectionTextRpt(2, 10) = "txtCONSUMIDO"
    SelectionTextRpt(1, 10) = srIdiConsumido
    SelectionTextRpt(2, 11) = "txtADJUDICADO"
    SelectionTextRpt(1, 11) = srIdiAdjudicado
    SelectionTextRpt(2, 12) = "txtAHORRO"
    SelectionTextRpt(1, 12) = srIdiAhorro
    SelectionTextRpt(2, 13) = "txtPORCENTAJEAHORRO"
    SelectionTextRpt(1, 13) = srIdiPorcentajeAhorroMat
    SelectionTextRpt(2, 14) = "txtPAG"
    SelectionTextRpt(1, 14) = srIdiPag
    SelectionTextRpt(2, 15) = "txtDE"
    SelectionTextRpt(1, 15) = srIdiDe
    SelectionTextRpt(2, 16) = "txtLADJUDICADO"
    SelectionTextRpt(1, 16) = srIdiAdjudicado
    SelectionTextRpt(2, 17) = "txtLPRESUPUESTADO"
    SelectionTextRpt(1, 17) = srIdiLPresupuestado
    SelectionTextRpt(2, 18) = "txtLPOSITIVO"
    SelectionTextRpt(1, 18) = srIdiLPositivo
    SelectionTextRpt(2, 19) = "txtLNEGATIVO"
    SelectionTextRpt(1, 19) = srIdiLNegativo
    SelectionTextRpt(2, 20) = "txtTITULO"
    SelectionTextRpt(1, 20) = sTitulo
            
    'SE HABRE EL REPORT
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next
    
    oCRInformes.ListadoAhorroAplMat oGestorInformes, oReport, iAnyoIni, iMesIni, iAnyoFin, iMesFin, dequivalencia, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, optDir, optReu, cmbNivel.Text
           
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub
Private Sub ObtenerInformeAhorrosPres1(ByVal iAnyoIni As Integer, ByVal iMesIni As Integer, ByVal iAnyoFin As Integer, ByVal iMesFin As Integer, ByVal bDetalleAnyo As Boolean, ByVal sGraf As String, ByVal FormulaMon As String)
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sTitulo As String
    Dim RepPath As String
    Dim OcultarNiveles As String
    Dim sSelProyecto As String
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    '    Dim SelectionTextRpt(1 To 2, 1 To 20) As String
    Dim SelectionTextRpt() As String
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
        
    Set oCRInformes = GenerarCRInformes
        
    sTitulo = sIdiTit & " - " & gParametrosGenerales.gsPlurPres1
    
    ' NIVEL de desglose de listado
    Select Case CInt(cmbNivel.Text)
    Case 1
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres1_1.rpt"
    Case 2
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres1_2.rpt"
    Case 3
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres1_3.rpt"
    Case 4
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres1_4.rpt"
    End Select
    If Not ComprobarArchivoRpt(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Exit Sub
    End If
            
    OcultarNiveles = ""
    If sProy4 <> "" Then
        sSelProyecto = gParametrosGenerales.gsSingPres1 & " : " & m_sLblUO & " " & sProy1 & " - " & sProy2 & " - " & sProy3 & " - " & sProy4
    Else
        If sProy3 <> "" Then
            sSelProyecto = gParametrosGenerales.gsSingPres1 & " : " & m_sLblUO & " " & sProy1 & " - " & sProy2 & " - " & sProy3
        Else
            If sProy2 <> "" Then
                sSelProyecto = gParametrosGenerales.gsSingPres1 & " : " & m_sLblUO & " " & sProy1 & " - " & sProy2
            Else
                If sProy1 <> "" Then
                    sSelProyecto = gParametrosGenerales.gsSingPres1 & " : " & m_sLblUO & " " & sProy1
                Else
                    sSelProyecto = gParametrosGenerales.gsSingPres2 & " : " & m_sLblUO
                End If
            End If
        End If
    End If
    '*********** FORMULA FIELDS REPORT
    ReDim SelectionTextRpt(1 To 2, 1 To 20) As String

    SelectionTextRpt(2, 1) = "SEL"
    SelectionTextRpt(1, 1) = sSeleccion
    SelectionTextRpt(2, 2) = "MONEDA"
    SelectionTextRpt(1, 2) = FormulaMon
    SelectionTextRpt(2, 3) = "MOSTRAR_ANYO"
    SelectionTextRpt(2, 4) = "MOSTRAR_MES"
    If chkHist = 0 Then
        SelectionTextRpt(1, 3) = "N"
        SelectionTextRpt(1, 4) = "N"
    Else
        If bDetalleAnyo Then
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "N"
        Else
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "S"
        End If
    End If
    
    SelectionTextRpt(2, 5) = "SELPROYECTO"
    SelectionTextRpt(1, 5) = sSelProyecto
    SelectionTextRpt(2, 6) = "TITULO"
    SelectionTextRpt(1, 6) = sTitulo
    SelectionTextRpt(2, 7) = "CONCEPTO"
    SelectionTextRpt(1, 7) = gParametrosGenerales.gsSingPres1
    SelectionTextRpt(2, 8) = "VGRAF"
    SelectionTextRpt(1, 8) = sGraf
    SelectionTextRpt(2, 9) = "txtPRESUPTOTAL"
    SelectionTextRpt(1, 9) = srIdiPresupTotal
    SelectionTextRpt(2, 10) = "txtPORCENTAJEOBJ"
    SelectionTextRpt(1, 10) = srIdiPorcentajeObj
    SelectionTextRpt(2, 11) = "txtCONSUMIDO"
    SelectionTextRpt(1, 11) = srIdiConsumido
    SelectionTextRpt(2, 12) = "txtADJUDICADO"
    SelectionTextRpt(1, 12) = srIdiAdjudicado
    SelectionTextRpt(2, 13) = "txtAHORRO"
    SelectionTextRpt(1, 13) = srIdiAhorro
    SelectionTextRpt(2, 14) = "txtPORCENTAJEAHORRO"
    SelectionTextRpt(1, 14) = srIdiPorcentajeAhorro
    SelectionTextRpt(2, 15) = "txtPAG"
    SelectionTextRpt(1, 15) = srIdiPag
    SelectionTextRpt(2, 16) = "txtDE"
    SelectionTextRpt(1, 16) = srIdiDe
    SelectionTextRpt(2, 17) = "txtLADJUDICADO"
    SelectionTextRpt(1, 17) = srIdiAdjudicado
    SelectionTextRpt(2, 18) = "txtLPRESUPUESTADO"
    SelectionTextRpt(1, 18) = srIdiLPresupuestado
    SelectionTextRpt(2, 19) = "txtLPOSITIVO"
    SelectionTextRpt(1, 19) = srIdiLPositivo
    SelectionTextRpt(2, 20) = "txtLNEGATIVO"
    SelectionTextRpt(1, 20) = srIdiLNegativo
    
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next
    oCRInformes.ListadoAhorroAplProy oReport, iAnyoIni, iMesIni, iAnyoFin, iMesFin, dequivalencia, sProy1, sProy2, sProy3, sProy4, optDir, optReu, cmbNivel.Text, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), sUON1, sUON2, sUON3
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub

Private Sub ObtenerInformeAhorrosPres2(ByVal iAnyoIni As Integer, ByVal iMesIni As Integer, ByVal iAnyoFin As Integer, ByVal iMesFin As Integer, ByVal bDetalleAnyo As Boolean, ByVal sGraf As String, ByVal FormulaMon As String)
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sTitulo As String
    Dim RepPath As String
    Dim OcultarNiveles As String
    Dim sSelParCon As String
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    '    Dim SelectionTextRpt(1 To 2, 1 To 20) As String
    Dim SelectionTextRpt() As String
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oCRInformes = GenerarCRInformes
            
    sTitulo = sIdiTit & " - " & gParametrosGenerales.gsPlurPres2
                
    ' NIVEL de desglose de listado
    Select Case CInt(cmbNivel.Text)
        Case 1
            RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres2_1.rpt"
        Case 2
            RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres2_2.rpt"
        Case 3
            RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres2_3.rpt"
        Case 4
            RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres2_4.rpt"
    End Select
    If Not ComprobarArchivoRpt(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Exit Sub
    End If
            
    OcultarNiveles = ""
    If sParCon4 <> "" Then
        sSelParCon = gParametrosGenerales.gsSingPres2 & " : " & m_sLblUO & " " & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " - " & sParCon4
    Else
        If sParCon3 <> "" Then
            sSelParCon = gParametrosGenerales.gsSingPres2 & " : " & m_sLblUO & " " & sParCon1 & " - " & sParCon2 & " - " & sParCon3
        Else
            If sParCon2 <> "" Then
                sSelParCon = gParametrosGenerales.gsSingPres2 & " : " & m_sLblUO & " " & sParCon1 & " - " & sParCon2
            Else
                If sParCon1 <> "" Then
                    sSelParCon = gParametrosGenerales.gsSingPres2 & " : " & m_sLblUO & " " & sParCon1
                Else
                    sSelParCon = gParametrosGenerales.gsSingPres2 & " : " & m_sLblUO
                End If
            End If
        End If
    End If
            
    '*********** FORMULA FIELDS REPORT
    
    ReDim SelectionTextRpt(1 To 2, 1 To 20) As String

    SelectionTextRpt(2, 1) = "SEL"
    SelectionTextRpt(1, 1) = sSeleccion
    SelectionTextRpt(2, 2) = "MONEDA"
    SelectionTextRpt(1, 2) = FormulaMon
    
    SelectionTextRpt(2, 3) = "MOSTRAR_ANYO"
    SelectionTextRpt(2, 4) = "MOSTRAR_MES"
    If chkHist = 0 Then
        SelectionTextRpt(1, 3) = "N"
        SelectionTextRpt(1, 4) = "N"
    Else
        If bDetalleAnyo Then
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "N"
        Else
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "S"
        End If
    End If
    
    SelectionTextRpt(2, 5) = "SELPARCON"
    SelectionTextRpt(1, 5) = sSelParCon
    SelectionTextRpt(2, 6) = "TITULO"
    SelectionTextRpt(1, 6) = sTitulo
    SelectionTextRpt(2, 7) = "CONCEPTO"
    SelectionTextRpt(1, 7) = gParametrosGenerales.gsSingPres2
    SelectionTextRpt(2, 8) = "VGRAF"
    SelectionTextRpt(1, 8) = sGraf
    SelectionTextRpt(2, 9) = "txtPRESUPTOTAL"
    SelectionTextRpt(1, 9) = srIdiPresupTotal
    SelectionTextRpt(2, 10) = "txtPORCENTAJEOBJ"
    SelectionTextRpt(1, 10) = srIdiPorcentajeObj
    SelectionTextRpt(2, 11) = "txtCONSUMIDO"
    SelectionTextRpt(1, 11) = srIdiConsumido
    SelectionTextRpt(2, 12) = "txtADJUDICADO"
    SelectionTextRpt(1, 12) = srIdiAdjudicado
    SelectionTextRpt(2, 13) = "txtAHORRO"
    SelectionTextRpt(1, 13) = srIdiAhorro
    SelectionTextRpt(2, 14) = "txtPORCENTAJEAHORRO"
    SelectionTextRpt(1, 14) = srIdiPorcentajeAhorro
    SelectionTextRpt(2, 15) = "txtPAG"
    SelectionTextRpt(1, 15) = srIdiPag
    SelectionTextRpt(2, 16) = "txtDE"
    SelectionTextRpt(1, 16) = srIdiDe
    SelectionTextRpt(2, 17) = "txtLADJUDICADO"
    SelectionTextRpt(1, 17) = srIdiAdjudicado
    SelectionTextRpt(2, 18) = "txtLPRESUPUESTADO"
    SelectionTextRpt(1, 18) = srIdiLPresupuestado
    SelectionTextRpt(2, 19) = "txtLPOSITIVO"
    SelectionTextRpt(1, 19) = srIdiLPositivo
    SelectionTextRpt(2, 20) = "txtLNEGATIVO"
    SelectionTextRpt(1, 20) = srIdiLNegativo
    'SE HABRE EL REPORT
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next
    oCRInformes.ListadoAhorroAplParCon oReport, iAnyoIni, iMesIni, iAnyoFin, iMesFin, dequivalencia, sParCon1, sParCon2, sParCon3, sParCon4, optDir, optReu, cmbNivel.Text, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), sUON1, sUON2, sUON3
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub

Private Sub ObtenerInformeAhorrosUO(ByVal iAnyoIni As Integer, ByVal iMesIni As Integer, ByVal iAnyoFin As Integer, ByVal iMesFin As Integer, ByVal bDetalleAnyo As Boolean, ByVal sGraf As String, ByVal FormulaMon As String)
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sTitulo As String
    Dim RepPath As String
    Dim OcultarNiveles As String
    Dim sSelUO As String
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    '    Dim SelectionTextRpt(1 To 2, 1 To 20) As String
    Dim SelectionTextRpt() As String
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass

    Set oCRInformes = GenerarCRInformes

    sTitulo = sIdiTitUO
                
    ' NIVEL de desglose de listado
    Select Case CInt(cmbNivel.Text)
    Case 1
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplUO1.rpt"
    Case 2
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplUO2.rpt"
    Case 3
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplUO3.rpt"
    End Select
    If Not ComprobarArchivoRpt(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Exit Sub
    End If
               
    OcultarNiveles = ""
    
    If sUON3 <> "" Then
        sSelUO = sIdiUO & ": " & sUON1 & " - " & sUON2 & " - " & sUON3
    Else
        If sUON2 <> "" Then
            sSelUO = sIdiUO & ": " & sUON1 & " - " & sUON2
        Else
            If sUON1 <> "" Then
                sSelUO = sIdiUO & ": " & sUON1
            End If
        End If
    End If
            
     '*********** FORMULA FIELDS REPORT
     ReDim SelectionTextRpt(1 To 2, 1 To 18) As String
     SelectionTextRpt(2, 1) = "SEL"
     SelectionTextRpt(1, 1) = sSeleccion
     SelectionTextRpt(2, 2) = "MONEDA"
     SelectionTextRpt(1, 2) = FormulaMon
     
     SelectionTextRpt(2, 3) = "MOSTRAR_ANYO"
     SelectionTextRpt(2, 4) = "MOSTRAR_MES"
     If chkHist = 0 Then
         SelectionTextRpt(1, 3) = "N"
         SelectionTextRpt(1, 4) = "N"
     Else
         If bDetalleAnyo Then
             SelectionTextRpt(1, 3) = "S"
             SelectionTextRpt(1, 4) = "N"
         Else
             SelectionTextRpt(1, 3) = "S"
             SelectionTextRpt(1, 4) = "S"
         End If
     End If
     
     SelectionTextRpt(2, 5) = "SELUO"
     SelectionTextRpt(1, 5) = sSelUO
     SelectionTextRpt(2, 6) = "VGRAF"
     SelectionTextRpt(1, 6) = sGraf
     SelectionTextRpt(2, 7) = "txtTITULO"
     SelectionTextRpt(1, 7) = sTitulo
     SelectionTextRpt(2, 8) = "txtUO"
     SelectionTextRpt(1, 8) = srIdiUOUO
     SelectionTextRpt(2, 9) = "txtPRESUPUESTO"
     SelectionTextRpt(1, 9) = srIdiPresupTotal
     SelectionTextRpt(2, 10) = "txtADJUDICADO"
     SelectionTextRpt(1, 10) = srIdiAdjudicado
     SelectionTextRpt(2, 11) = "txtAHORRO"
     SelectionTextRpt(1, 11) = srIdiAhorro
     SelectionTextRpt(2, 12) = "txtPORCENTAJEAHORRO"
     SelectionTextRpt(1, 12) = srIdiPorcentajeAhorro
     SelectionTextRpt(2, 13) = "txtPAG"
     SelectionTextRpt(1, 13) = srIdiPag
     SelectionTextRpt(2, 14) = "txtDE"
     SelectionTextRpt(1, 14) = srIdiDe
     SelectionTextRpt(2, 15) = "txtLADJUDICADO"
     SelectionTextRpt(1, 15) = srIdiAdjudicado
     SelectionTextRpt(2, 16) = "txtLPRESUPUESTADO"
     SelectionTextRpt(1, 16) = srIdiLPresupuestado
     SelectionTextRpt(2, 17) = "txtLPOSITIVO"
     SelectionTextRpt(1, 17) = srIdiLPositivo
     SelectionTextRpt(2, 18) = "txtLNEGATIVO"
     SelectionTextRpt(1, 18) = srIdiLNegativo
     
     'SE ABRE EL REPORT
     
     Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
     
    
     For i = 1 To UBound(SelectionTextRpt, 2)
         oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
     Next
     
     oCRInformes.ListadoAhorroAplUO oReport, iAnyoIni, iMesIni, iAnyoFin, iMesFin, dequivalencia, sUON1, sUON2, sUON3, optDir, optReu, cmbNivel.Text
            
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub
        
Private Sub ObtenerInformeAhorrosPres3(ByVal iAnyoIni As Integer, ByVal iMesIni As Integer, ByVal iAnyoFin As Integer, ByVal iMesFin As Integer, ByVal bDetalleAnyo As Boolean, ByVal sGraf As String, ByVal FormulaMon As String)
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sTitulo As String
    Dim RepPath As String
    Dim OcultarNiveles As String
    Dim sSelProyecto As String
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    '    Dim SelectionTextRpt(1 To 2, 1 To 20) As String
    Dim SelectionTextRpt() As String
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oCRInformes = GenerarCRInformes
        
    sTitulo = sIdiTit & " - " & gParametrosGenerales.gsPlurPres3
    
    ' NIVEL de desglose de listado
    Select Case CInt(cmbNivel.Text)
    Case 1
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres3_1.rpt"
    Case 2
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres3_2.rpt"
    Case 3
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres3_3.rpt"
    Case 4
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres3_4.rpt"
    End Select
    If Not ComprobarArchivoRpt(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Exit Sub
    End If
    
    OcultarNiveles = ""
    If sConcep3_4 <> "" Then
        sSelProyecto = gParametrosGenerales.gsSingPres3 & " : " & m_sLblUO & " " & sConcep3_1 & " - " & sConcep3_2 & " - " & sConcep3_3 & " - " & sConcep3_4
    Else
        If sConcep3_3 <> "" Then
            sSelProyecto = gParametrosGenerales.gsSingPres3 & " : " & m_sLblUO & " " & sConcep3_1 & " - " & sConcep3_2 & " - " & sConcep3_3
        Else
            If sConcep3_2 <> "" Then
                sSelProyecto = gParametrosGenerales.gsSingPres3 & " : " & m_sLblUO & " " & sConcep3_1 & " - " & sConcep3_2
            Else
                If sConcep3_1 <> "" Then
                    sSelProyecto = gParametrosGenerales.gsSingPres3 & " : " & m_sLblUO & " " & sConcep3_1
                Else
                        sSelProyecto = gParametrosGenerales.gsSingPres3 & " : " & m_sLblUO
                End If
            End If
        End If
    End If
            
    '*********** FORMULA FIELDS REPORT
    ReDim SelectionTextRpt(1 To 2, 1 To 20) As String

    SelectionTextRpt(2, 1) = "SEL"
    SelectionTextRpt(1, 1) = sSeleccion
    SelectionTextRpt(2, 2) = "MONEDA"
    SelectionTextRpt(1, 2) = FormulaMon
    SelectionTextRpt(2, 3) = "MOSTRAR_ANYO"
    SelectionTextRpt(2, 4) = "MOSTRAR_MES"
    If chkHist = 0 Then
        SelectionTextRpt(1, 3) = "N"
        SelectionTextRpt(1, 4) = "N"
    Else
        If bDetalleAnyo Then
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "N"
        Else
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "S"
        End If
    End If
    
    SelectionTextRpt(2, 5) = "SELPROYECTO"
    SelectionTextRpt(1, 5) = sSelProyecto
    SelectionTextRpt(2, 6) = "TITULO"
    SelectionTextRpt(1, 6) = sTitulo
    SelectionTextRpt(2, 7) = "CONCEPTO"
    SelectionTextRpt(1, 7) = gParametrosGenerales.gsSingPres3
    SelectionTextRpt(2, 8) = "VGRAF"
    SelectionTextRpt(1, 8) = sGraf
    SelectionTextRpt(2, 9) = "txtPRESUPTOTAL"
    SelectionTextRpt(1, 9) = srIdiPresupTotal
    SelectionTextRpt(2, 10) = "txtPORCENTAJEOBJ"
    SelectionTextRpt(1, 10) = srIdiPorcentajeObj
    SelectionTextRpt(2, 11) = "txtCONSUMIDO"
    SelectionTextRpt(1, 11) = srIdiConsumido
    SelectionTextRpt(2, 12) = "txtADJUDICADO"
    SelectionTextRpt(1, 12) = srIdiAdjudicado
    SelectionTextRpt(2, 13) = "txtAHORRO"
    SelectionTextRpt(1, 13) = srIdiAhorro
    SelectionTextRpt(2, 14) = "txtPORCENTAJEAHORRO"
    SelectionTextRpt(1, 14) = srIdiPorcentajeAhorro
    SelectionTextRpt(2, 15) = "txtPAG"
    SelectionTextRpt(1, 15) = srIdiPag
    SelectionTextRpt(2, 16) = "txtDE"
    SelectionTextRpt(1, 16) = srIdiDe
    SelectionTextRpt(2, 17) = "txtLADJUDICADO"
    SelectionTextRpt(1, 17) = srIdiAdjudicado
    SelectionTextRpt(2, 18) = "txtLPRESUPUESTADO"
    SelectionTextRpt(1, 18) = srIdiLPresupuestado
    SelectionTextRpt(2, 19) = "txtLPOSITIVO"
    SelectionTextRpt(1, 19) = srIdiLPositivo
    SelectionTextRpt(2, 20) = "txtLNEGATIVO"
    SelectionTextRpt(1, 20) = srIdiLNegativo
    
    'Se abre el report
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next
    
    oCRInformes.ListadoAhorroAplConcep3 oReport, iAnyoIni, iMesIni, iAnyoFin, iMesFin, dequivalencia, sConcep3_1, sConcep3_2, sConcep3_3, sConcep3_4, optDir, optReu, cmbNivel.Text, sUON1, sUON2, sUON3
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub
        
Private Sub ObtenerInformeAhorrosPres4(ByVal iAnyoIni As Integer, ByVal iMesIni As Integer, ByVal iAnyoFin As Integer, ByVal iMesFin As Integer, ByVal bDetalleAnyo As Boolean, ByVal sGraf As String, ByVal FormulaMon As String)
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sTitulo As String
    Dim RepPath As String
    Dim OcultarNiveles As String
    Dim sSelProyecto As String
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    '    Dim SelectionTextRpt(1 To 2, 1 To 20) As String
    Dim SelectionTextRpt() As String
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oCRInformes = GenerarCRInformes
        
    sTitulo = sIdiTit & " - " & gParametrosGenerales.gsPlurPres4
    
    ' NIVEL de desglose de listado
    Select Case CInt(cmbNivel.Text)
    Case 1
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres4_1.rpt"
    Case 2
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres4_2.rpt"
    Case 3
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres4_3.rpt"
    Case 4
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroAplPres4_4.rpt"
    End Select
            
    If Not ComprobarArchivoRpt(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Exit Sub
    End If
    OcultarNiveles = ""
    If sConcep4_4 <> "" Then
        sSelProyecto = gParametrosGenerales.gsSingPres4 & " : " & m_sLblUO & " " & sConcep4_1 & " - " & sConcep4_2 & " - " & sConcep4_3 & " - " & sConcep4_4
    Else
        If sConcep4_3 <> "" Then
            sSelProyecto = gParametrosGenerales.gsSingPres4 & " : " & m_sLblUO & " " & sConcep4_1 & " - " & sConcep4_2 & " - " & sConcep4_3
        Else
            If sConcep4_2 <> "" Then
                sSelProyecto = gParametrosGenerales.gsSingPres4 & " : " & m_sLblUO & " " & sConcep4_1 & " - " & sConcep4_2
            Else
                If sConcep4_1 <> "" Then
                    sSelProyecto = gParametrosGenerales.gsSingPres4 & " : " & m_sLblUO & " " & sConcep4_1
                Else
                    sSelProyecto = gParametrosGenerales.gsSingPres4 & " : " & m_sLblUO
                End If
            End If
        End If
    End If
    
    '*********** FORMULA FIELDS REPORT
    ReDim SelectionTextRpt(1 To 2, 1 To 20) As String

    SelectionTextRpt(2, 1) = "SEL"
    SelectionTextRpt(1, 1) = sSeleccion
    SelectionTextRpt(2, 2) = "MONEDA"
    SelectionTextRpt(1, 2) = FormulaMon
    SelectionTextRpt(2, 3) = "MOSTRAR_ANYO"
    SelectionTextRpt(2, 4) = "MOSTRAR_MES"
    If chkHist = 0 Then
        SelectionTextRpt(1, 3) = "N"
        SelectionTextRpt(1, 4) = "N"
    Else
        If bDetalleAnyo Then
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "N"
        Else
            SelectionTextRpt(1, 3) = "S"
            SelectionTextRpt(1, 4) = "S"
        End If
    End If
    
    SelectionTextRpt(2, 5) = "SELPROYECTO"
    SelectionTextRpt(1, 5) = sSelProyecto
    SelectionTextRpt(2, 6) = "TITULO"
    SelectionTextRpt(1, 6) = sTitulo
    SelectionTextRpt(2, 7) = "CONCEPTO"
    SelectionTextRpt(1, 7) = gParametrosGenerales.gsSingPres4
    SelectionTextRpt(2, 8) = "VGRAF"
    SelectionTextRpt(1, 8) = sGraf
    SelectionTextRpt(2, 9) = "txtPRESUPTOTAL"
    SelectionTextRpt(1, 9) = srIdiPresupTotal
    SelectionTextRpt(2, 10) = "txtPORCENTAJEOBJ"
    SelectionTextRpt(1, 10) = srIdiPorcentajeObj
    SelectionTextRpt(2, 11) = "txtCONSUMIDO"
    SelectionTextRpt(1, 11) = srIdiConsumido
    SelectionTextRpt(2, 12) = "txtADJUDICADO"
    SelectionTextRpt(1, 12) = srIdiAdjudicado
    SelectionTextRpt(2, 13) = "txtAHORRO"
    SelectionTextRpt(1, 13) = srIdiAhorro
    SelectionTextRpt(2, 14) = "txtPORCENTAJEAHORRO"
    SelectionTextRpt(1, 14) = srIdiPorcentajeAhorro
    SelectionTextRpt(2, 15) = "txtPAG"
    SelectionTextRpt(1, 15) = srIdiPag
    SelectionTextRpt(2, 16) = "txtDE"
    SelectionTextRpt(1, 16) = srIdiDe
    SelectionTextRpt(2, 17) = "txtLADJUDICADO"
    SelectionTextRpt(1, 17) = srIdiAdjudicado
    SelectionTextRpt(2, 18) = "txtLPRESUPUESTADO"
    SelectionTextRpt(1, 18) = srIdiLPresupuestado
    SelectionTextRpt(2, 19) = "txtLPOSITIVO"
    SelectionTextRpt(1, 19) = srIdiLPositivo
    SelectionTextRpt(2, 20) = "txtLNEGATIVO"
    SelectionTextRpt(1, 20) = srIdiLNegativo
    
    'Se abre el report
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next
    oCRInformes.ListadoAhorroAplConcep4 oReport, iAnyoIni, iMesIni, iAnyoFin, iMesFin, dequivalencia, sConcep4_1, sConcep4_2, sConcep4_3, sConcep4_4, optDir, optReu, cmbNivel.Text, sUON1, sUON2, sUON3

    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub

Private Sub CargarAnyos()
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyoDesde.AddItem iInd
        sdbcAnyoHasta.AddItem iInd
        If bOculto Then
            sdbcADesdePres.AddItem iInd
            sdbcAHastaPres.AddItem iInd
        End If
    Next
    
    sdbcAnyoDesde.Text = iAnyoActual
    sdbcAnyoDesde.ListAutoPosition = True
    sdbcAnyoDesde.Scroll 1, 7
    sdbcAnyoHasta.Text = iAnyoActual
    sdbcAnyoHasta.ListAutoPosition = True
    sdbcAnyoHasta.Scroll 1, 7
    sdbcMesDesde.Text = "1"
    sdbcMesDesde.ListAutoPosition = True
    sdbcMesHasta.Text = Month(Date) - 1
    sdbcMesHasta.ListAutoPosition = True
    sdbcADesdePres.Scroll 1, 7
    sdbcAHastaPres.Scroll 1, 7
    
    sdbcMesDesde.AddItem sIdiMeses(1)
    sdbcMesDesde.AddItem sIdiMeses(2)
    sdbcMesDesde.AddItem sIdiMeses(3)
    sdbcMesDesde.AddItem sIdiMeses(4)
    sdbcMesDesde.AddItem sIdiMeses(5)
    sdbcMesDesde.AddItem sIdiMeses(6)
    sdbcMesDesde.AddItem sIdiMeses(7)
    sdbcMesDesde.AddItem sIdiMeses(8)
    sdbcMesDesde.AddItem sIdiMeses(9)
    sdbcMesDesde.AddItem sIdiMeses(10)
    sdbcMesDesde.AddItem sIdiMeses(11)
    sdbcMesDesde.AddItem sIdiMeses(12)
    
    sdbcMesHasta.AddItem sIdiMeses(1)
    sdbcMesHasta.AddItem sIdiMeses(2)
    sdbcMesHasta.AddItem sIdiMeses(3)
    sdbcMesHasta.AddItem sIdiMeses(4)
    sdbcMesHasta.AddItem sIdiMeses(5)
    sdbcMesHasta.AddItem sIdiMeses(6)
    sdbcMesHasta.AddItem sIdiMeses(7)
    sdbcMesHasta.AddItem sIdiMeses(8)
    sdbcMesHasta.AddItem sIdiMeses(9)
    sdbcMesHasta.AddItem sIdiMeses(10)
    sdbcMesHasta.AddItem sIdiMeses(11)
    sdbcMesHasta.AddItem sIdiMeses(12)
    
    sdbcMesDesde.MoveFirst
    sdbcMesDesde.Text = sdbcMesDesde.Columns(0).Value
    
    sdbcMesHasta.MoveFirst
    
    For iInd = 1 To Month(Date) - 2
        sdbcMesHasta.MoveNext
    Next
    
    sdbcMesHasta.Text = sdbcMesHasta.Columns(0).Value
    
    
    
End Sub

Private Sub chkHist_Click()

If chkHist.Value = vbChecked Then
    CmbDetalle.Enabled = True
    CmbDetalle.ListIndex = 0
    If sOrigen <> "A4B2C1" And sOrigen <> "frmInfAhorroApliGen" Then
        opVerDat.Value = True
        opVerGraf.Enabled = False
        opDatGraf.Enabled = False
    End If
Else
    CmbDetalle.ListIndex = -1
    CmbDetalle.Enabled = False
    If sOrigen <> "A4B2C1" And sOrigen <> "frmInfAhorroApliGen" Then
        opVerDat.Value = True
        opVerGraf.Enabled = True
        opDatGraf.Enabled = True
    End If
End If

End Sub

Private Sub cmdBorrar_Click()
    txtEst.Text = ""
    m_sLblUO = ""
    sConcep3_1 = ""
    sConcep3_2 = ""
    sConcep3_3 = ""
    sConcep3_4 = ""
    sConcep4_1 = ""
    sConcep4_2 = ""
    sConcep4_3 = ""
    sConcep4_4 = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    sProy1 = ""
    sProy2 = ""
    sProy3 = ""
    sProy4 = ""
    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
End Sub

Private Sub cmdObtener_Click()
Dim iNivelAsig As Integer    ' control de material asociado

If bRMat Then
    
        Select Case gParametrosGenerales.giNEM
    
            Case 1
                    If sGMN1Cod = "" Then
                        oMensajes.NoValido sIdiMat
                        Exit Sub
                    End If
            Case 2
                    If sGMN2Cod = "" And sGMN1Cod <> "" Then
                        Set oICompAsignado = oGMN1Seleccionado
                     
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        If iNivelAsig < 1 Or iNivelAsig > 2 Then
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                    End If
            Case 3
                    If sGMN3Cod = "" And sGMN2Cod <> "" Then
                        Set oICompAsignado = oGMN2Seleccionado
                       
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        If iNivelAsig = 0 Or iNivelAsig > 2 Then
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                    Else
                        If sGMN2Cod = "" And sGMN1Cod <> "" Then
                            Set oICompAsignado = oGMN1Seleccionado
                          
                            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                            If iNivelAsig < 1 Or iNivelAsig > 2 Then
                                oMensajes.InfAhorroMatNoValido
                                Exit Sub
                            End If
                        End If
                    End If
            Case 4
                
                If sGMN4Cod = "" And sGMN3Cod <> "" Then
                  
                        Set oICompAsignado = oGMN3Seleccionado
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        'Si el material que tiene asignado el comprador es de nivel 4
                        ' no cargamos nada
                        If iNivelAsig = 0 Or iNivelAsig > 3 Then
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                Else
                    If sGMN3Cod = "" And sGMN2Cod <> "" Then
                        Set oICompAsignado = oGMN2Seleccionado
                  
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        If iNivelAsig = 0 Or iNivelAsig > 2 Then
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                    Else
                        If sGMN2Cod = "" And sGMN1Cod <> "" Then
                            Set oICompAsignado = oGMN1Seleccionado
                           
                            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                            If iNivelAsig < 1 Or iNivelAsig > 2 Then
                                oMensajes.InfAhorroMatNoValido
                                Exit Sub
                            End If
                        Else
                            If sGMN4Cod = "" Then
                                oMensajes.InfAhorroMatNoValido
                                Exit Sub
                            End If
                        End If
                    End If
                End If
        
        End Select
    
    End If
    
    If bRUO Then
        Select Case gParametrosGenerales.giNEO
    
            Case 1
                    If sUON1 = "" Then
                        oMensajes.NoValido sIdiMat
                        Exit Sub
                    End If
            Case 2
                    If m_iNivelUO > 0 And sUON1 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    ElseIf m_iNivelUO > 1 And sUON2 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    End If
            Case 3
                    If m_iNivelUO > 0 And sUON1 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    ElseIf m_iNivelUO > 1 And sUON2 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    ElseIf m_iNivelUO > 2 And sUON3 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    End If
        End Select
    
    End If

    If sdbcMon = "" Then
        oMensajes.NoValido sIdiMon
        If Me.Visible Then sdbcMon.SetFocus
        Exit Sub
    End If
    
    'Comprueba que la fecha de inicio no sea mayor que la de fin
    If val(sdbcAnyoDesde) > val(sdbcAnyoHasta) Then
        oMensajes.PeriodoNoValido
        If Me.Visible Then sdbcAnyoDesde.SetFocus
        Exit Sub
    Else
        If val(sdbcAnyoDesde) = val(sdbcAnyoHasta) Then
            If sdbcMesHasta.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1 > sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1 Then
                oMensajes.PeriodoNoValido
                If Me.Visible Then sdbcMesDesde.SetFocus
                Exit Sub
            End If
        End If
    End If
    
    If sOrigen = "A4B2C3" Or sOrigen = "frmPROY" Or sOrigen = "A4B2C4" Or sOrigen = "frmPARCON" Then
        If sdbcADesdePres.Text <> "" Then
            If Len(sdbcADesdePres.Text) <> 4 Then
                oMensajes.NoValido sIdiAnyo
                If Me.Visible Then sdbcADesdePres.SetFocus
                Exit Sub
            Else
                If Not IsNumeric(sdbcADesdePres.Text) Then
                    oMensajes.NoValido sIdiAnyo
                    If Me.Visible Then sdbcADesdePres.SetFocus
                    Exit Sub
                End If
            End If
            If sdbcAHastaPres.Text = "" Then
                bRespetarCombo = True
                sdbcAHastaPres.Text = sdbcADesdePres.Text
                bRespetarCombo = False
            End If
        End If
        If sdbcAHastaPres.Text <> "" Then
            If Len(sdbcAHastaPres.Text) <> 4 Then
                oMensajes.NoValido sIdiAnyo
                If Me.Visible Then sdbcAHastaPres.SetFocus
                Exit Sub
            Else
                If Not IsNumeric(sdbcAHastaPres.Text) Then
                    oMensajes.NoValido sIdiAnyo
                    If Me.Visible Then sdbcAHastaPres.SetFocus
                    Exit Sub
                End If
            End If
            If sdbcADesdePres.Text = "" Then
                bRespetarCombo = True
                sdbcADesdePres.Text = sdbcAHastaPres.Text
                bRespetarCombo = False
            End If
        End If
        If val(sdbcADesdePres.Text) > val(sdbcAHastaPres.Text) Then
            oMensajes.PeriodoNoValido
            If Me.Visible Then sdbcAHastaPres.SetFocus
            Exit Sub
        End If
    
    End If
    ObtenerInformeAhorrosApl
        
End Sub

Private Sub cmdSel_Click()
    Select Case sOrigen
        Case "A4B2C2", "frmInfAhorroApliMat" ' Material
            frmSELMAT.sOrigen = "frmLstINFAhorroApl" & sOrigen
            frmSELMAT.bRComprador = bRMat
            frmSELMAT.Show 1
            
        Case "A4B2C3", "frmPROY" ' Proyectos
            frmSELPresAnuUON.sOrigen = "frmLstINFAhorroApl" & sOrigen
            frmSELPresAnuUON.bRUO = bRUO
            frmSELPresAnuUON.g_iTipoPres = 1
            frmSELPresAnuUON.bMostrarBajas = True
            frmSELPresAnuUON.Show 1
            
        Case "A4B2C4", "frmPARCON" ' Partidas contables
            frmSELPresAnuUON.sOrigen = "frmLstINFAhorroApl" & sOrigen
            frmSELPresAnuUON.bRUO = bRUO
            frmSELPresAnuUON.g_iTipoPres = 2
            frmSELPresAnuUON.bMostrarBajas = True
            frmSELPresAnuUON.Show 1
            
        Case "A4B2C5", "frmInfAhorroApliUO" 'unidades organizativas
            frmSELUO.sOrigen = "frmLstINFAhorroApl" & sOrigen
            frmSELUO.bRUO = bRUO
            frmSELUO.bMostrarBajas = True
            frmSELUO.Show 1
            
        Case "A4B2C6", "frmInfAhorroApliConcep3"
            frmSELPresUO.sOrigen = "frmLstINFAhorroApl" & sOrigen
            frmSELPresUO.bRUO = bRUO
            frmSELPresUO.g_iTipoPres = 3
            frmSELPresUO.bMostrarBajas = True
            frmSELPresUO.Show 1
            
        Case "A4B2C7", "frmInfAhorroApliConcep4"
            frmSELPresUO.sOrigen = "frmLstINFAhorroApl" & sOrigen
            frmSELPresUO.bRUO = bRUO
            frmSELPresUO.g_iTipoPres = 4
            frmSELPresUO.bMostrarBajas = True
            frmSELPresUO.Show 1
    End Select

End Sub
Public Sub MostrarUOSeleccionada()
Dim iNivel As Integer
Dim i As Integer
    
    If frmSELUO.sUON3 = "" Then
        sUON3 = ""
        If frmSELUO.sUON2 = "" Then
            sUON2 = ""
            If frmSELUO.sUON1 = "" Then
                sUON1 = ""
            Else
                sUON1 = frmSELUO.sUON1
                txtEst = sUON1 & " - " & frmSELUO.sDen
                iNivel = 1
            End If
        Else
            sUON2 = frmSELUO.sUON2
            sUON1 = frmSELUO.sUON1
            txtEst = sUON1 & " - " & sUON2 & " - " & frmSELUO.sDen
            iNivel = 2
        End If
    Else
        sUON3 = frmSELUO.sUON3
        sUON2 = frmSELUO.sUON2
        sUON1 = frmSELUO.sUON1
        txtEst = sUON1 & " - " & sUON2 & " - " & sUON3 & " - " & frmSELUO.sDen
        iNivel = 3
    End If
    
    cmbNivel.clear
    For i = 3 To iNivel Step -1
        If i = 0 Then Exit For
        cmbNivel.AddItem i
    Next i
    
    cmbNivel.ListIndex = 0
    
    If iNivel = 1 Then
        cmbNivel.Text = "1"
    Else
        If iNivel = 3 Then
            cmbNivel.Text = "3"
        Else
            cmbNivel.Text = iNivel + 1
        End If
    End If
End Sub

Private Sub Form_Load()
Dim i As Integer

    bOculto = False
    ConfigurarSeguridad (sOrigen)

    Me.Height = 4500
    Me.Width = 8550
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    CargarRecursos
        
    PonerFieldSeparator Me
        
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , , , True
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        sMonedaOrig = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        dequivalencia = oMonedas.Item(1).Equiv
    End If
    
    
    Me.chkHist.Value = vbUnchecked
    Me.CmbDetalle.AddItem sIdiAnual
    Me.CmbDetalle.AddItem sIdiMensual
    
    Select Case sOrigen
        
       Case "A4B2C1", "frmInfAhorroApliGen"  ' General
            Me.caption = sIdiTitGeneral & "  " & sIdiOpc
            Me.chkHist.Value = vbChecked
            Me.CmbDetalle.ListIndex = 0
            Me.cmbNivel.Visible = False
            Me.lblNivel.Visible = False
            Me.fraAnyoPres.Visible = False
            Me.fraTipo.Visible = False
            Me.fraMon.Top = fraTipo.Top
            Me.opVerDat.Top = 525
            Me.opVerGraf.Top = 525
            Me.opDatGraf.Top = 525
           
        Case "A4B2C2", "frmInfAhorroApliMat" ' Material
            Me.caption = sIdiTitMat & " " & sIdiOpc
            Me.fraAnyoPres.Visible = False
            If sOrigen = "A4B2C2" Then
                For i = gParametrosGenerales.giNEM To 1 Step -1
                    Me.cmbNivel.AddItem i
                Next i
                Me.cmbNivel.ListIndex = 0
            End If
            
        Case "A4B2C3", "frmPROY" ' Proyectos
            Me.caption = sIdiTit & " - " & gParametrosGenerales.gsPlurPres1 & " " & sIdiOpc
            Me.fraTipo.caption = gParametrosGenerales.gsSingPres1
            Me.fraAnyoPres.Visible = True
            bOculto = True
            If sOrigen = "A4B2C3" Then
                For i = gParametrosGenerales.giNEPP To 1 Step -1
                    Me.cmbNivel.AddItem i
                Next i
                Me.cmbNivel.ListIndex = 0
            End If
        Case "A4B2C4", "frmPARCON" ' Partidas contables
            Me.caption = sIdiTit & " - " & gParametrosGenerales.gsPlurPres2 & " " & sIdiOpc
            Me.fraTipo.caption = gParametrosGenerales.gsSingPres2
            Me.fraAnyoPres.Visible = True
            bOculto = True
            If sOrigen = "A4B2C4" Then
                For i = gParametrosGenerales.giNEPC To 1 Step -1
                    Me.cmbNivel.AddItem i
                Next i
                Me.cmbNivel.ListIndex = 0
            End If
            
        Case "A4B2C5", "frmInfAhorroApliUO" ' unidades organizativas
            Me.caption = sIdiTitUO & " " & sIdiOpc
            Me.fraTipo.caption = sIdiUO
            Me.fraAnyoPres.Visible = False
            If sOrigen = "A4B2C5" Then
                For i = gParametrosGenerales.giNEO To 1 Step -1
                    Me.cmbNivel.AddItem i
                Next i
                cmbNivel.ListIndex = 0
            End If
            
        Case "A4B2C6", "frmInfAhorroApliConcep3" 'concepto 3
            Me.caption = sIdiTit & " - " & gParametrosGenerales.gsPlurPres3
            Me.fraTipo.caption = gParametrosGenerales.gsSingPres3
            fraAnyoPres.Visible = False
            If sOrigen = "A4B2C6" Then
                For i = gParametrosGenerales.giNEP3 To 1 Step -1
                    cmbNivel.AddItem i
                Next i
                cmbNivel.ListIndex = 0
            End If

        Case "A4B2C7", "frmInfAhorroApliConcep4" 'concepto 4
            Me.caption = sIdiTit & " - " & gParametrosGenerales.gsPlurPres4
            Me.fraTipo.caption = gParametrosGenerales.gsSingPres4
            fraAnyoPres.Visible = False
            If sOrigen = "A4B2C7" Then
                For i = gParametrosGenerales.giNEP4 To 1 Step -1
                    cmbNivel.AddItem i
                Next i
                cmbNivel.ListIndex = 0
            End If
            
        End Select
        
        If Not bOculto Then
            fraMon.Left = fraAnyos.Left
            fraMon.Width = fraAnyos.Width
        End If
        
        CargarAnyos
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set oMonedas = Nothing
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    Set oICompAsignado = Nothing
    sSeleccion = ""
    Me.txtEst = ""
    iNivel = 1
    bOculto = False
    
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""
    sProy1 = ""
    sProy2 = ""
    sProy3 = ""
    sProy4 = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    sConcep3_1 = ""
    sConcep3_2 = ""
    sConcep3_3 = ""
    sConcep3_4 = ""
    sConcep4_1 = ""
    sConcep4_2 = ""
    sConcep4_3 = ""
    sConcep4_4 = ""

End Sub



Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        sMoneda = ""
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    sMoneda = sdbcMon.Columns(1).Text
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
        
End Sub


Private Sub sdbcMon_DropDown()

    Dim oMon As CMoneda
    
    sdbcMon.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If bMonCargarComboDesde Then
        oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcMon.Text), , , True
    Else
        oMonedas.CargarTodasLasMonedas , , , , , False, True
    End If
    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    If bMonCargarComboDesde And Not oMonedas.EOF Then
        sdbcMon.AddItem "..."
    End If

    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcMon.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMon.Rows - 1
            bm = sdbcMon.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMon.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcMon.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
        sMoneda = ""
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
        
    End If
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTINFAHORROS_APL, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        sIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblAnyoDesde.caption = Ador(0).Value
        lblDesdePres.caption = Ador(0).Value
        sIdiPeriodoDesde = Ador(0).Value
        Ador.MoveNext
        lblAnyoHasta.caption = Ador(0).Value '5
        lblHastaPres = Ador(0).Value
        sIdiPeriodoHasta = Ador(0).Value
        Ador.MoveNext
        lblMon.caption = Ador(0).Value
        Ador.MoveNext
        optReu.caption = Ador(0).Value
        Ador.MoveNext
        optDir.caption = Ador(0).Value
        Ador.MoveNext
        optTodos.caption = Ador(0).Value '10
        Ador.MoveNext
        chkHist.caption = Ador(0).Value
        Ador.MoveNext
        lblNivel.caption = Ador(0).Value
        Ador.MoveNext
        opVerDat.caption = Ador(0).Value
        Ador.MoveNext
        opVerGraf.caption = Ador(0).Value
        Ador.MoveNext
        opDatGraf.caption = Ador(0).Value '15
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        
        sIdiAnual = Ador(0).Value
        Ador.MoveNext
        sIdiMensual = Ador(0).Value
        Ador.MoveNext
        sIdiTitGeneral = Ador(0).Value '19
        Ador.MoveNext
        sIdiTitMat = Ador(0).Value '20
        Ador.MoveNext
        sIdiTit = Ador(0).Value
        Ador.MoveNext
        sIdiTitUO = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(1) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(2) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(3) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(4) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(5) = Ador(0).Value '30
        Ador.MoveNext
        sIdiMeses(6) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(7) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(8) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(9) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(10) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(11) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(12) = Ador(0).Value
        Ador.MoveNext
        sIdiMat = Ador(0).Value
        fraTipo.caption = Ador(0).Value
        Ador.MoveNext
        sIdiMon = Ador(0).Value
        Ador.MoveNext
        sIdiAnyoInic = Ador(0).Value '40
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value
        Ador.MoveNext
        sIdiOpc = Ador(0).Value '45
        Ador.MoveNext
        sIdiUO = Ador(0).Value
        Ador.MoveNext
        sIdiAdjDir = Ador(0).Value '50
        Ador.MoveNext
        sIdiReu = Ador(0).Value
        Ador.MoveNext
        sIdiTodos = Ador(0).Value
        Ador.MoveNext
        sIdiEquivalencia = Ador(0).Value
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        srIdiPresupGen = Ador(0).Value
        Ador.MoveNext
        srIdiPorcentajeAhorro = Ador(0).Value
        Ador.MoveNext
        srIdiFechaGen = Ador(0).Value
        Ador.MoveNext
        srIdiAhorro = Ador(0).Value
        Ador.MoveNext
        srIdiAdjudicado = Ador(0).Value '205
        Ador.MoveNext
        srIdiMaterialMat = Ador(0).Value
        Ador.MoveNext
        srIdiPresupTotal = Ador(0).Value '208
        Ador.MoveNext
        srIdiConsumido = Ador(0).Value
        Ador.MoveNext
        srIdiPorcentajeAhorroMat = Ador(0).Value
        Ador.MoveNext
        srIdiPorcentajeObj = Ador(0).Value
        Ador.MoveNext
        srIdiUOUO = Ador(0).Value '220
        Ador.MoveNext
        srIdiPag = Ador(0).Value '225
        Ador.MoveNext
        srIdiDe = Ador(0).Value
        Ador.MoveNext
        srIdiLPresupuestado = Ador(0).Value
        Ador.MoveNext
        srIdiLPositivo = Ador(0).Value
        Ador.MoveNext
        srIdiLNegativo = Ador(0).Value
        Ador.MoveNext
        fraAnyoPres.caption = Ador(0).Value
        Ador.MoveNext
        sIdiAnyo = Ador(0).Value
        Ador.Close
    
    End If

   Set Ador = Nothing
End Sub

Public Sub MostrarPresSeleccionado3()
    Dim iNivel As Integer
    Dim i As Integer
    
    sConcep3_1 = frmSELPresUO.g_sPRES1
    sConcep3_2 = frmSELPresUO.g_sPRES2
    sConcep3_3 = frmSELPresUO.g_sPRES3
    sConcep3_4 = frmSELPresUO.g_sPRES4
    sUON1 = frmSELPresUO.g_sUON1
    sUON2 = frmSELPresUO.g_sUON2
    sUON3 = frmSELPresUO.g_sUON3
    
    m_sLblUO = ""
    If sUON1 <> "" Then
        m_sLblUO = "(" & sUON1
        If sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & sUON2
            If sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If
    
    iNivel = 1
    If sConcep3_4 <> "" Then
        txtEst = m_sLblUO & sConcep3_1 & " - " & sConcep3_2 & " - " & sConcep3_3 & " - " & sConcep3_4 & " " & frmSELPresUO.g_sDenPres
        iNivel = 4
    ElseIf sConcep3_3 <> "" Then
        txtEst = m_sLblUO & sConcep3_1 & " - " & sConcep3_2 & " - " & sConcep3_3 & " " & frmSELPresUO.g_sDenPres
        iNivel = 3
    ElseIf sConcep3_2 <> "" Then
        txtEst = m_sLblUO & sConcep3_1 & " - " & sConcep3_2 & " " & frmSELPresUO.g_sDenPres
        iNivel = 2
    ElseIf sConcep3_1 <> "" Then
        txtEst = m_sLblUO & sConcep3_1 & " " & frmSELPresUO.g_sDenPres
        iNivel = 1
    Else
        txtEst = m_sLblUO
    End If
    
    
    cmbNivel.clear
    For i = gParametrosGenerales.giNEPP To iNivel Step -1
        If i = 0 Then Exit For
        cmbNivel.AddItem i
    Next i
    
    cmbNivel.ListIndex = 0
    If iNivel = 1 Then
        If sConcep3_1 <> "" Then
            cmbNivel.Text = iNivel + 1
        Else
            cmbNivel.Text = "1"
        End If
    Else
        If iNivel = 4 Then
            cmbNivel.Text = "4"
        Else
            cmbNivel.Text = iNivel + 1
        End If
    End If
End Sub


Public Sub MostrarPresSeleccionado4()
    Dim iNivel As Integer
    Dim i As Integer
        
    sConcep4_1 = frmSELPresUO.g_sPRES1
    sConcep4_2 = frmSELPresUO.g_sPRES2
    sConcep4_3 = frmSELPresUO.g_sPRES3
    sConcep4_4 = frmSELPresUO.g_sPRES4
    sUON1 = frmSELPresUO.g_sUON1
    sUON2 = frmSELPresUO.g_sUON2
    sUON3 = frmSELPresUO.g_sUON3
    
    m_sLblUO = ""
    If sUON1 <> "" Then
        m_sLblUO = "(" & sUON1
        If sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & sUON2
            If sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If
    
    iNivel = 1
    If sConcep4_4 <> "" Then
        txtEst = m_sLblUO & sConcep4_1 & " - " & sConcep4_2 & " - " & sConcep4_3 & " - " & sConcep4_4 & " " & frmSELPresUO.g_sDenPres
        iNivel = 4
    ElseIf sConcep4_3 <> "" Then
        txtEst = m_sLblUO & sConcep4_1 & " - " & sConcep4_2 & " - " & sConcep4_3 & " " & frmSELPresUO.g_sDenPres
        iNivel = 3
    ElseIf sConcep4_2 <> "" Then
        txtEst = m_sLblUO & sConcep4_1 & " - " & sConcep4_2 & " " & frmSELPresUO.g_sDenPres
        iNivel = 2
    ElseIf sConcep4_1 <> "" Then
        txtEst = m_sLblUO & sConcep4_1 & " " & frmSELPresUO.g_sDenPres
        iNivel = 1
    Else
        txtEst = m_sLblUO
    End If
        
    cmbNivel.clear
    For i = gParametrosGenerales.giNEPP To iNivel Step -1
        If i = 0 Then Exit For
        cmbNivel.AddItem i
    Next i
    
    cmbNivel.ListIndex = 0
    If iNivel = 1 Then
        If sConcep4_1 <> "" Then
            cmbNivel.Text = iNivel + 1
        Else
            cmbNivel.Text = "1"
        End If
    Else
        If iNivel = 4 Then
            cmbNivel.Text = "4"
        Else
            cmbNivel.Text = iNivel + 1
        End If
    End If
End Sub

Private Sub sdbcADesdePres_CloseUp()
    sdbcAHastaPres.Text = sdbcADesdePres.Text
End Sub

Private Sub sdbcADesdePres_Validate(Cancel As Boolean)
    bRespetarCombo = True
    If sdbcADesdePres.Text = "" Then
        sdbcAHastaPres.Text = ""
    End If
    bRespetarCombo = False
End Sub

Private Sub sdbcAHastaPres_Validate(Cancel As Boolean)
    bRespetarCombo = True
    If sdbcAHastaPres.Text = "" Then
        sdbcAHastaPres.Text = sdbcADesdePres.Text
    End If
    bRespetarCombo = False
End Sub

Private Sub Timer1_Timer()
    If frmESPERA.ProgressBar2.Value < 90 Then
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
    End If
    If frmESPERA.ProgressBar1.Value < 10 Then
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    End If
End Sub
