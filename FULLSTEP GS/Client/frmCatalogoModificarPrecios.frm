VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCatalogoModificarPrecios 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Modificar precios"
   ClientHeight    =   5805
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6285
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCatalogoModificarPrecios.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5805
   ScaleWidth      =   6285
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture3 
      Height          =   1635
      Left            =   60
      ScaleHeight     =   1575
      ScaleWidth      =   6075
      TabIndex        =   19
      Top             =   3600
      Width           =   6135
      Begin VB.Frame Frame7 
         Height          =   1395
         Left            =   120
         TabIndex        =   20
         Top             =   60
         Width           =   5835
         Begin VB.OptionButton opAhorro 
            Caption         =   "Aumentar ahorro"
            Height          =   255
            Left            =   120
            TabIndex        =   27
            Top             =   0
            Width           =   3675
         End
         Begin VB.Frame Frame9 
            Height          =   915
            Left            =   180
            TabIndex        =   24
            Top             =   300
            Width           =   3555
            Begin VB.PictureBox picPorcenAho 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   615
               Left            =   120
               ScaleHeight     =   615
               ScaleWidth      =   3000
               TabIndex        =   32
               Top             =   240
               Width           =   3000
               Begin VB.TextBox txtPorcenAhorro 
                  Height          =   345
                  Left            =   2560
                  TabIndex        =   41
                  Top             =   100
                  Width           =   435
               End
               Begin MSComctlLib.Slider Slider3 
                  Height          =   315
                  Left            =   100
                  TabIndex        =   33
                  Top             =   120
                  Width           =   2415
                  _ExtentX        =   4260
                  _ExtentY        =   556
                  _Version        =   393216
                  Max             =   100
               End
            End
            Begin VB.OptionButton opPorcenAhorro 
               Caption         =   "Porcentaje"
               Height          =   315
               Left            =   60
               TabIndex        =   25
               Top             =   -60
               Width           =   1755
            End
            Begin VB.Label lblPocenAho 
               Caption         =   "%"
               Height          =   315
               Left            =   3180
               TabIndex        =   26
               Top             =   420
               Width           =   255
            End
         End
         Begin VB.Frame Frame8 
            Height          =   915
            Left            =   3900
            TabIndex        =   21
            Top             =   300
            Width           =   1755
            Begin VB.PictureBox picCantAho 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   500
               Left            =   200
               ScaleHeight     =   495
               ScaleWidth      =   975
               TabIndex        =   42
               Top             =   360
               Width           =   975
               Begin VB.TextBox txtCantAhorro 
                  Height          =   345
                  Left            =   0
                  TabIndex        =   43
                  Top             =   0
                  Width           =   855
               End
            End
            Begin VB.OptionButton opCantAhorro 
               Caption         =   "Cantidad"
               Height          =   255
               Left            =   120
               TabIndex        =   22
               Top             =   0
               Width           =   1095
            End
            Begin VB.Label lblMonCentraAho 
               Caption         =   "(EUR)"
               Height          =   315
               Left            =   1200
               TabIndex        =   23
               Top             =   420
               Width           =   435
            End
         End
      End
   End
   Begin VB.PictureBox Picture2 
      Height          =   1635
      Left            =   60
      ScaleHeight     =   1575
      ScaleWidth      =   6075
      TabIndex        =   10
      Top             =   1830
      Width           =   6135
      Begin VB.Frame Frame2 
         Height          =   1395
         Left            =   120
         TabIndex        =   11
         Top             =   60
         Width           =   5835
         Begin VB.Frame Frame6 
            Height          =   915
            Left            =   3900
            TabIndex        =   16
            Top             =   300
            Width           =   1755
            Begin VB.PictureBox picCantDec 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   500
               Left            =   200
               ScaleHeight     =   495
               ScaleWidth      =   975
               TabIndex        =   39
               Top             =   360
               Width           =   975
               Begin VB.TextBox txtCantDecPrecios 
                  Height          =   345
                  Left            =   0
                  TabIndex        =   40
                  Top             =   0
                  Width           =   855
               End
            End
            Begin VB.OptionButton opCantDecPrecios 
               Caption         =   "Cantidad"
               Height          =   255
               Left            =   120
               TabIndex        =   17
               Top             =   0
               Width           =   1095
            End
            Begin VB.Label lblMonCentralDec 
               Caption         =   "(EUR)"
               Height          =   315
               Left            =   1200
               TabIndex        =   18
               Top             =   420
               Width           =   435
            End
         End
         Begin VB.Frame Frame5 
            Height          =   915
            Left            =   180
            TabIndex        =   13
            Top             =   300
            Width           =   3555
            Begin VB.PictureBox picPorcenDec 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   615
               Left            =   120
               ScaleHeight     =   615
               ScaleWidth      =   3015
               TabIndex        =   30
               Top             =   240
               Width           =   3010
               Begin VB.TextBox txtPorcenDecPrecios 
                  Height          =   345
                  Left            =   2560
                  TabIndex        =   38
                  Top             =   100
                  Width           =   435
               End
               Begin MSComctlLib.Slider Slider2 
                  Height          =   315
                  Left            =   100
                  TabIndex        =   31
                  Top             =   120
                  Width           =   2415
                  _ExtentX        =   4260
                  _ExtentY        =   556
                  _Version        =   393216
                  Max             =   100
               End
            End
            Begin VB.OptionButton opPorcenDecPrecios 
               Caption         =   "Porcentaje"
               Height          =   315
               Left            =   60
               TabIndex        =   14
               Top             =   -60
               Width           =   1755
            End
            Begin VB.Label lblPorcenDec 
               Caption         =   "%"
               Height          =   315
               Left            =   3180
               TabIndex        =   15
               Top             =   420
               Width           =   255
            End
         End
         Begin VB.OptionButton opDecPrecios 
            Caption         =   "Decrementar precios"
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   0
            Width           =   1935
         End
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   1635
      Left            =   60
      ScaleHeight     =   1575
      ScaleWidth      =   6075
      TabIndex        =   2
      Top             =   60
      Width           =   6135
      Begin VB.Frame Frame1 
         Height          =   1395
         Left            =   120
         TabIndex        =   3
         Top             =   60
         Width           =   5835
         Begin VB.Frame Frame4 
            Height          =   915
            Left            =   3900
            TabIndex        =   7
            Top             =   300
            Width           =   1755
            Begin VB.PictureBox picCantInc 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   500
               Left            =   200
               ScaleHeight     =   495
               ScaleWidth      =   975
               TabIndex        =   36
               Top             =   360
               Width           =   975
               Begin VB.TextBox txtCantIncPrecios 
                  Height          =   345
                  Left            =   0
                  TabIndex        =   37
                  Top             =   0
                  Width           =   855
               End
            End
            Begin VB.OptionButton opCantIncPrecios 
               Caption         =   "Cantidad"
               Height          =   255
               Left            =   120
               TabIndex        =   8
               Top             =   0
               Width           =   1095
            End
            Begin VB.Label lblMonCentralInc 
               Caption         =   "(EUR)"
               Height          =   315
               Left            =   1200
               TabIndex        =   9
               Top             =   420
               Width           =   435
            End
         End
         Begin VB.Frame Frame3 
            Height          =   915
            Left            =   180
            TabIndex        =   5
            Top             =   300
            Width           =   3555
            Begin VB.PictureBox picPorcenInc 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   615
               Left            =   120
               ScaleHeight     =   615
               ScaleWidth      =   3015
               TabIndex        =   28
               Top             =   240
               Width           =   3010
               Begin VB.TextBox txtPorcenIncPrecios 
                  Height          =   345
                  Left            =   2560
                  MaxLength       =   5
                  TabIndex        =   35
                  Top             =   100
                  Width           =   435
               End
               Begin MSComctlLib.Slider Slider1 
                  Height          =   315
                  Left            =   100
                  TabIndex        =   29
                  Top             =   120
                  Width           =   2415
                  _ExtentX        =   4260
                  _ExtentY        =   556
                  _Version        =   393216
                  Max             =   100
               End
            End
            Begin VB.OptionButton opPorcenIncPrecios 
               Caption         =   "Porcentaje"
               Height          =   315
               Left            =   60
               TabIndex        =   6
               Top             =   -60
               Width           =   1755
            End
            Begin VB.Label lblPorcenInc 
               Caption         =   "%"
               Height          =   315
               Left            =   3180
               TabIndex        =   34
               Top             =   420
               Width           =   255
            End
         End
         Begin VB.OptionButton opIncPrecios 
            Caption         =   "Incrementar precios"
            Height          =   195
            Left            =   120
            TabIndex        =   4
            Top             =   0
            Width           =   1875
         End
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1620
      TabIndex        =   1
      Top             =   5400
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancelar 
      BackColor       =   &H00808000&
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3180
      TabIndex        =   0
      Top             =   5400
      Width           =   1395
   End
End
Attribute VB_Name = "frmCatalogoModificarPrecios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private bRespetarPorcen As Boolean
Private bRespetarCant As Boolean

'variable para controlar la recarga de la grid de cat�logo
Public bRecargarGrid As Boolean

'Variables multiidioma
Private sIdiCant As String
Private sIdiPorcen As String

Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_MODIF_PRECIOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.caption = ador(0).Value
        ador.MoveNext
        opIncPrecios.caption = ador(0).Value
        ador.MoveNext
        opDecPrecios.caption = ador(0).Value
        ador.MoveNext
        opAhorro.caption = ador(0).Value
        ador.MoveNext
        opPorcenIncPrecios.caption = ador(0).Value
        opPorcenDecPrecios.caption = ador(0).Value
        opPorcenAhorro.caption = ador(0).Value
        sIdiPorcen = ador(0).Value
        ador.MoveNext
        opCantIncPrecios.caption = ador(0).Value
        opCantDecPrecios.caption = ador(0).Value
        opCantAhorro.caption = ador(0).Value
        sIdiCant = ador(0).Value
        ador.MoveNext
        cmdAceptar.caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.caption = ador(0).Value

        ador.Close
        
    End If
    'Modificado el 23-3-2006
    'Se cambian las labels para que muestren las monedas del catalogo
    Me.lblMonCentraAho.caption = "(" & frmCatalogo.sdbgAdjudicaciones.Columns("MON").Value & ")"
    Me.lblMonCentralDec.caption = "(" & frmCatalogo.sdbgAdjudicaciones.Columns("MON").Value & ")"
    Me.lblMonCentralInc.caption = "(" & frmCatalogo.sdbgAdjudicaciones.Columns("MON").Value & ")"
     
    Set ador = Nothing

End Sub

Private Sub cmdAceptar_Click()
Dim i As Integer
Dim oLineasAModif As CLineasCatalogo
Dim bIncPrec As Boolean
Dim bIncAho As Boolean
Dim bPorcen As Boolean
Dim Cant As Double
Dim teserror As TipoErrorSummit
Dim oLinea As CLineaCatalogo

    bRecargarGrid = False
    
    If opIncPrecios.Value Then
        bIncPrec = True
        bIncAho = False
    Else
        If opDecPrecios.Value Then
            bIncPrec = False
            bIncAho = False
        Else
            bIncPrec = False
            bIncAho = True
        End If
    End If
    
    'Incrementar precios
    If opPorcenIncPrecios.Value Then
        If Not IsNumeric(txtPorcenIncPrecios.Text) Then
            oMensajes.NoValido sIdiPorcen
            Exit Sub
        End If
        bPorcen = True
        Cant = Slider1.Value
    End If
    
    If opCantIncPrecios.Value Then
        If Not IsNumeric(txtCantIncPrecios.Text) Then
            oMensajes.NoValido sIdiCant
            Exit Sub
        End If
        bPorcen = False
        Cant = txtCantIncPrecios.Text
    End If
    
    'Decrementar precios
    If opPorcenDecPrecios.Value Then
        If Not IsNumeric(txtPorcenDecPrecios.Text) Then
            oMensajes.NoValido sIdiPorcen
            Exit Sub
        End If
        bPorcen = True
        Cant = Slider2.Value
    End If
    
    If opCantDecPrecios.Value Then
        If Not IsNumeric(txtCantDecPrecios.Text) Then
            oMensajes.NoValido sIdiCant
            Exit Sub
        End If
        bPorcen = False
        Cant = txtCantDecPrecios.Text
    End If
    
    'Aumentar ahorro adj
    If opPorcenAhorro.Value Then
        If Not IsNumeric(txtPorcenAhorro.Text) Then
            oMensajes.NoValido sIdiPorcen
            Exit Sub
        End If
        bPorcen = True
        Cant = Slider3.Value
    End If
    
    If opCantAhorro.Value Then
        If Not IsNumeric(txtCantAhorro.Text) Then
            oMensajes.NoValido sIdiCant
            Exit Sub
        End If
        bPorcen = False
        Cant = txtCantAhorro.Text
    End If
    
    If Cant > 0 Then
        Screen.MousePointer = vbHourglass
        Set oLineasAModif = Nothing
        Set oLineasAModif = oFSGSRaiz.Generar_CLineasCatalogo
        
        For i = 0 To frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Count - 1
            If IsEmpty(frmCatalogo.sdbgAdjudicaciones.Columns("ANYO").CellValue(frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Item(i))) And bIncAho Then
                'Si se ha seleccionado alguna l�nea de cat�logo que no viene de una adjudicaci�n no hay ahorro
                oMensajes.ImposibleAumentarAhorroALinCatNoAdj
                Screen.MousePointer = vbNormal
                Set oLineasAModif = Nothing
                Exit Sub
            End If
            If bIncAho Then
            'Si vamos a aumentar el ahorro necesitamos el A�O,GMN1,PROCE,ITEM,PRES de la l�nea de cat�logo
                oLineasAModif.Add frmCatalogo.sdbgAdjudicaciones.Columns("ID").CellValue(frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Item(i)), , , , , , , , , , , , frmCatalogo.sdbgAdjudicaciones.Columns("CANTADJ").CellValue(frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Item(i)), frmCatalogo.sdbgAdjudicaciones.Columns("PrecioUB").CellValue(frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Item(i)), , , , , , , , , , , , , , , , , , frmCatalogo.sdbgAdjudicaciones.Columns("PRES").CellValue(frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Item(i))
            Else
                oLineasAModif.Add frmCatalogo.sdbgAdjudicaciones.Columns("ID").CellValue(frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Item(i)), , , , , , , , , , , , , frmCatalogo.sdbgAdjudicaciones.Columns("PrecioUB").CellValue(frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Item(i))
            End If
        Next
        
        teserror = oLineasAModif.ModificarPreciosDeLasLineas(bIncPrec, bIncAho, bPorcen, Cant)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    
        For Each oLinea In oLineasAModif
            frmCatalogo.oLineasCatalogo.Item(CStr(oLinea.Id)).FECACT = oLinea.FECACT
        Next
        
        frmCatalogo.g_bRefrescandoUniPed = True
        For i = 0 To frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Count - 1
            frmCatalogo.sdbgAdjudicaciones.Bookmark = frmCatalogo.sdbgAdjudicaciones.SelBookmarks.Item(i)
            frmCatalogo.sdbgAdjudicaciones.Columns("PrecioUB").Value = oLineasAModif.Item(frmCatalogo.sdbgAdjudicaciones.Columns("ID").Value).PrecioUC
            'Actualizamos tb la linea  de catalogo
            frmCatalogo.oLineasCatalogo.Item(frmCatalogo.sdbgAdjudicaciones.Columns("ID").Value).PrecioUC = oLineasAModif.Item(frmCatalogo.sdbgAdjudicaciones.Columns("ID").Value).PrecioUC
        Next
        frmCatalogo.sdbgAdjudicaciones.SelBookmarks.RemoveAll
        frmCatalogo.g_bRefrescandoUniPed = False
                
        Set oLineasAModif = Nothing
        Set oLinea = Nothing
    
    End If
    
    Screen.MousePointer = vbNormal
    frmCatalogo.sdbgAdjudicaciones.Update
    
    Unload Me

End Sub

Private Sub cmdCancelar_Click()
    frmCatalogo.sdbgAdjudicaciones.SelBookmarks.RemoveAll
    Unload Me
End Sub

Private Sub Form_Load()

    CargarRecursos
    
    'lblMonCentraAho.Caption = "(" & basParametros.gParametrosGenerales.gsMONCEN & ")"
    'lblMonCentralDec.Caption = lblMonCentraAho.Caption
    'lblMonCentralInc.Caption = lblMonCentraAho.Caption

    bRespetarCant = False
    bRespetarPorcen = False

    If FSEPConf Then
        Picture3.Visible = False
        Me.Height = Me.Height - Me.Picture3.Height - 100
        Me.cmdAceptar.Top = Me.Height - 800
        Me.cmdCancelar.Top = Me.cmdAceptar.Top
    End If

    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2

End Sub

Private Sub opAhorro_Click()
    If opAhorro.Value = True Then
        opIncPrecios.Value = False
        opPorcenIncPrecios.Value = False
        opCantIncPrecios.Value = False
        Slider1.Value = 0
        txtPorcenIncPrecios.Text = ""
        picPorcenInc.Enabled = False
        txtCantIncPrecios.Text = ""
        picCantInc.Enabled = False
        
        opDecPrecios.Value = False
        opPorcenDecPrecios.Value = False
        opCantDecPrecios.Value = False
        Slider2.Value = 0
        txtPorcenDecPrecios.Text = ""
        picPorcenDec.Enabled = False
        txtCantDecPrecios.Text = ""
        picCantDec.Enabled = False
    End If
End Sub

Private Sub opCantAhorro_Click()
    If Not opAhorro.Value Then
        opCantAhorro.Value = False
    Else
        opPorcenAhorro.Value = False
        picCantAho.Enabled = True
        picPorcenAho.Enabled = False
        Slider3.Value = 0
        txtPorcenAhorro.Text = ""
    End If
End Sub

Private Sub opCantDecPrecios_Click()
    If Not opDecPrecios.Value Then
        opCantDecPrecios.Value = False
    Else
        opPorcenDecPrecios.Value = False
        picCantDec.Enabled = True
        picPorcenDec.Enabled = False
        Slider2.Value = 0
        txtPorcenDecPrecios.Text = ""
    End If
End Sub

Private Sub opCantIncPrecios_Click()
    If Not opIncPrecios.Value Then
        opCantIncPrecios.Value = False
    Else
        opPorcenIncPrecios.Value = False
        picCantInc.Enabled = True
        picPorcenInc.Enabled = False
        Slider1.Value = 0
        txtPorcenIncPrecios.Text = ""
    End If
End Sub

Private Sub opDecPrecios_Click()
    If opDecPrecios.Value = True Then
        opIncPrecios.Value = False
        opPorcenIncPrecios.Value = False
        opCantIncPrecios.Value = False
        Slider1.Value = 0
        txtPorcenIncPrecios.Text = ""
        picPorcenInc.Enabled = False
        txtCantIncPrecios.Text = ""
        picCantInc.Enabled = False

        opAhorro.Value = False
        opPorcenAhorro.Value = False
        opCantAhorro.Value = False
        Slider3.Value = 0
        txtPorcenAhorro.Text = ""
        picPorcenAho.Enabled = False
        txtCantAhorro.Text = ""
        picCantAho.Enabled = False
    End If
End Sub

Private Sub opIncPrecios_Click()
    If opIncPrecios.Value = True Then
        opDecPrecios.Value = False
        opPorcenDecPrecios.Value = False
        opCantDecPrecios.Value = False
        Slider2.Value = 0
        txtPorcenDecPrecios.Text = ""
        picPorcenDec.Enabled = False
        txtCantDecPrecios.Text = ""
        picCantDec.Enabled = False

        opAhorro.Value = False
        opPorcenAhorro.Value = False
        opCantAhorro.Value = False
        Slider3.Value = 0
        txtPorcenAhorro.Text = ""
        picPorcenAho.Enabled = False
        txtCantAhorro.Text = ""
        picCantAho.Enabled = False
    End If
End Sub

Private Sub opPorcenAhorro_Click()
    If Not opAhorro.Value Then
        opPorcenAhorro.Value = False
    Else
        opCantAhorro.Value = False
        picPorcenAho.Enabled = True
        picCantAho.Enabled = False
        txtCantAhorro.Text = ""
    End If
End Sub

Private Sub opPorcenDecPrecios_Click()
    If Not opDecPrecios.Value Then
        opPorcenDecPrecios.Value = False
    Else
        opCantDecPrecios.Value = False
        picPorcenDec.Enabled = True
        picCantDec.Enabled = False
        txtCantDecPrecios.Text = ""
    End If
End Sub

Private Sub opPorcenIncPrecios_Click()
    If Not opIncPrecios.Value Then
        opPorcenIncPrecios.Value = False
    Else
        opCantIncPrecios.Value = False
        picPorcenInc.Enabled = True
        picCantInc.Enabled = False
        txtCantIncPrecios.Text = ""
    End If
End Sub

Private Sub Slider1_Change()
    If bRespetarPorcen Then
        bRespetarPorcen = False
        Exit Sub
    End If
    txtPorcenIncPrecios.Text = Slider1.Value
End Sub

Private Sub Slider2_Change()
    If bRespetarPorcen Then
        bRespetarPorcen = False
        Exit Sub
    End If
    txtPorcenDecPrecios.Text = Slider2.Value
End Sub

Private Sub Slider3_Change()
    If bRespetarPorcen Then
        bRespetarPorcen = False
        Exit Sub
    End If
    txtPorcenAhorro.Text = Slider3.Value
End Sub


Private Sub txtCantAhorro_Change()
    If Not bRespetarCant Then
        If Trim(txtCantAhorro.Text) = "" Then Exit Sub
        If Not IsNumeric(txtCantAhorro) Then
            bRespetarCant = True
'            txtCantIncPrecios.Text = Left(txtCantIncPrecios, Len(txtCantIncPrecios) - 1)
            txtCantAhorro.Text = ""
            bRespetarCant = False
            Exit Sub
        End If
    End If
End Sub

Private Sub txtCantDecPrecios_Change()
    If Not bRespetarCant Then
        If Trim(txtCantDecPrecios.Text) = "" Then Exit Sub
        If Not IsNumeric(txtCantDecPrecios) Then
            bRespetarCant = True
'            txtCantdecPrecios.Text = Left(txtCantdecPrecios, Len(txtCantdecPrecios) - 1)
            txtCantDecPrecios.Text = ""
            bRespetarCant = False
            Exit Sub
        End If
    End If
End Sub

Private Sub txtCantIncPrecios_Change()
    If Not bRespetarCant Then
        If Trim(txtCantIncPrecios.Text) = "" Then Exit Sub
        If Not IsNumeric(txtCantIncPrecios) Then
            bRespetarCant = True
'            txtCantIncPrecios.Text = Left(txtCantIncPrecios, Len(txtCantIncPrecios) - 1)
            txtCantIncPrecios.Text = ""
            bRespetarCant = False
            Exit Sub
        End If
    End If
End Sub

Private Sub txtPorcenAhorro_Change()
    If Trim(txtPorcenAhorro.Text) = "" Then
        bRespetarPorcen = True
        Slider1.Value = 0
        bRespetarPorcen = False
        Exit Sub
    End If
    
    If Not IsNumeric(txtPorcenAhorro) Then
        bRespetarPorcen = True
        txtPorcenAhorro.Text = Left(txtPorcenAhorro, Len(txtPorcenAhorro) - 1)
        bRespetarPorcen = False
        Exit Sub
    End If
    
    If CDbl(txtPorcenAhorro.Text) <= 0 Then
        txtPorcenAhorro.Text = ""
        Slider3.Value = 0
    Else
        If CDbl(txtPorcenAhorro.Text) > 100 Then
            txtPorcenAhorro.Text = Left(txtPorcenAhorro, Len(txtPorcenAhorro) - 1)
        End If
        bRespetarPorcen = True
        If txtPorcenAhorro.Text < 32767 Then
            Slider3.Value = txtPorcenAhorro.Text
        End If
        bRespetarPorcen = False
    End If

End Sub

Private Sub txtPorcenDecPrecios_Change()
    If Trim(txtPorcenDecPrecios.Text) = "" Then
        bRespetarPorcen = True
        Slider2.Value = 0
        bRespetarPorcen = False
        Exit Sub
    End If
    
    If Not IsNumeric(txtPorcenDecPrecios) Then
        bRespetarPorcen = True
        txtPorcenDecPrecios.Text = Left(txtPorcenDecPrecios, Len(txtPorcenDecPrecios) - 1)
        bRespetarPorcen = False
        Exit Sub
    End If
    
    If CDbl(txtPorcenDecPrecios.Text) <= 0 Then
        txtPorcenDecPrecios.Text = ""
        Slider2.Value = 0
    Else
        If CDbl(txtPorcenDecPrecios.Text) > 100 Then
            txtPorcenDecPrecios.Text = Left(txtPorcenDecPrecios, Len(txtPorcenDecPrecios) - 1)
        End If
        bRespetarPorcen = True
        If txtPorcenDecPrecios.Text < 32767 Then
            Slider2.Value = txtPorcenDecPrecios.Text
        End If
        bRespetarPorcen = False
    End If

End Sub

Private Sub txtPorcenIncPrecios_Change()
    If Trim(txtPorcenIncPrecios.Text) = "" Then
        bRespetarPorcen = True
        Slider1.Value = 0
        bRespetarPorcen = False
        Exit Sub
    End If
    
    If Not IsNumeric(txtPorcenIncPrecios) Then
        bRespetarPorcen = True
        txtPorcenIncPrecios.Text = Left(txtPorcenIncPrecios, Len(txtPorcenIncPrecios) - 1)
        bRespetarPorcen = False
        Exit Sub
    End If
    
    If CDbl(txtPorcenIncPrecios.Text) <= 0 Then
        txtPorcenIncPrecios.Text = ""
        Slider1.Value = 0
    Else
        If CDbl(txtPorcenIncPrecios.Text) > 100 Then
            txtPorcenIncPrecios.Text = Left(txtPorcenIncPrecios, Len(txtPorcenIncPrecios) - 1)
        End If
        bRespetarPorcen = True
        If txtPorcenIncPrecios.Text < 32767 Then
            Slider1.Value = txtPorcenIncPrecios.Text
        End If
        bRespetarPorcen = False
    End If
End Sub
