VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPROCEEscPres 
   BackColor       =   &H00808000&
   ClientHeight    =   3885
   ClientLeft      =   5505
   ClientTop       =   3300
   ClientWidth     =   6390
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   3885
   ScaleWidth      =   6390
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      Height          =   345
      Left            =   2798
      TabIndex        =   3
      Top             =   3465
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   345
      Left            =   3233
      TabIndex        =   2
      Top             =   3465
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   2153
      TabIndex        =   1
      Top             =   3465
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGridEscalados 
      Height          =   3000
      Left            =   180
      TabIndex        =   0
      Top             =   405
      Width           =   6015
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   5
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPROCEEscPres.frx":0000
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6403
      Columns(1).Caption=   "Cantidades directas"
      Columns(1).Name =   "DIR"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).NumberFormat=   "standard"
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Caption=   "Cantidad inicial"
      Columns(2).Name =   "INI"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "standard"
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Caption=   "Cantidad final"
      Columns(3).Name =   "FIN"
      Columns(3).Alignment=   1
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "standard"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   3200
      Columns(4).Caption=   "Presupuesto Unitario"
      Columns(4).Name =   "PRES"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,##0.00####################"
      Columns(4).FieldLen=   256
      _ExtentX        =   10610
      _ExtentY        =   5292
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblPresUnitEsc 
      BackColor       =   &H00808000&
      Caption         =   "Presupuestos unitarios escalados:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   4
      Top             =   90
      Width           =   4470
   End
End
Attribute VB_Name = "frmPROCEEscPres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public modo As TModoEscalado
Private SSDBGridEscalados_enEdicion As Boolean

Public g_bModoEdicionItem As Boolean
Public g_sCodGrupoSeleccionado As String
Public g_oGrupoSeleccionado As CGrupo

Public g_oItemEnEdicion As CItem
Public Ok As Boolean

Private m_bOptionAuto As Boolean

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String
Private Sub cmdAceptar_Click()

    'Si el grid est� en modo update hay que forzar actualizaci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If SSDBGridEscalados.DataChanged Then
    
        SSDBGridEscalados_BeforeUpdate (False)
        SSDBGridEscalados.Update
        
        If Not SSDBGridEscalados_enEdicion Then
            If ComprobarFormulario() Then
                If Not GuardarDatos Then Exit Sub
                Me.Hide
            End If
        Else
            SSDBGridEscalados_enEdicion = False
        End If
    Else
        If ComprobarFormulario() Then
            If Not GuardarDatos Then Exit Sub
            Me.Hide
        End If
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCerrar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "cmdCerrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    CargarRecursos

    PonerFieldSeparator Me

    SSDBGridEscalados_enEdicion = False

    'Listas de unidades

    If modo = ModoRangos Then
        ModoARangos
    Else
        ModoADirecto
    End If
    PrepararLista
    
    If g_bModoEdicionItem Then
        ModoEdicion
    Else
        ModoConsulta
    End If
    
    'Calculamos las  dimensiones del form
    Me.Width = 5670
    Dim nFilas As Long
    nFilas = Me.SSDBGridEscalados.Rows
    If nFilas > 10 Then nFilas = 10
    Me.Height = 1280 + (nFilas * Me.SSDBGridEscalados.RowHeight)
    
    'Lo Posicionamos
    Me.Top = frmPROCE.coord_y * Screen.TwipsPerPixelY + frmPROCE.m_UltY
    Me.Left = (frmPROCE.coord_x * Screen.TwipsPerPixelX) - (Me.Width / 2)
    
    If Me.Top + Me.Height > Screen.Height Then
        'Si no cabe debajo de la celda, lo colocamos por encima
        Me.Top = Me.Top - Me.Height - frmPROCE.sdbgItems.RowHeight
    End If
    
    If Me.Left + Me.Width > Screen.Width Then
        Me.Left = Me.Left - (Me.Left + Me.Width - Screen.Width)
        'Lo ajustamos a la derecha
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCEESCPRES, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCerrar.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblPresUnitEsc.caption = Ador(0).Value
        Ador.MoveNext
        Me.SSDBGridEscalados.Columns("DIR").caption = Ador(0).Value
        Ador.MoveNext
        Me.SSDBGridEscalados.Columns("INI").caption = Ador(0).Value
        Ador.MoveNext
        Me.SSDBGridEscalados.Columns("FIN").caption = Ador(0).Value
        Ador.MoveNext
        Me.SSDBGridEscalados.Columns("PRES").caption = Ador(0).Value
        Ador.MoveNext
        
        
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub


'Establece el formulario en modo edici�n
Private Sub ModoEdicion()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.cmdAceptar.Visible = True
    Me.cmdCancelar.Visible = True
    Me.cmdCerrar.Visible = False
    Me.SSDBGridEscalados.AllowUpdate = True
    Me.cmdAceptar.Default = True
    Me.cmdCancelar.Cancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "ModoEdicion", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

'Establece el formulario en modo consulta
Private Sub ModoConsulta()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.cmdAceptar.Visible = False
    Me.cmdCancelar.Visible = False
    Me.cmdCerrar.Visible = True
    Me.SSDBGridEscalados.AllowUpdate = False
    Me.cmdCerrar.Default = True
    Me.cmdCerrar.Cancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "ModoConsulta", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


'Establece el modo de escalado en Rangos
Private Sub ModoARangos()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSDBGridEscalados.Columns("DIR").Visible = False
    SSDBGridEscalados.Columns("INI").Visible = True
    SSDBGridEscalados.Columns("FIN").Visible = True
    modo = ModoRangos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "ModoARangos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

'Establece el modo de escalado en Cantidades directas
Private Sub ModoADirecto()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSDBGridEscalados.Columns("DIR").Visible = True
    SSDBGridEscalados.Columns("INI").Visible = False
    SSDBGridEscalados.Columns("FIN").Visible = False
    modo = ModoDirecto
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "ModoADirecto", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


''' <summary>
''' Inicializa la lista de Escalados
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo: 0,1</remarks>
Private Sub PrepararLista()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Me.g_oItemEnEdicion.CargarPresEscalados

If Not Me.g_oItemEnEdicion.Escalados Is Nothing Then

    Dim cEsc As CEscalado
    Dim i As Long
    Dim Linea As String
    For Each cEsc In Me.g_oItemEnEdicion.Escalados
    
        Linea = cEsc.Id & Chr(m_lSeparador)
    
        'Set cEsc = Me.g_oGrupoSeleccionado.Escalados.Item(i)
        Linea = Linea & cEsc.Inicial & Chr(m_lSeparador) & cEsc.Inicial & Chr(m_lSeparador)
        
        If Me.modo = ModoRangos Then
            Linea = Linea & cEsc.final & Chr(m_lSeparador)
        Else
            Linea = Linea & "" & Chr(m_lSeparador)
        End If
        
        Linea = Linea & cEsc.Presupuesto
        
        Me.SSDBGridEscalados.AddItem Linea

    Next
    
End If


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "PrepararLista", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


Private Sub Form_Resize()

On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    Dim separador As Long: separador = 180
    
    Dim SeparadorV As Long: SeparadorV = 80
    Dim Scroll As Long
        Scroll = 200
    Dim Marcadores As Long
        Marcadores = 380
    
    Dim Borde As Long: Borde = 32
    
    'Ajustar Tama�o del grid
    Me.SSDBGridEscalados.Left = separador
    Me.SSDBGridEscalados.Width = Me.Width - (2 * (separador + Borde))
    Me.SSDBGridEscalados.Top = lblPresUnitEsc.Top + lblPresUnitEsc.Height + SeparadorV
    Me.SSDBGridEscalados.Height = Me.Height - lblPresUnitEsc.Top - lblPresUnitEsc.Height - (3 * SeparadorV) - (2 * Borde) - Me.cmdAceptar.Height
    
   'Ajustar las columnas del grid
    If modo = ModoRangos Then
        Me.SSDBGridEscalados.Columns("INI").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.3
        Me.SSDBGridEscalados.Columns("FIN").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.3
        Me.SSDBGridEscalados.Columns("PRES").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.4
    Else
        Me.SSDBGridEscalados.Columns("DIR").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
        Me.SSDBGridEscalados.Columns("PRES").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
    End If
    

   'Centrar Botones
    Me.cmdCerrar.Left = (Me.Width - cmdCerrar.Width) / 2
    Me.cmdAceptar.Left = (Me.Width - (cmdAceptar.Width + cmdCancelar.Width + separador)) / 2
    Me.cmdCancelar.Left = Me.cmdAceptar.Left + Me.cmdAceptar.Width + separador
    
    Me.cmdCerrar.Top = Me.SSDBGridEscalados.Top + Me.SSDBGridEscalados.Height + SeparadorV
    Me.cmdAceptar.Top = Me.cmdCerrar.Top
    Me.cmdCancelar.Top = Me.cmdCerrar.Top

    

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub


'--<summary>
'--Comprueba que se han introducido correctamente los presupuestos unitarios necesarios
'--En caso de encontrar un valor no  v�lido muestra un mensaje, y se posiciona en la celda en modo edici�n
'--</summary>
'--<remarks>Click Bot�n Aceptar</remarks>
'--<revision>DPD 08/08/2011</revision>

Private Function ComprobarFormulario() As Boolean
    
    Dim vPres As Variant
    Dim i As Long
    Dim bm As Variant ' Bookmark
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 0 To Me.SSDBGridEscalados.Rows
        'Comprobamos los presupuestos
        bm = SSDBGridEscalados.AddItemBookmark(i)
        vPres = SSDBGridEscalados.Columns("PRES").CellValue(bm)
        If (Not IsNumeric(vPres)) And vPres <> "" Then
            oMensajes.valorCampoNoValido 'Valor no v�lido.
            SSDBGridEscalados.Bookmark = bm
            SSDBGridEscalados.Col = SSDBGridEscalados.Columns("PRES").Position
            'SSDBGridEscalados.Columns("PRES").Value = " "
            'SSDBGridEscalados.Columns("PRES").Value = ""
            'SSDBGridEscalados.Columns("PRES").CellStyleSet "Sel"
            ComprobarFormulario = False
            Exit Function
        End If
    Next i

    ComprobarFormulario = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "ComprobarFormulario", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

'--<summary>
'--Amacena en BD los presupuestos del �tem
'--</summary>
'--<returns>True si se almacena correctamente. False en caso contrario</returns>
'--<remarks>Llamada desde bot�n Aceptar</remarks>
'--<revision>DPD 12/08/11</revision>

Private Function GuardarDatos() As Boolean
    Dim bm As Variant ' Bookmark
    Dim i As Long
    Dim teserror As TipoErrorSummit

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Ok = False

    GuardarDatos = True
    For i = 0 To SSDBGridEscalados.Rows - 1
        bm = SSDBGridEscalados.AddItemBookmark(i)
        g_oItemEnEdicion.Escalados.Item(SSDBGridEscalados.Columns("ID").CellValue(bm)).Presupuesto = SSDBGridEscalados.Columns("PRES").CellValue(bm)
    Next i

    'Guardar en la base de datos
    teserror = g_oItemEnEdicion.Escalados.GuardarPresupuestos(False)
    If teserror.NumError <> TESnoerror Then
        GuardarDatos = False
    Else
        Ok = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "GuardarDatos", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEEscPres", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub SSDBGridEscalados_BeforeUpdate(Cancel As Integer)
    'DPD@@ Comprobar que los datos son correctos
End Sub

