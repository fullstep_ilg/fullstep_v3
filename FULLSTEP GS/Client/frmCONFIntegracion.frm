VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCONFIntegracion 
   Caption         =   "DConfiguraci�n de integraci�n"
   ClientHeight    =   7395
   ClientLeft      =   1275
   ClientTop       =   6060
   ClientWidth     =   11340
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCONFIntegracion.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7395
   ScaleWidth      =   11340
   Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
      Height          =   795
      Left            =   6000
      TabIndex        =   11
      Top             =   4110
      Width           =   2805
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ORDEN"
      Columns(0).Name =   "ORDEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "VALOR"
      Columns(1).Name =   "VALOR"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4948
      _ExtentY        =   1402
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin TabDlg.SSTab sstabPlantillas 
      Height          =   4995
      Left            =   210
      TabIndex        =   6
      Top             =   960
      Width           =   10845
      _ExtentX        =   19129
      _ExtentY        =   8811
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Entidades a integrar"
      TabPicture(0)   =   "frmCONFIntegracion.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "sdbgIntegracion"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdEdicEntidades"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Plantillas de atributos"
      TabPicture(1)   =   "frmCONFIntegracion.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sdbgAtribPlant"
      Tab(1).Control(1)=   "cmdEdicAtributos"
      Tab(1).Control(2)=   "cmdAnyadirAtr"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "cmdEliAtr"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "sdbddAmbito"
      Tab(1).Control(5)=   "cmdBajar"
      Tab(1).Control(6)=   "cmdSubir"
      Tab(1).ControlCount=   7
      TabCaption(2)   =   "Notificaciones"
      TabPicture(2)   =   "frmCONFIntegracion.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lblNotificados"
      Tab(2).Control(1)=   "Line1"
      Tab(2).Control(2)=   "sdbgContactos"
      Tab(2).Control(3)=   "cmdEdicNotific"
      Tab(2).Control(4)=   "Picture1"
      Tab(2).ControlCount=   5
      Begin VB.CommandButton cmdSubir 
         Height          =   435
         Left            =   -74880
         Picture         =   "frmCONFIntegracion.frx":0D06
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   480
         UseMaskColor    =   -1  'True
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.CommandButton cmdBajar 
         Height          =   435
         Left            =   -74880
         Picture         =   "frmCONFIntegracion.frx":1048
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   960
         UseMaskColor    =   -1  'True
         Visible         =   0   'False
         Width           =   435
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddAmbito 
         Height          =   495
         Left            =   -73200
         TabIndex        =   20
         Top             =   3660
         Width           =   2715
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   185
         Columns(0).Width=   3200
         Columns(0).Caption=   "AMBITO"
         Columns(0).Name =   "AMBITO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   4789
         _ExtentY        =   873
         _StockProps     =   77
      End
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   1095
         Left            =   -64710
         ScaleHeight     =   1095
         ScaleWidth      =   465
         TabIndex        =   17
         Top             =   900
         Width           =   465
         Begin VB.CommandButton cmdElimNotificadoCopia 
            Height          =   300
            Left            =   60
            Picture         =   "frmCONFIntegracion.frx":138A
            Style           =   1  'Graphical
            TabIndex        =   19
            Top             =   510
            UseMaskColor    =   -1  'True
            Width           =   300
         End
         Begin VB.CommandButton cmdAnyaNotificadoCopia 
            Height          =   300
            Left            =   60
            Picture         =   "frmCONFIntegracion.frx":141C
            Style           =   1  'Graphical
            TabIndex        =   18
            Top             =   120
            UseMaskColor    =   -1  'True
            Width           =   300
         End
      End
      Begin VB.CommandButton cmdEliAtr 
         Caption         =   "D&Eliminar"
         Height          =   345
         Left            =   -72660
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   4440
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdAnyadirAtr 
         Caption         =   "D&A�adir"
         Height          =   345
         Left            =   -73740
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   4440
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEdicAtributos 
         Caption         =   "D&Edici�n"
         Height          =   345
         Left            =   -74850
         TabIndex        =   14
         Top             =   4440
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEdicNotific 
         Caption         =   "D&Edici�n"
         Height          =   345
         Left            =   -74850
         TabIndex        =   13
         Top             =   4470
         Width           =   1005
      End
      Begin VB.CommandButton cmdEdicEntidades 
         Caption         =   "D&Edici�n"
         Height          =   345
         Left            =   150
         TabIndex        =   12
         Top             =   4440
         Width           =   1005
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgIntegracion 
         Height          =   3615
         Left            =   150
         TabIndex        =   7
         Top             =   420
         Width           =   10560
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   11
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCONFIntegracion.frx":149E
         stylesets(1).Name=   "Cabecera"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8421504
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCONFIntegracion.frx":14BA
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         CellNavigation  =   1
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   11
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "TABLA"
         Columns(0).Name =   "TABLA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1138
         Columns(1).Name =   "ACTIVA"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Style=   2
         Columns(1).HeadStyleSet=   "Cabecera"
         Columns(2).Width=   4921
         Columns(2).Caption=   "ENTIDAD"
         Columns(2).Name =   "ENTIDAD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16777152
         Columns(2).HeadStyleSet=   "Cabecera"
         Columns(3).Width=   3889
         Columns(3).Caption=   "SENTIDO"
         Columns(3).Name =   "SENTIDO"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   1
         Columns(3).HeadStyleSet=   "Cabecera"
         Columns(4).Width=   3200
         Columns(4).Caption=   "TIPO_TRADUCCION"
         Columns(4).Name =   "TIPO_TRADUCCION"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).HeadStyleSet=   "Cabecera"
         Columns(4).StyleSet=   "Normal"
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "SENTIDO_ID"
         Columns(5).Name =   "SENTIDO_ID"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   1984
         Columns(6).Caption=   "TABLA_INTERMEDIA"
         Columns(6).Name =   "TABLA_INTERMEDIA"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Style=   2
         Columns(6).HeadStyleSet=   "Cabecera"
         Columns(6).StyleSet=   "Normal"
         Columns(7).Width=   1984
         Columns(7).Caption=   "DEST"
         Columns(7).Name =   "DEST"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).Style=   4
         Columns(7).ButtonsAlways=   -1  'True
         Columns(7).HasBackColor=   -1  'True
         Columns(7).HeadStyleSet=   "Cabecera"
         Columns(8).Width=   3200
         Columns(8).Caption=   "ORIGEN"
         Columns(8).Name =   "ORIGEN"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Style=   4
         Columns(8).ButtonsAlways=   -1  'True
         Columns(8).HeadStyleSet=   "Cabecera"
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "TRADUCCION_ID"
         Columns(9).Name =   "TRADUCCION_ID"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "CLAVE"
         Columns(10).Name=   "CLAVE"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         _ExtentX        =   18627
         _ExtentY        =   6376
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgContactos 
         Height          =   1215
         Left            =   -74850
         TabIndex        =   8
         Top             =   870
         Width           =   10080
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   3
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCONFIntegracion.frx":14D6
         stylesets(1).Name=   "Cabecera"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8421504
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCONFIntegracion.frx":14F2
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         CellNavigation  =   1
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Caption=   "PERSONA"
         Columns(0).Name =   "PERSONA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "EMAIL"
         Columns(1).Name =   "EMAIL"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   17780
         _ExtentY        =   2143
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAtribPlant 
         Height          =   3000
         Left            =   -74400
         TabIndex        =   9
         Top             =   480
         Width           =   10065
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   22
         stylesets.count =   8
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCONFIntegracion.frx":150E
         stylesets(1).Name=   "Yellow"
         stylesets(1).BackColor=   8454143
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCONFIntegracion.frx":1679
         stylesets(2).Name=   "Gris"
         stylesets(2).BackColor=   13158600
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmCONFIntegracion.frx":1695
         stylesets(3).Name=   "Bold"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmCONFIntegracion.frx":16B1
         stylesets(4).Name=   "Proceso"
         stylesets(4).BackColor=   7895160
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmCONFIntegracion.frx":16CD
         stylesets(5).Name=   "Normal"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmCONFIntegracion.frx":16E9
         stylesets(6).Name=   "Green"
         stylesets(6).BackColor=   7912636
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmCONFIntegracion.frx":1705
         stylesets(7).Name=   "Azul"
         stylesets(7).BackColor=   16777160
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmCONFIntegracion.frx":1721
         UseGroups       =   -1  'True
         AllowUpdate     =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Groups.Count    =   2
         Groups(0).Width =   10583
         Groups(0).Caption=   "Definici�n de atributos"
         Groups(0).HasHeadForeColor=   -1  'True
         Groups(0).HasHeadBackColor=   -1  'True
         Groups(0).HeadForeColor=   16777215
         Groups(0).HeadBackColor=   32896
         Groups(0).Columns.Count=   10
         Groups(0).Columns(0).Width=   1296
         Groups(0).Columns(0).Visible=   0   'False
         Groups(0).Columns(0).Caption=   "ID"
         Groups(0).Columns(0).Name=   "ID"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(1).Width=   1429
         Groups(0).Columns(1).Visible=   0   'False
         Groups(0).Columns(1).Caption=   "IDATRIB"
         Groups(0).Columns(1).Name=   "IDATRIB"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(2).Width=   926
         Groups(0).Columns(2).Visible=   0   'False
         Groups(0).Columns(2).Caption=   "IDTIPO"
         Groups(0).Columns(2).Name=   "IDTIPO"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         Groups(0).Columns(3).Width=   1879
         Groups(0).Columns(3).Caption=   "Adjudicaci�n"
         Groups(0).Columns(3).Name=   "ADJUDICACION"
         Groups(0).Columns(3).DataField=   "Column 3"
         Groups(0).Columns(3).DataType=   8
         Groups(0).Columns(3).FieldLen=   256
         Groups(0).Columns(3).Style=   2
         Groups(0).Columns(3).HasHeadForeColor=   -1  'True
         Groups(0).Columns(3).HasHeadBackColor=   -1  'True
         Groups(0).Columns(3).HeadForeColor=   16777215
         Groups(0).Columns(3).HeadBackColor=   32896
         Groups(0).Columns(4).Width=   1138
         Groups(0).Columns(4).Caption=   "Pedido"
         Groups(0).Columns(4).Name=   "PEDIDO"
         Groups(0).Columns(4).DataField=   "Column 4"
         Groups(0).Columns(4).DataType=   8
         Groups(0).Columns(4).FieldLen=   256
         Groups(0).Columns(4).Style=   2
         Groups(0).Columns(4).HasHeadForeColor=   -1  'True
         Groups(0).Columns(4).HasHeadBackColor=   -1  'True
         Groups(0).Columns(4).HeadForeColor=   16777215
         Groups(0).Columns(4).HeadBackColor=   32896
         Groups(0).Columns(5).Width=   1773
         Groups(0).Columns(5).Caption=   "RECEP"
         Groups(0).Columns(5).Name=   "RECEP"
         Groups(0).Columns(5).DataField=   "Column 5"
         Groups(0).Columns(5).DataType=   8
         Groups(0).Columns(5).FieldLen=   256
         Groups(0).Columns(5).Style=   2
         Groups(0).Columns(5).HasHeadForeColor=   -1  'True
         Groups(0).Columns(5).HasHeadBackColor=   -1  'True
         Groups(0).Columns(5).HeadForeColor=   16777215
         Groups(0).Columns(5).HeadBackColor=   32896
         Groups(0).Columns(6).Width=   1376
         Groups(0).Columns(6).Caption=   "C�digo"
         Groups(0).Columns(6).Name=   "CODIGO"
         Groups(0).Columns(6).CaptionAlignment=   2
         Groups(0).Columns(6).DataField=   "Column 6"
         Groups(0).Columns(6).DataType=   8
         Groups(0).Columns(6).FieldLen=   256
         Groups(0).Columns(6).HasHeadForeColor=   -1  'True
         Groups(0).Columns(6).HasHeadBackColor=   -1  'True
         Groups(0).Columns(6).HasBackColor=   -1  'True
         Groups(0).Columns(6).HeadForeColor=   16777215
         Groups(0).Columns(6).HeadBackColor=   32896
         Groups(0).Columns(6).BackColor=   16777160
         Groups(0).Columns(7).Width=   1323
         Groups(0).Columns(7).Caption=   "Denominaci�n"
         Groups(0).Columns(7).Name=   "DENOMINACION"
         Groups(0).Columns(7).CaptionAlignment=   2
         Groups(0).Columns(7).DataField=   "Column 7"
         Groups(0).Columns(7).DataType=   8
         Groups(0).Columns(7).FieldLen=   256
         Groups(0).Columns(7).HasHeadForeColor=   -1  'True
         Groups(0).Columns(7).HasHeadBackColor=   -1  'True
         Groups(0).Columns(7).HasBackColor=   -1  'True
         Groups(0).Columns(7).HeadForeColor=   16777215
         Groups(0).Columns(7).HeadBackColor=   32896
         Groups(0).Columns(7).BackColor=   16777160
         Groups(0).Columns(8).Width=   1191
         Groups(0).Columns(8).Caption=   "Descr."
         Groups(0).Columns(8).Name=   "DESCRIPCION"
         Groups(0).Columns(8).Alignment=   2
         Groups(0).Columns(8).CaptionAlignment=   2
         Groups(0).Columns(8).DataField=   "Column 8"
         Groups(0).Columns(8).DataType=   8
         Groups(0).Columns(8).FieldLen=   256
         Groups(0).Columns(8).Style=   4
         Groups(0).Columns(8).ButtonsAlways=   -1  'True
         Groups(0).Columns(8).HasHeadForeColor=   -1  'True
         Groups(0).Columns(8).HasHeadBackColor=   -1  'True
         Groups(0).Columns(8).HeadForeColor=   16777215
         Groups(0).Columns(8).HeadBackColor=   32896
         Groups(0).Columns(8).StyleSet=   "Bold"
         Groups(0).Columns(9).Width=   1905
         Groups(0).Columns(9).Caption=   "Tipo de dato"
         Groups(0).Columns(9).Name=   "TIPODEDATO"
         Groups(0).Columns(9).DataField=   "Column 9"
         Groups(0).Columns(9).DataType=   8
         Groups(0).Columns(9).FieldLen=   256
         Groups(0).Columns(9).HasHeadForeColor=   -1  'True
         Groups(0).Columns(9).HasHeadBackColor=   -1  'True
         Groups(0).Columns(9).HasBackColor=   -1  'True
         Groups(0).Columns(9).HeadForeColor=   16777215
         Groups(0).Columns(9).HeadBackColor=   32896
         Groups(0).Columns(9).BackColor=   16777160
         Groups(1).Width =   22199
         Groups(1).Caption=   "Introducci�n"
         Groups(1).HasHeadForeColor=   -1  'True
         Groups(1).HasHeadBackColor=   -1  'True
         Groups(1).HeadForeColor=   16777215
         Groups(1).HeadBackColor=   32896
         Groups(1).Columns.Count=   12
         Groups(1).Columns(0).Width=   2223
         Groups(1).Columns(0).Caption=   "Selecci�n"
         Groups(1).Columns(0).Name=   "SELECCION"
         Groups(1).Columns(0).DataField=   "Column 10"
         Groups(1).Columns(0).DataType=   8
         Groups(1).Columns(0).FieldLen=   256
         Groups(1).Columns(0).Style=   2
         Groups(1).Columns(0).HasHeadForeColor=   -1  'True
         Groups(1).Columns(0).HasHeadBackColor=   -1  'True
         Groups(1).Columns(0).HeadForeColor=   16777215
         Groups(1).Columns(0).HeadBackColor=   32896
         Groups(1).Columns(1).Width=   1984
         Groups(1).Columns(1).Name=   "LISTAVALORES"
         Groups(1).Columns(1).Alignment=   2
         Groups(1).Columns(1).DataField=   "Column 11"
         Groups(1).Columns(1).DataType=   8
         Groups(1).Columns(1).FieldLen=   256
         Groups(1).Columns(1).Style=   4
         Groups(1).Columns(1).ButtonsAlways=   -1  'True
         Groups(1).Columns(1).HasHeadBackColor=   -1  'True
         Groups(1).Columns(1).HeadBackColor=   32896
         Groups(1).Columns(1).StyleSet=   "Bold"
         Groups(1).Columns(2).Width=   2910
         Groups(1).Columns(2).Caption=   "�mbito de atributo"
         Groups(1).Columns(2).Name=   "AMBITO"
         Groups(1).Columns(2).DataField=   "Column 12"
         Groups(1).Columns(2).DataType=   8
         Groups(1).Columns(2).FieldLen=   256
         Groups(1).Columns(2).HasHeadForeColor=   -1  'True
         Groups(1).Columns(2).HasHeadBackColor=   -1  'True
         Groups(1).Columns(2).HeadForeColor=   16777215
         Groups(1).Columns(2).HeadBackColor=   32896
         Groups(1).Columns(3).Width=   2249
         Groups(1).Columns(3).Caption=   "Obligatorio"
         Groups(1).Columns(3).Name=   "OBLIGATORIO"
         Groups(1).Columns(3).DataField=   "Column 13"
         Groups(1).Columns(3).DataType=   8
         Groups(1).Columns(3).FieldLen=   256
         Groups(1).Columns(3).Style=   2
         Groups(1).Columns(3).HasHeadForeColor=   -1  'True
         Groups(1).Columns(3).HasHeadBackColor=   -1  'True
         Groups(1).Columns(3).HeadForeColor=   16777215
         Groups(1).Columns(3).HeadBackColor=   32896
         Groups(1).Columns(4).Width=   1879
         Groups(1).Columns(4).Caption=   "Valor por defecto"
         Groups(1).Columns(4).Name=   "VALORPORDEFECTO"
         Groups(1).Columns(4).CaptionAlignment=   2
         Groups(1).Columns(4).DataField=   "Column 14"
         Groups(1).Columns(4).DataType=   8
         Groups(1).Columns(4).FieldLen=   256
         Groups(1).Columns(4).HasHeadForeColor=   -1  'True
         Groups(1).Columns(4).HasHeadBackColor=   -1  'True
         Groups(1).Columns(4).HeadForeColor=   16777215
         Groups(1).Columns(4).HeadBackColor=   32896
         Groups(1).Columns(5).Width=   2249
         Groups(1).Columns(5).Caption=   "Validaci�n ERP"
         Groups(1).Columns(5).Name=   "VALIDACION"
         Groups(1).Columns(5).DataField=   "Column 15"
         Groups(1).Columns(5).DataType=   8
         Groups(1).Columns(5).FieldLen=   256
         Groups(1).Columns(5).Style=   2
         Groups(1).Columns(5).HasHeadForeColor=   -1  'True
         Groups(1).Columns(5).HasHeadBackColor=   -1  'True
         Groups(1).Columns(5).HeadForeColor=   16777215
         Groups(1).Columns(5).HeadBackColor=   32896
         Groups(1).Columns(6).Width=   2328
         Groups(1).Columns(6).Caption=   "Relaci�n de atrib."
         Groups(1).Columns(6).Name=   "RELACION"
         Groups(1).Columns(6).Alignment=   2
         Groups(1).Columns(6).DataField=   "Column 16"
         Groups(1).Columns(6).DataType=   8
         Groups(1).Columns(6).FieldLen=   256
         Groups(1).Columns(6).Style=   4
         Groups(1).Columns(6).ButtonsAlways=   -1  'True
         Groups(1).Columns(6).HasHeadForeColor=   -1  'True
         Groups(1).Columns(6).HasHeadBackColor=   -1  'True
         Groups(1).Columns(6).HeadForeColor=   16777215
         Groups(1).Columns(6).HeadBackColor=   32896
         Groups(1).Columns(6).StyleSet=   "Bold"
         Groups(1).Columns(7).Width=   2011
         Groups(1).Columns(7).Visible=   0   'False
         Groups(1).Columns(7).Caption=   "HIDDENDESCR"
         Groups(1).Columns(7).Name=   "HIDDENDESCR"
         Groups(1).Columns(7).DataField=   "Column 17"
         Groups(1).Columns(7).DataType=   8
         Groups(1).Columns(7).FieldLen=   256
         Groups(1).Columns(8).Width=   3281
         Groups(1).Columns(8).Visible=   0   'False
         Groups(1).Columns(8).Caption=   "FECACT"
         Groups(1).Columns(8).Name=   "FECACT"
         Groups(1).Columns(8).DataField=   "Column 18"
         Groups(1).Columns(8).DataType=   8
         Groups(1).Columns(8).FieldLen=   256
         Groups(1).Columns(9).Width=   4551
         Groups(1).Columns(9).Visible=   0   'False
         Groups(1).Columns(9).Caption=   "FORMULA"
         Groups(1).Columns(9).Name=   "FORMULA"
         Groups(1).Columns(9).DataField=   "Column 19"
         Groups(1).Columns(9).DataType=   8
         Groups(1).Columns(9).FieldLen=   256
         Groups(1).Columns(10).Width=   2328
         Groups(1).Columns(10).Visible=   0   'False
         Groups(1).Columns(10).Caption=   "TIPO_PEDIDO"
         Groups(1).Columns(10).Name=   "TIPO_PEDIDO"
         Groups(1).Columns(10).DataField=   "Column 20"
         Groups(1).Columns(10).DataType=   8
         Groups(1).Columns(10).FieldLen=   256
         Groups(1).Columns(11).Width=   2328
         Groups(1).Columns(11).Caption=   "TIPO_PEDIDO_BTN"
         Groups(1).Columns(11).Name=   "TIPO_PEDIDO_BTN"
         Groups(1).Columns(11).Alignment=   2
         Groups(1).Columns(11).CaptionAlignment=   2
         Groups(1).Columns(11).DataField=   "Column 21"
         Groups(1).Columns(11).DataType=   8
         Groups(1).Columns(11).FieldLen=   256
         Groups(1).Columns(11).Style=   4
         Groups(1).Columns(11).ButtonsAlways=   -1  'True
         Groups(1).Columns(11).HasHeadForeColor=   -1  'True
         Groups(1).Columns(11).HasHeadBackColor=   -1  'True
         Groups(1).Columns(11).HeadForeColor=   16777215
         Groups(1).Columns(11).HeadBackColor=   32896
         Groups(1).Columns(11).StyleSet=   "Bold"
         _ExtentX        =   17754
         _ExtentY        =   5292
         _StockProps     =   79
         BackColor       =   -2147483648
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line Line1 
         X1              =   -74880
         X2              =   -64860
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Label lblNotificados 
         Caption         =   "NOTIFICACION DE ERRORES"
         Height          =   255
         Left            =   -74880
         TabIndex        =   10
         Top             =   450
         Width           =   8655
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcERP 
      Height          =   285
      Left            =   780
      TabIndex        =   5
      Top             =   150
      Width           =   2895
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   -2147483640
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   4604
      Columns(0).Caption=   "ERP"
      Columns(0).Name =   "DEN"
      Columns(0).CaptionAlignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "COD"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddSentido 
      Height          =   795
      Left            =   6510
      TabIndex        =   2
      Top             =   0
      Width           =   3705
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCONFIntegracion.frx":173D
      BevelColorFrame =   -2147483638
      BevelColorFace  =   -2147483648
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   10134
      Columns(0).Caption=   "DEN"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).StyleSet=   "Normal"
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).StyleSet=   "Normal"
      _ExtentX        =   6535
      _ExtentY        =   1402
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTraduccion 
      Height          =   795
      Left            =   5850
      TabIndex        =   3
      Top             =   0
      Width           =   3705
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCONFIntegracion.frx":1759
      BevelColorFrame =   -2147483638
      BevelColorFace  =   -2147483648
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   10134
      Columns(0).Caption=   "DEN"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).StyleSet=   "Normal"
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).StyleSet=   "Normal"
      _ExtentX        =   6535
      _ExtentY        =   1402
      _StockProps     =   77
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   465
      Left            =   0
      ScaleHeight     =   465
      ScaleWidth      =   11340
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   6930
      Width           =   11340
      Begin VB.CommandButton cmdEdicion 
         Caption         =   "D&Edici�n"
         Height          =   345
         Left            =   9960
         TabIndex        =   1
         Top             =   0
         Width           =   1005
      End
   End
   Begin VB.Label lblERP 
      Caption         =   "ERP:"
      Height          =   255
      Left            =   150
      TabIndex        =   4
      Top             =   150
      Width           =   975
   End
End
Attribute VB_Name = "frmCONFIntegracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bModoEdicion As Boolean
Private m_bModoEdicionEntidad As Boolean
Private m_bModoEdicionAtributos As Boolean
Private m_bModoEdicionNotificacion As Boolean
Private m_bAnyadiendoNotificado As Boolean
Private m_oIntEntidad As CEntidadInt
Private m_sSistemaExterno As String
Private m_sCodSistemaExterno As String
Private m_sSentido As Integer
Private bError As Boolean
Private bErrorContacto As Boolean
Private bCambio As Boolean
Private bCambioNotificado As Boolean
Public m_oERPsInt As CERPsInt
Private m_oEntidadEnEdicion As CEntidadInt
Private m_oNotificadoEnEdicion As CNotificadoInt
Public m_oEntidadesInt As CEntidadesInt
Public m_oNotificados As CNotificadosInt
Private oIBaseDatos As IBaseDatos
Private bModError As Boolean
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
''' Variables para idiomas
Private m_sEdicion As String
Private m_sConsulta As String
Private m_aEntidad(41) As String
Private m_aTablasExternas() As String
Private m_sFSGS As String
Private m_sDestino(1 To 4) As String
Private m_sTitDestino As String
Private m_sTraduccionExterna As String
Private m_sTraduccionInterna As String
Private m_asAtrib(7) As String
'''Variables para cargar atributos de integracion
Public m_oAtributos As CAtributos
Private m_oAtributo As CAtributo
Private m_bColModificada As Boolean
Public m_oAtributoEnEdicion As CAtributo

'Idiomas
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_asMensajes(1) As String
' Variable para el buscador de atributos
Public g_ofrmATRIB As frmAtrib
Public g_sOrigen As String

''' <summary>
''' Cargar las variables de pantalla multiidioma
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo:0 </remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONF_INTEGRACION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        m_sEdicion = Ador(0).Value
        cmdEdicion.caption = Ador(0).Value
        cmdEdicEntidades.caption = Ador(0).Value
        cmdEdicAtributos.caption = Ador(0).Value
        cmdEdicNotific.caption = Ador(0).Value
        Ador.MoveNext
        m_sConsulta = Ador(0).Value
        Ador.MoveNext
        sdbgIntegracion.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgIntegracion.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        m_sTitDestino = Ador(0).Value
        sdbgIntegracion.Columns(7).caption = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(1) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(2) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(3) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(4) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(5) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(6) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(7) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(8) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(9) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(10) = Ador(0).Value
        Ador.MoveNext
        m_sFSGS = Ador(0).Value
        Ador.MoveNext
        m_sDestino(1) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(11) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(12) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(13) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(14) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(15) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(16) = Ador(0).Value
        Ador.MoveNext
        sdbgIntegracion.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgIntegracion.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(17) = Ador(0).Value
        Ador.MoveNext
        m_sTraduccionExterna = Ador(0).Value
        Ador.MoveNext
        m_sTraduccionInterna = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        sdbgContactos.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgContactos.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        lblNotificados.caption = Ador(0).Value
        Ador.MoveNext
        sdbgIntegracion.Columns(8).caption = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(18) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(23) = Ador(0).Value
        Ador.MoveNext
        m_sDestino(2) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(25) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(26) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(27) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(28) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(29) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(30) = Ador(0).Value
        Ador.MoveNext
        m_sDestino(3) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(31) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(32) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(33) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(34) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(35) = Ador(0).Value
        Ador.MoveNext
        m_aEntidad(36) = Ador(0).Value 'Sociedad
        Ador.MoveNext
        m_aEntidad(37) = Ador(0).Value 'Asignacion de proveedores
        'Nuevos textos introducidos por atributos integracion
        Ador.MoveNext
        Ador.MoveNext
        'Solapas de la tab
        sstabPlantillas.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabPlantillas.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sstabPlantillas.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        'Grupos del grid de atributos
        sdbgAtribPlant.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtribPlant.Groups(1).caption = Ador(0).Value
        Ador.MoveNext
        'Columnas del grid de atributos
        sdbgAtribPlant.Columns(3).caption = Ador(0).Value 'Adjudicacion
        Ador.MoveNext
        sdbgAtribPlant.Columns(4).caption = Ador(0).Value 'Pedido
        Ador.MoveNext
        sdbgAtribPlant.Columns(6).caption = Ador(0).Value 'Codigo
        Ador.MoveNext
        sdbgAtribPlant.Columns(7).caption = Ador(0).Value 'Denominacion
        Ador.MoveNext
        sdbgAtribPlant.Columns(8).caption = Ador(0).Value 'Descr.
        Ador.MoveNext
        sdbgAtribPlant.Columns(9).caption = Ador(0).Value 'Tipo de dato
        Ador.MoveNext
        sdbgAtribPlant.Columns(10).caption = Ador(0).Value 'Seleccion
        Ador.MoveNext
        sdbgAtribPlant.Columns(12).caption = Ador(0).Value 'Ambito de
        Ador.MoveNext
        sdbgAtribPlant.Columns(13).caption = Ador(0).Value 'Obligatorio
        Ador.MoveNext
        sdbgAtribPlant.Columns(14).caption = Ador(0).Value 'Valor por defecto
        Ador.MoveNext
        sdbgAtribPlant.Columns(15).caption = Ador(0).Value 'Validacion ERP
        Ador.MoveNext
        sdbgAtribPlant.Columns(16).caption = Ador(0).Value 'Relacion de atrib
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value 'Si
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value 'No
        Ador.MoveNext
        m_sDestino(4) = Ador(0).Value
        Ador.MoveNext
        Me.sdbgAtribPlant.Columns("TIPO_PEDIDO_BTN").caption = Ador(0).Value
        Ador.MoveNext
        m_asAtrib(0) = Ador(0).Value 'TipoAmbitoProceso.AmbProceso
        Ador.MoveNext
        m_asAtrib(1) = Ador(0).Value 'TipoAmbitoProceso.AmbGrupo
        Ador.MoveNext
        m_asAtrib(2) = Ador(0).Value 'TipoAmbitoProceso.AmbItem
        Ador.MoveNext
        cmdAnyadirAtr.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliAtr.caption = Ador(0).Value
        Ador.MoveNext

        
        m_aEntidad(19) = gParametrosGenerales.gsSingPres1
        m_aEntidad(20) = gParametrosGenerales.gsSingPres2
        m_aEntidad(21) = gParametrosGenerales.gsSingPres3
        m_aEntidad(22) = gParametrosGenerales.gsSingPres4
        
        m_aEntidad(24) = "" 'este es para las tablas externas
        
        m_aEntidad(39) = Ador(0).Value 'Prove Articulo
        Ador.MoveNext
        m_aEntidad(40) = Ador(0).Value 'Prove Portal
        Ador.MoveNext
        m_aEntidad(41) = Ador(0).Value 'Certificado
        Ador.MoveNext
        sdbgAtribPlant.Columns("RECEP").caption = Ador(0).Value 'Recepci�n
        Ador.MoveNext
        m_asAtrib(3) = Ador(0).Value 'N�merico
        Ador.MoveNext
        m_asAtrib(4) = Ador(0).Value 'Fecha
        Ador.MoveNext
        m_asAtrib(5) = Ador(0).Value 'Texto
        Ador.MoveNext
        m_asAtrib(6) = Ador(0).Value 'S�/No
        Ador.MoveNext
        m_asAtrib(7) = Ador(0).Value 'Archivo
        Ador.Close
        
    End If

    Set Ador = Nothing

End Sub

''' <summary>
''' Carga el grid de atributos de integracion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Load del form  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub CargarGridAtributos(ByVal iCodErp As Integer)
Dim oatrib As CAtributo
Dim sTipo As String
Dim sAmbito As String
Dim sLibre As String
Dim sSeleccion As String
Dim sPrecFormula As String
Dim sFormula As String
Dim sBoton As String
Dim sInterno As String

Dim sObl As String
Dim sPedido As String
Dim sRecepcion As String
Dim sAdjudicacion As String
Dim sValidacionERP As String
Dim sTiposPedido As String
Dim oTipoPedido As CTipoPedido
Dim sLinea As String
Dim sValor As String
    sdbgAtribPlant.RemoveAll
    
    
    m_oAtributos.CargarAtributosIntegracionERP (iCodErp)

    
    If m_oAtributos.Count = 0 Then Exit Sub
    
    For Each oatrib In m_oAtributos
        
        Select Case oatrib.Tipo
            Case 1: sTipo = m_asAtrib(5) 'texto
            Case 2: sTipo = m_asAtrib(3) 'numerico
            Case 3: sTipo = m_asAtrib(4) 'fecha
            Case 4: sTipo = m_asAtrib(6) 'si/no
            Case 8: sTipo = m_asAtrib(7) 'Archivo
        End Select
        
        Select Case oatrib.ambito
            Case 1: sAmbito = m_asAtrib(0)
            Case 2: sAmbito = m_asAtrib(1)
            Case 3: sAmbito = m_asAtrib(2)
        End Select
        
        sLibre = 0
        sSeleccion = 0
        sBoton = ""
        If oatrib.TipoIntroduccion = 0 Then
            sLibre = "-1"
            sBoton = ""
        Else
            If oatrib.TipoIntroduccion = 1 And oatrib.Tipo <> TipoBoolean Then
                sSeleccion = "-1"
                sBoton = "..."
            End If
        End If
        
        If oatrib.pedido = True Then
            sPedido = "-1"
        Else
            sPedido = 0
        End If
        If oatrib.Recepcion = True Then
            sRecepcion = "-1"
        Else
            sRecepcion = 0
        End If
        If oatrib.Adjudicacion = True Then
            sAdjudicacion = "-1"
        Else
            sAdjudicacion = 0
        End If
        
        If oatrib.Obligatorio = True Then
            sObl = "-1"
        Else
            sObl = 0
        End If
        
         If oatrib.Validacion_ERP = True Then
            sValidacionERP = "-1"
        Else
            sValidacionERP = 0
        End If
        
        'Cargamos los tipos de pedido del atributo
        oatrib.CargarTiposPedido
        'Los metemos en la variable para asignarselo a la columna TIPO_PEDIDO
        sTiposPedido = ""
        If Not oatrib.TiposPedido Is Nothing Then
            For Each oTipoPedido In oatrib.TiposPedido
                If sTiposPedido = "" Then
                    sTiposPedido = oTipoPedido.Id
                Else
                    sTiposPedido = sTiposPedido & "," & oTipoPedido.Id
                End If
            Next
        End If
        
        sLinea = oatrib.Id & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & sAdjudicacion & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & sRecepcion & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & IIf(IsNull(oatrib.Descripcion), "", "...") & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & sSeleccion & Chr(m_lSeparador) & sBoton & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sObl & Chr(m_lSeparador)
        sValor = ""
        Select Case oatrib.Tipo
            Case 1
                sValor = NullToStr(oatrib.valorText)
            Case 2
                sValor = NullToStr(oatrib.valorNum)
            Case 3
                sValor = NullToStr(oatrib.valorFec)
            Case 4
                If Not IsNull(oatrib.valorBool) Then
                    If oatrib.valorBool Then
                        sValor = m_sIdiTrue
                    Else
                        sValor = m_sIdiFalse
                    End If
                End If
        End Select
        sLinea = sLinea & sValor & Chr(m_lSeparador) & sValidacionERP & Chr(m_lSeparador) & IIf(IsNull(oatrib.Formula), "", "...") & Chr(m_lSeparador) & oatrib.Descripcion & Chr(m_lSeparador) & oatrib.FECACT & Chr(m_lSeparador) & oatrib.Formula & Chr(m_lSeparador) & sTiposPedido & Chr(m_lSeparador) & IIf(sTiposPedido <> "", "...", "")
        sdbgAtribPlant.AddItem sLinea
    Next

    sdbgAtribPlant.Columns("VALORPORDEFECTO").Locked = True
    sdbgAtribPlant.Columns("VALORPORDEFECTO").DropDownHwnd = 0
    sdbgAtribPlant.Columns("VALORPORDEFECTO").Style = ssStyleEdit
    sdbddValor.Enabled = False
    
    sdbgAtribPlant.Update
End Sub

Public Sub CargarListaValores()
Dim oLista As CValorPond
Dim iIndice As Integer
Dim v As Variant

    frmLista.g_sOrigen = "frmCONFIntegracion"

    Set frmLista.g_oAtributo = m_oAtributos.Item(CStr(sdbgAtribPlant.Columns("IDATRIB").Value))
    'No se deja cambiar Libre/Combo. No se deja cambiar lista/lista externa
    frmLista.g_bEdicion = False

    frmLista.sdbgValores.RemoveAll
    
    If Not sdbgAtribPlant.IsAddRow Then
        frmLista.g_oAtributo.CargarListaDeValoresInt
        frmLista.g_iIndiCole = 1
        Set frmLista.g_oPondIntermedia = Nothing
        Set frmLista.g_oPondIntermedia = oFSGSRaiz.Generar_CValoresPond
        For Each oLista In frmLista.g_oAtributo.ListaPonderacion
            frmLista.sdbgValores.AddItem oLista.ValorLista & Chr(m_lSeparador) & frmLista.g_iIndiCole & Chr(m_lSeparador) & oLista.IDAtributo & Chr(m_lSeparador) & oLista.ValorPond & Chr(m_lSeparador) & oLista.FechaActualizacion
            frmLista.g_oPondIntermedia.Add oLista.IDAtributo, , , , , , oLista.ValorLista, , , , oLista.FechaActualizacion, frmLista.g_iIndiCole
            frmLista.g_iIndiCole = frmLista.g_iIndiCole + 1
        Next
    End If
    
    frmLista.optListaExterna.Value = m_oAtributos.Item(sdbgAtribPlant.Columns("IDATRIB").Value).ListaExterna
    
    frmLista.Show 1
        
End Sub

'Comando anyo llama al form Atrib para buscar y seleccionar una atributo para aniadir al grid de atributos integracion
Private Sub cmdAnyadirAtr_Click()
    
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If

    Set g_ofrmATRIB = New frmAtrib
    g_ofrmATRIB.g_sOrigen = "CONFINTEGRACION"
    
    
'    If m_bModifAtrib Then
        Screen.MousePointer = vbHourglass
        MDI.MostrarFormulario g_ofrmATRIB
        Screen.MousePointer = vbNormal
'    End If
    
    Set m_oAtributo = Nothing

End Sub

Private Sub cmdAnyaNotificado_Click()
Dim v As Variant

    If sdbgContactos.DataChanged Then
        v = sdbgContactos.ActiveCell.Value
        If Me.Visible Then sdbgContactos.SetFocus
        sdbgContactos.ActiveCell.Value = v
        bErrorContacto = False
        
        If sdbgContactos.Row = 0 Then   'Es la primera fila o la �nica
        
            If sdbgContactos.Rows = 1 Then
                sdbgContactos.Update
                If bErrorContacto Then
                    Exit Sub
                End If
            Else
                sdbgContactos.MoveNext
                DoEvents
                If bErrorContacto Then
                    Exit Sub
                Else
                    sdbgContactos.MovePrevious
                End If
                
            End If
            
        Else                              'No es la primera fila
            sdbgContactos.MovePrevious
            DoEvents
            If bErrorContacto Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                Exit Sub
            Else
                sdbgContactos.MoveNext
            End If
        End If
    End If
    sdbgContactos.AddNew
    
    sdbgContactos.col = 0

End Sub

Private Sub cmdAnyaNotificadoCopia_Click()
 
Dim v As Variant
 m_bAnyadiendoNotificado = True
 
 If sdbgContactos.DataChanged Then
        v = sdbgContactos.ActiveCell.Value
        If Me.Visible Then sdbgContactos.SetFocus
        sdbgContactos.ActiveCell.Value = v
        bErrorContacto = False
        
        If sdbgContactos.Row = 0 Then   'Es la primera fila o la �nica
        
            If sdbgContactos.Rows = 1 Then
                sdbgContactos.Update
                If bErrorContacto Then
                    Exit Sub
                End If
            Else
                sdbgContactos.MoveNext
                DoEvents
                If bErrorContacto Then
                    Exit Sub
                Else
                    sdbgContactos.MovePrevious
                End If
                
            End If
            
        Else                              'No es la primera fila
            sdbgContactos.MovePrevious
            DoEvents
            If bErrorContacto Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                Exit Sub
            Else
                sdbgContactos.MoveNext
            End If
        End If
    End If
    sdbgContactos.AddNew
    
    sdbgContactos.col = 0
    m_bAnyadiendoNotificado = False
End Sub

Private Sub cmdEdicAtributos_Click()

''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    Dim teserror As TipoErrorSummit
    

    If m_bModoEdicionAtributos Then
    
    sdbgAtribPlant.SelectTypeRow = ssSelectionTypeNone
     
     
     If sdbgAtribPlant.DataChanged = True Then
        
       
        
        v = sdbgAtribPlant.ActiveCell.Value
        If Me.Visible Then sdbgAtribPlant.SetFocus
        sdbgAtribPlant.ActiveCell.Value = v
        bError = False
        
        If sdbgAtribPlant.Row = 0 Then   'Es la primera fila o la �nica
        
            If sdbgAtribPlant.Rows = 1 Then
                sdbgAtribPlant.Update
                If bError Then
                    Exit Sub
                End If
            Else
                sdbgAtribPlant.MoveNext
                DoEvents
                If bError Then
                    Exit Sub
                Else
                    sdbgAtribPlant.MovePrevious
                End If

            End If

        Else                              'No es la primera fila
            sdbgAtribPlant.MovePrevious
            DoEvents
            If bError Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                Exit Sub
            Else
                sdbgAtribPlant.MoveNext
            End If
        End If
        
         sdbgAtribPlant.Update
     
    End If
    
    cmdEdicAtributos.caption = m_sEdicion
    cmdAnyadirAtr.Visible = False
    cmdEliAtr.Visible = False
            
    m_bModoEdicionAtributos = False
    
    sdbgAtribPlant.Columns("ADJUDICACION").Locked = True
    sdbgAtribPlant.Columns("PEDIDO").Locked = True
    sdbgAtribPlant.Columns("RECEP").Locked = True
    sdbgAtribPlant.Columns("DESCRIPCION").Locked = True '????
    sdbgAtribPlant.Columns("SELECCION").Locked = True
    sdbgAtribPlant.Columns("LISTAVALORES").Locked = True '????
    sdbgAtribPlant.Columns("AMBITO").Locked = True
    sdbgAtribPlant.Columns("AMBITO").DropDownHwnd = 0
    sdbgAtribPlant.Columns("OBLIGATORIO").Locked = True
    sdbgAtribPlant.Columns("VALORPORDEFECTO").Locked = True
    sdbgAtribPlant.Columns("VALIDACION").Locked = True
     '3276
    sdbgAtribPlant.Columns("RELACION").Locked = True '???
    'sdbgAtribPlant.Columns("RELACION").Locked = False '???
    
    sdbddAmbito.Enabled = False
    cmdBajar.Visible = False
    cmdSubir.Visible = False
Else

    cmdEdicAtributos.caption = m_sConsulta
    cmdAnyadirAtr.Visible = True
    cmdEliAtr.Visible = True
    
    sdbgAtribPlant.SelectTypeRow = ssSelectionTypeMultiSelectRange
     
    sdbgAtribPlant.Columns("ADJUDICACION").Locked = False
    sdbgAtribPlant.Columns("PEDIDO").Locked = False
    sdbgAtribPlant.Columns("RECEP").Locked = False
    sdbgAtribPlant.Columns("DESCRIPCION").Locked = True '????
    sdbgAtribPlant.Columns("SELECCION").Locked = True
    sdbgAtribPlant.Columns("LISTAVALORES").Locked = True '????
    sdbgAtribPlant.Columns("AMBITO").Locked = False
    sdbgAtribPlant.Columns("AMBITO").DropDownHwnd = sdbddAmbito.hWnd
    sdbgAtribPlant.Columns("OBLIGATORIO").Locked = False
    sdbgAtribPlant.Columns("VALORPORDEFECTO").Locked = False
    sdbgAtribPlant.Columns("VALIDACION").Locked = False
    sdbgAtribPlant.Columns("RELACION").Locked = False '???
    
    sdbddAmbito.Enabled = True
    
    m_bModoEdicionAtributos = True
    cmdBajar.Visible = True
    cmdSubir.Visible = True
End If

If Me.Visible Then sdbgAtribPlant.SetFocus
    

End Sub

Private Sub cmdEdicion_Click()

    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    Dim teserror As TipoErrorSummit
    Dim oEntidadesInt As CEntidadesInt
    Dim oEntidadInt As CEntidadInt
    

    If m_bModoEdicion Then
    

     If sdbgIntegracion.DataChanged = True Then
    
        v = sdbgIntegracion.ActiveCell.Value
        If Me.Visible Then sdbgIntegracion.SetFocus
        sdbgIntegracion.ActiveCell.Value = v
        bError = False
        
        If sdbgIntegracion.Row = 0 Then   'Es la primera fila o la �nica
        
            If sdbgIntegracion.Rows = 1 Then
                sdbgIntegracion.Update
                If bError Then
                    Exit Sub
                End If
            Else
                sdbgIntegracion.MoveNext
                DoEvents
                If bError Then
                    Exit Sub
                Else
                    sdbgIntegracion.MovePrevious
                End If
                
            End If
            
        Else                              'No es la primera fila
            sdbgIntegracion.MovePrevious
            DoEvents
            If bError Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                Exit Sub
            Else
                sdbgIntegracion.MoveNext
            End If
        End If
     Else
        'Si se a�ade una fila, y sin modificar nada se le da al boton de consulta se realizara esta validacion para que no meta un registro en blanco
        If sdbgContactos.IsAddRow Then
            If Trim(sdbgContactos.Columns("PERSONA").Value) = "" Then
                oMensajes.FaltanDatos sdbgContactos.Columns("PERSONA").caption
                If Me.Visible Then sdbgContactos.SetFocus
                bErrorContacto = True
                Exit Sub
            End If
            If Trim(sdbgContactos.Columns("EMAIL").Value) = "" Then
                oMensajes.FaltanDatos sdbgContactos.Columns("EMAIL").caption
                If Me.Visible Then sdbgContactos.SetFocus
                bErrorContacto = True
                Exit Sub
            End If
        End If
    End If
    
    Set oEntidadesInt = Nothing
    Set oEntidadesInt = oFSGSRaiz.Generar_CEntidadesInt

    oEntidadesInt.CargarTodasLasEntidades (m_sCodSistemaExterno)

    For Each oEntidadInt In oEntidadesInt
        If oEntidadInt.activa Then
            ' AHORA UNA ENTIDAD PUEDE ESTAR ACTIVA Y SOLO CON TABLA INTERMEDIA
            'If NullToStr(oEntidadInt.sentido) = "" Then
            '    oMensajes.FaltanDatos sdbgIntegracion.Columns("SENTIDO").Caption
            '    Exit Sub
            'End If
            If oEntidadInt.sentido <> "" Then
                If NullToStr(oEntidadInt.TipoTraduccion) = "" Then
                    oMensajes.FaltanDatos sdbgIntegracion.Columns("TIPO_TRADUCCION").caption
                    Exit Sub
                End If
            End If

            If oEntidadInt.sentido = SentidoIntegracion.EntradaSalida Or oEntidadInt.sentido = SentidoIntegracion.salida And Not oEntidadInt.DestinoTipo = tipodestinointegracion.WCF Then
                If NullToStr(oEntidadInt.DestinoTipo) = "" Or NullToStr(oEntidadInt.Destino) = "" Then
                    If oEntidadInt.Tabla <> EntidadIntegracion.TablasExternas Then
                        oMensajes.FaltanDatos m_aEntidad(oEntidadInt.Tabla) & " : " & m_sTitDestino
                    Else
                        oMensajes.FaltanDatos oEntidadInt.TablaExterna & " : " & m_sTitDestino
                    End If
                    Exit Sub
                End If
            End If
            
            If oEntidadInt.Entidad = EntidadIntegracion.Prove Then
                gParametrosGenerales.gbActivarCodProveErp = oEntidadInt.TablaIntermedia
            End If
        Else
            If oEntidadInt.Entidad = EntidadIntegracion.Prove Then
                gParametrosGenerales.gbActivarCodProveErp = False
            End If
        End If
    Next
    
    If sdbgContactos.DataChanged Then
        v = sdbgContactos.ActiveCell.Value
        If Me.Visible Then sdbgContactos.SetFocus
        sdbgContactos.ActiveCell.Value = v
        bErrorContacto = False
        
        If sdbgContactos.Row = 0 Then   'Es la primera fila o la �nica
        
            If sdbgContactos.Rows = 1 Then
                sdbgContactos.Update
                If bErrorContacto Then
                    Exit Sub
                End If
            Else
                sdbgContactos.MoveNext
                DoEvents
                If bErrorContacto Then
                    Exit Sub
                Else
                    sdbgContactos.MovePrevious
                End If
                
            End If
            
        Else                              'No es la primera fila
            sdbgContactos.MovePrevious
            DoEvents
            If bErrorContacto Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                Exit Sub
            Else
                sdbgContactos.MoveNext
            End If
        End If
    End If
    
    cmdEdicion.caption = m_sEdicion
            
    m_bModoEdicion = False
    sdbgIntegracion.Columns("TABLA_INTERMEDIA").Locked = True
    sdbgIntegracion.Columns("ACTIVA").Locked = True
    sdbgIntegracion.Columns("SENTIDO").Locked = True
    sdbgIntegracion.Columns("TIPO_TRADUCCION").Locked = True
       
    sdbgContactos.Columns("PERSONA").Locked = True
    sdbgContactos.Columns("EMAIL").Locked = True
    'cmdAnyaNotificado.Enabled = False
    'cmdElimNotificado.Enabled = False

    Screen.MousePointer = vbNormal
    ''' Actualizamos las variables que contienen los par�metros de integraci�n
    gParametrosIntegracion = oGestorParametros.DevolverParametrosIntegracion
    Set oEntidadInt = Nothing
    Set oEntidadesInt = Nothing
    
Else

    cmdEdicion.caption = m_sConsulta
    sdbgIntegracion.Columns("TABLA_INTERMEDIA").Locked = False
    sdbgIntegracion.Columns("ACTIVA").Locked = False
    sdbgIntegracion.Columns("SENTIDO").Locked = False
    sdbgIntegracion.Columns("TIPO_TRADUCCION").Locked = False
     
    
    sdbgContactos.Columns("PERSONA").Locked = False
    sdbgContactos.Columns("EMAIL").Locked = False
    'cmdAnyaNotificado.Enabled = True
    'cmdElimNotificado.Enabled = True
    
    m_bModoEdicion = True
    
End If

If Me.Visible Then sdbgIntegracion.SetFocus
    
End Sub

Private Sub cmdEdicNotific_Click()


    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    Dim teserror As TipoErrorSummit
    Dim oEntidadesInt As CEntidadesInt
    Dim oEntidadInt As CEntidadInt
    

    If m_bModoEdicionNotificacion Then
    
    If sdbgContactos.DataChanged Then
        v = sdbgContactos.ActiveCell.Value
        If Me.Visible Then sdbgContactos.SetFocus
        sdbgContactos.ActiveCell.Value = v
        bErrorContacto = False
        
        If sdbgContactos.Row = 0 Then   'Es la primera fila o la �nica
        
            If sdbgContactos.Rows = 1 Then
                sdbgContactos.Update
                If bErrorContacto Then
                    Exit Sub
                End If
            Else
                sdbgContactos.MoveNext
                DoEvents
                If bErrorContacto Then
                    Exit Sub
                Else
                    sdbgContactos.MovePrevious
                End If
                
            End If
            
        Else                              'No es la primera fila
            sdbgContactos.MovePrevious
            DoEvents
            If bErrorContacto Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                Exit Sub
            Else
                sdbgContactos.MoveNext
            End If
        End If

    End If
    
    cmdEdicNotific.caption = m_sEdicion
            
    m_bModoEdicionNotificacion = False
    
       
    sdbgContactos.Columns("PERSONA").Locked = True
    sdbgContactos.Columns("EMAIL").Locked = True
    cmdAnyaNotificadoCopia.Enabled = False
    cmdElimNotificadoCopia.Enabled = False

    Screen.MousePointer = vbNormal
    
Else

    cmdEdicNotific.caption = m_sConsulta
       
    sdbgContactos.Columns("PERSONA").Locked = False
    sdbgContactos.Columns("EMAIL").Locked = False
    cmdAnyaNotificadoCopia.Enabled = True
    cmdElimNotificadoCopia.Enabled = True
    
    m_bModoEdicionNotificacion = True
    
End If

If Me.Visible Then sdbgContactos.SetFocus

End Sub

Private Sub cmdEliAtr_Click()

Dim aIdentificadores As Variant
Dim aIdentEliminar As Variant
Dim aBookmarks As Variant
Dim aAux As Variant
Dim iIndice As Integer
Dim inum As Integer
Dim i As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit
  
    If sdbgAtribPlant.Rows = 0 Then Exit Sub
    
    If sdbgAtribPlant.Columns("ID").Text = "" Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    sdbgAtribPlant.GetBookmark sdbgAtribPlant.Row
    
    sdbgAtribPlant.SelBookmarks.Add sdbgAtribPlant.Bookmark
        
    If sdbgAtribPlant.SelBookmarks.Count > 0 Then
    
        Select Case sdbgAtribPlant.SelBookmarks.Count
            Case 0
                Screen.MousePointer = vbNormal
                Exit Sub
            Case 1
                irespuesta = oMensajes.PreguntaEliminar(m_asMensajes(0) & " " & sdbgAtribPlant.Columns("CODIGO").Text & " (" & sdbgAtribPlant.Columns("DENOMINACION").Text & ")")
            Case Is > 1
                irespuesta = oMensajes.PreguntaEliminarAtributosPlant(m_asMensajes(1))
        End Select
          
        If irespuesta = vbNo Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        
        ReDim aIdentificadores(sdbgAtribPlant.SelBookmarks.Count)
        ReDim aIdentEliminar(sdbgAtribPlant.SelBookmarks.Count)
        ReDim aBookmarks(sdbgAtribPlant.SelBookmarks.Count)
            
        i = 0
        While i < sdbgAtribPlant.SelBookmarks.Count
            aIdentificadores(i + 1) = sdbgAtribPlant.Columns("ID").Text
            aIdentEliminar(i + 1) = sdbgAtribPlant.Columns("IDATRIB").Text
            aBookmarks(i + 1) = sdbgAtribPlant.Columns("ID").Text
            sdbgAtribPlant.Bookmark = sdbgAtribPlant.SelBookmarks(i)
            i = i + 1
        Wend
        
        udtTeserror = m_oAtributos.EliminarAtributosIntegracion(aIdentificadores)
            If udtTeserror.NumError <> TESnoerror Then
              If udtTeserror.NumError = TESImposibleEliminar Then
                  iIndice = 1
                  aAux = udtTeserror.Arg1
                  inum = UBound(udtTeserror.Arg1, 2)
                  
                  For i = 0 To inum
                      sdbgAtribPlant.SelBookmarks.Remove (aAux(2, i) - iIndice)
                      iIndice = iIndice + 1
                  Next
                    
                  oMensajes.ImposibleEliminacionMultiple 345, udtTeserror.Arg1 'todos los atributos cambiar el 345
                  If sdbgAtribPlant.SelBookmarks.Count > 0 Then
                      sdbgAtribPlant.DeleteSelected
                      sdbgAtribPlant.ReBind
                  End If
              Else
                  TratarError udtTeserror
                  Screen.MousePointer = vbNormal
                  Exit Sub
              End If
            Else
'                For i = 1 To sdbgAtribPlant.SelBookmarks.Count
'                    If Not m_oAtributo Is Nothing Then
'                        If CStr(m_oAtributo.Id) = sdbgAtribPlant.Columns("IDATRIB").Text Then
'                            Set m_oAtributo = Nothing
'                        End If
'                    End If
'                    RegistrarAccion ACCPlantAtribEspEli, "Atributo eliminado: " & aBookmarks(i) & " Grupo " & g_sCodGRPEsp
'                    g_oPlantillaSeleccionada.AtributosEspecificacion.Remove CStr((aBookmarks(i)))
'                Next
                sdbgAtribPlant.DeleteSelected
                sdbgAtribPlant.ReBind
                DoEvents
            End If
    End If
     
     'Queremos eliminar los atributos borrados en BD de la coleccion de atributos de m_oAtributos
     For i = 1 To UBound(aIdentEliminar)
          m_oAtributos.Remove CStr((aIdentEliminar(i)))
     Next i
    
    sdbgAtribPlant.SelBookmarks.RemoveAll
    
    If sdbgAtribPlant.Row = "-1" Then
        sdbgAtribPlant.RemoveAll
    End If
    
    Screen.MousePointer = vbNormal


End Sub

Private Sub cmdElimNotificado_Click()
Dim lID As Long
Dim i As Integer
Dim vbook, v As Variant
Dim teserror As TipoErrorSummit

    If sdbgContactos.DataChanged Then sdbgContactos.Update

    If sdbgContactos.SelBookmarks.Count = 0 Then Exit Sub

    For i = 0 To sdbgContactos.SelBookmarks.Count - 1
        vbook = sdbgContactos.SelBookmarks(i)
        
        Set m_oNotificadoEnEdicion = oFSGSRaiz.Generar_CNotificadoInt
            
        If sdbgContactos.Columns("ID").Value = "" Then
        Else
            m_oNotificadoEnEdicion.Id = sdbgContactos.Columns("ID").Value
                
            Set oIBaseDatos = m_oNotificadoEnEdicion
            teserror = oIBaseDatos.EliminarDeBaseDatos
               
            If teserror.NumError <> TESnoerror Then
                v = sdbgContactos.ActiveCell.Value
                TratarError teserror
                If Me.Visible Then sdbgContactos.SetFocus
                bModError = True
                sdbgContactos.ActiveCell.Value = v
                sdbgContactos.DataChanged = False
            Else
                Set oIBaseDatos = Nothing
                Set m_oNotificadoEnEdicion = Nothing
            End If
        End If
    Next
    sdbgContactos.DeleteSelected
    
End Sub

Private Sub cmdElimNotificadoCopia_Click()

Dim lID As Long
Dim i As Integer
Dim vbook, v As Variant
Dim teserror As TipoErrorSummit

    'If sdbgContactos.DataChanged Then sdbgContactos.Update

    If sdbgContactos.SelBookmarks.Count = 0 Then Exit Sub

    For i = 0 To sdbgContactos.SelBookmarks.Count - 1
        vbook = sdbgContactos.SelBookmarks(i)
        
        Set m_oNotificadoEnEdicion = oFSGSRaiz.Generar_CNotificadoInt
            
        If sdbgContactos.Columns("ID").Value = "" Then
        Else
            m_oNotificadoEnEdicion.Id = sdbgContactos.Columns("ID").Value
                
            Set oIBaseDatos = m_oNotificadoEnEdicion
            teserror = oIBaseDatos.EliminarDeBaseDatos
               
            If teserror.NumError <> TESnoerror Then
                v = sdbgContactos.ActiveCell.Value
                TratarError teserror
                If Me.Visible Then sdbgContactos.SetFocus
                bModError = True
                sdbgContactos.ActiveCell.Value = v
                sdbgContactos.DataChanged = False
            Else
                Set oIBaseDatos = Nothing
                Set m_oNotificadoEnEdicion = Nothing
            End If
        End If
    Next
    sdbgContactos.DeleteSelected
    sdbgContactos.ReBind
    DoEvents
End Sub

Private Sub cmdEdicEntidades_Click()

''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    Dim teserror As TipoErrorSummit
    Dim oEntidadesInt As CEntidadesInt
    Dim oEntidadInt As CEntidadInt
    

    If m_bModoEdicionEntidad Then
    

     If sdbgIntegracion.DataChanged = True Then
    
        v = sdbgIntegracion.ActiveCell.Value
        If Me.Visible Then sdbgIntegracion.SetFocus
        sdbgIntegracion.ActiveCell.Value = v
        bError = False
        
        If sdbgIntegracion.Row = 0 Then   'Es la primera fila o la �nica
        
            If sdbgIntegracion.Rows = 1 Then
                sdbgIntegracion.Update
                If bError Then
                    Exit Sub
                End If
            Else
                sdbgIntegracion.MoveNext
                DoEvents
                If bError Then
                    Exit Sub
                Else
                    sdbgIntegracion.MovePrevious
                End If
                
            End If
            
        Else                              'No es la primera fila
            sdbgIntegracion.MovePrevious
            DoEvents
            If bError Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                Exit Sub
            Else
                sdbgIntegracion.MoveNext
            End If
        End If
     
    End If
    
    Set oEntidadesInt = Nothing
    Set oEntidadesInt = oFSGSRaiz.Generar_CEntidadesInt

    oEntidadesInt.CargarTodasLasEntidades (m_sCodSistemaExterno)

    For Each oEntidadInt In oEntidadesInt
        If oEntidadInt.activa Then
            ' AHORA UNA ENTIDAD PUEDE ESTAR ACTIVA Y SOLO CON TABLA INTERMEDIA
            'If NullToStr(oEntidadInt.sentido) = "" Then
            '    oMensajes.FaltanDatos sdbgIntegracion.Columns("SENTIDO").Caption
            '    Exit Sub
            'End If
            If oEntidadInt.sentido <> "" Then
                If NullToStr(oEntidadInt.TipoTraduccion) = "" Then
                    oMensajes.FaltanDatos sdbgIntegracion.Columns("TIPO_TRADUCCION").caption
                    Exit Sub
                End If
            End If

            If oEntidadInt.sentido = SentidoIntegracion.EntradaSalida Or oEntidadInt.sentido = SentidoIntegracion.salida Then
                If NullToStr(oEntidadInt.DestinoTipo) = "" Or NullToStr(oEntidadInt.Destino) = "" Then
                    If oEntidadInt.Tabla <> EntidadIntegracion.TablasExternas Then
                        oMensajes.FaltanDatos m_aEntidad(oEntidadInt.Tabla) & " : " & m_sTitDestino
                    Else
                        oMensajes.FaltanDatos oEntidadInt.TablaExterna & " : " & m_sTitDestino
                    End If
                    Exit Sub
                End If
            End If
            
            If oEntidadInt.Entidad = EntidadIntegracion.Prove Then
                gParametrosGenerales.gbActivarCodProveErp = oEntidadInt.TablaIntermedia
            End If
        Else
            If oEntidadInt.Entidad = EntidadIntegracion.Prove Then
                gParametrosGenerales.gbActivarCodProveErp = False
            End If
        End If
    Next
    
    
    cmdEdicEntidades.caption = m_sEdicion
            
    m_bModoEdicionEntidad = False
    sdbgIntegracion.Columns("TABLA_INTERMEDIA").Locked = True
    sdbgIntegracion.Columns("ACTIVA").Locked = True
    sdbgIntegracion.Columns("SENTIDO").Locked = True
    sdbgIntegracion.Columns("TIPO_TRADUCCION").Locked = True
       
    Screen.MousePointer = vbNormal
    ''' Actualizamos las variables que contienen los par�metros de integraci�n
    gParametrosIntegracion = oGestorParametros.DevolverParametrosIntegracion
    Set oEntidadInt = Nothing
    Set oEntidadesInt = Nothing
    
Else

    cmdEdicEntidades.caption = m_sConsulta
    sdbgIntegracion.Columns("TABLA_INTERMEDIA").Locked = False
    sdbgIntegracion.Columns("ACTIVA").Locked = False
    sdbgIntegracion.Columns("SENTIDO").Locked = False
    sdbgIntegracion.Columns("TIPO_TRADUCCION").Locked = False
    
    m_bModoEdicionEntidad = True
    
End If

If Me.Visible Then sdbgIntegracion.SetFocus

End Sub

Private Sub Form_Load()
Dim oERPInt As CERPInt
Dim oEntidadInt As CEntidadInt
Dim oNotificadoInt As CNotificadoInt
Dim sSentido As String
Dim sTextoTipoTraduccion As String
Dim bsactiva, bsTablaInter As String

    Me.Height = 7220
    Me.Width = 11175
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    ' Para que quede centrada en la MDI
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    ' Cargar los captions dependiendo del idioma
    CargarRecursos
    
    PonerFieldSeparator Me
    
    
    Set m_oERPsInt = Nothing
    Set m_oERPsInt = oFSGSRaiz.generar_CERPsInt
    
    Set m_oEntidadesInt = Nothing
    Set m_oEntidadesInt = oFSGSRaiz.Generar_CEntidadesInt
      
    Set m_oNotificados = Nothing
    Set m_oNotificados = oFSGSRaiz.Generar_CNotificadosInt
    
    Set m_oAtributos = Nothing
    Set m_oAtributos = oFSGSRaiz.Generar_CAtributos
      
    Show
    DoEvents
    m_bModoEdicion = False
    m_bModoEdicionEntidad = False
    m_bModoEdicionAtributos = False
    m_bModoEdicionNotificacion = False
     
    ''Carga el listado de ERP:
    m_oERPsInt.CargarTodosLosERPs
    sdbcERP.RemoveAll
    
    For Each oERPInt In m_oERPsInt
        sdbcERP.AddItem CStr(oERPInt.Den) & Chr(m_lSeparador) & CStr(oERPInt.Cod)
    Next
    ''Si s�lo existe un ERP, no se visualiza el combo ni el texto:
    If sdbcERP.Rows = 1 Then
        sdbcERP.Visible = False
        lblERP.Visible = False
    End If
    ''ERP visible: El primero
    m_sCodSistemaExterno = sdbcERP.Columns(1).Value
    m_sSistemaExterno = sdbcERP.Columns(0).Value
    
    sdbcERP.Text = m_sSistemaExterno
    
    ''Carga las entidades y notificados para el primer ERP:
    m_oEntidadesInt.CargarTodasLasEntidades (m_sCodSistemaExterno)
    m_oNotificados.CargarTodasLosNotificados (m_sCodSistemaExterno)
    CargarGridAtributos (m_sCodSistemaExterno)
    
    sdbgAtribPlant.AllowUpdate = True
    
    sdbddValor.AddItem ""
    'sdbgAtribPlant.Columns("VALORPORDEFECTO").DropDownHwnd = sdbddValor.hwnd
    sdbddValor.Enabled = False
    
    sdbddAmbito.AddItem ""
    sdbgAtribPlant.Columns("AMBITO").DropDownHwnd = 0
    sdbddAmbito.Enabled = False
    
    sdbgAtribPlant.Columns("ADJUDICACION").Locked = True
    sdbgAtribPlant.Columns("PEDIDO").Locked = True
    sdbgAtribPlant.Columns("RECEP").Locked = True
    sdbgAtribPlant.Columns("CODIGO").Locked = True 'Siempre bloqueado
    sdbgAtribPlant.Columns("DENOMINACION").Locked = True 'Siempre bloqueado
    sdbgAtribPlant.Columns("DESCRIPCION").Locked = True
    sdbgAtribPlant.Columns("TIPODEDATO").Locked = True 'Siempre bloqueado
    sdbgAtribPlant.Columns("SELECCION").Locked = True
    sdbgAtribPlant.Columns("LISTAVALORES").Locked = True
    sdbgAtribPlant.Columns("AMBITO").Locked = True
    sdbgAtribPlant.Columns("OBLIGATORIO").Locked = True
    sdbgAtribPlant.Columns("VALORPORDEFECTO").Locked = True
    sdbgAtribPlant.Columns("VALIDACION").Locked = True
    sdbgAtribPlant.Columns("RELACION").Locked = True
    
    ''''''m_sSistemaExterno = m_oEntidadesInt.DevolverNombreSistemaExterno
    
    sdbgIntegracion.RemoveAll
        
    For Each oEntidadInt In m_oEntidadesInt
        If FSEPConf And (oEntidadInt.Entidad = EntidadIntegracion.Adj Or _
            oEntidadInt.Entidad = EntidadIntegracion.PED_directo Or _
            oEntidadInt.Entidad = EntidadIntegracion.Rec_Directo) Or _
            (oEntidadInt.Entidad = 19 And Not gParametrosGenerales.gbUsarPres1) Or _
            (oEntidadInt.Entidad = 20 And Not gParametrosGenerales.gbUsarPres2) Or _
            (oEntidadInt.Entidad = 21 And Not gParametrosGenerales.gbUsarPres3) Or _
            (oEntidadInt.Entidad = 22 And Not gParametrosGenerales.gbUsarPres4) Then ' en el FSEP no hay adjudicaciones ni ped. directos
        Else
            If oEntidadInt.sentido = SentidoIntegracion.Entrada Then
                sSentido = m_sFSGS & " <- " & m_sSistemaExterno
            Else
                If oEntidadInt.sentido = SentidoIntegracion.EntradaSalida Then
                    sSentido = m_sFSGS & " <-> " & m_sSistemaExterno
                Else
                    If oEntidadInt.sentido = SentidoIntegracion.salida Then
                        sSentido = m_sFSGS & " -> " & m_sSistemaExterno
                    Else
                        sSentido = ""
                    End If
                End If
            End If
            Select Case oEntidadInt.TipoTraduccion
                Case TipoTraduccion.externo: sTextoTipoTraduccion = m_sTraduccionExterna
                Case TipoTraduccion.interno: sTextoTipoTraduccion = m_sTraduccionInterna
                Case Else: sTextoTipoTraduccion = NullToStr(oEntidadInt.TipoTraduccion)
            End Select

                bsactiva = CStr(BooleanToSQLBinary(oEntidadInt.activa))
                bsTablaInter = CStr(BooleanToSQLBinary(oEntidadInt.TablaIntermedia))
                
                If oEntidadInt.Tabla <> EntidadIntegracion.TablasExternas Then
                    sdbgIntegracion.AddItem CStr(oEntidadInt.Tabla) & Chr(m_lSeparador) & bsactiva & Chr(m_lSeparador) & m_aEntidad(oEntidadInt.Tabla) & Chr(m_lSeparador) & sSentido & Chr(m_lSeparador) & sTextoTipoTraduccion & Chr(m_lSeparador) & NullToStr(oEntidadInt.sentido) & Chr(m_lSeparador) & bsTablaInter & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oEntidadInt.TipoTraduccion) & Chr(m_lSeparador) & CStr(oEntidadInt.Entidad) & "_" & CStr(oEntidadInt.Tabla) & "_" & NullToStr(oEntidadInt.IdTablaExterna)
                Else
                    sdbgIntegracion.AddItem CStr(oEntidadInt.Tabla) & Chr(m_lSeparador) & bsactiva & Chr(m_lSeparador) & oEntidadInt.TablaExterna & Chr(m_lSeparador) & sSentido & Chr(m_lSeparador) & sTextoTipoTraduccion & Chr(m_lSeparador) & NullToStr(oEntidadInt.sentido) & Chr(m_lSeparador) & bsTablaInter & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oEntidadInt.TipoTraduccion) & Chr(m_lSeparador) & CStr(oEntidadInt.Entidad) & "_" & CStr(oEntidadInt.Tabla) & "_" & NullToStr(oEntidadInt.IdTablaExterna)
                End If
                
        End If
    Next
        
    sdbddSentido.AddItem ""
    sdbgIntegracion.Columns(3).DropDownHwnd = sdbddSentido.hWnd
    
    sdbddTraduccion.AddItem ""
    sdbgIntegracion.Columns(4).DropDownHwnd = sdbddTraduccion.hWnd
    
'     la grid est� en MODO ALLOW UPDATE
     sdbgIntegracion.Columns("ACTIVA").Locked = True
     sdbgIntegracion.Columns("ENTIDAD").Locked = True
     sdbgIntegracion.Columns("TABLA_INTERMEDIA").Locked = True
     sdbgIntegracion.Columns("SENTIDO").Locked = True
     sdbgIntegracion.Columns("TIPO_TRADUCCION").Locked = True
     cmdAnyaNotificadoCopia.Enabled = False
     cmdElimNotificadoCopia.Enabled = False
     
     
     sdbgContactos.RemoveAll
     sdbgContactos.Columns("PERSONA").Locked = True
     sdbgContactos.Columns("EMAIL").Locked = True
     For Each oNotificadoInt In m_oNotificados
        sdbgContactos.AddItem NullToStr(oNotificadoInt.nombre) & Chr(m_lSeparador) & NullToStr(oNotificadoInt.Email) & Chr(m_lSeparador) & oNotificadoInt.Id
     Next
        
    Screen.MousePointer = vbNormal
    
    cmdAnyadirAtr.Visible = False
    cmdEliAtr.Visible = False
    cmdEdicion.Visible = False
    
    m_bModoEdicion = False
    m_bModoEdicionEntidad = False
    m_bModoEdicionAtributos = False
    m_bModoEdicionNotificacion = False
      
    Set oEntidadInt = Nothing
    
    If cmdSubir.Width + cmdSubir.Left + 50 < 500 Then
        sdbgAtribPlant.Left = 600
    Else
        sdbgAtribPlant.Left = cmdSubir.Width + cmdSubir.Left + 50
    End If
    

End Sub

Private Sub Form_Resize()

    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 1500 Then Exit Sub
    If Me.Height < 10875 Then
        If Me.Height - 2000 > 0 Then
            sstabPlantillas.Height = Me.Height - 2000
        End If
     sstabPlantillas.Width = Me.Width - 450
    
'        sdbgIntegracion.Height = Me.Height - 3400  ''2800
'        sdbgIntegracion.Width = Me.Width - 250
          ' - 1800  ''2800
        sdbgIntegracion.Width = sstabPlantillas.Width - 450
    Else
     sstabPlantillas.Height = Me.Height - 2000
     sstabPlantillas.Width = Me.Width - 450
    
'        sdbgIntegracion.Height = Me.Height - 5400  ''4800
'        sdbgIntegracion.Width = Me.Width - 250
        sdbgIntegracion.Height = sstabPlantillas.Height - 1300  ''2800
        sdbgIntegracion.Width = sstabPlantillas.Width - 350
    End If
    If sstabPlantillas.Height - 1000 > 0 Then
        sdbgIntegracion.Height = sstabPlantillas.Height - 1000
    End If
    sdbgIntegracion.Columns("ACTIVA").Width = sdbgIntegracion.Width * 0.05
    sdbgIntegracion.Columns("ENTIDAD").Width = sdbgIntegracion.Width * 0.2
    sdbgIntegracion.Columns("SENTIDO").Width = sdbgIntegracion.Width * 0.2
    sdbgIntegracion.Columns("TIPO_TRADUCCION").Width = sdbgIntegracion.Width * 0.22
    sdbgIntegracion.Columns("TABLA_INTERMEDIA").Width = sdbgIntegracion.Width * 0.14
    sdbgIntegracion.Columns("DEST").Width = sdbgIntegracion.Width * 0.09
    sdbgIntegracion.Columns("ORIGEN").Width = sdbgIntegracion.Width * 0.07
    
    
''''    lblERP.Top = sdbgIntegracion.Height + 200
''''    sdbcERP.Top = sdbgIntegracion.Height + 200
    

    lblNotificados.Width = sstabPlantillas.Width - 200
    
   
    sdbgContactos.Width = sstabPlantillas.Width - 800
    sdbgContactos.Height = 1800
    If cmdSubir.Width + cmdSubir.Left + 50 < 500 Then
        sdbgAtribPlant.Left = 500
    Else
        sdbgAtribPlant.Left = cmdSubir.Width + cmdSubir.Left + 50
    End If
    sdbgAtribPlant.Width = sdbgIntegracion.Width - sdbgAtribPlant.Left
    sdbgAtribPlant.Height = sdbgIntegracion.Height
    sdbgAtribPlant.Top = sdbgIntegracion.Top
    cmdSubir.Top = sdbgAtribPlant.Top
    'Nueva linea Nsa
    Line1.Y1 = lblNotificados.Top + 200
    Line1.Y2 = lblNotificados.Top + 200
    Line1.X1 = lblNotificados.Left
    Line1.X2 = lblNotificados.Left + sdbgContactos.Width
    
    sdbgContactos.Columns(0).Width = sdbgContactos.Width * 0.5
    sdbgContactos.Columns(1).Width = sdbgContactos.Width * 0.4
    

    Me.Picture1.Left = sdbgContactos.Left + sdbgContactos.Width + 50
    Me.Picture1.Top = sdbgContactos.Top
    
    sdbddSentido.Width = sdbgIntegracion.Columns("SENTIDO").Width
    sdbddSentido.Columns("DEN").Width = sdbgIntegracion.Columns("SENTIDO").Width
    
    sdbddTraduccion.Width = sdbgIntegracion.Columns("TIPO_TRADUCCION").Width
    sdbddTraduccion.Columns("DEN").Width = sdbgIntegracion.Columns("TIPO_TRADUCCION").Width
    
    
    cmdEdicEntidades.Left = sdbgIntegracion.Left
    cmdEdicEntidades.Top = sstabPlantillas.Top + sstabPlantillas.Height - 1400
    cmdEdicAtributos.Left = sdbgAtribPlant.Left
    cmdEdicAtributos.Top = sstabPlantillas.Top + sstabPlantillas.Height - 1400
    cmdEdicNotific.Left = sdbgContactos.Left
    cmdEdicNotific.Top = sstabPlantillas.Top + sstabPlantillas.Height - 1400
    cmdAnyadirAtr.Top = cmdEdicAtributos.Top
    cmdAnyadirAtr.Left = cmdEdicAtributos.Left + (cmdAnyadirAtr.Width + 20)
    cmdEliAtr.Top = cmdEdicAtributos.Top
    cmdEliAtr.Left = cmdAnyadirAtr.Left + (cmdEliAtr.Width + 20)
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim oEntidadesInt As CEntidadesInt
Dim oEntidadInt As CEntidadInt

    If sdbgIntegracion.DataChanged Then
        sdbgIntegracion.Update
        If bError Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    Set oEntidadesInt = Nothing
    Set oEntidadesInt = oFSGSRaiz.Generar_CEntidadesInt

    oEntidadesInt.CargarTodasLasEntidades (m_sCodSistemaExterno)
    For Each oEntidadInt In oEntidadesInt
        If oEntidadInt.activa Then
            If NullToStr(oEntidadInt.sentido) = "" And oEntidadInt.TablaIntermedia = 0 Then
                    oMensajes.FaltanDatos sdbgIntegracion.Columns("SENTIDO").caption & " or " & sdbgIntegracion.Columns("TABLA_INTERMEDIA").caption
                    Cancel = True
                    Exit Sub
            End If
            If oEntidadInt.sentido = SentidoIntegracion.EntradaSalida Or oEntidadInt.sentido = SentidoIntegracion.salida Then
                If NullToStr(oEntidadInt.DestinoTipo) = "" Or (NullToStr(oEntidadInt.Destino) = "" And Not oEntidadInt.DestinoTipo = tipodestinointegracion.WCF) Then
                                        
                    If oEntidadInt.Tabla <> EntidadIntegracion.TablasExternas Then
                        oMensajes.FaltanDatos m_aEntidad(oEntidadInt.Tabla) & " : " & m_sTitDestino
                    Else
                        oMensajes.FaltanDatos oEntidadInt.TablaExterna & " : " & m_sTitDestino
                    End If
                    
                    Cancel = True
                    Exit Sub
                End If
            End If
        End If
    Next

    Set m_oEntidadesInt = Nothing
    Set m_oEntidadEnEdicion = Nothing
    Set m_oNotificados = Nothing
    Set m_oAtributos = Nothing
    Set m_oAtributo = Nothing
    
    gParametrosIntegracion = oGestorParametros.DevolverParametrosIntegracion
    Me.Visible = False
End Sub

Private Sub sdbcERP_Change()
    If Not bRespetarCombo Then
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcERP_CloseUp()
    Dim oEntidadesInt As CEntidadesInt
    Dim oEntidadInt As CEntidadInt
    Dim oNotificadoInt As CNotificadoInt
    Dim sSentido As String
    Dim sTextoTipoTraduccion As String
    Dim bsactiva, bsTablaInter As String
    Dim v As Variant
    Dim teserror As TipoErrorSummit
    
    ''En primer lugar guarda los cambios (si hay)
    'If m_bModoEdicion Then
    If m_bModoEdicionEntidad Or m_bModoEdicionNotificacion Then
        ''Cambios en las entidades integradas:
        If sdbgIntegracion.DataChanged = True Then
            v = sdbgIntegracion.ActiveCell.Value
            If Me.Visible Then sdbgIntegracion.SetFocus
            sdbgIntegracion.ActiveCell.Value = v
            bError = False
            
            If sdbgIntegracion.Row = 0 Then   'Es la primera fila o la �nica
                If sdbgIntegracion.Rows = 1 Then
                    sdbgIntegracion.Update
                    If bError Then
                       Exit Sub
                    End If
                Else
                    sdbgIntegracion.MoveNext
                    DoEvents
                    If bError Then
                        Exit Sub
                    Else
                        sdbgIntegracion.MovePrevious
                    End If
                End If
            Else                              'No es la primera fila
                sdbgIntegracion.MovePrevious
                DoEvents
                If bError Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                    Exit Sub
                Else
                    sdbgIntegracion.MoveNext
                End If
            End If
        End If
        ''Se comprueba si faltan datos obligatorios antes de cambiar de ERP
        Set oEntidadesInt = Nothing
        Set oEntidadesInt = oFSGSRaiz.Generar_CEntidadesInt
        oEntidadesInt.CargarTodasLasEntidades (m_sCodSistemaExterno)
        For Each oEntidadInt In oEntidadesInt
            If oEntidadInt.activa Then
                If oEntidadInt.sentido <> "" Then
                    If NullToStr(oEntidadInt.TipoTraduccion) = "" Then
                        oMensajes.FaltanDatos sdbgIntegracion.Columns("TIPO_TRADUCCION").caption
                        Exit Sub
                    End If
                End If
                If oEntidadInt.sentido = SentidoIntegracion.EntradaSalida Or oEntidadInt.sentido = SentidoIntegracion.salida Then
                    If NullToStr(oEntidadInt.DestinoTipo) = "" Or NullToStr(oEntidadInt.Destino) = "" Then
                        
                        If oEntidadInt.Tabla <> EntidadIntegracion.TablasExternas Then
                            oMensajes.FaltanDatos m_aEntidad(oEntidadInt.Tabla) & " : " & m_sTitDestino
                        Else
                            oMensajes.FaltanDatos oEntidadInt.TablaExterna & " : " & m_sTitDestino
                        End If
                        
                        Exit Sub
                    End If
                End If
            End If
        Next
        ''Cambios en los notificados:
        If sdbgContactos.DataChanged Then
            v = sdbgContactos.ActiveCell.Value
            If Me.Visible Then sdbgContactos.SetFocus
            sdbgContactos.ActiveCell.Value = v
            bErrorContacto = False
            
            If sdbgContactos.Row = 0 Then   'Es la primera fila o la �nica
                If sdbgContactos.Rows = 1 Then
                    sdbgContactos.Update
                    If bErrorContacto Then
                        Exit Sub
                    End If
                Else
                    sdbgContactos.MoveNext
                    DoEvents
                    If bErrorContacto Then
                        Exit Sub
                    Else
                        sdbgContactos.MovePrevious
                    End If
                End If
            Else                              'No es la primera fila
                sdbgContactos.MovePrevious
                DoEvents
                If bErrorContacto Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                    Exit Sub
                Else
                    sdbgContactos.MoveNext
                End If
            End If
        End If

        Screen.MousePointer = vbNormal
        ''' Actualizamos las variables que contienen los par�metros de integraci�n
        gParametrosIntegracion = oGestorParametros.DevolverParametrosIntegracion

    End If
    
    ''Actualiza los valores del ERP seleccionado:
    m_sCodSistemaExterno = sdbcERP.Columns(1).Value
    m_sSistemaExterno = sdbcERP.Columns(0).Value
    sdbcERP.Text = m_sSistemaExterno
    
    ''Carga los notificados para el nuevo ERP:
    Set m_oNotificados = Nothing
    Set m_oNotificados = oFSGSRaiz.Generar_CNotificadosInt
    m_oNotificados.CargarTodasLosNotificados (m_sCodSistemaExterno)
    sdbgContactos.RemoveAll
    For Each oNotificadoInt In m_oNotificados
       sdbgContactos.AddItem NullToStr(oNotificadoInt.nombre) & Chr(m_lSeparador) & NullToStr(oNotificadoInt.Email) & Chr(m_lSeparador) & oNotificadoInt.Id
    Next
    
    ''Carga las entidades para el nuevo ERP:
'    Set m_oEntidadesInt = Nothing
'    Set m_oEntidadesInt = oFSGSRaiz.Generar_CEntidadesInt
    m_oEntidadesInt.CargarTodasLasEntidades (m_sCodSistemaExterno)
    sdbgIntegracion.RemoveAll
    Set oEntidadInt = Nothing
    For Each oEntidadInt In m_oEntidadesInt
        If FSEPConf And (oEntidadInt.Entidad = EntidadIntegracion.Adj Or _
            oEntidadInt.Entidad = EntidadIntegracion.PED_directo Or _
            oEntidadInt.Entidad = EntidadIntegracion.Rec_Directo) Or _
            (oEntidadInt.Entidad = 19 And Not gParametrosGenerales.gbUsarPres1) Or _
            (oEntidadInt.Entidad = 20 And Not gParametrosGenerales.gbUsarPres2) Or _
            (oEntidadInt.Entidad = 21 And Not gParametrosGenerales.gbUsarPres3) Or _
            (oEntidadInt.Entidad = 22 And Not gParametrosGenerales.gbUsarPres4) Then ' en el FSEP no hay adjudicaciones ni ped. directos
        Else
            If oEntidadInt.sentido = SentidoIntegracion.Entrada Then
                sSentido = m_sFSGS & " <- " & m_sSistemaExterno
            Else
                If oEntidadInt.sentido = SentidoIntegracion.EntradaSalida Then
                    sSentido = m_sFSGS & " <-> " & m_sSistemaExterno
                Else
                    If oEntidadInt.sentido = SentidoIntegracion.salida Then
                        sSentido = m_sFSGS & " -> " & m_sSistemaExterno
                    Else
                        sSentido = ""
                    End If
                End If
            End If
            Select Case oEntidadInt.TipoTraduccion
                Case TipoTraduccion.externo: sTextoTipoTraduccion = m_sTraduccionExterna
                Case TipoTraduccion.interno: sTextoTipoTraduccion = m_sTraduccionInterna
                Case Else: sTextoTipoTraduccion = NullToStr(oEntidadInt.TipoTraduccion)
            End Select


            bsactiva = CStr(BooleanToSQLBinary(oEntidadInt.activa))
            bsTablaInter = CStr(BooleanToSQLBinary(oEntidadInt.TablaIntermedia))
            
            If oEntidadInt.Tabla <> EntidadIntegracion.TablasExternas Then
                sdbgIntegracion.AddItem CStr(oEntidadInt.Tabla) & Chr(m_lSeparador) & bsactiva & Chr(m_lSeparador) & m_aEntidad(oEntidadInt.Tabla) & Chr(m_lSeparador) & sSentido & Chr(m_lSeparador) & sTextoTipoTraduccion & Chr(m_lSeparador) & NullToStr(oEntidadInt.sentido) & Chr(m_lSeparador) & bsTablaInter & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oEntidadInt.TipoTraduccion) & Chr(m_lSeparador) & CStr(oEntidadInt.Entidad) & "_" & CStr(oEntidadInt.Tabla) & "_" & NullToStr(oEntidadInt.IdTablaExterna)
            Else
                sdbgIntegracion.AddItem CStr(oEntidadInt.Tabla) & Chr(m_lSeparador) & bsactiva & Chr(m_lSeparador) & oEntidadInt.TablaExterna & Chr(m_lSeparador) & sSentido & Chr(m_lSeparador) & sTextoTipoTraduccion & Chr(m_lSeparador) & NullToStr(oEntidadInt.sentido) & Chr(m_lSeparador) & bsTablaInter & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oEntidadInt.TipoTraduccion) & Chr(m_lSeparador) & CStr(oEntidadInt.Entidad) & "_" & CStr(oEntidadInt.Tabla) & "_" & NullToStr(oEntidadInt.IdTablaExterna)
            End If

        End If
    Next
        
    sdbddSentido.AddItem ""
    sdbgIntegracion.Columns(3).DropDownHwnd = sdbddSentido.hWnd
    
    sdbddTraduccion.AddItem ""
    sdbgIntegracion.Columns(4).DropDownHwnd = sdbddTraduccion.hWnd
    
    'Cargamos los atributos de integracion del nuevo ERP
    sdbgAtribPlant.Columns("VALORPORDEFECTO").DropDownHwnd = 0
    CargarGridAtributos (m_sCodSistemaExterno)
    
    Screen.MousePointer = vbNormal
    Set oEntidadInt = Nothing
    Set oEntidadesInt = Nothing
    Set oNotificadoInt = Nothing
End Sub

Private Sub sdbcERP_InitColumnProps()
    sdbcERP.DataFieldList = "Column 0"
    sdbcERP.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcERP_Validate(Cancel As Boolean)
    If sdbcERP.Text = "" Then Exit Sub
    bCargarComboDesde = False
End Sub

Private Sub sdbddAmbito_DropDown()

If sdbgAtribPlant.Columns("AMBITO").Locked Then
        sdbddAmbito.DroppedDown = False
        Exit Sub
    Else
        'If m_oAtributosEspecificacion Is Nothing Then
        '    Exit Sub
        'End If
    End If
    
    sdbddAmbito.RemoveAll
    
    sdbddAmbito.AddItem m_asAtrib(0)
    sdbddAmbito.AddItem m_asAtrib(1)
    sdbddAmbito.AddItem m_asAtrib(2)
  
End Sub

Private Sub sdbddAmbito_InitColumnProps()
    sdbddAmbito.DataFieldList = "Column 0"
    sdbddAmbito.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddSentido_CloseUp()

    sdbgIntegracion.Columns("SENTIDO").Value = sdbddSentido.Columns("DEN").Value
    sdbgIntegracion.Columns("SENTIDO_ID").Value = sdbddSentido.Columns("ID").Value
    sdbddSentido.Columns("DEN").Value = ""
    sdbddSentido.Columns("ID").Value = ""
    
   
End Sub
Private Sub sdbddTraduccion_CloseUp()

    sdbgIntegracion.Columns("TIPO_TRADUCCION").Value = sdbddTraduccion.Columns("DEN").Value
    sdbgIntegracion.Columns("TRADUCCION_ID").Value = sdbddTraduccion.Columns("ID").Value
    sdbddTraduccion.Columns("DEN").Value = ""
    sdbddTraduccion.Columns("ID").Value = ""
End Sub

Private Sub sdbddSentido_DropDown()

    If Not m_bModoEdicionEntidad Then
        sdbddSentido.DroppedDown = False
        Exit Sub
    End If
       
    If Not GridCheckToBoolean(sdbgIntegracion.Columns("ACTIVA").Value) Then
        sdbddSentido.DroppedDown = False
        Exit Sub
    End If
    
    sdbddSentido.RemoveAll

    If sdbgIntegracion.Columns("TABLA").Value = CStr(EntidadIntegracion.Adj) Then ' las adjudicaciones no se importan
        sdbddSentido.AddItem m_sFSGS & " -> " & m_sSistemaExterno & Chr(m_lSeparador) & SentidoIntegracion.salida
    Else
        If sdbgIntegracion.Columns("TABLA").Value = CStr(EntidadIntegracion.proceso) Then ' las adjudicaciones no se importan
            sdbddSentido.AddItem m_sFSGS & " <- " & m_sSistemaExterno & Chr(m_lSeparador) & SentidoIntegracion.Entrada
        Else
            If sdbgIntegracion.Columns("TABLA").Value = CStr(EntidadIntegracion.PED_Aprov) Or sdbgIntegracion.Columns("TABLA").Value = CStr(EntidadIntegracion.Rec_Aprov) Then
                ' aprovisionamiento, solo entrada no se contempla
                sdbddSentido.AddItem m_sFSGS & " <-> " & m_sSistemaExterno & Chr(m_lSeparador) & SentidoIntegracion.EntradaSalida
                sdbddSentido.AddItem m_sFSGS & " -> " & m_sSistemaExterno & Chr(m_lSeparador) & SentidoIntegracion.salida
            Else
                sdbddSentido.AddItem m_sFSGS & " <- " & m_sSistemaExterno & Chr(m_lSeparador) & SentidoIntegracion.Entrada
                sdbddSentido.AddItem m_sFSGS & " -> " & m_sSistemaExterno & Chr(m_lSeparador) & SentidoIntegracion.salida
                sdbddSentido.AddItem m_sFSGS & " <-> " & m_sSistemaExterno & Chr(m_lSeparador) & SentidoIntegracion.EntradaSalida
            End If
        End If
    End If
    
    
    sdbgIntegracion.ActiveCell.SelStart = 0
    sdbgIntegracion.ActiveCell.SelLength = Len(sdbgIntegracion.ActiveCell.Text)
        

End Sub
Private Sub sdbddTraduccion_DropDown()

    If Not m_bModoEdicionEntidad Then
        sdbddTraduccion.DroppedDown = False
        Exit Sub
    End If
       
    If Not GridCheckToBoolean(sdbgIntegracion.Columns("ACTIVA").Value) Then
        sdbddTraduccion.DroppedDown = False
        Exit Sub
    End If
    
    If IsNull(sdbgIntegracion.Columns("SENTIDO_ID").Value) Then
        sdbddTraduccion.DroppedDown = False
        Exit Sub
    End If
    
    sdbddTraduccion.RemoveAll


    sdbddTraduccion.AddItem m_sTraduccionExterna & Chr(m_lSeparador) & TipoTraduccion.externo
    sdbddTraduccion.AddItem m_sTraduccionInterna & Chr(m_lSeparador) & TipoTraduccion.interno
    
    sdbgIntegracion.ActiveCell.SelStart = 0
    sdbgIntegracion.ActiveCell.SelLength = Len(sdbgIntegracion.ActiveCell.Text)
        

End Sub

Private Sub sdbddSentido_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddSentido.DataFieldList = "Column 0"
    sdbddSentido.DataFieldToDisplay = "Column 0"
    

End Sub
Private Sub sdbddTraduccion_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddTraduccion.DataFieldList = "Column 0"
    sdbddTraduccion.DataFieldToDisplay = "Column 0"
    

End Sub


Private Sub sdbddSentido_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddSentido.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddSentido.Rows - 1
            bm = sdbddSentido.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddSentido.Columns("DEN").CellText(bm), 1, Len(Text))) Then
                sdbddSentido.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbddTraduccion_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddTraduccion.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddTraduccion.Rows - 1
            bm = sdbddTraduccion.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddTraduccion.Columns("DEN").CellText(bm), 1, Len(Text))) Then
                sdbddTraduccion.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub


Private Sub sdbddSentido_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)
'
'    If Text = "..." Then RtnPassed = False
'
'    ''' Si es nulo, correcto
'
'    If Text = "" Then
'        RtnPassed = True
'
'    Else
'
'        ''' Comprobar la existencia en la lista
'
'        If UCase(Text) = UCase(sdbddSentido.Columns(1).Value) Then
'            RtnPassed = True
'            Exit Sub
'        End If
'
'        m_oPaises.CargarTodosLosPaises Text, , True, , , False
'        If Not m_oPaises.Item(Text) Is Nothing Then
'            RtnPassed = True
'            sdbgDestinos.Columns(6).Value = ""
'        Else
'            sdbgDestinos.Columns(5).Value = ""
'            sdbgDestinos.Columns(6).Value = ""
'        End If
'
'    End If

End Sub

Private Sub sdbERP_CloseUp()
    Dim oEntidadesInt As CEntidadesInt
    Dim oEntidadInt As CEntidadInt
    Dim oNotificadoInt As CNotificadoInt
    Dim sSentido As String
    Dim sTextoTipoTraduccion As String
    Dim bsactiva As String
    Dim bsTablaInter As Boolean
    
    ''En primer lugar comprueba que no falten datos para el ERP que estaba seleccionado
    Set oEntidadesInt = Nothing
    Set oEntidadesInt = oFSGSRaiz.Generar_CEntidadesInt
    oEntidadesInt.CargarTodasLasEntidades (m_sCodSistemaExterno)
    For Each oEntidadInt In oEntidadesInt
        If oEntidadInt.activa Then
            ' AHORA UNA ENTIDAD PUEDE ESTAR ACTIVA Y SOLO CON TABLA INTERMEDIA
            'If NullToStr(oEntidadInt.sentido) = "" Then
            '    oMensajes.FaltanDatos sdbgIntegracion.Columns("SENTIDO").Caption
            '    Exit Sub
            'End If
            If oEntidadInt.sentido <> "" Then
                If NullToStr(oEntidadInt.TipoTraduccion) = "" Then
                    oMensajes.FaltanDatos sdbgIntegracion.Columns("TIPO_TRADUCCION").caption
                    sdbcERP.Columns(1).Value = m_sCodSistemaExterno ''Deja el ERP anterior en el combo
                    sdbcERP.Columns(0).Value = m_sSistemaExterno
                    sdbcERP.Text = m_sSistemaExterno
                    Exit Sub
                End If
            End If

            If oEntidadInt.sentido = SentidoIntegracion.EntradaSalida Or oEntidadInt.sentido = SentidoIntegracion.salida Then
                If NullToStr(oEntidadInt.DestinoTipo) = "" Or NullToStr(oEntidadInt.Destino) = "" Then
                    
                    If oEntidadInt.Tabla <> EntidadIntegracion.TablasExternas Then
                        oMensajes.FaltanDatos m_aEntidad(oEntidadInt.Tabla) & " : " & m_sTitDestino
                    Else
                        oMensajes.FaltanDatos oEntidadInt.TablaExterna & " : " & m_sTitDestino
                    End If
                    
                    sdbcERP.Columns(1).Value = m_sCodSistemaExterno ''Deja el ERP anterior en el combo
                    sdbcERP.Columns(0).Value = m_sSistemaExterno
                    sdbcERP.Text = m_sSistemaExterno
                    Exit Sub
                End If
            End If

        End If
    Next
    
    ''Actualiza los valores del ERP seleccionado:
    m_sCodSistemaExterno = sdbcERP.Columns(1).Text
    m_sSistemaExterno = sdbcERP.Columns(0).Text
    sdbcERP.Text = m_sSistemaExterno
    ''Carga los notificados:
    m_oNotificados.CargarTodasLosNotificados (m_sCodSistemaExterno)
    sdbgContactos.RemoveAll
    For Each oNotificadoInt In m_oNotificados
       sdbgContactos.AddItem NullToStr(oNotificadoInt.nombre) & Chr(m_lSeparador) & NullToStr(oNotificadoInt.Email) & Chr(m_lSeparador) & oNotificadoInt.Id
    Next
    ''Carga las entidades:
    m_oEntidadesInt.CargarTodasLasEntidades (m_sCodSistemaExterno)
    sdbgIntegracion.RemoveAll
        
    For Each oEntidadInt In m_oEntidadesInt
        If FSEPConf And (oEntidadInt.Entidad = EntidadIntegracion.Adj Or _
            oEntidadInt.Entidad = EntidadIntegracion.PED_directo Or _
            oEntidadInt.Entidad = EntidadIntegracion.Rec_Directo) Or _
            (oEntidadInt.Entidad = 19 And Not gParametrosGenerales.gbUsarPres1) Or _
            (oEntidadInt.Entidad = 20 And Not gParametrosGenerales.gbUsarPres2) Or _
            (oEntidadInt.Entidad = 21 And Not gParametrosGenerales.gbUsarPres3) Or _
            (oEntidadInt.Entidad = 22 And Not gParametrosGenerales.gbUsarPres4) Then ' en el FSEP no hay adjudicaciones ni ped. directos
        Else
            If oEntidadInt.sentido = SentidoIntegracion.Entrada Then
                sSentido = m_sFSGS & " <- " & m_sSistemaExterno
            Else
                If oEntidadInt.sentido = SentidoIntegracion.EntradaSalida Then
                    sSentido = m_sFSGS & " <-> " & m_sSistemaExterno
                Else
                    If oEntidadInt.sentido = SentidoIntegracion.salida Then
                        sSentido = m_sFSGS & " -> " & m_sSistemaExterno
                    Else
                        sSentido = ""
                    End If
                End If
            End If
            Select Case oEntidadInt.TipoTraduccion
                Case TipoTraduccion.externo: sTextoTipoTraduccion = m_sTraduccionExterna
                Case TipoTraduccion.interno: sTextoTipoTraduccion = m_sTraduccionInterna
                Case Else: sTextoTipoTraduccion = NullToStr(oEntidadInt.TipoTraduccion)
            End Select

                bsactiva = CStr(BooleanToSQLBinary(oEntidadInt.activa))
                bsTablaInter = CStr(BooleanToSQLBinary(oEntidadInt.TablaIntermedia))
                
                If oEntidadInt.Tabla <> EntidadIntegracion.TablasExternas Then
                    sdbgIntegracion.AddItem CStr(oEntidadInt.Tabla) & Chr(m_lSeparador) & bsactiva & Chr(m_lSeparador) & m_aEntidad(oEntidadInt.Tabla) & Chr(m_lSeparador) & sSentido & Chr(m_lSeparador) & sTextoTipoTraduccion & Chr(m_lSeparador) & NullToStr(oEntidadInt.sentido) & Chr(m_lSeparador) & bsTablaInter & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oEntidadInt.TipoTraduccion) & Chr(m_lSeparador) & CStr(oEntidadInt.Entidad) & "_" & CStr(oEntidadInt.Tabla) & "_" & NullToStr(oEntidadInt.IdTablaExterna)
                Else
                    sdbgIntegracion.AddItem CStr(oEntidadInt.Tabla) & Chr(m_lSeparador) & bsactiva & Chr(m_lSeparador) & oEntidadInt.TablaExterna & Chr(m_lSeparador) & sSentido & Chr(m_lSeparador) & sTextoTipoTraduccion & Chr(m_lSeparador) & NullToStr(oEntidadInt.sentido) & Chr(m_lSeparador) & bsTablaInter & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oEntidadInt.TipoTraduccion) & Chr(m_lSeparador) & CStr(oEntidadInt.Entidad) & "_" & CStr(oEntidadInt.Tabla) & "_" & NullToStr(oEntidadInt.IdTablaExterna)
                End If
                
        End If
    Next
        
    sdbddSentido.AddItem ""
    sdbgIntegracion.Columns(3).DropDownHwnd = sdbddSentido.hWnd
    
    sdbddTraduccion.AddItem ""
    sdbgIntegracion.Columns(4).DropDownHwnd = sdbddTraduccion.hWnd
    
    Screen.MousePointer = vbNormal
    Set oEntidadInt = Nothing

End Sub

Private Sub sdbERP_GotFocus()

End Sub

Private Sub sdbERP_InitColumnProps()
    sdbcERP.DataFieldList = "Column 0"
    sdbcERP.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddValor_CloseUp()
 sdbgAtribPlant.Update
End Sub

''' <summary>
''' Evento producido en el campo del valor del atributo al hacer click para expandir la lista de valores del atributo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en la lista de valores de un atributo </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub sdbddValor_DropDown()

Dim oLista As CValorPond
Dim iIndice As Integer


    If sdbgAtribPlant.Columns("VALORPORDEFECTO").Locked Then
        sdbddValor.DroppedDown = False
        Exit Sub
    Else
'        If m_oAtributo Is Nothing Then
'            Exit Sub
'        End If
    End If
    
    
iIndice = 1

sdbddValor.RemoveAll

    Select Case sdbgAtribPlant.Columns("IDTIPO").Value
    
    Case 1, 2, 3
            m_oAtributos.Item(CStr(sdbgAtribPlant.Columns("IDATRIB").Value)).CargarListaDeValoresInt
            For Each oLista In m_oAtributos.Item(CStr(sdbgAtribPlant.Columns("IDATRIB").Value)).ListaPonderacion
                sdbddValor.AddItem iIndice & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        
    Case 4
        'sdbddValor.AddItem "" & Chr(m_lSeparador) & ""
        sdbddValor.AddItem 1 & Chr(m_lSeparador) & m_sIdiTrue
        sdbddValor.AddItem 0 & Chr(m_lSeparador) & m_sIdiFalse
        
        
    End Select
End Sub

''' <summary>
''' Inicializacion de las columnas del combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_InitColumnProps()

 sdbddValor.DataFieldList = "Column 0"
 sdbddValor.DataFieldToDisplay = "Column 1"

End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))) Then
'                sdbgAtribPlant.Columns("VALORPORDEFECTO").Value = Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgAtribPlant_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
  DispPromptMsg = 0
End Sub

Private Sub sdbgAtribPlant_BeforeUpdate(Cancel As Integer)
Dim ind As Integer

Dim teserror As TipoErrorSummit

   If sdbgAtribPlant.col = -1 And g_sOrigen = "" Then Exit Sub
    
    Cancel = False
    
    If sdbgAtribPlant.Columns("CODIGO").Value = "" Then
        oMensajes.NoValido sdbgAtribPlant.Columns("COD").caption
        Cancel = True
        GoTo Salir
        Exit Sub
    End If
    If Trim(sdbgAtribPlant.Columns("DENOMINACION").Value) = "" Then
        oMensajes.NoValido sdbgAtribPlant.Columns("DEN").caption
        Cancel = True
        GoTo Salir
    End If
        
    If Trim(sdbgAtribPlant.Columns("TIPODEDATO").Value) = "" Then
        oMensajes.NoValido sdbgAtribPlant.Columns("TIPO").caption
        Cancel = True
        GoTo Salir
    End If
    
    If sdbgAtribPlant.Columns("AMBITO").Text <> "" Then
        If sdbgAtribPlant.Columns("AMBITO").Text <> m_asAtrib(0) Then
            If sdbgAtribPlant.Columns("AMBITO").Text <> m_asAtrib(1) Then
                If sdbgAtribPlant.Columns("AMBITO").Text <> m_asAtrib(2) Then
                    oMensajes.NoValido sdbgAtribPlant.Columns("AMBITO").caption
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
    Else
        sdbgAtribPlant.Columns("AMBITO").Text = m_asAtrib(2)
    End If

If sdbgAtribPlant.Columns("VALORPORDEFECTO").Text <> "" Then

    Select Case UCase(sdbgAtribPlant.Columns("IDTIPO").Text)

        Case 2 'Numero
                    If (Not IsNumeric(sdbgAtribPlant.Columns("VALORPORDEFECTO").Text)) Then
                        sdbgAtribPlant.Columns("VALORPORDEFECTO").Text = ""
                        oMensajes.AtributoValorNoValido ("TIPO2")
                        Cancel = True
                        GoTo Salir

                    End If



        Case 3 'Fecha
                        If (Not IsDate(sdbgAtribPlant.Columns("VALORPORDEFECTO").Text) And sdbgAtribPlant.Columns("VALORPORDEFECTO").Text <> "") Then
                            sdbgAtribPlant.Columns("VALORPORDEFECTO").Text = ""
                            oMensajes.AtributoValorNoValido ("TIPO3")
                            Cancel = True
                            GoTo Salir
                        End If
    End Select

End If

If sdbgAtribPlant.IsAddRow Then   'Insertar
            
        Set m_oAtributo = oFSGSRaiz.Generar_CAtributo
        m_oAtributo.Id = sdbgAtribPlant.Columns("IDATRIB").Value
        
        
        teserror = m_oAtributos.AnyadirAtributoInt(m_oAtributo.Id, m_sCodSistemaExterno, True)
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set m_oAtributo = Nothing
            Exit Sub
        Else
            sdbgAtribPlant.Columns("ID").Value = m_oAtributos.Item(CStr(sdbgAtribPlant.Columns("IDATRIB").Value)).Id
        End If
        
     '   RegistrarAccion ACCPlantAtribAnya, "Atributo A�adido: " & sdbgAtribPlant.Columns("ATRIB").Value & " Grupo " & g_sCodGRP
        
        Set m_oAtributo = Nothing
        
Else 'Modificar Atributo

        Set m_oAtributoEnEdicion = oFSGSRaiz.Generar_CAtributo
        
        'Traer los cambios del sgdb de atributos al atributo propiedad en edicion
        Set m_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtribPlant.Columns("IDATRIB").Text))
        
        If sdbgAtribPlant.Columns("ADJUDICACION").Value = "0" Or sdbgAtribPlant.Columns("ADJUDICACION").Value = "" Then
            m_oAtributoEnEdicion.Adjudicacion = False
        Else
            m_oAtributoEnEdicion.Adjudicacion = True
        End If
        
        If sdbgAtribPlant.Columns("PEDIDO").Value = "0" Or sdbgAtribPlant.Columns("PEDIDO").Value = "" Then
            m_oAtributoEnEdicion.pedido = False
        Else
            m_oAtributoEnEdicion.pedido = True
        End If
        If sdbgAtribPlant.Columns("RECEP").Value = "0" Or sdbgAtribPlant.Columns("RECEP").Value = "" Then
            m_oAtributoEnEdicion.Recepcion = False
        Else
            m_oAtributoEnEdicion.Recepcion = True
        End If
        If sdbgAtribPlant.Columns("OBLIGATORIO").Value = "0" Or sdbgAtribPlant.Columns("OBLIGATORIO").Value = "" Then
            m_oAtributoEnEdicion.Obligatorio = False
        Else
            m_oAtributoEnEdicion.Obligatorio = True
        End If
        
        If sdbgAtribPlant.Columns("VALIDACION").Value = "0" Or sdbgAtribPlant.Columns("VALIDACION").Value = "" Then
            m_oAtributoEnEdicion.Validacion_ERP = False
        Else
            m_oAtributoEnEdicion.Validacion_ERP = True
        End If
        
        'Tipos de pedido asociados al atributo
        If sdbgAtribPlant.Columns("TIPO_PEDIDO").Value <> "" Then
            Dim arrTiposPedido() As String
            Dim oTipoPedido As CTipoPedido
            Set m_oAtributoEnEdicion.TiposPedido = Nothing
            'Recojo los tipos de pedido de la columna
            arrTiposPedido = Split(sdbgAtribPlant.Columns("TIPO_PEDIDO").Value, ",")
            Set m_oAtributoEnEdicion.TiposPedido = oFSGSRaiz.Generar_CTiposPedido
                        If UBound(arrTiposPedido) < 0 Then
                'Si no hay ningun tipo de pedido seleccionado
                Set m_oAtributoEnEdicion.TiposPedido = Nothing
            End If
            For ind = 0 To UBound(arrTiposPedido)
                If arrTiposPedido(ind) <> "" Then
                    'Voy a�adiendo los tipos de pedido del atributo, lo unico que necesito es el ID del tipo de pedido(arrTiposPedido(ind))
                    m_oAtributoEnEdicion.TiposPedido.Add arrTiposPedido(ind), arrTiposPedido(ind), "", Gasto, NoAlmacenable, NoRececpionar
                End If
            Next
                Else
                        Set m_oAtributoEnEdicion.TiposPedido = Nothing
        End If
        
        
        Select Case UCase(sdbgAtribPlant.Columns("AMBITO").Text)
            Case UCase(m_asAtrib(0)): m_oAtributoEnEdicion.ambito = AmbProceso
            Case UCase(m_asAtrib(1)): m_oAtributoEnEdicion.ambito = AmbGrupo
            Case UCase(m_asAtrib(2)): m_oAtributoEnEdicion.ambito = AmbItem
            Case "": m_oAtributoEnEdicion.ambito = AmbItem
        End Select
        
        Select Case sdbgAtribPlant.Columns("IDTIPO").Value
            Case 1: m_oAtributoEnEdicion.valorText = sdbgAtribPlant.Columns("VALORPORDEFECTO").Text
                
            Case 2: m_oAtributoEnEdicion.valorNum = sdbgAtribPlant.Columns("VALORPORDEFECTO").Text
            Case 3: m_oAtributoEnEdicion.valorFec = sdbgAtribPlant.Columns("VALORPORDEFECTO").Text
            Case 4:
                    Select Case sdbgAtribPlant.Columns("VALORPORDEFECTO").Text
                        Case m_sIdiTrue: m_oAtributoEnEdicion.valorBool = 1
                        Case m_sIdiFalse: m_oAtributoEnEdicion.valorBool = 0
                        Case Else: m_oAtributoEnEdicion.valorBool = ""
                    End Select
        End Select
        
        If sdbgAtribPlant.Columns("RELACION").Value <> "" Or sdbgAtribPlant.Columns("FORMULA").Value <> "" Then
            m_oAtributoEnEdicion.Formula = sdbgAtribPlant.Columns("FORMULA").Value
        Else
            m_oAtributoEnEdicion.Formula = ""
        End If
        

        teserror = m_oAtributos.ModificarAtributoIntegracion(m_oAtributoEnEdicion.Atrib, m_oAtributoEnEdicion.Id)
        
         If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set m_oAtributo = Nothing
            Exit Sub
        End If
        
        Set m_oAtributoEnEdicion = Nothing
        
End If

'sdbgAtribPlant.Update

Salir:
    If Me.Visible Then sdbgAtribPlant.SetFocus

End Sub

Private Sub sdbgAtribPlant_BtnClick()
Dim sTipoPedidos As String
If sdbgAtribPlant.col < 0 Then Exit Sub


    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "DESCRIPCION" Then
        If Trim(sdbgAtribPlant.Columns("HIDDENDESCR").Value) <> "" Then
            frmATRIBDescr.g_bEdicion = False
            frmATRIBDescr.txtDescr.Text = sdbgAtribPlant.Columns("HIDDENDESCR").Value
            frmATRIBDescr.Show 1
        Else
            oMensajes.MensajeOKOnly (644)
            Exit Sub
        End If
    End If
    
     If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "LISTAVALORES" Then
        If sdbgAtribPlant.Columns("IDTIPO").Value <> 4 Then
            
            If sdbgAtribPlant.Columns("SELECCION").Value = "-1" Or sdbgAtribPlant.Columns("SELECCION").Value = "True" Then
                CargarListaValores
            End If
            
        End If
    End If
    
    If (sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "TIPO_PEDIDO_BTN") Then
        'mostramos la ventana de Seleccion de tipo de pedidos
        sTipoPedidos = FSGSForm.MostrarFormSelTipoPedidos(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgAtribPlant.Columns("TIPO_PEDIDO").Value, m_bModoEdicionAtributos, oFSGSRaiz)
        'Actualizo los tipos de pedido que se hayan seleccionado
        sdbgAtribPlant.Columns("TIPO_PEDIDO").Value = sTipoPedidos
        If sTipoPedidos <> "" Then
            sdbgAtribPlant.Columns("TIPO_PEDIDO_BTN").Text = "..."
        Else
            sdbgAtribPlant.Columns("TIPO_PEDIDO_BTN").Text = ""
        End If
        sdbgAtribPlant.Refresh
    End If
End Sub

Private Sub sdbgAtribPlant_RowLoaded(ByVal Bookmark As Variant)
    If sdbgAtribPlant.Columns("TIPO_PEDIDO").Value = "" Then
        sdbgAtribPlant.Columns("TIPO_PEDIDO_BTN").Text = ""
    Else
        sdbgAtribPlant.Columns("TIPO_PEDIDO_BTN").Text = "..."
    End If
    If sdbgAtribPlant.Columns("IDTIPO").Value = TiposDeAtributos.TipoArchivo Then
        sdbgAtribPlant.Columns("VALORPORDEFECTO").CellStyleSet "Gris"
        sdbgAtribPlant.Columns("SELECCION").CellStyleSet "Gris"
    End If
End Sub

Private Sub sdbgContactos_BeforeRowColChange(Cancel As Integer)
    'Esta validacion se realizara cuando se haya a�adido un registro y acto seguido se cambie de fila
    If sdbgContactos.IsAddRow And m_bAnyadiendoNotificado = False And sdbgContactos.col > -1 Then
        If Trim(sdbgContactos.Columns("PERSONA").Value) = "" Then
            oMensajes.FaltanDatos sdbgContactos.Columns("PERSONA").caption
            If Me.Visible Then sdbgContactos.SetFocus
            Cancel = True
            bErrorContacto = True
            Exit Sub
        End If
        If Trim(sdbgContactos.Columns("EMAIL").Value) = "" And m_bColModificada = False Then
            oMensajes.FaltanDatos sdbgContactos.Columns("EMAIL").caption
            If Me.Visible Then sdbgContactos.SetFocus
            Cancel = True
            bErrorContacto = True
            Exit Sub
        Else
            If sdbgContactos.Columns("EMAIL").Value <> "" Then
                If Not ComprobarEmail(sdbgContactos.Columns("EMAIL").Value) Then
                    oMensajes.FaltanDatos sdbgContactos.Columns("EMAIL").caption
                    If Me.Visible Then sdbgContactos.SetFocus
                    Cancel = True
                    bErrorContacto = True
                End If
            End If
        End If
        m_bColModificada = False
    End If
End Sub

Private Sub sdbgAtribPlant_Change()
    'Cuando pinchamos en la columna seleccion habilitando la seleccion, borraremos el campo valor por defecto si tendria valor
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "SELECCION" Then
        If sdbgAtribPlant.Columns("idTipo").Value = TiposDeAtributos.TipoArchivo Then
            sdbgAtribPlant.CancelUpdate
            Exit Sub
        End If

        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Value = -1 Then
            sdbgAtribPlant.Columns("VALORPORDEFECTO").Text = ""
        End If
    End If
End Sub

''' <summary>
''' Evento producido al cambiar de columna en el grid de atributos de especificaci�n
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtribPlant_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim oElem As CValorPond
Dim oLista As CValoresPond
Dim oatrib As CAtributo

If sdbgAtribPlant.Row = -1 Then Exit Sub
If sdbgAtribPlant.col = -1 Then Exit Sub

sdbgAtribPlant.Columns("VALORPORDEFECTO").Locked = False
sdbgAtribPlant.Columns("SELECCION").Locked = False
If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "VALORPORDEFECTO" Then
    If sdbgAtribPlant.Columns("SELECCION").Value = 0 Then
        'Libre
        If sdbgAtribPlant.Columns("IDTIPO").Value = TipoBoolean Then
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            sdbgAtribPlant.Columns("VALORPORDEFECTO").DropDownHwnd = sdbddValor.hWnd
            sdbddValor.Enabled = True
        Else
            sdbgAtribPlant.Columns("VALORPORDEFECTO").DropDownHwnd = 0
            sdbddValor.Enabled = False
            If sdbgAtribPlant.Columns("IDTIPO").Value = TiposDeAtributos.TipoArchivo Then
                sdbgAtribPlant.Columns("VALORPORDEFECTO").Locked = True
                sdbgAtribPlant.Columns("SELECCION").Locked = True
            End If
        End If
    Else
        'Lista
        sdbddValor.RemoveAll
        sdbddValor.AddItem ""
        sdbgAtribPlant.Columns("VALORPORDEFECTO").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
        sdbddValor_DropDown
        sdbddValor.DroppedDown = True
    End If
End If
    
End Sub

Private Sub sdbgContactos_AfterDelete(RtnDispErrMsg As Integer)
    If Me.sdbgContactos.Rows > 0 Then
        Me.sdbgContactos.Row = 0 'Cuando eliminamos un notificado situaremos el foco en la primera fila
    End If
End Sub

Private Sub sdbgContactos_AfterUpdate(RtnDispErrMsg As Integer)
''' * Objetivo: Actualizar la fila en edicion
''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    Dim v As Variant
           
    If m_oNotificadoEnEdicion Is Nothing Then
        DoEvents
        sdbgContactos_Change
        DoEvents
    End If
    
    bModError = False
    
    ''' Modificamos en la base de datos
    
    If sdbgContactos.Columns("ID").Value = "" Then
        Set m_oNotificadoEnEdicion = oFSGSRaiz.Generar_CNotificadoInt
                
        m_oNotificadoEnEdicion.nombre = sdbgContactos.Columns("PERSONA").Value
        m_oNotificadoEnEdicion.Email = sdbgContactos.Columns("EMAIL").Value
        m_oNotificadoEnEdicion.CodERP = m_sCodSistemaExterno
    
        Set oIBaseDatos = m_oNotificadoEnEdicion
        teserror = oIBaseDatos.AnyadirABaseDatos
           
        If teserror.NumError <> TESnoerror Then
            v = sdbgContactos.ActiveCell.Value
            TratarError teserror
            If Me.Visible Then sdbgContactos.SetFocus
            bModError = True
            sdbgContactos.ActiveCell.Value = v
            sdbgContactos.DataChanged = False
        Else
            sdbgContactos.Columns("ID").Value = m_oNotificadoEnEdicion.Id
            Set oIBaseDatos = Nothing
            Set m_oNotificadoEnEdicion = Nothing
        End If
    
    Else
        
        
        Set m_oNotificadoEnEdicion = oFSGSRaiz.Generar_CNotificadoInt
        m_oNotificadoEnEdicion.Id = sdbgContactos.Columns("ID").Value
        m_oNotificadoEnEdicion.nombre = sdbgContactos.Columns("PERSONA").Value
        m_oNotificadoEnEdicion.Email = sdbgContactos.Columns("EMAIL").Value
        
        Set oIBaseDatos = m_oNotificadoEnEdicion
        teserror = oIBaseDatos.FinalizarEdicionModificando
           
        If teserror.NumError <> TESnoerror Then
            v = sdbgContactos.ActiveCell.Value
            TratarError teserror
            If Me.Visible Then sdbgContactos.SetFocus
            bModError = True
            sdbgContactos.ActiveCell.Value = v
            sdbgContactos.DataChanged = False
        Else
            Set oIBaseDatos = Nothing
            Set m_oNotificadoEnEdicion = Nothing
        End If
        bCambioNotificado = False
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgContactos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgContactos_BeforeUpdate(Cancel As Integer)

    ''' AQUI VIENEN LAS VALIDACIONES
   If sdbgContactos.col > -1 Then

    If Trim(sdbgContactos.Columns("PERSONA").Value) = "" Then
        oMensajes.FaltanDatos sdbgContactos.Columns("PERSONA").caption
        If Me.Visible Then sdbgContactos.SetFocus
        Cancel = True
        bErrorContacto = True
    End If
    If Trim(sdbgContactos.Columns("EMAIL").Value) = "" Then
        oMensajes.FaltanDatos sdbgContactos.Columns("EMAIL").caption
        If Me.Visible Then sdbgContactos.SetFocus
        Cancel = True
        bErrorContacto = True
    Else
        If sdbgContactos.Columns("EMAIL").Value <> "" Then
            If Not ComprobarEmail(sdbgContactos.Columns("EMAIL").Value) Then
                oMensajes.FaltanDatos sdbgContactos.Columns("EMAIL").caption
                If Me.Visible Then sdbgContactos.SetFocus
                Cancel = True
                bErrorContacto = True
            End If
        End If
    End If
   End If
    
End Sub



Private Sub sdbgContactos_Change()
    m_bColModificada = True
    ''' **************************************************
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    ''' **************************************************
    
End Sub

Private Sub sdbgIntegracion_AfterUpdate(RtnDispErrMsg As Integer)

''' * Objetivo: Actualizar la fila en edicion
''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    Dim b, bTablaIntermedia As Boolean
       
    If m_oEntidadEnEdicion Is Nothing Then
        DoEvents
        sdbgIntegracion_Change
        DoEvents
    End If
    
    bModError = False
    
    ''' Modificamos en la base de datos
    b = GridCheckToBoolean(sdbgIntegracion.Columns("ACTIVA").Value)
    bTablaIntermedia = GridCheckToBoolean(sdbgIntegracion.Columns("TABLA_INTERMEDIA").Value)
     
    m_oEntidadEnEdicion.activa = b
    m_oEntidadEnEdicion.TablaIntermedia = bTablaIntermedia
    m_oEntidadEnEdicion.sentido = sdbgIntegracion.Columns("SENTIDO_ID").Value
    m_oEntidadEnEdicion.TipoTraduccion = DblToSQLNullFloat(garSimbolos, sdbgIntegracion.Columns("TRADUCCION_ID").Value)
    
    Set oIBaseDatos = m_oEntidadEnEdicion
    
    If m_oEntidadEnEdicion.ExisteEnBBDD Then
        teserror = oIBaseDatos.FinalizarEdicionModificando
    Else
        If m_oEntidadEnEdicion.activa Then
            teserror = oIBaseDatos.AnyadirABaseDatos
        End If
    End If
       
    If teserror.NumError <> TESnoerror Then
        v = sdbgIntegracion.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgIntegracion.SetFocus
        bModError = True
        sdbgIntegracion.ActiveCell.Value = v
        sdbgIntegracion.DataChanged = False
      
    Else
        Set oIBaseDatos = Nothing
        Set m_oEntidadEnEdicion = Nothing
    End If
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgIntegracion_BeforeUpdate(Cancel As Integer)

    ''' AQUI VIENEN LAS VALIDACIONES
    
    ' Si est� ACTIVA hay que marcar sentido o tabla intermedia
    If GridCheckToBoolean(sdbgIntegracion.Columns("ACTIVA").Value) And sdbgIntegracion.Columns("SENTIDO").Value = "" And sdbgIntegracion.Columns("TABLA_INTERMEDIA").Value = "0" Then
        oMensajes.FaltanDatos sdbgIntegracion.Columns("SENTIDO").caption & " " & vbLf & " " & sdbgIntegracion.Columns("TABLA_INTERMEDIA").caption
        If Me.Visible Then sdbgIntegracion.SetFocus
        Cancel = True
        bError = True
    End If
    If GridCheckToBoolean(sdbgIntegracion.Columns("ACTIVA").Value) And sdbgIntegracion.Columns("SENTIDO").Value <> "" Then
        If sdbgIntegracion.Columns("TIPO_TRADUCCION").Value = "" Then
            oMensajes.FaltanDatos sdbgIntegracion.Columns("TIPO_TRADUCCION").caption
            If Me.Visible Then sdbgIntegracion.SetFocus
            Cancel = True
            bError = True
        End If
    End If
    


End Sub

Private Sub sdbgIntegracion_BtnClick()
Dim v As Variant

    
    ' Si no est� activa la integraci�n, no hacemos nada
    If Not GridCheckToBoolean(sdbgIntegracion.Columns("ACTIVA").Value) Then Exit Sub
    
    
    If sdbgIntegracion.col < 0 Then Exit Sub
    
    ' Si no tiene ningun sentido especificado o es de entrada, si pulsan en DESTINO
    'If (Left(sdbgIntegracion.Columns(sdbgIntegracion.col).Name, 4) = "DEST") Then
    '    If sdbgIntegracion.Columns(5).Value = 2 Or sdbgIntegracion.Columns(5).Value = "" Then
    '        Exit Sub
    '    End If
    'End If

    If Not m_bModoEdicionEntidad Then
    ' MODO CONSULTA
        
        ' Si no tiene ningun sentido especificado o es de salida, si pulsan en ORIGEN
        If (Left(sdbgIntegracion.Columns(sdbgIntegracion.col).Name, 6) = "ORIGEN") Then
            If (sdbgIntegracion.Columns(5).Value = "") And sdbgIntegracion.Columns(6).Value = "0" Then
                Exit Sub
            End If
        End If
    
    
        If (Left(sdbgIntegracion.Columns(sdbgIntegracion.col).Name, 4) = "DEST") Then
        ' mostramos la ventana de destino en modo consulta
            Set frmINTDestino.m_oEntidadInt = m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value)
            frmINTDestino.m_bConsulta = True
            frmINTDestino.m_sEntidad = sdbgIntegracion.Columns("ENTIDAD").Value
            frmINTDestino.m_sCodERP = m_sCodSistemaExterno
            If Not IsNull(m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value).DestinoTipo) Then
                frmINTDestino.sdbcTipo.Value = m_sDestino(m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value).DestinoTipo)
                frmINTDestino.sdbcTipo_Validate False
            End If
            frmINTDestino.Left = Me.Left + 100
            frmINTDestino.Top = Me.Top + 1500
            Load frmINTDestino
            frmINTDestino.Hide
            frmINTDestino.Show 1
            Exit Sub
        End If
    
        If (Left(sdbgIntegracion.Columns(sdbgIntegracion.col).Name, 4) = "ORIG") Then
        ' mostramos la ventana de ORIGEN en modo consulta
            Set frmINTOrigen.m_oEntidadInt = m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value)
            frmINTOrigen.m_sCodERP = m_sCodSistemaExterno
            frmINTOrigen.m_bConsulta = True
            frmINTOrigen.m_sEntidad = sdbgIntegracion.Columns("ENTIDAD").Value
            If Not IsNull(m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value).OrigenTipo) Then
                frmINTOrigen.sdbcTipo.Value = m_sDestino(m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value).OrigenTipo)
                frmINTOrigen.sdbcTipo_Validate False
            End If
            frmINTOrigen.Left = Me.Left + 100
            frmINTOrigen.Top = Me.Top + 1500
            Load frmINTOrigen
            frmINTOrigen.Hide
            frmINTOrigen.Show 1
            Exit Sub
        End If
        
        
    Else
        ' MODO EDICION
    
        If (Left(sdbgIntegracion.Columns(sdbgIntegracion.col).Name, 4) = "DEST") Then
            
                If sdbgIntegracion.DataChanged = True Then
            
                    v = sdbgIntegracion.ActiveCell.Value
                    If Me.Visible Then sdbgIntegracion.SetFocus
                    sdbgIntegracion.ActiveCell.Value = v
                    bError = False
                    
                    If sdbgIntegracion.Row = 0 Then   'Es la primera fila o la �nica
                
                        If sdbgIntegracion.Rows = 1 Then
                            sdbgIntegracion.Update
                            If bError Then
                                Exit Sub
                            End If
                        Else
                            sdbgIntegracion.MoveNext
                            DoEvents
                            If bError Then
                                Exit Sub
                            Else
                                sdbgIntegracion.MovePrevious
                            End If
                        End If
                    Else                              'No es la primera fila
                        sdbgIntegracion.MovePrevious
                        DoEvents
                        If bError Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                            Exit Sub
                        Else
                            sdbgIntegracion.MoveNext
                        End If
                    End If
        
                
                End If
                
                Set frmINTDestino.m_oEntidadInt = m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value)
                frmINTDestino.m_bConsulta = False
                frmINTDestino.m_sEntidad = sdbgIntegracion.Columns("ENTIDAD").Value
                frmINTDestino.m_sCodERP = m_sCodSistemaExterno
                If Not IsNull(m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value).DestinoTipo) Then
                    frmINTDestino.sdbcTipo.Value = m_sDestino(m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value).DestinoTipo)
                    frmINTDestino.sdbcTipo_Validate False
                End If
                frmINTDestino.Left = Me.Left + 100
                frmINTDestino.Top = Me.Top + 1500
                Load frmINTDestino
                frmINTDestino.Hide
                frmINTDestino.Show 1
                Exit Sub
        End If
    
        If (sdbgIntegracion.Columns(sdbgIntegracion.col).Name = "ORIGEN") Then
            
                If sdbgIntegracion.DataChanged = True Then
            
                    v = sdbgIntegracion.ActiveCell.Value
                    If Me.Visible Then sdbgIntegracion.SetFocus
                    sdbgIntegracion.ActiveCell.Value = v
                    bError = False
                    
                    If sdbgIntegracion.Row = 0 Then   'Es la primera fila o la �nica
                
                        If sdbgIntegracion.Rows = 1 Then
                            sdbgIntegracion.Update
                            If bError Then
                                Exit Sub
                            End If
                        Else
                            sdbgIntegracion.MoveNext
                            DoEvents
                            If bError Then
                                Exit Sub
                            Else
                                sdbgIntegracion.MovePrevious
                            End If
                        End If
                    Else                              'No es la primera fila
                        sdbgIntegracion.MovePrevious
                        DoEvents
                        If bError Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                            Exit Sub
                        Else
                            sdbgIntegracion.MoveNext
                        End If
                    End If
        
                
                End If
                
                ' mostramos la ventana de ORIGEN en modo consulta
                Set frmINTOrigen.m_oEntidadInt = m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value)
                frmINTOrigen.m_sCodERP = m_sCodSistemaExterno
                frmINTOrigen.m_bConsulta = False
                frmINTOrigen.m_sEntidad = sdbgIntegracion.Columns("ENTIDAD").Value
                If Not IsNull(m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value).OrigenTipo) Then
                    frmINTOrigen.sdbcTipo.Value = m_sDestino(m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value).OrigenTipo)
                    frmINTOrigen.sdbcTipo_Validate False
                End If
                frmINTOrigen.Left = Me.Left + 100
                frmINTOrigen.Top = Me.Top + 1500
                Load frmINTOrigen
                frmINTOrigen.Hide
                frmINTOrigen.Show 1
                Exit Sub
        End If
    End If

    

End Sub

Private Sub sdbgIntegracion_Change()

    ''' **************************************************
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    ''' **************************************************
    
    Dim teserror As TipoErrorSummit
    
    If m_bModoEdicionEntidad Then
        
        DoEvents
        
        Set m_oEntidadEnEdicion = Nothing
        
        Set m_oEntidadEnEdicion = m_oEntidadesInt.Item(sdbgIntegracion.Columns("CLAVE").Value)
    
     
        Set oIBaseDatos = m_oEntidadEnEdicion
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
            
            TratarError teserror
            sdbgIntegracion.DataChanged = False
            sdbgIntegracion.Columns("ACTIVA").Value = BooleanToSQLBinary(m_oEntidadEnEdicion.activa)
            sdbgIntegracion.Columns("SENTIDO_ID").Value = m_oEntidadEnEdicion.sentido
            sdbgIntegracion.Columns("TIPO_TRADUCCION").Value = m_oEntidadEnEdicion.TipoTraduccion
            If m_oEntidadEnEdicion.TablaIntermedia Then
                sdbgIntegracion.Columns("TABLA_INTERMEDIA").Value = "1"
            Else
                sdbgIntegracion.Columns("TABLA_INTERMEDIA").Value = "0"
            End If
            teserror.NumError = TESnoerror
            
        End If
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgIntegracion.SetFocus
        Else
            If GridCheckToBoolean(sdbgIntegracion.Columns("ACTIVA").Value) = True Then
                sdbgIntegracion.Columns("ACTIVA").Value = "1"
                If sdbddSentido.Columns("ID").Value <> "" Then
                ' Se ha deplegado la combo de Sentidos
                    sdbgIntegracion.Columns("SENTIDO").Value = sdbddSentido.Columns("DEN").Value
                    sdbgIntegracion.Columns("SENTIDO_ID").Value = sdbddSentido.Columns("ID").Value
                End If
                ' Se ha deplegado la combo de tipos de traduccion
                If sdbddTraduccion.Columns("ID").Value <> "" Then
                    sdbgIntegracion.Columns("TIPO_TRADUCCION").Value = sdbddTraduccion.Columns("DEN").Value
                    sdbgIntegracion.Columns("TRADUCCION_ID").Value = sdbddTraduccion.Columns("ID").Value
                End If
            Else
                sdbgIntegracion.Columns("ACTIVA").Value = "0"
                sdbgIntegracion.Columns("SENTIDO").Value = ""
                sdbgIntegracion.Columns("SENTIDO_ID").Value = ""
                sdbgIntegracion.Columns("TIPO_TRADUCCION").Value = ""
                sdbgIntegracion.Columns("TRADUCCION_ID").Value = ""
                sdbgIntegracion.Columns("TABLA_INTERMEDIA").Value = "0"
            End If
        End If
    End If
    
End Sub

Private Sub sdbgIntegracion_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape Then
                                
        If sdbgIntegracion.DataChanged = False Then
            
            sdbgIntegracion.CancelUpdate
            sdbgIntegracion.DataChanged = False
            
            If Not m_oEntidadEnEdicion Is Nothing Then
                Set oIBaseDatos = Nothing
                Set m_oEntidadEnEdicion = Nothing
            End If
                'Accion = ACCSeguimientoPedConsulta
            
        End If
    
    End If

End Sub

''' <summary>
''' Esta subrutina organiza los atributos de la tabla en caso de existir alguno con el campo ORDEN a NULL.
''' </summary>
''' <remarks>
''' Llamada desde: frmCONFIntegracion.cmdSubir_Click, frmCONFIntegracion.cmdBajar_Click.
''' Tiempo m�ximo: aproximadamente 1 seg, dependiendo del n�mero de atributos de integraci�n existentes para el
''' ERP actual.
''' Revisado por: aam (16/09/2014).
''' </remarks>
Private Sub OrganizarAtributos()
    Dim oatrib, oAtribModif As CAtributo
    Dim teserror As TipoErrorSummit
    Dim bOrganizado As Boolean
    Dim i As Integer
    
    bOrganizado = True
    For Each oatrib In m_oAtributos
        If IsNull(oatrib.OrdenAtrib) Then
            Set oAtribModif = oatrib
            oAtribModif.OrdenAtrib = 0
            teserror = m_oAtributos.ModificarAtributoIntegracion(oAtribModif.Atrib, oAtribModif.Id)
            If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set oAtribModif = Nothing
                    Exit Sub
            End If
            bOrganizado = False
        End If
    Next
    
    If Not bOrganizado Then
        For i = 1 To (m_oAtributos.Count)
            Set oAtribModif = m_oAtributos.Item(i)
            oAtribModif.OrdenAtrib = i - 1
            teserror = m_oAtributos.ModificarAtributoIntegracion(oAtribModif.Atrib, oAtribModif.Id)
            If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set oAtribModif = Nothing
                    Exit Sub
            End If
        Next
    End If
End Sub

''' <summary>
''' Evento OnClick del bot�n de la flecha mirando hacia arriba. Esta subrutina sube una posici�n el atributo de integraci�n
''' seleccionado y baja una posici�n (en la tabla) el atributo de arriba al atributo seleccionado.
''' </summary>
''' <remarks>
''' Tiempo m�ximo: aproximadamente 1 seg dependiendo del n�mero de atributos de integraci�n existentes para el ERP seleccionado.
''' Revisado por: aam (17/09/2014).
''' </remarks>
Private Sub cmdSubir_Click()
    Dim lIdAtribSelec As Long
    Dim iOrdenSelect, i As Integer
    Dim oatrib, oAtribModif As CAtributo
    Dim teserror(1) As TipoErrorSummit

    If sdbgAtribPlant.Rows = 0 Then Exit Sub
    If sdbgAtribPlant.Columns("ID").Text = "" Then Exit Sub
    Screen.MousePointer = vbHourglass
    sdbgAtribPlant.GetBookmark sdbgAtribPlant.Row
    sdbgAtribPlant.SelBookmarks.Add sdbgAtribPlant.Bookmark

    '�nicamente permitimos la selecci�n de una l�nea.
    If sdbgAtribPlant.SelBookmarks.Count = 1 Then
        'Obtenemos el orden del atributo.
        lIdAtribSelec = sdbgAtribPlant.Columns("IDATRIB").Text
        For Each oatrib In m_oAtributos
            If IsNull(oatrib.OrdenAtrib) Then
                'Esta funci�n se encarga de quitar los �rdenes NULOS de TODOS los atributos para
                'el ERP actual.
                OrganizarAtributos
                Exit For
            End If
        Next
        iOrdenSelect = m_oAtributos.Item(CStr(lIdAtribSelec)).OrdenAtrib
        
        'Si es 0 no hacemos nada.
        If iOrdenSelect = 0 Then
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            'Le restamos 1.
            iOrdenSelect = iOrdenSelect - 1
            m_oAtributos.Item(CStr(lIdAtribSelec)).OrdenAtrib = iOrdenSelect
            'Ahora tenemos que coger el atributo con el mismo orden que el que hemos cambiado
            'y ponerle m�s abajo en el grid.
            For Each oatrib In m_oAtributos
                If oatrib.OrdenAtrib = iOrdenSelect And oatrib.Atrib <> lIdAtribSelec Then
                    m_oAtributos.Item(CStr(oatrib.Atrib)).OrdenAtrib = iOrdenSelect + 1
                    Set oAtribModif = oatrib
                    Exit For
                End If
            Next
            'Guardamos los cambios de orden de los atributos.
            teserror(0) = m_oAtributos.ModificarAtributoIntegracion(oAtribModif.Atrib, oAtribModif.Id)
            teserror(1) = m_oAtributos.ModificarAtributoIntegracion(lIdAtribSelec, m_oAtributos.Item(CStr(lIdAtribSelec)).Id)
            
            For i = 0 To 1
                If teserror(i).NumError <> TESnoerror Then
                    basErrores.TratarError teserror(i)
                    Set oAtribModif = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            Next
            
            'Cargamos la grid con los nuevos cambios.
            CargarGridAtributos m_sCodSistemaExterno
            sdbgAtribPlant.SelBookmarks.Add sdbgAtribPlant.RowBookmark(iOrdenSelect)
            
            sdbgAtribPlant.MoveRecords (iOrdenSelect - sdbgAtribPlant.Row)
        End If
    End If
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Evento OnClick del bot�n de la flecha mirando hacia abajo. Esta subrutina baja una posici�n de la tabla al atributo de
''' integraci�n seleccionado y sube una posici�n al atributo que estaba debajo de �ste.
''' </summary>
''' <remarks>
''' Tiempo m�ximo: aproximadamente 1 seg, dependiendo de la cantidad de atributos de integraci�n existentes para el ERP
''' seleccionado.
''' Llamada desde: aam (16/09/2014).
''' </remarks>
Private Sub cmdBajar_Click()
    Dim lIdAtribSelec As Long
    Dim iOrdenSelect, i As Integer
    Dim oatrib, oAtribModif As CAtributo
    Dim teserror(1) As TipoErrorSummit
    
    If sdbgAtribPlant.Rows = 0 Then Exit Sub
    If sdbgAtribPlant.Columns("ID").Text = "" Then Exit Sub
    Screen.MousePointer = vbHourglass
    sdbgAtribPlant.GetBookmark sdbgAtribPlant.Row

    '�nicamente permitimos la selecci�n de una l�nea.
    If sdbgAtribPlant.SelBookmarks.Count = 1 Then
        'Obtenemos el orden del atributo.
        lIdAtribSelec = sdbgAtribPlant.Columns("IDATRIB").Text
        For Each oatrib In m_oAtributos
            If IsNull(oatrib.OrdenAtrib) Then
                'Esta funci�n se encarga de quitar los �rdenes NULOS de TODOS los atributos para
                'el ERP actual.
                OrganizarAtributos
                Exit For
            End If
        Next
        iOrdenSelect = m_oAtributos.Item(CStr(lIdAtribSelec)).OrdenAtrib
        
        'Si es 0 no hacemos nada.
        If iOrdenSelect = (sdbgAtribPlant.Rows - 1) Then 'En BD se empieza a contar el orden desde 0.
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            'Le sumamos 1.
            iOrdenSelect = iOrdenSelect + 1
            m_oAtributos.Item(CStr(lIdAtribSelec)).OrdenAtrib = iOrdenSelect
            'Ahora tenemos que coger el atributo con el mismo orden que el que hemos cambiado
            'y ponerle m�s arriba en el grid.
            For Each oatrib In m_oAtributos
                If oatrib.OrdenAtrib = iOrdenSelect And oatrib.Atrib <> lIdAtribSelec Then
                    m_oAtributos.Item(CStr(oatrib.Atrib)).OrdenAtrib = iOrdenSelect - 1
                    Set oAtribModif = oatrib
                    Exit For
                End If
            Next
            'Guardamos los cambios de orden de los atributos.
            teserror(0) = m_oAtributos.ModificarAtributoIntegracion(oAtribModif.Atrib, oAtribModif.Id)
            teserror(1) = m_oAtributos.ModificarAtributoIntegracion(lIdAtribSelec, m_oAtributos.Item(CStr(lIdAtribSelec)).Id)
            
            For i = 0 To 1
                If teserror(i).NumError <> TESnoerror Then
                    basErrores.TratarError teserror(i)
                    Set oAtribModif = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            Next
            
            'Cargamos la grid con los nuevos cambios.
            CargarGridAtributos m_sCodSistemaExterno
            sdbgAtribPlant.SelBookmarks.Add sdbgAtribPlant.RowBookmark(iOrdenSelect)
            
            sdbgAtribPlant.MoveRecords (iOrdenSelect - sdbgAtribPlant.Row)
            
            
        End If
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub sstabPlantillas_Click(PreviousTab As Integer)

sdbgIntegracion.Visible = False
cmdEdicEntidades.Visible = False
sdbgAtribPlant.Visible = False
cmdEdicAtributos.Visible = False
cmdAnyadirAtr.Visible = False
cmdEliAtr.Visible = False
sdbgContactos.Visible = False
cmdEdicNotific.Visible = False

Select Case sstabPlantillas.Tab
    Case 0
        sdbgIntegracion.Visible = True
        cmdEdicEntidades.Visible = True
    Case 1
        sdbgAtribPlant.Visible = True
        cmdEdicAtributos.Visible = True
        If cmdEdicAtributos.caption = m_sConsulta Then
            cmdAnyadirAtr.Visible = True
            cmdEliAtr.Visible = True
        Else
            cmdAnyadirAtr.Visible = False
            cmdEliAtr.Visible = False
        End If
    Case 2
        sdbgContactos.Visible = True
        cmdEdicNotific.Visible = True
    
End Select

End Sub

