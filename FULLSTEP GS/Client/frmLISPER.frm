VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmLISPER 
   Caption         =   "Listados personalizados"
   ClientHeight    =   4575
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5415
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLISPER.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4575
   ScaleWidth      =   5415
   Begin VB.PictureBox picSeleccionar 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   5415
      TabIndex        =   0
      Top             =   4200
      Width           =   5415
      Begin VB.CommandButton cmdSeleccionar 
         Caption         =   "Seleccionar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   4260
         TabIndex        =   1
         Top             =   0
         Width           =   1155
      End
   End
   Begin MSComctlLib.ImageList imglstListados 
      Left            =   4800
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLISPER.frx":0CB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLISPER.frx":0DC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLISPER.frx":0ED6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwListados 
      Height          =   4155
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   7329
      _Version        =   393217
      Indentation     =   353
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "imglstListados"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmLISPER"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private sIdiA As String

Private Sub Arrange()

    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 3190 Then   'de tama�o de la ventana
            Me.Width = 3390        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 1475 Then  'cuando no se maximiza ni
            Me.Height = 1575       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width

    If Me.Width > 120 Then tvwListados.Width = Me.Width - 120
    If Me.Height > 825 Then tvwListados.Height = Me.Height - 825
  
    If Me.Width > (cmdSeleccionar.Width + 120) Then cmdSeleccionar.Left = Me.Width - cmdSeleccionar.Width - 120

End Sub

Private Sub cmdSeleccionar_Click()
    '''Obtiene el informe
    ObtenerInforme

End Sub

Private Sub Form_Load()
    Me.Height = 5145
    Me.Width = 5600
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    CargarListados
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub CargarRecursos()

Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LISPER, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.caption = ador(0).Value
        ador.MoveNext
        cmdSeleccionar.caption = ador(0).Value
        ador.MoveNext
        sIdiA = ador(0).Value
        
        ador.Close
    
    End If


   Set ador = Nothing

End Sub

Private Sub CargarListados()
    'Carga los listados personalizados para el usuario y tambi�n seg�n su perfil
    Dim ador As ador.Recordset
    Dim i As Integer
    
    If oUsuarioSummit.Tipo = TipoDeUsuario.Administrador Then
        Set ador = oGestorListadosPers.DevolverListadosPersonalizados(, , , "GS")
    ElseIf oUsuarioSummit.Perfil Is Nothing Then
        Set ador = oGestorListadosPers.DevolverListadosPersonalizados(oUsuarioSummit.Cod, , , "GS")
    Else
        Set ador = oGestorListadosPers.DevolverListadosPersonalizados(oUsuarioSummit.Cod, NullToStr(oUsuarioSummit.Perfil.Cod), , "GS")
    End If
    
    If Not ador Is Nothing Then
        'Carga el treview
        tvwListados.Indentation = 200
        tvwListados.ImageList = imglstListados
        tvwListados.Nodes.Add , tvwFirst, "A", sIdiA
        tvwListados.Nodes("A").Image = 1
        tvwListados.Nodes("A").ExpandedImage = 2
        tvwListados.Nodes("A").Expanded = True
        
        ador.MoveFirst
        i = 1
        
        Dim nodx As MSComctlLib.node
        While Not ador.EOF
            'tvwListados.Nodes.Add "A", tvwChild, "A" & Ador("RPT"), Ador("DEN"), NullToStr(Ador("FILTRO")), NullToStr(Ador("FILTROGS"))
            If Not IsNull(ador("FILTRO")) Then
                               
                Set nodx = tvwListados.Nodes.Add("A", tvwChild, "A" & ador("RPT"), ador("DEN"))
                nodx.Tag = NullToStr(ador("FILTRO")) & "???" & NullToStr(ador("FILTROGS"))
            Else
            
                tvwListados.Nodes.Add "A", tvwChild, "A" & ador("RPT"), ador("DEN")
            End If
            tvwListados.Nodes("A" & ador("RPT")).Image = 3
            
            i = i + 1
            ador.MoveNext
        Wend
        
        ador.Close
        Set ador = Nothing
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Me.Visible = False
End Sub

Private Sub tvwListados_NodeClick(ByVal node As MSComctlLib.node)
    Dim nod As MSComctlLib.node

    For Each nod In tvwListados.Nodes
        If nod.Image = 3 Then
            nod.Bold = False
            cmdSeleccionar.Enabled = False
        End If
    Next
    If node.Image = 3 Then
        node.Bold = True
        cmdSeleccionar.Enabled = True
    End If
End Sub

''' <summary>
''' Mostrar un listado rpt o dtsx
''' </summary>
''' <remarks>Llamada desde: cmdSeleccionar_Click ; Tiempo m�ximo: 0</remarks>
Private Sub ObtenerInforme()
    Dim sFichero As String
    Dim oReport As CRAXDRT.Report
    Dim oFos As FileSystemObject
    Dim pv As Preview
    Dim bParametros As Boolean
    Dim i As Integer
    Dim sSubRpt As CRAXDRT.Report
    Dim Table As CRAXDRT.DatabaseTable
    Dim intSub As Integer
    Dim bExiste As Boolean
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    'Comprueba que la ruta de archivos sea la correcta y que exista el informe
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
           oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    Dim sCadenaS As Variant
    If tvwListados.selectedItem.Tag <> "" Then
        sCadenaS = Split(tvwListados.selectedItem.Tag, "???", , vbTextCompare)
        
        Select Case sCadenaS(0)
            'Hago el select case del id
            Case 1, 2, 3
                frmLstTiemposMedios.m_sCasoReporte = sCadenaS(1)
                frmLstTiemposMedios.m_sNombreReporte = Mid(tvwListados.selectedItem.key, 2)
                frmLstTiemposMedios.Show vbModal
            Case 4, 5
                frmLstTiemposProveedor.m_sCasoReporte = sCadenaS(1)
                frmLstTiemposProveedor.m_sNombreReporte = Mid(tvwListados.selectedItem.key, 2)
                frmLstTiemposProveedor.Show vbModal
        End Select
    Else
       
        If LCase(Right(tvwListados.selectedItem.key, 4)) = ".rpt" Then
    
            sFichero = gParametrosInstalacion.gsRPTPATH & "\" & Mid(tvwListados.selectedItem.key, 2)
            Set oFos = New FileSystemObject
            If Not oFos.FileExists(sFichero) Then
                oMensajes.RutaDeRPTNoValida
                Set oReport = Nothing
                Set oFos = Nothing
                Exit Sub
            End If
            Set oFos = Nothing
                    
            Screen.MousePointer = vbHourglass
            
            If crs_Connected = False Then
                Set oFos = Nothing
                Exit Sub
            End If
            
            Set oReport = crs_crapp.OpenReport(sFichero, crOpenReportByTempCopy)
            
            'Se pasa a cada tabla el servidor y base de datos
        '    For Each Table In oReport.Database.Tables
        '        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
        '    Next
             ConectarReport oReport, crs_Server, crs_Database, crs_User, crs_Password
             
            'Comprueba si el informe tiene par�metros.En el caso de que no tenga comprobamos
            'si tiene subreports y si �stos a su vez tienen par�metros
            If oReport.ParameterFields.Count = 0 Then
                bParametros = False
            Else
                bParametros = True
            End If
            
            For i = 1 To oReport.FormulaFields.Count
                If oReport.FormulaFields(i).FormulaFieldName = "NUMSUB" Then
                    bExiste = True
                    Exit For
                End If
            Next i
            If bExiste = False Then
                GoTo Imprimir
            End If
            If oReport.FormulaFields(crs_FormulaIndex(oReport, "NUMSUB")).Text > 0 Then
                For i = 1 To oReport.FormulaFields(crs_FormulaIndex(oReport, "NUMSUB")).Text
                    Set sSubRpt = oReport.OpenSubreport("SubRpt" & i)
                    'Se pasa a cada tabla el servidor y base de datos, de cada uno de los subreports
                    ConectarReport sSubRpt, crs_Server, crs_Database, crs_User, crs_Password
        '            For Each Table In sSubRpt.Database.Tables
        '                Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
        '            Next
                    If sSubRpt.ParameterFields.Count > 0 Then
                        For intSub = 1 To sSubRpt.ParameterFields.Count
                            If Mid(sSubRpt.ParameterFields(intSub).ParameterFieldName, 1, 3) <> "Pm-" Then 'Que no sea un link del subreport
                                bParametros = True
                                GoTo Imprimir
                            End If
                        Next intSub
                    End If
                    Set sSubRpt = Nothing
                Next i
            End If
        Else '.xls
            MostrarPaginaFSNWeb frmLISPER.Name, "", tvwListados.selectedItem.Text, gParametrosGenerales.gsRutasFullstepWeb _
                , "&FileReport=" & Mid(tvwListados.selectedItem.key, 2) & "&ReportName=" & tvwListados.selectedItem.Text, gParametrosGenerales.gcolRutas("GSListadosDTSX")
            
            Exit Sub
        End If
    End If
    
Imprimir:
    'Si el listado tiene par�metros muestra la pantalla de par�metros.Sino, lo imprime
    If bParametros = False Then
        'No hay par�metros, imprime el informe
        If oReport Is Nothing Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        Set pv = New Preview
        pv.Hide
        pv.caption = tvwListados.selectedItem.Text
        Set pv.g_oReport = oReport
        pv.crViewer.ReportSource = oReport
        pv.crViewer.ViewReport
        pv.crViewer.Visible = True
        DoEvents
        pv.Show
        Set oReport = Nothing
        Screen.MousePointer = vbNormal
    
    Else
    
        'Muestra la pantalla con los par�metros del informe
        Set frmLISPERParam.oReport = oReport
        frmLISPERParam.caption = tvwListados.selectedItem.Text
        frmLISPERParam.CargarParametros
        
        Screen.MousePointer = vbNormal
        
        frmLISPERParam.Show vbModal
    End If
    
End Sub
