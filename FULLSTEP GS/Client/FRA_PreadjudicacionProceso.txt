
D�cision finale
---------------	

Cher/Ch�re @NOM_CONTACTO @APE_CONTACTO,

Ce communiqu� vous informe de la d�cision finale relative au processus d'achat auquel vous �tiez invit� �:  @DEN_PROCESO.

Veuillez noter que le processus n'a encore pas �t� valid�/ cl�tur�. 


R�sultat du comparatif du processus final�:
@RESULTADOCOMPARATIVA	

Fournisseurs adjug�s ayant les montants adjug�s correspondants�:
<!--COMIENZO_PROV-->
@PROVEEDOR
<!--FIN_PROV-->

Je me tiens � votre disposition pour tout renseignement compl�mentaire.


@NOM_COMPRADOR @APE_COMPRADOR  


Le Service des Achats
T�l�phone: @TFNO_COMPRADOR
E-mail: @MAIL_COMPRADOR

