Attribute VB_Name = "basPublic"
Option Explicit

'*************** DECLARACIONES EXTERNAS *********************
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Declare Function GetCapture Lib "user32" () As Long
Public Declare Function SetCapture Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function ReleaseCapture Lib "user32" () As Long
Public Declare Sub mouse_event Lib "user32" (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)
Public Declare Function BringWindowToTop Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function ShowWindow Lib "user32" (ByVal hWnd As Long, ByVal nCmdShow As Long) As Long
Public Declare Function SHGetFolderPath Lib "ShFolder" Alias "SHGetFolderPathA" (ByVal hWnd As Long, ByVal CSIDL As Long, ByVal TOKENHANDLE As Long, ByVal FLAGS As Long, ByVal lpPath As String) As Long
Public Declare Function RegQueryValueExAny Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, _
    lpType As Long, ByRef lpData As Any, lpcbData As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As Long, ByVal lpWindowName As String) As Long
Public Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hWnd As Long, lpdwProcessId As Long) As Long
    
'**************** TIPOS *************************
Public Type POINTAPI
    X As Long
    Y As Long
End Type
'Estructura a pasar con el mensaje TVM_HITTEST
Public Type TVHITTESTINFO
    pt As POINTAPI
    FLAGS As Long
    hitem As Long
End Type

'**************** ENUMERADOS ********************
Public Enum CSIDL_TYPES
    CSIDL_PERSONAL = &H5              ' My Documents
    CSIDL_SYSTEM = &H25               ' GetSystemDirectory()
    CSIDL_COMMON_APPDATA = &H23       ' All Users\Application Data
End Enum
Public Enum dwFlags_Type
    SHGFP_TYPE_CURRENT = 0     ' folder's current path
    SHGFP_TYPE_DEFAULT = 1     ' folder's default path
End Enum
Public Enum SHGetFolderPathResult_Type
    S_OK = &H0                 ' Success
    S_FALSE = &H1              ' The CSIDL is valid, but the folder does not exist.
    E_INVALIDARG = &H80070057  ' invalid CSIDL
End Enum
Public Enum OrigenBloqueoApertura
    NoBloqueo = 0
    ItemModificarValores = 1
    DatoAmbitoProce = 2
    SolicitudBuscar = 3
    ModificarGrupo = 4
    AnyadirPersonas = 5
    CambiarRol = 6
    CambiarCodigo = 7
    Eliminar = 8
    ModificarEsp = 9
    ModificarDatosGen = 10
    ModificarUON = 11
    ModificarConfig = 12
    ModificarAtrib = 13
    ModificarItems = 14
    ModificarPresup = 15
    responsable = 16
End Enum
Public Enum OrigenBloqueoComparativa
    NoBloqueo = 0
    proceso = 1
    Validando = 2
End Enum

Public Enum FSClientResource
    FRM_INFAHORRO_APLIGEN_PICLEGEND2 = 3
    FRM_INFAHORRO_APLIGEN_PICLEGEND = 4
    FRM_INFAHORRO_APLIMAT_PICLEGEND2 = 3
    FRM_INFAHORRO_APLIMAT_PICLEGEND = 4
    FRM_INFAHORRO_APLIPARCON_PICLEGEND2 = 3
    FRM_INFAHORRO_APLIPARCON_PICLEGEND = 4
    FRM_INFAHORRO_APLIPROY_PICLEGEND2 = 3
    FRM_INFAHORRO_APLIPROY_PICLEGEND = 4
    FRM_INFAHORRO_APLIUO_PICLEGEND2 = 3
    FRM_INFAHORRO_APLIUO_PICLEGEND = 4
    FRM_INFAHORRO_NEGDESDE_PICLEGEND2 = 3
    FRM_INFAHORRO_NEGDESDE_PICLEGEND = 4
    FRM_INFAHORRO_NEGEQPDESDE_PICLEGEND2 = 3
    FRM_INFAHORRO_NEGEQPDESDE_PICLEGEND = 4
    FRM_INFAHORRO_NEGEQPRESDESDE_PICLEGEND2 = 3
    FRM_INFAHORRO_NEGEQPRESDESDE_PICLEGEND = 4
    FRM_INFAHORRO_NEGEQPRESREU_PICLEGEND2 = 3
    FRM_INFAHORRO_NEGEQPRESREU_PICLEGEND = 4
    FRM_INFAHORRO_NEGEQPREU_PICLEGEND2 = 3
    FRM_INFAHORRO_NEGEQPREU_PICLEGEND = 4
    FRM_INFAHORRO_NEGMATDESDE_PICLEGEND2 = 3
    FRM_INFAHORRO_NEGMATDESDE_PICLEGEND = 4
    FRM_INFAHORRO_NEGMATREU_PICLEGEND2 = 3
    FRM_INFAHORRO_NEGMATREU_PICLEGEND = 4
    FRM_INFAHORRO_NEGREU_PICLEGEND2 = 3
    FRM_INFAHORRO_NEGREU_PICLEGEND = 4
    FRM_INFEVOLPRES_PICLEGEND2 = 5
    FRM_INFEVOLPRES_PICLEGEND = 6
End Enum

Public Enum TipoOptimaProceso
    OptimaProceso = 0
    OptimaGrupo = 1
    OptimaItem = 2
End Enum
      
'*************** CONSTANTES **********************
Public Const MOUSEEVENTF_ABSOLUTE = &H8000 '  absolute move
Public Const MOUSEEVENTF_LEFTDOWN = &H2 '  left button down
Public Const MOUSEEVENTF_LEFTUP = &H4 '  left button up
Public Const MOUSEEVENTF_MIDDLEDOWN = &H20 '  middle button down
Public Const MOUSEEVENTF_MIDDLEUP = &H40 '  middle button up
Public Const MOUSEEVENTF_MOVE = &H1 '  mouse move
Public Const MOUSEEVENTF_RIGHTDOWN = &H8 '  right button down
Public Const MOUSEEVENTF_RIGHTUP = &H10 '  right button up
Public Const MOUSETRAILS = 39
'Constantes de mensajes para el TreeView
Public Const TV_FIRST = &H1100
Public Const TVM_HITTEST = (TV_FIRST + 17)
Public Const TVHT_ONITEMSTATEICON = &H40
'Registro
Public Const HKEY_LOCAL_MACHINE As Long = &H80000002
Public Const ERROR_NONE As Long = 0
Public Const KEY_QUERY_VALUE As Long = &H1
Public Const FSEPConf As Boolean = False

#If ADMIN_OBLIGATORIO Then
    Public Const ADMIN_OBLIGATORIO  As Boolean = True
#Else
    Public Const ADMIN_OBLIGATORIO  As Boolean = False
#End If
'Ventanas
Public Const WM_CLOSE = &H10
'PAJANTIMAGE
'Display mode definitions
Public Const PJT_NONE            As Long = &H0
Public Const PJT_SCROLL          As Long = &H1
'Image part specifiers
Public Const PJT_FRAME           As Long = &H2000000
Public Const PJT_IMAGE           As Long = &H3000000
'Import modes
Public Const PJT_TONEWFRAME      As Long = &H20
Public Const PJT_TONEWIMAGE      As Long = &H30
'Import/export settings
Public Const PJT_CLIPBOARD         As Long = &H1
Public Const PJT_MEMJPEG           As Long = &H6
Public Const PJT_MEMPNG            As Long = &H8
'Resize flags
Public Const PJT_SIMPLE              As Long = &H0
Public Const PJT_INTERPOLATE         As Long = &H2

' ************ Objetos publicos del sistema ****************
Public oFSGSRaiz As CRaiz
Public gInstancia As String
Public gServidor As String
Public gBaseDatos As String
Public giDesacControlError As Integer
Public oUsuarioSummit As CUsuario
Public oGestorSeguridad As CGestorSeguridad
Public oGestorParametros As CGestorParametros
Public oDiccionarioSummit As CDiccionario
Public oSesionSummit As CSesion
Public oGestorInformes As CGestorInformes
Public gParametrosInstalacion As ParametrosInstalacion
Public oIdsMail As Email
Public gSubjectMultiIdiomaSMTP As String
'********************************************
'Se traspasa "oGestorIdiomas" y "oFSGSRaiz" a CUtilidades de FSGSLibrary par aque se pueda usar desde cualquier proyecto
'********************************************
Public oMensajes As FSGSLibrary.CMensajes
Public oGestorIdiomas As FSGSIdiomas.CGestorIdiomas
Public oGestorListadosPers As cGestorListadosPers
Public oGestorLisPerExternos As CGestorLisPerExternos
Public g_aFileNamesToDelete() As String
Public g_FormulariosInternet As Dictionary
Public g_bMDICerrar As Boolean 'Variable que indica que se cierra el MDI
Public g_CurPos As POINTAPI
Public g_iContadorBloqueo As Long
Public g_dicBloqueos As Dictionary
Public g_lIDRegistroEmail As Long 'Se usa para devolver el ID del mail guardado en REGISTRO_EMAIL
Public ofrmPedidos() As frmPEDIDOS

Declare Function SetWindowsHookEx Lib "user32" Alias _
"SetWindowsHookExA" (ByVal idHook As Long, ByVal lpfn As _
Long, ByVal hmod As Long, ByVal dwThreadId As Long) As Long

Declare Function UnhookWindowsHookEx Lib "user32" (ByVal _
Hhook As Long) As Long

Declare Function CallNextHookEx Lib "user32" (ByVal Hhook As _
Long, ByVal ncode As Long, ByVal wParam As Long, lParam As _
Any) As Long


Public Const WH_MOUSE = 7
Public Const HC_NOREMOVE = 3
Public Const WM_NCRBUTTONDOWN = &HA4
Public Const WM_RBUTTONDOWN = &H204

Global Hhook As Long 'Identificador del hook

''' <summary>
''' Sirve para saber si se ha metido una fecha correcta
''' </summary>
''' <Param>vFecha: Fecha a validar</Param>
''' <Return Value> Devuelve si es una fecha v�lida o no</Return Value>
''' <remarks>Llamada desde: GridAtrBuscar_BeforeColUpdate  ; Tiempo m�ximo: 0,2</remarks>
Private Function EsFechaCompleta(ByVal vFecha As Variant) As Boolean
EsFechaCompleta = False
If Not IsDate(vFecha) Then
    Exit Function
End If
Dim sFecha As String
Dim sDia As String
Dim sMes As String
Dim sA�o As String
Dim s As String
sFecha = vFecha
Do While Not sFecha = ""
    If IsNumeric(Left(sFecha, 1)) Then
        s = s & Left(sFecha, 1)
    Else
        If sDia = "" Then
            'No se admitir�n d�as de mas de dos d�gitos
            If Len(s) > 2 Then Exit Function
            sDia = s
        ElseIf sMes = "" Then
            'No se admitir�n meses de mas de dos d�gitos
            If Len(s) > 2 Then Exit Function
            sMes = s
        End If
        s = ""
    End If
    sFecha = Right(sFecha, Len(sFecha) - 1)
Loop
If sMes = "" Then
    'Falta el a�o porque el mes no lo ha rellenadop en el bucle
    Exit Function
Else
    sA�o = s
End If

If Day(CDate(vFecha)) <> val(sDia) Or Month(CDate(vFecha)) <> val(sMes) Or Year(CDate(vFecha)) <> val(sA�o) Then
    Exit Function
End If
EsFechaCompleta = True
End Function

''' <summary>
''' funci�n que obtiene la denominaci�n de los presupuestos para visualizarla en el detalle de la solicitud
''' </summary>
'''<returns>string con el texto</returns>
'''<param name="sValor">String con la concatenaci�n de los niveles de presupuestos almacenados en la solicitud</param>
'''<param name="iTipo">tipo de presupuesto  (1,2,3 o 4)</param>
'''<param name="bBajaLogica">Indica si hay que usar �ndice</param>
'''</params>
''' <remarks>Llamada desde: CargarGridCampos; MostrarPresSeleccionado </remarks>
Public Function GenerarDenominacionPresupuesto(ByVal sValor As String, ByVal iTipo As Integer, Optional ByRef bBajaLogica As Boolean, Optional ByVal bConPorcen As Boolean = True) As String

Dim arrPresupuestos() As String
Dim Ador As Ador.Recordset

Dim iTipoGS As TipoCampoGS

Select Case iTipo
    Case 1
        iTipoGS = Pres1
    Case 2
        iTipoGS = Pres2
    Case 3
        iTipoGS = Pres3
    Case 4
        iTipoGS = Pres4
End Select

arrPresupuestos = Split(sValor, "#")

Dim oPresup As Variant
Dim arrPresup As Variant
Dim iNivel As Integer
Dim lIdPresup As Long


Dim sTexto As String
sTexto = ""
Dim dPorcent As Double
For Each oPresup In arrPresupuestos
    arrPresup = Split(oPresup, "_")

    iNivel = arrPresup(0)
    dPorcent = Numero(arrPresup(2), ".")
    
    
    lIdPresup = arrPresup(1)
    Select Case iTipoGS
        Case TipoCampoGS.Pres1
        Dim oPresupuestos1 As CPresProyectosNivel1
            Set oPresupuestos1 = oFSGSRaiz.generar_CPresProyectosNivel1
            Select Case iNivel
                Case 1
                    Set Ador = oPresupuestos1.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
                Case 2
                    Set Ador = oPresupuestos1.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
                Case 3
                    Set Ador = oPresupuestos1.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
                Case 4
                    Set Ador = oPresupuestos1.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
            End Select
            Set oPresupuestos1 = Nothing
        
        Case TipoCampoGS.Pres2
            Dim oPresupuestos2 As CPresContablesNivel1
            Set oPresupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel1
            Select Case iNivel
                Case 1
                    Set Ador = oPresupuestos2.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
                Case 2
                    Set Ador = oPresupuestos2.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
                Case 3
                    Set Ador = oPresupuestos2.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
                Case 4
                    Set Ador = oPresupuestos2.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
            End Select
            Set oPresupuestos2 = Nothing
            
        Case TipoCampoGS.Pres3
            Dim oPresupuestos3  As CPresConceptos3Nivel1
            Set oPresupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel1
            Select Case iNivel
                Case 1
                    Set Ador = oPresupuestos3.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
                Case 2
                    Set Ador = oPresupuestos3.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
                Case 3
                    Set Ador = oPresupuestos3.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
                Case 4
                    Set Ador = oPresupuestos3.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
            End Select
            Set oPresupuestos3 = Nothing
            
        Case TipoCampoGS.Pres4
            Dim oPresupuestos4 As CPresConceptos4Nivel1
            Set oPresupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel1
            Select Case iNivel
                Case 1
                    Set Ador = oPresupuestos4.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
                Case 2
                    Set Ador = oPresupuestos4.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
                Case 3
                    Set Ador = oPresupuestos4.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
                Case 4
                    Set Ador = oPresupuestos4.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
            End Select
            Set oPresupuestos4 = Nothing
    End Select
                        
    If Not Ador Is Nothing Then
        If iTipoGS = TipoCampoGS.Pres1 Or iTipoGS = TipoCampoGS.Pres2 Then
            sTexto = sTexto & Ador.Fields("ANYO").Value & " - "
        End If
        If Not IsNull(Ador.Fields("PRES4").Value) Then
            sTexto = sTexto & Ador.Fields("PRES4").Value
        ElseIf Not IsNull(Ador.Fields("PRES3").Value) Then
            sTexto = sTexto & Ador.Fields("PRES3").Value
        ElseIf Not IsNull(Ador.Fields("PRES2").Value) Then
            sTexto = sTexto & Ador.Fields("PRES2").Value
        ElseIf Not IsNull(Ador.Fields("PRES1").Value) Then
            sTexto = sTexto & Ador.Fields("PRES1").Value
        End If
        If bConPorcen Then
            sTexto = sTexto & " (" & Format(dPorcent, "0.00%") & ")"
        End If
        sTexto = sTexto & "; "
        bBajaLogica = Ador.Fields("BAJALOG").Value
        
        Ador.Close
    End If
Next
GenerarDenominacionPresupuesto = sTexto

End Function



''' <summary>
''' Se actualiza el valor de una columna oculta para tener siempre el valor actualizado
''' </summary>'
''' <param name="oAmbito">Objeto DropDown con los valores de los ambitos posibles</param>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <remarks>Llamada desde frmEST, frmSeguimiento</remarks>
Public Sub ComboAmbito_CloseUp(ByRef oAmbito As SSDBDropDown, ByRef oAtrBuscar As SSDBGrid)
oAtrBuscar.Columns("AMBITO_VALOR").Value = oAmbito.Columns(0).Value
End Sub

'''' <summary>
''' Genera una colecci�n de de atributos a partir del objeto grid que se pasa
''' </summary>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <param name="sIdiTrue">Literal S�</param>
''' <param name="sIdiFalse">Literal No</param>
''' <returns>Objeto CAtributos</returns>
''' <remarks>Llamada desde: FrmEEST, frmSeguimiento</remarks>

Public Function MoverGridAtrBuscarAColleccion(ByRef oAtrBuscar As SSDBGrid, ByVal sIdiTrue As String, ByVal sIdiFalse As String) As CAtributosBuscar
Dim oAtributos As CAtributosBuscar
Dim i As Integer
Dim vValor As Variant
Dim oAtributo As CAtributoBuscar
With oAtrBuscar
    Set oAtributos = oFSGSRaiz.Generar_CAtributosBuscar
    .MoveFirst
    'Se recorre el objeto para llenar la colecci�n
    If .Rows > 0 Then
        For i = 0 To (.Rows - 1)
            If .Columns("IDTIPO").Value = TipoBoolean Then
                If .Columns("VALOR_ATRIB").Value = sIdiTrue Or .Columns("VALOR_ATRIB").Value = True Then
                    vValor = True
                ElseIf .Columns("VALOR_ATRIB").Value = sIdiFalse Or .Columns("VALOR_ATRIB").Value = False Then
                    vValor = False
                Else
                    vValor = ""
                End If
            Else
                vValor = .Columns("VALOR_ATRIB").Value
            End If
            oAtrBuscar.Columns("NUM").Value = i + 1
            Set oAtributo = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, vValor, .Columns("INTRO").Value, .Columns("MINIMO").Value, .Columns("MAXIMO").Value, .Columns("OPER").Value, nulltodbl0(.Columns("AMBITO_VALOR").Value), nulltodbl0(.Columns("LISTAEXTERNA").Value), i + 1)
            
            .MoveNext
        Next i
    End If
End With
Set MoverGridAtrBuscarAColleccion = oAtributos
End Function

'''' <summary>
''' Carga en la grid los valores metidos en al coleccion de atributos para el filtro
''' </summary>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <param name="oConf">objeto CConfSegPedidos cuando viene de frmSeguimiento, CConfVistaVisorSol cuando viene de frmEST</param>
''' <param name="sIdiTrue">Literal S�</param>
''' <param name="sIdiFalse">Literal No</param>
''' <param name="oAmbito">�mbito en el que se va a filtrar el atributo</param>
''' <param name="tAmbitoDefecto">En caso de que no haya �mbito es el ambito por defecto</param>
''' <returns>Grid (por referencia) Cargada con los atributos</returns>
''' <remarks>Llamada desde: FrmEEST, frmSeguimiento</remarks>

Public Sub CargarAtrBuscar(ByRef oAtrBuscar As SSDBGrid, ByRef oConf As Object, _
                        ByVal sIdiTrue As String, ByVal sIdiFalse As String, _
                        ByRef oAmbito As SSDBDropDown, ByVal tAmbitoDefecto As TAtributoAmbito)

Dim oatrib As CAtributoBuscar
Dim sLinea As String
Dim vValor As Variant
Dim i As Integer
With oAtrBuscar
    If Not oConf.AtrBuscar Is Nothing Then
        .RemoveAll
        If oConf.AtrBuscar.Count = 0 Then Exit Sub
        For Each oatrib In oConf.AtrBuscar
            Select Case oatrib.Tipo
                Case TipoBoolean
                    If oatrib.valor & "" <> "" Then
                        If CInt(oatrib.valor) = 0 Then
                            vValor = sIdiFalse
                        Else
                            vValor = sIdiTrue
                        End If
                    Else
                        vValor = ""
                    End If
                Case Else
                    vValor = oatrib.valor
            End Select
            If oatrib.AmbitoBuscar = 0 Then
                oatrib.AmbitoBuscar = tAmbitoDefecto
            End If
            ComboValorAtrBuscar_PositionList oAmbito, oatrib.AmbitoBuscar, oAtrBuscar
            sLinea = oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.Operador & Chr(m_lSeparador) & vValor & _
                    Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.idAtrib & Chr(m_lSeparador) & _
                    oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & vValor & Chr(m_lSeparador) & i & Chr(m_lSeparador) & _
                    oAmbito.Columns(1).Value & Chr(m_lSeparador) & oAmbito.Columns(0).Value & Chr(m_lSeparador) & oatrib.ListaExterna
            .AddItem sLinea, i
            DoEvents
            i = i + 1
        Next
    End If
End With
End Sub

''' <summary>
''' Antes de actualizar el valor se comprueban los atributos
''' </summary>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <param name="ColIndex">Columna de la Grid</param>
''' <param name="OldValue">Antiguo valor de la columna de la grid, pasado por referencia desde el propio evento</param>
''' <param name="Cancel">Parametro byref del propio evendo llamante</param>
''' <param name="sMsgMinMax">Literal para el mensaje entre baremos</param>
''' <remarks>Llamadas: frmEST, frmSeguimiento</remarks>
Public Sub GridAtrBuscar_BeforeColUpdate(ByRef oAtrBuscar As SSDBGrid, ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer, _
                                        sMsgMinMax As String)
With oAtrBuscar
    Select Case .Columns(ColIndex).Name
        Case "VALOR"
            'Comprobar que el valor introducido se corresponde con el tipo de atributo
            If .Columns("VALOR").Text <> "" Then
                Select Case UCase(.Columns("IDTIPO").Text)
                    Case TiposDeAtributos.TipoString
                    Case TiposDeAtributos.TipoNumerico
                        If (Not IsNumeric(.Columns("VALOR").Value)) Then
                            oMensajes.AtributoValorNoValido "TIPO2"
                            Cancel = True
                        Else
                            If .Columns("OPER").Text <> "" And .Columns("OPER").Text = "=" Then
                                If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                    Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value), sMsgMinMax)
                                End If
                            End If
                        End If
                        
                        If .Columns("OPER").Text = "" Then
                            .Columns("OPER").Text = "="
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value), sMsgMinMax)
                            End If
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If (Not EsFechaCompleta(.Columns("VALOR").Text) And .Columns("VALOR").Value <> "") Then
                            oMensajes.AtributoValorNoValido "TIPO3"
                            Cancel = True
                        Else
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                If CDate(.Columns("MINIMO").Value) > CDate(.Columns("VALOR").Value) Or CDate(.Columns("MAXIMO").Value) < CDate(.Columns("VALOR").Value) Then
                                    MsgBox Replace(Replace(sMsgMinMax, "@Valor1", .Columns("MINIMO").Value), "@Valor2", .Columns("MAXIMO").Value), vbInformation, "FULLSTEP"
                                    Cancel = True
                                End If
                            End If
                        End If
                    Case TiposDeAtributos.TipoBoolean
                End Select
                .Columns("VALOR_ATRIB").Value = .Columns("VALOR").Value
            End If
        Case "OPER"
            'Comprobar si se est� intentando dejar en blanco la col. del operando teniendo un valor introducido
            If .Columns("OPER").Text = vbNullString And .Columns("VALOR").Text <> vbNullString Then
                Cancel = True
            ElseIf .Columns("OPER").Text = "=" And .Columns("VALOR").Text <> vbNullString Then
                Select Case UCase(.Columns("IDTIPO").Text)
                    Case TiposDeAtributos.TipoNumerico
                        If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                            Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value), sMsgMinMax)
                        End If
                End Select
            End If
    End Select
End With

End Sub

''' <summary>
''' Se actualiza el valor de la columna "VALOR_ATRIB"
''' </summary>
''' <remarks>Llamada desde frmEST, frmSeguimiento</remarks>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
Public Sub GridAtrBuscar_Change(ByRef oAtrBuscar As SSDBGrid)
With oAtrBuscar
    .Columns("VALOR_ATRIB").Value = .Columns("VALOR").Value
End With
End Sub
''' <summary>Comprueba que un valor est� entre un m�nimo y un m�ximo</summary>
''' <remarks>Llamada desde: sdbgatributos_BeforeColUpdate</remarks>
''' <param name="dblValor">Valor</param>
''' <param name="dblMin">M�nimo</param>
''' <param name="dblMax">M�ximo</param>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

''' <summary>
''' Se recuperan los formatos de las columnas de la grid de busqueda por atributos
''' </summary>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <param name="LastRow">Variable byref del metodo llamante</param>
''' <param name="LastCol">Variable byref del metodo llamante</param>
''' <param name="oOper">DropDown de operadores posibles</param>
''' <param name="oValorAtrBuscar">DropDown de valores posibles en caso de que el atributo sea de tipo lista</param>
''' <param name="sIdiTrue">Literal S�</param>
''' <param name="sIdiFalse">Literal No</param>
''' <remarks>Llamadas desde frmEst y frmSeguimiento</remarks>

Public Sub GridAtrBuscar_RowColChange(ByRef oAtrBuscar As SSDBGrid, ByVal LastRow As Variant, ByVal LastCol As Integer, _
                                        ByRef oOper As SSDBDropDown, ByRef oValorAtrBuscar As SSDBDropDown, _
                                        ByVal sIdiTrue As String, ByVal sIdiFalse As String)
Dim bCargarCombo As Boolean
Dim oatrib As CAtributo
'Combo de operandos
With oAtrBuscar
    If .Columns("IDTIPO").Value = TipoNumerico Then
        .Columns("OPER").DropDownHwnd = oOper.hWnd
        .Columns("OPER").Locked = False
    Else
        .Columns("OPER").DropDownHwnd = 0
        .Columns("OPER").Locked = True
    End If
    'Combo de valores
    bCargarCombo = False
    If .col = .Columns("VALOR").Position Then
        Set oatrib = oFSGSRaiz.Generar_CAtributo
        Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(val(oAtrBuscar.Columns("ID_ATRIB").Value))
        If .Columns("INTRO").Value And oatrib.ListaExterna = 0 Then
            bCargarCombo = True
        Else
            If .Columns("IDTIPO").Value = TipoBoolean Then
                bCargarCombo = True
            End If
        End If
    End If
    If bCargarCombo Then
        oValorAtrBuscar.RemoveAll
        .Columns("VALOR").DropDownHwnd = oValorAtrBuscar.hWnd
        oValorAtrBuscar.Enabled = True
        ComboValorAtrBuscar_DropDown oValorAtrBuscar, oAtrBuscar, sIdiTrue, sIdiFalse
    Else
        .Columns("VALOR").DropDownHwnd = 0
    End If
    .Columns("VALOR").Value = .Columns("VALOR_ATRIB").Value
End With
End Sub

''' <summary>
''' Se cargan los valores en el combo
''' </summary>
''' <param name="oValorAtrBuscar">DropDown con los valores posibles de la lista del atributo</param>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <param name="sIdiTrue">Literal S�</param>
''' <param name="sIdiFalse">Literal No</param>

''' <remarks>Llamada desde frmEST y frmSeguimiento</remarks>
Public Sub ComboValorAtrBuscar_DropDown(ByRef oValorAtrBuscar As SSDBDropDown, ByRef oAtrBuscar As SSDBGrid, _
                                        ByVal sIdiTrue As String, ByVal sIdiFalse As String)
Dim oatrib As CAtributo
Dim iIdAtrib As Integer
Dim oLista As CValoresPond
Dim oElem As CValorPond
With oValorAtrBuscar
    .RemoveAll
    .AddItem ""
    iIdAtrib = val(oAtrBuscar.Columns("ID_ATRIB").Value)
    Set oatrib = oFSGSRaiz.Generar_CAtributo
    Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)
    If Not oatrib Is Nothing Then
        If oAtrBuscar.Columns("INTRO").Value = "1" Then
            oatrib.CargarListaDeValores
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                .AddItem oElem.ValorLista & Chr(m_lSeparador) & oElem.ValorLista
            Next
            Set oLista = Nothing
        Else
            'Campo de tipo boolean:
            If oAtrBuscar.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                .AddItem sIdiTrue & Chr(m_lSeparador) & sIdiTrue
                .AddItem sIdiFalse & Chr(m_lSeparador) & sIdiFalse
            End If
        End If
        Set oatrib = Nothing
    End If
End With
End Sub


Public Sub ComboValorAtrBuscar_InitColumnProps(ByRef oValorAtrBuscar As SSDBDropDown)
With oValorAtrBuscar
    .DataFieldList = "Column 0"
    .DataFieldToDisplay = "Column 1"
End With
End Sub


''' <summary>
''' Posiciona en la columna del combo para poner ltexto en la grid asociada
''' </summary>
''' <param name="oValorAtrBuscar">DropDown que tiene los valores posible de la lista</param>
''' <param name="Text">Texto del objeto oValorAtrBuscar</param>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <remarks>Llamada desde frmEST y frmSeguimiento</remarks>
Public Sub ComboValorAtrBuscar_PositionList(ByRef oValorAtrBuscar As SSDBDropDown, ByVal Text As String, ByRef oAtrBuscar As SSDBGrid)
Dim i As Long
Dim bm As Variant
On Error Resume Next
With oValorAtrBuscar
    .MoveFirst
    If Text <> "" Then
        For i = 0 To .Rows - 1
            bm = .GetBookmark(i)
            If UCase(Text) = UCase(Mid(.Columns(0).CellText(bm), 1, Len(Text))) Then
                oAtrBuscar.Columns(oAtrBuscar.col).Value = Mid(.Columns(1).CellText(bm), 1, Len(Text))
                .Bookmark = bm
                Exit For
            End If
        Next i
    End If
End With
End Sub


''' <summary>
''' Antes de seleccionar el valor se comprueban que sean validos
''' </summary>
''' <param name="Text">Valor Text del combo</param>
''' <param name="RtnPassed">Valor RtnPassed del Evento</param>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <param name="arOper">Matriz con los valores de los operadores</param>
''' <remarks>Llamada desde frmEST, frmSeguimiento</remarks>
Public Sub ComboOper_ValidateList(Text As String, RtnPassed As Integer, oAtrBuscar As SSDBGrid, ByRef arOper() As Variant)
Dim bExiste As Boolean
Dim i As Integer
If Text = "" Then
    RtnPassed = True
Else
    bExiste = False
    ''' Comprobar la existencia en la lista
    For i = 0 To UBound(arOper)
        If arOper(i) = oAtrBuscar.Columns(oAtrBuscar.col).Text Then
            bExiste = True
            Exit For
        End If
    Next
    If Not bExiste Then
        oMensajes.NoValido oAtrBuscar.Columns(oAtrBuscar.col).Text
        oAtrBuscar.Columns(oAtrBuscar.col).Text = ""
        RtnPassed = False
        Exit Sub
    End If
    RtnPassed = True
End If
End Sub



''' <summary>
''' Valida que los valores introducidos en la grid sean v�lidos en la lista
''' </summary>
''' <param name="Text">Texto a comprobar</param>
''' <param name="RtnPassed">valor por referencia del objeto desde donde se llama</param>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <param name="sIdiTrue">Literal S�</param>
''' <param name="sIdiFalse">Literal No</param>

''' <remarks>Llamada desde frmEST y frmSeguimiento</remarks>

Public Sub ComboValorAtrBuscar_ValidateList(Text As String, RtnPassed As Integer, ByRef oAtrBuscar As SSDBGrid, _
                                            ByVal sIdiTrue As String, ByVal sIdiFalse As String)
Dim bExiste As Boolean
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim oatrib As CAtributo
Dim iIdAtrib As Integer
If Text = "" Then
    RtnPassed = True
Else
    bExiste = False
    ''' Comprobar la existencia en la lista
    If oAtrBuscar.Columns("INTRO").Value = "1" Then
        iIdAtrib = val(oAtrBuscar.Columns("ID_ATRIB").Value)
        Set oatrib = oFSGSRaiz.Generar_CAtributo
        Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)
        If oatrib Is Nothing Then Exit Sub
        If oatrib.ListaExterna = 1 Then
            RtnPassed = True
            Exit Sub
        End If
        oatrib.CargarListaDeValores
        Set oLista = oatrib.ListaPonderacion
        For Each oElem In oLista
            If UCase(oElem.ValorLista) = UCase(oAtrBuscar.Columns(oAtrBuscar.col).Text) Then
                bExiste = True
                Exit For
            End If
        Next
    Else
        If oAtrBuscar.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
            If UCase(sIdiTrue) = UCase(oAtrBuscar.Columns(oAtrBuscar.col).Text) Then
                bExiste = True
            End If
            If UCase(sIdiFalse) = UCase(oAtrBuscar.Columns(oAtrBuscar.col).Text) Then
                bExiste = True
            End If
        End If
    End If
    If Not bExiste Then
        oAtrBuscar.Columns(oAtrBuscar.col).Text = ""
        oMensajes.NoValido oAtrBuscar.Columns(oAtrBuscar.col).caption
        RtnPassed = False
        Exit Sub
    End If
    RtnPassed = True
End If

End Sub



''' <summary>
''' Acci�n al scrollar en la grid porque se pueden nover todas las filas y se puede perder el valor
''' </summary>
''' <param name="oAtrBuscar">Grid a rellenar con los atributos</param>
''' <param name="Cancel">Variable byRef del procedimiento llamante</param>
''' <Remarks>Llamada desde frmEST y frmSeguimiento  </Remarks>
'
Public Sub GridAtrBuscar_Scroll(ByRef oAtrBuscar As SSDBGrid, Cancel As Integer)
'Cuando se hace scroll y no se vemn las filas , en caso de que la columna en la que se esta cuando se scrolla sea de tipo lista
'y lo que se va a cargar en la fiola que no se ve no est� dentro de los valores de esa lista, la columna no muestra el valor,
'Por eso se coloca en la columna y se pone la celda valor para que no sea de tipo lista
With oAtrBuscar
    .col = 0
    .Columns("VALOR").DropDownHwnd = 0
    .Bookmark = .Row
End With

End Sub


Public Function ComprobarValoresMaxMin(ByVal dblValor As Double, ByVal dblMin As Double, ByVal dblMax As Double, ByVal msMsgMinMax As String) As Boolean
    ComprobarValoresMaxMin = True
    
    If dblMin > dblValor Or dblMax < dblValor Then
        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", dblMin), "@Valor2", dblMax), vbInformation, "FULLSTEP"
        ComprobarValoresMaxMin = False
    End If
End Function

''' <summary>Carga el combo de operandos</summary>
''' <param name="arOper">Array de los operadores</param>
''' <param name="sdbddOper">Objeto DropDownCombo donde se cargar�n los valores del Array</param>
''' <remarks>Llamada desde: frmArtFiltrar, frmEST, frmSeguimiento</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <return> </return>
Public Sub CargarComboOperandos(ByRef arOper As Variant, ByRef sdbddOper As SSDBDropDown)
Dim i As Integer
arOper = Array("=", ">", ">=", "<", "<=")
sdbddOper.RemoveAll
sdbddOper.AddItem ""
For i = 0 To UBound(arOper)
    sdbddOper.AddItem arOper(i) & Chr(m_lSeparador) & arOper(i)
Next
End Sub


' Funci�n callback

Public Function MouseProc(ByVal uMSG As Long, _
ByVal wParam As Long, _
ByVal lParam As Long) As Long

If uMSG < 0 Then
MouseProc = CallNextHookEx(Hhook, uMSG, wParam, lParam)
End If

' Se comprueba si el mensaje es el bot�n derecho del rat�n
' Si es as� se ignora y se produce un beep
' De lo contrario se da la oportunidad a otros hooks de procesar el mensaje.

If uMSG <> HC_NOREMOVE And (wParam = WM_RBUTTONDOWN Or _
wParam = WM_NCRBUTTONDOWN) Then
Beep
MouseProc = 1
Else
MouseProc = CallNextHookEx(Hhook, uMSG, wParam, lParam)
End If
End Function

'Funci�n que devuelve el a�o de imputaci�n en funci�n de si el item est� adjudicado(Anyo de ITEM_ADJ) o no (INSTANCIA_PRES5_DESGLOSE)
Public Function Obtener_AnyoImputacion_Item(ByVal Anyo As Integer, ByVal GMN As String, ByVal Proce As Long, ByVal Item As Long, _
                                            ByVal Solicit As Long, ByVal LinSolicit As Integer) As Integer
Dim b As Boolean
Dim oParam As CParametroSM
Dim oItem As CItem
Dim AnyoImputacion As Integer
Dim teserror As TipoErrorSummit
'Iniciamos el valor
teserror.NumError = TESnoerror
Obtener_AnyoImputacion_Item = 0
b = False

'Vamos a mirar si est� activo el par�metro plurianual en alguna partida presupuestaria y si no lo est� el menu lo ocultamos
If g_oParametrosSM Is Nothing Then Exit Function
For Each oParam In g_oParametrosSM
    If oParam.Plurianual Then
        b = True
        Exit For
    End If
Next
Set oParam = Nothing
'Si no esta activo el par�metro no miramos m�s
If Not b Then Exit Function

Set oItem = oFSGSRaiz.Generar_CItem
Set oItem.proceso = oFSGSRaiz.Generar_CProceso
oItem.proceso.Anyo = Anyo
oItem.proceso.GMN1Cod = GMN
oItem.proceso.Cod = Proce
oItem.Id = Item
oItem.SolicitudId = Solicit
oItem.IdLineaSolicit = LinSolicit

teserror = oItem.Obtener_AnyoImputacion(AnyoImputacion)
Obtener_AnyoImputacion_Item = AnyoImputacion
Set oItem = Nothing
End Function

Public Sub PonerHook(HookForm As Form)
Hhook = SetWindowsHookEx(WH_MOUSE _
, AddressOf MouseProc _
, 0 _
, GetWindowThreadProcessId(HookForm.hWnd, 0)) ' Id del
' thread que cre� la ventana
End Sub

Public Sub QuitarHook()
UnhookWindowsHookEx Hhook
End Sub

''' <summary>
''' Envia un correo
''' </summary>
''' <param name="sTo">A quien/es va el correo</param>
''' <param name="sSubject">Subject del mail</param>
''' <param name="sBody">Cuerpo del mail</param>
''' <param name="sAttachments">Adjuntos del mail</param>
''' <param name="sCC">Carbon Copy del mail</param>
''' <param name="sBCC">Blind Carbon Copy del mail</param>
''' <param name="bReadReceipt">Pedir aviso de lectura si/no</param>
''' <param name="bHTML">Formato Html o text del mail</param>
''' <param name="Origen">Q comunicacion del mail</param>
''' <param name="Prove">Prove del mail</param>
''' <param name="NombreApellidos">nombre y Apellidos del contacto del mail</param>
''' <param name="sSubjectFrom">Multidioma para (De parte de: ###)</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmMensaje  basUtilidades/ComponerMensaje; Tiempo m�ximo:0</remarks>
Public Function EnviarMensaje(ByVal sTo As String, ByVal sSubject As String, ByVal sBody As String, Optional ByVal sAttachments As Variant _
, Optional ByVal sCC As String = "", Optional ByVal sBCC As String = "", Optional ByVal bReadReceipt As Boolean = False, Optional ByVal bHTML As Boolean = False _
, Optional ByVal Origen As String = "", Optional ByVal Prove As String = "", Optional ByVal NombreApellidos As String = "" _
, Optional ByVal sSubjectFrom As String = "", Optional lIdInstancia As Long, Optional ByVal entidadNotificacion As entidadNotificacion, _
  Optional ByVal tipoNotificacion As TipoNotificacionEmail, Optional Anyo As Integer, Optional GMN1 As String, Optional ByVal Proce As Long, _
  Optional sToName As String = "", Optional sSession As String = "", Optional sRemitenteEmpresa As String = "") As TipoErrorSummit
    Dim errormail As TipoErrorSummit
    Dim sTemp As String
    Dim From As String
    Dim iError As Long
    Dim StrError As String
    Dim i As Integer
    Dim auxAttachment As String
    Dim lIdError As Long
    Dim iErrorMail As Integer  '0 - Antes del envio, 1- En el envio, 2- Sin errir en envio, 3- Error en envio
    Dim bErrorMail As Boolean
    Dim sRemitente As String
    Dim oProce As CProceso
    
    On Error GoTo Error

    iErrorMail = 0
    errormail.NumError = TESnoerror
    sTemp = DevolverPathFichTemp

    If oIdsMail Is Nothing Then
        Set oIdsMail = IniciarSesionMail
    End If
           
    If oUsuarioSummit.Persona Is Nothing Then
        From = oUsuarioSummit.mail
    Else
        From = oUsuarioSummit.Persona.mail
    End If

    If Not IsMissing(sAttachments) Then
        auxAttachment = ""
        For i = 0 To UBound(sAttachments) - 1
            auxAttachment = auxAttachment & sAttachments(i) & ";"
        Next
    End If
    
    If sSubjectFrom = "" Then
        If Not gParametrosGenerales.gbUsarRemitenteEmpresa Then
            sSubjectFrom = gSubjectMultiIdiomaSMTP
        End If
    End If
    
    sSubject = sSubject & " " & Replace(sSubjectFrom, "###", From)

    iErrorMail = 1
    sRemitente = gParametrosGenerales.gsSMTPCuentaMail
    
    'Si hay que usar el remitente de la empresa lo sacamos de la empresa del proceso
    If gParametrosGenerales.gbUsarRemitenteEmpresa Then
        If Proce > 0 Then
            Set oProce = oFSGSRaiz.Generar_CProceso
            oProce.Anyo = Anyo
            oProce.GMN1Cod = GMN1
            oProce.Cod = Proce
            sRemitente = oProce.SacarRemitenteEmpresa()
            sRemitente = IIf(sRemitente = "", gParametrosGenerales.gsSMTPCuentaMail, sRemitente)
        ElseIf sRemitenteEmpresa <> "" Then
            sRemitente = sRemitenteEmpresa
        End If
    End If
    If sRemitente <> gParametrosGenerales.gsSMTPCuentaMail Then
        From = sRemitente
    End If
    oIdsMail.EnviarMail sRemitente, From, sSubject, sTo, sBody, NombreApellidos, , sCC, sBCC, auxAttachment, sTemp, bHTML, , bReadReceipt, iError, StrError
    
    lIdError = -1
    
    If sBody <> "" Then
        If iError = 2 Then
            'Ha enviado bien
            iErrorMail = 2
            bErrorMail = False
            StrError = ""
        Else
            iErrorMail = 3
            'Ha ido mal
            bErrorMail = True
            errormail.NumError = TESMailSMTP
            errormail.Arg1 = iError
            errormail.Arg2 = StrError & "$$" & CStr(iError)
        End If
    Else
        iErrorMail = 3
        bErrorMail = True
        If (iError < 2) Or (iError >= 4) Then
            errormail.NumError = TESMailSMTP
            errormail.Arg1 = iError
            errormail.Arg2 = StrError & "$$" & CStr(iError)
        Else
            StrError = "No hay cuerpo"
            errormail.NumError = TESMailSMTP
            errormail.Arg1 = "El mail no tiene cuerpo"
            errormail.Arg2 = StrError & "$$2"
        End If
    End If
    
    g_lIDRegistroEmail = oFSGSRaiz.GrabarEnviarMensaje(sSubject, sTo, sCC, sBCC, sRemitente, sBody, bReadReceipt, bHTML, bErrorMail, StrError, sAttachments, Origen, Prove, NombreApellidos, lIdError, oUsuarioSummit.Cod, lIdInstancia, entidadNotificacion, tipoNotificacion, Anyo, GMN1, Proce, sToName, sTemp, sSession)
    EnviarMensaje = errormail
    Exit Function

Error:

    errormail.NumError = erroressummit.TESMailSMTP
    errormail.Arg1 = err.Number
    errormail.Arg2 = err.Description
    Dim sCausa As String
    lIdError = -1
    Select Case iErrorMail
    Case 1 'Ha dado error el FSNLibraryCOM
        sCausa = "Error en FSNLibraryCOM.EnviarMail."
    Case 2  'Ha dado error despues de comunicar
        sCausa = "Error despues de enviar mail correctamente."
    Case 2  'Ha dado error despues de comunicar
        sCausa = "Error despues de enviar mail con error."
    End Select
        
    EnviarMensaje = errormail
End Function

''' <summary>Crea el Objeto SmtClient</summary>
''' <returns>Objeto SmtClient</returns>
''' <remarks>Llamada desde: pantallas q notifican ; Tiempo m�ximo: 0,2</remarks>
Public Function IniciarSesionMail() As Email
    Dim Email As Email
    Dim errormail As TipoErrorSummit
    
    On Error GoTo Error
  
    Set Email = New Email
    
    Email.SmtpServer = gParametrosGenerales.gsSMTPServer
    Email.SmtpPort = gParametrosGenerales.giSMTPPort
    Email.SmtpAuthentication = gParametrosGenerales.giSMTPAutent
    Email.SmtpEnableSsl = gParametrosGenerales.gbSMTPSSL
    Email.SmtpDeliveryMethod = gParametrosGenerales.giSMTPMetodoEntrega
    Email.SmtpUsu = gParametrosGenerales.gsSMTPUser
    Email.SmtpPwd = gParametrosGenerales.gsSMTPPwd
    Email.SmtpDominio = gParametrosGenerales.gsSMTPDominio

    Email.ClaseCom = 1
    
    Set IniciarSesionMail = Email
    Exit Function

Error:

    errormail.NumError = TESMailSMTP
    errormail.Arg1 = err.Number
    errormail.Arg2 = err.Description

    basErrores.TratarError errormail
End Function

''' <summary>Env�a las notificaciones de paso de un proveedor de QA a real</summary>
''' <param name="iAnyo">Anyo del proceso</param>
''' <param name="sGMN1">GMN1 del proceso</param>
''' <param name="lProce">Cod. del proceso</param>
''' <param name="sProve">Cod. proveedor</param>
''' <remarks>Llamada desde: frmADJ.Adjudicar</remarks>

Public Function NotificarPasoProveedorQAaReal(ByVal bDesdePedido As Boolean, Optional ByRef oProceso As CProceso, Optional ByVal sProve As String = "", Optional ByVal iOrdenAnyo As Integer, _
        Optional ByVal lNumPedido As Long, Optional ByVal lOrdenNumero As Long, Optional ByRef oAdjudicadoProves As CAdjsGrupo)
    Dim oPersonas As CPersonas
    Dim rsNotif As ADODB.Recordset
    Dim oProve As CProveedor
    Dim sCuerpo As String
    Dim sAsunto As String
    Dim sNombreApel As String
    
    Dim errormail As TipoErrorSummit
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo TratarError
    
    'Obtener los usuarios a los que hay que notificar
    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    If sProve <> "" Then
        Set rsNotif = oPersonas.DevolverNotificacionesProveQAaReal(sProve:=sProve)
    Else
        Set rsNotif = oPersonas.DevolverNotificacionesProveQAaReal(oProceso.Anyo, oProceso.GMN1Cod, oProceso.Cod)
    End If
    If Not rsNotif.EOF Then
        rsNotif.MoveFirst
        While Not rsNotif.EOF
            Dim oCEMailQA As CEmailQA
            Set oCEMailQA = GenerarCEmailQA(oMensajes, gParametrosInstalacion, basParametros.gLongitudesDeCodigos)
            sCuerpo = oCEMailQA.GenerarMensajePasoProveedorQAaReal(rsNotif("PROVECOD"), rsNotif("PROVEDEN"), IIf(IsNull(rsNotif("IDIOMA")), gParametrosInstalacion.gIdioma, rsNotif("IDIOMA")), SQLBinaryToBoolean(rsNotif("TIPOEMAIL")), _
                        bDesdePedido, oProceso, iOrdenAnyo, lNumPedido, lOrdenNumero, oAdjudicadoProves)
            Set oCEMailQA = Nothing
            
            If sCuerpo <> "" Then
                sAsunto = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADJ, gParametrosInstalacion.gIdioma, 288)(0)
                sNombreApel = rsNotif("APE") & IIf(NullToStr(rsNotif("NOM")) = "", "", ", " & NullToStr(rsNotif("NOM")))
                
                errormail = ComponerMensaje(rsNotif("EMAIL"), sAsunto, sCuerpo, , , SQLBinaryToBoolean(SQLBinaryToBoolean(rsNotif("TIPOEMAIL"))), rsNotif("PROVECOD"), sNombreApel, entidadNotificacion:=Calificaciones, _
                                            tipoNotificacion:=AvisoPasoProveQAaReal, sToName:=IIf(NullToStr(rsNotif("NOM")) = "", "", NullToStr(rsNotif("NOM")) & " ") & rsNotif("APE"))
                                                        
                If errormail.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError errormail
                    FinalizarSesionMail
                End If
                
                Set oProve = oFSGSRaiz.generar_CProveedor
                oProve.Cod = rsNotif("PROVECOD")
                oProve.PasoQAaRealNotificado rsNotif("ID")
            End If
            
            rsNotif.MoveNext
        Wend
    End If
    
Salir:
    Set oProve = Nothing
    Set oPersonas = Nothing
    Exit Function
TratarError:
    If err.Number <> 0 Then Call oFSGSRaiz.TratarError("M�dulo", "basPublic", "AbrirFormCalendar", err, Erl)
    Resume Salir
End Function

'''*******************************************************************************************************
''' Conjunto de metodos utilizados desde frmPROVEMatPorProve y frmPROVEProvePorMat
''' para notificar via mail a los usuarios de gesti�n de Materiales de QA
''' que se han asignado|desasignado materiales de QA a proveedores potenciales y/o de calidad.
''' por: Gorka Areitioaurtena
''' fecha: 03/04/2006
'''*******************************************************************************************************

Public Function NotificarAsigMatQA(ByVal sMailSubject As String, Optional ByVal oProvesAsig As CProveedores = Nothing, Optional ByVal oGMN1Asig As CGruposMatNivel1 = Nothing, Optional ByVal oGMN2Asig As CGruposMatNivel2 = Nothing, Optional ByVal oGMN3Asig As CGruposMatNivel3 = Nothing, Optional ByVal oGMN4Asig As CGruposMatNivel4 = Nothing, _
                                    Optional ByVal oProvesDesAsig As CProveedores = Nothing, Optional ByVal oGMN1DesAsig As CGruposMatNivel1 = Nothing, Optional ByVal oGMN2DesAsig As CGruposMatNivel2 = Nothing, Optional ByVal oGMN3DesAsig As CGruposMatNivel3 = Nothing, Optional ByVal oGMN4DesAsig As CGruposMatNivel4 = Nothing) As TipoErrorSummit
    Dim teserror As TipoErrorSummit
    Dim rsUsu As ADODB.Recordset
    Dim rsProveMat As ADODB.Recordset
    Dim oUsuarios As CUsuarios
    Dim oProveedores As CProveedores
    Dim bAsigMatProveReal As Boolean
    Dim bAsigMatProvePot As Boolean
    Dim bDesAsigMatProveReal As Boolean
    Dim bDesAsigMatProvePot As Boolean
    Dim sAsunto As String
    Dim sCuerpo As String
    Dim arCodIdiomas() As Variant 'array que contendr� los distintos codigos de Idioma de los usuarios
    Dim arTos() As Variant 'emails de los usuarios de cada idioma
    Dim arHTMLs() As Variant 'Booleanos que indicaran si el email tiene que ser HTML o Texto
    Dim i As Integer 'Indice para estos arrays
    Dim bEncontrado As Boolean
    
    'Obtenemos los emails de los notificados:
    Set oUsuarios = oFSGSRaiz.Generar_CUsuarios
    teserror = oUsuarios.getEmailsUsuariosGestMatQA(rsUsu)
    If teserror.NumError = TESnoerror Then
        If Not rsUsu.EOF Then
            If rsUsu("NOTIF_USU_ASIG_MAT_VAL") = 1 Then
                bAsigMatProveReal = True
            Else
                bAsigMatProveReal = False
            End If
            If rsUsu("NOTIF_USU_ASIG_MAT_POT") = 1 Then
                bAsigMatProvePot = True
            Else
                bAsigMatProvePot = False
            End If
            If rsUsu("NOTIF_USU_DESASIG_MAT_VAL") = 1 Then
                bDesAsigMatProveReal = True
            Else
                bDesAsigMatProveReal = False
            End If
            If rsUsu("NOTIF_USU_DESASIG_MAT_POT") = 1 Then
                bDesAsigMatProvePot = True
            Else
                bDesAsigMatProvePot = False
            End If
                           
            'Obtenemos la Relaci�n Proveedores - Materiales Asignados|Desasignados
            Set oProveedores = oFSGSRaiz.generar_CProveedores
            
            teserror = oProveedores.getRelacionAsigProveMatQA(rsProveMat, bAsigMatProveReal, bAsigMatProvePot, bDesAsigMatProveReal, bDesAsigMatProvePot, oProvesAsig, oGMN1Asig, oGMN2Asig, oGMN3Asig, oGMN4Asig, oProvesDesAsig, oGMN1DesAsig, oGMN2DesAsig, oGMN3DesAsig, oGMN4DesAsig)
            If teserror.NumError = TESnoerror Then
                If Not rsProveMat Is Nothing Then
                    If Not rsProveMat.EOF Then
                        sAsunto = sMailSubject
                        
                        ReDim arCodIdiomas(0)
                        ReDim arHTMLs(0)
                        ReDim arTos(0)
                        
                        arCodIdiomas(0) = NullToStr(rsUsu("IDIOMA").Value)
                        arHTMLs(0) = SQLBinaryToBoolean(rsUsu("TIPOEMAIL").Value)
                        arTos(0) = NullToStr(rsUsu("EMAIL").Value)
                        
                        rsUsu.MoveNext
                                                
                        While Not rsUsu.EOF
                            bEncontrado = False
                            For i = LBound(arCodIdiomas) To UBound(arCodIdiomas)
                                If arCodIdiomas(i) = NullToStr(rsUsu("IDIOMA").Value) And _
                                 arHTMLs(i) = SQLBinaryToBoolean(rsUsu("TIPOEMAIL").Value) Then
                                    bEncontrado = True
                                    Exit For
                                End If
                            Next
                            If bEncontrado Then
                                arTos(i) = arTos(i) & ";" & NullToStr(rsUsu("EMAIL").Value)
                            Else
                                ReDim Preserve arCodIdiomas(UBound(arCodIdiomas) + 1)
                                ReDim Preserve arHTMLs(UBound(arHTMLs) + 1)
                                ReDim Preserve arTos(UBound(arTos) + 1)
                                
                                arCodIdiomas(UBound(arCodIdiomas)) = NullToStr(rsUsu("IDIOMA").Value)
                                arHTMLs(UBound(arHTMLs)) = SQLBinaryToBoolean(rsUsu("TIPOEMAIL").Value)
                                arTos(UBound(arTos)) = NullToStr(rsUsu("EMAIL").Value)
                            End If
                                                       
                            rsUsu.MoveNext
                        Wend
                                                    
                        For i = LBound(arCodIdiomas) To UBound(arCodIdiomas)
                            Dim oCEMailQA As CEmailQA
                            Set oCEMailQA = GenerarCEmailQA(oMensajes, gParametrosInstalacion, basParametros.gLongitudesDeCodigos)
                            sCuerpo = oCEMailQA.GenerarMensajeNotificarAsigMatQA(rsProveMat, arCodIdiomas(i), arHTMLs(i))
                            Set oCEMailQA = Nothing
                            
                            'Y lo mandamos
                            If sCuerpo <> "" Then
                                teserror = ComponerMensaje(arTos(i), sAsunto, sCuerpo, , , arHTMLs(i))
                            Else
                                teserror = ComponerMensaje(arTos(i), sAsunto, sCuerpo, , , arHTMLs(i))
                                If teserror.NumError = TESnoerror Then
                                    teserror.NumError = TESNotifUsuMatQA
                                    teserror.Arg1 = arTos(i)
                                End If
                                basErrores.TratarError teserror
                                teserror.NumError = TESnoerror
                            End If
                         Next
                    End If
                End If
            End If
        End If
    
    End If
    
    NotificarAsigMatQA = teserror
End Function

''' <summary>
''' Compone un mensaje.
''' </summary>
''' <param name="sTo">A quien/es va el correo</param>
''' <param name="sSubject">Subject del mail</param>
''' <param name="sBody">Cuerpo del mail</param>
''' <param name="sAttachments">Adjuntos del mail</param>
''' <param name="sOrigen">Origen</param>
''' <param name="bHTML">Formato Html o text del mail</param>
''' <param name="Prove">Prove del mail</param>
''' <param name="NombreApellidos">nombre y Apellidos del contacto del mail</param>
''' <param name="sSubjectFrom">Multidioma para (De parte de: ###)</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: basUtilidades, frmSELProveResp, frmESTRMat, frmADJAnya, frmSolicitudAprobar, frmSolicitudAsignarComp, frmSolicitudGestionar, frmPedidosEmision1; Tiempo m�ximo:0</remarks>
''' <remarks>Revisado por: LTG  Fecha: 02/05/2011</remarks>
Public Function ComponerMensaje(ByVal sTo As String, ByVal sSubject As String, ByVal sBody As String, Optional ByVal sAttachments As Variant, _
                                Optional ByVal sOrigen As Variant, Optional ByVal bHTML As Boolean, Optional ByVal Prove As String, _
                                Optional ByVal NombreApellidos As String, Optional ByVal sSubjectFrom As String = "", Optional ByVal lIdInstancia As Long, _
                                Optional ByVal entidadNotificacion As entidadNotificacion, Optional ByVal tipoNotificacion As TipoNotificacionEmail, _
                                Optional ByVal Anyo As Integer, Optional ByVal GMN1 As String, Optional ByVal Proce As Long, _
                                Optional sToName As String = "", Optional sCC As String = "", Optional sCCO As String = "", Optional sRemitenteEmpresa As String = "") As TipoErrorSummit
    Dim errormail As TipoErrorSummit
    Dim i As Integer
    Dim sTemp As String
    Dim sSize As String
    Dim sAux As String
    
    sAux = Replace(sBody, Chr(13) & Chr(10), Chr(13))
    sAux = Replace(sAux, Chr(13), Chr(13) & Chr(10))
                
    errormail.NumError = TESnoerror
    
    If gParametrosInstalacion.gbMostrarMail And sBody <> "" Then
        DoEvents
        frmMensaje.txtAsunto = sSubject
        DoEvents
        frmMensaje.txtDestino = sTo
        DoEvents
        frmMensaje.g_sCuerpo = sAux
        frmMensaje.g_bHTML = bHTML
        frmMensaje.g_sProve = Prove
        frmMensaje.g_sNombreApellidos = NombreApellidos
        frmMensaje.g_sSubjectFrom = sSubjectFrom
        'parametros para registro mail
        frmMensaje.lIdInstancia = lIdInstancia
        frmMensaje.entidadNotificacion = entidadNotificacion
        frmMensaje.tipoNotificacion = tipoNotificacion
        frmMensaje.Anyo = Anyo
        frmMensaje.GMN1 = GMN1
        frmMensaje.Proce = Proce
        frmMensaje.sToName = sToName
        frmMensaje.sRemitenteEmpresa = sRemitenteEmpresa
        If sCC <> "" Then frmMensaje.txtCc.Text = sCC
        If sCCO <> "" Then frmMensaje.txtBcc.Text = sCCO
        DoEvents
        
        If Not IsMissing(sAttachments) Then
            For i = 0 To UBound(sAttachments)
                If sAttachments(i) <> "" Then
                    sTemp = FSGSLibrary.DevolverPathFichTemp
                    sSize = " (" & TamanyoAdjuntos(FileLen(sTemp & sAttachments(i)) / 1024) & " KB)"
                    frmMensaje.lstvwAdjuntos.ListItems.Add , "ADJ" & CStr(i), sAttachments(i) & sSize, , "ADJ"
                    frmMensaje.lstvwAdjuntos.ListItems.Item("ADJ" & i).Tag = sAttachments(i)
                End If
            Next
        End If
        If Not IsMissing(sOrigen) Then
            frmMensaje.g_sOrigen = sOrigen
        End If
        
        frmMensaje.CargarCuerpoMensaje
        frmMensaje.Show vbModal
        
        errormail = frmMensaje.errormail()
    Else
        If IsMissing(sOrigen) Then sOrigen = ""
        If sTo = "" Then
            Dim sIdiDirNoValida As String
            sIdiDirNoValida = oMensajes.CargarTexto(FRM_MENSAJE, 10)
            oMensajes.errormail sIdiDirNoValida
            Exit Function
        Else
            If Not ComprobarEmail(sTo) Then
                oMensajes.MensajeOKOnly 1132
                Exit Function
            End If
        End If
        errormail = EnviarMensaje(sTo, sSubject, sAux, sAttachments, Trim(gParametrosInstalacion.gsRecipientCC), Trim(gParametrosInstalacion.gsRecipientBCC), _
                                gParametrosInstalacion.gbAcuseRecibo, bHTML, sOrigen, Prove, NombreApellidos, sSubjectFrom, lIdInstancia, entidadNotificacion, _
                                tipoNotificacion, Anyo, GMN1, Proce, sToName, , sRemitenteEmpresa)
    End If
    
    ComponerMensaje = errormail
    
End Function

''' <summary>Compone el subject de un correo </summary>
''' <param name="sSubject">Subject original</param>
''' <param name="iProceAnyo">Anyo del proceso</param>
''' <param name="sProceGMN1">GMN1</param>
''' <param name="iProceCod">Codigo del proceso</param>
''' <param name="sProceDen">Denominaci�n del proceso</param>
''' <returns>String con el subject definitivo</returns>
''' <remarks>Llamada desde: ComponerMensaje; Tiempo m�ximo:0</remarks>
''' <remarks>Revisado por: LTG  Fecha: 02/05/2011</remarks>

Public Function ComponerAsuntoEMail(ByVal sSubject As String, ByVal iProceAnyo As Integer, ByVal sProceGMN1 As String, _
                                     ByVal iProceCod As Long, ByVal sProceDen As String) As String
    Dim lPos As Long
    Dim sAsunto As String
    Dim sTag As String
    
    sAsunto = sSubject
    
    'B�squeda y cambio de tags
    lPos = InStr(1, UCase(sAsunto), "[PROCECOD]")
    While lPos > 0
        sTag = Mid(sAsunto, lPos, 10)
        sAsunto = Replace(sAsunto, sTag, CStr(iProceAnyo) & "/" & sProceGMN1 & "/" & CStr(iProceCod))
        lPos = InStr(lPos + Len(CStr(iProceAnyo) & "/" & sProceGMN1 & "/" & CStr(iProceCod)), UCase(sAsunto), "[PROCECOD]")
    Wend
    lPos = InStr(1, UCase(sAsunto), "[PROCEDEN]")
    While lPos > 0
        sTag = Mid(sAsunto, lPos, 10)
        sAsunto = Replace(sAsunto, sTag, sProceDen)
        lPos = InStr(lPos + Len(sProceDen), UCase(sAsunto), "[PROCEDEN]")
    Wend
    
    ComponerAsuntoEMail = sAsunto
End Function

Public Sub EscribirEspecificacionADisco(ByRef oEsp As CEspecificacion, ByVal TipoLectura As TipoEspecificacion)
    Dim sFileName As String
    Dim teserror As TipoErrorSummit
    Dim sTemp As String

    sTemp = DevolverPathFichTemp

    sFileName = sTemp & oEsp.nombre
    
    ' Cargamos el contenido en la esp.
    
    teserror = oEsp.ComenzarLecturaData(TipoLectura)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
    Else
        oEsp.LeerAdjunto oEsp.Id, TipoLectura, oEsp.DataSize, sFileName
    End If
End Sub

Public Sub EscribirReglaADisco(ByRef oregla As CReglaSubasta)
    Dim sFileName As String
    Dim teserror As TipoErrorSummit
    Dim DataFile As Integer
    Dim Fl As Long
    Dim Chunks As Integer
    Dim Fragment As Integer
    Dim Chunk() As Byte
    Dim indChunk As Integer
    Dim sTemp As String

    sTemp = DevolverPathFichTemp

    sFileName = sTemp & oregla.Nom
    
    ' Cargamos el contenido en la esp.
    
    teserror = oregla.ComenzarLecturaData
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
    Else
        DataFile = 1
        'Abrimos el fichero para escritura binaria
        Open sFileName For Binary Access Write As DataFile
        Fl = oregla.DataSize
        Chunks = Fl \ giChunkSize
        Fragment = Fl Mod giChunkSize
        If Fragment > 0 Then
            ReDim Chunk(Fragment - 1)
            Chunk() = oregla.ReadData(Fragment)
            Put DataFile, , Chunk()
        End If
    
        ReDim Chunk(giChunkSize - 1)

        For indChunk = 1 To Chunks
            Chunk() = oregla.ReadData(giChunkSize)
            Put DataFile, , Chunk()
        Next

        oregla.FinalizarLecturaData
    
        'Verificar escritura
        If LOF(DataFile) <> oregla.DataSize Then
            oMensajes.ErrorDeEscrituraEnDisco
        End If
        
        Close DataFile
        
        DataFile = 0
    End If

End Sub

''' <summary>
''' Libera recursos del smtpclient
''' </summary>
''' <remarks>Llamada desde: pantallas q notifican y mdi_unload (realmente baspublic/FinDeSesion)  ; Tiempo m�ximo: 0,2</remarks>
Public Sub FinalizarSesionMail()
    oIdsMail.FuerzaFinalizeSmtpClient
    Set oIdsMail = Nothing
End Sub

''' <summary>
''' Valida el numero maximo de decimales de la cantidad del pedido
''' </summary>
''' <param name="UnidadCompra">Unidad de compra del pedido</param>
''' <param name="CantidadPedida">Cantidad pedida</param>
''' <returns>Si la validacion es correcta se envia -1, sino se envia el numero maximo de decimales permitido</returns>
''' <remarks>Llamada desde:frmSeguimiento,frmPedidos</remarks>
''' <remarks></remarks>
Public Function ValidarNumeroMaximoDecimales(ByVal UnidadCompra As String, ByVal CantidadPedida As Variant) As Integer
    Dim oUnidades As CUnidades
    Dim iLongitudParteEntera As Integer
    Dim iNumDecimales As Integer
    Dim dParteDecimal As Double
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    
    ValidarNumeroMaximoDecimales = -1
    
    UnidadCompra = Trim(UnidadCompra)
    
    oUnidades.CargarTodasLasUnidades UnidadCompra, , True
    If Not oUnidades.Item(UnidadCompra) Is Nothing Then
        iLongitudParteEntera = Len(CStr(Int(CantidadPedida)))
        iNumDecimales = Len(CStr(CantidadPedida)) - iLongitudParteEntera - 1
        If iNumDecimales <> -1 Then
            dParteDecimal = Round(Round(CantidadPedida, iNumDecimales) - Int(CantidadPedida), iNumDecimales)
            iNumDecimales = Len(Mid(CStr(dParteDecimal), 3, Len(CStr(dParteDecimal))))
            If Not IsNull(oUnidades.Item(UnidadCompra).NumDecimales) Then
                If iNumDecimales > oUnidades.Item(UnidadCompra).NumDecimales Then
                     ValidarNumeroMaximoDecimales = oUnidades.Item(UnidadCompra).NumDecimales
                Else
                     ValidarNumeroMaximoDecimales = -1
                End If
            Else
                ValidarNumeroMaximoDecimales = -1
            End If
        Else
            ValidarNumeroMaximoDecimales = -1
        End If
    End If
End Function

''' <summary>
''' Carga las textos multidioma de las posibles variables para la formula de una Variable de calidad
''' </summary>
''' <param name="oVarCal">Variable de calidad</param>
''' <remarks>Llamada desde: frmVARCalidad/sdbgSubVariables_BeforeUpdate     frmVARCalCompuestaNivel3/sdbgSubVariables_BeforeUpdate
''' frmVARCalCompuestaNivel4/sdbgSubVariables_BeforeUpdate     frmVARCalCompuestaNivel5/sdbgSubVariables_BeforeUpdate
''' ; Tiempo m�ximo: 0,1</remarks>
Public Sub CargaListaVariablesFormula(ByRef oVarCal As CVariableCalidad)
    Dim sX As String
    Dim sCadena As String
    Dim oVariablesXiMultiIdioma As CMultiidiomas
    Dim iDen() As Integer
    Dim Ador As Ador.Recordset
    Dim i, j As Integer

    Set oVariablesXiMultiIdioma = oFSGSRaiz.Generar_CMultiidiomas

    Select Case oVarCal.Subtipo
    Case CalidadSubtipo.CalPPM
        ReDim iDen(3)
        iDen(0) = 10
        iDen(1) = 11
        iDen(2) = 16
        iDen(3) = 17
    Case CalidadSubtipo.CalCargoProveedores
        ReDim iDen(3)
        iDen(0) = 12
        iDen(1) = 13
        iDen(2) = 16
        iDen(3) = 17
    Case CalidadSubtipo.CalTasaServicios
        ReDim iDen(3)
        iDen(0) = 14
        iDen(1) = 15
        iDen(2) = 16
        iDen(3) = 17
    Case CalidadSubtipo.CalNoConformidad
        ReDim iDen(6)
        iDen(0) = 57
        iDen(1) = 58
        iDen(2) = 59
        iDen(3) = 60
        iDen(4) = 67
        iDen(5) = 68
        iDen(6) = 69
    End Select

    For j = 0 To UBound(iDen)
        sCadena = ""
        For i = 1 To oVarCal.Denominaciones.Count
            If oVarCal.Subtipo = CalidadSubtipo.CalNoConformidad Then
                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIDAD_PUNT, oVarCal.Denominaciones.Item(i).Cod, iDen(j))
            Else
                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIDAD_SUB, oVarCal.Denominaciones.Item(i).Cod, iDen(j))
            End If

            If Not Ador Is Nothing Then
                sX = Ador(0).Value
                sX = Replace(sX, " =", ":")
                If sCadena <> "" Then sCadena = sCadena & "$"
                sCadena = sCadena & oVarCal.Denominaciones.Item(i).Cod & "#" & sX
            End If
        Next
        oVariablesXiMultiIdioma.Add "X" & j + 1, sCadena, "X" & j + 1
    Next

    Set oVarCal.VariablesXi = oVariablesXiMultiIdioma
    Set Ador = Nothing
End Sub

''' <summary>
''' Lanzar una pagina FSNWEB
''' </summary>
''' <param name="sOrigen">Nombre del fomulario q llama a esta funci�n</param>
''' <param name="sFormKey">Clave para el formulario en la colecci�n de formularios</param>
''' <param name="sNombre">Nombre del Dtsx</param>
''' <param name="sRuta">URL de la pagina a ejecutar</param>
''' <param name="sParams">Parametros de la pagina a ejecutar</param>
''' <param name="sPagina">P�gina a mostrar</param>
''' <returns>Formulario abierto</returns>
''' <remarks>Llamada desde: frmLISPER ; Tiempo m�ximo: 0,3</remarks>

Public Sub MostrarPaginaFSNWeb(ByVal sOrigen As String, ByVal sFormKey As String, ByVal sNombre As String, ByVal sRuta As String, ByVal sParams As String, _
        ByVal sPagina As String)
    Dim strSessionId As String
    Dim pv As frmInternet
    Dim bExiste As Boolean
    Dim sURL As String
    
    If sRuta <> "" And sPagina <> "" Then
        sURL = sRuta
                
        If Right(sRuta, 1) = "/" Then
            If Left(sPagina, 1) = "/" Then
                sURL = sURL & Right(sPagina, Len(sPagina) - 1)
            Else
                sURL = sURL & sPagina
            End If
        Else
            If Left(sPagina, 1) = "/" Then
                sURL = sURL & sPagina
            Else
                sURL = sURL & "/" & sPagina
            End If
        End If
        
        ' Se crea el id de sesion de fullstep web
        strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
            
        'Se muestra el formulario
        bExiste = False
        If sFormKey <> "" Then
            If g_FormulariosInternet Is Nothing Then Set g_FormulariosInternet = New Dictionary
            If Not g_FormulariosInternet.Exists(sOrigen & "|" & sFormKey) Then
                Set pv = New frmInternet
                g_FormulariosInternet.Add sOrigen & "|" & sFormKey, pv
            Else
                bExiste = True
                Set pv = g_FormulariosInternet.Item(sOrigen & "|" & sFormKey)
            End If
        Else
            Set pv = New frmInternet
        End If
        
        With pv
            If Not bExiste Then
                .g_sOrigen = sOrigen
                .g_sFormKey = sFormKey
                .g_sNombre = sNombre
                .g_sRuta = sURL & "?desdeGS=1&SessionId=" & strSessionId & sParams
                DoEvents
                .Show
            Else
                .SetFocus
            End If
        End With
    End If
End Sub

''' <summary>Abre la p�gina de la factura</summary>
''' <param name="sOrigen">Nombre del formulario de origen</param>
''' <param name="lIdFactura">Id de la factura</param>
''' <remarks>Llamada desde: frmPEDIDOS.sdbgMiPedido_LostFocus, frmSeguimiento.sdbgItemPorProve_BtnClick</remarks>
''' <revision>LTG 10/08/2012</revision>

Public Function AbrirFacturaFSNWeb(ByVal sOrigen As String, ByVal lIdFactura As Long)
    MostrarPaginaFSNWeb sOrigen, CStr(lIdFactura), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 132), gParametrosGenerales.gsRutasFullstepWeb, "&ID=" & CStr(lIdFactura), gParametrosGenerales.gcolRutas("DetalleFactura")
End Function

''' <summary>Descarga posibles formularios de facturas web abiertos</summary>
''' <param name="sOrigen">Nombre del formulario origen</param>
''' <param name="sFormKey">Clave para el formulario</param>
''' <remarks>Llamada desde: Form_Unload</remarks>
''' <revision>LTG 13/08/2012</revision>

Public Sub CerrarPaginaFSNWeb(ByVal sOrigen As String, Optional ByVal sFormKey As String)
    Dim i As Integer
    
    If Not g_FormulariosInternet Is Nothing Then
        If sFormKey <> "" Then
            If g_FormulariosInternet.Exists(sOrigen & "|" & sFormKey) Then
                Unload g_FormulariosInternet.Item(sOrigen & "|" & sFormKey)
            End If
        Else
            For i = g_FormulariosInternet.Count - 1 To 0 Step -1
                If Left(g_FormulariosInternet.Keys(i), Len(sOrigen)) = sOrigen Then
                    Unload g_FormulariosInternet.Item(g_FormulariosInternet.Keys(i))
                End If
            Next
        End If
    End If
End Sub

''' <summary>
''' Se comprueban las condiciones de bloqueo por etapa
''' </summary>
''' <param name="IdInstancia">Instancia</param>
''' <returns>si hay bloqueo o no</returns>
''' <remarks>Llamada desde: frmSolicitudDesglose/cmdPedidoDir_Click     frmSolicitudDetalle/cmdPedidoDir_Click      frmSolicitudes/cmdPedidoDir_Click; Tiempo m�ximo: 0,2</remarks>
Public Function BloqueoEtapa(IdInstancia As Long) As Boolean
    Dim oInstancia As CInstancia
    Dim Ador As Ador.Recordset

    Set oInstancia = oFSGSRaiz.Generar_CInstancia
    oInstancia.Id = IdInstancia
    Set Ador = oInstancia.ComprobarBloqueoEtapa()

    If Not Ador.EOF Then
        BloqueoEtapa = IIf(Ador.Fields("BLOQUEO_PEDIDOSDIRECTOS").Value = 1, False, True)
    Else
        BloqueoEtapa = False
    End If

    Ador.Close
    Set Ador = Nothing
    Set oInstancia = Nothing
End Function

''' <summary>
''' Se comprueban las condiciones de bloqueo por f�rmula
''' </summary>
''' <param name="IdInstancia">Instancia</param>
''' <param name="iTipoBloqueo">Tipo Bloqueo</param>
''' <param name="ErrorFormula">Error Formula</param>
''' <param name="ErrorValores">Error Valores</param>
''' <param name="bMostrarIdSolicitud">Indica si hay que mostrar el id de la solicitud en el mensaje</param>
''' <returns>si hay bloqueo o no</returns>
''' <remarks>Llamada desde: frmSolicitudDesglose/cmdPedidoDir_Click      frmSolicitudDetalle/cmdPedidoDir_Click     frmSolicitudes/cmdPedidoDir_Click; Tiempo m�ximo: 0,2</remarks>
Public Function BloqueoCondiciones(IdInstancia As Long, iTipoBloqueo As Integer, ByVal ErrorFormula As String, ByVal ErrorValores As String, Optional ByVal bMostrarIdSolicitud As Boolean = False, _
        Optional ByVal sTextoSolicitud As String) As Boolean
    Dim oInstancia As CInstancia
    Dim iBloqueo As Integer
    Dim bExisteBloqueo As Boolean
    Dim Ador As Ador.Recordset
    Dim sMensaje As String
    
    Set oInstancia = oFSGSRaiz.Generar_CInstancia
    oInstancia.Id = IdInstancia

    bExisteBloqueo = False

    Set Ador = oInstancia.ComprobarBloqueoCond(iTipoBloqueo)

    If Ador.EOF Then
        BloqueoCondiciones = False
        Ador.Close
        Set Ador = Nothing
        Set oInstancia = Nothing
        Exit Function
    Else
        While Not Ador.EOF And Not bExisteBloqueo
            iBloqueo = oInstancia.ComprobarBloqueoCondiciones(Ador.Fields("ID").Value, Ador.Fields("FORMULA").Value)
            Select Case iBloqueo
                Case 0  'No existe bloqueo

                Case 1  'Existe bloqueo
                    sMensaje = NullToStr(Ador.Fields("MENSAJE_" & gParametrosInstalacion.gIdioma).Value)
                    If bMostrarIdSolicitud Then sMensaje = sTextoSolicitud & " " & CStr(IdInstancia) & ": " & sMensaje
                    oMensajes.Bloqueo (sMensaje)
                    If Ador.Fields("TIPO").Value = TipoAvisoBloqueo.Bloquea Then
                        bExisteBloqueo = True
                    End If
                Case 3  'Error al realizar el c�lculo:F�rmula inv�lida.
                    sMensaje = ErrorFormula
                    If bMostrarIdSolicitud Then sMensaje = sTextoSolicitud & " " & CStr(IdInstancia) & ": " & sMensaje
                    oMensajes.FormulaIncorrecta (sMensaje)
                Case 4  'Error al realizar el c�lculo:Valores incorrectos.
                    sMensaje = ErrorValores
                    If bMostrarIdSolicitud Then sMensaje = sTextoSolicitud & " " & CStr(IdInstancia) & ": " & sMensaje
                    oMensajes.FormulaIncorrecta (sMensaje)
            End Select
            Ador.MoveNext
        Wend
    End If

    BloqueoCondiciones = bExisteBloqueo

    Ador.Close
    Set Ador = Nothing
    Set oInstancia = Nothing
End Function

Public Function actualizarcentrales(ByRef oArticuloCentral As CArticulo)
    oArticuloCentral.isCentral = True
    Dim oArticulo As CArticulo
    Dim oUON As IUon
    
    oArticuloCentral.deleteAllUons
    
    For Each oArticulo In oArticuloCentral.articulosAgregados
        'Actualizamos los articulos agregados para relacionarlos con el art�culo central
        's�lo actualizamos datos principales
        oArticulo.CodArticuloCentral = oArticuloCentral.Cod
        If oArticulo.uons.Count > 0 Then
            
            For Each oUON In oArticulo.uons
                If oArticuloCentral.addUon(oUON) = False Then
                    oMensajes.NoSePuedeAgregarArticulo oArticuloCentral.CodDen, oArticulo.CodDen
                    oArticuloCentral.deleteAllUons
                    oArticuloCentral.isCentral = False
                    actualizarcentrales = False
                    Exit Function
                End If
            Next
            Set oUON = Nothing
        Else
            oMensajes.NoSePuedeAgregarArticuloNoUons oArticulo.CodDen
            oArticuloCentral.isCentral = False
            actualizarcentrales = False
            Exit Function
        End If
    Next

    'guardar datos del central
    oArticuloCentral.actualizarCentral
    oArticuloCentral.actualizarArticuloCompleto
    actualizarcentrales = True

End Function

Public Function CrearCAEstructuraMat(ByRef oMensajes As CMensajes) As CAEstructuraMat
    Dim oEstrMat As CAEstructuraMat

    Set oEstrMat = New CAEstructuraMat
    Set oEstrMat.oMensajes = oMensajes

    Set CrearCAEstructuraMat = oEstrMat
End Function

'<summary>Crea un objeto Uon (de cualquier nivel) en funci�n de los codigos </summary>
'<returns>Objeto de tipo CUnidadOrgNivel1, 2 o 3
Public Function createUon(ByVal sUON1 As String, ByVal sUON2 As Variant, ByVal sUON3 As Variant) As Variant
    Dim oUON As IUon
    If NullToStr(sUON3) <> "" Then
        Set oUON = oFSGSRaiz.generar_CUnidadOrgNivel3
        oUON.CodUnidadOrgNivel3 = sUON3
        oUON.CodUnidadOrgNivel2 = sUON2
        oUON.CodUnidadOrgNivel1 = sUON1
    Else
        If NullToStr(sUON2) <> "" Then
            Set oUON = oFSGSRaiz.generar_CUnidadOrgNivel2
            oUON.CodUnidadOrgNivel1 = sUON1
            oUON.CodUnidadOrgNivel2 = sUON2
        Else
            If NullToStr(sUON1) <> "" Then
                Set oUON = oFSGSRaiz.generar_CUnidadOrgNivel1
                oUON.CodUnidadOrgNivel1 = sUON1
            Else
                Set oUON = oFSGSRaiz.Generar_CUnidadOrgNivel0
            End If
        End If
    End If
    
    Set createUon = oUON
    Set oUON = Nothing
End Function

'<summary>Abre el formulario calendario</summary>
'<param name="ctlDestination">Control que recibir� la fecha</param>

'---------------------------------------------------------------------------------------
' Procedure : AbrirFormCalendar
' Author    : ltg
' Date      : 26/10/2015
' Purpose   :
'---------------------------------------------------------------------------------------
'
Public Sub AbrirFormCalendar(ByRef oFormOrigen As Form, ByRef ctlDestination As Control, Optional ByVal sOrigen As String = "")
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
      
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set frmCalendar.frmDestination = oFormOrigen
    Set frmCalendar.ctrDestination = ctlDestination
    If sOrigen <> "" Then frmCalendar.sOrigen = sOrigen
    
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If ctlDestination.Text <> "" Then
        frmCalendar.Calendar.Value = ctlDestination.Text
    Else
        frmCalendar.Calendar.Value = Date
    End If
    
    frmCalendar.Show 1
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number <> 0 Then
        Call oFSGSRaiz.TratarError("M�dulo", "basPublic", "AbrirFormCalendar", err, Erl)
        Exit Sub
    End If
End Sub

Public Function DevolverMaterial(ByVal sMat As String) As Variant
Dim i As Integer
Dim arrMat(4) As String
Dim lLongCod As String


    For i = 1 To 4
        arrMat(i) = ""
    Next i
    
    i = 1
    
    While Trim(sMat) <> ""
        Select Case i
            Case 1
                lLongCod = basParametros.gLongitudesDeCodigos.giLongCodGMN1
            Case 2
                lLongCod = basParametros.gLongitudesDeCodigos.giLongCodGMN2
            Case 3
                lLongCod = basParametros.gLongitudesDeCodigos.giLongCodGMN3
            Case 4
                lLongCod = basParametros.gLongitudesDeCodigos.giLongCodGMN4
        End Select
        arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
        sMat = Mid(sMat, lLongCod + 1)
        i = i + 1
    Wend
            
    DevolverMaterial = arrMat

End Function

'<summary>Crea un objeto Uon (de cualquier nivel) en funci�n de los codigos </summary>
'<returns>Objeto de tipo CUnidadOrgNivel1, 2 o 3
Public Function createGMN(ByVal sGMN1 As String, ByVal sGMN2 As Variant, ByVal sGMN3 As Variant, ByVal sGMN4 As Variant) As Variant
    Dim oGmn As Variant
    If NullToStr(sGMN4) <> "" Then
        Set oGmn = oFSGSRaiz.Generar_CGrupoMatNivel4
        oGmn.GMN4Cod = sGMN4
        oGmn.GMN3Cod = sGMN3
        oGmn.GMN2Cod = sGMN2
        oGmn.GMN1Cod = sGMN1
    Else
        If NullToStr(sGMN3) <> "" Then
            Set oGmn = oFSGSRaiz.generar_CGrupoMatNivel3
            oGmn.GMN3Cod = sGMN3
            oGmn.GMN2Cod = sGMN2
            oGmn.GMN1Cod = sGMN1
        Else
            If NullToStr(sGMN2) <> "" Then
                Set oGmn = oFSGSRaiz.generar_CGrupoMatNivel2
                oGmn.GMN1Cod = sGMN1
                oGmn.GMN2Cod = sGMN2
            Else
                If NullToStr(sGMN1) <> "" Then
                    Set oGmn = oFSGSRaiz.generar_CGrupoMatNivel1
                    oGmn.GMN1Cod = sGMN1
                End If
            End If
        End If
    End If
    
    Set createGMN = oGmn
    Set oGmn = Nothing
End Function

Public Sub PositionList(ByRef sdbcControl As Object, ByVal Text As String, Optional ByVal vCol As Variant = 0)
Dim i As Long
Dim bm As Variant
On Error Resume Next
With sdbcControl
    .MoveFirst
    If Text <> "" Then
        For i = 0 To .Rows - 1
            bm = .GetBookmark(i)
            If UCase(Text) = UCase(Mid(.Columns(vCol).CellText(bm), 1, Len(Text))) Then
                .Bookmark = bm
                Exit For
            End If
        Next i
    End If
End With
End Sub

''' <summary>Comprueba si ha habido cambios en la f�rmula de una variable</summary>
''' <param name="oVarCalMod">variable de calidad modificada</param>
''' <param name="oVarCal">variable de calidad original</param>
''' <returns>Booleano indicando si ha habido cambios en la f�rmula de la variable</returns>
''' <remarks>Llamada desde: sdbgSubVariables_BtnClick</remarks>

Public Function VarCalHayCambiosFormula(ByRef oVarCalMod As CVariableCalidad, ByRef oVarCal As CVariableCalidad) As Boolean
    Dim bHayCambios As Boolean
    
    If Not oVarCal Is Nothing Then
        bHayCambios = ((Not IsNull(oVarCalMod.Formula) And IsNull(oVarCal.Formula)) Or (IsNull(oVarCalMod.Formula) And Not IsNull(oVarCal.Formula)))
        If Not bHayCambios Then
            If Not IsNull(oVarCalMod.Formula) And Not IsNull(oVarCal.Formula) Then
                bHayCambios = ((oVarCalMod.Formula <> oVarCal.Formula) And (oVarCalMod.TieneObjetivosSuelosEnFormula(oVarCalMod.Formula) Or oVarCal.TieneObjetivosSuelosEnFormula))
            End If
        End If
    End If
    
    VarCalHayCambiosFormula = bHayCambios
End Function

''' <summary>Agrega una columna a un grid</summary>
''' <param name="oGrid">Grid al que se quiere a�adir la columna</param>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>

Public Sub AgregarColumnaGrid(ByRef oGrid As SSDBGrid, ByVal iIndex As Integer, ByVal sName As String, ByVal bVisible As Boolean, Optional ByVal sCaption As Variant, Optional ByVal Alignment As Variant, _
        Optional ByVal CaptionAlignment As Variant, Optional HeadStyleSet As Variant, Optional ByVal Width As Variant, Optional ByVal Locked As Variant)
    Dim oColumn As SSDataWidgets_B.Column
    
    oGrid.Columns.Add iIndex
    Set oColumn = oGrid.Columns(iIndex)
    With oColumn
        .Name = sName
        .Visible = bVisible
        If Not IsMissing(sCaption) Then .caption = sCaption
        If Not IsMissing(Alignment) Then .Alignment = Alignment
        If Not IsMissing(CaptionAlignment) Then .CaptionAlignment = CaptionAlignment
        If Not IsMissing(HeadStyleSet) Then .HeadStyleSet = HeadStyleSet
        If Not IsMissing(Width) Then .Width = Width
        If Not IsMissing(Locked) Then .Locked = Locked
    End With
    DoEvents
    
    Set oColumn = Nothing
End Sub

''' <summary>Agrega una columna a un grupo de un grid</summary>
''' <param name="oGrupo">grupo al que se quiere a�adir la columna</param>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>

Public Sub AgregarColumnaGrupoGrid(ByRef oGrupo As SSDataWidgets_B.Group, ByVal iIndex As Integer, ByVal sName As String, ByVal bVisible As Boolean, Optional ByVal sCaption As Variant, Optional ByVal Alignment As Variant, _
        Optional ByVal CaptionAlignment As Variant, Optional HeadStyleSet As Variant, Optional ByVal Width As Variant, Optional ByVal Locked As Variant, Optional ByVal Level As Variant, Optional ByVal StyleSet As Variant, _
        Optional ByVal Style As Variant, Optional ByVal ButtonAlways As Variant)
    Dim oColumn As SSDataWidgets_B.Column
    
    oGrupo.Columns.Add iIndex
    Set oColumn = oGrupo.Columns(iIndex)
    With oColumn
        .Name = sName
        .Visible = bVisible
        If Not IsMissing(sCaption) Then .caption = sCaption
        If Not IsMissing(Alignment) Then .Alignment = Alignment
        If Not IsMissing(CaptionAlignment) Then .CaptionAlignment = CaptionAlignment
        If Not IsMissing(HeadStyleSet) Then .HeadStyleSet = HeadStyleSet
        If Not IsMissing(Width) Then .Width = Width
        If Not IsMissing(Locked) Then .Locked = Locked
        If Not IsMissing(Level) Then .Level = Level
        If Not IsMissing(StyleSet) Then .StyleSet = StyleSet
        If Not IsMissing(Style) Then .Style = Style
        If Not IsMissing(ButtonAlways) Then .ButtonsAlways = ButtonAlways
    End With
    DoEvents
    
    Set oColumn = Nothing
End Sub


