VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRCOMPBuscar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar comprador+"
   ClientHeight    =   4455
   ClientLeft      =   1230
   ClientTop       =   1515
   ClientWidth     =   6540
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRCOMPBuscar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   6540
   ShowInTaskbar   =   0   'False
   Begin SSDataWidgets_B.SSDBGrid sdbgCompradores 
      Height          =   2490
      Left            =   120
      TabIndex        =   4
      Top             =   1425
      Width           =   6315
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   5
      stylesets.count =   1
      stylesets(0).Name=   "ActiveRowBlue"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8388608
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmESTRCOMPBuscar.frx":014A
      UseGroups       =   -1  'True
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Groups.Count    =   2
      Groups(0).Width =   4180
      Groups(0).Caption=   "Equipo"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   2
      Groups(0).Columns(0).Width=   1138
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "CODEQP"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).BackColor=   16449500
      Groups(0).Columns(1).Width=   3043
      Groups(0).Columns(1).Caption=   "Denominaci�n"
      Groups(0).Columns(1).Name=   "DENEQP"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).BackColor=   16449500
      Groups(1).Width =   6906
      Groups(1).Caption=   "Comprador"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns.Count=   3
      Groups(1).Columns(0).Width=   1191
      Groups(1).Columns(0).Caption=   "C�digo"
      Groups(1).Columns(0).Name=   "CODCOMP"
      Groups(1).Columns(0).DataField=   "Column 2"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(1).Width=   3572
      Groups(1).Columns(1).Caption=   "Apellidos"
      Groups(1).Columns(1).Name=   "APEL"
      Groups(1).Columns(1).DataField=   "Column 3"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(2).Width=   2143
      Groups(1).Columns(2).Caption=   "Nombre"
      Groups(1).Columns(2).Name=   "NOMBRE"
      Groups(1).Columns(2).DataField=   "Column 4"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      TabNavigation   =   1
      _ExtentX        =   11139
      _ExtentY        =   4392
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCargar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6060
      Picture         =   "frmESTRCOMPBuscar.frx":0166
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Cargar"
      Top             =   1020
      Width           =   315
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      Height          =   1215
      Left            =   120
      ScaleHeight     =   1155
      ScaleWidth      =   5775
      TabIndex        =   7
      Top             =   105
      Width           =   5835
      Begin VB.TextBox txtCod 
         Height          =   285
         Left            =   4020
         MaxLength       =   50
         TabIndex        =   2
         Top             =   660
         Width           =   1515
      End
      Begin VB.TextBox txtNom 
         Height          =   285
         Left            =   1275
         MaxLength       =   50
         TabIndex        =   1
         Top             =   660
         Width           =   1575
      End
      Begin VB.TextBox txtApe 
         Height          =   285
         Left            =   1275
         MaxLength       =   150
         TabIndex        =   0
         Top             =   180
         Width           =   4260
      End
      Begin VB.Label Label1 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:+"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   3015
         TabIndex        =   10
         Top             =   720
         Width           =   945
      End
      Begin VB.Label lblNom 
         BackColor       =   &H00808000&
         Caption         =   "Nombre:+"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label lblApel 
         BackColor       =   &H00808000&
         Caption         =   "Apellidos:+"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   105
         TabIndex        =   8
         Top             =   240
         Width           =   1110
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar+"
      Default         =   -1  'True
      Height          =   345
      Left            =   1815
      TabIndex        =   5
      Top             =   4050
      Width           =   1125
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar+"
      Height          =   345
      Left            =   3180
      TabIndex        =   6
      Top             =   4050
      Width           =   1125
   End
End
Attribute VB_Name = "frmESTRCOMPBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oCompradoresEncontrados As CCompradores
Private oeqp As CEquipo
'MULTILENGUAJE
Private sMensajes(3) As String  'mensajes

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTR_COMPBUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmESTRCOMPBuscar.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value & ":"                   'C�digo
        sdbgCompradores.Columns(0).caption = Ador(0).Value
        sdbgCompradores.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        lblApel.caption = Ador(0).Value & ":"                  'Apellidos
        sdbgCompradores.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        lblNom.caption = Ador(0).Value & ":"                   'nombre
        sdbgCompradores.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgCompradores.Groups(0).caption = Ador(0).Value      'Equipo
        sMensajes(1) = Ador(0).Value
        Ador.MoveNext
        sdbgCompradores.Groups(1).caption = Ador(0).Value      'Comprador
        sMensajes(2) = Ador(0).Value
        Ador.MoveNext
        sdbgCompradores.Columns(1).caption = Ador(0).Value     'denominacion
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub CargarGrid()

Dim oCom As CComprador
    
    sdbgCompradores.RemoveAll
    
    For Each oCom In oCompradoresEncontrados
        sdbgCompradores.AddItem oCom.codEqp & Chr(m_lSeparador) & oCom.DenEqp & Chr(m_lSeparador) _
        & oCom.Cod & Chr(m_lSeparador) & oCom.Apel & Chr(m_lSeparador) & oCom.nombre
    Next


End Sub

Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
On Error GoTo NoSeEncuentra

If sdbgCompradores.Rows = 0 Then Exit Sub
    
Screen.MousePointer = vbHourglass
Set nodx = frmESTRCOMP.tvwEstrComp.Nodes.Item("COM" & CStr(sdbgCompradores.Columns(0).Value) & String(basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(sdbgCompradores.Columns(0).Value), " ") & CStr(sdbgCompradores.Columns(2).Value))
nodx.Parent.Expanded = True
nodx.Selected = True
'frmESTRCOMP.SetFocus
Unload Me
Screen.MousePointer = vbNormal
Exit Sub

NoSeEncuentra:
    'frmESTRCOMP.cmdRestaurar_Click
    'cmdAceptar_Click
    If sdbgCompradores.Columns(3).Value = "" Then
        oMensajes.DatoEliminado sMensajes(1)   'Equipo
    Else
        oMensajes.DatoEliminado sMensajes(2)  'Comprador
    End If
    Screen.MousePointer = vbNormal
    Unload Me
End Sub

Private Sub cmdCancelar_Click()

Unload Me

End Sub

Private Sub cmdCargar_Click()

    Screen.MousePointer = vbHourglass
    
    Set oCompradoresEncontrados = Nothing
    
    If frmESTRCOMP.bREqp And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
        oeqp.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe)
        Set oCompradoresEncontrados = Nothing
        Set oCompradoresEncontrados = oeqp.Compradores
        
    Else
        Set oCompradoresEncontrados = Nothing
        Set oCompradoresEncontrados = oFSGSRaiz.generar_CCompradores
        oCompradoresEncontrados.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe), False, , , , , , True
    End If
    
    If Not oCompradoresEncontrados Is Nothing Then
        CargarGrid
        If Me.Visible Then sdbgCompradores.SetFocus
    Else
        sdbgCompradores.RemoveAll
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Load()

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    ' MULTILENGUAJE
    CargarRecursos
    
If frmESTRCOMP.bREqp Then
    Set oeqp = Nothing
    Set oeqp = oFSGSRaiz.generar_CEquipo
    oeqp.Cod = oUsuarioSummit.comprador.codEqp
  
End If

    PonerFieldSeparator Me

End Sub

Private Sub Form_Unload(Cancel As Integer)

Set oCompradoresEncontrados = Nothing

End Sub

Private Sub sdbgCompradores_DblClick()
    cmdAceptar_Click
End Sub


Private Sub sdbgCompradores_HeadClick(ByVal ColIndex As Integer)
Set oCompradoresEncontrados = Nothing

Select Case ColIndex
    
    Case 0 'Por codigo de equipo
            If frmESTRCOMP.bREqp And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
                Exit Sub
            Else
                Screen.MousePointer = vbHourglass
                Set oCompradoresEncontrados = Nothing
                Set oCompradoresEncontrados = oFSGSRaiz.generar_CCompradores
                oCompradoresEncontrados.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe), , , , True, , False, True
            End If
            
    Case 1 'Por denominacion de equipo
            If frmESTRCOMP.bREqp And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
                Exit Sub
            Else
                Screen.MousePointer = vbHourglass
                Set oCompradoresEncontrados = Nothing
                Set oCompradoresEncontrados = oFSGSRaiz.generar_CCompradores
                oCompradoresEncontrados.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe), False, False, False, True, True, False, True
            End If
    
    Case 2 'Por codigo de comprador
            If frmESTRCOMP.bREqp And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
                Screen.MousePointer = vbHourglass
                oeqp.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe), , , , False
                Set oCompradoresEncontrados = Nothing
                Set oCompradoresEncontrados = oeqp.Compradores
  
            Else
                Screen.MousePointer = vbHourglass
                Set oCompradoresEncontrados = Nothing
                Set oCompradoresEncontrados = oFSGSRaiz.generar_CCompradores
                oCompradoresEncontrados.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe), , , , , , False, True
            
            End If
    
    Case 4 ' Por Nombre
                        
            If frmESTRCOMP.bREqp And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
                Screen.MousePointer = vbHourglass
                oeqp.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe), , True, , False
                Set oCompradoresEncontrados = Nothing
                Set oCompradoresEncontrados = oeqp.Compradores
           
            Else
                Screen.MousePointer = vbHourglass
                Set oCompradoresEncontrados = Nothing
                Set oCompradoresEncontrados = oFSGSRaiz.generar_CCompradores
                oCompradoresEncontrados.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe), , True, , , , False, True
            End If
                                    
    Case 3 ' Por Apellido
            If frmESTRCOMP.bREqp And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
                Screen.MousePointer = vbHourglass
                oeqp.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe), , , True, False
                Set oCompradoresEncontrados = Nothing
                Set oCompradoresEncontrados = oeqp.Compradores
                
             Else
                Screen.MousePointer = vbHourglass
                Set oCompradoresEncontrados = Nothing
                Set oCompradoresEncontrados = oFSGSRaiz.generar_CCompradores
                oCompradoresEncontrados.CargarTodosLosCompradores Trim(txtCod), Trim(txtNom), Trim(txtApe), False, , True, , , False, True
            End If

End Select

CargarGrid

Screen.MousePointer = vbNormal

End Sub
