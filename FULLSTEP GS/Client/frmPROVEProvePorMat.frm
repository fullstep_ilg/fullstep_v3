VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPROVEProvePorMat 
   Caption         =   "Proveedores por material"
   ClientHeight    =   4695
   ClientLeft      =   405
   ClientTop       =   1560
   ClientWidth     =   7680
   Icon            =   "frmPROVEProvePorMat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4695
   ScaleWidth      =   7680
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   60
      Top             =   60
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   15
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEProvePorMat.frx":014A
            Key             =   "Prove"
            Object.Tag             =   "Prove"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab stabProve 
      Height          =   4575
      Left            =   30
      TabIndex        =   11
      Top             =   90
      Width           =   7575
      _ExtentX        =   13361
      _ExtentY        =   8070
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selección de material"
      TabPicture(0)   =   "frmPROVEProvePorMat.frx":01B5
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Proveedores"
      TabPicture(1)   =   "frmPROVEProvePorMat.frx":01D1
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picEdit"
      Tab(1).Control(1)=   "picNavigate"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "picProveHom"
      Tab(1).Control(3)=   "picProve"
      Tab(1).ControlCount=   4
      Begin VB.PictureBox picProve 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3555
         Left            =   -74865
         ScaleHeight     =   3555
         ScaleWidth      =   7290
         TabIndex        =   21
         Top             =   450
         Width           =   7290
         Begin SSDataWidgets_B.SSDBGrid sdbgProveedores 
            Height          =   3555
            Left            =   0
            TabIndex        =   16
            Top             =   0
            Width           =   7305
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   6
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPROVEProvePorMat.frx":01ED
            AllowUpdate     =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterPos     =   2
            Columns.Count   =   6
            Columns(0).Width=   873
            Columns(0).Caption=   "Asig."
            Columns(0).Name =   "SEL"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Style=   2
            Columns(1).Width=   2223
            Columns(1).Caption=   "Código"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   4789
            Columns(2).Caption=   "Denominación"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   1323
            Columns(3).Caption=   "Calif1."
            Columns(3).Name =   "CAL1"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(4).Width=   1323
            Columns(4).Caption=   "Calif2."
            Columns(4).Name =   "CAL2"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(5).Width=   1323
            Columns(5).Caption=   "Calif3."
            Columns(5).Name =   "CAL3"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Locked=   -1  'True
            _ExtentX        =   12885
            _ExtentY        =   6271
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgProveHom 
            Height          =   3555
            Left            =   0
            TabIndex        =   12
            Top             =   0
            Width           =   7290
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   3
            AllowUpdate     =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterPos     =   1
            Columns.Count   =   3
            Columns(0).Width=   3413
            Columns(0).Caption=   "Código"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777152
            Columns(1).Width=   7223
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777152
            Columns(2).Width=   1191
            Columns(2).Caption=   "Hom."
            Columns(2).Name =   "HOM"
            Columns(2).CaptionAlignment=   2
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   2
            _ExtentX        =   12859
            _ExtentY        =   6271
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame2 
         Height          =   1305
         Left            =   270
         TabIndex        =   28
         Top             =   2910
         Width           =   7095
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiCod 
            Height          =   285
            Left            =   1110
            TabIndex        =   9
            Top             =   480
            Width           =   1725
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3043
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiDen 
            Height          =   285
            Left            =   2880
            TabIndex        =   10
            Top             =   480
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblArtiCod 
            Caption         =   "Artículo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   225
            TabIndex        =   29
            Top             =   540
            Width           =   825
         End
      End
      Begin VB.Frame Frame1 
         Height          =   2355
         Left            =   270
         TabIndex        =   23
         Top             =   420
         Width           =   7065
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   6600
            Picture         =   "frmPROVEProvePorMat.frx":0209
            Style           =   1  'Graphical
            TabIndex        =   0
            Top             =   180
            Width           =   345
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   1800
            TabIndex        =   1
            Top             =   480
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
            Height          =   285
            Left            =   1800
            TabIndex        =   3
            Top             =   900
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
            Height          =   285
            Left            =   1800
            TabIndex        =   5
            Top             =   1320
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
            Height          =   285
            Left            =   1800
            TabIndex        =   7
            Top             =   1740
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
            Height          =   285
            Left            =   2880
            TabIndex        =   4
            Top             =   900
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
            Height          =   285
            Left            =   2880
            TabIndex        =   6
            Top             =   1320
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
            Height          =   285
            Left            =   2880
            TabIndex        =   8
            Top             =   1740
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
            Height          =   285
            Left            =   2880
            TabIndex        =   2
            Top             =   480
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblGMN3_4 
            Caption         =   "Subfamilia:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   225
            TabIndex        =   27
            Top             =   1380
            Width           =   1560
         End
         Begin VB.Label lblGMN2_4 
            Caption         =   "Familia:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   225
            TabIndex        =   26
            Top             =   960
            Width           =   1560
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "Commodity:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   225
            TabIndex        =   25
            Top             =   540
            Width           =   1560
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "Grupo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   225
            TabIndex        =   24
            Top             =   1800
            Width           =   1560
         End
      End
      Begin VB.PictureBox picProveHom 
         BorderStyle     =   0  'None
         Height          =   405
         Left            =   -74925
         ScaleHeight     =   405
         ScaleWidth      =   7050
         TabIndex        =   30
         Top             =   4080
         Width           =   7050
         Begin VB.CommandButton cmdCancelarProve 
            Caption         =   "&Cancelar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3780
            TabIndex        =   15
            Top             =   30
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdAceptarProve 
            Caption         =   "&Aceptar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2520
            TabIndex        =   14
            Top             =   30
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdModificarProve 
            Caption         =   "&Modificar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   60
            TabIndex        =   13
            Top             =   30
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigate 
         BorderStyle     =   0  'None
         Height          =   405
         Left            =   -74925
         ScaleHeight     =   405
         ScaleWidth      =   7350
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   4080
         Width           =   7350
         Begin VB.CommandButton cmdAsignar 
            Caption         =   "&Añadir"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1185
            TabIndex        =   18
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar 
            Caption         =   "&Restaurar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2325
            TabIndex        =   19
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdDesAsignar 
            Caption         =   "&Edición"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   60
            TabIndex        =   17
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdListado 
            Caption         =   "&Listado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3420
            TabIndex        =   20
            Top             =   30
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEdit 
         BorderStyle     =   0  'None
         Height          =   405
         Left            =   -74925
         ScaleHeight     =   405
         ScaleWidth      =   7305
         TabIndex        =   31
         Top             =   4080
         Visible         =   0   'False
         Width           =   7305
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            Height          =   345
            Left            =   2520
            TabIndex        =   33
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "Cancelar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3780
            TabIndex        =   32
            Top             =   30
            Width           =   1005
         End
      End
   End
End
Attribute VB_Name = "frmPROVEProvePorMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de seguridad
Private bModif As Boolean
Private bRMat As Boolean
Public bREqp As Boolean

'Variables para caso de bRMat
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4


'Variables para la seleccion en combos
Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4
Public iNivelSeleccion As Integer

'variable para guardar los compradores que estan asignados
'Private oProvesAsig As CProveedores
' variable para guardar los proveedores a asignar
Private oProves As CProveedores
Private oProvesSeleccionados As CProveedores
Private oProvesDesSeleccionados As CProveedores
Private oIProveAsig As ICompProveAsignados
'variables para los combos
Private oPaises As CPaises

'Variable que tiene el Asunto para la Notificación de Asignación de Materiales de QA a Proveedores
Private sNotificarAsigMatQASubject As String

Private Accion As accionessummit

''' Combos
Private bRespetarCombo As Boolean

Private bRespetarComboGMN1 As Boolean
Private bRespetarComboGMN2 As Boolean
Private bRespetarComboGMN3 As Boolean
Private bRespetarComboGMN4 As Boolean

Private bGMN1CargarComboDesde As Boolean
Private bGMN2CargarComboDesde As Boolean
Private bGMN3CargarComboDesde As Boolean
Private bGMN4CargarComboDesde As Boolean
Private bCargarComboDesde As Boolean

Private oArtiSeleccionado As CArticulo
Private oIProveHom As IProveedoresHomologados
Private oProveedores As CProveedores

'Multilenguaje
Private sIdiPotenciales As String

'Variables de control:
Private m_bDesactivarTabClick As Boolean
Private m_bModoEdicion As Boolean

Private Sub Arrange()
    
    If Me.Width >= 225 Then stabProve.Width = Me.Width - 225
    If Me.Height >= 525 Then stabProve.Height = Me.Height - 525
    
    If stabProve.Width >= 285 Then picProve.Width = stabProve.Width - 285
            
    If stabProve.Height >= 1020 Then picProve.Height = stabProve.Height - 1020
    
    If picProve.Width >= 100 Then
        sdbgProveedores.Width = picProve.Width
        sdbgProveHom.Width = sdbgProveedores.Width
    End If
    
    If picProve.Height >= 45 Then
        sdbgProveedores.Height = picProve.Height
        sdbgProveHom.Height = sdbgProveedores.Height
    End If
    
    picNavigate.Top = picProve.Top + picProve.Height + 45
    picEdit.Top = picProve.Top + picProve.Height + 45
    picProveHom.Top = picEdit.Top
    picEdit.Left = (sdbgProveedores.Width / 2) - (picEdit.Width / 2)
    
    sdbgProveHom.Columns("COD").Width = sdbgProveHom.Width * 0.26
    sdbgProveHom.Columns("DEN").Width = sdbgProveHom.Width * 0.562
    sdbgProveHom.Columns("HOM").Width = sdbgProveHom.Width * 0.093
 
    sdbgProveedores.Columns(0).Width = sdbgProveedores.Width * 0.068
    sdbgProveedores.Columns(1).Width = sdbgProveedores.Width * 0.173
    sdbgProveedores.Columns(2).Width = sdbgProveedores.Width * 0.38
    sdbgProveedores.Columns(3).Width = sdbgProveedores.Width * 0.11
    sdbgProveedores.Columns(4).Width = sdbgProveedores.Width * 0.11
    sdbgProveedores.Columns(5).Width = sdbgProveedores.Width * 0.11
    
    'sdbgProveedores.Columns(0).Width = sdbgProveedores.Width * 5 / 100
    'sdbgProveedores.Columns(1).Width = sdbgProveedores.Width * 20 / 100
    'sdbgProveedores.Columns(2).Width = sdbgProveedores.Width * 45 / 100 - 200
    'sdbgProveedores.Columns(3).Width = sdbgProveedores.Width * 10 / 100
    'sdbgProveedores.Columns(4).Width = sdbgProveedores.Width * 10 / 100
    'sdbgProveedores.Columns(5).Width = sdbgProveedores.Width * 10 / 100

End Sub

Private Sub cmdAceptarProve_Click()
Dim i As Integer
Dim vbm As Variant
Dim teserror As TipoErrorSummit

    Screen.MousePointer = vbHourglass
    
    'Estamos modificando la homologación de artículos
    
    i = 0
    
    For i = 0 To sdbgProveHom.Rows - 1
        vbm = sdbgProveHom.AddItemBookmark(i)
        oProveedores.Item(sdbgProveHom.Columns("COD").CellValue(vbm)).Homologado = Abs((sdbgProveHom.Columns("HOM").CellValue(vbm)) <> "" And (sdbgProveHom.Columns("HOM").CellValue(vbm)) <> "0")
    Next i
    
    
    teserror = oIProveHom.ActualizarProveedoresHomologadosDelArticulo(sdbcArtiCod, oProveedores)
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
     RegistrarAccion accionessummit.ACCProvePorMatModHom, "Cod:" & Trim(sdbcArtiCod.Text)
     
    stabProve.TabEnabled(0) = True
    sdbgProveHom.AllowUpdate = False
    cmdModificarProve.Visible = True
    cmdCancelarProve.Visible = False
    cmdAceptarProve.Visible = False
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdCancelarProve_Click()
    
    stabProve.TabEnabled(0) = True
    sdbgProveHom.AllowUpdate = False
    cmdModificarProve.Visible = True
    cmdCancelarProve.Visible = False
    cmdAceptarProve.Visible = False
    
    
End Sub

Private Sub cmdModificarProve_Click()
    
    stabProve.TabEnabled(0) = False
    sdbgProveHom.AllowUpdate = True
    cmdModificarProve.Visible = False
    cmdCancelarProve.Visible = True
    cmdAceptarProve.Visible = True
    
End Sub

Private Sub sdbcArtiCod_Click()
    If Not sdbcArtiCod.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiCod_CloseUp()
 
    If sdbcArtiCod.Value = "..." Then
        sdbcArtiCod.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiCod.Value = "" Then Exit Sub
    
    
    bRespetarCombo = True
    sdbcArtiDen.Text = sdbcArtiCod.Columns(1).Text
    sdbcArtiCod.Text = sdbcArtiCod.Columns(0).Text
    bRespetarCombo = False
    
    ArticuloSeleccionado
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcArtiCod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcArtiDen.Text = ""
        bRespetarCombo = False
        Set oArtiSeleccionado = Nothing
        bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcArtiCod_DropDown()
    Dim oArt As CArticulo
    
    sdbcArtiCod.RemoveAll
   
    Screen.MousePointer = vbHourglass
    GMN4Seleccionado
    
    If bCargarComboDesde Then
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, sdbcArtiCod
    Else
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos
    End If
    
    
    For Each oArt In oGMN4Seleccionado.ARTICULOS
   
        sdbcArtiCod.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
   
    Next
    If oGMN4Seleccionado.ARTICULOS.Count = gParametrosInstalacion.giCargaMaximaCombos Then
        sdbcArtiCod.AddItem "..."
    End If

    sdbcArtiCod.SelStart = 0
    sdbcArtiCod.SelLength = Len(sdbcArtiCod.Text)
    sdbcArtiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiCod_InitColumnProps()
    
    sdbcArtiCod.DataFieldList = "Column 0"
    sdbcArtiCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcArtiCod_PositionList(ByVal Text As String)
PositionList sdbcArtiCod, Text
End Sub

Private Sub sdbcArtiCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcArtiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    bExiste = False
    If oGMN4Seleccionado Is Nothing Then
        Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    End If
    
    Screen.MousePointer = vbHourglass
    oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, sdbcArtiCod, , True
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN4Seleccionado.ARTICULOS.Count = 0)
    
    If Not bExiste Then
        sdbcArtiCod.Text = ""
    Else
        bRespetarCombo = True
        sdbcArtiDen.Text = oGMN4Seleccionado.ARTICULOS.Item(1).Den
        
        sdbcArtiCod.Columns(0).Value = sdbcArtiCod.Text
        sdbcArtiCod.Columns(1).Value = sdbcArtiDen.Text
        
        bRespetarCombo = False
        ArticuloSeleccionado
        bCargarComboDesde = False
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        Set oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub

Private Sub sdbcArtiDen_Click()
    
    If Not sdbcArtiDen.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiDen_CloseUp()
    
    If sdbcArtiDen.Value = "..." Then
        sdbcArtiDen.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcArtiCod.Text = sdbcArtiDen.Columns(1).Text
    sdbcArtiDen.Text = sdbcArtiDen.Columns(0).Text
    bRespetarCombo = False
    
    ArticuloSeleccionado
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcArtiDen_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcArtiCod.Text = ""
        bRespetarCombo = False
        Set oArtiSeleccionado = Nothing
        bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcArtiDen_DropDown()
    Dim oArt As CArticulo
    
    sdbcArtiDen.RemoveAll

    If oGMN4Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
    Else
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , True
    End If
    
    
    For Each oArt In oGMN4Seleccionado.ARTICULOS
   
        sdbcArtiDen.AddItem oArt.Den & Chr(m_lSeparador) & oArt.Cod
   
    Next
    If oGMN4Seleccionado.ARTICULOS.Count = gParametrosInstalacion.giCargaMaximaCombos Then
        sdbcArtiDen.AddItem "..."
    End If
    
    sdbcArtiDen.SelStart = 0
    sdbcArtiDen.SelLength = Len(sdbcArtiDen.Text)
    sdbcArtiDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiDen_InitColumnProps()
    
    sdbcArtiDen.DataFieldList = "Column 0"
    sdbcArtiDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcArtiDen_PositionList(ByVal Text As String)
PositionList sdbcArtiDen, Text
End Sub

Private Sub sdbcArtiDen_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcArtiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4Seleccionado.ARTICULOS.Count = 0)
            
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiDen.Text = ""
    Else
        bRespetarCombo = True
        sdbcArtiCod.Text = oGMN4Seleccionado.ARTICULOS.Item(1).Cod
        
        sdbcArtiDen.Columns(0).Value = sdbcArtiDen.Text
        sdbcArtiDen.Columns(1).Value = sdbcArtiCod.Text
        
        bRespetarCombo = False
        ArticuloSeleccionado
        bCargarComboDesde = False
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        Set oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub

Private Sub sdbcGMN1_4Cod_Click()
     
     If Not sdbcGMN1_4Cod.DroppedDown Then
        sdbcGMN1_4Cod = ""
        sdbcGMN1_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
        
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    bRespetarComboGMN1 = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    bRespetarComboGMN1 = False
    
    GMN1Seleccionado
    
    bGMN1CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    If bRMat Then
        
        Set oIMAsig = oUsuarioSummit.comprador
         Set oGruposMN1 = Nothing
        If bGMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         If bGMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If bGMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        bRespetarComboGMN1 = True
        sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
        bRespetarComboGMN1 = False
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text Then
        bRespetarComboGMN1 = True
        sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
        bRespetarComboGMN1 = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
       Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False)
    Else
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
    Else
        bRespetarComboGMN1 = True
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
        bRespetarComboGMN1 = False
        GMN1Seleccionado
        bGMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing

End Sub

Private Sub sdbcGMN1_4Den_Click()
     
     If Not sdbcGMN1_4Den.DroppedDown Then
        sdbcGMN1_4Cod = ""
        sdbcGMN1_4Den = ""
    End If
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN1_4Den_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN1_4Den.Text = "" Then Exit Sub
    
     If sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text Then
        bRespetarComboGMN1 = True
        sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
        bRespetarComboGMN1 = False
        Exit Sub
    End If
    
    If sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text Then
        bRespetarComboGMN1 = True
        sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
        bRespetarComboGMN1 = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, , False)
    Else
        Screen.MousePointer = vbHourglass
        oGMN1s.CargarTodosLosGruposMat , sdbcGMN1_4Den.Text, True, , False
        Screen.MousePointer = vbNormal
    End If
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Den.Text = ""
   Else
        bRespetarComboGMN1 = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        
        sdbcGMN1_4Den.Columns(0).Value = sdbcGMN1_4Den.Text
        sdbcGMN1_4Den.Columns(1).Value = sdbcGMN1_4Cod.Text
        
        bRespetarComboGMN1 = False
        GMN1Seleccionado
    End If
    
    Set oGMN1s = Nothing

End Sub

Private Sub sdbcGMN2_4Cod_Click()
     If Not sdbcGMN2_4Cod.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Private Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text Then
        bRespetarComboGMN2 = True
        sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
        bRespetarComboGMN2 = False
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text Then
        bRespetarComboGMN2 = True
        sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
        bRespetarComboGMN2 = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    If Not oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        bRespetarComboGMN2 = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        
        bRespetarComboGMN2 = False
        GMN2Seleccionado
        bGMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    
End Sub

Private Sub sdbcGMN2_4Den_Click()
     If Not sdbcGMN2_4Den.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN2_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text Then
        bRespetarComboGMN2 = True
        sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
        bRespetarComboGMN2 = False
        Exit Sub
    End If
    
    If sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text Then
        bRespetarComboGMN2 = True
        sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
        bRespetarComboGMN2 = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, , Trim(sdbcGMN2_4Den), True, , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4Den.Text, True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Den.Text = ""
    Else
        bRespetarComboGMN2 = True
        sdbcGMN2_4Cod.Text = oGMN2s.Item(1).Cod
        
        sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
        sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
        
        bRespetarComboGMN2 = False
        GMN2Seleccionado
        bGMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing

End Sub

Private Sub sdbcGMN3_4Cod_Click()
     
     If Not sdbcGMN3_4Cod.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Private Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text Then
        bRespetarComboGMN3 = True
        sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
        bRespetarComboGMN3 = False
        Exit Sub
    End If
    
    If sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text Then
        bRespetarComboGMN3 = True
        sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
        bRespetarComboGMN3 = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
   Else
        bRespetarComboGMN3 = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        
        bRespetarComboGMN3 = False
        GMN3Seleccionado
        bGMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not bRespetarComboGMN3 Then

        bRespetarComboGMN3 = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        bRespetarComboGMN3 = False
        
        Set oGMN3Seleccionado = Nothing
        If sdbcGMN3_4Cod.Value = "" Then
            bGMN3CargarComboDesde = False
        Else
            bGMN3CargarComboDesde = True
        End If
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Den_Click()
     
     If Not sdbcGMN3_4Den.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Den.Value = "" Then Exit Sub
    
    bRespetarComboGMN3 = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    bRespetarComboGMN3 = False
    
    GMN3Seleccionado
    
    bGMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Den.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN3 = Nothing
        If bGMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False)
        End If
    
    Else
        
         If bGMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If bGMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If stabProve.Tab = 1 Then Exit Sub
    
    
    If sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text Then
        bRespetarComboGMN3 = True
        sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
        bRespetarComboGMN3 = False
        Exit Sub
    End If
    
    If sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text Then
        bRespetarComboGMN3 = True
        sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
        bRespetarComboGMN3 = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, , Trim(sdbcGMN3_4Den), True, , False)
        Else
             oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4Den.Text, True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Den.Text = ""
    Else
        bRespetarComboGMN3 = True
        sdbcGMN3_4Cod.Text = oGMN3s.Item(1).Cod
        
        sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
        sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
        
        bRespetarComboGMN3 = False
        GMN3Seleccionado
        bGMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN4_4Cod_Click()
     If Not sdbcGMN4_4Cod.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Private Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text Then
        bRespetarComboGMN4 = True
        sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
        bRespetarComboGMN4 = False
        If oGMN4Seleccionado Is Nothing Then
            GMN4Seleccionado
        End If
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text Then
        bRespetarComboGMN4 = True
        sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
        bRespetarComboGMN4 = False
        If oGMN4Seleccionado Is Nothing Then
            GMN4Seleccionado
        End If
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        bRespetarComboGMN4 = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        
        bRespetarComboGMN4 = False
        GMN4Seleccionado
        bGMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub

Private Sub sdbcGMN4_4Den_Click()
    
     If Not sdbcGMN4_4Den.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub
Private Sub sdbcGMN1_4Cod_Change()

    If Not bRespetarComboGMN1 Then
    
        bRespetarComboGMN1 = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        bRespetarComboGMN1 = False
        
        Set oGMN1Seleccionado = Nothing
        
        If sdbcGMN1_4Cod.Value = "" Then
            bGMN1CargarComboDesde = False
        Else
            bGMN1CargarComboDesde = True
        End If
        
    End If

End Sub
Private Sub sdbcGMN1_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Den.RemoveAll

    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        If bGMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        If bGMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If bGMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If Not bRespetarComboGMN2 Then
    
        bRespetarComboGMN2 = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        bRespetarComboGMN2 = False
        
        Set oGMN2Seleccionado = Nothing
        
        If sdbcGMN2_4Cod.Value = "" Then
            bGMN2CargarComboDesde = False
        Else
            bGMN2CargarComboDesde = True
        End If
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    bRespetarComboGMN2 = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    bRespetarComboGMN2 = False
    
    GMN2Seleccionado
    
    bGMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer
    
    sdbcGMN2_4Cod.RemoveAll

    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN2 = Nothing
        If bGMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False)
        End If
    Else
        
        If bGMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If bGMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If Not bRespetarComboGMN2 Then

        bRespetarComboGMN2 = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        bRespetarComboGMN2 = False
        
        Set oGMN2Seleccionado = Nothing
        If sdbcGMN2_4Den.Value = "" Then
            bGMN2CargarComboDesde = False
        Else
            bGMN2CargarComboDesde = True
        End If
        
    End If

    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Den.Value = "" Then Exit Sub
    
    bRespetarComboGMN2 = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    bRespetarComboGMN2 = False
    
    GMN2Seleccionado
    
    bGMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer
    
    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN2 = Nothing
        If bGMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , False)
        End If
    
    Else
        
        If bGMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If bGMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh

    Screen.MousePointer = vbNormal
End Sub



Private Sub sdbcGMN3_4Cod_Change()
    
    If Not bRespetarComboGMN3 Then
    
        bRespetarComboGMN3 = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        bRespetarComboGMN3 = False
        
        Set oGMN3Seleccionado = Nothing
        If sdbcGMN3_4Cod.Value = "" Then
            bGMN3CargarComboDesde = False
        Else
            bGMN3CargarComboDesde = True
        End If
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
        
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    bRespetarComboGMN3 = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    bRespetarComboGMN3 = False
    
    GMN3Seleccionado
    
    bGMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Cod.RemoveAll

    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
       Set oGruposMN3 = Nothing
        If bGMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False)
        End If
    Else
        
         If bGMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If bGMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not bRespetarComboGMN1 Then

        bRespetarComboGMN1 = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        bRespetarComboGMN1 = False
        
        Set oGMN1Seleccionado = Nothing
        
        If sdbcGMN1_4Den.Value = "" Then
            bGMN1CargarComboDesde = False
        Else
            bGMN1CargarComboDesde = True
        End If
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Den.Value = "" Then Exit Sub
        
    bRespetarComboGMN1 = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    bRespetarComboGMN1 = False
    
    GMN1Seleccionado
    
    bGMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
    If Not bRespetarComboGMN4 Then
    
        bRespetarComboGMN4 = True
        sdbcGMN4_4Den.Text = ""
        bRespetarComboGMN4 = False
        
        sdbcArtiCod = ""
        sdbcArtiDen = ""
        
        Set oGMN4Seleccionado = Nothing
        If sdbcGMN4_4Cod.Value = "" Then
            bGMN4CargarComboDesde = False
        Else
            bGMN4CargarComboDesde = True
        End If
        
    End If
    
End Sub



Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Value = "" Then
        bGMN4CargarComboDesde = False
        Exit Sub
    End If
    
    bRespetarComboGMN4 = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    bRespetarComboGMN4 = False
    
    GMN4Seleccionado
    
    bGMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Cod.RemoveAll

    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN4 = Nothing
        If bGMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False)
        End If
    Else
        
         If bGMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If bGMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4Den_Change()
    
    If Not bRespetarComboGMN4 Then

        bRespetarComboGMN4 = True
        sdbcGMN4_4Cod.Text = ""
        bRespetarComboGMN4 = False
        Set oGMN4Seleccionado = Nothing
        If sdbcGMN4_4Cod.Value = "" Then
            bGMN4CargarComboDesde = False
        Else
            bGMN4CargarComboDesde = True
        End If
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Den.Value = "" Then
         bGMN4CargarComboDesde = False
        Exit Sub
    End If
    
    bRespetarComboGMN4 = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    bRespetarComboGMN4 = False
    
    GMN4Seleccionado
    
    bGMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Den.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN4 = Nothing
        If bGMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False)
        End If
    Else
        
        If bGMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If bGMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdAsignar_Click()
            
    
    Set oProvesSeleccionados = Nothing
    Set oProvesDesSeleccionados = Nothing
    Set oProvesSeleccionados = oFSGSRaiz.generar_CProveedores
    Set oProvesDesSeleccionados = oFSGSRaiz.generar_CProveedores
    
    frmPROVEBuscar.sOrigen = "ProvePorMat"
    If bREqp Then
        frmPROVEBuscar.codEqp = basOptimizacion.gCodEqpUsuario
    End If
    
'    frmPROVEBuscar.Caption = "Seleccione proveedores potenciales"
    frmPROVEBuscar.caption = sIdiPotenciales

    frmPROVEBuscar.Hide
    frmPROVEBuscar.Show 1
    
End Sub

Private Sub cmdDesAsignar_Click()

    Set oProvesSeleccionados = Nothing
    Set oProvesDesSeleccionados = Nothing
    Set oProvesDesSeleccionados = oFSGSRaiz.generar_CProveedores
    Set oProvesSeleccionados = oFSGSRaiz.generar_CProveedores
    
    Accion = accionessummit.ACCProvePorMatMod
    
    ModoEdicion
    
End Sub

Private Sub cmdlistado_Click()
    ''' * Objetivo: Obtener un listado de compradores por material
    
    If Not oGMN1Seleccionado Is Nothing Then
        frmLstPROVEProvPorMat.sGMN1Cod = oGMN1Seleccionado.Cod
        frmLstPROVEProvPorMat.txtEstMat = oGMN1Seleccionado.Cod
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        frmLstPROVEProvPorMat.sGMN2Cod = oGMN2Seleccionado.Cod
        frmLstPROVEProvPorMat.txtEstMat = frmLstPROVEProvPorMat.txtEstMat & " - " & oGMN2Seleccionado.Cod
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        frmLstPROVEProvPorMat.sGMN3Cod = oGMN3Seleccionado.Cod
        frmLstPROVEProvPorMat.txtEstMat = frmLstPROVEProvPorMat.txtEstMat & " - " & oGMN3Seleccionado.Cod
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        frmLstPROVEProvPorMat.sGMN4Cod = oGMN4Seleccionado.Cod
        frmLstPROVEProvPorMat.txtEstMat = frmLstPROVEProvPorMat.txtEstMat & " - " & oGMN4Seleccionado.Cod
    End If
    
    frmLstPROVEProvPorMat.Show vbModal


End Sub

Private Sub cmdRestaurar_Click()
        
    CargarProveedores
End Sub

Private Sub cmdSelMat_Click()
     
    frmSELMAT.sOrigen = "frmPROVEProvePorMat"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Hide
    frmSELMAT.Show 1

End Sub

Private Sub Form_Load()
    Dim oIMAsig As IMaterialAsignado
    
    Me.Height = 5100
    Me.Width = 7800
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Arrange
    
    Accion = accionessummit.ACCProvePorMatCon
    
    cmdAsignar.Enabled = False
    cmdDesAsignar.Enabled = False
    cmdRestaurar.Enabled = False
        
    ConfigurarNombres
    
    ConfigurarSeguridad
    
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
        'Si la aplicacion se ejecuta en modo pymes
        'Obtener el gmn1 de la pyme
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        bRespetarComboGMN1 = True
        sdbcGMN1_4Cod.Text = oGruposMN1.Item(1).Cod
        sdbcGMN1_4Den.Text = oGruposMN1.Item(1).Den
        
        Set oGMN1Seleccionado = Nothing
        Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        
        oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
        
    End If
    Set oProves = oFSGSRaiz.generar_CProveedores
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    m_bModoEdicion = False
End Sub
Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then

    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ProvePorGRUPModificar)) Is Nothing) Then
        bModif = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorGRUPRestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ProvePorGRUPRestEquipo)) Is Nothing) And basParametros.gParametrosGenerales.gbOblProveEqp Then
        bREqp = True
    End If

Else
    
    bModif = True

End If

If Not bModif Then
    
    cmdRestaurar.Left = cmdAsignar.Left
    cmdListado.Left = cmdDesAsignar.Left
    cmdAsignar.Visible = False
    cmdDesAsignar.Visible = False
    cmdModificarProve.Visible = False
End If

End Sub

Private Sub Form_Resize()

    Arrange
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

'Unload frmESTRProveProveorGruSel

Set oGruposMN1 = Nothing
Set oGruposMN2 = Nothing
Set oGruposMN3 = Nothing
Set oGruposMN4 = Nothing
Set oGMN1Seleccionado = Nothing
Set oGMN2Seleccionado = Nothing
Set oGMN3Seleccionado = Nothing
Set oGMN4Seleccionado = Nothing
Set oProves = Nothing
Set oPaises = Nothing
Set oProvesSeleccionados = Nothing
Set oProvesDesSeleccionados = Nothing
Me.Visible = False

End Sub

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    
    sdbgProveedores.Update
 
    If oProvesSeleccionados.Count > 0 Then
        Set oIProveAsig.Proveedores = Nothing
        Set oIProveAsig.Proveedores = oProvesSeleccionados
        
        teserror = oIProveAsig.AsignarProveedores
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
    End If
    
    If oProvesDesSeleccionados.Count > 0 Then
        Set oIProveAsig.Proveedores = Nothing
        Set oIProveAsig.Proveedores = oProvesDesSeleccionados
        
        Set oIProveAsig.Proveedores = Nothing
        Set oIProveAsig.Proveedores = oProvesDesSeleccionados
        teserror = oIProveAsig.DesAsignarProveedores
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
    End If
    
    If basParametros.gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
        'Hay que notificar a los usuarios de Gestion de Material de QA la [des| ]asignación de materiales de QA a Proveedores
        
        'Crear las Estructuras de Memoria:
        Dim oGMN1s As CGruposMatNivel1
        Dim oGMN2s As CGruposMatNivel2
        Dim oGMN3s As CGruposMatNivel3
        Dim oGMN4s As CGruposMatNivel4
        
        If oProvesSeleccionados.Count = 0 Then
            Set oProvesSeleccionados = Nothing
        End If
        If oProvesDesSeleccionados.Count = 0 Then
            Set oProvesDesSeleccionados = Nothing
        End If
        If Not oGMN4Seleccionado Is Nothing Then
            Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
            oGMN4s.Add oGMN4Seleccionado.GMN1Cod, oGMN4Seleccionado.GMN2Cod, oGMN4Seleccionado.GMN3Cod, oGMN4Seleccionado.GMN1Den, oGMN4Seleccionado.GMN2Den, oGMN4Seleccionado.GMN3Den, oGMN4Seleccionado.Cod, oGMN4Seleccionado.Den
        Else
            If Not oGMN3Seleccionado Is Nothing Then
                Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
                oGMN3s.Add oGMN3Seleccionado.GMN1Cod, oGMN3Seleccionado.GMN2Cod, oGMN3Seleccionado.Cod, oGMN3Seleccionado.GMN1Den, oGMN3Seleccionado.GMN2Den, oGMN3Seleccionado.Den
            Else
                If Not oGMN2Seleccionado Is Nothing Then
                    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
                    oGMN2s.Add oGMN2Seleccionado.GMN1Cod, oGMN2Seleccionado.GMN1Den, oGMN2Seleccionado.Cod, oGMN2Seleccionado.Den
                Else
                    If Not oGMN1Seleccionado Is Nothing Then
                        Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
                        oGMN1s.Add oGMN1Seleccionado.Cod, oGMN1Seleccionado.Den
                    End If
                End If
            End If
        End If
        
        teserror = NotificarAsigMatQA(sNotificarAsigMatQASubject, oProvesSeleccionados, oGMN1s, oGMN2s, oGMN3s, oGMN4s, oProvesDesSeleccionados, oGMN1s, oGMN2s, oGMN3s, oGMN4s)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            'Exit Sub
        End If
    End If
    
    Set oProvesSeleccionados = Nothing
    Set oProvesDesSeleccionados = Nothing
    cmdRestaurar_Click
    ModoConsulta
End Sub

Private Sub cmdCancelar_Click()
    sdbgProveedores.CancelUpdate
    sdbgProveedores.DataChanged = False
    ModoConsulta
    Set oProvesSeleccionados = Nothing
    Set oProvesDesSeleccionados = Nothing
End Sub

Private Sub sdbcGMN4_4Den_Validate(Cancel As Boolean)
    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If stabProve.Tab = 1 Then Exit Sub
    
    If sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    If sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text Then
        bRespetarComboGMN4 = True
        sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
        bRespetarComboGMN4 = False
        If oGMN4Seleccionado Is Nothing Then
            GMN4Seleccionado
        End If
        Exit Sub
    End If
    
    If sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text Then
        bRespetarComboGMN4 = True
        sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
        bRespetarComboGMN4 = False
        If oGMN4Seleccionado Is Nothing Then
            GMN4Seleccionado
        End If
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, , Trim(sdbcGMN4_4Den), , True, False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4Den.Text, True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Den.Text = ""
    Else
        bRespetarComboGMN4 = True
        sdbcGMN4_4Cod.Text = oGMN4s.Item(1).Cod
        
        sdbcGMN4_4Den.Columns(0).Value = sdbcGMN4_4Den.Text
        sdbcGMN4_4Den.Columns(1).Value = sdbcGMN4_4Cod.Text
        
        bRespetarComboGMN4 = False
        GMN4Seleccionado
        bGMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub

Private Sub sdbgProveedores_Change()
   
    If sdbgProveedores.Columns(0).Value = "-1" Or sdbgProveedores.Columns(0).Value = "1" Then
        ' Checked
        If oProves.Item(CStr(sdbgProveedores.Columns(1).Value)) Is Nothing Then
            'Si no esta asignado desde antes, lo asignaremos
            oProvesSeleccionados.Add sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
        End If
        
        oProvesDesSeleccionados.Remove CStr(sdbgProveedores.Columns(1).Value)
    Else
        ' Unchecked
        If Not oProves.Item(CStr(sdbgProveedores.Columns(1).Value)) Is Nothing Then
                oProves.CargarDatosProveedor (CStr(sdbgProveedores.Columns(1).Value))
                If Not ComprobarAtributosEnArticulos(oProves.Item(CStr(sdbgProveedores.Columns(1).Value))) Then
                    sdbgProveedores.CancelUpdate
                    Exit Sub
                End If
                If Not oProves.Item(CStr(sdbgProveedores.Columns(1).Value)).EsPremium Then
                    oProvesDesSeleccionados.Add sdbgProveedores.Columns(1).Value, "", "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
                Else
                    oMensajes.ImposibleDesasignarProveedorPremium
                    sdbgProveedores.CancelUpdate
                    Exit Sub
                End If
        End If
        
        oProvesSeleccionados.Remove CStr(sdbgProveedores.Columns(1).Value)
 
    
    End If
   
   sdbgProveedores.Update
   
End Sub
Private Sub CargarProveedores(Optional ByVal OrdPorDen As Boolean, Optional ByVal OrdPorCalif1 As Boolean, Optional ByVal OrdPorCalif2 As Boolean, Optional ByVal OrdPorcalif3 As Boolean)
Dim Codigos As TipoDatosProveedores
Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    'Ahora hay que cargar los proveedores
    sdbgProveedores.RemoveAll
    
    If bREqp Then
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, , , , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , OrdPorDen, OrdPorCalif1, OrdPorCalif2, OrdPorcalif3, False
    Else
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , OrdPorDen, OrdPorCalif1, OrdPorCalif2, OrdPorcalif3, False
    End If
    
        Codigos = oProves.DevolverLosDatos
        
        For i = 0 To UBound(Codigos.Cod) - 1
           
            sdbgProveedores.AddItem "1" & Chr(m_lSeparador) & Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cal1(i) & Chr(m_lSeparador) & Codigos.Cal2(i) & Chr(m_lSeparador) & Codigos.Cal3(i)
           
        Next
        
        If Not oProves.EOF Then
            sdbgProveedores.AddItem "0" & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "....."
        End If
         
        If bModif Then
            cmdDesAsignar.Enabled = True
            cmdAsignar.Enabled = True
        End If
        cmdRestaurar.Enabled = True
        
    Screen.MousePointer = vbNormal
End Sub
Private Sub GMN1Seleccionado()
    
    sdbcGMN2_4Cod.RemoveAll
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN2_4Den.RemoveAll
    sdbcGMN2_4Cod.Text = ""
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcGMN2_4Den.Text = ""
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    stabProve.Enabled = True
    
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    
    stabProve.Enabled = True
    
End Sub
Private Sub GMN3Seleccionado()
    

    Set oGMN3Seleccionado = Nothing
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    
    stabProve.Enabled = True
    
End Sub
Private Sub GMN4Seleccionado()
    

    Set oGMN4Seleccionado = Nothing
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
    stabProve.Enabled = True
    
End Sub

Private Sub ArticuloSeleccionado()
    

    Set oArtiSeleccionado = Nothing
    Set oArtiSeleccionado = oFSGSRaiz.generar_CArticulo
    oArtiSeleccionado.GMN1Cod = sdbcGMN1_4Cod
    oArtiSeleccionado.GMN2Cod = sdbcGMN2_4Cod
    oArtiSeleccionado.GMN3Cod = sdbcGMN3_4Cod
    oArtiSeleccionado.GMN4Cod = sdbcGMN4_4Cod
    
End Sub
Private Sub ConfigurarNombres()

    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"

    sdbgProveedores.Columns(3).caption = gParametrosGenerales.gsDEN_CAL1
    sdbgProveedores.Columns(4).caption = gParametrosGenerales.gsDEN_CAL2
    sdbgProveedores.Columns(5).caption = gParametrosGenerales.gsDEN_CAL3
    
End Sub

Private Sub sdbgProveedores_HeadClick(ByVal ColIndex As Integer)
    If m_bModoEdicion Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    CargarProveedores (ColIndex = 2), (ColIndex = 3), (ColIndex = 4), (ColIndex = 5)
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgProveHom_Change()
    
    sdbgProveHom.Update
    
End Sub

Private Sub sdbgProveHom_HeadClick(ByVal ColIndex As Integer)
    Dim oProve As CProveedor
    
    sdbgProveHom.RemoveAll
    
    Select Case basParametros.gParametrosGenerales.giNEM
            
            Case 1
            
            Case 2
            
            Case 3
            
            Case 4
                    Screen.MousePointer = vbHourglass
                    Set oProveedores = oIProveHom.DevolverProveedoresDelArticulo(sdbcArtiCod, basOptimizacion.gCodEqpUsuario, (ColIndex = 1), (ColIndex = 2))
        
        End Select
        
        
        
        For Each oProve In oProveedores
            
            sdbgProveHom.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den & Chr(m_lSeparador) & oProve.Homologado
        
        Next
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub stabPROVE_Click(PreviousTab As Integer)
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer
Dim oProve As CProveedor

    If m_bDesactivarTabClick = True Then Exit Sub

    If stabProve.Tab = 1 Then
        'Tab 1 pulsado
        If m_bModoEdicion = True Then Exit Sub
        
        If oArtiSeleccionado Is Nothing Then
            
            picProve.Visible = True
            sdbgProveedores.Visible = True
            sdbgProveHom.Visible = False
            picProveHom.Visible = False
            picNavigate.Visible = True
            
            If oGMN4Seleccionado Is Nothing And oGMN3Seleccionado Is Nothing And oGMN2Seleccionado Is Nothing And oGMN1Seleccionado Is Nothing Then
                stabProve.Tab = 0
                Exit Sub
            End If
                    
                    
            If Not oGMN4Seleccionado Is Nothing Then
                Set oIProveAsig = oGMN4Seleccionado
                
                CargarProveedores
                Exit Sub
            End If
            
            If Not oGMN3Seleccionado Is Nothing Then
                Set oIProveAsig = oGMN3Seleccionado
                If bRMat Then
                    Set oICompAsignado = oGMN3Seleccionado
                    iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                    If iNivelAsig = 0 Or iNivelAsig > 3 Then
                        stabProve.Tab = 0
                        Exit Sub
                    End If
                End If
                
                CargarProveedores
                Exit Sub
            End If
            
            If Not oGMN2Seleccionado Is Nothing Then
                Set oICompAsignado = oGMN2Seleccionado
                Set oIProveAsig = oGMN2Seleccionado
                If bRMat Then
                    iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                    If iNivelAsig = 0 Or iNivelAsig > 2 Then
                        stabProve.Tab = 0
                        Exit Sub
                    End If
                        
                End If
                
                
                CargarProveedores
                
                Exit Sub
            End If
            
            If Not oGMN1Seleccionado Is Nothing Then
                Set oICompAsignado = oGMN1Seleccionado
                Set oIProveAsig = oGMN1Seleccionado
                  If bRMat Then
                    iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                    If iNivelAsig < 1 Or iNivelAsig > 2 Then
                        stabProve.Tab = 0
                        Exit Sub
                    End If
                End If
                
                
                CargarProveedores
                Exit Sub
            End If
                
        Else
            
            
            sdbgProveHom.RemoveAll
            
            sdbgProveedores.Visible = False
            sdbgProveHom.Visible = True
            picProve.Visible = True
            picNavigate.Visible = False
            picProveHom.Visible = True
            
            Set oIProveHom = oArtiSeleccionado
             If bREqp Then
                Set oProveedores = oIProveHom.DevolverProveedoresDelArticulo(sdbcArtiCod, basOptimizacion.gCodEqpUsuario)
            Else
                Set oProveedores = oIProveHom.DevolverProveedoresDelArticulo(sdbcArtiCod)
            End If
            For Each oProve In oProveedores
                
                sdbgProveHom.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den & Chr(m_lSeparador) & oProve.Homologado
            
            Next
            
            
        End If
        
            
    Else
        'Tab 0 pulsado
        If Accion = accionessummit.ACCProvePorMatMod Then
            If m_bModoEdicion Then
                m_bDesactivarTabClick = True
                stabProve.Tab = 1
                m_bDesactivarTabClick = False
                Exit Sub
            End If
    
            sdbgProveedores.RemoveAll
            stabProve.Tab = 1
        Else
            
            sdbgProveedores.RemoveAll
            cmdAsignar.Enabled = False
            cmdDesAsignar.Enabled = False
            cmdRestaurar.Enabled = False
        End If
            
    End If
End Sub

Private Sub ModoEdicion()
    Accion = ACCProvePorMatMod
    sdbgProveedores.AllowUpdate = True
    picProve.BackColor = &H808000
    picNavigate.Visible = False
    picEdit.Visible = True
    m_bModoEdicion = True
End Sub

Private Sub ModoConsulta()
    Accion = ACCProvePorMatCon
    
    picNavigate.Visible = True
    picEdit.Visible = False
    sdbgProveedores.AllowUpdate = False
    picProve.BackColor = vbInactiveTitleBar
    
    m_bModoEdicion = False
End Sub
Public Sub CargarProveedoresConBusqueda()
Dim i As Integer

If frmPROVEBuscar.sdbgProveedores.SelBookmarks.Count = 0 Then Exit Sub

For i = 0 To frmPROVEBuscar.sdbgProveedores.SelBookmarks.Count - 1
    
    With frmPROVEBuscar.sdbgProveedores
        .Bookmark = .SelBookmarks.Item(i)
        
        If oProves.Item(CStr(.Columns(0).Value)) Is Nothing Then
            'Si no esta asignado desde antes, lo asignaremos
            oProvesSeleccionados.Add .Columns(0).Value, "", "", "", "", "", "", "", "", "", "", 0, 0, 0, "", "", ""
            sdbgProveedores.AddItem "1" & Chr(m_lSeparador) & .Columns(0).Value & Chr(m_lSeparador) & .Columns(1).Value & Chr(m_lSeparador) & .Columns(2).Value & Chr(m_lSeparador) & .Columns(3).Value & Chr(m_lSeparador) & .Columns(4).Value
        End If
        
        oProvesDesSeleccionados.Remove CStr(.Columns(0).Value)
        
    End With
    
Next
    
Unload frmPROVEBuscar

ModoEdicion



End Sub
Public Sub PonerMatSeleccionadoEnCombos()
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
        
    End If
    
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVE_PROVEPORMAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        
        stabProve.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        stabProve.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        
        lblArtiCod.caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgProveedores.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgProveedores.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgProveedores.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgProveedores.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgProveedores.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgProveedores.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        
        cmdAceptar.caption = Ador(0).Value
        cmdAceptarProve.caption = Ador(0).Value
        Ador.MoveNext
        cmdAsignar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        cmdCancelarProve.caption = Ador(0).Value
        Ador.MoveNext
        cmdDesAsignar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdModificarProve.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        sdbcArtiCod.Columns(0).caption = Ador(0).Value
        sdbcArtiDen.Columns(1).caption = Ador(0).Value

        Ador.MoveNext
       
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        sdbcArtiCod.Columns(1).caption = Ador(0).Value
        sdbcArtiDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sIdiPotenciales = Ador(0).Value
#If VERSION >= 31000 Then
        Ador.MoveNext
        sNotificarAsigMatQASubject = Ador(0).Value
#End If
                
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub


Private Function ComprobarAtributosEnArticulos(ByVal oProve As CProveedor) As Boolean
Dim bExisten As Boolean
Dim irespuesta As Integer
Dim oIMaterial As IMaterialAsignado

    Set oIMaterial = oProve
    If Not oGMN4Seleccionado Is Nothing Then
        Set oIMaterial.GruposMN4 = oFSGSRaiz.Generar_CGruposMatNivel4
        oIMaterial.GruposMN4.Add oGMN4Seleccionado.GMN1Cod, oGMN4Seleccionado.GMN2Cod, oGMN4Seleccionado.GMN3Cod, oGMN4Seleccionado.GMN1Den, oGMN4Seleccionado.GMN2Den, oGMN4Seleccionado.GMN3Den, oGMN4Seleccionado.Cod, oGMN4Seleccionado.Den
    ElseIf Not oGMN3Seleccionado Is Nothing Then
        Set oIMaterial.GruposMN3 = oFSGSRaiz.Generar_CGruposMatNivel3
        oIMaterial.GruposMN3.Add oGMN3Seleccionado.GMN1Cod, oGMN3Seleccionado.GMN2Cod, oGMN3Seleccionado.Cod, oGMN3Seleccionado.GMN1Den, oGMN3Seleccionado.GMN2Den, oGMN3Seleccionado.Den
    ElseIf Not oGMN2Seleccionado Is Nothing Then
        Set oIMaterial.GruposMN2 = oFSGSRaiz.Generar_CGruposMatNivel2
        oIMaterial.GruposMN2.Add oGMN2Seleccionado.GMN1Cod, oGMN2Seleccionado.GMN1Den, oGMN2Seleccionado.Cod, oGMN2Seleccionado.Den
    ElseIf Not oGMN1Seleccionado Is Nothing Then
        Set oIMaterial.GruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        oIMaterial.GruposMN1.Add oGMN1Seleccionado.Cod, oGMN1Seleccionado.Den
    Else
        ComprobarAtributosEnArticulos = True
        Set oIMaterial = Nothing
        Exit Function
    End If
    bExisten = oIMaterial.ExistenValoresDeAtributos
    If bExisten Then
      irespuesta = oMensajes.PreguntaDesasignarMaterialProve(1)
      If irespuesta = vbYes Then
          ComprobarAtributosEnArticulos = True
      Else
          ComprobarAtributosEnArticulos = False
      End If
    Else
      ComprobarAtributosEnArticulos = True
    End If
    
    Set oIMaterial = Nothing
End Function

