VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmFlujosConfVisibilidadDef 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "dVisibilidad por defecto"
   ClientHeight    =   6840
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7935
   Icon            =   "frmFlujosConfVisibilidadDef.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6840
   ScaleWidth      =   7935
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdBajarCampo 
      Height          =   312
      Left            =   120
      Picture         =   "frmFlujosConfVisibilidadDef.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1440
      Width           =   375
   End
   Begin VB.CommandButton cmdSubirCampo 
      Height          =   312
      Left            =   120
      Picture         =   "frmFlujosConfVisibilidadDef.frx":01A4
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1080
      Width           =   375
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   5415
      Left            =   540
      TabIndex        =   0
      Top             =   1080
      Width           =   6735
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   9
      stylesets.count =   7
      stylesets(0).Name=   "Calculado"
      stylesets(0).BackColor=   16766421
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFlujosConfVisibilidadDef.frx":01FE
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "No"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFlujosConfVisibilidadDef.frx":0265
      stylesets(1).AlignmentPicture=   2
      stylesets(2).Name=   "S�"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmFlujosConfVisibilidadDef.frx":0281
      stylesets(2).AlignmentPicture=   2
      stylesets(3).Name=   "Gris"
      stylesets(3).BackColor=   -2147483633
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmFlujosConfVisibilidadDef.frx":029D
      stylesets(4).Name=   "Normal"
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmFlujosConfVisibilidadDef.frx":02B9
      stylesets(5).Name=   "ActiveRow"
      stylesets(5).ForeColor=   16777215
      stylesets(5).BackColor=   8388608
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmFlujosConfVisibilidadDef.frx":02D5
      stylesets(6).Name=   "Amarillo"
      stylesets(6).BackColor=   12648447
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmFlujosConfVisibilidadDef.frx":02F1
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowDelete     =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6720
      Columns(1).Caption=   "DDato"
      Columns(1).Name =   "DATO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "TIPO"
      Columns(2).Name =   "TIPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   2
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "CAMPO_GS"
      Columns(3).Name =   "CAMPO_GS"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   2
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "SUBTIPO"
      Columns(4).Name =   "SUBTIPO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   2
      Columns(4).FieldLen=   256
      Columns(5).Width=   1376
      Columns(5).Caption=   "Dvis."
      Columns(5).Name =   "VISIBLE"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   11
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(6).Width=   1376
      Columns(6).Caption=   "DEsc."
      Columns(6).Name =   "ESCRITURA"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   11
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(7).Width=   1376
      Columns(7).Caption=   "DObl."
      Columns(7).Name =   "OBLIGATORIO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   11
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "INTRO"
      Columns(8).Name =   "INTRO"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   11
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      _ExtentX        =   11889
      _ExtentY        =   9551
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.TabStrip ssTabGrupos 
      Height          =   6060
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   10689
      _Version        =   327682
      BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
         NumTabs         =   1
         BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   ""
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCumplimentacion 
      Caption         =   "DIndique que campos tendr� visibles, cuales podr� modificar, y cuales ser�n obligatorios de cumplimentar:"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   240
      Width           =   7800
   End
End
Attribute VB_Name = "frmFlujosConfVisibilidadDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables Publicas
Public lIdFlujo As Long

'Variables Privadas
Private msDato As String
Private msVisible As String
Private msEscritura As String
Private msObligatorio As String
Private m_oRol As CPMRol

Private m_bErrorCumplimentaciones As Boolean

''' Variables de control
Private bModoEdicion As Boolean

Public m_lIdRol As Long
Public m_sRol As String
Public m_udtTipo As TipoPMRol
Public m_bModifFlujo As Boolean
Public m_lIdFormulario As Long

Private Sub cmdBajarCampo_Click()
Dim i As Integer
Dim arrValoresMat() As Variant
Dim arrValoresArt() As Variant
Dim arrValoresDen() As Variant
Dim arrValoresAux() As Variant
Dim bOrdenoMat, bOrdenoPais As Boolean
Dim bOrdenoArt, bOrdenoProvi As Boolean
Dim bOrdenoCampoMat, bOrdenoCampoPais As Boolean
Dim bOrdenoDen As Boolean
Dim vbm As Variant

    If sdbgCampos.SelBookmarks.Count = 0 Then Exit Sub
    
    If sdbgCampos.AddItemRowIndex(sdbgCampos.SelBookmarks.Item(0)) = sdbgCampos.Rows - 1 Then Exit Sub
        
    i = 0
    ReDim arrValoresMat(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresArt(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresDen(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresAux(sdbgCampos.Columns.Count - 1)
    
    'Ordenacion del Material // Provincia
    If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.Provincia) Or _
        (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And ((sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.CodArticulo) Or (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.NuevoCodArticulo))) Then
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.DenArticulo) Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                bOrdenoMat = True
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdenoPais = True
            End If
                
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).Value
            Next i
    'Ordenacion del articulo // Provincia
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Provincia Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
        vbm = sdbgCampos.GetBookmark(-1)
        If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Provincia And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais) Or _
           ((sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) Or (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo) And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material) Then
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Provincia Or sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) <> TipoCampoGS.DenArticulo Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
                bOrdenoProvi = True
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdenoArt = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    'Ordenacion de la denominacion
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
        If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
        bOrdenoDen = True
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
            arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
        Next i
    Else
        vbm = sdbgCampos.GetBookmark(1)
        
        If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.Provincia) Or _
           (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And ((sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.CodArticulo) Or sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.NuevoCodArticulo)) Then
            'Por si debajo del campo a bajar, esta el Material // Provincia
            If (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoGS.DenArticulo) Then
                bOrdenoCampoMat = True
            Else
                bOrdenoCampoPais = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
            Next i
        End If
    End If
    
    
    If bOrdenoMat Or bOrdenoPais Then
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        
        If bOrdenoMat Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        sdbgCampos.MovePrevious
        End If
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If bOrdenoMat Then
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresAux(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -3
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -2
        End If
        
        sdbgCampos.MoveNext
        sdbgCampos.SelBookmarks.RemoveAll

    ElseIf bOrdenoArt Or bOrdenoProvi Then
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)  'donde est� la provincia pongo el pa�s
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        If bOrdenoArt Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        sdbgCampos.MovePrevious
        End If
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If bOrdenoArt Then
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresAux(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -3
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -2
        End If
        
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.SelBookmarks.RemoveAll
    
    ElseIf bOrdenoCampoMat Or bOrdenoCampoPais Then
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        If bOrdenoCampoMat Then
            sdbgCampos.MoveNext
        End If
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        If bOrdenoCampoMat Then
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 3
        Else
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 2
        End If
        
        If bOrdenoCampoMat Then
            sdbgCampos.MovePrevious
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -1
        End If
      
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -1
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -1
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        If bOrdenoCampoMat Then
            sdbgCampos.MoveNext
        End If
        sdbgCampos.SelBookmarks.RemoveAll
        
    ElseIf bOrdenoDen = True Then
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresDen(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -3
        
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.SelBookmarks.RemoveAll
        

    Else  'Ordeno normal,de 1 en 1

        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
        Next i
        
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        sdbgCampos.MovePrevious
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -1
        
        sdbgCampos.MoveNext
        sdbgCampos.SelBookmarks.RemoveAll
        

    End If
    sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark
    sdbgCampos.Update
    
End Sub

Private Sub cmdSubirCampo_Click()
    Dim i As Integer
    Dim arrValoresMat() As Variant
    Dim arrValoresArt() As Variant
    Dim arrValoresDen() As Variant
    Dim arrValoresAux() As Variant
    Dim bOrdenoMat, bOrdenoPais As Boolean
    Dim bOrdenoArt, bOrdenoProv As Boolean
    Dim bOrdenoDen As Boolean
    Dim bOrdenoCampoMat, bOrdenoCampoProv As Boolean
    Dim vbm As Variant

    If sdbgCampos.SelBookmarks.Count = 0 Then Exit Sub
    
    If sdbgCampos.AddItemRowIndex(sdbgCampos.SelBookmarks.Item(0)) = 0 Then Exit Sub
    
    ReDim arrValoresMat(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresArt(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresDen(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresAux(sdbgCampos.Columns.Count - 1)
    i = 0
    '''Ordenacion del Material // Pais
    If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Or (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
        vbm = sdbgCampos.GetBookmark(1)
        If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Provincia) Or _
           (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Or _
           (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo) Then
           
            If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Provincia) Or _
            (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2))) <> TipoCampoGS.DenArticulo Then
                bOrdenoPais = True
                For i = 0 To sdbgCampos.Columns.Count - 1
                   arrValoresMat(i) = sdbgCampos.Columns(i).Value
                   arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(vbm)
                
                Next i
            Else
                bOrdenoMat = True
                For i = 0 To sdbgCampos.Columns.Count - 1
                    arrValoresMat(i) = sdbgCampos.Columns(i).Value
                    arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(vbm)
                    arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                Next i
            End If
           
        End If
    'Ordenacion del c�digo del art�culo // Provincia
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or _
    sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or _
     sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Provincia Then
        If val(sdbgCampos.Row) = 1 Then Exit Sub
        vbm = sdbgCampos.GetBookmark(-1)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
            bOrdenoArt = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
                arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(1))
            Next i
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
            bOrdenoProv = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
            Next i
        End If

    'Ordenacion de la denominacion
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
        If val(sdbgCampos.Row) = 2 Then Exit Sub
        bOrdenoDen = True
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
            arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
        Next i
        
    Else
        vbm = sdbgCampos.GetBookmark(-1)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.DenArticulo Or _
         sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Provincia Or _
         sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo Then
           If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.DenArticulo Then
               bOrdenoCampoMat = True
           Else
               bOrdenoCampoProv = True
           End If
        End If
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
        Next i
       
         
    End If

    If bOrdenoMat Or bOrdenoArt Or bOrdenoDen Or bOrdenoPais Or bOrdenoProv Then
        sdbgCampos.MovePrevious
        If bOrdenoArt Or bOrdenoDen Or bOrdenoProv Then
            sdbgCampos.MovePrevious
        End If
        
        If bOrdenoDen Then
            sdbgCampos.MovePrevious
        End If
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -1
        sdbgCampos.MoveNext
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -1
        
        If Not bOrdenoPais And Not bOrdenoProv Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -1
        End If
        
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        
        If bOrdenoMat Or bOrdenoArt Or bOrdenoDen Then
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 3
        Else
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 2
        End If
        
        sdbgCampos.SelBookmarks.RemoveAll
        
        If Not bOrdenoArt And Not bOrdenoDen And Not bOrdenoPais And Not bOrdenoProv Then
            sdbgCampos.MovePrevious
        End If
        If Not bOrdenoDen And Not bOrdenoProv Then
            sdbgCampos.MovePrevious
        End If
        sdbgCampos.MovePrevious

    ElseIf bOrdenoCampoMat Or bOrdenoCampoProv Then
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If Not bOrdenoCampoProv Then
            sdbgCampos.MovePrevious
        End If

        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        If bOrdenoCampoProv Then
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -2
        Else
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -3
        End If

        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1

        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        
        If Not bOrdenoCampoProv Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        End If

        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If Not bOrdenoCampoProv Then
            sdbgCampos.MovePrevious
        End If

    Else  'Ordeno normal,de 1 en 1
    
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), -1
        sdbgCampos.MoveNext

        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        GrabarOrden CStr(sdbgCampos.Columns("ID").Value), 1
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MovePrevious
        
   End If

    sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark
    sdbgCampos.Update
    
   
End Sub

Sub GrabarOrden(idx As String, Orden As Integer)
Dim oCumplimentacion As CPMConfCumplimentacion
Dim teserror As TipoErrorSummit

Set oCumplimentacion = m_oRol.Cumplimentaciones.Item(idx)
    
oCumplimentacion.Orden = oCumplimentacion.Orden + Orden


teserror = oCumplimentacion.ModificarOrdenCumplimentacionDef
    
If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    Set oCumplimentacion = Nothing
    Exit Sub
End If
Set oCumplimentacion = Nothing
'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
frmFlujos.HayCambios
End Sub



Private Sub Form_Load()
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    ConfigurarSeguridad '?
    
    bModoEdicion = False
    
    PonerFieldSeparator Me
    
    CargarDatosTab
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSCONFVISIBILIDADDEF, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value '1 Visibilidad por defecto
        Ador.MoveNext
        Me.lblCumplimentacion.caption = Ador(0).Value '2 Indique que campos tendr� visibles, cuales podr� modificar...
        Ador.MoveNext
        msDato = Ador(0).Value '3 Dato
        Ador.MoveNext
        msVisible = Ador(0).Value '4 Vis. (Visible)
        Ador.MoveNext
        msEscritura = Ador(0).Value '5 Esc. (Escritura)
        Ador.MoveNext
        msObligatorio = Ador(0).Value '6 Obl. (Obligatorio)
        Ador.MoveNext
        cmdSubirCampo.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdBajarCampo.ToolTipText = Ador(0).Value
        Ador.MoveNext

        If m_udtTipo = RolObservador Then
            Me.lblCumplimentacion.caption = Ador(0).Value ' Indique que campos tendr� visibles
        End If
        
        Ador.Close
    End If
End Sub

Private Sub ConfigurarSeguridad()
    sdbgCampos.Columns("ESCRITURA").Visible = Not (m_udtTipo = RolObservador)
    sdbgCampos.Columns("OBLIGATORIO").Visible = Not (m_udtTipo = RolObservador)
    
End Sub

Private Sub CargarDatosTab()
    CargarGrupos
    CargarCumplimentacionRol
End Sub

Private Sub CargarCumplimentacionRol()
    Dim oCumplimentacion As CPMConfCumplimentacion
    sdbgCampos.RemoveAll
        
    Set m_oRol = oFSGSRaiz.Generar_CPMRol
    m_oRol.CargarCumplimentacionDef m_lIdRol, CLng(ssTabGrupos.selectedItem.Tag)
        
    If Not m_oRol.Cumplimentaciones Is Nothing Then
        For Each oCumplimentacion In m_oRol.Cumplimentaciones
            sdbgCampos.AddItem oCumplimentacion.IdCampo & Chr(m_lSeparador) & oCumplimentacion.Campo.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oCumplimentacion.Campo.TipoPredef & Chr(m_lSeparador) & oCumplimentacion.Campo.CampoGS & Chr(m_lSeparador) & oCumplimentacion.Campo.Tipo & Chr(m_lSeparador) & oCumplimentacion.Visible & Chr(m_lSeparador) & oCumplimentacion.Escritura & Chr(m_lSeparador) & oCumplimentacion.Obligatorio & Chr(m_lSeparador) & oCumplimentacion.Intro
        Next
    End If
End Sub

Private Sub CargarGrupos()
Dim oFormulario As CFormulario
Dim iTab As Integer
Dim oGrupo As CFormGrupo

    
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = m_lIdFormulario
    oFormulario.CargarTodosLosGrupos
    
    ssTabGrupos.Tabs.clear
    iTab = 1
    
    For Each oGrupo In oFormulario.Grupos
        ssTabGrupos.Tabs.Add iTab, "A" & oGrupo.Id, NullToStr(oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        ssTabGrupos.Tabs(iTab).Tag = CStr(oGrupo.Id)
    
        iTab = iTab + 1
    Next
    
    Set oGrupo = Nothing
    Set oFormulario = Nothing
    
End Sub

Private Sub Form_Resize()
    If Me.Width < 2000 Or Me.Height < 4275 Then Exit Sub

    ssTabGrupos.Width = Me.Width - 540
    sdbgCampos.Width = Me.Width - 1215
    If m_udtTipo = RolObservador Then
        sdbgCampos.Columns("DATO").Width = sdbgCampos.Width - sdbgCampos.Columns("VISIBLE").Width - 580
    Else
        sdbgCampos.Columns("DATO").Width = sdbgCampos.Width - sdbgCampos.Columns("VISIBLE").Width - sdbgCampos.Columns("ESCRITURA").Width - sdbgCampos.Columns("OBLIGATORIO").Width - 600
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If (actualizarYSalir() = True) Then
        Cancel = True
        Exit Sub
    End If
    Me.Visible = False
    Set m_oRol = Nothing
End Sub

Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
End Function

Private Sub sdbgCampos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim oCumplimentacion As CPMConfCumplimentacion
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit

    If sdbgCampos.Columns("ID").Value <> "" And sdbgCampos.Columns("ID").Value <> "0" Then
        Set oCumplimentacion = m_oRol.Cumplimentaciones.Item(CStr(sdbgCampos.Columns("ID").Value))
        If Not oCumplimentacion Is Nothing Then
            oCumplimentacion.Visible = CBool(sdbgCampos.Columns("VISIBLE").Value)
            oCumplimentacion.Escritura = CBool(sdbgCampos.Columns("ESCRITURA").Value)
            oCumplimentacion.Obligatorio = CBool(sdbgCampos.Columns("OBLIGATORIO").Value)
            teserror = oCumplimentacion.ModificarCumplimentacionDef
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                sdbgCampos.CancelUpdate
                If Me.Visible Then sdbgCampos.SetFocus
                Set oCumplimentacion = Nothing
                Set oIBaseDatos = Nothing
                m_bErrorCumplimentaciones = True
                Exit Sub
            End If
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
            Set oCumplimentacion = Nothing
            Set oIBaseDatos = Nothing
            m_bErrorCumplimentaciones = False
        End If
    End If
End Sub

Private Sub sdbgCampos_BtnClick()
    If sdbgCampos.col < 0 Then Exit Sub
    Select Case sdbgCampos.Columns(sdbgCampos.col).Name
        Case "DATO"
            If ssTabGrupos.selectedItem.Tag <> "" And ssTabGrupos.selectedItem.Tag <> "0" And _
                sdbgCampos.Columns("ID").Value > 0 Then
                    frmFlujosConfVisibilidadDefDesglose.m_lIdRol = m_lIdRol
                    frmFlujosConfVisibilidadDefDesglose.m_sRol = m_sRol
                    frmFlujosConfVisibilidadDefDesglose.m_lGrupo = CLng(ssTabGrupos.selectedItem.Tag)
                    frmFlujosConfVisibilidadDefDesglose.m_sGrupo = ssTabGrupos.selectedItem.caption
                    frmFlujosConfVisibilidadDefDesglose.m_lCampoPadre = sdbgCampos.Columns("ID").Value
                    frmFlujosConfVisibilidadDefDesglose.m_sCampoPadre = sdbgCampos.Columns("DATO").Value
                    frmFlujosConfVisibilidadDefDesglose.m_udtTipo = m_udtTipo
                    frmFlujosConfVisibilidadDefDesglose.m_bModifFlujo = m_bModifFlujo
                    frmFlujosConfVisibilidadDefDesglose.Show vbModal
                    Unload frmFlujosConfVisibilidadDefDesglose
            End If

    End Select
End Sub

Private Sub sdbgCampos_Change()

    If sdbgCampos.col < 0 Then Exit Sub
                    
    Select Case sdbgCampos.Columns(sdbgCampos.col).Name
        Case "VISIBLE"
            If Not sdbgCampos.Columns("VISIBLE").Value Then
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Or _
                   sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgCampos.Columns("VISIBLE").Value = True
                        Exit Sub
                End If
                                                    
                sdbgCampos.Columns("ESCRITURA").Value = False
                sdbgCampos.Columns("OBLIGATORIO").Value = False
            End If
            
        Case "ESCRITURA"
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.PrecioUnitarioAdj Or _
                sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.ProveedorAdj Or _
                sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.CantidadAdj Or _
                sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NumSolicitERP Or _
                sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.ImporteSolicitudesVinculadas Or _
                sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                    sdbgCampos.Columns("ESCRITURA").Value = False
                    Exit Sub
            End If
            
            If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                sdbgCampos.Columns("ESCRITURA").Value = False
                Exit Sub
            End If
            
            If Not sdbgCampos.Columns("ESCRITURA").Value Then
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Or _
                   sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgCampos.Columns("ESCRITURA").Value = True
                        Exit Sub
                End If
                
                sdbgCampos.Columns("OBLIGATORIO").Value = False
            Else
                If Not sdbgCampos.Columns("VISIBLE").Value Then
                    sdbgCampos.Columns("ESCRITURA").Value = False
                    Exit Sub
                End If
            End If
            
        Case "OBLIGATORIO"
            If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Or _
                sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                sdbgCampos.Columns("OBLIGATORIO").Value = False
                Exit Sub
            End If
            
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.ImporteSolicitudesVinculadas Then
                sdbgCampos.Columns("OBLIGATORIO").Value = False
                Exit Sub
            End If
            
            If Not sdbgCampos.Columns("OBLIGATORIO").Value Then
                'Los campos de certificado "Certificado" y "Fecha de expiraci�n" tienen que tener obligatorio siempre checkeado:
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Or _
                   sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgCampos.Columns("OBLIGATORIO").Value = True
                        Exit Sub
                End If
            Else
                If Not sdbgCampos.Columns("VISIBLE").Value Or Not sdbgCampos.Columns("ESCRITURA").Value Then
                    sdbgCampos.Columns("OBLIGATORIO").Value = False
                    Exit Sub
                End If
            End If
    End Select
    
    sdbgCampos.Update
End Sub

Private Sub sdbgCampos_InitColumnProps()
    sdbgCampos.Columns("DATO").caption = msDato
    sdbgCampos.Columns("VISIBLE").caption = msVisible
    sdbgCampos.Columns("ESCRITURA").caption = msEscritura
    sdbgCampos.Columns("OBLIGATORIO").caption = msObligatorio
End Sub

Private Sub sdbgCampos_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgCampos.DataChanged Then
                sdbgCampos.Update
                If m_bErrorCumplimentaciones Then
                    Exit Sub
                End If
            End If
    End Select
End Sub

Private Sub sdbgCampos_LostFocus()
    If sdbgCampos.DataChanged Then
        sdbgCampos.Update
    End If
End Sub

Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
        sdbgCampos.Columns("DATO").Style = ssStyleEditButton
    Else
        sdbgCampos.Columns("DATO").Style = ssStyleEdit
    End If
End Sub

Private Sub sdbgCampos_RowLoaded(ByVal Bookmark As Variant)
    If sdbgCampos.Columns("VISIBLE").CellValue(Bookmark) = False Then
        sdbgCampos.Columns("DATO").CellStyleSet "Gris"
    ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
        sdbgCampos.Columns("DATO").CellStyleSet "Amarillo"
    ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
        sdbgCampos.Columns("DATO").CellStyleSet "Calculado"
    Else
        sdbgCampos.Columns("DATO").CellStyleSet ""
    End If
End Sub

Private Sub SSTabGrupos_Click()
    If sdbgCampos.DataChanged Then
        sdbgCampos.Update
        If m_bErrorCumplimentaciones Then
            Exit Sub
        End If
    End If
    CargarCumplimentacionRol
End Sub

