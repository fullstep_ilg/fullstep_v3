VERSION 5.00
Begin VB.Form frmCATDatExtImagenes1 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cargar imagenes"
   ClientHeight    =   4560
   ClientLeft      =   2925
   ClientTop       =   4215
   ClientWidth     =   7275
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATDatExtImagenes1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4560
   ScaleWidth      =   7275
   Begin VB.CommandButton cmdDrive 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6570
      Picture         =   "frmCATDatExtImagenes1.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   450
      Width           =   315
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   ">>"
      Default         =   -1  'True
      Height          =   315
      Left            =   1980
      TabIndex        =   2
      Top             =   4140
      Width           =   1155
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3300
      TabIndex        =   3
      Top             =   4140
      Width           =   1155
   End
   Begin VB.DirListBox lstDirIma 
      Height          =   3240
      Left            =   360
      TabIndex        =   1
      Top             =   810
      Width           =   6555
   End
   Begin VB.TextBox txtRuta 
      Height          =   315
      Left            =   360
      TabIndex        =   0
      Top             =   420
      Width           =   6105
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Seleccione la carpeta donde se encuentran las imagenes"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   120
      Width           =   5775
   End
End
Attribute VB_Name = "frmCATDatExtImagenes1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_oProve As CProveedor
Public g_bImagenes As Boolean
Private bRespetarControl As Boolean
Private bChangeTxt As Boolean

Private Sub cmdAceptar_Click()
Dim oFos As FileSystemObject
Dim oFolder As Folder
Dim oFiles As Files

    If Trim(txtRuta.Text) = "" Then
        MsgBox Label1.caption, vbExclamation, "FULLSTEP GS"
        Exit Sub
    End If
    
    On Error GoTo Error
    
    Set oFos = New FileSystemObject
    Set oFolder = oFos.GetFolder(txtRuta.Text)
    Set oFiles = oFolder.Files
    If oFiles.Count = 0 Then
        oMensajes.NoExistenArchivos
        Exit Sub
    End If
    
    frmCATDatExtImagenes3.g_bImagenes = g_bImagenes
    Set frmCATDatExtImagenes3.g_oProve = g_oProve
    frmCATDatExtImagenes3.sPath = txtRuta.Text
    frmCATDatExtImagenes3.Show 1
    Unload Me

fin:
    Set oFiles = Nothing
    Set oFolder = Nothing
    Set oFos = Nothing
    Exit Sub
Error:
    If err.Number = 76 Then
        oMensajes.CarpetaNoExiste
    Else
        MsgBox err.Description, vbCritical, "FULLSTEP"
    End If
    If Me.Visible Then txtRuta.SetFocus
    Resume fin
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub Form_Load()

Me.Height = 4965
Me.Width = 7395
Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

CargarRecursos

txtRuta.Text = lstDirIma.Path

End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_Imagenes1, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.caption = ador(0).Value
        ador.MoveNext
        Label1.caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.caption = ador(0).Value
        ador.Close
            
    End If
    
    Set ador = Nothing
End Sub

'Private Sub Form_Resize()
'
'If Me.Width < 1000 Then Exit Sub
'If Me.Height < 1000 Then Exit Sub
'txtRuta.Width = Me.Width - 810
'cmdDrive.Left = txtRuta.Width + txtRuta.Left + 90
'lstDirIma.Width = Me.Width - 840
'lstDirIma.Height = Me.Height / 2 + 307
'cmdAceptar.Top = lstDirIma.Height + lstDirIma.Top + 90
'If Me.Width > 2500 Then
'    cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 90
'Else
'    cmdAceptar.Left = 10
'End If
'cmdCancelar.Left = Me.Width / 2 + 90
'cmdCancelar.Top = lstDirIma.Height + lstDirIma.Top + 90
'End Sub

Private Sub Form_Unload(Cancel As Integer)
Set g_oProve = Nothing
End Sub

Private Sub cmdDrive_Click()
Dim sBuffer As String
Dim sTit As String
Dim sInicio As String

On Error GoTo Error

sTit = Label1.caption & ":"
sInicio = txtRuta.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, sTit, sInicio)



If sBuffer <> "" Then
    bRespetarControl = True
    lstDirIma.Path = sBuffer
    txtRuta.Text = sBuffer
    bChangeTxt = False
    bRespetarControl = False
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    
    txtRuta.Text = ""
    bChangeTxt = False
    bRespetarControl = False
    
End Sub

Private Sub lstDirIma_Change()
    If Not bRespetarControl Then
        bRespetarControl = True
        txtRuta.Text = lstDirIma.Path
        bRespetarControl = False
    End If
    
End Sub

Private Sub txtRuta_Change()
    
    If Not bRespetarControl Then
        bChangeTxt = True
    End If
End Sub

Private Sub txtRuta_Validate(Cancel As Boolean)

On Error GoTo Error

    If txtRuta.Text <> lstDirIma.Path Then
        bRespetarControl = True
        lstDirIma.Path = txtRuta.Text
        bChangeTxt = False
        bRespetarControl = False
    End If
    Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    
    txtRuta.Text = ""
    bChangeTxt = False
    bRespetarControl = False
    
End Sub

