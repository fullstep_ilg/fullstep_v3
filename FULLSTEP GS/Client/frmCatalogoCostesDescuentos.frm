VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCatalogoCostesDescuentos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DCostes - Descuentos"
   ClientHeight    =   6105
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10980
   Icon            =   "frmCatalogoCostesDescuentos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6105
   ScaleWidth      =   10980
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab tabCostesDescuentosLineaCatalogo 
      Height          =   5865
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   10785
      _ExtentX        =   19024
      _ExtentY        =   10345
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DCostes"
      TabPicture(0)   =   "frmCatalogoCostesDescuentos.frx":0562
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frmCamposPedidoLinea"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "DDescuentos"
      TabPicture(1)   =   "frmCatalogoCostesDescuentos.frx":057E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picNavigateAdj"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "frmCamposPedidoCabecera"
      Tab(1).ControlCount=   2
      Begin VB.PictureBox picNavigateAdj 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   -74880
         ScaleHeight     =   420
         ScaleWidth      =   11250
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   6945
         Width           =   11250
      End
      Begin VB.Frame frmCamposPedidoCabecera 
         Height          =   5175
         Left            =   -74880
         TabIndex        =   4
         Top             =   480
         Width           =   10575
         Begin SSDataWidgets_B.SSDBDropDown sdbddTipoDescuento 
            Height          =   360
            Left            =   6360
            TabIndex        =   12
            Top             =   4560
            Width           =   2775
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCostesDescuentos.frx":059A
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "VALOR"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "COD"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   4895
            _ExtentY        =   635
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddOperacionDescuento 
            Height          =   360
            Left            =   3960
            TabIndex        =   13
            Top             =   4440
            Width           =   3015
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCostesDescuentos.frx":05B6
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "VALOR"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5318
            _ExtentY        =   635
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValorDescuento 
            Height          =   360
            Left            =   3960
            TabIndex        =   14
            Top             =   4800
            Width           =   3015
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCostesDescuentos.frx":05D2
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "VALOR"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5318
            _ExtentY        =   635
            _StockProps     =   77
         End
         Begin VB.CommandButton cmdA�adirDescuento 
            Caption         =   "dA�adir"
            Height          =   375
            Left            =   120
            TabIndex        =   19
            Top             =   4680
            Width           =   1215
         End
         Begin VB.CommandButton cmdEliminarDescuento 
            Caption         =   "dEliminar"
            Height          =   375
            Left            =   1440
            TabIndex        =   18
            Top             =   4680
            Width           =   1215
         End
         Begin VB.CommandButton cmdDeshacerDescuento 
            Caption         =   "dDeshacer"
            Height          =   375
            Left            =   2760
            TabIndex        =   17
            Top             =   4680
            Width           =   1215
         End
         Begin VB.CommandButton cmdEdicionDescuento 
            Caption         =   "dEdicion"
            Height          =   375
            Left            =   9240
            TabIndex        =   16
            Top             =   4680
            Width           =   1215
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgDescuentosLineaCatalogo 
            Height          =   4290
            Left            =   120
            TabIndex        =   15
            Top             =   240
            Width           =   10350
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   15
            stylesets.count =   4
            stylesets(0).Name=   "ConDescr"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCostesDescuentos.frx":05EE
            stylesets(1).Name=   "Gris"
            stylesets(1).BackColor=   -2147483633
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCatalogoCostesDescuentos.frx":060A
            stylesets(2).Name=   "Normal"
            stylesets(2).ForeColor=   0
            stylesets(2).BackColor=   16777215
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmCatalogoCostesDescuentos.frx":0626
            stylesets(3).Name=   "Selected"
            stylesets(3).ForeColor=   16777215
            stylesets(3).BackColor=   8388608
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmCatalogoCostesDescuentos.frx":0642
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   -2147483630
            ForeColorOdd    =   -2147483630
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            Columns.Count   =   15
            Columns(0).Width=   1773
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16776960
            Columns(1).Width=   4551
            Columns(1).Caption=   "NOMBRE"
            Columns(1).Name =   "NOMBRE"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16776960
            Columns(2).Width=   1217
            Columns(2).Caption=   "DESCR"
            Columns(2).Name =   "DESCR_BTN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   4
            Columns(2).ButtonsAlways=   -1  'True
            Columns(3).Width=   3200
            Columns(3).Caption=   "OPERACION"
            Columns(3).Name =   "OPERACION"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Caption=   "VALOR"
            Columns(4).Name =   "VALOR"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Caption=   "TIPO"
            Columns(5).Name =   "TIPO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Caption=   "GRUPO"
            Columns(6).Name =   "GRUPO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "DESCR"
            Columns(7).Name =   "DESCR"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "INTRO"
            Columns(8).Name =   "INTRO"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "MIN"
            Columns(9).Name =   "MIN"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "MAX"
            Columns(10).Name=   "MAX"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "ATRIB_ID"
            Columns(11).Name=   "ATRIB_ID"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "ID"
            Columns(12).Name=   "ID"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "TIPO_HIDDEN"
            Columns(13).Name=   "TIPO_HIDDEN"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Visible=   0   'False
            Columns(14).Caption=   "OPERACION_HIDDEN"
            Columns(14).Name=   "OPERACION_HIDDEN"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            _ExtentX        =   18256
            _ExtentY        =   7567
            _StockProps     =   79
            BackColor       =   -2147483643
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame frmCamposPedidoLinea 
         Height          =   5295
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   10575
         Begin SSDataWidgets_B.SSDBDropDown sdbddTipoCoste 
            Height          =   360
            Left            =   6360
            TabIndex        =   7
            Top             =   1800
            Width           =   2775
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCostesDescuentos.frx":065E
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "VALOR"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "COD"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   4895
            _ExtentY        =   635
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddOperacionCoste 
            Height          =   360
            Left            =   3240
            TabIndex        =   6
            Top             =   1680
            Width           =   3015
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCostesDescuentos.frx":067A
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "VALOR"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5318
            _ExtentY        =   635
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValorCoste 
            Height          =   360
            Left            =   120
            TabIndex        =   3
            Top             =   1560
            Width           =   3015
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCostesDescuentos.frx":0696
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "VALOR"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5318
            _ExtentY        =   635
            _StockProps     =   77
         End
         Begin VB.CommandButton cmdEdicionCoste 
            Caption         =   "dEdicion"
            Height          =   375
            Left            =   9240
            TabIndex        =   11
            Top             =   4800
            Width           =   1215
         End
         Begin VB.CommandButton cmdDeshacerCoste 
            Caption         =   "dDeshacer"
            Height          =   375
            Left            =   2760
            TabIndex        =   10
            Top             =   4800
            Width           =   1215
         End
         Begin VB.CommandButton cmdEliminarCoste 
            Caption         =   "dEliminar"
            Height          =   375
            Left            =   1440
            TabIndex        =   9
            Top             =   4800
            Width           =   1215
         End
         Begin VB.CommandButton cmdA�adirCoste 
            Caption         =   "dA�adir"
            Height          =   375
            Left            =   120
            TabIndex        =   8
            Top             =   4800
            Width           =   1215
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgCostesLineaCatalogo 
            Height          =   4170
            Left            =   120
            TabIndex        =   2
            Top             =   480
            Width           =   10335
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   17
            stylesets.count =   5
            stylesets(0).Name=   "ConDescr"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCatalogoCostesDescuentos.frx":06B2
            stylesets(1).Name=   "Gris"
            stylesets(1).BackColor=   -2147483633
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCatalogoCostesDescuentos.frx":0D34
            stylesets(2).Name=   "Normal"
            stylesets(2).ForeColor=   0
            stylesets(2).BackColor=   16777215
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmCatalogoCostesDescuentos.frx":0D50
            stylesets(3).Name=   "HayUniPed"
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmCatalogoCostesDescuentos.frx":0D6C
            stylesets(3).AlignmentPicture=   0
            stylesets(4).Name=   "Selected"
            stylesets(4).ForeColor=   16777215
            stylesets(4).BackColor=   8388608
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmCatalogoCostesDescuentos.frx":0DD0
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   -2147483630
            ForeColorOdd    =   -2147483630
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            Columns.Count   =   17
            Columns(0).Width=   1773
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16776960
            Columns(1).Width=   4551
            Columns(1).Caption=   "NOMBRE"
            Columns(1).Name =   "NOMBRE"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16776960
            Columns(2).Width=   1217
            Columns(2).Caption=   "DESCR"
            Columns(2).Name =   "DESCR_BTN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   4
            Columns(2).ButtonsAlways=   -1  'True
            Columns(3).Width=   3200
            Columns(3).Caption=   "OPERACION"
            Columns(3).Name =   "OPERACION"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Caption=   "VALOR"
            Columns(4).Name =   "VALOR"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Caption=   "TIPO"
            Columns(5).Name =   "TIPO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Caption=   "GRUPO"
            Columns(6).Name =   "GRUPO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "DESCR"
            Columns(7).Name =   "DESCR"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "INTRO"
            Columns(8).Name =   "INTRO"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "MIN"
            Columns(9).Name =   "MIN"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "MAX"
            Columns(10).Name=   "MAX"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "ATRIB_ID"
            Columns(11).Name=   "ATRIB_ID"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "ID"
            Columns(12).Name=   "ID"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "TIPO_HIDDEN"
            Columns(13).Name=   "TIPO_HIDDEN"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Visible=   0   'False
            Columns(14).Caption=   "IMPUESTOS"
            Columns(14).Name=   "IMPUESTOS"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   11
            Columns(14).FieldLen=   256
            Columns(15).Width=   3200
            Columns(15).Caption=   "IMPUESTOS_BTN"
            Columns(15).Name=   "IMPUESTOS_BTN"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(15).Style=   4
            Columns(15).ButtonsAlways=   -1  'True
            Columns(16).Width=   3200
            Columns(16).Visible=   0   'False
            Columns(16).Caption=   "OPERACION_HIDDEN"
            Columns(16).Name=   "OPERACION_HIDDEN"
            Columns(16).DataField=   "Column 16"
            Columns(16).DataType=   8
            Columns(16).FieldLen=   256
            _ExtentX        =   18230
            _ExtentY        =   7355
            _StockProps     =   79
            BackColor       =   -2147483643
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
End
Attribute VB_Name = "frmCatalogoCostesDescuentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables publicas
Public g_oLinea         As CLineaCatalogo
Public g_oCostesA�adir As CAtributos 'Costes que se a�adiran a la grid desde el maestro de atributos
Public g_oDescuentosA�adir As CAtributos 'Descuentos que se a�adiran a la grid desde el maestro de atributos
Public g_ofrmATRIBMod As frmAtribMod

'Variables privadas
Private m_bEdicionCostes As Boolean 'Variable que indica si se permite modificar y a�adir costes
Private m_bEdicionDescuentos As Boolean 'Variable que indica si se permite modificar y a�adir descuentos
Private m_oCostes As CAtributos
Private m_oDescuentos As CAtributos
Private m_sFijo As String
Private m_sOpcionalSimple As String
Private m_sOpcionalExcluyente As String
Private m_sObligatorioExcluyente As String
Private m_sNuevo As String
Private m_sDesdeProceso As String
Private m_sEdicionCaption As String
Private m_sConsultaCaption As String
Private m_sNoHayCosteSeleccionado As String
Private m_sNoHayDescuentoSeleccionado As String
Private m_sIntroduceValor As String
Private m_sIntroduceOperacionValida As String
Private m_sIntroduceGrupo As String
Private m_sDeseaEliminarCoste As String
Private m_sDeseaEliminarDescuento As String
Private m_oCosteEnEdicion As CAtributo
Private m_oDescuentoEnEdicion As CAtributo
Private m_bErrorUpdate As Boolean
Private m_iRowActual As Long
Private m_bA�adiendo As Boolean 'Variable que nos indicara cuando estamos a�adiendo un coste o descuento

Private WithEvents cPAnyadirCostes As cPopupMenu
Attribute cPAnyadirCostes.VB_VarHelpID = -1
Private WithEvents cPAnyadirDescuentos As cPopupMenu
Attribute cPAnyadirDescuentos.VB_VarHelpID = -1

'Constantes
Private Const m_sSumar As String = "+"
Private Const m_sSumarPorcentaje As String = "+%"
Private Const m_sRestar As String = "-"
Private Const m_sRestarPorcentaje As String = "-%"

Private m_bNoUpdatable As Boolean
Private m_bMensajeRecienSelecc As Boolean


Private Sub Form_Load()
    PonerFieldSeparator Me
    
    CargarRecursos
    
    'Configura como Dropdowns las columnas de operacion y tipo que siempre seran combos
    ConfigurarCombosGridCostes
    'Configura como Dropdowns las columnas de operacion y tipo que siempre seran combos
    ConfigurarCombosGridDescuentos
    'Cargamos la grid de costes
    CargarCostesLineaCatalogo
    'Cargamos la grid de descuentos
    CargarDescuentosLineaCatalogo
    'Configuramos la edicion del grid de costes
    ConfigurarEdicionGridCostes (True)
    'Configuramos la edicion del grid de descuentos
    ConfigurarEdicionGridDescuentos (True)
    'Cargamos las opciones del submenu del boton de a�adir costes
    cargarcPAnyadirCostes
    'Cargamos las opciones del submenu del boton de a�adir descuentos
    cargarcPAnyadirDescuentos
End Sub

''' <summary>Carga Textos de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next

    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_COSTESDESCUENTOS, basPublic.gParametrosInstalacion.gIdioma)

    If Not ador Is Nothing Then
        m_sFijo = ador(0).Value '1 Fijo
        ador.MoveNext
        m_sOpcionalSimple = ador(0).Value '2 OpcionalSimple
        ador.MoveNext
        m_sOpcionalExcluyente = ador(0).Value '3 OpcionalExcluyente
        ador.MoveNext
        m_sObligatorioExcluyente = ador(0).Value '4 Obligatorio Excluyente
        ador.MoveNext
        cmdA�adirCoste.caption = ador(0).Value '5 a�adir
        cmdA�adirDescuento.caption = ador(0).Value
        ador.MoveNext
        Me.cmdEliminarCoste.caption = ador(0).Value  '6 Eliminar
        Me.cmdEliminarDescuento.caption = ador(0).Value
        ador.MoveNext
        Me.cmdDeshacerCoste.caption = ador(0).Value  '7 Deshacer
        Me.cmdDeshacerDescuento.caption = ador(0).Value
        ador.MoveNext
        m_sEdicionCaption = ador(0).Value '8 Edicion
        ador.MoveNext
        m_sConsultaCaption = ador(0).Value '9 Consulta
        ador.MoveNext
        m_sNuevo = ador(0).Value '10 Nuevo
        ador.MoveNext
        m_sDesdeProceso = ador(0).Value '11 Desde proceso
        ador.MoveNext
        tabCostesDescuentosLineaCatalogo.TabCaption(0) = ador(0).Value '12 Costes
        Me.caption = ador(0).Value
        ador.MoveNext
        tabCostesDescuentosLineaCatalogo.TabCaption(1) = ador(0).Value '13 Descuentos
        Me.caption = Me.caption & " - " & ador(0).Value
        ador.MoveNext
        Me.sdbgCostesLineaCatalogo.Columns("COD").caption = ador(0).Value '14 Codigo
        Me.sdbgDescuentosLineaCatalogo.Columns("COD").caption = ador(0).Value '14 Codigo
        ador.MoveNext
        Me.sdbgCostesLineaCatalogo.Columns("NOMBRE").caption = ador(0).Value '15 Nombre
        Me.sdbgDescuentosLineaCatalogo.Columns("NOMBRE").caption = ador(0).Value '15 Nombre
        ador.MoveNext
        Me.sdbgCostesLineaCatalogo.Columns("DESCR_BTN").caption = ador(0).Value '16 Descr
        Me.sdbgDescuentosLineaCatalogo.Columns("DESCR_BTN").caption = ador(0).Value '16 DDescr
        ador.MoveNext
        Me.sdbgCostesLineaCatalogo.Columns("OPERACION").caption = ador(0).Value '17 Operacion
        Me.sdbgDescuentosLineaCatalogo.Columns("OPERACION").caption = ador(0).Value '17 Operacion
        ador.MoveNext
        Me.sdbgCostesLineaCatalogo.Columns("VALOR").caption = ador(0).Value '18 Valor
        Me.sdbgDescuentosLineaCatalogo.Columns("VALOR").caption = ador(0).Value '18 Valor
        ador.MoveNext
        Me.sdbgCostesLineaCatalogo.Columns("TIPO").caption = ador(0).Value '19 Tipo
        Me.sdbgDescuentosLineaCatalogo.Columns("TIPO").caption = ador(0).Value '19 Tipo
        ador.MoveNext
        Me.sdbgCostesLineaCatalogo.Columns("GRUPO").caption = ador(0).Value '20 Grupo
        Me.sdbgDescuentosLineaCatalogo.Columns("GRUPO").caption = ador(0).Value '20 Grupo
        ador.MoveNext
        m_sNoHayCosteSeleccionado = ador(0).Value '21
        ador.MoveNext
        m_sNoHayDescuentoSeleccionado = ador(0).Value '22
        ador.MoveNext
        m_sIntroduceValor = ador(0).Value '23
        ador.MoveNext
        m_sIntroduceOperacionValida = ador(0).Value '24
        ador.MoveNext
        m_sIntroduceGrupo = ador(0).Value '25 Debe introducir un grupo
        ador.MoveNext
        m_sDeseaEliminarCoste = ador(0).Value '26 �Desea eliminar este coste?
        ador.MoveNext
        m_sDeseaEliminarDescuento = ador(0).Value '27 �Desea eliminar este descuento?
        ador.MoveNext
        Me.sdbgCostesLineaCatalogo.Columns("IMPUESTOS_BTN").caption = ador(0).Value '28 Impuestos
        ador.Close
    End If
End Sub
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'        COSTES
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

''' <summary>
''' Carga del submen� que se muestra al pulsar el boton de a�adir costes
''' </summary>
''' <remarks>Llamada desde: frmCatalogoAtribEspyAdj; Tiempo m�ximo:0,1</remarks>
Private Sub cargarcPAnyadirCostes()

    Set cPAnyadirCostes = New cPopupMenu
    cPAnyadirCostes.hWndOwner = Me.hWnd
    
    cPAnyadirCostes.clear
    Dim indice As Integer
    indice = 1
    cPAnyadirCostes.AddItem m_sNuevo, , indice, , , , , "MAESTROATR"
    indice = indice + 1
    cPAnyadirCostes.AddItem m_sDesdeProceso, , indice, , , , , "ARTOPROVEOPROCE"
End Sub

''' <summary>Configura el estado de edicion del grid, bloqueando o no las columnas, y habilita o no los botones de edicion</summary>
Private Sub ConfigurarEdicionGridCostes(ByVal Locked As Boolean)
    sdbgCostesLineaCatalogo.Columns("VALOR").Locked = Locked
    sdbgCostesLineaCatalogo.Columns("OPERACION").Locked = Locked
    sdbgCostesLineaCatalogo.Columns("TIPO").Locked = Locked
    sdbgCostesLineaCatalogo.Columns("GRUPO").Locked = Locked
    
    Me.cmdA�adirCoste.Enabled = Not Locked
    Me.cmdDeshacerCoste.Enabled = Not Locked
    Me.cmdEliminarCoste.Enabled = Not Locked
    Me.cmdDeshacerCoste.Enabled = False
    Me.cmdEdicionCoste.caption = IIf(Locked, m_sEdicionCaption, m_sConsultaCaption)
    m_bEdicionCostes = Not Locked
End Sub
''' <summary>Configura como Dropdowns las columnas de operacion y tipo que siempre seran combos</summary>
Private Sub ConfigurarCombosGridCostes()
    'Le asigno los dropdowns a las columnas operacion y tipo
    sdbddOperacionCoste.RemoveAll
    sdbddOperacionCoste.AddItem ""
    sdbddTipoCoste.RemoveAll
    sdbddTipoCoste.AddItem ""
    sdbgCostesLineaCatalogo.Columns("OPERACION").DropDownHwnd = sdbddOperacionCoste.hWnd
    sdbgCostesLineaCatalogo.Columns("TIPO").DropDownHwnd = sdbddTipoCoste.hWnd
    sdbddTipoCoste.Enabled = True
    sdbddOperacionCoste.Enabled = True
End Sub

''' <summary>Muestra el menu popup con las distintas opciones que hay para a�adir costes de linea de catalogo</summary>
Private Sub cmdA�adirCoste_Click()
    m_bMensajeRecienSelecc = True
    MostrarcPAnyadirCostes cmdA�adirCoste.Left, cmdA�adirCoste.Top + cmdA�adirCoste.Height
    m_bMensajeRecienSelecc = False
End Sub

''' <summary>
''' Lanza las opciones del menu contextual de a�adir costes
''' </summary>
''' <param name="ItemNumber">Indice del men�</param>
''' <remarks>Llamada desde: Click de cmdOtrasAcciones; Tiempo m�ximo:0,1</remarks>
Private Sub cPAnyadirCostes_Click(ItemNumber As Long)
    Dim tsError As TipoErrorSummit
    Dim oCoste As CAtributo
    Dim oAtr As CAtributo
    Dim i As Integer
    
    m_bA�adiendo = True 'Indicamos que se va a a�adir un coste, esto nos sera util para evitar el evento Row_colchange al a�adirlo al grid, si lo hicieramos
    Select Case cPAnyadirCostes.itemKey(ItemNumber)
        'A�adimos un coste desde el maestro de atributos
        Case "MAESTROATR"
         
            If g_oLinea Is Nothing Then Unload Me
            
            Set g_oCostesA�adir = oFSGSRaiz.Generar_CAtributos
            
            'A�ade un campo de tipo atributo de GS
            If Not g_ofrmATRIBMod Is Nothing Then
                Unload g_ofrmATRIBMod
                Set g_ofrmATRIBMod = Nothing
            End If
        
            Set g_ofrmATRIBMod = New frmAtribMod
            g_ofrmATRIBMod.g_sOrigen = "frmCatalogoCostesDescuentos_Costes"
            
            g_ofrmATRIBMod.Show vbModal
            
            'A�adir atributos
            If Not g_oCostesA�adir Is Nothing Then
                If g_oCostesA�adir.Count > 0 Then
                    AgregarAtributos g_oCostesA�adir, sdbgCostesLineaCatalogo
                Else
                    m_bA�adiendo = False
                End If
            End If
                        
        Case "ARTOPROVEOPROCE"
            Set g_oCostesA�adir = Nothing
            Set g_oCostesA�adir = oFSGSRaiz.Generar_CAtributos
            Set frmCostesDescuentosProce.g_oLinea = Me.g_oLinea
            frmCostesDescuentosProce.g_sOrigen = "frmCatalogoCostesDescuentos_Costes"
            frmCostesDescuentosProce.Show vbModal
            
            'GUARDAMOS LOS CAMPOS A�ADIDOS EN BDD
            
            If Not g_oCostesA�adir Is Nothing Then
                For Each oCoste In g_oCostesA�adir
                    tsError = oCoste.AnyadirCosteDescuentoLineaCatalogo(g_oLinea.Id, Coste)
                    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
                    If tsError.NumError <> TESnoerror Then
                        TratarError tsError
                    End If
                Next
            Else
                m_bA�adiendo = False 'No se ha a�adido ningun coste
            End If

            'RECARGAMOS LAS L�NEAS
            Set m_oCostes = Nothing
            Set m_oCostes = oFSGSRaiz.Generar_CAtributos
            Set m_oCostes = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , Coste, Linea)
            'Ponemos el ID a cada l�nea del grid
            With sdbgCostesLineaCatalogo
                .MoveFirst
                If .Rows > 0 Then
                    For Each oAtr In m_oCostes
                        For i = 0 To .Rows - 1
                            If (.Columns("ID").Value = CStr(0) Or .Columns("ID").Value = "") And .Columns("COD").Value = oAtr.Cod And .Columns("ATRIB_ID").Value = CStr(oAtr.Atrib) Then
                                .Columns("ID").Value = oAtr.Id
                            End If
                            .MoveNext
                        Next
                        .MoveFirst
                    Next
                End If
            End With
    End Select
End Sub


''' <summary>Elimina el coste seleccionado de la grid y de BD</summary>
Private Sub cmdEliminarCoste_Click()
    Screen.MousePointer = vbHourglass
    Dim inum As Integer
    Dim oCostes As CAtributos
    Dim oCoste As CAtributo
    Dim tsError As TipoErrorSummit
    Dim i As Integer
    
    'No sirve de nada q diga "Tipo incorrecto" en una l�nea q voy a borrar
    m_bNoUpdatable = True
    
    sdbgCostesLineaCatalogo.Columns("VALOR").DropDownHwnd = 0
    With sdbgCostesLineaCatalogo
        If .SelBookmarks.Count > 0 Then
            i = oMensajes.PreguntaEliminar(m_sDeseaEliminarCoste)
            If i = vbYes Then
                inum = 0
                Set oCostes = oFSGSRaiz.Generar_CAtributos
                While inum < .SelBookmarks.Count
                    .Bookmark = .SelBookmarks(inum)
                    oCostes.Add .Columns("ID").Value, .Columns("COD").Text, .Columns("NOMBRE").Text, TipoNumerico
                    inum = inum + 1
                Wend
                '3. ELIMINAR DE BDD
                
                For Each oCoste In oCostes
                    If Not (NullToStr(oCoste.Id) = "") Then
                        tsError = oCoste.EliminarCosteDescuentoLineaCatalogo
                        If tsError.NumError <> TESnoerror Then
                            Screen.MousePointer = vbNormal
                            TratarError tsError
                            Exit Sub
                        End If
                    End If
                Next
            
                '2.ELIMINAR DEL GRID
                .DeleteSelected
                .Update
                '.Refresh
                '4. RECARGAR LA VARIABLE DE LOS COSTES
                Set m_oCostes = Nothing
                Set m_oCostes = oFSGSRaiz.Generar_CAtributos
                Set m_oCostes = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , Coste, Linea)
            Else
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox m_sNoHayCosteSeleccionado, vbExclamation
            Exit Sub
        End If
    End With
    sdbgCostesLineaCatalogo.MoveLast
    Screen.MousePointer = vbNormal
    
    m_bNoUpdatable = False
End Sub

''' <summary>Deshace los cambios de una fila del grid de costes</summary>
Private Sub cmdDeshacerCoste_Click()
    sdbgCostesLineaCatalogo.CancelUpdate
    sdbgCostesLineaCatalogo.DataChanged = False
    
    'Elimino el objeto en edicion
    If Not m_oCosteEnEdicion Is Nothing Then
        Set m_oCosteEnEdicion = Nothing
    End If
    
    If sdbgCostesLineaCatalogo.Rows = 1 Then
        sdbgCostesLineaCatalogo.MoveFirst
        sdbgCostesLineaCatalogo.Refresh
        sdbgCostesLineaCatalogo.Bookmark = sdbgCostesLineaCatalogo.SelBookmarks(0)
        sdbgCostesLineaCatalogo.SelBookmarks.RemoveAll
    End If
    
    cmdDeshacerCoste.Enabled = False
End Sub

''' <summary>
''' muestra el submen� de a�adir costes de linea de catalogo
''' </summary>
''' <param name="X">Coordenada horizontal</param>
''' <param name="Y">Coordenada vertical</param>
''' <remarks>Llamada desde: frmCatalogoAtribEspYAdj; Tiempo m�ximo:0,1</remarks>
Private Sub MostrarcPAnyadirCostes(ByVal X As Single, ByVal Y As Single)
    cPAnyadirCostes.ShowPopupMenu X, Y
End Sub

''' <summary>Cambia de modo edicion a consulta el grid de costes y viceversa</summary>
Private Sub cmdEdicionCoste_Click()
    Dim v As Variant
    
    If m_bEdicionCostes Then
        'Se pasa a modo consulta
        If sdbgCostesLineaCatalogo.DataChanged = True Then
    
            v = sdbgCostesLineaCatalogo.ActiveCell.Value
            If Me.Visible Then sdbgCostesLineaCatalogo.SetFocus
            sdbgCostesLineaCatalogo.ActiveCell.Value = v
            m_bErrorUpdate = False
            
            If sdbgCostesLineaCatalogo.Row = 0 Then   'Es la primera fila o la �nica
            
                If sdbgCostesLineaCatalogo.Rows = 1 Then
                    sdbgCostesLineaCatalogo.Update
                    If m_bErrorUpdate Then
                        Exit Sub
                    End If
                Else
                    sdbgCostesLineaCatalogo.MoveNext
                    DoEvents
                    If m_bErrorUpdate Then
                        Exit Sub
                    Else
                        sdbgCostesLineaCatalogo.MovePrevious
                    End If
                End If
            Else                              'No es la primera fila
                sdbgCostesLineaCatalogo.MovePrevious
                DoEvents
                If m_bErrorUpdate Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                    Exit Sub
                Else
                    sdbgCostesLineaCatalogo.MoveNext
                End If
            End If
         
        End If
        'Si el grid esta en modo consulta, se pasara a modo consulta y se deshabilitaran botones de edicion
        ConfigurarEdicionGridCostes (True)
        m_bEdicionCostes = False
    Else
        'Si el grid esta en modo consulta, se pasara a modo edicion y se habilitaran botones de edici�n
        ConfigurarEdicionGridCostes (False)
        m_bEdicionCostes = True
    End If
End Sub

''' <summary>Carga el objeto de m_oCamposPedidoCategoria con los campos de pedido del ambito linea de la categoria de catalogo</summary>
Private Sub CargarCostesLineaCatalogo()
    If g_oLinea Is Nothing Then Unload Me
    
    Set m_oCostes = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , Coste)
    CargarGridCostesLineaCatalogo
End Sub

''' <summary>Carga en la grid los costes de la linea de catalogo</summary>
Private Sub CargarGridCostesLineaCatalogo()
    Dim oAtrib As CAtributo
    sdbgCostesLineaCatalogo.RemoveAll
    
    If Not m_oCostes Is Nothing Then
        For Each oAtrib In m_oCostes
            sdbgCostesLineaCatalogo.AddItem oAtrib.Cod & Chr(m_lSeparador) & oAtrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtrib.OperacionCosteDescuento & Chr(m_lSeparador) & oAtrib.valor & Chr(m_lSeparador) & oAtrib.TipoCosteDescuento & Chr(m_lSeparador) & oAtrib.GrupoCosteDescuento & Chr(m_lSeparador) & oAtrib.Descripcion & Chr(m_lSeparador) & oAtrib.TipoIntroduccion & Chr(m_lSeparador) & oAtrib.Minimo & Chr(m_lSeparador) & oAtrib.Maximo & Chr(m_lSeparador) & oAtrib.Atrib & Chr(m_lSeparador) & oAtrib.Id & Chr(m_lSeparador) & oAtrib.TipoCosteDescuento & Chr(m_lSeparador) & oAtrib.TieneImpuestos & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtrib.OperacionCosteDescuento
        Next
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Me.sdbgCostesLineaCatalogo.Rows > 0 Or Me.sdbgDescuentosLineaCatalogo.Rows > 0 Then
        frmCatalogo.sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS").Value = "1"
    End If
    
    sdbgCostesLineaCatalogo.Update
    sdbgDescuentosLineaCatalogo.Update
    
    Set g_oCostesA�adir = Nothing
    Set g_oDescuentosA�adir = Nothing
End Sub

'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DE DROPDOWNs de COSTES
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>evento que salta al elegir una opcion del dropdown</summary>
Private Sub sdbddTipoCoste_CloseUp()
    sdbgCostesLineaCatalogo.Columns("TIPO_HIDDEN").Value = sdbddTipoCoste.Columns("VALOR").Value
    
    'Si el tipo de coste es fijo o opcional simple, la columna de grupo se le pondra estilo gris, quedara deshabilitada
    'y si tendria valor se eliminaria, sino se cambiara el estilo a normal
    Select Case sdbddTipoCoste.Columns("VALOR").Value
        Case TipoCostesDescuentosLineaCatalogo.Fijos
            sdbgCostesLineaCatalogo.Columns("GRUPO").CellStyleSet "Gris"
            sdbgCostesLineaCatalogo.Columns("GRUPO").Text = ""
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
            sdbgCostesLineaCatalogo.Columns("GRUPO").CellStyleSet "Gris"
            sdbgCostesLineaCatalogo.Columns("GRUPO").Text = ""
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
            sdbgCostesLineaCatalogo.Columns("GRUPO").CellStyleSet "Normal"
        Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes
            sdbgCostesLineaCatalogo.Columns("GRUPO").CellStyleSet "Normal"
    End Select
    sdbgCostesLineaCatalogo.Refresh
End Sub

Private Sub sdbddOperacionCoste_CloseUp()
    'sdbgCostesLineaCatalogo.Columns("OPERACION").Value = sdbddOperacionCoste.Columns("VALOR").Value
    sdbgCostesLineaCatalogo.Columns("OPERACION_HIDDEN").Value = sdbddOperacionCoste.Columns("VALOR").Value
End Sub

''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddTipoCoste_DropDown()
    
    If sdbgCostesLineaCatalogo.Rows = 0 Then Exit Sub
   
    If sdbgCostesLineaCatalogo.Columns("VALOR").Locked Then
        sdbddTipoCoste.DroppedDown = False
        Exit Sub
    End If
    sdbddTipoCoste.RemoveAll
    sdbddTipoCoste.DroppedDown = True
    
    'A�adimos los tipos de costes
    sdbddTipoCoste.AddItem TipoCostesDescuentosLineaCatalogo.Fijos & Chr(m_lSeparador) & m_sFijo
    sdbddTipoCoste.AddItem TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes & Chr(m_lSeparador) & m_sObligatorioExcluyente
    sdbddTipoCoste.AddItem TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes & Chr(m_lSeparador) & m_sOpcionalExcluyente
    sdbddTipoCoste.AddItem TipoCostesDescuentosLineaCatalogo.Opcionales_Simples & Chr(m_lSeparador) & m_sOpcionalSimple
            
    'Se le modifica el ancho del dropdown
    sdbddTipoCoste.Width = sdbgCostesLineaCatalogo.Columns("TIPO").Width
    sdbddTipoCoste.Columns("DESC").Width = sdbddTipoCoste.Width
End Sub
''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddOperacionCoste_DropDown()
    If sdbgCostesLineaCatalogo.Rows = 0 Then Exit Sub
   
    If sdbgCostesLineaCatalogo.Columns("VALOR").Locked Then
        sdbddOperacionCoste.DroppedDown = False
        Exit Sub
    End If
    sdbddOperacionCoste.RemoveAll
    sdbddOperacionCoste.DroppedDown = True
    
    'A�adimos las distintas operaciones
    sdbddOperacionCoste.AddItem m_sSumar & Chr(m_lSeparador) & m_sSumar
    sdbddOperacionCoste.AddItem m_sSumarPorcentaje & Chr(m_lSeparador) & m_sSumarPorcentaje
    
    'Se le modifica el ancho del dropdown
    sdbddOperacionCoste.Width = sdbgCostesLineaCatalogo.Columns("OPERACION").Width
    sdbddOperacionCoste.Columns("DESC").Width = sdbddOperacionCoste.Width
End Sub
''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddValorCoste_DropDown()
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1
    
    If sdbgCostesLineaCatalogo.Rows = 0 Then Exit Sub
   
    If sdbgCostesLineaCatalogo.Columns("VALOR").Locked Then
        sdbddValorCoste.DroppedDown = False
        Exit Sub
    End If
    sdbddValorCoste.RemoveAll
    sdbddValorCoste.DroppedDown = True
    
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    'Recogemos el atributo
    oAtributo.Id = sdbgCostesLineaCatalogo.Columns("ATRIB_ID").Value
    
    If oAtributo.Id > 0 Then
        If sdbgCostesLineaCatalogo.Columns("INTRO").Value = "1" Then
            'Cargamos la lista de valores del atributo
            oAtributo.CargarDatosAtributo
            oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
            For Each oLista In oAtributo.ListaPonderacion
                sdbddValorCoste.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        End If
    End If
    'Se le modifica el ancho del dropdown
    sdbddValorCoste.Width = sdbgCostesLineaCatalogo.Columns("VALOR").Width
    sdbddValorCoste.Columns("DESC").Width = sdbddValorCoste.Width
End Sub

''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddTipoCoste_InitColumnProps()
    sdbddTipoCoste.DataFieldList = "Column 0"
    sdbddTipoCoste.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddOperacionCoste_InitColumnProps()
    sdbddOperacionCoste.DataFieldList = "Column 0"
    sdbddOperacionCoste.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddValorCoste_InitColumnProps()
    sdbddValorCoste.DataFieldList = "Column 0"
    sdbddValorCoste.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddValorCoste_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValorCoste.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValorCoste.Rows - 1
            bm = sdbddValorCoste.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValorCoste.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgCostesLineaCatalogo.Columns("VALOR").Value = Mid(sdbddValorCoste.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValorCoste.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddOperacionCoste_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddOperacionCoste.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOperacionCoste.Rows - 1
            bm = sdbddOperacionCoste.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOperacionCoste.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddOperacionCoste.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddTipoCoste_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddTipoCoste.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddTipoCoste.Rows - 1
            bm = sdbddTipoCoste.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddTipoCoste.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddTipoCoste.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DEL GRID DE COSTES DE LINEA DE CATALOGO
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

''' <summary>evento del grid que se produce por cada linea que se carga</summary>
Private Sub sdbgCostesLineaCatalogo_RowLoaded(ByVal Bookmark As Variant)
    
    'Si el atributo tiene descripcion, se cambia el StyleSet para cambiarle el estilo al boton
    If sdbgCostesLineaCatalogo.Columns("DESCR").CellValue(Bookmark) <> "" Then
        sdbgCostesLineaCatalogo.Columns("DESCR_BTN").Text = "..."
    End If

    'Si el tipo de coste es fijo o opcional simple, la columna de grupo se le pondra estilo gris, quedara deshabilitada
    Select Case sdbgCostesLineaCatalogo.Columns("TIPO_HIDDEN").CellValue(Bookmark)
        Case TipoCostesDescuentosLineaCatalogo.Fijos
            sdbgCostesLineaCatalogo.Columns("GRUPO").CellStyleSet "Gris"
            sdbgCostesLineaCatalogo.Columns("TIPO").Text = m_sFijo
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
            sdbgCostesLineaCatalogo.Columns("GRUPO").CellStyleSet "Gris"
            sdbgCostesLineaCatalogo.Columns("TIPO").Text = m_sOpcionalSimple
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
            sdbgCostesLineaCatalogo.Columns("TIPO").Text = m_sOpcionalExcluyente
        Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes
            sdbgCostesLineaCatalogo.Columns("TIPO").Text = m_sObligatorioExcluyente
    End Select
    
    Select Case sdbgCostesLineaCatalogo.Columns("OPERACION_HIDDEN").CellValue(Bookmark)
        Case m_sSumar
            sdbgCostesLineaCatalogo.Columns("OPERACION").Text = m_sSumar
        Case m_sSumarPorcentaje
            sdbgCostesLineaCatalogo.Columns("OPERACION").Text = m_sSumarPorcentaje
        Case Else
             sdbgCostesLineaCatalogo.Columns("OPERACION").Text = m_sSumar
    End Select
    
    
    Select Case sdbgCostesLineaCatalogo.Columns("IMPUESTOS").CellValue(Bookmark)
        Case True
            sdbgCostesLineaCatalogo.Columns("IMPUESTOS_BTN").CellStyleSet "HayUniPed"
        Case False
            sdbgCostesLineaCatalogo.Columns("IMPUESTOS_BTN").CellStyleSet "Normal"
    End Select
    
    
End Sub
''' <summary>evento del grid que se produce el pulsar el las columnas que son boton</summary>
Private Sub sdbgCostesLineaCatalogo_BtnClick()
    
    Select Case Me.sdbgCostesLineaCatalogo.Columns(Me.sdbgCostesLineaCatalogo.Col).Name
        Case "DESCR_BTN"
            If m_bEdicionCostes Then
                frmATRIBDescr.g_bEdicion = True
            Else
                frmATRIBDescr.g_bEdicion = False
            End If
            frmATRIBDescr.g_sOrigen = "frmCatalogoCostesDescuentos_Costes"
            frmATRIBDescr.txtDescr.Text = sdbgCostesLineaCatalogo.Columns("DESCR").Value
            frmATRIBDescr.Show 1
        
        
            'acabo con la edicion del atributo cuando me devuelve modificaciones.
            If sdbgCostesLineaCatalogo.DataChanged Then
                If m_oCosteEnEdicion Is Nothing Then
                    Set m_oCosteEnEdicion = m_oCostes.Item(sdbgCostesLineaCatalogo.Columns("ID").Value)
                End If
            End If
        Case "IMPUESTOS_BTN"
            If sdbgCostesLineaCatalogo.DataChanged = True Then
                sdbgCostesLineaCatalogo.Update
            End If
            If sdbgCostesLineaCatalogo.Columns("ID").Value <> "" Then Set m_oCosteEnEdicion = m_oCostes.Item(sdbgCostesLineaCatalogo.Columns("ID").Value)
            frmImpuestos.g_sOrigen = "frmCatalogo_ImpuestosCostesLineas"
            If Me.sdbgCostesLineaCatalogo.Columns("ID").Value <> "" Then
                frmImpuestos.g_lAtribId = sdbgCostesLineaCatalogo.Columns("ID").Value
            Else
                frmImpuestos.g_lAtribId = 0
            End If
            frmImpuestos.PermiteModif = m_bEdicionCostes
            frmImpuestos.g_ConceptoLineaCatalogo = Null
            frmImpuestos.g_sCaptionFormulario = sdbgCostesLineaCatalogo.Columns("COD").Text & " - " & sdbgCostesLineaCatalogo.Columns("NOMBRE").Text
            frmImpuestos.Show vbModal
    End Select
    
End Sub

''' <summary>evento del grid que se produce cuando el usuario cambia de celda o de fila</summary>
Private Sub sdbgCostesLineaCatalogo_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    
    If sdbgCostesLineaCatalogo.Col > 0 Then
        If sdbgCostesLineaCatalogo.Columns(sdbgCostesLineaCatalogo.Col).Name = "VALOR" Then
            'Si el atributo es de tipo lista ponemos a la columna valor el dropdown
            If sdbgCostesLineaCatalogo.Columns("INTRO").Value = 1 Then
                'Lista
                sdbddValorCoste.RemoveAll
                sdbddValorCoste.AddItem ""
                sdbgCostesLineaCatalogo.Columns("VALOR").DropDownHwnd = sdbddValorCoste.hWnd
                sdbddValorCoste.Enabled = True
            Else
                'Introduccion libre
                sdbgCostesLineaCatalogo.Columns("VALOR").DropDownHwnd = 0
                sdbddValorCoste.Enabled = False
            End If
        End If
    End If
    
    'Si el tipo de coste es fijo o opcional simple, la columna de grupo quedara deshabilitada
    Select Case sdbgCostesLineaCatalogo.Columns("TIPO_HIDDEN").Value
        Case TipoCostesDescuentosLineaCatalogo.Fijos, TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
            sdbgCostesLineaCatalogo.Columns("GRUPO").Locked = True
        Case Else
            'Si es otro tipo de coste, si esta permitida la edicion, desbloquearemos la columna
            If m_bEdicionCostes Then
                sdbgCostesLineaCatalogo.Columns("GRUPO").Locked = False
            End If
    End Select
    'Si el atributo se acaba de a�adir no se hacen las comprobaciones, se haran a la hora de grabar en bd
    If m_bA�adiendo = False Then
         'Compruebo si se ha cambiado de fila
        If m_iRowActual <> sdbgCostesLineaCatalogo.Row Then
            'Si se ha cambiado de fila lanzo la actualizaci�n, ira al evento BeforeUpdate y AfterUpdate del grid
            sdbgCostesLineaCatalogo.Update
        End If
    End If
    m_iRowActual = sdbgCostesLineaCatalogo.Row
    m_bA�adiendo = False
End Sub

''' <summary>evento del grid que se produce cuando se lanza el update de la grid, haremos las comprobaciones de datos</summary>
Private Sub sdbgCostesLineaCatalogo_BeforeUpdate(Cancel As Integer)
    'No sirve de nada q diga "Tipo incorrecto" en una l�nea q voy a borrar
    If m_bNoUpdatable Then
        Exit Sub
    End If
    
    Cancel = False
    'Comprobamos que el valor introducido es numerico
    If sdbgCostesLineaCatalogo.Columns("VALOR").Text <> "" Then
        If (Not IsNumeric(sdbgCostesLineaCatalogo.Columns("VALOR").Text)) Then
            m_bErrorUpdate = True
            oMensajes.AtributoValorNoValido ("TIPO2")
            Cancel = True
            GoTo Salir
        Else
            If sdbgCostesLineaCatalogo.Columns("MIN").Text <> "" And sdbgCostesLineaCatalogo.Columns("MAX").Text <> "" Then
                If StrToDbl0(sdbgCostesLineaCatalogo.Columns("MIN").Text) > StrToDbl0(sdbgCostesLineaCatalogo.Columns("VALOR").Text) Or StrToDbl0(sdbgCostesLineaCatalogo.Columns("MAX").Text) < StrToDbl0(sdbgCostesLineaCatalogo.Columns("VALOR").Text) Then
                    m_bErrorUpdate = True
                    oMensajes.ValorEntreMaximoYMinimo sdbgCostesLineaCatalogo.Columns("MIN").Text, sdbgCostesLineaCatalogo.Columns("MAX").Text
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
        
        'Comprobaremos si es una lista, que se haya elegido un valor de la lista
        If sdbgCostesLineaCatalogo.Columns("INTRO").Text = "1" Then
            Dim oAtributo As CAtributo
            Dim bValorEncontrado As Boolean
            Dim oLista As CValorPond
            Set oAtributo = oFSGSRaiz.Generar_CAtributo
            'Recogemos el atributo
            oAtributo.Id = sdbgCostesLineaCatalogo.Columns("ATRIB_ID").Value
            
            If oAtributo.Id > 0 Then
                'Cargamos la lista de valores del atributo
                oAtributo.CargarDatosAtributo
                oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
                For Each oLista In oAtributo.ListaPonderacion
                    If oLista.ValorLista = sdbgCostesLineaCatalogo.Columns("VALOR").Text Then
                        bValorEncontrado = True
                        Exit For
                    End If
                Next
                If bValorEncontrado = False And sdbgCostesLineaCatalogo.Columns("VALOR").Text <> "" Then
                    oMensajes.AtributoValorNoValido ("NO_LISTA")
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
    Else
        m_bErrorUpdate = True
        If Not m_bMensajeRecienSelecc Then oMensajes.AtributoValorNoValido "TIPO2"
        Cancel = True
        GoTo Salir
    End If
    
    'Compruebo los campos operacion y tipo
    If sdbgCostesLineaCatalogo.Columns("OPERACION").Text = "" Then
        m_bErrorUpdate = True
        oMensajes.NoValido sdbgDescuentosLineaCatalogo.Columns("OPERACION").caption
        Cancel = True
    ElseIf sdbgCostesLineaCatalogo.Columns("TIPO_HIDDEN").Text = "" Then
        m_bErrorUpdate = True
        oMensajes.NoValido sdbgDescuentosLineaCatalogo.Columns("TIPO").caption
        Cancel = True
    Else
        'Si el coste es de estos 2 tipos, se debera introducir un grupo
        Select Case sdbgCostesLineaCatalogo.Columns("TIPO_HIDDEN").Value
            Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes, TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
                If sdbgCostesLineaCatalogo.Columns("GRUPO").Value = "" Then
                    m_bErrorUpdate = True
                    oMensajes.GrupoCompatibilidadNoIndicado
                    Cancel = True
                Else
                    If Not ComprobarGrupos(True, sdbgCostesLineaCatalogo.Columns("GRUPO").Value, sdbgCostesLineaCatalogo.Columns("TIPO_HIDDEN").Value) Then
                        m_bErrorUpdate = True
                        oMensajes.GruposCostesDescIncorrectos
                        Cancel = True
                    End If
                End If
        End Select
    End If
    
Salir:
End Sub

''' <summary>'Comprueba que no se han metido costes/descuentos obligatorios y opcionales en el mismo grupo</summary>

Private Function ComprobarGrupos(ByVal bCoste As Boolean, ByVal sGrupo As String, ByVal Tipo As TipoCostesDescuentosLineaCatalogo) As Boolean
    Dim oAtrib As CAtributo
    Dim oColAtrib As CAtributos
    Dim dicObligatorios As Dictionary
    Dim dicOpcionales As Dictionary
    
    ComprobarGrupos = True
    
    If bCoste Then
        Set oColAtrib = m_oCostes
    Else
        Set oColAtrib = m_oDescuentos
    End If
    
    If Not oColAtrib Is Nothing Then
        If oColAtrib.Count > 0 Then
            Set dicObligatorios = New Dictionary
            Set dicOpcionales = New Dictionary
            
            'Recorrer los costes/descuentos ya existentes. Si est� metiendo uno opcional miro los grupos de los obligatorios y al rev�s
            For Each oAtrib In oColAtrib
                If Tipo = Opcionales_Excluyentes And oAtrib.TipoCosteDescuento = Obligatorios_Excluyentes Then
                    If Not dicObligatorios.Exists(oAtrib.GrupoCosteDescuento) Then dicObligatorios.Add oAtrib.GrupoCosteDescuento, oAtrib.GrupoCosteDescuento
                ElseIf Tipo = Obligatorios_Excluyentes And oAtrib.TipoCosteDescuento = Opcionales_Excluyentes Then
                    If Not dicOpcionales.Exists(oAtrib.GrupoCosteDescuento) Then dicOpcionales.Add oAtrib.GrupoCosteDescuento, oAtrib.GrupoCosteDescuento
                End If
            Next
            Set oAtrib = Nothing
            
            If Tipo = Opcionales_Excluyentes Then
                ComprobarGrupos = (Not dicObligatorios.Exists(sGrupo))
            ElseIf Tipo = Obligatorios_Excluyentes Then
                ComprobarGrupos = (Not dicOpcionales.Exists(sGrupo))
            End If
            
            Set dicObligatorios = Nothing
            Set dicOpcionales = Nothing
        End If
        
        Set oColAtrib = Nothing
    End If
End Function

''' <summary>
''' evento que salta al eliminar una fila del grid
''' </summary>
Private Sub sdbgCostesLineaCatalogo_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If IsEmpty(sdbgCostesLineaCatalogo.RowBookmark(sdbgCostesLineaCatalogo.Row)) Then
        sdbgCostesLineaCatalogo.Bookmark = sdbgCostesLineaCatalogo.RowBookmark(sdbgCostesLineaCatalogo.Row - 1)
    Else
        sdbgCostesLineaCatalogo.Bookmark = sdbgCostesLineaCatalogo.RowBookmark(sdbgCostesLineaCatalogo.Row)
    End If
End Sub
''' <summary>
''' Evita que se muestre el mensaje propio del grid al eliminar la fila
''' </summary>
Private Sub sdbgCostesLineaCatalogo_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub


''' <summary>
''' Actualiza la fila en edicion, graba los cambios en BD
''' </summary>
''' <param name="RtnDispErrMsg">Buffer con los datos y bookmark de la fila</param>
Private Sub sdbgCostesLineaCatalogo_AfterUpdate(RtnDispErrMsg As Integer)
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    
    'No sirve de nada q diga "Tipo incorrecto" en una l�nea q voy a borrar
    If m_bNoUpdatable Then
        Exit Sub
    End If
      
    With sdbgCostesLineaCatalogo
        'Si no ha habido cambios en la fila, el objeto m_oCosteEnEdicion sera Nothing y no se grabara nada
        If Not m_oCosteEnEdicion Is Nothing Then
           m_bErrorUpdate = False
            ''' Actualizamos el valor de las propiedades
            m_oCosteEnEdicion.valor = .Columns("VALOR").Value
            m_oCosteEnEdicion.OperacionCosteDescuento = .Columns("OPERACION_HIDDEN").Value
            m_oCosteEnEdicion.TipoCosteDescuento = .Columns("TIPO_HIDDEN").Value
            m_oCosteEnEdicion.GrupoCosteDescuento = .Columns("GRUPO").Value
            m_oCosteEnEdicion.Descripcion = .Columns("DESCR").Value
            
            ''' Modificamos en la base de datos
            teserror = m_oCosteEnEdicion.ModificarCosteDescuentoLineaCatalogo
            
            If teserror.NumError <> TESnoerror Then
                v = .ActiveCell.Value
                TratarError teserror
                If Me.Visible Then .SetFocus
                m_bErrorUpdate = True
                .ActiveCell.Value = v
                .DataChanged = False
                .CancelUpdate
            Else
                Set m_oCosteEnEdicion = Nothing
            End If
            'Cuando ya se ha actualizado la fila, el boton de deshacer se deshabilitara
            cmdDeshacerCoste.Enabled = False
            Screen.MousePointer = vbNormal
        Else
            'Se trata de un nuevo atributo
            Set m_oCosteEnEdicion = oFSGSRaiz.Generar_CAtributo
            m_oCosteEnEdicion.Cod = .Columns("COD").Value
            m_oCosteEnEdicion.Den = .Columns("NOMBRE").Value
            m_oCosteEnEdicion.Descripcion = .Columns("DESCR").Value
            m_oCosteEnEdicion.valor = .Columns("VALOR").Value
            m_oCosteEnEdicion.valorNum = .Columns("VALOR").Value
            m_oCosteEnEdicion.OperacionCosteDescuento = .Columns("OPERACION_HIDDEN").Value
            m_oCosteEnEdicion.TipoCosteDescuento = .Columns("TIPO_HIDDEN").Value
            m_oCosteEnEdicion.GrupoCosteDescuento = .Columns("GRUPO").Value
            m_oCosteEnEdicion.Descripcion = .Columns("DESCR").Value
            m_oCosteEnEdicion.Minimo = .Columns("MIN").Value
            m_oCosteEnEdicion.Maximo = .Columns("MAX").Value
            m_oCosteEnEdicion.TipoIntroduccion = .Columns("INTRO").Value
            m_oCosteEnEdicion.Atrib = .Columns("ATRIB_ID").Value
            
            teserror = m_oCosteEnEdicion.AnyadirCosteDescuentoLineaCatalogo(g_oLinea.Id, Coste)
            'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
            Else
                'Ponemos el ID a la l�nea del grid
                .Columns("ID").Value = m_oCosteEnEdicion.Id
                
                m_oCostes.addAtributo m_oCosteEnEdicion
            End If
        End If
    End With
End Sub
''' <summary>
''' evento que salta al realizar cualquier cambio en la grid
''' </summary>
Private Sub sdbgCostesLineaCatalogo_Change()
    If m_bEdicionCostes Then
        'Recojo el atributo que se va a modificar
        Set m_oCosteEnEdicion = m_oCostes.Item(sdbgCostesLineaCatalogo.Columns("ID").Value)
        'Si se hace un cambio en la fila, habilitamos el boton de deshacer
        cmdDeshacerCoste.Enabled = True
    End If
End Sub

'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
' DESCUENTOS
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


''' <summary>
''' Carga del submen� que se muestra al pulsar el boton de a�adir Descuentos
''' </summary>
''' <remarks>Llamada desde: frmCatalogoAtribEspyAdj; Tiempo m�ximo:0,1</remarks>
Private Sub cargarcPAnyadirDescuentos()

    Set cPAnyadirDescuentos = New cPopupMenu
    cPAnyadirDescuentos.hWndOwner = Me.hWnd
    
    cPAnyadirDescuentos.clear
    Dim indice As Integer
    indice = 1
    cPAnyadirDescuentos.AddItem m_sNuevo, , indice, , , , , "MAESTROATR"
    indice = indice + 1
    cPAnyadirDescuentos.AddItem m_sDesdeProceso, , indice, , , , , "ARTOPROVEOPROCE"
End Sub

''' <summary>Configura el estado de edicion del grid, bloqueando o no las columnas, y habilita o no los botones de edicion</summary>
Private Sub ConfigurarEdicionGridDescuentos(ByVal Locked As Boolean)
    sdbgDescuentosLineaCatalogo.Columns("VALOR").Locked = Locked
    sdbgDescuentosLineaCatalogo.Columns("OPERACION").Locked = Locked
    sdbgDescuentosLineaCatalogo.Columns("TIPO").Locked = Locked
    sdbgDescuentosLineaCatalogo.Columns("GRUPO").Locked = Locked
    
    If Locked Then
        Me.cmdA�adirDescuento.Enabled = False
        Me.cmdDeshacerDescuento.Enabled = False
        Me.cmdEliminarDescuento.Enabled = False
        Me.cmdDeshacerDescuento.Enabled = False
        Me.cmdEdicionDescuento.caption = m_sEdicionCaption
        m_bEdicionDescuentos = False
    Else
        Me.cmdA�adirDescuento.Enabled = True
        Me.cmdDeshacerDescuento.Enabled = True
        Me.cmdEliminarDescuento.Enabled = True
        Me.cmdDeshacerDescuento.Enabled = False
        Me.cmdEdicionDescuento.caption = m_sConsultaCaption
        m_bEdicionDescuentos = True
    End If
End Sub
''' <summary>Configura como Dropdowns las columnas de operacion y tipo que siempre seran combos</summary>
Private Sub ConfigurarCombosGridDescuentos()
    'Le asigno los dropdowns a las columnas operacion y tipo
    sdbddOperacionDescuento.RemoveAll
    sdbddOperacionDescuento.AddItem ""
    sdbddTipoDescuento.RemoveAll
    sdbddTipoDescuento.AddItem ""
    sdbgDescuentosLineaCatalogo.Columns("OPERACION").DropDownHwnd = sdbddOperacionDescuento.hWnd
    sdbgDescuentosLineaCatalogo.Columns("TIPO").DropDownHwnd = sdbddTipoDescuento.hWnd
    sdbddTipoDescuento.Enabled = True
    sdbddOperacionDescuento.Enabled = True
End Sub

''' <summary>Muestra el menu popup con las distintas opciones que hay para a�adir Descuentos de linea de catalogo</summary>
Private Sub cmdA�adirdescuento_Click()
    m_bMensajeRecienSelecc = True
    MostrarcPAnyadirDescuentos cmdA�adirDescuento.Left, cmdA�adirDescuento.Top + cmdA�adirDescuento.Height
    m_bMensajeRecienSelecc = False
End Sub

''' <summary>
''' Lanza las opciones del menu contextual de a�adir Descuentos
''' </summary>
''' <param name="ItemNumber">Indice del men�</param>
''' <remarks>Llamada desde: Click de cmdOtrasAcciones; Tiempo m�ximo:0,1</remarks>
Private Sub cPAnyadirDescuentos_Click(ItemNumber As Long)
    Dim oAtr As CAtributo
    Dim odescuento As CAtributo
    Dim tsError As TipoErrorSummit
    Dim i As Integer
    
    m_bA�adiendo = True 'Indicamos que se va a a�adir un descuento, esto nos sera util para evitar el evento Row_colchange al a�adirlo al grid, si lo hicieramos
    Select Case cPAnyadirDescuentos.itemKey(ItemNumber)
        'A�adimos un Descuento desde el maestro de atributos
        Case "MAESTROATR"
           
            If g_oLinea Is Nothing Then Unload Me
            
            Set g_oDescuentosA�adir = oFSGSRaiz.Generar_CAtributos
            
            'A�ade un campo de tipo atributo de GS
            If Not g_ofrmATRIBMod Is Nothing Then
                Unload g_ofrmATRIBMod
                Set g_ofrmATRIBMod = Nothing
            End If
        
            Set g_ofrmATRIBMod = New frmAtribMod
            g_ofrmATRIBMod.g_sOrigen = "frmCatalogoCostesDescuentos_Descuentos"
            
            g_ofrmATRIBMod.Show vbModal
            
            'A�adir atributos
            If g_oDescuentosA�adir.Count > 0 Then
                AgregarAtributos g_oDescuentosA�adir, sdbgDescuentosLineaCatalogo
            Else
                m_bA�adiendo = False
            End If
                        
        Case "ARTOPROVEOPROCE"
            Set g_oDescuentosA�adir = Nothing
            Set g_oDescuentosA�adir = oFSGSRaiz.Generar_CAtributos
            Set frmCostesDescuentosProce.g_oLinea = Me.g_oLinea
            frmCostesDescuentosProce.g_sOrigen = "frmCatalogoCostesDescuentos_Descuentos"
            frmCostesDescuentosProce.Show vbModal
            
            'GUARDAMOS LOS CAMPOS A�ADIDOS EN BDD
            
            If Not g_oDescuentosA�adir Is Nothing Then
                For Each odescuento In g_oDescuentosA�adir
                    tsError = odescuento.AnyadirCosteDescuentoLineaCatalogo(g_oLinea.Id, Descuento)
                    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
                    If tsError.NumError <> TESnoerror Then
                        TratarError tsError
                    End If
                Next
            Else
                m_bA�adiendo = False 'No se ha a�adido ningun coste
            End If

            'RECARGAMOS LAS L�NEAS
            Set m_oDescuentos = Nothing
            Set m_oDescuentos = oFSGSRaiz.Generar_CAtributos
            Set m_oDescuentos = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , Descuento, Linea)
            'Ponemos el ID a cada l�nea del grid
            With sdbgDescuentosLineaCatalogo
                .MoveFirst
                If .Rows > 0 Then
                    For Each oAtr In m_oDescuentos
                        For i = 0 To .Rows - 1
                            If (.Columns("ID").Value = CStr(0) Or .Columns("ID").Value = "") And .Columns("COD").Value = oAtr.Cod And .Columns("ATRIB_ID").Value = CStr(oAtr.Atrib) Then
                                .Columns("ID").Value = oAtr.Id
                            End If
                            .MoveNext
                        Next
                         .MoveFirst
                    Next
                End If
            End With
            
    End Select
End Sub

Private Sub AgregarAtributos(ByVal oAtributosNuevos As CAtributos, ByRef oGrid As SSDBGrid)
    Dim oAtrib As CAtributo
    Dim inum As Integer
    Dim vbm As Variant
    Dim bAdd As Boolean
    
    With oGrid
        For Each oAtrib In oAtributosNuevos
            'AddNew
                                   
            bAdd = True
            'Comprobar que no existe ya
            If .Rows > 0 Then
                For inum = 0 To .Rows - 1
                    vbm = .AddItemBookmark(inum)
                    If CStr(oAtrib.Atrib) = .Columns("ATRIB_ID").CellValue(vbm) Then
                        .CancelUpdate
                        bAdd = False
                    End If
                Next
            End If
            
            If bAdd Then
                'Si no se hace as�, si se hiciera un refresh (al cambiar el tipo) estando la l�nea en edici�n, se perder�an todos los datos
                .AddItem oAtrib.Cod & Chr(m_lSeparador) & oAtrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtrib.OperacionCosteDescuento & Chr(m_lSeparador) & _
                    oAtrib.valor & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtrib.Descripcion & Chr(m_lSeparador) & _
                    oAtrib.TipoIntroduccion & Chr(m_lSeparador) & oAtrib.Minimo & Chr(m_lSeparador) & oAtrib.Maximo & Chr(m_lSeparador) & oAtrib.Atrib & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                
                'Poner en edici�n
                .Row = .Rows - 1
                .Columns("OPERACION_HIDDEN").Value = ""
                .Columns("OPERACION_HIDDEN").Value = oAtrib.OperacionCosteDescuento
            End If
        Next
        
        'Actualizar grid
        .Update
        .Refresh
    End With
    
    Set oAtributosNuevos = oFSGSRaiz.Generar_CAtributos
    
    Set oAtrib = Nothing
End Sub


''' <summary>Elimina el Descuento seleccionado de la grid y de BD</summary>
Private Sub cmdEliminardescuento_Click()
    Screen.MousePointer = vbHourglass
    Dim inum As Integer
    Dim odescuentos As CAtributos
    Dim odescuento As CAtributo
    Dim tsError As TipoErrorSummit
    Dim i As Integer
    
    'No sirve de nada q diga "Tipo incorrecto" en una l�nea q voy a borrar
    m_bNoUpdatable = True
    
    sdbgDescuentosLineaCatalogo.Columns("VALOR").DropDownHwnd = 0
    With sdbgDescuentosLineaCatalogo
        If .SelBookmarks.Count > 0 Then
            i = oMensajes.PreguntaEliminar(m_sDeseaEliminarDescuento)
            If i = vbYes Then
                inum = 0
                Set odescuentos = oFSGSRaiz.Generar_CAtributos
                While inum < .SelBookmarks.Count
                    .Bookmark = .SelBookmarks(inum)
                    odescuentos.Add .Columns("ID").Value, .Columns("COD").Text, .Columns("NOMBRE").Text, TipoNumerico
                inum = inum + 1
                Wend
                '3. ELIMINAR DE BDD
                
                For Each odescuento In odescuentos
                    If Not (NullToStr(odescuento.Id) = "") Then
                        tsError = odescuento.EliminarCosteDescuentoLineaCatalogo
                        If tsError.NumError <> TESnoerror Then
                            Screen.MousePointer = vbNormal
                            TratarError tsError
                            Exit Sub
                        End If
                    End If
                Next
            
                '2.ELIMINAR DEL GRID
                .DeleteSelected
                .Update
                '.Refresh
                '4. RECARGAR LA VARIABLE DE LOS DescuentoS
                Set m_oDescuentos = Nothing
                Set m_oDescuentos = oFSGSRaiz.Generar_CAtributos
                Set m_oDescuentos = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , Descuento, Linea)
            Else
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox m_sNoHayDescuentoSeleccionado, vbExclamation
            Exit Sub
        End If
    End With
    sdbgDescuentosLineaCatalogo.MoveLast
    Screen.MousePointer = vbNormal
    
    m_bNoUpdatable = False
End Sub

''' <summary>Deshace los cambios de una fila del grid de Descuentos</summary>
Private Sub cmdDeshacerDescuento_Click()
    sdbgDescuentosLineaCatalogo.CancelUpdate
    sdbgDescuentosLineaCatalogo.DataChanged = False
    
    'Elimino el objeto en edicion
    If Not m_oDescuentoEnEdicion Is Nothing Then
        Set m_oDescuentoEnEdicion = Nothing
    End If
    
    If sdbgDescuentosLineaCatalogo.Rows = 1 Then
        sdbgDescuentosLineaCatalogo.MoveFirst
        sdbgDescuentosLineaCatalogo.Refresh
        sdbgDescuentosLineaCatalogo.Bookmark = sdbgDescuentosLineaCatalogo.SelBookmarks(0)
        sdbgDescuentosLineaCatalogo.SelBookmarks.RemoveAll
    End If
    
    cmdDeshacerDescuento.Enabled = False
End Sub

''' <summary>
''' muestra el submen� de a�adir Descuentos de linea de catalogo
''' </summary>
''' <param name="X">Coordenada horizontal</param>
''' <param name="Y">Coordenada vertical</param>
''' <remarks>Llamada desde: frmCatalogoAtribEspYAdj; Tiempo m�ximo:0,1</remarks>
Private Sub MostrarcPAnyadirDescuentos(ByVal X As Single, ByVal Y As Single)
    cPAnyadirDescuentos.ShowPopupMenu X, Y
End Sub

''' <summary>Cambia de modo edicion a consulta el grid de Descuentos y viceversa</summary>
Private Sub cmdEdicionDescuento_Click()
    Dim v As Variant
    
    If m_bEdicionDescuentos Then
        'Se pasa a modo consulta
        If sdbgDescuentosLineaCatalogo.DataChanged = True Then
    
            v = sdbgDescuentosLineaCatalogo.ActiveCell.Value
            If Me.Visible Then sdbgDescuentosLineaCatalogo.SetFocus
            sdbgDescuentosLineaCatalogo.ActiveCell.Value = v
            m_bErrorUpdate = False
            
            If sdbgDescuentosLineaCatalogo.Row = 0 Then   'Es la primera fila o la �nica
            
                If sdbgDescuentosLineaCatalogo.Rows = 1 Then
                    sdbgDescuentosLineaCatalogo.Update
                    If m_bErrorUpdate Then
                        Exit Sub
                    End If
                Else
                    sdbgDescuentosLineaCatalogo.MoveNext
                    DoEvents
                    If m_bErrorUpdate Then
                        Exit Sub
                    Else
                        sdbgDescuentosLineaCatalogo.MovePrevious
                    End If
                End If
            Else                              'No es la primera fila
                sdbgDescuentosLineaCatalogo.MovePrevious
                DoEvents
                If m_bErrorUpdate Then  'Si ha ocurrido un error de validaci�n o de modificaci�n
                    Exit Sub
                Else
                    sdbgDescuentosLineaCatalogo.MoveNext
                End If
            End If
         
        End If
        'Si el grid esta en modo consulta, se pasara a modo consulta y se deshabilitaran botones de edicion
        ConfigurarEdicionGridDescuentos (True)
        m_bEdicionDescuentos = False
    Else
        'Si el grid esta en modo consulta, se pasara a modo edicion y se habilitaran botones de edici�n
        ConfigurarEdicionGridDescuentos (False)
        m_bEdicionDescuentos = True
    End If
End Sub

''' <summary>Carga el objeto de m_oCamposPedidoCategoria con los campos de pedido del ambito linea de la categoria de catalogo</summary>
Private Sub CargarDescuentosLineaCatalogo()
    If g_oLinea Is Nothing Then Unload Me
    
    Set m_oDescuentos = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, , Descuento)
    CargarGridDescuentosLineaCatalogo
End Sub

''' <summary>Carga en la grid los Descuentos de la linea de catalogo</summary>
Private Sub CargarGridDescuentosLineaCatalogo()
    Dim oAtrib As CAtributo
    sdbgDescuentosLineaCatalogo.RemoveAll
    
    If Not m_oDescuentos Is Nothing Then
        For Each oAtrib In m_oDescuentos
            sdbgDescuentosLineaCatalogo.AddItem oAtrib.Cod & Chr(m_lSeparador) & oAtrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtrib.OperacionCosteDescuento & Chr(m_lSeparador) & oAtrib.valor & Chr(m_lSeparador) & oAtrib.TipoCosteDescuento & Chr(m_lSeparador) & oAtrib.GrupoCosteDescuento & Chr(m_lSeparador) & oAtrib.Descripcion & Chr(m_lSeparador) & oAtrib.TipoIntroduccion & Chr(m_lSeparador) & oAtrib.Minimo & Chr(m_lSeparador) & oAtrib.Maximo & Chr(m_lSeparador) & oAtrib.Atrib & Chr(m_lSeparador) & oAtrib.Id & Chr(m_lSeparador) & oAtrib.TipoCosteDescuento
        Next
    End If
    
End Sub




'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DE DROPDOWNs de DescuentoS
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>evento que salta al elegir una opcion del dropdown</summary>
Private Sub sdbddTipodescuento_CloseUp()
    With sdbgDescuentosLineaCatalogo
        .Columns("TIPO_HIDDEN").Value = sdbddTipoDescuento.Columns("VALOR").Value
        
        'Si el tipo de Descuento es fijo o opcional simple, la columna de grupo se le pondra estilo gris, quedara deshabilitada
        'y si tendria valor se eliminaria, sino se cambiara el estilo a normal
        Select Case sdbddTipoDescuento.Columns("VALOR").Value
            Case TipoCostesDescuentosLineaCatalogo.Fijos
                .Columns("GRUPO").CellStyleSet "Gris"
                .Columns("GRUPO").Text = ""
            Case TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
                .Columns("GRUPO").CellStyleSet "Gris"
                .Columns("GRUPO").Text = ""
            Case TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
                .Columns("GRUPO").CellStyleSet "Normal"
            Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes
                .Columns("GRUPO").CellStyleSet "Normal"
        End Select
        
        .Refresh
    End With
End Sub

Private Sub sdbddOperaciondescuento_CloseUp()
    sdbgDescuentosLineaCatalogo.Columns("OPERACION").Value = sdbddOperacionDescuento.Columns("VALOR").Value
    sdbgDescuentosLineaCatalogo.Columns("OPERACION_HIDDEN").Value = sdbddOperacionDescuento.Columns("VALOR").Value
End Sub

''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddTipodescuento_DropDown()
    
    If sdbgDescuentosLineaCatalogo.Rows = 0 Then Exit Sub
   
    If sdbgDescuentosLineaCatalogo.Columns("VALOR").Locked Then
        sdbddTipoDescuento.DroppedDown = False
        Exit Sub
    End If
    sdbddTipoDescuento.RemoveAll
    sdbddTipoDescuento.DroppedDown = True
    
    'A�adimos los tipos de Descuentos
    sdbddTipoDescuento.AddItem TipoCostesDescuentosLineaCatalogo.Fijos & Chr(m_lSeparador) & m_sFijo
    sdbddTipoDescuento.AddItem TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes & Chr(m_lSeparador) & m_sObligatorioExcluyente
    sdbddTipoDescuento.AddItem TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes & Chr(m_lSeparador) & m_sOpcionalExcluyente
    sdbddTipoDescuento.AddItem TipoCostesDescuentosLineaCatalogo.Opcionales_Simples & Chr(m_lSeparador) & m_sOpcionalSimple
            
    'Se le modifica el ancho del dropdown
    sdbddTipoDescuento.Width = sdbgDescuentosLineaCatalogo.Columns("TIPO").Width
    sdbddTipoDescuento.Columns("DESC").Width = sdbddTipoDescuento.Width
End Sub
''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddOperaciondescuento_DropDown()
    If sdbgDescuentosLineaCatalogo.Rows = 0 Then Exit Sub
   
    If sdbgDescuentosLineaCatalogo.Columns("VALOR").Locked Then
        sdbddOperacionDescuento.DroppedDown = False
        Exit Sub
    End If
    sdbddOperacionDescuento.RemoveAll
    sdbddOperacionDescuento.DroppedDown = True
    
    'A�adimos las distintas operaciones
    sdbddOperacionDescuento.AddItem m_sRestar & Chr(m_lSeparador) & m_sRestar
    sdbddOperacionDescuento.AddItem m_sRestarPorcentaje & Chr(m_lSeparador) & m_sRestarPorcentaje
    
    'Se le modifica el ancho del dropdown
    sdbddOperacionDescuento.Width = sdbgDescuentosLineaCatalogo.Columns("OPERACION").Width
    sdbddOperacionDescuento.Columns("DESC").Width = sdbddOperacionDescuento.Width
End Sub
''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddValordescuento_DropDown()
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1
    
    If sdbgDescuentosLineaCatalogo.Rows = 0 Then Exit Sub
   
    If sdbgDescuentosLineaCatalogo.Columns("VALOR").Locked Then
        sdbddValorDescuento.DroppedDown = False
        Exit Sub
    End If
    sdbddValorDescuento.RemoveAll
    sdbddValorDescuento.DroppedDown = True
    
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    'Recogemos el atributo
    oAtributo.Id = sdbgDescuentosLineaCatalogo.Columns("ATRIB_ID").Value
    
    If oAtributo.Id > 0 Then
        If sdbgDescuentosLineaCatalogo.Columns("INTRO").Value = "1" Then
            'Cargamos la lista de valores del atributo
            oAtributo.CargarDatosAtributo
            oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
            For Each oLista In oAtributo.ListaPonderacion
                sdbddValorDescuento.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        End If
    End If
    'Se le modifica el ancho del dropdown
    sdbddValorDescuento.Width = sdbgDescuentosLineaCatalogo.Columns("VALOR").Width
    sdbddValorDescuento.Columns("DESC").Width = sdbddValorDescuento.Width
End Sub

''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddTipodescuento_InitColumnProps()
    sdbddTipoDescuento.DataFieldList = "Column 0"
    sdbddTipoDescuento.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddOperaciondescuento_InitColumnProps()
    sdbddOperacionDescuento.DataFieldList = "Column 0"
    sdbddOperacionDescuento.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddValordescuento_InitColumnProps()
    sdbddValorDescuento.DataFieldList = "Column 0"
    sdbddValorDescuento.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddValordescuento_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValorDescuento.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValorDescuento.Rows - 1
            bm = sdbddValorDescuento.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValorDescuento.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgDescuentosLineaCatalogo.Columns("VALOR").Value = Mid(sdbddValorDescuento.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValorDescuento.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddOperaciondescuento_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddOperacionDescuento.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOperacionDescuento.Rows - 1
            bm = sdbddOperacionDescuento.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOperacionDescuento.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddOperacionDescuento.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddTipodescuento_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddTipoDescuento.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddTipoDescuento.Rows - 1
            bm = sdbddTipoDescuento.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddTipoDescuento.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddTipoDescuento.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub






'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DEL GRID DE DescuentoS DE LINEA DE CATALOGO
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

''' <summary>evento del grid que se produce por cada linea que se carga</summary>
Private Sub sdbgDescuentosLineaCatalogo_RowLoaded(ByVal Bookmark As Variant)
    
    'Si el atributo tiene descripcion, se cambia el StyleSet para cambiarle el estilo al boton
    If sdbgDescuentosLineaCatalogo.Columns("DESCR").CellValue(Bookmark) <> "" Then
        sdbgDescuentosLineaCatalogo.Columns("DESCR_BTN").Text = "..."
    End If

    'Si el tipo de Descuento es fijo o opcional simple, la columna de grupo se le pondra estilo gris, quedara deshabilitada
    Select Case sdbgDescuentosLineaCatalogo.Columns("TIPO_HIDDEN").CellValue(Bookmark)
        Case TipoCostesDescuentosLineaCatalogo.Fijos
            sdbgDescuentosLineaCatalogo.Columns("GRUPO").CellStyleSet "Gris"
            sdbgDescuentosLineaCatalogo.Columns("TIPO").Text = m_sFijo
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
            sdbgDescuentosLineaCatalogo.Columns("GRUPO").CellStyleSet "Gris"
            sdbgDescuentosLineaCatalogo.Columns("TIPO").Text = m_sOpcionalSimple
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
            sdbgDescuentosLineaCatalogo.Columns("TIPO").Text = m_sOpcionalExcluyente
        Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes
            sdbgDescuentosLineaCatalogo.Columns("TIPO").Text = m_sObligatorioExcluyente
    End Select
    
    Select Case sdbgDescuentosLineaCatalogo.Columns("OPERACION_HIDDEN").CellValue(Bookmark)
        Case m_sRestar
            sdbgDescuentosLineaCatalogo.Columns("OPERACION").Text = m_sRestar
        Case m_sRestarPorcentaje
            sdbgDescuentosLineaCatalogo.Columns("OPERACION").Text = m_sRestarPorcentaje
        Case Else
            sdbgDescuentosLineaCatalogo.Columns("OPERACION").Text = m_sRestar
    End Select
End Sub
''' <summary>evento del grid que se produce el pulsar el las columnas que son boton</summary>
Private Sub sdbgDescuentosLineaCatalogo_BtnClick()
    'acabo con la edicion del atributo
'    If sdbgDescuentosLineaCatalogo.DataChanged = True Then
'        sdbgDescuentosLineaCatalogo.Update
'        If m_bError Then
'            Exit Sub
'        End If
'    End If

    If m_bEdicionDescuentos Then
        frmATRIBDescr.g_bEdicion = True
    Else
        frmATRIBDescr.g_bEdicion = False
    End If
    frmATRIBDescr.g_sOrigen = "frmCatalogoCostesDescuentos_Descuentos"
    frmATRIBDescr.txtDescr.Text = sdbgDescuentosLineaCatalogo.Columns("DESCR").Value
    frmATRIBDescr.Show 1


    'acabo con la edicion del atributo cuando me devuelve modificaciones.
    If sdbgDescuentosLineaCatalogo.DataChanged Then
        If m_oDescuentoEnEdicion Is Nothing Then
            Set m_oDescuentoEnEdicion = m_oDescuentos.Item(sdbgDescuentosLineaCatalogo.Columns("ID").Value)
        End If
    End If
'
'        sdbgDescuentosLineaCatalogo.Update
'        If m_bError Then
'            Exit Sub
'        End If
'    End If
'
End Sub

''' <summary>evento del grid que se produce cuando el usuario cambia de celda o de fila</summary>
Private Sub sdbgDescuentosLineaCatalogo_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    If sdbgDescuentosLineaCatalogo.Col > 0 Then
        If sdbgDescuentosLineaCatalogo.Columns(sdbgDescuentosLineaCatalogo.Col).Name = "VALOR" Then
            'Si el atributo es de tipo lista ponemos a la columna valor el dropdown
            If sdbgDescuentosLineaCatalogo.Columns("INTRO").Value = 1 Then
                'Lista
                sdbddValorDescuento.RemoveAll
                sdbddValorDescuento.AddItem ""
                sdbgDescuentosLineaCatalogo.Columns("VALOR").DropDownHwnd = sdbddValorDescuento.hWnd
                sdbddValorDescuento.Enabled = True
            Else
                'Introduccion libre
                sdbgDescuentosLineaCatalogo.Columns("VALOR").DropDownHwnd = 0
                sdbddValorDescuento.Enabled = False
            End If
        End If
    End If
    
    'Si el tipo de Descuento es fijo o opcional simple, la columna de grupo quedara deshabilitada
    Select Case sdbgDescuentosLineaCatalogo.Columns("TIPO_HIDDEN").Value
        Case TipoCostesDescuentosLineaCatalogo.Fijos, TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
            sdbgDescuentosLineaCatalogo.Columns("GRUPO").Locked = True
        Case Else
            'Si es otro tipo de Descuento, si esta permitida la edicion, desbloquearemos la columna
            If m_bEdicionDescuentos Then
                sdbgDescuentosLineaCatalogo.Columns("GRUPO").Locked = False
            End If
    End Select
    'Si el atributo se acaba de a�adir no se hacen las comprobaciones, se haran a la hora de grabar en bd
    If m_bA�adiendo = False Then
         'Compruebo si se ha cambiado de fila
        If m_iRowActual <> sdbgDescuentosLineaCatalogo.Row Then
            'Si se ha cambiado de fila lanzo la actualizaci�n, ira al evento BeforeUpdate y AfterUpdate del grid
            sdbgDescuentosLineaCatalogo.Update
        End If
    End If
    m_iRowActual = sdbgDescuentosLineaCatalogo.Row
    m_bA�adiendo = False
End Sub

''' <summary>evento del grid que se produce cuando se lanza el update de la grid, haremos las comprobaciones de datos</summary>
Private Sub sdbgDescuentosLineaCatalogo_BeforeUpdate(Cancel As Integer)
    
    'No sirve de nada q diga "Tipo incorrecto" en una l�nea q voy a borrar
    If m_bNoUpdatable Then
        Exit Sub
    End If
    
    Cancel = False
    'Comprobamos que el valor introducido es numerico
    If sdbgDescuentosLineaCatalogo.Columns("VALOR").Text <> "" Then
        If (Not IsNumeric(sdbgDescuentosLineaCatalogo.Columns("VALOR").Text)) Then
            m_bErrorUpdate = True
            oMensajes.AtributoValorNoValido ("TIPO2")
            Cancel = True
            GoTo Salir
        Else
            If sdbgDescuentosLineaCatalogo.Columns("MIN").Text <> "" And sdbgDescuentosLineaCatalogo.Columns("MAX").Text <> "" Then
                If StrToDbl0(sdbgDescuentosLineaCatalogo.Columns("MIN").Text) > StrToDbl0(sdbgDescuentosLineaCatalogo.Columns("VALOR").Text) Or StrToDbl0(sdbgDescuentosLineaCatalogo.Columns("MAX").Text) < StrToDbl0(sdbgDescuentosLineaCatalogo.Columns("VALOR").Text) Then
                    m_bErrorUpdate = True
                    oMensajes.ValorEntreMaximoYMinimo sdbgDescuentosLineaCatalogo.Columns("MIN").Text, sdbgDescuentosLineaCatalogo.Columns("MAX").Text
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
        
        'Comprobaremos si es una lista, que se haya elegido un valor de la lista
        If sdbgDescuentosLineaCatalogo.Columns("INTRO").Text = "1" Then
            Dim oAtributo As CAtributo
            Dim bValorEncontrado As Boolean
            Dim oLista As CValorPond
            Set oAtributo = oFSGSRaiz.Generar_CAtributo
            'Recogemos el atributo
            oAtributo.Id = sdbgDescuentosLineaCatalogo.Columns("ATRIB_ID").Value
            
            If oAtributo.Id > 0 Then
                'Cargamos la lista de valores del atributo
                oAtributo.CargarDatosAtributo
                oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
                For Each oLista In oAtributo.ListaPonderacion
                    If oLista.ValorLista = sdbgDescuentosLineaCatalogo.Columns("VALOR").Text Then
                        bValorEncontrado = True
                        Exit For
                    End If
                Next
                If bValorEncontrado = False And sdbgDescuentosLineaCatalogo.Columns("VALOR").Text <> "" Then
                    oMensajes.AtributoValorNoValido ("NO_LISTA")
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
    Else
        m_bErrorUpdate = True
        If Not m_bMensajeRecienSelecc Then oMensajes.AtributoValorNoValido "TIPO2"
        Cancel = True
        GoTo Salir
    End If
    
    'Compruebo los campos operacion y tipo
    If sdbgDescuentosLineaCatalogo.Columns("OPERACION").Text = "" Then
        m_bErrorUpdate = True
        oMensajes.NoValido sdbgDescuentosLineaCatalogo.Columns("OPERACION").caption
        Cancel = True
    ElseIf sdbgDescuentosLineaCatalogo.Columns("TIPO_HIDDEN").Text = "" Then
        m_bErrorUpdate = True
        oMensajes.NoValido sdbgDescuentosLineaCatalogo.Columns("TIPO").caption
        Cancel = True
    Else
        'Si el Descuento es de estos 2 tipos, se debera introducir un grupo
        Select Case sdbgDescuentosLineaCatalogo.Columns("TIPO_HIDDEN").Value
            Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes, TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
                If sdbgDescuentosLineaCatalogo.Columns("GRUPO").Value = "" Then
                    m_bErrorUpdate = True
                    oMensajes.GrupoCompatibilidadNoIndicado
                    Cancel = True
                Else
                    If Not ComprobarGrupos(False, sdbgDescuentosLineaCatalogo.Columns("GRUPO").Value, sdbgCostesLineaCatalogo.Columns("TIPO_HIDDEN").Value) Then
                        m_bErrorUpdate = True
                        oMensajes.GruposCostesDescIncorrectos
                        Cancel = True
                    End If
                End If
        End Select
    End If
    
Salir:
End Sub

''' <summary>
''' evento que salta al eliminar una fila del grid
''' </summary>
Private Sub sdbgDescuentosLineaCatalogo_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If IsEmpty(sdbgDescuentosLineaCatalogo.RowBookmark(sdbgDescuentosLineaCatalogo.Row)) Then
        sdbgDescuentosLineaCatalogo.Bookmark = sdbgDescuentosLineaCatalogo.RowBookmark(sdbgDescuentosLineaCatalogo.Row - 1)
    Else
        sdbgDescuentosLineaCatalogo.Bookmark = sdbgDescuentosLineaCatalogo.RowBookmark(sdbgDescuentosLineaCatalogo.Row)
    End If
End Sub
''' <summary>
''' Evita que se muestre el mensaje propio del grid al eliminar la fila
''' </summary>
Private Sub sdbgDescuentosLineaCatalogo_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>
''' Actualiza la fila en edicion, graba los cambios en BD
''' </summary>
''' <param name="RtnDispErrMsg">Buffer con los datos y bookmark de la fila</param>
Private Sub sdbgDescuentosLineaCatalogo_AfterUpdate(RtnDispErrMsg As Integer)
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    
    'No sirve de nada q diga "Tipo incorrecto" en una l�nea q voy a borrar
    If m_bNoUpdatable Then Exit Sub
          
    With sdbgDescuentosLineaCatalogo
        'Si no ha habido cambios en la fila, el objeto m_oDescuentoEnEdicion sera Nothing y no se grabara nada
        If Not m_oDescuentoEnEdicion Is Nothing Then
           m_bErrorUpdate = False
            ''' Actualizamos el valor de las propiedades
            m_oDescuentoEnEdicion.valor = .Columns("VALOR").Value
            m_oDescuentoEnEdicion.OperacionCosteDescuento = .Columns("OPERACION_HIDDEN").Value
            m_oDescuentoEnEdicion.TipoCosteDescuento = .Columns("TIPO_HIDDEN").Value
            m_oDescuentoEnEdicion.GrupoCosteDescuento = .Columns("GRUPO").Value
            m_oDescuentoEnEdicion.Descripcion = .Columns("DESCR").Value
            
            ''' Modificamos en la base de datos
            teserror = m_oDescuentoEnEdicion.ModificarCosteDescuentoLineaCatalogo
            
            If teserror.NumError <> TESnoerror Then
                v = .ActiveCell.Value
                TratarError teserror
                If Me.Visible Then .SetFocus
                m_bErrorUpdate = True
                .ActiveCell.Value = v
                .DataChanged = False
                .CancelUpdate
            Else
                Set m_oDescuentoEnEdicion = Nothing
            End If
            'Cuando ya se ha actualizado la fila, el boton de deshacer se deshabilitara
            cmdDeshacerDescuento.Enabled = False
            Screen.MousePointer = vbNormal
        Else
            'Se trata de un nuevo atributo
            Set m_oDescuentoEnEdicion = oFSGSRaiz.Generar_CAtributo
            m_oDescuentoEnEdicion.Cod = .Columns("COD").Value
            m_oDescuentoEnEdicion.Den = .Columns("NOMBRE").Value
            m_oDescuentoEnEdicion.Descripcion = .Columns("DESCR").Value
            m_oDescuentoEnEdicion.valor = .Columns("VALOR").Value
            m_oDescuentoEnEdicion.valorNum = .Columns("VALOR").Value
            m_oDescuentoEnEdicion.OperacionCosteDescuento = .Columns("OPERACION_HIDDEN").Value
            m_oDescuentoEnEdicion.TipoCosteDescuento = .Columns("TIPO_HIDDEN").Value
            m_oDescuentoEnEdicion.GrupoCosteDescuento = .Columns("GRUPO").Value
            m_oDescuentoEnEdicion.Descripcion = .Columns("DESCR").Value
            m_oDescuentoEnEdicion.Minimo = .Columns("MIN").Value
            m_oDescuentoEnEdicion.Maximo = .Columns("MAX").Value
            m_oDescuentoEnEdicion.TipoIntroduccion = .Columns("INTRO").Value
            m_oDescuentoEnEdicion.Atrib = .Columns("ATRIB_ID").Value
            
            teserror = m_oDescuentoEnEdicion.AnyadirCosteDescuentoLineaCatalogo(g_oLinea.Id, Descuento)
            'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
            Else
                'Ponemos el ID a la l�nea del grid
                .Columns("ID").Value = m_oDescuentoEnEdicion.Id
                
                m_oDescuentos.addAtributo m_oDescuentoEnEdicion
            End If
        End If
    End With
End Sub
''' <summary>
''' evento que salta al realizar cualquier cambio en la grid
''' </summary>
Private Sub sdbgDescuentosLineaCatalogo_Change()
    If m_bEdicionDescuentos Then
        'Recojo el atributo que se va a modificar
        Set m_oDescuentoEnEdicion = m_oDescuentos.Item(sdbgDescuentosLineaCatalogo.Columns("ID").Value)
        'Si se hace un cambio en la fila, habilitamos el boton de deshacer
        cmdDeshacerDescuento.Enabled = True
    End If
End Sub
