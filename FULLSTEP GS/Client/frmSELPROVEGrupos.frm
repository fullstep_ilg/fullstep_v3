VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSELPROVEGrupos 
   BackColor       =   &H00808000&
   Caption         =   "Selecci�n del proveedores / asignaci�n de grupos"
   ClientHeight    =   4980
   ClientLeft      =   720
   ClientTop       =   3120
   ClientWidth     =   10875
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELPROVEGrupos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4980
   ScaleWidth      =   10875
   Begin SSDataWidgets_B.SSDBGrid sdbgSelProve 
      Height          =   4770
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   10680
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   3
      stylesets.count =   7
      stylesets(0).Name=   "Asignado"
      stylesets(0).BackColor=   11007475
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSELPROVEGrupos.frx":0CB2
      stylesets(1).Name=   "Premium"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSELPROVEGrupos.frx":0CCE
      stylesets(1).AlignmentText=   0
      stylesets(1).AlignmentPicture=   1
      stylesets(2).Name=   "Deshabilitado"
      stylesets(2).ForeColor=   8421504
      stylesets(2).BackColor=   14671839
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSELPROVEGrupos.frx":103F
      stylesets(3).Name=   "PremiumAsig"
      stylesets(3).BackColor=   11007475
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmSELPROVEGrupos.frx":105B
      stylesets(3).AlignmentText=   0
      stylesets(3).AlignmentPicture=   1
      stylesets(4).Name=   "Normal"
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmSELPROVEGrupos.frx":1077
      stylesets(5).Name=   "Habilitado"
      stylesets(5).ForeColor=   0
      stylesets(5).BackColor=   16777215
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmSELPROVEGrupos.frx":1093
      stylesets(6).Name=   "Tan"
      stylesets(6).BackColor=   10079487
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmSELPROVEGrupos.frx":10AF
      DividerType     =   2
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "PROVECOD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   6085
      Columns(1).Caption=   "Proveedor"
      Columns(1).Name =   "PROVEDEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   14745599
      Columns(2).Width=   3916
      Columns(2).Caption=   "Grupo1"
      Columns(2).Name =   "GRU1"
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   2
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16777215
      _ExtentX        =   18838
      _ExtentY        =   8414
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSELPROVEGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_oProceso As CProceso
Private m_oIAsignaciones As IAsignaciones
Private m_oAsigs As CAsignaciones
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Public sOrigen As String
Private m_sMsgError As String

Private Sub CargarRecursos()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    Select Case sOrigen
        Case "ASIGNAR"
            Me.caption = oMensajes.CargarTexto(147, 38)
        Case "BLOQUEAR"
            Me.caption = oMensajes.CargarTexto(147, 55)
    End Select
    
    sdbgSelProve.Columns("PROVEDEN").caption = oMensajes.CargarTexto(147, 39)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEGrupos", "CargarRecursos", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEGrupos", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bDescargarFrm = False
m_bActivado = False
Me.Width = 10995
Me.Height = 5460

Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
            
CargarRecursos

PonerFieldSeparator Me

CargarProveedoresYGrupos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEGrupos", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub
Private Sub CargarProveedoresYGrupos()
Dim oAsig As CAsignacion
Dim oGrupo As CGrupo
Dim sGrupos As String
Dim bBloquear As Boolean
Dim iValor As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
GenerarColumnasDeGrupos
    
Set m_oIAsignaciones = g_oProceso
Select Case UCase(sOrigen)
    Case "ASIGNAR"
        bBloquear = False
        
    Case "BLOQUEAR"
        bBloquear = True
End Select
If frmSELPROVE.bVerAsigComp Then
    Set m_oAsigs = m_oIAsignaciones.DevolverAsignaciones(OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , True, bBloquear)
Else
    If frmSELPROVE.bVerAsigEqp Then
        Set m_oAsigs = m_oIAsignaciones.DevolverAsignaciones(OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, , , , True, bBloquear)
    Else
        Set m_oAsigs = m_oIAsignaciones.DevolverAsignaciones(OrdAsigPorCodProve, , , , , True, bBloquear)
    End If
End If
For Each oAsig In m_oAsigs
    sGrupos = ""
    For Each oGrupo In g_oProceso.Grupos
        If Not oAsig.Grupos.Item(oGrupo.Codigo) Is Nothing Then
            Select Case UCase(sOrigen)
                Case "ASIGNAR"
                    iValor = 1
                Case "BLOQUEAR"
                    iValor = oAsig.Grupos.Item(oGrupo.Codigo).Bloqueo
           End Select
        Else
            iValor = 0
        End If
        sGrupos = sGrupos & Chr(m_lSeparador) & CStr(iValor)
    Next
    sdbgSelProve.AddItem oAsig.CodProve & Chr(m_lSeparador) & oAsig.DenProve & sGrupos
Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEGrupos", "CargarProveedoresYGrupos", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub
Private Sub GenerarColumnasDeGrupos()
Dim i As Integer
Dim oGrupo As CGrupo
Dim oColumn As SSDataWidgets_B.Column

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oProceso.Grupos.Count > 0 Then
    i = 2
    For Each oGrupo In g_oProceso.Grupos
        If i = 2 Then
            sdbgSelProve.Columns(2).Name = "GR_" & oGrupo.Codigo & "_" & oGrupo.Id
            sdbgSelProve.Columns(2).caption = oGrupo.Den
        Else
            sdbgSelProve.Columns.Add i
            Set oColumn = sdbgSelProve.Columns(i)
            oColumn.Name = "GR_" & CStr(oGrupo.Codigo) & "_" & oGrupo.Id
            oColumn.CaptionAlignment = 2
            oColumn.Alignment = 0
            oColumn.Style = 2
            oColumn.caption = oGrupo.Den
            oColumn.Visible = True
            oColumn.Locked = False
            Set oColumn = Nothing
        End If
        i = i + 1
    Next
 End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEGrupos", "GenerarColumnasDeGrupos", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub Form_Resize()
Dim i As Integer
Dim j As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next

    'Redimensiona el form
    If Me.Width < 1500 Then Exit Sub
    If Me.Height < 1500 Then Exit Sub
    
    sdbgSelProve.Width = Me.Width - 300
    sdbgSelProve.Height = Me.Height - 600
        
    i = sdbgSelProve.Cols - 2
    
    For j = 1 To i
        sdbgSelProve.Columns(j + 1).Width = 2220
    Next
    If sdbgSelProve.Width < (i * 2220) + 3500 + 240 Then
        sdbgSelProve.Columns("PROVEDEN").Width = 3500
        sdbgSelProve.SplitterPos = 1
        sdbgSelProve.SplitterVisible = True
    Else
        sdbgSelProve.Columns("PROVEDEN").Width = sdbgSelProve.Width - (i * 2220) - 240
        sdbgSelProve.SplitterVisible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEGrupos", "Form_Resize", err, Erl)
        Exit Sub
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oProceso = Nothing
    Set m_oIAsignaciones = Nothing
    Set m_oAsigs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEGrupos", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgSelProve_Change()
Dim vGrupo As Variant ' (0) Codigo de Grupo, (1) ID del grupo
Dim teserror As TipoErrorSummit
Dim sCod As String
Dim iRes As Integer
Dim bGuardarVisor As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
vGrupo = Split(Right(sdbgSelProve.Columns(sdbgSelProve.col).Name, Len(sdbgSelProve.Columns(sdbgSelProve.col).Name) - 3), "_")

sCod = sdbgSelProve.Columns("PROVECOD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgSelProve.Columns("PROVECOD").Value))

bGuardarVisor = False
Select Case UCase(sOrigen)
    Case "ASIGNAR"
        If GridCheckToBoolean(sdbgSelProve.Columns(sdbgSelProve.col).Value) = True Then
            teserror = m_oAsigs.Item(sCod).AsignarGrupo(g_oProceso.Anyo, g_oProceso.GMN1Cod, g_oProceso.Cod, vGrupo(0), sdbgSelProve.Columns(sdbgSelProve.col).caption, vGrupo(1))
        Else
            If m_oAsigs.Item(sCod).Grupos.Item(vGrupo(0)).ComprobarItemsCerrados(sdbgSelProve.Columns("PROVECOD").Value) Then
                oMensajes.MensajeOKOnly 735 'Existen items cerrados asignados al proveedor
                sdbgSelProve.CancelUpdate
                Exit Sub
            End If
            If m_oAsigs.Item(sCod).Grupos.Item(vGrupo(0)).ComprobarOfertas(sdbgSelProve.Columns("PROVECOD").Value) Then
                iRes = oMensajes.MensajeYesNo(734) 'Existen ofertas para el prove-grupo
                If iRes = vbNo Then
                    sdbgSelProve.CancelUpdate
                    Exit Sub
                End If
            End If
            If g_oProceso.Estado >= ConObjetivosSinNotificarYPreadjudicado And g_oProceso.Estado < conadjudicaciones Then
                If m_oAsigs.Item(sCod).Grupos.Item(vGrupo(0)).ComprobarAdjudicaciones Then
                    iRes = oMensajes.PreguntaHayAdjudicacionesGuardadas
                    If iRes = vbNo Then
                        sdbgSelProve.CancelUpdate
                        Exit Sub
                    End If
                    bGuardarVisor = True
                End If
            End If
            
            teserror = m_oAsigs.Item(sCod).DesasignarGrupo(g_oProceso.Anyo, g_oProceso.GMN1Cod, g_oProceso.Cod, vGrupo(0), vGrupo(1))
        End If
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            sdbgSelProve.CancelUpdate
            Exit Sub
        End If
        
        If GridCheckToBoolean(sdbgSelProve.Columns(sdbgSelProve.col).Value) = True Then
            RegistrarAccion ACCSelProveAsigGrupo, "Proceso: " & Trim(g_oProceso.Anyo) & " / " & Trim(g_oProceso.GMN1Cod) & " / " & g_oProceso.Cod & " Grupo: " & vGrupo(0) & " Prove: " & sdbgSelProve.Columns("PROVECOD").Value
        Else
            RegistrarAccion ACCSelProveDesasigGrupo, "Proceso: " & Trim(g_oProceso.Anyo) & " / " & Trim(g_oProceso.GMN1Cod) & " / " & g_oProceso.Cod & " Grupo: " & vGrupo(0) & " Prove: " & sdbgSelProve.Columns("PROVECOD").Value
        End If
        'Tengo que eliminar el dato Adjudicado y Consumido del proceso
        If bGuardarVisor Then
            teserror = g_oProceso.ActualizarTotalesVisor(Null, Null)
        End If
    Case "BLOQUEAR"
        If Not m_oAsigs.Item(sCod).Grupos.Item(vGrupo(0)) Is Nothing Then
            m_oAsigs.Item(sCod).BloquearGrupo g_oProceso.Anyo, g_oProceso.GMN1Cod, g_oProceso.Cod, _
                                        GridCheckToBoolean(sdbgSelProve.Columns(sdbgSelProve.col).Value), vGrupo(1)
        Else
            sdbgSelProve.Columns(sdbgSelProve.col).Value = False
        End If
End Select


sdbgSelProve.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEGrupos", "sdbgSelProve_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub sdbgSelProve_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
Dim vGrupo As Variant
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case UCase(sOrigen)
    Case "BLOQUEAR"
        sCod = sdbgSelProve.Columns("PROVECOD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgSelProve.Columns("PROVECOD").Value))
        For i = 2 To sdbgSelProve.Columns.Count - 1
            vGrupo = Split(Right(sdbgSelProve.Columns(i).Name, Len(sdbgSelProve.Columns(i).Name) - 3), "_")
            If m_oAsigs.Item(sCod).Grupos.Item(vGrupo(0)) Is Nothing Then
                sdbgSelProve.Columns(i).CellStyleSet "Deshabilitado"
            Else
                sdbgSelProve.Columns(i).CellStyleSet "Habilitado"
            End If
        Next i
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEGrupos", "sdbgSelProve_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub



