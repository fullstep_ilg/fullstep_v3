VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPARTipoSolicitAnya 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DA�adir tipo de solicitud"
   ClientHeight    =   2010
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6585
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPARTipoSolicitAnya.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2010
   ScaleWidth      =   6585
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtCod 
      Height          =   285
      Left            =   1280
      MaxLength       =   20
      TabIndex        =   0
      Top             =   200
      Width           =   2550
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3060
      TabIndex        =   3
      Top             =   1620
      Width           =   975
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1860
      TabIndex        =   2
      Top             =   1620
      Width           =   975
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgDen 
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   880
      Width           =   6355
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   3
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2514
      Columns(0).Caption=   "IDI"
      Columns(0).Name =   "IDI"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   14671839
      Columns(1).Width=   8123
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD_IDI"
      Columns(2).Name =   "COD_IDI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   11210
      _ExtentY        =   1085
      _StockProps     =   79
   End
   Begin VB.Label lblCod 
      BackColor       =   &H00808000&
      Caption         =   "DC�digo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   240
      Width           =   855
   End
   Begin VB.Label lblDen 
      BackColor       =   &H00808000&
      Caption         =   "DDenominaci�n:"
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   120
      TabIndex        =   4
      Top             =   640
      Width           =   1200
   End
End
Attribute VB_Name = "frmPARTipoSolicitAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_sIdiCaption As String
Private m_sIdiCaptionModif As String
Private m_sIdiCodigo As String

Public g_oIdiomas As CIdiomas

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES_ANYA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        lblCod.caption = Ador(0).Value & ":" '1 C�digo
        m_sIdiCodigo = Ador(0).Value
        Ador.MoveNext
        sdbgDen.Columns("DEN").caption = Ador(0).Value '2 Denominaci�n
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  '3 &Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '4 &Cancelar
        Ador.MoveNext
        m_sIdiCaption = Ador(0).Value ' 5 A�adir tipo de solicitud
        Ador.MoveNext
        m_sIdiCaptionModif = Ador(0).Value ' 6 Modificar tipo de solicitud
        Ador.MoveNext
        sdbgDen.Columns("IDI").caption = Ador(0).Value  '7 Idioma
        Ador.MoveNext
        lblDen.caption = Ador(0).Value  '8 Denominaciones:

        Ador.Close
    End If
    
    Set Ador = Nothing

End Sub

Private Sub cmdAceptar_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim oIdioma As CIdioma
    Dim oMultiidi As CMultiidiomas
    Dim oTipo As CTipoSolicit
    Dim i As Integer
    Dim vbm As Variant
    
    Dim cadenaidioma As String
    
    'Validaci�n de los datos
    If Trim(txtCod.Text) = "" Then
        oMensajes.NoValido m_sIdiCodigo
        If Me.Visible Then txtCod.SetFocus
        Exit Sub
    End If
    
    'Tiene que introducir la denominaci�n en todos los idiomas:
    sdbgDen.MoveFirst
    For i = 0 To sdbgDen.Rows - 1
        vbm = sdbgDen.AddItemBookmark(i)
        If sdbgDen.Columns("COD_IDI").CellValue(vbm) = gParametrosInstalacion.gIdioma Then
            If sdbgDen.Columns("DEN").CellValue(vbm) = "" Then
                oMensajes.NoValido sdbgDen.Columns("IDI").CellValue(vbm)
                Exit Sub
            Else
                cadenaidioma = sdbgDen.Columns("DEN").CellValue(vbm)
            End If
            Exit For
        End If
    Next i


    Screen.MousePointer = vbHourglass
    
    'Carga las denominaciones en todos los idiomas
    Set oMultiidi = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In g_oIdiomas
        For i = 0 To sdbgDen.Rows - 1
            vbm = sdbgDen.AddItemBookmark(i)
            If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                If sdbgDen.Columns("DEN").CellValue(vbm) <> "" Then
                    oMultiidi.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                Else
                    oMultiidi.Add oIdioma.Cod, Trim(cadenaidioma)
                End If
                Exit For
            End If
        Next i
    Next
            
    Select Case frmPARTipoSolicit.g_Accion
     
        Case ACCTipoSolicitudModificar
            'Modifica las denominaciones de los grupos:
            frmPARTipoSolicit.g_oTipoSeleccionado.Cod = Trim(txtCod.Text)
            Set frmPARTipoSolicit.g_oTipoSeleccionado.Denominaciones = oMultiidi

            Set oIBaseDatos = frmPARTipoSolicit.g_oTipoSeleccionado
            udtTeserror = oIBaseDatos.FinalizarEdicionModificando
            If udtTeserror.NumError = TESnoerror Then
                'Cambia el combo del tipo de solicitud en frmPARTipoSolicit
                frmPARTipoSolicit.AnyaModifTipo
                RegistrarAccion ACCTipoSolicitudModificar, "Id:" & frmPARTipoSolicit.g_oTipoSeleccionado.Id
                Unload Me
            Else
                Screen.MousePointer = vbNormal
                TratarError udtTeserror
                Exit Sub
            End If
        
        Case ACCTipoSolicitudAnyadir
            'A�ade el grupo:
            Set oTipo = oFSGSRaiz.Generar_CTipoSolicit
            oTipo.Cod = Trim(txtCod.Text)
            Set oTipo.Denominaciones = oMultiidi
            
            Set oIBaseDatos = oTipo
            
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos = True Then  'comprueba que no exista un tipo de solicitud con ese c�digo
                Screen.MousePointer = vbNormal
                oMensajes.ExisteTipoSolicitud
                Set oTipo = Nothing
                Set oIBaseDatos = Nothing
                Exit Sub
            End If
        
            udtTeserror = oIBaseDatos.AnyadirABaseDatos
                
            If udtTeserror.NumError = TESnoerror Then
                frmPARTipoSolicit.g_oTipos.Add oTipo.Id, oTipo.Cod, oTipo.Denominaciones, oTipo.FECACT
                frmPARTipoSolicit.AnyaModifTipo oTipo.Id
                RegistrarAccion ACCTipoSolicitudAnyadir, "Id:" & oTipo.Id
            Else
                Screen.MousePointer = vbNormal
                TratarError udtTeserror
                Exit Sub
            End If
            Set oTipo = Nothing
            
    End Select
    
    Set oIBaseDatos = Nothing
    Set oMultiidi = Nothing
    
    Screen.MousePointer = vbNormal
    
    frmPARTipoSolicit.g_Accion = ACCTipoSolicitudCons
    
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()

    CargarRecursos
    
    PonerFieldSeparator Me
    
    CargarGridIdiomas
        
    'Ajusta la pantalla para que no haya que hacer scroll
    sdbgDen.Height = (sdbgDen.Rows + 1.2) * sdbgDen.RowHeight
    cmdAceptar.Top = sdbgDen.Top + sdbgDen.Height + 85
    cmdCancelar.Top = cmdAceptar.Top
    Me.Height = cmdAceptar.Top + cmdAceptar.Height + 550
    
    If frmPARTipoSolicit.g_Accion = ACCTipoSolicitudModificar Then
        Me.caption = m_sIdiCaptionModif
    Else
        Me.caption = m_sIdiCaption
    End If
End Sub

Private Sub CargarGridIdiomas()
Dim oIdioma As CIdioma

    'Carga el c�digo:
    If frmPARTipoSolicit.g_Accion = ACCTipoSolicitudModificar Then
        txtCod.Text = frmPARTipoSolicit.g_oTipoSeleccionado.Cod
    End If
    
    'Carga las denominaciones del grupo de datos generales en todos los idiomas:
    For Each oIdioma In g_oIdiomas
        If frmPARTipoSolicit.g_Accion = ACCTipoSolicitudModificar Then
            sdbgDen.AddItem oIdioma.Den & Chr(m_lSeparador) & frmPARTipoSolicit.g_oTipoSeleccionado.Denominaciones.Item(oIdioma.Cod).Den & Chr(m_lSeparador) & oIdioma.Cod
        Else
            sdbgDen.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
        End If
    Next
    sdbgDen.MoveFirst

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oIdiomas = Nothing
    
    frmPARTipoSolicit.g_Accion = ACCTipoSolicitudCons
End Sub

