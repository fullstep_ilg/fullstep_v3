VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstLISPERPerf 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listados por perfiles"
   ClientHeight    =   2115
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6090
   Icon            =   "frmLstLISPERPerf.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2115
   ScaleWidth      =   6090
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   6090
      TabIndex        =   0
      Top             =   1740
      Width           =   6090
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4725
         TabIndex        =   1
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1695
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   6060
      _ExtentX        =   10689
      _ExtentY        =   2990
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selección"
      TabPicture(0)   =   "frmLstLISPERPerf.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstLISPERPerf.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraOrden"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame3 
         Height          =   1275
         Left            =   90
         TabIndex        =   6
         Top             =   315
         Width           =   5850
         Begin SSDataWidgets_B.SSDBCombo sdbcPerfCod 
            Height          =   285
            Left            =   750
            TabIndex        =   7
            Top             =   525
            Width           =   855
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1508
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPerfDen 
            Height          =   285
            Left            =   1605
            TabIndex        =   8
            Top             =   525
            Width           =   4080
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   7197
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblPerf 
            AutoSize        =   -1  'True
            Caption         =   "Perfil:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   195
            TabIndex        =   9
            Top             =   570
            Width           =   420
         End
      End
      Begin VB.Frame fraOrden 
         Caption         =   "Orden"
         Height          =   1155
         Left            =   -74865
         TabIndex        =   3
         Top             =   435
         Width           =   5775
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominación"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   3090
            TabIndex        =   5
            Top             =   510
            Width           =   1515
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "Código"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   735
            TabIndex        =   4
            Top             =   510
            Value           =   -1  'True
            Width           =   1515
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4080
         Top             =   150
      End
   End
End
Attribute VB_Name = "frmLstLISPERPerf"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de manejo de combos
Private RespetarCombo As Boolean
Private CargarComboDesde As Boolean

Private oPerfiles As CPerfiles
Public oPerfilBaseSeleccionado As CPerfil

'Variables del multilenguaje
Private sIdiTitulo As String
Private sIdiGenerando As String
Private sIdiSeleccionando As String
Private sIdiVisualizando As String
Private srIdiTitulo As String
Private srIdiSeleccion As String
Private srIdiTodos As String
Private srIdiCodigo As String
Private srIdiPerfil As String
Private srIdiPag As String
Private srIdiDe As String

'Si true->Listados por perfiles.Si no Perfiles por listados
Public bListPerf As Boolean


Private Sub cmdObtener_Click()
    Dim oCRSeguridad As CRSeguridad
    Dim pv As Preview
    Dim oReport As Object
    Dim SubListado As CRAXDRT.Report
    Dim RepPath  As String
    Dim oFos As FileSystemObject
    Dim sSeleccion As String
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Set oCRSeguridad = GenerarCRSeguridad
    
    Screen.MousePointer = vbHourglass
    
 
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
           Screen.MousePointer = vbNormal
           oMensajes.RutaDeRPTNoValida
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    If bListPerf Then
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptLisperLisPerf.rpt"
    Else
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptLisperPerfLis.rpt"
    End If
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Exit Sub
    End If
    Set oFos = Nothing
    
    If crs_Connected = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
                                                            
    If sdbcPerfCod.Text <> "" Then
        sSeleccion = srIdiPerfil & " " & sdbcPerfCod.Text
    Else
        sSeleccion = srIdiTodos
    End If
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTITULO")).Text = """" & srIdiTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSELECCION")).Text = """" & srIdiSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtListado")).Text = """" & srIdiCodigo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPERFIL")).Text = """" & srIdiPerfil & """"
    
    
    oCRSeguridad.ListadoLISPERPerf oReport, sdbcPerfCod.Text, opOrdDen.Value, bListPerf

    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sIdiTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
        
    frmESPERA.lblGeneral.caption = sIdiGenerando & " " & pv.caption
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Load()
    Me.Height = 2520
    Me.Width = 6210
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oPerfiles = Nothing
    Set oPerfilBaseSeleccionado = Nothing
End Sub

Private Sub Timer1_Timer()

   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub


Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LST_LISPER_PERF, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        If bListPerf Then
            caption = Ador(0).Value
            Ador.MoveNext
        Else
            Ador.MoveNext
            caption = Ador(0).Value
        End If
        Ador.MoveNext
        
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblPerf.caption = Ador(0).Value
        Ador.MoveNext
        fraOrden.caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext

        If bListPerf Then
            sIdiTitulo = Ador(0).Value
            Ador.MoveNext
        Else
            Ador.MoveNext
            sIdiTitulo = Ador(0).Value
        End If
        
        Ador.MoveNext
        sIdiGenerando = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext
                
        If bListPerf Then
            srIdiTitulo = Ador(0).Value
            Ador.MoveNext
        Else
            Ador.MoveNext
            srIdiTitulo = Ador(0).Value
        End If
        Ador.MoveNext
        srIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        srIdiTodos = Ador(0).Value
        Ador.MoveNext
        srIdiCodigo = Ador(0).Value
        Ador.MoveNext
        srIdiPerfil = Ador(0).Value
        Ador.MoveNext
        srIdiPag = Ador(0).Value
        Ador.MoveNext
        srIdiDe = Ador(0).Value
  
        Ador.Close
    
    End If


   Set Ador = Nothing

End Sub

Private Sub sdbcPerfCod_Change()
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcPerfDen.Text = ""
        RespetarCombo = False
        
        CargarComboDesde = True
        Set oPerfilBaseSeleccionado = Nothing
                
    End If

End Sub
Private Sub sdbcPerfCod_CloseUp()

    Dim i As Integer
    
    If sdbcPerfCod.Value = "..." Then
        sdbcPerfCod.Text = ""
        Exit Sub
    End If
    
    If sdbcPerfCod.Text = "" Then
        CargarComboDesde = False
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcPerfDen.Text = sdbcPerfCod.Columns(1).Text
    sdbcPerfCod.Text = sdbcPerfCod.Columns(0).Text
    RespetarCombo = False
        
    Screen.MousePointer = vbHourglass
    Set oPerfilBaseSeleccionado = oPerfiles.Item(sdbcPerfCod.Columns(0).Text)
    Screen.MousePointer = vbNormal
       
    CargarComboDesde = False
    
End Sub

Private Sub sdbcPerfCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcPerfCod.RemoveAll
    
    Set oPerfiles = Nothing
    
    Screen.MousePointer = vbHourglass
 
    If CargarComboDesde Then
        Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfilesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcPerfCod.Text), , , False)
    Else
        Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfiles(, , , , False)
    End If
        
    Codigos = oGestorSeguridad.DevolverLosCodigosDePerfil(oPerfiles)
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPerfCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesde And Not oGestorSeguridad.EOF Then
        sdbcPerfCod.AddItem "..."
    End If

    sdbcPerfCod.SelStart = 0
    sdbcPerfCod.SelLength = Len(sdbcPerfCod.Text)
    sdbcPerfCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPerfCod_InitColumnProps()

    sdbcPerfCod.DataFieldList = "Column 0"
    sdbcPerfCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcPerfCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPerfCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPerfCod.Rows - 1
            bm = sdbcPerfCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPerfCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPerfCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Public Sub sdbcPerfCod_Validate(Cancel As Boolean)

    Dim oPerfiles As CPerfiles
    Dim bExiste As Boolean
    
    Dim irespuesta As Boolean
    
    Screen.MousePointer = vbHourglass
    Set oPerfiles = oFSGSRaiz.Generar_CPerfiles
    
    If sdbcPerfCod.Text = "" Then
        CargarComboDesde = False
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el perfil
    
    Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfiles(sdbcPerfCod.Text, , True, , False)
    
    bExiste = Not (oPerfiles.Count = 0)
    
    If Not bExiste Then
        sdbcPerfCod.Text = ""
    Else
        RespetarCombo = True
        sdbcPerfCod.Text = oPerfiles.Item(1).Cod
        sdbcPerfDen.Text = oPerfiles.Item(1).Den
        sdbcPerfCod.Columns(0).Text = sdbcPerfCod.Text
        sdbcPerfCod.Columns(1).Text = sdbcPerfDen.Text
        RespetarCombo = False
        
        Set oPerfilBaseSeleccionado = oPerfiles.Item(1)
        CargarComboDesde = False
    End If
    
    Set oPerfiles = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPerfDen_Change()

    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcPerfCod.Text = ""
        RespetarCombo = False
        
        CargarComboDesde = True
        Set oPerfilBaseSeleccionado = Nothing
    
        
    End If
    
End Sub



Private Sub sdbcPerfDen_CloseUp()

    Dim irespuesta As Boolean
    
    If sdbcPerfDen.Value = "..." Then
        sdbcPerfDen.Text = ""
        Exit Sub
    End If
    
    If sdbcPerfDen.Text = "" Then
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcPerfCod.Text = sdbcPerfDen.Columns(1).Text
    sdbcPerfDen.Text = sdbcPerfDen.Columns(0).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set oPerfilBaseSeleccionado = oPerfiles.Item(sdbcPerfDen.Columns(1).Text)
    Screen.MousePointer = vbNormal
    
    '''
    CargarComboDesde = False
        

End Sub



Private Sub sdbcPerfDen_DropDown()
  
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcPerfDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If CargarComboDesde Then
        Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfilesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPerfDen.Text), True, False)
    Else
        Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfiles(, , , True, False)
    End If
    
    Codigos = oGestorSeguridad.DevolverLosCodigosDePerfil(oPerfiles)
        
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPerfDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If CargarComboDesde And Not oGestorSeguridad.EOF Then
        sdbcPerfDen.AddItem "..."
    End If

    sdbcPerfDen.SelStart = 0
    sdbcPerfDen.SelLength = Len(sdbcPerfDen.Text)
    sdbcPerfCod.Refresh
    Screen.MousePointer = vbNormal
  
End Sub

Private Sub sdbcPerfDen_InitColumnProps()

    sdbcPerfDen.DataFieldList = "Column 0"
    sdbcPerfDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcPerfDen_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyDelete Then
        sdbcPerfCod.DroppedDown = False
        sdbcPerfCod.Text = ""
        sdbcPerfCod.RemoveAll
        sdbcPerfDen.DroppedDown = False
        sdbcPerfDen.Text = ""
        sdbcPerfDen.RemoveAll
        Set oPerfilBaseSeleccionado = Nothing
    End If
    
End Sub

Private Sub sdbcPerfDen_Validate(Cancel As Boolean)

    Dim oPerfiles As CPerfiles
    Dim bExiste As Boolean
    
    Set oPerfiles = oFSGSRaiz.Generar_CPerfiles
    
    If sdbcPerfDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el perfil
    Screen.MousePointer = vbHourglass
    Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfiles(, sdbcPerfDen.Text, True, , False)
    
    bExiste = Not (oPerfiles.Count = 0)
    
    If Not bExiste Then
        sdbcPerfDen.Text = ""
    
    Else
        RespetarCombo = True
        sdbcPerfCod.Text = oPerfiles.Item(1).Cod
        sdbcPerfDen.Text = oPerfiles.Item(1).Den
        sdbcPerfDen.Columns(1).Text = sdbcPerfCod.Text
        sdbcPerfDen.Columns(0).Text = sdbcPerfDen.Text
        RespetarCombo = False
        
        Set oPerfilBaseSeleccionado = oPerfiles.Item(1)
        CargarComboDesde = False
        
    End If
    
    Set oPerfiles = Nothing
    Screen.MousePointer = vbNormal

End Sub

