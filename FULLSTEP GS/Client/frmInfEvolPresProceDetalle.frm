VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfEvolPresProceDetalle 
   BackColor       =   &H00808000&
   Caption         =   "Detalle de proceso"
   ClientHeight    =   5295
   ClientLeft      =   705
   ClientTop       =   2610
   ClientWidth     =   10365
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfEvolPresProceDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5295
   ScaleWidth      =   10365
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   2415
      Left            =   90
      TabIndex        =   16
      Top             =   1770
      Width           =   10185
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   17
      stylesets.count =   3
      stylesets(0).Name=   "Red"
      stylesets(0).BackColor=   4744445
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfEvolPresProceDetalle.frx":0CB2
      stylesets(1).Name=   "Blue"
      stylesets(1).BackColor=   16777152
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfEvolPresProceDetalle.frx":0CCE
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfEvolPresProceDetalle.frx":0CEA
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   17
      Columns(0).Width=   2064
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   5221296
      Columns(1).Width=   3200
      Columns(1).Caption=   "Descripcion"
      Columns(1).Name =   "DESCR"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   5221296
      Columns(2).Width=   3200
      Columns(2).Caption=   "PRESUNI"
      Columns(2).Name =   "PRESUNI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "CANTIDAD"
      Columns(3).Name =   "CANTIDAD"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3201
      Columns(4).Caption=   "Presupuesto"
      Columns(4).Name =   "PRES"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16777215
      Columns(5).Width=   3200
      Columns(5).Caption=   "CONS"
      Columns(5).Name =   "CONSUMIDO"
      Columns(5).Alignment=   1
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "Standard"
      Columns(5).FieldLen=   256
      Columns(5).HasBackColor=   -1  'True
      Columns(5).BackColor=   16777215
      Columns(6).Width=   3016
      Columns(6).Caption=   "Adjudicado"
      Columns(6).Name =   "ADJ"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "Standard"
      Columns(6).FieldLen=   256
      Columns(6).HasBackColor=   -1  'True
      Columns(6).BackColor=   16777215
      Columns(7).Width=   3069
      Columns(7).Caption=   "Ahorro"
      Columns(7).Name =   "AHO"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   2
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).NumberFormat=   "Standard"
      Columns(7).FieldLen=   256
      Columns(8).Width=   1852
      Columns(8).Caption=   "%"
      Columns(8).Name =   "PORCEN"
      Columns(8).Alignment=   1
      Columns(8).CaptionAlignment=   2
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).NumberFormat=   "0.0#\%"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ASIG"
      Columns(9).Name =   "ASIG"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "DEST"
      Columns(10).Name=   "DEST"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "UNI"
      Columns(11).Name=   "UNI"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "PAG"
      Columns(12).Name=   "PAG"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "INI"
      Columns(13).Name=   "INI"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "FIN"
      Columns(14).Name=   "FIN"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "ID"
      Columns(15).Name=   "ID"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "ESTADO"
      Columns(16).Name=   "ESTADO"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      _ExtentX        =   17965
      _ExtentY        =   4260
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   2175
      Left            =   90
      OleObjectBlob   =   "frmInfEvolPresProceDetalle.frx":0D06
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   2010
      Visible         =   0   'False
      Width           =   10185
   End
   Begin VB.CommandButton cmdGrid 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   450
      Picture         =   "frmInfEvolPresProceDetalle.frx":2741
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   1470
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.PictureBox picTipoGrafico 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   840
      ScaleHeight     =   315
      ScaleWidth      =   1905
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   1440
      Visible         =   0   'False
      Width           =   1905
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
         Height          =   285
         Left            =   120
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   0
         Width           =   1515
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Row.Count       =   5
         Row(0)          =   "Barras 2D"
         Row(1)          =   "Barras 3D"
         Row(2)          =   "Lineas 2D"
         Row(3)          =   "Lineas 3D"
         Row(4)          =   "Tarta"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "TIPO"
         Columns(0).Name =   "TIPO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2672
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
   End
   Begin VB.CommandButton cmdActualizar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   120
      Picture         =   "frmInfEvolPresProceDetalle.frx":288B
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   1470
      Width           =   315
   End
   Begin VB.CommandButton cmdGrafico 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   450
      Picture         =   "frmInfEvolPresProceDetalle.frx":2916
      Style           =   1  'Graphical
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   1470
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   525
      Left            =   60
      TabIndex        =   7
      Top             =   4330
      Width           =   10290
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   6
      stylesets.count =   2
      stylesets(0).Name=   "Bold"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfEvolPresProceDetalle.frx":2C58
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfEvolPresProceDetalle.frx":2C74
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      MaxSelectedRows =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   -2147483633
      BackColorOdd    =   -2147483633
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Caption=   "Totales"
      Columns(0).Name =   "TOT"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Abierto"
      Columns(1).Name =   "ABIERTO"
      Columns(1).Alignment=   1
      Columns(1).CaptionAlignment=   2
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).NumberFormat=   "Standard"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Consumido"
      Columns(2).Name =   "CONS"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "Adjudicado"
      Columns(3).Name =   "ADJ"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "Ahorrado"
      Columns(4).Name =   "AHO"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "%Ahorro"
      Columns(5).Name =   "PORCEN"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "Standard"
      Columns(5).FieldLen=   256
      _ExtentX        =   18150
      _ExtentY        =   926
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Height          =   1035
      Left            =   5685
      TabIndex        =   5
      Top             =   45
      Width           =   4545
      Begin VB.Label txtResponsable 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   345
         TabIndex        =   14
         Top             =   555
         Width           =   3500
      End
      Begin VB.Label lblResponsable 
         BackStyle       =   0  'Transparent
         Caption         =   "Responsable:"
         ForeColor       =   &H00FFFFFF&
         Height          =   180
         Left            =   360
         TabIndex        =   6
         Top             =   255
         Width           =   1275
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   45
      TabIndex        =   0
      Top             =   45
      Width           =   5445
      Begin VB.Label txtPresentacion 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   4005
         TabIndex        =   13
         Top             =   615
         Width           =   1200
      End
      Begin VB.Label txtLimOfe 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   4005
         TabIndex        =   12
         Top             =   240
         Width           =   1200
      End
      Begin VB.Label txtApertura 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1200
         TabIndex        =   11
         Top             =   615
         Width           =   1200
      End
      Begin VB.Label txtNecesidad 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1200
         TabIndex        =   10
         Top             =   240
         Width           =   1200
      End
      Begin VB.Label lblPresentacion 
         BackStyle       =   0  'Transparent
         Caption         =   "Presentaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   2610
         TabIndex        =   4
         Top             =   660
         Width           =   1200
      End
      Begin VB.Label lblLimOfe 
         BackStyle       =   0  'Transparent
         Caption         =   "L�mite de ofertas:"
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   2610
         TabIndex        =   3
         Top             =   270
         Width           =   1425
      End
      Begin VB.Label lblApertura 
         BackStyle       =   0  'Transparent
         Caption         =   "Apertura:"
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   210
         TabIndex        =   2
         Top             =   660
         Width           =   1020
      End
      Begin VB.Label lblNecesidad 
         BackStyle       =   0  'Transparent
         Caption         =   "Necesidad:"
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   180
         TabIndex        =   1
         Top             =   270
         Width           =   1095
      End
   End
   Begin ComctlLib.TabStrip sstabGeneral 
      Height          =   3200
      Left            =   60
      TabIndex        =   22
      Top             =   1110
      Width           =   10290
      _ExtentX        =   18150
      _ExtentY        =   5636
      _Version        =   327682
      BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
         NumTabs         =   1
         BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   ""
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label lblItemSinAsig 
      BackColor       =   &H00FFFFC0&
      Caption         =   "Items sin asignar al proyecto"
      Height          =   195
      Left            =   6400
      TabIndex        =   9
      Top             =   4900
      Width           =   3900
   End
   Begin VB.Label lblItemAsig 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Items asignados al proyecto"
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   2500
      TabIndex        =   8
      Top             =   4900
      Width           =   3500
   End
End
Attribute VB_Name = "frmInfEvolPresProceDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Origen de la llamda al formulario
Public iConcepto As Integer
Private oDestinos As CDestinos
Private oPagos As CPagos
Private oUnidades As CUnidades
Private oArticulo As CArticulo
Private oAdjudicaciones As CAdjsDeArt
Private oIBaseDatos As IBaseDatos

Private oProcesos As CProcesos

'Proceso seleccionado
Public oProcesoSeleccionado As CProceso

'Variables de proceso
Public EvolAnyo As Integer
Public EvolGmn1 As String
Public EvolProce As Long

'Variables para proyectos
 Public g_sProy1 As String
 Public g_sProy2 As String
 Public g_sProy3 As String
 Public g_sProy4 As String
 
 'Variables para UO
 Public g_sUON1 As String
 Public g_sUON2 As String
 Public g_sUON3 As String
 
 'Variables para partidas
Public g_sParCon1 As String
Public g_sParCon2 As String
Public g_sParCon3 As String
Public g_sParCon4 As String

 Public g_dEquivalencia As Double

'Variables multiidioma
Private sIdiItem As String
Private m_sTodos As String
Public bParCerrado As Boolean
Private sIdiTipoGrafico(5) As String
Private Sub Arrange()
    
    On Error Resume Next

    If Me.Width > 10000 And Me.Height > 5000 Then
        
        sstabGeneral.Width = Me.Width - 195
        sstabGeneral.Height = Me.Height - 2500
                
        'Grid de resultados
        sdbgRes.Width = sstabGeneral.Width - 105
        sdbgRes.Height = sstabGeneral.Height - 785
        
        If sstabGeneral.selectedItem.Index = 1 Then
            sdbgRes.Columns("COD").Width = sdbgRes.Width * 0.08
            sdbgRes.Columns("DESCR").Width = sdbgRes.Width * 0.2465
            sdbgRes.Columns("PRES").Width = sdbgRes.Width * 0.15
            sdbgRes.Columns("CONSUMIDO").Width = sdbgRes.Width * 0.13
            sdbgRes.Columns("ADJ").Width = sdbgRes.Width * 0.13
            sdbgRes.Columns("AHO").Width = sdbgRes.Width * 0.13
            If Me.WindowState = vbMaximized Then
                sdbgRes.Columns("PORCEN").Width = sdbgRes.Width * 0.11
            Else
                sdbgRes.Columns("PORCEN").Width = sdbgRes.Width * 0.1
            End If
        Else
            sdbgRes.Columns("COD").Width = sdbgRes.Width * 0.1
            sdbgRes.Columns("DESCR").Width = sdbgRes.Width * 0.1035
            sdbgRes.Columns("PRESUNI").Width = sdbgRes.Width * 0.09
            sdbgRes.Columns("CANTIDAD").Width = sdbgRes.Width * 0.09
            sdbgRes.Columns("PRES").Width = sdbgRes.Width * 0.12
            sdbgRes.Columns("CONSUMIDO").Width = sdbgRes.Width * 0.13
            sdbgRes.Columns("ADJ").Width = sdbgRes.Width * 0.13
            sdbgRes.Columns("AHO").Width = sdbgRes.Width * 0.12
            If Me.Width > 12000 Then
                sdbgRes.Columns("PORCEN").Width = sdbgRes.Width * 0.095
            Else
                sdbgRes.Columns("PORCEN").Width = sdbgRes.Width * 0.0849
            End If
        End If
        
        'Grid de Totales
        sdbgTotales.Width = sstabGeneral.Width - 20
        sdbgTotales.Left = sstabGeneral.Left
        sdbgTotales.Columns("TOT").Width = sdbgTotales.Width * 0.1
        sdbgTotales.Columns("ABIERTO").Width = sdbgTotales.Width * 0.2
        sdbgTotales.Columns("CONS").Width = sdbgTotales.Width * 0.2
        sdbgTotales.Columns("ADJ").Width = sdbgTotales.Width * 0.2
        sdbgTotales.Columns("AHO").Width = sdbgTotales.Width * 0.15
        sdbgTotales.Columns("PORCEN").Width = sdbgTotales.Width * 0.123
        sdbgTotales.Top = sdbgRes.Top + sdbgRes.Height + 145

        lblItemSinAsig.Top = sdbgTotales.Top + sdbgTotales.Height + 45
        lblItemAsig.Top = lblItemSinAsig.Top
'        lblItemCerrado.Top = lblItemSinAsig.Top
        lblItemSinAsig.Left = sdbgTotales.Left + sdbgTotales.Width - lblItemSinAsig.Width - 20
        lblItemAsig.Left = lblItemSinAsig.Left - lblItemAsig.Width - 100
'        lblItemCerrado.Left = Label1.Left - 2500
    End If
    
    MSChart1.Width = sdbgRes.Width
    MSChart1.Height = sdbgRes.Height - 240
        
End Sub

Private Sub MostrarDetalleItem()

Dim teserror As TipoErrorSummit
Dim oAdj As CAdjDeArt
Dim sCod As String
Dim oItem As CItem

    
    Screen.MousePointer = vbHourglass
    frmItemDetalle.lblItem.caption = sdbgRes.Columns("DESCR").Value
    frmItemDetalle.lblCodDest.caption = sdbgRes.Columns("DEST").Value
    oDestinos.CargarTodosLosDestinos sdbgRes.Columns("DEST").Value, , True, , , , , , , , True
    If oDestinos.Count <> 0 Then
        frmItemDetalle.lblDenDest.caption = oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If
    
    frmItemDetalle.lblCodPag.caption = sdbgRes.Columns("PAG").Value
    oPagos.CargarTodosLosPagos sdbgRes.Columns("PAG").Value, , True
    If oPagos.Count <> 0 Then
        frmItemDetalle.lblDenPag.caption = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If
    
    frmItemDetalle.lblCodUni.caption = sdbgRes.Columns("UNI").Value
    oUnidades.CargarTodasLasUnidades sdbgRes.Columns("UNI").Value, , True
    If oUnidades.Count <> 0 Then
        frmItemDetalle.lblDenUni.caption = oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If
    
    frmItemDetalle.lblFecFin.caption = sdbgRes.Columns("FIN").Value
    frmItemDetalle.lblFecIni.caption = sdbgRes.Columns("INI").Value
        
    sCod = EvolGmn1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(EvolGmn1))
    sCod = EvolAnyo & sCod & EvolProce
       
    oProcesos.CargarTodosLosProcesosDesde 1, 0, 20, 1, EvolAnyo, EvolGmn1, , , , EvolProce
    
    oProcesos.CargarDatosGeneralesProceso EvolAnyo, EvolGmn1, val(EvolProce)
    
    Set oProcesoSeleccionado = Nothing
    
    Set oProcesoSeleccionado = oProcesos.Item(sCod)

'    oProcesoSeleccionado.CargarTodosLosItems OrdItemPorCodArt, CStr(sdbgRes.Columns("COD").Value), CStr(sdbgRes.Columns("DESCR").Value), , True
    oProcesoSeleccionado.CargarTodosLosItems OrdItemPorOrden, CStr(sdbgRes.Columns("COD").Value), , , True
    Set oItem = oProcesoSeleccionado.Items.Item(CStr(sdbgRes.Columns("ID").Value))
    
    Set oIBaseDatos = oItem
    teserror = oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        oMensajes.DatoEliminado sIdiItem
        Set oIBaseDatos = Nothing
        Screen.MousePointer = vbNormal
        Unload Me
        Exit Sub
    End If
    
    oIBaseDatos.CancelarEdicion
    Set frmItemDetalle.g_oItemSeleccionado = oItem
    frmItemDetalle.txtEsp.Text = NullToStr(oItem.esp)
        
    'Realizar la carga de las adjudicaciones anteriores
    'Comprobamos si existen adjudicaciones de summit
    oArticulo.Cod = sdbgRes.Columns("COD").Value

    ' Las actuales
    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, oArticulo, False, True

''    If oAdjudicaciones.Count = 0 Then
''        'Las anteriores al summit
''        oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, oArticulo, True, True
''    End If

    For Each oAdj In oAdjudicaciones
       frmItemDetalle.sdbgAdjudicaciones.AddItem oAdj.ProveCod & Chr(m_lSeparador) & oAdj.Descr & Chr(m_lSeparador) & oAdj.Precio * oProcesoSeleccionado.Cambio & Chr(m_lSeparador) & oAdj.Cantidad & Chr(m_lSeparador) & oAdj.Porcentaje & Chr(m_lSeparador) & oAdj.FechaInicioSuministro & Chr(m_lSeparador) & oAdj.FechaFinSuministro & Chr(m_lSeparador) & oAdj.DestCod & Chr(m_lSeparador) & oAdj.UniCod
    Next
    Screen.MousePointer = vbNormal
    frmItemDetalle.Show 1
    Unload frmItemDetalle
    
    Set oItem = Nothing
    
End Sub

Private Sub CargarRecursos()

    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFEVOLPRESPROCEDETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        caption = Ador(0).Value & " " & EvolAnyo & "/" & EvolGmn1 & "/" & EvolProce
        Ador.MoveNext
        lblNecesidad.caption = Ador(0).Value
        Ador.MoveNext
        lblApertura.caption = Ador(0).Value
        Ador.MoveNext
        lblLimOfe.caption = Ador(0).Value
        Ador.MoveNext
        lblPresentacion.caption = Ador(0).Value
        Ador.MoveNext
        lblResponsable.caption = Ador(0).Value
        Ador.MoveNext
        
        For i = 0 To 4
            sdbgRes.Columns(i).caption = Ador(0).Value
            Ador.MoveNext
        Next
        
        sdbgRes.Columns(5).caption = Ador(0).Value
        sdbgTotales.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgRes.Columns(6).caption = Ador(0).Value
        sdbgTotales.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
                                
        sdbgRes.Columns(7).caption = Ador(0).Value
        sdbgTotales.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgRes.Columns(8).caption = Ador(0).Value
        sdbgTotales.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgTotales.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgTotales.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        
        If iConcepto = 1 Then
            lblItemAsig.caption = Replace(Ador(0).Value, "PROY1", LCase(gParametrosGenerales.gsSingPres1))
            Ador.MoveNext
            lblItemSinAsig.caption = Replace(Ador(0).Value, "PROY1", LCase(gParametrosGenerales.gsSingPres1))
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            sIdiItem = Ador(0).Value
            Ador.MoveNext
'            lblItemCerrado.Caption = Ador(0).Value
            Ador.MoveNext
            m_sTodos = Ador(0).Value

        Else
            Ador.MoveNext
            Ador.MoveNext
            lblItemAsig.caption = Replace(Ador(0).Value, "PROY2", LCase(gParametrosGenerales.gsSingPres2))
            Ador.MoveNext
            lblItemSinAsig.caption = Replace(Ador(0).Value, "PROY2", LCase(gParametrosGenerales.gsSingPres2))
            Ador.MoveNext
            sIdiItem = Ador(0).Value
            Ador.MoveNext
'            lblItemCerrado.Caption = Ador(0).Value
            Ador.MoveNext
            m_sTodos = Ador(0).Value

        End If
        
        Ador.MoveNext
        sstabGeneral.Tabs(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        
        
        Ador.Close
    
    End If

    Set Ador = Nothing

End Sub

Private Sub Form_Load()

    Me.Height = 5670
    Me.Width = 10455
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
    
    PonerFieldSeparator Me
    
    If iConcepto = 1 Then
        txtNecesidad = MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("FECNEC").Value
        txtLimOfe = MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("FECLIMIOFE").Value
        txtApertura = MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("FECAPE").Value
        txtPresentacion = MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("FECPRES").Value
        txtResponsable = MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("COM").Value & " " & MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("NOM").Value & " " & MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("APE").Value
    ElseIf iConcepto = 2 Then
        txtNecesidad = MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("FECNEC").Value
        txtLimOfe = MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("FECLIMIOFE").Value
        txtApertura = MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("FECAPE").Value
        txtPresentacion = MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("FECPRES").Value
        txtResponsable = MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("COM").Value & " " & MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("NOM").Value & " " & MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("APE").Value
    End If
        
    CargarGridGrupos
        
    Set oProcesos = oFSGSRaiz.generar_CProcesos
    
    Set oDestinos = oFSGSRaiz.generar_CDestinos
    
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    
    Set oPagos = oFSGSRaiz.generar_CPagos
    
    Set oAdjudicaciones = oFSGSRaiz.Generar_CAdjsDeArt

    Set oArticulo = oFSGSRaiz.generar_CArticulo
    
'    lblItemCerrado.Visible = False
    lblItemAsig.Visible = False
    lblItemSinAsig.Visible = False

End Sub

Private Sub Form_Resize()
    Arrange
End Sub
Private Sub sdbgRes_DblClick()
    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    If sdbgRes.Columns("ID").Value = "" Then Exit Sub
    
    MostrarDetalleItem
End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
    
   Dim i As Integer
   
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
    
    If sdbgRes.Columns("ASIG").Value = 0 Then
        For i = 0 To 9
            sdbgRes.Columns(i).CellStyleSet "Blue"
        Next
        If picTipoGrafico.Visible = False Then
            sdbgRes.Columns("CONSUMIDO").Value = ""
            sdbgRes.Columns("ADJ").Value = ""
            sdbgRes.Columns("AHO").Value = ""
            sdbgRes.Columns("PORCEN").Value = ""
        End If
    End If
'    If bParCerrado Then
'        If sdbgRes.Columns("ESTADO").Value = 1 Then
'            sdbgRes.Columns(0).CellStyleSet "Grey"
'            sdbgRes.Columns(1).CellStyleSet "Grey"
'        End If
'    End If
    

End Sub

Private Sub sdbgTotales_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
    For i = 0 To 5
            sdbgTotales.Columns(i).CellStyleSet "Bold"
    Next
End Sub

Private Sub CargarGridGrupos()
Dim ADORs As Ador.Recordset
Dim i As Integer

    sdbgRes.RemoveAll
    
    If iConcepto = 1 Then
    
        If g_sProy4 <> "" Then
            Set ADORs = oGestorInformes.InfEvolucionConcepto1Grupos(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, g_sProy2, g_sProy3, g_sProy4, , MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
        Else
            If g_sProy3 <> "" Then
                Set ADORs = oGestorInformes.InfEvolucionConcepto1Grupos(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, g_sProy2, g_sProy3, , , MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
            Else
                If g_sProy2 <> "" Then
                    Set ADORs = oGestorInformes.InfEvolucionConcepto1Grupos(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, g_sProy2, , , , MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                Else
                    If g_sProy1 <> "" Then
                        Set ADORs = oGestorInformes.InfEvolucionConcepto1Grupos(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, , , , , val(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value), val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                    End If
                 End If
            End If
        End If

    Else
    
        If g_sParCon4 <> "" Then
            Set ADORs = oGestorInformes.InfEvolucionConcepto2Grupos(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ANYO").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("GMN1").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("COD").Value, g_sParCon1, g_sParCon2, g_sParCon3, g_sParCon4, , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
        Else
            If g_sParCon3 <> "" Then
                Set ADORs = oGestorInformes.InfEvolucionConcepto2Grupos(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ANYO").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("GMN1").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("COD").Value, g_sParCon1, g_sParCon2, g_sParCon3, , , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
            Else
                If g_sParCon2 <> "" Then
                    Set ADORs = oGestorInformes.InfEvolucionConcepto2Grupos(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ANYO").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("GMN1").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("COD").Value, g_sParCon1, g_sParCon2, , , , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                Else
                    If g_sParCon1 <> "" Then
                        Set ADORs = oGestorInformes.InfEvolucionConcepto2Grupos(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ANYO").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("GMN1").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("COD").Value, g_sParCon1, , , , , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                    End If
                 End If
            End If
        End If
    End If

    If ADORs Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass

    If ADORs.RecordCount = 1 Then
        sstabGeneral.Tabs.Add 2
    ElseIf ADORs.RecordCount > 1 Then
        For i = 1 To ADORs.RecordCount
            sstabGeneral.Tabs.Add i + 1
        Next
        sstabGeneral.Tabs.Add sstabGeneral.Tabs.Count + 1
        sstabGeneral.Tabs(sstabGeneral.Tabs.Count).caption = m_sTodos
    End If

    i = 1
    
    If Not ADORs Is Nothing Then
        
        sdbgRes.Columns("PRES").Visible = True
        sdbgRes.Columns("PRESUNI").Visible = False
        sdbgRes.Columns("CANTIDAD").Visible = False
        
        While Not ADORs.EOF
            sdbgRes.AddItem ADORs("COD").Value & Chr(m_lSeparador) & ADORs("DEN").Value & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & Chr(m_lSeparador) & g_dEquivalencia * ADORs("PRES") & Chr(m_lSeparador) & g_dEquivalencia * ADORs("CONSUMIDO").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("ADJUDICADO").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("AHORRO").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("AHORROPORC").Value
            sstabGeneral.Tabs(i + 1).caption = ADORs("COD") & " - " & ADORs("DEN")
            sstabGeneral.Tabs(i + 1).key = "$" & ADORs("ID").Value
            i = i + 1
            ADORs.MoveNext
        Wend
    End If


    If Not ADORs Is Nothing Then
    
        If iConcepto = 1 Then
            If sdbgRes.Columns("CONSUMIDO").Value = 0 Then
                sdbgTotales.AddItem Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ABIERTO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("CONSUMIDO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ADJUDICADO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("AHORRADO").Value)) & Chr(m_lSeparador) & 0
            Else
                sdbgTotales.AddItem Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ABIERTO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("CONSUMIDO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ADJUDICADO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("AHORRADO").Value)) & Chr(m_lSeparador) & (NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("AHORRADO").Value)) / NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("CONSUMIDO").Value))) * 100
            End If
        ElseIf iConcepto = 2 Then
            If sdbgRes.Columns("CONSUMIDO").Value = 0 Then
                sdbgTotales.AddItem Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ABIERTO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("CONSUMIDO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ADJUDICADO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("AHORRADO").Value)) & Chr(m_lSeparador) & 0
            Else
                sdbgTotales.AddItem Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ABIERTO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("CONSUMIDO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ADJUDICADO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("AHORRADO").Value)) & Chr(m_lSeparador) & (NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("AHORRADO").Value)) / NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("CONSUMIDO").Value))) * 100
            End If
        End If
        
        ADORs.Close
        Set ADORs = Nothing
    Else
        sdbgTotales.AddItem Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0
    End If
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sstabGeneral_Click()
Dim sCodigo As String
Dim lIdGrupo As Long
Dim ADORs As Ador.Recordset

    sdbgRes.RemoveAll
    
    lblItemAsig.Visible = False
    lblItemSinAsig.Visible = False
    
    Arrange

    sCodigo = sstabGeneral.selectedItem.caption
    If sstabGeneral.selectedItem.key <> "" Then
        lIdGrupo = Right(sstabGeneral.selectedItem.key, Len(sstabGeneral.selectedItem.key) - 1)
    End If
    If (UCase(sCodigo) = UCase(oMensajes.CargarTextoMensaje(318))) Then sCodigo = ""

    If sstabGeneral.selectedItem.Index = 1 Then
        
        If iConcepto = 1 Then
        
            If g_sProy4 <> "" Then
                Set ADORs = oGestorInformes.InfEvolucionConcepto1Grupos(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, g_sProy2, g_sProy3, g_sProy4, , MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
            Else
                If g_sProy3 <> "" Then
                    Set ADORs = oGestorInformes.InfEvolucionConcepto1Grupos(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, g_sProy2, g_sProy3, , , MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                Else
                    If g_sProy2 <> "" Then
                        Set ADORs = oGestorInformes.InfEvolucionConcepto1Grupos(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, g_sProy2, , , , MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                    Else
                        If g_sProy1 <> "" Then
                            Set ADORs = oGestorInformes.InfEvolucionConcepto1Grupos(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, , , , , val(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value), val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                        End If
                     End If
                End If
            End If
        
        Else
        
            If g_sParCon4 <> "" Then
                Set ADORs = oGestorInformes.InfEvolucionConcepto2Grupos(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ANYO").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("GMN1").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("COD").Value, g_sParCon1, g_sParCon2, g_sParCon3, g_sParCon4, , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
            Else
                If g_sParCon3 <> "" Then
                    Set ADORs = oGestorInformes.InfEvolucionConcepto2Grupos(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ANYO").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("GMN1").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("COD").Value, g_sParCon1, g_sParCon2, g_sParCon3, , , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                Else
                    If g_sParCon2 <> "" Then
                        Set ADORs = oGestorInformes.InfEvolucionConcepto2Grupos(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ANYO").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("GMN1").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("COD").Value, g_sParCon1, g_sParCon2, , , , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                    Else
                        If g_sParCon1 <> "" Then
                            Set ADORs = oGestorInformes.InfEvolucionConcepto2Grupos(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ANYO").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("GMN1").Value, MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("COD").Value, g_sParCon1, , , , , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3)
                        End If
                     End If
                End If
            End If
        End If
        
        If Not ADORs Is Nothing Then
            sdbgRes.Columns("PRES").Visible = True
            sdbgRes.Columns("PRESUNI").Visible = False
            sdbgRes.Columns("CANTIDAD").Visible = False
            While Not ADORs.EOF
                sdbgRes.AddItem ADORs("COD").Value & Chr(m_lSeparador) & ADORs("DEN").Value & Chr(m_lSeparador) & Chr(m_lSeparador) & Chr(m_lSeparador) & g_dEquivalencia * ADORs("PRES").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("CONSUMIDO").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("ADJUDICADO").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("AHORRO").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("AHORROPORC").Value
                ADORs.MoveNext
            Wend
        End If
        
    ElseIf (UCase(sCodigo) = UCase(oMensajes.CargarTextoMensaje(318))) Then
    
    Else
'        lblItemCerrado.Visible = True
        lblItemAsig.Visible = True
        lblItemSinAsig.Visible = True
        If sCodigo <> "" Then
            sCodigo = Left(sCodigo, InStr(sCodigo, " ") - 1)
            sdbgRes.Columns("PRES").Visible = True
        End If
        If iConcepto = 1 Then
            If g_sProy4 <> "" Then
                Set ADORs = oGestorInformes.InfEvolucionConcepto1Detalle(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, g_sProy2, g_sProy3, g_sProy4, , MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3, lIdGrupo)
            Else
                If g_sProy3 <> "" Then
                    Set ADORs = oGestorInformes.InfEvolucionConcepto1Detalle(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, g_sProy2, g_sProy3, , , MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3, lIdGrupo)
                Else
                    If g_sProy2 <> "" Then
                        Set ADORs = oGestorInformes.InfEvolucionConcepto1Detalle(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, g_sProy2, , , , MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3, lIdGrupo)
                    Else
                        If g_sProy1 <> "" Then
                            Set ADORs = oGestorInformes.InfEvolucionConcepto1Detalle(val(MDI.g_ofrmInfEvolPres1.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres1.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres1.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres1.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres1.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sProy1, , , , , val(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ESTADOID").Value), val(MDI.g_ofrmInfEvolPres1.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres1.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3, lIdGrupo)
                        End If
                     End If
                End If
            End If
        Else
            If g_sParCon4 <> "" Then
                Set ADORs = oGestorInformes.InfEvolucionConcepto2Detalle(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sParCon1, g_sParCon2, g_sParCon3, g_sParCon4, , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3, lIdGrupo)
            Else
                If g_sParCon3 <> "" Then
                    Set ADORs = oGestorInformes.InfEvolucionConcepto2Detalle(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sParCon1, g_sParCon2, g_sParCon3, , , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3, lIdGrupo)
                Else
                    If g_sParCon2 <> "" Then
                        Set ADORs = oGestorInformes.InfEvolucionConcepto2Detalle(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sParCon1, g_sParCon2, , , , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3, lIdGrupo)
                    Else
                        If g_sParCon1 <> "" Then
                            Set ADORs = oGestorInformes.InfEvolucionConcepto2Detalle(val(MDI.g_ofrmInfEvolPres2.sdbcAnyoDesde), MDI.g_ofrmInfEvolPres2.sdbcMesDesde.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesDesde.Bookmark) + 1, val(MDI.g_ofrmInfEvolPres2.sdbcAnyoHasta), MDI.g_ofrmInfEvolPres2.sdbcMesHasta.AddItemRowIndex(MDI.g_ofrmInfEvolPres2.sdbcMesHasta.Bookmark) + 1, EvolAnyo, EvolGmn1, EvolProce, g_sParCon1, , , , , MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ESTADOID").Value, val(MDI.g_ofrmInfEvolPres2.sdbcADesdePres.Text), val(MDI.g_ofrmInfEvolPres2.sdbcAHastaPres.Text), g_sUON1, g_sUON2, g_sUON3, lIdGrupo)
                        End If
                     End If
                End If
            End If
        End If
    
        If Not ADORs Is Nothing Then
            sdbgRes.Columns("PRES").Visible = True
            sdbgRes.Columns("PRESUNI").Visible = True
            sdbgRes.Columns("CANTIDAD").Visible = True
            While Not ADORs.EOF
                sdbgRes.AddItem ADORs("ART").Value & Chr(m_lSeparador) & ADORs("DESCR").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("PREC").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("CANT").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("PRES").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("CONSUMIDO").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("ADJUDICADO").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("AHORRO").Value & Chr(m_lSeparador) & g_dEquivalencia * ADORs("AHORROPORC").Value & Chr(m_lSeparador) & ADORs("ASIG").Value & Chr(m_lSeparador) & ADORs("DEST").Value & Chr(m_lSeparador) & ADORs("UNI").Value & Chr(m_lSeparador) & ADORs("PAG").Value & Chr(m_lSeparador) & ADORs("FECINI").Value & Chr(m_lSeparador) & ADORs("FECFIN").Value & Chr(m_lSeparador) & ADORs("ID").Value & Chr(m_lSeparador) & ADORs("EST").Value
                ADORs.MoveNext
            Wend
        End If
                        
        If Not ADORs Is Nothing Then
            If iConcepto = 1 Then
                If sdbgRes.Columns("CONSUMIDO").Value = 0 Or sdbgRes.Columns("CONSUMIDO").Value = "" Then
                    sdbgTotales.AddItem Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ABIERTO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("CONSUMIDO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ADJUDICADO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("AHORRADO").Value)) & Chr(m_lSeparador) & 0
                Else
                    sdbgTotales.AddItem Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ABIERTO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("CONSUMIDO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("ADJUDICADO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("AHORRADO").Value)) & Chr(m_lSeparador) & (NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("AHORRADO").Value)) / NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres1.sdbgRes.Columns("CONSUMIDO").Value))) * 100
                End If
            ElseIf iConcepto = 2 Then
                If sdbgRes.Columns("CONSUMIDO").Value = 0 Or sdbgRes.Columns("CONSUMIDO").Value = "" Then
                    sdbgTotales.AddItem Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ABIERTO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("CONSUMIDO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ADJUDICADO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("AHORRADO").Value)) & Chr(m_lSeparador) & 0
                Else
                    sdbgTotales.AddItem Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ABIERTO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("CONSUMIDO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("ADJUDICADO").Value)) & Chr(m_lSeparador) & NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("AHORRADO").Value)) & Chr(m_lSeparador) & (NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("AHORRADO").Value)) / NullToDbl0(StrToDblOrNull(MDI.g_ofrmInfEvolPres2.sdbgRes.Columns("CONSUMIDO").Value))) * 100
                End If
            End If
            ADORs.Close
            Set ADORs = Nothing
        Else
            sdbgTotales.AddItem Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & 0
        End If

    End If
    
    sCodigo = ""
      
End Sub
Private Sub cmdGrafico_Click()
    If sdbgRes.Rows = 0 Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    picTipoGrafico.Visible = True
    sdbcTipoGrafico.Value = sIdiTipoGrafico(2)
    MostrarGrafico sdbcTipoGrafico.Value
    cmdGrafico.Visible = False
    cmdGrid.Visible = True
    sdbgRes.Visible = False
    MSChart1.Visible = True
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    MSChart1.Visible = True
    
    If sdbgRes.Columns("AHO").Text = "" Then sdbgRes.Columns("AHO").Text = 0
    If sdbgRes.Columns("ADJ").Text = "" Then sdbgRes.Columns("ADJ").Text = 0
    If sdbgRes.Columns("PRES").Value = "" Then sdbgRes.Columns("PRES").Value = 0
    
    Select Case Tipo
    
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        If sdbgRes.Columns("AHO").Text = "" Then sdbgRes.Columns("AHO").Text = 0
                        If sdbgRes.Columns("ADJ").Text = "" Then sdbgRes.Columns("ADJ").Text = 0
                        If sdbgRes.Columns("PRES").Value = "" Then sdbgRes.Columns("PRES").Value = 0
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Text) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
       
End Sub

Private Sub cmdGrid_Click()

    picTipoGrafico.Visible = False
    cmdGrafico.Visible = True
    cmdGrid.Visible = False
    sdbgRes.Visible = True
    MSChart1.Visible = False
    
End Sub
Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub
