VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstTiemposProveedor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listado de tiempos por proveedor"
   ClientHeight    =   5475
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8400
   Icon            =   "frmLstTiemposProveedor.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   8400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdObtener 
      Caption         =   "cmdObtener"
      Height          =   495
      Left            =   6720
      TabIndex        =   21
      Top             =   4905
      Width           =   1575
   End
   Begin TabDlg.SSTab stabSolicitud 
      Height          =   4725
      Left            =   135
      TabIndex        =   0
      Top             =   75
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   8334
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstTiemposProveedor.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblProveedor"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblIdentificador"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblFecHasta"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblFecDesde"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblCodigo"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "sdbcProveDen"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "sdbcProveCod"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "sdbcDenSolicitud"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "sdbcCodSolicitud"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "txtIdentificador"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "cmdCalFecHasta"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtFecHasta"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "cmdCalFecDesde"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "txtFecDesde"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "chdDetalle"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "fraSeleccion"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "cmdBuscar"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).ControlCount=   17
      Begin VB.CommandButton cmdBuscar 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7440
         Picture         =   "frmLstTiemposProveedor.frx":0166
         Style           =   1  'Graphical
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   2910
         Width           =   315
      End
      Begin VB.Frame fraSeleccion 
         Caption         =   "Clasificaci�n"
         Height          =   1905
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   7845
         Begin VB.CheckBox chkEstado 
            Caption         =   "DEn Curso"
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   4
            Top             =   4560
            Width           =   4500
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCarpetaNiv1 
            Height          =   285
            Left            =   600
            TabIndex        =   5
            Top             =   360
            Width           =   7065
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   13573
            Columns(0).Caption=   "DEN"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "ID"
            Columns(1).Name =   "ID"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   12462
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCarpetaNiv2 
            Height          =   285
            Left            =   600
            TabIndex        =   6
            Top             =   720
            Width           =   7065
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   13573
            Columns(0).Caption=   "DEN"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "ID"
            Columns(1).Name =   "ID"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   12462
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCarpetaNiv3 
            Height          =   285
            Left            =   600
            TabIndex        =   7
            Top             =   1080
            Width           =   7065
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   13573
            Columns(0).Caption=   "DEN"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "ID"
            Columns(1).Name =   "ID"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   12462
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCarpetaNiv4 
            Height          =   285
            Left            =   600
            TabIndex        =   8
            Top             =   1440
            Width           =   7065
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   13573
            Columns(0).Caption=   "DEN"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "ID"
            Columns(1).Name =   "ID"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   12462
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.CheckBox chdDetalle 
         Caption         =   "Mostrar el detalle de todas las solicitudes"
         Height          =   195
         Left            =   705
         TabIndex        =   13
         Top             =   3345
         Width           =   4215
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   1560
         TabIndex        =   17
         Top             =   4230
         Width           =   1670
      End
      Begin VB.CommandButton cmdCalFecDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3270
         Picture         =   "frmLstTiemposProveedor.frx":01F3
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   4230
         Width           =   315
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   5800
         TabIndex        =   19
         Top             =   4230
         Width           =   1590
      End
      Begin VB.CommandButton cmdCalFecHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7440
         Picture         =   "frmLstTiemposProveedor.frx":077D
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   4230
         Width           =   315
      End
      Begin VB.TextBox txtIdentificador 
         Height          =   285
         Left            =   1560
         TabIndex        =   15
         Top             =   3750
         Width           =   1670
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCodSolicitud 
         Height          =   285
         Left            =   1560
         TabIndex        =   9
         Top             =   2520
         Width           =   1695
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8811
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2990
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcDenSolicitud 
         Height          =   285
         Left            =   3360
         TabIndex        =   10
         Top             =   2520
         Width           =   4420
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3201
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8811
         Columns(1).Caption=   "PET"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7796
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
         Height          =   285
         Left            =   1000
         TabIndex        =   11
         Top             =   2910
         Width           =   2235
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominacion"
         Columns(1).Name =   "Denominacion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasForeColor=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   3942
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
         Height          =   285
         Left            =   3360
         TabIndex        =   23
         Top             =   2910
         Width           =   4065
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 1"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 0"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7170
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblCodigo 
         AutoSize        =   -1  'True
         Caption         =   "C�digo de solicitud:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   2570
         Width           =   1380
      End
      Begin VB.Label lblFecDesde 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   4275
         Width           =   840
      End
      Begin VB.Label lblFecHasta 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   4560
         TabIndex        =   16
         Top             =   4275
         Width           =   840
      End
      Begin VB.Label lblIdentificador 
         AutoSize        =   -1  'True
         Caption         =   "Identificador:"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   3795
         Width           =   915
      End
      Begin VB.Label lblProveedor 
         Caption         =   "Proveedor:"
         Height          =   270
         Left            =   90
         TabIndex        =   12
         Top             =   2955
         Width           =   885
      End
   End
End
Attribute VB_Name = "frmLstTiemposProveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_oCarpetasSolicitN1 As CCarpetasSolicitN1
Private bRespetarComboProve As Boolean
Private bCargarComboDesde As Boolean
Private oProves As CProveedores
Public m_sCasoReporte As String
Public m_sNombreReporte As String


Private Sub VisualizarPantalla()

Select Case m_sCasoReporte

Case "frmFiltroTiemposMediosNeg1"
    lblProveedor.Visible = True
    sdbcProveCod.Visible = True
    sdbcProveDen.Visible = True
    chdDetalle.Visible = True
    lblIdentificador.Visible = False
    txtIdentificador.Visible = False
    
    lblCodigo.Top = fraSeleccion.Top + fraSeleccion.Height + 200
    sdbcCodSolicitud.Top = fraSeleccion.Top + fraSeleccion.Height + 150
    sdbcDenSolicitud.Top = fraSeleccion.Top + fraSeleccion.Height + 150
    
    lblProveedor.Top = sdbcCodSolicitud.Top + sdbcCodSolicitud.Height + 200
    sdbcProveCod.Top = sdbcCodSolicitud.Top + sdbcCodSolicitud.Height + 150
    sdbcProveDen.Top = sdbcCodSolicitud.Top + sdbcCodSolicitud.Height + 150
    cmdBuscar.Top = sdbcCodSolicitud.Top + sdbcCodSolicitud.Height + 150
    
    chdDetalle.Top = sdbcProveCod.Top + sdbcProveCod.Height + 150
    
    lblFecDesde.Top = chdDetalle.Top + chdDetalle.Height + 220
    txtFecDesde.Top = chdDetalle.Top + chdDetalle.Height + 170
    cmdCalFecDesde.Top = chdDetalle.Top + chdDetalle.Height + 170
    
    lblFecHasta.Top = chdDetalle.Top + chdDetalle.Height + 220
    txtFecHasta.Top = chdDetalle.Top + chdDetalle.Height + 170
    cmdCalFecHasta.Top = chdDetalle.Top + chdDetalle.Height + 170
    
    stabSolicitud.Height = lblFecHasta.Top + 400
    cmdObtener.Top = stabSolicitud.Top + stabSolicitud.Height + 50
    Me.Height = stabSolicitud.Top + stabSolicitud.Height + 1100
    
Case "frmFiltroTiemposMediosNeg2"
    lblProveedor.Visible = True
    sdbcProveCod.Visible = True
    sdbcProveDen.Visible = True
    chdDetalle.Visible = True
    lblIdentificador.Visible = True
    txtIdentificador.Visible = True
    chdDetalle.Visible = False
    lblFecDesde.Visible = False
    txtFecDesde.Visible = False
    cmdCalFecDesde.Visible = False
    lblFecHasta.Visible = False
    txtFecHasta.Visible = False
    cmdCalFecHasta.Visible = False
    
    lblCodigo.Top = fraSeleccion.Top + fraSeleccion.Height + 150
    sdbcCodSolicitud.Top = fraSeleccion.Top + fraSeleccion.Height + 100
    sdbcDenSolicitud.Top = fraSeleccion.Top + fraSeleccion.Height + 100
    
    lblProveedor.Top = sdbcCodSolicitud.Top + sdbcCodSolicitud.Height + 200
    sdbcProveCod.Top = sdbcCodSolicitud.Top + sdbcCodSolicitud.Height + 150
    sdbcProveDen.Top = sdbcCodSolicitud.Top + sdbcCodSolicitud.Height + 150
    cmdBuscar.Top = sdbcCodSolicitud.Top + sdbcCodSolicitud.Height + 150
  
    lblIdentificador.Top = sdbcProveCod.Top + sdbcProveCod.Height + 200
    txtIdentificador.Top = sdbcProveCod.Top + sdbcProveCod.Height + 150
    
    stabSolicitud.Height = lblIdentificador.Top + 400
    cmdObtener.Top = stabSolicitud.Top + stabSolicitud.Height + 50
    Me.Height = stabSolicitud.Top + stabSolicitud.Height + 1100

End Select

End Sub


Private Sub cmdBuscar_Click()
    frmPROVEBuscar.sOrigen = "TiemposProveedor"
    frmPROVEBuscar.Show 1
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub cmdObtener_Click()

    Dim sFichero As String
    Dim oReport As CRAXDRT.Report
    Dim oFos As FileSystemObject
    Dim pv As Preview
    Dim bParametros As Boolean
    Dim i As Integer
    Dim sSubRpt As CRAXDRT.Report
    Dim intSub As Integer
    Dim bExiste As Boolean
    
    
    If ComprobarCamposObligatorios Then
            
            If crs_Connected = False Then
                Exit Sub
            End If
            
            'Comprueba que la ruta de archivos sea la correcta y que exista el informe
            If gParametrosInstalacion.gsRPTPATH = "" Then
                If gParametrosGenerales.gsRPTPATH = "" Then
                   oMensajes.RutaDeRPTNoValida
                   Set oReport = Nothing
                   Exit Sub
                Else
                    gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
                    g_GuardarParametrosIns = True
                End If
            End If
                           
            
            sFichero = gParametrosInstalacion.gsRPTPATH & "\" & m_sNombreReporte
            Set oFos = New FileSystemObject
            If Not oFos.FileExists(sFichero) Then
                oMensajes.RutaDeRPTNoValida
                Set oReport = Nothing
                Set oFos = Nothing
                Exit Sub
            End If
            Set oFos = Nothing
            
            Screen.MousePointer = vbHourglass
            
            If crs_Connected = False Then
                Set oFos = Nothing
                Exit Sub
            End If
            
            Set oReport = crs_crapp.OpenReport(sFichero, crOpenReportByTempCopy)
            
            ConectarReport oReport, crs_Server, crs_Database, crs_User, crs_Password
             
            'Comprueba si el informe tiene par�metros.En el caso de que no tenga comprobamos
            'si tiene subreports y si �stos a su vez tienen par�metros
            If oReport.ParameterFields.Count = 0 Then
                bParametros = False
            Else
                bParametros = True
            End If
            
            For i = 1 To oReport.FormulaFields.Count
                If oReport.FormulaFields(i).FormulaFieldName = "NUMSUB" Then
                    bExiste = True
                    Exit For
                End If
            Next i
            If bExiste = False Then
                GoTo Imprimir
            End If
            If oReport.FormulaFields(crs_FormulaIndex(oReport, "NUMSUB")).Text > 0 Then
                For i = 1 To oReport.FormulaFields(crs_FormulaIndex(oReport, "NUMSUB")).Text
                    Set sSubRpt = oReport.OpenSubreport("SubRpt" & i)
                    'Se pasa a cada tabla el servidor y base de datos, de cada uno de los subreports
                    ConectarReport sSubRpt, crs_Server, crs_Database, crs_User, crs_Password
                    If sSubRpt.ParameterFields.Count > 0 Then
                        For intSub = 1 To sSubRpt.ParameterFields.Count
                            If Mid(sSubRpt.ParameterFields(intSub).ParameterFieldName, 1, 3) <> "Pm-" Then 'Que no sea un link del subreport
                                bParametros = True
                                GoTo Imprimir
                            End If
                        Next intSub
                    End If
                    Set sSubRpt = Nothing
                Next i
            End If
                
            
Imprimir:
        
        Dim intTipo As Integer
        intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "Solicitud")).ValueType
        
        If sdbcCarpetaNiv1.Value <> "" Then
            oReport.ParameterFields(crs_ParameterIndex(oReport, "Solicitud")).SetCurrentValue CLng(sdbcCarpetaNiv1.Value), intTipo
            If sdbcCarpetaNiv2.Value <> "" Then
                oReport.ParameterFields(crs_ParameterIndex(oReport, "Proceso")).SetCurrentValue CLng(sdbcCarpetaNiv2.Value), 7 ' 7 Numerico
                If sdbcCarpetaNiv3.Value <> "" Then
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Flujo")).SetCurrentValue CLng(sdbcCarpetaNiv3.Value), 7 ' 7 Numerico
                Else
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Flujo")).SetCurrentValue 0, 7 ' 7 Numerico
                End If
            Else
                oReport.ParameterFields(crs_ParameterIndex(oReport, "Proceso")).SetCurrentValue CLng(0), 7 ' 7 Numerico
                oReport.ParameterFields(crs_ParameterIndex(oReport, "Flujo")).SetCurrentValue CLng(0), 7 ' 7 Numerico
            End If
        Else
            oReport.ParameterFields(crs_ParameterIndex(oReport, "Solicitud")).SetCurrentValue CLng(0), 7 ' 7 Numerico
            oReport.ParameterFields(crs_ParameterIndex(oReport, "Proceso")).SetCurrentValue CLng(0), 7 ' 7 Numerico
            oReport.ParameterFields(crs_ParameterIndex(oReport, "Flujo")).SetCurrentValue CLng(0), 7 ' 7 Numerico
        End If
        
        Select Case m_sCasoReporte
            Case "frmFiltroTiemposMediosNeg1"
                intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "Desde")).ValueType
                If txtFecDesde.Text <> "" Then
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Desde")).SetCurrentValue CStr(txtFecDesde), intTipo
                Else
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Desde")).SetCurrentValue "", intTipo
                End If
                
                intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "Hasta")).ValueType
                If txtFecHasta.Text <> "" Then
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Hasta")).SetCurrentValue CStr(txtFecHasta), intTipo
                Else
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Hasta")).SetCurrentValue "", intTipo
                End If
                
                If chdDetalle.Value = vbChecked Then
                    intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "Detalle")).ValueType
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Detalle")).SetCurrentValue "S", intTipo
                Else
                    intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "Detalle")).ValueType
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Detalle")).SetCurrentValue "N", intTipo
                End If
                intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "TipoSolicitud")).ValueType
                oReport.ParameterFields(crs_ParameterIndex(oReport, "TipoSolicitud")).SetCurrentValue sdbcCodSolicitud.Value, intTipo
                
                If sdbcProveCod.Text <> "" Then
                    intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "Proveedor")).ValueType
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Proveedor")).SetCurrentValue sdbcProveCod.Text, intTipo
                Else
                    intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "Proveedor")).ValueType
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "Proveedor")).SetCurrentValue "", intTipo
                End If
                
            Case "frmFiltroTiemposMediosNeg2"
                    intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "TipoSolicitud")).ValueType
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "TipoSolicitud")).SetCurrentValue sdbcCodSolicitud.Value, intTipo
                    
                    intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "NumSolicitud")).ValueType
                    oReport.ParameterFields(crs_ParameterIndex(oReport, "NumSolicitud")).SetCurrentValue CLng(txtIdentificador.Text), intTipo
                    
                    If sdbcProveCod.Value <> "" Then
                        intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "Proveedor")).ValueType
                        oReport.ParameterFields(crs_ParameterIndex(oReport, "Proveedor")).SetCurrentValue sdbcProveCod.Value, intTipo
                    Else
                        intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, "Proveedor")).ValueType
                        oReport.ParameterFields(crs_ParameterIndex(oReport, "Proveedor")).SetCurrentValue "", intTipo
                    End If
        End Select
                        
        Me.Hide
        Set pv = New Preview
        pv.Hide
        pv.caption = Me.caption
        Set pv.g_oReport = oReport
        pv.crViewer.ReportSource = oReport
        pv.crViewer.ViewReport
        pv.crViewer.Visible = True
        DoEvents
        pv.Show
        Set oReport = Nothing
        Screen.MousePointer = vbNormal
        
        Unload Me
            
                
    End If

End Sub


Private Sub CargarRecursos()

    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LST_TIEMPOS_PROVEEDOR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        stabSolicitud.caption = Ador(0).Value
        Ador.MoveNext
        fraSeleccion.caption = Ador(0).Value
        Ador.MoveNext
        lblCodigo.caption = Ador(0).Value
        Ador.MoveNext
        lblProveedor.caption = Ador(0).Value
        Ador.MoveNext
        chdDetalle.caption = Ador(0).Value
        Ador.MoveNext
        
        lblFecDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        
        sdbcCodSolicitud.Columns(0).caption = Ador(0).Value
        sdbcDenSolicitud.Columns(0).caption = Ador(0).Value
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcCodSolicitud.Columns(1).caption = Ador(0).Value
        sdbcDenSolicitud.Columns(1).caption = Ador(0).Value
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        
        Ador.MoveNext '12
        lblIdentificador.caption = Ador(0).Value
        
               
        Ador.Close

    
    End If

   Set Ador = Nothing

End Sub



Private Sub Form_Load()


CargarRecursos

    PonerFieldSeparator Me

Dim inum1 As Integer
Dim inum2 As Integer
Dim inum3 As Integer
Dim inum4 As Integer


inum4 = oFSGSRaiz.Generar_CCarpetasSolicitN1.DevolverNumeroEstructuraCarpetas(4)
inum3 = oFSGSRaiz.Generar_CCarpetasSolicitN1.DevolverNumeroEstructuraCarpetas(3)
inum2 = oFSGSRaiz.Generar_CCarpetasSolicitN1.DevolverNumeroEstructuraCarpetas(2)
inum1 = oFSGSRaiz.Generar_CCarpetasSolicitN1.DevolverNumeroEstructuraCarpetas(1)

Select Case basParametros.gParametrosGenerales.giNEO

    Case 1:
            sdbcCarpetaNiv2.Visible = False
            sdbcCarpetaNiv3.Visible = False
            sdbcCarpetaNiv4.Visible = False
            fraSeleccion.Height = fraSeleccion.Height - 1000
    
    Case 2:
            If inum2 = 0 Then
                sdbcCarpetaNiv2.Visible = False
            End If
    
            fraSeleccion.Height = fraSeleccion.Height - 850
            sdbcCarpetaNiv3.Visible = False
            sdbcCarpetaNiv4.Visible = False
    Case 3:
            If inum3 = 0 Then
                sdbcCarpetaNiv3.Visible = False
            End If
            If inum2 = 0 Then
                sdbcCarpetaNiv2.Visible = False
            End If
            sdbcCarpetaNiv4.Visible = False
            fraSeleccion.Height = fraSeleccion.Height - 400
           
    Case 4:
            If inum4 = 0 Then
                sdbcCarpetaNiv4.Visible = False
            End If
            If inum3 = 0 Then
                sdbcCarpetaNiv3.Visible = False
            End If
            If inum2 = 0 Then
                sdbcCarpetaNiv2.Visible = False
            End If
                
            fraSeleccion.Height = fraSeleccion.Height - 300

End Select

Set m_oCarpetasSolicitN1 = Nothing
Set m_oCarpetasSolicitN1 = oFSGSRaiz.Generar_CCarpetasSolicitN1.DevolverEstructuraCarpetas(True)

VisualizarPantalla


End Sub



Private Sub sdbcCarpetaNiv1_CloseUp()

    sdbcCarpetaNiv2.RemoveAll
    sdbcCarpetaNiv2.Text = ""
    sdbcCarpetaNiv2.Value = ""
    sdbcCarpetaNiv2.Refresh
    
    
    sdbcCarpetaNiv3.RemoveAll
    sdbcCarpetaNiv3.Text = ""
    sdbcCarpetaNiv3.Value = ""
    sdbcCarpetaNiv3.Refresh
    
    sdbcCarpetaNiv4.RemoveAll
    sdbcCarpetaNiv4.Text = ""
    sdbcCarpetaNiv4.Value = ""
    sdbcCarpetaNiv4.Refresh

    sdbcCodSolicitud.RemoveAll
    sdbcCodSolicitud.Text = ""
    sdbcCodSolicitud.Value = ""
    sdbcCodSolicitud.Refresh

    sdbcDenSolicitud.RemoveAll
    sdbcDenSolicitud.Text = ""
    sdbcDenSolicitud.Value = ""
    sdbcDenSolicitud.Refresh

End Sub

Private Sub sdbcCarpetaNiv1_DropDown()
Dim oCSN1 As CCarpetaSolicitN1

    Screen.MousePointer = vbHourglass
    
    sdbcCarpetaNiv1.RemoveAll
        
    For Each oCSN1 In m_oCarpetasSolicitN1
        sdbcCarpetaNiv1.AddItem oCSN1.Den & Chr(m_lSeparador) & oCSN1.Id
        
    Next
        
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcCarpetaNiv1_InitColumnProps()
    sdbcCarpetaNiv1.DataFieldList = "Column 1"
    sdbcCarpetaNiv1.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCarpetaNiv1_Click()
    If Not sdbcCarpetaNiv1.DroppedDown Then
        sdbcCarpetaNiv1 = ""
    End If
End Sub



Private Sub sdbcCarpetaNiv2_CloseUp()

    sdbcCarpetaNiv3.RemoveAll
    sdbcCarpetaNiv3.Text = ""
    sdbcCarpetaNiv3.Value = ""
    sdbcCarpetaNiv3.Refresh
    
    sdbcCarpetaNiv4.RemoveAll
    sdbcCarpetaNiv4.Text = ""
    sdbcCarpetaNiv4.Value = ""
    sdbcCarpetaNiv4.Refresh

    sdbcCodSolicitud.RemoveAll
    sdbcCodSolicitud.Text = ""
    sdbcCodSolicitud.Value = ""
    sdbcCodSolicitud.Refresh

    sdbcDenSolicitud.RemoveAll
    sdbcDenSolicitud.Text = ""
    sdbcDenSolicitud.Value = ""
    sdbcDenSolicitud.Refresh

End Sub

Private Sub sdbcCarpetaNiv2_DropDown()
Dim oCSN1 As CCarpetaSolicitN1
Dim oCSN2 As CCarpetaSolicitN2

    Screen.MousePointer = vbHourglass

    sdbcCarpetaNiv2.RemoveAll

    For Each oCSN1 In m_oCarpetasSolicitN1
        
        If oCSN1.Den = sdbcCarpetaNiv1.Columns(0).Value Then
        
        For Each oCSN2 In oCSN1.CarpetasSolicitN2
            sdbcCarpetaNiv2.AddItem oCSN2.Den & Chr(m_lSeparador) & oCSN2.Id
        Next
        
        End If
        
   Screen.MousePointer = vbNormal

    Next
    
End Sub



Private Sub sdbcCarpetaNiv2_InitColumnProps()

    sdbcCarpetaNiv2.DataFieldList = "Column 1"
    sdbcCarpetaNiv2.DataFieldToDisplay = "Column 0"

End Sub




Private Sub sdbcCarpetaNiv3_CloseUp()

    sdbcCarpetaNiv4.RemoveAll
    sdbcCarpetaNiv4.Text = ""
    sdbcCarpetaNiv4.Value = ""
    sdbcCarpetaNiv4.Refresh

    sdbcCodSolicitud.RemoveAll
    sdbcCodSolicitud.Text = ""
    sdbcCodSolicitud.Value = ""
    sdbcCodSolicitud.Refresh

    sdbcDenSolicitud.RemoveAll
    sdbcDenSolicitud.Text = ""
    sdbcDenSolicitud.Value = ""
    sdbcDenSolicitud.Refresh

End Sub

Private Sub sdbcCarpetaNiv3_DropDown()
Dim oCSN1 As CCarpetaSolicitN1
Dim oCSN2 As CCarpetaSolicitN2
Dim oCSN3 As CCarpetaSolicitN3

    Screen.MousePointer = vbHourglass

    sdbcCarpetaNiv3.RemoveAll

    For Each oCSN1 In m_oCarpetasSolicitN1
        
        If oCSN1.Den = sdbcCarpetaNiv1.Columns(0).Value Then
        
        For Each oCSN2 In oCSN1.CarpetasSolicitN2
        
            If oCSN1.Den = sdbcCarpetaNiv1.Columns(0).Value And oCSN2.Den = sdbcCarpetaNiv2.Columns(0).Value Then
                    
                For Each oCSN3 In oCSN2.CarpetasSolicitN3
                
                    sdbcCarpetaNiv3.AddItem oCSN3.Den & Chr(m_lSeparador) & oCSN3.Id
                Next
                
            End If
        Next
        
        End If
        
   Screen.MousePointer = vbNormal

    Next
    
End Sub



Private Sub sdbcCarpetaNiv3_InitColumnProps()

    sdbcCarpetaNiv3.DataFieldList = "Column 1"
    sdbcCarpetaNiv3.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbcCarpetaNiv4_CloseUp()

    sdbcCodSolicitud.RemoveAll
    sdbcCodSolicitud.Text = ""
    sdbcCodSolicitud.Value = ""
    sdbcCodSolicitud.Refresh

    sdbcDenSolicitud.RemoveAll
    sdbcDenSolicitud.Text = ""
    sdbcDenSolicitud.Value = ""
    sdbcDenSolicitud.Refresh

End Sub

Private Sub sdbcCarpetaNiv4_DropDown()
Dim oCSN1 As CCarpetaSolicitN1
Dim oCSN2 As CCarpetaSolicitN2
Dim oCSN3 As CCarpetaSolicitN3
Dim oCSN4 As CCarpetaSolicitN4

    Screen.MousePointer = vbHourglass

    sdbcCarpetaNiv4.RemoveAll

    For Each oCSN1 In m_oCarpetasSolicitN1
        
        If oCSN1.Den = sdbcCarpetaNiv1.Columns(0).Value Then
        
            For Each oCSN2 In oCSN1.CarpetasSolicitN2
            
                If oCSN1.Den = sdbcCarpetaNiv1.Columns(0).Value And oCSN2.Den = sdbcCarpetaNiv2.Columns(0).Value Then
                        
                    For Each oCSN3 In oCSN2.CarpetasSolicitN3
                    
                        If oCSN1.Den = sdbcCarpetaNiv1.Columns(0).Value And oCSN2.Den = sdbcCarpetaNiv2.Columns(0).Value And oCSN3.Den = sdbcCarpetaNiv3.Columns(0).Value Then
                        
                            For Each oCSN4 In oCSN3.CarpetasSolicitN4
                    
                                sdbcCarpetaNiv4.AddItem oCSN4.Den & Chr(m_lSeparador) & oCSN4.Id
                            Next
                            
                        End If
                    Next
                    
                End If
            Next
        
        End If

    Next
    
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcCarpetaNiv4_InitColumnProps()
    sdbcCarpetaNiv4.DataFieldList = "Column 1"
    sdbcCarpetaNiv4.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbcProveCod_Change()
    If Not bRespetarComboProve Then
    
        bRespetarComboProve = True
        sdbcProveDen.Text = ""
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcProveCod_Click()
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod.Text = ""
        sdbcProveDen.Text = ""
        bCargarComboDesde = False
    End If
End Sub

Private Sub sdbcProveCod_CloseUp()
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    bRespetarComboProve = False
    bCargarComboDesde = False
    DoEvents
End Sub

Private Sub sdbcProveCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    

    If bCargarComboDesde Then
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod.Text)
    Else
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If

    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False

End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)
    
    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
     
    If oProves Is Nothing Then
        Set oProves = oFSGSRaiz.generar_CProveedores
    End If
    
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        bRespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Text = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Text = sdbcProveDen.Text
            
        bRespetarComboProve = False
    End If
    bCargarComboDesde = True
    Set oProves = Nothing

End Sub
 
Private Sub sdbcProveCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveCod.Rows - 1
            bm = sdbcProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
 
Private Sub sdbcCodSolicitud_CloseUp()
    
    sdbcCodSolicitud.Text = sdbcCodSolicitud.Columns(0).Value
    sdbcDenSolicitud.Text = sdbcCodSolicitud.Columns(1).Value

End Sub

Private Sub sdbcCodSolicitud_DropDown()


Dim oSolicitudes As CSolicitudes
Dim Ador As ADODB.Recordset

    Set Ador = Nothing
    
    Screen.MousePointer = vbHourglass
    
    sdbcCodSolicitud.RemoveAll
    sdbcDenSolicitud.RemoveAll
    
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes

    If sdbcCarpetaNiv4.Columns(0).Value <> "" Then
           Set Ador = oSolicitudes.DevolverSolicitudes(4, sdbcCarpetaNiv4.Columns(1).Value)
    Else
        If sdbcCarpetaNiv3.Columns(0).Value <> "" Then
            Set Ador = oSolicitudes.DevolverSolicitudes(3, sdbcCarpetaNiv3.Columns(1).Value)
        Else
            If sdbcCarpetaNiv2.Columns(0).Value <> "" Then
                Set Ador = oSolicitudes.DevolverSolicitudes(2, sdbcCarpetaNiv2.Columns(1).Value)
            Else
                If sdbcCarpetaNiv1.Columns(0).Value <> "" Then
                    Set Ador = oSolicitudes.DevolverSolicitudes(1, sdbcCarpetaNiv1.Columns(1).Value)
                End If
            End If
        End If
    
    End If
    
    If Not Ador Is Nothing Then

       While Not Ador.EOF
            sdbcCodSolicitud.AddItem Ador.Fields("COD").Value & Chr(m_lSeparador) & Ador.Fields("DEN").Value
            sdbcDenSolicitud.AddItem Ador.Fields("COD").Value & Chr(m_lSeparador) & Ador.Fields("DEN").Value
            Ador.MoveNext
        Wend
    
        Ador.Close
        Set Ador = Nothing
    End If
    
    
        Set oSolicitudes = Nothing


    
        Screen.MousePointer = vbNormal


End Sub


Private Function ComprobarCamposObligatorios() As Boolean

ComprobarCamposObligatorios = True

    If m_sCasoReporte <> "frmFiltroTiemposMediosNeg2" Then
        If sdbcCarpetaNiv1.Value = "" Then
            oMensajes.CamposObligatoriosFiltroTiemposMedios 1
            ComprobarCamposObligatorios = False
            Exit Function
        End If
    End If
    Select Case m_sCasoReporte
    
    Case "frmFiltroTiemposMediosNeg1"
            
        If sdbcCodSolicitud.Value = "" Then
            oMensajes.CamposObligatoriosFiltroTiemposMedios 2
            ComprobarCamposObligatorios = False
            Exit Function
        End If
    
    Case "frmFiltroTiemposMediosNeg2"

            If txtIdentificador.Text = "" Or Not IsNumeric(txtIdentificador.Text) Then
                oMensajes.CamposObligatoriosFiltroTiemposMedios 3
                ComprobarCamposObligatorios = False
                Exit Function
            
            End If
    
    End Select

ComprobarCamposObligatorios = True

End Function

Private Sub sdbcDenSolicitud_CloseUp()
    
    sdbcCodSolicitud.Text = sdbcDenSolicitud.Columns(0).Value
    sdbcDenSolicitud.Text = sdbcDenSolicitud.Columns(1).Value


End Sub

Private Sub sdbcDenSolicitud_DropDown()

sdbcCodSolicitud_DropDown

End Sub

Private Sub sdbcDenSolicitud_InitColumnProps()
    sdbcCarpetaNiv4.DataFieldList = "Column 0"
    sdbcCarpetaNiv4.DataFieldToDisplay = "Column 1"

End Sub

Private Sub sdbcProveDen_Change()
    If Not bRespetarComboProve Then
        bRespetarComboProve = True
        sdbcProveCod.Text = ""
        bRespetarComboProve = False

        bCargarComboDesde = True
    End If
End Sub


Private Sub sdbcProveDen_Click()
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod.Text = ""
        sdbcProveDen.Text = ""
        bCargarComboDesde = False
    End If
End Sub

Private Sub sdbcProveDen_CloseUp()
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    bRespetarComboProve = False
    bCargarComboDesde = False
    DoEvents
End Sub

Private Sub sdbcProveDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    sdbcProveCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    If bCargarComboDesde Then
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveDen.Text), bOrdenadosPorDen:=True
    Else
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, bOrdenadosPorDen:=True
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next

    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveDen.Refresh
    Screen.MousePointer = vbNormal
    bCargarComboDesde = False
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub

Private Sub sdbcProveDen_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcProveDen.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProveDen.Text), True, , False
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Den) = UCase(sdbcProveDen.Text))
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        sdbcProveDen.Text = ""
    Else
        bRespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        sdbcProveDen.Text = oProves.Item(1).Den
        sdbcProveDen.Columns(1).Text = sdbcProveCod.Text
        sdbcProveDen.Columns(0).Text = sdbcProveDen.Text
        bRespetarComboProve = False
    End If
    bCargarComboDesde = True
    Set oProves = Nothing
End Sub

Public Sub CargarProveedorConBusqueda()

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
   
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveCod_Validate False

End Sub

