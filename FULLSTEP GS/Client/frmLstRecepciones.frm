VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstRecepciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de recepciones de pedido"
   ClientHeight    =   5445
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11625
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstRecepciones.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5445
   ScaleWidth      =   11625
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   11625
      TabIndex        =   0
      Top             =   5070
      Width           =   11625
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   7725
         TabIndex        =   4
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5025
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11580
      _ExtentX        =   20426
      _ExtentY        =   8864
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstRecepciones.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstRecepciones.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   3900
         Left            =   -74880
         TabIndex        =   7
         Top             =   315
         Width           =   8850
         Begin VB.CheckBox chkitems 
            Caption         =   "incluir items"
            Height          =   225
            Left            =   600
            TabIndex        =   8
            Top             =   600
            Width           =   3015
         End
      End
      Begin VB.Frame Frame3 
         Height          =   4620
         Left            =   120
         TabIndex        =   6
         Top             =   315
         Width           =   11325
         Begin VB.CheckBox chkBloqueo 
            Caption         =   "Drecepciones con facturacion bloqueada"
            Height          =   225
            Left            =   6150
            TabIndex        =   52
            Top             =   3300
            Width           =   4785
         End
         Begin VB.CheckBox chkPedDirec 
            Caption         =   "DPedidos negociados"
            Height          =   225
            Left            =   7440
            TabIndex        =   32
            Top             =   240
            Width           =   3615
         End
         Begin VB.TextBox txtFacDesde 
            Height          =   285
            Left            =   1260
            TabIndex        =   24
            Top             =   2850
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFacDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2460
            Picture         =   "frmLstRecepciones.frx":0CEA
            Style           =   1  'Graphical
            TabIndex        =   23
            Top             =   2850
            Width           =   315
         End
         Begin VB.TextBox txtFacHasta 
            Height          =   285
            Left            =   4110
            TabIndex        =   22
            Top             =   2850
            Width           =   1065
         End
         Begin VB.CommandButton cmdCalFacHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5235
            Picture         =   "frmLstRecepciones.frx":0FFC
            Style           =   1  'Graphical
            TabIndex        =   21
            Top             =   2850
            Width           =   315
         End
         Begin VB.TextBox txtFacNum 
            Height          =   285
            Left            =   4110
            TabIndex        =   20
            Top             =   3300
            Width           =   1635
         End
         Begin VB.CheckBox chkPedAprov 
            Caption         =   "DPedidos de cat�logo"
            Height          =   225
            Left            =   7440
            TabIndex        =   19
            Top             =   600
            Width           =   2685
         End
         Begin VB.TextBox txtalbaran 
            Height          =   285
            Left            =   7200
            MaxLength       =   100
            TabIndex        =   18
            Top             =   1500
            Width           =   1950
         End
         Begin VB.TextBox txtFecDesde 
            Height          =   285
            Left            =   1260
            TabIndex        =   17
            Top             =   2190
            Width           =   1110
         End
         Begin VB.TextBox txtFecHasta 
            Height          =   285
            Left            =   4110
            TabIndex        =   16
            Top             =   2190
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2460
            Picture         =   "frmLstRecepciones.frx":130E
            Style           =   1  'Graphical
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   2190
            Width           =   315
         End
         Begin VB.CommandButton cmdCalFecHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5265
            Picture         =   "frmLstRecepciones.frx":1898
            Style           =   1  'Graphical
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   2190
            Width           =   315
         End
         Begin VB.CommandButton cmdBuscarProve 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5565
            Picture         =   "frmLstRecepciones.frx":1E22
            Style           =   1  'Graphical
            TabIndex        =   13
            Top             =   1170
            Width           =   315
         End
         Begin VB.TextBox txtPedido 
            Height          =   285
            Left            =   2340
            TabIndex        =   12
            Top             =   795
            Width           =   795
         End
         Begin VB.TextBox txtOrdEntrega 
            Height          =   285
            Left            =   3195
            TabIndex        =   11
            Top             =   795
            Width           =   765
         End
         Begin VB.TextBox txtPedidoExt 
            Height          =   285
            Left            =   1260
            MaxLength       =   100
            TabIndex        =   10
            Top             =   1545
            Width           =   1860
         End
         Begin VB.CheckBox chkPedErp 
            Caption         =   "DPedidos de ERP"
            Height          =   225
            Left            =   7440
            TabIndex        =   9
            Top             =   960
            Width           =   1530
         End
         Begin VB.CheckBox ChkSinRecep 
            Caption         =   "DOrdenes sin recepciones"
            Height          =   225
            Left            =   1320
            TabIndex        =   1
            Top             =   3975
            Value           =   1  'Checked
            Width           =   2655
         End
         Begin VB.CheckBox chkParcial 
            Caption         =   "DOrdenes parcialmente recibidas"
            Height          =   225
            Left            =   4080
            TabIndex        =   2
            Top             =   3975
            Width           =   2895
         End
         Begin VB.CheckBox chkTotal 
            Caption         =   "DOrdenes totalmente recibidas"
            Height          =   225
            Left            =   7080
            TabIndex        =   3
            Top             =   3975
            Width           =   2655
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
            Height          =   285
            Left            =   1260
            TabIndex        =   25
            Top             =   795
            Width           =   1020
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmLstRecepciones.frx":1EAF
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HeadStyleSet=   "Normal"
            Columns(0).StyleSet=   "Normal"
            _ExtentX        =   1799
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   2355
            TabIndex        =   26
            Top             =   1170
            Width           =   3180
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmLstRecepciones.frx":1ECB
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4048
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HeadStyleSet=   "Normal"
            Columns(0).StyleSet=   "Normal"
            Columns(1).Width=   1931
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HeadStyleSet=   "Normal"
            Columns(1).StyleSet=   "Normal"
            _ExtentX        =   5609
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   1260
            TabIndex        =   27
            Top             =   1170
            Width           =   1020
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmLstRecepciones.frx":1EE7
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HeadStyleSet=   "Normal"
            Columns(0).StyleSet=   "Normal"
            Columns(1).Width=   4048
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HeadStyleSet=   "Normal"
            Columns(1).StyleSet=   "Normal"
            _ExtentX        =   1799
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEmpresa 
            Height          =   285
            Left            =   1260
            TabIndex        =   28
            Top             =   165
            Width           =   4500
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   2487
            Columns(0).Caption=   "CIF"
            Columns(0).Name =   "CIF"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6747
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "ID"
            Columns(2).Name =   "ID"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   7937
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAprov 
            Height          =   285
            Left            =   7200
            TabIndex        =   29
            Top             =   1875
            Width           =   3855
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   2593
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7382
            Columns(1).Caption=   "Denominacion"
            Columns(1).Name =   "Denominacion"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "Per"
            Columns(2).Name =   "Per"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   6800
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcReceptor 
            Height          =   285
            Left            =   7200
            TabIndex        =   30
            Top             =   2250
            Width           =   3855
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   -2147483630
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6509
            Columns(1).Caption=   "Denominacion"
            Columns(1).Name =   "Denominacion"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "Per"
            Columns(2).Name =   "Per"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   6800
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcFacAnyo 
            Height          =   285
            Left            =   1260
            TabIndex        =   31
            Top             =   3300
            Width           =   1020
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1799
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoPedido 
            Height          =   285
            Left            =   4110
            TabIndex        =   33
            Top             =   1545
            Width           =   1755
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   9
            Columns(0).Width=   1958
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasForeColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   4604
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            Columns(2).Width=   1931
            Columns(2).Caption=   "Concepto"
            Columns(2).Name =   "INV"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   1773
            Columns(3).Caption=   "Almacen"
            Columns(3).Name =   "ALMAC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   1693
            Columns(4).Caption=   "Recepci�n"
            Columns(4).Name =   "RECEP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "INV_"
            Columns(5).Name =   "INV_"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "ALMAC_"
            Columns(6).Name =   "ALMAC_"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "RECEP_"
            Columns(7).Name =   "RECEP_"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "ID"
            Columns(8).Name =   "ID"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            _ExtentX        =   3096
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   -2147483642
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEstadoFactura 
            Height          =   285
            Left            =   7200
            TabIndex        =   34
            Top             =   2850
            Width           =   3855
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6350
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   6800
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   -2147483642
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 0"
         End
         Begin VB.Label lblSelec 
            Caption         =   "Seleccione los criterios de b�squeda"
            Height          =   255
            Left            =   7320
            TabIndex        =   51
            Top             =   600
            Visible         =   0   'False
            Width           =   3825
         End
         Begin VB.Line Line6 
            X1              =   6000
            X2              =   11280
            Y1              =   1320
            Y2              =   1320
         End
         Begin VB.Label lblFacDesde 
            Caption         =   "DFecha factura desde:"
            ForeColor       =   &H00000000&
            Height          =   375
            Left            =   90
            TabIndex        =   50
            Top             =   2790
            Width           =   1080
         End
         Begin VB.Line Line5 
            X1              =   30
            X2              =   11230
            Y1              =   2670
            Y2              =   2670
         End
         Begin VB.Line Line4 
            X1              =   30
            X2              =   5880
            Y1              =   1980
            Y2              =   1980
         End
         Begin VB.Line Line3 
            X1              =   0
            X2              =   5880
            Y1              =   630
            Y2              =   630
         End
         Begin VB.Label lblFacAnyo 
            AutoSize        =   -1  'True
            Caption         =   "DA�o factura:"
            Height          =   195
            Left            =   90
            TabIndex        =   49
            Top             =   3330
            Width           =   1020
         End
         Begin VB.Label lblFacNum 
            AutoSize        =   -1  'True
            Caption         =   "DN�m factura:"
            Height          =   195
            Left            =   3060
            TabIndex        =   48
            Top             =   3330
            Width           =   1050
         End
         Begin VB.Line Line2 
            X1              =   5910
            X2              =   5910
            Y1              =   120
            Y2              =   2670
         End
         Begin VB.Label lblReceptor 
            Caption         =   "DReceptor:"
            Height          =   255
            Left            =   6060
            TabIndex        =   47
            Top             =   2265
            Width           =   975
         End
         Begin VB.Label lblAprov 
            Caption         =   "DAprovisionador:"
            Height          =   255
            Left            =   6030
            TabIndex        =   46
            Top             =   1890
            Width           =   1155
         End
         Begin VB.Label lblEmpresa 
            Caption         =   "DEmpresa:"
            Height          =   255
            Left            =   105
            TabIndex        =   45
            Top             =   195
            Width           =   945
         End
         Begin VB.Label lblAlbaran 
            Caption         =   "DAlbar�n:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   6030
            TabIndex        =   44
            Top             =   1530
            Width           =   1020
         End
         Begin VB.Label lblFecDesde 
            Caption         =   "DFecha entrega desde:"
            ForeColor       =   &H00000000&
            Height          =   375
            Left            =   75
            TabIndex        =   43
            Top             =   2160
            Width           =   1080
         End
         Begin VB.Label lblProve 
            Caption         =   "DProveedor:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   105
            TabIndex        =   42
            Top             =   1200
            Width           =   870
         End
         Begin VB.Label lblAnyo 
            Caption         =   "DPedido:"
            Height          =   225
            Left            =   90
            TabIndex        =   41
            Top             =   810
            Width           =   975
         End
         Begin VB.Label lblNumOrden 
            Caption         =   "D(a�o/cesta/pedido)"
            Height          =   225
            Left            =   4110
            TabIndex        =   40
            Top             =   855
            Width           =   1635
         End
         Begin VB.Label LblPedExt 
            Caption         =   "DN�m.pedido externo:"
            ForeColor       =   &H00000000&
            Height          =   435
            Left            =   90
            TabIndex        =   39
            Top             =   1530
            Width           =   1200
         End
         Begin VB.Label lblEstFac 
            Caption         =   "DEstado:"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   6150
            TabIndex        =   38
            Top             =   2865
            Width           =   960
         End
         Begin VB.Label lblFecHasta 
            Caption         =   "DHasta:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   3060
            TabIndex        =   37
            Top             =   2220
            Width           =   645
         End
         Begin VB.Label lblFacHasta 
            Caption         =   "DHasta:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   3060
            TabIndex        =   36
            Top             =   2910
            Width           =   645
         End
         Begin VB.Label lblTipoPedido 
            Caption         =   "DTipo pedido:"
            ForeColor       =   &H00000000&
            Height          =   435
            Left            =   3210
            TabIndex        =   35
            Top             =   1530
            Width           =   960
         End
         Begin VB.Line Line1 
            X1              =   -240
            X2              =   11280
            Y1              =   3720
            Y2              =   3720
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   8400
         Top             =   150
      End
   End
End
Attribute VB_Name = "frmLstRecepciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables para func. combos
Private bRespetarCombo As Boolean
Private RespetarComboProve As Boolean
Private bCargarComboDesde As Boolean
Private m_bRespetarComboEmpresa As Boolean
Private m_bRespetarComboUsu As Boolean
Private m_bRespetarComboRecep As Boolean
Private m_bRespetarComboTipoPed As Boolean
Private m_bRespetarComboEstFac As Boolean

'Variables para permisos
Private bRMat As Boolean
Private bRUsuAprov As Boolean
Private m_bPedAprov As Boolean
Private m_bPedDirec As Boolean
Private m_bPedErp As Boolean
Private m_bREmpresa As Boolean

'Proveedor seleccionado
Public oProveSeleccionado As CProveedor
'variable para la coleccion de proveedores
Private oProves As CProveedores
Private oFos As FileSystemObject
Private RepPath As String
Private sSeleccion As String

' Variables para textos
Private sFecDesde As String
Private sFecHasta As String
Private sPedido As String
Private sOrden As String
Private sTextosEstados() As String
Private sProve As String
Private sEspera(1 To 3) As String
Private m_sEmpresa As String
Private m_sTipoPedido As String
Private m_sEstadoFactura As String

' variables para textos del listado
Private sTit As String
Private stxtSeleccion As String
Private stxtPag As String '200
Private stxtDe As String
Private stxtFecEmision As String
Private stxtDirecto As String
Private stxtAprovisiona As String
Private stxtRecepIncorrecta As String
Private stxtSi As String
Private stxtNo As String
Private stxtComentario As String
Private stxtArticulo As String
Private stxtCantPedida As String
Private stxtCantRecib As String
Private stxtPrecio As String
Private stxtDest As String
Private stxtUni As String
Private stxtFecEntrega As String
Private stxtRecibidoOK As String
Private stxtFecRecep As String
Private stxtAprovisionador As String
Private stxtReceptor As String
Private txtCentro As String
Private stxtCampo3 As String
Private stxtCampo4 As String
Private stxtNumErp As String
Private stxtUsuario As String
Private stxtRecepPor As String

Private m_oEmpresas As CEmpresas
Private m_oTiposPedido As CTiposPedido
Public m_oTipoPedido As CTipoPedido
Private m_arrConcep(2) As String
Private m_arrRecep(2) As String
Private m_arrAlmac(2) As String

Private m_sBloqueo As String

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde Form_Load</remarks>
''' <revision>LTG 04/04/2012</revision>

Private Sub ConfigurarSeguridad()

    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
    
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECRestMat)) Is Nothing) And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
            bRMat = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECRestUsuAprov)) Is Nothing) Then
            bRUsuAprov = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECSoloAprov)) Is Nothing Then
            m_bPedAprov = True
        End If
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECSoloDirectos)) Is Nothing Then
            m_bPedDirec = True
        End If
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECErp)) Is Nothing Then
            m_bPedErp = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECRestEmpresaUsu)) Is Nothing Then
            m_bREmpresa = True
        End If
            
    End If

    If FSEPConf Then
       chkPedDirec.Value = vbUnchecked
       chkPedAprov.Value = vbChecked
       chkPedDirec.Visible = False
       chkPedAprov.Visible = False
    Else
        If Not gParametrosGenerales.gbPedidosAprov Or Not gParametrosGenerales.gbPedidosDirectos Then
           chkPedAprov.Value = Abs(CInt(gParametrosGenerales.gbPedidosAprov))
           chkPedDirec.Value = Abs(CInt(gParametrosGenerales.gbPedidosDirectos))
           chkPedDirec.Visible = False
           chkPedAprov.Visible = False
        Else
            If m_bPedAprov Then
               chkPedAprov.Value = vbChecked
               chkPedDirec.Value = vbUnchecked
               chkPedDirec.Visible = False
               chkPedAprov.Visible = False
            Else
                If m_bPedDirec Then
                   chkPedAprov.Value = vbUnchecked
                   chkPedDirec.Value = vbChecked
                   chkPedDirec.Visible = False
                   chkPedAprov.Visible = False
                End If
             End If
        End If
    End If
    
    If gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso Then
        OcultarFacturas
    End If

End Sub
''' <summary>
''' Carga los idiomas del formulario.
''' </summary>
''' <remarks>Llamada desde=Form_load; Tiempo m�ximo=0,2</remarks>
  Sub CargarRecursos()

    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTRECEPCIONES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    Me.caption = Ador(0).Value
    sTit = Ador(0).Value
    Ador.MoveNext
    LblPedExt.caption = Ador(0).Value
    Ador.MoveNext
    lblFecDesde.caption = Ador(0).Value & ":"
    sFecDesde = Ador(0).Value
    Ador.MoveNext
    lblFecHasta.caption = Ador(0).Value
    lblFacHasta.caption = Ador(0).Value
    sFecHasta = Ador(0).Value
    Ador.MoveNext
    chkParcial.caption = Ador(0).Value
    Ador.MoveNext
    chkTotal.caption = Ador(0).Value
    Ador.MoveNext
'    lblAnyo.caption = Ador(0).Value & ":"
    'sAnyo = Ador(0).Value
    Ador.MoveNext
'    lblNumPed.caption = ador(0).Value
    sPedido = Ador(0).Value
    Ador.MoveNext
'    lblNumOrden.caption = Ador(0).Value
    lblAnyo.caption = Ador(0).Value
    sOrden = Ador(0).Value
    Ador.MoveNext
    lblProve.caption = Ador(0).Value & ":"
    sProve = Ador(0).Value
    Ador.MoveNext
    sdbcProveCod.Columns(0).caption = Ador(0).Value
    sdbcProveDen.Columns(1).caption = Ador(0).Value
    sdbcAnyo.Columns(0).caption = Ador(0).Value
    sdbcAprov.Columns(0).caption = Ador(0).Value
    sdbcReceptor.Columns(0).caption = Ador(0).Value
    
    Ador.MoveNext
    
    sdbcProveCod.Columns(1).caption = Ador(0).Value
    sdbcProveDen.Columns(0).caption = Ador(0).Value
    sdbcAprov.Columns(1).caption = Ador(0).Value
    sdbcReceptor.Columns(1).caption = Ador(0).Value
    
    Ador.MoveNext
          
    chkPedDirec.caption = Ador(0).Value
    Ador.MoveNext
    
    chkPedAprov.caption = Ador(0).Value
    Ador.MoveNext
        
    lblAlbaran.caption = Ador(0).Value
    Ador.MoveNext
        
    cmdObtener.caption = Ador(0).Value
    Ador.MoveNext
        
    SSTab1.TabCaption(0) = Ador(0).Value
    Ador.MoveNext
    SSTab1.TabCaption(1) = Ador(0).Value
    Ador.MoveNext
    
    sEspera(1) = Ador(0).Value
    Ador.MoveNext
    sEspera(2) = Ador(0).Value
    Ador.MoveNext
    sEspera(3) = Ador(0).Value
    Ador.MoveNext

    ReDim sTextosEstados(3 To 6)
    sTextosEstados(3) = Ador(0).Value
    Ador.MoveNext
    sTextosEstados(4) = Ador(0).Value
    Ador.MoveNext
    sTextosEstados(5) = Ador(0).Value
    Ador.MoveNext
    sTextosEstados(6) = Ador(0).Value
    Ador.MoveNext
    stxtSeleccion = Ador(0).Value
    Ador.MoveNext
    stxtPag = Ador(0).Value
    Ador.MoveNext
    stxtFecEmision = Ador(0).Value
    Ador.MoveNext
    stxtDirecto = Ador(0).Value
    Ador.MoveNext
    stxtAprovisiona = Ador(0).Value
    Ador.MoveNext
    stxtRecepIncorrecta = Ador(0).Value
    Ador.MoveNext
    stxtSi = Ador(0).Value
    Ador.MoveNext
    stxtNo = Ador(0).Value
    Ador.MoveNext
    stxtComentario = Ador(0).Value
    Ador.MoveNext
    stxtArticulo = Ador(0).Value
    Ador.MoveNext
    stxtDest = Ador(0).Value
    Ador.MoveNext
    stxtUni = Ador(0).Value
    Ador.MoveNext
    stxtFecEntrega = Ador(0).Value
    Ador.MoveNext
    stxtPrecio = Ador(0).Value
    Ador.MoveNext
    stxtCantPedida = Ador(0).Value
    Ador.MoveNext
    stxtCantRecib = Ador(0).Value
    Ador.MoveNext
    stxtRecibidoOK = Ador(0).Value
    Ador.MoveNext
    stxtFecRecep = Ador(0).Value
    Ador.MoveNext
    ChkSinRecep.caption = Ador(0).Value
    Ador.MoveNext
    chkitems.caption = Ador(0).Value
    Ador.MoveNext
    stxtAprovisionador = Ador(0).Value & ":"
    lblAprov.caption = Ador(0).Value & ":"
    Ador.MoveNext
    stxtReceptor = Ador(0).Value & ":"
    lblReceptor.caption = Ador(0).Value & ":"
    Ador.MoveNext
    lblEmpresa.caption = Ador(0).Value & ":"
    m_sEmpresa = Ador(0).Value
    Ador.MoveNext
    sdbcEmpresa.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbcEmpresa.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    chkPedErp.caption = Ador(0).Value
    Ador.MoveNext
    lblNumOrden.caption = Ador(0).Value
    Ador.MoveNext
    sdbcTipoPedido.Columns("INV").caption = Ador(0).Value
    Ador.MoveNext
    sdbcTipoPedido.Columns("ALMAC").caption = Ador(0).Value
    Ador.MoveNext
    sdbcTipoPedido.Columns("RECEP").caption = Ador(0).Value
    Ador.MoveNext
    m_arrConcep(0) = Ador(0).Value
    Ador.MoveNext
    m_arrConcep(1) = Ador(0).Value
    Ador.MoveNext
    m_arrConcep(2) = Ador(0).Value
    Ador.MoveNext
    m_arrAlmac(1) = Ador(0).Value
    m_arrRecep(1) = Ador(0).Value
    Ador.MoveNext
    m_arrAlmac(0) = Ador(0).Value
    Ador.MoveNext
    m_arrAlmac(2) = Ador(0).Value
    m_arrRecep(2) = Ador(0).Value
    Ador.MoveNext
    m_arrRecep(0) = Ador(0).Value
    Ador.MoveNext
    lblTipoPedido.caption = Ador(0).Value & ":"
    m_sTipoPedido = Ador(0).Value
    Ador.MoveNext
    lblFacDesde.caption = Ador(0).Value & ":"
    Ador.MoveNext
    lblFacAnyo.caption = Ador(0).Value & ":"
    Ador.MoveNext
    lblFacNum.caption = Ador(0).Value & ":"
    Ador.MoveNext
    lblEstFac.caption = Ador(0).Value & ":"
    m_sEstadoFactura = Ador(0).Value
    Ador.MoveNext
    If Not gParametrosGenerales.gbUsarOrgCompras Then
        stxtCampo3 = Ador(0).Value
        Ador.MoveNext
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            stxtCampo4 = Ador(0).Value
        End If
    Else
        Ador.MoveNext
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            stxtCampo3 = Ador(0).Value
        End If
    End If
    Ador.MoveNext
    txtCentro = Ador(0).Value
    
    Ador.MoveNext
    chkBloqueo.caption = Ador(0).Value
    Ador.MoveNext
    m_sBloqueo = Ador(0).Value
    Ador.MoveNext
    stxtRecepPor = Ador(0).Value
    Ador.MoveNext
    stxtUsuario = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing

End Sub
' Variables para textos
Private Function GenerarTextoSeleccion() As String
Dim sSeleccion As String

    sSeleccion = ""
    
    If val(sdbcAnyo) <> 0 Then
        sSeleccion = lblAnyo.caption & Format(val(sdbcAnyo), "0000")
    End If
        
    If chkPedDirec.Visible Then
        If chkPedDirec.Value = vbChecked Then
            sSeleccion = sSeleccion & "; " & chkPedDirec.caption
        Else
            If chkPedAprov.Value = vbChecked Then
                sSeleccion = sSeleccion & "; " & chkPedAprov.caption
            End If
        End If
    End If
    If ChkSinRecep.Value = vbChecked Then
        sSeleccion = sSeleccion & "; " & ChkSinRecep.caption
    End If
    If chkParcial.Value = vbChecked Then
        sSeleccion = sSeleccion & "; " & chkParcial.caption
    End If
    If chkTotal.Value = vbChecked Then
        sSeleccion = sSeleccion & "; " & chkTotal.caption
    End If
       
    If txtPedido.Text <> "" Then
'        sSeleccion = sSeleccion & "; " & lblNumPed.caption & txtPedido
        sSeleccion = sSeleccion & "; " & txtPedido
    End If
    
    If txtOrdEntrega.Text <> "" Then
        sSeleccion = sSeleccion & "; " & lblNumOrden.caption & txtOrdEntrega
    End If
    
    If txtalbaran.Text <> "" Then
        sSeleccion = sSeleccion & "; " & lblAlbaran.caption & txtalbaran
    End If
    
    If txtFecDesde <> "" Then
        sSeleccion = sSeleccion & "; " & lblFecDesde.caption & txtFecDesde
    End If
    
    If txtFecHasta <> "" Then
        sSeleccion = sSeleccion & "; " & lblFecHasta.caption & txtFecHasta
    End If

    If txtPedidoExt <> "" Then
        sSeleccion = sSeleccion & "; " & LblPedExt.caption & txtPedidoExt
    End If

    If sdbcProveCod <> "" Then
        sSeleccion = sSeleccion & "; " & lblProve.caption & " " & Trim(sdbcProveCod)
    End If
    
    If sdbcEmpresa.Text <> "" Then
        sSeleccion = sSeleccion & "; " & lblEmpresa.caption & " " & Trim(sdbcEmpresa.Text)
    End If
    
    If sdbcAprov.Text <> "" Then
        sSeleccion = sSeleccion & "; " & lblAprov.caption & " " & Trim(sdbcAprov.Text)
    End If
    
    If sdbcReceptor.Text <> "" Then
        sSeleccion = sSeleccion & "; " & lblReceptor.caption & " " & Trim(sdbcReceptor.Text)
    End If
    
    GenerarTextoSeleccion = sSeleccion

End Function

Private Sub ProveedorSeleccionado()
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
End Sub

Public Sub CargarProveedorConBusqueda()

Dim oProves As CProveedores

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    RespetarComboProve = True
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveDen.Text = oProves.Item(1).Den
    RespetarComboProve = False

End Sub


Function BuscarProveedor(CodProve As String) As CProveedor
Dim oProves As CProveedores
Dim oProve As CProveedor

    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado sProve
        Set oProves = Nothing
        Exit Function
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    Set BuscarProveedor = oProve
    Set oProve = Nothing
    Set oProves = Nothing
End Function


Private Sub CargarAnyos()
    Dim iAnyoActual As Integer
    Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        sdbcAnyo.AddItem iInd
        sdbcFacAnyo.AddItem iInd
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub


Private Sub chkParcial_Click()
If chkParcial.Value = False And chkTotal.Value = False Then
    txtalbaran.Enabled = False
Else
    txtalbaran.Enabled = True
    
    End If
If chkParcial.Value = False Then
    chkTotal.Value = False
End If
End Sub

Private Sub chkTotal_Click()
    If chkParcial.Value = vbUnchecked And chkTotal.Value = vbUnchecked Then
        txtalbaran.Enabled = False
    Else
        txtalbaran.Enabled = True
    End If
    If chkTotal.Value = vbChecked Then
        chkParcial.Value = vbChecked
    End If
End Sub

Private Sub cmdBuscarProve_Click()
    frmPROVEBuscar.sOrigen = "frmLstRecepciones"
    frmPROVEBuscar.bREqp = False
    frmPROVEBuscar.bRMat = bRMat
    frmPROVEBuscar.CodGMN1 = ""
    frmPROVEBuscar.Show 1
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFacDesde_Click()
    AbrirFormCalendar Me, txtFacDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFacHasta_Click()
    AbrirFormCalendar Me, txtFacHasta
End Sub

''' <summary>Obtener listado</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub cmdObtener_Click()

Dim FormulaFields(1 To 2, 1 To 2) As String
Dim i As Integer
Dim j As Integer
Dim oReport As CRAXDRT.Report
Dim sin As Boolean
Dim parcial As Boolean
Dim total As Boolean
Dim pv As Preview
Dim indi As Integer
Dim iPedido As Double
Dim iOrdenEntrega As Double
Dim sDenEstado As String
Dim b As Boolean
Dim Adores As ADODB.Recordset
Dim AdoresImpu As ADODB.Recordset
Dim SubListado As Object
Dim lEmpresa As Long
Dim sReceptor As String
Dim sAprov As String
Dim arFiltros(1 To 5) As Variant
Dim sOpt As String



'------------- Validaci�n fichero RPT

    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If

    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPedidoRecep.rpt"

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
'------------- Validaci�n campos ventana

    If Not IsDate(txtFecDesde) And txtFecDesde <> "" Then
        oMensajes.NoValido sFecDesde
        Exit Sub
    End If
    
    If Not IsDate(txtFecHasta) And txtFecHasta <> "" Then
        oMensajes.NoValido sFecHasta
        Exit Sub
    End If

    If (txtFecDesde <> "" And txtFecHasta <> "") Then
        If CDate(txtFecDesde) > CDate(txtFecHasta) Then
            oMensajes.FechaDesdeMayorFechaHasta
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(txtPedido) <> "" Then
        If Not IsNumeric(txtPedido) Then
            oMensajes.NoValido sPedido
            Exit Sub
        End If
    End If

    If Trim(txtOrdEntrega) <> "" Then
        If Not IsNumeric(txtOrdEntrega) Then
            oMensajes.NoValido sOrden
            Exit Sub
        End If
    End If
    
    If m_bREmpresa = True And Trim(sdbcEmpresa.Text) = "" Then
        m_oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        If m_oEmpresas.Count = 0 Then
            oMensajes.UnidadUOSinEmpresa
        Else
            oMensajes.SeleccionarEmpresa
        End If
        If Me.Visible Then sdbcEmpresa.SetFocus
        Exit Sub
    End If
    
    If txtPedido.Text = "" Then
        iPedido = 0
    Else
        iPedido = CDbl(txtPedido)
    End If
    
    If txtOrdEntrega.Text = "" Then
        iOrdenEntrega = 0
    Else
        iOrdenEntrega = CDbl(txtOrdEntrega)
    End If
        
    If ChkSinRecep.Value = vbChecked Then
        sin = True
    Else
        sin = False
    End If
   
    If chkParcial.Value = vbChecked Then
        parcial = True
    Else
        parcial = False
    End If

    If chkTotal.Value = vbChecked Then
        total = True
    Else
        total = False
    End If

    If sdbcEmpresa.Text <> "" Then
        lEmpresa = sdbcEmpresa.Columns("ID").Value
    End If
    
    If bRUsuAprov = True Then
        sReceptor = basOptimizacion.gCodPersonaUsuario
    ElseIf sdbcReceptor.Text <> "" Then
        sReceptor = sdbcReceptor.Columns("COD").Value
    End If
    
    If sdbcAprov.Text <> "" Then
        sAprov = sdbcAprov.Columns("COD").Value
    End If
    
    If txtFacDesde.Text <> "" Then
        arFiltros(1) = txtFacDesde.Text
    End If
    If txtFacHasta.Text <> "" Then
        arFiltros(2) = txtFacHasta.Text
    End If
    If sdbcFacAnyo.Text <> "" Then
        arFiltros(3) = sdbcFacAnyo.Text
    End If
    If txtFacNum.Text <> "" Then
        arFiltros(4) = txtFacNum.Text
    End If
    If sdbcEstadoFactura.Text <> "" Then
        arFiltros(5) = sdbcEstadoFactura.Columns("ID").Value
    End If
    


    sSeleccion = GenerarTextoSeleccion

    For i = 1 To 2 'Inicializo el array de formulas
        For j = 1 To UBound(FormulaFields, 2)
            FormulaFields(i, j) = ""
        Next j
    Next i

    Screen.MousePointer = vbHourglass
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    frmESPERA.lblGeneral.caption = sEspera(1)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    
    'Selecci�n que se lista, formula 1 - @SEL
    FormulaFields(2, 1) = "SEL"
    If sSeleccion = "" Then
        FormulaFields(1, 1) = ""
    Else
        FormulaFields(1, 1) = stxtSeleccion & " " & Trim(sSeleccion)
    End If
    FormulaFields(2, 2) = "ITEMS"
    If chkitems.Value = vbChecked Then
       FormulaFields(1, 2) = "S"
    Else
       FormulaFields(1, 2) = "N"
    End If
    
    'Paso los textos de los estados
    For i = 3 To 6
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst" & i)).Text = """" & sTextosEstados(i) & """"
    Next i
    
    'Etiquetas del report principal
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTit & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & stxtPag & """"
    ' textos orden
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAprovisiona")).Text = """" & stxtAprovisiona & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDirecto")).Text = """" & stxtDirecto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecEmision")).Text = """" & stxtFecEmision & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPedidoExt")).Text = """" & LblPedExt.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRecepIncorrecta")).Text = """" & stxtRecepIncorrecta & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSi")).Text = """" & stxtSi & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNo")).Text = """" & stxtNo & """"
    ' textos recepcion
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecRecep")).Text = """" & stxtFecRecep & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtalbaran")).Text = """" & lblAlbaran.caption & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtcomentario")).Text = """" & stxtComentario & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRecibidoOK")).Text = """" & stxtRecibidoOK & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNumErp")).Text = """" & gParametrosGenerales.gsNomCodPersonalizRecep & ":" & """"
    ' textos items
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtArticulo")).Text = """" & stxtArticulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCantPedida")).Text = """" & stxtCantPedida & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCantRecib")).Text = """" & stxtCantRecib & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDest")).Text = """" & stxtDest & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUNI")).Text = """" & stxtUni & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrecio")).Text = """" & stxtPrecio & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecEntrega")).Text = """" & stxtFecEntrega & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAprovisionador")).Text = """" & stxtAprovisionador & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtReceptor")).Text = """" & stxtReceptor & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTipoPedido")).Text = """" & m_sTipoPedido & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtBloqueoFactura")).Text = """" & m_sBloqueo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAdministrador")).Text = """" & stxtUsuario & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRecepcionadoPor")).Text = """" & stxtRecepPor & """"
    
    If Not gParametrosGenerales.gbUsarOrgCompras Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCAMPO3")).Text = """" & stxtCampo3 & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMOSTRARCAMPO3")).Text = """" & "S" & """"
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCAMPO4")).Text = """" & stxtCampo4 & """"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMOSTRARCAMPO4")).Text = """" & "S" & """"
        Else
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCAMPO4")).Text = """" & "" & """"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMOSTRARCAMPO4")).Text = """" & "N" & """"
        End If
    Else
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCAMPO3")).Text = """" & stxtCampo3 & """"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMOSTRARCAMPO3")).Text = """" & "N" & """"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCAMPO4")).Text = """" & "" & """"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMOSTRARCAMPO4")).Text = """" & "S" & """"
        Else
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMOSTRARCAMPO3")).Text = """" & "N" & """"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMOSTRARCAMPO4")).Text = """" & "N" & """"
        End If
        
    End If
    
    
    For i = 1 To UBound(FormulaFields, 2)
        If FormulaFields(2, i) <> "" Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, FormulaFields(2, i))).Text = """" & FormulaFields(1, i) & """"
        End If
    Next i
    
    'Report
    Set Adores = oGestorInformes.ListadoPEDIDORecep(bRMat, val(sdbcAnyo), chkPedDirec, chkPedAprov, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, iPedido, iOrdenEntrega, txtPedidoExt, Trim(txtFecDesde), Trim(txtFecHasta), Trim(sdbcProveCod), txtalbaran.Text, sin, parcial, total, lEmpresa, sAprov, sReceptor, chkPedErp.Value, arFiltros, sdbcTipoPedido.Columns("ID").Value, chkBloqueo.Value)
    
    If Not Adores Is Nothing Then
        oReport.Database.SetDataSource Adores
    Else
        Screen.MousePointer = vbNormal
        oMensajes.NoHayDatos
        Set oReport = Nothing
        Exit Sub
    End If
    Set Adores = Nothing
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If FormulaFields(1, 2) = "S" Then  'Opci�n de Incluir items activada.
        'Subreport imputaciones
        Set SubListado = oReport.OpenSubreport("rptPedidosImpu")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCentro")).Text = """" & txtCentro & """"
            
        Set AdoresImpu = oGestorInformes.ListadoPEDIDORecepImputaciones(bRMat, val(sdbcAnyo), chkPedDirec, chkPedAprov, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, iPedido, iOrdenEntrega, txtPedidoExt, Trim(txtFecDesde), Trim(txtFecHasta), Trim(sdbcProveCod), txtalbaran.Text, sin, parcial, total, lEmpresa, sAprov, sReceptor, chkPedErp.Value, arFiltros, sdbcTipoPedido.Columns("ID").Value)
        If Not AdoresImpu Is Nothing Then
            sOpt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "OptImpu")).Text = """" & sOpt & """"
            SubListado.Database.SetDataSource AdoresImpu
        Else
            sOpt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "OptImpu")).Text = """" & sOpt & """"
        End If
        Set AdoresImpu = Nothing
    End If


    Set pv = New Preview
    Me.Hide
    pv.Hide
    pv.caption = sTit
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show

    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show


    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

    Set oReport = Nothing
    
End Sub

Private Sub Form_Load()

    CargarRecursos
    
    PonerFieldSeparator Me
    
    ConfigurarSeguridad
    
    'Ocultar filtro de facturas.
    If gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso Then
        Me.Width = 11900
        Me.Height = 5100
        Me.Top = 1900
        Me.Left = 2300
        If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
            Me.Top = 0
            Me.Left = 0
        End If
    Else
        Me.Width = 11900 '9150
        Me.Height = 6100 '5175
        Me.Top = 1900
        Me.Left = 2300
        If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
            Me.Top = 0
            Me.Left = 0
        End If
    End If
    
    CargarAnyos
       
    Set oProves = oFSGSRaiz.generar_CProveedores
    Set m_oEmpresas = oFSGSRaiz.Generar_CEmpresas
    
    'Cargo los tipos de pedido del combo
    Set m_oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    m_oTiposPedido.CargarTodosLosTiposPedidos gParametrosInstalacion.gIdioma


    'Carga la empresa por defecto que tenga asignada el usuario:
    If m_bREmpresa = True Then
        sdbcEmpresa.AllowInput = False
        m_oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        If m_oEmpresas.Count > 0 Then
            sdbcEmpresa.Text = m_oEmpresas.Item(1).Den
            sdbcEmpresa_Validate False
        End If
    End If
    
    
    chkPedDirec.Value = vbUnchecked
    chkPedAprov.Value = vbUnchecked
    chkPedErp.Value = vbUnchecked
    chkPedDirec.Visible = True
    chkPedAprov.Visible = True
    chkPedErp.Visible = True
    lblSelec.Visible = False
    
    If FSEPConf Then
       chkPedDirec.Value = vbUnchecked
       chkPedAprov.Value = vbChecked
       chkPedErp.Value = vbUnchecked
       chkPedDirec.Visible = False
       chkPedAprov.Visible = False
       chkPedErp.Visible = False
    Else
        'Si solo hay un tipo de pedido configurado o el usuario s�lo tiene permiso para uno de los tipos no se ver�n los checks.
        If (gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP) Or (m_bPedDirec And Not m_bPedAprov And Not m_bPedErp) Then
            chkPedAprov.Value = vbUnchecked
            chkPedDirec.Value = vbChecked
            chkPedErp.Value = vbUnchecked
            chkPedDirec.Visible = False
            chkPedAprov.Visible = False
            chkPedErp.Visible = False
            lblSelec.Visible = True
        'Si solo hay un tipo de pedido configurado o el usuario s�lo tiene permiso para uno de los tipos no se ver�n los checks.
        ElseIf (gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosERP) Or (m_bPedAprov And Not m_bPedDirec And Not m_bPedErp) Then
            chkPedAprov.Value = vbChecked
            chkPedDirec.Value = vbUnchecked
            chkPedErp.Value = vbUnchecked
            chkPedDirec.Visible = False
            chkPedAprov.Visible = False
            chkPedErp.Visible = False
            lblSelec.Visible = True
        'Si solo hay un tipo de pedido configurado o el usuario s�lo tiene permiso para uno de los tipos no se ver�n los checks.
        ElseIf (gParametrosGenerales.gbPedidosERP And Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov) Or (m_bPedErp And Not m_bPedDirec And Not m_bPedAprov) Then
            chkPedAprov.Value = vbUnchecked
            chkPedErp.Value = vbChecked
            chkPedDirec.Value = vbUnchecked
            chkPedDirec.Visible = False
            chkPedAprov.Visible = False
            chkPedErp.Visible = False
            lblSelec.Visible = True
        'Si hay alg�n tipo de pedido no configurado o el usuario no tiene permiso para �l, no se ver�.
        ElseIf (gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP) Or (m_bPedDirec And m_bPedAprov And Not m_bPedErp) Then
            chkPedErp.Visible = False
            chkPedDirec.Top = chkPedDirec.Top + 100
            chkPedAprov.Top = chkPedAprov.Top + 150
        'Si hay alg�n tipo de pedido no configurado o el usuario no tiene permiso para �l, no se ver�.
        ElseIf (gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP) Or (m_bPedDirec And Not m_bPedAprov And m_bPedErp) Then
            chkPedAprov.Visible = False
            chkPedDirec.Top = chkPedDirec.Top + 100
            chkPedErp.Top = chkPedAprov.Top + 150
        'Si hay alg�n tipo de pedido no configurado o el usuario no tiene permiso para �l, no se ver�.
        ElseIf (Not gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP) Or (Not m_bPedDirec And m_bPedAprov And m_bPedErp) Then
            chkPedDirec.Visible = False
            chkPedErp.Top = chkPedAprov.Top + 150
            chkPedAprov.Top = chkPedDirec.Top + 100
        End If
    End If
    

    If bRUsuAprov Then
        sdbcReceptor.Visible = False
    End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Set oProves = Nothing
    Set m_oEmpresas = Nothing
    
End Sub

Private Sub sdbcAnyo_Change()

    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcProveCod = ""
        sdbcProveDen = ""
        txtPedido = ""
        txtOrdEntrega = ""
        bRespetarCombo = False
    End If
End Sub

Private Sub sdbcAnyo_Click()
    txtPedido = ""
    txtOrdEntrega = ""

End Sub

Private Sub sdbcEstadoFactura_Change()
    If Not m_bRespetarComboEstFac Then
       bCargarComboDesde = True
    End If
End Sub


''' <summary>
''' Carga el combo de estados de pago con los posibles estados que puede tener un pago de una factura
''' </summary>
''' <remarks>Tiempo m�ximo=0,1seg.</remarks>
Private Sub sdbcEstadoFactura_DropDown()
    Dim oFacturas As CFacturas
    Dim rs As Recordset
    Screen.MousePointer = vbHourglass
    
    sdbcEstadoFactura.RemoveAll
    Set oFacturas = oFSGSRaiz.Generar_CFacturas
    
    Set rs = oFacturas.devolverTipoEstados(gParametrosInstalacion.gIdioma)
   ' sdbcEstadoFactura.AddItem 0 & Chr(m_lSeparador) & ""
    While Not rs.EOF
        sdbcEstadoFactura.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("DEN").Value
        rs.MoveNext
    Wend
    Set oFacturas = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>
''' Almacena el valor del estado de una factura seleccionado
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbcEstadoFactura_CloseUp()
    If sdbcEstadoFactura.Value = "0" Or Trim(sdbcEstadoFactura.Value) = "" Then
        sdbcTipoPedido.Text = ""
        Exit Sub
    End If
    m_bRespetarComboEstFac = True
    sdbcEstadoFactura.Text = sdbcEstadoFactura.Columns(1).Value
    m_bRespetarComboEstFac = True
End Sub

Private Sub sdbcEstadoFactura_InitColumnProps()
    sdbcEstadoFactura.DataFieldList = "Column 0"
    sdbcEstadoFactura.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Valida que el Estado de una factura introducida sea el correcto
''' </summary>
''' <param name="Cancel">Propio del evento</param>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Public Sub sdbcEstadoFactura_Validate(Cancel As Boolean)
Dim oFacturas As CFacturas
Dim rs As Recordset

    If sdbcEstadoFactura.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    Set oFacturas = oFSGSRaiz.Generar_CFacturas
    Set rs = oFacturas.devolverTipoEstados(gParametrosInstalacion.gIdioma)

    If rs.EOF Then
        sdbcEstadoFactura.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sEstadoFactura

    Else
        m_bRespetarComboEstFac = True

        sdbcEstadoFactura.Columns(0).Value = rs.Fields("ID").Value
        sdbcEstadoFactura.Columns(1).Value = rs.Fields("DEN").Value
        
        m_bRespetarComboEstFac = False

        bCargarComboDesde = False
    End If

    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">Valor de la celda</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub sdbcEstadoFactura_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEstadoFactura.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEstadoFactura.Rows - 1
            bm = sdbcEstadoFactura.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEstadoFactura.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcEstadoFactura.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcProveCod_Change()
    
    If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveDen = ""
        Set oProveSeleccionado = Nothing
        RespetarComboProve = False
   End If

End Sub

Private Sub sdbcProveCod_Click()
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
    
End Sub

Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
      
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' cargar el combo de codigos proveedor
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>

Private Sub sdbcProveCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sDesde As String
    Dim oIMAsig As IMaterialAsignado
    Dim oGruposMN1 As CGruposMatNivel1
    
    sdbcProveCod.RemoveAll
     
    Screen.MousePointer = vbHourglass
    
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        
        If sdbcProveCod.Text <> "" Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveCod.Text, , , , , Trim(oGruposMN1.Item(1).Cod)
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , Trim(oGruposMN1.Item(1).Cod)
        End If
    Else
        If sdbcProveCod.Text <> "" Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveCod.Text, , , , , , , , , , , , False
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , False
        End If
    End If
    
    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next
        
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveCod.Rows - 1
            bm = sdbcProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub

''' <summary>
''' Validar el codigo proveedor
''' </summary>
''' <param name="Cancel">cancelar la edicion</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>

Public Sub sdbcProveCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oIMAsig As IMaterialAsignado
    Dim oGruposMN1 As CGruposMatNivel1
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    If sdbcProveCod.Text = sdbcProveCod.Columns(0).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
        RespetarComboProve = False
        Exit Sub
    End If
    
    If sdbcProveCod.Text = sdbcProveDen.Columns(1).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
        RespetarComboProve = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proveedor
    Screen.MousePointer = vbHourglass
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        
        oProves.BuscarTodosLosProveedoresDesde 1, , Trim(sdbcProveCod.Text), , , , , Trim(oGruposMN1.Item(1).Cod)
    Else
        oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Text = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Text = sdbcProveDen.Text
            
        RespetarComboProve = False
        
        Screen.MousePointer = vbHourglass
        ProveedorSeleccionado
        Screen.MousePointer = vbNormal
    End If

End Sub

Private Sub sdbcProveDen_Change()

    If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveCod = ""
        RespetarComboProve = False
        Set oProveSeleccionado = Nothing
    End If
    
End Sub

Private Sub sdbcProveDen_Click()
      If Not sdbcProveDen.DroppedDown Then
        sdbcProveDen = ""
        sdbcProveCod = ""
    End If

End Sub

Private Sub sdbcProveDen_CloseUp()
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' Cargar el combo de denominaciones proveedor
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcProveDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sDesde As String
    Dim oIMAsig As IMaterialAsignado
    Dim oGruposMN1 As CGruposMatNivel1
    
    sdbcProveDen.RemoveAll
        
    Screen.MousePointer = vbHourglass
    
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        
        If sdbcProveDen.Text <> "" Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , sdbcProveDen.Text, , , , Trim(oGruposMN1.Item(1).Cod), , , , , , , True
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , Trim(oGruposMN1.Item(1).Cod), , , , , , , True
        End If
    Else
        If sdbcProveDen.Text = "" Then
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , True
            
        Else
            oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , sdbcProveDen.Text, , , , , , , , , , , True
        End If
    End If
        
    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    'If ((sdbcProceCod.Text = "") And (Not oProves.EOF)) Then
    '    sdbcProveDen.AddItem "..."
    'End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveDen.Refresh

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcTipoPedido_CloseUp()
    bRespetarCombo = True
    m_bRespetarComboTipoPed = True
    sdbcTipoPedido.Value = sdbcTipoPedido.Columns(0).Text & " - " & sdbcTipoPedido.Columns(1).Text
    m_bRespetarComboTipoPed = False
    Set m_oTipoPedido = m_oTiposPedido.Item(sdbcTipoPedido.Columns("ID").Value)
    bRespetarCombo = False
End Sub
Private Sub sdbcTipoPedido_Change()
    If Not m_bRespetarComboTipoPed Then
       bCargarComboDesde = True
       If sdbcTipoPedido.Value = "" Then
            Set m_oTipoPedido = Nothing
       End If
    End If
End Sub

Private Sub sdbcTipoPedido_DropDown()
    Dim i As Integer
    Dim oTipoPedido As CTipoPedido
    Dim iConcep As Integer

    Screen.MousePointer = vbHourglass

    sdbcTipoPedido.RemoveAll
       
    For Each oTipoPedido In m_oTiposPedido
        'iConcep = oTipoPedido.CodConcep
        sdbcTipoPedido.AddItem oTipoPedido.Cod & Chr(m_lSeparador) & oTipoPedido.Den & _
        Chr(m_lSeparador) & m_arrConcep(oTipoPedido.CodConcep) & Chr(m_lSeparador) & m_arrAlmac(oTipoPedido.CodAlmac) & Chr(m_lSeparador) & m_arrRecep(oTipoPedido.CodRecep) & _
        Chr(m_lSeparador) & oTipoPedido.CodConcep & Chr(m_lSeparador) & oTipoPedido.CodAlmac & Chr(m_lSeparador) & oTipoPedido.CodRecep & _
        Chr(m_lSeparador) & oTipoPedido.indice
    Next

    sdbcTipoPedido.SelStart = 0
    sdbcTipoPedido.SelLength = Len(sdbcTipoPedido.Text)
    sdbcTipoPedido.Refresh


    Screen.MousePointer = vbNormal

End Sub


Public Sub sdbcTipoPedido_Validate(Cancel As Boolean)
    If sdbcTipoPedido.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    m_oTiposPedido.CargarTodosLosTiposPedidos gParametrosInstalacion.gIdioma

    If m_oTiposPedido.Count = 0 Then
        sdbcTipoPedido.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sTipoPedido

    Else
        m_bRespetarComboTipoPed = True
        
        sdbcTipoPedido.Columns(0).Value = m_oTiposPedido.Item(1).Cod
        sdbcTipoPedido.Columns(1).Value = m_oTiposPedido.Item(1).Den
        sdbcTipoPedido.Columns(2).Value = m_oTiposPedido.Item(1).CodConcep
        sdbcTipoPedido.Columns(3).Value = m_oTiposPedido.Item(1).CodAlmac
        sdbcTipoPedido.Columns(4).Value = m_oTiposPedido.Item(1).CodRecep
        
        m_bRespetarComboTipoPed = False

        bCargarComboDesde = False
    End If

    Screen.MousePointer = vbNormal
End Sub

Private Sub Timer1_Timer()

   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub

Private Sub sdbcEmpresa_Change()
    If Not m_bRespetarComboEmpresa Then
       bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcEmpresa_CloseUp()
    If sdbcEmpresa.Value = "..." Or sdbcEmpresa.Text = "" Then
        sdbcEmpresa.Text = ""
        Exit Sub
    End If
    
    m_bRespetarComboEmpresa = True
    sdbcEmpresa.Value = sdbcEmpresa.Columns(1).Value
    m_bRespetarComboEmpresa = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcEmpresa_DropDown()
    Dim i As Integer
    Dim oEmpresa As CEmpresa
    
    Screen.MousePointer = vbHourglass
    
    sdbcEmpresa.RemoveAll
    
    If bCargarComboDesde Then
        If m_bREmpresa = True Then
            m_oEmpresas.CargarTodasLasEmpresasDesde sdbcEmpresa.Text, , , , True, True, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        Else
            m_oEmpresas.CargarTodasLasEmpresasDesde sdbcEmpresa.Text, , , , True, True
        End If
    Else
        If m_bREmpresa = True Then
            m_oEmpresas.CargarTodasLasEmpresasDesde , , , , True, , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        Else
            m_oEmpresas.CargarTodasLasEmpresasDesde , , , , True
        End If
    End If
    
    For Each oEmpresa In m_oEmpresas
        sdbcEmpresa.AddItem oEmpresa.nif & Chr(m_lSeparador) & oEmpresa.Den & Chr(m_lSeparador) & oEmpresa.Id
    Next
        
    sdbcEmpresa.SelStart = 0
    sdbcEmpresa.SelLength = Len(sdbcEmpresa.Text)
    sdbcEmpresa.Refresh
    
    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEmpresa_InitColumnProps()
    sdbcEmpresa.DataFieldList = "Column 0"
    sdbcEmpresa.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcEmpresa_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEmpresa.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEmpresa.Rows - 1
            bm = sdbcEmpresa.GetBookmark(i)
            If UCase(sdbcEmpresa.Text) = UCase(Mid(sdbcEmpresa.Columns(0).CellText(bm), 1, Len(sdbcEmpresa.Text))) Then
                sdbcEmpresa.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Public Sub sdbcEmpresa_Validate(Cancel As Boolean)
    If sdbcEmpresa.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    m_oEmpresas.CargarTodasLasEmpresasDesde , Trim(sdbcEmpresa.Text)

    If m_oEmpresas.Count = 0 Then
        sdbcEmpresa.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sEmpresa

    Else
        m_bRespetarComboEmpresa = True

        sdbcEmpresa.Columns(0).Value = m_oEmpresas.Item(1).nif
        sdbcEmpresa.Columns(1).Value = m_oEmpresas.Item(1).Den
        sdbcEmpresa.Columns(2).Value = m_oEmpresas.Item(1).Id
        
        m_bRespetarComboEmpresa = False

        bCargarComboDesde = False
    End If

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcAprov_Change()
    If Not m_bRespetarComboUsu Then
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcAprov_CloseUp()
    Dim i As Integer
    
    If sdbcAprov.Value = "..." Then
        sdbcAprov.Text = ""
        Exit Sub
    End If
    
    If sdbcAprov.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    m_bRespetarComboUsu = True
    sdbcAprov.Text = sdbcAprov.Columns(0).Text
    m_bRespetarComboUsu = False
    
    bCargarComboDesde = False
End Sub


Private Sub sdbcAprov_DropDown()
    Dim oRes As Ador.Recordset
    Dim i As Integer
        
    sdbcAprov.RemoveAll
    
    'Cargamos los codigos
    Screen.MousePointer = vbHourglass

    If bCargarComboDesde Then
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(Trim(sdbcAprov.Text), False, TipoOrdenacionPersonas.OrdPorCod, , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True)
    Else
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(, False, TipoOrdenacionPersonas.OrdPorCod, , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True)
    End If

    While Not oRes.EOF
        sdbcAprov.AddItem oRes("CODPER").Value & Chr(m_lSeparador) & oRes("NOM").Value & " " & oRes("APE").Value & Chr(m_lSeparador) & oRes("USUCOD").Value
        oRes.MoveNext
    Wend

    oRes.Close
    Set oRes = Nothing
    
    sdbcAprov.SelStart = 0
    sdbcAprov.SelLength = Len(sdbcAprov.Text)
    sdbcAprov.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcAprov_InitColumnProps()
    sdbcAprov.DataFieldList = "Column 0"
    sdbcAprov.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcAprov_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcAprov.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcAprov.Rows - 1
            bm = sdbcAprov.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcAprov.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcAprov.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Public Sub sdbcAprov_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oRes As Ador.Recordset
    
    
    If sdbcAprov.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el usuario
    
    Screen.MousePointer = vbHourglass
    Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(sdbcAprov.Columns("COD").Value, True, , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True)
    
    bExiste = Not (oRes.EOF)
    
    If Not bExiste Then
        sdbcAprov.Text = ""
    Else
        m_bRespetarComboUsu = True
        sdbcAprov.Text = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcAprov.Columns(0).Value = oRes("CODPER").Value
        sdbcAprov.Columns(1).Value = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcAprov.Columns(2).Value = oRes("USUCOD").Value
        m_bRespetarComboUsu = False
        bCargarComboDesde = False
    End If
    
    oRes.Close
    Set oRes = Nothing
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcReceptor_Change()
    If Not m_bRespetarComboRecep Then
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcReceptor_CloseUp()
    Dim i As Integer
    
    If sdbcReceptor.Value = "..." Then
        sdbcReceptor.Text = ""
        Exit Sub
    End If
    
    If sdbcReceptor.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    m_bRespetarComboRecep = True
    sdbcReceptor.Text = sdbcReceptor.Columns(0).Text
    m_bRespetarComboRecep = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcReceptor_DropDown()
    Dim oRes As Ador.Recordset

    sdbcReceptor.RemoveAll
    
    'Cargamos los codigos
    Screen.MousePointer = vbHourglass

    If bCargarComboDesde Then
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(Trim(sdbcReceptor.Text), False, TipoOrdenacionPersonas.OrdPorCod, , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , True)
    Else
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(, False, TipoOrdenacionPersonas.OrdPorCod, , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , True)
    End If

    While Not oRes.EOF
        sdbcReceptor.AddItem oRes("CODPER").Value & Chr(m_lSeparador) & oRes("NOM").Value & " " & oRes("APE").Value & Chr(m_lSeparador) & oRes("USUCOD").Value
        oRes.MoveNext
    Wend

    oRes.Close
    Set oRes = Nothing
    
    sdbcReceptor.SelStart = 0
    sdbcReceptor.SelLength = Len(sdbcReceptor.Text)
    sdbcReceptor.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcReceptor_InitColumnProps()
    sdbcReceptor.DataFieldList = "Column 0"
    sdbcReceptor.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcReceptor_PositionList(ByVal Text As String)
     ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcReceptor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcReceptor.Rows - 1
            bm = sdbcReceptor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcReceptor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcReceptor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Public Sub sdbcReceptor_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oRes As Ador.Recordset
    
    
    If sdbcReceptor.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el usuario
    
    Screen.MousePointer = vbHourglass
    Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(sdbcReceptor.Columns("COD").Value, True, , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , True)
    
    bExiste = Not (oRes.EOF)
    
    If Not bExiste Then
        sdbcReceptor.Text = ""
    Else
        m_bRespetarComboRecep = True
        sdbcReceptor.Text = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcReceptor.Columns(0).Value = oRes("CODPER").Value
        sdbcReceptor.Columns(1).Value = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcReceptor.Columns(2).Value = oRes("USUCOD").Value
        m_bRespetarComboRecep = False
        bCargarComboDesde = False
    End If
    
    oRes.Close
    Set oRes = Nothing
    Screen.MousePointer = vbNormal
End Sub


''' <summary>
''' Oculta la parte del filtro de las facturas.
''' </summary>
''' <param name></param>
''' <remarks>Llamada desde: ConfigurarSeguridad; Tiempo m�ximo=0seg.</remarks>

Private Sub OcultarFacturas()

lblFacDesde.Visible = False
txtFacDesde.Visible = False
cmdCalFacDesde.Visible = False
lblFacHasta.Visible = False
txtFacHasta.Visible = False
cmdCalFacHasta.Visible = False
lblEstFac.Visible = False
sdbcEstadoFactura.Visible = False
lblFacAnyo.Visible = False
sdbcFacAnyo.Visible = False
lblFacNum.Visible = False
txtFacNum.Visible = False
Line1.Visible = False
Frame3.Height = Frame3.Height - 1000
Frame1.Height = Frame1.Height - 1000
ChkSinRecep.Top = ChkSinRecep.Top - 1000
chkParcial.Top = chkParcial.Top - 1000
chkTotal.Top = chkTotal.Top - 1000
SSTab1.Height = SSTab1.Height - 1000
Me.Height = Me.Height - 2000

Me.chkBloqueo.Visible = False

End Sub

