VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRORGAnyaDep 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "A�adir departamento"
   ClientHeight    =   3450
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4815
   Icon            =   "frmESTRORGAnyaDep.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3450
   ScaleWidth      =   4815
   ShowInTaskbar   =   0   'False
   Begin VB.OptionButton optNuevo 
      BackColor       =   &H00808000&
      Caption         =   "Nuevo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   180
      TabIndex        =   2
      Top             =   1440
      Width           =   2310
   End
   Begin VB.PictureBox picNuevo 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   1005
      Left            =   270
      ScaleHeight     =   1005
      ScaleWidth      =   4260
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1800
      Width           =   4260
      Begin VB.TextBox txtCod 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1170
         TabIndex        =   3
         Top             =   135
         Width           =   690
      End
      Begin VB.TextBox txtDen 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1170
         MaxLength       =   100
         TabIndex        =   4
         Top             =   540
         Width           =   2985
      End
      Begin VB.Label lblDen 
         BackStyle       =   0  'Transparent
         Caption         =   "Denominaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   60
         TabIndex        =   12
         Top             =   600
         Width           =   1080
      End
      Begin VB.Label lblCod 
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   60
         TabIndex        =   11
         Top             =   180
         Width           =   1035
      End
   End
   Begin VB.PictureBox picExistente 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   780
      Left            =   225
      ScaleHeight     =   780
      ScaleWidth      =   4290
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   495
      Width           =   4290
      Begin SSDataWidgets_B.SSDBCombo sdbcDepCod 
         Height          =   285
         Left            =   90
         TabIndex        =   0
         Top             =   225
         Width           =   1035
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcDepDen 
         Height          =   285
         Left            =   1125
         TabIndex        =   1
         Top             =   225
         Width           =   3090
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 1"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 0"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5450
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   540
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   3060
      Width           =   3825
      Begin VB.CommandButton cmdAceptar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   465
         TabIndex        =   5
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdCancelar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1755
         TabIndex        =   6
         Top             =   0
         Width           =   1095
      End
   End
   Begin VB.OptionButton optExistente 
      BackColor       =   &H00808000&
      Caption         =   "Existente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   180
      TabIndex        =   7
      Top             =   180
      Value           =   -1  'True
      Width           =   2310
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H8000000E&
      Height          =   1185
      Left            =   165
      Top             =   1725
      Width           =   4470
   End
   Begin VB.Shape Shape2 
      BorderColor     =   &H8000000E&
      Height          =   870
      Left            =   180
      Top             =   450
      Width           =   4455
   End
End
Attribute VB_Name = "frmESTRORGAnyaDep"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CargarComboDesde As Boolean
Private RespetarCombo As Boolean

Private oDeps As CDepartamentos

'HBA
Private sIdiDept As String
Private sIdiCod As String
Private sIdiDen As String


Private Sub sdbcDepCod_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcDepDen.Text = ""
        RespetarCombo = False
        
        CargarComboDesde = True
        
    End If

End Sub



Private Sub sdbcDepCod_CloseUp()

    If sdbcDepCod.Value = "..." Then
        sdbcDepCod.Text = ""
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcDepDen.Text = sdbcDepCod.Columns(1).Text
    sdbcDepCod.Text = sdbcDepCod.Columns(0).Text
    RespetarCombo = False
        
    CargarComboDesde = False
      
End Sub

Private Sub sdbcDepCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer


    'If sdbcDepCod.Rows <> 0 Then Exit Sub
    sdbcDepCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If CargarComboDesde Then
        oDeps.CargarTodosLosdepartamentosDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcDepCod.Text), , , False
    Else
        oDeps.CargarTodosLosDepartamentos , , , , False
    End If
    
    Codigos = oDeps.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcDepCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next

    If CargarComboDesde And Not oDeps.EOF Then
        sdbcDepCod.AddItem "..."
    End If

    sdbcDepCod.SelStart = 0
    sdbcDepCod.SelLength = Len(sdbcDepCod.Text)
    sdbcDepCod.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcDepCod_InitColumnProps()
    sdbcDepCod.DataFieldList = "Column 0"
    sdbcDepCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcDepCod_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyDelete Then
'        sdbcDepCod.DroppedDown = False
'        sdbcDepCod.Text = ""
'        sdbcDepCod.RemoveAll
'        sdbcDepDen.DroppedDown = False
'        sdbcDepDen.Text = ""
'        sdbcDepDen.RemoveAll
'    End If
End Sub

Private Sub sdbcDepCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcDepCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcDepCod.Rows - 1
            bm = sdbcDepCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcDepCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcDepCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcDepCod_Validate(Cancel As Boolean)

    Dim oDeps As CDepartamentos
    Dim bExiste As Boolean
    
    If sdbcDepCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el departamento
    
    Screen.MousePointer = vbHourglass
    
    Set oDeps = oFSGSRaiz.Generar_CDepartamentos
    oDeps.CargarTodosLosDepartamentos sdbcDepCod.Text, , True, , False
    
    bExiste = Not (oDeps.Count = 0)
    
    If Not bExiste Then
        sdbcDepCod.Text = ""
'        oMensajes.NoValido "Departamento"
        oMensajes.NoValido sIdiDept
    Else
        RespetarCombo = True
        sdbcDepDen.Text = oDeps.Item(1).Den
        RespetarCombo = False
        CargarComboDesde = False
    End If
    
    Set oDeps = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcDepDen_Change()
    
    'If Not sdbcDepDen.DroppedDown Then
    '    sdbcDepDen.RemoveAll
    'End If
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcDepCod.Text = ""
        RespetarCombo = False
        CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcDepDen_Click()
    
'    If sdbcDepDen.DroppedDown Then Exit Sub
'    If sdbcDepDen.Value = "" Or sdbcDepDen.Value = "..." Then Exit Sub
'    sdbcDepDen.ListAutoPosition = False
'    sdbcDepCod.Text = sdbcDepDen.Columns(1).Value
'    sdbcDepDen.ListAutoPosition = True
  
End Sub

Private Sub sdbcDepDen_CloseUp()
    
    'If sdbcDepDen.Value = "" Then Exit Sub
    'sdbcDepDen.ListAutoPosition = False
    'sdbcDepCod.Text = sdbcDepDen.Columns(1).Value
    'sdbcDepDen.ListAutoPosition = True
  
    If sdbcDepDen.Value = "..." Then
        sdbcDepDen.Text = ""
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcDepCod.Text = sdbcDepDen.Columns(1).Text
    sdbcDepDen.Text = sdbcDepDen.Columns(0).Text
    RespetarCombo = False
    
    CargarComboDesde = False
      
End Sub

Private Sub sdbcDepDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    'If sdbcDepDen.Rows <> 0 Then Exit Sub
    sdbcDepDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If CargarComboDesde Then
        oDeps.CargarTodosLosdepartamentosDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcDepDen.Text), True, False
    Else
        oDeps.CargarTodosLosDepartamentos , , , True, False
    End If
    
    Codigos = oDeps.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcDepDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next

    If CargarComboDesde And Not oDeps.EOF Then
        sdbcDepDen.AddItem "..."
    End If

    sdbcDepDen.SelStart = 0
    sdbcDepDen.SelLength = Len(sdbcDepDen.Text)
    sdbcDepCod.Refresh

    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcDepDen_InitColumnProps()
    sdbcDepDen.DataFieldList = "Column 0"
    sdbcDepDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcDepDen_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyDelete Then
    '    sdbcDepCod.DroppedDown = False
    '    sdbcDepCod.Text = ""
    '    sdbcDepCod.RemoveAll
    '    sdbcDepDen.DroppedDown = False
    '    sdbcDepDen.Text = ""
    '    sdbcDepDen.RemoveAll
    'End If
End Sub
Private Sub cmdAceptar_Click()

Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim oDep As CDepartamento
Dim oDepAsoc As CDepAsociado
Dim varCod As Variant
Dim varDen As Variant
Dim varDepAsocCodUON1 As Variant
Dim varDepAsocCodUON2 As Variant
Dim varDepAsocCodUON3 As Variant
Dim varCodEnEstr As Variant

Screen.MousePointer = vbHourglass
teserror.NumError = TESnoerror

If optNuevo.Value = True Then
        
        varCod = Trim(txtCod)
        varDen = Trim(txtDen)
    
        '********* Validar datos *********
        If varCod = "" Then
'            oMensajes.NoValido "Codigo"
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiCod
            If Me.Visible Then txtCod.SetFocus
            Exit Sub
        End If
        
        If varDen = "" Then
'            oMensajes.NoValida "Denominacion"
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiDen
            If Me.Visible Then txtDen.SetFocus
            Exit Sub
        End If
        
        Set oDep = oFSGSRaiz.Generar_CDepartamento
        oDep.Cod = varCod
        oDep.Den = varDen
        Set oIBaseDatos = oDep
     
        teserror = oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            Set oDep = Nothing
            Exit Sub
        End If
        
        RegistrarAccion accionessummit.ACCDepAnya, "Cod:" & Trim(txtCod)
        Set oDep = Nothing
        
Else
   If Trim(sdbcDepCod.Text) = "" Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
   
   varCod = sdbcDepCod.Text
   varDen = sdbcDepDen.Text
End If

    Set oDepAsoc = oFSGSRaiz.generar_CDepAsociado
    
    Select Case Int(val(Mid(frmESTRORG.tvwestrorg.selectedItem.Tag, 4, 1)))
        Case 1
                oDepAsoc.CodUON1 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem)
                varDepAsocCodUON1 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem)
                varCodEnEstr = CStr(varDepAsocCodUON1)
        Case 2
                oDepAsoc.CodUON2 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem)
                oDepAsoc.CodUON1 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem.Parent)
                varDepAsocCodUON2 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem)
                varDepAsocCodUON1 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem.Parent)
                varCodEnEstr = CStr(varDepAsocCodUON1) & CStr(varDepAsocCodUON2)
        Case 3
                oDepAsoc.CodUON3 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem)
                oDepAsoc.CodUON2 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem.Parent)
                oDepAsoc.CodUON1 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem.Parent.Parent)
                varDepAsocCodUON3 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem)
                varDepAsocCodUON2 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem.Parent)
                varDepAsocCodUON1 = frmESTRORG.DevolverCod(frmESTRORG.tvwestrorg.selectedItem.Parent.Parent)
                varCodEnEstr = CStr(varDepAsocCodUON1) & CStr(varDepAsocCodUON2) & CStr(varDepAsocCodUON3)
        End Select
        
    oDepAsoc.Cod = varCod
    varCodEnEstr = varCodEnEstr & CStr(varCod)
    
    Set oIBaseDatos = oDepAsoc
    teserror = oIBaseDatos.AnyadirABaseDatos
    
    If teserror.NumError = TESnoerror Then
        AnyadirDepAEstructura varCodEnEstr, varCod, varDen
        RegistrarAccion accionessummit.ACCDepAsocAnya, "Cod:" & CStr(sdbcDepCod)
        Set oDepAsoc = Nothing
        
        Unload Me
    Else
        Screen.MousePointer = vbNormal
        TratarError teserror
        Set oDepAsoc = Nothing
    End If
    
    frmESTRORG.tvwestrorg_NodeClick frmESTRORG.tvwestrorg.selectedItem
    
    Screen.MousePointer = vbNormal
           
End Sub

Private Sub cmdCancelar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodDEP
    CargarRecursos
    Set oDeps = oFSGSRaiz.Generar_CDepartamentos
   
    PonerFieldSeparator Me
End Sub


Private Sub optExistente_Click()
    
    txtCod = ""
    txtDen = ""
    picExistente.Enabled = True
    picNuevo.Enabled = False
    If Me.Visible Then sdbcDepCod.SetFocus
    
End Sub

Private Sub optNuevo_Click()
        
    picExistente.Enabled = False
    picNuevo.Enabled = True
    sdbcDepCod.Text = ""
    sdbcDepDen.Text = ""
    If Me.Visible Then txtCod.SetFocus
    
End Sub
Private Sub AnyadirDepAEstructura(ByVal CodDepAsoc As Variant, ByVal CodDep As Variant, ByVal Den As String)
Dim nodx As node
Dim node As node

Set node = frmESTRORG.tvwestrorg.selectedItem

Set nodx = frmESTRORG.tvwestrorg.Nodes.Add(node.key, tvwChild, "DEPA" & CodDepAsoc, CodDep & " - " & Den, "Departamento")
nodx.Tag = "DEP" & Mid(node.Tag, 4, 1) & CStr(CodDep)
nodx.Selected = True
nodx.EnsureVisible

End Sub

Private Sub sdbcDepDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcDepDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcDepDen.Rows - 1
            bm = sdbcDepDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcDepDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcDepDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcDepDen_Validate(Cancel As Boolean)

    Dim oDeps As CDepartamentos
    Dim bExiste As Boolean
    
    If sdbcDepDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el departamento
    
    Screen.MousePointer = vbHourglass
    Set oDeps = oFSGSRaiz.Generar_CDepartamentos

    oDeps.CargarTodosLosDepartamentos , sdbcDepDen.Text, True, , False
    
    bExiste = Not (oDeps.Count = 0)
    
    If Not bExiste Then
        sdbcDepDen.Text = ""
'        oMensajes.NoValido "Departamento"
        oMensajes.NoValido sIdiDept
    Else
        RespetarCombo = True
        sdbcDepCod.Text = oDeps.Item(1).Cod
        RespetarCombo = False
        CargarComboDesde = False
    End If
    
    Set oDeps = Nothing

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRORG_ANYADEP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        optExistente.caption = Ador(0).Value
        Ador.MoveNext
        optNuevo.caption = Ador(0).Value
        Ador.MoveNext
        lblCod.caption = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiDept = Ador(0).Value
        Ador.MoveNext
        sIdiCod = Ador(0).Value
        sdbcDepCod.Columns(0).caption = Ador(0).Value
        sdbcDepDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sIdiDen = Ador(0).Value
        sdbcDepCod.Columns(1).caption = Ador(0).Value
        sdbcDepDen.Columns(0).caption = Ador(0).Value
        
        Ador.Close
    
    End If

    Set Ador = Nothing

End Sub
