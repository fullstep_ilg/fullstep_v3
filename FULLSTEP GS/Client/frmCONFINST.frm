VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "*\AUserControls\FSGSUserControls.vbp"
Begin VB.Form frmCONFINST 
   Caption         =   "DConfiguraci�n de la instalaci�n"
   ClientHeight    =   7650
   ClientLeft      =   5520
   ClientTop       =   1620
   ClientWidth     =   12420
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCONFINST.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7650
   ScaleWidth      =   12420
   Begin TabDlg.SSTab tabCONFINST 
      Height          =   7995
      Left            =   120
      TabIndex        =   1
      Top             =   90
      Width           =   12135
      _ExtentX        =   21405
      _ExtentY        =   14102
      _Version        =   393216
      Style           =   1
      Tabs            =   7
      TabsPerRow      =   7
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DMantenimiento"
      TabPicture(0)   =   "frmCONFINST.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraManten(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraManten(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "DGestion"
      TabPicture(1)   =   "frmCONFINST.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sstabGestion"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "DOfertas"
      TabPicture(2)   =   "frmCONFINST.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "SSTabOfertas"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "DReuniones"
      TabPicture(3)   =   "frmCONFINST.frx":0D06
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "picReuPlantillas"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "DPedidos"
      TabPicture(4)   =   "frmCONFINST.frx":0D22
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "TabPedidos"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "DCarpetas"
      TabPicture(5)   =   "frmCONFINST.frx":0D3E
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "fraListados"
      Tab(5).Control(1)=   "fraSolicitudes"
      Tab(5).ControlCount=   2
      TabCaption(6)   =   "DOpciones"
      TabPicture(6)   =   "frmCONFINST.frx":0D5A
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "fraLenguajePortal"
      Tab(6).Control(1)=   "fraLenguaje"
      Tab(6).Control(2)=   "fraInterfaz"
      Tab(6).Control(3)=   "frmAvisoCierre"
      Tab(6).Control(4)=   "Frame26"
      Tab(6).Control(5)=   "fraTimeZone"
      Tab(6).ControlCount=   6
      Begin VB.Frame fraTimeZone 
         Caption         =   "DZona horaria"
         Height          =   855
         Left            =   -74880
         TabIndex        =   380
         Top             =   5040
         Width           =   9270
         Begin VB.PictureBox picTZ 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   120
            ScaleHeight     =   495
            ScaleWidth      =   9090
            TabIndex        =   381
            Top             =   240
            Width           =   9090
            Begin SSDataWidgets_B.SSDBCombo sdbcTZ 
               Height          =   285
               Left            =   45
               TabIndex        =   382
               Top             =   120
               Width           =   8880
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   8811
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "KEY"
               Columns(1).Name =   "KEY"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   15663
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
         End
      End
      Begin VB.Frame fraManten 
         Caption         =   "DAlta de art�culos"
         Height          =   1065
         Index           =   1
         Left            =   720
         TabIndex        =   231
         Top             =   3300
         Width           =   7995
         Begin VB.PictureBox picSugerir 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   510
            ScaleHeight     =   465
            ScaleWidth      =   7245
            TabIndex        =   232
            Top             =   330
            Width           =   7245
            Begin VB.CheckBox chkSugerir 
               Caption         =   "Sugerir c�digos de art�culo"
               Height          =   345
               Left            =   0
               TabIndex        =   8
               Top             =   30
               Width           =   6615
            End
         End
      End
      Begin VB.Frame fraSolicitudes 
         Caption         =   "DRuta para los archivos de emails de solicitudes de ofertas"
         Height          =   2535
         Left            =   -74640
         TabIndex        =   229
         Top             =   3240
         Width           =   8685
         Begin VB.PictureBox picSolicitudes 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2175
            Left            =   360
            ScaleHeight     =   2175
            ScaleWidth      =   7815
            TabIndex        =   230
            Top             =   240
            Width           =   7815
            Begin VB.CommandButton cmdDrive 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   7170
               Picture         =   "frmCONFINST.frx":0D76
               Style           =   1  'Graphical
               TabIndex        =   102
               Top             =   90
               Width           =   315
            End
            Begin VB.DirListBox DirSolicitudes 
               Height          =   1665
               Left            =   0
               TabIndex        =   103
               Top             =   435
               Width           =   7515
            End
            Begin VB.TextBox txtRutaSolicitudes 
               Height          =   315
               Left            =   0
               TabIndex        =   101
               Top             =   60
               Width           =   7140
            End
         End
      End
      Begin VB.Frame fraListados 
         Caption         =   "DRuta para los archivos de dise�o de listados (RPT)"
         Height          =   2775
         Left            =   -74610
         TabIndex        =   116
         Top             =   360
         Width           =   8610
         Begin VB.PictureBox picListados 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2385
            Left            =   360
            ScaleHeight     =   2385
            ScaleWidth      =   7815
            TabIndex        =   117
            Top             =   240
            Width           =   7815
            Begin VB.CommandButton cmdDrive 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   7170
               Picture         =   "frmCONFINST.frx":0E35
               Style           =   1  'Graphical
               TabIndex        =   99
               Top             =   90
               Width           =   315
            End
            Begin VB.TextBox txtRuta 
               Height          =   315
               Left            =   0
               TabIndex        =   98
               Top             =   60
               Width           =   7140
            End
            Begin VB.DirListBox DirRPT 
               Height          =   1890
               Left            =   0
               TabIndex        =   100
               Top             =   435
               Width           =   7515
            End
         End
      End
      Begin TabDlg.SSTab sstabGestion 
         Height          =   6165
         Left            =   -74865
         TabIndex        =   189
         Top             =   360
         Width           =   9180
         _ExtentX        =   16193
         _ExtentY        =   10874
         _Version        =   393216
         Style           =   1
         Tabs            =   5
         TabsPerRow      =   5
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Valores por defecto"
         TabPicture(0)   =   "frmCONFINST.frx":0EF4
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraValores(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraValores(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Configuraci�n de proceso por defecto"
         TabPicture(1)   =   "frmCONFINST.frx":0F10
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraAdminPub"
         Tab(1).Control(1)=   "fraPonderar"
         Tab(1).Control(2)=   "fraAmbito"
         Tab(1).Control(3)=   "fraConfProce(4)"
         Tab(1).Control(4)=   "fraSolicitud"
         Tab(1).Control(5)=   "cmdConfigSubasta"
         Tab(1).ControlCount=   6
         TabCaption(2)   =   "Reglas de gesti�n"
         TabPicture(2)   =   "frmCONFINST.frx":0F2C
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "fraRegGest"
         Tab(2).Control(1)=   "fraRegGest2"
         Tab(2).Control(2)=   "fraRegGest3"
         Tab(2).ControlCount=   3
         TabCaption(3)   =   "Avisos"
         TabPicture(3)   =   "frmCONFINST.frx":0F48
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "ctrlConfGenAvisos"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "DSolicitudes de compra"
         TabPicture(4)   =   "frmCONFINST.frx":0F64
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "fraSolicit(1)"
         Tab(4).Control(1)=   "fraSolicit(0)"
         Tab(4).ControlCount=   2
         Begin FSGSUserControls.ctrlConfGenAvisos ctrlConfGenAvisos 
            Height          =   4710
            Left            =   -74760
            TabIndex        =   395
            Top             =   465
            Width           =   8055
            _ExtentX        =   14208
            _ExtentY        =   8308
            General         =   0   'False
         End
         Begin VB.CommandButton cmdConfigSubasta 
            Caption         =   "..."
            Height          =   300
            Left            =   -71040
            TabIndex        =   379
            Top             =   2900
            Width           =   300
         End
         Begin VB.Frame fraSolicitud 
            Caption         =   "DSolicitudes de ofertas"
            Height          =   2230
            Left            =   -74790
            TabIndex        =   348
            Top             =   2680
            Width           =   8760
            Begin VB.PictureBox picSolicitud 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1945
               Left            =   120
               ScaleHeight     =   1950
               ScaleWidth      =   8325
               TabIndex        =   349
               Top             =   240
               Width           =   8325
               Begin VB.CheckBox chkNoPublicarFinSum 
                  Caption         =   "No publicar fecha de fin de suministro"
                  Height          =   285
                  Left            =   120
                  TabIndex        =   357
                  Top             =   1675
                  Width           =   4215
               End
               Begin VB.CheckBox chkSolOfe 
                  Caption         =   "DSolicitar cantidades m�ximas de suministro"
                  Height          =   285
                  Index           =   2
                  Left            =   120
                  TabIndex        =   356
                  Top             =   475
                  Width           =   6840
               End
               Begin VB.CheckBox chkSolOfe 
                  Caption         =   "DPedir alternativas de precios"
                  Height          =   285
                  Index           =   1
                  Left            =   120
                  TabIndex        =   355
                  Top             =   235
                  Width           =   3840
               End
               Begin VB.CheckBox chkSolOfe 
                  Caption         =   "DRecepci�n de ofertas en modo subasta"
                  Height          =   285
                  Index           =   0
                  Left            =   120
                  TabIndex        =   354
                  Top             =   -5
                  Width           =   3840
               End
               Begin VB.CheckBox chkSolOfe 
                  Caption         =   "DPermitir adjudicar archivos a nivel de proceso"
                  Height          =   285
                  Index           =   3
                  Left            =   120
                  TabIndex        =   353
                  Top             =   955
                  Width           =   6000
               End
               Begin VB.CheckBox chkSolOfe 
                  Caption         =   "DPermitir adjudicar archivos a nivel de grupo"
                  Height          =   285
                  Index           =   4
                  Left            =   120
                  TabIndex        =   352
                  Top             =   1195
                  Width           =   3840
               End
               Begin VB.CheckBox chkSolOfe 
                  Caption         =   "DPermitir adjudicar archivos a nivel de �tem"
                  Height          =   285
                  Index           =   5
                  Left            =   120
                  TabIndex        =   351
                  Top             =   1435
                  Width           =   3840
               End
               Begin VB.CheckBox chkSolOfe 
                  Caption         =   "DOfertar en la moneda del proceso"
                  Height          =   285
                  Index           =   6
                  Left            =   120
                  TabIndex        =   350
                  Top             =   715
                  Width           =   3840
               End
            End
         End
         Begin VB.Frame fraSolicit 
            Caption         =   "DGeneraci�n de ofertas desde solicitud"
            Height          =   1215
            Index           =   1
            Left            =   -74730
            TabIndex        =   337
            Top             =   2500
            Width           =   8340
            Begin VB.PictureBox picGenSolicit 
               BorderStyle     =   0  'None
               Height          =   720
               Index           =   1
               Left            =   240
               ScaleHeight     =   720
               ScaleWidth      =   7905
               TabIndex        =   342
               Top             =   300
               Width           =   7900
               Begin SSDataWidgets_B.SSDBCombo sdbcPeriodoSolic 
                  Height          =   285
                  Left            =   3120
                  TabIndex        =   341
                  Top             =   90
                  Width           =   2415
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Caption=   "PERIODO"
                  Columns(0).Name =   "PERIODO"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "BUZPER"
                  Columns(1).Name =   "BUZPER"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4260
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblPeriodoSolic 
                  Caption         =   "DPer�odo de validez:"
                  Height          =   255
                  Index           =   0
                  Left            =   0
                  TabIndex        =   343
                  Top             =   120
                  Width           =   2895
               End
            End
         End
         Begin VB.Frame fraSolicit 
            Caption         =   "DGeneraci�n de procesos desde solicitud"
            Height          =   1700
            Index           =   0
            Left            =   -74730
            TabIndex        =   336
            Top             =   600
            Width           =   8340
            Begin VB.PictureBox picGenSolicit 
               BorderStyle     =   0  'None
               Height          =   1300
               Index           =   0
               Left            =   240
               ScaleHeight     =   1305
               ScaleWidth      =   7905
               TabIndex        =   338
               Top             =   300
               Width           =   7900
               Begin VB.PictureBox picSolicImporte 
                  BorderStyle     =   0  'None
                  Height          =   500
                  Left            =   480
                  ScaleHeight     =   495
                  ScaleWidth      =   7335
                  TabIndex        =   344
                  Top             =   360
                  Width           =   7335
                  Begin VB.CheckBox chkAdjDirImp 
                     Caption         =   "DPara importes menores de:"
                     Height          =   255
                     Left            =   0
                     TabIndex        =   346
                     Top             =   20
                     Width           =   3400
                  End
                  Begin VB.TextBox txtAdjDirImp 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Left            =   3540
                     TabIndex        =   345
                     Top             =   0
                     Width           =   1575
                  End
                  Begin VB.Label lblPeriodoSolic 
                     Caption         =   "EUR"
                     Height          =   255
                     Index           =   1
                     Left            =   5270
                     TabIndex        =   347
                     Top             =   40
                     Width           =   855
                  End
               End
               Begin VB.OptionButton optGenerarProc 
                  Caption         =   "DGenerar procesos de reuni�n"
                  Height          =   255
                  Index           =   1
                  Left            =   0
                  TabIndex        =   340
                  Top             =   900
                  Width           =   4695
               End
               Begin VB.OptionButton optGenerarProc 
                  Caption         =   "DGenerar procesos de adjudicaci�n directa"
                  Height          =   255
                  Index           =   0
                  Left            =   0
                  TabIndex        =   339
                  Top             =   0
                  Width           =   4695
               End
            End
         End
         Begin VB.Frame fraValores 
            Caption         =   "DOtros"
            Height          =   1575
            Index           =   1
            Left            =   270
            TabIndex        =   332
            Top             =   3600
            Width           =   8340
            Begin VB.PictureBox picValDef 
               BorderStyle     =   0  'None
               Height          =   1215
               Index           =   1
               Left            =   120
               ScaleHeight     =   1215
               ScaleWidth      =   8160
               TabIndex        =   333
               Top             =   240
               Width           =   8160
               Begin SSDataWidgets_B.SSDBCombo sdbcPagoDen 
                  Height          =   285
                  Left            =   3030
                  TabIndex        =   15
                  Top             =   650
                  Width           =   4155
                  DataFieldList   =   "Column 0"
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   4815
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2302
                  Columns(1).Caption=   "C�digo"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   7329
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  DataFieldToDisplay=   "Column 0"
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcDestCod 
                  Height          =   285
                  Left            =   1680
                  TabIndex        =   12
                  Top             =   120
                  Width           =   1335
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "DCod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "DDenominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   2355
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcDestDen 
                  Height          =   285
                  Left            =   3030
                  TabIndex        =   13
                  Top             =   120
                  Width           =   4155
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   6165
                  Columns(0).Caption=   "DDenominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2117
                  Columns(1).Caption=   "DCod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   7329
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcPagoCod 
                  Height          =   285
                  Left            =   1680
                  TabIndex        =   14
                  Top             =   650
                  Width           =   1335
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1826
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).HasForeColor=   -1  'True
                  Columns(0).HasBackColor=   -1  'True
                  Columns(0).BackColor=   16777215
                  Columns(1).Width=   6033
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(1).HasBackColor=   -1  'True
                  Columns(1).BackColor=   16777215
                  _ExtentX        =   2355
                  _ExtentY        =   503
                  _StockProps     =   93
                  ForeColor       =   -2147483642
                  BackColor       =   -2147483643
                  DataFieldToDisplay=   "Column 0"
               End
               Begin VB.Label lblPago 
                  Caption         =   "DForma de pago:"
                  Height          =   255
                  Left            =   300
                  TabIndex        =   335
                  Top             =   670
                  Width           =   2055
               End
               Begin VB.Label Label19 
                  Caption         =   "DDestino:"
                  Height          =   195
                  Left            =   300
                  TabIndex        =   334
                  Top             =   150
                  Width           =   1155
               End
            End
         End
         Begin VB.Frame fraConfProce 
            Caption         =   "Pedidos"
            Height          =   520
            Index           =   4
            Left            =   -74790
            TabIndex        =   327
            Top             =   5500
            Width           =   8760
            Begin VB.PictureBox picDatosConf 
               BorderStyle     =   0  'None
               Height          =   255
               Index           =   7
               Left            =   100
               ScaleHeight     =   255
               ScaleWidth      =   3855
               TabIndex        =   328
               Top             =   240
               Width           =   3855
               Begin VB.CheckBox chkUnSoloPedido 
                  Caption         =   "Permitir un s�lo pedido directo"
                  Height          =   195
                  Left            =   105
                  TabIndex        =   329
                  Top             =   0
                  Width           =   6495
               End
            End
         End
         Begin VB.Frame fraRegGest 
            Caption         =   "Adjuntar especificaciones del art�culo a items"
            Height          =   1800
            Left            =   -74790
            TabIndex        =   258
            Top             =   460
            Width           =   8415
            Begin VB.PictureBox picRegGest4 
               BorderStyle     =   0  'None
               Height          =   495
               Left            =   120
               ScaleHeight     =   495
               ScaleWidth      =   8205
               TabIndex        =   269
               Top             =   1160
               Width           =   8205
               Begin VB.OptionButton optPresPlanif 
                  Caption         =   "DCargar con presupuesto planificado"
                  Height          =   285
                  Left            =   3120
                  TabIndex        =   264
                  Top             =   0
                  Width           =   3405
               End
               Begin VB.OptionButton optPresAdj 
                  Caption         =   "DCargar presupuesto unitario con precio de �ltima adjudicaci�n"
                  Height          =   340
                  Left            =   180
                  TabIndex        =   263
                  Top             =   0
                  Width           =   2800
               End
            End
            Begin VB.PictureBox picRegGest 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   475
               Left            =   120
               ScaleHeight     =   480
               ScaleWidth      =   8205
               TabIndex        =   259
               Top             =   450
               Width           =   8205
               Begin VB.OptionButton opNoAdj 
                  Caption         =   "DNo adjuntar"
                  Height          =   285
                  Left            =   5940
                  TabIndex        =   262
                  Top             =   0
                  Width           =   2175
               End
               Begin VB.OptionButton opAdjPreguntar 
                  Caption         =   "DPreguntar si adjuntar"
                  Height          =   285
                  Left            =   3120
                  TabIndex        =   261
                  Top             =   0
                  Width           =   2565
               End
               Begin VB.OptionButton opAdjAutomat 
                  Caption         =   "DAdjuntar autom�ticamente"
                  Height          =   340
                  Left            =   180
                  TabIndex        =   260
                  Top             =   0
                  Width           =   2415
               End
            End
            Begin VB.Line Line2 
               Index           =   0
               X1              =   240
               X2              =   8000
               Y1              =   960
               Y2              =   960
            End
         End
         Begin VB.Frame fraRegGest2 
            Caption         =   "DPaso automatico entre opciones"
            Height          =   1035
            Left            =   -74790
            TabIndex        =   256
            Top             =   2380
            Width           =   8415
            Begin VB.PictureBox picRegGest2 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   705
               Left            =   120
               ScaleHeight     =   705
               ScaleWidth      =   8175
               TabIndex        =   257
               Top             =   240
               Width           =   8175
               Begin VB.OptionButton OptNoPasar 
                  Caption         =   "DNo pasar"
                  Height          =   285
                  Left            =   5940
                  TabIndex        =   267
                  Top             =   180
                  Width           =   2145
               End
               Begin VB.OptionButton OptSiPasar 
                  Caption         =   "DPreguntar si pasar"
                  Height          =   285
                  Left            =   3090
                  TabIndex        =   266
                  Top             =   180
                  Width           =   2565
               End
               Begin VB.OptionButton OptPasoAut 
                  Caption         =   "DPasar autom�ticamente"
                  Height          =   285
                  Left            =   180
                  TabIndex        =   265
                  Top             =   210
                  Width           =   2415
               End
            End
         End
         Begin VB.Frame fraRegGest3 
            Caption         =   "DSelecci�n de proveedores"
            Height          =   1035
            Left            =   -74790
            TabIndex        =   254
            Top             =   3535
            Width           =   8415
            Begin VB.PictureBox picRegGest3 
               BorderStyle     =   0  'None
               Height          =   705
               Left            =   120
               ScaleHeight     =   705
               ScaleWidth      =   8175
               TabIndex        =   255
               Top             =   240
               Width           =   8175
               Begin VB.CheckBox chkSelPositiva 
                  Caption         =   "DSelecci�n positiva"
                  Height          =   285
                  Left            =   180
                  TabIndex        =   268
                  Top             =   210
                  Width           =   4215
               End
            End
         End
         Begin VB.Frame fraAmbito 
            Caption         =   "Ambito de datos"
            Height          =   1800
            Left            =   -74790
            TabIndex        =   251
            Top             =   360
            Width           =   8760
            Begin VB.PictureBox picAmbitoDatos 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   1475
               Left            =   120
               ScaleHeight     =   1470
               ScaleWidth      =   8475
               TabIndex        =   252
               Top             =   240
               Width           =   8475
               Begin SSDataWidgets_B.SSDBGrid sdbgDatosGrid 
                  Height          =   1470
                  Left            =   90
                  TabIndex        =   253
                  Top             =   0
                  Width           =   8340
                  ScrollBars      =   2
                  _Version        =   196617
                  DataMode        =   2
                  RecordSelectors =   0   'False
                  Col.Count       =   7
                  stylesets.count =   3
                  stylesets(0).Name=   "Yellow"
                  stylesets(0).BackColor=   11862015
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCONFINST.frx":0F80
                  stylesets(1).Name=   "Normal"
                  stylesets(1).ForeColor=   0
                  stylesets(1).BackColor=   14671839
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCONFINST.frx":0F9C
                  stylesets(2).Name=   "Header"
                  stylesets(2).HasFont=   -1  'True
                  BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(2).Picture=   "frmCONFINST.frx":0FB8
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowColumnSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   0
                  HeadStyleSet    =   "Header"
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   7
                  Columns(0).Width=   1455
                  Columns(0).Caption=   "Usar"
                  Columns(0).Name =   "Usar"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).Style=   2
                  Columns(1).Width=   8255
                  Columns(1).Caption=   "Dato"
                  Columns(1).Name =   "Dato"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   1508
                  Columns(2).Caption=   "Proceso"
                  Columns(2).Name =   "Proceso"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(2).Style=   2
                  Columns(3).Width=   1508
                  Columns(3).Caption=   "Grupo"
                  Columns(3).Name =   "Grupo"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(3).Style=   2
                  Columns(4).Width=   1508
                  Columns(4).Caption=   "Item"
                  Columns(4).Name =   "Item"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(4).Style=   2
                  Columns(5).Width=   3200
                  Columns(5).Visible=   0   'False
                  Columns(5).Caption=   "Obligatoria"
                  Columns(5).Name =   "Obligatoria"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(6).Width=   3200
                  Columns(6).Visible=   0   'False
                  Columns(6).Caption=   "Fila"
                  Columns(6).Name =   "Fila"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  TabNavigation   =   1
                  _ExtentX        =   14711
                  _ExtentY        =   2593
                  _StockProps     =   79
                  BackColor       =   14671839
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
            End
         End
         Begin VB.Frame fraPonderar 
            Caption         =   "DPonderaci�n"
            Height          =   510
            Left            =   -74790
            TabIndex        =   248
            Top             =   4945
            Width           =   8760
            Begin VB.PictureBox picPonderar 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Left            =   210
               ScaleHeight     =   240
               ScaleWidth      =   7965
               TabIndex        =   249
               Top             =   225
               Width           =   7965
               Begin VB.CheckBox chkPonderar 
                  Caption         =   "DHabilitar ponderaci�n en atributos"
                  Height          =   195
                  Left            =   0
                  TabIndex        =   250
                  Top             =   0
                  Width           =   5370
               End
            End
         End
         Begin VB.Frame fraAdminPub 
            Height          =   510
            Left            =   -74790
            TabIndex        =   245
            Top             =   2160
            Width           =   8760
            Begin VB.PictureBox picAdminPub 
               BorderStyle     =   0  'None
               Height          =   240
               Left            =   210
               ScaleHeight     =   240
               ScaleWidth      =   7965
               TabIndex        =   246
               Top             =   240
               Width           =   7965
               Begin VB.CheckBox chkAdminPub 
                  Caption         =   "DProceso de administraci�n p�blica"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Left            =   0
                  TabIndex        =   247
                  Top             =   0
                  Width           =   6765
               End
            End
         End
         Begin VB.Frame fraValores 
            Height          =   2820
            Index           =   0
            Left            =   270
            TabIndex        =   190
            Top             =   555
            Width           =   8340
            Begin VB.PictureBox picValDef 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   2430
               Index           =   0
               Left            =   120
               ScaleHeight     =   2430
               ScaleWidth      =   8160
               TabIndex        =   191
               Top             =   240
               Width           =   8160
               Begin VB.TextBox txtDescVal 
                  Height          =   1005
                  Left            =   255
                  MaxLength       =   500
                  MultiLine       =   -1  'True
                  TabIndex        =   11
                  Top             =   1320
                  Width           =   7695
               End
               Begin VB.TextBox txtDenVal 
                  Height          =   285
                  Left            =   1680
                  MaxLength       =   100
                  TabIndex        =   10
                  Top             =   510
                  Width           =   6210
               End
               Begin VB.TextBox txtCodVal 
                  Height          =   285
                  Left            =   1680
                  TabIndex        =   9
                  Top             =   60
                  Width           =   2145
               End
               Begin VB.Label lblDescrip1 
                  Caption         =   "DDescripci�n del grupo"
                  Height          =   255
                  Left            =   255
                  TabIndex        =   194
                  Top             =   1050
                  Width           =   2415
               End
               Begin VB.Label lblDen1 
                  Caption         =   "DDenominaci�n"
                  Height          =   255
                  Left            =   270
                  TabIndex        =   193
                  Top             =   540
                  Width           =   1395
               End
               Begin VB.Label lblCod1 
                  Caption         =   "DC�digo"
                  Height          =   255
                  Left            =   300
                  TabIndex        =   192
                  Top             =   90
                  Width           =   1275
               End
            End
         End
      End
      Begin VB.Frame Frame26 
         Caption         =   "DMostrar los mensajes antes de enviarlos"
         Height          =   2280
         Left            =   -74880
         TabIndex        =   221
         Top             =   360
         Width           =   9250
         Begin VB.PictureBox picMostrar 
            BorderStyle     =   0  'None
            Height          =   1905
            Index           =   1
            Left            =   5460
            ScaleHeight     =   1905
            ScaleWidth      =   3705
            TabIndex        =   324
            Top             =   210
            Width           =   3700
            Begin VB.OptionButton optFormatoEMail 
               Caption         =   "TXT"
               Height          =   195
               Index           =   1
               Left            =   1740
               TabIndex        =   109
               Top             =   480
               Value           =   -1  'True
               Width           =   1095
            End
            Begin VB.OptionButton optFormatoEMail 
               Caption         =   "HTML"
               Height          =   195
               Index           =   0
               Left            =   180
               TabIndex        =   108
               Top             =   480
               Width           =   975
            End
            Begin VB.Label lblFormatoMail 
               Caption         =   "(Se aplicar� a los emais de:Solicitudes de compra, aprovisionamiento y aviso de disposici�n a no ofertar)"
               BeginProperty Font 
                  Name            =   "Small Fonts"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Index           =   1
               Left            =   0
               TabIndex        =   326
               Top             =   780
               Width           =   3135
            End
            Begin VB.Label lblFormatoMail 
               Caption         =   "DRecibir los emails en formato:"
               Height          =   255
               Index           =   0
               Left            =   45
               TabIndex        =   325
               Top             =   165
               Width           =   2295
            End
         End
         Begin VB.PictureBox picMostrar 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            FillColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   1905
            Index           =   0
            Left            =   150
            ScaleHeight     =   1905
            ScaleWidth      =   4665
            TabIndex        =   222
            Top             =   210
            Width           =   4665
            Begin VB.CheckBox chkAcuseRecibo 
               Caption         =   "DPedir acuse de recibo"
               Height          =   195
               Left            =   120
               TabIndex        =   107
               Top             =   1575
               Width           =   4095
            End
            Begin VB.CheckBox chkMostrarMail 
               Caption         =   "DMostrar los mensajes antes de enviarlos"
               Height          =   195
               Left            =   75
               TabIndex        =   104
               Top             =   165
               Width           =   4095
            End
            Begin VB.TextBox txtCc 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   630
               TabIndex        =   105
               Top             =   585
               Width           =   3975
            End
            Begin VB.TextBox txtBcc 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   630
               TabIndex        =   106
               Top             =   975
               Width           =   3975
            End
            Begin VB.Label lblCc 
               Caption         =   "Cc:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   90
               TabIndex        =   224
               Top             =   645
               Width           =   495
            End
            Begin VB.Label lblBcc 
               Caption         =   "Bcc:"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   90
               TabIndex        =   223
               Top             =   1035
               Width           =   495
            End
         End
      End
      Begin VB.Frame frmAvisoCierre 
         Caption         =   "Aviso de cierre de aplicaci�n"
         Height          =   855
         Left            =   -74880
         TabIndex        =   187
         Top             =   2850
         Width           =   4905
         Begin VB.PictureBox picMensaje 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   375
            Left            =   90
            ScaleHeight     =   375
            ScaleWidth      =   4005
            TabIndex        =   188
            Top             =   240
            Width           =   4005
            Begin VB.CheckBox chkMostrarMensaje 
               Caption         =   "Mostrar mensaje"
               Height          =   255
               Left            =   75
               TabIndex        =   110
               Top             =   60
               Value           =   1  'Checked
               Width           =   3495
            End
         End
      End
      Begin VB.Frame fraManten 
         Caption         =   "DValores por defecto"
         Height          =   1995
         Index           =   0
         Left            =   720
         TabIndex        =   131
         Top             =   900
         Width           =   7995
         Begin VB.PictureBox picDef 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   1295
            Left            =   480
            ScaleHeight     =   1290
            ScaleWidth      =   7155
            TabIndex        =   132
            TabStop         =   0   'False
            Top             =   450
            Width           =   7155
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
               Height          =   285
               Left            =   1260
               TabIndex        =   4
               Top             =   420
               Width           =   1335
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "DCod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "DDenominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2355
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
               Height          =   285
               Left            =   2610
               TabIndex        =   5
               Top             =   420
               Width           =   4155
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "DDenominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "DCod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   7329
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonDen 
               Height          =   285
               Left            =   2610
               TabIndex        =   3
               Top             =   0
               Width           =   4155
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "DDenominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "DCod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   7329
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
               Height          =   285
               Left            =   1260
               TabIndex        =   6
               Top             =   840
               Width           =   1335
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "DCod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "DDenominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2355
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
               Height          =   285
               Left            =   2610
               TabIndex        =   7
               Top             =   840
               Width           =   4155
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "DDenominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "DCod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   7329
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
               Height          =   285
               Left            =   1260
               TabIndex        =   2
               Top             =   0
               Width           =   1335
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "DCod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "DDenominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2355
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label Label18 
               Caption         =   "DProvincia:"
               Height          =   195
               Left            =   60
               TabIndex        =   135
               Top             =   900
               Width           =   1185
            End
            Begin VB.Label Label17 
               Caption         =   "DPa�s:"
               Height          =   195
               Left            =   60
               TabIndex        =   134
               Top             =   480
               Width           =   1110
            End
            Begin VB.Label Label16 
               Caption         =   "DMoneda:"
               Height          =   195
               Left            =   60
               TabIndex        =   133
               Top             =   60
               Width           =   1155
            End
         End
      End
      Begin VB.PictureBox picReuPlantillas 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6600
         Left            =   -74880
         ScaleHeight     =   6600
         ScaleWidth      =   8850
         TabIndex        =   123
         Top             =   570
         Width           =   8850
         Begin VB.Frame Frame15 
            Caption         =   "DActa"
            Height          =   1750
            Left            =   450
            TabIndex        =   130
            Top             =   4575
            Width           =   8115
            Begin VB.PictureBox Picture1 
               BorderStyle     =   0  'None
               Height          =   390
               Left            =   1335
               ScaleHeight     =   390
               ScaleWidth      =   6360
               TabIndex        =   358
               Top             =   210
               Width           =   6360
               Begin VB.OptionButton optActaCrystal 
                  Caption         =   "DActaCrystal"
                  Height          =   345
                  Left            =   2025
                  TabIndex        =   360
                  Top             =   15
                  Width           =   2070
               End
               Begin VB.OptionButton OptActaWord 
                  Caption         =   "DActaWord"
                  Height          =   315
                  Left            =   180
                  TabIndex        =   359
                  Top             =   30
                  Value           =   -1  'True
                  Width           =   2160
               End
            End
            Begin VB.OptionButton optReuActa 
               Caption         =   "DTipo2:"
               Height          =   255
               Index           =   1
               Left            =   165
               TabIndex        =   82
               Top             =   950
               Width           =   1095
            End
            Begin VB.OptionButton optReuActa 
               Caption         =   "DTipo1:"
               Height          =   255
               Index           =   0
               Left            =   165
               TabIndex        =   79
               Top             =   600
               Value           =   -1  'True
               Width           =   1200
            End
            Begin VB.CommandButton cmdPlantilla 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   40
               Left            =   7620
               Picture         =   "frmCONFINST.frx":0FD4
               Style           =   1  'Graphical
               TabIndex        =   84
               Top             =   950
               Width           =   315
            End
            Begin VB.TextBox txtPlantilla 
               Alignment       =   1  'Right Justify
               Height          =   285
               Index           =   39
               Left            =   1485
               TabIndex        =   83
               Top             =   950
               Width           =   6045
            End
            Begin VB.CommandButton cmdPlantilla 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   19
               Left            =   7620
               Picture         =   "frmCONFINST.frx":1093
               Style           =   1  'Graphical
               TabIndex        =   81
               Top             =   600
               Width           =   315
            End
            Begin VB.TextBox txtPlantilla 
               Alignment       =   1  'Right Justify
               Height          =   285
               Index           =   19
               Left            =   1485
               MaxLength       =   255
               TabIndex        =   80
               Top             =   600
               Width           =   6045
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPlantilla 
               Height          =   285
               Index           =   2
               Left            =   2000
               TabIndex        =   386
               Top             =   650
               Width           =   5900
               DataFieldList   =   "Column 1"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   10398
               Columns(1).Caption=   "NOM"
               Columns(1).Name =   "NOM"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   10407
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPlantilla 
               Height          =   285
               Index           =   3
               Left            =   2000
               TabIndex        =   387
               Top             =   1000
               Width           =   5900
               DataFieldList   =   "Column 1"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   10398
               Columns(1).Caption=   "NOM"
               Columns(1).Name =   "NOM"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   10407
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPlantilla 
               Height          =   285
               Index           =   4
               Left            =   2000
               TabIndex        =   391
               Top             =   1350
               Width           =   5900
               DataFieldList   =   "Column 1"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   10398
               Columns(1).Caption=   "NOM"
               Columns(1).Name =   "NOM"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   10407
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label label10 
               Caption         =   "DPlantilla:"
               Height          =   255
               Index           =   4
               Left            =   165
               TabIndex        =   390
               Top             =   1350
               Width           =   2000
            End
            Begin VB.Label label10 
               Caption         =   "DPlantilla:"
               Height          =   255
               Index           =   3
               Left            =   165
               TabIndex        =   389
               Top             =   1000
               Width           =   2000
            End
            Begin VB.Label label10 
               Caption         =   "DPlantilla:"
               Height          =   255
               Index           =   2
               Left            =   165
               TabIndex        =   388
               Top             =   650
               Width           =   2000
            End
         End
         Begin VB.Frame Frame13 
            Caption         =   "DAgenda"
            Height          =   1050
            Left            =   450
            TabIndex        =   128
            Top             =   3480
            Width           =   8115
            Begin SSDataWidgets_B.SSDBCombo sdbcPlantilla 
               Height          =   285
               Index           =   0
               Left            =   2000
               TabIndex        =   383
               Top             =   225
               Width           =   6000
               DataFieldList   =   "Column 1"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   10583
               Columns(1).Caption=   "NOM"
               Columns(1).Name =   "NOM"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   10583
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPlantilla 
               Height          =   285
               Index           =   1
               Left            =   2000
               TabIndex        =   385
               Top             =   600
               Width           =   6000
               DataFieldList   =   "Column 1"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   10583
               Columns(1).Caption=   "DEN"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   10583
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label label10 
               Caption         =   "DFormato exportaci�n:"
               Height          =   255
               Index           =   1
               Left            =   165
               TabIndex        =   384
               Top             =   645
               Width           =   1750
            End
            Begin VB.Label label10 
               Caption         =   "DPlantilla:"
               Height          =   255
               Index           =   0
               Left            =   165
               TabIndex        =   129
               Top             =   300
               Width           =   1750
            End
         End
         Begin VB.Frame Frame12 
            Caption         =   "DConvocatoria"
            Height          =   2565
            Left            =   450
            TabIndex        =   126
            Top             =   855
            Width           =   8115
            Begin VB.Frame frmEmailConv 
               Caption         =   "DEmail"
               Height          =   1815
               Left            =   120
               TabIndex        =   368
               Top             =   600
               Width           =   7835
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   43
                  Left            =   1320
                  MaxLength       =   255
                  TabIndex        =   375
                  Top             =   960
                  Width           =   6045
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   44
                  Left            =   7455
                  Picture         =   "frmCONFINST.frx":1152
                  Style           =   1  'Graphical
                  TabIndex        =   374
                  Top             =   960
                  Width           =   315
               End
               Begin VB.TextBox txtConvMailSubject 
                  Height          =   285
                  Left            =   1845
                  MaxLength       =   255
                  TabIndex        =   371
                  Top             =   1350
                  Width           =   5910
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   17
                  Left            =   1320
                  MaxLength       =   255
                  TabIndex        =   370
                  Top             =   600
                  Width           =   6045
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   17
                  Left            =   7455
                  Picture         =   "frmCONFINST.frx":1211
                  Style           =   1  'Graphical
                  TabIndex        =   369
                  Top             =   600
                  Width           =   315
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcIdiomaConv 
                  Height          =   285
                  Left            =   1320
                  TabIndex        =   377
                  Top             =   240
                  Width           =   2190
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   3863
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblIdiConv 
                  Caption         =   "DIdioma"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   378
                  Top             =   270
                  Width           =   1155
               End
               Begin VB.Label Label23 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   376
                  Top             =   990
                  Width           =   1215
               End
               Begin VB.Label Label6 
                  Caption         =   "DAsunto para e-mail:"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   373
                  Top             =   1410
                  Width           =   1680
               End
               Begin VB.Label Label9 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   372
                  Top             =   630
                  Width           =   1215
               End
            End
            Begin VB.CommandButton cmdPlantilla 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   16
               Left            =   7620
               Picture         =   "frmCONFINST.frx":12D0
               Style           =   1  'Graphical
               TabIndex        =   78
               Top             =   255
               Width           =   315
            End
            Begin VB.TextBox txtPlantilla 
               Alignment       =   1  'Right Justify
               Height          =   285
               Index           =   16
               Left            =   1485
               MaxLength       =   255
               TabIndex        =   77
               Top             =   255
               Width           =   6045
            End
            Begin VB.Label Label8 
               Caption         =   "DCarta:"
               Height          =   255
               Left            =   165
               TabIndex        =   127
               Top             =   300
               Width           =   825
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "DHojas de adjudicaci�n"
            Height          =   765
            Left            =   450
            TabIndex        =   124
            Top             =   45
            Width           =   8115
            Begin VB.CommandButton cmdPlantilla 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   15
               Left            =   7620
               Picture         =   "frmCONFINST.frx":138F
               Style           =   1  'Graphical
               TabIndex        =   76
               Top             =   315
               Width           =   315
            End
            Begin VB.TextBox txtPlantilla 
               Alignment       =   1  'Right Justify
               Height          =   285
               Index           =   15
               Left            =   1485
               MaxLength       =   255
               TabIndex        =   75
               Top             =   300
               Width           =   6045
            End
            Begin VB.Label Label4 
               Caption         =   "DImpreso:"
               Height          =   255
               Left            =   165
               TabIndex        =   125
               Top             =   330
               Width           =   1155
            End
         End
      End
      Begin VB.Frame fraInterfaz 
         Caption         =   "DInterfaz de usuario"
         Height          =   855
         Left            =   -69855
         TabIndex        =   122
         Top             =   2850
         Width           =   4230
         Begin VB.CommandButton cmdCambiarFondo 
            Caption         =   "DCambiar fondo..."
            Height          =   315
            Left            =   180
            TabIndex        =   112
            Top             =   330
            Width           =   1875
         End
         Begin VB.CommandButton cmdQuitarFondo 
            Caption         =   "DQuitar fondo"
            Height          =   315
            Left            =   2160
            TabIndex        =   113
            Top             =   330
            Width           =   1875
         End
      End
      Begin VB.Frame fraLenguaje 
         Caption         =   "DIdioma"
         Height          =   855
         Left            =   -69840
         TabIndex        =   120
         Top             =   3930
         Width           =   4230
         Begin VB.PictureBox picIdioma 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   240
            ScaleHeight     =   495
            ScaleWidth      =   3765
            TabIndex        =   121
            Top             =   240
            Width           =   3765
            Begin SSDataWidgets_B.SSDBCombo sdbcIdiomas 
               Height          =   285
               Left            =   0
               TabIndex        =   114
               Top             =   90
               Width           =   3735
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   6773
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "ID"
               Columns(1).Name =   "ID"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "OFFSET"
               Columns(2).Name =   "OFFSET"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   6588
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
         End
      End
      Begin VB.Frame fraLenguajePortal 
         Caption         =   "DIdioma para los datos del portal"
         Height          =   855
         Left            =   -74880
         TabIndex        =   118
         Top             =   3930
         Width           =   4905
         Begin VB.PictureBox picIdiomaPortal 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Left            =   15
            ScaleHeight     =   465
            ScaleWidth      =   4095
            TabIndex        =   119
            Top             =   240
            Width           =   4095
            Begin SSDataWidgets_B.SSDBCombo sdbcIdiomaPortal 
               Height          =   285
               Left            =   150
               TabIndex        =   111
               Top             =   60
               Width           =   3735
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   6773
               Columns(0).Caption=   "DDenominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "ID"
               Columns(1).Name =   "ID"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "OFFSET"
               Columns(2).Name =   "OFFSET"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   6588
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
         End
      End
      Begin TabDlg.SSTab SSTabOfertas 
         Height          =   5985
         Left            =   -74880
         TabIndex        =   136
         Top             =   480
         Width           =   8625
         _ExtentX        =   15214
         _ExtentY        =   10557
         _Version        =   393216
         Style           =   1
         Tabs            =   6
         Tab             =   5
         TabsPerRow      =   6
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "DPetici�n"
         TabPicture(0)   =   "frmCONFINST.frx":144E
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "SSTabHabSub"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "DComunicaci�n de objetivos"
         TabPicture(1)   =   "frmCONFINST.frx":146A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Frame18"
         Tab(1).Control(1)=   "Frame17"
         Tab(1).Control(2)=   "Frame16"
         Tab(1).ControlCount=   3
         TabCaption(2)   =   "DComparativa"
         TabPicture(2)   =   "frmCONFINST.frx":1486
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Frame28"
         Tab(2).Control(1)=   "Frame4"
         Tab(2).Control(2)=   "Frame1"
         Tab(2).ControlCount=   3
         TabCaption(3)   =   "DAdjudicaci�n"
         TabPicture(3)   =   "frmCONFINST.frx":14A2
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "FrameAdj(3)"
         Tab(3).Control(1)=   "FrameAdj(1)"
         Tab(3).Control(2)=   "FrameAdj(2)"
         Tab(3).Control(3)=   "picAdj5"
         Tab(3).ControlCount=   4
         TabCaption(4)   =   "DNo adjudicaci�n"
         TabPicture(4)   =   "frmCONFINST.frx":14BE
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "Frame22"
         Tab(4).Control(1)=   "Frame23"
         Tab(4).ControlCount=   2
         TabCaption(5)   =   "Buz�n de ofertas"
         TabPicture(5)   =   "frmCONFINST.frx":14DA
         Tab(5).ControlEnabled=   -1  'True
         Tab(5).Control(0)=   "picBuzonOfe"
         Tab(5).Control(0).Enabled=   0   'False
         Tab(5).ControlCount=   1
         Begin VB.Frame Frame1 
            Caption         =   "DHoja comparativa de calidad"
            Height          =   1125
            Left            =   -74730
            TabIndex        =   364
            Top             =   3090
            Width           =   7965
            Begin VB.PictureBox picCompQA 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   780
               Left            =   45
               ScaleHeight     =   780
               ScaleWidth      =   7845
               TabIndex        =   365
               TabStop         =   0   'False
               Top             =   240
               Width           =   7845
               Begin VB.TextBox txtLongCabCompQA 
                  Height          =   285
                  Left            =   3150
                  TabIndex        =   51
                  Top             =   465
                  Width           =   615
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   43
                  Left            =   7500
                  Picture         =   "frmCONFINST.frx":14F6
                  Style           =   1  'Graphical
                  TabIndex        =   50
                  Top             =   60
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   42
                  Left            =   1035
                  MaxLength       =   255
                  TabIndex        =   49
                  Top             =   60
                  Width           =   6420
               End
               Begin VB.Label Label22 
                  Caption         =   "DComienzo de comparativa en la fila:"
                  Height          =   255
                  Left            =   90
                  TabIndex        =   367
                  Top             =   480
                  Width           =   3000
               End
               Begin VB.Label Label11 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Left            =   90
                  TabIndex        =   366
                  Top             =   120
                  Width           =   915
               End
            End
         End
         Begin VB.PictureBox picAdj5 
            BorderStyle     =   0  'None
            Height          =   1560
            Left            =   -74730
            ScaleHeight     =   1560
            ScaleWidth      =   8175
            TabIndex        =   233
            Top             =   500
            Width           =   8175
            Begin VB.CheckBox chkPlantAdj 
               Caption         =   "Plantilla para fichero adjunto de notificaci�n de adjudicaci�n"
               Height          =   240
               Index           =   1
               Left            =   90
               TabIndex        =   331
               Top             =   765
               Width           =   4700
            End
            Begin VB.Frame FrameAdj 
               Caption         =   " "
               Height          =   735
               Index           =   4
               Left            =   0
               TabIndex        =   237
               Top             =   780
               Width           =   7965
               Begin VB.PictureBox picAdj4 
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "Terminal"
                     Size            =   6
                     Charset         =   255
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   375
                  Left            =   120
                  ScaleHeight     =   375
                  ScaleWidth      =   7785
                  TabIndex        =   238
                  Top             =   240
                  Width           =   7785
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   38
                     Left            =   1860
                     TabIndex        =   54
                     Top             =   60
                     Width           =   5430
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   39
                     Left            =   7350
                     Picture         =   "frmCONFINST.frx":15B5
                     Style           =   1  'Graphical
                     TabIndex        =   55
                     Top             =   60
                     Width           =   315
                  End
                  Begin VB.Label Label20 
                     AutoSize        =   -1  'True
                     Caption         =   "DPlantilla:"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   239
                     Top             =   120
                     Width           =   705
                  End
               End
            End
            Begin VB.Frame FrameAdj 
               Height          =   735
               Index           =   0
               Left            =   0
               TabIndex        =   234
               Top             =   0
               Width           =   7965
               Begin VB.CheckBox chkPlantAdj 
                  Caption         =   "Plantilla para fichero adjunto de orden de compra"
                  Height          =   240
                  Index           =   0
                  Left            =   90
                  TabIndex        =   330
                  Top             =   0
                  Width           =   4700
               End
               Begin VB.PictureBox PicAdj1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  Enabled         =   0   'False
                  ForeColor       =   &H80000008&
                  Height          =   400
                  Left            =   120
                  ScaleHeight     =   405
                  ScaleWidth      =   7785
                  TabIndex        =   235
                  Top             =   240
                  Width           =   7785
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   9
                     Left            =   1860
                     MaxLength       =   255
                     TabIndex        =   52
                     Top             =   60
                     Width           =   5430
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   9
                     Left            =   7350
                     Picture         =   "frmCONFINST.frx":1674
                     Style           =   1  'Graphical
                     TabIndex        =   53
                     Top             =   60
                     Width           =   315
                  End
                  Begin VB.Label Label40 
                     Caption         =   "DPlantilla:"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   236
                     Top             =   120
                     Width           =   840
                  End
               End
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "DHoja comparativa"
            Height          =   1125
            Left            =   -74730
            TabIndex        =   225
            Top             =   1800
            Width           =   7965
            Begin VB.PictureBox picCompItem 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   780
               Left            =   45
               ScaleHeight     =   780
               ScaleWidth      =   7845
               TabIndex        =   226
               TabStop         =   0   'False
               Top             =   240
               Width           =   7845
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   37
                  Left            =   1035
                  MaxLength       =   255
                  TabIndex        =   46
                  Top             =   60
                  Width           =   6420
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   38
                  Left            =   7500
                  Picture         =   "frmCONFINST.frx":1733
                  Style           =   1  'Graphical
                  TabIndex        =   47
                  Top             =   60
                  Width           =   315
               End
               Begin VB.TextBox txtLongCabCompItem 
                  Height          =   285
                  Left            =   3150
                  TabIndex        =   48
                  Top             =   465
                  Width           =   615
               End
               Begin VB.Label Label5 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Left            =   90
                  TabIndex        =   228
                  Top             =   120
                  Width           =   915
               End
               Begin VB.Label Label7 
                  Caption         =   "DComienzo de comparativa en la fila:"
                  Height          =   255
                  Left            =   90
                  TabIndex        =   227
                  Top             =   480
                  Width           =   3000
               End
            End
         End
         Begin TabDlg.SSTab SSTabHabSub 
            Height          =   4200
            Left            =   -74865
            TabIndex        =   198
            Top             =   360
            Width           =   8340
            _ExtentX        =   14711
            _ExtentY        =   7408
            _Version        =   393216
            Style           =   1
            Tabs            =   2
            Tab             =   1
            TabHeight       =   520
            TabCaption(0)   =   "DPetici�n habitual"
            TabPicture(0)   =   "frmCONFINST.frx":17F2
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "Frame9"
            Tab(0).Control(1)=   "Frame10"
            Tab(0).Control(2)=   "Frame2"
            Tab(0).ControlCount=   3
            TabCaption(1)   =   "DModo subasta"
            TabPicture(1)   =   "frmCONFINST.frx":180E
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "fraModSub(2)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "fraModSub(1)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "fraModSub(0)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).ControlCount=   3
            Begin VB.Frame fraModSub 
               Caption         =   "DFormulario de oferta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   795
               Index           =   0
               Left            =   225
               TabIndex        =   218
               Top             =   495
               Width           =   7890
               Begin VB.PictureBox picModSub 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   555
                  Index           =   0
                  Left            =   135
                  ScaleHeight     =   555
                  ScaleWidth      =   7680
                  TabIndex        =   219
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7680
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   33
                     Left            =   2070
                     MaxLength       =   255
                     TabIndex        =   25
                     Top             =   120
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   37
                     Left            =   7350
                     Picture         =   "frmCONFINST.frx":182A
                     Style           =   1  'Graphical
                     TabIndex        =   26
                     Top             =   120
                     Width           =   315
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DPlantilla:"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   240
                     Index           =   0
                     Left            =   0
                     TabIndex        =   220
                     Top             =   165
                     Width           =   1905
                  End
               End
            End
            Begin VB.Frame fraModSub 
               Caption         =   "DPlantillas para peticiones de oferta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1575
               Index           =   1
               Left            =   225
               TabIndex        =   213
               Top             =   1440
               Width           =   7890
               Begin VB.PictureBox picModSub 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   1275
                  Index           =   1
                  Left            =   90
                  ScaleHeight     =   1275
                  ScaleWidth      =   7755
                  TabIndex        =   214
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7755
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   36
                     Left            =   7410
                     Picture         =   "frmCONFINST.frx":18E9
                     Style           =   1  'Graphical
                     TabIndex        =   28
                     Top             =   120
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   34
                     Left            =   2115
                     MaxLength       =   255
                     TabIndex        =   27
                     Top             =   120
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   35
                     Left            =   7410
                     Picture         =   "frmCONFINST.frx":19A8
                     Style           =   1  'Graphical
                     TabIndex        =   30
                     Top             =   540
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   35
                     Left            =   2115
                     MaxLength       =   255
                     TabIndex        =   29
                     Top             =   540
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   34
                     Left            =   7410
                     Picture         =   "frmCONFINST.frx":1A67
                     Style           =   1  'Graphical
                     TabIndex        =   32
                     Top             =   960
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   36
                     Left            =   2115
                     MaxLength       =   255
                     TabIndex        =   31
                     Top             =   960
                     Width           =   5235
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DPlantilla carta:"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   1
                     Left            =   60
                     TabIndex        =   217
                     Top             =   165
                     Width           =   2055
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DPlantilla e-mail: "
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   315
                     Index           =   2
                     Left            =   60
                     TabIndex        =   216
                     Top             =   585
                     Width           =   2055
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DPlantilla web:"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   315
                     Index           =   3
                     Left            =   60
                     TabIndex        =   215
                     Top             =   975
                     Width           =   2055
                  End
               End
            End
            Begin VB.Frame fraModSub 
               Caption         =   "DE-mail de petici�n de oferta"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   795
               Index           =   2
               Left            =   225
               TabIndex        =   210
               Top             =   3165
               Width           =   7890
               Begin VB.PictureBox picModSub 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   435
                  Index           =   2
                  Left            =   120
                  ScaleHeight     =   435
                  ScaleWidth      =   7665
                  TabIndex        =   211
                  TabStop         =   0   'False
                  Top             =   240
                  Width           =   7665
                  Begin VB.TextBox txtMailSubject 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   8
                     Left            =   1530
                     MaxLength       =   255
                     TabIndex        =   33
                     Top             =   60
                     Width           =   6135
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DAsunto:"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   255
                     Index           =   4
                     Left            =   0
                     TabIndex        =   212
                     Top             =   90
                     Width           =   1395
                  End
               End
            End
            Begin VB.Frame Frame2 
               Caption         =   "DE-mail de petici�n de oferta"
               Height          =   795
               Left            =   -74760
               TabIndex        =   207
               Top             =   3165
               Width           =   7890
               Begin VB.PictureBox picPetOfe3 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   465
                  Left            =   120
                  ScaleHeight     =   465
                  ScaleWidth      =   7740
                  TabIndex        =   208
                  TabStop         =   0   'False
                  Top             =   225
                  Width           =   7740
                  Begin VB.TextBox txtMailSubject 
                     Height          =   285
                     Index           =   0
                     Left            =   1530
                     MaxLength       =   255
                     TabIndex        =   24
                     Top             =   90
                     Width           =   6135
                  End
                  Begin VB.Label Label3 
                     Caption         =   "DAsunto:"
                     Height          =   255
                     Left            =   0
                     TabIndex        =   209
                     Top             =   120
                     Width           =   1035
                  End
               End
            End
            Begin VB.Frame Frame10 
               Caption         =   "DPlantillas para peticiones de oferta"
               Height          =   1575
               Left            =   -74760
               TabIndex        =   202
               Top             =   1440
               Width           =   7890
               Begin VB.PictureBox PicPetOfe2 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   1275
                  Left            =   90
                  ScaleHeight     =   1275
                  ScaleWidth      =   7755
                  TabIndex        =   203
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7755
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   1
                     Left            =   7410
                     Picture         =   "frmCONFINST.frx":1B26
                     Style           =   1  'Graphical
                     TabIndex        =   19
                     Top             =   120
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   1
                     Left            =   2115
                     MaxLength       =   255
                     TabIndex        =   18
                     Top             =   120
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   2
                     Left            =   7410
                     Picture         =   "frmCONFINST.frx":1BE5
                     Style           =   1  'Graphical
                     TabIndex        =   21
                     Top             =   540
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   2
                     Left            =   2115
                     MaxLength       =   255
                     TabIndex        =   20
                     Top             =   540
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   3
                     Left            =   7410
                     Picture         =   "frmCONFINST.frx":1CA4
                     Style           =   1  'Graphical
                     TabIndex        =   23
                     Top             =   960
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   3
                     Left            =   2115
                     MaxLength       =   255
                     TabIndex        =   22
                     Top             =   960
                     Width           =   5235
                  End
                  Begin VB.Label Label34 
                     Caption         =   "DPlantilla carta:"
                     Height          =   285
                     Left            =   60
                     TabIndex        =   206
                     Top             =   165
                     Width           =   1980
                  End
                  Begin VB.Label Label35 
                     Caption         =   "DPlantilla e-mail:"
                     Height          =   285
                     Left            =   60
                     TabIndex        =   205
                     Top             =   585
                     Width           =   1980
                  End
                  Begin VB.Label Label36 
                     Caption         =   "DPlantilla web:"
                     Height          =   285
                     Left            =   60
                     TabIndex        =   204
                     Top             =   975
                     Width           =   1980
                  End
               End
            End
            Begin VB.Frame Frame9 
               Caption         =   "DFormulario de Oferta"
               Height          =   795
               Left            =   -74775
               TabIndex        =   199
               Top             =   495
               Width           =   7890
               Begin VB.PictureBox picPetOfe1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   495
                  Left            =   135
                  ScaleHeight     =   495
                  ScaleWidth      =   7665
                  TabIndex        =   200
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7665
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   0
                     Left            =   7350
                     Picture         =   "frmCONFINST.frx":1D63
                     Style           =   1  'Graphical
                     TabIndex        =   17
                     Top             =   120
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   0
                     Left            =   2070
                     MaxLength       =   255
                     TabIndex        =   16
                     Top             =   120
                     Width           =   5235
                  End
                  Begin VB.Label Label33 
                     Caption         =   "DPlantilla:"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   201
                     Top             =   165
                     Width           =   1935
                  End
               End
            End
         End
         Begin VB.Frame Frame23 
            Caption         =   "DE-mail de NO Adjudicaci�n"
            Height          =   795
            Left            =   -74730
            TabIndex        =   174
            Top             =   1935
            Width           =   7965
            Begin VB.PictureBox PicAdjNo2 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               ForeColor       =   &H80000008&
               Height          =   435
               Left            =   120
               ScaleHeight     =   435
               ScaleWidth      =   7785
               TabIndex        =   175
               TabStop         =   0   'False
               Top             =   240
               Width           =   7785
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   3
                  Left            =   1320
                  MaxLength       =   255
                  TabIndex        =   67
                  Top             =   60
                  Width           =   6360
               End
               Begin VB.Label Label46 
                  Caption         =   "DAsunto:"
                  Height          =   210
                  Left            =   0
                  TabIndex        =   176
                  Top             =   120
                  Width           =   900
               End
            End
         End
         Begin VB.Frame Frame22 
            Caption         =   "DComunicaci�n de NO adjudicaci�n"
            Height          =   1155
            Left            =   -74730
            TabIndex        =   170
            Top             =   540
            Width           =   7965
            Begin VB.PictureBox PicAdjNo1 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   855
               Left            =   120
               ScaleHeight     =   855
               ScaleWidth      =   7785
               TabIndex        =   171
               Top             =   240
               Width           =   7785
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   14
                  Left            =   1860
                  MaxLength       =   255
                  TabIndex        =   65
                  Top             =   480
                  Width           =   5430
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   14
                  Left            =   7350
                  Picture         =   "frmCONFINST.frx":1E22
                  Style           =   1  'Graphical
                  TabIndex        =   66
                  Top             =   480
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   13
                  Left            =   1860
                  MaxLength       =   255
                  TabIndex        =   63
                  Top             =   60
                  Width           =   5430
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   13
                  Left            =   7350
                  Picture         =   "frmCONFINST.frx":1EE1
                  Style           =   1  'Graphical
                  TabIndex        =   64
                  Top             =   60
                  Width           =   315
               End
               Begin VB.Label Label51 
                  Caption         =   "DPlantilla e-mail:"
                  Height          =   270
                  Left            =   0
                  TabIndex        =   173
                  Top             =   540
                  Width           =   1590
               End
               Begin VB.Label Label50 
                  Caption         =   "DPlantilla carta:"
                  Height          =   285
                  Left            =   0
                  TabIndex        =   172
                  Top             =   75
                  Width           =   1590
               End
            End
         End
         Begin VB.Frame FrameAdj 
            Caption         =   "DE-mail de adjudicaci�n"
            Height          =   735
            Index           =   2
            Left            =   -74730
            TabIndex        =   167
            Top             =   3140
            Width           =   7965
            Begin VB.PictureBox PicAdj3 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   375
               Left            =   120
               ScaleHeight     =   375
               ScaleWidth      =   7815
               TabIndex        =   168
               TabStop         =   0   'False
               Top             =   240
               Width           =   7815
               Begin VB.TextBox txtMailSubject 
                  Height          =   315
                  Index           =   2
                  Left            =   1275
                  MaxLength       =   255
                  TabIndex        =   60
                  Top             =   60
                  Width           =   6390
               End
               Begin VB.Label Label43 
                  Caption         =   "DAsunto:"
                  Height          =   255
                  Left            =   0
                  TabIndex        =   169
                  Top             =   120
                  Width           =   840
               End
            End
         End
         Begin VB.Frame FrameAdj 
            Caption         =   "DPlantillas para mensaje de comunicaci�n de adjudicaci�n "
            Height          =   1060
            Index           =   1
            Left            =   -74730
            TabIndex        =   163
            Top             =   2035
            Width           =   7965
            Begin VB.PictureBox PicAdj2 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   780
               Left            =   120
               ScaleHeight     =   780
               ScaleWidth      =   7755
               TabIndex        =   164
               Top             =   240
               Width           =   7755
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   11
                  Left            =   1890
                  MaxLength       =   255
                  TabIndex        =   58
                  Top             =   440
                  Width           =   5430
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   11
                  Left            =   7350
                  Picture         =   "frmCONFINST.frx":1FA0
                  Style           =   1  'Graphical
                  TabIndex        =   59
                  Top             =   440
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   10
                  Left            =   1890
                  MaxLength       =   255
                  TabIndex        =   56
                  Top             =   60
                  Width           =   5430
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   10
                  Left            =   7350
                  Picture         =   "frmCONFINST.frx":205F
                  Style           =   1  'Graphical
                  TabIndex        =   57
                  Top             =   60
                  Width           =   315
               End
               Begin VB.Label Label41 
                  Caption         =   "DPlantilla e-mail:"
                  Height          =   210
                  Left            =   0
                  TabIndex        =   166
                  Top             =   500
                  Width           =   1530
               End
               Begin VB.Label Label42 
                  Caption         =   "DPlantilla carta:"
                  Height          =   195
                  Left            =   0
                  TabIndex        =   165
                  Top             =   120
                  Width           =   1530
               End
            End
         End
         Begin VB.Frame FrameAdj 
            Caption         =   "DHoja de adjudicaci�n directa"
            Height          =   735
            Index           =   3
            Left            =   -74730
            TabIndex        =   159
            Top             =   3920
            Width           =   7965
            Begin VB.CommandButton cmdPlantilla 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   20
               Left            =   6060
               Picture         =   "frmCONFINST.frx":211E
               Style           =   1  'Graphical
               TabIndex        =   162
               Top             =   840
               Width           =   375
            End
            Begin VB.PictureBox picAdjDir 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   375
               Left            =   120
               ScaleHeight     =   375
               ScaleWidth      =   7815
               TabIndex        =   160
               Top             =   240
               Width           =   7815
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   12
                  Left            =   7350
                  Picture         =   "frmCONFINST.frx":2460
                  Style           =   1  'Graphical
                  TabIndex        =   62
                  Top             =   60
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   12
                  Left            =   1860
                  MaxLength       =   255
                  TabIndex        =   61
                  Top             =   60
                  Width           =   5430
               End
               Begin VB.Label Label52 
                  Caption         =   "DPlantilla:"
                  Height          =   270
                  Left            =   15
                  TabIndex        =   161
                  Top             =   120
                  Width           =   810
               End
            End
         End
         Begin VB.Frame Frame28 
            Caption         =   "DHoja comparativa"
            Height          =   1125
            Left            =   -74730
            TabIndex        =   155
            Top             =   540
            Width           =   7965
            Begin VB.PictureBox picComp 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   780
               Left            =   45
               ScaleHeight     =   780
               ScaleWidth      =   7815
               TabIndex        =   156
               TabStop         =   0   'False
               Top             =   240
               Width           =   7815
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   8
                  Left            =   1035
                  MaxLength       =   255
                  TabIndex        =   43
                  Top             =   60
                  Width           =   6420
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   8
                  Left            =   7500
                  Picture         =   "frmCONFINST.frx":251F
                  Style           =   1  'Graphical
                  TabIndex        =   44
                  Top             =   60
                  Width           =   315
               End
               Begin VB.TextBox txtLongCabcomp 
                  Height          =   285
                  Left            =   3150
                  TabIndex        =   45
                  Top             =   465
                  Width           =   615
               End
               Begin VB.Label Label53 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Left            =   60
                  TabIndex        =   158
                  Top             =   120
                  Width           =   915
               End
               Begin VB.Label Label1 
                  Caption         =   "DComienzo de comparativa en la fila:"
                  Height          =   255
                  Left            =   90
                  TabIndex        =   157
                  Top             =   480
                  Width           =   3000
               End
            End
         End
         Begin VB.Frame Frame16 
            Caption         =   "DFormulario de oferta"
            Height          =   855
            Left            =   -74730
            TabIndex        =   152
            Top             =   540
            Width           =   7965
            Begin VB.PictureBox picObj1 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   555
               Left            =   120
               ScaleHeight     =   555
               ScaleWidth      =   7785
               TabIndex        =   153
               TabStop         =   0   'False
               Top             =   180
               Width           =   7785
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   4
                  Left            =   1860
                  MaxLength       =   255
                  TabIndex        =   34
                  Top             =   135
                  Width           =   5430
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   4
                  Left            =   7350
                  Picture         =   "frmCONFINST.frx":25DE
                  Style           =   1  'Graphical
                  TabIndex        =   35
                  Top             =   150
                  Width           =   315
               End
               Begin VB.Label label2 
                  Caption         =   "DPlantilla:"
                  Height          =   195
                  Left            =   15
                  TabIndex        =   154
                  Top             =   180
                  Width           =   855
               End
            End
         End
         Begin VB.Frame Frame17 
            Caption         =   "DPlantillas para comunicaci�n de objetivos"
            Height          =   1575
            Left            =   -74730
            TabIndex        =   147
            Top             =   1440
            Width           =   7965
            Begin VB.PictureBox picObj2 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   1275
               Left            =   120
               ScaleHeight     =   1275
               ScaleWidth      =   7815
               TabIndex        =   148
               TabStop         =   0   'False
               Top             =   180
               Width           =   7815
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   5
                  Left            =   7350
                  Picture         =   "frmCONFINST.frx":269D
                  Style           =   1  'Graphical
                  TabIndex        =   37
                  Top             =   120
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   5
                  Left            =   1860
                  MaxLength       =   255
                  TabIndex        =   36
                  Top             =   120
                  Width           =   5430
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   6
                  Left            =   7350
                  Picture         =   "frmCONFINST.frx":275C
                  Style           =   1  'Graphical
                  TabIndex        =   39
                  Top             =   540
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   6
                  Left            =   1860
                  MaxLength       =   255
                  TabIndex        =   38
                  Top             =   540
                  Width           =   5430
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   7
                  Left            =   7350
                  Picture         =   "frmCONFINST.frx":281B
                  Style           =   1  'Graphical
                  TabIndex        =   41
                  Top             =   960
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   7
                  Left            =   1860
                  MaxLength       =   255
                  TabIndex        =   40
                  Top             =   960
                  Width           =   5430
               End
               Begin VB.Label Label12 
                  Caption         =   "DPlantilla carta:"
                  Height          =   210
                  Left            =   15
                  TabIndex        =   151
                  Top             =   180
                  Width           =   1545
               End
               Begin VB.Label Label13 
                  Caption         =   "DPlantilla e-mail:"
                  Height          =   240
                  Left            =   15
                  TabIndex        =   150
                  Top             =   600
                  Width           =   1545
               End
               Begin VB.Label Label14 
                  Caption         =   "DPlantilla web:"
                  Height          =   255
                  Left            =   15
                  TabIndex        =   149
                  Top             =   975
                  Width           =   1545
               End
            End
         End
         Begin VB.Frame Frame18 
            Caption         =   "DE-mail de comunicaci�n de objetivos"
            Height          =   795
            Left            =   -74715
            TabIndex        =   144
            Top             =   3060
            Width           =   7965
            Begin VB.PictureBox picObj3 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   435
               Left            =   120
               ScaleHeight     =   435
               ScaleWidth      =   7785
               TabIndex        =   145
               TabStop         =   0   'False
               Top             =   240
               Width           =   7785
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   1
                  Left            =   1170
                  MaxLength       =   255
                  TabIndex        =   42
                  Top             =   60
                  Width           =   6465
               End
               Begin VB.Label Label15 
                  Caption         =   "DAsunto:"
                  Height          =   210
                  Left            =   15
                  TabIndex        =   146
                  Top             =   120
                  Width           =   1065
               End
            End
         End
         Begin VB.PictureBox picBuzonOfe 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   5475
            Left            =   120
            ScaleHeight     =   5475
            ScaleWidth      =   8325
            TabIndex        =   137
            Top             =   480
            Width           =   8325
            Begin VB.Frame frmAvisoOfertas 
               Caption         =   "DAviso de recepci�n de ofertas del portal"
               Height          =   970
               Left            =   270
               TabIndex        =   361
               Top             =   4320
               Width           =   7890
               Begin VB.CheckBox chkCompAsig 
                  Caption         =   "DSi soy el comprador asignado"
                  Height          =   375
                  Left            =   4080
                  TabIndex        =   363
                  Top             =   360
                  Width           =   3375
               End
               Begin VB.CheckBox chkCompResp 
                  Caption         =   "DSi soy el comprador responsable"
                  Height          =   375
                  Left            =   360
                  TabIndex        =   362
                  Top             =   360
                  Width           =   3375
               End
            End
            Begin VB.Frame fraNumDecRecepcion 
               Caption         =   "DN�mero de decimales a mostrar por defecto en recepci�n de ofertas"
               Height          =   970
               Left            =   270
               TabIndex        =   243
               Top             =   3240
               Width           =   7890
               Begin MSComCtl2.UpDown UpDownDec 
                  Height          =   310
                  Left            =   2730
                  TabIndex        =   74
                  Top             =   360
                  Width           =   240
                  _ExtentX        =   423
                  _ExtentY        =   556
                  _Version        =   393216
                  AutoBuddy       =   -1  'True
                  BuddyControl    =   "txtNumDecRecep"
                  BuddyDispid     =   196784
                  OrigLeft        =   2770
                  OrigTop         =   425
                  OrigRight       =   3010
                  OrigBottom      =   740
                  Max             =   20
                  Enabled         =   -1  'True
               End
               Begin VB.TextBox txtNumDecRecep 
                  Height          =   285
                  Left            =   2400
                  TabIndex        =   73
                  Top             =   390
                  Width           =   315
               End
               Begin VB.Label lblNumDecRecep 
                  Caption         =   "DN�mero de decimales:"
                  Height          =   300
                  Left            =   315
                  TabIndex        =   244
                  Top             =   450
                  Width           =   2000
               End
            End
            Begin VB.Frame fraBuzPeriodo 
               Caption         =   "Periodo de fechas a mostrar por defecto en buz�n de ofertas"
               Height          =   970
               Left            =   270
               TabIndex        =   142
               Top             =   60
               Width           =   7890
               Begin SSDataWidgets_B.SSDBCombo sdbcAntigOfer 
                  Height          =   285
                  Left            =   3735
                  TabIndex        =   68
                  Top             =   390
                  Width           =   2565
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  MaxDropDownItems=   9
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   -2147483640
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Caption=   "PERIODO"
                  Columns(0).Name =   "PERIODO"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "BUZPER"
                  Columns(1).Name =   "BUZPER"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4524
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblbuzperiodo 
                  Caption         =   "Antig�edad de las ofertas a mostrar:"
                  Height          =   300
                  Left            =   315
                  TabIndex        =   143
                  Top             =   450
                  Width           =   3375
               End
            End
            Begin VB.Frame fraLeerOfer 
               Caption         =   "Mostrar por defecto los siguientes datos"
               Height          =   970
               Left            =   270
               TabIndex        =   141
               Top             =   1120
               Width           =   7890
               Begin VB.OptionButton opLeerTodas 
                  Caption         =   "Todas"
                  Height          =   300
                  Left            =   5500
                  TabIndex        =   71
                  Top             =   375
                  Width           =   2055
               End
               Begin VB.OptionButton opNoLeidas 
                  Caption         =   "S�lo no le�das"
                  Height          =   300
                  Left            =   2782
                  TabIndex        =   70
                  Top             =   375
                  Width           =   2350
               End
               Begin VB.OptionButton opLeidas 
                  Caption         =   "S�lo le�das"
                  Height          =   300
                  Left            =   390
                  TabIndex        =   69
                  Top             =   375
                  Width           =   2055
               End
            End
            Begin VB.Frame fraComprobarOfer 
               Caption         =   "Intervalo de comprobaci�n de nuevas ofertas"
               Height          =   970
               Left            =   270
               TabIndex        =   138
               Top             =   2180
               Width           =   7890
               Begin VB.TextBox txtMinutos 
                  Height          =   285
                  Left            =   4125
                  TabIndex        =   72
                  Top             =   450
                  Width           =   765
               End
               Begin VB.Label lblMinutos 
                  Caption         =   "minutos"
                  Height          =   270
                  Left            =   5325
                  TabIndex        =   140
                  Top             =   450
                  Width           =   720
               End
               Begin VB.Label lblComprobarOfer 
                  Caption         =   "Comprobar llegada de nuevas ofertas cada:"
                  Height          =   300
                  Left            =   115
                  TabIndex        =   139
                  Top             =   450
                  Width           =   3700
               End
            End
         End
      End
      Begin TabDlg.SSTab TabPedidos 
         Height          =   5775
         Left            =   -74820
         TabIndex        =   177
         Top             =   390
         Width           =   11805
         _ExtentX        =   20823
         _ExtentY        =   10186
         _Version        =   393216
         Style           =   1
         Tabs            =   6
         TabsPerRow      =   6
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "DNotificaci�n de emisi�n"
         TabPicture(0)   =   "frmCONFINST.frx":28DA
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "picNotifPed"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "DNotificaci�n de aceptaci�n"
         TabPicture(1)   =   "frmCONFINST.frx":28F6
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "picNotifAcepPic"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "dAnulaci�n"
         TabPicture(2)   =   "frmCONFINST.frx":2912
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "picAnulaPed"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "dRecepci�n"
         TabPicture(3)   =   "frmCONFINST.frx":292E
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "PicRecepPed"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "dIm�genes y atributos para cat�logo"
         TabPicture(4)   =   "frmCONFINST.frx":294A
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "picExternosPed"
         Tab(4).ControlCount=   1
         TabCaption(5)   =   "DValores por defecto de atributos"
         TabPicture(5)   =   "frmCONFINST.frx":2966
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "sdbgCamposPedido"
         Tab(5).Control(1)=   "sdbddValor"
         Tab(5).Control(2)=   "picAtribDefecto"
         Tab(5).ControlCount=   3
         Begin VB.PictureBox picAtribDefecto 
            BorderStyle     =   0  'None
            Height          =   300
            Left            =   -74760
            ScaleHeight     =   300
            ScaleWidth      =   9975
            TabIndex        =   396
            Top             =   480
            Width           =   9975
            Begin VB.CommandButton cmdAtrAdd 
               Height          =   312
               Index           =   1
               Left            =   7750
               Picture         =   "frmCONFINST.frx":2982
               Style           =   1  'Graphical
               TabIndex        =   398
               Top             =   0
               Width           =   432
            End
            Begin VB.CommandButton cmdAtrAdd 
               Height          =   312
               Index           =   0
               Left            =   7250
               Picture         =   "frmCONFINST.frx":2A14
               Style           =   1  'Graphical
               TabIndex        =   397
               Top             =   0
               Width           =   432
            End
         End
         Begin VB.PictureBox picNotifAcepPic 
            BorderStyle     =   0  'None
            Height          =   3615
            Left            =   -74880
            ScaleHeight     =   3615
            ScaleWidth      =   8745
            TabIndex        =   313
            Top             =   480
            Width           =   8745
            Begin VB.Frame fraNotifAcep 
               Caption         =   "DEmail de aceptaci�n"
               Height          =   855
               Left            =   270
               TabIndex        =   320
               Top             =   1200
               Width           =   8055
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   9
                  Left            =   1365
                  MaxLength       =   255
                  TabIndex        =   322
                  Top             =   360
                  Width           =   6525
               End
               Begin VB.Label label10 
                  Caption         =   "DAsunto:"
                  Height          =   255
                  Index           =   8
                  Left            =   150
                  TabIndex        =   323
                  Top             =   390
                  Width           =   975
               End
            End
            Begin VB.Frame frmNotifPed 
               Caption         =   "DPlantillas para notificaci�n de aceptaci�n de pedido"
               Height          =   1095
               Left            =   270
               TabIndex        =   314
               Top             =   90
               Width           =   7965
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   40
                  Left            =   1515
                  MaxLength       =   255
                  TabIndex        =   315
                  Top             =   300
                  Width           =   5925
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   41
                  Left            =   7485
                  Picture         =   "frmCONFINST.frx":2A96
                  Style           =   1  'Graphical
                  TabIndex        =   317
                  Top             =   300
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   41
                  Left            =   1515
                  MaxLength       =   255
                  TabIndex        =   319
                  Top             =   660
                  Width           =   5925
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   42
                  Left            =   7485
                  Picture         =   "frmCONFINST.frx":2B55
                  Style           =   1  'Graphical
                  TabIndex        =   321
                  Top             =   660
                  Width           =   315
               End
               Begin VB.Label LblPedMail 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   5
                  Left            =   150
                  TabIndex        =   318
                  Top             =   690
                  Width           =   1335
               End
               Begin VB.Label LblPedCarta 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   4
                  Left            =   150
                  TabIndex        =   316
                  Top             =   330
                  Width           =   1395
               End
            End
         End
         Begin VB.PictureBox picAnulaPed 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   2775
            Left            =   -74880
            ScaleHeight     =   2775
            ScaleWidth      =   8295
            TabIndex        =   302
            Top             =   480
            Width           =   8295
            Begin VB.Frame frmAnuPed 
               Caption         =   "dPlantillas para anulaci�n de pedidos"
               Height          =   1095
               Left            =   270
               TabIndex        =   306
               Top             =   90
               Width           =   7965
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   24
                  Left            =   7515
                  Picture         =   "frmCONFINST.frx":2C14
                  Style           =   1  'Graphical
                  TabIndex        =   310
                  Top             =   660
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   23
                  Left            =   1545
                  MaxLength       =   255
                  TabIndex        =   309
                  Top             =   660
                  Width           =   5925
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   23
                  Left            =   7515
                  Picture         =   "frmCONFINST.frx":2CD3
                  Style           =   1  'Graphical
                  TabIndex        =   308
                  Top             =   300
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   22
                  Left            =   1545
                  MaxLength       =   255
                  TabIndex        =   307
                  Top             =   300
                  Width           =   5925
               End
               Begin VB.Label LblPedCarta 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   1
                  Left            =   150
                  TabIndex        =   312
                  Top             =   330
                  Width           =   1395
               End
               Begin VB.Label LblPedMail 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   1
                  Left            =   150
                  TabIndex        =   311
                  Top             =   690
                  Width           =   1335
               End
            End
            Begin VB.Frame fraAsuntoAnuPed 
               Caption         =   "DE-mail de anulaciones"
               Height          =   795
               Left            =   270
               TabIndex        =   303
               Top             =   1320
               Width           =   7965
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   5
                  Left            =   1365
                  MaxLength       =   255
                  TabIndex        =   304
                  Top             =   360
                  Width           =   6435
               End
               Begin VB.Label label10 
                  Caption         =   "DAsunto:"
                  Height          =   255
                  Index           =   9
                  Left            =   150
                  TabIndex        =   305
                  Top             =   390
                  Width           =   1185
               End
            End
         End
         Begin VB.PictureBox PicRecepPed 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   3735
            Left            =   -74880
            ScaleHeight     =   3735
            ScaleWidth      =   8445
            TabIndex        =   281
            Top             =   360
            Width           =   8445
            Begin VB.Frame fraAsuntoRecPed 
               Caption         =   "DE-mail de recepciones"
               Height          =   735
               Left            =   270
               TabIndex        =   298
               Top             =   2940
               Width           =   7965
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   4
                  Left            =   1335
                  MaxLength       =   255
                  TabIndex        =   299
                  Top             =   300
                  Width           =   6525
               End
               Begin VB.Label label10 
                  Caption         =   "DAsunto:"
                  Height          =   255
                  Index           =   10
                  Left            =   150
                  TabIndex        =   300
                  Top             =   315
                  Width           =   1095
               End
            End
            Begin VB.Frame fraRecepKOPed 
               Caption         =   "dErr�nea"
               Height          =   1095
               Left            =   270
               TabIndex        =   291
               Top             =   1770
               Width           =   7965
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   29
                  Left            =   1695
                  MaxLength       =   255
                  TabIndex        =   295
                  Top             =   300
                  Width           =   5805
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   30
                  Left            =   7545
                  Picture         =   "frmCONFINST.frx":2D92
                  Style           =   1  'Graphical
                  TabIndex        =   294
                  Top             =   300
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   28
                  Left            =   1695
                  MaxLength       =   255
                  TabIndex        =   293
                  Top             =   660
                  Width           =   5805
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   29
                  Left            =   7545
                  Picture         =   "frmCONFINST.frx":2E51
                  Style           =   1  'Graphical
                  TabIndex        =   292
                  Top             =   660
                  Width           =   315
               End
               Begin VB.Label LblPedCarta 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   3
                  Left            =   150
                  TabIndex        =   297
                  Top             =   330
                  Width           =   1455
               End
               Begin VB.Label LblPedMail 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   3
                  Left            =   150
                  TabIndex        =   296
                  Top             =   690
                  Width           =   1485
               End
            End
            Begin VB.Frame fraRecepOKPed 
               Caption         =   "Correcta"
               Height          =   1095
               Left            =   270
               TabIndex        =   284
               Top             =   600
               Width           =   7965
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   27
                  Left            =   1695
                  MaxLength       =   255
                  TabIndex        =   288
                  Top             =   300
                  Width           =   5805
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   28
                  Left            =   7545
                  Picture         =   "frmCONFINST.frx":2F10
                  Style           =   1  'Graphical
                  TabIndex        =   287
                  Top             =   300
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   26
                  Left            =   1695
                  MaxLength       =   255
                  TabIndex        =   286
                  Top             =   660
                  Width           =   5805
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   27
                  Left            =   7545
                  Picture         =   "frmCONFINST.frx":2FCF
                  Style           =   1  'Graphical
                  TabIndex        =   285
                  Top             =   660
                  Width           =   315
               End
               Begin VB.Label LblPedCarta 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   2
                  Left            =   150
                  TabIndex        =   290
                  Top             =   330
                  Width           =   1395
               End
               Begin VB.Label LblPedMail 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   2
                  Left            =   150
                  TabIndex        =   289
                  Top             =   690
                  Width           =   1335
               End
            End
            Begin VB.CommandButton cmdPlantilla 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   32
               Left            =   7875
               Picture         =   "frmCONFINST.frx":308E
               Style           =   1  'Graphical
               TabIndex        =   283
               Top             =   240
               Width           =   315
            End
            Begin VB.TextBox txtPlantilla 
               Alignment       =   1  'Right Justify
               Height          =   285
               Index           =   31
               Left            =   1980
               MaxLength       =   255
               TabIndex        =   282
               Top             =   240
               Width           =   5850
            End
            Begin VB.Label LblCartaPed 
               Caption         =   "DPlantilla recepci�n:"
               Height          =   255
               Index           =   5
               Left            =   285
               TabIndex        =   301
               Top             =   240
               Width           =   1725
            End
         End
         Begin VB.PictureBox picExternosPed 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   2775
            Left            =   -74880
            ScaleHeight     =   2775
            ScaleWidth      =   8385
            TabIndex        =   270
            Top             =   480
            Width           =   8385
            Begin VB.Frame frmDatExt 
               Caption         =   "dPlantilla para petici�n de datos externos"
               Height          =   1335
               Left            =   270
               TabIndex        =   274
               Top             =   240
               Width           =   7965
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   26
                  Left            =   7500
                  Picture         =   "frmCONFINST.frx":314D
                  Style           =   1  'Graphical
                  TabIndex        =   278
                  Top             =   720
                  Width           =   315
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   25
                  Left            =   7500
                  Picture         =   "frmCONFINST.frx":320C
                  Style           =   1  'Graphical
                  TabIndex        =   277
                  Top             =   360
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   25
                  Left            =   1470
                  MaxLength       =   255
                  TabIndex        =   276
                  Top             =   720
                  Width           =   5985
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   24
                  Left            =   1470
                  MaxLength       =   255
                  TabIndex        =   275
                  Top             =   360
                  Width           =   5985
               End
               Begin VB.Label label10 
                  Caption         =   "DXSL "
                  Height          =   255
                  Index           =   11
                  Left            =   150
                  TabIndex        =   280
                  Top             =   750
                  Width           =   1275
               End
               Begin VB.Label LblPedMail 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   4
                  Left            =   150
                  TabIndex        =   279
                  Top             =   390
                  Width           =   1335
               End
            End
            Begin VB.Frame fraDatosExtSubject 
               Caption         =   "DE-mail de notificaciones"
               Height          =   915
               Left            =   270
               TabIndex        =   271
               Top             =   1720
               Width           =   7965
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   7
                  Left            =   1095
                  MaxLength       =   255
                  TabIndex        =   272
                  Top             =   360
                  Width           =   6705
               End
               Begin VB.Label label10 
                  Caption         =   "DAsunto:"
                  Height          =   255
                  Index           =   12
                  Left            =   120
                  TabIndex        =   273
                  Top             =   420
                  Width           =   1035
               End
            End
         End
         Begin VB.PictureBox picNotifPed 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   5325
            Left            =   120
            ScaleHeight     =   5325
            ScaleWidth      =   8235
            TabIndex        =   178
            Top             =   360
            Width           =   8235
            Begin VB.Frame fraNotiPed 
               Caption         =   "DEmail de emisiones CCO"
               Height          =   1605
               Index           =   3
               Left            =   270
               TabIndex        =   392
               Top             =   3720
               Width           =   7965
               Begin VB.CheckBox chkNotifPed 
                  Caption         =   "DActivar CC en el e-mail de emisi�n de pedido"
                  Height          =   225
                  Index           =   2
                  Left            =   150
                  TabIndex        =   394
                  Top             =   270
                  Width           =   4215
               End
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   10
                  Left            =   1695
                  MaxLength       =   255
                  TabIndex        =   95
                  Top             =   570
                  Width           =   6135
               End
               Begin VB.CheckBox chkNotifPed 
                  Caption         =   "DIncluir al Peticionario en CC"
                  Height          =   255
                  Index           =   0
                  Left            =   120
                  TabIndex        =   96
                  Top             =   990
                  Width           =   6675
               End
               Begin VB.CheckBox chkNotifPed 
                  Caption         =   "DIncluir al Comprador en CC"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   97
                  Top             =   1290
                  Width           =   6675
               End
               Begin VB.Line Line2 
                  BorderColor     =   &H80000000&
                  BorderStyle     =   6  'Inside Solid
                  Index           =   1
                  X1              =   90
                  X2              =   7850
                  Y1              =   900
                  Y2              =   900
               End
               Begin VB.Label label10 
                  Caption         =   "DLista e-mails CC.:"
                  Height          =   255
                  Index           =   5
                  Left            =   150
                  TabIndex        =   393
                  Top             =   585
                  Width           =   1725
               End
            End
            Begin VB.Frame fraDecComparativa 
               Caption         =   "DN�mero de decimales en la comparativa y en el panel de toma de decisiones"
               Height          =   745
               Left            =   270
               TabIndex        =   240
               Top             =   2955
               Width           =   7965
               Begin VB.PictureBox picPonderacion 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   445
                  Left            =   120
                  ScaleHeight     =   450
                  ScaleWidth      =   6255
                  TabIndex        =   241
                  Top             =   200
                  Width           =   6255
                  Begin VB.TextBox txtNumDec 
                     Height          =   285
                     Left            =   1560
                     TabIndex        =   94
                     Top             =   160
                     Width           =   1935
                  End
                  Begin VB.Label label10 
                     Caption         =   "DN� decimales:"
                     Height          =   255
                     Index           =   6
                     Left            =   0
                     TabIndex        =   242
                     Top             =   220
                     Width           =   1815
                  End
               End
            End
            Begin VB.Frame FraForPedido 
               Caption         =   "DFormulario pedido"
               Height          =   1095
               Left            =   270
               TabIndex        =   184
               Top             =   -15
               Width           =   7965
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   32
                  Left            =   1680
                  MaxLength       =   255
                  TabIndex        =   87
                  Top             =   680
                  Width           =   5775
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   33
                  Left            =   7500
                  Picture         =   "frmCONFINST.frx":32CB
                  Style           =   1  'Graphical
                  TabIndex        =   88
                  Top             =   680
                  Width           =   315
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   31
                  Left            =   7500
                  Picture         =   "frmCONFINST.frx":338A
                  Style           =   1  'Graphical
                  TabIndex        =   86
                  Top             =   270
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   30
                  Left            =   1680
                  MaxLength       =   255
                  TabIndex        =   85
                  Top             =   270
                  Width           =   5775
               End
               Begin VB.Label LblCartaPed 
                  Caption         =   "DPlantilla EXCELL:"
                  Height          =   255
                  Index           =   0
                  Left            =   120
                  TabIndex        =   186
                  Top             =   695
                  Width           =   1365
               End
               Begin VB.Label LblCartaPed 
                  Caption         =   "DPlantilla WORD:"
                  Height          =   255
                  Index           =   4
                  Left            =   120
                  TabIndex        =   185
                  Top             =   285
                  Width           =   1485
               End
            End
            Begin VB.Frame fraAsuntoEmiPed 
               Caption         =   "DE-mail de notificaciones"
               Height          =   745
               Left            =   270
               TabIndex        =   182
               Top             =   2190
               Width           =   7965
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   6
                  Left            =   1485
                  MaxLength       =   255
                  TabIndex        =   93
                  Top             =   360
                  Width           =   6345
               End
               Begin VB.Label label10 
                  Caption         =   "DAsunto:"
                  Height          =   255
                  Index           =   7
                  Left            =   120
                  TabIndex        =   183
                  Top             =   420
                  Width           =   1035
               End
            End
            Begin VB.Frame frmNotiPed 
               Caption         =   "dPlantillas para notificaci�n de pedidos"
               Height          =   1095
               Left            =   270
               TabIndex        =   179
               Top             =   1095
               Width           =   7965
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   21
                  Left            =   1680
                  MaxLength       =   255
                  TabIndex        =   89
                  Top             =   300
                  Width           =   5775
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   22
                  Left            =   7500
                  Picture         =   "frmCONFINST.frx":3449
                  Style           =   1  'Graphical
                  TabIndex        =   90
                  Top             =   300
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   20
                  Left            =   1680
                  MaxLength       =   255
                  TabIndex        =   91
                  Top             =   660
                  Width           =   5775
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   21
                  Left            =   7500
                  Picture         =   "frmCONFINST.frx":3508
                  Style           =   1  'Graphical
                  TabIndex        =   92
                  Top             =   660
                  Width           =   315
               End
               Begin VB.Label LblPedCarta 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   0
                  Left            =   120
                  TabIndex        =   181
                  Top             =   300
                  Width           =   1515
               End
               Begin VB.Label LblPedMail 
                  Caption         =   "DE-Mail:"
                  Height          =   195
                  Index           =   0
                  Left            =   120
                  TabIndex        =   180
                  Top             =   720
                  Width           =   1335
               End
            End
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
            Height          =   915
            Left            =   -69000
            TabIndex        =   399
            Top             =   2640
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCONFINST.frx":35C7
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
            BackColor       =   8421376
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgCamposPedido 
            Height          =   4695
            Left            =   -74760
            TabIndex        =   400
            Top             =   840
            Width           =   8175
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   10
            stylesets.count =   3
            stylesets(0).Name=   "Gris"
            stylesets(0).BackColor=   12632256
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCONFINST.frx":35E3
            stylesets(1).Name=   "Normal"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   16777215
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCONFINST.frx":35FF
            stylesets(2).Name=   "Selected"
            stylesets(2).ForeColor=   16777215
            stylesets(2).BackColor=   8388608
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmCONFINST.frx":361B
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeRow   =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   -2147483630
            ForeColorOdd    =   -2147483630
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            Columns.Count   =   10
            Columns(0).Width=   3016
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16776960
            Columns(1).Width=   6800
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16776960
            Columns(2).Width=   3519
            Columns(2).Caption=   "Valor"
            Columns(2).Name =   "VAL"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "TIPO_INTRODUCCION"
            Columns(3).Name =   "TIPO_INTRODUCCION"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "TIPO_DATOS"
            Columns(4).Name =   "TIPO_DATOS"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "MIN"
            Columns(5).Name =   "MIN"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "MAX"
            Columns(6).Name =   "MAX"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "ATRIBID"
            Columns(7).Name =   "ATRIBID"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "ESINTEGRA"
            Columns(8).Name =   "ESINTEGRA"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "LISTA_EXTERNA"
            Columns(9).Name =   "LISTA_EXTERNA"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            _ExtentX        =   14420
            _ExtentY        =   8281
            _StockProps     =   79
            BackColor       =   -2147483643
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   12420
      TabIndex        =   195
      TabStop         =   0   'False
      Top             =   6780
      Width           =   12420
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "DCancelar"
         Height          =   345
         Left            =   4680
         TabIndex        =   197
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "D&Aceptar"
         Height          =   345
         Left            =   3540
         TabIndex        =   196
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
   End
   Begin MSComDlg.CommonDialog cdlArchivo 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "DSeleccione plantilla"
      Filter          =   "Plantillas de Word (*.dot)|*.dot"
   End
   Begin MSComDlg.CommonDialog cdlFondo 
      Left            =   0
      Top             =   600
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "DSeleccione imagen"
      Filter          =   "Im�genes (*.bmp;*.gif;*.jpg)|*.bmp;*.gif;*.jpg"
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   12420
      TabIndex        =   115
      TabStop         =   0   'False
      Top             =   7215
      Width           =   12420
      Begin VB.CommandButton cmdEdicion 
         Caption         =   "D&Edici�n"
         Height          =   345
         Left            =   8640
         TabIndex        =   0
         Top             =   90
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmCONFINST"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Valores par�metros subasta
Private m_iSubTipo As Integer
Private m_iSubModo As Integer
Private m_vSubDuracion As Variant
Private m_iSubastaEspera As Integer
Private m_bSubPublicar As Boolean
Private m_bSubNotifEventos As Boolean
Private m_bSubastaProve As Boolean
Private m_bSubVerDesdePrimPuja As Boolean
Private m_bSubastaPrecioPuja As Boolean
Private m_bSubastaPujas As Boolean
Private m_bSubastaBajMinPuja As Boolean
Private m_iSubBajMinGanTipo As Integer
Private m_vSubBajMinGanProcVal As Variant
Private m_vSubBajMinGanGrupoVal As Variant
Private m_vSubBajMinGanItemVal As Variant
Private m_bSubBajMinProve As Boolean
Private m_iSubBajMinProveTipo As Integer
Private m_vSubBajMinProveProcVal As Variant
Private m_vSubBajMinProveGrupoVal As Variant
Private m_vSubBajMinProveItemVal As Variant
Private m_dcSubTextosFin As Dictionary

Private oFos As FileSystemObject
Private m_bMonRespetarCombo As Boolean
Private m_bMonCargarComboDesde As Boolean
Private m_bPaiRespetarCombo As Boolean
Private m_bPaiCargarComboDesde As Boolean
Private m_bProviRespetarCombo As Boolean
Private m_bProviCargarComboDesde As Boolean
Private m_bDestRespetarCombo As Boolean
Private m_bDestCargarComboDesde As Boolean
Private m_bPagoRespetarCombo As Boolean
Private m_bPagoCargarComboDesde As Boolean
Private bRespetarControl As Boolean
Private bChangeTxt As Boolean
Private m_bNoActuProvi As Boolean

Private m_oIdiomas As CIdiomas
Private m_oIdiomasPortal As CIdiomas
Private m_oMonedas As CMonedas
Private m_oPaises As CPaises
Private m_oPaisSeleccionado As CPais
Private m_oDestinos As CDestinos
Private m_oPagos As CPagos

Private m_bCambiarIdioma As Boolean

Private m_vIdiIdiomasRow As Variant
Private m_vIdiConvocatoriaRow As Variant
Private m_vIdiIdiomasPortalRow As Variant
Private m_sIdiCC As String
Private m_sIdiBCC As String

Private m_sCap(1 To 13) As String
Private m_sTextosFormatoPlantilla(1 To 5) As String
Private m_sVistasPlantilla(1 To 2) As String
Private m_sSemana As String
Private m_sSemanas As String
Private m_sMes As String
Private m_sMeses As String
Private m_sDia As String
Private m_sDias As String
Private m_sDatos(10) As String
Private m_sAnyo As String
Private m_sAnyos As String

Private m_sIdiCodGrupo As String

'Indica si se mostrar� o no la solicitud de compra
Private m_bMostrarSolicitud As Boolean
Private m_bMostrarCarpetas  As Boolean

'Convocatoria
Private oLiteralesAModificar As CLiterales

'Pedidos Valores por defecto
Private m_oAtribs As CAtributos
Private msTrue As String
Private msFalse As String
Private msCamposNoSeleccionados As String
Public g_ofrmATRIBMod As frmAtribMod
Public g_oAtribsAnyadir As CAtributos
Private bModError As Boolean
Private m_sDenListaExterna As String

''' <summary>Actualiza los par�metros con los datos introducidos</summary>
''' <remarks>Llamada desde cmdAceptar_Click</remarks>

Private Sub PrepararParametros()
Dim i As Integer
With gParametrosInstalacion
    .gsMoneda = sdbcMonCod
    .gsPais = sdbcPaiCod
    .gsProvincia = sdbcProviCod
    .gsDestino = sdbcDestCod
    .gsFormaPago = sdbcPagoCod
    .gbSugerirCodArticulos = (chkSugerir.Value = vbChecked)
    .gsPetOfe = txtPlantilla(0)
    .gsPetOfeCarta = txtPlantilla(1)
    .gsPetOfeMail = txtPlantilla(2)
    .gsPetOfeWeb = txtPlantilla(3)
    .gsObj = txtPlantilla(4)
    .gsObjCarta = txtPlantilla(5)
    .gsObjMail = txtPlantilla(6)
    .gsObjWeb = txtPlantilla(7)
    .gsComparativa = txtPlantilla(8)
    .gsComparativaItem = txtPlantilla(37)
    .gsComparativaQA = txtPlantilla(42)
    .gsAdjNotif = txtPlantilla(38)
    .gsAdj = txtPlantilla(9)
    .gsAdjCarta = txtPlantilla(10)
    .gsAdjMail = txtPlantilla(11)
    .gsHojaAdjDir = txtPlantilla(12)
    .gsAdjNoCarta = txtPlantilla(13)
    .gsAdjNoMail = txtPlantilla(14)
    .gsDatosExternosMail = txtPlantilla(24)
    .gsDatosExternosExcel = txtPlantilla(25)
    .gsPedidoDirRecepCartaOK = txtPlantilla(27)
    .gsPedidoDirRecepMailOK = txtPlantilla(26)
    .gsPedidoDirRecepCartaKO = txtPlantilla(29)
    .gsPedidoDirRecepMailKO = txtPlantilla(28)
    .gsPedidoDot = txtPlantilla(30)
    .gsPedRec = txtPlantilla(31)
    .gsPedidoXLT = txtPlantilla(32)
    .gsPedRecMailSubject = txtMailSubject(4)
    .gsPedAnuMailSubject = txtMailSubject(5)
    .gsPedEmiMailSubject = txtMailSubject(6)
    .gsDatosExtMailSubject = txtMailSubject(7)
    .gbAdjOrden = (chkPlantAdj(0).Value = vbChecked)
    .gbAdjNotif = (chkPlantAdj(1).Value = vbChecked)
    .gsHojaAdjProce = txtPlantilla(15)
    .gsConvocatoria = txtPlantilla(16)
    If Not sdbcPlantilla(0).Value = "" Then .giIDAGENDA = sdbcPlantilla(0).Value
    .giFORMATOAGENDA = IIf(sdbcPlantilla(1).Value = "", 0, sdbcPlantilla(1).Value)
    If optActaCrystal.Value = True Then
        .giActaTipo = ActaCrystal
        .gsActa = txtPlantilla(19)
        .gsActa2 = txtPlantilla(39)
    Else
        .giActaTipo = ActaWord
        If Not sdbcPlantilla(3).Value = "" Then .giIDACTA = sdbcPlantilla(3).Value
        .giFORMATOACTA = IIf(sdbcPlantilla(4).Value = "", 0, sdbcPlantilla(4).Value)
    End If
    
    .gsPedidoDirEmisionMail = txtPlantilla(20)
    .gsPedidoDirEmisionCarta = txtPlantilla(21)
    .gsPedidoDirAnulacionCarta = txtPlantilla(22)
    .gsPedidoDirAnulacionMail = txtPlantilla(23)
    .gsPEDNotifAcepTXT = txtPlantilla(41)
    .gsPEDNotifAcepHTML = txtPlantilla(40)
    Select Case UCase(Me.sdbcIdiomaConv.Columns("ID").Value)
    Case "SPA"
        .gsConvocatoriaMailHtmlSPA = txtPlantilla(17)
        .gsConvocatoriaMailHtmlENG = oLiteralesAModificar.Item("ENG" & CStr(10008)).Den
        .gsConvocatoriaMailHtmlGER = oLiteralesAModificar.Item("GER" & CStr(10008)).Den
        .gsConvocatoriaMailHtmlFRA = oLiteralesAModificar.Item("FRA" & CStr(10008)).Den
        .gsConvocatoriaMailTextSPA = txtPlantilla(43)
        .gsConvocatoriaMailTextENG = oLiteralesAModificar.Item("ENG" & CStr(10009)).Den
        .gsConvocatoriaMailTextGER = oLiteralesAModificar.Item("GER" & CStr(10009)).Den
        .gsConvocatoriaMailTextFRA = oLiteralesAModificar.Item("FRA" & CStr(10009)).Den
        .gsConvocatoriaMailSubjectSPA = txtConvMailSubject
        .gsConvocatoriaMailSubjectENG = oLiteralesAModificar.Item("ENG" & CStr(10010)).Den
        .gsConvocatoriaMailSubjectGER = oLiteralesAModificar.Item("GER" & CStr(10010)).Den
        .gsConvocatoriaMailSubjectFRA = oLiteralesAModificar.Item("FRA" & CStr(10010)).Den
    Case "ENG"
        .gsConvocatoriaMailHtmlSPA = oLiteralesAModificar.Item("SPA" & CStr(10008)).Den
        .gsConvocatoriaMailHtmlENG = txtPlantilla(17)
        .gsConvocatoriaMailHtmlGER = oLiteralesAModificar.Item("GER" & CStr(10008)).Den
        .gsConvocatoriaMailHtmlFRA = oLiteralesAModificar.Item("FRA" & CStr(10008)).Den
        .gsConvocatoriaMailTextSPA = oLiteralesAModificar.Item("SPA" & CStr(10009)).Den
        .gsConvocatoriaMailTextENG = txtPlantilla(43)
        .gsConvocatoriaMailTextGER = oLiteralesAModificar.Item("GER" & CStr(10009)).Den
        .gsConvocatoriaMailTextFRA = oLiteralesAModificar.Item("FRA" & CStr(10009)).Den
        .gsConvocatoriaMailSubjectSPA = oLiteralesAModificar.Item("SPA" & CStr(10010)).Den
        .gsConvocatoriaMailSubjectENG = txtConvMailSubject
        .gsConvocatoriaMailSubjectGER = oLiteralesAModificar.Item("GER" & CStr(10010)).Den
        .gsConvocatoriaMailSubjectFRA = oLiteralesAModificar.Item("FRA" & CStr(10010)).Den
    Case "GER"
        .gsConvocatoriaMailHtmlSPA = oLiteralesAModificar.Item("SPA" & CStr(10008)).Den
        .gsConvocatoriaMailHtmlENG = oLiteralesAModificar.Item("ENG" & CStr(10008)).Den
        .gsConvocatoriaMailHtmlGER = txtPlantilla(17)
        .gsConvocatoriaMailHtmlFRA = oLiteralesAModificar.Item("FRA" & CStr(10008)).Den
        .gsConvocatoriaMailTextSPA = oLiteralesAModificar.Item("SPA" & CStr(10009)).Den
        .gsConvocatoriaMailTextENG = oLiteralesAModificar.Item("ENG" & CStr(10009)).Den
        .gsConvocatoriaMailTextGER = txtPlantilla(43)
        .gsConvocatoriaMailTextFRA = oLiteralesAModificar.Item("FRA" & CStr(10009)).Den
        .gsConvocatoriaMailSubjectSPA = oLiteralesAModificar.Item("SPA" & CStr(10010)).Den
        .gsConvocatoriaMailSubjectENG = oLiteralesAModificar.Item("ENG" & CStr(10010)).Den
        .gsConvocatoriaMailSubjectGER = txtConvMailSubject
        .gsConvocatoriaMailSubjectFRA = oLiteralesAModificar.Item("FRA" & CStr(10010)).Den
     Case "FRA"
        .gsConvocatoriaMailHtmlSPA = oLiteralesAModificar.Item("SPA" & CStr(10008)).Den
        .gsConvocatoriaMailHtmlENG = oLiteralesAModificar.Item("ENG" & CStr(10008)).Den
        .gsConvocatoriaMailHtmlGER = oLiteralesAModificar.Item("GER" & CStr(10008)).Den
        .gsConvocatoriaMailHtmlFRA = txtPlantilla(17)
        .gsConvocatoriaMailTextSPA = oLiteralesAModificar.Item("SPA" & CStr(10009)).Den
        .gsConvocatoriaMailTextENG = oLiteralesAModificar.Item("ENG" & CStr(10009)).Den
        .gsConvocatoriaMailTextGER = oLiteralesAModificar.Item("GER" & CStr(10009)).Den
        .gsConvocatoriaMailTextFRA = txtPlantilla(43)
        .gsConvocatoriaMailSubjectSPA = oLiteralesAModificar.Item("SPA" & CStr(10010)).Den
        .gsConvocatoriaMailSubjectENG = oLiteralesAModificar.Item("ENG" & CStr(10010)).Den
        .gsConvocatoriaMailSubjectGER = oLiteralesAModificar.Item("GER" & CStr(10010)).Den
        .gsConvocatoriaMailSubjectFRA = txtConvMailSubject
    End Select
    .gsPetOfeMailSubject = txtMailSubject(0)
    .gsObjMailSubject = txtMailSubject(1)
    .gsAdjMailSubject = txtMailSubject(2)
    .gsAdjNoMailSubject = txtMailSubject(3)
    .gsPedNotifMailSubject = txtMailSubject(9)
    .gbMostrarMail = (chkMostrarMail.Value = vbChecked)
    .gbAcuseRecibo = (chkAcuseRecibo.Value = vbChecked)
    .gsRecipientCC = txtCc.Text
    .gsRecipientBCC = txtBcc.Text
    .gbMensajeCierre = (chkMostrarMensaje.Value = vbChecked)
    .gbTipoMail = (optFormatoEMail(0).Value = True)
    .gsRPTPATH = txtRuta
    If m_bMostrarCarpetas Then .gsPathSolicitudes = txtRutaSolicitudes
    .giLongCabComp = val(txtLongCabcomp)
    .giLongCabCompItem = val(txtLongCabCompItem)
    .giLongCabCompQA = val(txtLongCabCompQA)
    m_bCambiarIdioma = (.gIdioma <> sdbcIdiomas.Columns("ID").Value)
    .gIdioma = sdbcIdiomas.Columns("ID").Value
    oMensajes.IdiomaInstalacion = sdbcIdiomas.Columns("ID").Value
    oUsuarioSummit.idioma = sdbcIdiomas.Columns("ID").Value
    m_vIdiIdiomasRow = sdbcIdiomas.Bookmark
    .gIdiomaOffSet = sdbcIdiomas.Columns("OFFSET").Value
    If gParametrosGenerales.giINSTWEB = ConPortal Then
        .gIdiomaPortal = sdbcIdiomaPortal.Columns("ID").Value
        m_vIdiIdiomasPortalRow = sdbcIdiomaPortal.Bookmark
    End If
    .giBuzonPeriodo = sdbcAntigOfer.Columns(1).Value
    If opLeidas.Value = True Then
        .giBuzonOfeLeidas = TipoBuzonOfertas.Leidas
    ElseIf opNoLeidas.Value = True Then
        .giBuzonOfeLeidas = TipoBuzonOfertas.NoLeidas
    Else
        .giBuzonOfeLeidas = TipoBuzonOfertas.Todas
    End If
    .giBuzonTimer = txtMinutos.Text
    If OptPasoAut.Value = True Then
        .giPasoAutomatico = 1
    Else
        If OptSiPasar.Value = True Then
            .giPasoAutomatico = 2
        Else
            If OptNoPasar.Value = True Then
                .giPasoAutomatico = 3
            End If
        End If
    End If
    .gbAvisoPortalCompResp = (chkCompResp.Value = vbChecked)
    .gbAvisoPortalCompAsign = (chkCompAsig.Value = vbChecked)
    .gbAvisoAdj = Me.ctrlConfGenAvisos.AvisoAdjSoloSiResp
    .giAvisoAdj = Me.ctrlConfGenAvisos.AntelacionAdjudicacion
    .gbAvisoDespublica = Me.ctrlConfGenAvisos.AvisoVisorGSSoloSiResp
    .giAvisoDespublica = Me.ctrlConfGenAvisos.AntelacionVisorGS
    If opAdjAutomat.Value = True Then
        .giAnyadirEspecArticulo = Adjuntar
    ElseIf opAdjPreguntar.Value = True Then
        .giAnyadirEspecArticulo = PreguntarAdj
    Else
        .giAnyadirEspecArticulo = NoAdjuntar
    End If
    .giNumDecimalesComp = val(txtNumDec.Text)
    .giNumDecimalesOfe = val(txtNumDecRecep.Text)
    .gbPedNotifMailCCActivado = (chkNotifPed(2).Value)
    .gsPedNotifMailCC = txtMailSubject(10).Text
    .giPedNotifMailCCPeti = IIf(chkNotifPed(0).Value, 1, 0)
    .giPedNotifMailCCComp = IIf(chkNotifPed(1).Value, 1, 0)
    '�mbito de datos
    sdbgDatosGrid.MoveFirst
    For i = 0 To sdbgDatosGrid.Rows - 1
        Select Case sdbgDatosGrid.Columns("Fila").Value
            Case 0
                'Destino
                If sdbgDatosGrid.Columns.Item(2).Value = True Then
                    .gCPDestino = EnProceso
                Else
                    If sdbgDatosGrid.Columns.Item(3).Value = True Then
                        .gCPDestino = EnGrupo
                    Else
                        .gCPDestino = EnItem
                    End If
                End If
            Case 1
                'Forma pago
                If sdbgDatosGrid.Columns.Item(2).Value = True Then
                    .gCPPago = EnProceso
                Else
                    If sdbgDatosGrid.Columns.Item(3).Value = True Then
                        .gCPPago = EnGrupo
                    Else
                        .gCPPago = EnItem
                    End If
                End If
            Case 2
                'Fecha de suministro
                If sdbgDatosGrid.Columns.Item(2).Value = True Then
                    .gCPFechasSuministro = EnProceso
                Else
                    If sdbgDatosGrid.Columns.Item(3).Value = True Then
                        .gCPFechasSuministro = EnGrupo
                    Else
                        .gCPFechasSuministro = EnItem
                    End If
                End If
        
            Case 3
                'Prov actual
                If sdbgDatosGrid.Columns.Item(0).Value = True Then
                    If sdbgDatosGrid.Columns.Item(2).Value = True Then
                        .gCPProveActual = EnProceso
                    Else
                        If sdbgDatosGrid.Columns.Item(3).Value = True Then
                            .gCPProveActual = EnGrupo
                        Else
                            .gCPProveActual = EnItem
                        End If
                    End If
                Else
                    .gCPProveActual = NoDefinido
                End If
            Case 4
                'Especificaciones
                If sdbgDatosGrid.Columns.Item(0).Value = True Then
                    If sdbgDatosGrid.Columns.Item(2).Value = True Then
                        .gCPEspProce = True
                    Else
                        .gCPEspProce = False
                    End If
                    If sdbgDatosGrid.Columns.Item(3).Value = True Then
                        .gCPEspGrupo = True
                    Else
                        .gCPEspGrupo = False
                    End If
                    If sdbgDatosGrid.Columns.Item(4).Value = True Then
                        .gCPEspItem = True
                    Else
                        .gCPEspItem = False
                    End If
                Else
                    .gCPEspProce = False
                    .gCPEspGrupo = False
                    .gCPEspItem = False
                End If
            Case 5
                'Distibuci�n en UON
                If sdbgDatosGrid.Columns.Item(0).Value = True Then
                    If sdbgDatosGrid.Columns.Item(2).Value = True Then
                        .gCPDistUON = EnProceso
                    Else
                        If sdbgDatosGrid.Columns.Item(3).Value = True Then
                            .gCPDistUON = EnGrupo
                        Else
                            .gCPDistUON = EnItem
                        End If
                    End If
                Else
                    .gCPDistUON = NoDefinido
                End If
            Case 6
                'Pres. Anu1
                If gParametrosGenerales.gbUsarPres1 = True Then
                    If sdbgDatosGrid.Columns.Item(0).Value = True Then
                        If sdbgDatosGrid.Columns.Item(2).Value = True Then
                            .gCPPresAnu1 = EnProceso
                        Else
                            If sdbgDatosGrid.Columns.Item(3).Value = True Then
                                .gCPPresAnu1 = EnGrupo
                            Else
                                .gCPPresAnu1 = EnItem
                            End If
                        End If
                    Else
                        .gCPPresAnu1 = NoDefinido
                    End If
                End If
            Case 7
                'Pres. Anu2
                If gParametrosGenerales.gbUsarPres2 = True Then
                    If sdbgDatosGrid.Columns.Item(0).Value = True Then
                        If sdbgDatosGrid.Columns.Item(2).Value = True Then
                            .gCPPresAnu2 = EnProceso
                        Else
                            If sdbgDatosGrid.Columns.Item(3).Value = True Then
                                .gCPPresAnu2 = EnGrupo
                            Else
                                .gCPPresAnu2 = EnItem
                            End If
                        End If
                    Else
                        .gCPPresAnu2 = NoDefinido
                    End If
                End If
            Case 8
                'Pres. Anu3
                If gParametrosGenerales.gbUsarPres3 = True Then
                    If sdbgDatosGrid.Columns.Item(0).Value = True Then
                        If sdbgDatosGrid.Columns.Item(2).Value = True Then
                            .gCPPres1 = EnProceso
                        Else
                            If sdbgDatosGrid.Columns.Item(3).Value = True Then
                                .gCPPres1 = EnGrupo
                            Else
                                .gCPPres1 = EnItem
                            End If
                        End If
                    Else
                        .gCPPres1 = NoDefinido
                    End If
                End If
            Case 9
                'Pres. Anu4
                If gParametrosGenerales.gbUsarPres4 = True Then
                    If sdbgDatosGrid.Columns.Item(0).Value = True Then
                        If sdbgDatosGrid.Columns.Item(2).Value = True Then
                            .gCPPres2 = EnProceso
                        Else
                            If sdbgDatosGrid.Columns.Item(3).Value = True Then
                                .gCPPres2 = EnGrupo
                            Else
                                .gCPPres2 = EnItem
                            End If
                        End If
                    Else
                        .gCPPres2 = NoDefinido
                    End If
                End If
            Case 10
                'Solicitud de compras
                Select Case sdbgDatosGrid.Columns.Item(0).Value
                    Case True
                        Select Case sdbgDatosGrid.Columns.Item(2).Value
                            Case True
                                .gCPSolicitud = EnProceso
                            Case False
                                Select Case sdbgDatosGrid.Columns.Item(3).Value
                                    Case True
                                        .gCPSolicitud = EnGrupo
                                    Case False
                                        .gCPSolicitud = EnItem
                                End Select
                        End Select
                    Case False
                        .gCPSolicitud = NoDefinido
                End Select
        End Select
        sdbgDatosGrid.MoveNext
    Next i
    sdbgDatosGrid.MoveFirst
    
    If chkAdminPub.Value = vbChecked Then
        .gCPDestino = EnProceso
        .gCPPago = EnProceso
        .gCPProveActual = NoDefinido
        .gCPSolicitud = NoDefinido
    End If
    .gsGrupoCod = txtCodVal.Text
    .gsGrupoDen = txtDenVal.Text
    .gsGrupoDescrip = txtDescVal.Text
    .gCPSubasta = (chkSolOfe(0).Value = vbChecked)
    .gCPAlternativasPrec = (chkSolOfe(1).Value = vbChecked)
    .gCPSolCantMax = (chkSolOfe(2).Value = vbChecked)
    .gCPCambiarMonOferta = Not (chkSolOfe(6).Value = vbChecked)
    .gCPAdjunOfe = (chkSolOfe(3).Value = vbChecked)
    .gCPAdjunGrupo = (chkSolOfe(4).Value = vbChecked)
    .gCPAdjunItem = (chkSolOfe(5).Value = vbChecked)
    .gCPNoPublicarFinSum = (Me.chkNoPublicarFinSum.Value = vbChecked)
    .gbUnSoloPedido = (Me.chkUnSoloPedido.Value = vbChecked)
    .gCPPonderar = (chkPonderar.Value = vbChecked)
    If ADMIN_OBLIGATORIO Then
        .gCPProcesoAdminPub = True
    Else
        .gCPProcesoAdminPub = (chkAdminPub.Value = vbChecked)
    End If
    .gsPETSUBDOT = txtPlantilla(33).Text
    .gsPETSUBCARTADOT = txtPlantilla(34).Text
    .gsPETSUBMAILDOT = txtPlantilla(35).Text
    .gsPETSUBWEBDOT = txtPlantilla(36).Text
    .gsPETSUBMAILSUBJECT = txtMailSubject(8).Text
    .gbSeleccionPositiva = (chkSelPositiva.Value = vbChecked)
    .gbPresupuestoPlanificado = (optPresPlanif.Value = True)
    .gbTipoActa = (optReuActa(1).Value = True)
    .gsOFENotifAvisoDespubHTML = Me.ctrlConfGenAvisos.PlantillaDespub(0)
    .gsOFENotifAvisoDespubTXT = Me.ctrlConfGenAvisos.PlantillaDespub(1)
    .gsOFENotifAvisoDespubSubject = Me.ctrlConfGenAvisos.AsuntoAvisoDespub
    If m_bMostrarSolicitud Then .gbSolicProcAdjDir = (optGenerarProc(0).Value = True)
    If m_bMostrarSolicitud Then
        If Trim(txtAdjDirImp.Text) = "" Or Me.chkAdjDirImp.Value = vbUnchecked Then
            .gvImporteSolicProcAdjDir = Null
        Else
            .gvImporteSolicProcAdjDir = Trim(txtAdjDirImp.Text)
        End If
    End If
    If m_bMostrarSolicitud Then .giValidezSolicOfe = sdbcPeriodoSolic.Columns(1).Value
    'Par�metros subasta
    .gCPSubTipo = m_iSubTipo
    .gCPSubModo = m_iSubModo
    .gCPSubDuracion = m_vSubDuracion
    .gCPSubastaEspera = m_iSubastaEspera
    .gCPSubPublicar = m_bSubPublicar
    .gCPSubNotifEventos = m_bSubNotifEventos
    .gCPSubastaProve = m_bSubastaProve
    .gCPSubVerDesdePrimPuja = m_bSubVerDesdePrimPuja
    .gCPSubastaPrecioPuja = m_bSubastaPrecioPuja
    .gCPSubastaPujas = m_bSubastaPujas
    .gCPSubastaBajMinPuja = m_bSubastaBajMinPuja
    .gCPSubBajMinProveTipo = m_iSubBajMinGanTipo
    .gCPSubBajMinGanProcVal = m_vSubBajMinGanProcVal
    .gCPSubBajMinGanGrupoVal = m_vSubBajMinGanGrupoVal
    .gCPSubBajMinGanItemVal = m_vSubBajMinGanItemVal
    .gCPSubBajMinProve = m_bSubBajMinProve
    .gCPSubBajMinProveTipo = m_iSubBajMinProveTipo
    .gCPSubBajMinProveProcVal = m_vSubBajMinProveProcVal
    .gCPSubBajMinProveGrupoVal = m_vSubBajMinProveGrupoVal
    .gCPSubBajMinProveItemVal = m_vSubBajMinProveItemVal
    Set .gCPSubTextoFin = m_dcSubTextosFin
End With
End Sub

''' <summary>Comprueba que los valores de los par�metros sean correctos</summary>
''' <remarks>Llamada desde cmdAceptar_Click</remarks>

Private Function VerificarParametros() As Boolean
Dim sFileName As String
VerificarParametros = False
If sdbcMonCod <> "" Then
    sdbcMonCod_Validate False
    If sdbcMonCod = "" Then
        oMensajes.ImposibleActualizarParam m_sCap(1)
        Exit Function
    End If
End If
If sdbcPaiCod <> "" Then
    sdbcPaiCod_Validate False
    If sdbcPaiCod = "" Then
        oMensajes.ImposibleActualizarParam m_sCap(2)
        Exit Function
    End If
End If
If sdbcProviCod <> "" Then
    sdbcProviCod_Validate False
    If sdbcProviCod = "" Then
        oMensajes.ImposibleActualizarParam m_sCap(3)
        Exit Function
    End If
End If
If sdbcDestCod <> "" Then
    sdbcDestCod_Validate False
    If sdbcDestCod = "" Then
        oMensajes.ImposibleActualizarParam m_sCap(4)
        Exit Function
    End If
End If
If Not IsNumeric(txtLongCabcomp) Or val(txtLongCabcomp.Text) <= 0 Then
    oMensajes.ImposibleActualizarParam m_sCap(5)
    Exit Function
End If
If Not IsNumeric(txtLongCabCompItem) Or val(txtLongCabCompItem.Text) <= 0 Then
    oMensajes.ImposibleActualizarParam m_sCap(5)
    Exit Function
End If
If Not IsNumeric(txtLongCabCompQA) Or val(txtLongCabCompQA.Text) <= 0 Then
    oMensajes.ImposibleActualizarParam m_sCap(5)
    Exit Function
End If
If Not oFos.FolderExists(txtRuta) Then
    oMensajes.RutaNoValida ("RPT")
    Exit Function
End If
If m_bMostrarCarpetas Then
    If Not oFos.FolderExists(txtRutaSolicitudes) Then
        oMensajes.RutaNoValida ("Solicit")
    Exit Function
    End If
End If
If sdbcAntigOfer = "" Then
    oMensajes.ImposibleActualizarParam lblbuzperiodo.caption
    Exit Function
End If
If txtMinutos.Text = "" Then oMensajes.ImposibleActualizarParam lblComprobarOfer.caption
If ((opAdjAutomat.Value = False) And (opAdjPreguntar.Value = False) And (opNoAdj.Value = False)) Or _
   ((optPresAdj.Value = False) And (optPresPlanif.Value = False)) Then
    oMensajes.ImposibleActualizarParam fraRegGest.caption
    Exit Function
End If
If ((OptPasoAut.Value = False) And (OptSiPasar.Value = False) And (OptNoPasar.Value = False)) Then
    oMensajes.ImposibleActualizarParam fraRegGest2.caption
    Exit Function
End If
If Not IsInteger(txtNumDec.Text) Then
    oMensajes.ImposibleActualizarParam Label10(6).caption
    Exit Function
End If
If Not IsInteger(txtNumDecRecep.Text) Then
    oMensajes.ImposibleActualizarParam fraNumDecRecepcion.caption
    Exit Function
Else
    If val(txtNumDecRecep.Text) < 0 Or val(txtNumDecRecep.Text) > 20 Then
        oMensajes.ImposibleActualizarParam fraNumDecRecepcion.caption
        Exit Function
    End If
End If
If txtCc.Text <> "" Then
    If InStr(1, txtCc.Text, "@") = 0 Then
        oMensajes.ImposibleActualizarParam m_sIdiCC
        Exit Function
    End If
End If
If txtBcc.Text <> "" Then
    If InStr(1, txtBcc.Text, "@") = 0 Then
        oMensajes.ImposibleActualizarParam m_sIdiBCC
        Exit Function
    End If
End If
If txtCodVal.Text <> "" Then
    If Not NombreDeGrupoValido(txtCodVal.Text) Then
        oMensajes.CodigoGrupoNoValido m_sIdiCodGrupo
        Exit Function
    End If
End If
'Comprueba que las plantillas de aceptaci�n de pedido comienzen por "IDIOMA_"
If Trim(txtPlantilla(40).Text) <> "" Then
    sFileName = oFos.GetFileName(Trim(txtPlantilla(40).Text))
    If m_oIdiomas.Item(Mid(sFileName, 1, 3)) Is Nothing Then
        oMensajes.ImposibleActualizarParamNotifReceptor
        Exit Function
    End If
End If
If Trim(txtPlantilla(41).Text) <> "" Then
    sFileName = oFos.GetFileName(Trim(txtPlantilla(41).Text))
    If m_oIdiomas.Item(Mid(sFileName, 1, 3)) Is Nothing Then
        oMensajes.ImposibleActualizarParamNotifReceptor
        Exit Function
    End If
End If
If m_bMostrarSolicitud Then
    If Trim(txtAdjDirImp.Text) <> "" Then
        If Not IsNumeric(txtAdjDirImp.Text) Or val(txtLongCabcomp.Text) <= 0 Then
            oMensajes.ImposibleActualizarParam m_sCap(12)
            Exit Function
        End If
    End If
    If sdbcPeriodoSolic.Text = "" Then
        oMensajes.ImposibleActualizarParam fraSolicit(1).caption
        Exit Function
    End If
End If
If Not OptActaWord.Value Then
    sFileName = oFos.GetFileName(Trim(txtPlantilla(19).Text))
    If sFileName <> "" Then
        If UCase(Right(sFileName, 3)) <> "RPT" Then
            oMensajes.MensajeOKOnly oMensajes.CargarTextoMensaje(180), TipoIconoMensaje.Information
            Exit Function
        End If
    End If
    sFileName = oFos.GetFileName(Trim(txtPlantilla(39).Text))
    If sFileName <> "" Then
        If UCase(Right(sFileName, 3)) <> "RPT" Then
            oMensajes.MensajeOKOnly oMensajes.CargarTextoMensaje(180), TipoIconoMensaje.Information
            Exit Function
        End If
    End If
End If
If Not txtMailSubject(10).Text = "" Then
     If Not ComprobarEmail(txtMailSubject(10).Text) Then
         tabCONFINST.Tab = 4
         TabPedidos.Tab = 0
         oMensajes.MailIncorrecto txtMailSubject(10).Text
         If Me.Visible Then txtMailSubject(10).SetFocus
         Exit Function
    End If
End If
VerificarParametros = True
End Function

''' <summary>Establece el modo de consulta de la pantalla</summary>
''' <remarks>Llamada desde cmdAceptar_Click, cmdCancelar_Click, Form_Load</remarks>

Private Sub ModoConsulta()
    Dim i As Integer
    For i = 0 To 2
        picModSub(i).Enabled = False
    Next i
    picSugerir.Enabled = False
    picObj1.Enabled = False
    picObj2.Enabled = False
    picObj3.Enabled = False
    picListados.Enabled = False
    If m_bMostrarCarpetas Then
        picSolicitudes.Enabled = False
    End If
    picDef.Enabled = False
    picPetOfe1.Enabled = False
    PicPetOfe2.Enabled = False
    picPetOfe3.Enabled = False
    picEdit.Visible = False
    picReuPlantillas.Enabled = False
    picNavigate.Visible = True
    tabCONFINST.Tab = 0
    fraInterfaz.Visible = True
    PicAdj1.Enabled = False
    PicAdj2.Enabled = False
    PicAdj3.Enabled = False
    PicAdjNo1.Enabled = False
    PicAdjNo2.Enabled = False
    picMostrar(0).Enabled = False
    picMostrar(1).Enabled = False
    picMensaje.Enabled = False
    picComp.Enabled = False
    picCompItem.Enabled = False
    picCompQA.Enabled = False
    picAdjDir.Enabled = False
    picIdioma.Enabled = False
    picIdiomaPortal.Enabled = False
    picBuzonOfe.Enabled = False
    picNotifPed.Enabled = False
    picAnulaPed.Enabled = False
    PicRecepPed.Enabled = False
    picExternosPed.Enabled = False
    Me.ctrlConfGenAvisos.ModoConsulta = True
    picValDef(0).Enabled = False
    picValDef(1).Enabled = False
    picRegGest.Enabled = False
    picRegGest2.Enabled = False
    picRegGest3.Enabled = False
    picRegGest4.Enabled = False
    picSolicitud.Enabled = False
    picPonderar.Enabled = False
    picDatosConf(7).Enabled = False
    picPonderacion.Enabled = False
    picAmbitoDatos.Enabled = True
    For i = 0 To sdbgDatosGrid.Columns.Count - 1
        sdbgDatosGrid.Columns(i).Locked = True
    Next i
    picAdminPub.Enabled = False
    picAdj5.Enabled = False
    picNotifAcepPic.Enabled = False
    If m_bMostrarSolicitud Then
        picGenSolicit(0).Enabled = False
        picGenSolicit(1).Enabled = False
    End If
    picTZ.Enabled = False
    picAtribDefecto.Enabled = False
    For i = 0 To sdbgCamposPedido.Columns.Count - 1
        sdbgCamposPedido.Columns(i).Locked = True
    Next i
End Sub

''' <summary>Establece el modo de edici�n de la pantalla</summary>
''' <remarks>Llamada desde cmdEdicion_Click</remarks>

Private Sub ModoEdicion()
    Dim i As Integer
    For i = 0 To 2
        picModSub(i).Enabled = True
    Next i
    picSugerir.Enabled = True
    picObj1.Enabled = True
    picObj2.Enabled = True
    picObj3.Enabled = True
    picListados.Enabled = True
    If m_bMostrarCarpetas Then picSolicitudes.Enabled = True
    picDef.Enabled = True
    picPetOfe1.Enabled = True
    PicPetOfe2.Enabled = True
    picPetOfe3.Enabled = True
    picNavigate.Visible = False
    picEdit.Visible = True
    picReuPlantillas.Enabled = True
    fraInterfaz.Visible = False
    picMostrar(0).Enabled = True
    picMostrar(1).Enabled = True
    picMensaje.Enabled = True
    PicAdj1.Enabled = True
    PicAdj2.Enabled = True
    PicAdj3.Enabled = True
    PicAdjNo1.Enabled = True
    PicAdjNo2.Enabled = True
    picComp.Enabled = True
    picCompItem.Enabled = True
    picCompQA.Enabled = True
    picAdjDir.Enabled = True
    picIdioma.Enabled = True
    picIdiomaPortal.Enabled = True
    picBuzonOfe.Enabled = True
    picNotifPed.Enabled = True
    picAnulaPed.Enabled = True
    PicRecepPed.Enabled = True
    picExternosPed.Enabled = True
    Me.ctrlConfGenAvisos.ModoConsulta = False
    picValDef(0).Enabled = True
    picValDef(1).Enabled = True
    picRegGest.Enabled = True
    picRegGest2.Enabled = True
    picRegGest3.Enabled = True
    picRegGest4.Enabled = True
    picSolicitud.Enabled = True
    picPonderacion.Enabled = True
    fraPonderar.Visible = (gParametrosGenerales.gbUsarPonderacion = True)
    picPonderar.Enabled = (gParametrosGenerales.gbUsarPonderacion = True)
    picDatosConf(7).Enabled = True
    picAmbitoDatos.Enabled = True
    For i = 0 To sdbgDatosGrid.Columns.Count - 1
        If sdbgDatosGrid.Columns(i).Name = "Dato" Then
            sdbgDatosGrid.Columns(i).Locked = True
        Else
            sdbgDatosGrid.Columns(i).Locked = False
        End If
    Next i
    picAdminPub.Enabled = True
    picAdj5.Enabled = True
    picNotifAcepPic.Enabled = True
    If m_bMostrarSolicitud Then
        picGenSolicit(0).Enabled = True
        picGenSolicit(1).Enabled = True
    End If
    Set oLiteralesAModificar = Nothing
    Set oLiteralesAModificar = New CLiterales
    With gParametrosInstalacion
        oLiteralesAModificar.Add "SPA", .gsConvocatoriaMailHtmlSPA, 10008
        oLiteralesAModificar.Add "SPA", .gsConvocatoriaMailTextSPA, 10009
        oLiteralesAModificar.Add "SPA", .gsConvocatoriaMailSubjectSPA, 10010
        oLiteralesAModificar.Add "ENG", .gsConvocatoriaMailHtmlENG, 10008
        oLiteralesAModificar.Add "ENG", .gsConvocatoriaMailTextENG, 10009
        oLiteralesAModificar.Add "ENG", .gsConvocatoriaMailSubjectENG, 10010
        oLiteralesAModificar.Add "GER", .gsConvocatoriaMailHtmlGER, 10008
        oLiteralesAModificar.Add "GER", .gsConvocatoriaMailTextGER, 10009
        oLiteralesAModificar.Add "GER", .gsConvocatoriaMailSubjectGER, 10010
        oLiteralesAModificar.Add "FRA", .gsConvocatoriaMailHtmlFRA, 10008
        oLiteralesAModificar.Add "FRA", .gsConvocatoriaMailTextFRA, 10009
        oLiteralesAModificar.Add "FRA", .gsConvocatoriaMailSubjectFRA, 10010
    End With
    picTZ.Enabled = True
    picAtribDefecto.Enabled = True
    For i = 0 To sdbgCamposPedido.Columns.Count - 1
        If sdbgCamposPedido.Columns(i).Name = "VAL" Then
            sdbgCamposPedido.Columns(i).Locked = False
        End If
    Next i
End Sub


Private Sub chkAdminPub_Click()
    Dim i As Integer
    Dim vbm As Variant
    If chkAdminPub.Value = vbChecked Then
        'Desactiva la recepci�n en modo subasta
        chkSolOfe(0).Value = vbUnchecked
        chkSolOfe(0).Visible = False
        cmdConfigSubasta.Visible = False
        'Desactiva Pedir alternativas de precio
        chkSolOfe(1).Value = vbUnchecked
        chkSolOfe(1).Visible = False
        'Desactiva ofertar en mopneda del proceso
        chkSolOfe(6).Value = vbChecked
        chkSolOfe(6).Visible = False
        'Desactiva las cantidades m�ximas
        chkSolOfe(2).Value = vbUnchecked
        chkSolOfe(2).Visible = False
        'permitir adjuntar archivos a nivel de proceso
        chkSolOfe(3).Value = vbUnchecked
        chkSolOfe(3).Visible = False
        chkSolOfe(4).Top = 0
        chkSolOfe(5).Top = 230
        Me.chkNoPublicarFinSum.Top = 460
        'Elimina los �mbitos de datos correspondientes de la grid
        i = sdbgDatosGrid.Rows - 1
        While i >= 0
            vbm = sdbgDatosGrid.AddItemBookmark(i)
            Select Case sdbgDatosGrid.Columns("Fila").CellValue(vbm)
                Case 0
                    'Destino
                    sdbgDatosGrid.RemoveItem (sdbgDatosGrid.AddItemRowIndex(vbm))
                Case 1
                    'forma de pago
                    sdbgDatosGrid.RemoveItem (sdbgDatosGrid.AddItemRowIndex(vbm))
                Case 3
                    'Proveedor actual
                    sdbgDatosGrid.RemoveItem (sdbgDatosGrid.AddItemRowIndex(vbm))
                Case 10
                    'Solicitud de compra
                    sdbgDatosGrid.RemoveItem (sdbgDatosGrid.AddItemRowIndex(vbm))
            End Select
            i = i - 1
        Wend
    Else
        'Vuelve a hacer visibles las checks
        If gParametrosGenerales.giINSTWEB <> SinWeb Then
            chkSolOfe(0).Visible = True
            cmdConfigSubasta.Visible = True
        End If
        chkSolOfe(1).Visible = True
        chkSolOfe(2).Visible = True
        chkSolOfe(6).Visible = True
        chkSolOfe(3).Visible = True
        chkSolOfe(4).Top = 900
        chkSolOfe(5).Top = 1120
        Me.chkNoPublicarFinSum.Top = 1340
        'Carga otra vez la grid
        CargarAmbitoDatos
    End If
End Sub

Private Sub chkSolOfe_Click(Index As Integer)
    If Index = 0 Then
        'Si el proceso es de subasta no se debe permitir seleccionar "Solicitar cantidades m�ximas ..."
        If chkSolOfe(0).Value = vbChecked Then chkSolOfe(2).Value = vbUnchecked
        chkSolOfe(2).Enabled = (chkSolOfe(0).Value = vbUnchecked)
    End If
End Sub

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim lParametros As ParametrosInstalacion
    Dim sUsuTZ As String

    If Not VerificarParametros Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    lParametros = gParametrosInstalacion
    
    PrepararParametros
    teserror = oGestorParametros.GuardarParametrosInstalacion(gParametrosInstalacion, oUsuarioSummit.Cod)
    If teserror.NumError <> TESnoerror Then
        gParametrosInstalacion = lParametros
        oUsuarioSummit.idioma = gParametrosInstalacion.gIdioma
        TratarError teserror
        Exit Sub
    Else
        If IsNull(oUsuarioSummit.TimeZone) Or IsEmpty(oUsuarioSummit.TimeZone) Then
            sUsuTZ = ""
        Else
            sUsuTZ = oUsuarioSummit.TimeZone
        End If
        If sUsuTZ <> sdbcTZ.Columns(1).Value Then
            'Actualizar el TimeZone del usuario si ha cambiado
            oUsuarioSummit.TimeZone = sdbcTZ.Columns(1).Value
            oUsuarioSummit.ActualizarTZ
        End If
    End If
    If m_bCambiarIdioma Then
        'El idioma se guarda tambi�n en el registro para saberlo al entrar a la aplicaci�n
        SaveStringSetting "FULLSTEP GS", gInstancia, "Opciones", "Idioma", gParametrosInstalacion.gIdioma
        gParametrosGenerales = oGestorParametros.DevolverParametrosGenerales(basPublic.gParametrosInstalacion.gIdioma)
        If ADMIN_OBLIGATORIO Then
            ConfigurarAdminPublicaObligatoria
        End If
        MDI.CambiarIdioma
    End If
    ModoConsulta
    Screen.MousePointer = vbNormal
    g_GuardarParametrosIns = False
    oMensajes.ConfiguracionInstalacionActualizada
End Sub

Private Sub cmdCambiarFondo_Click()
    On Error GoTo imposible
    cdlFondo.Action = 1
    DoEvents
    Screen.MousePointer = vbHourglass
    Set MDI.Picture = LoadPicture(cdlFondo.filename)
    Screen.MousePointer = vbNormal
    MDI.Hide
    MDI.Show
    oGestorParametros.GuardarFondoInstalacion cdlFondo.filename, oUsuarioSummit.Cod
    Exit Sub
imposible:
    MsgBox m_sCap(6), vbExclamation, "FullStep"
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdCancelar_Click()
    ModoConsulta
    MostrarParametros
    InicializarValoresSubasta
    If sdbgCamposPedido.DataChanged = True Then sdbgCamposPedido.CancelUpdate
    DoEvents
End Sub

''' <summary>Abre el formulario de configuraci�n de subasta.</summary>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: 1sg</remarks>

Private Sub cmdConfigSubasta_Click()
On Error GoTo ERROR
Dim oConfSubasta As New frmCONFSubasta
With oConfSubasta
    .g_sOrigen = "frmCONFINST"
    .g_bModoEdicion = picSolicitud.Enabled
    .g_iSubTipo = m_iSubTipo
    .g_iSubModo = m_iSubModo
    .g_vSubDuracion = m_vSubDuracion
    .g_iSubastaEspera = m_iSubastaEspera
    .g_bSubPublicar = m_bSubPublicar
    .g_bSubNotifEventos = m_bSubNotifEventos
    .g_bSubastaProve = m_bSubastaProve
    .g_bSubVerDesdePrimPuja = m_bSubVerDesdePrimPuja
    .g_bSubastaPrecioPuja = m_bSubastaPrecioPuja
    .g_bSubastaPujas = m_bSubastaPujas
    .g_bSubastaBajMinPuja = m_bSubastaBajMinPuja
    .g_iSubBajMinGanTipo = m_iSubBajMinGanTipo
    .g_vSubBajMinGanProcVal = m_vSubBajMinGanProcVal
    .g_vSubBajMinGanGrupoVal = m_vSubBajMinGanGrupoVal
    .g_vSubBajMinGanItemVal = m_vSubBajMinGanItemVal
    .g_bSubBajMinProve = m_bSubBajMinProve
    .g_iSubBajMinProveTipo = m_iSubBajMinProveTipo
    .g_vSubBajMinProveProcVal = m_vSubBajMinProveProcVal
    .g_vSubBajMinProveGrupoVal = m_vSubBajMinProveGrupoVal
    .g_vSubBajMinProveItemVal = m_vSubBajMinProveItemVal
    Set .g_dcSubTextosFin = m_dcSubTextosFin
    .Show vbModal
    If .g_bOK Then
        m_iSubTipo = .g_iSubTipo
        m_iSubModo = .g_iSubModo
        m_vSubDuracion = .g_vSubDuracion
        m_iSubastaEspera = .g_iSubastaEspera
        m_bSubPublicar = .g_bSubPublicar
        m_bSubNotifEventos = .g_bSubNotifEventos
        m_bSubastaProve = .g_bSubastaProve
        m_bSubVerDesdePrimPuja = .g_bSubVerDesdePrimPuja
        m_bSubastaPrecioPuja = .g_bSubastaPrecioPuja
        m_bSubastaPujas = .g_bSubastaPujas
        m_bSubastaBajMinPuja = .g_bSubastaBajMinPuja
        m_iSubBajMinGanTipo = .g_iSubBajMinGanTipo
        m_vSubBajMinGanProcVal = .g_vSubBajMinGanProcVal
        m_vSubBajMinGanGrupoVal = .g_vSubBajMinGanGrupoVal
        m_vSubBajMinGanItemVal = .g_vSubBajMinGanItemVal
        m_bSubBajMinProve = .g_bSubBajMinProve
        m_iSubBajMinProveTipo = .g_iSubBajMinProveTipo
        m_vSubBajMinProveProcVal = .g_vSubBajMinProveProcVal
        m_vSubBajMinProveGrupoVal = .g_vSubBajMinProveGrupoVal
        m_vSubBajMinProveItemVal = .g_vSubBajMinProveItemVal
        Set m_dcSubTextosFin = .g_dcSubTextosFin
        chkSolOfe(0).Value = vbChecked
    End If
End With
Salir:
    Unload Me
    Set oConfSubasta = Nothing
    Exit Sub
ERROR:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume Salir
End Sub

Private Sub cmdEdicion_Click()
    ModoEdicion
End Sub

''' <summary>
''' Muestra una pantalla de seleccion de archivos
''' </summary>
''' <param name="Index">Indicedel boton pulsado</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdPlantilla_Click(Index As Integer)
On Error GoTo ERROR:
    If Index = 8 Or Index = 38 Or Index = 33 Or Index = 43 Then
        cdlArchivo.Filter = m_sCap(7) & " (*.xlt)|*.xlt"
        cdlArchivo.DefaultExt = "xlt"
    ElseIf Index = 26 Then
        cdlArchivo.Filter = m_sCap(7) & " (*.xls)|*.xls"
        cdlArchivo.DefaultExt = "xls"
    ElseIf Index = 41 Or Index = 17 Or Index = 2 Or Index = 3 Or Index = 35 Or Index = 34 Or Index = 6 Or Index = 7 Or Index = 11 Or Index = 14 Then
        cdlArchivo.Filter = m_sCap(10) & " (*.htm)|*.htm"
        cdlArchivo.DefaultExt = "htm"
    ElseIf Index = 42 Or Index = 44 Then
        cdlArchivo.Filter = m_sCap(9) & " (*.txt)|*.txt"
        cdlArchivo.DefaultExt = "txt"
    ElseIf Index = 19 Or Index = 40 Then
        If OptActaWord.Value = True Then
            cdlArchivo.Filter = m_sCap(8) & " (*.dot)|*.dot"
            cdlArchivo.DefaultExt = "dot"
        Else
            cdlArchivo.Filter = m_sCap(13) & " (*.rpt)|*.rpt"
            cdlArchivo.DefaultExt = "rpt"
        End If
    Else
        cdlArchivo.Filter = m_sCap(8) & " (*.dot)|*.dot"
        cdlArchivo.DefaultExt = "dot"
    End If
    
    cdlArchivo.Action = 1
    If Index > 20 Then
        Select Case Index
        Case 37
            txtPlantilla(33) = cdlArchivo.filename
        Case 36
            txtPlantilla(34) = cdlArchivo.filename
        Case 35
            txtPlantilla(35) = cdlArchivo.filename
        Case 34
            txtPlantilla(36) = cdlArchivo.filename
        Case Else
            txtPlantilla(Index - 1) = cdlArchivo.filename
        End Select
    Else
        txtPlantilla(Index) = cdlArchivo.filename
    End If
    If Index = 17 Then
        oLiteralesAModificar.Item(sdbcIdiomaConv.Columns("ID").Value & CStr(10008)).Den = txtPlantilla(17).Text
    ElseIf Index = 44 Then
        oLiteralesAModificar.Item(sdbcIdiomaConv.Columns("ID").Value & CStr(10009)).Den = txtPlantilla(43).Text
    End If
ERROR:
    If err.Number = cdlCancel Then
        Exit Sub
    Else
        Resume Next
    End If
End Sub

Private Sub cmdQuitarFondo_Click()
    Set MDI.Picture = LoadPicture("")
    MDI.Hide
    MDI.Show
    oGestorParametros.GuardarFondoInstalacion "", oUsuarioSummit.Cod
End Sub

''' <summary>Carga los recursos de texto del formulario</summary>
''' <remarks>Llamada desde Form_Load</remarks>
Private Sub CargarRecursos()
Dim adoresAdo As Ador.Recordset
Dim i As Integer
    On Error Resume Next
    Set adoresAdo = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIST, basPublic.gParametrosInstalacion.gIdioma)
    If Not adoresAdo Is Nothing Then
        For i = 1 To 8
            m_sCap(i) = adoresAdo(0).Value
            adoresAdo.MoveNext
        Next
        tabCONFINST.TabCaption(0) = adoresAdo(0).Value
        adoresAdo.MoveNext
        tabCONFINST.TabCaption(2) = adoresAdo(0).Value '10
        adoresAdo.MoveNext
        tabCONFINST.TabCaption(3) = adoresAdo(0).Value
        adoresAdo.MoveNext
        tabCONFINST.TabCaption(6) = adoresAdo(0).Value
        adoresAdo.MoveNext
        tabCONFINST.TabCaption(5) = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraManten(0).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label16.caption = adoresAdo(0).Value '15
        adoresAdo.MoveNext
        Label17.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label18.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label19.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        sdbcMonCod.Columns(0).caption = adoresAdo(0).Value '19 Cod
        sdbcPaiCod.Columns(0).caption = adoresAdo(0).Value
        sdbcProviCod.Columns(0).caption = adoresAdo(0).Value
        sdbcDestCod.Columns(0).caption = adoresAdo(0).Value
        sdbcPagoCod.Columns(0).caption = adoresAdo(0).Value
        sdbcMonDen.Columns(1).caption = adoresAdo(0).Value
        sdbcPaiDen.Columns(1).caption = adoresAdo(0).Value
        sdbcProviDen.Columns(1).caption = adoresAdo(0).Value
        sdbcDestDen.Columns(1).caption = adoresAdo(0).Value
        sdbcPagoDen.Columns(1).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        sdbcMonDen.Columns(0).caption = adoresAdo(0).Value '20 Denominaci�n
        sdbcPaiDen.Columns(0).caption = adoresAdo(0).Value
        sdbcProviDen.Columns(0).caption = adoresAdo(0).Value
        sdbcDestDen.Columns(0).caption = adoresAdo(0).Value
        sdbcPagoDen.Columns(0).caption = adoresAdo(0).Value
        sdbcMonCod.Columns(1).caption = adoresAdo(0).Value
        sdbcPaiCod.Columns(1).caption = adoresAdo(0).Value
        sdbcProviCod.Columns(1).caption = adoresAdo(0).Value
        sdbcDestCod.Columns(1).caption = adoresAdo(0).Value
        sdbcPagoCod.Columns(1).caption = adoresAdo(0).Value
        lblDen1.caption = adoresAdo(0).Value & ":"
        sdbgCamposPedido.Columns("DEN").caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        SSTabOfertas.TabCaption(0) = adoresAdo(0).Value
        adoresAdo.MoveNext
        SSTabOfertas.TabCaption(1) = adoresAdo(0).Value
        adoresAdo.MoveNext
        SSTabOfertas.TabCaption(2) = adoresAdo(0).Value
        adoresAdo.MoveNext
        SSTabOfertas.TabCaption(3) = adoresAdo(0).Value
        adoresAdo.MoveNext
        SSTabOfertas.TabCaption(4) = adoresAdo(0).Value '25
        adoresAdo.MoveNext
        Frame9.caption = adoresAdo(0).Value '26 Formulario de oferta
        Frame16.caption = adoresAdo(0).Value
        fraModSub(0).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label33.caption = adoresAdo(0).Value '27 Plantilla:
        label2.caption = adoresAdo(0).Value
        Label53.caption = adoresAdo(0).Value
        Label5.caption = Label53.caption
        Label11.caption = Label53.caption
        Label40.caption = adoresAdo(0).Value
        Label52.caption = adoresAdo(0).Value
        lblModSub(0).caption = adoresAdo(0).Value
        Label20.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame10.caption = adoresAdo(0).Value 'Plantillas para peticiones de oferta
        fraModSub(1).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label34.caption = adoresAdo(0).Value '29 Plantilla carta:
        Label12.caption = adoresAdo(0).Value
        Label42.caption = adoresAdo(0).Value
        Label50.caption = adoresAdo(0).Value
        lblModSub(1).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label35.caption = adoresAdo(0).Value '30 Plantilla e-mail:
        Label13.caption = adoresAdo(0).Value
        Label41.caption = adoresAdo(0).Value
        Label51.caption = adoresAdo(0).Value
        lblModSub(2).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label36.caption = adoresAdo(0).Value '31 Plantilla web:
        Label14.caption = adoresAdo(0).Value
        lblModSub(3).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame2.caption = adoresAdo(0).Value 'Email de petici�n de oferta
        fraModSub(2).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label3.caption = adoresAdo(0).Value '33 Asunto:
        Label15.caption = adoresAdo(0).Value
        Label43.caption = adoresAdo(0).Value
        Label46.caption = adoresAdo(0).Value
        Label10(8).caption = adoresAdo(0).Value
        Label10(9).caption = adoresAdo(0).Value
        Label10(7).caption = adoresAdo(0).Value
        Label10(10).caption = adoresAdo(0).Value
        Label10(12).caption = adoresAdo(0).Value
        lblModSub(4).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame17.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame18.caption = adoresAdo(0).Value '35
        adoresAdo.MoveNext
        Frame28.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label1.caption = adoresAdo(0).Value
        Label7.caption = Label1.caption
        Label22.caption = Label1.caption
        adoresAdo.MoveNext
        'FrameAdj(0).Caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        'ChkAdjOrden.Caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        FrameAdj(1).caption = adoresAdo(0).Value '40
        adoresAdo.MoveNext
        FrameAdj(2).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        FrameAdj(3).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame22.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame23.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame3.caption = adoresAdo(0).Value '45
        adoresAdo.MoveNext
        Label4.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame12.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label8.caption = adoresAdo(0).Value '48 Carta:
        LblPedCarta(0).caption = adoresAdo(0).Value
        LblPedCarta(1).caption = adoresAdo(0).Value
        LblPedCarta(2).caption = adoresAdo(0).Value
        LblPedCarta(3).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        LblPedMail(0).caption = adoresAdo(0).Value
        LblPedMail(1).caption = adoresAdo(0).Value
        LblPedMail(2).caption = adoresAdo(0).Value
        LblPedMail(3).caption = adoresAdo(0).Value
        LblPedMail(4).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label6.caption = adoresAdo(0).Value '50 ' Asunto
        adoresAdo.MoveNext
        Frame13.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame15.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame26.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        chkMostrarMail.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraInterfaz.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        cmdCambiarFondo.caption = adoresAdo(0).Value '60
        adoresAdo.MoveNext
        cmdQuitarFondo.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraLenguaje.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraListados.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        cmdEdicion.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        cmdAceptar.caption = adoresAdo(0).Value      '65
        adoresAdo.MoveNext
        cmdCancelar.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        frmCONFINST.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        cdlArchivo.DialogTitle = adoresAdo(0).Value
        adoresAdo.MoveNext
        cdlFondo.DialogTitle = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraLenguajePortal.caption = adoresAdo(0).Value '70
        adoresAdo.MoveNext
        SSTabOfertas.TabCaption(5) = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraBuzPeriodo.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        lblbuzperiodo.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraLeerOfer.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        opLeidas.caption = adoresAdo(0).Value '75
        adoresAdo.MoveNext
        opNoLeidas.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        opLeerTodas.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraComprobarOfer.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        lblComprobarOfer.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        lblMinutos.caption = adoresAdo(0).Value '80
        adoresAdo.MoveNext
        m_sSemana = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sSemanas = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sMes = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sMeses = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraRegGest.caption = adoresAdo(0).Value   '85
        adoresAdo.MoveNext
        opAdjAutomat.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        opAdjPreguntar.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        opNoAdj.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        tabCONFINST.TabCaption(1) = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraRegGest2.caption = adoresAdo(0).Value '90
        adoresAdo.MoveNext
        OptPasoAut.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        OptSiPasar.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        OptNoPasar.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        frmNotiPed.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        frmAnuPed.caption = adoresAdo(0).Value '95
        adoresAdo.MoveNext
        tabCONFINST.TabCaption(4) = adoresAdo(0).Value
        Me.fraConfProce(4).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        frmDatExt.caption = adoresAdo(0)
        adoresAdo.MoveNext
        Label10(11).caption = adoresAdo(0)
        LblCartaPed(0).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        TabPedidos.TabCaption(0) = adoresAdo(0).Value
        adoresAdo.MoveNext
        TabPedidos.TabCaption(2) = adoresAdo(0).Value '100
        adoresAdo.MoveNext
        TabPedidos.TabCaption(3) = adoresAdo(0).Value
        adoresAdo.MoveNext
        TabPedidos.TabCaption(4) = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraRecepOKPed.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraRecepKOPed.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraAsuntoRecPed.caption = adoresAdo(0).Value '105
        adoresAdo.MoveNext
        fraAsuntoAnuPed.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraAsuntoEmiPed.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraDecComparativa.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label10(6).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        FraForPedido.caption = adoresAdo(0).Value '110
        adoresAdo.MoveNext
        LblCartaPed(5).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        LblCartaPed(4).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraDatosExtSubject.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        m_sDia = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sDias = adoresAdo(0).Value
        adoresAdo.MoveNext
        frmAvisoCierre.caption = adoresAdo(0).Value '120
        adoresAdo.MoveNext
        chkMostrarMensaje.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        sstabGestion.TabCaption(0) = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraValores(0).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        lblCod1.caption = adoresAdo(0).Value & ":"
        sdbgCamposPedido.Columns("COD").caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        lblDescrip1.caption = adoresAdo(0).Value '125
        adoresAdo.MoveNext
        sstabGestion.TabCaption(2) = adoresAdo(0).Value
        adoresAdo.MoveNext
        sstabGestion.TabCaption(1) = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraAmbito.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraSolicitud.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkSolOfe(0).caption = adoresAdo(0).Value '140
        adoresAdo.MoveNext
        chkSolOfe(1).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkSolOfe(2).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkSolOfe(3).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkSolOfe(4).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkSolOfe(5).caption = adoresAdo(0).Value '145
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        fraPonderar.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkPonderar.caption = adoresAdo(0).Value '150
        adoresAdo.MoveNext
        sdbgDatosGrid.Columns(0).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        sdbgDatosGrid.Columns(1).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        sdbgDatosGrid.Columns(2).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        sdbgDatosGrid.Columns(3).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        sdbgDatosGrid.Columns(4).caption = adoresAdo(0).Value '155
        adoresAdo.MoveNext
        For i = 0 To 9
            m_sDatos(i) = adoresAdo(0).Value
            adoresAdo.MoveNext
        Next i
        If gParametrosGenerales.gsPlurPres1 <> "" Then m_sDatos(6) = gParametrosGenerales.gsPlurPres1
        If gParametrosGenerales.gsPlurPres2 <> "" Then m_sDatos(7) = gParametrosGenerales.gsPlurPres2
        If gParametrosGenerales.gsPlurPres3 <> "" Then m_sDatos(8) = gParametrosGenerales.gsPlurPres3
        If gParametrosGenerales.gsPlurPres4 <> "" Then m_sDatos(9) = gParametrosGenerales.gsPlurPres4
        SSTabHabSub.TabCaption(0) = adoresAdo(0).Value '156
        adoresAdo.MoveNext
        SSTabHabSub.TabCaption(1) = adoresAdo(0).Value '157
        adoresAdo.MoveNext
        lblBcc.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sIdiCC = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sIdiBCC = adoresAdo(0).Value '160
        adoresAdo.MoveNext
        Frame4.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkAcuseRecibo.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraListados.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraSolicitudes.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sDatos(10) = adoresAdo(0).Value
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        chkAdminPub.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraManten(1).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkSugerir.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        chkPlantAdj(1).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkPlantAdj(0).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkNoPublicarFinSum.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraRegGest3.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkSelPositiva.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraNumDecRecepcion.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        lblNumDecRecep.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        optPresAdj.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        optPresPlanif.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        optReuActa(0).caption = adoresAdo(0).Value & "1:"
        optReuActa(1).caption = adoresAdo(0).Value & "2:"
        adoresAdo.MoveNext
        fraNotifAcep.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        frmNotifPed.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        LblPedCarta(4).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        LblPedMail(5).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        TabPedidos.TabCaption(1) = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sCap(9) = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sCap(10) = adoresAdo(0).Value
        adoresAdo.MoveNext
        lblFormatoMail(0).caption = adoresAdo(0).Value  '188 Recibir los emails en formato:
        adoresAdo.MoveNext
        lblFormatoMail(1).caption = adoresAdo(0).Value  '189
        adoresAdo.MoveNext
        optFormatoEMail(0).caption = adoresAdo(0).Value '190 HTML
        Label9.caption = adoresAdo(0).Value & ":"
        adoresAdo.MoveNext
        optFormatoEMail(1).caption = adoresAdo(0).Value '191 txt
        Label23.caption = adoresAdo(0).Value & ":"
        adoresAdo.MoveNext
        sstabGestion.TabCaption(3) = adoresAdo(0).Value  '192 Avisos
        adoresAdo.MoveNext
        If gParametrosGenerales.gbSincronizacionMat Then fraLenguajePortal.caption = adoresAdo(0).Value               '193 Idioma para los datos de la estructura de materiales
        adoresAdo.MoveNext
        chkUnSoloPedido.caption = adoresAdo(0).Value  '194 Permitir un solo pedido directo
        adoresAdo.MoveNext
        lblPago.caption = adoresAdo(0).Value  '195 Forma de pago:
        adoresAdo.MoveNext
        fraValores(1).caption = adoresAdo(0).Value  '196 otros
        adoresAdo.MoveNext
        m_sCap(11) = adoresAdo(0).Value '197 La forma de pago seleccionada por defecto ya no es correcta.
        If m_bMostrarSolicitud Then
            adoresAdo.MoveNext
            sstabGestion.TabCaption(4) = adoresAdo(0).Value  '198 Solicitudes de compra
            adoresAdo.MoveNext
            fraSolicit(0).caption = adoresAdo(0).Value  '199 Generaci�n de procesos desde solicitud
            adoresAdo.MoveNext
            fraSolicit(1).caption = adoresAdo(0).Value  '200 Generaci�n de ofertas desde solicitud
            adoresAdo.MoveNext
            optGenerarProc(0).caption = adoresAdo(0).Value  '201 Generar procesos de adjudicaci�n directa
            adoresAdo.MoveNext
            optGenerarProc(1).caption = adoresAdo(0).Value  '202 Generar procesos de reuni�n
            adoresAdo.MoveNext
            chkAdjDirImp.caption = adoresAdo(0).Value  '203 Para importes menores de:
            adoresAdo.MoveNext
            lblPeriodoSolic(0).caption = adoresAdo(0).Value   '204 Per�odo de validez
        Else
            adoresAdo.MoveNext
            adoresAdo.MoveNext
            adoresAdo.MoveNext
            adoresAdo.MoveNext
            adoresAdo.MoveNext
            adoresAdo.MoveNext
            adoresAdo.MoveNext
        End If
        adoresAdo.MoveNext
        m_sCap(12) = adoresAdo(0).Value  '205
        adoresAdo.MoveNext
        m_sAnyo = adoresAdo(0).Value  '206 a�o
        adoresAdo.MoveNext
        m_sAnyos = adoresAdo(0).Value '207 a�os
        adoresAdo.MoveNext
        chkSolOfe(6).caption = adoresAdo(0).Value '208
        adoresAdo.MoveNext
        OptActaWord.caption = adoresAdo(0).Value ' 209 Acta en Word
        adoresAdo.MoveNext
        optActaCrystal.caption = adoresAdo(0).Value ' 210 Acta en Crystal
        adoresAdo.MoveNext
        m_sCap(13) = adoresAdo(0).Value '211 Plantillas de crystal
        adoresAdo.MoveNext
        frmAvisoOfertas.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkCompResp.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkCompAsig.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Frame1.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        Me.frmEmailConv.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        lblIdiConv.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraTimeZone.caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label10(0).caption = adoresAdo(0).Value
        Label10(3).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label10(1).caption = adoresAdo(0).Value
        Label10(4).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label10(2).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sTextosFormatoPlantilla(1) = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sTextosFormatoPlantilla(2) = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sTextosFormatoPlantilla(3) = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sTextosFormatoPlantilla(4) = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sTextosFormatoPlantilla(5) = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sVistasPlantilla(1) = adoresAdo(0).Value
        adoresAdo.MoveNext
        m_sVistasPlantilla(2) = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkNotifPed(2).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        Label10(5).caption = adoresAdo(0).Value & ":"
        adoresAdo.MoveNext
        chkNotifPed(0).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkNotifPed(1).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        fraNotiPed(3).caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        TabPedidos.TabCaption(5) = adoresAdo(0).Value
        adoresAdo.MoveNext
        sdbgCamposPedido.Columns("VALOR").caption = adoresAdo(0).Value
        adoresAdo.MoveNext
        msTrue = adoresAdo(0).Value
        adoresAdo.MoveNext
        msFalse = adoresAdo(0).Value
        adoresAdo.MoveNext
        'No se usa el mensaje generico
        m_sDenListaExterna = adoresAdo(0).Value
        adoresAdo.MoveNext
        msCamposNoSeleccionados = adoresAdo(0).Value
        adoresAdo.Close
    End If
    Set adoresAdo = Nothing
    m_sIdiCodGrupo = fraValores(0) & " (" & lblCod1.caption & ")"
    If m_bMostrarSolicitud Then lblPeriodoSolic(1).caption = gParametrosGenerales.gsMONCEN
End Sub

Private Sub ctrlConfGenAvisos_AbrirDialogoArchivo(Index As Integer)
On Error GoTo ERROR:
If Index = 0 Then
    cdlArchivo.Filter = m_sCap(10) & " (*.htm)|*.htm"
    cdlArchivo.DefaultExt = "htm"
ElseIf Index = 1 Then
    cdlArchivo.Filter = m_sCap(9) & " (*.txt)|*.txt"
    cdlArchivo.DefaultExt = "txt"
End If
cdlArchivo.Action = 1
Me.ctrlConfGenAvisos.PlantillaDespub(Index) = cdlArchivo.filename
ERROR:
    If err.Number = cdlCancel Then
        Exit Sub
    Else
        Resume Next
    End If
End Sub

Private Sub Form_Load()
    Dim oIdioma As CIdioma
    Dim i As Integer
    Dim iBuzper As Integer
    
    Me.Width = 9855
    Me.Height = 7620
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    ctrlConfGenAvisos.idioma = gParametrosInstalacion.gIdioma
    Set ctrlConfGenAvisos.GestorIdiomas = oGestorIdiomas
    ctrlConfGenAvisos.Initialize
    
    m_bCambiarIdioma = False
    PonerFieldSeparator Me
    ctrlConfGenAvisos.fraPartic.Visible = False
    ctrlConfGenAvisos.fraAdj0.Top = 30
    ctrlConfGenAvisos.fraAdj1.Top = 1350
    If FSEPConf Then
        tabCONFINST.TabVisible(1) = False
        tabCONFINST.TabVisible(2) = False
        tabCONFINST.TabVisible(3) = False
        fraSolicitudes.Visible = False
        m_bMostrarSolicitud = False
        fraLenguaje.Top = frmAvisoCierre.Top
        m_bMostrarCarpetas = False
        Me.sstabGestion.TabVisible(4) = False
    Else
        'Si no se tiene acceso a GS ocultar las pesta�as Gesti�n, Ofertas y Reuniones
        If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
            tabCONFINST.TabVisible(1) = False
            tabCONFINST.TabVisible(2) = False
            tabCONFINST.TabVisible(3) = False
        End If
        'Comprueba si se mostrar� o no la solicitud de compra
        If basParametros.gParametrosGenerales.gbSolicitudesCompras = False Then
            m_bMostrarSolicitud = False
            fraSolicitudes.Visible = True
            m_bMostrarCarpetas = True
            Me.sstabGestion.TabVisible(4) = False
        Else
            m_bMostrarSolicitud = True
            fraSolicitudes.Visible = True
            m_bMostrarCarpetas = True
            Me.sstabGestion.TabVisible(4) = True
        End If
    End If
    CargarRecursos
    Set oFos = New FileSystemObject
    cmdEdicion.Left = tabCONFINST.Left + tabCONFINST.Width - cmdEdicion.Width
    cdlArchivo.FLAGS = cdlOFNFileMustExist & cdlOFNHideReadOnly
    cdlFondo.FLAGS = cdlOFNFileMustExist & cdlOFNHideReadOnly
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas
    i = 0
    For Each oIdioma In m_oIdiomas
        sdbcIdiomas.AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdiomaConv.AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        If oIdioma.Cod = gParametrosInstalacion.gIdioma Then
            m_vIdiIdiomasRow = sdbcIdiomas.GetBookmark(i)
            m_vIdiConvocatoriaRow = sdbcIdiomaConv.GetBookmark(i)
        End If
        i = i + 1
    Next
    If gParametrosGenerales.giINSTWEB = ConPortal Then
        fraLenguajePortal.Visible = True
        Set m_oIdiomasPortal = oGestorParametros.DevolverIdiomas(True)
        i = 0
        For Each oIdioma In m_oIdiomasPortal
            sdbcIdiomaPortal.AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod
            If oIdioma.Cod = gParametrosInstalacion.gIdiomaPortal Then m_vIdiIdiomasPortalRow = sdbcIdiomaPortal.GetBookmark(i)
            i = i + 1
        Next
    Else
        fraLenguajePortal.Visible = False
    End If
    If gParametrosGenerales.giINSTWEB = SinWeb Then
        SSTabHabSub.TabVisible(1) = False
        Label14.Visible = False
        Label36.Visible = False
        txtPlantilla(7).Visible = False
        txtPlantilla(3).Visible = False
        cmdPlantilla(7).Visible = False
        cmdPlantilla(3).Visible = False
        Frame10.Height = Frame10.Height - 325
        Frame17.Height = Frame17.Height - 325
        PicPetOfe2.Height = PicPetOfe2.Height - 325
        picObj2.Height = picObj2.Height - 325
        Frame18.Top = Frame18.Top - 325
        Frame2.Top = Frame2.Top - 325
        fraComprobarOfer.Visible = False
        fraNumDecRecepcion.Top = fraComprobarOfer.Top
        chkSolOfe(0).Visible = False
        cmdConfigSubasta.Visible = False
    End If
    If ADMIN_OBLIGATORIO = True Then SSTabHabSub.TabVisible(1) = False
    Me.fraConfProce(4).Visible = (gParametrosGenerales.gbPedidosDirectos)
     'Rellenar combo de Antig�edad de las ofertas a mostrar de las despublicaciones y de las adjudicaciones
    With sdbcAntigOfer
        iBuzper = 7
        i = 1
        .AddItem i & " " & m_sSemana & Chr(m_lSeparador) & iBuzper
        For i = 2 To 3
           .AddItem i & " " & m_sSemanas & Chr(m_lSeparador) & iBuzper * i
        Next i
        iBuzper = 30
        i = 1
        .AddItem i & " " & m_sMes & Chr(m_lSeparador) & iBuzper
        For i = 2 To 6
            .AddItem i & " " & m_sMeses & Chr(m_lSeparador) & iBuzper * i
        Next i
    End With
    If m_bMostrarSolicitud Then
        With sdbcPeriodoSolic
            iBuzper = 7
            i = 1
            .AddItem i & " " & m_sSemana & Chr(m_lSeparador) & iBuzper
            For i = 2 To 3
               .AddItem i & " " & m_sSemanas & Chr(m_lSeparador) & iBuzper * i
            Next i
            iBuzper = 30
            i = 1
            .AddItem i & " " & m_sMes & Chr(m_lSeparador) & iBuzper
            For i = 2 To 6
                .AddItem i & " " & m_sMeses & Chr(m_lSeparador) & iBuzper * i
            Next i
            iBuzper = 365
            i = 1
            .AddItem i & " " & m_sAnyo & Chr(m_lSeparador) & iBuzper
            For i = 2 To 3
                .AddItem i & " " & m_sAnyos & Chr(m_lSeparador) & iBuzper * i
            Next i
        End With
    End If
    'Cargar combo zonas horarias
    CargarComboTZ
    If gParametrosGenerales.gbSubasta = False Then
        chkSolOfe(0).Visible = False
        cmdConfigSubasta.Visible = False
    Else
        If gParametrosGenerales.giINSTWEB <> SinWeb Then
            chkSolOfe(0).Visible = True
            cmdConfigSubasta.Visible = True
        End If
    End If
    fraPonderar.Visible = Not (gParametrosGenerales.gbUsarPonderacion = False)
    MostrarParametros
    ModoConsulta
    txtCodVal.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE
    If ADMIN_OBLIGATORIO Then
        'No aparece la check de "Proceso de admin.pub" porque ser�n obligatoriamente de admin.p�blica
        fraAmbito.Height = 2260
        picAmbitoDatos.Height = 1915
        sdbgDatosGrid.Height = 1915
        fraAdminPub.Visible = False
    Else
        If basParametros.gParametrosGenerales.gbAdminPublica = True Then
            'Check para indicar si ser� o no un proceso de administraci�n p�blica:
            fraAdminPub.Visible = True
        Else
            fraAmbito.Height = 2260
            picAmbitoDatos.Height = 1915
            sdbgDatosGrid.Height = 1915
            fraAdminPub.Visible = False
        End If
    End If
    InicializarValoresSubasta
    
    Set m_oAtribs = oFSGSRaiz.Generar_CAtributos
    CargarAtributos
End Sub
''' <summary>Carga los combos de zonas horarias</summary>
Private Sub CargarComboTZ()
    Dim dcListaZonasHorarias As Dictionary  'TIME_ZONE_INFO
    Dim iIndex As Integer
    On Error GoTo ERROR
    Set dcListaZonasHorarias = ObtenerZonasHorarias
    For iIndex = 0 To dcListaZonasHorarias.Count - 1
        sdbcTZ.AddItem dcListaZonasHorarias.Items(iIndex) & Chr(m_lSeparador) & dcListaZonasHorarias.Keys(iIndex)
    Next
    sdbcTZ.ListAutoPosition = True
    Set dcListaZonasHorarias = Nothing
    Exit Sub
ERROR:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
End Sub
''' <summary>Inicializa los valores de las variables de par�metros de subasta</summary>
''' <remarks>Llamada desde Form_Load, cmdCancelar_Click</remarks>
Private Sub InicializarValoresSubasta()
With gParametrosInstalacion
    m_iSubTipo = .gCPSubTipo
    m_iSubModo = .gCPSubModo
    m_vSubDuracion = .gCPSubDuracion
    m_iSubastaEspera = .gCPSubastaEspera
    m_bSubPublicar = .gCPSubPublicar
    m_bSubNotifEventos = .gCPSubNotifEventos
    m_bSubastaProve = .gCPSubastaProve
    m_bSubVerDesdePrimPuja = .gCPSubVerDesdePrimPuja
    m_bSubastaPrecioPuja = .gCPSubastaPrecioPuja
    m_bSubastaPujas = .gCPSubastaPujas
    m_bSubastaBajMinPuja = .gCPSubastaBajMinPuja
    m_iSubBajMinGanTipo = .gCPSubBajMinProveTipo
    m_vSubBajMinGanProcVal = .gCPSubBajMinGanProcVal
    m_vSubBajMinGanGrupoVal = .gCPSubBajMinGanGrupoVal
    m_vSubBajMinGanItemVal = .gCPSubBajMinGanItemVal
    m_bSubBajMinProve = .gCPSubBajMinProve
    m_iSubBajMinProveTipo = .gCPSubBajMinProveTipo
    m_vSubBajMinProveProcVal = .gCPSubBajMinProveProcVal
    m_vSubBajMinProveGrupoVal = .gCPSubBajMinProveGrupoVal
    m_vSubBajMinProveItemVal = .gCPSubBajMinProveItemVal
    Set m_dcSubTextosFin = .gCPSubTextoFin
End With
End Sub
''' <summary>Carga los controles del formulario con los datos de los par�metros</summary>
''' <remarks>Llamada desde Form_Load, cmdCancelar_Click</remarks>
Private Sub MostrarParametros()
Dim dcListaZonasHorarias As Dictionary
Dim bm As Variant
Dim i As Integer
On Error GoTo ERROR:
With gParametrosInstalacion
    sdbcMonCod = .gsMoneda
    sdbcMonCod_Validate False
    sdbcPaiCod = .gsPais
    sdbcPaiCod_Validate False
    sdbcProviCod = .gsProvincia
    sdbcProviCod_Validate False
    sdbcDestCod = .gsDestino
    sdbcDestCod_Validate False
    sdbcPagoCod = .gsFormaPago
    sdbcPagoCod_Validate False
    chkSugerir.Value = IIf(.gbSugerirCodArticulos, vbChecked, vbUnchecked)
    txtPlantilla(0) = .gsPetOfe
    txtPlantilla(1) = .gsPetOfeCarta
    txtPlantilla(2) = .gsPetOfeMail
    txtPlantilla(3) = .gsPetOfeWeb
    txtPlantilla(4) = .gsObj
    txtPlantilla(5) = .gsObjCarta
    txtPlantilla(6) = .gsObjMail
    txtPlantilla(7) = .gsObjWeb
    txtPlantilla(8) = .gsComparativa
    txtPlantilla(37) = .gsComparativaItem
    txtPlantilla(42) = .gsComparativaQA
    txtPlantilla(9) = .gsAdj
    txtPlantilla(10) = .gsAdjCarta
    txtPlantilla(11) = .gsAdjMail
    txtPlantilla(12) = .gsHojaAdjDir
    txtPlantilla(13) = .gsAdjNoCarta
    txtPlantilla(14) = .gsAdjNoMail
    txtPlantilla(38) = .gsAdjNotif
    chkPlantAdj(0).Value = IIf(.gbAdjOrden, vbChecked, vbUnchecked)
    chkPlantAdj(1).Value = IIf(.gbAdjNotif, vbChecked, vbUnchecked)
    txtPlantilla(15) = .gsHojaAdjProce
    txtPlantilla(16) = .gsConvocatoria
    txtConvMailSubject.ToolTipText = txtConvMailSubject
    Select Case UCase(.gIdioma)
    Case "SPA"
        txtPlantilla(17) = .gsConvocatoriaMailHtmlSPA
        txtPlantilla(43) = .gsConvocatoriaMailTextSPA
        txtConvMailSubject = .gsConvocatoriaMailSubjectSPA
    Case "ENG"
        txtPlantilla(17) = .gsConvocatoriaMailHtmlENG
        txtPlantilla(43) = .gsConvocatoriaMailTextENG
        txtConvMailSubject = .gsConvocatoriaMailSubjectENG
    Case "GER"
        txtPlantilla(17) = .gsConvocatoriaMailHtmlGER
        txtPlantilla(43) = .gsConvocatoriaMailTextGER
        txtConvMailSubject = .gsConvocatoriaMailSubjectGER
    Case "FRA"
        txtPlantilla(17) = .gsConvocatoriaMailHtmlFRA
        txtPlantilla(43) = .gsConvocatoriaMailTextFRA
        txtConvMailSubject = .gsConvocatoriaMailSubjectFRA
    End Select
    If .giActaTipo = ActaWord Then
        OptActaWord.Value = True
        Label10(2).Visible = True
        Label10(3).Visible = True
        optReuActa(0).Visible = False
        txtPlantilla(19).Visible = False
        cmdPlantilla(19).Visible = False
        sdbcPlantilla(2).Visible = True
        optReuActa(1).Visible = False
        txtPlantilla(39).Visible = False
        cmdPlantilla(40).Visible = False
        sdbcPlantilla(3).Visible = True
    Else
        optActaCrystal.Value = True
        Label10(2).Visible = False
        Label10(3).Visible = False
        optReuActa(0).Visible = True
        txtPlantilla(19).Visible = True
        cmdPlantilla(19).Visible = True
        sdbcPlantilla(2).Visible = False
        optReuActa(1).Visible = True
        txtPlantilla(39).Visible = True
        cmdPlantilla(40).Visible = True
        sdbcPlantilla(3).Visible = False
    End If
    CargarDatosPlantillas
    txtPlantilla(19) = .gsActa
    txtPlantilla(39) = .gsActa2
    txtPlantilla(20) = .gsPedidoDirEmisionMail
    txtPlantilla(21) = .gsPedidoDirEmisionCarta
    txtPlantilla(22) = .gsPedidoDirAnulacionCarta
    txtPlantilla(23) = .gsPedidoDirAnulacionMail
    txtPlantilla(25) = .gsDatosExternosExcel
    txtPlantilla(24) = .gsDatosExternosMail
    txtPlantilla(27) = .gsPedidoDirRecepCartaOK
    txtPlantilla(26) = .gsPedidoDirRecepMailOK
    txtPlantilla(29) = .gsPedidoDirRecepCartaKO
    txtPlantilla(28) = .gsPedidoDirRecepMailKO
    txtPlantilla(30) = .gsPedidoDot
    txtPlantilla(31) = .gsPedRec
    txtPlantilla(32) = .gsPedidoXLT
    txtPlantilla(40) = .gsPEDNotifAcepHTML
    txtPlantilla(41) = .gsPEDNotifAcepTXT
    txtMailSubject(0) = .gsPetOfeMailSubject
    txtMailSubject(1) = .gsObjMailSubject
    txtMailSubject(2) = .gsAdjMailSubject
    txtMailSubject(3) = .gsAdjNoMailSubject
    txtMailSubject(4) = .gsPedRecMailSubject
    txtMailSubject(5) = .gsPedAnuMailSubject
    txtMailSubject(6) = .gsPedEmiMailSubject
    txtMailSubject(7) = .gsDatosExtMailSubject
    txtMailSubject(9) = .gsPedNotifMailSubject
    chkMostrarMail = IIf(.gbMostrarMail, vbChecked, vbUnchecked)
    chkAcuseRecibo = IIf(.gbAcuseRecibo, vbChecked, vbUnchecked)
    txtCc.Text = .gsRecipientCC
    txtBcc.Text = .gsRecipientBCC
    chkMostrarMensaje = IIf(.gbMensajeCierre, vbChecked, vbUnchecked)
    optFormatoEMail(0).Value = (.gbTipoMail = True)
    optFormatoEMail(1).Value = Not (.gbTipoMail = True)
    tabCONFINST.Tab = 0
    If .gsRPTPATH <> "" Then
        DirRPT.Path = .gsRPTPATH
        txtRuta = .gsRPTPATH
    End If
    If .gsPathSolicitudes <> "" Then
        DirSolicitudes.Path = .gsPathSolicitudes
        txtRutaSolicitudes = .gsPathSolicitudes
    End If
    txtLongCabcomp.Text = .giLongCabComp
    txtLongCabCompItem.Text = .giLongCabCompItem
    txtLongCabCompQA.Text = .giLongCabCompQA
    sdbcIdiomas.Text = m_oIdiomas.Item(CStr(.gIdioma)).Den
    sdbcIdiomas.Bookmark = m_vIdiIdiomasRow
    sdbcIdiomaConv.Text = m_oIdiomas.Item(CStr(.gIdioma)).Den
    sdbcIdiomaConv.Bookmark = m_vIdiConvocatoriaRow
    'Zona horaria
    Set dcListaZonasHorarias = ObtenerZonasHorarias
    sdbcTZ.Text = dcListaZonasHorarias.Item(oUsuarioSummit.TimeZone)
    sdbcTZ.Columns(0).Value = dcListaZonasHorarias.Item(oUsuarioSummit.TimeZone)
    sdbcTZ.Columns(1).Value = oUsuarioSummit.TimeZone
    Set dcListaZonasHorarias = Nothing
    If gParametrosGenerales.giINSTWEB = ConPortal Then
         sdbcIdiomaPortal.Text = m_oIdiomasPortal.Item(CStr(.gIdiomaPortal)).Den
         sdbcIdiomaPortal.Bookmark = m_vIdiIdiomasPortalRow
    End If
    sdbcAntigOfer.MoveFirst
    For i = 0 To sdbcAntigOfer.Rows - 1
        bm = sdbcAntigOfer.GetBookmark(i)
        If .giBuzonPeriodo = sdbcAntigOfer.Columns(1).Value Then
            sdbcAntigOfer.Text = sdbcAntigOfer.Columns(0).Value
            Exit For
        End If
        sdbcAntigOfer.MoveNext
    Next i
    Select Case .giBuzonOfeLeidas
        Case 0:     opNoLeidas.Value = True
        Case 1:     opLeidas.Value = True
        Case 2:     opLeerTodas.Value = True
    End Select
    txtMinutos.Text = .giBuzonTimer
    chkCompResp = IIf(.gbAvisoPortalCompResp = True, vbChecked, vbUnchecked)
    chkCompAsig = IIf(.gbAvisoPortalCompAsign = True, vbChecked, vbUnchecked)
    Select Case .giAnyadirEspecArticulo
        Case 1:     opAdjAutomat.Value = True
        Case 2:     opAdjPreguntar.Value = True
        Case 3:     opNoAdj.Value = True
    End Select
    Select Case .giPasoAutomatico
        Case 1:     OptPasoAut.Value = True
        Case 2:     OptSiPasar.Value = True
        Case 3:     OptNoPasar.Value = True
    End Select
    Me.ctrlConfGenAvisos.AvisoAdjSoloSiResp = .gbAvisoAdj
    Me.ctrlConfGenAvisos.AvisoVisorGSSoloSiResp = .gbAvisoDespublica
    Me.ctrlConfGenAvisos.AntelacionAdjudicacion = .giAvisoAdj
    Me.ctrlConfGenAvisos.AntelacionVisorGS = .giAvisoDespublica
    If FSEPConf Then
        tabCONFINST.TabVisible(4) = True
        TabPedidos.TabVisible(4) = True
    Else
        If Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov Then
            tabCONFINST.TabVisible(4) = False
        Else
            tabCONFINST.TabVisible(4) = True
            TabPedidos.TabVisible(4) = (gParametrosGenerales.gbPedidosAprov = True)
        End If
    End If
    fraPonderar.Visible = (gParametrosGenerales.gbUsarPonderacion = True)
    
    If gParametrosGenerales.gbUsarPonderacion = True Then
        chkPonderar.Value = IIf(.gCPPonderar, vbChecked, vbUnchecked)
    Else
        chkPonderar.Value = vbUnchecked
    End If
        
    txtCodVal.Text = .gsGrupoCod
    txtDenVal.Text = .gsGrupoDen
    txtDescVal.Text = .gsGrupoDescrip
    chkSolOfe(1).Value = IIf(.gCPAlternativasPrec = True, vbChecked, vbUnchecked)
    chkSolOfe(2).Value = IIf(.gCPSolCantMax = True, vbChecked, vbUnchecked)
    chkSolOfe(6).Value = IIf(.gCPCambiarMonOferta = True, vbUnchecked, vbChecked)
    chkSolOfe(0) = IIf(.gCPSubasta = True, vbChecked, vbUnchecked)
    chkSolOfe(3).Value = IIf(.gCPAdjunOfe = True, vbChecked, vbUnchecked)
    chkSolOfe(4).Value = IIf(.gCPAdjunGrupo = True, vbChecked, vbUnchecked)
    chkSolOfe(5).Value = IIf(.gCPAdjunItem = True, vbChecked, vbUnchecked)
    Me.chkNoPublicarFinSum.Value = IIf(.gCPNoPublicarFinSum = True, vbChecked, vbUnchecked)
    chkAdminPub.Value = IIf(.gCPProcesoAdminPub = True, vbChecked, vbUnchecked)
    Me.chkUnSoloPedido.Value = IIf(.gbUnSoloPedido = True, vbChecked, vbUnchecked)
    txtNumDec.Text = .giNumDecimalesComp
    chkNotifPed(2).Value = IIf(.gbPedNotifMailCCActivado, vbChecked, vbUnchecked)
    txtMailSubject(10).Text = .gsPedNotifMailCC
    chkNotifPed(0).Value = IIf(.giPedNotifMailCCPeti = 1, vbChecked, vbUnchecked)
    chkNotifPed(1).Value = IIf(.giPedNotifMailCCComp = 1, vbChecked, vbUnchecked)
    txtNumDecRecep.Text = .giNumDecimalesOfe
    UpDownDec.Value = .giNumDecimalesOfe
    txtPlantilla(33).Text = .gsPETSUBDOT
    txtPlantilla(34).Text = .gsPETSUBCARTADOT
    txtPlantilla(35).Text = .gsPETSUBMAILDOT
    txtPlantilla(36).Text = .gsPETSUBWEBDOT
    txtMailSubject(8).Text = .gsPETSUBMAILSUBJECT
    SSTabHabSub.TabVisible(1) = Not (gParametrosGenerales.giINSTWEB = SinWeb Or Not gParametrosGenerales.gbSubasta)
    chkSelPositiva.Value = IIf(.gbSeleccionPositiva, vbChecked, vbUnchecked)
    optPresPlanif.Value = (.gbPresupuestoPlanificado = True)
    optPresAdj.Value = Not (.gbPresupuestoPlanificado = True)
    If .gbTipoActa = True Then
        optReuActa(1).Value = True
    Else
        optReuActa(0).Value = True
    End If
    ctrlConfGenAvisos.PlantillaDespub(0) = .gsOFENotifAvisoDespubHTML
    ctrlConfGenAvisos.PlantillaDespub(1) = .gsOFENotifAvisoDespubTXT
    ctrlConfGenAvisos.AsuntoAvisoDespub = .gsOFENotifAvisoDespubSubject
    If m_bMostrarSolicitud Then
        If .gbSolicProcAdjDir = True Then
            optGenerarProc(0).Value = True
        Else
            optGenerarProc(1).Value = True
        End If
    End If
    If m_bMostrarSolicitud Then
        If Not IsNull(.gvImporteSolicProcAdjDir) Then
            chkAdjDirImp.Value = vbChecked
            txtAdjDirImp.Text = .gvImporteSolicProcAdjDir
        Else
            chkAdjDirImp.Value = vbUnchecked
            txtAdjDirImp.Text = ""
        End If
    End If
    If m_bMostrarSolicitud Then
        sdbcPeriodoSolic.MoveFirst
        For i = 0 To sdbcPeriodoSolic.Rows - 1
            bm = sdbcPeriodoSolic.GetBookmark(i)
            If .giValidezSolicOfe = sdbcPeriodoSolic.Columns(1).Value Then
                sdbcPeriodoSolic.Text = sdbcPeriodoSolic.Columns(0).Value
                Exit For
            End If
            sdbcPeriodoSolic.MoveNext
        Next i
    End If
End With
Exit Sub
ERROR:
    If err.Number = 76 Or err.Number = 68 Then
        Resume Next
    End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Set oFos = Nothing
    Set m_oIdiomas = Nothing
    Set m_oIdiomasPortal = Nothing
    Set m_oMonedas = Nothing
    Set m_oPaises = Nothing
    Set m_oDestinos = Nothing
    Set m_oPagos = Nothing
    Set oLiteralesAModificar = Nothing
    Set m_dcSubTextosFin = Nothing
    Set m_oAtribs = Nothing
    Set g_oAtribsAnyadir = Nothing
    Set g_ofrmATRIBMod = Nothing
    Me.Visible = False
End Sub
Private Sub chkPlantAdj_Click(Index As Integer)
    Select Case Index
        Case 0:
            If chkPlantAdj(0).Value = vbChecked And chkPlantAdj(1).Value = vbChecked Then chkPlantAdj(1).Value = vbUnchecked
        Case 1:
            If chkPlantAdj(0).Value = vbChecked And chkPlantAdj(1).Value = vbChecked Then chkPlantAdj(0).Value = vbUnchecked
    End Select
End Sub
Private Sub optActaCrystal_Click()
    OptActaWord.Value = False
    Label10(2).Visible = False
    Label10(3).Visible = False
    optReuActa(0).Visible = True
    txtPlantilla(19).Visible = True
    cmdPlantilla(19).Visible = True
    sdbcPlantilla(2).Visible = False
    optReuActa(1).Visible = True
    txtPlantilla(39).Visible = True
    cmdPlantilla(40).Visible = True
    sdbcPlantilla(3).Visible = False
    Label10(4).Visible = False
    sdbcPlantilla(4).Visible = False
End Sub
Private Sub OptActaWord_Click()
    optActaCrystal.Value = False
    Label10(2).Visible = True
    Label10(3).Visible = True
    optReuActa(0).Visible = False
    txtPlantilla(19).Visible = False
    cmdPlantilla(19).Visible = False
    sdbcPlantilla(2).Visible = True
    optReuActa(1).Visible = False
    txtPlantilla(39).Visible = False
    cmdPlantilla(40).Visible = False
    sdbcPlantilla(3).Visible = True
    Label10(4).Visible = True
    sdbcPlantilla(4).Visible = True
End Sub
Private Sub optGenerarProc_Click(Index As Integer)
    If optGenerarProc(0).Value = True Then
        picSolicImporte.Enabled = True
    Else
        picSolicImporte.Enabled = False
        chkAdjDirImp.Value = vbUnchecked
        txtAdjDirImp.Text = ""
    End If
End Sub

Private Sub sdbcAntigOfer_Validate(Cancel As Boolean)
    Dim bm As Variant
    Dim bExiste As Boolean
    Dim i As Integer
    Dim iFila As Integer
    iFila = sdbcAntigOfer.Row
    If Me.picBuzonOfe.Enabled Then
        'Estamos en modo consulta
        bExiste = False
        sdbcAntigOfer.MoveFirst
        For i = 0 To sdbcAntigOfer.Rows - 1
            bm = sdbcAntigOfer.GetBookmark(i)
            If sdbcAntigOfer.Text = sdbcAntigOfer.Columns(0).Value Then
                bExiste = True
                sdbcAntigOfer.Bookmark = bm
                Exit For
            End If
            sdbcAntigOfer.MoveNext
        Next i
        If Not bExiste Then
            sdbcAntigOfer.Text = ""
        Else
            sdbcAntigOfer.Row = iFila
            sdbcAntigOfer.Columns(0).Value = sdbcAntigOfer.Text
        End If
    End If
End Sub

Private Sub sdbcDestDen_PositionList(ByVal Text As String)
    PositionList sdbcDestDen, Text
End Sub

Private Sub sdbcIdiomaConv_CloseUp()
    If picReuPlantillas.Enabled = False Then
        'Estamos en modo consulta
        If sdbcIdiomaConv.Value <> "" Then
            With gParametrosInstalacion
                Select Case UCase(sdbcIdiomaConv.Columns("ID").Value)
                Case "SPA"
                    txtPlantilla(17).Text = .gsConvocatoriaMailHtmlSPA
                    txtPlantilla(43).Text = .gsConvocatoriaMailTextSPA
                    txtConvMailSubject.Text = .gsConvocatoriaMailSubjectSPA
                Case "ENG"
                    txtPlantilla(17).Text = .gsConvocatoriaMailHtmlENG
                    txtPlantilla(43).Text = .gsConvocatoriaMailTextENG
                    txtConvMailSubject.Text = .gsConvocatoriaMailSubjectENG
                Case "GER"
                    txtPlantilla(17).Text = .gsConvocatoriaMailHtmlGER
                    txtPlantilla(43).Text = .gsConvocatoriaMailTextGER
                    txtConvMailSubject.Text = .gsConvocatoriaMailSubjectGER
                Case "FRA"
                    txtPlantilla(17).Text = .gsConvocatoriaMailHtmlFRA
                    txtPlantilla(43).Text = .gsConvocatoriaMailTextFRA
                    txtConvMailSubject.Text = .gsConvocatoriaMailSubjectFRA
                End Select
            End With
        End If
    Else
        txtPlantilla(17).Text = oLiteralesAModificar.Item(sdbcIdiomaConv.Columns("ID").Value & CStr(10008)).Den
        txtPlantilla(43).Text = oLiteralesAModificar.Item(sdbcIdiomaConv.Columns("ID").Value & CStr(10009)).Den
        txtConvMailSubject.Text = oLiteralesAModificar.Item(sdbcIdiomaConv.Columns("ID").Value & CStr(10010)).Den
    End If
End Sub

Private Sub sdbcIdiomaPortal_PositionList(ByVal Text As String)
PositionList sdbcDestDen, Text
End Sub

Private Sub sdbcIdiomas_PositionList(ByVal Text As String)
PositionList sdbcIdiomas, Text
End Sub

Private Sub sdbcMonDen_PositionList(ByVal Text As String)
PositionList sdbcMonDen, Text
End Sub

Private Sub sdbcPagoCod_Change()
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Value = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
End Sub

Private Sub sdbcPagoCod_Click()
    If Not sdbcPagoCod.DroppedDown Then
        sdbcPagoCod.Value = ""
        sdbcPagoDen.Value = ""
    End If
End Sub

Private Sub sdbcPagoCod_CloseUp()
    If sdbcPagoCod.Value = "..." Or sdbcPagoCod.Value = "" Then
        sdbcPagoCod.Value = ""
        Exit Sub
    End If
    m_bPagoRespetarCombo = True
    sdbcPagoDen.Value = sdbcPagoCod.Columns(1).Value
    sdbcPagoCod.Value = sdbcPagoCod.Columns(0).Value
    m_bPagoRespetarCombo = False
    m_bPagoCargarComboDesde = False
End Sub

Private Sub sdbcPagoCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    Screen.MousePointer = vbHourglass
    Set m_oPagos = Nothing
    Set m_oPagos = oFSGSRaiz.Generar_CPagos
    sdbcPagoCod.RemoveAll
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbcPagoCod.Value), , , False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
    End If
    Codigos = m_oPagos.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    If Not m_oPagos.EOF Then
        sdbcPagoCod.AddItem "..."
    End If
    sdbcPagoCod.SelStart = 0
    sdbcPagoCod.SelLength = Len(sdbcPagoCod.Value)
    sdbcPagoCod.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPagoCod_InitColumnProps()
    sdbcPagoCod.DataFieldList = "Column 0"
    sdbcPagoCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcPagoCod_PositionList(ByVal Text As String)
PositionList sdbcPagoCod, Text
End Sub


Private Sub sdbcPagoCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    If sdbcPagoCod.Value = "" Then Exit Sub
    Set m_oPagos = oFSGSRaiz.Generar_CPagos
    Screen.MousePointer = vbHourglass
    m_oPagos.CargarTodosLosPagos sdbcPagoCod.Value, , True, , False
    Screen.MousePointer = vbNormal
    bExiste = Not (m_oPagos.Count = 0)
    If Not bExiste Then
        sdbcPagoCod.Value = ""
    Else
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Value = m_oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcPagoCod.Columns(0).Value = sdbcPagoCod.Value
        sdbcPagoCod.Columns(1).Value = sdbcPagoDen.Value
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = False
    End If
    Set m_oPagos = Nothing
End Sub

Private Sub sdbcPagoDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcPagoCod.DroppedDown = False
        sdbcPagoCod.Text = ""
        sdbcPagoCod.RemoveAll
        sdbcPagoCod.DroppedDown = False
        sdbcPagoCod.Text = ""
        sdbcPagoCod.RemoveAll
    End If
End Sub

Private Sub sdbcPaiCod_Change()
If Not m_bPaiRespetarCombo Then
    m_bPaiRespetarCombo = True
    sdbcPaiDen.Text = ""
    m_bPaiRespetarCombo = False
    m_bPaiCargarComboDesde = True
    Set m_oPaisSeleccionado = Nothing
End If
If Not m_bNoActuProvi Then
    sdbcProviCod = ""
    sdbcProviDen = ""
End If
End Sub

Private Sub sdbcPaiCod_CloseUp()
    If sdbcPaiCod.Value = "..." Or sdbcPaiCod.Value = "" Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    m_bPaiRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    m_bPaiRespetarCombo = False
    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiCod.Columns(0).Text)
    m_bPaiCargarComboDesde = False
End Sub

Private Sub sdbcPaiCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    sdbcPaiCod.RemoveAll
    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcPaiCod.Text), , , False
    Else
        m_oPaises.CargarTodosLosPaises , , , , , False
    End If
    Codigos = m_oPaises.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    If m_bPaiCargarComboDesde And Not m_oPaises.EOF Then sdbcPaiCod.AddItem "..."
    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPaiCod_InitColumnProps()
    sdbcPaiCod.DataFieldList = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcPaiCod_PositionList(ByVal Text As String)
PositionList sdbcPaiCod, Text
End Sub
Private Sub sdbcPaiCod_Validate(Cancel As Boolean)
    Dim oPaises As CPaises
    Dim bExiste As Boolean
    Set oPaises = oFSGSRaiz.Generar_CPaises
    If sdbcPaiCod.Text = "" Then Exit Sub
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    oPaises.CargarTodosLosPaises sdbcPaiCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    bExiste = Not (oPaises.Count = 0)
    If Not bExiste Then
        sdbcPaiCod.Text = ""
    Else
        'edu incidencia 6578. Solucionar diferencias de c�digo entre mayusculas y minusculas.
        'Sustituir el valor tecleado por el valor del C�digo en la BDD
        m_bNoActuProvi = True
        sdbcPaiCod.Value = oPaises.Item(1).Cod
        m_bPaiRespetarCombo = True
        sdbcPaiDen.Text = oPaises.Item(1).Den
        m_bNoActuProvi = False
        sdbcPaiCod.Columns(0).Value = sdbcPaiCod.Text
        sdbcPaiCod.Columns(1).Value = sdbcPaiDen.Text
        
        m_bPaiRespetarCombo = False
        Set m_oPaisSeleccionado = oPaises.Item(1)
        m_bPaiCargarComboDesde = False
    End If
    Set oPaises = Nothing
End Sub
Private Sub sdbcPaiDen_Change()
    If Not m_bPaiRespetarCombo Then
        m_bPaiRespetarCombo = True
        sdbcPaiCod.Text = ""
        m_bPaiRespetarCombo = False
        m_bPaiCargarComboDesde = True
        Set m_oPaisSeleccionado = Nothing
    End If
    If Not m_bNoActuProvi Then
        sdbcProviCod = ""
        sdbcProviDen = ""
    End If
End Sub
Private Sub sdbcPaiDen_CloseUp()
    If sdbcPaiDen.Value = "..." Or sdbcPaiDen.Value = "" Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    m_bPaiRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    m_bPaiRespetarCombo = False
    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiDen.Columns(1).Text)
    m_bPaiCargarComboDesde = False
End Sub

Private Sub sdbcPaiDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    Screen.MousePointer = vbHourglass
    sdbcPaiDen.RemoveAll
    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPaiDen.Text), True, False
    Else
        m_oPaises.CargarTodosLosPaises , , , True, , False
    End If
    Codigos = m_oPaises.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    If m_bPaiCargarComboDesde And Not m_oPaises.EOF Then sdbcPaiDen.AddItem "..."
    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiCod.Refresh
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcPaiDen_InitColumnProps()
    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcPaiDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcPaiCod.DroppedDown = False
        sdbcPaiCod.Text = ""
        sdbcPaiCod.RemoveAll
        sdbcPaiDen.DroppedDown = False
        sdbcPaiDen.Text = ""
        sdbcPaiDen.RemoveAll
        Set m_oPaisSeleccionado = Nothing
    End If
End Sub
Private Sub sdbcMonCod_Change()
    If Not m_bMonRespetarCombo Then
        m_bMonRespetarCombo = True
        sdbcMonDen.Text = ""
        m_bMonRespetarCombo = False
        m_bMonCargarComboDesde = True
    End If
End Sub

Private Sub sdbcMonCod_CloseUp()
    If sdbcMonCod.Value = "..." Or sdbcMonCod.Value = "" Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    m_bMonRespetarCombo = True
    sdbcMonDen.Text = sdbcMonCod.Columns(1).Text
    sdbcMonCod.Text = sdbcMonCod.Columns(0).Text
    m_bMonRespetarCombo = False
    m_bMonCargarComboDesde = False
End Sub

Private Sub sdbcMonCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    Set m_oMonedas = Nothing
    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
    sdbcMonCod.RemoveAll
    If m_bMonCargarComboDesde Then
        m_oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcMonCod.Text), , , True
    Else
        m_oMonedas.CargarTodasLasMonedas , , , , , False, True
    End If
    Codigos = m_oMonedas.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcMonCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    If m_bMonCargarComboDesde And Not m_oMonedas.EOF Then sdbcMonCod.AddItem "..."
    sdbcMonCod.SelStart = 0
    sdbcMonCod.SelLength = Len(sdbcMonCod.Text)
    sdbcMonCod.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMonCod_InitColumnProps()
    sdbcMonCod.DataFieldList = "Column 0"
    sdbcMonCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcMonCod_PositionList(ByVal Text As String)
PositionList sdbcMonCod, Text
End Sub
Private Sub sdbcMonCod_Validate(Cancel As Boolean)
    Dim oMonedas As CMonedas
    Dim bExiste As Boolean
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    If sdbcMonCod.Text = "" Then Exit Sub
    ''' Solo continuamos si existe el Moneda
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedas sdbcMonCod.Text, , True, , False, , True
    Screen.MousePointer = vbNormal
    bExiste = Not (oMonedas.Count = 0)
    If Not bExiste Then
        sdbcMonCod.Text = ""
    Else
        'edu incidencia 6578. Solucionar diferencias de c�digo entre mayusculas y minusculas.
        'Sustituir el valor tecleado por el valor del C�digo en la BDD
        sdbcMonCod.Value = oMonedas.Item(1).Cod
        m_bMonRespetarCombo = True
        sdbcMonDen.Text = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcMonCod.Columns(0).Value = sdbcMonCod.Text
        sdbcMonCod.Columns(1).Value = sdbcMonDen.Text
        m_bMonRespetarCombo = False
        m_bMonCargarComboDesde = False
    End If
    Set oMonedas = Nothing
End Sub
Private Sub sdbcMonden_Change()
    If Not m_bMonRespetarCombo Then
        m_bMonRespetarCombo = True
        sdbcMonCod.Text = ""
        m_bMonRespetarCombo = False
        m_bMonCargarComboDesde = True
    End If
End Sub
Private Sub sdbcMonDen_CloseUp()
    If sdbcMonDen.Value = "..." Or sdbcMonDen.Value = "" Then
        sdbcMonDen.Text = ""
        Exit Sub
    End If
    m_bMonRespetarCombo = True
    sdbcMonCod.Text = sdbcMonDen.Columns(1).Text
    sdbcMonDen.Text = sdbcMonDen.Columns(0).Text
    m_bMonRespetarCombo = False
    m_bMonCargarComboDesde = False
End Sub

Private Sub sdbcMonDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    Set m_oMonedas = Nothing
    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
    sdbcMonDen.RemoveAll
    If m_bMonCargarComboDesde Then
        m_oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcMonDen.Text), True, True
    Else
        m_oMonedas.CargarTodasLasMonedas , , , True, , False, True
    End If
    Codigos = m_oMonedas.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcMonDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    If m_bMonCargarComboDesde And Not m_oMonedas.EOF Then sdbcMonDen.AddItem "..."
    sdbcMonDen.SelStart = 0
    sdbcMonDen.SelLength = Len(sdbcMonDen.Text)
    sdbcMonCod.Refresh
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcMonDen_InitColumnProps()
    sdbcMonDen.DataFieldList = "Column 0"
    sdbcMonDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcMonDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcMonCod.DroppedDown = False
        sdbcMonCod.Text = ""
        sdbcMonCod.RemoveAll
        sdbcMonDen.DroppedDown = False
        sdbcMonDen.Text = ""
        sdbcMonDen.RemoveAll
    End If
End Sub

Private Sub sdbcPaiDen_PositionList(ByVal Text As String)
PositionList sdbcPaiDen, Text
End Sub

Private Sub sdbcProviCod_Change()
    If Not m_bProviRespetarCombo Then
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = ""
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = True
    End If
End Sub
Private Sub sdbcProviCod_CloseUp()
    If sdbcProviCod.Value = "..." Or sdbcProviCod.Value = "" Then
        sdbcProviCod.Text = ""
        Exit Sub
    End If
    m_bProviRespetarCombo = True
    sdbcProviDen.Text = sdbcProviCod.Columns(1).Text
    sdbcProviCod.Text = sdbcProviCod.Columns(0).Text
    m_bProviRespetarCombo = False
    m_bProviCargarComboDesde = False
End Sub

Private Sub sdbcProviCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    sdbcProviCod.RemoveAll
    If Not m_oPaisSeleccionado Is Nothing Then
        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProviCod.Text), , , False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , , False
        End If
        Codigos = m_oPaisSeleccionado.DevolverLosCodigosDeProvincias
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.EOF Then sdbcProviCod.AddItem "..."
    End If
    sdbcProviCod.SelStart = 0
    sdbcProviCod.SelLength = Len(sdbcProviCod.Text)
    sdbcProviCod.Refresh
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProviCod_InitColumnProps()
    sdbcProviCod.DataFieldList = "Column 0"
    sdbcProviCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcProviCod_PositionList(ByVal Text As String)
PositionList sdbcProviCod, Text
End Sub
Private Sub sdbcProviCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    If sdbcProviCod.Text = "" Then Exit Sub
    ''' Solo continuamos si existe la provincia
    If Not m_oPaisSeleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        m_oPaisSeleccionado.CargarTodasLasProvincias sdbcProviCod.Text, , True, , False
        Screen.MousePointer = vbNormal
        bExiste = Not (m_oPaisSeleccionado.Provincias.Count = 0)
    Else
        bExiste = False
    End If
    If Not bExiste Then
        sdbcProviCod.Text = ""
    Else
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = m_oPaisSeleccionado.Provincias.Item(1).Den
        sdbcProviCod.Columns(0).Value = sdbcProviCod.Text
        sdbcProviCod.Columns(1).Value = sdbcProviDen.Text
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = False
    End If
End Sub
Private Sub sdbcProviDen_Change()
    If Not m_bProviRespetarCombo Then
        m_bProviRespetarCombo = True
        sdbcProviCod.Text = ""
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = True
    End If
End Sub
Private Sub sdbcProviDen_CloseUp()
    If sdbcProviDen.Value = "..." Or sdbcProviDen.Value = "" Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If
    m_bProviRespetarCombo = True
    sdbcProviCod.Text = sdbcProviDen.Columns(1).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(0).Text
    m_bProviRespetarCombo = False
    m_bProviCargarComboDesde = False
End Sub
Private Sub sdbcProviDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Screen.MousePointer = vbHourglass
    sdbcProviDen.RemoveAll
    If Not m_oPaisSeleccionado Is Nothing Then
        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProviDen.Text), True, False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If
        Codigos = m_oPaisSeleccionado.DevolverLosCodigosDeProvincias
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.EOF Then sdbcProviDen.AddItem "..."
    End If
    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
    sdbcProviCod.Refresh
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProviDen_InitColumnProps()
    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcProviDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcProviCod.DroppedDown = False
        sdbcProviCod.Text = ""
        sdbcProviCod.RemoveAll
        sdbcProviDen.DroppedDown = False
        sdbcProviDen.Text = ""
        sdbcProviDen.RemoveAll
    End If
End Sub
Private Sub sdbcDestCod_Change()
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
End Sub
Private Sub sdbcDestCod_CloseUp()
    If sdbcDestCod.Value = "..." Or sdbcDestCod.Value = "" Then
        sdbcDestCod.Text = ""
        Exit Sub
    End If
    m_bDestRespetarCombo = True
    sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
    sdbcDestCod.Text = sdbcDestCod.Columns(0).Text
    m_bDestRespetarCombo = False
    m_bDestCargarComboDesde = False
End Sub

Private Sub sdbcDestCod_DropDown()
Dim ADORs As Ador.Recordset
Dim bApeCons As Boolean
Dim bRDest As Boolean
    Set m_oDestinos = Nothing
    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
    Screen.MousePointer = vbHourglass
    sdbcDestCod.RemoveAll
    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing) Then
            bApeCons = True
        Else
            bApeCons = False
        End If
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDest)) Is Nothing) Then bRDest = True
    Else
        bRDest = False
        bApeCons = True
    End If
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            If bApeCons Then
                If bRDest Then
                    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(sdbcDestCod.Text, , , bRDest)
                Else
                    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(sdbcDestCod.Text, , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
                End If
            Else
                Set ADORs = m_oDestinos.DevolverTodosLosDestinos(sdbcDestCod.Text, , , True, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
            End If
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(sdbcDestCod.Text, , , bRDest)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            If bApeCons Then
                If bRDest Then
                    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest)
                Else
                    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
                End If
            Else
                Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , True, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
            End If
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest)
        End If
    End If
    If ADORs Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    While Not ADORs.EOF
        sdbcDestCod.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    ADORs.Close
    Set ADORs = Nothing
    If m_bDestCargarComboDesde And Not m_oDestinos.EOF Then sdbcDestCod.AddItem "..."
    sdbcDestCod.SelStart = 0
    sdbcDestCod.SelLength = Len(sdbcDestCod.Text)
    sdbcDestCod.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcDestCod_InitColumnProps()
    sdbcDestCod.DataFieldList = "Column 0"
    sdbcDestCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcDestCod_PositionList(ByVal Text As String)
PositionList sdbcDestCod, Text
End Sub
Private Sub sdbcDestCod_Validate(Cancel As Boolean)
    Dim m_oDestinos As CDestinos
    Dim bExiste As Boolean
    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
    If sdbcDestCod.Text = "" Then Exit Sub
    ''' Solo continuamos si existe el Destino
    Screen.MousePointer = vbHourglass
    m_oDestinos.CargarTodosLosDestinos sdbcDestCod.Text, , True, , , , , , , False, True, False
    Screen.MousePointer = vbNormal
    bExiste = Not (m_oDestinos.Count = 0)
    If Not bExiste Then
        sdbcDestCod.Text = ""
    Else
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = m_oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcDestCod.Columns(0).Value = sdbcDestCod.Text
        sdbcDestCod.Columns(1).Value = sdbcDestDen.Text
        
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = False
    End If
    Set m_oDestinos = Nothing
End Sub
Private Sub sdbcDestDen_Change()
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestCod.Text = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
End Sub
Private Sub sdbcDestDen_CloseUp()
    If sdbcDestDen.Value = "..." Or sdbcDestDen.Value = "" Then
        sdbcDestDen.Text = ""
        Exit Sub
    End If
    m_bDestRespetarCombo = True
    sdbcDestCod.Text = sdbcDestDen.Columns(1).Text
    sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
    m_bDestRespetarCombo = False
    
    m_bDestCargarComboDesde = False
End Sub

Private Sub sdbcDestDen_DropDown()
Dim ADORs As Ador.Recordset
Dim bApeCons As Boolean
Dim bRDest As Boolean
    Screen.MousePointer = vbHourglass
    Set m_oDestinos = Nothing
    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
    sdbcDestDen.RemoveAll
    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing) Then
            bApeCons = True
        Else
            bApeCons = False
        End If
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDest)) Is Nothing) Then bRDest = True
    Else
        bRDest = False
        bApeCons = True
    
    End If
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            If bApeCons Then
                If bRDest Then
                    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, sdbcDestDen.Text, , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
                Else
                    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, sdbcDestDen.Text, , bRDest)
                End If
            Else
                Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, sdbcDestDen.Text, , True, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
            End If
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, sdbcDestDen.Text, , bRDest)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            If bApeCons Then
                If bRDest Then
                    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
                Else
                    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest)
                End If
            Else
                Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , True, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
            End If
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , bRDest)
        End If
    End If
    
    If ADORs Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    While Not ADORs.EOF
        sdbcDestDen.AddItem ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    ADORs.Close
    Set ADORs = Nothing
    If m_bDestCargarComboDesde And Not m_oDestinos.EOF Then sdbcDestDen.AddItem "..."
    sdbcDestDen.SelStart = 0
    sdbcDestDen.SelLength = Len(sdbcDestDen.Text)
    sdbcDestCod.Refresh
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcDestDen_InitColumnProps()
    sdbcDestDen.DataFieldList = "Column 0"
    sdbcDestDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcDestDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcDestCod.DroppedDown = False
        sdbcDestCod.Text = ""
        sdbcDestCod.RemoveAll
        sdbcDestDen.DroppedDown = False
        sdbcDestDen.Text = ""
        sdbcDestDen.RemoveAll
    End If
End Sub
Private Sub sdbcProviDen_PositionList(ByVal Text As String)
PositionList sdbcProviDen, Text
End Sub
Private Sub sdbcTZ_InitColumnProps()
    With sdbcTZ
        .DataFieldList = "Column 0, Column 1"
        .DataFieldToDisplay = "Column 0"
    End With
End Sub
Private Sub sdbcTZ_PositionList(ByVal Text As String)
PositionList sdbcTZ, Text
End Sub

Private Function sdbddValorValidateList(Text As String) As Boolean
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer

    If Text = "" Then
        sdbddValorValidateList = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If sdbgCamposPedido.Columns("TIPO_INTRODUCCION").Value = "1" Then
            iIdAtrib = val(sdbgCamposPedido.Columns("ATRIBID").Value)

            Set oatrib = oFSGSRaiz.Generar_CAtributo
            Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)
            
            If oatrib Is Nothing Then Exit Function
            
            oatrib.CargarListaDeValores
            
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgCamposPedido.Columns(sdbgCamposPedido.col).Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If sdbgCamposPedido.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
                If UCase(msTrue) = UCase(sdbgCamposPedido.Columns(sdbgCamposPedido.col).Text) Then
                    bExiste = True
                End If
                If UCase(msFalse) = UCase(sdbgCamposPedido.Columns(sdbgCamposPedido.col).Text) Then
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            sdbddValorValidateList = False
            Exit Function
        End If
        sdbddValorValidateList = True
    End If
End Function

Private Sub sdbgDatosGrid_Change()
Dim i As Integer
Select Case sdbgDatosGrid.Columns.Item("Obligatoria").Value
    Case "1"
        Select Case sdbgDatosGrid.col
            Case 0
                sdbgDatosGrid.Columns.Item("Usar").Value = True
            Case 2, 3, 4
                sdbgDatosGrid.Columns.Item(sdbgDatosGrid.col).Value = True
                For i = 2 To 4
                    If (i <> sdbgDatosGrid.col) Then sdbgDatosGrid.Columns.Item(i).Value = False
                Next i
        End Select
    Case Else
        Select Case sdbgDatosGrid.col
            Case 0
                If sdbgDatosGrid.Columns.Item(0).Value = True Then
                    sdbgDatosGrid.Columns.Item("Item").Value = True
                    sdbgDatosGrid.Columns(0).CellStyleSet "Yellow", sdbgDatosGrid.Row
                    sdbgDatosGrid.Columns(1).CellStyleSet "Yellow", sdbgDatosGrid.Row
                Else
                    'Si la DIST o los PRESUP son obligatorios no se pueden descheckear
                    Select Case sdbgDatosGrid.Columns("Fila").Value
                        Case 5 'DIST
                                sdbgDatosGrid.Columns(0).Value = "-1"
                                Exit Sub
                        Case 6 'PRES1
                                If gParametrosGenerales.gbOBLPP Then
                                    sdbgDatosGrid.Columns(0).Value = "-1"
                                    Exit Sub
                                End If
                        Case 7 'PRES2
                                If gParametrosGenerales.gbOBLPC Then
                                    sdbgDatosGrid.Columns(0).Value = "-1"
                                    Exit Sub
                                End If
                        Case 8 'PRES3
                                If gParametrosGenerales.gbOBLPres3 Then
                                    sdbgDatosGrid.Columns(0).Value = "-1"
                                    Exit Sub
                                End If
                        Case 9 'PRES4
                                If gParametrosGenerales.gbOBLPres4 Then
                                    sdbgDatosGrid.Columns(0).Value = "-1"
                                    Exit Sub
                                End If
                    End Select
                    sdbgDatosGrid.Columns.Item("Proceso").Value = False
                    sdbgDatosGrid.Columns.Item("Grupo").Value = False
                    sdbgDatosGrid.Columns.Item("Item").Value = False
                    sdbgDatosGrid.Columns(0).CellStyleSet "Normal", sdbgDatosGrid.Row
                    sdbgDatosGrid.Columns(1).CellStyleSet "Normal", sdbgDatosGrid.Row
                End If
            Case 2, 3, 4
                If sdbgDatosGrid.Columns.Item(0).Value = True Then
                        If ((sdbgDatosGrid.Columns.Item(2).Value = False) And (sdbgDatosGrid.Columns.Item(3).Value = False) And (sdbgDatosGrid.Columns.Item(4).Value = False)) Then
                            'Si la DIST o los PRESUP son obligatorios no se pueden descheckear
                            Select Case sdbgDatosGrid.Columns("Fila").Value
                                    Case 5 'DIST
                                            sdbgDatosGrid.Columns(0).Value = "-1"
                                            sdbgDatosGrid.Columns(sdbgDatosGrid.col).Value = "-1"
                                            Exit Sub
                                    Case 6 'PRES1
                                            If gParametrosGenerales.gbOBLPP Then
                                                sdbgDatosGrid.Columns(0).Value = "-1"
                                                sdbgDatosGrid.Columns(sdbgDatosGrid.col).Value = "-1"
                                                Exit Sub
                                            End If
                                    Case 7 'PRES2
                                            If gParametrosGenerales.gbOBLPC Then
                                                sdbgDatosGrid.Columns(0).Value = "-1"
                                                sdbgDatosGrid.Columns(sdbgDatosGrid.col).Value = "-1"
                                                Exit Sub
                                            End If
                                    Case 8 'PRES3
                                            If gParametrosGenerales.gbOBLPres3 Then
                                                sdbgDatosGrid.Columns(0).Value = "-1"
                                                sdbgDatosGrid.Columns(sdbgDatosGrid.col).Value = "-1"
                                                Exit Sub
                                            End If
                                    Case 9 'PRES4
                                            If gParametrosGenerales.gbOBLPres4 Then
                                                sdbgDatosGrid.Columns(0).Value = "-1"
                                                sdbgDatosGrid.Columns(sdbgDatosGrid.col).Value = "-1"
                                                Exit Sub
                                            End If
                            End Select
                            sdbgDatosGrid.Columns.Item("Usar").Value = False
                            sdbgDatosGrid.Columns(0).CellStyleSet "Normal", sdbgDatosGrid.Row
                            sdbgDatosGrid.Columns(1).CellStyleSet "Normal", sdbgDatosGrid.Row
                        Else
                            If (sdbgDatosGrid.Columns.Item(1).Value <> m_sDatos(4)) Then
                                sdbgDatosGrid.Columns.Item(sdbgDatosGrid.col).Value = True
                                For i = 2 To 4
                                    If (i <> sdbgDatosGrid.col) Then sdbgDatosGrid.Columns.Item(i).Value = False
                                Next i
                            End If
                        End If
                    Else
                        sdbgDatosGrid.Columns.Item("Usar").Value = True
                        sdbgDatosGrid.Columns(0).CellStyleSet "Yellow", sdbgDatosGrid.Row
                        sdbgDatosGrid.Columns(1).CellStyleSet "Yellow", sdbgDatosGrid.Row
                End If
            End Select
    End Select
End Sub

Private Sub sdbgDatosGrid_InitColumnProps()
Dim sEspProce As String
Dim sEspGrupo As String
Dim sEspItem As String
Dim sEspUsada As String
Dim bModif As Boolean
    bModif = False
    If gParametrosInstalacion.gCPProcesoAdminPub = False Then
        Select Case gParametrosInstalacion.gCPDestino
            Case 0
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(0) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(0) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(0) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(0) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
        End Select
        Select Case gParametrosInstalacion.gCPPago
            Case 0
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(1) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(1) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(1) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(1) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
        End Select
    End If
    Select Case gParametrosInstalacion.gCPFechasSuministro
        Case 0
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(2) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 1
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(2) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 2
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(2) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 3
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(2) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
    End Select
    If gParametrosInstalacion.gCPProcesoAdminPub = False Then
        Select Case gParametrosInstalacion.gCPProveActual
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(3) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
        End Select
    End If
    If gParametrosInstalacion.gCPEspProce = True Or gParametrosInstalacion.gCPEspGrupo Or gParametrosInstalacion.gCPEspItem = True Then
        sEspUsada = "-1"
    Else
        sEspUsada = "0"
    End If
    If gParametrosInstalacion.gCPEspProce = True Then
        sEspProce = "-1"
    Else
        sEspProce = "0"
    End If
    If gParametrosInstalacion.gCPEspGrupo = True Then
        sEspGrupo = "-1"
    Else
        sEspGrupo = "0"
    End If
    If gParametrosInstalacion.gCPEspItem = True Then
        sEspItem = "-1"
    Else
        sEspItem = "0"
    End If
    sdbgDatosGrid.AddItem sEspUsada & Chr(m_lSeparador) & m_sDatos(4) & Chr(m_lSeparador) & sEspProce & Chr(m_lSeparador) & sEspGrupo & Chr(m_lSeparador) & sEspItem & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "4"
    ' Si la DISTRIB o alguno de los PRESUP son obligatorios pero no est�n definidos
    ' seg�n los par�metros de la instalaci�n leemos el valor de los par�metros generales y guardamos los valores
    ' y guardamos el valor
    If gParametrosInstalacion.gCPDistUON = NoDefinido Then
        gParametrosInstalacion.gCPDistUON = gParametrosGenerales.gCPDistUON
        bModif = True
    End If
    Select Case gParametrosInstalacion.gCPDistUON
        Case 0
            sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 1
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(5) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 2
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 3
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
    End Select
    If gParametrosGenerales.gbUsarPres1 = True Then
        If gParametrosGenerales.gbOBLPP Then
            If gParametrosInstalacion.gCPPresAnu1 = NoDefinido Then
                gParametrosInstalacion.gCPPresAnu1 = gParametrosGenerales.gCPPresAnu1
                bModif = True
            End If
        End If
        Select Case gParametrosInstalacion.gCPPresAnu1
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(6) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
        End Select
    End If
    If gParametrosGenerales.gbUsarPres2 = True Then
        If gParametrosGenerales.gbOBLPC Then
            If gParametrosInstalacion.gCPPresAnu2 = NoDefinido Then
                gParametrosInstalacion.gCPPresAnu2 = gParametrosGenerales.gCPPresAnu2
                bModif = True
            End If
        End If
        Select Case gParametrosInstalacion.gCPPresAnu2
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(7) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
        End Select
    End If
    If gParametrosGenerales.gbUsarPres3 = True Then
        If gParametrosGenerales.gbOBLPres3 Then
            If gParametrosInstalacion.gCPPres1 = NoDefinido Then
                gParametrosInstalacion.gCPPres1 = gParametrosGenerales.gCPPres1
                bModif = True
            End If
        End If
        Select Case gParametrosInstalacion.gCPPres1
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(8) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
        End Select
    End If
    If gParametrosGenerales.gbUsarPres4 = True Then
        If gParametrosGenerales.gbOBLPres4 Then
            If gParametrosInstalacion.gCPPres2 = NoDefinido Then
                gParametrosInstalacion.gCPPres2 = gParametrosGenerales.gCPPres2
                bModif = True
            End If
        End If
        Select Case gParametrosInstalacion.gCPPres2
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(9) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
        End Select
    End If
    If m_bMostrarSolicitud = True Then
        If gParametrosInstalacion.gCPProcesoAdminPub = False Then
            Select Case gParametrosInstalacion.gCPSolicitud
                Case 0
                    sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
                Case 1
                    sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(10) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
                Case 2
                    sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
                Case 3
                    sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
            End Select
        End If
    End If
    sdbgDatosGrid.Columns.Item(1).Locked = True
    If bModif Then
        g_GuardarParametrosIns = True
    End If
End Sub

Private Sub sdbgDatosGrid_RowLoaded(ByVal Bookmark As Variant)
Select Case CStr(Bookmark)
    Case "0", "1", "2"
        sdbgDatosGrid.Columns(0).CellStyleSet "Yellow", sdbgDatosGrid.Row
        sdbgDatosGrid.Columns(1).CellStyleSet "Yellow", sdbgDatosGrid.Row
    Case Else
        If sdbgDatosGrid.Columns.Item("Usar").Value = True Then
            sdbgDatosGrid.Columns(0).CellStyleSet "Yellow", sdbgDatosGrid.Row
            sdbgDatosGrid.Columns(1).CellStyleSet "Yellow", sdbgDatosGrid.Row
        End If
    End Select
End Sub

Private Sub CargarAmbitoDatos()
Dim sEspProce As String
Dim sEspGrupo As String
Dim sEspItem As String
Dim sEspUsada As String
Dim bModif As Boolean
    bModif = False
    sdbgDatosGrid.RemoveAll
    Select Case gParametrosInstalacion.gCPDestino
        Case 0
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(0) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
        Case 1
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(0) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
        Case 2
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(0) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
        Case 3
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(0) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
    End Select
    Select Case gParametrosInstalacion.gCPPago
        Case 0
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(1) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
        Case 1
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(1) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
        Case 2
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(1) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
        Case 3
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(1) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
    End Select
    Select Case gParametrosInstalacion.gCPFechasSuministro
        Case 0
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(2) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 1
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(2) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 2
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(2) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 3
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(2) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
    End Select
    Select Case gParametrosInstalacion.gCPProveActual
        Case 0
            sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
        Case 1
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(3) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
        Case 2
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
        Case 3
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
    End Select
    If gParametrosInstalacion.gCPEspProce = True Or gParametrosInstalacion.gCPEspGrupo Or gParametrosInstalacion.gCPEspItem = True Then
        sEspUsada = "-1"
    Else
        sEspUsada = "0"
    End If
    If gParametrosInstalacion.gCPEspProce = True Then
        sEspProce = "-1"
    Else
        sEspProce = "0"
    End If
    If gParametrosInstalacion.gCPEspGrupo = True Then
        sEspGrupo = "-1"
    Else
        sEspGrupo = "0"
    End If
    If gParametrosInstalacion.gCPEspItem = True Then
        sEspItem = "-1"
    Else
        sEspItem = "0"
    End If
    sdbgDatosGrid.AddItem sEspUsada & Chr(m_lSeparador) & m_sDatos(4) & Chr(m_lSeparador) & sEspProce & Chr(m_lSeparador) & sEspGrupo & Chr(m_lSeparador) & sEspItem & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "4"
    ' Si la DISTRIB o alguno de los PRESUP son obligatorios pero no est�n definidos
    ' seg�n los par�metros de la instalaci�n leemos el valor de los par�metros generales y guardamos los valores
    ' y guardamos el valor
    If gParametrosInstalacion.gCPDistUON = NoDefinido Then
        gParametrosInstalacion.gCPDistUON = gParametrosGenerales.gCPDistUON
        bModif = True
    End If
    Select Case gParametrosInstalacion.gCPDistUON
        Case 0
            sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 1
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(5) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 2
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 3
            sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
    End Select
    If gParametrosGenerales.gbUsarPres1 = True Then
        If gParametrosGenerales.gbOBLPP Then
            If gParametrosInstalacion.gCPPresAnu1 = NoDefinido Then
                gParametrosInstalacion.gCPPresAnu1 = gParametrosGenerales.gCPPresAnu1
                bModif = True
            End If
        End If
        Select Case gParametrosInstalacion.gCPPresAnu1
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(6) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
        End Select
    End If
    If gParametrosGenerales.gbUsarPres2 = True Then
        If gParametrosGenerales.gbOBLPC Then
            If gParametrosInstalacion.gCPPresAnu2 = NoDefinido Then
                gParametrosInstalacion.gCPPresAnu2 = gParametrosGenerales.gCPPresAnu2
                bModif = True
            End If
        End If
        Select Case gParametrosInstalacion.gCPPresAnu2
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(7) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
        End Select
    End If
    If gParametrosGenerales.gbUsarPres3 = True Then
        If gParametrosGenerales.gbOBLPres3 Then
            If gParametrosInstalacion.gCPPres1 = NoDefinido Then
                gParametrosInstalacion.gCPPres1 = gParametrosGenerales.gCPPres1
                bModif = True
            End If
        End If
        Select Case gParametrosInstalacion.gCPPres1
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(8) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
        End Select
    End If
    If gParametrosGenerales.gbUsarPres4 = True Then
        If gParametrosGenerales.gbOBLPres4 Then
            If gParametrosInstalacion.gCPPres2 = NoDefinido Then
                gParametrosInstalacion.gCPPres2 = gParametrosGenerales.gCPPres2
                bModif = True
            End If
        End If
        Select Case gParametrosInstalacion.gCPPres2
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(9) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
        End Select
    End If
    If m_bMostrarSolicitud = True Then
        Select Case gParametrosInstalacion.gCPSolicitud
            Case 0
                sdbgDatosGrid.AddItem "0" & Chr(m_lSeparador) & m_sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
            Case 1
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(10) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
            Case 2
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
            Case 3
                sdbgDatosGrid.AddItem "-1" & Chr(m_lSeparador) & m_sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
        End Select
    End If
End Sub

Private Sub cmdDrive_Click(Index As Integer)
Dim sBuffer As String
Dim sTit As String
Dim sInicio As String
On Error GoTo ERROR
If Index = 0 Then
    sTit = fraListados.caption & ":"
    sInicio = txtRuta.Text
Else
    sTit = fraSolicitudes.caption & ":"
    sInicio = txtRutaSolicitudes.Text
End If
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, sTit, sInicio)
If sBuffer <> "" Then
    bRespetarControl = True
    If Index = 0 Then
        DirRPT.Path = sBuffer
        txtRuta.Text = sBuffer
    Else
        DirSolicitudes.Path = sBuffer
        txtRutaSolicitudes.Text = sBuffer
    End If
    bChangeTxt = False
    bRespetarControl = False
End If
Exit Sub
ERROR:
    TratarErrorBrowse err.Number, err.Description
    If Index = 0 Then
        txtRuta.Text = ""
    Else
        txtRutaSolicitudes.Text = ""
    End If
    bChangeTxt = False
    bRespetarControl = False
End Sub

Private Sub DirRPT_Change()
    If Not bRespetarControl Then
        bRespetarControl = True
        txtRuta.Text = DirRPT.Path
        bRespetarControl = False
    End If
End Sub
Private Sub txtConvMailSubject_Validate(Cancel As Boolean)
    oLiteralesAModificar.Item(sdbcIdiomaConv.Columns("ID").Value & CStr(10010)).Den = txtConvMailSubject.Text
End Sub
Private Sub txtNumDecRecep_Validate(Cancel As Boolean)
    If Not IsInteger(txtNumDecRecep.Text) Then
        txtNumDecRecep = ""
        Exit Sub
    End If
    If val(txtNumDecRecep.Text) < 0 Or val(txtNumDecRecep.Text) > 20 Then
        txtNumDecRecep = ""
        Exit Sub
    End If
    UpDownDec.Value = Trim(txtNumDecRecep.Text)
End Sub
Private Sub txtPlantilla_Validate(Index As Integer, Cancel As Boolean)
    If Index = 17 Then
        oLiteralesAModificar.Item(sdbcIdiomaConv.Columns("ID").Value & CStr(10008)).Den = txtPlantilla(Index).Text
    ElseIf Index = 43 Then
        oLiteralesAModificar.Item(sdbcIdiomaConv.Columns("ID").Value & CStr(10009)).Den = txtPlantilla(Index).Text
    End If
End Sub

Private Sub txtRuta_Change()
    If Not bRespetarControl Then
        bChangeTxt = True
    End If
End Sub

Private Sub txtRuta_Validate(Cancel As Boolean)
On Error GoTo ERROR
    If txtRuta.Text <> DirRPT.Path Then
        bRespetarControl = True
        DirRPT.Path = txtRuta.Text
        bChangeTxt = False
        bRespetarControl = False
    End If
    Exit Sub
ERROR:
    TratarErrorBrowse err.Number, err.Description
    txtRuta.Text = ""
    bChangeTxt = False
    bRespetarControl = False
End Sub

Private Sub DirSolicitudes_Change()
    If Not bRespetarControl Then
        bRespetarControl = True
        txtRutaSolicitudes.Text = DirSolicitudes.Path
        bRespetarControl = False
    End If
End Sub

Private Sub txtRutaSolicitudes_Change()
    If Not bRespetarControl Then bChangeTxt = True
End Sub

Private Sub txtRutaSolicitudes_Validate(Cancel As Boolean)
On Error GoTo ERROR
    If txtRutaSolicitudes.Text <> DirSolicitudes.Path Then
        bRespetarControl = True
        DirSolicitudes.Path = txtRutaSolicitudes.Text
        bChangeTxt = False
        bRespetarControl = False
    End If
    Exit Sub
ERROR:
    TratarErrorBrowse err.Number, err.Description
    txtRutaSolicitudes.Text = ""
    bChangeTxt = False
    bRespetarControl = False
End Sub

Private Sub UpDownDec_Change()
    'Asocia el valor del control Updown al texbox:
    txtNumDecRecep.Text = UpDownDec.Value
End Sub

Private Sub sdbcPagoDen_Change()
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoCod.Value = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
End Sub

Private Sub sdbcPagoDen_Click()
    If Not sdbcPagoDen.DroppedDown Then
        sdbcPagoDen.Value = ""
        sdbcPagoCod.Value = ""
    End If
End Sub

Private Sub sdbcPagoDen_CloseUp()
    If sdbcPagoDen.Value = "..." Or sdbcPagoDen.Value = "" Then
        sdbcPagoDen.Value = ""
        Exit Sub
    End If
    m_bPagoRespetarCombo = True
    sdbcPagoCod.Value = sdbcPagoDen.Columns(1).Value
    sdbcPagoDen.Value = sdbcPagoDen.Columns(0).Value
    m_bPagoRespetarCombo = False
    m_bPagoCargarComboDesde = False
End Sub

Private Sub sdbcPagoDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    Screen.MousePointer = vbHourglass
    Set m_oPagos = Nothing
    Set m_oPagos = oFSGSRaiz.Generar_CPagos
    sdbcPagoDen.RemoveAll
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , CStr(sdbcPagoDen.Value), True, False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
    End If
    Codigos = m_oPagos.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    If Not m_oPagos.EOF Then sdbcPagoDen.AddItem "..."
    sdbcPagoDen.SelStart = 0
    sdbcPagoDen.SelLength = Len(sdbcPagoDen.Value)
    sdbcPagoDen.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPagoDen_InitColumnProps()
    sdbcPagoDen.DataFieldList = "Column 0"
    sdbcPagoDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcPagoDen_PositionList(ByVal Text As String)
PositionList sdbcPagoDen, Text
End Sub

Private Sub sdbcPlantilla_InitColumnProps(Index As Integer)
    sdbcPlantilla(Index).DataFieldList = "Column 0"
    sdbcPlantilla(Index).DataFieldToDisplay = "Column 1"
End Sub

Public Sub CargarDatosPlantillas()
    sdbcPlantilla(1).RemoveAll
    sdbcPlantilla(1).AddItem FormatoPlantillaGS.DOC & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(1)
    sdbcPlantilla(1).AddItem FormatoPlantillaGS.DOCX & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(2)
    sdbcPlantilla(1).AddItem FormatoPlantillaGS.PDF & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(3)
    sdbcPlantilla(1).AddItem FormatoPlantillaGS.ODT & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(4)
    sdbcPlantilla(1).AddItem FormatoPlantillaGS.EPUB & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(5)
    sdbcPlantilla(2).RemoveAll
    sdbcPlantilla(2).AddItem VistaPlantillaGS.ActaProveedor & Chr(m_lSeparador) & m_sVistasPlantilla(1)
    sdbcPlantilla(2).AddItem VistaPlantillaGS.ActaItem & Chr(m_lSeparador) & m_sVistasPlantilla(2)
    sdbcPlantilla(4).RemoveAll
    sdbcPlantilla(4).AddItem FormatoPlantillaGS.DOC & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(1)
    sdbcPlantilla(4).AddItem FormatoPlantillaGS.DOCX & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(2)
    sdbcPlantilla(4).AddItem FormatoPlantillaGS.PDF & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(3)
    sdbcPlantilla(4).AddItem FormatoPlantillaGS.ODT & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(4)
    sdbcPlantilla(4).AddItem FormatoPlantillaGS.EPUB & Chr(m_lSeparador) & m_sTextosFormatoPlantilla(5)
    Dim Ador As Ador.Recordset
    Set Ador = oGestorParametros.DevolverPlantillasAgendaUsu(oUsuarioSummit.Cod, oUsuarioSummit.idioma, gUON1Usuario, gUON2Usuario, gUON3Usuario)
    sdbcPlantilla(0).RemoveAll
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            sdbcPlantilla(0).AddItem Ador("ID").Value & Chr(m_lSeparador) & Ador("DEN").Value & ": " & Ador("NOM").Value
            If CBool(Ador("DEFECTO").Value) Then
                sdbcPlantilla(0).Value = Ador("ID").Value
                sdbcPlantilla(1).Value = Ador("FORMATO").Value
            End If
            Ador.MoveNext
        Wend
    End If
    Dim VistaDefecto As Integer
    VistaDefecto = 0
    Set Ador = oGestorParametros.DevolverPlantillasActaUsu(oUsuarioSummit.Cod, oUsuarioSummit.idioma, gUON1Usuario, gUON2Usuario, gUON3Usuario)
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            If CBool(Ador("DEFECTO").Value) Then VistaDefecto = Ador("VISTA").Value
            Ador.MoveNext
        Wend
        If Not VistaDefecto = 0 Then sdbcPlantilla(2).Value = VistaDefecto
    End If
End Sub

Private Sub sdbcPlantilla_Change(Index As Integer)
    If Index = 2 Then
        sdbcPlantilla(3).RemoveAll
        Dim Ador As Ador.Recordset
        Set Ador = oGestorParametros.DevolverPlantillasActaUsu(oUsuarioSummit.Cod, oUsuarioSummit.idioma, gUON1Usuario, gUON2Usuario, gUON3Usuario)
        If Not Ador Is Nothing Then
            While Not Ador.EOF
                If Ador("VISTA").Value = CInt(sdbcPlantilla(2).Value) Then
                    sdbcPlantilla(3).AddItem Ador("ID").Value & Chr(m_lSeparador) & Ador("DEN").Value & ": " & Ador("NOM").Value
                    If CBool(Ador("DEFECTO").Value) Then
                        sdbcPlantilla(3).Value = Ador("ID").Value
                        sdbcPlantilla(4).Value = Ador("FORMATO").Value
                    End If
                End If
                Ador.MoveNext
            Wend
        End If
    End If
End Sub

Private Sub sdbcPlantilla_CloseUp(Index As Integer)
    If Index = 2 Then
        If sdbcPlantilla(2).Value = "" Then Exit Sub
        sdbcPlantilla(3).RemoveAll
        Dim Ador As Ador.Recordset
        Set Ador = oGestorParametros.DevolverPlantillasActaUsu(oUsuarioSummit.Cod, oUsuarioSummit.idioma, gUON1Usuario, gUON2Usuario, gUON3Usuario)
        While Not Ador.EOF
            If Ador("VISTA").Value = CInt(sdbcPlantilla(2).Value) Then
                sdbcPlantilla(3).AddItem Ador("ID").Value & Chr(m_lSeparador) & Ador("DEN").Value & ": " & Ador("NOM").Value
                If CBool(Ador("DEFECTO").Value) Then
                    gParametrosInstalacion.giIDACTA = Ador("ID").Value
                    sdbcPlantilla(3).Value = Ador("ID").Value
                    sdbcPlantilla(4).Value = Ador("FORMATO").Value
                End If
            End If
            Ador.MoveNext
        Wend
        If Not CInt(sdbcPlantilla(2).Value) = gParametrosInstalacion.giIDACTA Then sdbcPlantilla(3).Value = 0
    End If
End Sub

Private Sub sdbgCamposPedido_BeforeUpdate(Cancel As Integer)
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
End Sub

Private Sub sdbgCamposPedido_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)

Cancel = False

If (sdbgCamposPedido.Columns("LISTA_EXTERNA").Value = "1") Then Exit Sub

If sdbgCamposPedido.Columns("VAL").Text <> "" Then
    Select Case UCase(sdbgCamposPedido.Columns("TIPO_DATOS").Text)
        Case TiposDeAtributos.TipoNumerico 'Numero
            If (Not IsNumeric(sdbgCamposPedido.Columns("VAL").Text)) Then
                oMensajes.AtributoValorNoValido ("TIPO2")
                Cancel = True
                GoTo Salir
            ElseIf sdbgCamposPedido.Columns("TIPO_INTRODUCCION").Value = "1" Then
                If Not sdbddValorValidateList(sdbgCamposPedido.Columns("VAL").Text) Then
                    oMensajes.NoValido sdbgCamposPedido.Columns(sdbgCamposPedido.col).caption
                    Cancel = True
                    GoTo Salir
                End If
            Else
                If sdbgCamposPedido.Columns("MIN").Text <> "" And sdbgCamposPedido.Columns("MAX").Text <> "" Then
                    If StrToDbl0(sdbgCamposPedido.Columns("MIN").Text) > StrToDbl0(sdbgCamposPedido.Columns("VAL").Text) Or StrToDbl0(sdbgCamposPedido.Columns("MAX").Text) < StrToDbl0(sdbgCamposPedido.Columns("VAL").Text) Then
                        oMensajes.ValorEntreMaximoYMinimo sdbgCamposPedido.Columns("MIN").Text, sdbgCamposPedido.Columns("MAX").Text
                        Cancel = True
                        GoTo Salir
                    End If
                End If
            End If
        Case TiposDeAtributos.TipoFecha 'Fecha
            If (Not IsDate(sdbgCamposPedido.Columns("VAL").Text) And sdbgCamposPedido.Columns("VAL").Text <> "") Then
                oMensajes.AtributoValorNoValido ("TIPO3")
                Cancel = True
                GoTo Salir
            ElseIf sdbgCamposPedido.Columns("TIPO_INTRODUCCION").Value = "1" Then
                If Not sdbddValorValidateList(sdbgCamposPedido.Columns("VAL").Text) Then
                    oMensajes.NoValido sdbgCamposPedido.Columns(sdbgCamposPedido.col).caption
                    Cancel = True
                    GoTo Salir
                End If
            Else
                If sdbgCamposPedido.Columns("MIN").Text <> "" And sdbgCamposPedido.Columns("MAX").Text <> "" Then
                    If CDate(sdbgCamposPedido.Columns("MIN").Text) > CDate(sdbgCamposPedido.Columns("VAL").Text) Or CDate(sdbgCamposPedido.Columns("MAX").Text) < CDate(sdbgCamposPedido.Columns("VAL").Text) Then
                     oMensajes.ValorEntreMaximoYMinimo sdbgCamposPedido.Columns("MIN").Text, sdbgCamposPedido.Columns("MAX").Text
                        Cancel = True
                        GoTo Salir
                    End If
                End If
            End If
        Case TiposDeAtributos.TipoBoolean
            If Not sdbddValorValidateList(sdbgCamposPedido.Columns("VAL").Text) Then
                oMensajes.NoValido sdbgCamposPedido.Columns(sdbgCamposPedido.col).caption
                Cancel = True
                GoTo Salir
            End If
        Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio:
            If sdbgCamposPedido.Columns("TIPO_INTRODUCCION").Value = "1" Then
                If Not sdbddValorValidateList(sdbgCamposPedido.Columns("VAL").Text) Then
                    oMensajes.NoValido sdbgCamposPedido.Columns(sdbgCamposPedido.col).caption
                    Cancel = True
                    GoTo Salir
                End If
            End If
    End Select
Else
    oMensajes.NoValido sdbgCamposPedido.Columns(sdbgCamposPedido.col).caption
    Cancel = True
End If
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
Exit Sub
Salir:
    If Me.Visible Then sdbgCamposPedido.SetFocus
End Sub

Private Sub sdbgCamposPedido_RowLoaded(ByVal Bookmark As Variant)
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
    If sdbgCamposPedido.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoArchivo Then
        sdbgCamposPedido.Columns("VAL").CellStyleSet "Gris"
    End If
End Sub

Private Sub sdbgCamposPedido_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
    If Me.sdbgCamposPedido.Columns("ESINTEGRA").Value = AtributosLineaCatalogo.SinOrigen Then
        Me.sdbgCamposPedido.Columns("VAL").Value = ""
        Cancel = True
    End If
End Sub

Private Sub sdbgCamposPedido_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
   If IsEmpty(sdbgCamposPedido.RowBookmark(sdbgCamposPedido.Row)) Then
        sdbgCamposPedido.Bookmark = sdbgCamposPedido.RowBookmark(sdbgCamposPedido.Row - 1)
    Else
        sdbgCamposPedido.Bookmark = sdbgCamposPedido.RowBookmark(sdbgCamposPedido.Row)
    End If
End Sub

Private Sub sdbgCamposPedido_BtnClick()
    If sdbgCamposPedido.col < 0 Then Exit Sub

    If sdbgCamposPedido.Columns(sdbgCamposPedido.col).Name = "VAL" Then

        frmATRIBDescr.g_bEdicion = True
        frmATRIBDescr.caption = sdbgCamposPedido.Columns("COD").Value & ": " & sdbgCamposPedido.Columns("DEN").Value
        frmATRIBDescr.txtDescr.Text = sdbgCamposPedido.Columns(sdbgCamposPedido.col).Value
        frmATRIBDescr.g_sOrigen = "frmCONFINST"
        frmATRIBDescr.Show 1

        If sdbgCamposPedido.DataChanged Then
            sdbgCamposPedido.Update
        End If
    End If
End Sub

Private Sub sdbgCamposPedido_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        If sdbgCamposPedido.DataChanged = True Then
            sdbgCamposPedido.CancelUpdate
            sdbgCamposPedido.DataChanged = False
        End If
    End If
End Sub

Private Sub sdbgCamposPedido_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    If Not picAtribDefecto.Enabled Then Exit Sub

    sdbgCamposPedido.Columns("VAL").Locked = False
    If sdbgCamposPedido.Columns("TIPO_INTRODUCCION").Value = 0 Then 'Libre
        If sdbgCamposPedido.Columns("TIPO_DATOS").Value = TipoBoolean Then
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            sdbgCamposPedido.Columns("VAL").DropDownHwnd = sdbddValor.hWnd
            sdbddValor.Enabled = True
        Else
            sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
            sdbddValor.Enabled = False
            If sdbgCamposPedido.Columns("TIPO_DATOS").Value = 1 Then
                sdbgCamposPedido.Columns("VAL").Style = ssStyleEditButton
            ElseIf sdbgCamposPedido.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoArchivo Then
                sdbgCamposPedido.Columns("VAL").Locked = True
            End If
        End If
    Else 'Lista
        sdbddValor.RemoveAll
        sdbddValor.AddItem ""
        If Not sdbgCamposPedido.Columns("LISTA_EXTERNA").Value = "1" Then
            sdbgCamposPedido.Columns("VAL").DropDownHwnd = sdbddValor.hWnd
            sdbddValor.Enabled = True
            sdbddValor.DroppedDown = True
            sdbddValor_DropDown
            sdbddValor.DroppedDown = True
        Else
            sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
            sdbddValor.Enabled = False
            
        End If
    End If
End Sub

Private Sub sdbgCamposPedido_Scroll(Cancel As Integer)
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
End Sub

Private Sub sdbgCamposPedido_AfterColUpdate(ByVal ColIndex As Integer)
    ''' Modificamos en la base de datos
    ''Comprobacion de atributos de oferta obligatorios
    With sdbgCamposPedido
        Dim tsError As TipoErrorSummit
        
        If .Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
            tsError = m_oAtribs.ActualizarAtributoPedidoDefecto(oUsuarioSummit.Cod, .Columns("ATRIBID").Value, IIf(.Columns("VAL").Value = msTrue, 1, IIf(.Columns("VAL").Value = msFalse, 0, "")), .Columns("TIPO_DATOS").Value)
        Else
            tsError = m_oAtribs.ActualizarAtributoPedidoDefecto(oUsuarioSummit.Cod, .Columns("ATRIBID").Value, .Columns("VAL").Value, .Columns("TIPO_DATOS").Value)
        End If
        
        Set m_oAtribs = Nothing
        Set m_oAtribs = oFSGSRaiz.Generar_CAtributos
        Set m_oAtribs = m_oAtribs.DevolverAtribPedidoDefecto(oUsuarioSummit.Cod)
        
        'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
        If tsError.NumError <> TESnoerror Then
            TratarError tsError
            If Me.Visible Then .SetFocus
            .DataChanged = False
        End If
    End With
End Sub

Private Sub CargarAtributos()
    Dim oatrib As CAtributo
    Dim vValor As Variant
    
    Set m_oAtribs = m_oAtribs.DevolverAtribPedidoDefecto(oUsuarioSummit.Cod)
        
    If Not m_oAtribs Is Nothing Then
        If m_oAtribs.Count > 0 Then
            With sdbgCamposPedido
                For Each oatrib In m_oAtribs
                    vValor = oatrib.valor
                    If oatrib.Tipo = TiposDeAtributos.TipoBoolean Then
                        If oatrib.valor = 1 Then
                            vValor = msTrue
                        ElseIf oatrib.valor = 0 Then
                            vValor = msFalse
                        End If
                    End If
                    
                    If oatrib.ListaExterna Then
                        .AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & " " & m_sDenListaExterna & Chr(m_lSeparador) & vValor & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo _
                            & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Origen _
                            & Chr(m_lSeparador) & oatrib.ListaExterna
                    Else
                        .AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & vValor & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo _
                            & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Origen _
                            & Chr(m_lSeparador) & oatrib.ListaExterna
                    End If
                Next
            End With
        End If
    End If
End Sub

Private Sub cmdAtrAdd_Click(Index As Integer)
    Select Case Index
    Case 0 'add
        addAtributoPantalla
    Case 1 'elim
        elimAtributoPantalla
    End Select
End Sub

Private Sub addAtributoPantalla()
    Set g_oAtribsAnyadir = Nothing
    Set g_oAtribsAnyadir = oFSGSRaiz.Generar_CAtributos
    
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
    If Not g_ofrmATRIBMod Is Nothing Then
        Unload g_ofrmATRIBMod
        Set g_ofrmATRIBMod = Nothing
    End If
    
    Set g_ofrmATRIBMod = New frmAtribMod

    g_ofrmATRIBMod.g_sOrigen = "frmCONFINST"
    
    g_ofrmATRIBMod.g_bSoloSeleccion = True
    
    g_ofrmATRIBMod.g_sGmn1 = m_sDenListaExterna
    
    g_ofrmATRIBMod.Show vbModal
        
    If Not g_oAtribsAnyadir Is Nothing Then
        Dim tsError As TipoErrorSummit
        
        tsError = m_oAtribs.AnyadirAtributoPedidoDefecto(oUsuarioSummit.Cod, g_oAtribsAnyadir)
        'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
        If tsError.NumError <> TESnoerror Then
            TratarError tsError
            Exit Sub
        End If
    
        Set m_oAtribs = Nothing
        Set m_oAtribs = oFSGSRaiz.Generar_CAtributos
        Set m_oAtribs = m_oAtribs.DevolverAtribPedidoDefecto(oUsuarioSummit.Cod)
        
        Me.cmdAtrAdd(1).Enabled = True

    End If
End Sub

Private Sub elimAtributoPantalla()
    Dim sConfirmEliminarCampos As String
    
    Screen.MousePointer = vbHourglass

    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
    
    Dim i As Long
    Dim teserror As TipoErrorSummit
    'INDICA sdbgCamposPedido.SelBookmarks.Count LAS FILAS SELECCIONADAS EN LA GRID?????
    If sdbgCamposPedido.SelBookmarks.Count > 0 Then
        sConfirmEliminarCampos = ""
        Dim oAtribsAEliminar As CAtributos
        Set oAtribsAEliminar = oFSGSRaiz.Generar_CAtributos
        
        For i = 0 To sdbgCamposPedido.SelBookmarks.Count - 1
            With sdbgCamposPedido
                oAtribsAEliminar.Add .Columns("ATRIBID").CellValue(.SelBookmarks(i)), .Columns("COD").CellValue(.SelBookmarks(i)), .Columns("DEN").CellValue(.SelBookmarks(i)), .Columns("TIPO_DATOS").CellValue(.SelBookmarks(i)) _
                , oOrigen:=.Columns("ESINTEGRA").CellValue(.SelBookmarks(i))
                
                sConfirmEliminarCampos = sConfirmEliminarCampos & .Columns("COD").CellValue(.SelBookmarks(i)) & " - " & Replace(.Columns("DEN").CellValue(.SelBookmarks(i)), m_sDenListaExterna, "") & "#@\"
            End With
        Next
        
        sConfirmEliminarCampos = Left(sConfirmEliminarCampos, Len(sConfirmEliminarCampos) - 3)
        sConfirmEliminarCampos = Replace(sConfirmEliminarCampos, "#@\", vbCrLf)
    
        i = oMensajes.PreguntaEliminar(sConfirmEliminarCampos)
        If i = vbYes Then
            teserror = oAtribsAEliminar.LimpiarAtributoPedidoDefecto(oUsuarioSummit.Cod, oAtribsAEliminar)
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            LockWindowUpdate Me.hWnd
            
            sdbgCamposPedido.RemoveAll
            
            CargarAtributos
            
            LockWindowUpdate 0&
            
            If sdbgCamposPedido.Rows <= 0 Then
                Me.cmdAtrAdd(1).Enabled = False
            End If
        Else
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        Set oAtribsAEliminar = Nothing
        
        Set m_oAtribs = Nothing
        Set m_oAtribs = oFSGSRaiz.Generar_CAtributos
        Set m_oAtribs = m_oAtribs.DevolverAtribPedidoDefecto(oUsuarioSummit.Cod)
        
        sdbgCamposPedido.MoveFirst
        Screen.MousePointer = Normal
    Else
        Screen.MousePointer = vbNormal
        'PONER MENSAJE DE QUE NO HAY SELECCIONADO NING�N CAMPO DE PEDIDO
        MsgBox msCamposNoSeleccionados, vbInformation, Me.TabPedidos.TabCaption(5)
    End If
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant
    On Error Resume Next
    sdbddValor.MoveFirst
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbgCamposPedido.Columns("VAL").Value = Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1
    
    If sdbgCamposPedido.Rows = 0 Then Exit Sub
   
    If sdbgCamposPedido.Columns("VAL").Locked Then
        sdbddValor.DroppedDown = False
        Exit Sub
    End If
    sdbddValor.RemoveAll
    sdbddValor.DroppedDown = True
    
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    oAtributo.Id = sdbgCamposPedido.Columns("ATRIBID").Value
    
    If oAtributo.Id > 0 Then
        If sdbgCamposPedido.Columns("TIPO_INTRODUCCION").Value = "1" Then
            oAtributo.CargarDatosAtributo
            oAtributo.CargarListaDeValores
            For Each oLista In oAtributo.ListaPonderacion
                sdbddValor.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        Else
            If sdbgCamposPedido.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem "1" & Chr(m_lSeparador) & msTrue
                sdbddValor.AddItem "0" & Chr(m_lSeparador) & msFalse
            End If
        End If
    End If
    
    sdbddValor.Width = sdbgCamposPedido.Columns("VAL").Width
    sdbddValor.Columns("DESC").Width = sdbddValor.Width
End Sub
