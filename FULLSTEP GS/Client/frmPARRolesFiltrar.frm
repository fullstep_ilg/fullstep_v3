VERSION 5.00
Begin VB.Form frmPARRolesFiltrar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Relaciones proceso / personas involucradas (Filtro)"
   ClientHeight    =   2475
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5430
   Icon            =   "frmPARRolesFiltrar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2475
   ScaleWidth      =   5430
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   60
      TabIndex        =   6
      Top             =   1080
      Width           =   5235
      Begin VB.CheckBox chkIgualDen 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3150
         TabIndex        =   9
         Top             =   420
         Width           =   2025
      End
      Begin VB.TextBox txtDEN 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   210
         MaxLength       =   100
         TabIndex        =   8
         Top             =   360
         Width           =   2865
      End
      Begin VB.OptionButton optDEN 
         BackColor       =   &H00808000&
         Caption         =   "Por denominaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   120
         TabIndex        =   7
         Top             =   -60
         Width           =   2100
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   60
      TabIndex        =   2
      Top             =   60
      Width           =   5235
      Begin VB.CheckBox chkIgualCod 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   1140
         TabIndex        =   5
         Top             =   420
         Width           =   2100
      End
      Begin VB.TextBox txtCOD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   3
         TabIndex        =   4
         Top             =   360
         Width           =   795
      End
      Begin VB.OptionButton optCOD 
         BackColor       =   &H00808000&
         Caption         =   "Por c�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   0
         Value           =   -1  'True
         Width           =   2100
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2655
      TabIndex        =   1
      Top             =   2100
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1425
      TabIndex        =   0
      Top             =   2100
      Width           =   1005
   End
End
Attribute VB_Name = "frmPARRolesFiltrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmPARRolesFiltrar
''' *** Creacion: 28/12/1998 (Javier Arana)
''' *** Ultima revision: 14/01/1999 (Alfredo Magallon)

Option Explicit
Private Sub Form_Load()

    ''' * Objetivo: Situar el formulario respecto al
    ''' * Objetivo: principal de Roles
    
    On Error Resume Next
    
    Me.Left = frmPARRoles.Left + 500
    Me.Top = frmPARRoles.Top + 1000
    CargarRecursos
    txtCOD.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodROL
    
End Sub
Private Sub optCOD_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    If optCOD.Value = True Then
        optDEN.Value = False
    End If
        
    If Me.Visible Then txtCOD.SetFocus
   
End Sub
Private Sub chkIgualDen_Click()
    
    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    If Me.Visible Then txtDEN.SetFocus

End Sub
Private Sub chkIgualCod_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    If Me.Visible Then txtCOD.SetFocus
    
End Sub
Private Sub optDEN_Click()

    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    If optDEN.Value = True Then
        optCOD.Value = False
    End If
    
    If Me.Visible Then txtDEN.SetFocus
    
End Sub

Private Sub cmdAceptar_Click()

    ''' * Objetivo: Aplicar el filtro y descargar
    ''' * Objetivo: el formulario
    
    Screen.MousePointer = vbHourglass
    
    Set frmPARRoles.oRoles = Nothing
    Set frmPARRoles.oRoles = oFSGSRaiz.generar_CRoles
       
    If optCOD.Value = True And txtCOD <> "" Then
        
        If Not chkIgualCod.Value = vbChecked Then
        
            frmPARRoles.oRoles.CargarTodosLosRoles Trim(txtCOD), , , , , True, frmPARRoles.sdbcIdi.Columns(1).Value
'            frmPARRoles.Caption = "Roles (Consulta Codigo = " & txtCOD & "*)"
            frmPARRoles.ponerCaption Trim(txtCOD), 1, True
            
        Else
        
            frmPARRoles.oRoles.CargarTodosLosRoles Trim(txtCOD), , True, , , True, frmPARRoles.sdbcIdi.Columns(1).Value
'            frmPARRoles.Caption = "Roles (Consulta Codigo = " & txtCOD & ")"
            frmPARRoles.ponerCaption Trim(txtCOD), 1, False
            
        End If
        
    Else
    
        If optDEN.Value = True And txtDEN <> "" Then
        
            If Not chkIgualDen.Value = vbChecked Then
                
                frmPARRoles.oRoles.CargarTodosLosRoles , txtDEN, , , , True, frmPARRoles.sdbcIdi.Columns(1).Value
'                frmPARRoles.Caption = "Roles (Consulta Denominacion = " & txtDEN & "*)"
                frmPARRoles.ponerCaption Trim(txtDEN), 2, True
                
            Else
                
                frmPARRoles.oRoles.CargarTodosLosRoles , txtDEN, True, , , True, frmPARRoles.sdbcIdi.Columns(1).Value
'                frmPARRoles.Caption = "Roles (Consulta Denominacion = " & txtDEN & ")"
                frmPARRoles.ponerCaption Trim(txtDEN), 2, False
                
            End If
        
        Else
            frmPARRoles.caption = ""
            frmPARRoles.oRoles.CargarTodosLosRoles , , , , , True, frmPARRoles.sdbcIdi.Columns(1).Value
            
        End If
        
    End If
        
        
    frmPARRoles.sdbgRoles.ReBind
    MDI.MostrarFormulario frmPARRoles, True
    frmPARRoles.sdbgRoles.MoveFirst
    
    Screen.MousePointer = vbNormal
    
    Unload Me

    frmPARRoles.SetFocus
    
End Sub
Private Sub cmdCancelar_Click()

    ''' * Objetivo: Cerrar el formulario
    
    MDI.MostrarFormulario frmPARRoles, True
    
    Unload Me
    
    frmPARRoles.SetFocus
    
    
    
End Sub
Private Sub Form_Activate()

    ''' * Objetivo: Iniciar el formulario
    
    If Me.Visible Then txtCOD.SetFocus
    
End Sub
Private Sub txtCOD_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por codigo

    optCOD.Value = True
    optDEN.Value = False
    
End Sub
Private Sub txtDEN_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por denominacion

    optCOD.Value = False
    optDEN.Value = True
    
End Sub




Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PARROLES_FILTRAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    caption = Ador(0).Value
    Ador.MoveNext
    optCOD.caption = Ador(0).Value
    Ador.MoveNext
    chkIgualCod.caption = Ador(0).Value
    chkIgualDen.caption = Ador(0).Value
    Ador.MoveNext
    optDEN.caption = Ador(0).Value
    Ador.MoveNext
    cmdAceptar.caption = Ador(0).Value
    Ador.MoveNext
    cmdCancelar.caption = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub


