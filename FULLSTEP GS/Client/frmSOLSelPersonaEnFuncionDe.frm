VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSOLSelPersonaEnFuncionDe 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   6945
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7995
   Icon            =   "frmSOLSelPersonaEnFuncionDe.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6945
   ScaleWidth      =   7995
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdEliminarAprobadorPorPeticionario 
      Height          =   312
      Left            =   120
      Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":0562
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   11280
      Width           =   432
   End
   Begin VB.CommandButton cmdAnyadirAprobadorPorPeticionario 
      Height          =   312
      Left            =   120
      Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":05F4
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   10920
      Width           =   432
   End
   Begin VB.TextBox tbBuscarCodigo 
      Height          =   285
      Left            =   1965
      TabIndex        =   8
      Top             =   1560
      Width           =   1980
   End
   Begin VB.CommandButton cmdBuscarCodigo 
      Height          =   295
      Left            =   4050
      Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":0676
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   1560
      Width           =   335
   End
   Begin VB.OptionButton optPersonaEnFuncionCentroCoste 
      BackColor       =   &H00808000&
      Caption         =   "DPersona en funci�n del centro de coste"
      ForeColor       =   &H8000000E&
      Height          =   375
      Left            =   240
      TabIndex        =   6
      Top             =   1200
      Width           =   4575
   End
   Begin VB.OptionButton optPersonaEnFuncionPeticionario 
      BackColor       =   &H00808000&
      Caption         =   "dPersona en funci�n del peticionario"
      ForeColor       =   &H8000000E&
      Height          =   375
      Left            =   240
      TabIndex        =   5
      Top             =   840
      Width           =   4455
   End
   Begin VB.OptionButton optPersonaEnFuncionEmpresa 
      BackColor       =   &H00808000&
      Caption         =   "DPersona en funci�n de la empresa"
      ForeColor       =   &H8000000E&
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   480
      Width           =   4455
   End
   Begin VB.OptionButton optPersonaDirecta 
      BackColor       =   &H00808000&
      Caption         =   "DPersona directa"
      ForeColor       =   &H8000000E&
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   4455
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2760
      TabIndex        =   2
      Top             =   6480
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   3960
      TabIndex        =   1
      Top             =   6480
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwEstrOrg 
      Height          =   4455
      Left            =   240
      TabIndex        =   0
      Top             =   1920
      Width           =   7560
      _ExtentX        =   13335
      _ExtentY        =   7858
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPerEnFuncionEmpresa 
      Height          =   4455
      Left            =   240
      TabIndex        =   10
      Top             =   1920
      Width           =   7605
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   9
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSOLSelPersonaEnFuncionDe.frx":06C7
      stylesets(1).Name=   "StringTachado"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(1).Picture=   "frmSOLSelPersonaEnFuncionDe.frx":06E3
      stylesets(2).Name=   "Bloqueado"
      stylesets(2).BackColor=   12632256
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSOLSelPersonaEnFuncionDe.frx":06FF
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "EMP_HIDDEN"
      Columns(0).Name =   "EMP_HIDDEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2805
      Columns(1).Caption=   "NIF"
      Columns(1).Name =   "NIF"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16776960
      Columns(2).Width=   3889
      Columns(2).Caption=   "EMP"
      Columns(2).Name =   "EMP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16776960
      Columns(3).Width=   5583
      Columns(3).Caption=   "PER_DEP_UON"
      Columns(3).Name =   "PER_DEP_UON"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "UON1"
      Columns(4).Name =   "UON1"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "UON2"
      Columns(5).Name =   "UON2"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "UON3"
      Columns(6).Name =   "UON3"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "DEP"
      Columns(7).Name =   "DEP"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "PER"
      Columns(8).Name =   "PER"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      _ExtentX        =   13414
      _ExtentY        =   7858
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":071B
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":0AE3
            Key             =   "Persona"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":0BED
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":0F41
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":1295
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":15E9
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":197D
            Key             =   "PerUsu"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":1A87
            Key             =   "PersonaBaja"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelPersonaEnFuncionDe.frx":1B12
            Key             =   "PerUsuBaja"
         EndProperty
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPerEnFuncionDePeticionario 
      Height          =   4455
      Left            =   600
      TabIndex        =   11
      Top             =   10920
      Width           =   7245
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   12
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSOLSelPersonaEnFuncionDe.frx":1BB8
      stylesets(1).Name=   "StringTachado"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(1).Picture=   "frmSOLSelPersonaEnFuncionDe.frx":1BD4
      stylesets(2).Name=   "Bloqueado"
      stylesets(2).BackColor=   12632256
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSOLSelPersonaEnFuncionDe.frx":1BF0
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   12
      Columns(0).Width=   5927
      Columns(0).Caption=   "PET_DEN"
      Columns(0).Name =   "PET_DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "PET_UON1"
      Columns(1).Name =   "PET_UON1"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "PET_UON2"
      Columns(2).Name =   "PET_UON2"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "PET_UON3"
      Columns(3).Name =   "PET_UON3"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "PET_DEP"
      Columns(4).Name =   "PET_DEP"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "PET_PER"
      Columns(5).Name =   "PET_PER"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   6033
      Columns(6).Caption=   "APROB_DEN"
      Columns(6).Name =   "APROB_DEN"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "APROB_UON1"
      Columns(7).Name =   "APROB_UON1"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "APROB_UON2"
      Columns(8).Name =   "APROB_UON2"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "APROB_UON3"
      Columns(9).Name =   "APROB_UON3"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "APROB_DEP"
      Columns(10).Name=   "APROB_DEP"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "APROB_PER"
      Columns(11).Name=   "APROB_PER"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      _ExtentX        =   12779
      _ExtentY        =   7858
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPersonaEnFuncionCentroDeCoste 
      Height          =   4455
      Left            =   240
      TabIndex        =   14
      Top             =   7200
      Width           =   7605
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   8
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSOLSelPersonaEnFuncionDe.frx":1C0C
      stylesets(1).Name=   "StringTachado"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(1).Picture=   "frmSOLSelPersonaEnFuncionDe.frx":1C28
      stylesets(2).Name=   "Bloqueado"
      stylesets(2).BackColor=   12632256
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSOLSelPersonaEnFuncionDe.frx":1C44
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "CENTRO_HIDDEN"
      Columns(0).Name =   "CENTRO_HIDDEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5715
      Columns(1).Caption=   "CENTRO_DEN"
      Columns(1).Name =   "CENTRO_DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16776960
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "UON1"
      Columns(2).Name =   "UON1"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "UON2"
      Columns(3).Name =   "UON2"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "UON3"
      Columns(4).Name =   "UON3"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "DEP"
      Columns(5).Name =   "DEP"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "PER"
      Columns(6).Name =   "PER"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   6138
      Columns(7).Caption=   "PER_DEP_UON_DEN"
      Columns(7).Name =   "PER_DEP_UON_DEN"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   13414
      _ExtentY        =   7858
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblBusqueda 
      BackColor       =   &H00808000&
      Caption         =   "B�squeda por c�digo :"
      ForeColor       =   &H8000000E&
      Height          =   285
      Left            =   240
      TabIndex        =   9
      Top             =   1590
      Width           =   1665
   End
End
Attribute VB_Name = "frmSOLSelPersonaEnFuncionDe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_iTipoAprobador As Integer
Public g_sOrigen As String
Public bRUO As Boolean
Public bRDep As Boolean
Public bAllowSelUON As Boolean
Public bAllowSelUON0 As Boolean 'Nos indicar� si se puede seleccionar el UON0 (toda la organizaci�n)
Public g_sPer As String
Public g_sUON0 As String
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String
Public g_sDEP As String
Public g_sEmpresas As String
Public g_oRolEnEdicion As CPMRol
Public g_lIdSolicitud As Long
Public g_TipoSolicitud As TipoSolicitud
' Variable de control de flujo
Public Accion As accionessummit

Private scod1 As String
Private scod2 As String
Private scod3 As String
Private scod4 As String
Private EstructuraOrganizacionCargada As Boolean
Private m_sMensajeNoSeleccionado As String
Private m_sMensajeSeleccioneAprobadorPeticionario As String
Private m_sEliminarRegistro As String


Private Sub Form_Load()
    PonerFieldSeparator Me
    EstructuraOrganizacionCargada = False
    CargarRecursos
    'Dependiendo del tipo de aprobador
    Select Case g_iTipoAprobador
        Case 0, TipoAprobadorSolicitudPedido.PersonaDirecta
            optPersonaDirecta.Value = True
        Case TipoAprobadorSolicitudPedido.PersonaEnFuncionCentroDeCoste
            Me.optPersonaEnFuncionCentroCoste.Value = True
        Case TipoAprobadorSolicitudPedido.PersonaEnFuncionEmpresa
            optPersonaEnFuncionEmpresa.Value = True
        Case TipoAprobadorSolicitudPedido.PersonaEnFuncionPeticionario
            optPersonaEnFuncionPeticionario.Value = True
    End Select
End Sub
''' <summary>
''' Carga los textos de la pantalla
''' </summary>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SEL_APROBADORES, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.optPersonaDirecta.caption = Ador(0).Value
        Ador.MoveNext
        Me.optPersonaEnFuncionEmpresa.caption = Ador(0).Value
        Ador.MoveNext
        Me.optPersonaEnFuncionPeticionario.caption = Ador(0).Value
        Ador.MoveNext
        Me.optPersonaEnFuncionCentroCoste.caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgPersonaEnFuncionCentroDeCoste.Columns("CENTRO_DEN").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgPersonaEnFuncionCentroDeCoste.Columns("PER_DEP_UON_DEN").caption = Ador(0).Value
        Me.sdbgPerEnFuncionDePeticionario.Columns("APROB_DEN").caption = Ador(0).Value
        Me.sdbgPerEnFuncionEmpresa.Columns("PER_DEP_UON").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgPerEnFuncionDePeticionario.Columns("PET_DEN").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgPerEnFuncionEmpresa.Columns("NIF").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgPerEnFuncionEmpresa.Columns("EMP").caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.MoveNext
        m_sMensajeNoSeleccionado = Ador(0).Value
        Ador.MoveNext
        m_sMensajeSeleccioneAprobadorPeticionario = Ador(0).Value
        Ador.MoveNext
        m_sEliminarRegistro = Ador(0).Value
        Ador.Close
    End If
    Set Ador = Nothing
End Sub

Private Sub cmdAnyadirAprobadorPorPeticionario_Click()
    sdbgPerEnFuncionDePeticionario.AddNew
End Sub

Private Sub cmdEliminarAprobadorPorPeticionario_Click()
    Dim I As Integer
    With sdbgPerEnFuncionDePeticionario
        If .SelBookmarks.Count > 0 Then
            I = oMensajes.PreguntaEliminar(m_sEliminarRegistro)
            If I = vbYes Then
                'ELIMINAR DEL GRID
                .DeleteSelected
                .Update
                .Refresh
            Else
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N REGISTRO
            MsgBox m_sMensajeNoSeleccionado, vbExclamation
            Exit Sub
        End If
    End With
End Sub

Private Sub cmdBuscarCodigo_Click()

Dim intPos As Integer
Dim blnEncontrado As Boolean

blnEncontrado = False
intPos = 1

Do While Not blnEncontrado And intPos <= tvwEstrOrg.Nodes.Count

    
    If UCase(Mid(tvwEstrOrg.Nodes(intPos).Tag, 5)) = UCase(tbBuscarCodigo.Text) Then
    blnEncontrado = True
    Else
    intPos = intPos + 1
    End If
Loop

If blnEncontrado Then
    Set tvwEstrOrg.selectedItem = tvwEstrOrg.Nodes(intPos)
        tvwEstrOrg.selectedItem.EnsureVisible
Else
    Set tvwEstrOrg.selectedItem = tvwEstrOrg.Nodes(1)
End If

End Sub


Private Sub cmdAceptar_Click()
    If Me.optPersonaDirecta.Value = True Then
        'Persona directa, en esta opcion la persona, departamento o UON elegida se sigue grabando en PM_ROL
        'Se grabara en el update del grid de frmFlujosRoles
        DevolverPersonaDepUonSeleccionada
    ElseIf Me.optPersonaEnFuncionEmpresa.Value = True Then
        'Personas por empresa: por cada empresa que tenga esta solicitud configurada en CATN_TIPOPEDIDO_EMP_SOLICITUD se le asigna una persona/dep/UON
        DevolverAprobadorPorEmpresas
    ElseIf Me.optPersonaEnFuncionPeticionario.Value = True Then
        'Personas por peticionario: Se asocia a cada peticionario de esa solicitud, el que seria su aprobador
        DevolverAprobadorPorPeticionario
    Else
        'Personas por centro de coste: Dependiendo del centro de coste del pedido, ese sera su aprobador
        DevolverAprobadorPorCentroCoste
    End If
    'Indico que se tienen que actualizar los aprobadores de este rol, se grabaran en el BeforeUpdate del grid de frmFlujosRoles
    frmFlujosRoles.g_bActualizarAprobadores = True
    Screen.MousePointer = vbNormal
    Unload Me
End Sub
''' <summary>
''' Asigno los aprobadores por empresa seleccionados para este rol a la propiedad de frmFlujos
''' </summary>
Private Sub DevolverAprobadorPorEmpresas()
    Dim I As Integer
    Dim arrAprobadoresPorEmpresa() As String
    
    ReDim arrAprobadoresPorEmpresa(Me.sdbgPerEnFuncionEmpresa.Rows - 1, 5)
    'Recorremos la grid de aprobadores por empresa,a�adiendolos a un array
    sdbgPerEnFuncionEmpresa.MoveFirst
    For I = 0 To Me.sdbgPerEnFuncionEmpresa.Rows - 1
        If sdbgPerEnFuncionEmpresa.Columns("PER_DEP_UON").Value <> "" Then
            arrAprobadoresPorEmpresa(I, 0) = sdbgPerEnFuncionEmpresa.Columns("EMP_HIDDEN").Value
            arrAprobadoresPorEmpresa(I, 1) = sdbgPerEnFuncionEmpresa.Columns("UON1").Value
            arrAprobadoresPorEmpresa(I, 2) = sdbgPerEnFuncionEmpresa.Columns("UON2").Value
            arrAprobadoresPorEmpresa(I, 3) = sdbgPerEnFuncionEmpresa.Columns("UON3").Value
            arrAprobadoresPorEmpresa(I, 4) = sdbgPerEnFuncionEmpresa.Columns("DEP").Value
            arrAprobadoresPorEmpresa(I, 5) = sdbgPerEnFuncionEmpresa.Columns("PER").Value
        End If
        sdbgPerEnFuncionEmpresa.MoveNext
    Next
    'Asigno los aprobadores por empresa a la propiedad de frmFlujos, que luego en el BeforeUpdate del grid de roles sera utilizada para grabarlos
    frmFlujosRoles.arrAprobadoresPorEmpresa = arrAprobadoresPorEmpresa
    'Le indico el tipo de aprobador seleccionado, esta primera sentencia poniendo un tipo de aprobador que no es correcto se hace para
    'que si por ejemplo el tipo de aprobador ya era por centro de coste, se pincha en el grid para modificarlo y se modifica algun aprobador, no saltaria el evento BeforeUpdate del grid de frmFlujosRoles y no se grabarian esos cambios
    frmFlujosRoles.sdbgRoles.Columns("TIPO_APROBADOR").Value = TipoAprobadorSolicitudPedido.PersonaDirecta
    'Se indica el tipo de aprobador correcto
    frmFlujosRoles.sdbgRoles.Columns("TIPO_APROBADOR").Value = TipoAprobadorSolicitudPedido.PersonaEnFuncionEmpresa
    frmFlujosRoles.sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = "..." 'Indico que tiene aprobadores
End Sub

''' <summary>
''' Asigno los aprobadores por centro de coste seleccionados para este rol a la propiedad de frmFlujos
''' </summary>
Private Sub DevolverAprobadorPorCentroCoste()
    Dim I As Integer
    Dim arrAprobadoresPorCentroCoste() As String
    
    ReDim arrAprobadoresPorCentroCoste(Me.sdbgPersonaEnFuncionCentroDeCoste.Rows - 1, 5)
    'Recorremos la grid de aprobadores por centro de coste,a�adiendolos a un array
    sdbgPersonaEnFuncionCentroDeCoste.MoveFirst
    For I = 0 To sdbgPersonaEnFuncionCentroDeCoste.Rows - 1
        If sdbgPersonaEnFuncionCentroDeCoste.Columns("PER_DEP_UON_DEN").Value <> "" Then
            arrAprobadoresPorCentroCoste(I, 0) = sdbgPersonaEnFuncionCentroDeCoste.Columns("CENTRO_HIDDEN").Value
            arrAprobadoresPorCentroCoste(I, 1) = sdbgPersonaEnFuncionCentroDeCoste.Columns("UON1").Value
            arrAprobadoresPorCentroCoste(I, 2) = sdbgPersonaEnFuncionCentroDeCoste.Columns("UON2").Value
            arrAprobadoresPorCentroCoste(I, 3) = sdbgPersonaEnFuncionCentroDeCoste.Columns("UON3").Value
            arrAprobadoresPorCentroCoste(I, 4) = sdbgPersonaEnFuncionCentroDeCoste.Columns("DEP").Value
            arrAprobadoresPorCentroCoste(I, 5) = sdbgPersonaEnFuncionCentroDeCoste.Columns("PER").Value
        End If
        sdbgPersonaEnFuncionCentroDeCoste.MoveNext
    Next
    'Asigno los aprobadores por empresa a la propiedad de frmFlujos, que luego en el BeforeUpdate del grid de roles sera utilizada para grabarlos
    frmFlujosRoles.arrAprobadoresPorCentroCoste = arrAprobadoresPorCentroCoste
    'Le indico el tipo de aprobador seleccionado, esta primera sentencia poniendo un tipo de aprobador que no es correcto se hace para
    'que si por ejemplo el tipo de aprobador ya era por centro de coste, se pincha en el grid para modificarlo y se modifica algun aprobador, no saltaria el evento BeforeUpdate del grid de frmFlujosRoles y no se grabarian esos cambios
    frmFlujosRoles.sdbgRoles.Columns("TIPO_APROBADOR").Value = TipoAprobadorSolicitudPedido.PersonaDirecta
    'Se indica el tipo de aprobador correcto
    frmFlujosRoles.sdbgRoles.Columns("TIPO_APROBADOR").Value = TipoAprobadorSolicitudPedido.PersonaEnFuncionCentroDeCoste
    frmFlujosRoles.sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = "..." 'Indico que tiene aprobadores
End Sub

''' <summary>
''' Asigno los aprobadores por peticionario seleccionados para este rol a la propiedad de frmFlujos
''' </summary>
Private Sub DevolverAprobadorPorPeticionario()
    Dim I As Integer
    Dim arrAprobadoresPorPeticionario() As String
    
    
    ReDim arrAprobadoresPorPeticionario(sdbgPerEnFuncionDePeticionario.Rows - 1, 9)
    'Recorremos la grid de aprobadores por peticionario,a�adiendolos a un array
    sdbgPerEnFuncionDePeticionario.MoveFirst
    For I = 0 To sdbgPerEnFuncionDePeticionario.Rows - 1
        'Validacion de que se hayan seleccionado tanto aprobador como peticionario
        If sdbgPerEnFuncionDePeticionario.Columns("APROB_DEN").Value = "" Or sdbgPerEnFuncionDePeticionario.Columns("PET_DEN").Value = "" Then
            MsgBox m_sMensajeSeleccioneAprobadorPeticionario, vbExclamation
            Exit Sub
        End If
        arrAprobadoresPorPeticionario(I, 0) = sdbgPerEnFuncionDePeticionario.Columns("APROB_UON1").Value
        arrAprobadoresPorPeticionario(I, 1) = sdbgPerEnFuncionDePeticionario.Columns("APROB_UON2").Value
        arrAprobadoresPorPeticionario(I, 2) = sdbgPerEnFuncionDePeticionario.Columns("APROB_UON3").Value
        arrAprobadoresPorPeticionario(I, 3) = sdbgPerEnFuncionDePeticionario.Columns("APROB_DEP").Value
        arrAprobadoresPorPeticionario(I, 4) = sdbgPerEnFuncionDePeticionario.Columns("APROB_PER").Value
        
        arrAprobadoresPorPeticionario(I, 5) = sdbgPerEnFuncionDePeticionario.Columns("PET_UON1").Value
        arrAprobadoresPorPeticionario(I, 6) = sdbgPerEnFuncionDePeticionario.Columns("PET_UON2").Value
        arrAprobadoresPorPeticionario(I, 7) = sdbgPerEnFuncionDePeticionario.Columns("PET_UON3").Value
        arrAprobadoresPorPeticionario(I, 8) = sdbgPerEnFuncionDePeticionario.Columns("PET_DEP").Value
        arrAprobadoresPorPeticionario(I, 9) = sdbgPerEnFuncionDePeticionario.Columns("PET_PER").Value
        sdbgPerEnFuncionDePeticionario.MoveNext
    Next
    'Asigno los aprobadores por peticionario a la propiedad de frmFlujos, que luego en el BeforeUpdate del grid de roles sera utilizada para grabarlos
    frmFlujosRoles.arrAprobadoresPorPeticionario = arrAprobadoresPorPeticionario
    'Le indico el tipo de aprobador seleccionado, esta primera sentencia poniendo un tipo de aprobador que no es correcto se hace para
    'que si por ejemplo el tipo de aprobador ya era por centro de coste, se pincha en el grid para modificarlo y se modifica algun aprobador, no saltaria el evento BeforeUpdate del grid de frmFlujosRoles y no se grabarian esos cambios
    frmFlujosRoles.sdbgRoles.Columns("TIPO_APROBADOR").Value = TipoAprobadorSolicitudPedido.PersonaDirecta
    'Se indica el tipo de aprobador correcto
    frmFlujosRoles.sdbgRoles.Columns("TIPO_APROBADOR").Value = TipoAprobadorSolicitudPedido.PersonaEnFuncionPeticionario
    frmFlujosRoles.sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = "..." 'Indico que tiene aprobadores
End Sub

''' <summary>
''' Devuelve a frmFlujosRoles las persona/Dep/Uon seleccionada para que se grabe en PM_ROL
''' </summary>
Private Sub DevolverPersonaDepUonSeleccionada()
    Dim nodx As MSComctlLib.node
    Dim scod1 As String
    Dim oPer As CPersona

    Set nodx = tvwEstrOrg.selectedItem
    
    If nodx Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    scod1 = DevolverCod(nodx)
    'Indico al rol el tipo de aprobador que tendra la solicitud de pedido
    frmFlujosRoles.sdbgRoles.Columns("TIPO_APROBADOR").Value = TipoAprobadorSolicitudPedido.PersonaDirecta
    Select Case nodx.Image
        Case "UON0"
            oMensajes.PersonaNoValida
            Screen.MousePointer = vbNormal
            If Me.Visible Then tvwEstrOrg.SetFocus
            Exit Sub
        Case "UON1"
            frmFlujosRoles.sdbgRoles.Columns("UON0").Value = "False"
            frmFlujosRoles.sdbgRoles.Columns("UON1").Value = scod1
            frmFlujosRoles.sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = scod1 & " - " & DevolverDen(nodx)
            
            frmFlujosRoles.sdbgRoles.Columns("UON2").Value = ""
            frmFlujosRoles.sdbgRoles.Columns("UON3").Value = ""
            frmFlujosRoles.sdbgRoles.Columns("DEP").Value = ""
            frmFlujosRoles.sdbgRoles.Columns("PER").Value = ""

        Case "UON2"
            frmFlujosRoles.sdbgRoles.Columns("UON0").Value = "False"
            frmFlujosRoles.sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent)
            frmFlujosRoles.sdbgRoles.Columns("UON2").Value = scod1
            frmFlujosRoles.sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
            
            frmFlujosRoles.sdbgRoles.Columns("UON3").Value = ""
            frmFlujosRoles.sdbgRoles.Columns("DEP").Value = ""
            frmFlujosRoles.sdbgRoles.Columns("PER").Value = ""
            
        Case "UON3"
            frmFlujosRoles.sdbgRoles.Columns("UON0").Value = "False"
            frmFlujosRoles.sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
            frmFlujosRoles.sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent)
            frmFlujosRoles.sdbgRoles.Columns("UON3").Value = scod1
            frmFlujosRoles.sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & scod1 & " - " & DevolverDen(nodx)
            
            frmFlujosRoles.sdbgRoles.Columns("DEP").Value = ""
            frmFlujosRoles.sdbgRoles.Columns("PER").Value = ""
                    
        Case "Departamento"
            frmFlujosRoles.sdbgRoles.Columns("UON0").Value = "False"
            frmFlujosRoles.sdbgRoles.Columns("DEP").Value = scod1
            frmFlujosRoles.sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = scod1 & " - " & DevolverDen(nodx)
            
            Select Case Left(nodx.Tag, 4)
                Case "DEP0"
                    frmFlujosRoles.sdbgRoles.Columns("UON1").Value = ""
                    frmFlujosRoles.sdbgRoles.Columns("UON2").Value = ""
                    frmFlujosRoles.sdbgRoles.Columns("UON3").Value = ""
                Case "DEP1"
                    frmFlujosRoles.sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON2").Value = ""
                    frmFlujosRoles.sdbgRoles.Columns("UON3").Value = ""
                Case "DEP2"
                    frmFlujosRoles.sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON3").Value = ""
                Case "DEP3"
                    frmFlujosRoles.sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON3").Value = DevolverCod(nodx.Parent)
            End Select
            frmFlujosRoles.sdbgRoles.Columns("PER").Value = ""
                                
        Case "PerUsu"
            frmFlujosRoles.sdbgRoles.Columns("UON0").Value = "False"
            Set oPer = oFSGSRaiz.Generar_CPersona
            oPer.Cod = scod1
            oPer.CargarTodosLosDatos
            
            frmFlujosRoles.sdbgRoles.Columns("PER").Value = scod1
            frmFlujosRoles.sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = NullToStr(oPer.nombre) & " " & NullToStr(oPer.Apellidos) & " ( " & oPer.mail & " )"
            
            Select Case Left(nodx.Tag, 4)
                Case "PER0"
                    frmFlujosRoles.sdbgRoles.Columns("UON1").Value = ""
                    frmFlujosRoles.sdbgRoles.Columns("UON2").Value = ""
                    frmFlujosRoles.sdbgRoles.Columns("UON3").Value = ""
                Case "PER1"
                    frmFlujosRoles.sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON2").Value = ""
                    frmFlujosRoles.sdbgRoles.Columns("UON3").Value = ""
                Case "PER2"
                    frmFlujosRoles.sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON3").Value = ""
                Case "PER3"
                    frmFlujosRoles.sdbgRoles.Columns("UON1").Value = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON2").Value = DevolverCod(nodx.Parent.Parent.Parent)
                    frmFlujosRoles.sdbgRoles.Columns("UON3").Value = DevolverCod(nodx.Parent.Parent)
            End Select
            frmFlujosRoles.sdbgRoles.Columns("DEP").Value = DevolverCod(nodx.Parent)
        Case Else
            oMensajes.PersonaNoValida
            Screen.MousePointer = vbNormal
            If Me.Visible Then tvwEstrOrg.SetFocus
            Exit Sub
    End Select
    frmFlujosRoles.sdbgRoles.Columns("TIPO_APROBADOR").Value = TipoAprobadorSolicitudPedido.PersonaDirecta
End Sub

''' <summary>
''' Genera el arbol
''' </summary>
''' <param name="bOrdenadoPorDen">Si se desea ordenar por nombre</param>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo;1,2seg.</remarks>
Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Departamentos asociados
Dim oDepsAsocN0 As CDepAsociados
Dim oDepsAsocN1 As CDepAsociados
Dim oDepsAsocN2 As CDepAsociados
Dim oDepsAsocN3 As CDepAsociados
Dim oDepAsoc As CDepAsociado

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3

' Personas
Dim oPersN0 As CPersonas
Dim oPersN1 As CPersonas
Dim oPersN2 As CPersonas
Dim oPersN3 As CPersonas
Dim oPer As CPersona

' Otras
Dim nodx As node
Dim varCodPersona As Variant


Dim sUON1Empresas As String
Dim sUON2Empresas As String
Dim sUON3Empresas As String
    
    
    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
     
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
         
    If (oUsuarioSummit.Tipo = TipoDeUsuario.Persona Or oUsuarioSummit.Tipo = TipoDeUsuario.comprador) And _
       (bRUO Or bRDep) Then
        
        varCodPersona = oUsuarioSummit.Persona.Cod
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False
        
        ' Cargamos las personas  de esos departamentos.
        For Each oDepAsoc In oDepsAsocN0
             oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
        Next
            
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False
                ' Cargamos las personas  de esos departamentos.
        
                'Personas
                
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                   oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                    
                Next
        End Select
        
        
    Else
        Dim bsoloPM
        
        If g_sOrigen = "frmUsuarios" Then
            bsoloPM = True
        Else
            bsoloPM = False
        End If
        
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
        oPersN0.CargarTodasLasPersonas , , , , 0, , True, , , False, , True, , , , bsoloPM
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False, , , , , , , , , , , , g_sEmpresas, sUON1Empresas
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True, , , , bsoloPM
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False, , , , , , , , , , , , g_sEmpresas, sUON1Empresas
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False, , , , , , , , , , , , g_sEmpresas, sUON1Empresas, sUON2Empresas
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True, , , , bsoloPM
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True, , , , bsoloPM
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, , , , , , , , , , , , , g_sEmpresas, sUON1Empresas
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, , , , , , , , , , , , , g_sEmpresas, sUON1Empresas, sUON2Empresas
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen, , , , , , , , , , , , , g_sEmpresas, sUON1Empresas, sUON2Empresas, sUON3Empresas
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True, , , , bsoloPM
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True, , , , bsoloPM
                oPersN3.CargarTodasLasPersonas , , , , 3, , True, , , False, , True, , , , bsoloPM
                
        End Select
        
    End If
    
   '************************************************************
    'Generamos la estructura arborea
    tvwEstrOrg.Nodes.clear

    ' Unidades organizativas
    Set nodx = tvwEstrOrg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    Dim bEncontrado As Boolean
    bEncontrado = False
    
    'g_sEmpresas = ""
    
    If g_sEmpresas <> "" Then
        For Each oUON1 In oUnidadesOrgN1
            bEncontrado = False
            If ((InStr(1, sUON1Empresas, oUON1.Cod & ",") > 0) Or (InStr(1, sUON2Empresas, oUON1.Cod & "#") > 0) Or (InStr(1, sUON3Empresas, oUON1.Cod & "#") > 0)) Then
                bEncontrado = True
            End If
            If bEncontrado Then
                scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
                Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
                nodx.Tag = "UON1" & CStr(oUON1.Cod)
            End If
        Next
        
        For Each oUON2 In oUnidadesOrgN2
            bEncontrado = False
            If InStr(1, sUON2Empresas, oUON2.CodUnidadOrgNivel1 & "#" & oUON2.Cod & ",") > 0 Then
                bEncontrado = True
            End If
            If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                If InStr(1, sUON1Empresas, oUON2.CodUnidadOrgNivel1 & ",") > 0 Then
                    bEncontrado = True
                End If
            End If
            'o Inferior
            If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                If InStr(1, sUON3Empresas, oUON2.CodUnidadOrgNivel1 & "#" & oUON2.Cod & "#") > 0 Then
                    bEncontrado = True
                End If
            End If
            If bEncontrado Then
                scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
                scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
                Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
                nodx.Tag = "UON2" & CStr(oUON2.Cod)
            End If
                
        Next
        
        For Each oUON3 In oUnidadesOrgN3
            bEncontrado = False
            If InStr(1, sUON3Empresas, oUON3.CodUnidadOrgNivel1 & "#" & oUON3.CodUnidadOrgNivel2 & "#" & oUON3.Cod & ",") > 0 Then
                bEncontrado = True
            End If
            
            If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                If (InStr(1, sUON2Empresas, oUON3.CodUnidadOrgNivel1 & "#" & oUON3.CodUnidadOrgNivel2 & ",") > 0) Then
                    bEncontrado = True
                End If
            End If
            
            If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                If InStr(1, sUON1Empresas, oUON3.CodUnidadOrgNivel1 & ",") > 0 Then
                    bEncontrado = True
                End If
            End If
            
            
            If bEncontrado Then
                scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
                scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
                scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
                Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
                nodx.Tag = "UON3" & CStr(oUON3.Cod)
            End If
                
        Next
    
    Else
    
    
        For Each oUON1 In oUnidadesOrgN1
            scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
            nodx.Tag = "UON1" & CStr(oUON1.Cod)
        Next
        
        For Each oUON2 In oUnidadesOrgN2
            
            scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
            scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
            Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
            nodx.Tag = "UON2" & CStr(oUON2.Cod)
                
        Next
        
        For Each oUON3 In oUnidadesOrgN3
            
            scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
            scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
            scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
            Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
            nodx.Tag = "UON3" & CStr(oUON3.Cod)
                
        Next
    
    
    End If
    
    
    
    ' Departamentos
    
    If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador) _
        And (bRUO Or bRDep) Then
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
            Next
        Next
                   
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN1
                
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                bEncontrado = False
                If ((InStr(1, sUON1Empresas, oDepAsoc.CodUON1 & ",") > 0) Or (InStr(1, sUON2Empresas, oDepAsoc.CodUON1 & "#") > 0) Or (InStr(1, sUON3Empresas, oDepAsoc.CodUON1 & "#") > 0)) Then
                    bEncontrado = True
                End If
                
                If (bEncontrado) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
                    For Each oPer In oDepAsoc.Personas
                        If Not IsNull(oPer.Usuario) Then
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                        Else
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                        End If
                        nodx.Tag = "PER1" & CStr(oPer.Cod)
                    Next
                End If
            Next
        Else
            For Each oDepAsoc In oDepsAsocN1
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
                For Each oPer In oDepAsoc.Personas
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER1" & CStr(oPer.Cod)
                Next
            Next
        
        End If
        
        If g_sEmpresas <> "" Then
            
            For Each oDepAsoc In oDepsAsocN2
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                  
                bEncontrado = Buscar(sUON2Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oDepAsoc.CodUON1 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON3Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & "#")
                End If
                    
                                   
                If bEncontrado Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
                    For Each oPer In oDepAsoc.Personas
                        If Not IsNull(oPer.Usuario) Then
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                        Else
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                        End If
                        nodx.Tag = "PER2" & CStr(oPer.Cod)
                    Next
                End If
            Next
        Else
            
            For Each oDepAsoc In oDepsAsocN2
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                    
                Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
                For Each oPer In oDepAsoc.Personas
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER2" & CStr(oPer.Cod)
                Next
            Next
        
        End If
        
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN3
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
                scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                bEncontrado = Buscar(sUON3Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & "#" & oDepAsoc.CodUON3 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON2Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oDepAsoc.CodUON1 & ",")
                End If
                
                If bEncontrado Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
                    For Each oPer In oDepAsoc.Personas
                        If Not IsNull(oPer.Usuario) Then
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                        Else
                            Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                        End If
                        nodx.Tag = "PER3" & CStr(oPer.Cod)
                    Next
                End If
            Next
        Else
            For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
        Next
        End If
    
    Else
    
        'Departamentos
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
        
        Next
        'Departamentos de la UON1
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN1
                bEncontrado = False
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                If ((InStr(1, sUON1Empresas, oDepAsoc.CodUON1 & ",") > 0) Or (InStr(1, sUON2Empresas, oDepAsoc.CodUON1 & "#") > 0) Or (InStr(1, sUON3Empresas, oDepAsoc.CodUON1 & "#") > 0)) Then
                    bEncontrado = True
                End If
                
                If (bEncontrado) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
                End If
                
            Next
        Else
            For Each oDepAsoc In oDepsAsocN1
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
                
            Next
        End If
                
        'Departamentos de la UON2
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN2
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                bEncontrado = Buscar(sUON2Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oDepAsoc.CodUON1 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON3Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & "#")
                End If
                    
                                   
                If bEncontrado Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
                End If
                
            Next
            
        Else
            For Each oDepAsoc In oDepsAsocN2
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                    
                Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            Next
        
        End If
        
        
        'Departamentos de la UON3
        If g_sEmpresas <> "" Then
            For Each oDepAsoc In oDepsAsocN3
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
                scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                
                bEncontrado = Buscar(sUON3Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & "#" & oDepAsoc.CodUON3 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON2Empresas, oDepAsoc.CodUON1 & "#" & oDepAsoc.CodUON2 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oDepAsoc.CodUON1 & ",")
                End If
                
                
                If bEncontrado Then
                    Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                    nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
                End If
                
            Next
        Else
            For Each oDepAsoc In oDepsAsocN3
                scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
                scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
                scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
                scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
                Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
                
            Next
        
        End If
                
        'Personas
        'UON0
        For Each oPer In oPersN0
            scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER0" & CStr(oPer.Cod)
        Next
        'UON1
        If g_sEmpresas <> "" Then
            
            For Each oPer In oPersN1
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                bEncontrado = False
                If ((InStr(1, sUON1Empresas, oPer.UON1 & ",") > 0) Or (InStr(1, sUON2Empresas, oPer.UON1 & "#") > 0) Or (InStr(1, sUON3Empresas, oPer.UON1 & "#") > 0)) Then
                    bEncontrado = True
                End If
                
                If bEncontrado Then
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER1" & CStr(oPer.Cod)
                End If
            Next
        Else
            For Each oPer In oPersN1
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
            Next
        
        End If
        'UON2
        If g_sEmpresas <> "" Then
            For Each oPer In oPersN2
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                
                bEncontrado = Buscar(sUON2Empresas, oPer.UON1 & "#" & oPer.UON2 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oPer.UON1 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON3Empresas, oPer.UON1 & "#" & oPer.UON2 & "#")
                End If
                    
                                   
                If bEncontrado Then
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER2" & CStr(oPer.Cod)
                End If
            Next
        Else
            For Each oPer In oPersN2
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
            Next
        End If
        'UON3
        If g_sEmpresas <> "" Then
            For Each oPer In oPersN3
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
                scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                
                bEncontrado = Buscar(sUON3Empresas, oPer.UON1 & "#" & oPer.UON2 & "#" & oPer.UON3 & ",")
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON2Empresas, oPer.UON1 & "#" & oPer.UON2 & ",")
                End If
                
                If Not bEncontrado Then 'Mirar si la empresa esta en arbol superior
                    bEncontrado = Buscar(sUON1Empresas, oPer.UON1 & ",")
                End If
                
                
                If bEncontrado Then
                    If Not IsNull(oPer.Usuario) Then
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                    Else
                        Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                    End If
                    nodx.Tag = "PER3" & CStr(oPer.Cod)
                End If
            Next
        Else
            For Each oPer In oPersN3
                scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
                scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
                scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
                scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
                
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
        
        End If
    End If
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
    
End Sub
''' <summary>
''' Devuelve la denominaci�n del nodo
''' </summary>
''' <param name="node">Nodo del que se desea obtener la denominacion</param>
Private Function DevolverDen(ByVal node As MSComctlLib.node) As Variant

    If node Is Nothing Then Exit Function
    
    DevolverDen = Right(node.Text, (Len(node.Text) - (Len(node.Tag) - 1)))
    
End Function
''' <summary>
''' Devuelve el codigo del nodo
''' </summary>
''' <param name="node">Nodo del que se desea obtener el codigo</param>
Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
    If node Is Nothing Then Exit Function
    
    If Len(node.Tag) < 4 Then
        DevolverCod = node.Tag
        Exit Function
    End If
    DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
        
End Function

Private Sub cmdCancelar_Click()
    Unload Me
End Sub


''' <summary>
''' Configura la pantalla mostrando el arbol de la organizacion de empresa para seleccionar la persona,departamento o UON
''' </summary>
Private Sub optPersonaDirecta_Click()
    If EstructuraOrganizacionCargada = False Then
        'Solo cargo el arbol de organizacion una vez
        GenerarEstructuraOrg False
        EstructuraOrganizacionCargada = True
    End If
    'Muestro el grid correspondiente
    ConfigurarPantalla (PersonaDirecta)
    
    'Selecciono la persona, departamento o UON en el arbol de la organizacion
    SeleccionarEnUnidadOrganizativa
End Sub
''' <summary>
''' Configura la pantalla mostrando el grid de aprobadores por centros de coste y cargando esa grid
''' </summary>
Private Sub optPersonaEnFuncionCentroCoste_Click()
    On Error GoTo Error
    Dim rs As ADODB.Recordset
    Dim oNewRol As CPMRol
    'Muestro el grid correspondiente
    ConfigurarPantalla (PersonaEnFuncionCentroDeCoste)
    
    If Not g_oRolEnEdicion Is Nothing Then
        'Si se esta editando un rol
        Set rs = g_oRolEnEdicion.CargarAprobadoresPorCentroCoste(g_lIdSolicitud, g_TipoSolicitud)
    Else
        'Si es un rol nuevo
        Set oNewRol = oFSGSRaiz.Generar_CPMRol
        Set rs = oNewRol.CargarAprobadoresPorCentroCoste(g_lIdSolicitud, g_TipoSolicitud)
    End If
    
    Dim iLen As Integer
    
    iLen = UBound(frmFlujosRoles.arrAprobadoresPorCentroCoste, 1)
    
    If iLen >= 0 Then
        'Ya se habia entrado en la pantalla y se habia dado a aceptar y los datos estan guardados en el array arrAprobadoresPorCentroCoste
         CargarGridAprobadoresPorCentroCosteArray frmFlujosRoles.arrAprobadoresPorCentroCoste, rs
    End If
    Exit Sub
Error:
    
    
    'Cargo el grid con los centros de coste y sus aprobadores
    CargarGridAprobadoresPorCentroCoste rs
End Sub

''' <summary>
''' Configura la pantalla mostrando el grid de aprobadores por empresa y cargando esa grid
''' </summary>
Private Sub optPersonaEnFuncionEmpresa_Click()
    On Error GoTo Error
    Dim rs As ADODB.Recordset
    Dim oNewRol As CPMRol
    'Muestro el grid correspondiente
    ConfigurarPantalla (PersonaEnFuncionEmpresa)
    'Cargo el grid con las empresas
    
    
    If Not g_oRolEnEdicion Is Nothing Then
        'Si se esta editando un rol
        Set rs = g_oRolEnEdicion.CargarAprobadoresPorEmpresa(g_lIdSolicitud, g_TipoSolicitud)
    Else
        'Si es un rol nuevo
        Set oNewRol = oFSGSRaiz.Generar_CPMRol
        Set rs = oNewRol.CargarAprobadoresPorEmpresa(g_lIdSolicitud, g_TipoSolicitud)
    End If
    
    Dim iLen As Integer
    
    iLen = UBound(frmFlujosRoles.arrAprobadoresPorEmpresa, 1)
    
    If iLen >= 0 Then
        'Ya se habia entrado en la pantalla y se habia dado a aceptar y los datos estan guardados en el array arrAprobadoresPorEmpresa
         CargarGridAprobadoresPorEmpresaArray frmFlujosRoles.arrAprobadoresPorEmpresa, rs
    End If
    Exit Sub
Error:
    
    
    CargarGridAprobadoresPorEmpresa rs
    
End Sub
''' <summary>
''' Configura la pantalla mostrando el grid de aprobadores por peticionario
''' </summary>
Private Sub optPersonaEnFuncionPeticionario_Click()
    On Error GoTo Error
    
    Dim rs As ADODB.Recordset
    
    'Muestro el grid correspondiente
    ConfigurarPantalla (PersonaEnFuncionPeticionario)
    
    'Cargo el grid con los aprobadores por cada peticionario
    If Not g_oRolEnEdicion Is Nothing Then
        'Es un rol ya existente, si es un rol nuevo no se cargara nada en el grid
        Set rs = g_oRolEnEdicion.CargarAprobadoresPorPeticionario
    End If
    
    Dim iLen As Integer
    iLen = UBound(frmFlujosRoles.arrAprobadoresPorPeticionario, 1)
    
    If iLen >= 0 Then
        'Ya se habia entrado en la pantalla y se habia dado a aceptar y los datos estan guardados en el array arrAprobadoresPorEmpresa
         CargarGridAprobadoresPorPeticionarioArray frmFlujosRoles.arrAprobadoresPorPeticionario
    End If
    Exit Sub
Error:
    'Cargo el grid con los aprobadores por cada peticionario
    If Not g_oRolEnEdicion Is Nothing Then
        'Es un rol ya existente, si es un rol nuevo no se cargara nada en el grid
        CargarGridAprobadoresPorPeticionario rs
    End If
    
End Sub

''' <summary>
''' Carga la grid de aprobadores por peticionario
''' </summary>
''' <param name="rs">recordset con los datos de aprobadores por peticionario</param>
Private Sub CargarGridAprobadoresPorPeticionario(ByVal rs As ADODB.Recordset)
    Dim sPerDepUonAprobador As String
    Dim sPerDepUonPeticionario As String
    sdbgPerEnFuncionDePeticionario.RemoveAll
    While Not rs.EOF
        'Obtengo la denominacion del aprobador, segun sea persona, departamento o UON
        If NullToStr(rs("PER_COD_APROB").Value) <> "" Then
            'Persona
            sPerDepUonAprobador = rs("PER_COD_APROB").Value & "-" & rs("PER_NOM_APROB").Value
        ElseIf NullToStr(rs("DEP_COD_APROB").Value) <> "" Then
            'Departamento
            sPerDepUonAprobador = rs("DEP_COD_APROB").Value & "-" & rs("DEP_DEN_APROB").Value
        ElseIf NullToStr(rs("APROB_UON3").Value) <> "" Then
            'Uon3
            sPerDepUonAprobador = rs("APROB_UON1").Value & " " & rs("APROB_UON2").Value & " " & rs("APROB_UON3").Value & "-" & rs("UON3_DEN_APROB").Value
        ElseIf NullToStr(rs("APROB_UON2").Value) <> "" Then
            'Uon2
            sPerDepUonAprobador = rs("APROB_UON1").Value & " " & rs("APROB_UON2").Value & "-" & rs("UON2_DEN_APROB").Value
        ElseIf NullToStr(rs("APROB_UON1").Value) <> "" Then
            'Uon1
            sPerDepUonAprobador = rs("APROB_UON1").Value & "-" & rs("UON1_DEN_APROB").Value
        End If
        
        'Obtengo la denominacion del peticionario, segun sea persona, departamento o UON
        If NullToStr(rs("PER_COD_PET").Value) <> "" Then
            'Persona
            sPerDepUonPeticionario = rs("PER_COD_PET").Value & "-" & rs("PER_NOM_PET").Value
        ElseIf NullToStr(rs("DEP_COD_PET").Value) <> "" Then
            'Departamento
            sPerDepUonPeticionario = rs("DEP_COD_PET").Value & "-" & rs("DEP_DEN_PET").Value
        ElseIf NullToStr(rs("PET_UON3").Value) <> "" Then
            'Uon3
            sPerDepUonPeticionario = rs("PET_UON1").Value & " " & rs("PET_UON2").Value & " " & rs("PET_UON3").Value & "-" & rs("UON3_DEN_PET").Value
        ElseIf NullToStr(rs("PET_UON2").Value) <> "" Then
            'Uon2
            sPerDepUonPeticionario = rs("PET_UON1").Value & " " & rs("PET_UON2").Value & "-" & rs("UON2_DEN_PET").Value
        ElseIf NullToStr(rs("PET_UON1").Value) <> "" Then
            'Uon1
            sPerDepUonPeticionario = rs("PET_UON1").Value & "-" & rs("UON1_DEN_PET").Value
        Else
            sPerDepUonPeticionario = ""
        End If
        
        sdbgPerEnFuncionDePeticionario.AddItem sPerDepUonPeticionario & Chr(m_lSeparador) & rs("PET_UON1").Value & Chr(m_lSeparador) & rs("PET_UON2").Value & Chr(m_lSeparador) & rs("PET_UON3").Value & Chr(m_lSeparador) & rs("DEP_COD_PET").Value & Chr(m_lSeparador) & rs("PER_COD_PET").Value & Chr(m_lSeparador) & sPerDepUonAprobador & Chr(m_lSeparador) & rs("APROB_UON1").Value & Chr(m_lSeparador) & rs("APROB_UON2").Value & Chr(m_lSeparador) & rs("APROB_UON3").Value & Chr(m_lSeparador) & rs("DEP_COD_APROB").Value & Chr(m_lSeparador) & rs("PER_COD_APROB").Value
        rs.MoveNext
    Wend
    Set rs = Nothing
End Sub
''' <summary>
''' Carga la grid de aprobadores por peticionario
''' </summary>
''' <param name="rs">recordset con los datos de aprobadores por peticionario</param>
Private Sub CargarGridAprobadoresPorPeticionarioArray(ByRef arrDatos() As String)
    Dim sPerDepUonAprobador As String
    Dim sPerDepUonPeticionario As String
    sdbgPerEnFuncionDePeticionario.RemoveAll
    Dim oPersona As CPersona
    Dim oDepartamentos As CDepartamentos
    Dim oUnidOrgsNivel3 As CUnidadesOrgNivel3
    Dim oUnidOrgsNivel2 As CUnidadesOrgNivel2
    Dim oUnidOrgsNivel1 As CUnidadesOrgNivel1
    Dim I As Integer
    
    For I = 0 To UBound(arrDatos, 1)
        'Obtengo la denominacion del aprobador, segun sea persona, departamento o UON
        If NullToStr(arrDatos(I, 4)) <> "" Then
            'Persona
            Set oPersona = oFSGSRaiz.Generar_CPersona
            oPersona.Cod = arrDatos(I, 4)
            oPersona.CargarTodosLosDatos True
            sPerDepUonAprobador = oPersona.Cod & "-" & oPersona.nombre & " " & oPersona.Apellidos
        ElseIf NullToStr(arrDatos(I, 3)) <> "" Then
            'Departamento
            Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
            oDepartamentos.CargarTodosLosdepartamentosDesde 1, NullToStr(arrDatos(I, 3))
            sPerDepUonAprobador = oDepartamentos.Item(1).Cod & "-" & oDepartamentos.Item(1).Den
        ElseIf NullToStr(arrDatos(I, 2)) <> "" Then
            'Uon3
            Set oUnidOrgsNivel3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
            oUnidOrgsNivel3.CargarTodasLasUnidadesOrgNivel3 , , , , , , NullToStr(arrDatos(I, 2))
            sPerDepUonAprobador = NullToStr(arrDatos(I, 0)) & " " & NullToStr(arrDatos(I, 1)) & " " & NullToStr(arrDatos(I, 2)) & "-" & oUnidOrgsNivel3.Item(1).Den
        ElseIf NullToStr(arrDatos(I, 1)) <> "" Then
            'Uon2
            Set oUnidOrgsNivel2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
            oUnidOrgsNivel2.CargarTodasLasUnidadesOrgNivel2 , , , , , , NullToStr(arrDatos(I, 1))
            sPerDepUonAprobador = NullToStr(arrDatos(I, 0)) & " " & NullToStr(arrDatos(I, 1)) & "-" & oUnidOrgsNivel2.Item(1).Den
        ElseIf NullToStr(arrDatos(I, 0)) <> "" Then
            'Uon1
            Set oUnidOrgsNivel1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
            oUnidOrgsNivel1.CargarTodasLasUnidadesOrgNivel1 , , , , , , NullToStr(arrDatos(I, 0))
            sPerDepUonAprobador = NullToStr(arrDatos(I, 0)) & "-" & oUnidOrgsNivel1.Item(1).Den
        End If
        
        'Obtengo la denominacion del peticionario, segun sea persona, departamento o UON
        If NullToStr(arrDatos(I, 9)) <> "" Then
            'Persona
            Set oPersona = oFSGSRaiz.Generar_CPersona
            oPersona.Cod = arrDatos(I, 9)
            oPersona.CargarTodosLosDatos True
            sPerDepUonPeticionario = oPersona.Cod & "-" & oPersona.nombre & " " & oPersona.Apellidos
        ElseIf NullToStr(arrDatos(I, 8)) <> "" Then
            'Departamento
            Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
            oDepartamentos.CargarTodosLosdepartamentosDesde 1, NullToStr(arrDatos(I, 8))
            sPerDepUonPeticionario = oDepartamentos.Item(1).Cod & "-" & oDepartamentos.Item(1).Den
        ElseIf NullToStr(arrDatos(I, 7)) <> "" Then
            'Uon3
            Set oUnidOrgsNivel3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
            oUnidOrgsNivel3.CargarTodasLasUnidadesOrgNivel3 , , , , , , NullToStr(arrDatos(I, 7))
            sPerDepUonPeticionario = NullToStr(arrDatos(I, 5)) & " " & NullToStr(arrDatos(I, 6)) & " " & NullToStr(arrDatos(I, 7)) & "-" & oUnidOrgsNivel3.Item(1).Den
        ElseIf NullToStr(arrDatos(I, 6)) <> "" Then
            'Uon2
            Set oUnidOrgsNivel2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
            oUnidOrgsNivel2.CargarTodasLasUnidadesOrgNivel2 , , , , , , NullToStr(arrDatos(I, 6))
            sPerDepUonPeticionario = NullToStr(arrDatos(I, 5)) & " " & NullToStr(arrDatos(I, 6)) & "-" & oUnidOrgsNivel2.Item(1).Den
        ElseIf NullToStr(arrDatos(I, 5)) <> "" Then
            'Uon1
            Set oUnidOrgsNivel1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
            oUnidOrgsNivel1.CargarTodasLasUnidadesOrgNivel1 , , , , , , NullToStr(arrDatos(I, 5))
            sPerDepUonPeticionario = NullToStr(arrDatos(I, 5)) & "-" & oUnidOrgsNivel1.Item(1).Den
        Else
            sPerDepUonPeticionario = ""
        End If
        
        sdbgPerEnFuncionDePeticionario.AddItem sPerDepUonPeticionario & Chr(m_lSeparador) & NullToStr(arrDatos(I, 5)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 6)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 7)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 8)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 9)) & Chr(m_lSeparador) & sPerDepUonAprobador & Chr(m_lSeparador) & NullToStr(arrDatos(I, 0)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 1)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 2)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 3)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 4))
    Next
    
End Sub

''' <summary>
''' Carga la grid de empresas por aprobador
''' </summary>
''' <param name="rs">recordset con los datos de aprobadores por empresa</param>
Private Sub CargarGridAprobadoresPorEmpresa(ByVal rs As ADODB.Recordset)
    Dim sPerDepUon As String
    sdbgPerEnFuncionEmpresa.RemoveAll
    While Not rs.EOF
        'Obtengo la denominacion del aprobador, segun sea persona, departamento o UON
        If NullToStr(rs("PER_COD").Value) <> "" Then
            'Persona
            sPerDepUon = rs("PER_COD").Value & "-" & rs("PER_NOM").Value
        ElseIf NullToStr(rs("DEP_COD").Value) <> "" Then
            'Departamento
            sPerDepUon = rs("DEP_COD").Value & "-" & rs("DEP_DEN").Value
        ElseIf NullToStr(rs("UON3_COD").Value) <> "" Then
            'Uon3
            sPerDepUon = rs("UON1_COD").Value & " " & rs("UON2_COD").Value & " " & rs("UON3_COD").Value & "-" & rs("UON3_DEN").Value
        ElseIf NullToStr(rs("UON2_COD").Value) <> "" Then
            'Uon2
            sPerDepUon = rs("UON1_COD").Value & " " & rs("UON2_COD").Value & "-" & rs("UON2_DEN").Value
        ElseIf NullToStr(rs("UON1_COD").Value) <> "" Then
            'Uon1
            sPerDepUon = rs("UON1_COD").Value & "-" & rs("UON1_DEN").Value
        Else
            sPerDepUon = ""
        End If
        sdbgPerEnFuncionEmpresa.AddItem rs("EMP").Value & Chr(m_lSeparador) & rs("NIF").Value & Chr(m_lSeparador) & rs("DEN_EMP").Value & Chr(m_lSeparador) & sPerDepUon & Chr(m_lSeparador) & rs("UON1_COD").Value & Chr(m_lSeparador) & rs("UON2_COD").Value & Chr(m_lSeparador) & rs("UON3_COD").Value & Chr(m_lSeparador) & rs("DEP_COD").Value & Chr(m_lSeparador) & rs("PER_COD").Value
        rs.MoveNext
    Wend
    Set rs = Nothing
End Sub
''' <summary>
''' Carga la grid de empresas por aprobador
''' </summary>
''' <param name="rs">recordset con los datos de aprobadores por empresa</param>
Private Sub CargarGridAprobadoresPorEmpresaArray(ByRef arrDatos() As String, ByVal rs As ADODB.Recordset)
    Dim I As Integer
    Dim oEmpresas As CEmpresas
    Dim sPerDepUon As String
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    Dim bEncontrado As Boolean
    
    sdbgPerEnFuncionEmpresa.RemoveAll
    While Not rs.EOF
       bEncontrado = False
       For I = 0 To UBound(arrDatos, 1)
            If arrDatos(I, 0) = rs("EMP").Value Then
                bEncontrado = True
                'Obtengo la denominacion del aprobador, segun sea persona, departamento o UON
                If NullToStr(arrDatos(I, 5)) <> "" Then
                    'Persona
                    Dim oPersona As CPersona
                    Set oPersona = oFSGSRaiz.Generar_CPersona
                    oPersona.Cod = arrDatos(I, 5)
                    oPersona.CargarTodosLosDatos True
                    sPerDepUon = oPersona.Cod & "-" & oPersona.nombre & " " & oPersona.Apellidos
                ElseIf NullToStr(arrDatos(I, 4)) <> "" Then
                    'Departamento
                    Dim oDepartamentos As CDepartamentos
                    Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
                    oDepartamentos.CargarTodosLosdepartamentosDesde 1, NullToStr(arrDatos(I, 4))
                    sPerDepUon = oDepartamentos.Item(1).Cod & "-" & oDepartamentos.Item(1).Den
                ElseIf NullToStr(arrDatos(I, 3)) <> "" Then
                    'Uon3
                    Dim oUnidOrgsNivel3 As CUnidadesOrgNivel3
                    Set oUnidOrgsNivel3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
                    oUnidOrgsNivel3.CargarTodasLasUnidadesOrgNivel3 , , , , , , NullToStr(arrDatos(I, 3))
                    sPerDepUon = NullToStr(arrDatos(I, 1)) & " " & NullToStr(arrDatos(I, 2)) & " " & NullToStr(arrDatos(I, 3)) & "-" & oUnidOrgsNivel3.Item(1).Den
                ElseIf NullToStr(arrDatos(I, 2)) <> "" Then
                    'Uon2
                    Dim oUnidOrgsNivel2 As CUnidadesOrgNivel2
                    Set oUnidOrgsNivel2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
                    oUnidOrgsNivel2.CargarTodasLasUnidadesOrgNivel2 , , , , , , NullToStr(arrDatos(I, 2))
                    sPerDepUon = NullToStr(arrDatos(I, 1)) & " " & NullToStr(arrDatos(I, 2)) & "-" & oUnidOrgsNivel2.Item(1).Den
                ElseIf NullToStr(arrDatos(I, 1)) <> "" Then
                    'Uon1
                    Dim oUnidOrgsNivel1 As CUnidadesOrgNivel1
                    Set oUnidOrgsNivel1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
                    oUnidOrgsNivel1.CargarTodasLasUnidadesOrgNivel1 , , , , , , NullToStr(arrDatos(I, 1))
                    sPerDepUon = NullToStr(arrDatos(I, 1)) & "-" & oUnidOrgsNivel1.Item(1).Den
                Else
                    sPerDepUon = ""
                End If
                
                sdbgPerEnFuncionEmpresa.AddItem rs("EMP").Value & Chr(m_lSeparador) & rs("NIF").Value & Chr(m_lSeparador) & rs("DEN_EMP").Value & Chr(m_lSeparador) & sPerDepUon & Chr(m_lSeparador) & NullToStr(arrDatos(I, 1)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 2)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 3)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 4)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 5))
            End If
        Next
        If bEncontrado = False Then
            sdbgPerEnFuncionEmpresa.AddItem rs("EMP").Value & Chr(m_lSeparador) & rs("NIF").Value & Chr(m_lSeparador) & rs("DEN_EMP").Value & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        End If
        rs.MoveNext
    Wend
 
End Sub

''' <summary>
''' Carga la grid de centros de coste con sus aprobadores
''' </summary>
''' <param name="rs">recordset con los datos de aprobadores por centro de coste</param>
Private Sub CargarGridAprobadoresPorCentroCoste(ByVal rs As ADODB.Recordset)
    Dim sPerDepUon As String
    Dim sCentroDen As String
    sdbgPersonaEnFuncionCentroDeCoste.RemoveAll
    While Not rs.EOF
        'Obtengo la denominacion del aprobador, segun sea persona, departamento o UON
        If NullToStr(rs("PER_COD").Value) <> "" Then
            'Persona
            sPerDepUon = rs("PER_COD").Value & "-" & rs("PER_NOM").Value
        ElseIf NullToStr(rs("DEP_COD").Value) <> "" Then
            'Departamento
            sPerDepUon = rs("DEP_COD").Value & "-" & rs("DEP_DEN").Value
        ElseIf NullToStr(rs("UON3_COD").Value) <> "" Then
            'Uon3
            sPerDepUon = rs("UON1_COD").Value & " " & rs("UON2_COD").Value & " " & rs("UON3_COD").Value & "-" & rs("UON3_DEN").Value
        ElseIf NullToStr(rs("UON2_COD").Value) <> "" Then
            'Uon2
            sPerDepUon = rs("UON1_COD").Value & " " & rs("UON2_COD").Value & "-" & rs("UON2_DEN").Value
        ElseIf NullToStr(rs("UON1_COD").Value) <> "" Then
            'Uon1
            sPerDepUon = rs("UON1_COD").Value & "-" & rs("UON1_DEN").Value
        Else
            sPerDepUon = ""
        End If
        'Obtengo la denominacion del Centro de coste
        If NullToStr(rs("CS_UON4").Value) <> "" Then
            sCentroDen = rs("CS_UON4").Value & "-" & rs("CS_UON3").Value & "-" & rs("CS_UON2").Value & "-" & rs("CS_UON1").Value & "-" & rs("CS_COD").Value & "-" & rs("CS_DEN").Value
        ElseIf NullToStr(rs("CS_UON3").Value) <> "" Then
            sCentroDen = rs("CS_UON3").Value & "-" & rs("CS_UON2").Value & "-" & rs("CS_UON1").Value & "-" & rs("CS_COD").Value & "-" & rs("CS_DEN").Value
        ElseIf NullToStr(rs("CS_UON2").Value) <> "" Then
            sCentroDen = rs("CS_UON2").Value & "-" & rs("CS_UON1").Value & "-" & rs("CS_COD").Value & "-" & rs("CS_DEN").Value
        ElseIf NullToStr(rs("CS_UON1").Value) <> "" Then
            sCentroDen = rs("CS_UON1").Value & "-" & rs("CS_COD").Value & "-" & rs("CS_DEN").Value
        Else
            sCentroDen = rs("CS_COD").Value & "-" & rs("CS_DEN").Value
        End If
        sdbgPersonaEnFuncionCentroDeCoste.AddItem rs("CS_COD").Value & Chr(m_lSeparador) & sCentroDen & Chr(m_lSeparador) & rs("UON1_COD").Value & Chr(m_lSeparador) & rs("UON2_COD").Value & Chr(m_lSeparador) & rs("UON3_COD").Value & Chr(m_lSeparador) & rs("DEP_COD").Value & Chr(m_lSeparador) & rs("PER_COD").Value & Chr(m_lSeparador) & sPerDepUon
        rs.MoveNext
    Wend
    Set rs = Nothing
End Sub

''' <summary>
''' Carga la grid de centros de coste con sus aprobadores
''' </summary>
''' <param name="rs">recordset con los datos de aprobadores por centro de coste</param>
Private Sub CargarGridAprobadoresPorCentroCosteArray(ByRef arrDatos() As String, ByVal rs As ADODB.Recordset)
    Dim sPerDepUon As String
    Dim sCentroDen As String
    Dim oPersona As CPersona
    Dim oDepartamentos As CDepartamentos
    Dim oUnidOrgsNivel3 As CUnidadesOrgNivel3
    Dim oUnidOrgsNivel2 As CUnidadesOrgNivel2
    Dim oUnidOrgsNivel1 As CUnidadesOrgNivel1
    Dim oCentrosCoste As CCentrosCoste
    Dim bEncontrado As Boolean
    Dim I As Integer
    sdbgPersonaEnFuncionCentroDeCoste.RemoveAll
    While Not rs.EOF
        bEncontrado = False
        For I = 0 To UBound(arrDatos, 1)
            If NullToStr(arrDatos(I, 0)) = rs("CS_COD").Value Then
                    bEncontrado = True
                    'Obtengo la denominacion del aprobador, segun sea persona, departamento o UON
                    If NullToStr(arrDatos(I, 5)) <> "" Then
                        'Persona
                        Set oPersona = oFSGSRaiz.Generar_CPersona
                        oPersona.Cod = arrDatos(I, 5)
                        oPersona.CargarTodosLosDatos True
                        sPerDepUon = oPersona.Cod & "-" & oPersona.nombre & " " & oPersona.Apellidos
                    ElseIf NullToStr(arrDatos(I, 4)) <> "" Then
                        'Departamento
                        Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
                        oDepartamentos.CargarTodosLosdepartamentosDesde 1, NullToStr(arrDatos(I, 4))
                        sPerDepUon = oDepartamentos.Item(1).Cod & "-" & oDepartamentos.Item(1).Den
                    ElseIf NullToStr(arrDatos(I, 3)) <> "" Then
                        'Uon3
                        Set oUnidOrgsNivel3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
                        oUnidOrgsNivel3.CargarTodasLasUnidadesOrgNivel3 , , , , , , NullToStr(arrDatos(I, 3))
                        sPerDepUon = NullToStr(arrDatos(I, 1)) & " " & NullToStr(arrDatos(I, 2)) & " " & NullToStr(arrDatos(I, 3)) & "-" & oUnidOrgsNivel3.Item(1).Den
                    ElseIf NullToStr(arrDatos(I, 2)) <> "" Then
                        'Uon2
                        Set oUnidOrgsNivel2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
                        oUnidOrgsNivel2.CargarTodasLasUnidadesOrgNivel2 , , , , , , NullToStr(arrDatos(I, 2))
                        sPerDepUon = NullToStr(arrDatos(I, 1)) & " " & NullToStr(arrDatos(I, 2)) & "-" & oUnidOrgsNivel2.Item(1).Den
                    ElseIf NullToStr(arrDatos(I, 1)) <> "" Then
                        'Uon1
                        Set oUnidOrgsNivel1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
                        oUnidOrgsNivel1.CargarTodasLasUnidadesOrgNivel1 , , , , , , NullToStr(arrDatos(I, 1))
                        sPerDepUon = NullToStr(arrDatos(I, 1)) & "-" & oUnidOrgsNivel1.Item(1).Den
                    Else
                        sPerDepUon = ""
                    End If
                    'Obtengo la denominacion del Centro de coste
                    If NullToStr(arrDatos(I, 0)) <> "" Then
                        Set oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste
                        oCentrosCoste.CargarCentrosDeCoste NullToStr(arrDatos(I, 0))
                        If oCentrosCoste.Item(1).UON4 <> "" Then
                            sCentroDen = oCentrosCoste.Item(1).UON4 & "-" & oCentrosCoste.Item(1).UON3 & "-" & oCentrosCoste.Item(1).UON2 & "-" & oCentrosCoste.Item(1).UON1 & "-" & oCentrosCoste.Item(1).COD_SM & "-" & oCentrosCoste.Item(1).DEN_SM
                        ElseIf oCentrosCoste.Item(1).UON3 <> "" Then
                            sCentroDen = oCentrosCoste.Item(1).UON3 & "-" & oCentrosCoste.Item(1).UON2 & "-" & oCentrosCoste.Item(1).UON1 & "-" & oCentrosCoste.Item(1).COD_SM & "-" & oCentrosCoste.Item(1).DEN_SM
                        ElseIf oCentrosCoste.Item(1).UON2 <> "" Then
                            sCentroDen = oCentrosCoste.Item(1).UON2 & "-" & oCentrosCoste.Item(1).UON1 & "-" & oCentrosCoste.Item(1).COD_SM & "-" & oCentrosCoste.Item(1).DEN_SM
                        ElseIf oCentrosCoste.Item(1).UON1 <> "" Then
                            sCentroDen = oCentrosCoste.Item(1).UON1 & "-" & oCentrosCoste.Item(1).COD_SM & "-" & oCentrosCoste.Item(1).DEN_SM
                        Else
                            sCentroDen = oCentrosCoste.Item(1).Den
                        End If
                    End If
                    sdbgPersonaEnFuncionCentroDeCoste.AddItem oCentrosCoste.Item(1).COD_SM & Chr(m_lSeparador) & sCentroDen & Chr(m_lSeparador) & NullToStr(arrDatos(I, 1)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 2)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 3)) & Chr(m_lSeparador) & NullToStr(arrDatos(0, 4)) & Chr(m_lSeparador) & NullToStr(arrDatos(I, 5)) & Chr(m_lSeparador) & sPerDepUon
                
            End If
        Next
        If bEncontrado = False Then
            'Obtengo la denominacion del Centro de coste
            If NullToStr(rs("CS_UON4").Value) <> "" Then
                sCentroDen = rs("CS_UON4").Value & "-" & rs("CS_UON3").Value & "-" & rs("CS_UON2").Value & "-" & rs("CS_UON1").Value & "-" & rs("CS_COD").Value & "-" & rs("CS_DEN").Value
            ElseIf NullToStr(rs("CS_UON3").Value) <> "" Then
                sCentroDen = rs("CS_UON3").Value & "-" & rs("CS_UON2").Value & "-" & rs("CS_UON1").Value & "-" & rs("CS_COD").Value & "-" & rs("CS_DEN").Value
            ElseIf NullToStr(rs("CS_UON2").Value) <> "" Then
                sCentroDen = rs("CS_UON2").Value & "-" & rs("CS_UON1").Value & "-" & rs("CS_COD").Value & "-" & rs("CS_DEN").Value
            ElseIf NullToStr(rs("CS_UON1").Value) <> "" Then
                sCentroDen = rs("CS_UON1").Value & "-" & rs("CS_COD").Value & "-" & rs("CS_DEN").Value
            Else
                sCentroDen = rs("CS_COD").Value & "-" & rs("CS_DEN").Value
            End If
            sdbgPersonaEnFuncionCentroDeCoste.AddItem rs("CS_COD").Value & Chr(m_lSeparador) & sCentroDen & Chr(m_lSeparador) & rs("UON1_COD").Value & Chr(m_lSeparador) & rs("UON2_COD").Value & Chr(m_lSeparador) & rs("UON3_COD").Value & Chr(m_lSeparador) & rs("DEP_COD").Value & Chr(m_lSeparador) & rs("PER_COD").Value & Chr(m_lSeparador) & ""
        End If
        rs.MoveNext
    Wend
    
    
End Sub

''' <summary>
''' Nos indica si el parametro2 se encuentra en la cadena del parametro1
''' </summary>
''' <param name="sCadenaBuscar">Cadena donde se busca</param>
''' <param name="sCadenaABuscar">que queremos buscar en la cadenada param1</param>
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde:GenerarEstructuraOrg; Tiempo m�ximo:0seg.</remarks>
Function Buscar(ByVal sCadenaBuscar As String, ByVal sCadenaABuscar) As Boolean
    
    Dim bResultado As Boolean
    bResultado = False
    
    If InStr(1, sCadenaBuscar, sCadenaABuscar) > 0 Then
        bResultado = True
    
    End If
    Buscar = bResultado

End Function
''' <summary>
''' Configura la pantalla colocando los controles dependiendo de la opcion seleccionada
''' </summary>
''' <param name="Tipo">Tipo de aprobadores seleccionado para visualizar</param>
Private Sub ConfigurarPantalla(ByVal Tipo As TipoAprobadorSolicitudPedido)
    Select Case Tipo
    Case TipoAprobadorSolicitudPedido.PersonaDirecta
        'Muestro el arbol con las unidades organizativas y bajo el resto de grids del resto de opciones
        tvwEstrOrg.Top = 1920
        sdbgPerEnFuncionEmpresa.Top = 9000
        sdbgPerEnFuncionDePeticionario.Top = 9000
        sdbgPersonaEnFuncionCentroDeCoste.Top = 9000
        cmdAnyadirAprobadorPorPeticionario.Top = 9000
        cmdEliminarAprobadorPorPeticionario.Top = 9000
        'Muestro la caja de texto para busqueda de persona
        Me.lblBusqueda.Visible = True
        Me.cmdBuscarCodigo.Visible = True
        Me.tbBuscarCodigo.Visible = True
    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionEmpresa
        'Muestro el grid con las empresas y bajo el resto de grids del resto de opciones
        sdbgPerEnFuncionEmpresa.Top = 1920
        tvwEstrOrg.Top = 9000
        sdbgPerEnFuncionDePeticionario.Top = 9000
        cmdAnyadirAprobadorPorPeticionario.Top = 9000
        cmdEliminarAprobadorPorPeticionario.Top = 9000
        sdbgPersonaEnFuncionCentroDeCoste.Top = 9000
        'Oculto la caja de texto para busqueda de persona
        Me.lblBusqueda.Visible = False
        Me.cmdBuscarCodigo.Visible = False
        Me.tbBuscarCodigo.Visible = False
    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionPeticionario
        'Muestro el grid con los aprobadores por peticionario y bajo el resto de grids del resto de opciones
        sdbgPerEnFuncionDePeticionario.Top = 1920
        cmdAnyadirAprobadorPorPeticionario.Top = 1920
        cmdEliminarAprobadorPorPeticionario.Top = cmdAnyadirAprobadorPorPeticionario.Top + cmdAnyadirAprobadorPorPeticionario.Height + 100
        sdbgPerEnFuncionEmpresa.Top = 9000
        sdbgPersonaEnFuncionCentroDeCoste.Top = 9000
        tvwEstrOrg.Top = 9000
        'Oculto la caja de texto para busqueda de persona
        Me.lblBusqueda.Visible = False
        Me.cmdBuscarCodigo.Visible = False
        Me.tbBuscarCodigo.Visible = False
    Case TipoAprobadorSolicitudPedido.PersonaEnFuncionCentroDeCoste
        'Muestro el grid con los aprobadores por centro de coste y bajo el resto de grids del resto de opciones
        sdbgPersonaEnFuncionCentroDeCoste.Top = 1920
        sdbgPerEnFuncionDePeticionario.Top = 9000
        cmdAnyadirAprobadorPorPeticionario.Top = 9000
        cmdEliminarAprobadorPorPeticionario.Top = 9000
        sdbgPerEnFuncionEmpresa.Top = 9000
        tvwEstrOrg.Top = 9000
        'Oculto la caja de texto para busqueda de persona
        Me.lblBusqueda.Visible = False
        Me.cmdBuscarCodigo.Visible = False
        Me.tbBuscarCodigo.Visible = False
    End Select
    
    'Si hay acceso a FSSM muestro el radio button de Centros de coste
    If gParametrosGenerales.gsAccesoFSSM Then
        optPersonaEnFuncionCentroCoste.Visible = True
    Else
        optPersonaEnFuncionCentroCoste.Visible = False
    End If
End Sub

''' <summary>
''' Si el rol ya tenia una persona, departamento o Uon seleccionada, se selecciona en el arbol de la organizacion
''' </summary>
Private Sub SeleccionarEnUnidadOrganizativa()
    Dim nodx As MSComctlLib.node
    
    scod1 = g_sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(g_sUON1))
    scod2 = g_sUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(g_sUON2))
    scod3 = g_sUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(g_sUON3))
    scod4 = g_sDEP & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(g_sDEP))
    
    If g_sPer <> "" Then 'Seleccionar la persona en la estructura de la organizacion
        Set nodx = Me.tvwEstrOrg.Nodes.Item("PERS" & g_sPer)
        nodx.Parent.Expanded = True
        nodx.Selected = True
    ElseIf g_sDEP <> "" Then 'Seleccionar el departamento en la estructura de la organizacion
        Set nodx = Me.tvwEstrOrg.Nodes.Item("DEPA" & LTrim(scod1) & LTrim(scod2) & LTrim(scod3) & scod4)
        nodx.Parent.Expanded = True
        nodx.Selected = True
    ElseIf g_sUON3 <> "" Then 'Seleccionar el uon3 en la estructura de la organizacion
        Set nodx = Me.tvwEstrOrg.Nodes.Item("UON3" & scod1 & scod2 & scod3)
        nodx.Parent.Expanded = True
        nodx.Selected = True
    ElseIf g_sUON2 <> "" Then 'Seleccionar el uon2 en la estructura de la organizacion
        Set nodx = Me.tvwEstrOrg.Nodes.Item("UON2" & scod1 & scod2)
        nodx.Parent.Expanded = True
        nodx.Selected = True
    ElseIf g_sUON1 <> "" Then 'Seleccionar el uon1 en la estructura de la organizacion
        Set nodx = Me.tvwEstrOrg.Nodes.Item("UON1" & scod1)
        nodx.Parent.Expanded = True
        nodx.Selected = True
    ElseIf g_sUON0 <> "" Then 'Seleccionar el uon0 en la estructura de la organizacion
        Set nodx = Me.tvwEstrOrg.Nodes.Item("UON0")
        nodx.Selected = True
    End If
End Sub




'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''EVENTOS DE GRIDS'''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

''' <summary>
''' Evento que se produce al cambiar de celda o fila en la grid
''' </summary>
Private Sub sdbgPerEnFuncionEmpresa_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    sdbgPerEnFuncionEmpresa.Columns("PER_DEP_UON").Style = ssStyleEditButton
End Sub
''' <summary>
''' Evento que se produce al pulsar el boton de una celda del grid
''' </summary>
Private Sub sdbgPerEnFuncionEmpresa_BtnClick()
    If sdbgPerEnFuncionEmpresa.Col <> -1 Then
        If sdbgPerEnFuncionEmpresa.Columns(sdbgPerEnFuncionEmpresa.Col).Name = "PER_DEP_UON" Then
            frmSOLSelPersona.g_oTipoAprobador = TipoAprobadorSolicitudPedido.PersonaEnFuncionEmpresa
            frmSOLSelPersona.g_sOrigen = "frmSOLSelPersonaEnFuncionDe"
            frmSOLSelPersona.Show vbModal
        End If
    End If
End Sub
''' <summary>
''' Evento que se produce al cambiar el contenido de una celda
''' </summary>
Private Sub sdbgPerEnFuncionEmpresa_Change()
    'En caso de borrar la denominacion del aprobador se borran sus campos asociados
    If sdbgPerEnFuncionEmpresa.Col <> -1 Then
        If sdbgPerEnFuncionEmpresa.Columns(sdbgPerEnFuncionEmpresa.Col).Name = "PER_DEP_UON" And sdbgPerEnFuncionEmpresa.Columns("PER_DEP_UON").Value = "" Then
            sdbgPerEnFuncionEmpresa.Columns("UON1").Value = ""
            sdbgPerEnFuncionEmpresa.Columns("UON2").Value = ""
            sdbgPerEnFuncionEmpresa.Columns("UON3").Value = ""
            sdbgPerEnFuncionEmpresa.Columns("DEP").Value = ""
            sdbgPerEnFuncionEmpresa.Columns("PER").Value = ""
        End If
    End If
End Sub

''' <summary>
''' Evento que se produce al cambiar de celda o fila en la grid
''' </summary>
Private Sub sdbgPersonaEnFuncionCentroDeCoste_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    sdbgPersonaEnFuncionCentroDeCoste.Columns("PER_DEP_UON_DEN").Style = ssStyleEditButton
End Sub

''' <summary>
''' Evento que se produce al pulsar el boton de una celda del grid
''' </summary>
Private Sub sdbgPersonaEnFuncionCentroDeCoste_BtnClick()
    If sdbgPersonaEnFuncionCentroDeCoste.Col <> -1 Then
        If sdbgPersonaEnFuncionCentroDeCoste.Columns(sdbgPersonaEnFuncionCentroDeCoste.Col).Name = "PER_DEP_UON_DEN" Then
            frmSOLSelPersona.g_oTipoAprobador = TipoAprobadorSolicitudPedido.PersonaEnFuncionCentroDeCoste
            frmSOLSelPersona.g_sOrigen = "frmSOLSelPersonaEnFuncionDe"
            frmSOLSelPersona.Show vbModal
        End If
    End If
End Sub

''' <summary>
''' Evento que se produce al cambiar el contenido de una celda
''' </summary>
Private Sub sdbgPersonaEnFuncionCentroDeCoste_Change()
    'En caso de borrar la denominacion del aprobador se borran sus campos asociados
    If sdbgPersonaEnFuncionCentroDeCoste.Col <> -1 Then
        If sdbgPersonaEnFuncionCentroDeCoste.Columns(sdbgPersonaEnFuncionCentroDeCoste.Col).Name = "PER_DEP_UON_DEN" And sdbgPersonaEnFuncionCentroDeCoste.Columns("PER_DEP_UON_DEN").Value = "" Then
            sdbgPersonaEnFuncionCentroDeCoste.Columns("UON1").Value = ""
            sdbgPersonaEnFuncionCentroDeCoste.Columns("UON2").Value = ""
            sdbgPersonaEnFuncionCentroDeCoste.Columns("UON3").Value = ""
            sdbgPersonaEnFuncionCentroDeCoste.Columns("DEP").Value = ""
            sdbgPersonaEnFuncionCentroDeCoste.Columns("PER").Value = ""
        End If
    End If
End Sub


''' <summary>
''' Evento que se produce al cambiar de celda o fila en la grid
''' </summary>
Private Sub sdbgPerEnFuncionDePeticionario_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    sdbgPerEnFuncionDePeticionario.Columns("PET_DEN").Style = ssStyleEditButton
    sdbgPerEnFuncionDePeticionario.Columns("APROB_DEN").Style = ssStyleEditButton
End Sub
''' <summary>
''' Evento que se produce al pulsar el boton de una celda del grid
''' </summary>
Private Sub sdbgPerEnFuncionDePeticionario_BtnClick()
    If sdbgPerEnFuncionDePeticionario.Col <> -1 Then
        If sdbgPerEnFuncionDePeticionario.Columns(sdbgPerEnFuncionDePeticionario.Col).Name = "PET_DEN" Then
            'Se indica que la persona/departamento/UON seleccionada se metera en el campo PETICIONARIO
            frmSOLSelPersona.g_sAprobadoroPeticionario = "PET"
        ElseIf sdbgPerEnFuncionDePeticionario.Columns(sdbgPerEnFuncionDePeticionario.Col).Name = "APROB_DEN" Then
            'Se indica que la persona/departamento/UON seleccionada se metera en el campo APROBADOR
            frmSOLSelPersona.g_sAprobadoroPeticionario = "APROB"
        End If
        frmSOLSelPersona.g_oTipoAprobador = TipoAprobadorSolicitudPedido.PersonaEnFuncionPeticionario
        frmSOLSelPersona.g_sOrigen = "frmSOLSelPersonaEnFuncionDe"
        frmSOLSelPersona.Show vbModal
    End If
End Sub

Private Sub sdbgPerEnFuncionDePeticionario_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>
''' Evento que se produce al cambiar el contenido de una celda
''' </summary>
Private Sub sdbgPerEnFuncionDePeticionario_Change()
    'En caso de borrar la denominacion del aprobador o el peticionario se borran sus campos asociados
    If sdbgPerEnFuncionDePeticionario.Col <> -1 Then
        If sdbgPerEnFuncionDePeticionario.Columns(sdbgPerEnFuncionEmpresa.Col).Name = "PET_DEN" And sdbgPerEnFuncionDePeticionario.Columns("PET_DEN").Value = "" Then
            sdbgPerEnFuncionDePeticionario.Columns("PET_UON1").Value = ""
            sdbgPerEnFuncionDePeticionario.Columns("PET_UON2").Value = ""
            sdbgPerEnFuncionDePeticionario.Columns("PET_UON3").Value = ""
            sdbgPerEnFuncionDePeticionario.Columns("PET_DEP").Value = ""
            sdbgPerEnFuncionDePeticionario.Columns("PET_PER").Value = ""
        End If
        
        If sdbgPerEnFuncionDePeticionario.Columns(sdbgPerEnFuncionEmpresa.Col).Name = "APROB_DEN" And sdbgPerEnFuncionDePeticionario.Columns("APROB_DEN").Value = "" Then
            sdbgPerEnFuncionDePeticionario.Columns("APROB_UON1").Value = ""
            sdbgPerEnFuncionDePeticionario.Columns("APROB_UON2").Value = ""
            sdbgPerEnFuncionDePeticionario.Columns("APROB_UON3").Value = ""
            sdbgPerEnFuncionDePeticionario.Columns("APROB_DEP").Value = ""
            sdbgPerEnFuncionDePeticionario.Columns("APROB_PER").Value = ""
        End If
    End If
End Sub
