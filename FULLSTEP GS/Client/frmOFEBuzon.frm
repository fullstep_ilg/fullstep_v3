VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{1E673DEF-C82F-4FBB-BAB9-77B0634E8F4D}#1.0#0"; "ProceSelector.ocx"
Begin VB.Form frmOFEBuzon 
   Caption         =   "Buz�n de ofertas"
   ClientHeight    =   6480
   ClientLeft      =   870
   ClientTop       =   1905
   ClientWidth     =   12015
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOFEBuzon.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6480
   ScaleWidth      =   12015
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1275
      Left            =   60
      ScaleHeight     =   1245
      ScaleWidth      =   11895
      TabIndex        =   14
      Top             =   0
      Width           =   11925
      Begin VB.CommandButton cmdColaboracion 
         Enabled         =   0   'False
         Height          =   285
         Left            =   9750
         Picture         =   "frmOFEBuzon.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   825
         Width           =   315
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   900
         TabIndex        =   0
         Top             =   60
         Width           =   1110
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   3240
         TabIndex        =   1
         Top             =   60
         Width           =   1110
      End
      Begin VB.OptionButton opLeidas 
         Caption         =   "S�lo le�das"
         Height          =   195
         Left            =   4850
         TabIndex        =   10
         Top             =   120
         Width           =   1420
      End
      Begin VB.OptionButton opNoLeidas 
         Caption         =   "S�lo no le�das"
         Height          =   195
         Left            =   6350
         TabIndex        =   11
         Top             =   120
         Width           =   2350
      End
      Begin VB.OptionButton opTodas 
         Caption         =   "Todas"
         Height          =   195
         Left            =   8780
         TabIndex        =   12
         Top             =   120
         Width           =   800
      End
      Begin VB.CommandButton cmdBuscarProce 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9240
         Picture         =   "frmOFEBuzon.frx":03D3
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   480
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscarProve 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5250
         Picture         =   "frmOFEBuzon.frx":0460
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   900
         Width           =   315
      End
      Begin VB.CommandButton cmdCalFecDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2070
         Picture         =   "frmOFEBuzon.frx":04ED
         Style           =   1  'Graphical
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   60
         Width           =   315
      End
      Begin VB.CommandButton cmdCalFecHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4425
         Picture         =   "frmOFEBuzon.frx":0A77
         Style           =   1  'Graphical
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   60
         Width           =   315
      End
      Begin VB.CommandButton cmdActualizar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9750
         Picture         =   "frmOFEBuzon.frx":1001
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   105
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9750
         Picture         =   "frmOFEBuzon.frx":108C
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   465
         UseMaskColor    =   -1  'True
         Width           =   330
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   65535
         Left            =   9300
         Top             =   15
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
         Height          =   285
         Left            =   2300
         TabIndex        =   7
         Top             =   900
         Width           =   2820
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4048
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1931
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4974
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
         Height          =   285
         Left            =   900
         TabIndex        =   6
         Top             =   900
         Width           =   1350
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1931
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4048
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2381
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEstadoDen 
         Height          =   285
         Left            =   7150
         TabIndex        =   9
         Top             =   900
         Width           =   2400
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   3519
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 1"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1588
         Columns(1).Caption=   "Cod."
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4233
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEstadoCod 
         Height          =   285
         Left            =   6270
         TabIndex        =   8
         Top             =   900
         Width           =   855
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1588
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3519
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1508
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   2400
         TabIndex        =   3
         Top             =   480
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   900
         TabIndex        =   2
         Top             =   480
         Width           =   960
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1693
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
         Height          =   285
         Left            =   4500
         TabIndex        =   4
         Top             =   480
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4419
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
         Height          =   285
         Left            =   5580
         TabIndex        =   5
         Top             =   480
         Width           =   3630
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5292
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6403
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SelectorDeProcesos.ProceSelector ProceSelector1 
         Height          =   315
         Left            =   3480
         TabIndex        =   21
         Top             =   480
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
      End
      Begin VB.Label lblTZCaption 
         Caption         =   "DZona horaria:"
         Height          =   255
         Left            =   10200
         TabIndex        =   31
         Top             =   120
         Width           =   1575
      End
      Begin VB.Label lblTZ 
         Caption         =   "TZIni"
         Height          =   255
         Left            =   10200
         TabIndex        =   30
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label lblFecDesde 
         Caption         =   "Desde:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   120
         TabIndex        =   28
         Top             =   120
         Width           =   660
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Hasta:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   2460
         TabIndex        =   27
         Top             =   120
         Width           =   645
      End
      Begin VB.Line Line1 
         X1              =   4800
         X2              =   4800
         Y1              =   0
         Y2              =   420
      End
      Begin VB.Line Line2 
         X1              =   4860
         X2              =   9600
         Y1              =   420
         Y2              =   420
      End
      Begin VB.Line Line3 
         X1              =   4860
         X2              =   0
         Y1              =   420
         Y2              =   420
      End
      Begin VB.Line Line4 
         X1              =   0
         X2              =   9600
         Y1              =   840
         Y2              =   840
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   5700
         TabIndex        =   26
         Top             =   960
         Width           =   690
      End
      Begin VB.Label lblProve 
         Caption         =   "Proveedor:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   60
         TabIndex        =   25
         Top             =   960
         Width           =   870
      End
      Begin VB.Label lblCProceCod 
         Caption         =   "Proceso:"
         Height          =   180
         Left            =   3840
         TabIndex        =   24
         Top             =   540
         Width           =   960
      End
      Begin VB.Label lblAnyo 
         Caption         =   "A�o:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   120
         TabIndex        =   23
         Top             =   525
         Width           =   735
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Comm:"
         Height          =   225
         Left            =   1890
         TabIndex        =   22
         Top             =   525
         Width           =   810
      End
      Begin VB.Line Line5 
         X1              =   9615
         X2              =   9615
         Y1              =   0
         Y2              =   1200
      End
   End
   Begin VB.PictureBox Picture2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5115
      Left            =   60
      ScaleHeight     =   5055
      ScaleWidth      =   11865
      TabIndex        =   13
      Top             =   1320
      Width           =   11925
      Begin SSDataWidgets_B.SSDBGrid sdbgOfertas 
         Height          =   5055
         Left            =   0
         TabIndex        =   29
         Top             =   0
         Width           =   11850
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   19
         stylesets.count =   7
         stylesets(0).Name=   "Head"
         stylesets(0).BackColor=   -2147483633
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmOFEBuzon.frx":118E
         stylesets(1).Name=   "Leido"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmOFEBuzon.frx":11AA
         stylesets(2).Name=   "No leido"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmOFEBuzon.frx":14FC
         stylesets(3).Name=   "Bold"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmOFEBuzon.frx":188E
         stylesets(4).Name=   "Normal"
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmOFEBuzon.frx":18AA
         stylesets(5).Name=   "RowSelection"
         stylesets(5).ForeColor=   16777215
         stylesets(5).BackColor=   8388608
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmOFEBuzon.frx":18C6
         stylesets(6).Name=   "Adjunto"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmOFEBuzon.frx":18E2
         UseGroups       =   -1  'True
         DividerType     =   0
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         HeadStyleSet    =   "Head"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ActiveRowStyleSet=   "RowSelection"
         Groups.Count    =   3
         Groups(0).Width =   6350
         Groups(0).Caption=   "Proveedor"
         Groups(0).HasHeadBackColor=   -1  'True
         Groups(0).HeadBackColor=   -2147483633
         Groups(0).Columns.Count=   6
         Groups(0).Columns(0).Width=   1111
         Groups(0).Columns(0).Caption=   "LEIDA"
         Groups(0).Columns(0).Name=   "LEIDA"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(0).Locked=   -1  'True
         Groups(0).Columns(1).Width=   1111
         Groups(0).Columns(1).Caption=   "ADJUNTOS"
         Groups(0).Columns(1).Name=   "ADJUNTOS"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(1).Locked=   -1  'True
         Groups(0).Columns(2).Width=   2302
         Groups(0).Columns(2).Caption=   "C�digo"
         Groups(0).Columns(2).Name=   "PROVECOD"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         Groups(0).Columns(2).Locked=   -1  'True
         Groups(0).Columns(3).Width=   2831
         Groups(0).Columns(3).Caption=   "Denominaci�n"
         Groups(0).Columns(3).Name=   "PROVEDEN"
         Groups(0).Columns(3).DataField=   "Column 3"
         Groups(0).Columns(3).DataType=   8
         Groups(0).Columns(3).FieldLen=   256
         Groups(0).Columns(3).Locked=   -1  'True
         Groups(0).Columns(4).Width=   1402
         Groups(0).Columns(4).Visible=   0   'False
         Groups(0).Columns(4).Caption=   "A"
         Groups(0).Columns(4).Name=   "A"
         Groups(0).Columns(4).DataField=   "Column 4"
         Groups(0).Columns(4).DataType=   8
         Groups(0).Columns(4).FieldLen=   256
         Groups(0).Columns(5).Width=   3545
         Groups(0).Columns(5).Visible=   0   'False
         Groups(0).Columns(5).Caption=   "L"
         Groups(0).Columns(5).Name=   "L"
         Groups(0).Columns(5).DataField=   "Column 5"
         Groups(0).Columns(5).DataType=   8
         Groups(0).Columns(5).FieldLen=   256
         Groups(1).Width =   7223
         Groups(1).Caption=   "Proceso"
         Groups(1).HasHeadBackColor=   -1  'True
         Groups(1).HeadBackColor=   -2147483633
         Groups(1).Columns.Count=   5
         Groups(1).Columns(0).Width=   1111
         Groups(1).Columns(0).Caption=   "A�o"
         Groups(1).Columns(0).Name=   "ANYO"
         Groups(1).Columns(0).DataField=   "Column 6"
         Groups(1).Columns(0).DataType=   8
         Groups(1).Columns(0).FieldLen=   256
         Groups(1).Columns(0).Locked=   -1  'True
         Groups(1).Columns(1).Width=   953
         Groups(1).Columns(1).Caption=   "Gmn1"
         Groups(1).Columns(1).Name=   "GMN1"
         Groups(1).Columns(1).DataField=   "Column 7"
         Groups(1).Columns(1).DataType=   8
         Groups(1).Columns(1).FieldLen=   256
         Groups(1).Columns(1).Locked=   -1  'True
         Groups(1).Columns(2).Width=   1349
         Groups(1).Columns(2).Caption=   "C�digo"
         Groups(1).Columns(2).Name=   "PROCECOD"
         Groups(1).Columns(2).DataField=   "Column 8"
         Groups(1).Columns(2).DataType=   8
         Groups(1).Columns(2).FieldLen=   256
         Groups(1).Columns(2).Locked=   -1  'True
         Groups(1).Columns(3).Width=   3810
         Groups(1).Columns(3).Caption=   "Descripci�n"
         Groups(1).Columns(3).Name=   "PROCEDEN"
         Groups(1).Columns(3).DataField=   "Column 9"
         Groups(1).Columns(3).DataType=   8
         Groups(1).Columns(3).FieldLen=   256
         Groups(1).Columns(3).Locked=   -1  'True
         Groups(1).Columns(4).Width=   3493
         Groups(1).Columns(4).Visible=   0   'False
         Groups(1).Columns(4).Caption=   "Estado"
         Groups(1).Columns(4).Name=   "ESTADOID"
         Groups(1).Columns(4).DataField=   "Column 10"
         Groups(1).Columns(4).DataType=   8
         Groups(1).Columns(4).FieldLen=   256
         Groups(2).Width =   6006
         Groups(2).Caption=   "Oferta"
         Groups(2).Columns.Count=   8
         Groups(2).Columns(0).Width=   2143
         Groups(2).Columns(0).Caption=   "Fecha"
         Groups(2).Columns(0).Name=   "FECHA"
         Groups(2).Columns(0).DataField=   "Column 11"
         Groups(2).Columns(0).DataType=   8
         Groups(2).Columns(0).FieldLen=   256
         Groups(2).Columns(0).Locked=   -1  'True
         Groups(2).Columns(1).Width=   1720
         Groups(2).Columns(1).Caption=   "N�mero"
         Groups(2).Columns(1).Name=   "NUM"
         Groups(2).Columns(1).DataField=   "Column 12"
         Groups(2).Columns(1).DataType=   8
         Groups(2).Columns(1).FieldLen=   256
         Groups(2).Columns(1).Locked=   -1  'True
         Groups(2).Columns(2).Width=   2143
         Groups(2).Columns(2).Caption=   "Estado"
         Groups(2).Columns(2).Name=   "ESTADOOFE"
         Groups(2).Columns(2).DataField=   "Column 13"
         Groups(2).Columns(2).DataType=   8
         Groups(2).Columns(2).FieldLen=   256
         Groups(2).Columns(2).Locked=   -1  'True
         Groups(2).Columns(3).Width=   1323
         Groups(2).Columns(3).Visible=   0   'False
         Groups(2).Columns(3).Caption=   "Comprador"
         Groups(2).Columns(3).Name=   "COMPRADOR"
         Groups(2).Columns(3).DataField=   "Column 14"
         Groups(2).Columns(3).DataType=   8
         Groups(2).Columns(3).FieldLen=   256
         Groups(2).Columns(4).Width=   1455
         Groups(2).Columns(4).Visible=   0   'False
         Groups(2).Columns(4).Caption=   "Equipo"
         Groups(2).Columns(4).Name=   "EQUIPO"
         Groups(2).Columns(4).DataField=   "Column 15"
         Groups(2).Columns(4).DataType=   8
         Groups(2).Columns(4).FieldLen=   256
         Groups(2).Columns(5).Width=   2963
         Groups(2).Columns(5).Visible=   0   'False
         Groups(2).Columns(5).Caption=   "Cerrado"
         Groups(2).Columns(5).Name=   "CERRADO"
         Groups(2).Columns(5).DataField=   "Column 16"
         Groups(2).Columns(5).DataType=   8
         Groups(2).Columns(5).FieldLen=   256
         Groups(2).Columns(6).Width=   2937
         Groups(2).Columns(6).Visible=   0   'False
         Groups(2).Columns(6).Caption=   "PORTAL"
         Groups(2).Columns(6).Name=   "PORTAL"
         Groups(2).Columns(6).DataField=   "Column 17"
         Groups(2).Columns(6).DataType=   8
         Groups(2).Columns(6).FieldLen=   256
         Groups(2).Columns(7).Width=   2937
         Groups(2).Columns(7).Visible=   0   'False
         Groups(2).Columns(7).Caption=   "USU"
         Groups(2).Columns(7).Name=   "USU"
         Groups(2).Columns(7).DataField=   "Column 18"
         Groups(2).Columns(7).DataType=   8
         Groups(2).Columns(7).FieldLen=   256
         _ExtentX        =   20902
         _ExtentY        =   8916
         _StockProps     =   79
         Caption         =   "Ofertas recibidas"
         ForeColor       =   0
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmOFEBuzon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'variable para la coleccion de proveedores
Private oProves As CProveedores

'Variables para func. combos
Private bRespetarCombo As Boolean
Private RespetarComboProve As Boolean
Private GMN1RespetarCombo As Boolean
Private GMN1CargarComboDesde As Boolean
Private bCargarComboDesde As Boolean

'Variables para materiales
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGruposMN1 As CGruposMatNivel1
Public codEqp As Variant

'Variables de seguridad
Private m_bRUsuUON As Boolean
Private m_bRPerfUON As Boolean
Private m_bRUsuDep As Boolean
Private bRMat As Boolean
Private bRAsig As Boolean
Private bREqpAsig As Boolean
Private bRCompResponsable As Boolean
Private bROfeEqp As Boolean
Private bROfeTodas As Boolean
Private bRUsuAper As Boolean
Private m_bAltaProvAsig As Boolean
Private m_bAltaProvCompAsig As Boolean
Private m_bElimOfeDeProv As Boolean
Private m_bElimOfeDeUsu As Boolean
Private m_bRestElimOfeProvUsu As Boolean
Private m_bRestElimOfeUsu As Boolean
Private m_bRestProvMatComp As Boolean
Private m_bRestProvEquComp As Boolean

'Proceso seleccionado
Public oProcesoSeleccionado As CProceso
'Proveedor seleccionado
Public oProveSeleccionado As CProveedor

' Coleccion de procesos a cargar
Private oProcesos As CProcesos
'Colecci�n de Estados de las ofertas
Private oEstados As COfeEstados
'Colecci�n de ofertas
Private oOfertas As COfertas
Private oOferta As COferta
Private oIOfertas As iOfertas
Private oOfertaSeleccionada As COferta

' Interface para obtener proveedores asignados a un proceso
Private oIasig As IAsignaciones
Public oIBaseDatos As IBaseDatos
Private Ador As Ador.Recordset

'Multilenguaje
'Oferta
Private sIdiLaOfe As String
'Proceso
Private sIdiProceso As String
'Proveedor
Private sIdiProve As String
'C�digo
Private sIdiCodigo As String
'Estado
Private sIdiEstado As String
'Material
Private sIdiMaterial As String
'Fecha Desde
Private sFecDesde As String
'Fecha Hasta
Private sFecHasta As String
'A�o
Private sIdiAnyo As String
'Variable de control de flujo de proceso
Public Accion As accionessummit
'Variable para el Timer
Private iIntervalo As Integer
Private m_vTZHora As Variant

'Permiso de consulta sobre determinado proceso (para el caso en que el
'Buz�n y la Recepci�n de ofertas tengan distintas restricciones)
Public g_bConsultProce As Boolean
Public sOrigen  As String

Private Sub CargarBuzLec()
    
    If oUsuarioSummit.Tipo <> comprador Then
        opNoLeidas.Enabled = False
        opLeidas.Enabled = False
        opTodas.Value = True
    
    Else  'Si es un usuario comprador habilita el poder filtrar por ofertas
          'leidas,no leidas y todas
        Select Case gParametrosInstalacion.giBuzonOfeLeidas
        
            Case 0:     opNoLeidas.Value = True
            
            Case 1:     opLeidas.Value = True
        
            Case 2:     opTodas.Value = True
        
        End Select
    End If

End Sub

Private Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
        
End Sub

Public Sub MarcarComoLeida()

    Dim teserror As TipoErrorSummit

    'si no es comprador o responsable no se marca
    If Not (basOptimizacion.gCodPersonaUsuario = sdbgOfertas.Columns("COMPRADOR").Value Or basOptimizacion.gCodPersonaUsuario = sdbgOfertas.Columns("RESPONSABLE").Value) Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    teserror = oOfertas.MarcarComoLeida(sdbgOfertas.Columns("ANYO").Value, CStr(sdbgOfertas.Columns("GMN1").Value), sdbgOfertas.Columns("PROCECOD").Value, CStr(sdbgOfertas.Columns("PROVECOD").Value), sdbgOfertas.Columns("NUM").Value, basOptimizacion.gCodPersonaUsuario)
    Screen.MousePointer = vbNormal

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    Else
        sdbgOfertas.Columns("L").Value = 1
        sdbgOfertas.Refresh
    End If
End Sub

Public Sub MarcarComoNoLeida()

    Dim teserror As TipoErrorSummit

    'si no es comprador o responsable no se marca
    If Not (basOptimizacion.gCodPersonaUsuario = sdbgOfertas.Columns("COMPRADOR").Value Or basOptimizacion.gCodPersonaUsuario = sdbgOfertas.Columns("RESPONSABLE").Value) Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    teserror = oOfertas.MarcarComoNoLeida(sdbgOfertas.Columns("ANYO").Value, CStr(sdbgOfertas.Columns("GMN1").Value), sdbgOfertas.Columns("PROCECOD").Value, CStr(sdbgOfertas.Columns("PROVECOD").Value), sdbgOfertas.Columns("NUM").Value, basOptimizacion.gCodPersonaUsuario)
    Screen.MousePointer = vbNormal

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    Else
        sdbgOfertas.Columns("L").Value = 0
        sdbgOfertas.Refresh
    End If
End Sub

Private Sub ProcesoSeleccionado()

    Dim sCod As String
    
    Screen.MousePointer = vbHourglass
        
    sCod = sdbcGMN1_4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod))
    sCod = CStr(sdbcAnyo) & sCod & sdbcProceCod
       
    oProcesos.CargarDatosGeneralesProceso sdbcAnyo, sdbcGMN1_4Cod, val(sdbcProceCod)
    
    Set oProcesoSeleccionado = Nothing
    
    Set oProcesoSeleccionado = oProcesos.Item(sCod)
    
    If oProcesoSeleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oIasig = oProcesoSeleccionado
  
    Screen.MousePointer = vbNormal

End Sub
Private Sub ProveedorSeleccionado()
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
'    If Not oProveSeleccionado Is Nothing Then
'        Set oIEquiposAsignados = oProveSeleccionado
'    End If
    
End Sub

Public Sub CargarProcesoConBusqueda()

    Set oProcesos = Nothing
    Set oProcesos = frmPROCEBuscar.oProceEncontrados
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    sdbcAnyo = oProcesos.Item(1).Anyo
    bRespetarCombo = True
    sdbcGMN1_4Cod = oProcesos.Item(1).GMN1Cod
    sdbcGMN1_4Cod_Validate False
    sdbcProceCod = oProcesos.Item(1).Cod
    bRespetarCombo = True
    sdbcProceDen = oProcesos.Item(1).Den
    bRespetarCombo = False
    ProcesoSeleccionado
    If Not oProcesoSeleccionado Is Nothing Then
        If oProcesoSeleccionado.Estado >= TipoEstadoProceso.sinitems And oProcesoSeleccionado.Estado <= TipoEstadoProceso.ConItemsSinValidar Then
            ProceSelector1.Seleccion = 0
        Else
            If oProcesoSeleccionado.Estado >= TipoEstadoProceso.validado And oProcesoSeleccionado.Estado <= TipoEstadoProceso.PreadjYConObjNotificados Then
                ProceSelector1.Seleccion = 1
            ElseIf oProcesoSeleccionado.Estado = TipoEstadoProceso.ParcialmenteCerrado Then
                ProceSelector1.Seleccion = 7
            Else
                ProceSelector1.Seleccion = 2
            End If
            
        End If
    End If
            
End Sub

Public Sub CargarProveedorConBusqueda()
Dim oProves As CProveedores

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    RespetarComboProve = True
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveDen.Text = oProves.Item(1).Den
    RespetarComboProve = False

End Sub

Private Sub CargarAnyos()
    Dim iAnyoActual As Integer
    Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next
    
End Sub

Private Sub CargarFechas()
    Dim miFecha As Date
    
    miFecha = DateAdd("d", -gParametrosInstalacion.giBuzonPeriodo, Date)
    txtFecDesde = miFecha
    txtFecHasta = Date

End Sub

Private Sub CargarRecursos()

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_OFEBUZON, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    caption = Ador(0).Value
    Ador.MoveNext
    lblFecDesde.caption = Ador(0).Value
    Ador.MoveNext
    lblFecHasta.caption = Ador(0).Value
    Ador.MoveNext
    opLeidas.caption = Ador(0).Value
    Ador.MoveNext
    opNoLeidas.caption = Ador(0).Value
    Ador.MoveNext
    opTodas.caption = Ador(0).Value
    Ador.MoveNext
    lblAnyo.caption = Ador(0).Value
    Ador.MoveNext
    'lblGMN1_4Cod.Caption = Ador(0).Value
    'Ador.MoveNext
    ProceSelector1.AbrevParaPendientes = Ador(0).Value
    Ador.MoveNext
    ProceSelector1.AbrevParaAbiertos = Ador(0).Value
    Ador.MoveNext
    ProceSelector1.AbrevParaCerrados = Ador(0).Value
    Ador.MoveNext
    ProceSelector1.AbrevParaTodos = Ador(0).Value
    Ador.MoveNext
    ProceSelector1.DenParaPendientes = Ador(0).Value
    Ador.MoveNext
    ProceSelector1.DenParaAbiertos = Ador(0).Value
    Ador.MoveNext
    ProceSelector1.DenParaCerrados = Ador(0).Value
    Ador.MoveNext
    ProceSelector1.DenParaTodos = Ador(0).Value
    Ador.MoveNext
    
    lblCProceCod.caption = Ador(0).Value
    Ador.MoveNext
    
    sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
    sdbcProceCod.Columns(0).caption = Ador(0).Value
    sdbcProceDen.Columns(1).caption = Ador(0).Value
    sdbcProveCod.Columns(0).caption = Ador(0).Value
    sdbcProveDen.Columns(1).caption = Ador(0).Value
    sdbcEstadoCod.Columns(0).caption = Ador(0).Value
    sdbcEstadoDen.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    
    sdbcProceCod.Columns(1).caption = Ador(0).Value
    sdbcProceDen.Columns(0).caption = Ador(0).Value
    sdbcProveCod.Columns(1).caption = Ador(0).Value
    sdbcProveDen.Columns(0).caption = Ador(0).Value
    sdbcEstadoCod.Columns(1).caption = Ador(0).Value
    sdbcEstadoDen.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    
    lblProve.caption = Ador(0).Value
    Ador.MoveNext
    lblEstado.caption = Ador(0).Value
    Ador.MoveNext

    sdbgOfertas.caption = Ador(0).Value
    Ador.MoveNext
    sdbgOfertas.Groups(0).caption = Ador(0).Value
    sIdiProve = Ador(0).Value
    Ador.MoveNext
    sdbgOfertas.Groups(1).caption = Ador(0).Value
    sIdiProceso = Ador(0).Value
    Ador.MoveNext
    sdbgOfertas.Groups(2).caption = Ador(0).Value
    sIdiLaOfe = Ador(0).Value
    Ador.MoveNext
    sIdiCodigo = Ador(0).Value
    Ador.MoveNext
    sIdiEstado = Ador(0).Value
    Ador.MoveNext
    sIdiMaterial = Ador(0).Value
    Ador.MoveNext
    sFecDesde = Ador(0).Value
    Ador.MoveNext
    sFecHasta = Ador(0).Value
    Ador.MoveNext
    ProceSelector1.AbrevParaParcialCerrados = Ador(0).Value
    Ador.MoveNext
    ProceSelector1.DenParaParcialCerrados = Ador(0).Value
    Ador.MoveNext
    sIdiAnyo = Ador(0).Value
    Ador.MoveNext
    lblTZCaption.caption = Ador(0).Value & ":"
    
    Ador.Close
    
    End If

    Set Ador = Nothing

End Sub
Private Sub ConfigurarMenuVisor()
    Dim oICompOfertaAsig As iOfertas
    
    MDI.mnuPopUpOfeMenu(1).Enabled = True
    MDI.mnuPopUpOfeMenu(2).Enabled = True
    MDI.mnuPopUpOfeMenu(3).Enabled = True
    MDI.mnuPopUpOfeMenu(4).Enabled = False
    MDI.mnuPopUpOfeMenu(5).Enabled = False
    MDI.mnuPopUpOfeMenu(6).Enabled = False
        
    If basOptimizacion.gTipoDeUsuario <> Administrador Then
        'comprueba si tiene permisos para dar de alta nuevas ofertas en recepci�n de ofertas:
        If m_bAltaProvAsig = True Then 'Solo puede crear nuevas ofertas de proveedores asignados al usuario
            If Not (sdbgOfertas.Columns("EQUIPO").Value = gCodEqpUsuario And sdbgOfertas.Columns("COMPRADOR").Value = gCodCompradorUsuario) Then
                MDI.mnuPopUpOfeMenu(3).Enabled = False
            End If
        ElseIf m_bAltaProvCompAsig = True Then  'Solo puede crear nuevas ofertas de proveedores asignados a compradores del equipo del usuario
            If Not (sdbgOfertas.Columns("EQUIPO").Value = gCodEqpUsuario) Then
                MDI.mnuPopUpOfeMenu(3).Enabled = False
            End If
        End If

        'Comprueba si tiene permisos para eliminar las ofertas en recepci�n de ofertas:
        If (m_bElimOfeDeProv = True Or m_bElimOfeDeUsu = True) Then
            If m_bRestElimOfeProvUsu = True Then 'Restringir la eliminaci�n a las ofertas de los proveedores asignados al usuario
                If Not (sdbgOfertas.Columns("EQUIPO").Value = gCodEqpUsuario And sdbgOfertas.Columns("COMPRADOR").Value = gCodCompradorUsuario) Then
                    MDI.mnuPopUpOfeMenu(2).Enabled = False
                End If
            End If
            If MDI.mnuPopUpOfeMenu(2).Enabled = True Then
                If m_bElimOfeDeProv = True Then 'Permitir eliminar ofertas introducidas por los proveedores
                    If sdbgOfertas.Columns("PORTAL").Value = 0 And Not m_bElimOfeDeUsu Then
                        MDI.mnuPopUpOfeMenu(2).Enabled = False
                    End If
                End If
                
                If MDI.mnuPopUpOfeMenu(2).Enabled = True Then
                    If m_bElimOfeDeUsu = True Then  'Permitir eliminar ofertas introducidas por los usuarios de GS
                        If sdbgOfertas.Columns("PORTAL").Value = 1 And Not m_bElimOfeDeProv = True Then
                            MDI.mnuPopUpOfeMenu(2).Enabled = False
                        Else
                            If m_bRestElimOfeUsu = True Then 'Restringir la eliminaci�n a las ofertas introducidas por el usuario
                                If sdbgOfertas.Columns("USU").Value <> oUsuarioSummit.Cod Then
                                    MDI.mnuPopUpOfeMenu(2).Enabled = False
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Else
            MDI.mnuPopUpOfeMenu(2).Enabled = False
        End If
        
        ' edu T525
        If basOptimizacion.gCodPersonaUsuario = sdbgOfertas.Columns("COMPRADOR").Value Or basOptimizacion.gCodPersonaUsuario = sdbgOfertas.Columns("RESPONSABLE").Value Then
            'Permitir marcar como leida o no leida
            If sdbgOfertas.Columns("L").Value = 1 Then
                MDI.mnuPopUpOfeMenu(4).Enabled = True
                MDI.mnuPopUpOfeMenu(6).Enabled = True
            Else
                MDI.mnuPopUpOfeMenu(4).Enabled = True
                MDI.mnuPopUpOfeMenu(5).Enabled = True
            End If
        Else
            MDI.mnuPopUpOfeMenu(4).Enabled = False
            MDI.mnuPopUpOfeMenu(5).Enabled = False
            MDI.mnuPopUpOfeMenu(6).Enabled = False
        End If

    End If


    'Si el proceso est� cerrado
    If sdbgOfertas.Columns("ESTADOID").Value > 11 Then
        MDI.mnuPopUpOfeMenu(2).Enabled = False
        MDI.mnuPopUpOfeMenu(3).Enabled = False
        MDI.mnuPopUpOfeMenu(4).Visible = False
        MDI.mnuPopUpOfeMenu(5).Visible = False
        MDI.mnuPopUpOfeMenu(6).Visible = False
    End If
    
    'si el proceso est� parcialmente cerrado
    If sdbgOfertas.Columns("ESTADOID").Value = 11 Then
        If sdbgOfertas.Columns("CERRADO").Value = 1 Then
            MDI.mnuPopUpOfeMenu(2).Enabled = False
            MDI.mnuPopUpOfeMenu(4).Visible = False
            MDI.mnuPopUpOfeMenu(5).Visible = False
            MDI.mnuPopUpOfeMenu(6).Visible = False
        End If
    End If
End Sub
Private Sub ConfigurarSeguridad()
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
                          
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestMatComprador)) Is Nothing Then
            bRMat = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestComprador)) Is Nothing Then
            bRAsig = True
        End If
            
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestResponsable)) Is Nothing Then
            bRCompResponsable = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestEquipo)) Is Nothing Then
            bREqpAsig = True
        End If
    
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEVerOfertasEquipo)) Is Nothing Then
            bROfeEqp = True
        End If
    
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEVerOfertasTodas)) Is Nothing Then
            bROfeTodas = True
        End If
                  
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEAltaProvAsig)) Is Nothing) Then
            m_bAltaProvAsig = True
        Else
            m_bAltaProvAsig = False
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEAltaProvCompAsig)) Is Nothing) Then
            m_bAltaProvCompAsig = True
        Else
            m_bAltaProvCompAsig = False
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestElimOfeProv)) Is Nothing) Then
            m_bRestElimOfeProvUsu = True
        Else
            m_bRestElimOfeProvUsu = False
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestProvMatComp)) Is Nothing Then
            m_bRestProvMatComp = True
        Else
            m_bRestProvMatComp = False
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestProvEquComp)) Is Nothing Then
            m_bRestProvEquComp = True
        Else
            m_bRestProvEquComp = False
        End If
    
    End If
    
    bRUsuAper = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestUsuAper)) Is Nothing)
    m_bRUsuUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestUsuUON)) Is Nothing)
    m_bRUsuDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestUsuDep)) Is Nothing)
    m_bRPerfUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFERestPerfUON)) Is Nothing)
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEElimOfeProv)) Is Nothing) Then
        m_bElimOfeDeProv = True
    Else
        m_bElimOfeDeProv = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEElimOfeUsu)) Is Nothing) Then
        m_bElimOfeDeUsu = True
    Else
        m_bElimOfeDeUsu = False
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestElimOfeUsu)) Is Nothing) Then
        m_bRestElimOfeUsu = True
    Else
        m_bRestElimOfeUsu = False
    End If
    
    If (basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.comprador) Then
        bROfeTodas = True
        m_bRestProvMatComp = False
        m_bRestProvEquComp = False
    End If
    
End Sub
Public Sub EliminarOferta()
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit
    
    irespuesta = oMensajes.PreguntaEliminar(" " & sIdiLaOfe & " " & sdbgOfertas.Columns("NUM").Value & " " & sIdiProceso & " " & sdbgOfertas.Columns("PROCECOD").Value & " " & sIdiProve & " " & sdbgOfertas.Columns("PROVECOD").Value)
    
    If irespuesta = vbYes Then
    
        Set oOferta = oFSGSRaiz.Generar_COferta
        
        Set oIBaseDatos = Nothing
        
        oOfertas.Add sdbgOfertas.Columns("ANYO").Value, sdbgOfertas.Columns("GMN1").Value, sdbgOfertas.Columns("PROCECOD").Value, sdbgOfertas.Columns("PROVECOD").Value, sdbgOfertas.Columns("NUM").Value
        
        oOferta.Anyo = oOfertas.Item(1).Anyo
        oOferta.GMN1Cod = oOfertas.Item(1).GMN1Cod
        oOferta.Proce = oOfertas.Item(1).Proce
        oOferta.Prove = oOfertas.Item(1).Prove
        oOferta.Num = oOfertas.Item(1).Num
        
        Set oIBaseDatos = oOferta
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.EliminarDeBaseDatos
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        Else
            basSeguridad.RegistrarAccion ACCRecOfeEli, "Anyo:" & sdbcAnyo.Value & "GMN1:" & sdbcGMN1_4Cod.Value & "Proce:" & sdbcProceCod.Value & "Prove:" & sdbcProveCod.Value & "Ofe:" & sdbgOfertas.Columns("NUM").Value
            cmdActualizar_Click
        End If
        
    End If
    
    oOfertas.Remove (1)
    Screen.MousePointer = vbNormal
    Set oIBaseDatos = Nothing
    Set oOferta = Nothing
    
End Sub

Public Sub cmdActualizar_Click()
    Dim Leidas As Integer
    Dim lIdPerfil As Long
    
    sdbgOfertas.RemoveAll
    
    If Not IsDate(txtFecDesde) Then
        oMensajes.NoValido sFecDesde
        Exit Sub
    End If
    
    If Not IsDate(txtFecHasta) Then
        oMensajes.NoValido sFecHasta
        Exit Sub
    End If

    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
    
    If basOptimizacion.gTipoDeUsuario = comprador Then
        If opLeidas Then
            Leidas = 1
        ElseIf opNoLeidas Then
            Leidas = 0
        Else
            Leidas = 2
        End If
    
        Screen.MousePointer = vbHourglass
        
        Set Ador = oGestorInformes.DevolVerOfertasRecibidasDesde(Trim(txtFecDesde), Trim(txtFecHasta), val(sdbcAnyo), Trim(sdbcGMN1_4Cod), val(sdbcProceCod), Trim(sdbcProveCod), Trim(sdbcEstadoCod), Leidas, True, bRMat, bRAsig, bREqpAsig, bRCompResponsable, bROfeEqp, bROfeTodas, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, TipoOrdenacionBuzon.OrdPorFechaRecep, ProceSelector1.Seleccion, bRUsuAper, CStr(basOptimizacion.gvarCodUsuario), m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
    Else
        Leidas = 2
        Screen.MousePointer = vbHourglass
        Set Ador = oGestorInformes.DevolVerOfertasRecibidasDesde(Trim(txtFecDesde), Trim(txtFecHasta), val(sdbcAnyo), Trim(sdbcGMN1_4Cod), val(sdbcProceCod), Trim(sdbcProveCod), Trim(sdbcEstadoCod), Leidas, True, bRMat, bRAsig, bREqpAsig, bRCompResponsable, bROfeEqp, bROfeTodas, , , TipoOrdenacionBuzon.OrdPorFechaRecep, ProceSelector1.Seleccion, bRUsuAper, CStr(basOptimizacion.gvarCodUsuario), m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
    End If
          
    CargarGrid Ador
    
    If Not Ador Is Nothing Then Ador.Close
    
    Set Ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Carga los datos de las ofertas en el grid</summary>
''' <param name="Ador">Recordset con los datos de las ofertas</param>
''' <remarks>Tiempo m�ximo: >2 seg</remarks>

Private Sub CargarGrid(ByVal Ador As Ador.Recordset)
    Dim dtFecRec As Date
    Dim dtHoraRec As Date
    Dim sFecRec As String
    
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            'Pasar la fecha de la oferta a la zona horaria del usuario
            ConvertirUTCaTZ DateValue(Ador("FECREC").Value), TimeValue(Ador("FECREC").Value), m_vTZHora, dtFecRec, dtHoraRec
            sFecRec = CStr(dtFecRec) & " " & CStr(dtHoraRec)
            
            sdbgOfertas.AddItem "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & Ador("PROVECOD").Value & Chr(m_lSeparador) & Ador("PROVEDEN").Value & Chr(m_lSeparador) & Ador("NumAdjun").Value & Chr(m_lSeparador) & Ador("LEIDO").Value & Chr(m_lSeparador) & Ador("ANYO").Value & Chr(m_lSeparador) & Ador("GMN1").Value & Chr(m_lSeparador) & Ador("PROCECOD").Value & Chr(m_lSeparador) & Ador("PROCEDEN").Value & Chr(m_lSeparador) & Ador("ESTADOPROCE").Value & Chr(m_lSeparador) & sFecRec & Chr(m_lSeparador) & Ador("OFE").Value & Chr(m_lSeparador) & Ador("EST").Value & Chr(m_lSeparador) & Ador("COM").Value & Chr(m_lSeparador) & Ador("EQP").Value & Chr(m_lSeparador) & Ador("CERRADO").Value & Chr(m_lSeparador) & Ador("PORTAL").Value & Chr(m_lSeparador) & Ador("USU").Value & Chr(m_lSeparador) & Ador("RESPONSABLE").Value
            Ador.MoveNext
        Wend
    End If
End Sub

Private Sub cmdBuscarProce_Click()
        
    frmPROCEBuscar.bRDest = False
    frmPROCEBuscar.bRUsuAper = False
    frmPROCEBuscar.m_bProveAsigComp = False
    frmPROCEBuscar.m_bProveAsigEqp = False
    
    frmPROCEBuscar.bRUsuAper = bRUsuAper
    frmPROCEBuscar.sOrigen = "frmOFEBuzon"
    frmPROCEBuscar.bRAsig = bRAsig
    frmPROCEBuscar.bRMat = bRMat
    frmPROCEBuscar.bRCompResponsable = bRCompResponsable
    frmPROCEBuscar.bRUsuDep = m_bRUsuDep
    frmPROCEBuscar.bRUsuUON = m_bRUsuUON
    frmPROCEBuscar.bREqpAsig = bREqpAsig
    frmPROCEBuscar.bRestProvMatComp = m_bRestProvMatComp
    frmPROCEBuscar.bRestProvEquComp = m_bRestProvEquComp
'    frmPROCEBuscar.m_bProveAsigComp = m_bRestProvMatComp
'    frmPROCEBuscar.m_bProveAsigEqp = m_bRestProvEquComp
    frmPROCEBuscar.sdbcAnyo = sdbcAnyo
    frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod
    frmPROCEBuscar.Show 1
End Sub

Private Sub cmdBuscarProve_Click()

    frmPROVEBuscar.bREqp = bROfeEqp

    frmPROVEBuscar.sOrigen = "frmOFEBuzon"
    frmPROVEBuscar.bREqp = bREqpAsig Or m_bRestProvEquComp
    frmPROVEBuscar.bRMat = m_bRestProvMatComp
'    frmPROVEBuscar.bRestProvEquComp = m_bRestProvEquComp

    frmPROVEBuscar.CodGMN1 = sdbcGMN1_4Cod.Text
    frmPROVEBuscar.Show 1
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub cmdColaboracion_Click()
    Dim strSessionId As String
    strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
    
    With frmEditor
        .caption = "FULLSTEP Networks"
        .g_sRuta = gParametrosGenerales.gsURLCN & "cn_NuevoMensaje_GS.aspx?sessionId=" & strSessionId & "&desdeGS=1"
        .g_sOrigen = "Colaboracion"
        .g_iAnio_ProceCompra = sdbcAnyo.Value
        .g_sGMN1_ProceCompra = sdbcGMN1_4Cod.Value
        .g_iCod_ProceCompra = sdbcProceCod.Value
        .g_bReadOnly = False
        .Show vbModal
    End With
End Sub

Private Sub cmdImprimir_Click()

    frmLstOFEBuzon.sOrigen = "frmOFEBuzon"

    frmLstOFEBuzon.WindowState = vbNormal

    frmLstOFEBuzon.txtFecDesde = txtFecDesde
    frmLstOFEBuzon.txtFecHasta = txtFecHasta
    
    If Not oProcesoSeleccionado Is Nothing Then
        frmLstOFEBuzon.sdbcAnyo.Text = oProcesoSeleccionado.Anyo
        frmLstOFEBuzon.sdbcGMN1_4Cod.Text = oProcesoSeleccionado.GMN1Cod
        frmLstOFEBuzon.sdbcGMN1_4Cod_Validate False
        frmLstOFEBuzon.txtProceCod.Text = CStr(oProcesoSeleccionado.Cod)
        frmLstOFEBuzon.ProceSelector1.Seleccion = ProceSelector1.Seleccion
    Else
        frmLstOFEBuzon.sdbcAnyo.Text = sdbcAnyo
        frmLstOFEBuzon.sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod
        frmLstOFEBuzon.sdbcGMN1_4Cod_Validate False
        frmLstOFEBuzon.ProceSelector1.Seleccion = ProceSelector1.Seleccion
    End If

    If Not oProveSeleccionado Is Nothing Then
        frmLstOFEBuzon.sdbcProveCod.Text = oProveSeleccionado.Cod
        frmLstOFEBuzon.RespetarComboProve = True
        frmLstOFEBuzon.sdbcProveDen.Text = oProveSeleccionado.Den
        frmLstOFEBuzon.ProceSelector1.Seleccion = ProceSelector1.Seleccion
    End If
    
    frmLstOFEBuzon.sdbcEstadoCod.Text = sdbcEstadoCod.Value
    frmLstOFEBuzon.bRespetarCombo = True
    frmLstOFEBuzon.sdbcEstadoDen.Text = sdbcEstadoDen.Value
    
    frmLstOFEBuzon.Show vbModal

End Sub


Private Sub Form_Activate()
    'Es posible que la zona horaria haya cambiado en otra pantalla, actualizarla
    If oUsuarioSummit.TimeZone <> m_vTZHora Then
        m_vTZHora = oUsuarioSummit.TimeZone
        SetTZValue
        
        cmdActualizar_Click
    End If
End Sub

Private Sub Form_Load()

    Me.Width = 10500
    Me.Height = 6885
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    PonerFieldSeparator Me
    
    'Si hay restricci�n de equipo configuro el interfaz adecuadamente
    If bREqpAsig Then
        codEqp = basOptimizacion.gCodEqpUsuario
    End If

    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarAnyos
    
    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    ProceSelector1.Seleccion = 3
    CargarFechas
        
    CargarBuzLec
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    Set oProcesos = oFSGSRaiz.Generar_CProcesos
    
    Set oEstados = oFSGSRaiz.Generar_COfeEstados
    
    Set oOfertas = oFSGSRaiz.Generar_COfertas
    
    'edu T525 Nueva columna RESPONSABLE
    Dim n As Integer
    n = sdbgOfertas.Columns.Count
    sdbgOfertas.Columns.Add n
    sdbgOfertas.Columns(n).caption = "Responsable"
    sdbgOfertas.Columns(n).Name = "RESPONSABLE"
    sdbgOfertas.Columns(n).Position = n
    sdbgOfertas.Columns(n).Visible = False
    'fin de nueva columna
    
    SetFormTZ
    SetTZValue
        
    If sOrigen <> "Visor" Then
        cmdActualizar_Click
    End If
    
    iIntervalo = 0
    Timer1.Enabled = True
    
    cmdColaboracion.Visible = (gParametrosGenerales.gsAccesoFSCN = TipoAccesoFSCN.AccesoFSCN)
End Sub

''' <summary>Establece el valor de la label de zona horaria</summary>
''' <remarks>Llamada desde: Form_Activate, Form_Load</remarks>

Private Sub SetTZValue()
    Dim dcListaZonasHorarias As Dictionary
    
    Set dcListaZonasHorarias = ObtenerZonasHorarias
    
    lblTZ.caption = Mid(dcListaZonasHorarias.Item(m_vTZHora), 2, 9)
    
    Set dcListaZonasHorarias = Nothing
End Sub

''' <summary>Asigna la zona horaria por defecto que se usar� para las horas de subasta</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub SetFormTZ()
    If Not IsNull(oUsuarioSummit.TimeZone) And Not IsEmpty(oUsuarioSummit.TimeZone) Then
        m_vTZHora = oUsuarioSummit.TimeZone
    Else
        'Asignar la zona horaria del equipo
        m_vTZHora = GetTimeZone.key
    End If
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>Realiza el ajuste del tama�o del formulario</summary>
''' <remarks>Llamada desde: Form_Resize</remarks>
''' <revision>LTG 28/09/2011</revision>

Private Sub Arrange()
    Dim vbm As Variant
    
    On Error Resume Next
    
    LockWindowUpdate Me.hWnd

    If Me.Width >= 12135 Or Me.Height > 6885 Then
        Picture2.Width = Me.Width - 230
        If Me.Height > 1700 Then
            Picture2.Height = Me.Height - 1700
        End If
    Else
        Me.Width = 12135
        Picture2.Width = 11925
        Picture2.Height = 5115
    End If
    
    Picture1.Width = Me.Width - 255
        
    sdbgOfertas.Width = Me.Width - 265
    
    If Me.Height >= 2500 Then
        sdbgOfertas.Height = Me.Height - 1830
    End If
    'Si estas situado en una fila que no sea de las primeras falla el resize del group
    'para evitarlo guardo el bookmark de la fila actual y pongo la fila 0
    If sdbgOfertas.Rows > 0 Then
        vbm = sdbgOfertas.Bookmark
        sdbgOfertas.Bookmark = 0
    End If
    sdbgOfertas.Groups(0).Width = sdbgOfertas.Width * 0.35
    sdbgOfertas.Columns("LEIDA").Width = sdbgOfertas.Groups(0).Width * 0.1
    sdbgOfertas.Columns("ADJUNTOS").Width = sdbgOfertas.Groups(0).Width * 0.1
    sdbgOfertas.Columns("PROVECOD").Width = sdbgOfertas.Groups(0).Width * 0.25
    sdbgOfertas.Columns("PROVEDEN").Width = sdbgOfertas.Groups(0).Width * 0.55
    sdbgOfertas.Groups(1).Width = sdbgOfertas.Width * 0.4
    sdbgOfertas.Columns("ANYO").Width = sdbgOfertas.Groups(1).Width * 0.12
    sdbgOfertas.Columns("GMN1").Width = sdbgOfertas.Groups(1).Width * 0.1
    sdbgOfertas.Columns("PROCECOD").Width = sdbgOfertas.Groups(1).Width * 0.15
    sdbgOfertas.Columns("PROCEDEN").Width = sdbgOfertas.Groups(1).Width * 0.63
    sdbgOfertas.Groups(2).Width = sdbgOfertas.Width * 0.25
    sdbgOfertas.Columns("FECHA").Width = sdbgOfertas.Groups(2).Width * 0.6
    sdbgOfertas.Columns("NUM").Width = sdbgOfertas.Groups(2).Width * 0.1
    sdbgOfertas.Columns("ESTADOOFE").Width = sdbgOfertas.Groups(2).Width * 0.35

    If sdbgOfertas.Rows > 0 Then
        'Recup�ro el bookmark y si no se ve la fila la pongo la priemra visible
        sdbgOfertas.Bookmark = vbm
        If sdbgOfertas.AddItemRowIndex(sdbgOfertas.Bookmark) < sdbgOfertas.AddItemRowIndex(sdbgOfertas.FirstRow) Or sdbgOfertas.AddItemRowIndex(sdbgOfertas.Bookmark) > sdbgOfertas.AddItemRowIndex(sdbgOfertas.FirstRow) + sdbgOfertas.VisibleRows - 1 Then
            sdbgOfertas.FirstRow = sdbgOfertas.Bookmark
        End If
    End If
LockWindowUpdate 0&
End Sub


Private Sub Form_Unload(Cancel As Integer)
    sOrigen = ""
    Me.Visible = False
    frmEST.cmdRestaurar_Click
End Sub

Private Sub opLeidas_Click()
    sdbgOfertas.RemoveAll
End Sub

Private Sub opNoLeidas_Click()
    sdbgOfertas.RemoveAll
End Sub

Private Sub opTodas_Click()
    sdbgOfertas.RemoveAll
End Sub

Private Sub ProceSelector1_Click(ByVal Opcion As SelectorDeProcesos.PSSeleccion)
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcAnyo_Change()
    sdbgOfertas.RemoveAll
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcGMN1_4Cod = ""
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        sdbcProveCod.Text = ""
        sdbcProveCod.Columns(0).Value = ""
        sdbcProveCod.Columns(1).Value = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen.Text = ""
        sdbcEstadoCod.Text = ""
        sdbcEstadoDen.Text = ""
        bRespetarCombo = False
    End If
End Sub

Private Sub sdbcAnyo_Click()
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcEstadoCod_Change()
    
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcEstadoDen = ""
        bRespetarCombo = False
    End If
    
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcEstadoCod_Click()
    If Not sdbcEstadoCod.DroppedDown Then
        sdbcEstadoCod = ""
    End If
    
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcEstadoCod_CloseUp()
    If sdbcEstadoCod.Value = "..." Or sdbcEstadoCod = "" Then
        sdbcEstadoCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcEstadoDen.Text = sdbcEstadoCod.Columns(1).Text
    sdbcEstadoCod.Text = sdbcEstadoCod.Columns(0).Text
    bRespetarCombo = False

End Sub

Private Sub sdbcEstadoCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
    sdbcEstadoCod.RemoveAll
        
    Screen.MousePointer = vbHourglass
    
'    Set oEstadoSeleccionado = Nothing
    
    oEstados.CargarTodosLosOfeEstados , , , , , , , basPublic.gParametrosInstalacion.gIdioma
    
    Codigos = oEstados.DevolverLosCodigos
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcEstadoCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
    
    sdbcEstadoCod.SelStart = 0
    sdbcEstadoCod.SelLength = Len(sdbcEstadoCod.Text)
    sdbcEstadoCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEstadoCod_InitColumnProps()
    sdbcEstadoCod.DataFieldList = "Column 0"
    sdbcEstadoCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcEstadoCod_PositionList(ByVal Text As String)
PositionList sdbcEstadoCod, Text
End Sub

Private Sub sdbcEstadoCod_Validate(Cancel As Boolean)
    
Dim Codigos As TipoDatosCombo

If Trim(sdbcEstadoCod.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcEstadoCod.Value)) = UCase(Trim(sdbcEstadoCod.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcEstadoDen = sdbcEstadoCod.Columns(1).Value
        bRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    oEstados.CargarTodosLosOfeEstados sdbcEstadoCod.Text, , True, , , , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    If oEstados.Item(1) Is Nothing Then
        oMensajes.NoValido sIdiEstado
        sdbcEstadoCod = ""
        Exit Sub
    Else
        If UCase(oEstados.Item(1).Cod) <> UCase(sdbcEstadoCod.Value) Then
            oMensajes.NoValido sIdiEstado
            sdbcEstadoCod = ""
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcEstadoDen = oEstados.Item(1).Den
            sdbcEstadoCod.Columns(0).Value = sdbcEstadoCod.Text
            sdbcEstadoCod.Columns(1).Value = sdbcEstadoDen.Text
            bRespetarCombo = False
        End If
    End If

End Sub


Private Sub sdbcEstadoDen_Change()
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcEstadoCod = ""
        bRespetarCombo = False
    End If
    
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcEstadoDen_Click()
    If Not sdbcEstadoDen.DroppedDown Then
        sdbcEstadoDen = ""
    End If
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcEstadoDen_CloseUp()
    If sdbcEstadoDen.Value = "..." Or sdbcEstadoDen = "" Then
        sdbcEstadoDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcEstadoCod.Text = sdbcEstadoDen.Columns(1).Text
    sdbcEstadoDen.Text = sdbcEstadoDen.Columns(0).Text
    bRespetarCombo = False

End Sub

Private Sub sdbcEstadoDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
    sdbcEstadoDen.RemoveAll
        
    Screen.MousePointer = vbHourglass
    
''    Set oEstadoSeleccionado = Nothing
    
    oEstados.CargarTodosLosOfeEstados , , , True, , , , basPublic.gParametrosInstalacion.gIdioma
    
    Codigos = oEstados.DevolverLosCodigos
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcEstadoDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
    
    sdbcEstadoDen.SelStart = 0
    sdbcEstadoDen.SelLength = Len(sdbcEstadoCod.Text)
    sdbcEstadoDen.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEstadoDen_InitColumnProps()
    sdbcEstadoDen.DataFieldList = "Column 0"
    sdbcEstadoDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcEstadoDen_PositionList(ByVal Text As String)
PositionList sdbcEstadoDen, Text
End Sub

Private Sub sdbcEstadoDen_Validate(Cancel As Boolean)

Dim Codigos As TipoDatosCombo

    If Trim(sdbcEstadoDen.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcEstadoDen.Value)) = UCase(Trim(sdbcEstadoDen.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcEstadoCod = sdbcEstadoDen.Columns(1).Value
        bRespetarCombo = False
        Exit Sub
    End If
    
    If UCase(Trim(sdbcEstadoDen.Value)) = UCase(Trim(sdbcEstadoCod.Columns(1).Value)) Then
        bRespetarCombo = True
        sdbcEstadoCod = sdbcEstadoCod.Columns(0).Value
        bRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    oEstados.CargarTodosLosOfeEstados , sdbcEstadoDen.Text, True, , , , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    If oEstados.Item(1) Is Nothing Then
        oMensajes.NoValido sIdiEstado
        sdbcEstadoDen = ""
        Exit Sub
    Else
        If UCase(oEstados.Item(1).Den) <> UCase(sdbcEstadoDen.Value) Then
            oMensajes.NoValido sIdiEstado
            sdbcEstadoDen = ""
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcEstadoCod = oEstados.Item(1).Cod
            sdbcEstadoDen.Columns(0).Value = sdbcEstadoDen.Text
            sdbcEstadoDen.Columns(1).Value = sdbcEstadoCod.Text
            bRespetarCombo = False
        End If
    End If

End Sub

Private Sub sdbcGMN1_4Cod_Change()
    sdbgOfertas.RemoveAll
    
    If Not GMN1RespetarCombo Then
        GMN1CargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oProcesoSeleccionado = Nothing
        Accion = ACCProceCon
        bRespetarCombo = True
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        sdbcProveCod.Text = ""
        sdbcProveCod.Columns(0).Value = ""
        sdbcProveCod.Columns(1).Value = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen.Text = ""
        sdbcEstadoCod.Text = ""
        sdbcEstadoDen.Text = ""
        bRespetarCombo = False
    End If

End Sub

Private Sub sdbcGMN1_4Cod_Click()
    If Not sdbcGMN1_4Cod.DroppedDown Then
        sdbcGMN1_4Cod = ""
    End If
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    sdbcProceDen.Text = ""
    sdbcProceDen.Columns(0).Value = ""
    sdbcProceDen.Columns(1).Value = ""
    sdbcProceDen.RemoveAll
    sdbcProveCod.Text = ""
    sdbcProveCod.Columns(0).Value = ""
    sdbcProveCod.Columns(1).Value = ""
    sdbcProveCod.RemoveAll
    sdbcProveDen.Text = ""
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    Set oProcesoSeleccionado = Nothing

End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If GMN1CargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
             oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
        
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN1 = Nothing
        Screen.MousePointer = vbHourglass
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiMaterial
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
        
            bCargarComboDesde = False
            
        End If
        Screen.MousePointer = vbNormal
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
 
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiMaterial
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
           
            bCargarComboDesde = False
            
        End If
    
    End If
        
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing

End Sub

Private Sub sdbcProceCod_Change()

    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        sdbcProveCod.Text = ""
        sdbcProveCod.Columns(0).Value = ""
        sdbcProveCod.Columns(1).Value = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen.Text = ""
        sdbcEstadoCod.Text = ""
        sdbcEstadoDen.Text = ""
        Set oProcesoSeleccionado = Nothing
        bRespetarCombo = False
    End If
    
    sdbgOfertas.RemoveAll
    
    If sdbcProceCod.Text <> "" Then
        cmdBuscarProve.Enabled = False
    Else
        cmdBuscarProve.Enabled = True
    End If

    If sdbcProceCod.Value = "" Then
        cmdColaboracion.Enabled = False
    Else
        cmdColaboracion.Enabled = True
    End If
End Sub

Private Sub sdbcProceCod_Click()
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
    End If

    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcProceCod_CloseUp()
    If sdbcProceCod.Value = "..." Or sdbcProceCod = "" Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    sdbcProveCod.Text = ""
    sdbcProveCod.Columns(0).Value = ""
    sdbcProveCod.Columns(1).Value = ""
    sdbcProveCod.RemoveAll
    sdbcProveDen.Text = ""
    bRespetarCombo = False
    
    ProcesoSeleccionado
    
    If sdbcProceCod <> "" Then
        cmdBuscarProve.Enabled = False
    Else
        cmdBuscarProve.Enabled = True
    End If

End Sub

Private Sub sdbcProceCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim i As Integer
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim lIdPerfil As Long
    
    sdbcProceCod.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    If sdbcAnyo.Value = "" Then
        oMensajes.FaltanDatos sIdiAnyo
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Set oProcesoSeleccionado = Nothing
    
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.conofertas
        Case PSSeleccion.PSAbiertos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = TipoEstadoProceso.ParcialmenteCerrado
            udtEstHasta = TipoEstadoProceso.ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.Cerrado
    End Select
    
    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod.Value), , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRPerfUON, lIdPerfil
    End If
    
    Codigos = oProcesos.DevolverLosCodigos(True)
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProceCod_InitColumnProps()
    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProceCod_PositionList(ByVal Text As String)
PositionList sdbcProceCod, Text
End Sub


Private Sub sdbcProceDen_Change()

    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        sdbcProveCod.Text = ""
        sdbcProveCod.Columns(0).Value = ""
        sdbcProveCod.Columns(1).Value = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen.Text = ""
        sdbcEstadoCod.Text = ""
        sdbcEstadoDen.Text = ""
        Set oProcesoSeleccionado = Nothing
        bRespetarCombo = False
    End If
    
    sdbgOfertas.RemoveAll
    
    If sdbcProceCod.Value = "" Then
        cmdColaboracion.Enabled = False
    Else
        cmdColaboracion.Enabled = True
    End If
End Sub

Private Sub sdbcProceDen_Click()
    
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
    End If
    
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcProceDen_CloseUp()
    If sdbcProceDen.Value = "....." Or sdbcProceDen = "" Then
        sdbcProceDen.Text = ""
        sdbcProceDen.Columns(0).Value = ""
        sdbcProceDen.Columns(1).Value = ""
        sdbcProceDen.RemoveAll
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    sdbcProveCod.Text = ""
    sdbcProveCod.Columns(0).Value = ""
    sdbcProveCod.Columns(1).Value = ""
    sdbcProveCod.RemoveAll
    sdbcProveDen.Text = ""
    bRespetarCombo = False
    
    ProcesoSeleccionado

End Sub


Private Sub sdbcProceDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim oIProveAsig As ICompProveAsignados
    Dim i As Integer
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim lIdPerfil As Long
    
    sdbcProceDen.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    If sdbcAnyo.Value = "" Then
        oMensajes.FaltanDatos sIdiAnyo
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Set oProcesoSeleccionado = Nothing

    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.conofertas
        Case PSSeleccion.PSAbiertos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = TipoEstadoProceso.ParcialmenteCerrado
            udtEstHasta = TipoEstadoProceso.ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.Cerrado
    End Select
    
    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , sdbcProceDen, False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRPerfUON, lIdPerfil
    End If
    
    Codigos = oProcesos.DevolverLosCodigos(True)
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
        
    If Not oProcesos.EOF Then
        sdbcProceDen.AddItem "....."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProceDen_InitColumnProps()
    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"

End Sub


Private Sub sdbcProceDen_PositionList(ByVal Text As String)
PositionList sdbcProceDen, Text
End Sub

Private Sub sdbcProveCod_Change()
    
    If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveDen = ""
        sdbcEstadoCod = ""
        sdbcEstadoDen = ""
        Set oProveSeleccionado = Nothing
        RespetarComboProve = False
   End If
   sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcProveCod_Click()
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod.Text = ""
        sdbcProveCod.Columns(0).Value = ""
        sdbcProveCod.Columns(1).Value = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen = ""
    End If
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        sdbcProveCod.Columns(0).Value = ""
        sdbcProveCod.Columns(1).Value = ""
        sdbcProveCod.RemoveAll
        Exit Sub
    End If
      
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sDesde As String
        
        
    sdbcProveCod.RemoveAll
     
    Screen.MousePointer = vbHourglass
    
    If sdbcProceCod.Text = "" Then
        oProves.BuscarProveedoresOFEBuzon gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text, , , , , , , , False, , , , , , , , , , , bROfeTodas, bROfeEqp, m_bRestProvMatComp, m_bRestProvEquComp
    Else
        ProcesoSeleccionado
        Set oProves = oIasig.DevolverProveedoresOFEBuzon(sdbcProveCod.Text, "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , , , , bROfeTodas, bROfeEqp, m_bRestProvMatComp, m_bRestProvEquComp)
    End If

'    If m_bRestProvMatComp Or m_bRestProvEquComp Then
'        If sdbcProceCod.Text = "" Then
'            If sdbcProveCod.Text <> "" Then
'                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text, , , , , , , , False
''                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveCod.Text, , , , , , , , , , , , False, , , , , , , , , , , , m_bRestProvMatComp, m_bRestProvEquComp, oUsuarioSummit.Comprador.codEqp, oUsuarioSummit.Comprador.Cod
'            Else
'                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , , , , False
''                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , False, , , , , , , , , , , , m_bRestProvMatComp, m_bRestProvEquComp, oUsuarioSummit.Comprador.codEqp, oUsuarioSummit.Comprador.Cod
'            End If
'        Else
'            ProcesoSeleccionado
'            If ((Not bROfeEqp) And (Not bROfeTodas)) Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'                End If
'            ElseIf (bROfeEqp) Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
'                End If
'            ElseIf (bROfeTodas) Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve)
'                End If
'            End If
'        End If
'    Else
'        If sdbcProceCod.Text = "" And Not m_bRestProvMatComp And Not m_bRestProvEquComp Then
'            If sdbcProveCod.Text <> "" Then
'                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveCod.Text, , , , , , , , , , , , False
'            Else
'                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , False
'            End If
'        Else
'            If ((Not bROfeEqp) And (Not bROfeTodas)) Or m_bRestProvMatComp Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'                End If
'            ElseIf (bROfeEqp) Or m_bRestProvEquComp Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
'                End If
'            ElseIf (bROfeTodas) Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve)
'                End If
'            End If
'        End If
'    End If
    
    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next
    
    If ((sdbcProceCod.Text = "") And (Not oProves.EOF)) Then
        sdbcProveCod.AddItem "..."
    End If
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub

Private Sub sdbcProceCod_Validate(Cancel As Boolean)
    Dim udtEstDesde As TipoEstadoProceso
    Dim udtEstHasta As TipoEstadoProceso
    Dim lIdPerfil As Long
    
    If sdbcProceCod.Text = "" Then Exit Sub
    
    If sdbcAnyo.Value = "" Then
        sdbcProceCod.Value = ""
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        oMensajes.FaltanDatos sIdiAnyo
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProceso
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If Not IsNumeric(sdbcProceCod) Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiCodigo
        Set oProcesoSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceDen.Columns(1).Text Then
        bRespetarCombo = True
        sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
        bRespetarCombo = False
        If oProcesoSeleccionado Is Nothing Then
            ProcesoSeleccionado
        End If
        Exit Sub
    End If
    
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.conofertas
        Case PSSeleccion.PSAbiertos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstDesde = TipoEstadoProceso.ParcialmenteCerrado
            udtEstHasta = TipoEstadoProceso.ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstDesde = TipoEstadoProceso.conadjudicaciones
            udtEstHasta = TipoEstadoProceso.Cerrado
        Case PSSeleccion.PSTodos
            udtEstDesde = TipoEstadoProceso.conasignacionvalida
            udtEstHasta = TipoEstadoProceso.Cerrado
    End Select
    
    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
    
    oProcesos.CargarTodosLosProcesosDesde 1, udtEstDesde, udtEstHasta, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod.Value), , True, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, bRAsig, bRCompResponsable, bREqpAsig, bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , , , , m_bRPerfUON, lIdPerfil
    
    If oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Value = ""
        sdbcProceCod.Columns(1).Value = ""
        sdbcProceCod.RemoveAll
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProceso
        Set oProcesoSeleccionado = Nothing
    Else
        bRespetarCombo = True
        sdbcProceDen.Text = oProcesos.Item(1).Den
        sdbcProceCod.Columns(0).Text = sdbcProceCod.Text
        sdbcProceCod.Columns(1).Text = sdbcProceDen.Text
        bRespetarCombo = False
        Set oProcesoSeleccionado = Nothing
        Set oProcesoSeleccionado = oProcesos.Item(1)
        ProcesoSeleccionado
    End If

End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    If sdbcProveCod.Text = sdbcProveCod.Columns(0).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
        RespetarComboProve = False
        Exit Sub
    End If
    
    If sdbcProveCod.Text = sdbcProveDen.Columns(1).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
        RespetarComboProve = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proveedor
    Screen.MousePointer = vbHourglass
    
    If sdbcProceCod.Text = "" Then
        oProves.BuscarProveedoresOFEBuzon gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text, , , , , , , , False, , , , , , , , , , , bROfeTodas, bROfeEqp, m_bRestProvMatComp, m_bRestProvEquComp
    Else
        ProcesoSeleccionado
        Set oProves = oIasig.DevolverProveedoresOFEBuzon(sdbcProveCod.Text, "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , , , , bROfeTodas, bROfeEqp, m_bRestProvMatComp, m_bRestProvEquComp)
    End If
    
'    If sdbcProceCod.Text = "" And Not m_bRestProvMatComp And Not m_bRestProvEquComp Then
'        oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
'    Else
'        If ((Not bROfeEqp) And (Not bROfeTodas)) Or m_bRestProvMatComp Then
'            Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'        ElseIf (bROfeEqp) Or m_bRestProvEquComp Then
'            Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
'        ElseIf (bROfeTodas) Then
'            Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveCod.Text, "", False, OrdAsigPorCodProve)
'        End If
'    End If
    
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
        sdbcProveCod.Columns(0).Value = ""
        sdbcProveCod.Columns(1).Value = ""
        sdbcProveCod.RemoveAll
    Else
        RespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Text = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Text = sdbcProveDen.Text
            
        RespetarComboProve = False
        
        Screen.MousePointer = vbHourglass
        ProveedorSeleccionado
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub sdbcProveDen_Change()

    If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveCod.Text = ""
        sdbcProveCod.Columns(0).Value = ""
        sdbcProveCod.Columns(1).Value = ""
        sdbcProveCod.RemoveAll
        sdbcEstadoCod.Text = ""
        sdbcEstadoDen.Text = ""
        RespetarComboProve = False
        Set oProveSeleccionado = Nothing
    End If
    
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcProveDen_Click()
      If Not sdbcProveDen.DroppedDown Then
        sdbcProveDen.Text = ""
        sdbcProveCod.Text = ""
        sdbcProveCod.Columns(0).Value = ""
        sdbcProveCod.Columns(1).Value = ""
        sdbcProveCod.RemoveAll
    End If
  
    sdbgOfertas.RemoveAll
End Sub

Private Sub sdbcProveDen_CloseUp()
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sDesde As String
   
    sdbcProveDen.RemoveAll
        
    Screen.MousePointer = vbHourglass
    
    If sdbcProceCod.Text = "" Then
        oProves.BuscarProveedoresOFEBuzon gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text, , , , , , , , False, , , , , , , , , , , bROfeTodas, bROfeEqp, m_bRestProvMatComp, m_bRestProvEquComp
    Else
        ProcesoSeleccionado
        Set oProves = oIasig.DevolverProveedoresOFEBuzon(sdbcProveCod.Text, "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , , , , bROfeTodas, bROfeEqp, m_bRestProvMatComp, m_bRestProvEquComp)
    End If
    
'    If m_bRestProvMatComp Or m_bRestProvEquComp Then
'        If sdbcProceCod.Text = "" Then
'            If sdbcProveCod.Text <> "" Then
'                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , sdbcProveDen.Text, , , , , , , False
''                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveCod.Text, , , , , , , , , , , , False, , , , , , , , , , , , m_bRestProvMatComp, m_bRestProvEquComp, oUsuarioSummit.Comprador.codEqp, oUsuarioSummit.Comprador.Cod
'            Else
'                oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , , , , False
''                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , False, , , , , , , , , , , , m_bRestProvMatComp, m_bRestProvEquComp, oUsuarioSummit.Comprador.codEqp, oUsuarioSummit.Comprador.Cod
'            End If
'        Else
'            If ((Not bROfeEqp) And (Not bROfeTodas)) Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde("", sdbcProveDen.Text, False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'                End If
'            ElseIf (bROfeEqp) Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde("", sdbcProveDen.Text, False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
'                End If
'            ElseIf (bROfeTodas) Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde("", sdbcProveDen.Text, False, OrdAsigPorCodProve)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve)
'                End If
'            End If
'        End If
'    Else
'        If sdbcProceCod.Text = "" And Not m_bRestProvMatComp And Not m_bRestProvEquComp Then
'            If sdbcProveCod.Text <> "" Then
'                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , sdbcProveDen.Text, , , , , , , , , , , False
'            Else
'                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , False
'            End If
'        Else
'            If ((Not bROfeEqp) And (Not bROfeTodas)) Or m_bRestProvMatComp Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde("", sdbcProveDen.Text, False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'                End If
'            ElseIf (bROfeEqp) Or m_bRestProvEquComp Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde("", sdbcProveDen.Text, False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve, basOptimizacion.gCodEqpUsuario)
'                End If
'            ElseIf (bROfeTodas) Then
'                If sdbcProveCod.Text <> "" Then
'                    Set oProves = oIasig.DevolverProveedoresDesde("", sdbcProveDen.Text, False, OrdAsigPorCodProve)
'                Else
'                    Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorCodProve)
'                End If
'            End If
'        End If
'    End If
        
'    If sdbcProceDen.Text = "" And Not m_bRestProvMatComp And Not m_bRestProvEquComp Then
'        If m_bRestProvMatComp Or m_bRestProvEquComp Then
'            If sdbcProveDen.Text <> "" Then
'                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , sdbcProveDen.Text, , , , , , , , , , , False
'            Else
'                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , False
'            End If
'        Else
'            If sdbcProveDen.Text <> "" Then
'                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , sdbcProveDen.Text, , , , , , , , , , , False
'            Else
'                oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , False
'            End If
'        End If
'    Else
'        If ((Not bROfeEqp) And (Not bROfeTodas)) Or m_bRestProvMatComp Then
'            If sdbcProveDen.Text <> "" Then
'                Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveDen.Text, "", False, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'            Else
'                Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
'            End If
'        ElseIf (bROfeEqp) Or m_bRestProvEquComp Then
'            If sdbcProveDen.Text <> "" Then
'                Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveDen.Text, "", False, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
'            Else
'                Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorDenProve, basOptimizacion.gCodEqpUsuario)
'            End If
'        ElseIf (bROfeTodas) Then
'            If sdbcProveDen.Text <> "" Then
'                Set oProves = oIasig.DevolverProveedoresDesde(sdbcProveDen.Text, "", False, OrdAsigPorDenProve)
'            Else
'                Set oProves = oIasig.DevolverProveedoresDesde("", "", False, OrdAsigPorDenProve)
'            End If
'        End If
'    End If

    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    If ((sdbcProceCod.Text = "") And (Not oProves.EOF)) Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveDen.Refresh

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Public Sub sdbgOfertas_DblClick()
    Dim teserror As TipoErrorSummit
    
    If sdbgOfertas.Rows = 0 Then Exit Sub
    
    ' edu 525
    ' da igual si es comprador o no, se marca al usuario
    
    If sdbgOfertas.Columns("L").Value = 0 Then ' no est� leida

        MarcarComoLeida

    End If

    IrARecepcionOfertas

    Me.cmdActualizar_Click
    
End Sub

Public Sub IrARecepcionOfertas(Optional bAnyadir As Boolean)
    
    Screen.MousePointer = vbHourglass
    Unload frmOFERec
    Select Case sdbgOfertas.Columns("ESTADOID").Value
        Case 6, 7
            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSPendientes
        Case 12, 13, 20
            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSCerrados
        Case 11
            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSParcialCerrados
        Case Else
            frmOFERec.ProceSelector1.Seleccion = PSSeleccion.PSAbiertos
    End Select

    g_bConsultProce = True
    frmOFERec.bOrigenBuzon = True
    frmOFERec.sdbcAnyo.Text = sdbgOfertas.Columns("ANYO").Value
    frmOFERec.sdbcGMN1_4Cod.Text = sdbgOfertas.Columns("GMN1").Value
    frmOFERec.sdbcGMN1_4Cod_Validate False
    frmOFERec.sdbcProceCod.Text = sdbgOfertas.Columns("PROCECOD").Value
    frmOFERec.sdbcProceCod_Validate False
    If Not g_bConsultProce Then
        Unload frmOFERec
        Exit Sub
    End If
    
    If bAnyadir Then
        'Se coloca en el proveedor y se pulsa a�adir
        frmOFERec.tvwProce.Nodes("P_" & sdbgOfertas.Columns("PROVECOD").Value).Selected = True
        frmOFERec.tvwProce.selectedItem.Expanded = True
        frmOFERec.tvwProce_NodeClick frmOFERec.tvwProce.selectedItem
        frmOFERec.cmdA�adir_Click
    Else
        If Not frmOFERec.m_oOfertasProceso Is Nothing Then
            If frmOFERec.m_oOfertasProceso.Item(sdbgOfertas.Columns("PROVECOD").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sdbgOfertas.Columns("PROVECOD").Value)) & CStr(sdbgOfertas.Columns("NUM").Value)) Is Nothing Then
                oMensajes.DatoEliminado sIdiLaOfe & " " & CStr(sdbgOfertas.Columns("NUM").Value)
                frmOFERec.tvwProce.Nodes("P_" & sdbgOfertas.Columns("PROVECOD").Value).Selected = True
            Else
                frmOFERec.tvwProce.Nodes("O_" & sdbgOfertas.Columns("PROVECOD").Value & "_" & sdbgOfertas.Columns("NUM").Value).Selected = True
            End If
        Else
            frmOFERec.tvwProce.Nodes("P_" & sdbgOfertas.Columns("PROVECOD").Value).Selected = True
        End If
    
        frmOFERec.tvwProce.selectedItem.Expanded = True
        frmOFERec.tvwProce_NodeClick frmOFERec.tvwProce.selectedItem
    End If
    frmOFERec.bCargarComboDesde = False
    frmOFERec.bOrigenBuzon = False
    
    'Muestra la pantalla de recepci�n de ofertas
    Screen.MousePointer = vbNormal
    
    frmOFERec.Show

End Sub

Private Sub sdbgOfertas_GrpHeadClick(ByVal GrpIndex As Integer)
    Dim Ador As Ador.Recordset
    Dim Leidas As Integer
    Dim lIdPerfil As Long
    
    If sdbgOfertas.Rows = 0 Then Exit Sub
    
    sdbgOfertas.RemoveAll
    
    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
    
    If basOptimizacion.gTipoDeUsuario = comprador Then
        If opLeidas Then
            Leidas = 1
        ElseIf opNoLeidas Then
            Leidas = 0
        Else
            Leidas = 2
        End If
    
        Screen.MousePointer = vbHourglass
        
        Select Case GrpIndex
            Case 0 'Por codigo de proveedor
                Set Ador = oGestorInformes.DevolVerOfertasRecibidasDesde(Trim(txtFecDesde), Trim(txtFecHasta), val(sdbcAnyo), Trim(sdbcGMN1_4Cod), val(sdbcProceCod), Trim(sdbcProveCod), Trim(sdbcEstadoCod), Leidas, True, bRMat, bRAsig, bREqpAsig, bRCompResponsable, bROfeEqp, bROfeTodas, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, TipoOrdenacionBuzon.OrdPorProve, ProceSelector1.Seleccion, bRUsuAper, CStr(basOptimizacion.gvarCodUsuario), m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
           
            Case 1 'Por c�digo de proceso
                Set Ador = oGestorInformes.DevolVerOfertasRecibidasDesde(Trim(txtFecDesde), Trim(txtFecHasta), val(sdbcAnyo), Trim(sdbcGMN1_4Cod), val(sdbcProceCod), Trim(sdbcProveCod), Trim(sdbcEstadoCod), Leidas, True, bRMat, bRAsig, bREqpAsig, bRCompResponsable, bROfeEqp, bROfeTodas, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, TipoOrdenacionBuzon.OrdPorProce, ProceSelector1.Seleccion, bRUsuAper, CStr(basOptimizacion.gvarCodUsuario), m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
        
            Case 2 'Por fecha de recepcion
                Set Ador = oGestorInformes.DevolVerOfertasRecibidasDesde(Trim(txtFecDesde), Trim(txtFecHasta), val(sdbcAnyo), Trim(sdbcGMN1_4Cod), val(sdbcProceCod), Trim(sdbcProveCod), Trim(sdbcEstadoCod), Leidas, True, bRMat, bRAsig, bREqpAsig, bRCompResponsable, bROfeEqp, bROfeTodas, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, TipoOrdenacionBuzon.OrdPorFechaRecep, ProceSelector1.Seleccion, bRUsuAper, CStr(basOptimizacion.gvarCodUsuario), m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
     
        End Select
    Else
        Leidas = 2
        Screen.MousePointer = vbHourglass
        
        Select Case GrpIndex
            Case 0 'Por codigo de proveedor
                Set Ador = oGestorInformes.DevolVerOfertasRecibidasDesde(Trim(txtFecDesde), Trim(txtFecHasta), val(sdbcAnyo), Trim(sdbcGMN1_4Cod), val(sdbcProceCod), Trim(sdbcProveCod), Trim(sdbcEstadoCod), Leidas, True, bRMat, bRAsig, bREqpAsig, bRCompResponsable, bROfeEqp, bROfeTodas, , , TipoOrdenacionBuzon.OrdPorProve, ProceSelector1.Seleccion, bRUsuAper, basOptimizacion.gvarCodUsuario, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
    
            Case 1 'Por c�digo de proceso
                 Set Ador = oGestorInformes.DevolVerOfertasRecibidasDesde(Trim(txtFecDesde), Trim(txtFecHasta), val(sdbcAnyo), Trim(sdbcGMN1_4Cod), val(sdbcProceCod), Trim(sdbcProveCod), Trim(sdbcEstadoCod), Leidas, True, bRMat, bRAsig, bREqpAsig, bRCompResponsable, bROfeEqp, bROfeTodas, , , TipoOrdenacionBuzon.OrdPorProce, ProceSelector1.Seleccion, bRUsuAper, basOptimizacion.gvarCodUsuario, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
   
            Case 2 'Por fecha de recepcion
                Set Ador = oGestorInformes.DevolVerOfertasRecibidasDesde(Trim(txtFecDesde), Trim(txtFecHasta), val(sdbcAnyo), Trim(sdbcGMN1_4Cod), val(sdbcProceCod), Trim(sdbcProveCod), Trim(sdbcEstadoCod), Leidas, True, bRMat, bRAsig, bREqpAsig, bRCompResponsable, bROfeEqp, bROfeTodas, , , TipoOrdenacionBuzon.OrdPorFechaRecep, ProceSelector1.Seleccion, bRUsuAper, basOptimizacion.gvarCodUsuario, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
        
        End Select
    End If
         
    CargarGrid Ador
    
    If Not Ador Is Nothing Then Ador.Close
    
    Set Ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbgOfertas_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim iFirstRow As Integer
Dim i As Integer
Dim iNumFilas As Integer
    
    If sdbgOfertas.Rows = 0 Then Exit Sub
    
    If Button = 2 Then
        mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
        mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
        DoEvents
        ConfigurarMenuVisor
        PopupMenu MDI.mnuPopUpOfe
    End If
    
End Sub

Private Sub sdbgOfertas_RowLoaded(ByVal Bookmark As Variant)
    
    If sdbgOfertas.Columns("A").Value >= "1" Then
        sdbgOfertas.Columns("ADJUNTOS").CellStyleSet "Adjunto"
    Else
        sdbgOfertas.Columns("ADJUNTOS").CellStyleSet "Normal"
    End If
    
    If sdbgOfertas.Columns("L").Value = "1" Then
        sdbgOfertas.Columns("LEIDA").CellStyleSet "Leido"
    Else
        Dim i As Integer
        For i = 2 To 14
            sdbgOfertas.Columns(i).CellStyleSet "Bold"
        Next
        sdbgOfertas.Columns("LEIDA").CellStyleSet "No Leido"
    End If
    
End Sub

Private Sub Timer1_Timer()
    
    iIntervalo = iIntervalo + 1
    
    If iIntervalo = gParametrosInstalacion.giBuzonTimer Then
        iIntervalo = 0
        cmdActualizar_Click
    End If
    
End Sub

Private Sub txtFecDesde_Change()
    sdbgOfertas.RemoveAll
End Sub


Private Sub txtFecHasta_Change()
    sdbgOfertas.RemoveAll
End Sub

