VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmADJCierreParc 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DCierre parcial del proceso:"
   ClientHeight    =   3990
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8250
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmADJCierreParc.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3990
   ScaleWidth      =   8250
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBGrid sdbgAdj 
      Height          =   3135
      Left            =   60
      TabIndex        =   3
      Top             =   60
      Width           =   8130
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   9
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   4577
      Columns(0).Caption=   "Item"
      Columns(0).Name =   "ITEM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4419
      Columns(1).Caption=   "Proveedor"
      Columns(1).Name =   "PROV"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1852
      Columns(2).Caption=   "Cantidad"
      Columns(2).Name =   "CANT"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2858
      Columns(3).Caption=   "Precio"
      Columns(3).Name =   "PRECIO"
      Columns(3).Alignment=   1
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "ID"
      Columns(4).Name =   "ID"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "CODPROV"
      Columns(5).Name =   "CODPROV"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ADJPORCEN"
      Columns(6).Name =   "ADJPORCEN"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "NUMOFE"
      Columns(7).Name =   "NUMOFE"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "ADJ"
      Columns(8).Name =   "ADJ"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   11
      Columns(8).FieldLen=   256
      _ExtentX        =   14340
      _ExtentY        =   5530
      _StockProps     =   79
      Caption         =   "DCierre de Items"
      BackColor       =   -2147483633
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   8250
      TabIndex        =   0
      Top             =   3495
      Width           =   8250
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "D&Aceptar"
         Height          =   345
         Left            =   3000
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "D&Cancelar"
         Height          =   345
         Left            =   4100
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   60
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   3180
      Width           =   8135
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   3
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmADJCierreParc.frx":014A
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmADJCierreParc.frx":0166
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmADJCierreParc.frx":0182
      DividerType     =   1
      DividerStyle    =   2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   3
      Columns(0).Width=   9551
      Columns(0).Caption=   "DESCR"
      Columns(0).Name =   "DESCR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "TOTAL"
      Columns(1).Name =   "TOTAL"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1773
      Columns(2).Caption=   "MON"
      Columns(2).Name =   "MON"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   14349
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmADJCierreParc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public oAdjs As CAdjudicaciones
Public g_oAdjsGrupo As CAdjsGrupo
Private oIAdj As IAdjudicaciones
Public oGRCierre As CGrupos
Public m_oProcesoSeleccionado As CProceso
Public m_oGrupoSeleccionado As CGrupo

Public bCierreGrupo As Boolean
Public bCierreTotal As Boolean
Public frmOrigen As Form

Public errorcierre

Private m_sProceso As String
Private m_sGrupo As String

Private m_sIdiErrorEvaluacion(2) As String

Private txtAsuntoCierreProc As String
Private txtAsuntoPreadjProc As String
Private oFos As Scripting.FileSystemObject
Private m_sCons As String
Private m_sAdj As String
Private m_sAhorr As String
Private m_sAbierto As String
Private m_sConsumido As String
Private m_sAdjudicado As String
Private m_sAhorrado As String
'form:control de errores
Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean

Public m_dYaCerrado As Double

Private m_sMsgError As String
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADJ_CIERREPARC, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value
        m_sProceso = Ador(0).Value
        Ador.MoveNext
        sdbgAdj.caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdj.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdj.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdj.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdj.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgTotales.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        m_sGrupo = Ador(0).Value
        
        Ador.MoveNext
        m_sIdiErrorEvaluacion(1) = Ador(0).Value  '11 Error al realizar el c�lculo:F�rmula inv�lida.
        Ador.MoveNext
        m_sIdiErrorEvaluacion(2) = Ador(0).Value  '12 Error al realizar el c�lculo:Valores incorrectos.
        
        Ador.MoveNext
        m_sCons = Ador(0).Value
        Ador.MoveNext
        m_sAdj = Ador(0).Value
        Ador.MoveNext
        m_sAhorr = Ador(0).Value
        Ador.MoveNext
        txtAsuntoCierreProc = Ador(0).Value
        Ador.MoveNext
        txtAsuntoPreadjProc = Ador(0).Value
        Ador.MoveNext
        m_sAbierto = Ador(0).Value
        Ador.MoveNext
        m_sConsumido = Ador(0).Value
        Ador.MoveNext
        m_sAdjudicado = Ador(0).Value
        Ador.MoveNext
        m_sAhorrado = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
        
End Sub

''' <summary>
''' Cierre parcial de proceso
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim oAtribsEspec As CAtributosEspecificacion
    Dim oAtribEspec As CAtributoEspecificacion
    Dim sAtributosObligatorios As String
    Dim oPers As CPersona
    Dim NombreNotif As String
    Dim ApellNotif As String
    Dim MailNotif As String
    Dim sCuerpo As String
    Dim sAsunto As String
    Dim errormail As TipoErrorSummit
    Dim oPer As CPersona
    Dim oPersonas As CPersonas
    Dim oIPersAsig As IPersonasAsig
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit
    Dim oGrupo As CGrupo

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    If sdbgAdj.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If Not Preadjudicar Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    frmOrigen.m_udtOrigBloqueo = 2 'Validando
    If bCierreTotal Then
        Set oAtribsEspec = m_oProcesoSeleccionado.ValidarAtributosEspObligatorios(TValidacionAtrib.TVAdjudicacion)
        If oAtribsEspec.Count > 0 Then
            For Each oAtribEspec In oAtribsEspec
                sAtributosObligatorios = sAtributosObligatorios & oAtribEspec.Den & vbCrLf
            Next
        End If
        If sAtributosObligatorios <> "" Then
            Call oMensajes.AtributosEspObl(sAtributosObligatorios, TVAdjudicacion)
            Exit Sub
        End If
        
        frmOrigen.sdbgResultados.MoveFirst
        If frmOrigen.Name = "frmRESREU" Then
            frmOrigen.errorcierre = m_oProcesoSeleccionado.ValidacionYCierre(basOptimizacion.gvarCodUsuario, frmOrigen.m_oAdjs, frmOrigen.g_dblAdjudicadoProcCierre, CDate(frmOrigen.sdbcFecReu.Columns(0).Value))
        Else
            If Not CtrlIgnorarRestriccAdjDir(True) Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
                    
            frmOrigen.errorcierre = m_oProcesoSeleccionado.ValidacionYCierre(basOptimizacion.gvarCodUsuario, frmOrigen.m_oAdjs, frmOrigen.g_dblAdjudicadoProcCierre)
        End If

        frmOrigen.m_udtOrigBloqueo = 1 'Proceso
        teserror = frmOrigen.errorcierre
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Screen.MousePointer = vbNormal
        Else
            oMensajes.ProcesoValidado
            basSeguridad.RegistrarAccion accionessummit.ACCOfeCompAdjudicar, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & "GMN1:" & m_oProcesoSeleccionado.GMN1Cod & "Proce:" & m_oProcesoSeleccionado.Cod & "Fecha:" & CStr(Date)
            
            NotificarPasoProveedorQAaReal False, m_oProcesoSeleccionado, oAdjudicadoProves:=frmOrigen.m_oAdjs
            
            'Proceso validado
            If frmADJ.m_bValidar Or frmRESREU.m_bValidar Then
                If gParametrosGenerales.gbActAdjMail Then
                    Set oIPersAsig = m_oProcesoSeleccionado
                    Set oPersonas = oIPersAsig.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorCod, True)
                    
                    irespuesta = vbNo
                    'La pregunta de confirmaci�n antes del envio de mails se visualizar� solo si hay personas con notificaci�n activada
                    If oPersonas.Count > 0 Then
                        For Each oPer In oPersonas
                            If BooleanToSQLBinary(oPer.Notificado) <> 0 Then
                                irespuesta = oMensajes.ConfirmacionNotificacionValidar
                                Exit For
                            End If
                        Next
                    End If
                    
                    If irespuesta = vbYes Then
                        For Each oPer In oPersonas
                            If BooleanToSQLBinary(oPer.Notificado) <> 0 Then
                                Set oPers = Nothing
                                Set oPers = oFSGSRaiz.Generar_CPersona
                                oPers.CargarInfoDefectoPersona oPer.Cod
                                
                                NombreNotif = oPer.nombre
                                ApellNotif = oPer.Apellidos
                                MailNotif = oPer.mail
                                
                                sCuerpo = GenerarCuerpoMensajeAdjudicacion(oPers, NombreNotif, ApellNotif)
                                
                                If sCuerpo <> "" Then
                                    sAsunto = txtAsuntoCierreProc & " " & m_oProcesoSeleccionado.Anyo & "/" & m_oProcesoSeleccionado.GMN1Cod & "/" & m_oProcesoSeleccionado.Cod & " " & m_oProcesoSeleccionado.Den
                                
                                    errormail = ComponerMensaje(MailNotif, sAsunto, sCuerpo, , , oPers.TipoMail, , ApellNotif & ", " & NombreNotif, _
                                                    entidadNotificacion:=ProcesoCompra, _
                                                    tipoNotificacion:=AvisoCierreProceso, _
                                                    Anyo:=m_oProcesoSeleccionado.Anyo, _
                                                    GMN1:=m_oProcesoSeleccionado.GMN1Cod, _
                                                    Proce:=m_oProcesoSeleccionado.Cod, _
                                                    sToName:=NombreNotif & " " & ApellNotif)
                                                    
                                    If errormail.NumError <> TESnoerror Then
                                        Screen.MousePointer = vbNormal
                                        basErrores.TratarError errormail
                                        FinalizarSesionMail
                                    End If
                                    
                                Else
                                    'Si entra aqui, es porque no ha habido plantilla, por lo que ya ha salido el aviso y no  hace falta seguir mostrandolo por cada proveedor
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                End If
            Else
                If frmADJ.m_bGuardar Or frmRESREU.m_bGuardar Then
                    If gParametrosGenerales.gbActPreAdjMail Then
                        Set oIPersAsig = m_oProcesoSeleccionado
                        Set oPersonas = oIPersAsig.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorCod, True)
                        
                        irespuesta = vbNo
                        'La pregunta de confirmaci�n antes del envio de mails se visualizar� solo si hay personas con notificaci�n activada
                        If oPersonas.Count > 0 Then
                            For Each oPer In oPersonas
                                If BooleanToSQLBinary(oPer.Notificado) <> 0 Then
                                    irespuesta = oMensajes.ConfirmacionNotificacionGuardar
                                    Exit For
                                End If
                            Next
                        End If
                        
                        If irespuesta = vbYes Then
                            For Each oPer In oPersonas
                                If BooleanToSQLBinary(oPer.Notificado) <> 0 Then
                                    Set oPers = Nothing
                                    Set oPers = oFSGSRaiz.Generar_CPersona
                                    oPers.CargarInfoDefectoPersona oPer.Cod
                                    
                                    NombreNotif = oPer.nombre
                                    ApellNotif = oPer.Apellidos
                                    MailNotif = oPer.mail
                                                                        
                                    sCuerpo = GenerarCuerpoMensajeAdjudicacion(oPers, NombreNotif, ApellNotif)
                                    
                                    If sCuerpo <> "" Then
                                        sAsunto = txtAsuntoPreadjProc & " " & m_oProcesoSeleccionado.Anyo & "/" & m_oProcesoSeleccionado.GMN1Cod & "/" & m_oProcesoSeleccionado.Cod & " " & m_oProcesoSeleccionado.Den
                                    
                                        errormail = ComponerMensaje(MailNotif, sAsunto, sCuerpo, , , oPers.TipoMail, , ApellNotif & ", " & NombreNotif, _
                                                        entidadNotificacion:=ProcesoCompra, _
                                                        tipoNotificacion:=AvisoPreadjProceso, _
                                                        Anyo:=m_oProcesoSeleccionado.Anyo, _
                                                        GMN1:=m_oProcesoSeleccionado.GMN1Cod, _
                                                        Proce:=m_oProcesoSeleccionado.Cod, _
                                                        sToName:=NombreNotif & " " & ApellNotif)
                                                        
                                        If errormail.NumError <> TESnoerror Then
                                            Screen.MousePointer = vbNormal
                                            basErrores.TratarError errormail
                                            FinalizarSesionMail
                                        End If
                                                        
                                    Else
                                        'Si entra aqui, es porque no ha habido plantilla, por lo que ya ha salido el aviso y no  hace falta seguir mostrandolo por cada proveedor
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            End If
            
            Unload Me
            Screen.MousePointer = vbNormal
            If frmOrigen.Name = "frmADJ" Then
                frmADJ.ProcesoCerrado
            Else
                frmRESREU.ProcesoCerrado
            End If
        End If
    Else
        'Cierre parcial del proceso
        If frmOrigen.Name = "frmRESREU" Then
            errorcierre = m_oProcesoSeleccionado.ValidacionYCierreParcial(basOptimizacion.gvarCodUsuario, oAdjs, frmOrigen.m_oAdjParcsGrupo, CDate(frmOrigen.sdbcFecReu.Columns(0).Value))
        Else
            If Not CtrlIgnorarRestriccAdjDir Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            errorcierre = m_oProcesoSeleccionado.ValidacionYCierreParcial(basOptimizacion.gvarCodUsuario, oAdjs, frmOrigen.m_oAdjParcsGrupo)
        End If
        
        frmOrigen.m_udtOrigBloqueo = 1 'Proceso
        teserror = errorcierre
                
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Screen.MousePointer = vbNormal
        Else
            oMensajes.ProcesoParcialmenteCerrado
            basSeguridad.RegistrarAccion accionessummit.ACCOfeCompAdjudicar, "Anyo:" & CStr(m_oProcesoSeleccionado.Anyo) & "GMN1:" & m_oProcesoSeleccionado.GMN1Cod & "Proce:" & m_oProcesoSeleccionado.Cod & "Fecha:" & CStr(Date)
            
            NotificarPasoProveedorQAaReal False, m_oProcesoSeleccionado, oAdjudicadoProves:=frmOrigen.m_oAdjParcsGrupo
            
            'Proceso validado
            If frmADJ.m_bValidar Or frmRESREU.m_bValidar Then
                If gParametrosGenerales.gbActAdjMail Then
                    Set oIPersAsig = m_oProcesoSeleccionado
                    Set oPersonas = oIPersAsig.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorCod, True)
                    
                    irespuesta = vbNo
                    'La pregunta de confirmaci�n antes del envio de mails se visualizar� solo si hay personas con notificaci�n activada
                    If oPersonas.Count > 0 Then
                        For Each oPer In oPersonas
                            If BooleanToSQLBinary(oPer.Notificado) <> 0 Then
                                irespuesta = oMensajes.ConfirmacionNotificacionValidar
                                Exit For
                            End If
                        Next
                    End If
                    
                    If irespuesta = vbYes Then
                        For Each oPer In oPersonas
                            If BooleanToSQLBinary(oPer.Notificado) <> 0 Then
                                Set oPers = Nothing
                                Set oPers = oFSGSRaiz.Generar_CPersona
                                oPers.CargarInfoDefectoPersona oPer.Cod
                                
                                NombreNotif = oPer.nombre
                                ApellNotif = oPer.Apellidos
                                MailNotif = oPer.mail
                                
                                sCuerpo = GenerarCuerpoMensajeAdjudicacion(oPers, NombreNotif, ApellNotif)
                                
                                If sCuerpo <> "" Then
                                    sAsunto = txtAsuntoCierreProc & " " & m_oProcesoSeleccionado.Anyo & "/" & m_oProcesoSeleccionado.GMN1Cod & "/" & m_oProcesoSeleccionado.Cod & " " & m_oProcesoSeleccionado.Den
                                
                                    errormail = ComponerMensaje(MailNotif, sAsunto, sCuerpo, , , oPers.TipoMail, , ApellNotif & ", " & NombreNotif, _
                                                    entidadNotificacion:=ProcesoCompra, _
                                                    tipoNotificacion:=AvisoCierreProceso, _
                                                    Anyo:=m_oProcesoSeleccionado.Anyo, _
                                                    GMN1:=m_oProcesoSeleccionado.GMN1Cod, _
                                                    Proce:=m_oProcesoSeleccionado.Cod, _
                                                    sToName:=NombreNotif & " " & ApellNotif)
                                                    
                                    If errormail.NumError <> TESnoerror Then
                                        Screen.MousePointer = vbNormal
                                        basErrores.TratarError errormail
                                        FinalizarSesionMail
                                    End If
                                                    
                                Else
                                    'Si entra aqui, es porque no ha habido plantilla, por lo que ya ha salido el aviso y no  hace falta seguir mostrandolo por cada proveedor
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                End If
            Else
                If frmADJ.m_bGuardar Or frmRESREU.m_bGuardar Then
                    If gParametrosGenerales.gbActAdjMail Then
                        Set oIPersAsig = m_oProcesoSeleccionado
                        Set oPersonas = oIPersAsig.DevolverTodasLasPersonasAsignadas(TipoOrdenacionPersonas.OrdPorCod, True)
                        
                        irespuesta = vbNo
                        'La pregunta de confirmaci�n antes del envio de mails se visualizar� solo si hay personas con notificaci�n activada
                        If oPersonas.Count > 0 Then
                            For Each oPer In oPersonas
                                If BooleanToSQLBinary(oPer.Notificado) <> 0 Then
                                    irespuesta = oMensajes.ConfirmacionNotificacionGuardar
                                    Exit For
                                End If
                            Next
                        End If
                        
                        If irespuesta = vbYes Then
                            For Each oPer In oPersonas
                                If BooleanToSQLBinary(oPer.Notificado) <> 0 Then
                                    Set oPers = Nothing
                                    Set oPers = oFSGSRaiz.Generar_CPersona
                                    oPers.CargarInfoDefectoPersona oPer.Cod
                                    
                                    NombreNotif = oPer.nombre
                                    ApellNotif = oPer.Apellidos
                                    MailNotif = oPer.mail
                                    
                                    sCuerpo = GenerarCuerpoMensajeAdjudicacion(oPers, NombreNotif, ApellNotif)
                                    
                                    If sCuerpo <> "" Then
                                        sAsunto = txtAsuntoCierreProc & " " & m_oProcesoSeleccionado.Anyo & "/" & m_oProcesoSeleccionado.GMN1Cod & "/" & m_oProcesoSeleccionado.Cod & " " & m_oProcesoSeleccionado.Den
                                    
                                        errormail = ComponerMensaje(MailNotif, sAsunto, sCuerpo, , , oPers.TipoMail, , ApellNotif & ", " & NombreNotif, _
                                                        entidadNotificacion:=ProcesoCompra, _
                                                        tipoNotificacion:=AvisoPreadjProceso, _
                                                        Anyo:=m_oProcesoSeleccionado.Anyo, _
                                                        GMN1:=m_oProcesoSeleccionado.GMN1Cod, _
                                                        Proce:=m_oProcesoSeleccionado.Cod, _
                                                        sToName:=NombreNotif & " " & ApellNotif)
                                                        
                                        If errormail.NumError <> TESnoerror Then
                                            Screen.MousePointer = vbNormal
                                            basErrores.TratarError errormail
                                            FinalizarSesionMail
                                        End If
                                                        
                                    Else
                                        'Si entra aqui, es porque no ha habido plantilla, por lo que ya ha salido el aviso y no  hace falta seguir mostrandolo por cada proveedor
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            End If
            
            Unload Me
            Screen.MousePointer = vbNormal
            If frmOrigen.Name = "frmADJ" Then
                For Each oGrupo In oGRCierre
                    frmADJ.m_oProcesoSeleccionado.Grupos.Item(oGrupo.Codigo).Cerrado = 1
                Next
                frmADJ.m_oProcesoSeleccionado.Estado = ParcialmenteCerrado
                frmADJ.g_bSesionCerradoParc = True
                frmADJ.ActualizarGrid
            Else
                For Each oGrupo In oGRCierre
                    frmRESREU.m_oProcesoSeleccionado.Grupos.Item(oGrupo.Codigo).Cerrado = 1
                Next
                frmRESREU.m_oProcesoSeleccionado.Estado = ParcialmenteCerrado
                frmRESREU.g_bSesionCerradoParc = True
                frmRESREU.ActualizarGrid
            End If
            
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>Crea el Cuerpo del mail</summary>
''' <param name="Persona">Persona a quien se ha de notificar</param>
''' <param name="Nombre">Nombre</param>
''' <param name="Apellido">Apellido</param>
''' <param name="g_oProcesoSeleccionado">Proceso</param>
''' <returns>Cuerpo del mail</returns>
''' <remarks>Llamada desde: cmdAceptar_click </remarks>
Private Function GenerarCuerpoMensajeAdjudicacion(ByVal Persona As CPersona, ByVal nombre As String, ByVal Apellido As String) As String
    Dim oCEmailAdj As CEmailAdj
    Set oCEmailAdj = GenerarCEmailAdj(oFSGSRaiz, gParametrosGenerales, gParametrosInstalacion, oMensajes, oUsuarioSummit, basParametros.gLongitudesDeCodigos, basOptimizacion.gTipoDeUsuario)
    If frmOrigen.Name = "frmADJ" Then
        GenerarCuerpoMensajeAdjudicacion = oCEmailAdj.GenerarCuerpoMensajeAdjudicacionCierreParc(Persona, nombre, Apellido, m_bDescargarFrm, m_bActivado, (frmADJ.m_bGuardar Or frmRESREU.m_bGuardar), _
                    m_oProcesoSeleccionado, frmADJ.m_oProvesAsig, frmADJ.m_oAdjs, oAdjs, m_sAbierto, frmADJ.m_dblAbiertoProc, m_sConsumido, frmADJ.m_dblConsumidoProc, m_sAdjudicado, frmADJ.m_dblAdjudicadoProc, _
                    m_sAhorrado, frmADJ.m_dblAhorradoProc, frmADJ.m_dblAhorradoPorcenProc, m_sCons, m_sAdj, m_sAhorr)
    Else
        GenerarCuerpoMensajeAdjudicacion = oCEmailAdj.GenerarCuerpoMensajeAdjudicacionCierreParc(Persona, nombre, Apellido, m_bDescargarFrm, m_bActivado, (frmADJ.m_bGuardar Or frmRESREU.m_bGuardar), _
                    m_oProcesoSeleccionado, frmRESREU.m_oProvesAsig, frmRESREU.m_oAdjs, oAdjs, m_sAbierto, frmRESREU.m_dblAbiertoProc, m_sConsumido, frmRESREU.m_dblConsumidoProc, m_sAdjudicado, frmRESREU.m_dblAdjudicadoProc, _
                    m_sAhorrado, frmRESREU.m_dblAhorradoProc, frmRESREU.m_dblAhorradoPorcenProc, m_sCons, m_sAdj, m_sAhorr)
    End If
    Set oCEmailAdj = Nothing
End Function

Private Sub cmdCancelar_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
    'Form_load
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    
    CargarRecursos
    
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    'Caption del form
    If m_oGrupoSeleccionado Is Nothing Then
        Me.caption = m_sProceso & " " & m_oProcesoSeleccionado.Anyo & " " & _
        m_oProcesoSeleccionado.GMN1Cod & " " & m_oProcesoSeleccionado.Cod & " - " & m_oProcesoSeleccionado.Den
    Else
        Me.caption = m_sGrupo & " " & m_oGrupoSeleccionado.Codigo & " - " & m_oGrupoSeleccionado.Den
    End If
    
    Set oIAdj = m_oProcesoSeleccionado
    
    PonerFieldSeparator Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Valida el proceso. Solo se debe dar los diferentes mensajes de aviso de bloqueo por f�rmula una vez para todo el proceso
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,4</remarks>
Private Function Preadjudicar() As Boolean
    Dim j As Integer
    Dim teserror As TipoErrorSummit
    Dim dPrecio As Double
    Dim iIndice As Integer
    Dim dPorcenAdj As Double
    Dim vbm As Variant
    Dim bAdjudicadas As Boolean
        
    Dim bBloqueoEtapa As Boolean
    Dim bBloqueoCondiciones As Boolean
    Dim SolicitudId As Variant
    Dim oAdj As CAdjudicacion
    Dim oGrupo As CGrupo
    Dim ListaAvisos As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bAdjudicadas = False
    
    iIndice = 0
    
    For j = 0 To sdbgAdj.Rows - 1
        'Primero almaceno las adjudicaciones para cada proveedor
        vbm = sdbgAdj.AddItemBookmark(j)
        
        If sdbgAdj.Columns("ADJPORCEN").CellValue(vbm) <> "" Then
            dPorcenAdj = CDbl(sdbgAdj.Columns("ADJPORCEN").CellValue(vbm))
            bAdjudicadas = True
            'Las de reuni�n siempre van a ser adjudicables
            If frmOrigen.Name = "frmADJ" Then
                If sdbgAdj.Columns("ADJ").CellValue(vbm) = False Then
                    oMensajes.OfertaNoAdjudicable
                    frmOrigen.errorcierre = TESOtroerror
                    Preadjudicar = False
                    Exit Function
                End If
            End If
        Else
            dPorcenAdj = 0
        End If
        
        If sdbgAdj.Columns("PRECIO").CellValue(vbm) = "" Then
            dPrecio = 0
        Else
            dPrecio = CDbl(sdbgAdj.Columns("PRECIO").CellValue(vbm))
        End If
        
        'oAdjs.Add sdbgAdj.Columns("CODPROV").CellValue(vbm), val(sdbgAdj.Columns("NUMOFE").CellValue(vbm)), val(sdbgAdj.Columns("ID").CellValue(vbm)), dPrecio, dPorcenAdj, iIndice, Null, Null
        
        iIndice = iIndice + 1
        
    Next j
    
    
    If frmOrigen.Name = "frmRESREU" Then
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If gParametrosGenerales.gbSolicitudesCompras Then
            bBloqueoEtapa = False
            bBloqueoCondiciones = False
    
            For Each oAdj In oAdjs

                For Each oGrupo In m_oProcesoSeleccionado.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        If Not oGrupo.Items.Item(CStr(oAdj.Id)) Is Nothing Then
                            SolicitudId = oGrupo.Items.Item(CStr(oAdj.Id)).SolicitudId
                            Exit For
                        End If
                    End If
                Next

                'Se comprueban las condiciones de bloqueo por etapa
                If Not bBloqueoEtapa And Not NoHayParametro(SolicitudId) Then
                    bBloqueoEtapa = BloqueoEtapa(SolicitudId)
                    If bBloqueoEtapa Then
                        oMensajes.MensajeOKOnly 903, Exclamation  'La solicitud se encuentra en una etapa en la que no se permiten adjudicaciones
                        Exit For
                    Else
                        'Se comprueban las condiciones de bloqueo por f�rmula
                        bBloqueoCondiciones = BloqueoCondiciones(SolicitudId, ListaAvisos, oAdjs)
                        If bBloqueoCondiciones Then
                            Exit For
                        End If
                    End If
                End If
    
            Next

            If bBloqueoEtapa Or bBloqueoCondiciones Then
                Preadjudicar = False
                Exit Function
            End If
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        teserror = oIAdj.RealizarAdjudicacionesParciales(oAdjs, CDate(frmOrigen.sdbcFecReu.Columns(0).Value), True, g_oAdjsGrupo, True, frmOrigen.g_dblConsumidoProcCierre, frmOrigen.g_dblAdjudicadoProcCierre, , oUsuarioSummit)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Preadjudicar = False
        Else
            Preadjudicar = True
        End If
        Exit Function
    End If
    
    
    If Not bAdjudicadas Then
        oMensajes.FaltanDatos (86)
        Exit Function
    Else
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If gParametrosGenerales.gbSolicitudesCompras Then
            bBloqueoEtapa = False
            bBloqueoCondiciones = False
    
            For Each oAdj In oAdjs

                For Each oGrupo In m_oProcesoSeleccionado.Grupos
                    If Not oGrupo.Items Is Nothing Then
                        If Not oGrupo.Items.Item(CStr(oAdj.Id)) Is Nothing Then
                            SolicitudId = oGrupo.Items.Item(CStr(oAdj.Id)).SolicitudId
                            Exit For
                        End If
                    End If
                Next

                'Se comprueban las condiciones de bloqueo por etapa
                If Not bBloqueoEtapa And Not NoHayParametro(SolicitudId) Then
                    bBloqueoEtapa = BloqueoEtapa(SolicitudId)
                    If bBloqueoEtapa Then
                        oMensajes.MensajeOKOnly 903, Exclamation  'La solicitud se encuentra en una etapa en la que no se permiten adjudicaciones
                        Exit For
                    Else
                        'Se comprueban las condiciones de bloqueo por f�rmula
                        bBloqueoCondiciones = BloqueoCondiciones(SolicitudId, ListaAvisos, oAdjs)
                        If bBloqueoCondiciones Then
                            Exit For
                        End If
                    End If
                End If
    
            Next

            If bBloqueoEtapa Or bBloqueoCondiciones Then
                Preadjudicar = False
                Exit Function
            End If
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If frmOrigen.Name = "frmADJ" Then
            teserror = oIAdj.RealizarAdjudicacionesParciales(oAdjs, , True, g_oAdjsGrupo, True, frmOrigen.g_dblConsumidoProcCierre, frmOrigen.g_dblAdjudicadoProcCierre, , oUsuarioSummit)
        Else
            teserror = oIAdj.RealizarAdjudicacionesParciales(oAdjs, CDate(frmOrigen.sdbcFecReu.Text), True, g_oAdjsGrupo, True, frmOrigen.g_dblConsumidoProcCierre, frmOrigen.g_dblAdjudicadoProcCierre, oUsuarioSummit)
        End If
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Preadjudicar = False
        Else
            Preadjudicar = True
        End If
    
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "Preadjudicar", err, Erl, , m_bActivado)
      Exit Function
   End If
    
End Function


Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oAdjs = Nothing
    Set m_oProcesoSeleccionado = Nothing
    Set m_oGrupoSeleccionado = Nothing
    Set oIAdj = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgAdj_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmOrigen.sstabComparativa.selectedItem.Tag = "ALL" Then
        'Est� en la pesta�a de ALL
        sdbgAdj.Columns("CANT").NumberFormat = FormateoNumericoComp(frmOrigen.m_oVistaSeleccionadaAll.DecCant)
        sdbgAdj.Columns("PRECIO").NumberFormat = FormateoNumericoComp(frmOrigen.m_oVistaSeleccionadaAll.DecPrecios)
    Else
        'Est� en una pesta�a de grupo
        sdbgAdj.Columns("CANT").NumberFormat = FormateoNumericoComp(frmOrigen.m_oVistaSeleccionadaGr.DecCant)
        sdbgAdj.Columns("PRECIO").NumberFormat = FormateoNumericoComp(frmOrigen.m_oVistaSeleccionadaGr.DecPrecios)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "sdbgAdj_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbgTotales_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmOrigen.sstabComparativa.selectedItem.Tag = "ALL" Then
        'Est� en la pesta�a de ALL
        sdbgTotales.Columns("TOTAL").NumberFormat = FormateoNumericoComp(frmOrigen.m_oVistaSeleccionadaAll.DecResult)
    Else
        'Est� en una pesta�a de grupo
        sdbgTotales.Columns("TOTAL").NumberFormat = FormateoNumericoComp(frmOrigen.m_oVistaSeleccionadaGr.DecResult)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "sdbgTotales_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Function BloqueoEtapa(IdInstancia As Variant) As Boolean
    Dim oInstancia As CInstancia
    Dim Ador As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oInstancia = oFSGSRaiz.Generar_CInstancia
    oInstancia.Id = IdInstancia
    Set Ador = oInstancia.ComprobarBloqueoEtapa()
    
    If Not Ador.EOF Then
        BloqueoEtapa = IIf(Ador.Fields("BLOQUEO_ADJUDICACION").Value = 1, False, True)
    Else
        BloqueoEtapa = False
    End If
    
    Ador.Close
    Set Ador = Nothing
    Set oInstancia = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "BloqueoEtapa", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Se comprueban las condiciones de bloqueo por f�rmula
''' </summary>
''' <param name="IdInstancia">Instancia</param>
''' <param name="ListaAvisos">ByRef. Lista de condiciones de aviso q no cumple</param>
''' <param name="oAdjs">Adjudicaciones a realizar</param>
''' <returns>Si no cumple alguna condici�n de bloqueo. ListaAvisos</returns>
''' <remarks>Llamada desde: Preadjudicar; Tiempo m�ximo: 0,1</remarks>
Private Function BloqueoCondiciones(IdInstancia As Variant, ByRef ListaAvisos As String, Optional ByVal oAdjs As CAdjudicaciones) As Boolean
    Dim oInstancia As CInstancia
    Dim iBloqueo As Integer
    Dim bExisteBloqueo As Boolean
    Dim Ador As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oInstancia = oFSGSRaiz.Generar_CInstancia
    oInstancia.Id = IdInstancia
            
    bExisteBloqueo = False
    
    Set Ador = oInstancia.ComprobarBloqueoCond(TipoBloqueo.Adjudicacion)

    If Ador.EOF Then
        BloqueoCondiciones = False
        Ador.Close
        Set Ador = Nothing
        Set oInstancia = Nothing
        Exit Function
    Else
        While Not Ador.EOF And Not bExisteBloqueo
            iBloqueo = oInstancia.ComprobarBloqueoCondiciones(Ador.Fields("ID").Value, Ador.Fields("FORMULA").Value, , oAdjs, m_oProcesoSeleccionado.Anyo, m_oProcesoSeleccionado.Cod, m_oProcesoSeleccionado.GMN1Cod)
            Select Case iBloqueo
                Case 0  'No existe bloqueo
                
                Case 1  'Existe bloqueo
                    If InStr(ListaAvisos, "@" & Ador.Fields("ID").Value & "@") = 0 Then
                        oMensajes.Bloqueo (NullToStr(Ador.Fields("MENSAJE_" & gParametrosInstalacion.gIdioma).Value))
                        ListaAvisos = ListaAvisos & IIf(ListaAvisos = "", "@", "") & Ador.Fields("ID").Value & "@"
                    End If
                    
                    If Ador.Fields("TIPO").Value = TipoAvisoBloqueo.Bloquea Then
                        bExisteBloqueo = True
                    End If
                Case 3  'Error al realizar el c�lculo:F�rmula inv�lida.
                    If InStr(ListaAvisos, "@" & Ador.Fields("ID").Value & "@") = 0 Then
                        oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                        ListaAvisos = ListaAvisos & IIf(ListaAvisos = "", "@", "") & Ador.Fields("ID").Value & "@"
                    End If
                Case 4  'Error al realizar el c�lculo:Valores incorrectos.
                    If InStr(ListaAvisos, "@" & Ador.Fields("ID").Value & "@") = 0 Then
                        oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(2))
                        ListaAvisos = ListaAvisos & IIf(ListaAvisos = "", "@", "") & Ador.Fields("ID").Value & "@"
                    End If
            End Select
            Ador.MoveNext
        Wend
    End If
    
    BloqueoCondiciones = bExisteBloqueo

    Ador.Close
    Set Ador = Nothing
    Set oInstancia = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "BloqueoCondiciones", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Antes de hacer ValidacionYCierre se comprueba la restricciones de importe/presupuesto maximo
''' </summary>
''' <param name="CierreTotal">true->Cierre Total proceso false->Cierre un item</param>
''' <returns>Si se puede cerrar o no</returns>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Function CtrlIgnorarRestriccAdjDir(Optional ByVal CierreTotal As Boolean = False) As Boolean
    Dim teserror As TipoErrorSummit
    Dim oAdjGr As CAdjudicacion
    Dim SumaAdjudicado As Double
    Dim SumaAdjudicadoMonProce As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    CtrlIgnorarRestriccAdjDir = True
    
    If Not frmOrigen.m_oProcesoSeleccionado.PermitirAdjDirecta Then Exit Function
    
    If Not CierreTotal Then
        SumaAdjudicado = m_dYaCerrado
        For Each oAdjGr In oAdjs
            SumaAdjudicado = SumaAdjudicado + oAdjGr.ImporteAdjTot
            SumaAdjudicadoMonProce = SumaAdjudicadoMonProce + (oAdjGr.ImporteAdjTot / oAdjGr.Cambio)
        Next
    Else
        SumaAdjudicado = frmOrigen.g_dblAdjudicadoProcCierre
    End If
    
    If (gParametrosGenerales.gdVOL_MAX_ADJ_DIR > 0) _
    And ((gParametrosGenerales.gdVOL_MAX_ADJ_DIR * frmOrigen.m_oProcesoSeleccionado.Cambio) < frmOrigen.m_dblAbiertoProc) Then
    
        teserror.NumError = TESVolumenAdjDirSuperado
        teserror.Arg1 = gParametrosGenerales.gdVOL_MAX_ADJ_DIR * frmOrigen.m_oProcesoSeleccionado.Cambio
        teserror.Arg2 = frmOrigen.m_dblAbiertoProc
    
        If frmOrigen.m_bIgnorarRestricAdjDirecta Then
            If oMensajes.PreguntarAdjDirSuperado(teserror) <> vbYes Then
                CtrlIgnorarRestriccAdjDir = False
                Exit Function
            End If
        Else
            oMensajes.NoValidaAdjDirSuperado teserror
            
            CtrlIgnorarRestriccAdjDir = False
            Exit Function
        End If
    End If
            
    If (gParametrosGenerales.gdIMP_MAX_ADJ_DIR > 0) _
    And ((gParametrosGenerales.gdIMP_MAX_ADJ_DIR * frmOrigen.m_oProcesoSeleccionado.Cambio) < SumaAdjudicadoMonProce) Then
    
        teserror.NumError = TESImporteAdjDirSuperado
        teserror.Arg1 = gParametrosGenerales.gdIMP_MAX_ADJ_DIR * frmOrigen.m_oProcesoSeleccionado.Cambio
        teserror.Arg2 = SumaAdjudicado
    
        If frmOrigen.m_bIgnorarRestricAdjDirecta Then
            If oMensajes.PreguntarAdjDirSuperado(teserror) <> vbYes Then
                CtrlIgnorarRestriccAdjDir = False
                Exit Function
            End If
        Else
            oMensajes.NoValidaAdjDirSuperado teserror
        
            CtrlIgnorarRestriccAdjDir = False
            Exit Function
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmADJCierreParc", "CtrlIgnorarRestriccAdjDir", err, Erl, , m_bActivado)
      Exit Function
   End If
    
End Function
