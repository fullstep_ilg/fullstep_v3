VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPRESAsig 
   BackColor       =   &H00808000&
   Caption         =   "Asignar presupuesto"
   ClientHeight    =   6705
   ClientLeft      =   2100
   ClientTop       =   3345
   ClientWidth     =   10245
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPRESAsig.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6705
   ScaleWidth      =   10245
   Begin VB.CommandButton cmdBuscarUO 
      Caption         =   "D&Buscar"
      Height          =   345
      Left            =   1200
      TabIndex        =   29
      Top             =   6330
      Width           =   1005
   End
   Begin VB.CommandButton cmdRestaurarUO 
      Caption         =   "&Restaurar"
      Height          =   345
      Left            =   120
      TabIndex        =   25
      Top             =   6330
      Width           =   1005
   End
   Begin VB.PictureBox picSepar 
      Height          =   495
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   10095
      TabIndex        =   13
      Top             =   0
      Width           =   10155
      Begin VB.TextBox txtPorcenPend 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9015
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   60
         Width           =   765
      End
      Begin VB.TextBox txtPendiente 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6915
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   60
         Width           =   2070
      End
      Begin VB.TextBox txtAsignado 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3270
         Locked          =   -1  'True
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   60
         Width           =   1500
      End
      Begin VB.TextBox txtAbierto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   850
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   60
         Width           =   1400
      End
      Begin VB.TextBox txtPorcenAsig 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4750
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   60
         Width           =   765
      End
      Begin VB.Label lblPorcen 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Index           =   2
         Left            =   9840
         TabIndex        =   24
         Top             =   105
         Width           =   240
      End
      Begin VB.Label lblPrendiente 
         Caption         =   "Pendiente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Left            =   5955
         TabIndex        =   21
         Top             =   90
         Width           =   900
      End
      Begin VB.Label lblAbierto 
         Caption         =   "Abierto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Left            =   100
         TabIndex        =   20
         Top             =   90
         Width           =   720
      End
      Begin VB.Label lblPorcen 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Index           =   1
         Left            =   5595
         TabIndex        =   19
         Top             =   105
         Width           =   240
      End
      Begin VB.Label lblAsignado 
         Caption         =   "Asignado:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Left            =   2350
         TabIndex        =   18
         Top             =   90
         Width           =   900
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   3510
      TabIndex        =   12
      Top             =   6330
      Width           =   1275
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   4950
      TabIndex        =   11
      Top             =   6330
      Width           =   1275
   End
   Begin TabDlg.SSTab SSTabPresupuestos 
      Height          =   5775
      Left            =   0
      TabIndex        =   0
      Top             =   495
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   10186
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Seleccione la unidad organizativa"
      TabPicture(0)   =   "frmPRESAsig.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblEstrorg"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "ImageList1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "ImageList2"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "tvwestrorg"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Seleccione el presupuesto"
      TabPicture(1)   =   "frmPRESAsig.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblUO"
      Tab(1).Control(1)=   "lblOtras"
      Tab(1).Control(2)=   "sdbcAnyo"
      Tab(1).Control(3)=   "tvwestrPres"
      Tab(1).Control(4)=   "picPresup"
      Tab(1).Control(5)=   "cmdOtras"
      Tab(1).ControlCount=   6
      Begin VB.CommandButton cmdOtras 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   -65370
         TabIndex        =   34
         Top             =   405
         Width           =   375
      End
      Begin VB.PictureBox picPresup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Left            =   -74850
         ScaleHeight     =   825
         ScaleWidth      =   9825
         TabIndex        =   1
         Top             =   4800
         Width           =   9885
         Begin VB.TextBox txtPresupuesto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2010
            Locked          =   -1  'True
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   0
            Width           =   2655
         End
         Begin VB.TextBox txtObj 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7560
            Locked          =   -1  'True
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   0
            Width           =   1905
         End
         Begin VB.PictureBox picPorcen 
            BorderStyle     =   0  'None
            Height          =   495
            Left            =   2000
            ScaleHeight     =   495
            ScaleWidth      =   7785
            TabIndex        =   26
            Top             =   420
            Width           =   7785
            Begin VB.TextBox txtPorcenAsignar 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5565
               TabIndex        =   32
               TabStop         =   0   'False
               Top             =   0
               Width           =   1905
            End
            Begin VB.TextBox txtAsignar 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   30
               TabIndex        =   28
               Top             =   0
               Width           =   2655
            End
            Begin MSComctlLib.Slider Slider1 
               Height          =   375
               Left            =   2805
               TabIndex        =   27
               Top             =   0
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   661
               _Version        =   393216
               Max             =   100
            End
            Begin VB.Label lblPorcen 
               Caption         =   "%"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00400000&
               Height          =   225
               Index           =   3
               Left            =   7560
               TabIndex        =   33
               Top             =   30
               Width           =   240
            End
         End
         Begin VB.Line Line1 
            X1              =   0
            X2              =   9840
            Y1              =   330
            Y2              =   330
         End
         Begin VB.Label lblAsignar 
            Caption         =   "Asignar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   750
            TabIndex        =   6
            Top             =   450
            Width           =   900
         End
         Begin VB.Label lblPresupuesto 
            Caption         =   "Presupuesto:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00400000&
            Height          =   225
            Left            =   750
            TabIndex        =   5
            Top             =   30
            Width           =   1140
         End
         Begin VB.Label lblObjetivo 
            Caption         =   "Objetivo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00400000&
            Height          =   225
            Left            =   6450
            TabIndex        =   4
            Top             =   60
            Width           =   1035
         End
      End
      Begin MSComctlLib.TreeView tvwestrorg 
         Height          =   4905
         Left            =   150
         TabIndex        =   7
         Top             =   735
         Width           =   9885
         _ExtentX        =   17436
         _ExtentY        =   8652
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   0
         Top             =   500
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   6
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":0CEA
               Key             =   "Raiz"
               Object.Tag             =   "Raiz"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":17B4
               Key             =   "PRES1"
               Object.Tag             =   "PRES1"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":1C06
               Key             =   "PRES2"
               Object.Tag             =   "PRES2"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":2058
               Key             =   "PRES3"
               Object.Tag             =   "PRES3"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":24AA
               Key             =   "PRES4"
               Object.Tag             =   "PRES4"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":28FC
               Key             =   "PRESA"
               Object.Tag             =   "PRESA"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView tvwestrPres 
         Height          =   4005
         Left            =   -74850
         TabIndex        =   8
         Top             =   735
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   7064
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList2"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   500
         Top             =   500
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   12
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":2C4E
               Key             =   "UON0"
               Object.Tag             =   "UON0"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":2CEC
               Key             =   "UON1"
               Object.Tag             =   "UON1"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":2D9C
               Key             =   "UON2"
               Object.Tag             =   "UON2"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":2E4C
               Key             =   "UON3"
               Object.Tag             =   "UON3"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":2EDE
               Key             =   "UON1A"
               Object.Tag             =   "UON1A"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":2F9E
               Key             =   "UON2A"
               Object.Tag             =   "UON2A"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":3060
               Key             =   "UON3A"
               Object.Tag             =   "UON3A"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":3120
               Key             =   "UON2D"
               Object.Tag             =   "UON2D"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":31E2
               Key             =   "UON1D"
               Object.Tag             =   "UON1D"
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":32A4
               Key             =   "UON3D"
               Object.Tag             =   "UON3D"
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":3366
               Key             =   "UON0A"
               Object.Tag             =   "UON0A"
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmPRESAsig.frx":3409
               Key             =   "UON0D"
               Object.Tag             =   "UON0D"
            EndProperty
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   -67110
         TabIndex        =   22
         Top             =   390
         Width           =   840
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1482
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "2002"
         BackColor       =   16777215
      End
      Begin VB.Label lblOtras 
         Caption         =   "Others -->"
         Height          =   195
         Left            =   -66225
         TabIndex        =   35
         Top             =   450
         Width           =   825
      End
      Begin VB.Label lblEstrorg 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Unidad organizativa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   150
         TabIndex        =   10
         Top             =   390
         Width           =   9885
      End
      Begin VB.Label lblUO 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Unidad organizativa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -74850
         TabIndex        =   9
         Top             =   390
         Width           =   8595
      End
   End
   Begin VB.CommandButton cmdRestaurarPres 
      Caption         =   "&RestaurarP"
      Height          =   345
      Left            =   120
      TabIndex        =   30
      Top             =   6330
      Width           =   1005
   End
   Begin VB.CommandButton cmdBuscarPres 
      Caption         =   "&BuscarP"
      Height          =   345
      Left            =   1200
      TabIndex        =   31
      Top             =   6330
      Width           =   1005
   End
End
Attribute VB_Name = "frmPRESAsig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Para el popup de Otras
Private WithEvents cP As cPopupMenu
Attribute cP.VB_VarHelpID = -1
Private Const mcWEBSITE = -&H8000&
Private m_arTag As Variant

'Tipo de presupuesto
Public g_iTipoPres As Integer
'Ambito del presupuesto,1 proce, 2 grupo, 3 item
Public g_iAmbitoPresup As Integer
'Abierto original, no se modifica
Public g_dblAbierto As Double
Public g_dblAsignado As Double
Private m_dblPorcenAsig As Double
Private m_dblPendiente As Double 'Abierto - Asignado
Public g_dblAsignadoInicial As Double 'Lo guardo al princio para el restore
'Si al entrar el pendiente es distino de 0 y de 100
'los porcentajes se calculan en relaci�n al nuevo abierto
Private m_bPorcenAsigAbierto As Boolean 'Si True hay que recalcular
Public g_bModif As Boolean
Public g_bHayPresBajaLog As Boolean

Private m_bOBLPres1 As Boolean
Private m_bOBLPres2 As Boolean
Private m_bOBLPres3 As Boolean
Private m_bOBLPres4 As Boolean
Private m_iNIVPres1 As Integer
Private m_iNIVPres2 As Integer
Private m_iNIVPres3 As Integer
Private m_iNIVPres4 As Integer

' Unidades organizativas
Private m_oUnidadesOrgN1 As CUnidadesOrgNivel1
Private m_oUnidadesOrgN2 As CUnidadesOrgNivel2
Private m_oUnidadesOrgN3 As CUnidadesOrgNivel3

'Variables para la estructura de presupuestos
Private m_oPresupuestos1 As CPresProyectosNivel1
Private m_oPresupuestos2 As CPresContablesNivel1
Private m_oPresupuestos3 As CPresConceptos3Nivel1
Private m_oPresupuestos4 As CPresConceptos4Nivel1

'Presupuestos asignados al proceso, grupo o �tem
Public m_oPresupuestos1Asignados As CPresProyectosNivel4
Public m_oPresupuestos2Asignados As CPresContablesNivel4
Public m_oPresupuestos3Asignados As CPresConceptos3Nivel4
Public m_oPresupuestos4Asignados As CPresConceptos4Nivel4
'Para el boton otras
Private m_oPresupuestos1Otros As CPresProyectosNivel4
Private m_oPresupuestos2Otros As CPresContablesNivel4
Private m_oPresupuestos3Otros As CPresConceptos3Nivel4
Private m_oPresupuestos4Otros As CPresConceptos4Nivel4

'Restricci�n UO
Public g_bRUO As Boolean
Public g_bRPerfUO As Boolean
Public g_bRUORama As Boolean
Private m_iUOBase As Integer
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String

Private m_bRespetarPorcen As Boolean
Private m_stexto As String

Public g_sOrigen As String
Public g_oOrigen As Form
Public g_iCol As Integer
Public g_iLstGrupoItem As Integer


'Grupo al que se van a mover los �tems
Public g_sGrupoNuevo As String

Private m_bAceptar As Boolean

'Para indicar desde otras pantallas si el presupuesto actual est� distribuido o no al entrar
Public g_bHayPres As Boolean
'Para que no haga las comprobaciones en el unload cuando es un timeout
Public g_bTimeout As Boolean
Public g_bSinPermisos As Boolean


Public g_sValorPresFormulario As String

Public msPresupPlural As String
Public m_bRestringirMultiplePres As Boolean
Dim m_sItems As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub ActualizarEstrOrg(ByVal node As MSComctlLib.node)
Dim oPres1Niv4 As CPresProyNivel4
Dim oPres2Niv4 As CPresconNivel4
Dim oPres3Niv4 As CPresConcep3Nivel4
Dim oPres4Niv4 As CPresConcep4Nivel4
Dim bConPres As Boolean
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case Left(node.Tag, 4)
        
        Case "UON0"
        
                    If node.Image = "UON0A" Then
                        bConPres = True
                    Else
                        bConPres = False
                    End If
                    
                    Select Case g_iTipoPres
                        Case 1
                            If bConPres Then
                                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                                    If NullToStr(oPres1Niv4.UON1) = g_sUON1 And NullToStr(oPres1Niv4.UON2) = g_sUON2 And NullToStr(oPres1Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON0D"
                            Else
                                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                                    If NullToStr(oPres1Niv4.UON1) = g_sUON1 And NullToStr(oPres1Niv4.UON2) = g_sUON2 And NullToStr(oPres1Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON0A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 2
                            If bConPres Then
                                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                                    If NullToStr(oPres2Niv4.UON1) = g_sUON1 And NullToStr(oPres2Niv4.UON2) = g_sUON2 And NullToStr(oPres2Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON0D"
                            Else
                                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                                    If NullToStr(oPres2Niv4.UON1) = g_sUON1 And NullToStr(oPres2Niv4.UON2) = g_sUON2 And NullToStr(oPres2Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON0A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 3
                            If bConPres Then
                                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                                    If NullToStr(oPres3Niv4.UON1) = g_sUON1 And NullToStr(oPres3Niv4.UON2) = g_sUON2 And NullToStr(oPres3Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON0D"
                            Else
                                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                                    If NullToStr(oPres3Niv4.UON1) = g_sUON1 And NullToStr(oPres3Niv4.UON2) = g_sUON2 And NullToStr(oPres3Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON0A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 4
                            If bConPres Then
                                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                                    If NullToStr(oPres4Niv4.UON1) = g_sUON1 And NullToStr(oPres4Niv4.UON2) = g_sUON2 And NullToStr(oPres4Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON0D"
                            Else
                                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                                    If NullToStr(oPres4Niv4.UON1) = g_sUON1 And NullToStr(oPres4Niv4.UON2) = g_sUON2 And NullToStr(oPres4Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON0A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                    End Select
            
            Case "UON1"
                    If node.Image = "UON1A" Then
                        bConPres = True
                    Else
                        bConPres = False
                    End If
                    Select Case g_iTipoPres
                        Case 1
                            If bConPres Then
                                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                                    If NullToStr(oPres1Niv4.UON1) = g_sUON1 And NullToStr(oPres1Niv4.UON2) = g_sUON2 And NullToStr(oPres1Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON1D"
                            Else
                                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                                    If NullToStr(oPres1Niv4.UON1) = g_sUON1 And NullToStr(oPres1Niv4.UON2) = g_sUON2 And NullToStr(oPres1Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON1A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 2
                            If bConPres Then
                                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                                    If NullToStr(oPres2Niv4.UON1) = g_sUON1 And NullToStr(oPres2Niv4.UON2) = g_sUON2 And NullToStr(oPres2Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON1D"
                            Else
                                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                                    If NullToStr(oPres2Niv4.UON1) = g_sUON1 And NullToStr(oPres2Niv4.UON2) = g_sUON2 And NullToStr(oPres2Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON1A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 3
                            If bConPres Then
                                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                                    If NullToStr(oPres3Niv4.UON1) = g_sUON1 And NullToStr(oPres3Niv4.UON2) = g_sUON2 And NullToStr(oPres3Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON1D"
                            Else
                                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                                    If NullToStr(oPres3Niv4.UON1) = g_sUON1 And NullToStr(oPres3Niv4.UON2) = g_sUON2 And NullToStr(oPres3Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON1A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 4
                            If bConPres Then
                                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                                    If NullToStr(oPres4Niv4.UON1) = g_sUON1 And NullToStr(oPres4Niv4.UON2) = g_sUON2 And NullToStr(oPres4Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON1D"
                            Else
                                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                                    If NullToStr(oPres4Niv4.UON1) = g_sUON1 And NullToStr(oPres4Niv4.UON2) = g_sUON2 And NullToStr(oPres4Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON1A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                    End Select
                    
            
            Case "UON2"
                    If node.Image = "UON2A" Then
                        bConPres = True
                    Else
                        bConPres = False
                    End If
                    
                    Select Case g_iTipoPres
                        Case 1
                            If bConPres Then
                                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                                    If NullToStr(oPres1Niv4.UON1) = g_sUON1 And NullToStr(oPres1Niv4.UON2) = g_sUON2 And NullToStr(oPres1Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON2D"
                            Else
                                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                                    If NullToStr(oPres1Niv4.UON1) = g_sUON1 And NullToStr(oPres1Niv4.UON2) = g_sUON2 And NullToStr(oPres1Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON2A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 2
                            If bConPres Then
                                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                                    If NullToStr(oPres2Niv4.UON1) = g_sUON1 And NullToStr(oPres2Niv4.UON2) = g_sUON2 And NullToStr(oPres2Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON2D"
                            Else
                                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                                    If NullToStr(oPres2Niv4.UON1) = g_sUON1 And NullToStr(oPres2Niv4.UON2) = g_sUON2 And NullToStr(oPres2Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON2A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 3
                            If bConPres Then
                                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                                    If NullToStr(oPres3Niv4.UON1) = g_sUON1 And NullToStr(oPres3Niv4.UON2) = g_sUON2 And NullToStr(oPres3Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON2D"
                            Else
                                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                                    If NullToStr(oPres3Niv4.UON1) = g_sUON1 And NullToStr(oPres3Niv4.UON2) = g_sUON2 And NullToStr(oPres3Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON2A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 4
                            If bConPres Then
                                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                                    If NullToStr(oPres4Niv4.UON1) = g_sUON1 And NullToStr(oPres4Niv4.UON2) = g_sUON2 And NullToStr(oPres4Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON2D"
                            Else
                                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                                    If NullToStr(oPres4Niv4.UON1) = g_sUON1 And NullToStr(oPres4Niv4.UON2) = g_sUON2 And NullToStr(oPres4Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON2A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                    End Select
                    
                
            Case "UON3"
                    If node.Image = "UON3A" Then
                        bConPres = True
                    Else
                        bConPres = False
                    End If
                    
                    Select Case g_iTipoPres
                        Case 1
                            If bConPres Then
                                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                                    If NullToStr(oPres1Niv4.UON1) = g_sUON1 And NullToStr(oPres1Niv4.UON2) = g_sUON2 And NullToStr(oPres1Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON3D"
                            Else
                                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                                    If NullToStr(oPres1Niv4.UON1) = g_sUON1 And NullToStr(oPres1Niv4.UON2) = g_sUON2 And NullToStr(oPres1Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON3A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 2
                            If bConPres Then
                                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                                    If NullToStr(oPres2Niv4.UON1) = g_sUON1 And NullToStr(oPres2Niv4.UON2) = g_sUON2 And NullToStr(oPres2Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON3D"
                            Else
                                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                                    If NullToStr(oPres2Niv4.UON1) = g_sUON1 And NullToStr(oPres2Niv4.UON2) = g_sUON2 And NullToStr(oPres2Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON3A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 3
                            If bConPres Then
                                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                                    If NullToStr(oPres3Niv4.UON1) = g_sUON1 And NullToStr(oPres3Niv4.UON2) = g_sUON2 And NullToStr(oPres3Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON3D"
                            Else
                                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                                    If NullToStr(oPres3Niv4.UON1) = g_sUON1 And NullToStr(oPres3Niv4.UON2) = g_sUON2 And NullToStr(oPres3Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON3A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 4
                            If bConPres Then
                                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                                    If NullToStr(oPres4Niv4.UON1) = g_sUON1 And NullToStr(oPres4Niv4.UON2) = g_sUON2 And NullToStr(oPres4Niv4.UON3) = g_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON3D"
                            Else
                                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                                    If NullToStr(oPres4Niv4.UON1) = g_sUON1 And NullToStr(oPres4Niv4.UON2) = g_sUON2 And NullToStr(oPres4Niv4.UON3) = g_sUON3 Then
                                        node.Image = "UON3A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                    End Select
                                    
        End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "ActualizarEstrOrg", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Almacena la seleccion de presupuestos de tipo 1 seleccionado
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdAceptar_click sistema; Tiempo m�ximo:0</remarks>

Private Sub AlmacenarPres1()
Dim udtTeserror As TipoErrorSummit
Dim oGrupo As CGrupo
Dim iDef As Integer
Dim oPRES1 As CPresProyNivel4
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
        Case "frmFormularios", "frmDesgloseValores", "frmSolicitudDetalle", "frmSolicitudDesglose", "frmSolicitudDesgloseP", "frmSOLAbrirFaltan"
            Dim sValor As String
            Dim sTexto As String
            sValor = ""
            sTexto = ""
            For Each oPRES1 In m_oPresupuestos1Asignados
                sValor = sValor & oPRES1.IDNivel & "_" & oPRES1.Id & "_" & DblToSQLFloat(garSimbolos, oPRES1.Porcen) & "#"
            Next
            If sValor <> "" Then
                sValor = Mid(sValor, 1, Len(sValor) - 1)
            End If
            Select Case g_sOrigen
                Case "frmSolicitudDetalle"
                    frmSolicitudes.g_ofrmDetalleSolic.MostrarPresSeleccionado sValor, 1
                Case "frmFormularios"
                    If m_oPresupuestos1Asignados.Count = 1 Then
                        frmFormularios.MostrarPresSeleccionado sValor, 1, m_oPresupuestos1Asignados.Item(1).BajaLog
                    Else
                        frmFormularios.MostrarPresSeleccionado sValor, 1
                    End If
                Case "frmDesgloseValores"
                    frmDesgloseValores.MostrarPresSeleccionado sValor, 1
                Case "frmSolicitudDesglose"
                    frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.MostrarPresSeleccionado sValor, 1
                Case "frmSolicitudDesgloseP"
                    g_oOrigen.g_ofrmDetalleSolic.g_ofrmDesgloseValores.MostrarPresSeleccionado sValor, 1
                Case "frmSOLAbrirFaltan"
                    frmSOLAbrirFaltan.MostrarPresSeleccionado sValor, 1
            End Select
            
            
    
        Case "frmPROCEModifConfig" 'Modificaci�n de la configuraci�n de un proceso existente
    
                If g_iCol = 2 Then 'Pasa a PROCESO
                    iDef = frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1
                    frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso
                    Set frmPROCE.g_oProcesoSeleccionado.Pres1Nivel4 = m_oPresupuestos1Asignados
                    udtTeserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(PresAnualTipo1)
                    If udtTeserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError udtTeserror
                        frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = iDef
                        frmPROCE.MostrarConfiguracion
                        Unload Me
                        Set oGrupo = Nothing
                        Exit Sub
                    End If
                    frmPROCE.sdbcGrupo.Columns(0).Value = ""
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefPresAnualTipo1 Then
                            oGrupo.DefPresAnualTipo1 = False
                            oGrupo.HayPresAnualTipo1 = False
                        End If
                    Next
                    If StrToDbl0(txtPorcenAsig.ToolTipText) = 0 Then
                        frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 = False
                    Else
                        frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 = True
                    End If
                End If
                If g_iCol = 3 Then 'Pasa a GRUPO
                    frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo
                    Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems(g_iLstGrupoItem).Tag))
                    If oGrupo Is Nothing Then
                        Screen.MousePointer = vbNormal
                        Set oGrupo = Nothing
                        Exit Sub
                    End If
                    
                    oGrupo.DefPresAnualTipo1 = True
                    Set oGrupo.Pres1Nivel4 = m_oPresupuestos1Asignados
                    If StrToDbl0(txtPorcenAsig.ToolTipText) = 0 Then
                        oGrupo.HayPresAnualTipo1 = False
                    Else
                        oGrupo.HayPresAnualTipo1 = True
                    End If
                    frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 = False
                End If
    
    
        Case "frmPROCEElimGrupo" 'Eliminar un grupo
                Set frmPROCE.g_oProcesoSeleccionado.Pres1Nivel4 = m_oPresupuestos1Asignados
    
        Case "MODIF_ITEMS" 'Modificar m�ltiples �tems desde frmItemModificarValores
                Set frmItemModificarValores.g_oPresupuestos1Nivel4 = m_oPresupuestos1Asignados
                
        Case "PROCE_MODIFITEMS" 'Modificar presupuesto de m�ltiples �tems desde frmPRESItem
                udtTeserror = m_oPresupuestos1Asignados.AlmacenarPresDeMultiplesItems(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_arItems)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    Exit Sub
                End If
                AnyadirPresupAGridItems
                
        Case "CANT_ITEMS", "PRES_ITEMS" 'Modificar la cantidad o el precio de m�ltiples �tems
                Set frmDatoAmbitoProce.g_oPresupuestos1Nivel4 = m_oPresupuestos1Asignados
                
        Case "ITEM" 'Modificar la cantidad o el precio de un �tem
                Set frmPROCE.g_oPresupuestos1ItemModif = m_oPresupuestos1Asignados
                                
        Case "ANYA_ITEMS" 'Anyadir m�ltiples �tems
                Set frmItemsWizard.g_oPresupuestos1Nivel4 = m_oPresupuestos1Asignados
                
        Case "ANYA_ITEM", "XLS_ITEMS" 'Anyadir un �tem desde la grid de �tems o varios desde excel
                Set frmPROCE.g_oPresupuestos1ItemModif = m_oPresupuestos1Asignados
                
        Case "DUPLICAR_ITEMS" 'Duplicar �tems
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres1Nivel4 = m_oPresupuestos1Asignados
                Else
                    If g_iAmbitoPresup = 2 Then
                        Set frmPROCE.g_oGrupoSeleccionado.Pres1Nivel4 = m_oPresupuestos1Asignados
                    End If
                End If
                
        Case "GRUPOS" 'Grupo antiguo (Cambiar items de grupo)
                Set frmDatoAmbitoProce.g_oPresupuestos1Nivel4 = m_oPresupuestos1Asignados
                
        Case "GRUPO_NUEVO" 'Grupo nuevo (Cambiar items de grupo)
                Set frmDatoAmbitoProce.g_oPresupuestos1Nivel4Nuevos = m_oPresupuestos1Asignados
        
        Case "PROCEN" 'Proceso nuevo, presupuesto a nivel de proceso o grupo
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres1Nivel4 = m_oPresupuestos1Asignados
                    frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 = True
                    AnyadirPresupAGridApertura
                Else
                    If frmPROCE.g_iAnyadir = 1 Then
                        Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).Pres1Nivel4 = m_oPresupuestos1Asignados
                        frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).HayPresAnualTipo1 = True
                        AnyadirPresupAGridApertura
                    Else
                        If g_sGrupoNuevo = "TODOS" Then 'Asignar el pres a todos los grupos
                            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                                Set oGrupo.Pres1Nivel4 = m_oPresupuestos1Asignados
                                oGrupo.HayPresAnualTipo1 = True
                            Next
                        Else
                            Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupoNuevo).Pres1Nivel4 = m_oPresupuestos1Asignados
                            frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupoNuevo).HayPresAnualTipo1 = True
                        End If
                    End If
                End If
          
        Case "GRUPON"
                Set frmPROCE.g_oGrupoSeleccionado.Pres1Nivel4 = m_oPresupuestos1Asignados
                frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 = True
                            
        Case "ELIM_ITEMS" 'Eliminar �tems
                Set frmPROCE.g_oPresupuestos1ItemModif = m_oPresupuestos1Asignados
                
        Case "AFTER_INSERT" 'Obligatorio el presupuesto al a�adir un �tem a la grid de �tems
                udtTeserror = m_oPresupuestos1Asignados.AlmacenarPresDeItem(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, False)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    Exit Sub
                End If
                
        Case "PROCE_PLANT" 'Proceso nuevo usando una plantilla
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres1Nivel4 = m_oPresupuestos1Asignados
                    If m_oPresupuestos1Asignados.Count = 0 Then
                        frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 = False
                    Else
                        frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 = True
                    End If
                Else
                    Set frmPROCE.g_oGrupoSeleccionado.Pres1Nivel4 = m_oPresupuestos1Asignados
                    If m_oPresupuestos1Asignados.Count = 0 Then
                        frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 = False
                    Else
                        frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 = True
                    End If
                End If
                AnyadirPresupAGridApertura
         
        Case "MODIF_PRES"  'Modificar presupuesto de proceso,grupo o �tem
    
                Select Case g_iAmbitoPresup
                        Case 1, 2
                            If g_iAmbitoPresup = 1 Then
                                udtTeserror = m_oPresupuestos1Asignados.AlmacenarPresDeProceso(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, False)
                                If udtTeserror.NumError <> TESnoerror Then
                                    Screen.MousePointer = vbNormal
                                    basErrores.TratarError udtTeserror
                                    Exit Sub
                                End If
                                If m_oPresupuestos1Asignados.Count = 0 Then
                                    frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 = False
                                Else
                                    frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 = True
                                End If
                                basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " TipoPres:" & g_iTipoPres
                                
                            Else
                                udtTeserror = m_oPresupuestos1Asignados.AlmacenarPresDeGrupo(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, False)
                                If udtTeserror.NumError <> TESnoerror Then
                                    Screen.MousePointer = vbNormal
                                    basErrores.TratarError udtTeserror
                                    Exit Sub
                                End If
                                If m_oPresupuestos1Asignados.Count = 0 Then
                                    frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 = False
                                Else
                                    frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 = True
                                End If
                                basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " Grupo:" & frmPROCE.g_oGrupoSeleccionado.Codigo & " TipoPres:" & g_iTipoPres
                            End If
                            AnyadirPresupAGridApertura
                        Case 3
                                udtTeserror = m_oPresupuestos1Asignados.AlmacenarPresDeItem(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, False)
                                If udtTeserror.NumError <> TESnoerror Then
                                    Screen.MousePointer = vbNormal
                                    basErrores.TratarError udtTeserror
                                    Exit Sub
                                End If
                                AnyadirPresupAGridItems
                                basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " Item:" & frmPRESItem.g_oItem.Id & " TipoPres:" & g_iTipoPres
                                
                End Select
        Case "frmPEDIDOS" 'Se lleva en clases, no est� guardado
                For Each oPRES1 In m_oPresupuestos1Asignados
                    oPRES1.CargaPorId
                Next
                g_oOrigen.m_bValidarCambioPresup = False
                Set g_oOrigen.g_oLineaSeleccionada.Presupuestos1 = m_oPresupuestos1Asignados
                
        Case "frmSeguimiento" 'Esta guardado y hay que modificar
            If Not m_oPresupuestos1Asignados Is Nothing Then
                'Gestamp Validez
                For Each oPRES1 In m_oPresupuestos1Asignados
                    oPRES1.CargaPorId
                Next
                
                If gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_directo) Then
                    If Not frmSeguimiento.MiraMapper(False, 1, m_oPresupuestos1Asignados) Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If
                
                'Cuando se creoa una linea de pedido desde seguimiento, a�n no esta grabada y el ID es negativo, en ese caso no hay que grabar todav�a.
                If frmSeguimiento.g_oLineaPedido.Id > 0 Then
                    udtTeserror = m_oPresupuestos1Asignados.AlmacenarPresDeLineaPedido(NullToDbl0(frmSeguimiento.g_oLineaPedido.Id), False, NullToDbl0(frmSeguimiento.g_oLineaPedido.Anyo), NullToStr(frmSeguimiento.g_oLineaPedido.GMN1Proce), NullToDbl0(frmSeguimiento.g_oLineaPedido.ProceCod), frmSeguimiento.g_oLineaPedido.Item, gCodPersonaUsuario, gvarCodUsuario)
                    If udtTeserror.NumError <> TESnoerror Then
                        TratarError udtTeserror
                        Exit Sub
                    End If
                End If
                
                Set frmSeguimiento.g_oLineaPedido.Presupuestos1 = m_oPresupuestos1Asignados
            Else
                Set frmSeguimiento.g_oLineaPedido.Presupuestos1 = oFSGSRaiz.Generar_CPresProyectosNivel4
            End If
                
    End Select
    
    Screen.MousePointer = vbNormal
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "AlmacenarPres1", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Almacena la seleccion de presupuestos de tipo 2 seleccionado
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdAceptar_click sistema; Tiempo m�ximo:0</remarks>

Private Sub AlmacenarPres2()
Dim udtTeserror As TipoErrorSummit
Dim oGrupo As CGrupo
Dim iDef As Integer
Dim oPRES2 As CPresconNivel4
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
        Case "frmFormularios", "frmDesgloseValores", "frmSolicitudDetalle", "frmSolicitudDesglose", "frmSolicitudDesgloseP", "frmSOLAbrirFaltan"
            Dim sValor As String
            Dim sTexto As String
            sValor = ""
            sTexto = ""
            For Each oPRES2 In m_oPresupuestos2Asignados
                sValor = sValor & oPRES2.IDNivel & "_" & oPRES2.Id & "_" & DblToSQLFloat(garSimbolos, oPRES2.Porcen) & "#"
            Next
            If sValor <> "" Then sValor = Mid(sValor, 1, Len(sValor) - 1)
            
            Select Case g_sOrigen
                Case "frmSolicitudDetalle"
                    frmSolicitudes.g_ofrmDetalleSolic.MostrarPresSeleccionado sValor, 2
                Case "frmFormularios"
                    If m_oPresupuestos2Asignados.Count = 1 Then
                        frmFormularios.MostrarPresSeleccionado sValor, 2, m_oPresupuestos2Asignados.Item(1).BajaLog
                    Else
                        frmFormularios.MostrarPresSeleccionado sValor, 2
                    End If
                Case "frmDesgloseValores"
                    frmDesgloseValores.MostrarPresSeleccionado sValor, 2
                Case "frmSolicitudDesglose"
                    frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.MostrarPresSeleccionado sValor, 2
                Case "frmSolicitudDesgloseP"
                    g_oOrigen.g_ofrmDetalleSolic.g_ofrmDesgloseValores.MostrarPresSeleccionado sValor, 2
                Case "frmSOLAbrirFaltan"
                    frmSOLAbrirFaltan.MostrarPresSeleccionado sValor, 2
            End Select

        Case "frmPROCEModifConfig" 'Modificaci�n de la configuraci�n de un proceso existente
    
                If g_iCol = 2 Then 'Pasa a PROCESO
                    iDef = frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2
                    frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso
                    Set frmPROCE.g_oProcesoSeleccionado.Pres2Nivel4 = m_oPresupuestos2Asignados
                    udtTeserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(PresAnualTipo2)
                    If udtTeserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError udtTeserror
                        frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = iDef
                        frmPROCE.MostrarConfiguracion
                        Unload Me
                        Set oGrupo = Nothing
                        Exit Sub
                    End If
                    frmPROCE.sdbcGrupo.Columns(0).Value = ""
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefPresAnualTipo2 Then
                            oGrupo.DefPresAnualTipo2 = False
                            oGrupo.HayPresAnualTipo2 = False
                        End If
                    Next
                    If StrToDbl0(txtPorcenAsig.ToolTipText) = 0 Then
                        frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 = False
                    Else
                        frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 = True
                    End If
                End If
                If g_iCol = 3 Then 'Pasa a GRUPO
                    frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo
                    Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems(g_iLstGrupoItem).Tag))
                    If oGrupo Is Nothing Then
                        Screen.MousePointer = vbNormal
                        Set oGrupo = Nothing
                        Exit Sub
                    End If
                    oGrupo.DefPresAnualTipo2 = True
                    Set oGrupo.Pres2Nivel4 = m_oPresupuestos2Asignados
                    If StrToDbl0(txtPorcenAsig.ToolTipText) = 0 Then
                        oGrupo.HayPresAnualTipo2 = False
                    Else
                        oGrupo.HayPresAnualTipo2 = True
                    End If
                    frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 = False
                End If
    
        Case "frmPROCEElimGrupo" 'Eliminar un grupo
                Set frmPROCE.g_oProcesoSeleccionado.Pres2Nivel4 = m_oPresupuestos2Asignados
    
        Case "MODIF_ITEMS" 'Modificar m�ltiples �tems desde frmItemModificarValores
                Set frmItemModificarValores.g_oPresupuestos2Nivel4 = m_oPresupuestos2Asignados
                
        Case "PROCE_MODIFITEMS" 'Modificar presupuesto de m�ltiples �tems desde frmPRESItem
                udtTeserror = m_oPresupuestos2Asignados.AlmacenarPresDeMultiplesItems(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_arItems)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    Exit Sub
                End If
                AnyadirPresupAGridItems
                
        Case "CANT_ITEMS", "PRES_ITEMS" 'Modificar la cantidad o el precio de m�ltiples �tems
                Set frmDatoAmbitoProce.g_oPresupuestos2Nivel4 = m_oPresupuestos2Asignados
        
        Case "ITEM" 'Modificar la cantidad o el precio de un �tem
                Set frmPROCE.g_oPresupuestos2ItemModif = m_oPresupuestos2Asignados
        
        Case "ANYA_ITEMS" 'Anyadir m�ltiples �tems
                Set frmItemsWizard.g_oPresupuestos2Nivel4 = m_oPresupuestos2Asignados
                
        Case "ANYA_ITEM", "XLS_ITEMS" 'Anyadir un �tem desde la grid de �tems o varios desde Excel
                Set frmPROCE.g_oPresupuestos2ItemModif = m_oPresupuestos2Asignados
                
        Case "DUPLICAR_ITEMS" 'Duplicar �tems
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres2Nivel4 = m_oPresupuestos2Asignados
                Else
                    If g_iAmbitoPresup = 2 Then
                        Set frmPROCE.g_oGrupoSeleccionado.Pres2Nivel4 = m_oPresupuestos2Asignados
                    End If
                End If
        
        Case "GRUPOS" 'Grupo antiguo (Cambiar items de grupo)
                Set frmDatoAmbitoProce.g_oPresupuestos2Nivel4 = m_oPresupuestos2Asignados
        
        Case "GRUPO_NUEVO" 'Grupo nuevo (Cambiar items de grupo)
                Set frmDatoAmbitoProce.g_oPresupuestos2Nivel4Nuevos = m_oPresupuestos2Asignados
        
        Case "PROCEN" 'Proceso nuevo, presupuesto a nivel de proceso
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres2Nivel4 = m_oPresupuestos2Asignados
                    frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 = True
                    AnyadirPresupAGridApertura
                Else
                    If frmPROCE.g_iAnyadir = 1 Then
                        Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).Pres2Nivel4 = m_oPresupuestos2Asignados
                        frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).HayPresAnualTipo2 = True
                        AnyadirPresupAGridApertura
                    Else
                        If g_sGrupoNuevo = "TODOS" Then 'Asignar el pres a todos los grupos
                            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                                Set oGrupo.Pres2Nivel4 = m_oPresupuestos2Asignados
                                oGrupo.HayPresAnualTipo2 = True
                            Next
                        Else
                            Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupoNuevo).Pres2Nivel4 = m_oPresupuestos2Asignados
                            frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupoNuevo).HayPresAnualTipo2 = True
                        End If
                    End If
                End If
           
        Case "GRUPON"
                Set frmPROCE.g_oGrupoSeleccionado.Pres2Nivel4 = m_oPresupuestos2Asignados
                frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 = True
                
        Case "ELIM_ITEMS" 'Eliminar �tems
                Set frmPROCE.g_oPresupuestos2ItemModif = m_oPresupuestos2Asignados
        
        Case "AFTER_INSERT" 'Obligatorio el presupuesto al a�adir un �tem a la grid de �tems
                udtTeserror = m_oPresupuestos2Asignados.AlmacenarPresDeItem(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, False)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    Exit Sub
                End If
        
        Case "PROCE_PLANT" 'Proceso nuevo usando una plantilla
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres2Nivel4 = m_oPresupuestos2Asignados
                    If m_oPresupuestos2Asignados.Count = 0 Then
                        frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 = False
                    Else
                        frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 = True
                    End If
                Else
                    Set frmPROCE.g_oGrupoSeleccionado.Pres2Nivel4 = m_oPresupuestos2Asignados
                    If m_oPresupuestos2Asignados.Count = 0 Then
                        frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 = False
                    Else
                        frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 = True
                    End If
                End If
                AnyadirPresupAGridApertura
        
        Case "MODIF_PRES"  'Modificar presupuesto de proceso,grupo o �tem
                Select Case g_iAmbitoPresup
                    Case 1, 2
                        If g_iAmbitoPresup = 1 Then
                            udtTeserror = m_oPresupuestos2Asignados.AlmacenarPresDeProceso(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, False)
                            If udtTeserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                basErrores.TratarError udtTeserror
                                Exit Sub
                            End If
                            If m_oPresupuestos2Asignados.Count = 0 Then
                                frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 = False
                            Else
                                frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 = True
                            End If
                            basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " TipoPres:" & g_iTipoPres
                            
                        Else
                            udtTeserror = m_oPresupuestos2Asignados.AlmacenarPresDeGrupo(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, False)
                            If udtTeserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                basErrores.TratarError udtTeserror
                                Exit Sub
                            End If
                            If m_oPresupuestos2Asignados.Count = 0 Then
                                frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 = False
                            Else
                                frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 = True
                            End If
                            basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " Grupo:" & frmPROCE.g_oGrupoSeleccionado.Codigo & " TipoPres:" & g_iTipoPres
                        End If
                        AnyadirPresupAGridApertura
                        
                    Case 3
                            udtTeserror = m_oPresupuestos2Asignados.AlmacenarPresDeItem(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, False)
                            If udtTeserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                basErrores.TratarError udtTeserror
                                Exit Sub
                            End If
                            AnyadirPresupAGridItems
                            basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Item:" & frmPRESItem.g_oItem.Id & " TipoPres:" & g_iTipoPres
                            
                End Select
        
        Case "frmPEDIDOS" 'Se lleva en clases, no est� guardado
                For Each oPRES2 In m_oPresupuestos2Asignados
                    oPRES2.CargaPorId
                Next
                g_oOrigen.m_bValidarCambioPresup = False
                Set g_oOrigen.g_oLineaSeleccionada.Presupuestos2 = m_oPresupuestos2Asignados
                
        Case "frmSeguimiento" 'Esta guardado y hay que modificar
            If Not m_oPresupuestos2Asignados Is Nothing Then
                'Gestamp Validez
                For Each oPRES2 In m_oPresupuestos2Asignados
                    oPRES2.CargaPorId
                Next
                
                    If gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_directo) Then
                        If Not frmSeguimiento.MiraMapper(False, 2, m_oPresupuestos2Asignados) Then
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                    End If
                
                'Cuando se creoa una linea de pedido desde seguimiento, a�n no esta grabada y el ID es negativo, en ese caso no hay que grabar todav�a.
                If frmSeguimiento.g_oLineaPedido.Id > 0 Then
                    udtTeserror = m_oPresupuestos2Asignados.AlmacenarPresDeLineaPedido(NullToDbl0(frmSeguimiento.g_oLineaPedido.Id), False, NullToDbl0(frmSeguimiento.g_oLineaPedido.Anyo), NullToStr(frmSeguimiento.g_oLineaPedido.GMN1Proce), NullToDbl0(frmSeguimiento.g_oLineaPedido.ProceCod), frmSeguimiento.g_oLineaPedido.Item, gCodPersonaUsuario, gvarCodUsuario)
                    If udtTeserror.NumError <> TESnoerror Then
                        TratarError udtTeserror
                        Exit Sub
                    End If
                End If

                Set frmSeguimiento.g_oLineaPedido.Presupuestos2 = m_oPresupuestos2Asignados
            Else
                Set frmSeguimiento.g_oLineaPedido.Presupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel4
            End If
    End Select
    
    Screen.MousePointer = vbNormal
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "AlmacenarPres2", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Almacena la seleccion de presupuestos de tipo 3 seleccionado
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdAceptar_click sistema; Tiempo m�ximo:0</remarks>

Private Sub AlmacenarPres3()
Dim udtTeserror As TipoErrorSummit
Dim oGrupo As CGrupo
Dim iDef As Integer
Dim oPRES3 As CPresConcep3Nivel4
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
        Case "frmFormularios", "frmDesgloseValores", "frmSolicitudDetalle", "frmSolicitudDesglose", "frmSolicitudDesgloseP", "frmSOLAbrirFaltan"
            Dim sValor As String
            Dim sTexto As String
            sValor = ""
            sTexto = ""
            For Each oPRES3 In m_oPresupuestos3Asignados
                sValor = sValor & oPRES3.IDNivel & "_" & oPRES3.Id & "_" & DblToSQLFloat(garSimbolos, oPRES3.Porcen) & "#"
            
            Next
            If sValor <> "" Then
                sValor = Mid(sValor, 1, Len(sValor) - 1)
            End If
            Select Case g_sOrigen
                Case "frmSolicitudDetalle"
                    frmSolicitudes.g_ofrmDetalleSolic.MostrarPresSeleccionado sValor, 3
                Case "frmFormularios"
                    If m_oPresupuestos3Asignados.Count = 1 Then
                        frmFormularios.MostrarPresSeleccionado sValor, 3, m_oPresupuestos3Asignados.Item(1).BajaLog
                    Else
                        frmFormularios.MostrarPresSeleccionado sValor, 3
                    End If
                Case "frmDesgloseValores"
                    frmDesgloseValores.MostrarPresSeleccionado sValor, 3
                Case "frmSolicitudDesglose"
                    frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.MostrarPresSeleccionado sValor, 3
                Case "frmSolicitudDesgloseP"
                    g_oOrigen.g_ofrmDetalleSolic.g_ofrmDesgloseValores.MostrarPresSeleccionado sValor, 3
                Case "frmSOLAbrirFaltan"
                    frmSOLAbrirFaltan.MostrarPresSeleccionado sValor, 3
            End Select
            
        Case "frmPROCEModifConfig" 'Modificaci�n de la configuraci�n de un proceso existente
    
                If g_iCol = 2 Then 'Pasa a PROCESO
                    iDef = frmPROCE.g_oProcesoSeleccionado.DefPresTipo1
                    frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso
                    Set frmPROCE.g_oProcesoSeleccionado.Pres3Nivel4 = m_oPresupuestos3Asignados
                    udtTeserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(PresTipo1)
                    If udtTeserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError udtTeserror
                        frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = iDef
                        frmPROCE.MostrarConfiguracion
                        Unload Me
                        Set oGrupo = Nothing
                        Exit Sub
                    End If
                    frmPROCE.sdbcGrupo.Columns(0).Value = ""
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefPresTipo1 Then
                            oGrupo.DefPresTipo1 = False
                            oGrupo.HayPresTipo1 = False
                        End If
                    Next
                    If StrToDbl0(txtPorcenAsig.ToolTipText) = 0 Then
                        frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 = False
                    Else
                        frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 = True
                    End If
                End If
                If g_iCol = 3 Then 'Pasa a GRUPO
                    frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo
                    Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems(g_iLstGrupoItem).Tag))
                    If oGrupo Is Nothing Then
                        Screen.MousePointer = vbNormal
                        Set oGrupo = Nothing
                        Exit Sub
                    End If
                    
                    oGrupo.DefPresTipo1 = True
                    Set oGrupo.Pres3Nivel4 = m_oPresupuestos3Asignados
                    If StrToDbl0(txtPorcenAsig.ToolTipText) = 0 Then
                        oGrupo.HayPresTipo1 = False
                    Else
                        oGrupo.HayPresTipo1 = True
                    End If
                    frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 = False
                End If
    
        Case "frmPROCEElimGrupo" 'Eliminar un grupo
                Set frmPROCE.g_oProcesoSeleccionado.Pres3Nivel4 = m_oPresupuestos3Asignados
    
        Case "MODIF_ITEMS" 'Modificar m�ltiples �tems desde frmItemModificarValores
                Set frmItemModificarValores.g_oPresupuestos3Nivel4 = m_oPresupuestos3Asignados
                
        Case "PROCE_MODIFITEMS" 'Modificar presupuesto de m�ltiples �tems desde frmPRESItem
                udtTeserror = m_oPresupuestos3Asignados.AlmacenarPresDeMultiplesItems(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_arItems)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    Exit Sub
                End If
                AnyadirPresupAGridItems
        
        Case "CANT_ITEMS", "PRES_ITEMS" 'Modificar la cantidad o el precio de m�ltiples �tems
                Set frmDatoAmbitoProce.g_oPresupuestos3Nivel4 = m_oPresupuestos3Asignados
                
        Case "ITEM" 'Modificar la cantidad o el precio de un �tem
                Set frmPROCE.g_oPresupuestos3ItemModif = m_oPresupuestos3Asignados
                
        Case "ANYA_ITEMS" 'Anyadir m�ltiples �tems
                Set frmItemsWizard.g_oPresupuestos3Nivel4 = m_oPresupuestos3Asignados
    
        Case "ANYA_ITEM", "XLS_ITEMS" 'Anyadir un �tem desde la grid de �tems ovarios desde excel
                Set frmPROCE.g_oPresupuestos3ItemModif = m_oPresupuestos3Asignados
    
        Case "DUPLICAR_ITEMS" 'Duplicar �tems
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres3Nivel4 = m_oPresupuestos3Asignados
                Else
                    If g_iAmbitoPresup = 2 Then
                        Set frmPROCE.g_oGrupoSeleccionado.Pres3Nivel4 = m_oPresupuestos3Asignados
                    End If
                End If
    
        Case "GRUPOS" 'Grupo antiguo (Cambiar items de grupo)
                Set frmDatoAmbitoProce.g_oPresupuestos3Nivel4 = m_oPresupuestos3Asignados
        
        Case "GRUPO_NUEVO" 'Grupo nuevo (Cambiar items de grupo)
                Set frmDatoAmbitoProce.g_oPresupuestos3Nivel4Nuevos = m_oPresupuestos3Asignados
        
        Case "PROCEN" 'Proceso nuevo, presupuesto a nivel de proceso
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres3Nivel4 = m_oPresupuestos3Asignados
                    frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 = True
                    AnyadirPresupAGridApertura
                Else
                    If frmPROCE.g_iAnyadir = 1 Then
                        Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).Pres3Nivel4 = m_oPresupuestos3Asignados
                        frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).HayPresTipo1 = True
                        AnyadirPresupAGridApertura
                    Else
                        If g_sGrupoNuevo = "TODOS" Then 'Asignar el pres a todos los grupos
                            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                                Set oGrupo.Pres3Nivel4 = m_oPresupuestos3Asignados
                                oGrupo.HayPresTipo1 = True
                            Next
                        Else
                            Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupoNuevo).Pres3Nivel4 = m_oPresupuestos3Asignados
                            frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupoNuevo).HayPresTipo1 = True
                        End If
                    End If
                End If
        
        Case "GRUPON"
                Set frmPROCE.g_oGrupoSeleccionado.Pres3Nivel4 = m_oPresupuestos3Asignados
                frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 = True
                
        Case "ELIM_ITEMS" 'Eliminar �tems
                Set frmPROCE.g_oPresupuestos3ItemModif = m_oPresupuestos3Asignados
        
        Case "AFTER_INSERT" 'Obligatorio el presupuesto al a�adir un �tem a la grid de �tems
                udtTeserror = m_oPresupuestos3Asignados.AlmacenarPresDeItem(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, False)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    Exit Sub
                End If
        
        Case "PROCE_PLANT" 'Proceso nuevo usando una plantilla
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres3Nivel4 = m_oPresupuestos3Asignados
                    If m_oPresupuestos3Asignados.Count = 0 Then
                        frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 = False
                    Else
                        frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 = True
                    End If
                Else
                    Set frmPROCE.g_oGrupoSeleccionado.Pres3Nivel4 = m_oPresupuestos3Asignados
                    If m_oPresupuestos3Asignados.Count = 0 Then
                        frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 = False
                    Else
                        frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 = True
                    End If
                End If
                AnyadirPresupAGridApertura
        
        Case "MODIF_PRES"  'Modificar presupuesto de proceso,grupo o �tem
        
                Select Case g_iAmbitoPresup
                    Case 1, 2
                        If g_iAmbitoPresup = 1 Then
                            udtTeserror = m_oPresupuestos3Asignados.AlmacenarPresDeProceso(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, False)
                            If udtTeserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                basErrores.TratarError udtTeserror
                                Exit Sub
                            End If
                            If m_oPresupuestos3Asignados.Count = 0 Then
                                frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 = False
                            Else
                                frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 = True
                            End If
                            basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " TipoPres:" & g_iTipoPres
                        Else
                            udtTeserror = m_oPresupuestos3Asignados.AlmacenarPresDeGrupo(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, False)
                            If udtTeserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                basErrores.TratarError udtTeserror
                                Exit Sub
                            End If
                            If m_oPresupuestos3Asignados.Count = 0 Then
                                frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 = False
                            Else
                                frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 = True
                            End If
                            basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " Grupo:" & frmPROCE.g_oGrupoSeleccionado.Codigo & " TipoPres:" & g_iTipoPres
                        End If
                        AnyadirPresupAGridApertura
                    
                    Case 3
                        udtTeserror = m_oPresupuestos3Asignados.AlmacenarPresDeItem(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, False)
                        If udtTeserror.NumError <> TESnoerror Then
                            Screen.MousePointer = vbNormal
                            basErrores.TratarError udtTeserror
                            Exit Sub
                        End If
                        AnyadirPresupAGridItems
                        basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " Item:" & frmPRESItem.g_oItem.Id & " TipoPres:" & g_iTipoPres
                End Select
        
        Case "frmPEDIDOS" 'Se lleva en clases, no est� guardado
                For Each oPRES3 In m_oPresupuestos3Asignados
                    oPRES3.CargaPorId
                Next
                g_oOrigen.m_bValidarCambioPresup = False
                Set g_oOrigen.g_oLineaSeleccionada.Presupuestos3 = m_oPresupuestos3Asignados
                
        Case "frmSeguimiento" 'Esta guardado y hay que modificar
            If Not m_oPresupuestos3Asignados Is Nothing Then
                'Gestamp Validez
                For Each oPRES3 In m_oPresupuestos3Asignados
                    oPRES3.CargaPorId
                Next
                
                    If gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_directo) Then
                        If Not frmSeguimiento.MiraMapper(False, 3, m_oPresupuestos3Asignados) Then
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                    End If
                If frmSeguimiento.g_oLineaPedido.Id > 0 Then
                    udtTeserror = m_oPresupuestos3Asignados.AlmacenarPresDeLineaPedido(NullToDbl0(frmSeguimiento.g_oLineaPedido.Id), False, NullToDbl0(frmSeguimiento.g_oLineaPedido.Anyo), NullToStr(frmSeguimiento.g_oLineaPedido.GMN1Proce), NullToDbl0(frmSeguimiento.g_oLineaPedido.ProceCod), frmSeguimiento.g_oLineaPedido.Item, gCodPersonaUsuario, gvarCodUsuario)
                    If udtTeserror.NumError <> TESnoerror Then
                        TratarError udtTeserror
                        Exit Sub
                    End If
                End If
                Set frmSeguimiento.g_oLineaPedido.Presupuestos3 = m_oPresupuestos3Asignados
            Else
                Set frmSeguimiento.g_oLineaPedido.Presupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel4
            End If
                            
    End Select
    
    Screen.MousePointer = vbNormal
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "AlmacenarPres3", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Almacena la seleccion de presupuestos de tipo 4 seleccionado
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdAceptar_click sistema; Tiempo m�ximo:0</remarks>

Private Sub AlmacenarPres4()
Dim udtTeserror As TipoErrorSummit
Dim oGrupo As CGrupo
Dim iDef As Integer
Dim oPRES4 As CPresConcep4Nivel4

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
        Case "frmFormularios", "frmDesgloseValores", "frmSolicitudDetalle", "frmSolicitudDesglose", "frmSolicitudDesgloseP", "frmSOLAbrirFaltan"
            Dim sValor As String
            Dim sTexto As String
            sValor = ""
            sTexto = ""
            For Each oPRES4 In m_oPresupuestos4Asignados
                sValor = sValor & oPRES4.IDNivel & "_" & oPRES4.Id & "_" & DblToSQLFloat(garSimbolos, oPRES4.Porcen) & "#"
            
            Next
            If sValor <> "" Then
                sValor = Mid(sValor, 1, Len(sValor) - 1)
            End If
            Select Case g_sOrigen
                Case "frmSolicitudDetalle"
                    frmSolicitudes.g_ofrmDetalleSolic.MostrarPresSeleccionado sValor, 4
                Case "frmFormularios"
                    If m_oPresupuestos4Asignados.Count = 1 Then
                        frmFormularios.MostrarPresSeleccionado sValor, 4, m_oPresupuestos4Asignados.Item(1).BajaLog
                    Else
                        frmFormularios.MostrarPresSeleccionado sValor, 4
                    End If
                Case "frmDesgloseValores"
                    frmDesgloseValores.MostrarPresSeleccionado sValor, 4
                Case "frmSolicitudDesglose"
                    frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.MostrarPresSeleccionado sValor, 4
                Case "frmSolicitudDesgloseP"
                    g_oOrigen.g_ofrmDetalleSolic.g_ofrmDesgloseValores.MostrarPresSeleccionado sValor, 4
                Case "frmSOLAbrirFaltan"
                    frmSOLAbrirFaltan.MostrarPresSeleccionado sValor, 4
            End Select
    
        Case "frmPROCEModifConfig" 'Modificaci�n de la configuraci�n de un proceso existente
    
                If g_iCol = 2 Then 'Pasa a PROCESO
                    iDef = frmPROCE.g_oProcesoSeleccionado.DefPresTipo2
                    frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso
                    Set frmPROCE.g_oProcesoSeleccionado.Pres4Nivel4 = m_oPresupuestos4Asignados
                    udtTeserror = frmPROCE.g_oProcesoSeleccionado.ModificarConfiguracionProceso(PresTipo2)
                    If udtTeserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError udtTeserror
                        frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = iDef
                        frmPROCE.MostrarConfiguracion
                        Unload Me
                        Set oGrupo = Nothing
                        Exit Sub
                    End If
                    frmPROCE.sdbcGrupo.Columns(0).Value = ""
                    For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                        If oGrupo.DefPresTipo2 Then
                            oGrupo.DefPresTipo2 = False
                            oGrupo.HayPresTipo2 = False
                        End If
                    Next
                    If StrToDbl0(txtPorcenAsig.ToolTipText) = 0 Then
                        frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 = False
                    Else
                        frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 = True
                    End If
                End If
                If g_iCol = 3 Then 'Pasa a GRUPO
                    frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo
                    Set oGrupo = frmPROCE.g_oProcesoSeleccionado.Grupos.Item(CStr(frmPROCEGrupos.lstGrupos.ListItems(g_iLstGrupoItem).Tag))
                    If oGrupo Is Nothing Then
                        Screen.MousePointer = vbNormal
                        Set oGrupo = Nothing
                        Exit Sub
                    End If
                    
                    oGrupo.DefPresTipo2 = True
                    Set oGrupo.Pres4Nivel4 = m_oPresupuestos4Asignados
                    If StrToDbl0(txtPorcenAsig.ToolTipText) = 0 Then
                        oGrupo.HayPresTipo2 = False
                    Else
                        oGrupo.HayPresTipo2 = True
                    End If
                    frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 = False
                End If
    
        Case "frmPROCEElimGrupo" 'Eliminar un grupo
                Set frmPROCE.g_oProcesoSeleccionado.Pres4Nivel4 = m_oPresupuestos4Asignados
    
        Case "MODIF_ITEMS" 'Modificar m�ltiples �tems desde frmItemModificarValores
                Set frmItemModificarValores.g_oPresupuestos4Nivel4 = m_oPresupuestos4Asignados
                                
        Case "PROCE_MODIFITEMS" 'Modificar presupuesto de m�ltiples �tems desde frmPRESItem
                udtTeserror = m_oPresupuestos4Asignados.AlmacenarPresDeMultiplesItems(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_arItems)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    Exit Sub
                End If
                AnyadirPresupAGridItems
                                
        Case "CANT_ITEMS", "PRES_ITEMS" 'Modificar la cantidad o el precio de m�ltiples �tems
                Set frmDatoAmbitoProce.g_oPresupuestos4Nivel4 = m_oPresupuestos4Asignados
                                
        Case "ITEM" 'Modificar la cantidad o el precio de un �tem
                Set frmPROCE.g_oPresupuestos4ItemModif = m_oPresupuestos4Asignados
                                
        Case "ANYA_ITEMS" 'Anyadir m�ltiples �tems
                Set frmItemsWizard.g_oPresupuestos4Nivel4 = m_oPresupuestos4Asignados
    
        Case "ANYA_ITEM", "XLS_ITEMS" 'Anyadir un �tem desde la grid de �tems o varios desde excel
                Set frmPROCE.g_oPresupuestos4ItemModif = m_oPresupuestos4Asignados
    
        Case "DUPLICAR_ITEMS" 'Duplicar �tems
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres4Nivel4 = m_oPresupuestos4Asignados
                Else
                    If g_iAmbitoPresup = 2 Then
                        Set frmPROCE.g_oGrupoSeleccionado.Pres4Nivel4 = m_oPresupuestos4Asignados
                    End If
                End If
    
        Case "GRUPOS" 'Grupo antiguo (Cambiar items de grupo)
                Set frmDatoAmbitoProce.g_oPresupuestos4Nivel4 = m_oPresupuestos4Asignados
    
        Case "GRUPO_NUEVO" 'Grupo nuevo (Cambiar items de grupo)
                Set frmDatoAmbitoProce.g_oPresupuestos4Nivel4Nuevos = m_oPresupuestos4Asignados
    
        Case "PROCEN" 'Proceso nuevo, presupuesto a nivel de proceso
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres4Nivel4 = m_oPresupuestos4Asignados
                    frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 = True
                    AnyadirPresupAGridApertura
                Else
                    If frmPROCE.g_iAnyadir = 1 Then
                        Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).Pres4Nivel4 = m_oPresupuestos4Asignados
                        frmPROCE.g_oProcesoSeleccionado.Grupos.Item(1).HayPresTipo2 = True
                        AnyadirPresupAGridApertura
                    Else
                        If g_sGrupoNuevo = "TODOS" Then 'Asignar el pres a todos los grupos
                            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                                Set oGrupo.Pres4Nivel4 = m_oPresupuestos4Asignados
                                oGrupo.HayPresTipo2 = True
                            Next
                        Else
                            Set frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupoNuevo).Pres4Nivel4 = m_oPresupuestos4Asignados
                            frmPROCE.g_oProcesoSeleccionado.Grupos.Item(g_sGrupoNuevo).HayPresTipo2 = True
                        End If
                    End If
                End If
        
        Case "GRUPON"
                Set frmPROCE.g_oGrupoSeleccionado.Pres4Nivel4 = m_oPresupuestos4Asignados
                frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 = True
                
        Case "ELIM_ITEMS" 'Eliminar �tems
                Set frmPROCE.g_oPresupuestos4ItemModif = m_oPresupuestos4Asignados
        
        Case "AFTER_INSERT" 'Obligatorio el presupuesto al a�adir un �tem a la grid de �tems
                udtTeserror = m_oPresupuestos4Asignados.AlmacenarPresDeItem(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, False)
                If udtTeserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError udtTeserror
                    Exit Sub
                End If
        
        Case "PROCE_PLANT" 'Proceso nuevo usando una plantilla
                If g_iAmbitoPresup = 1 Then
                    Set frmPROCE.g_oProcesoSeleccionado.Pres4Nivel4 = m_oPresupuestos4Asignados
                    If m_oPresupuestos4Asignados.Count = 0 Then
                        frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 = False
                    Else
                        frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 = True
                    End If
                Else
                    Set frmPROCE.g_oGrupoSeleccionado.Pres4Nivel4 = m_oPresupuestos4Asignados
                    If m_oPresupuestos4Asignados.Count = 0 Then
                        frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 = False
                    Else
                        frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 = True
                    End If
                End If
                AnyadirPresupAGridApertura
        
        Case "MODIF_PRES"  'Modificar presupuesto de proceso,grupo o �tem
    
                Select Case g_iAmbitoPresup
                    Case 1, 2
                        If g_iAmbitoPresup = 1 Then
                            udtTeserror = m_oPresupuestos4Asignados.AlmacenarPresDeProceso(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, False)
                            If udtTeserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                basErrores.TratarError udtTeserror
                                Exit Sub
                            End If
                            If m_oPresupuestos4Asignados.Count = 0 Then
                                frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 = False
                            Else
                                frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 = True
                            End If
                            basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " TipoPres:" & g_iTipoPres
                        Else
                            udtTeserror = m_oPresupuestos4Asignados.AlmacenarPresDeGrupo(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, False)
                            If udtTeserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                basErrores.TratarError udtTeserror
                                Exit Sub
                            End If
                            If m_oPresupuestos4Asignados.Count = 0 Then
                                frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 = False
                            Else
                                frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 = True
                            End If
                            basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " Grupo:" & frmPROCE.g_oGrupoSeleccionado.Codigo & " TipoPres:" & g_iTipoPres
                        End If
                        AnyadirPresupAGridApertura
                        
                    Case 3
                        udtTeserror = m_oPresupuestos4Asignados.AlmacenarPresDeItem(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, False)
                        If udtTeserror.NumError <> TESnoerror Then
                            Screen.MousePointer = vbNormal
                            basErrores.TratarError udtTeserror
                            Exit Sub
                        End If
                        AnyadirPresupAGridItems
                        basSeguridad.RegistrarAccion accionessummit.ACCProceModPresAsig, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Proce:" & frmPROCE.sdbcProceCod.Value & " Item:" & frmPRESItem.g_oItem.Id & " TipoPres:" & g_iTipoPres
                End Select

        Case "frmPEDIDOS" 'Se lleva en clases, no est� guardado
                For Each oPRES4 In m_oPresupuestos4Asignados
                    oPRES4.CargaPorId
                Next
                g_oOrigen.m_bValidarCambioPresup = False
                Set g_oOrigen.g_oLineaSeleccionada.Presupuestos4 = m_oPresupuestos4Asignados
                
        Case "frmSeguimiento" 'Esta guardado y hay que modificar
            If Not m_oPresupuestos4Asignados Is Nothing Then
                'Gestamp Validez
                For Each oPRES4 In m_oPresupuestos4Asignados
                    oPRES4.CargaPorId
                Next
                
                    If gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_directo) Then
                        If Not frmSeguimiento.MiraMapper(False, 4, m_oPresupuestos4Asignados) Then
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                    End If
                'Cuando se creoa una linea de pedido desde seguimiento, a�n no esta grabada y el ID es negativo, en ese caso no hay que grabar todav�a.
                If frmSeguimiento.g_oLineaPedido.Id > 0 Then
                    udtTeserror = m_oPresupuestos4Asignados.AlmacenarPresDeLineaPedido(frmSeguimiento.g_oLineaPedido.Id, False, NullToDbl0(frmSeguimiento.g_oLineaPedido.Anyo), NullToStr(frmSeguimiento.g_oLineaPedido.GMN1Proce), frmSeguimiento.g_oLineaPedido.ProceCod, frmSeguimiento.g_oLineaPedido.Item, gCodPersonaUsuario, gvarCodUsuario)
                    If udtTeserror.NumError <> TESnoerror Then
                        TratarError udtTeserror
                        Exit Sub
                    End If
                End If
                Set frmSeguimiento.g_oLineaPedido.Presupuestos4 = m_oPresupuestos4Asignados
            Else
                Set frmSeguimiento.g_oLineaPedido.Presupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel4
            End If
    End Select
    
    Screen.MousePointer = vbNormal
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "AlmacenarPres4", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub AnyadirPresupAGridItems()
Dim sUON As String
Dim sPresup As String
Dim i As Integer
Dim oPres1Niv4 As CPresProyNivel4
Dim oPres2Niv4 As CPresconNivel4
Dim oPres3Niv4 As CPresConcep3Nivel4
Dim oPres4Niv4 As CPresConcep4Nivel4

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_iTipoPres
        Case 1
                frmPRESItem.sdbgPresupuestos.MoveFirst
                For i = 1 To frmPRESItem.sdbgPresupuestos.Rows
                    If frmPRESItem.sdbgPresupuestos.Columns("NUMTIPO").Value = 1 Then
                        frmPRESItem.sdbgPresupuestos.RemoveItem (frmPRESItem.sdbgPresupuestos.AddItemRowIndex(frmPRESItem.sdbgPresupuestos.AddItemBookmark(frmPRESItem.sdbgPresupuestos.Row)))
                    Else
                        frmPRESItem.sdbgPresupuestos.MoveNext
                    End If
                Next
                frmPRESItem.g_bHayPresAnu1 = False
                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                    sUON = ""
                    If NullToStr(oPres1Niv4.UON1) <> "" Then
                        sUON = oPres1Niv4.UON1
                        If NullToStr(oPres1Niv4.UON2) <> "" Then
                            sUON = sUON & "-" & oPres1Niv4.UON2
                            If NullToStr(oPres1Niv4.UON3) <> "" Then
                                sUON = sUON & "-" & oPres1Niv4.UON3
                            End If
                        End If
                    End If
                    sPresup = oPres1Niv4.Anyo & "-" & oPres1Niv4.CodPRES1
                    If oPres1Niv4.CodPRES2 <> "" Then
                        sPresup = sPresup & "-" & oPres1Niv4.CodPRES2
                        If oPres1Niv4.CodPRES3 <> "" Then
                            sPresup = sPresup & "-" & oPres1Niv4.CodPRES3
                            If oPres1Niv4.Cod <> "" Then
                                sPresup = sPresup & "-" & oPres1Niv4.Cod
                            End If
                        End If
                    End If
                    sPresup = sPresup & "-" & oPres1Niv4.Den
                    frmPRESItem.sdbgPresupuestos.AddItem gParametrosGenerales.gsSingPres1 & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & sPresup & Chr(m_lSeparador) & oPres1Niv4.Porcen * 100 & "%" & Chr(m_lSeparador) & 1
                    frmPRESItem.g_bHayPresAnu1 = True
                Next
        Case 2
                frmPRESItem.sdbgPresupuestos.MoveFirst
                For i = 1 To frmPRESItem.sdbgPresupuestos.Rows
                    If frmPRESItem.sdbgPresupuestos.Columns("NUMTIPO").Value = 2 Then
                        frmPRESItem.sdbgPresupuestos.RemoveItem (frmPRESItem.sdbgPresupuestos.AddItemRowIndex(frmPRESItem.sdbgPresupuestos.AddItemBookmark(frmPRESItem.sdbgPresupuestos.Row)))
                    Else
                        frmPRESItem.sdbgPresupuestos.MoveNext
                    End If
                Next
                frmPRESItem.g_bHayPresAnu2 = False
                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                    sUON = ""
                    If NullToStr(oPres2Niv4.UON1) <> "" Then
                        sUON = oPres2Niv4.UON1
                        If NullToStr(oPres2Niv4.UON2) <> "" Then
                            sUON = sUON & "-" & oPres2Niv4.UON2
                            If NullToStr(oPres2Niv4.UON3) <> "" Then
                                sUON = sUON & "-" & oPres2Niv4.UON3
                            End If
                        End If
                    End If
                    sPresup = oPres2Niv4.Anyo & "-" & oPres2Niv4.CodPRES1
                    If oPres2Niv4.CodPRES2 <> "" Then
                        sPresup = sPresup & "-" & oPres2Niv4.CodPRES2
                        If oPres2Niv4.CodPRES3 <> "" Then
                            sPresup = sPresup & "-" & oPres2Niv4.CodPRES3
                            If oPres2Niv4.Cod <> "" Then
                                sPresup = sPresup & "-" & oPres2Niv4.Cod
                            End If
                        End If
                    End If
                    sPresup = sPresup & "-" & oPres2Niv4.Den
                    frmPRESItem.sdbgPresupuestos.AddItem gParametrosGenerales.gsSingPres2 & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & sPresup & Chr(m_lSeparador) & oPres2Niv4.Porcen * 100 & "%" & Chr(m_lSeparador) & 2
                    frmPRESItem.g_bHayPresAnu2 = True
                Next
        Case 3
                frmPRESItem.sdbgPresupuestos.MoveFirst
                For i = 1 To frmPRESItem.sdbgPresupuestos.Rows
                    If frmPRESItem.sdbgPresupuestos.Columns("NUMTIPO").Value = 3 Then
                        frmPRESItem.sdbgPresupuestos.RemoveItem (frmPRESItem.sdbgPresupuestos.AddItemRowIndex(frmPRESItem.sdbgPresupuestos.AddItemBookmark(frmPRESItem.sdbgPresupuestos.Row)))
                    Else
                        frmPRESItem.sdbgPresupuestos.MoveNext
                    End If
                Next
                frmPRESItem.g_bHayPres1 = False
                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                    sUON = ""
                    If NullToStr(oPres3Niv4.UON1) <> "" Then
                        sUON = oPres3Niv4.UON1
                        If NullToStr(oPres3Niv4.UON2) <> "" Then
                            sUON = sUON & "-" & oPres3Niv4.UON2
                            If NullToStr(oPres3Niv4.UON3) <> "" Then
                                sUON = sUON & "-" & oPres3Niv4.UON3
                            End If
                        End If
                    End If
                    sPresup = oPres3Niv4.CodPRES1
                    If oPres3Niv4.CodPRES2 <> "" Then
                        sPresup = sPresup & "-" & oPres3Niv4.CodPRES2
                        If oPres3Niv4.CodPRES3 <> "" Then
                            sPresup = sPresup & "-" & oPres3Niv4.CodPRES3
                            If oPres3Niv4.Cod <> "" Then
                                sPresup = sPresup & "-" & oPres3Niv4.Cod
                            End If
                        End If
                    End If
                    sPresup = sPresup & "-" & oPres3Niv4.Den
                    frmPRESItem.sdbgPresupuestos.AddItem gParametrosGenerales.gsSingPres3 & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & sPresup & Chr(m_lSeparador) & oPres3Niv4.Porcen * 100 & "%" & Chr(m_lSeparador) & 3
                    frmPRESItem.g_bHayPres1 = True
                Next
        Case 4
                frmPRESItem.sdbgPresupuestos.MoveFirst
                For i = 1 To frmPRESItem.sdbgPresupuestos.Rows
                    If frmPRESItem.sdbgPresupuestos.Columns("NUMTIPO").Value = 4 Then
                        frmPRESItem.sdbgPresupuestos.RemoveItem (frmPRESItem.sdbgPresupuestos.AddItemRowIndex(frmPRESItem.sdbgPresupuestos.AddItemBookmark(frmPRESItem.sdbgPresupuestos.Row)))
                    Else
                        frmPRESItem.sdbgPresupuestos.MoveNext
                    End If
                Next
                frmPRESItem.g_bHayPres2 = False
                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                    sUON = ""
                    If NullToStr(oPres4Niv4.UON1) <> "" Then
                        sUON = oPres4Niv4.UON1
                        If NullToStr(oPres4Niv4.UON2) <> "" Then
                            sUON = sUON & "-" & oPres4Niv4.UON2
                            If NullToStr(oPres4Niv4.UON3) <> "" Then
                                sUON = sUON & "-" & oPres4Niv4.UON3
                            End If
                        End If
                    End If
                    sPresup = oPres4Niv4.CodPRES1
                    If oPres4Niv4.CodPRES2 <> "" Then
                        sPresup = sPresup & "-" & oPres4Niv4.CodPRES2
                        If oPres4Niv4.CodPRES3 <> "" Then
                            sPresup = sPresup & "-" & oPres4Niv4.CodPRES3
                            If oPres4Niv4.Cod <> "" Then
                                sPresup = sPresup & "-" & oPres4Niv4.Cod
                            End If
                        End If
                    End If
                    sPresup = sPresup & "-" & oPres4Niv4.Den
                    frmPRESItem.sdbgPresupuestos.AddItem gParametrosGenerales.gsSingPres4 & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & sPresup & Chr(m_lSeparador) & oPres4Niv4.Porcen * 100 & "%" & Chr(m_lSeparador) & 4
                    frmPRESItem.g_bHayPres2 = True
                Next

    End Select

    Set oPres1Niv4 = Nothing
    Set oPres2Niv4 = Nothing
    Set oPres3Niv4 = Nothing
    Set oPres4Niv4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "AnyadirPresupAGridItems", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub AnyadirPresupAGridApertura()
Dim sUON As String
Dim sPresup As String
Dim i As Integer
Dim oPres1Niv4 As CPresProyNivel4
Dim oPres2Niv4 As CPresconNivel4
Dim oPres3Niv4 As CPresConcep3Nivel4
Dim oPres4Niv4 As CPresConcep4Nivel4

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_iTipoPres
        Case 1
                frmPROCE.sdbgPresupuestos.MoveFirst
                For i = 1 To frmPROCE.sdbgPresupuestos.Rows
                    If frmPROCE.sdbgPresupuestos.Columns("NUMTIPO").Value = 1 Then
                        frmPROCE.sdbgPresupuestos.RemoveItem (frmPROCE.sdbgPresupuestos.AddItemRowIndex(frmPROCE.sdbgPresupuestos.AddItemBookmark(frmPROCE.sdbgPresupuestos.Row)))
                    Else
                        frmPROCE.sdbgPresupuestos.MoveNext
                    End If
                Next
                For Each oPres1Niv4 In m_oPresupuestos1Asignados
                    sUON = ""
                    If NullToStr(oPres1Niv4.UON1) <> "" Then
                        sUON = oPres1Niv4.UON1
                        If NullToStr(oPres1Niv4.UON2) <> "" Then
                            sUON = sUON & "-" & oPres1Niv4.UON2
                            If NullToStr(oPres1Niv4.UON3) <> "" Then
                                sUON = sUON & "-" & oPres1Niv4.UON3
                            End If
                        End If
                    End If
                    sPresup = oPres1Niv4.Anyo & "-" & oPres1Niv4.CodPRES1
                    If oPres1Niv4.CodPRES2 <> "" Then
                        sPresup = sPresup & "-" & oPres1Niv4.CodPRES2
                        If oPres1Niv4.CodPRES3 <> "" Then
                            sPresup = sPresup & "-" & oPres1Niv4.CodPRES3
                            If oPres1Niv4.Cod <> "" Then
                                sPresup = sPresup & "-" & oPres1Niv4.Cod
                            End If
                        End If
                    End If
                    sPresup = sPresup & "-" & oPres1Niv4.Den
                    frmPROCE.sdbgPresupuestos.AddItem gParametrosGenerales.gsSingPres1 & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & sPresup & Chr(m_lSeparador) & oPres1Niv4.Porcen * 100 & "%" & Chr(m_lSeparador) & 1
                Next
        Case 2
                frmPROCE.sdbgPresupuestos.MoveFirst
                For i = 1 To frmPROCE.sdbgPresupuestos.Rows
                    If frmPROCE.sdbgPresupuestos.Columns("NUMTIPO").Value = 2 Then
                        frmPROCE.sdbgPresupuestos.RemoveItem (frmPROCE.sdbgPresupuestos.AddItemRowIndex(frmPROCE.sdbgPresupuestos.AddItemBookmark(frmPROCE.sdbgPresupuestos.Row)))
                    Else
                        frmPROCE.sdbgPresupuestos.MoveNext
                    End If
                Next
                For Each oPres2Niv4 In m_oPresupuestos2Asignados
                    sUON = ""
                    If NullToStr(oPres2Niv4.UON1) <> "" Then
                        sUON = oPres2Niv4.UON1
                        If NullToStr(oPres2Niv4.UON2) <> "" Then
                            sUON = sUON & "-" & oPres2Niv4.UON2
                            If NullToStr(oPres2Niv4.UON3) <> "" Then
                                sUON = sUON & "-" & oPres2Niv4.UON3
                            End If
                        End If
                    End If
                    sPresup = oPres2Niv4.Anyo & "-" & oPres2Niv4.CodPRES1
                    If oPres2Niv4.CodPRES2 <> "" Then
                        sPresup = sPresup & "-" & oPres2Niv4.CodPRES2
                        If oPres2Niv4.CodPRES3 <> "" Then
                            sPresup = sPresup & "-" & oPres2Niv4.CodPRES3
                            If oPres2Niv4.Cod <> "" Then
                                sPresup = sPresup & "-" & oPres2Niv4.Cod
                            End If
                        End If
                    End If
                    sPresup = sPresup & "-" & oPres2Niv4.Den
                    frmPROCE.sdbgPresupuestos.AddItem gParametrosGenerales.gsSingPres2 & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & sPresup & Chr(m_lSeparador) & oPres2Niv4.Porcen * 100 & "%" & Chr(m_lSeparador) & 2
                Next
        Case 3
                frmPROCE.sdbgPresupuestos.MoveFirst
                For i = 1 To frmPROCE.sdbgPresupuestos.Rows
                    If frmPROCE.sdbgPresupuestos.Columns("NUMTIPO").Value = 3 Then
                        frmPROCE.sdbgPresupuestos.RemoveItem (frmPROCE.sdbgPresupuestos.AddItemRowIndex(frmPROCE.sdbgPresupuestos.AddItemBookmark(frmPROCE.sdbgPresupuestos.Row)))
                    Else
                        frmPROCE.sdbgPresupuestos.MoveNext
                    End If
                Next
                For Each oPres3Niv4 In m_oPresupuestos3Asignados
                    sUON = ""
                    If NullToStr(oPres3Niv4.UON1) <> "" Then
                        sUON = oPres3Niv4.UON1
                        If NullToStr(oPres3Niv4.UON2) <> "" Then
                            sUON = sUON & "-" & oPres3Niv4.UON2
                            If NullToStr(oPres3Niv4.UON3) <> "" Then
                                sUON = sUON & "-" & oPres3Niv4.UON3
                            End If
                        End If
                    End If
                    sPresup = oPres3Niv4.CodPRES1
                    If oPres3Niv4.CodPRES2 <> "" Then
                        sPresup = sPresup & "-" & oPres3Niv4.CodPRES2
                        If oPres3Niv4.CodPRES3 <> "" Then
                            sPresup = sPresup & "-" & oPres3Niv4.CodPRES3
                            If oPres3Niv4.Cod <> "" Then
                                sPresup = sPresup & "-" & oPres3Niv4.Cod
                            End If
                        End If
                    End If
                    sPresup = sPresup & "-" & oPres3Niv4.Den
                    frmPROCE.sdbgPresupuestos.AddItem gParametrosGenerales.gsSingPres3 & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & sPresup & Chr(m_lSeparador) & oPres3Niv4.Porcen * 100 & "%" & Chr(m_lSeparador) & 3
                Next
        Case 4
                frmPROCE.sdbgPresupuestos.MoveFirst
                For i = 1 To frmPROCE.sdbgPresupuestos.Rows
                    If frmPROCE.sdbgPresupuestos.Columns("NUMTIPO").Value = 4 Then
                        frmPROCE.sdbgPresupuestos.RemoveItem (frmPROCE.sdbgPresupuestos.AddItemRowIndex(frmPROCE.sdbgPresupuestos.AddItemBookmark(frmPROCE.sdbgPresupuestos.Row)))
                    Else
                        frmPROCE.sdbgPresupuestos.MoveNext
                    End If
                Next
                For Each oPres4Niv4 In m_oPresupuestos4Asignados
                    sUON = ""
                    If NullToStr(oPres4Niv4.UON1) <> "" Then
                        sUON = oPres4Niv4.UON1
                        If NullToStr(oPres4Niv4.UON2) <> "" Then
                            sUON = sUON & "-" & oPres4Niv4.UON2
                            If NullToStr(oPres4Niv4.UON3) <> "" Then
                                sUON = sUON & "-" & oPres4Niv4.UON3
                            End If
                        End If
                    End If
                    sPresup = oPres4Niv4.CodPRES1
                    If oPres4Niv4.CodPRES2 <> "" Then
                        sPresup = sPresup & "-" & oPres4Niv4.CodPRES2
                        If oPres4Niv4.CodPRES3 <> "" Then
                            sPresup = sPresup & "-" & oPres4Niv4.CodPRES3
                            If oPres4Niv4.Cod <> "" Then
                                sPresup = sPresup & "-" & oPres4Niv4.Cod
                            End If
                        End If
                    End If
                    sPresup = sPresup & "-" & oPres4Niv4.Den
                    frmPROCE.sdbgPresupuestos.AddItem gParametrosGenerales.gsSingPres4 & Chr(m_lSeparador) & sUON & Chr(m_lSeparador) & sPresup & Chr(m_lSeparador) & oPres4Niv4.Porcen * 100 & "%" & Chr(m_lSeparador) & 4
                Next

    End Select
    
    Set oPres1Niv4 = Nothing
    Set oPres2Niv4 = Nothing
    Set oPres3Niv4 = Nothing
    Set oPres4Niv4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "AnyadirPresupAGridApertura", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub Arrange()

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    SSTabPresupuestos.Height = Me.Height - 1335
    SSTabPresupuestos.Width = Me.Width - 125
    tvwestrorg.Height = SSTabPresupuestos.Height - 870
    tvwestrorg.Width = SSTabPresupuestos.Width - 300
    tvwEstrPres.Height = tvwestrorg.Height - 905
    tvwEstrPres.Width = tvwestrorg.Width
    lblEstrorg.Width = tvwestrorg.Width
    'lblUO.Width = tvwestrPres.Width
    picSepar.Width = SSTabPresupuestos.Width
    picPresup.Top = tvwEstrPres.Top + tvwEstrPres.Height + 50
    picPresup.Width = tvwEstrPres.Width
    cmdAceptar.Top = SSTabPresupuestos.Height + SSTabPresupuestos.Top + 20
    cmdCancelar.Top = cmdAceptar.Top
    cmdRestaurarUO.Top = cmdAceptar.Top
    cmdBuscarUO.Top = cmdAceptar.Top
    cmdRestaurarPres.Top = cmdAceptar.Top
    cmdBuscarPres.Top = cmdAceptar.Top
    cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 100
    cmdCancelar.Left = Me.Width / 2 + 50
    sdbcAnyo.Top = lblUO.Top
    'sdbcAnyo.Left = lblUO.Left + lblUO.Width - sdbcAnyo.Width
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub

Private Sub Cargar(Optional ByVal Anyo As String)
Dim oPresupuestos As Object

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DatosInicio
    
    GenerarEstructuraOrg False
    If SSTabPresupuestos.TabVisible(0) Then 'Se carga el arbol de UON s�lo si la presta�a est� visible
        If basOptimizacion.gTipoDeUsuario <> Administrador Then
            BuscarUONUsuario
        End If
        Select Case g_iTipoPres
        Case 1
            Set oPresupuestos = m_oPresupuestos1Asignados
        Case 2
            Set oPresupuestos = m_oPresupuestos2Asignados
        Case 3
            Set oPresupuestos = m_oPresupuestos3Asignados
        Case 4
            Set oPresupuestos = m_oPresupuestos4Asignados
        End Select

    Else
        Select Case g_iTipoPres
        Case 1
            GenerarEstructuraPresupuestosTipo1 False, True
            Set oPresupuestos = m_oPresupuestos1Asignados
        Case 2
            GenerarEstructuraPresupuestosTipo2 False, True
            Set oPresupuestos = m_oPresupuestos2Asignados
        Case 3
            GenerarEstructuraPresupuestosTipo3 False
            Set oPresupuestos = m_oPresupuestos3Asignados
        Case 4
            GenerarEstructuraPresupuestosTipo4 False
            Set oPresupuestos = m_oPresupuestos4Asignados
        End Select
    End If
    If g_iTipoPres < 3 Then
        If oPresupuestos.Count = 0 Then
            If Anyo = "" Then
                sdbcAnyo.Value = Year(Date)
            Else
                sdbcAnyo.Value = Anyo
            End If
        Else
            sdbcAnyo.Value = oPresupuestos.Item(1).Anyo
        End If
    Else
        sdbcAnyo.Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "Cargar", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub



Private Sub CargarRecursos()
Dim Ador As Ador.Recordset


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESASIG, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        If g_bModif Then
            caption = Ador(0).Value
        End If
        Ador.MoveNext
        lblAbierto.caption = Ador(0).Value
        Ador.MoveNext
        lblAsignado.caption = Ador(0).Value
        Ador.MoveNext
        lblPrendiente.caption = Ador(0).Value
        Ador.MoveNext
        SSTabPresupuestos.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTabPresupuestos.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblUO.caption = Ador(0).Value
        lblEstrorg.caption = Ador(0).Value
        Ador.MoveNext
        lblPresupuesto.caption = Ador(0).Value
        Ador.MoveNext
        lblObjetivo.caption = Ador(0).Value
        Ador.MoveNext
        lblAsignar.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurarUO.caption = Ador(0).Value
        cmdRestaurarPres.caption = Ador(0).Value
        Ador.MoveNext
        cmdBuscarUO.caption = Ador(0).Value
        cmdBuscarPres.caption = Ador(0).Value
        Ador.MoveNext
        lblOtras.caption = Ador(0).Value
        Ador.MoveNext
        If Not g_bModif Then
            caption = Ador(0).Value
        End If
        Ador.MoveNext
        If (g_sOrigen = "frmPEDIDOS" Or g_sOrigen = "frmSeguimiento" Or g_sOrigen = "frmPedidosEmision2") Then
            lblAbierto.caption = Ador(0).Value
        End If
        Ador.Close

    End If

    Set Ador = Nothing
    
    Select Case g_iTipoPres
        Case 1
                Me.caption = Me.caption & " " & gParametrosGenerales.gsSingPres1
                msPresupPlural = gParametrosGenerales.gsPlurPres1
        Case 2
                Me.caption = Me.caption & " " & gParametrosGenerales.gsSingPres2
                msPresupPlural = gParametrosGenerales.gsPlurPres2
        Case 3
                Me.caption = Me.caption & " " & gParametrosGenerales.gsSingPres3
                msPresupPlural = gParametrosGenerales.gsPlurPres3
        Case 4
                Me.caption = Me.caption & " " & gParametrosGenerales.gsSingPres4
                msPresupPlural = gParametrosGenerales.gsPlurPres4
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Public Sub ConfigurarInterfazPresup(ByVal node As MSComctlLib.node)
Dim dPorcen As Double
Dim dPorcentajeHermano As Double
Dim nodoHermano As MSComctlLib.node
Dim dSumaPorcenHijos As Double
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_bModif Then
    'Configuramos seg�n nivel de de distribuci�n m�nimo
    Select Case g_iTipoPres
    
        Case 1
            Select Case m_iNIVPres1
                Case 2
                    If Mid(node.Image, 5, 1) = "1" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 3
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 4
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Or Mid(node.Image, 5, 1) = "3" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case Else
                    picPorcen.Enabled = True
            End Select
    
        Case 2
            Select Case m_iNIVPres2
                Case 2
                    If Mid(node.Image, 5, 1) = "1" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 3
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 4
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Or Mid(node.Image, 5, 1) = "3" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case Else
                    picPorcen.Enabled = True
            End Select
    
        Case 3
            Select Case m_iNIVPres3
                Case 2
                    If Mid(node.Image, 5, 1) = "1" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 3
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 4
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Or Mid(node.Image, 5, 1) = "3" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case Else
                    picPorcen.Enabled = True
            End Select
    
        Case 4
            Select Case m_iNIVPres4
                Case 2
                    If Mid(node.Image, 5, 1) = "1" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 3
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 4
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Or Mid(node.Image, 5, 1) = "3" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case Else
                    picPorcen.Enabled = True
            End Select
    
    End Select
Else
    picPorcen.Enabled = False
End If
    
    MostrarDatosBarraInf
    
    m_stexto = node.Text
    Slider1.Max = 100
    If node.Image = "PRESA" Then
        dPorcen = DevolverPorcentaje(node)
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = CDec(dPorcen * 100)
        m_bRespetarPorcen = True
        If dPorcen < 32767 Then
            Slider1.Value = Int(CDec(dPorcen * 100))
        End If
        
        If txtAbierto.Visible Then
            m_bRespetarPorcen = True
            txtAsignar.Text = dPorcen * g_dblAbierto
        End If
        If Slider1.Value = 0 Then
            If Int(100 - CDec(m_dblPorcenAsig * 100)) > 1 Then
                Slider1.Max = Int(100 - CDec(m_dblPorcenAsig * 100))
            Else
                Slider1.Max = 100
            End If
        Else
            Slider1.Max = Slider1.Value + Int(100 - CDec(m_dblPorcenAsig * 100))
        End If
        m_bRespetarPorcen = False
    Else
        If m_bRestringirMultiplePres And node.Image <> "Raiz" And val(txtPorcenPend.Text) <> 0 And val(txtPorcenPend.Text) <> 100 Then
            oMensajes.ImposibleImputarMultiplesPresupuestos (msPresupPlural)
            picPorcen.Enabled = False
            Exit Sub
        End If
        
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        If txtAbierto.Visible Then
            m_bRespetarPorcen = True
            txtAsignar.Text = ""
        End If
        If Int(100 - CDec(m_dblPorcenAsig * 100)) > 1 Then
            Slider1.Max = Int(100 - CDec(m_dblPorcenAsig * 100))
        Else
            Slider1.Max = 100
        End If

        m_bRespetarPorcen = False
    End If
    
    If picPorcen.Enabled And node.Image <> "PRESA" Then
        Dim scod1 As String
        Dim scod2 As String
        Dim scod3 As String
        Dim scod4 As String
        Dim oPres1Niv4 As CPresProyNivel4
        Dim oPres2Niv4 As CPresconNivel4
        Dim oPres3Niv4 As CPresConcep3Nivel4
        Dim oPres4Niv4 As CPresConcep4Nivel4
        Dim nodo As MSComctlLib.node
        
        If CDec(m_dblPorcenAsig) < 1 And SusPadresNoTienenAsignaciones(node) Then
            dSumaPorcenHijos = SumaDeLosHijos(node)
            If dSumaPorcenHijos > 0 Then
                'Si los hijos tienen porcentajes hay que poner esa suma
                LimpiarHijos node
                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
            Else
                Slider1.Value = Slider1.Max
            End If
        Else
            Select Case g_iTipoPres
            
                Case 1
                    If m_oPresupuestos1Asignados.Count < 2 Then
                        If m_oPresupuestos1Asignados.Count = 0 Then
                            Slider1.Value = Slider1.Max
                        Else
                        'Tenemos que quitar el presupuesto al que lo lleva ahora para asignarselo al nuevo
                                
                            For Each oPres1Niv4 In m_oPresupuestos1Asignados
                                If oPres1Niv4.Anyo = sdbcAnyo.Value And NullToStr(oPres1Niv4.UON1) = g_sUON1 And NullToStr(oPres1Niv4.UON2) = g_sUON2 And NullToStr(oPres1Niv4.UON3) = g_sUON3 Then
                                    Select Case oPres1Niv4.IDNivel
                                        Case 1
                                            scod1 = oPres1Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres1Niv4.CodPRES1))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES1" & scod1)
                                            nodo.Image = "PRES1"
                                        Case 2
                                            scod1 = oPres1Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres1Niv4.CodPRES1))
                                            scod2 = oPres1Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPres1Niv4.CodPRES2))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES2" & scod1 & scod2)
                                            nodo.Image = "PRES2"
                                        Case 3
                                            scod1 = oPres1Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres1Niv4.CodPRES1))
                                            scod2 = oPres1Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPres1Niv4.CodPRES2))
                                            scod3 = oPres1Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPres1Niv4.CodPRES3))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                                            nodo.Image = "PRES3"
                                        Case 4
                                            scod1 = oPres1Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres1Niv4.CodPRES1))
                                            scod2 = oPres1Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPres1Niv4.CodPRES2))
                                            scod3 = oPres1Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPres1Niv4.CodPRES3))
                                            scod4 = oPres1Niv4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(oPres1Niv4.Cod))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                                            nodo.Image = "PRES4"
                                    End Select
                                    dPorcentajeHermano = DevolverPorcentaje(nodo)
                                    nodo.Text = QuitarPorcentaje(nodo.Text)
                                    AsignarPres 0, nodo
                                    txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                                Else
                                    'hay uno al 100% pero en otro a�o.
                                    'Solo se cambia si el usuario tiene permisos
                                    sCod = EsPresupDelUsuario(oPres1Niv4)
                                    If sCod <> "" Then
                                        m_dblPorcenAsig = m_dblPorcenAsig - oPres1Niv4.Porcen
                                        Set m_oPresupuestos1Asignados = oFSGSRaiz.Generar_CPresProyectosNivel4
                                        Set m_oPresupuestos1Otros = oFSGSRaiz.Generar_CPresProyectosNivel4
                                        ConfigurarEtiquetas 1
                                        Slider1.Max = 100
                                        Slider1.Value = Slider1.Max
                                    End If
                                End If
                                
                            Next
                        
                        End If
                    Else
                        'Hay m�s de un presupuesto asignado. Si el nodo solo tiene un hermano asignado, o un padre entonces hay que poner ese valor en el nuevo nodo pulsado
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            ' Si tiene asignaciones m�s abajo hay que poner la suma de esos
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                dPorcentajeHermano = DevolverPorcentaje(nodoHermano)
                                nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                                nodoHermano.Image = "PRES" & Mid(nodoHermano.Tag, 5, 1)
                                AsignarPres 0, nodoHermano
                                txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                            End If
                        Else
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                Slider1.Max = 100
                            End If
                        End If
                    
                    End If
            
                Case 2
                    If m_oPresupuestos2Asignados.Count < 2 Then
                        If m_oPresupuestos2Asignados.Count = 0 Then
                            Slider1.Value = Slider1.Max
                        Else
                        'Tenemos que quitar el presupuesto al que lo lleva ahora para asignarselo al nuevo
                            For Each oPres2Niv4 In m_oPresupuestos2Asignados
                                If oPres2Niv4.Anyo = sdbcAnyo.Value And NullToStr(oPres2Niv4.UON1) = g_sUON1 And NullToStr(oPres2Niv4.UON2) = g_sUON2 And NullToStr(oPres2Niv4.UON3) = g_sUON3 Then
                                    Select Case oPres2Niv4.IDNivel
                                        Case 1
                                            scod1 = oPres2Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres2Niv4.CodPRES1))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES1" & scod1)
                                            nodo.Image = "PRES1"
                                        Case 2
                                            scod1 = oPres2Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPres2Niv4.CodPRES1))
                                            scod2 = oPres2Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPres2Niv4.CodPRES2))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES2" & scod1 & scod2)
                                            nodo.Image = "PRES2"
                                        
                                        Case 3
                                            scod1 = oPres2Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPres2Niv4.CodPRES1))
                                            scod2 = oPres2Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPres2Niv4.CodPRES2))
                                            scod3 = oPres2Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPres2Niv4.CodPRES3))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                                            nodo.Image = "PRES3"
                                        
                                        Case 4
                                            scod1 = oPres2Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPres2Niv4.CodPRES1))
                                            scod2 = oPres2Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPres2Niv4.CodPRES2))
                                            scod3 = oPres2Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPres2Niv4.CodPRES3))
                                            scod4 = oPres2Niv4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(oPres2Niv4.Cod))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                                            nodo.Image = "PRES4"
                                            
                                    End Select
                                    dPorcentajeHermano = DevolverPorcentaje(nodo)
                                    nodo.Text = QuitarPorcentaje(nodo.Text)
                                    AsignarPres 0, nodo
                                    txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                               
                                Else
                                    'hay uno al 100% pero en otro a�o.... que liada.
                                    sCod = EsPresupDelUsuario(oPres2Niv4)
                                    If sCod <> "" Then
                                        m_dblPorcenAsig = m_dblPorcenAsig - oPres2Niv4.Porcen
                                        Set m_oPresupuestos2Asignados = oFSGSRaiz.Generar_CPresContablesNivel4
                                        Set m_oPresupuestos2Otros = oFSGSRaiz.Generar_CPresContablesNivel4
                                        ConfigurarEtiquetas 1
                                        Slider1.Max = 100
                                        Slider1.Value = Slider1.Max
                                    End If
                                End If
                            Next
                        End If
                    Else
                    'Hay m�s de un presupuesto asignado. Si el nodo solo tiene un hermano asignado, o un padre entonces hay que poner ese valor en el nuevo nodo pulsado
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            ' Si tiene asignaciones m�s abajo hay que poner la suma de esos
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                dPorcentajeHermano = DevolverPorcentaje(nodoHermano)
                                nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                                nodoHermano.Image = "PRES" & Mid(nodoHermano.Tag, 5, 1)
                                AsignarPres 0, nodoHermano
                                txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                            End If
                        Else
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                Slider1.Max = 100
                            End If
                        End If
                    
                    End If
            
                Case 3
                    If m_oPresupuestos3Asignados.Count < 2 Then
                        If m_oPresupuestos3Asignados.Count = 0 Then
                            Slider1.Value = Slider1.Max
                        Else
                        'Aqu� es cuando la liamos... tenemos que quitar el presupuesto al que lo lleva ahora para asignarselo al nuevo
                            For Each oPres3Niv4 In m_oPresupuestos3Asignados
                                If NullToStr(oPres3Niv4.UON1) = g_sUON1 And NullToStr(oPres3Niv4.UON2) = g_sUON2 And NullToStr(oPres3Niv4.UON3) = g_sUON3 Then
                                    Select Case oPres3Niv4.IDNivel
                                        Case 1
                                            scod1 = oPres3Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPres3Niv4.CodPRES1))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES1" & scod1)
                                            nodo.Image = "PRES1"
                                        Case 2
                                            scod1 = oPres3Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPres3Niv4.CodPRES1))
                                            scod2 = oPres3Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPres3Niv4.CodPRES2))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES2" & scod1 & scod2)
                                            nodo.Image = "PRES2"
                                        Case 3
                                            scod1 = oPres3Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPres3Niv4.CodPRES1))
                                            scod2 = oPres3Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPres3Niv4.CodPRES2))
                                            scod3 = oPres3Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPres3Niv4.CodPRES3))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                                            nodo.Image = "PRES3"
                                        Case 4
                                            scod1 = oPres3Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPres3Niv4.CodPRES1))
                                            scod2 = oPres3Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPres3Niv4.CodPRES2))
                                            scod3 = oPres3Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPres3Niv4.CodPRES3))
                                            scod4 = oPres3Niv4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34 - Len(oPres3Niv4.Cod))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                                            nodo.Image = "PRES4"
                                    End Select
                                    dPorcentajeHermano = DevolverPorcentaje(nodo)
                                    nodo.Text = QuitarPorcentaje(nodo.Text)
                                    AsignarPres 0, nodo
                                    txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                               
                                Else
                                    ''hay uno pero en otra UO, si no tiene permiso no le dejamos
                                    sCod = EsPresupDelUsuario(oPres3Niv4)
                                    If sCod <> "" Then
                                        m_dblPorcenAsig = m_dblPorcenAsig - oPres3Niv4.Porcen
                                        Set m_oPresupuestos3Asignados = oFSGSRaiz.Generar_CPresConceptos3Nivel4
                                        Set m_oPresupuestos3Otros = oFSGSRaiz.Generar_CPresConceptos3Nivel4
                                        ConfigurarEtiquetas 1
                                        Slider1.Max = 100
                                        Slider1.Value = Slider1.Max
                                    End If
                                End If
                               
                            Next
                        End If
                    Else
                    'Hay m�s de un presupuesto asignado. Si el nodo solo tiene un hermano asignado, o un padre entonces hay que poner ese valor en el nuevo nodo pulsado
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            ' Si tiene asignaciones m�s abajo hay que poner la suma de esos
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                dPorcentajeHermano = DevolverPorcentaje(nodoHermano)
                                nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                                nodoHermano.Image = "PRES" & Mid(nodoHermano.Tag, 5, 1)
                                AsignarPres 0, nodoHermano
                                txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                            End If
                        Else
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                Slider1.Max = 100
                            End If
                        End If
                    
                    End If
                Case 4
                    If m_oPresupuestos4Asignados.Count < 2 Then
                        If m_oPresupuestos4Asignados.Count = 0 Then
                            Slider1.Value = Slider1.Max
                        Else
                        'Aqu� es cuando la liamos... tenemos que quitar el presupuesto al que lo lleva ahora para asignarselo al nuevo
                            For Each oPres4Niv4 In m_oPresupuestos4Asignados
                                If NullToStr(oPres4Niv4.UON1) = g_sUON1 And NullToStr(oPres4Niv4.UON2) = g_sUON2 And NullToStr(oPres4Niv4.UON3) = g_sUON3 Then
                                    Select Case oPres4Niv4.IDNivel
                                        Case 1
                                            scod1 = oPres4Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPres4Niv4.CodPRES1))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES1" & scod1)
                                            nodo.Image = "PRES1"
                                        Case 2
                                            scod1 = oPres4Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPres4Niv4.CodPRES1))
                                            scod2 = oPres4Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPres4Niv4.CodPRES2))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES2" & scod1 & scod2)
                                            nodo.Image = "PRES2"
                                        Case 3
                                            scod1 = oPres4Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPres4Niv4.CodPRES1))
                                            scod2 = oPres4Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPres4Niv4.CodPRES2))
                                            scod3 = oPres4Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPres4Niv4.CodPRES3))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                                            nodo.Image = "PRES3"
                                        Case 4
                                            scod1 = oPres4Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPres4Niv4.CodPRES1))
                                            scod2 = oPres4Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPres4Niv4.CodPRES2))
                                            scod3 = oPres4Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPres4Niv4.CodPRES3))
                                            scod4 = oPres4Niv4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(oPres4Niv4.Cod))
                                            Set nodo = tvwEstrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                                            nodo.Image = "PRES4"
                                    End Select
                                    dPorcentajeHermano = DevolverPorcentaje(nodo)
                                    nodo.Text = QuitarPorcentaje(nodo.Text)
                                    AsignarPres 0, nodo
                                    txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                               
                                Else
                                    'hay uno al 100% pero en otro a�o
                                    sCod = EsPresupDelUsuario(oPres4Niv4)
                                    If sCod <> "" Then
                                        m_dblPorcenAsig = m_dblPorcenAsig - oPres4Niv4.Porcen
                                        Set m_oPresupuestos4Asignados = oFSGSRaiz.Generar_CPresConceptos4Nivel4
                                        Set m_oPresupuestos4Otros = oFSGSRaiz.Generar_CPresConceptos4Nivel4
                                        ConfigurarEtiquetas 1
                                        Slider1.Max = 100
                                        Slider1.Value = Slider1.Max
                                    End If
                                End If
                            Next
                        End If
                    Else
                    'Hay m�s de un presupuesto asignado. Si el nodo solo tiene un hermano asignado, o un padre entonces hay que poner ese valor en el nuevo nodo pulsado
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            ' Si tiene asignaciones m�s abajo hay que poner la suma de esos
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                dPorcentajeHermano = DevolverPorcentaje(nodoHermano)
                                nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                                nodoHermano.Image = "PRES" & Mid(nodoHermano.Tag, 5, 1)
                                AsignarPres 0, nodoHermano
                                txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                            End If
                        Else
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                Slider1.Max = 100
                            End If
                        End If
                    
                    End If
            End Select
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "ConfigurarInterfazPresup", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub ConfigurarInterfazSeguridadUON(ByVal node As MSComctlLib.node)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblEstrorg.caption = node.Text
    lblUO.caption = lblEstrorg.caption
    
    If Right(node.Image, 1) = "A" Or Right(node.Image, 1) = "D" Then
        SSTabPresupuestos.TabVisible(1) = True
    Else
        SSTabPresupuestos.TabVisible(1) = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "ConfigurarInterfazSeguridadUON", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub DatosInicio()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_dblAsignado = g_dblAsignadoInicial
    If g_dblAbierto = 0 Then
        m_dblPendiente = 0
    Else
        m_dblPendiente = g_dblAbierto - g_dblAsignado
    End If
    
    'Truncar el abierto y el asignado
    txtAbierto.Text = TruncarDato(g_dblAbierto)
    txtAbierto.ToolTipText = g_dblAbierto
    txtAsignado.Text = TruncarDato(g_dblAsignado)
    txtAsignado.ToolTipText = g_dblAsignado
    
    If g_dblAbierto = 0 Then
        If g_bHayPres Then
            If g_bHayPresBajaLog Then
                m_dblPendiente = 1 - g_dblAsignado
                txtPorcenPend.Text = TruncarDato(CDec(m_dblPendiente) * 100)
                txtPorcenPend.ToolTipText = CDec(m_dblPendiente)
                txtPorcenAsig.Text = TruncarDato(CDec(1 - m_dblPendiente) * 100)
                txtPorcenAsig.ToolTipText = CDec(1 - m_dblPendiente)
                m_dblPorcenAsig = CDec(1 - m_dblPendiente)
            Else
                txtPorcenPend.Text = Format("0", "0.00")
                txtPorcenPend.ToolTipText = 0
                txtPorcenAsig.Text = Format("100", "0.00")
                txtPorcenAsig.ToolTipText = 100
                m_dblPorcenAsig = 1
            End If
        Else
            txtPorcenPend.Text = Format("100", "0.00")
            txtPorcenPend.ToolTipText = 100
            txtPorcenAsig.Text = Format("0", "0.00")
            txtPorcenAsig.ToolTipText = 0
            m_dblPorcenAsig = 0
        End If
    Else
        If m_dblPendiente = 0 Then
            txtPorcenAsig.Text = Format("100", "0.00")
            txtPorcenAsig.ToolTipText = 100
            txtPendiente.Text = Format("0", "0.00")
            txtPendiente.ToolTipText = 0
            txtPorcenPend.Text = Format("0", "0.00")
            txtPorcenPend.ToolTipText = 0
            m_dblPorcenAsig = 1
        Else
            txtPendiente.Text = TruncarDato(m_dblPendiente)
            txtPendiente.ToolTipText = m_dblPendiente
            txtPorcenPend.Text = TruncarDato(CDec((m_dblPendiente / g_dblAbierto) * 100))
            txtPorcenPend.ToolTipText = CDec((m_dblPendiente / g_dblAbierto) * 100)
            m_dblPorcenAsig = CDec(g_dblAsignado / g_dblAbierto)
            txtPorcenAsig.Text = TruncarDato(CDec(m_dblPorcenAsig * 100))
            txtPorcenAsig.ToolTipText = CDec(m_dblPorcenAsig * 100)
        End If
        If m_dblPendiente < 0 Then
            txtPendiente.Backcolor = RGB(253, 100, 72)
            txtPorcenPend.Backcolor = RGB(253, 100, 72)
        End If
        If CDec(m_dblPendiente / g_dblAbierto) = 0 Then
            m_bPorcenAsigAbierto = False
        Else
            If CDec(m_dblPendiente / g_dblAbierto) = 1 Or g_bHayPresBajaLog Then
                m_bPorcenAsigAbierto = False
            Else
                m_bPorcenAsigAbierto = True
            End If
        End If
    End If
    
    CalcularNuevosDatos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "DatosInicio", err, Erl, , m_bActivado)
      Exit Sub
   End If
        
End Sub

Private Function DevolverPorcentaje(ByVal nodx As MSComctlLib.node) As Double
Dim dPorcen As Double
Dim sCod As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sCod = DevolverCodigoColeccion(nodx)
    
    Select Case g_iTipoPres
    Case 1
        If m_oPresupuestos1Asignados.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = m_oPresupuestos1Asignados.Item(sCod).Porcen
        End If
    Case 2
        If m_oPresupuestos2Asignados.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = m_oPresupuestos2Asignados.Item(sCod).Porcen
        End If
    Case 3
        If m_oPresupuestos3Asignados.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = m_oPresupuestos3Asignados.Item(sCod).Porcen
        End If
    Case 4
        If m_oPresupuestos4Asignados.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = m_oPresupuestos4Asignados.Item(sCod).Porcen
        End If
    End Select
    
    DevolverPorcentaje = dPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "DevolverPorcentaje", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub cmdAceptar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bTimeout Then Exit Sub
    If g_bSinPermisos Then Exit Sub
    Select Case g_iTipoPres
        Case 1
            If m_bRestringirMultiplePres And m_oPresupuestos1Asignados.Count > 1 Then
                oMensajes.ImposibleImputarMultiplesPresupuestos (msPresupPlural)
                Exit Sub
            Else
                If m_bOBLPres1 Then
                    'Obligatorio distribuir el 100%
                    If StrToDbl0(txtPorcenAsig.ToolTipText) <> 100 Then
                        oMensajes.AsignarPresup100Obl (txtPorcenAsig.ToolTipText)
                        Exit Sub
                    End If
                Else
                    If StrToDbl0(txtPorcenAsig.ToolTipText) <> 0 Then
                        If txtPorcenAsig.ToolTipText <> 100 Then
                            oMensajes.AsignarPresup0100Obl (txtPorcenAsig.ToolTipText)
                            Exit Sub
                        End If
                    End If
                End If
                m_bAceptar = True
                AlmacenarPres1
            End If
        
        Case 2
            If m_bRestringirMultiplePres And m_oPresupuestos2Asignados.Count > 1 Then
                oMensajes.ImposibleImputarMultiplesPresupuestos (msPresupPlural)
                Exit Sub
            Else
                If m_bOBLPres2 Then
                    'Obligatorio distribuir el 100%
                    If StrToDbl0(txtPorcenAsig.ToolTipText) <> 100 Then
                        oMensajes.AsignarPresup100Obl (txtPorcenAsig.ToolTipText)
                        Exit Sub
                    End If
                Else
                    If StrToDbl0(txtPorcenAsig.ToolTipText) <> 0 Then
                        If txtPorcenAsig.ToolTipText <> 100 Then
                            oMensajes.AsignarPresup0100Obl (txtPorcenAsig.ToolTipText)
                            Exit Sub
                        End If
                    End If
                End If
                m_bAceptar = True
                AlmacenarPres2
            End If
        
        Case 3
            If m_bRestringirMultiplePres And m_oPresupuestos3Asignados.Count > 1 Then
                oMensajes.ImposibleImputarMultiplesPresupuestos (msPresupPlural)
                Exit Sub
            Else
                If m_bOBLPres3 Then
                    'Obligatorio distribuir el 100%
                    If StrToDbl0(txtPorcenAsig.ToolTipText) <> 100 Then
                        oMensajes.AsignarPresup100Obl (txtPorcenAsig.ToolTipText)
                        Exit Sub
                    End If
                Else
                    If StrToDbl0(txtPorcenAsig.ToolTipText) <> 0 Then
                        If txtPorcenAsig.ToolTipText <> 100 Then
                            oMensajes.AsignarPresup0100Obl (txtPorcenAsig.ToolTipText)
                            Exit Sub
                        End If
                    End If
                End If
                m_bAceptar = True
                AlmacenarPres3
            End If
        Case 4
            If m_bRestringirMultiplePres And m_oPresupuestos4Asignados.Count > 1 Then
                oMensajes.ImposibleImputarMultiplesPresupuestos (msPresupPlural)
                Exit Sub
            Else
                If m_bOBLPres4 Then
                    'Obligatorio distribuir el 100%
                    If StrToDbl0(txtPorcenAsig.ToolTipText) <> 100 Then
                        oMensajes.AsignarPresup100Obl (txtPorcenAsig.ToolTipText)
                        Exit Sub
                    End If
                Else
                    If StrToDbl0(txtPorcenAsig.ToolTipText) <> 0 Then
                        If txtPorcenAsig.ToolTipText <> 100 Then
                            oMensajes.AsignarPresup0100Obl (txtPorcenAsig.ToolTipText)
                            Exit Sub
                        End If
                    End If
                End If
                m_bAceptar = True
                AlmacenarPres4
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
                    
End Sub

Private Sub cmdBuscarPres_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_iTipoPres
        Case 1
                frmPRESProyBuscar.sOrigen = Me.Name
                frmPRESProyBuscar.Show 1
        Case 2
                frmPRESConBuscar.sOrigen = Me.Name
                frmPRESConBuscar.Show 1
        Case 3
                frmPRESCon3Buscar.sOrigen = Me.Name
                frmPRESCon3Buscar.Show 1
        Case 4
                frmPRESCon4Buscar.sOrigen = Me.Name
                frmPRESCon4Buscar.Show 1
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "cmdBuscarPres_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBuscarUO_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmESTRORGBuscarUO.g_sOrigen = "frmPRESAsig"
    frmESTRORGBuscarUO.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "cmdBuscarUO_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Click en el boton cancelar
''' </summary>
'''Tiempo m�ximo:0</remarks>
Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_sOrigen = "frmPEDIDOS" And Not g_bSinPermisos Then
        g_bTimeout = True
        Select Case g_iTipoPres
        Case 1
            Set g_oOrigen.g_oLineaSeleccionada.Presupuestos1 = g_oOrigen.g_oCopiaPresupuestos
        Case 2
            Set g_oOrigen.g_oLineaSeleccionada.Presupuestos2 = g_oOrigen.g_oCopiaPresupuestos
        Case 3
            Set g_oOrigen.g_oLineaSeleccionada.Presupuestos3 = g_oOrigen.g_oCopiaPresupuestos
        Case 4
            Set g_oOrigen.g_oLineaSeleccionada.Presupuestos4 = g_oOrigen.g_oCopiaPresupuestos
        End Select
    End If
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdRestaurarPres_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    CargarAsignadosInicial
    
    Cargar sdbcAnyo.Value
    
    SSTabPresupuestos_Click (0)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "cmdRestaurarPres_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdRestaurarUO_Click()
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bSinPermisos Then
        g_bSinPermisos = False
        g_bTimeout = False
        ConfigurarTab0
        If g_bSinPermisos Then Exit Sub
        CargarAsignadosInicial
        cmdBuscarUO.Enabled = True
        cmdRestaurarPres.Enabled = True
    End If
    Cargar
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "cmdRestaurarUO_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cP_Click(ItemNumber As Long)
Dim sTag As String
Dim oPres As Object
Dim sCod As String
Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sTag = CStr(m_arTag(ItemNumber))
    
    Select Case g_iTipoPres
    Case 1
        Set oPres = m_oPresupuestos1Otros.Item(sTag)
    Case 2
        Set oPres = m_oPresupuestos2Otros.Item(sTag)
    Case 3
        Set oPres = m_oPresupuestos3Otros.Item(sTag)
    Case 4
        Set oPres = m_oPresupuestos4Otros.Item(sTag)
    End Select
    
    sCod = EsPresupDelUsuario(oPres)
    If sCod = "" Then
        oMensajes.MensajeOKOnly 728
        Exit Sub
    Else
        Set nodx = tvwestrorg.Nodes.Item(sCod)
    End If
    
    nodx.Selected = True
    tvwestrorg_NodeClick nodx
    
    If g_iTipoPres < 3 Then
        sdbcAnyo.Value = oPres.Anyo
    End If
    SSTabPresupuestos_Click 0
    
    Set oPres = Nothing


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "cP_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bAceptar = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, True
m_bActivado = False
    Set cP = New cPopupMenu
    cP.hWndOwner = Me.hWnd
    
    If (g_sOrigen = "frmPEDIDOS" Or g_sOrigen = "frmSeguimiento" Or g_sOrigen = "frmPedidosEmision2") Then
        m_bOBLPres1 = gParametrosGenerales.gbOBLPedPres1
        m_bOBLPres2 = gParametrosGenerales.gbOBLPedPres2
        m_bOBLPres3 = gParametrosGenerales.gbOBLPedPres3
        m_bOBLPres4 = gParametrosGenerales.gbOBLPedPres4
        m_iNIVPres1 = gParametrosGenerales.giNIVPedPres1
        m_iNIVPres2 = gParametrosGenerales.giNIVPedPres2
        m_iNIVPres3 = gParametrosGenerales.giNIVPedPres3
        m_iNIVPres4 = gParametrosGenerales.giNIVPedPres4
    Else
        m_bOBLPres1 = gParametrosGenerales.gbOBLPP
        m_bOBLPres2 = gParametrosGenerales.gbOBLPC
        m_bOBLPres3 = gParametrosGenerales.gbOBLPres3
        m_bOBLPres4 = gParametrosGenerales.gbOBLPres4
        m_iNIVPres1 = gParametrosGenerales.giNIVPP
        m_iNIVPres2 = gParametrosGenerales.giNIVPC
        m_iNIVPres3 = gParametrosGenerales.giNIVPres3
        m_iNIVPres4 = gParametrosGenerales.giNIVPres4
    End If
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarAnyos
    
    CargarRecursos
    
    g_bTimeout = False
    m_bAceptar = False
    g_bSinPermisos = False
    
    ConfigurarTab0

    If g_dblAbierto = 0 Then
        lblAsignado.Left = lblAbierto.Left
        txtPorcenAsig.Left = lblAsignado.Left + lblAsignado.Width + 100
        lblPorcen(1).Left = txtPorcenAsig.Left + txtPorcenAsig.Width + 10
        lblPrendiente.Left = lblPorcen(1).Left + 500
        txtPorcenPend.Left = lblPrendiente.Left + lblPrendiente.Width + 100
        lblPorcen(2).Left = txtPorcenPend.Left + txtPorcenPend.Width + 10
        txtPorcenAsignar.Left = 0
        lblPorcen(3).Left = txtPorcenAsignar.Left + txtPorcenAsignar.Width + 10
    
        lblAbierto.Visible = False
        txtAbierto.Visible = False
        txtAsignado.Visible = False
        txtPendiente.Visible = False
        txtAsignar.Visible = False
    End If
        
    g_dblAsignadoInicial = g_dblAsignado
    
    If Not g_bModif Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        cmdBuscarPres.Visible = False
        cmdBuscarUO.Visible = False
        txtAsignar.Locked = True
        txtPorcenAsignar.Locked = True
        Slider1.Enabled = False
    End If
        
    If Not g_bSinPermisos Then
                    
        CargarAsignadosInicial
        
        QuitarPresupuestosAsignadosBajaLogica
        
        Cargar
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Configura el tab0 de la pantalla de asignacion de presupuestos
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:FOrm_Load; cmdRestaurarUO_Click ; Tiempo m�ximo: Puede exceder los 2 seg dependiendo del numero de presupuestos</remarks>
Private Sub ConfigurarTab0()
    Dim arPresup As Variant
    Dim i As Integer
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_sItems = ""

    m_iUOBase = 0
    If g_bRUO Or g_bRPerfUO Or g_bRUORama Then
        If (CStr(basOptimizacion.gUON3Usuario) <> "") Then
            m_iUOBase = 3
            g_sUON3 = NullToStr(basOptimizacion.gUON3Usuario)
            g_sUON2 = NullToStr(basOptimizacion.gUON2Usuario)
            g_sUON1 = NullToStr(basOptimizacion.gUON1Usuario)
        ElseIf (CStr(basOptimizacion.gUON2Usuario) <> "") Then
            m_iUOBase = 2
            g_sUON2 = NullToStr(basOptimizacion.gUON2Usuario)
            g_sUON1 = NullToStr(basOptimizacion.gUON1Usuario)
        ElseIf (NullToStr(basOptimizacion.gUON1Usuario) <> "") Then
            m_iUOBase = 1
            g_sUON1 = basOptimizacion.gUON1Usuario
        End If
    End If
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    Select Case g_iTipoPres
    Case 1
        Set m_oPresupuestos1 = oFSGSRaiz.generar_CPresProyectosNivel1
        Select Case g_sOrigen
        Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
            If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                Select Case g_iAmbitoPresup
                Case 1
                    If ProcesoTieneItems Then
                        arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil)
                    Else
                        arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
                    End If
                Case 2
                    If g_sOrigen = "GRUPO_NUEVO" Then
                        arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil)
                    Else
                        If GrupoTieneItems Then
                            arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil)
                        Else
                            arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        End If
                    End If
                Case 3
                    If g_sOrigen = "PROCE_MODIFITEMS" Then
                        For i = 1 To UBound(frmPROCE.g_arItems)
                            m_sItems = m_sItems & frmPROCE.g_arItems(i)
                            If i < UBound(frmPROCE.g_arItems) Then
                                 m_sItems = m_sItems & ","
                            End If
                        Next
                        arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil)
                    Else
                        arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil)
                    End If
                End Select
            Else
                arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama)
            End If
        Case "frmPEDIDOS"
            If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil)
            Else
                arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case "frmSeguimiento"
            If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil)
            Else
                arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case Else
            arPresup = m_oPresupuestos1.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
        End Select
        
    Case 2
        Set m_oPresupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel1
        Select Case g_sOrigen
        Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
            If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                Select Case g_iAmbitoPresup
                Case 1
                    If ProcesoTieneItems Then
                        arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil)
                    Else
                        arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
                    End If
                Case 2
                    If g_sOrigen = "GRUPO_NUEVO" Then
                        arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil)
                    Else
                        If GrupoTieneItems Then
                            arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil)
                        Else
                            arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        End If
                    End If
                Case 3
                    If g_sOrigen = "PROCE_MODIFITEMS" Then
                        For i = 1 To UBound(frmPROCE.g_arItems)
                            m_sItems = m_sItems & frmPROCE.g_arItems(i)
                            If i < UBound(frmPROCE.g_arItems) Then
                                 m_sItems = m_sItems & ","
                            End If
                        Next
                        arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil)
                    Else
                        arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil)
                    End If
                End Select
            Else
                arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case "frmPEDIDOS"
            If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil)
            Else
                arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case "frmSeguimiento"
            If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil)
            Else
                arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case Else
            arPresup = m_oPresupuestos2.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
        End Select
    
    Case 3
        Set m_oPresupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel1
        Select Case g_sOrigen
        Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
            If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                Select Case g_iAmbitoPresup
                Case 1
                    If ProcesoTieneItems Then
                        arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil)
                    Else
                        arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
                    End If
                Case 2
                    If g_sOrigen = "GRUPO_NUEVO" Then
                        arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil)
                    Else
                        If GrupoTieneItems Then
                            arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil)
                        Else
                            arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        End If
                    End If
                Case 3
                    If g_sOrigen = "PROCE_MODIFITEMS" Then
                        For i = 1 To UBound(frmPROCE.g_arItems)
                            m_sItems = m_sItems & frmPROCE.g_arItems(i)
                            If i < UBound(frmPROCE.g_arItems) Then
                                 m_sItems = m_sItems & ","
                            End If
                        Next
                        arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil)
                    Else
                        arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil)
                    End If
                End Select
            Else
                arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case "frmPEDIDOS"
            If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil)
            Else
                arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case "frmSeguimiento"
             If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil)
            Else
                arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case Else
            arPresup = m_oPresupuestos3.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
        End Select
    
    Case 4
        Set m_oPresupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel1
        Select Case g_sOrigen
        Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
            If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                Select Case g_iAmbitoPresup
                Case 1
                    If ProcesoTieneItems Then
                        arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil)
                    Else
                        arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
                    End If
                Case 2
                    If g_sOrigen = "GRUPO_NUEVO" Then
                        arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil)
                    Else
                        If GrupoTieneItems Then
                            arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil)
                        Else
                            arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        End If
                    End If
                Case 3
                    If g_sOrigen = "PROCE_MODIFITEMS" Then
                        For i = 1 To UBound(frmPROCE.g_arItems)
                            m_sItems = m_sItems & frmPROCE.g_arItems(i)
                            If i < UBound(frmPROCE.g_arItems) Then
                                 m_sItems = m_sItems & ","
                            End If
                        Next
                        arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil)
                    Else
                        arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil)
                    End If
                End Select
            Else
                arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case "frmPEDIDOS"
            If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil)
            Else
                arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case "frmSeguimiento"
            If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil)
            Else
                arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
            End If
        Case Else
            arPresup = m_oPresupuestos4.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, g_bRUO, g_bRUORama, , , , , , , , , , g_bRPerfUO, lIdPerfil)
        End Select
        
    End Select

    
    Select Case arPresup(0)
    Case 0
        'No hay ningun presupuesto para las ramas permitidas
        oMensajes.MensajeOKOnly 728
        g_bSinPermisos = True
        cmdBuscarUO.Enabled = False
        cmdRestaurarPres.Enabled = False
        If g_sOrigen = "XLS_ITEMS" Then
            g_bTimeout = True
            frmPROCE.g_bCancelPres = True
        End If
        SSTabPresupuestos.TabVisible(0) = True
        SSTabPresupuestos.TabVisible(1) = False
    Case 1
        cmdBuscarUO.Visible = False
        cmdRestaurarUO.Visible = False
        cmdBuscarPres.Visible = True
        cmdRestaurarPres.Visible = True
        SSTabPresupuestos.TabVisible(1) = True
        SSTabPresupuestos.TabVisible(0) = False
        g_sUON1 = NullToStr(arPresup(1))
        g_sUON2 = NullToStr(arPresup(2))
        g_sUON3 = NullToStr(arPresup(3))
        If g_sUON3 <> "" Then
            If m_oUnidadesOrgN3 Is Nothing Then
                Set m_oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
            End If
            lblUO.caption = g_sUON3 & " - " & m_oUnidadesOrgN3.DevolverDenominacion(g_sUON1, g_sUON2, g_sUON3)
        Else
            If g_sUON2 <> "" Then
                If m_oUnidadesOrgN2 Is Nothing Then
                    Set m_oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
                End If
                lblUO.caption = g_sUON2 & " - " & m_oUnidadesOrgN2.DevolverDenominacion(g_sUON1, g_sUON2)
            Else
                If m_oUnidadesOrgN1 Is Nothing Then
                    Set m_oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
                End If
                lblUO.caption = g_sUON1 & " - " & m_oUnidadesOrgN1.DevolverDenominacion(g_sUON1)
            End If
        End If
    Case Else
        SSTabPresupuestos.TabVisible(1) = False
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "ConfigurarTab0", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub



Private Sub CargarAnyos()
Dim iAnyoActual As Integer
Dim iInd As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "CargarAnyos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Genera la estructura organizativa
''' </summary>
''' <param name="bOrdenadoPorDen">Si se ordena por denominacion la estructura organizativa</param>
''' <returns></returns>
''' <remarks>Llamada desde:Cargar; Tiempo m�ximo: Puede exceder los 2 seg dependiendo de la estructura organizativa</remarks>
Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    Dim nodx As MSComctlLib.node
    Dim bUON0 As Boolean
    Dim i As Integer
    Dim lIdPerfil As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    tvwestrorg.Nodes.clear
    
    Set m_oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set m_oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set m_oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    
    If g_sOrigen = "PROCE_MODIFITEMS" Then
        m_sItems = ""
        For i = 1 To UBound(frmPROCE.g_arItems)
            m_sItems = m_sItems & frmPROCE.g_arItems(i)
            If i < UBound(frmPROCE.g_arItems) Then
                 m_sItems = m_sItems & ","
            End If
        Next
    End If
         
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And (g_bRUO Or g_bRPerfUO) Then
        
         'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                Select Case g_sOrigen
                Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
                    If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                        Select Case g_iAmbitoPresup
                        Case 1
                            If ProcesoTieneItems Then
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil)
                            Else
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                            End If
                        Case 2
                            If g_sOrigen = "GRUPO_NUEVO" Then
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil)
                            Else
                                If GrupoTieneItems Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil)
                                Else
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                                End If
                            End If
                        Case 3
                            If g_sOrigen = "PROCE_MODIFITEMS" Then
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil)
                            Else
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil)
                            End If
                        End Select
                    Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                    End If
                Case "frmPEDIDOS"
                    If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil)
                    Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                    End If
                Case "frmSeguimiento"
                    If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil)
                    Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                    End If
                Case Else
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                End Select
                
            Case 2
                Select Case g_sOrigen
                Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
                    If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                        Select Case g_iAmbitoPresup
                        Case 1
                            If ProcesoTieneItems Then
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil
                            Else
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                            End If
                        Case 2
                            If g_sOrigen = "GRUPO_NUEVO" Then
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil
                            Else
                                If GrupoTieneItems Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil
                                Else
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                                End If
                            End If
                        Case 3
                            If g_sOrigen = "PROCE_MODIFITEMS" Then
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil
                            Else
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil
                            End If
                        End Select
                    Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                    End If
                Case "frmPEDIDOS"
                    If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil
                    Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                    End If
                Case "frmSeguimiento"
                    If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil
                    Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                    End If
                Case Else
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                End Select
                
            Case 3
                Select Case g_sOrigen
                Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
                    If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                        Select Case g_iAmbitoPresup
                        Case 1
                            If ProcesoTieneItems Then
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil
                                m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , , g_bRPerfUO, lIdPerfil
                            Else
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                                m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                            End If
                        Case 2
                            If g_sOrigen = "GRUPO_NUEVO" Then
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil
                                m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo, , , , g_bRPerfUO, lIdPerfil
                            Else
                                If GrupoTieneItems Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil
                                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id, , , , g_bRPerfUO, lIdPerfil
                                Else
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                                End If
                            End If
                        Case 3
                            If g_sOrigen = "PROCE_MODIFITEMS" Then
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil
                                m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems, g_bRPerfUO, lIdPerfil
                            Else
                                bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil)
                                m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil
                                m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id, , , g_bRPerfUO, lIdPerfil
                            End If
                        End Select
                    Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                        m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                    End If
                Case "frmPEDIDOS"
                    If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil
                        m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item, , , g_bRPerfUO, lIdPerfil
                    Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                        m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                    End If
                Case "frmSeguimiento"
                    If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil
                        m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item, , , g_bRPerfUO, lIdPerfil
                    Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                        m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                    End If
                Case Else
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , g_bRUORama, , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, g_bRUO, bOrdenadoPorDen, , , , , , , , , , , , , , , , , , g_bRPerfUO, lIdPerfil
                End Select
                
                
        End Select
               
    Else
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                Select Case g_sOrigen
                    Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
                        If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                            Select Case g_iAmbitoPresup
                            Case 1
                                If ProcesoTieneItems Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value)
                                Else
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                                End If
                            Case 2
                                If g_sOrigen = "GRUPO_NUEVO" Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo)
                                Else
                                    If GrupoTieneItems Then
                                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id)
                                    Else
                                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                                    End If
                                End If
                            Case 3
                                If g_sOrigen = "PROCE_MODIFITEMS" Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems)
                                Else
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id)
                                End If
                            End Select
                        Else
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                        End If
                    Case "frmPEDIDOS"
                        If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item)
                        Else
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                        End If
                    Case "frmSeguimiento"
                        If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item)
                        Else
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                        End If
                    Case Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                End Select
            
            Case 2
                Select Case g_sOrigen
                    Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
                        If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                            Select Case g_iAmbitoPresup
                            Case 1
                                If ProcesoTieneItems Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value
                                Else
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                                End If
                            Case 2
                                If g_sOrigen = "GRUPO_NUEVO" Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo
                                Else
                                    If GrupoTieneItems Then
                                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id)
                                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id
                                    Else
                                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                                    End If
                                End If
                            Case 3
                                If g_sOrigen = "PROCE_MODIFITEMS" Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems
                                Else
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id
                                End If
                            End Select
                        Else
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                        End If
                    Case "frmPEDIDOS"
                        If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item
                        Else
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                        End If
                    Case "frmSeguimiento"
                        If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item
                        Else
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                        End If
                    Case Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                End Select
                
            Case 3
                Select Case g_sOrigen
                    Case "MODIF_PRES", "GRUPO_NUEVO", "PROCE_MODIFITEMS", "AFTER_INSERT", "frmPROCEModifConfig"
                        If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                            Select Case g_iAmbitoPresup
                            Case 1
                                If ProcesoTieneItems Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value
                                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , True, 1, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value
                                Else
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                                End If
                            Case 2
                                If g_sOrigen = "GRUPO_NUEVO" Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo
                                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo
                                Else
                                    If GrupoTieneItems Then
                                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id)
                                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id
                                        m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , True, 2, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id
                                    Else
                                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                                        m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                                    End If
                                End If
                            Case 3
                                If g_sOrigen = "PROCE_MODIFITEMS" Then
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems
                                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , , m_sItems
                                Else
                                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id)
                                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id
                                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , frmPRESItem.g_oItem.Id
                                End If
                            End Select
                        Else
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                            m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                        End If
                    Case "frmPEDIDOS"
                        If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item
                            m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , True, 3, g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, , g_oOrigen.g_oLineaSeleccionada.Item
                        Else
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                            m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                        End If
                    Case "frmSeguimiento"
                        If gParametrosGenerales.gbOblAsigPresUonArtPeDir Then
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item
                            m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , True, 3, frmSeguimiento.g_oLineaPedido.Anyo, frmSeguimiento.g_oLineaPedido.GMN1Proce, frmSeguimiento.g_oLineaPedido.ProceCod, , frmSeguimiento.g_oLineaPedido.Item
                        Else
                            bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                            m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                            m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                        End If
                    Case Else
                        bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                        m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                        m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                End Select
        End Select
        
        
    End If
    
        
   '************************************************************
    'Generamos la estructura arborea
    Dim iPresup As Integer
    Dim sKeyNodo As String
    
    ' Unidades organizativas
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    If bUON0 Then
        If ComprobarSiEstaAsignado() Then
            nodx.Image = "UON0A"
            nodx.Expanded = True
        Else
            nodx.Image = "UON0D"
        End If
    Else
        nodx.Image = "UON0"
    End If
    nodx.Tag = "UON0"
    nodx.Expanded = True
    
    iPresup = 0
    sKeyNodo = ""

    For Each oUON1 In m_oUnidadesOrgN1
    
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        If ComprobarSiEstaAsignado(oUON1.Cod) Then
            nodx.Image = "UON1A"
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        ElseIf oUON1.ConPresup Then
            nodx.Image = "UON1D"
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        End If
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
    Next
    
    For Each oUON2 In m_oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        If ComprobarSiEstaAsignado(oUON2.CodUnidadOrgNivel1, oUON2.Cod) Then
            nodx.Image = "UON2A"
            nodx.Parent.Expanded = True
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        ElseIf oUON2.ConPresup Then
            nodx.Image = "UON2D"
            nodx.Parent.Expanded = True
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        End If
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
    Next
    
    For Each oUON3 In m_oUnidadesOrgN3

        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        If ComprobarSiEstaAsignado(oUON3.CodUnidadOrgNivel1, oUON3.CodUnidadOrgNivel2, oUON3.Cod) Then
            nodx.Image = "UON3A"
            nodx.Parent.Parent.Expanded = True
            nodx.Parent.Expanded = True
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        ElseIf oUON3.ConPresup Then
            nodx.Image = "UON3D"
            nodx.Parent.Parent.Expanded = True
            nodx.Parent.Expanded = True
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        End If
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
    Next
    
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    If iPresup = 1 Then 'Solo hay una org compras seleccionable
        tvwestrorg.Nodes(sKeyNodo).Selected = True
        tvwestrorg_NodeClick tvwestrorg.Nodes(sKeyNodo)
        SSTabPresupuestos.Tab = 1
        SSTabPresupuestos_Click 0
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "GenerarEstructuraOrg", err, Erl, , m_bActivado)
      Exit Sub
   End If
        
End Sub

Private Sub Form_Resize()
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

''' <summary>
''' Descarga el formulario
''' </summary>
''' <param name="Cancel">Indica si se cancela la descarga del formulario</param>
''' <returns></returns>
''' <remarks>Llamada desde:Sistema; Tiempo m�ximo: 0</remarks>

Private Sub Form_Unload(Cancel As Integer)
    Dim irespuesta As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If m_bDescargarFrm = False Then
    'Desde la configuraci�n del proceso puede cancelarse (aunque sea obligatorio)
    'y el presupuesto volver�a al ambito anterior.17/06/2002
    'Al modificar el presup a nivel de proceso grupo o �tem puede cancelarse aunque sea oblig
    'porque se supone que si est� en BD ya tiene todos los datos obligatorios.19/09/2002
    
    'Si g_btimeout esta desbloqueando y no se comprueba
    If Not g_bTimeout And g_bModif Then
        Select Case g_iTipoPres
        
            Case 1
                    If m_bOBLPres1 Then
                        If Not m_bAceptar Then
                            If g_sOrigen = "GRUPON" Then
                                irespuesta = oMensajes.PreguntaCancelarAnyadirProcesoGrupoPres(basParametros.gParametrosGenerales.gsSingPres1, _
                                                basParametros.gParametrosGenerales.gsSingPres2, basParametros.gParametrosGenerales.gsSingPres3, _
                                                basParametros.gParametrosGenerales.gsSingPres4, g_iTipoPres, True)
                                If irespuesta = vbNo Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            ElseIf g_sOrigen = "PROCEN" And frmPROCE.g_iAnyadir = 1 Then
                                irespuesta = oMensajes.PreguntaCancelarAnyadirProcesoGrupoPres(basParametros.gParametrosGenerales.gsSingPres1, _
                                                    basParametros.gParametrosGenerales.gsSingPres2, basParametros.gParametrosGenerales.gsSingPres3, _
                                                    basParametros.gParametrosGenerales.gsSingPres4, g_iTipoPres, False)
                                If irespuesta = vbNo Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            ElseIf (g_sOrigen <> "frmPROCEModifConfig" And g_sOrigen <> "MODIF_PRES" And g_sOrigen <> "frmPEDIDOS" And g_sOrigen <> "frmPedidosEmision2" And g_sOrigen <> "frmSeguimiento" And g_sOrigen <> "frmFormularios" And g_sOrigen <> "frmDesgloseValores" And g_sOrigen <> "frmSolicitudDetalle" And g_sOrigen <> "frmSolicitudDesglose" And g_sOrigen <> "frmSolicitudDesgloseP" And g_sOrigen <> "frmSOLAbrirFaltan") Then
                                oMensajes.PresupuestoDistribOblig g_iTipoPres, basParametros.gParametrosGenerales.gsSingPres1, basParametros.gParametrosGenerales.gsSingPres2, _
                                            basParametros.gParametrosGenerales.gsSingPres3, basParametros.gParametrosGenerales.gsSingPres4
                                Cancel = True
                                Exit Sub
                            End If
                        Else
                            If StrToDbl0(txtPorcenAsig.ToolTipText) <> 100 Then
                                oMensajes.AsignarPresup100Obl (txtPorcenAsig.ToolTipText)
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                    Else
                        If m_bAceptar Then
                            If StrToDbl0(txtPorcenAsig.ToolTipText) <> 0 Then
                                If txtPorcenAsig.ToolTipText <> 100 Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            End If
                         End If
                    End If
                    If g_sOrigen = "ANYA_ITEMS" Then
                        frmItemsWizard.g_bPresAnu1OK = True
                    End If
            
            Case 2
                    If m_bOBLPres2 Then
                        If Not m_bAceptar Then
                            If g_sOrigen = "GRUPON" Then
                                irespuesta = oMensajes.PreguntaCancelarAnyadirProcesoGrupoPres(basParametros.gParametrosGenerales.gsSingPres1, basParametros.gParametrosGenerales.gsSingPres2, _
                                                    basParametros.gParametrosGenerales.gsSingPres3, basParametros.gParametrosGenerales.gsSingPres4, g_iTipoPres, True)
                                If irespuesta = vbNo Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            ElseIf g_sOrigen = "PROCEN" And frmPROCE.g_iAnyadir = 1 Then
                                irespuesta = oMensajes.PreguntaCancelarAnyadirProcesoGrupoPres(basParametros.gParametrosGenerales.gsSingPres1, _
                                                    basParametros.gParametrosGenerales.gsSingPres2, basParametros.gParametrosGenerales.gsSingPres3, _
                                                    basParametros.gParametrosGenerales.gsSingPres4, g_iTipoPres, False)
                                If irespuesta = vbNo Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            ElseIf (g_sOrigen <> "frmPROCEModifConfig" And g_sOrigen <> "MODIF_PRES" And g_sOrigen <> "frmPEDIDOS" And g_sOrigen <> "frmPedidosEmision2" And g_sOrigen <> "frmSeguimiento" And g_sOrigen <> "frmFormularios" And g_sOrigen <> "frmDesgloseValores" And g_sOrigen <> "frmSolicitudDetalle" And g_sOrigen <> "frmSolicitudDesglose" And g_sOrigen <> "frmSolicitudDesgloseP" And g_sOrigen <> "frmSOLAbrirFaltan") Then
                                oMensajes.PresupuestoDistribOblig g_iTipoPres, basParametros.gParametrosGenerales.gsSingPres1, basParametros.gParametrosGenerales.gsSingPres2, _
                                            basParametros.gParametrosGenerales.gsSingPres3, basParametros.gParametrosGenerales.gsSingPres4
                                Cancel = True
                                Exit Sub
                            End If
                        Else
                            If StrToDbl0(txtPorcenAsig.ToolTipText) <> 100 Then
                                oMensajes.AsignarPresup100Obl (txtPorcenAsig.ToolTipText)
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                    Else
                        If m_bAceptar Then
                            If StrToDbl0(txtPorcenAsig.ToolTipText) <> 0 Then
                                If txtPorcenAsig.ToolTipText <> 100 Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If
                    If g_sOrigen = "ANYA_ITEMS" Then
                        frmItemsWizard.g_bPresAnu2OK = True
                    End If
                    
            Case 3
                    If m_bOBLPres3 Then
                        If Not m_bAceptar Then
                            If g_sOrigen = "GRUPON" Then
                                irespuesta = oMensajes.PreguntaCancelarAnyadirProcesoGrupoPres(basParametros.gParametrosGenerales.gsSingPres1, _
                                                    basParametros.gParametrosGenerales.gsSingPres2, basParametros.gParametrosGenerales.gsSingPres3, _
                                                    basParametros.gParametrosGenerales.gsSingPres4, g_iTipoPres, True)
                                If irespuesta = vbNo Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            ElseIf g_sOrigen = "PROCEN" And frmPROCE.g_iAnyadir = 1 Then
                                irespuesta = oMensajes.PreguntaCancelarAnyadirProcesoGrupoPres(basParametros.gParametrosGenerales.gsSingPres1, _
                                                    basParametros.gParametrosGenerales.gsSingPres2, basParametros.gParametrosGenerales.gsSingPres3, _
                                                    basParametros.gParametrosGenerales.gsSingPres4, g_iTipoPres, False)
                                If irespuesta = vbNo Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            ElseIf (g_sOrigen <> "frmPROCEModifConfig" And g_sOrigen <> "MODIF_PRES" And g_sOrigen <> "frmPEDIDOS" And g_sOrigen <> "frmPedidosEmision2" And g_sOrigen <> "frmSeguimiento" And g_sOrigen <> "frmFormularios" And g_sOrigen <> "frmDesgloseValores" And g_sOrigen <> "frmSolicitudDetalle" And g_sOrigen <> "frmSolicitudDesglose" And g_sOrigen <> "frmSolicitudDesgloseP" And g_sOrigen <> "frmSOLAbrirFaltan") Then
                                oMensajes.PresupuestoDistribOblig g_iTipoPres, basParametros.gParametrosGenerales.gsSingPres1, basParametros.gParametrosGenerales.gsSingPres2, _
                                            basParametros.gParametrosGenerales.gsSingPres3, basParametros.gParametrosGenerales.gsSingPres4
                                Cancel = True
                                Exit Sub
                            End If
                        Else
                            If StrToDbl0(txtPorcenAsig.ToolTipText) <> 100 Then
                                oMensajes.AsignarPresup100Obl (txtPorcenAsig.ToolTipText)
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                    Else
                        If m_bAceptar Then
                            If StrToDbl0(txtPorcenAsig.ToolTipText) <> 0 Then
                                If txtPorcenAsig.ToolTipText <> 100 Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If
                    If g_sOrigen = "ANYA_ITEMS" Then
                        frmItemsWizard.g_bPres1OK = True
                    End If
        
            Case 4
                    If m_bOBLPres4 Then
                        If Not m_bAceptar Then
                            If g_sOrigen = "GRUPON" Then
                                irespuesta = oMensajes.PreguntaCancelarAnyadirProcesoGrupoPres(basParametros.gParametrosGenerales.gsSingPres1, _
                                                    basParametros.gParametrosGenerales.gsSingPres2, basParametros.gParametrosGenerales.gsSingPres3, _
                                                    basParametros.gParametrosGenerales.gsSingPres4, g_iTipoPres, True)
                                If irespuesta = vbNo Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            ElseIf g_sOrigen = "PROCEN" And frmPROCE.g_iAnyadir = 1 Then
                                irespuesta = oMensajes.PreguntaCancelarAnyadirProcesoGrupoPres(basParametros.gParametrosGenerales.gsSingPres1, _
                                                    basParametros.gParametrosGenerales.gsSingPres2, basParametros.gParametrosGenerales.gsSingPres3, _
                                                    basParametros.gParametrosGenerales.gsSingPres4, g_iTipoPres, False)
                                If irespuesta = vbNo Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            ElseIf (g_sOrigen <> "frmPROCEModifConfig" And g_sOrigen <> "MODIF_PRES" And g_sOrigen <> "frmPEDIDOS" And g_sOrigen <> "frmPedidosEmision2" And g_sOrigen <> "frmSeguimiento" And g_sOrigen <> "frmFormularios" And g_sOrigen <> "frmDesgloseValores" And g_sOrigen <> "frmSolicitudDetalle" And g_sOrigen <> "frmSolicitudDesglose" And g_sOrigen <> "frmSolicitudDesgloseP" And g_sOrigen <> "frmSOLAbrirFaltan") Then
                                oMensajes.PresupuestoDistribOblig g_iTipoPres, basParametros.gParametrosGenerales.gsSingPres1, basParametros.gParametrosGenerales.gsSingPres2, _
                                            basParametros.gParametrosGenerales.gsSingPres3, basParametros.gParametrosGenerales.gsSingPres4
                                Cancel = True
                                Exit Sub
                            End If
                        Else
                            If StrToDbl0(txtPorcenAsig.ToolTipText) <> 100 Then
                                oMensajes.AsignarPresup100Obl (txtPorcenAsig.ToolTipText)
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                    Else
                        If m_bAceptar Then
                            If StrToDbl0(txtPorcenAsig.ToolTipText) <> 0 Then
                                If txtPorcenAsig.ToolTipText <> 100 Then
                                    Cancel = True
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If
                    If g_sOrigen = "ANYA_ITEMS" Then
                        frmItemsWizard.g_bPres2OK = True
                    End If
        
        End Select
    
    End If
End If
m_bDescargarFrm = False

    If g_sOrigen = "frmPEDIDOS" And Not g_bSinPermisos Then
        If Not m_bAceptar Then
        'Si se sale de la ventana sin dar al bot�n Aceptar
        'se restituye la situaci�n original.
            Select Case g_iTipoPres
                Case 1
                    Set g_oOrigen.g_oLineaSeleccionada.Presupuestos1 = g_oOrigen.g_oCopiaPresupuestos
                Case 2
                    Set g_oOrigen.g_oLineaSeleccionada.Presupuestos2 = g_oOrigen.g_oCopiaPresupuestos
                Case 3
                    Set g_oOrigen.g_oLineaSeleccionada.Presupuestos3 = g_oOrigen.g_oCopiaPresupuestos
                Case 4
                    Set g_oOrigen.g_oLineaSeleccionada.Presupuestos4 = g_oOrigen.g_oCopiaPresupuestos
            End Select
        End If
    End If
    
    g_iAmbitoPresup = 0
    Set m_oPresupuestos1 = Nothing
    Set m_oPresupuestos2 = Nothing
    Set m_oPresupuestos3 = Nothing
    Set m_oPresupuestos4 = Nothing
    Set m_oPresupuestos1Asignados = Nothing
    Set m_oPresupuestos2Asignados = Nothing
    Set m_oPresupuestos3Asignados = Nothing
    Set m_oPresupuestos4Asignados = Nothing
    Set m_oPresupuestos1Otros = Nothing
    Set m_oPresupuestos2Otros = Nothing
    Set m_oPresupuestos3Otros = Nothing
    Set m_oPresupuestos4Otros = Nothing
    Set m_oUnidadesOrgN1 = Nothing
    Set m_oUnidadesOrgN2 = Nothing
    Set m_oUnidadesOrgN3 = Nothing
    g_sUON1 = ""
    g_sUON2 = ""
    g_sUON3 = ""
    m_bAceptar = False
    m_bPorcenAsigAbierto = False
    g_bHayPres = False
    g_bTimeout = False
    g_dblAbierto = 0
    g_dblAsignado = 0
    m_dblPorcenAsig = 0
    m_dblPendiente = 0
    g_dblAsignadoInicial = 0
    m_bOBLPres1 = False
    m_bOBLPres2 = False
    m_bOBLPres3 = False
    m_bOBLPres4 = False
    m_iNIVPres1 = 0
    m_iNIVPres2 = 0
    m_iNIVPres3 = 0
    m_iNIVPres4 = 0
    g_bRUO = False
    g_bRPerfUO = False
    g_bRUORama = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcAnyo_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtObj.Text = ""
    txtPresupuesto.Text = ""
    If txtAbierto.Visible Then
        m_bRespetarPorcen = True
        txtAsignar.Text = ""
    End If
    m_bRespetarPorcen = True
    txtPorcenAsignar.Text = ""
    m_bRespetarPorcen = True
    Slider1.Value = 0
    m_bRespetarPorcen = False

    Screen.MousePointer = vbHourglass
    Select Case g_iTipoPres
        Case 1
                GenerarEstructuraPresupuestosTipo1 False
        Case 2
                GenerarEstructuraPresupuestosTipo2 False
        Case 3
                GenerarEstructuraPresupuestosTipo3 False
        Case 4
                GenerarEstructuraPresupuestosTipo4 False
    End Select
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "sdbcAnyo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub Slider1_Change()
Dim nodx As MSComctlLib.node
Dim dPorcen As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwEstrPres.selectedItem
    
    If nodx Is Nothing Then
        Slider1.Value = 0
        Exit Sub
    End If
    
    If nodx.Parent Is Nothing Then
        Slider1.Value = 0
        Exit Sub
    End If
    
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
'    m_bRespetarPorcen = True
    If Slider1.Value >= Slider1.Max Then
        dPorcen = CDec(DevolverPorcentaje(tvwEstrPres.selectedItem) * 100)
        dPorcen = dPorcen + (100 - CDec(m_dblPorcenAsig * 100))
    Else
        dPorcen = Slider1.Value
    End If
    txtPorcenAsignar.Text = dPorcen
    
    If StrToDbl0(txtPorcenAsignar.Text) = 0 Then
        If txtAbierto.Visible Then
            m_bRespetarPorcen = True
            txtAsignar.Text = ""
            m_bRespetarPorcen = False
        End If
    Else
        If txtAbierto.Visible Then
            m_bRespetarPorcen = True
            txtAsignar.Text = CDec(dPorcen / 100) * g_dblAbierto
            m_bRespetarPorcen = False
        End If
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "Slider1_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Function DevolverAnyoAsignado() As Integer
Dim nodo As MSComctlLib.node
Dim oPres As Object

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set nodo = tvwestrorg.selectedItem

Select Case Left(nodo.Tag, 4)
    Case "UON3"
        g_sUON1 = DevolverCod(nodo.Parent.Parent)
        g_sUON2 = DevolverCod(nodo.Parent)
        g_sUON3 = DevolverCod(nodo)
    Case "UON2"
        g_sUON1 = DevolverCod(nodo.Parent)
        g_sUON2 = DevolverCod(nodo)
        g_sUON3 = ""
    Case "UON1"
        g_sUON1 = DevolverCod(nodo)
        g_sUON2 = ""
        g_sUON3 = ""
    Case "UON0"
        g_sUON1 = ""
        g_sUON2 = ""
        g_sUON3 = ""
End Select

Select Case g_iTipoPres
Case 1
    For Each oPres In m_oPresupuestos1Asignados
        If NullToStr(oPres.UON1) = g_sUON1 And NullToStr(oPres.UON2) = g_sUON2 And NullToStr(oPres.UON3) = g_sUON3 Then
            DevolverAnyoAsignado = oPres.Anyo
            Exit Function
        End If
    Next
Case 2
    For Each oPres In m_oPresupuestos2Asignados
        If NullToStr(oPres.UON1) = g_sUON1 And NullToStr(oPres.UON2) = g_sUON2 And NullToStr(oPres.UON3) = g_sUON3 Then
            DevolverAnyoAsignado = oPres.Anyo
            Exit Function
        End If
    Next
End Select
DevolverAnyoAsignado = sdbcAnyo.Value
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "DevolverAnyoAsignado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub SSTabPresupuestos_Click(PreviousTab As Integer)
Dim nodx As MSComctlLib.node
Dim sImage As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
    
    If g_bSinPermisos Then
        SSTabPresupuestos.Tab = 0
        Exit Sub
    End If
    If SSTabPresupuestos.Tab = 1 Then
        If tvwestrorg.selectedItem Is Nothing Then
            If SSTabPresupuestos.TabVisible(0) = True Then
                SSTabPresupuestos.Tab = 0
            End If
        Else 'Si esta asignado y es de a�o, pongo e el combo el a�o del presupuesto asignado
            If g_iTipoPres < 3 Then
                sImage = tvwestrorg.selectedItem.Image
                If Right(sImage, 1) = "A" Then
                    sdbcAnyo.Value = DevolverAnyoAsignado
                End If
            End If
        End If
        
        cmdBuscarUO.Visible = False
        cmdRestaurarUO.Visible = False
        cmdBuscarPres.Visible = True
        cmdRestaurarPres.Visible = True
        Screen.MousePointer = vbHourglass
        
        Select Case g_iTipoPres
            Case 1
                    GenerarEstructuraPresupuestosTipo1 False, True
            Case 2
                    GenerarEstructuraPresupuestosTipo2 False, True
            Case 3
                    GenerarEstructuraPresupuestosTipo3 False
            Case 4
                    GenerarEstructuraPresupuestosTipo4 False
        End Select
        Screen.MousePointer = vbNormal

        txtObj.Text = ""
        txtPresupuesto.Text = ""
        If txtAbierto.Visible Then
            m_bRespetarPorcen = True
            txtAsignar.Text = ""
        End If
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
    
    Else
        Set nodx = tvwestrorg.selectedItem
        
        If nodx Is Nothing Then Exit Sub
        
        cmdBuscarUO.Visible = True
        cmdRestaurarUO.Visible = True
        cmdBuscarPres.Visible = False
        cmdRestaurarPres.Visible = False
        
        ActualizarEstrOrg nodx
        
'        tvwestrorg.SetFocus
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "SSTabPresupuestos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub




Private Sub GenerarEstructuraPresupuestosTipo1(ByVal bOrdenadoPorDen As Boolean, Optional ByVal bBuscarEnOtrosAnyos As Boolean = False)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oPRES1 As CPresProyNivel1
Dim oPRES2 As CPresProyNivel2
Dim oPRES3 As CPresProyNivel3
Dim oPRES4 As CPresProyNivel4
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim dblPorcen As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwEstrPres.Nodes.clear

    Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres1, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    Set m_oPresupuestos1 = oFSGSRaiz.generar_CPresProyectosNivel1

    If SSTabPresupuestos.TabVisible(0) = True Then
        
        Set nodo = tvwestrorg.selectedItem
        If nodo Is Nothing Then Exit Sub
        
        Select Case Left(nodo.Tag, 4)
            Case "UON3"
                g_sUON1 = DevolverCod(nodo.Parent.Parent)
                g_sUON2 = DevolverCod(nodo.Parent)
                g_sUON3 = DevolverCod(nodo)
                m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, bBuscarEnOtrosAnyos, False
            Case "UON2"
                g_sUON1 = DevolverCod(nodo.Parent)
                g_sUON2 = DevolverCod(nodo)
                g_sUON3 = ""
                m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, g_sUON1, g_sUON2, , bBuscarEnOtrosAnyos, False
            Case "UON1"
                g_sUON1 = DevolverCod(nodo)
                g_sUON2 = ""
                g_sUON3 = ""
                m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, g_sUON1, , , bBuscarEnOtrosAnyos, False
            Case "UON0"
                g_sUON1 = ""
                g_sUON2 = ""
                g_sUON3 = ""
                m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, , , , bBuscarEnOtrosAnyos, False
        End Select
    Else
         m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, bBuscarEnOtrosAnyos, False
    End If

    
    'Ponemos en la combo el a�o cuyos presupuestos aparecen en pantalla y
    'lo a�adimos a la combo si no se encuentra ya en ella.
    If Not m_oPresupuestos1.Item(1) Is Nothing Then
        sdbcAnyo.Value = m_oPresupuestos1.Item(1).Anyo
        If (m_oPresupuestos1.Item(1).Anyo < Year(Date) - 10) Or (m_oPresupuestos1.Item(1).Anyo > Year(Date) + 10) Then
            If Not ExisteAnyoEnCombo(CStr(m_oPresupuestos1.Item(1).Anyo)) Then
                sdbcAnyo.AddItem sdbcAnyo.Text
            End If
        End If
    End If

    Select Case gParametrosGenerales.giNEPP

        Case 1

                For Each oPRES1 In m_oPresupuestos1
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
                Next

        Case 2

                For Each oPRES1 In m_oPresupuestos1
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresProyectosNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
                    Next
                Next

        Case 3

                For Each oPRES1 In m_oPresupuestos1
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresProyectosNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
     
                            For Each oPRES3 In oPRES2.PresProyectosNivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                            Next
                    Next
                Next

        Case 4

                For Each oPRES1 In m_oPresupuestos1
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresProyectosNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresProyectosNivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                                
                                For Each oPRES4 In oPRES3.PresProyectosNivel4
                                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(oPRES4.Cod))
                                    Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                    nodx.Tag = "PRES4" & oPRES4.Cod & "-%$" & oPRES4.Id & "-%$" & oPRES4.Den & "-%$" & oPRES4.importe
                                Next
                            Next
                    Next
                Next

    End Select
    
    'Marcar los presupuestos asignados
    Dim lIndO As Long
    lIndO = 1
    Set m_oPresupuestos1Otros = oFSGSRaiz.Generar_CPresProyectosNivel4
    
    For Each oPRES4 In m_oPresupuestos1Asignados
        If oPRES4.Anyo = sdbcAnyo.Value And NullToStr(oPRES4.UON1) = g_sUON1 And NullToStr(oPRES4.UON2) = g_sUON2 And NullToStr(oPRES4.UON3) = g_sUON3 Then
                Select Case oPRES4.IDNivel
                    Case 1
                        scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                        Set nodo = tvwEstrPres.Nodes.Item("PRES1" & scod1)
                        nodo.Image = "PRESA"
                        If Not m_bPorcenAsigAbierto Then
                            nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                        Else
                            dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                            If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                                oPRES4.Porcen = CDec(dblPorcen / 100)
                            End If
                            nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                        End If
                        nodo.Expanded = True

                    Case 2
                        scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                        scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES4.CodPRES2))
                        Set nodo = tvwEstrPres.Nodes.Item("PRES2" & scod1 & scod2)
                        nodo.Image = "PRESA"
                        If Not m_bPorcenAsigAbierto Then
                            nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                        Else
                            dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                            If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                                oPRES4.Porcen = CDec(dblPorcen / 100)
                            End If
                            nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                        End If
                        nodo.Parent.Expanded = True

                    Case 3
                        scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                        scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES4.CodPRES2))
                        scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPRES4.CodPRES3))
                        Set nodo = tvwEstrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                        nodo.Image = "PRESA"
                        If Not m_bPorcenAsigAbierto Then
                            nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                        Else
                            dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                            If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                                oPRES4.Porcen = CDec(dblPorcen / 100)
                            End If
                            nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                        End If
                        nodo.Parent.Parent.Expanded = True
                        nodo.Parent.Expanded = True

                    Case 4
                        scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                        scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES4.CodPRES2))
                        scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPRES4.CodPRES3))
                        scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(oPRES4.Cod))
                        Set nodo = tvwEstrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                        nodo.Image = "PRESA"
                        If Not m_bPorcenAsigAbierto Then
                            nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                        Else
                            dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                            If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                                oPRES4.Porcen = CDec(dblPorcen / 100)
                            End If
                            nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                        End If
                        nodo.Parent.Parent.Parent.Expanded = True
                        nodo.Parent.Parent.Expanded = True
                        nodo.Parent.Expanded = True

                End Select
        Else
            m_oPresupuestos1Otros.Add oPRES4.Anyo, oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, oPRES4.Objetivo, lIndO, oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True, oPRES4.BajaLog, oPRES4.FecIniVal, oPRES4.FecFinVal
            lIndO = lIndO + 1
        End If

    Next
    ConfigurarEtiquetas lIndO
    Set oPRES1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    Set nodo = Nothing
    Set nodx = Nothing
    Exit Sub

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "GenerarEstructuraPresupuestosTipo1", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
Private Function ConfigurarEtiquetas(ByVal lIndice As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next

        If lIndice = 1 Then
            cmdOtras.Visible = False
            lblOtras.Visible = False
            lblUO.Width = tvwEstrPres.Width
            sdbcAnyo.Left = lblUO.Left + lblUO.Width - sdbcAnyo.Width
        Else
            cmdOtras.Visible = True
            lblOtras.Visible = True
            lblUO.Width = tvwEstrPres.Width - 1400
            sdbcAnyo.Left = lblUO.Left + lblUO.Width - sdbcAnyo.Width
        End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "ConfigurarEtiquetas", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function


Private Function ExisteAnyoEnCombo(ByVal sAnyo As String) As Boolean
'***********************************************************************************
'*** Descripci�n: Comprueba la existencia en la combo de a�os (sdbcAnyo) del a�o ***
'***              que le es pasado en el argumento de tipo string.               ***
'***                                                                             ***
'*** Par�metros : sAnyo ::>> Es de tipo string y especifica el a�o cuya          ***
'***                         existencia en la combo se est� comprobando.         ***
'***                                                                             ***
'*** Valor que devuelve: TRUE: Si el a�o existe en la combo.                     ***
'***                     FALSE: Si el a�o no existe en la combo.                 ***
'***********************************************************************************
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcAnyo.MoveFirst
    For i = 1 To sdbcAnyo.Rows
        If sdbcAnyo.Columns.Item(0).Value = sAnyo Then
            ExisteAnyoEnCombo = True
            Exit Function
        End If
        sdbcAnyo.MoveNext
    Next i
    ExisteAnyoEnCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "ExisteAnyoEnCombo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Sub GenerarEstructuraPresupuestosTipo2(ByVal bOrdenadoPorDen As Boolean, Optional ByVal bBuscarEnOtrosAnyos As Boolean = False)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oPRES1 As CPresConNivel1
Dim oPRES2 As CPresconNivel2
Dim oPRES3 As CPresConNivel3
Dim oPRES4 As CPresconNivel4
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim dblPorcen As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwEstrPres.Nodes.clear

    Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres2, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    Set m_oPresupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel1

    If SSTabPresupuestos.TabVisible(0) = True Then
        
        Set nodo = tvwestrorg.selectedItem
        If nodo Is Nothing Then Exit Sub
        
        Select Case Left(nodo.Tag, 4)
            Case "UON3"
                g_sUON1 = DevolverCod(nodo.Parent.Parent)
                g_sUON2 = DevolverCod(nodo.Parent)
                g_sUON3 = DevolverCod(nodo)
                m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, bBuscarEnOtrosAnyos, False
            Case "UON2"
                g_sUON1 = DevolverCod(nodo.Parent)
                g_sUON2 = DevolverCod(nodo)
                g_sUON3 = ""
                m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, g_sUON1, g_sUON2, , bBuscarEnOtrosAnyos, False
            Case "UON1"
                g_sUON1 = DevolverCod(nodo)
                g_sUON2 = ""
                g_sUON3 = ""
                m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, g_sUON1, , , bBuscarEnOtrosAnyos, False
            Case "UON0"
                g_sUON1 = ""
                g_sUON2 = ""
                g_sUON3 = ""
                m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, , , , bBuscarEnOtrosAnyos, False
        End Select
    Else
        m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, bBuscarEnOtrosAnyos, False
    End If

    
    'Ponemos en la combo el a�o cuyos presupuestos aparecen en pantalla y
    'lo a�adimos a la combo si no se encuentra ya en ella.
    If Not m_oPresupuestos2.Item(1) Is Nothing Then
        sdbcAnyo.Text = m_oPresupuestos2.Item(1).Anyo
        If (m_oPresupuestos2.Item(1).Anyo < Year(Date) - 10) Or (m_oPresupuestos2.Item(1).Anyo > Year(Date) + 10) Then
            If Not ExisteAnyoEnCombo(CStr(m_oPresupuestos2.Item(1).Anyo)) Then
                sdbcAnyo.AddItem sdbcAnyo.Text
            End If
        End If
    End If

    Select Case gParametrosGenerales.giNEPC

        Case 1

                For Each oPRES1 In m_oPresupuestos2
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
                Next

        Case 2

                For Each oPRES1 In m_oPresupuestos2
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresContablesNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
                    Next
                Next

        Case 3

                For Each oPRES1 In m_oPresupuestos2
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresContablesNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresContablesNivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                            Next
                    Next
                Next

        Case 4

                For Each oPRES1 In m_oPresupuestos2
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresContablesNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresContablesNivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                                
                                For Each oPRES4 In oPRES3.PresContablesNivel4
                                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(oPRES4.Cod))
                                    Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                    nodx.Tag = "PRES4" & oPRES4.Cod & "-%$" & oPRES4.Id & "-%$" & oPRES4.Den & "-%$" & oPRES4.importe
                                Next
                            Next
                    Next
                Next

    End Select
    
    'Marcar los presupuestos asignados
    Dim lIndO As Long
    lIndO = 1
    Set m_oPresupuestos2Otros = oFSGSRaiz.Generar_CPresContablesNivel4
    For Each oPRES4 In m_oPresupuestos2Asignados
        If oPRES4.Anyo = sdbcAnyo.Value And NullToStr(oPRES4.UON1) = g_sUON1 And NullToStr(oPRES4.UON2) = g_sUON2 And NullToStr(oPRES4.UON3) = g_sUON3 Then
            Select Case oPRES4.IDNivel
                Case 1
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES1" & scod1)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Expanded = True
                    
                Case 2
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES4.CodPRES2))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES2" & scod1 & scod2)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Parent.Expanded = True
                
                Case 3
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES4.CodPRES3))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                
                Case 4
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES4.CodPRES3))
                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(oPRES4.Cod))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Parent.Parent.Parent.Expanded = True
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                    
            End Select
        Else
            m_oPresupuestos2Otros.Add oPRES4.Anyo, oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, oPRES4.Objetivo, lIndO, oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True
            lIndO = lIndO + 1
        End If
    Next
    ConfigurarEtiquetas lIndO
    Set oPRES1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    Set nodo = Nothing
    Set nodx = Nothing
    Exit Sub

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "GenerarEstructuraPresupuestosTipo2", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub GenerarEstructuraPresupuestosTipo3(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oPRES1 As CPresConcep3Nivel1
Dim oPRES2 As CPresConcep3Nivel2
Dim oPRES3 As CPresConcep3Nivel3
Dim oPRES4 As CPresConcep3Nivel4
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim dblPorcen As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwEstrPres.Nodes.clear

    Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres3, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    Set m_oPresupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel1

    If SSTabPresupuestos.TabVisible(0) = True Then
        
        Set nodo = tvwestrorg.selectedItem
        If nodo Is Nothing Then Exit Sub
        
        Select Case Left(nodo.Tag, 4)
            Case "UON3"
                g_sUON1 = DevolverCod(nodo.Parent.Parent)
                g_sUON2 = DevolverCod(nodo.Parent)
                g_sUON3 = DevolverCod(nodo)
                m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, False
            Case "UON2"
                g_sUON1 = DevolverCod(nodo.Parent)
                g_sUON2 = DevolverCod(nodo)
                g_sUON3 = ""
                m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, g_sUON1, g_sUON2, , False
            Case "UON1"
                g_sUON1 = DevolverCod(nodo)
                g_sUON2 = ""
                g_sUON3 = ""
                m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, g_sUON1, , , False
            Case "UON0"
                g_sUON1 = ""
                g_sUON2 = ""
                g_sUON3 = ""
                m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, , , , False
        End Select
    Else
        m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, False
    End If

    
    Select Case gParametrosGenerales.giNEP3

        Case 1

                For Each oPRES1 In m_oPresupuestos3
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
                Next

        Case 2

                For Each oPRES1 In m_oPresupuestos3
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos3Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
                    Next
                Next

        Case 3

                For Each oPRES1 In m_oPresupuestos3
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos3Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresConceptos3Nivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                            Next
                    Next
                Next

        Case 4

                For Each oPRES1 In m_oPresupuestos3
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos3Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresConceptos3Nivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                                
                                For Each oPRES4 In oPRES3.PresConceptos3Nivel4
                                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34 - Len(oPRES4.Cod))
                                    Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                    nodx.Tag = "PRES4" & oPRES4.Cod & "-%$" & oPRES4.Id & "-%$" & oPRES4.Den & "-%$" & oPRES4.importe
                                Next
                            Next
                    Next
                Next

    End Select
    
    Dim lIndO As Long
    lIndO = 1
    Set m_oPresupuestos3Otros = oFSGSRaiz.Generar_CPresConceptos3Nivel4
    For Each oPRES4 In m_oPresupuestos3Asignados
        If NullToStr(oPRES4.UON1) = g_sUON1 And NullToStr(oPRES4.UON2) = g_sUON2 And NullToStr(oPRES4.UON3) = g_sUON3 Then
            Select Case oPRES4.IDNivel
                Case 1
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES4.CodPRES1))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES1" & scod1)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
'                        dblPorcen = (oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto) * 100
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(g_dblAsignado)) / CDbl(g_dblAbierto)) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Expanded = True
                    
                Case 2
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES4.CodPRES2))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES2" & scod1 & scod2)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(g_dblAsignado)) / CDbl(g_dblAbierto)) * 100
'                        dblPorcen = (oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Parent.Expanded = True
                
                Case 3
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPRES4.CodPRES3))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(g_dblAsignado)) / CDbl(g_dblAbierto)) * 100
'                        dblPorcen = (oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                
                Case 4
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPRES4.CodPRES3))
                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34 - Len(oPRES4.Cod))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(g_dblAsignado)) / CDbl(g_dblAbierto)) * 100
'                        dblPorcen = (oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Parent.Parent.Parent.Expanded = True
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                    
            End Select
        Else
            m_oPresupuestos3Otros.Add oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, oPRES4.Objetivo, lIndO, oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True
            lIndO = lIndO + 1
        End If
    Next
    ConfigurarEtiquetas lIndO
    Set oPRES1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    Set nodo = Nothing
    Set nodx = Nothing
    Exit Sub

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "GenerarEstructuraPresupuestosTipo3", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub GenerarEstructuraPresupuestosTipo4(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oPRES1 As CPresConcep4Nivel1
Dim oPRES2 As CPresConcep4Nivel2
Dim oPRES3 As CPresConcep4Nivel3
Dim oPRES4 As CPresConcep4Nivel4
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
Dim dblPorcen As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwEstrPres.Nodes.clear

    Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres4, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    Set m_oPresupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel1

    If SSTabPresupuestos.TabVisible(0) = True Then
        
        Set nodo = tvwestrorg.selectedItem
        If nodo Is Nothing Then Exit Sub
        
        Select Case Left(nodo.Tag, 4)
            Case "UON3"
                g_sUON1 = DevolverCod(nodo.Parent.Parent)
                g_sUON2 = DevolverCod(nodo.Parent)
                g_sUON3 = DevolverCod(nodo)
                m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, False
            Case "UON2"
                g_sUON1 = DevolverCod(nodo.Parent)
                g_sUON2 = DevolverCod(nodo)
                g_sUON3 = ""
                m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, g_sUON1, g_sUON2, , False
            Case "UON1"
                g_sUON1 = DevolverCod(nodo)
                g_sUON2 = ""
                g_sUON3 = ""
                m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, g_sUON1, , , False
            Case "UON0"
                g_sUON1 = ""
                g_sUON2 = ""
                g_sUON3 = ""
                m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, , , , False
        End Select
    Else
        m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, False
    End If

    
    Select Case gParametrosGenerales.giNEP4

        Case 1

                For Each oPRES1 In m_oPresupuestos4
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
                Next

        Case 2

                For Each oPRES1 In m_oPresupuestos4
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos4Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
                    Next
                Next

        Case 3

                For Each oPRES1 In m_oPresupuestos4
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos4Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresConceptos4Nivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                            Next
                    Next
                Next

        Case 4

                For Each oPRES1 In m_oPresupuestos4
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos4Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresConceptos4Nivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                                
                                For Each oPRES4 In oPRES3.PresConceptos4Nivel4
                                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(oPRES4.Cod))
                                    Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                    nodx.Tag = "PRES4" & oPRES4.Cod & "-%$" & oPRES4.Id & "-%$" & oPRES4.Den & "-%$" & oPRES4.importe
                                Next
                            Next
                    Next
                Next

    End Select
    
    'Marcar los presupuestos asignados, al proceso o al grupo
    Dim lIndO As Long
    lIndO = 1
    Set m_oPresupuestos4Otros = oFSGSRaiz.Generar_CPresConceptos4Nivel4
    For Each oPRES4 In m_oPresupuestos4Asignados
        If NullToStr(oPRES4.UON1) = g_sUON1 And NullToStr(oPRES4.UON2) = g_sUON2 And NullToStr(oPRES4.UON3) = g_sUON3 Then
            Select Case oPRES4.IDNivel
                Case 1
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES4.CodPRES1))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES1" & scod1)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Expanded = True
                    
                Case 2
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES4.CodPRES2))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES2" & scod1 & scod2)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Parent.Expanded = True
                
                Case 3
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPRES4.CodPRES3))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                
                Case 4
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPRES4.CodPRES3))
                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(oPRES4.Cod))
                    Set nodo = tvwEstrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                    nodo.Image = "PRESA"
                    If Not m_bPorcenAsigAbierto Then
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    Else
                        dblPorcen = CDec((oPRES4.Porcen * CDbl(txtAsignado.Text)) / CDbl(g_dblAbierto)) * 100
                        If dblPorcen <> oPRES4.Porcen Then 'Reasignar presupuestos
                            oPRES4.Porcen = CDec(dblPorcen / 100)
                        End If
                        nodo.Text = nodo.Text & "(" & dblPorcen & "%)"
                    End If
                    nodo.Parent.Parent.Parent.Expanded = True
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                    
            End Select
        Else
            m_oPresupuestos4Otros.Add oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, oPRES4.Objetivo, lIndO, oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True
            lIndO = lIndO + 1
        End If
    Next
    ConfigurarEtiquetas lIndO
    Set oPRES1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    Set nodx = Nothing
    Set nodo = Nothing
    Exit Sub

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "GenerarEstructuraPresupuestosTipo4", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function
    
    Select Case Left(node.Tag, 5)
        Case "PRES1", "PRES2", "PRES3", "PRES4"
            iPosicion = InStr(5, node.Tag, "-%$")
            DevolverCod = Mid(node.Tag, 6, iPosicion - 6)
    End Select
    
    Select Case Left(node.Tag, 4)
        Case "UON1", "UON2", "UON3"
            DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Function DevolverDen(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer
Dim iPos3 As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function

    'posici�n del primer separador
    iPosicion = InStr(1, node.Tag, "-%$")
    'posici�n del segundo separador
    iPosicion = InStr(iPosicion + 1, node.Tag, "-%$")
    'posici�n del tercer separador
    iPos3 = InStr(iPosicion + 1, node.Tag, "-%$")
    iPosicion = iPosicion + 3
    DevolverDen = Mid(node.Tag, iPosicion, iPos3 - iPosicion)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "DevolverDen", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function
    
    'posici�n del primer separador
    iPosicion = InStr(6, node.Tag, "-%$")
    DevolverId = val(Right(node.Tag, Len(node.Tag) - (iPosicion + 2)))
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "DevolverId", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub tvwestrorg_Collapse(ByVal node As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
  ConfigurarInterfazSeguridadUON node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "tvwestrorg_Collapse", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub tvwestrorg_NodeClick(ByVal node As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfazSeguridadUON node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "tvwestrorg_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub tvwEstrPres_NodeClick(ByVal node As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfazPresup node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "tvwEstrPres_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Public Sub MostrarDatosBarraInf()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim vImporte As Variant
Dim vObjetivo As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 5)
        
            Case "Raiz "
                
                txtPresupuesto.Text = ""
                txtObj.Text = ""
                
            Case "PRES1"
            
                Select Case g_iTipoPres
                    Case 1
                        scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx)))
                        txtPresupuesto.Text = DblToStr(m_oPresupuestos1.Item(scod1).importe)
                        txtObj.Text = Format(DblToStr(m_oPresupuestos1.Item(scod1).Objetivo), "0.0#\%")
                    Case 2
                        scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx)))
                        txtPresupuesto.Text = DblToStr(m_oPresupuestos2.Item(scod1).importe)
                        txtObj.Text = Format(DblToStr(m_oPresupuestos2.Item(scod1).Objetivo), "0.0#\%")
                    Case 3
                        scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(DevolverCod(nodx)))
                        txtPresupuesto.Text = DblToStr(m_oPresupuestos3.Item(scod1).importe)
                        txtObj.Text = Format(DblToStr(m_oPresupuestos3.Item(scod1).Objetivo), "0.0#\%")
                    Case 4
                        scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx)))
                        txtPresupuesto.Text = DblToStr(m_oPresupuestos4.Item(scod1).importe)
                        txtObj.Text = Format(DblToStr(m_oPresupuestos4.Item(scod1).Objetivo), "0.0#\%")
                
                End Select
                
            Case "PRES2"
            
                Select Case g_iTipoPres
                    Case 1
                        scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx.Parent)))
                        scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).importe
                        vObjetivo = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).Objetivo
                    Case 2
                        scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent)))
                        scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe
                        vObjetivo = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Objetivo
                    Case 3
                        scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(DevolverCod(nodx.Parent)))
                        scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).importe
                        vObjetivo = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).Objetivo
                    Case 4
                        scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent)))
                        scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).importe
                        vObjetivo = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).Objetivo
                
                End Select
                txtPresupuesto.Text = DblToStr(vImporte)
                txtObj.Text = Format(DblToStr(vObjetivo), "0.0#\%")
            
            Case "PRES3"
            
                Select Case g_iTipoPres
                    Case 1
                        scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(DevolverCod(nodx.Parent)))
                        scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).importe
                        vObjetivo = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).Objetivo
                    Case 2
                        scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent)))
                        scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe
                        vObjetivo = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Objetivo
                    Case 3
                        scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(DevolverCod(nodx.Parent)))
                        scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).PresConceptos3Nivel3.Item(scod1 & scod2 & scod3).importe
                        vObjetivo = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).PresConceptos3Nivel3.Item(scod1 & scod2 & scod3).Objetivo
                    Case 4
                        scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx.Parent)))
                        scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).importe
                        vObjetivo = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).Objetivo
                End Select
                txtPresupuesto.Text = DblToStr(vImporte)
                txtObj.Text = Format(DblToStr(vObjetivo), "0.0#\%")
            
            Case "PRES4"
            
                Select Case g_iTipoPres
                    Case 1
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(DevolverCod(nodx.Parent)))
                        scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).PresProyectosNivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                        vObjetivo = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).PresProyectosNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                    Case 2
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx.Parent)))
                        scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                        vObjetivo = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                    Case 3
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(DevolverCod(nodx.Parent)))
                        scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).PresConceptos3Nivel3.Item(scod1 & scod2 & scod3).PresConceptos3Nivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                        vObjetivo = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).PresConceptos3Nivel3.Item(scod1 & scod2 & scod3).PresConceptos3Nivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                    Case 4
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(DevolverCod(nodx.Parent)))
                        scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).PresConceptos4Nivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                        vObjetivo = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).PresConceptos4Nivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                End Select
                txtPresupuesto.Text = DblToStr(vImporte)
                txtObj.Text = Format(DblToStr(vObjetivo), "0.0#\%")
        
        End Select
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "MostrarDatosBarraInf", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub





Private Sub txtAsignar_Change()
Dim nodx As MSComctlLib.node
Dim dblImporteOld As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwEstrPres.selectedItem
    If nodx Is Nothing Then
        txtAsignar.Text = ""
        Exit Sub
    End If
    
    If nodx.Parent Is Nothing Then
        txtAsignar.Text = ""
        Exit Sub
    End If
    
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    dblImporteOld = DevolverPorcentaje(nodx)
    dblImporteOld = g_dblAbierto * dblImporteOld
    
    If Trim(txtAsignar.Text) = "" Then
        If dblImporteOld <> 0 Then
            m_bRespetarPorcen = True
            txtPorcenAsignar.Text = ""
            m_bRespetarPorcen = True
            Slider1.Value = 0
            m_stexto = QuitarPorcentaje(m_stexto)
            tvwEstrPres.selectedItem.Text = m_stexto
            tvwEstrPres.selectedItem.Image = Left(tvwEstrPres.selectedItem.Tag, 5)
            m_bRespetarPorcen = False
            
            AsignarPres (0)
            
            RefrescarTotales
            
            If CDbl(txtPorcenPend.Text) >= 0 Then
                txtPorcenPend.Backcolor = txtPorcenAsig.Backcolor
                txtPendiente.Backcolor = txtAsignado.Backcolor
            End If
        End If
        Exit Sub
    End If
        
    If Not IsNumeric(txtAsignar.Text) Then
        m_bRespetarPorcen = True
        txtAsignar.Text = Left(txtAsignar.Text, Len(txtAsignar.Text) - 1)
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    Set nodx = tvwEstrPres.selectedItem
    
    If nodx Is Nothing Then Exit Sub
    
    m_stexto = nodx.Text
    
    If Mid(nodx.Image, 5, 1) = "A" Then
        m_stexto = QuitarPorcentaje(m_stexto)
    End If
    
    If txtAsignar.Text <= 0 Then
        If txtAsignar.Text <> 0 Then
            m_bRespetarPorcen = True
            txtAsignar.Text = ""
        End If
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        
        tvwEstrPres.selectedItem.Text = m_stexto
        tvwEstrPres.selectedItem.Image = Left(tvwEstrPres.selectedItem.Tag, 5)
        
        AsignarPres (0)
        RefrescarTotales
        
        
        If CDbl(txtPorcenPend.Text) >= 0 Then
            txtPorcenPend.Backcolor = txtPorcenAsig.Backcolor
            txtPendiente.Backcolor = txtAsignado.Backcolor
        End If
        
    Else
        If CDbl(txtAsignar.Text) <> dblImporteOld Then
            If CDec(txtAsignar.Text / g_dblAbierto) * 100 < 32767 Then
                m_bRespetarPorcen = True
                Slider1.Value = Int(CDec(txtAsignar.Text / g_dblAbierto) * 100)
            End If
            m_bRespetarPorcen = True
            txtPorcenAsignar.Text = CDec(txtAsignar.Text / g_dblAbierto) * 100
            m_bRespetarPorcen = False
            tvwEstrPres.selectedItem.Text = m_stexto & " (" & CDbl(txtPorcenAsignar.Text) & "%" & ")"
            tvwEstrPres.selectedItem.Image = "PRESA"
            
            AsignarPres CDec(txtAsignar.Text / g_dblAbierto)
            
            RefrescarTotales
            
            If CDbl(txtPorcenPend.Text) >= 0 Then
                txtPorcenPend.Backcolor = txtPorcenAsig.Backcolor
                txtPendiente.Backcolor = txtAsignado.Backcolor
            End If
            If CDbl(txtPorcenAsig.Text) > 100 Then
                txtAsignar.Text = Left(txtAsignar.Text, Len(txtAsignar.Text) - 1)
            End If
        End If
                        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "txtAsignar_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub



Private Function QuitarPorcentaje(ByVal sTexto As String) As String
Dim i As Integer
Dim sAux As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sAux = sTexto
    
    If sTexto = "" Then Exit Function
    
    i = Len(sTexto)
    
    If i = 1 Then Exit Function
    
    sTexto = Left(sTexto, Len(sTexto) - 1)
    
    While i > 1
        
        If Mid(sTexto, Len(sTexto), 1) <> "(" Then
            sTexto = Left(sTexto, Len(sTexto) - 1)
            i = i - 1
        Else
            sTexto = Left(sTexto, Len(sTexto) - 2)
            i = -1
        End If
    
    Wend
    
    If i = 0 Or i = 1 Then
        sTexto = sAux
    End If
    
    QuitarPorcentaje = sTexto
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "QuitarPorcentaje", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function



Private Sub txtPorcenAsignar_Change()
Dim nodx As MSComctlLib.node
Dim dPorcen As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwEstrPres.selectedItem
    If nodx Is Nothing Then
        txtPorcenAsignar.Text = ""
        Exit Sub
    End If
    
    If nodx.Parent Is Nothing Then
        txtPorcenAsignar.Text = ""
        Exit Sub
    End If
    
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If

    dPorcen = DevolverPorcentaje(nodx)
    
    If Trim(txtPorcenAsignar.Text) = "" Then
        If dPorcen <> 0 Then
            If txtAbierto.Visible Then
                m_bRespetarPorcen = True
                txtAsignar.Text = ""
            End If
            m_bRespetarPorcen = True
            Slider1.Value = 0
            m_stexto = QuitarPorcentaje(m_stexto)
            tvwEstrPres.selectedItem.Text = m_stexto
            tvwEstrPres.selectedItem.Image = Left(tvwEstrPres.selectedItem.Tag, 5)
            m_bRespetarPorcen = False
            
            AsignarPres (0)
            
            RefrescarTotales
            If CDbl(txtPorcenPend.Text) >= 0 Then
                txtPorcenPend.Backcolor = txtPorcenAsig.Backcolor
                txtPendiente.Backcolor = txtAsignado.Backcolor
            End If
        End If
        Exit Sub
    End If
    
    If Not IsNumeric(txtPorcenAsignar.Text) Then
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = Left(txtPorcenAsignar.Text, Len(txtPorcenAsignar.Text) - 1)
        m_bRespetarPorcen = False
        Exit Sub
    End If

    m_stexto = nodx.Text

    If Mid(nodx.Image, 5, 1) = "A" Then
        m_stexto = QuitarPorcentaje(m_stexto)
    End If

    If txtPorcenAsignar.Text <= 0 Then
        If txtPorcenAsignar.Text <> 0 Then
            m_bRespetarPorcen = True
            txtPorcenAsignar.Text = ""
        End If
        m_bRespetarPorcen = True
        Slider1.Value = 0
        If txtAbierto.Visible Then
            m_bRespetarPorcen = True
            txtAsignar.Text = ""
        End If
        m_bRespetarPorcen = False
        tvwEstrPres.selectedItem.Text = m_stexto
        tvwEstrPres.selectedItem.Image = Left(tvwEstrPres.selectedItem.Tag, 5)
        
        AsignarPres (0)
        
        RefrescarTotales
        
        If CDbl(txtPorcenPend.Text) >= 0 Then
            txtPorcenPend.Backcolor = txtPorcenAsig.Backcolor
            txtPendiente.Backcolor = txtAsignado.Backcolor
        End If
    Else
        If txtPorcenAsignar.Text <> CDec(dPorcen * 100) Then
            m_bRespetarPorcen = True
            If txtPorcenAsignar.Text > 100 Then
                While txtPorcenAsignar.Text > 100
                    m_bRespetarPorcen = True
                    txtPorcenAsignar.Text = Left(txtPorcenAsignar.Text, Len(txtPorcenAsignar.Text) - 1)
                Wend
            Else
                If txtPorcenAsignar.Text < 32767 Then
                    Slider1.Value = Int(txtPorcenAsignar.Text)
                End If
            End If
            If txtAbierto.Visible Then
                m_bRespetarPorcen = True
                txtAsignar.Text = CDec(txtPorcenAsignar.Text / 100) * g_dblAbierto
                If CDec(txtAsignar.Text) > g_dblAbierto Then 'pasado a cadena por la comparaci�n de doubles
                    m_bRespetarPorcen = True
                    txtPorcenAsignar.Text = Left(txtPorcenAsignar.Text, Len(txtPorcenAsignar.Text) - 1)
                End If
            End If
            m_bRespetarPorcen = False
    
            tvwEstrPres.selectedItem.Text = m_stexto & " (" & txtPorcenAsignar.Text & "%" & ")"
            tvwEstrPres.selectedItem.Image = "PRESA"
    
            AsignarPres (CDec(txtPorcenAsignar.Text / 100))
            
            RefrescarTotales
            
            If CDbl(txtPorcenPend.Text) >= 0 Then
                txtPorcenPend.Backcolor = txtPorcenAsig.Backcolor
                txtPendiente.Backcolor = txtAsignado.Backcolor
            End If
            If CDbl(txtPorcenAsig.Text) > 100 Then
                txtPorcenAsignar.Text = Left(txtPorcenAsignar.Text, Len(txtPorcenAsignar.Text) - 1)
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "txtPorcenAsignar_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub




Private Sub AsignarPres(ByVal dblPorcen As Double, Optional ByVal nodx As MSComctlLib.node)
'*******************************************************************************************
'M�todo que a�ade, modifica o elimina de la coleccion de presupuestos la selecci�n actual.
'*******************************************************************************************

'Dim nodx As MSComctlLib.Node
Dim lIdNodo As Long
Dim iNivel As Integer
Dim sPres1 As String
Dim sPRES2 As String
Dim sPRES3 As String
Dim sPRES4 As String
Dim sDen As String
Dim oPres1Niv4 As CPresProyNivel4
Dim oPres2Niv4 As CPresconNivel4
Dim oPres3Niv4 As CPresConcep3Nivel4
Dim oPres4Niv4 As CPresConcep4Nivel4
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nodx Is Nothing Then
        Set nodx = tvwEstrPres.selectedItem
    End If

    If nodx Is Nothing Then Exit Sub

    lIdNodo = DevolverId(nodx)
    sCod = DevolverCodigoColeccion(nodx)
    
    Select Case Left(nodx.Tag, 5)
        Case "PRES1"
                    sPres1 = DevolverCod(nodx)
                    sPRES2 = ""
                    sPRES3 = ""
                    sPRES4 = ""
                    iNivel = 1
        Case "PRES2"
                    sPres1 = DevolverCod(nodx.Parent)
                    sPRES2 = DevolverCod(nodx)
                    sPRES3 = ""
                    sPRES4 = ""
                    iNivel = 2
        Case "PRES3"
                    sPres1 = DevolverCod(nodx.Parent.Parent)
                    sPRES2 = DevolverCod(nodx.Parent)
                    sPRES3 = DevolverCod(nodx)
                    sPRES4 = ""
                    iNivel = 3
        Case "PRES4"
                    sPres1 = DevolverCod(nodx.Parent.Parent.Parent)
                    sPRES2 = DevolverCod(nodx.Parent.Parent)
                    sPRES3 = DevolverCod(nodx.Parent)
                    sPRES4 = DevolverCod(nodx)
                    iNivel = 4
    End Select
    
    Select Case g_iTipoPres
        Case 1
                Set oPres1Niv4 = m_oPresupuestos1Asignados.Item(sCod)
                If oPres1Niv4 Is Nothing Then
                    'No existe
                    If dblPorcen <> 0 Then
                        sDen = DevolverDen(nodx)
                        m_oPresupuestos1Asignados.Add sdbcAnyo.Value, sPres1, sPRES2, sPRES3, sPRES4, sDen, , , , StrToNull(g_sUON1), StrToNull(g_sUON2), StrToNull(g_sUON3), lIdNodo, iNivel, dblPorcen, True
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                        g_dblAsignado = CDec(m_dblPorcenAsig * g_dblAbierto)
                        m_dblPendiente = g_dblAbierto - g_dblAsignado
                    End If
                Else
                    'Existe
                    m_dblPorcenAsig = m_dblPorcenAsig - oPres1Niv4.Porcen
                    If dblPorcen = 0 Then
                        m_oPresupuestos1Asignados.Remove sCod
                    Else
                        oPres1Niv4.Porcen = dblPorcen
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                    g_dblAsignado = CDec(m_dblPorcenAsig * g_dblAbierto)
                    m_dblPendiente = g_dblAbierto - g_dblAsignado
                End If
                
        Case 2
                Set oPres2Niv4 = m_oPresupuestos2Asignados.Item(sCod)
                If oPres2Niv4 Is Nothing Then
                    'No existe
                    If dblPorcen <> 0 Then
                        sDen = DevolverDen(nodx)
                        m_oPresupuestos2Asignados.Add sdbcAnyo.Value, sPres1, sPRES2, sPRES3, sPRES4, sDen, , , , StrToNull(g_sUON1), StrToNull(g_sUON2), StrToNull(g_sUON3), lIdNodo, iNivel, dblPorcen, True
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                        g_dblAsignado = CDec(m_dblPorcenAsig * g_dblAbierto)
                        m_dblPendiente = g_dblAbierto - g_dblAsignado
                    End If
                Else
                    'Existe
                    m_dblPorcenAsig = m_dblPorcenAsig - oPres2Niv4.Porcen
                    If dblPorcen = 0 Then
                        m_oPresupuestos2Asignados.Remove sCod
                    Else
                        oPres2Niv4.Porcen = dblPorcen
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                    g_dblAsignado = CDec(m_dblPorcenAsig * g_dblAbierto)
                    m_dblPendiente = g_dblAbierto - g_dblAsignado
                End If
        Case 3
                Set oPres3Niv4 = m_oPresupuestos3Asignados.Item(sCod)
                If oPres3Niv4 Is Nothing Then
                    'No existe
                    If dblPorcen <> 0 Then
                        sDen = DevolverDen(nodx)
                        m_oPresupuestos3Asignados.Add sPres1, sPRES2, sPRES3, sPRES4, sDen, , , , StrToNull(g_sUON1), StrToNull(g_sUON2), StrToNull(g_sUON3), lIdNodo, iNivel, dblPorcen, True
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                        g_dblAsignado = CDec(m_dblPorcenAsig * g_dblAbierto)
                        m_dblPendiente = g_dblAbierto - g_dblAsignado
                    End If
                Else
                    'Existe
                    m_dblPorcenAsig = m_dblPorcenAsig - oPres3Niv4.Porcen
                    If dblPorcen = 0 Then
                        m_oPresupuestos3Asignados.Remove sCod
                    Else
                        oPres3Niv4.Porcen = dblPorcen
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                    g_dblAsignado = CDec(m_dblPorcenAsig * g_dblAbierto)
                    m_dblPendiente = g_dblAbierto - g_dblAsignado
                End If
        Case 4
                
                Set oPres4Niv4 = m_oPresupuestos4Asignados.Item(sCod)
                If oPres4Niv4 Is Nothing Then
                    'No existe
                    If dblPorcen <> 0 Then
                        sDen = DevolverDen(nodx)
                        m_oPresupuestos4Asignados.Add sPres1, sPRES2, sPRES3, sPRES4, sDen, , , , StrToNull(g_sUON1), StrToNull(g_sUON2), StrToNull(g_sUON3), lIdNodo, iNivel, dblPorcen, True
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                        g_dblAsignado = CDec(m_dblPorcenAsig * g_dblAbierto)
                        m_dblPendiente = g_dblAbierto - g_dblAsignado
                    End If
                Else
                    'Existe
                    m_dblPorcenAsig = m_dblPorcenAsig - oPres4Niv4.Porcen
                    If dblPorcen = 0 Then
                        m_oPresupuestos4Asignados.Remove sCod
                    Else
                        oPres4Niv4.Porcen = dblPorcen
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                    g_dblAsignado = CDec(m_dblPorcenAsig * g_dblAbierto)
                    m_dblPendiente = g_dblAbierto - g_dblAsignado
                End If

    End Select

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "AsignarPres", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Function DevolverCodigoColeccion(nodx As MSComctlLib.node) As String
Dim sCod As String
Dim sPres1 As String
Dim sPRES2 As String
Dim sPRES3 As String
Dim sPRES4 As String
Dim sTag As String
Dim iLongP1 As Integer
Dim iLongP2 As Integer
Dim iLongP3 As Integer
Dim iLongP4 As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbcAnyo.Visible Then
    sCod = sdbcAnyo.Text & "%UON0-%$"
Else
    sCod = "%UON0-%$"
End If
    
If g_sUON1 <> "" Then
    sCod = sCod & "%UON1-%$" & g_sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(g_sUON1))
    If g_sUON2 <> "" Then
        sCod = sCod & "%UON2-%$" & g_sUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(g_sUON2))
        If g_sUON3 <> "" Then
            sCod = sCod & "%UON3-%$" & g_sUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(g_sUON3))
        End If
    End If
End If
    
Select Case Left(nodx.Tag, 5)
Case "PRES1"
    sPres1 = Left(nodx.Tag, InStr(1, nodx.Tag, "-%$") - 1)
    sPres1 = Right(sPres1, Len(sPres1) - 5)
    sPRES2 = ""
    sPRES3 = ""
    sPRES4 = ""
Case "PRES2"
    sTag = nodx.Parent.Tag
    sPres1 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPres1 = Right(sPres1, Len(sPres1) - 5)
    sPRES2 = Left(nodx.Tag, InStr(1, nodx.Tag, "-%$") - 1)
    sPRES2 = Right(sPRES2, Len(sPRES2) - 5)
Case "PRES3"
    sTag = nodx.Parent.Parent.Tag
    sPres1 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPres1 = Right(sPres1, Len(sPres1) - 5)
    sTag = nodx.Parent.Tag
    sPRES2 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPRES2 = Right(sPRES2, Len(sPRES2) - 5)
    sPRES3 = Left(nodx.Tag, InStr(1, nodx.Tag, "-%$") - 1)
    sPRES3 = Right(sPRES3, Len(sPRES3) - 5)
Case "PRES4"
    sTag = nodx.Parent.Parent.Parent.Tag
    sPres1 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPres1 = Right(sPres1, Len(sPres1) - 5)
    sTag = nodx.Parent.Parent.Tag
    sPRES2 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPRES2 = Right(sPRES2, Len(sPRES2) - 5)
    sTag = nodx.Parent.Tag
    sPRES3 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPRES3 = Right(sPRES3, Len(sPRES3) - 5)
    sPRES4 = Left(nodx.Tag, InStr(1, nodx.Tag, "-%$") - 1)
    sPRES4 = Right(sPRES4, Len(sPRES4) - 5)
End Select
Select Case g_iTipoPres
Case 1
    iLongP1 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1
    iLongP2 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2
    iLongP3 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3
    iLongP4 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4
Case 2
    iLongP1 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON1
    iLongP2 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON2
    iLongP3 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON3
    iLongP4 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON4
Case 3
    iLongP1 = basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31
    iLongP2 = basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32
    iLongP3 = basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33
    iLongP4 = basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34
Case 4
    iLongP1 = basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41
    iLongP2 = basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42
    iLongP3 = basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43
    iLongP4 = basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44
End Select

sCod = sCod & "%PRES1-%$" & sPres1 & Mid$("                         ", 1, iLongP1 - Len(sPres1))
sCod = sCod & "%PRES2-%$" & sPRES2 & Mid$("                         ", 1, iLongP2 - Len(sPRES2))
sCod = sCod & "%PRES3-%$" & sPRES3 & Mid$("                         ", 1, iLongP3 - Len(sPRES3))
sCod = sCod & "%PRES4-%$" & sPRES4 & Mid$("                         ", 1, iLongP4 - Len(sPRES4))

DevolverCodigoColeccion = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "DevolverCodigoColeccion", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function



Private Sub RefrescarTotales()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtPorcenAsig.Text = TruncarDato(CDec(m_dblPorcenAsig * 100))
    txtPorcenAsig.ToolTipText = CDec(m_dblPorcenAsig * 100)
    txtPorcenPend.Text = TruncarDato(100 - CDec(m_dblPorcenAsig * 100))
    txtPorcenPend.ToolTipText = 100 - CDec(m_dblPorcenAsig * 100)
    If txtAbierto.Visible Then
        txtAsignado.Text = TruncarDato(g_dblAsignado)
        txtAsignado.ToolTipText = g_dblAsignado
        txtPendiente.Text = TruncarDato(m_dblPendiente)
        txtPendiente.ToolTipText = m_dblPendiente
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "RefrescarTotales", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Function SoloUnHermanoAsignado(ByVal node As MSComctlLib.node) As Object
Dim lCount As Long
Dim nod As MSComctlLib.node
Dim nodoHermano As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
lCount = 0

Set nodoHermano = Nothing

Set nod = node.FirstSibling
While Not nod Is Nothing
    If Mid(nod.Image, 5, 1) = "A" And nod.key <> node.key Then
        Set nodoHermano = Nothing
        Set nodoHermano = nod
        lCount = lCount + 1
    End If
    Set nod = nod.Next
Wend

'La modificamos para que compruebe lo mismo con el padre
If nodoHermano Is Nothing Then
    Set nod = node.Parent
    While Not nod Is Nothing
        If Mid(nod.Image, 5, 1) = "A" And nod.key <> node.key Then
            Set nodoHermano = Nothing
            Set nodoHermano = nod
        End If
        Set nod = nod.Parent
    Wend
End If

Set SoloUnHermanoAsignado = nodoHermano
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "SoloUnHermanoAsignado", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function SumaDeLosHijos(ByVal node As MSComctlLib.node) As Double
Dim nodo As MSComctlLib.node
Dim dSuma As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
dSuma = 0
If node Is Nothing Then Exit Function
Set nodo = Nothing
Set nodo = node.Child
While Not nodo Is Nothing
    If Mid(nodo.Image, 5, 1) = "A" Then
        dSuma = dSuma + DevolverPorcentaje(nodo)
    End If
    dSuma = dSuma + SumaDeLosHijos(nodo)
    Set nodo = nodo.Next
Wend

SumaDeLosHijos = dSuma
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "SumaDeLosHijos", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Private Sub LimpiarHijos(ByVal node As MSComctlLib.node)
Dim nodo As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If node Is Nothing Then Exit Sub
Set nodo = node.Child
While Not nodo Is Nothing
    If Mid(nodo.Image, 5, 1) = "A" Then
        nodo.Text = QuitarPorcentaje(nodo.Text)
        AsignarPres 0, nodo
        nodo.Image = "PRES" & Mid(nodo.Tag, 5, 1)
    End If
    LimpiarHijos nodo
    Set nodo = nodo.Next
Wend
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "LimpiarHijos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
Private Function SusPadresNoTienenAsignaciones(ByVal node As MSComctlLib.node) As Boolean
Dim b As Boolean
Dim nod As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
b = True
Set nod = node.Parent
If Not nod Is Nothing Then
    If Mid(nod.Image, 5, 1) = "A" Then
       b = False
    Else
       b = SusPadresNoTienenAsignaciones(nod)
    End If
End If

SusPadresNoTienenAsignaciones = b
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "SusPadresNoTienenAsignaciones", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function ComprobarSiEstaAsignado(Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String) As Boolean
    Dim oPres As Object
    Dim oPresups As Object
    Dim oPerfil As CPerfil
    Dim rstUONsPerf As ADODB.Recordset
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ComprobarSiEstaAsignado = False
    Select Case g_iTipoPres
        Case 1
            If m_oPresupuestos1Asignados Is Nothing Then Exit Function
            If m_oPresupuestos1Asignados.Count = 0 Then Exit Function
            Set oPresups = m_oPresupuestos1Asignados
        Case 2
            If m_oPresupuestos2Asignados Is Nothing Then Exit Function
            If m_oPresupuestos2Asignados.Count = 0 Then Exit Function
            Set oPresups = m_oPresupuestos2Asignados
        Case 3
            If m_oPresupuestos3Asignados Is Nothing Then Exit Function
            If m_oPresupuestos3Asignados.Count = 0 Then Exit Function
            Set oPresups = m_oPresupuestos3Asignados
        Case 4
            If m_oPresupuestos4Asignados Is Nothing Then Exit Function
            If m_oPresupuestos4Asignados.Count = 0 Then Exit Function
            Set oPresups = m_oPresupuestos4Asignados
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    If g_bRPerfUO And lIdPerfil > 0 Then
        Set oPerfil = oFSGSRaiz.Generar_CPerfil
        oPerfil.Id = lIdPerfil
        Set rstUONsPerf = oPerfil.DevolverUONsPerfil
        Set oPerfil = Nothing
    End If
    
    For Each oPres In oPresups
        If sUON3 <> "" Then
            If sUON1 = oPres.UON1 And sUON2 = oPres.UON2 And sUON3 = oPres.UON3 Then
                If g_bRUO Or g_bRPerfUO Then
                    If g_bRUO Then
                        If ComprobarAsignacionUON(sUON1, sUON2, sUON3) Then
                            ComprobarSiEstaAsignado = True
                            Set oPresups = Nothing
                            Exit Function
                        End If
                    End If
                    
                    If g_bRPerfUO Then
                        If Not rstUONsPerf Is Nothing Then
                            If rstUONsPerf.RecordCount > 0 Then
                                rstUONsPerf.MoveFirst
                                While Not rstUONsPerf.EOF
                                    If ComprobarAsignacionUON(NullToStr(rstUONsPerf("UON1")), NullToStr(rstUONsPerf("UON2")), NullToStr(rstUONsPerf("UON3"))) Then
                                        ComprobarSiEstaAsignado = True
                                        Set oPresups = Nothing
                                        Exit Function
                                    End If
                                    
                                    rstUONsPerf.MoveNext
                                Wend
                            End If
                        End If
                    End If
                Else
                    ComprobarSiEstaAsignado = True
                    Set oPresups = Nothing
                    Exit Function
                End If
            End If
        ElseIf sUON2 <> "" Then
            If oPres.UON1 = sUON1 And oPres.UON2 = sUON2 And IsNull(oPres.UON3) Then
                If g_bRUO Or g_bRPerfUO Then
                    If g_bRUO Then
                        If ComprobarAsignacionUON(sUON1, sUON2, sUON3) Then
                            ComprobarSiEstaAsignado = True
                            Set oPresups = Nothing
                            Exit Function
                        End If
                    End If
                    
                    If g_bRPerfUO Then
                        If Not rstUONsPerf Is Nothing Then
                            If rstUONsPerf.RecordCount > 0 Then
                                rstUONsPerf.MoveFirst
                                While Not rstUONsPerf.EOF
                                    If ComprobarAsignacionUON(NullToStr(rstUONsPerf("UON1")), NullToStr(rstUONsPerf("UON2")), NullToStr(rstUONsPerf("UON3"))) Then
                                        ComprobarSiEstaAsignado = True
                                        Set oPresups = Nothing
                                        Exit Function
                                    End If
                                    
                                    rstUONsPerf.MoveNext
                                Wend
                            End If
                        End If
                    End If
                Else
                    ComprobarSiEstaAsignado = True
                    Set oPresups = Nothing
                    Exit Function
                End If
            End If
        ElseIf sUON1 <> "" Then
            If oPres.UON1 = sUON1 And IsNull(oPres.UON2) And IsNull(oPres.UON3) Then
                If g_bRUO Or g_bRPerfUO Then
                    If g_bRUO Then
                        If ComprobarAsignacionUON(sUON1, sUON2, sUON3) Then
                            ComprobarSiEstaAsignado = True
                            Set oPresups = Nothing
                            Exit Function
                        End If
                    End If
                    
                    If g_bRPerfUO Then
                        If Not rstUONsPerf Is Nothing Then
                            If rstUONsPerf.RecordCount > 0 Then
                                rstUONsPerf.MoveFirst
                                While Not rstUONsPerf.EOF
                                    If ComprobarAsignacionUON(NullToStr(rstUONsPerf("UON1")), NullToStr(rstUONsPerf("UON2")), NullToStr(rstUONsPerf("UON3"))) Then
                                        ComprobarSiEstaAsignado = True
                                        Set oPresups = Nothing
                                        Exit Function
                                    End If
                                    
                                    rstUONsPerf.MoveNext
                                Wend
                            End If
                        End If
                    End If
                Else
                    ComprobarSiEstaAsignado = True
                    Set oPresups = Nothing
                    Exit Function
                End If
            End If
        Else
            If IsNull(oPres.UON1) And IsNull(oPres.UON2) And IsNull(oPres.UON3) Then
                If g_bRUO Or g_bRPerfUO Then
                    If m_iUOBase = 0 Then
                        ComprobarSiEstaAsignado = True
                        Set oPresups = Nothing
                        Exit Function
                    Else
                        If g_bRUORama Then
                            ComprobarSiEstaAsignado = True
                            Set oPresups = Nothing
                            Exit Function
                        End If
                    End If
                Else
                    ComprobarSiEstaAsignado = True
                    Set oPresups = Nothing
                    Exit Function
                End If
            End If
        End If
    Next
    
    Set oPresups = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "ComprobarSiEstaAsignado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>Comprueba la asignaci�n</summary>
''' <param name="sUON1">UON1</param>
''' <param name="sUON2">UON2</param>
''' <param name="sUON3">UON3</param>
''' <returns>Booleano indicando la asignaci�n</returns>
''' <remarks>Llamada desde: ComprobarSiEstaAsignado</remarks>
''' <revision>LTG 25/01/2013</revision>

Private Function ComprobarAsignacionUON(ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ComprobarAsignacionUON = False
                    
    Select Case m_iUOBase
        Case 1
            If sUON1 = basOptimizacion.gUON1Usuario Then
                ComprobarAsignacionUON = True
            End If
        Case 2
            If sUON2 <> "" Then
                If sUON1 = basOptimizacion.gUON1Usuario And sUON2 = basOptimizacion.gUON2Usuario Then
                    ComprobarAsignacionUON = True
                End If
            ElseIf sUON1 <> "" Then
                If g_bRUORama Then
                    If sUON1 = basOptimizacion.gUON1Usuario Then
                        ComprobarAsignacionUON = True
                    End If
                End If
            End If
        Case 3
            If sUON3 <> "" Then
                If sUON1 = basOptimizacion.gUON1Usuario And sUON2 = basOptimizacion.gUON2Usuario And sUON3 = basOptimizacion.gUON3Usuario Then
                    ComprobarAsignacionUON = True
                End If
            ElseIf sUON2 <> "" Then
                If g_bRUORama Then
                    If sUON1 = basOptimizacion.gUON1Usuario And sUON2 = basOptimizacion.gUON2Usuario Then
                        ComprobarAsignacionUON = True
                    End If
                End If
            ElseIf sUON1 <> "" Then
                If g_bRUORama Then
                    If sUON1 = basOptimizacion.gUON1Usuario Then
                        ComprobarAsignacionUON = True
                    End If
                End If
            End If
        Case 0
            ComprobarAsignacionUON = True
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "ComprobarAsignacionUON", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Cargar Presupuestos Inicialmente Asignados
''' </summary>
''' <remarks>Llamada desde: Form_Load   cmdRestaurarUO_Click     cmdRestaurarPres_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarAsignadosInicial()
Dim oPRES1 As CPresProyNivel4
Dim oPRES2 As CPresconNivel4
Dim oPRES3 As CPresConcep3Nivel4
Dim oPRES4 As CPresConcep4Nivel4
Dim oProce As CProceso
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim i As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bRestringirMultiplePres = False

If g_sOrigen = "frmPedidosEmision2" Then Exit Sub

Select Case g_iTipoPres
    Case 1
            Set m_oPresupuestos1Asignados = oFSGSRaiz.Generar_CPresProyectosNivel4
    Case 2
            Set m_oPresupuestos2Asignados = oFSGSRaiz.Generar_CPresContablesNivel4
    Case 3
            Set m_oPresupuestos3Asignados = oFSGSRaiz.Generar_CPresConceptos3Nivel4
            sdbcAnyo.Visible = False
    Case 4
            Set m_oPresupuestos4Asignados = oFSGSRaiz.Generar_CPresConceptos4Nivel4
            sdbcAnyo.Visible = False
End Select


'Proceso nuevo, pres en proce o en grupo. Modificar presupuesto de m�ltiples �tems, Configuraci�n de proceso.Proceso nuevo con plantilla y alg�n pres obligatorio
Select Case g_sOrigen
Case "frmPEDIDOS"
    Select Case g_iTipoPres
    Case 1
        For Each oPRES1 In g_oOrigen.g_oLineaSeleccionada.Presupuestos1
            m_oPresupuestos1Asignados.Add oPRES1.Anyo, oPRES1.CodPRES1, oPRES1.CodPRES2, oPRES1.CodPRES3, oPRES1.Cod, oPRES1.Den, oPRES1.importe, , , oPRES1.UON1, oPRES1.UON2, oPRES1.UON3, oPRES1.Id, oPRES1.IDNivel, oPRES1.Porcen, True, oPRES1.BajaLog, oPRES1.FecIniVal, oPRES1.FecFinVal
        Next
    Case 2
        For Each oPRES2 In g_oOrigen.g_oLineaSeleccionada.Presupuestos2
            m_oPresupuestos2Asignados.Add oPRES2.Anyo, oPRES2.CodPRES1, oPRES2.CodPRES2, oPRES2.CodPRES3, oPRES2.Cod, oPRES2.Den, oPRES2.importe, , , oPRES2.UON1, oPRES2.UON2, oPRES2.UON3, oPRES2.Id, oPRES2.IDNivel, oPRES2.Porcen, True, oPRES2.BajaLog, oPRES2.FecIniVal, oPRES2.FecFinVal
        Next
    Case 3
        For Each oPRES3 In g_oOrigen.g_oLineaSeleccionada.Presupuestos3
            m_oPresupuestos3Asignados.Add oPRES3.CodPRES1, oPRES3.CodPRES2, oPRES3.CodPRES3, oPRES3.Cod, oPRES3.Den, oPRES3.importe, , , oPRES3.UON1, oPRES3.UON2, oPRES3.UON3, oPRES3.Id, oPRES3.IDNivel, oPRES3.Porcen, True, oPRES3.BajaLog, oPRES3.FecIniVal, oPRES3.FecFinVal
        Next
    Case 4
        For Each oPRES4 In g_oOrigen.g_oLineaSeleccionada.Presupuestos4
            m_oPresupuestos4Asignados.Add oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, , , oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True, oPRES4.BajaLog, oPRES4.FecIniVal, oPRES4.FecFinVal
        Next
    End Select
    
    
    Set oItem = oFSGSRaiz.Generar_CItem
    m_bRestringirMultiplePres = oItem.RestriccionMultiplePres(g_oOrigen.g_oLineaSeleccionada.Anyo, g_oOrigen.g_oLineaSeleccionada.GMN1Proce, g_oOrigen.g_oLineaSeleccionada.ProceCod, g_oOrigen.g_oLineaSeleccionada.Item, "PEDIR")

    
Case "frmSeguimiento"
    
    'Si no se trata de una l�nea nueva
    If Not IsNull(frmSeguimiento.g_oLineaPedido.Id) Then
        Select Case g_iTipoPres
        Case 1
            m_oPresupuestos1Asignados.CargarPresDeLineaPedido frmSeguimiento.g_oLineaPedido.Id
        Case 2
            m_oPresupuestos2Asignados.CargarPresDeLineaPedido frmSeguimiento.g_oLineaPedido.Id
        Case 3
            m_oPresupuestos3Asignados.CargarPresDeLineaPedido frmSeguimiento.g_oLineaPedido.Id
        Case 4
            m_oPresupuestos4Asignados.CargarPresDeLineaPedido frmSeguimiento.g_oLineaPedido.Id
        End Select
    Else
        'Si se trata de una nueva l�nea los presupuestos ya deber�an estar cargados en la l�nea
        Select Case g_iTipoPres
        Case 1
            For Each oPRES1 In frmSeguimiento.g_oLineaPedido.Presupuestos1
                m_oPresupuestos1Asignados.Add oPRES1.Anyo, oPRES1.CodPRES1, oPRES1.CodPRES2, oPRES1.CodPRES3, oPRES1.Cod, oPRES1.Den, oPRES1.importe, , , oPRES1.UON1, oPRES1.UON2, oPRES1.UON3, oPRES1.Id, oPRES1.IDNivel, oPRES1.Porcen, True, oPRES1.BajaLog, oPRES1.FecIniVal, oPRES1.FecFinVal
            Next
        Case 2
            For Each oPRES2 In frmSeguimiento.g_oLineaPedido.Presupuestos2
                m_oPresupuestos2Asignados.Add oPRES2.Anyo, oPRES2.CodPRES1, oPRES2.CodPRES2, oPRES2.CodPRES3, oPRES2.Cod, oPRES2.Den, oPRES2.importe, , , oPRES2.UON1, oPRES2.UON2, oPRES2.UON3, oPRES2.Id, oPRES2.IDNivel, oPRES2.Porcen, True, oPRES2.BajaLog, oPRES2.FecIniVal, oPRES2.FecFinVal
            Next
        Case 3
            For Each oPRES3 In frmSeguimiento.g_oLineaPedido.Presupuestos3
                m_oPresupuestos3Asignados.Add oPRES3.CodPRES1, oPRES3.CodPRES2, oPRES3.CodPRES3, oPRES3.Cod, oPRES3.Den, oPRES3.importe, , , oPRES3.UON1, oPRES3.UON2, oPRES3.UON3, oPRES3.Id, oPRES3.IDNivel, oPRES3.Porcen, True, oPRES3.BajaLog, oPRES3.FecIniVal, oPRES3.FecFinVal
            Next
        Case 4
            For Each oPRES4 In frmSeguimiento.g_oLineaPedido.Presupuestos4
                m_oPresupuestos4Asignados.Add oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, , , oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True, oPRES4.BajaLog, oPRES4.FecIniVal, oPRES4.FecFinVal
        Next
    End Select
    End If
    
    Set oItem = oFSGSRaiz.Generar_CItem
    m_bRestringirMultiplePres = oItem.RestriccionMultiplePres(NullToDbl0(frmSeguimiento.g_oLineaPedido.Anyo), NullToStr(frmSeguimiento.g_oLineaPedido.GMN1Proce), NullToDbl0(frmSeguimiento.g_oLineaPedido.ProceCod), NullToDbl0(frmSeguimiento.g_oLineaPedido.Item), "PEDIR")
    
    
Case "frmFormularios", "frmDesgloseValores", "frmSolicitudDetalle", "frmSolicitudDesglose", "frmSolicitudDesgloseP"
    If g_sValorPresFormulario <> "" Then
    
    Select Case g_iTipoPres
    Case 1
        m_oPresupuestos1Asignados.CargarPresValorFormulario g_sValorPresFormulario
    Case 2
        m_oPresupuestos2Asignados.CargarPresValorFormulario g_sValorPresFormulario
    Case 3
        m_oPresupuestos3Asignados.CargarPresValorFormulario g_sValorPresFormulario
    Case 4
        m_oPresupuestos4Asignados.CargarPresValorFormulario g_sValorPresFormulario
    End Select

    End If
    
Case "PROCEN", "PROCE_MODIFITEMS", "PROCE_PLANT", "GRUPON", "XLS_ITEMS" ', "frmPROCEModifConfig"
    Select Case g_iAmbitoPresup
            Case 1
                Set oProce = oFSGSRaiz.Generar_CProceso
                m_bRestringirMultiplePres = oProce.RestriccionMultiplePres(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value)
            Case 2
                Set oGrupo = oFSGSRaiz.generar_cgrupo
                m_bRestringirMultiplePres = oGrupo.RestriccionMultiplePres(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id)
            Case 3
                If g_sOrigen = "PROCE_MODIFITEMS" Then
                    m_sItems = ""
                    For i = 1 To UBound(frmPROCE.g_arItems)
                        m_sItems = m_sItems & frmPROCE.g_arItems(i)
                        If i < UBound(frmPROCE.g_arItems) Then
                             m_sItems = m_sItems & ","
                        End If
                    Next
                
                    Set oItem = oFSGSRaiz.Generar_CItem
                    m_bRestringirMultiplePres = oItem.RestriccionMultiplePresVariosItems(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, m_sItems, "Aper")
                Else
                    Set oItem = oFSGSRaiz.Generar_CItem
                    m_bRestringirMultiplePres = oItem.RestriccionMultiplePres(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, "Aper")
                End If
        End Select
    

Case "frmSOLAbrirFaltan"
    m_bRestringirMultiplePres = frmSOLAbrirFaltan.g_oSolicitudSeleccionada.RestriccionMultiplePres
        
Case Else
    
    Select Case g_iAmbitoPresup
        Case 1
            Set oProce = oFSGSRaiz.Generar_CProceso
            m_bRestringirMultiplePres = oProce.RestriccionMultiplePres(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value)
        Case 2
            Set oGrupo = oFSGSRaiz.generar_cgrupo
            If g_sOrigen = "GRUPO_NUEVO" Then
                m_bRestringirMultiplePres = oGrupo.RestriccionMultiplePres(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo)
            Else
                m_bRestringirMultiplePres = oGrupo.RestriccionMultiplePres(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id)
            End If
        Case 3
            Set oItem = oFSGSRaiz.Generar_CItem
            m_bRestringirMultiplePres = oItem.RestriccionMultiplePres(frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id, "Aper")
    End Select
    
    
    Select Case g_iTipoPres
        Case 1
                Select Case g_iAmbitoPresup
                    Case 1
                        m_oPresupuestos1Asignados.CargarPresDeProceso frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value
                    Case 2
                        If g_sOrigen = "GRUPO_NUEVO" Then
                            m_oPresupuestos1Asignados.CargarPresDeGrupo frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo
                        ElseIf g_sOrigen = "frmPROCEModifConfig" Then
                            m_oPresupuestos1Asignados.CargarPresDeProceso frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value
                        Else
                            m_oPresupuestos1Asignados.CargarPresDeGrupo frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id
                        End If
                    Case 3
                        m_oPresupuestos1Asignados.CargarPresDeItem frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id
                End Select
        Case 2
                Select Case g_iAmbitoPresup
                    Case 1
                        m_oPresupuestos2Asignados.CargarPresDeProceso frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value
                    Case 2
                        If g_sOrigen = "GRUPO_NUEVO" Then
                            m_oPresupuestos2Asignados.CargarPresDeGrupo frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo
                        Else
                            m_oPresupuestos2Asignados.CargarPresDeGrupo frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id
                        End If
                    Case 3
                        m_oPresupuestos2Asignados.CargarPresDeItem frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id
                End Select
                
        Case 3
                Select Case g_iAmbitoPresup
                    Case 1
                        m_oPresupuestos3Asignados.CargarPresDeProceso frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value
                    Case 2
                        If g_sOrigen = "GRUPO_NUEVO" Then
                            m_oPresupuestos3Asignados.CargarPresDeGrupo frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo
                        Else
                            m_oPresupuestos3Asignados.CargarPresDeGrupo frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id
                        End If
                    Case 3
                        m_oPresupuestos3Asignados.CargarPresDeItem frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id
                End Select
                
        Case 4
                Select Case g_iAmbitoPresup
                    Case 1
                        m_oPresupuestos4Asignados.CargarPresDeProceso frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value
                    Case 2
                        If g_sOrigen = "GRUPO_NUEVO" Then
                            m_oPresupuestos4Asignados.CargarPresDeGrupo frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, g_sGrupoNuevo
                        ElseIf g_sOrigen = "frmPROCEModifConfig" Then
                            m_oPresupuestos4Asignados.CargarPresDeProceso frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value
                        Else
                            m_oPresupuestos4Asignados.CargarPresDeGrupo frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPROCE.g_oGrupoSeleccionado.Id
                        End If
                    Case 3
                        m_oPresupuestos4Asignados.CargarPresDeItem frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, frmPRESItem.g_oItem.Id
                End Select
            
    End Select

End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "CargarAsignadosInicial", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
Private Sub cmdOtras_Click()
Dim oPres As Object
Dim oPresupuestos As Object
Dim scod1 As String
Dim sDen As String
Dim i As Integer


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case g_iTipoPres
Case 1
    Set oPresupuestos = m_oPresupuestos1Otros
Case 2
    Set oPresupuestos = m_oPresupuestos2Otros
Case 3
    Set oPresupuestos = m_oPresupuestos3Otros
Case 4
    Set oPresupuestos = m_oPresupuestos4Otros
End Select

ReDim m_arTag(1 To 1)

cP.clear

i = 1
For Each oPres In oPresupuestos

    If NullToStr(oPres.UON3) <> "" Then
        scod1 = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        scod1 = scod1 & oPres.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPres.UON2))
        scod1 = scod1 & oPres.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPres.UON3))
        If g_bRUO Or g_bRPerfUO Then
            If m_oUnidadesOrgN3 Is Nothing Then
                Set m_oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
            End If
            sDen = m_oUnidadesOrgN3.DevolverDenominacion(oPres.UON1, oPres.UON2, oPres.UON3)
        Else
            sDen = m_oUnidadesOrgN3.Item(scod1).Den
        End If
    ElseIf NullToStr(oPres.UON2) <> "" Then
        scod1 = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        scod1 = scod1 & oPres.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPres.UON2))
        If g_bRUO Or g_bRPerfUO Then
            If m_oUnidadesOrgN2 Is Nothing Then
                Set m_oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
            End If
            sDen = m_oUnidadesOrgN2.DevolverDenominacion(oPres.UON1, oPres.UON2)
        Else
            sDen = m_oUnidadesOrgN2.Item(scod1).Den
        End If
    ElseIf NullToStr(oPres.UON1) <> "" Then
        scod1 = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        If g_bRUO Or g_bRPerfUO Then
            If m_oUnidadesOrgN1 Is Nothing Then
                Set m_oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
            End If
            sDen = m_oUnidadesOrgN1.DevolverDenominacion(oPres.UON1)
        Else
            sDen = m_oUnidadesOrgN1.Item(scod1).Den
        End If
    Else
        sDen = gParametrosGenerales.gsDEN_UON0
    End If
    
    If g_iTipoPres < 3 Then
        sDen = sDen & " - " & oPres.Anyo & " - " & oPres.Den & " (" & oPres.Porcen * 100 & "%)"
    Else
        sDen = sDen & " - " & oPres.Den & " (" & oPres.Porcen * 100 & "%)"
    End If
    cP.AddItem sDen, , i, , , , , i
    m_arTag(i) = oPres.indice
    i = i + 1
    ReDim Preserve m_arTag(1 To i)
Next


If i > 1 Then
    cP.ShowPopupMenu cmdOtras.Left, cmdOtras.Top + 850
End If
Set oPresupuestos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "cmdOtras_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub BuscarUONUsuario()
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim sCod As String
Dim oPres As Object
Dim nodx As MSComctlLib.node
Dim iBase As Integer
Dim bBusca As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bBusca = True
    If Not NoHayParametro(basOptimizacion.gUON1Usuario) Then
        scod1 = basOptimizacion.gUON1Usuario & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(basOptimizacion.gUON1Usuario))
        iBase = 1
        If m_oUnidadesOrgN1.Item(scod1) Is Nothing Then bBusca = False
    End If
    If Not NoHayParametro(basOptimizacion.gUON2Usuario) And bBusca Then
        scod2 = scod1 & basOptimizacion.gUON2Usuario & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(basOptimizacion.gUON2Usuario))
        iBase = 2
        If m_oUnidadesOrgN2.Item(scod2) Is Nothing Then bBusca = False
    End If
    If Not NoHayParametro(basOptimizacion.gUON3Usuario) And bBusca Then
        scod3 = scod2 & basOptimizacion.gUON3Usuario & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(basOptimizacion.gUON3Usuario))
        iBase = 3
        If m_oUnidadesOrgN3.Item(scod3) Is Nothing Then bBusca = False
    End If

    If scod3 <> "" And bBusca Then
        If tvwestrorg.Nodes("UON3" & scod3).Image = "UON3A" Then
            tvwestrorg.Nodes("UON3" & scod3).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON3" & scod3)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
    End If
    If scod2 <> "" And bBusca Then
        If g_bRUORama Or (Not g_bRUORama And iBase <= 2) Then
        If tvwestrorg.Nodes("UON2" & scod2).Image = "UON2A" Then
            tvwestrorg.Nodes("UON2" & scod2).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON2" & scod2)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
        End If
    End If
    If scod1 <> "" And bBusca Then
        If g_bRUORama Or (Not g_bRUORama And iBase <= 1) Then
            If tvwestrorg.Nodes("UON1" & scod1).Image = "UON1A" Then
                tvwestrorg.Nodes("UON1" & scod1).Selected = True
                tvwestrorg_NodeClick tvwestrorg.Nodes("UON1" & scod1)
    '            If g_iTipoPres < 3 Then
    '                sdbcAnyo.Value = oPres.Anyo
    '            End If
                SSTabPresupuestos.Tab = 1
                SSTabPresupuestos_Click 0
                Exit Sub
            End If
        End If
    End If
    If tvwestrorg.Nodes("UON0").Image = "UON0A" Then
        tvwestrorg.Nodes("UON0").Selected = True
        tvwestrorg_NodeClick tvwestrorg.Nodes("UON0")
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
        SSTabPresupuestos.Tab = 1
        SSTabPresupuestos_Click 0
        Exit Sub
    End If
    'Si ni su UO ni sus padres est�n asignadas entonces muestra la que est� asinada al 100%
    Select Case g_iTipoPres
    Case 1
        If m_oPresupuestos1Asignados.Count = 1 Then
            Set oPres = m_oPresupuestos1Asignados.Item(1)
        End If
    Case 2
        If m_oPresupuestos2Asignados.Count = 1 Then
            Set oPres = m_oPresupuestos2Asignados.Item(1)
        End If
    Case 3
        If m_oPresupuestos3Asignados.Count = 1 Then
            Set oPres = m_oPresupuestos3Asignados.Item(1)
        End If
    Case 4
        If m_oPresupuestos4Asignados.Count = 1 Then
            Set oPres = m_oPresupuestos4Asignados.Item(1)
        End If
    End Select
    If Not oPres Is Nothing Then
        sCod = EsPresupDelUsuario(oPres)
        If sCod <> "" Then
            Set nodx = tvwestrorg.Nodes.Item(sCod)
            nodx.Selected = True
            tvwestrorg_NodeClick nodx
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Set oPres = Nothing
            Exit Sub
        End If
    End If
    Set oPres = Nothing
    'Si no est� asignado muestro la suya o su padre que tenga presupuestos
    If scod3 <> "" And bBusca Then
        If tvwestrorg.Nodes("UON3" & scod3).Image = "UON3D" Then
            tvwestrorg.Nodes("UON3" & scod3).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON3" & scod3)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
    End If
    If scod2 <> "" And bBusca Then
        If tvwestrorg.Nodes("UON2" & scod2).Image = "UON2D" Then
            tvwestrorg.Nodes("UON2" & scod2).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON2" & scod2)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
    End If
    If scod1 <> "" And bBusca Then
        If tvwestrorg.Nodes("UON1" & scod1).Image = "UON1D" Then
            tvwestrorg.Nodes("UON1" & scod1).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON1" & scod1)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
    End If
    If tvwestrorg.Nodes("UON0").Image = "UON0D" Then
        tvwestrorg.Nodes("UON0").Selected = True
        tvwestrorg_NodeClick tvwestrorg.Nodes("UON0")
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
        SSTabPresupuestos.Tab = 1
        SSTabPresupuestos_Click 0
        Exit Sub
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "BuscarUONUsuario", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Function EsPresupDelUsuario(ByVal oPres As Object) As String
    Dim sCod As String
    Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If NullToStr(oPres.UON3) <> "" Then
        sCod = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        sCod = sCod & oPres.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPres.UON2))
        sCod = sCod & oPres.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPres.UON3))
        If g_bRUO Or g_bRPerfUO Then
            If m_oUnidadesOrgN3.Item(sCod) Is Nothing Then
                EsPresupDelUsuario = ""
                Exit Function
            Else
                Set nodx = tvwestrorg.Nodes.Item("UON3" & sCod)
                If Right(CStr(nodx.Image), 1) <> "A" Then
                    EsPresupDelUsuario = ""
                    Set nodx = Nothing
                    Exit Function
                End If
                Set nodx = Nothing
            End If
        End If
        EsPresupDelUsuario = "UON3" & sCod

    ElseIf NullToStr(oPres.UON2) <> "" Then
        sCod = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        sCod = sCod & oPres.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPres.UON2))
        If g_bRUO Or g_bRPerfUO Then
            If m_oUnidadesOrgN2.Item(sCod) Is Nothing Then
                EsPresupDelUsuario = ""
                Exit Function
            Else
                Set nodx = tvwestrorg.Nodes.Item("UON2" & sCod)
                If Right(CStr(nodx.Image), 1) <> "A" Then
                    EsPresupDelUsuario = ""
                    Set nodx = Nothing
                    Exit Function
                End If
                Set nodx = Nothing
            End If
        End If
        EsPresupDelUsuario = "UON2" & sCod
    ElseIf NullToStr(oPres.UON1) <> "" Then
        sCod = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        If g_bRUO Or g_bRPerfUO Then
            If m_oUnidadesOrgN1.Item(sCod) Is Nothing Then
                EsPresupDelUsuario = ""
                Exit Function
            Else
                Set nodx = tvwestrorg.Nodes.Item("UON1" & sCod)
                If Right(CStr(nodx.Image), 1) <> "A" Then
                    EsPresupDelUsuario = ""
                    Set nodx = Nothing
                    Exit Function
                End If
                Set nodx = Nothing
            End If
        End If
        EsPresupDelUsuario = "UON1" & sCod
    Else
        If g_bRUO And Not g_bRUORama Then
            If NullToStr(basOptimizacion.gUON1Usuario) <> "" Then
                EsPresupDelUsuario = ""
                Exit Function
            End If
        End If
        EsPresupDelUsuario = "UON0"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "EsPresupDelUsuario", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function PorcentajeTotalAsignados() As Double
'''Calcula el porcentaje total asignado de los
'''presupuestos asignados (sin baja l�gica) de tipo g_iTipoPres
    Dim oPres1Niv4 As CPresProyNivel4
    Dim oPres2Niv4 As CPresconNivel4
    Dim oPres3Niv4 As CPresConcep3Nivel4
    Dim oPres4Niv4 As CPresConcep4Nivel4
    Dim dblPorcen As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dblPorcen = 0
    Select Case g_iTipoPres
    
        Case 1
            For Each oPres1Niv4 In m_oPresupuestos1Asignados
                dblPorcen = dblPorcen + oPres1Niv4.Porcen
            Next
        
        Case 2
            For Each oPres2Niv4 In m_oPresupuestos2Asignados
                dblPorcen = dblPorcen + oPres2Niv4.Porcen
            Next
        
        Case 3
            For Each oPres3Niv4 In m_oPresupuestos3Asignados
                dblPorcen = dblPorcen + oPres3Niv4.Porcen
            Next
        
        Case 4
            For Each oPres4Niv4 In m_oPresupuestos4Asignados
                dblPorcen = dblPorcen + oPres4Niv4.Porcen
            Next
    
    End Select

    PorcentajeTotalAsignados = dblPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "PorcentajeTotalAsignados", err, Erl, , m_bActivado)
      Exit Function
   End If
    
End Function

Private Sub CalcularNuevosDatos()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_dblPorcenAsig = PorcentajeTotalAsignados
    g_dblAsignado = CDec(m_dblPorcenAsig * g_dblAbierto)
    m_dblPendiente = g_dblAbierto - g_dblAsignado
    RefrescarTotales
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "CalcularNuevosDatos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub QuitarPresupuestosAsignadosBajaLogica()
'''Si hay alg�n presupuesto asignado que est� de baja l�gica
'''entonces lo eliminamos de la lista de asignados.
Dim oPRES1 As CPresProyNivel4
Dim oPRES2 As CPresconNivel4
Dim oPRES3 As CPresConcep3Nivel4
Dim oPRES4 As CPresConcep4Nivel4
Dim iIndice As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iIndice = 1

    Select Case g_iTipoPres
        Case 1
            For Each oPRES1 In m_oPresupuestos1Asignados
            
                If oPRES1.BajaLog Then
                    m_oPresupuestos1Asignados.Remove (iIndice)
                Else
                    iIndice = iIndice + 1
                End If
            
            Next
        Case 2
            For Each oPRES2 In m_oPresupuestos2Asignados
            
                If oPRES2.BajaLog Then
                    m_oPresupuestos2Asignados.Remove (iIndice)
                Else
                    iIndice = iIndice + 1
                End If
            
            Next
        Case 3
            For Each oPRES3 In m_oPresupuestos3Asignados
            
                If oPRES3.BajaLog Then
                    m_oPresupuestos3Asignados.Remove (iIndice)
                Else
                    iIndice = iIndice + 1
                End If
            
            Next
        Case 4
            For Each oPRES4 In m_oPresupuestos4Asignados
            
                If oPRES4.BajaLog Then
                    m_oPresupuestos4Asignados.Remove (iIndice)
                Else
                    iIndice = iIndice + 1
                End If
            
            Next
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "QuitarPresupuestosAsignadosBajaLogica", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Function ProcesoTieneItems() As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ProcesoTieneItems = False

    If Not frmPROCE.g_oProcesoSeleccionado.Items Is Nothing Then
        If frmPROCE.g_oProcesoSeleccionado.Items.Count > 0 Then
            ProcesoTieneItems = True
            Exit Function
        End If
    End If
    
    If Not frmPROCE.g_oProcesoSeleccionado Is Nothing Then
        If Not frmPROCE.g_oProcesoSeleccionado.Grupos Is Nothing Then
            Dim oGrupo As CGrupo
            For Each oGrupo In frmPROCE.g_oProcesoSeleccionado.Grupos
                If Not oGrupo.Items Is Nothing Then
                    If oGrupo.Items.Count > 0 Then
                        ProcesoTieneItems = True
                        Exit For
                    End If
                End If
            Next
            Set oGrupo = Nothing
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "ProcesoTieneItems", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function GrupoTieneItems() As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    GrupoTieneItems = False

    If Not frmPROCE.g_oGrupoSeleccionado.Items Is Nothing Then
        If frmPROCE.g_oGrupoSeleccionado.Items.Count > 0 Then
            GrupoTieneItems = True
            Exit Function
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPRESAsig", "GrupoTieneItems", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
