VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmNotificaPedido 
   BackColor       =   &H00808000&
   Caption         =   "DNotificaci�n de emisi�n de pedido al proveedor"
   ClientHeight    =   5040
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8430
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmNotificaPedido.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5040
   ScaleWidth      =   8430
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Height          =   315
      Left            =   2925
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   4620
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   4125
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   4620
      Width           =   1005
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   75
      ScaleHeight     =   1515
      ScaleWidth      =   8205
      TabIndex        =   0
      Top             =   120
      Width           =   8265
      Begin VB.CheckBox chkAdjPed 
         BackColor       =   &H00808000&
         Caption         =   "DIncluir adjuntos del pedido"
         ForeColor       =   &H00FFFFFF&
         Height          =   435
         Left            =   75
         TabIndex        =   5
         Top             =   990
         Visible         =   0   'False
         Width           =   2745
      End
      Begin VB.CommandButton cmdMailDot 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7530
         Picture         =   "frmNotificaPedido.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   540
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.TextBox txtMailDot 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2340
         TabIndex        =   3
         Top             =   540
         Width           =   5055
      End
      Begin VB.TextBox txtDOT 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2340
         TabIndex        =   1
         Top             =   120
         Width           =   5055
      End
      Begin VB.CommandButton cmdDOT 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7530
         Picture         =   "frmNotificaPedido.frx":0D71
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.CheckBox chkWord 
         BackColor       =   &H00808000&
         Caption         =   "DAdjuntar pedido en Word"
         ForeColor       =   &H00FFFFFF&
         Height          =   435
         Left            =   2835
         TabIndex        =   6
         Top             =   990
         Visible         =   0   'False
         Width           =   2745
      End
      Begin VB.CheckBox chkExcel 
         BackColor       =   &H00808000&
         Caption         =   "DAdjuntar pedido en Excel"
         ForeColor       =   &H00FFFFFF&
         Height          =   435
         Left            =   5610
         TabIndex        =   7
         Top             =   990
         Visible         =   0   'False
         Width           =   2370
      End
      Begin VB.Label Label2 
         BackColor       =   &H00808000&
         Caption         =   "DPlantilla para e-mail:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   75
         TabIndex        =   12
         Top             =   600
         Width           =   2190
      End
      Begin VB.Label Label7 
         BackColor       =   &H00808000&
         Caption         =   "DPlantilla para carta:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   75
         TabIndex        =   11
         Top             =   180
         Width           =   2190
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPet 
      Height          =   2730
      Left            =   75
      TabIndex        =   8
      Top             =   1800
      Width           =   8265
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   15
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmNotificaPedido.frx":0E30
      stylesets(1).Name=   "Tan"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmNotificaPedido.frx":0E4C
      UseGroups       =   -1  'True
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   2
      Groups(0).Width =   11007
      Groups(0).Caption=   "Contactos"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   2
      Groups(0).Columns(0).Width=   6932
      Groups(0).Columns(0).Caption=   "Apellidos"
      Groups(0).Columns(0).Name=   "APE"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   4075
      Groups(0).Columns(1).Caption=   "Nombre"
      Groups(0).Columns(1).Name=   "NOM"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(1).Width =   2540
      Groups(1).Caption=   "Via de notificaci�n"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns.Count=   13
      Groups(1).Columns(0).Width=   1296
      Groups(1).Columns(0).Caption=   "Mail"
      Groups(1).Columns(0).Name=   "MAIL"
      Groups(1).Columns(0).DataField=   "Column 2"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Style=   2
      Groups(1).Columns(1).Width=   1244
      Groups(1).Columns(1).Caption=   "Carta"
      Groups(1).Columns(1).Name=   "IMP"
      Groups(1).Columns(1).DataField=   "Column 3"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Style=   2
      Groups(1).Columns(2).Width=   661
      Groups(1).Columns(2).Visible=   0   'False
      Groups(1).Columns(2).Caption=   "EMAIL"
      Groups(1).Columns(2).Name=   "EMAIL"
      Groups(1).Columns(2).DataField=   "Column 4"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(3).Width=   1640
      Groups(1).Columns(3).Visible=   0   'False
      Groups(1).Columns(3).Caption=   "CODCON"
      Groups(1).Columns(3).Name=   "CODCON"
      Groups(1).Columns(3).DataField=   "Column 5"
      Groups(1).Columns(3).DataType=   8
      Groups(1).Columns(3).FieldLen=   256
      Groups(1).Columns(4).Width=   820
      Groups(1).Columns(4).Visible=   0   'False
      Groups(1).Columns(4).Caption=   "TFNO"
      Groups(1).Columns(4).Name=   "TFNO"
      Groups(1).Columns(4).DataField=   "Column 6"
      Groups(1).Columns(4).DataType=   8
      Groups(1).Columns(4).FieldLen=   256
      Groups(1).Columns(5).Width=   1138
      Groups(1).Columns(5).Visible=   0   'False
      Groups(1).Columns(5).Caption=   "TFNO2"
      Groups(1).Columns(5).Name=   "TFNO2"
      Groups(1).Columns(5).DataField=   "Column 7"
      Groups(1).Columns(5).DataType=   8
      Groups(1).Columns(5).FieldLen=   256
      Groups(1).Columns(6).Width=   1323
      Groups(1).Columns(6).Visible=   0   'False
      Groups(1).Columns(6).Caption=   "FAX"
      Groups(1).Columns(6).Name=   "FAX"
      Groups(1).Columns(6).DataField=   "Column 8"
      Groups(1).Columns(6).DataType=   8
      Groups(1).Columns(6).FieldLen=   256
      Groups(1).Columns(7).Width=   1402
      Groups(1).Columns(7).Visible=   0   'False
      Groups(1).Columns(7).Caption=   "FSP_COD"
      Groups(1).Columns(7).Name=   "FSP_COD"
      Groups(1).Columns(7).DataField=   "Column 9"
      Groups(1).Columns(7).DataType=   8
      Groups(1).Columns(7).FieldLen=   256
      Groups(1).Columns(8).Width=   1402
      Groups(1).Columns(8).Visible=   0   'False
      Groups(1).Columns(8).Caption=   "PREMIUM"
      Groups(1).Columns(8).Name=   "PREMIUM"
      Groups(1).Columns(8).DataField=   "Column 10"
      Groups(1).Columns(8).DataType=   11
      Groups(1).Columns(8).FieldLen=   256
      Groups(1).Columns(9).Width=   1402
      Groups(1).Columns(9).Visible=   0   'False
      Groups(1).Columns(9).Caption=   "ACTIVO"
      Groups(1).Columns(9).Name=   "ACTIVO"
      Groups(1).Columns(9).DataField=   "Column 11"
      Groups(1).Columns(9).DataType=   11
      Groups(1).Columns(9).FieldLen=   256
      Groups(1).Columns(10).Width=   1402
      Groups(1).Columns(10).Visible=   0   'False
      Groups(1).Columns(10).Caption=   "OFERTADOWEB"
      Groups(1).Columns(10).Name=   "OFERTADOWEB"
      Groups(1).Columns(10).DataField=   "Column 12"
      Groups(1).Columns(10).DataType=   11
      Groups(1).Columns(10).FieldLen=   256
      Groups(1).Columns(11).Width=   1402
      Groups(1).Columns(11).Visible=   0   'False
      Groups(1).Columns(11).Caption=   "TFNO_MOVIL"
      Groups(1).Columns(11).Name=   "TFNO_MOVIL"
      Groups(1).Columns(11).DataField=   "Column 13"
      Groups(1).Columns(11).DataType=   8
      Groups(1).Columns(11).FieldLen=   256
      Groups(1).Columns(12).Width=   1402
      Groups(1).Columns(12).Visible=   0   'False
      Groups(1).Columns(12).Caption=   "PORT"
      Groups(1).Columns(12).Name=   "PORT"
      Groups(1).Columns(12).DataField=   "Column 14"
      Groups(1).Columns(12).DataType=   11
      Groups(1).Columns(12).FieldLen=   256
      _ExtentX        =   14579
      _ExtentY        =   4815
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConApe 
      Height          =   1515
      Left            =   450
      TabIndex        =   13
      Top             =   2820
      Width           =   5715
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmNotificaPedido.frx":0E68
      stylesets(0).AlignmentText=   0
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   4022
      Columns(0).Caption=   "DApellidos"
      Columns(0).Name =   "APE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1852
      Columns(1).Caption=   "DNombre"
      Columns(1).Name =   "NOM"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3757
      Columns(2).Caption=   "DMail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1984
      Columns(4).Caption=   "DTipoPet"
      Columns(4).Name =   "TIPOPET"
      Columns(4).DataField=   "Column 8"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "FAX"
      Columns(5).Name =   "FAX"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "TFNO"
      Columns(6).Name =   "TFNO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TFNO_MOVIL"
      Columns(7).Name =   "TFNO_MOVIL"
      Columns(7).DataField=   "Column 8"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   10081
      _ExtentY        =   2672
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConNom 
      Height          =   1515
      Left            =   1800
      TabIndex        =   14
      Top             =   2880
      Width           =   5775
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "General"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmNotificaPedido.frx":0E84
      stylesets(0).AlignmentText=   0
      StyleSet        =   "General"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   1852
      Columns(0).Caption=   "DNombre"
      Columns(0).Name =   "NOM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4022
      Columns(1).Caption=   "DApellidos"
      Columns(1).Name =   "APE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3942
      Columns(2).Caption=   "DMail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   2037
      Columns(4).Caption=   "DTipoPet"
      Columns(4).Name =   "TIPOPET"
      Columns(4).DataField=   "Column 8"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "FAX"
      Columns(5).Name =   "FAX"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "TFNO"
      Columns(6).Name =   "TFNO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TFNO_MOVIL"
      Columns(7).Name =   "TFNO_MOVIL"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   2672
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmNotificaPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmNotificaPedido
''' *** Creacion: 07/08/2001


Option Explicit
Private sFormatoNumber As String

Private sIdioma(3) As String

Public TipoPedi As TipoPedido
Public TipoComu As Integer
Public CodProve As String
Public DenProve As String
Public Orden As Long
Public pedido As Long
Public Cierre As Boolean
''' Pantalla con las comunicaciones realizadas al proveedor

Public oOrdenEntrega As COrdenEntrega
Private oProve As CProveedor
Private oProves As CProveedores
Private oContactos As CContactos
Private appword  As Object

Private oFos As Scripting.FileSystemObject
'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private sayFileNames() As String
Private Nombres() As String

Private bCargarComboDesdeDD As Boolean
'Textos de la traducci�n
Private sCap(1 To 24) As String
Private Formato As Integer ' = 0 Nada , = 1 Word , = 2 Excel
Private sPedido As String

Public g_bCancelarMail As Boolean

Private m_oEmpresas As CEmpresas

Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sContactosProveERP As String

Sub CargarCarpetaPlantilla(ByRef sCarpeta As String, ByRef sMarcaPlantillas As String, ByRef sRemitenteEmpresa As String)
Dim oEmpresas As CEmpresas
Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , , , , oOrdenEntrega.Empresa
If InStr(gParametrosInstalacion.gsPedidoDot, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
    InStr(gParametrosInstalacion.gsPedidoXLT, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
    InStr(gParametrosInstalacion.gsPedRec, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
    'Buscamos la empresa CARPETA_PLANTILLAS
    
    If oEmpresas.Count > 0 Then sCarpeta = oEmpresas.Item(1).Carpeta_Plantillas
 
End If
sRemitenteEmpresa = oEmpresas.Item(1).Remitente
Set oEmpresas = Nothing
If sCarpeta = "" And gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
    'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
    sMarcaPlantillas = "\" & gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
Else
    sMarcaPlantillas = gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
End If
End Sub

Private Sub chkWord_Click()

    If chkExcel.Value = vbChecked And chkWord.Value = vbChecked Then
        chkExcel.Value = vbUnchecked
    End If
End Sub

Private Sub chkExcel_Click()

    If chkWord.Value = vbChecked And chkExcel.Value = vbChecked Then
       chkWord.Value = vbUnchecked
    End If

End Sub

''' <summary>
''' Lanzar la notificaci�n del pedido directo
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim oNotificacionPedi As CNotificacionPedido
    Dim oNotificacionesPedi As CNotificacionesPedido
    Dim teserror As TipoErrorSummit
    Dim CartaWord As Object
    Dim rangeword As Object
    Dim oPedidoWord As Object
    Dim oPedidoExcel As Object
    Dim appexcel As Object
    Dim bSesionIniciada As Boolean   'Indica si se ha iniciado una sesion de correo
    Dim i As Integer
    Dim splantilla As String
    Dim sPlantillaMail As String
    Dim bAdjuntarImpreso As Boolean
    Dim bWeb As Boolean
    Dim bEMail As Boolean
    Dim bImp As Boolean
    Dim sFileName As String
    Dim sTemp As String
    Dim bActivarWord As Boolean
    Dim iNumPet As Integer
    Dim iNumPets As Integer
    Dim bSalvar As Boolean
    Dim bDisplayAlerts As Boolean
    Dim errormail As TipoErrorSummit
    Dim sCuerpo As String
    Dim bNoExcel  As Boolean
    Dim oPersona As CPersona
    Dim oMonedas As CMonedas
    Dim oPagos As CPagos
    Dim oFSO As Scripting.FileSystemObject
    Dim sMails As String
    Dim sDirTemp As String
    Dim oRPedido As CRPedido
    Dim sTfnoCon As String
    Dim sFAXCon As String
    Dim sMailCon As String
    Dim sMovilCon As String
    Dim sCargoCon As String
    Dim sDptoCon As String
    ' Se utiliza un �nico documento
    On Error GoTo Error:
    
    'Comprobamos que las plantillas  de word son v�lidas
    If Trim(txtDOT) = "" Then
        oMensajes.NoValido sCap(1)
        If Me.Visible Then txtDOT.SetFocus
        Exit Sub
    Else
        If Right(txtDOT, 3) <> "dot" Then
            oMensajes.NoValido sCap(1)
            If Me.Visible Then txtDOT.SetFocus
            Exit Sub
        End If
    End If
    If Not oFos.FileExists(txtDOT) Then
        oMensajes.PlantillaNoEncontrada txtDOT
        txtDOT.SetFocus
        Exit Sub
    End If
    
    
    splantilla = txtDOT
   
  ' Plantillas para notificaciones via mail
    If gParametrosGenerales.giMail <> 0 Then
        If Trim(txtMailDot) = "" Then
            oMensajes.NoValido sCap(1)
            If Me.Visible Then txtMailDot.SetFocus
            Exit Sub
        Else
            If Right(txtMailDot, 3) <> "dot" Then
                oMensajes.NoValido sCap(1)
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
            If Not oFos.FileExists(txtMailDot) Then
                oMensajes.PlantillaNoEncontrada txtMailDot
                If Me.Visible Then txtMailDot.SetFocus
                Exit Sub
            End If
        End If
        sPlantillaMail = txtMailDot
    End If
    
    If chkWord.Value = Unchecked And chkExcel.Value = Unchecked Then
        Formato = 0
    Else
        If chkExcel.Value = Checked Then
            Formato = 2
        Else
            Formato = 1
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    frmESPERA.lblGeneral.caption = sCap(2)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show
    DoEvents
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = sCap(3)
    
    'Creamos las notificaciones de comunicacion
    Set oNotificacionesPedi = Nothing
    Set oNotificacionesPedi = oFSGSRaiz.Generar_CNotificacionesPedido
    sMails = RellenaPedNotif_CCO(sMails, Nothing, oOrdenEntrega)
    'Generamos las notificaciones correspondientes
    sdbgPet.MoveFirst
    iNumPet = 0
    For i = 0 To sdbgPet.Rows - 1
        
        If sdbgPet.Columns(0).Value <> "" And (sdbgPet.Columns(2).Value <> "0" Or sdbgPet.Columns(3).Value <> "0") Then
            bWeb = False
            bEMail = False
            bImp = False
            
            If sdbgPet.Columns(2).Value <> "0" Then
                bEMail = True
            Else
                If sdbgPet.Columns(3).Value <> "0" Then
                    bImp = True
                End If
            End If
            oNotificacionesPedi.Add , oOrdenEntrega.idPedido, oOrdenEntrega.Id, CodProve, DenProve, sdbgPet.Columns(5).Value, sdbgPet.Columns(0).Value, sdbgPet.Columns(1).Value, sdbgPet.Columns(4).Value, TipoComu, Date, bWeb, bEMail, bImp, Formato, iNumPet
            iNumPet = iNumPet + 1
        End If
        sdbgPet.MoveNext
    Next
    
    'Para sacar datos del proveedor en plantillas
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores

    oProves.Add CodProve, DenProve
    oProves.CargarDatosProveedor CodProve
  
    ' Recorremos las notificaciones para enviar un mail u obtener un impreso
    
    iNumPet = 0
    iNumPets = oNotificacionesPedi.Count
    
    For Each oNotificacionPedi In oNotificacionesPedi
        g_bCancelarMail = False
        
        iNumPet = iNumPet + 1
        ReDim Nombres(0)
        frmESPERA.lblContacto = sCap(4) & " " & oNotificacionPedi.nombre & " " & oNotificacionPedi.Apellidos
        frmESPERA.lblContacto.Refresh
        DoEvents
        frmESPERA.ProgressBar2.Value = val((iNumPet / iNumPets) * 100)
        frmESPERA.ProgressBar1.Value = 2
        frmESPERA.lblDetalle = sCap(5)
        frmESPERA.lblDetalle.Refresh
 
        DoEvents
        Set oPersona = Nothing
        Set oPersona = oFSGSRaiz.Generar_CPersona
        oOrdenEntrega.LeerPersonaProceso
        oOrdenEntrega.LeerReceptorProceso
        
        Set oMonedas = oFSGSRaiz.Generar_CMonedas
        oMonedas.CargarTodasLasMonedasDesde 1, gParametrosGenerales.gsMONCEN, , , True, True, True
        Set oPagos = Nothing
        If Not IsNull(oProves.Item(1).FormaPagoCod) Then
            Set oPagos = oFSGSRaiz.generar_CPagos
            oPagos.CargarTodosLosPagosDesde 1, oProves.Item(1).FormaPagoCod, , , True
        End If
                
        
        Dim sCarpeta As String
        Dim sMarcaPlantillas As String
        Dim sRemitenteEmpresa As String
        CargarCarpetaPlantilla sCarpeta, sMarcaPlantillas, sRemitenteEmpresa
        
                
        If oNotificacionPedi.mail Then
            ' Env�o de mensaje al proveedor VIA MAIL
            If Not bSesionIniciada Or oIdsMail Is Nothing Then
                frmESPERA.ProgressBar1.Value = 4
                frmESPERA.lblDetalle = sCap(5)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set oIdsMail = IniciarSesionMail
                bSesionIniciada = True
            End If
                
            If appword Is Nothing Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sCap(6)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set appword = CreateObject("Word.Application")
                bSalvar = appword.Options.SavePropertiesPrompt
                appword.Options.SavePropertiesPrompt = False
            End If
            If Formato = 2 Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sCap(6)
                frmESPERA.lblDetalle.Refresh
                Set appexcel = CreateObject("Excel.Application")
                bDisplayAlerts = appexcel.DisplayAlerts
                appexcel.DisplayAlerts = False
            End If
            frmESPERA.ProgressBar1.Value = 3
            DoEvents
            
            'notificaciones
            frmESPERA.lblDetalle = sCap(7) & " " & sPlantillaMail & " ..."
            frmESPERA.lblDetalle.Refresh
            Set oRPedido = New CRPedido
            Select Case oNotificacionPedi.Tipo
                Case 0, 1
                    Set CartaWord = oRPedido.CartaNotificacionPedidoDirecto(appword, oNotificacionPedi, sPlantillaMail, oOrdenEntrega, oProves.Item(1), m_oEmpresas.Item(1), gParametrosGenerales)
                Case 2, 3
                    If Not oContactos Is Nothing Then
                        If oContactos.Count > 0 Then
                            sTfnoCon = NullToStr(oContactos.Item(sdbddConApe.Columns(3).Value).Tfno)
                            sFAXCon = NullToStr(oContactos.Item(sdbddConApe.Columns(3).Value).Fax)
                            sMailCon = NullToStr(oContactos.Item((sdbddConApe.Columns(3).Value)).mail)
                            sMovilCon = NullToStr(oContactos.Item((sdbddConApe.Columns(3).Value)).tfnomovil)
                            sCargoCon = NullToStr(oContactos.Item((sdbddConApe.Columns(3).Value)).Cargo)
                            sDptoCon = NullToStr(oContactos.Item((sdbddConApe.Columns(3).Value)).Departamento)
                        End If
                    End If
                    Set CartaWord = oRPedido.CartaRecepcionPedido(appword, oNotificacionPedi, sPlantillaMail, oOrdenEntrega, frmRecepcion.sdbcFecRec.Text, frmRecepcion.txtalbaran, frmRecepcion.txtComent, _
                        m_oEmpresas, oProves, sTfnoCon, sFAXCon, sMailCon, sMovilCon, sCargoCon, sDptoCon)
            End Select
            Set oPedidoWord = Nothing
            sCuerpo = CartaWord.Range.Text
            CartaWord.Close False
                            
            ' Primero adjunto el documento con el impreso a rellenar.
            
            frmESPERA.ProgressBar1.Value = 4
            frmESPERA.lblDetalle.Refresh
            If Formato = 1 Then
                Select Case oNotificacionPedi.Tipo
                    Case 0, 1
                        Set oFSO = New Scripting.FileSystemObject
                        If Not oFSO.FileExists(Replace(gParametrosInstalacion.gsPedidoDot, sCarpeta, sMarcaPlantillas)) Then
                            oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedidoDot, sCarpeta, sMarcaPlantillas)
                            Set oPedidoWord = Nothing
                        Else
                            Set oRPedido = New CRPedido
                            Set oPedidoWord = oRPedido.PedidoWord(oNotificacionPedi, appword, oOrdenEntrega, oProves.Item(1), m_oEmpresas.Item(1), "Standard", oFSGSRaiz, oGestorIdiomas, gParametrosGenerales, gParametrosInstalacion, _
                                oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63), oUsuarioSummit.Perfil)
                            Set oRPedido = Nothing
                        End If
                        Set oFSO = Nothing
                    Case 2, 3
                        Set oPedidoWord = DetalleRecepcion(appword)
                End Select
            Else
                If Formato = 2 Then
                    bNoExcel = False
                    If gParametrosInstalacion.gsPedidoXLT = "" Then
                        If gParametrosGenerales.gsPedidoXLT = "" Then
                            oMensajes.NoValido sCap(1)
                            bNoExcel = True
                        Else
                            gParametrosInstalacion.gsPedidoXLT = gParametrosGenerales.gsPedidoXLT
                            g_GuardarParametrosIns = True
                        End If
                    End If
                    
                    If Right(gParametrosInstalacion.gsPedidoXLT, 3) <> "xlt" Then
                        oMensajes.NoValido sCap(1) & ":" & Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)
                        bNoExcel = True
                    End If
                        
                    If Not oFos.FileExists(Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)) Then
                        oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)
                        bNoExcel = True
                    End If
                    If Not bNoExcel Then
                        Set oRPedido = New CRPedido
                        Set oPedidoExcel = oRPedido.PedidoExcel(appexcel, oOrdenEntrega, m_oEmpresas.Item(1), oProves.Item(1), "#,##0.00", oFSGSRaiz, gParametrosGenerales, gParametrosInstalacion, _
                                                                oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63), g_oParametrosSM)
                        Set oRPedido = Nothing
                    End If
                End If
            End If
            DoEvents
            If Not oPedidoWord Is Nothing Then
                sTemp = DevolverPathFichTemp
                oPedidoWord.SaveAs sTemp & sPedido & ".doc"
                oPedidoWord.Close
                sayFileNames(UBound(sayFileNames)) = sTemp & sPedido & ".doc"
                ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                Nombres(UBound(Nombres)) = sPedido & ".doc"
                ReDim Preserve Nombres(UBound(Nombres) + 1)
            End If
            If Not oPedidoExcel Is Nothing Then
                sTemp = DevolverPathFichTemp
                oPedidoExcel.SaveAs sTemp & sPedido & ".xls", oPedidoExcel.fileformat
                sTemp = sTemp & sPedido & ".xls"
                oPedidoExcel.Close
                sayFileNames(UBound(sayFileNames)) = sTemp
                ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                Nombres(UBound(Nombres)) = sPedido & ".xls"
                ReDim Preserve Nombres(UBound(Nombres) + 1)
                'Cuando la notificacion es via mail, pueden abrir el adjunto excel y cerrarlo
                'el objeto appexcel sigue manteniendo una referencia al EXCEL correcta,
                ' todo el codigo se ejecuta OK, pero al abrir un segundo adjunto, se queda colgada
                ' el EXCEL. Asi que lo soluciono, creando una nueva referencia a EXCEL cada vez
                appexcel.Quit
                Set appexcel = Nothing
            End If
             
            If chkAdjPed.Value = Checked Then
                'Adjuntar adjuntos de la orden
                Dim oAdjun As CAdjunto
                If oOrdenEntrega.Adjuntos Is Nothing Then oOrdenEntrega.CargarAdjuntos
                If Not oOrdenEntrega.Adjuntos Is Nothing Then
                        
                    sDirTemp = FSGSLibrary.DevolverPathFichTemp
                    Set oFSO = New Scripting.FileSystemObject
                    
                    For Each oAdjun In oOrdenEntrega.Adjuntos
                        Set oAdjun.Orden = oOrdenEntrega
                        If oAdjun.EnLOG Then
                            oAdjun.Tipo = TipoAdjunto.Log_Orden_Entrega
                        Else
                            oAdjun.Tipo = TipoAdjunto.Orden_Entrega
                        End If
                        sFileName = sDirTemp & oAdjun.nombre
                        
                        'Comprueba si existe el fichero y si existe se borra:
                        If oFSO.FileExists(sFileName) Then
                            oFSO.DeleteFile sFileName, True
                        End If
                        
                        oAdjun.LeerAdjunto sFileName
                                
                        sayFileNames(UBound(sayFileNames)) = sFileName
                        ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)
                        Nombres(UBound(Nombres)) = oAdjun.nombre
                        ReDim Preserve Nombres(UBound(Nombres) + 1)
                    Next
                    
                    Set oAdjun = Nothing
                    Set oFSO = Nothing
                End If
            End If
            
            Select Case oNotificacionPedi.Tipo
                Case 0 'Emision
                    errormail = ComponerMensaje(oNotificacionPedi.Email, gParametrosInstalacion.gsPedEmiMailSubject, sCuerpo, Nombres, "frmNotificaPedido", _
                                                lIdInstancia:=oOrdenEntrega.Id, _
                                                entidadNotificacion:=PedDirecto, _
                                                tipoNotificacion:=NotifPedidoReceptor, _
                                                sToName:=oNotificacionPedi.nombre & " " & oNotificacionPedi.Apellidos, _
                                                sCC:=gParametrosInstalacion.gsRecipientCC, sCCO:=sMails, sRemitenteEmpresa:=sRemitenteEmpresa)
                Case 1 'Anulaci�n
                    errormail = ComponerMensaje(oNotificacionPedi.Email, gParametrosInstalacion.gsPedAnuMailSubject, sCuerpo, Nombres, "frmNotificaPedido", _
                                                lIdInstancia:=oOrdenEntrega.Id, _
                                                entidadNotificacion:=PedDirecto, _
                                                tipoNotificacion:=TipoNotificacionEmail.PedAnulacionMail, _
                                                sToName:=oNotificacionPedi.nombre & " " & oNotificacionPedi.Apellidos, _
                                                sCC:=gParametrosInstalacion.gsRecipientCC, sCCO:=sMails, sRemitenteEmpresa:=sRemitenteEmpresa)
                Case 2, 3 'Recepcion
                    Dim Tipo As TipoNotificacionEmail
                    If oNotificacionPedi.Tipo = 2 Then
                        Tipo = PedRecepOk
                    Else
                        Tipo = PedRecepKo
                    End If
                    errormail = ComponerMensaje(oNotificacionPedi.Email, gParametrosInstalacion.gsPedRecMailSubject, sCuerpo, Nombres, "frmNotificaPedido", _
                                                lIdInstancia:=oOrdenEntrega.Id, _
                                                entidadNotificacion:=PedDirecto, _
                                                tipoNotificacion:=Tipo, _
                                                sToName:=oNotificacionPedi.nombre & " " & oNotificacionPedi.Apellidos, _
                                                sCC:=gParametrosInstalacion.gsRecipientCC, sCCO:=sMails, sRemitenteEmpresa:=sRemitenteEmpresa)
            End Select
            If errormail.NumError <> TESnoerror Then
                TratarError errormail
                oNotificacionPedi.Fallido = True
            ElseIf g_bCancelarMail = True Then
                'Si se ha cancelado el env�o no se podr� almacenar en BD
            Else
                frmESPERA.ProgressBar1.Value = 8
                frmESPERA.lblDetalle = sCap(8)
                frmESPERA.lblDetalle.Refresh
            End If
            DoEvents

        Else
                
            'Generamos un documento de notificaci�n impresa para cada contacto
            
            ' *************** PETICIONES VIA IMPRESO **********************
            bActivarWord = True
            If appword Is Nothing Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sCap(6)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                Set appword = CreateObject("Word.Application")
                bSalvar = appword.Options.SavePropertiesPrompt
                appword.Options.SavePropertiesPrompt = False
            End If
    
    
            frmESPERA.ProgressBar1.Value = 3
            
            frmESPERA.lblDetalle = sCap(7) & " " & splantilla & " ..."
            frmESPERA.lblDetalle.Refresh
            Set oRPedido = New CRPedido
            Select Case oNotificacionPedi.Tipo
                Case 0, 1
                    Set CartaWord = oRPedido.CartaNotificacionPedidoDirecto(appword, oNotificacionPedi, splantilla, oOrdenEntrega, oProves.Item(1), m_oEmpresas.Item(1), gParametrosGenerales)
                Case 2, 3
                    If Not oContactos Is Nothing Then
                        If oContactos.Count > 0 Then
                            sTfnoCon = NullToStr(oContactos.Item(sdbddConApe.Columns(3).Value).Tfno)
                            sFAXCon = NullToStr(oContactos.Item(sdbddConApe.Columns(3).Value).Fax)
                            sMailCon = NullToStr(oContactos.Item((sdbddConApe.Columns(3).Value)).mail)
                            sMovilCon = NullToStr(oContactos.Item((sdbddConApe.Columns(3).Value)).tfnomovil)
                            sCargoCon = NullToStr(oContactos.Item((sdbddConApe.Columns(3).Value)).Cargo)
                            sDptoCon = NullToStr(oContactos.Item((sdbddConApe.Columns(3).Value)).Departamento)
                        End If
                    End If
                    Set CartaWord = oRPedido.CartaRecepcionPedido(appword, oNotificacionPedi, splantilla, oOrdenEntrega, frmRecepcion.sdbcFecRec.Text, frmRecepcion.txtalbaran, frmRecepcion.txtComent, _
                        m_oEmpresas, oProves, sTfnoCon, sFAXCon, sMailCon, sMovilCon, sCargoCon, sDptoCon)
            End Select
            Set oRPedido = Nothing
            
            bAdjuntarImpreso = True
        End If
        
        'Almacenamos las notificaciones en la BD
        If Not g_bCancelarMail = True Then
            oNotificacionPedi.IDRegistroMail = g_lIDRegistroEmail
            teserror = oOrdenEntrega.RealizarComunicacion(oNotificacionPedi)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Unload frmESPERA
                Screen.MousePointer = vbNormal
                Exit Sub
            Else
                oOrdenEntrega.Notificado = Notificado
                oOrdenEntrega.ConComunicaciones = True
            End If
        End If
    Next
         
    ' Miramos si se ha realizado alguna petici�n impresa y si se ha solicitado el pedido en formato Word o Excel
    If bAdjuntarImpreso Then
           
        frmESPERA.ProgressBar1.Value = 6
        'frmESPERA.lblDetalle = sIdioma(16)
        frmESPERA.lblDetalle.Refresh
        DoEvents
        If Formato = 1 Then
            Select Case TipoComu
                Case 0, 1
                    Set oFSO = New Scripting.FileSystemObject
                    If Not oFSO.FileExists(Replace(gParametrosInstalacion.gsPedidoDot, sCarpeta, sMarcaPlantillas)) Then
                        oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedidoDot, sCarpeta, sMarcaPlantillas)
                        Set oPedidoWord = Nothing
                    Else
                        Set oRPedido = New CRPedido
                        Set oPedidoWord = oRPedido.PedidoWord(oNotificacionPedi, appword, oOrdenEntrega, oProves.Item(1), m_oEmpresas.Item(1), "Standard", oFSGSRaiz, oGestorIdiomas, gParametrosGenerales, gParametrosInstalacion, _
                            oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63), oUsuarioSummit.Perfil)
                        Set oRPedido = Nothing
                    End If
                    Set oFSO = Nothing
                Case 2, 3
                    Set oPedidoWord = DetalleRecepcion(appword)
            End Select
        Else
            If Formato = 2 Then
                frmESPERA.ProgressBar1.Value = 2
                frmESPERA.lblDetalle = sCap(6)
                frmESPERA.lblDetalle.Refresh
                DoEvents
                If appexcel Is Nothing Then
                    DoEvents
                    Set appexcel = CreateObject("Excel.Application")
                    bDisplayAlerts = appexcel.DisplayAlerts
                    appexcel.DisplayAlerts = False
                End If
                bNoExcel = False
                If gParametrosInstalacion.gsPedidoXLT = "" Then
                    If gParametrosGenerales.gsPedidoXLT = "" Then
                        oMensajes.NoValido sCap(1)
                        bNoExcel = True
                    Else
                        gParametrosInstalacion.gsPedidoXLT = gParametrosGenerales.gsPedidoXLT
                        g_GuardarParametrosIns = True
                    End If
                End If
                
                If Right(gParametrosInstalacion.gsPedidoXLT, 3) <> "xlt" Then
                    oMensajes.NoValido sCap(1) & ":" & Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)
                    bNoExcel = True
                End If
                    
                If Not oFos.FileExists(Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)) Then
                    oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedidoXLT, sMarcaPlantillas, sCarpeta)
                    bNoExcel = True
                End If
                If Not bNoExcel Then
                    Set oRPedido = New CRPedido
                    Set oPedidoExcel = oRPedido.PedidoExcel(appexcel, oOrdenEntrega, m_oEmpresas.Item(1), oProves.Item(1), "#,##0.00", oFSGSRaiz, gParametrosGenerales, gParametrosInstalacion, _
                                                            oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 64), oMensajes.CargarTexto(FRM_PEDIDOS_DIRECTOS, 63), g_oParametrosSM)
                    Set oRPedido = Nothing
                End If
            End If
        End If

        If Not oPedidoWord Is Nothing Then
            ' Tengo que pegar este �ltimo documento en las peticiones anteriores
            oPedidoWord.Range.Select
            oPedidoWord.Range.Copy
            DoEvents
            oPedidoWord.Close False
                        
            For Each CartaWord In appword.Documents
                Set rangeword = CartaWord.Range
                rangeword.Collapse Direction:=0 'wdCollapseEnd
                rangeword.InsertBreak Type:=7 'wdPageBreak
                rangeword.Paste
                Set rangeword = Nothing
            Next
        End If
        
        If Formato = 2 Then
            appexcel.Visible = True
        End If
        
        appword.Options.SavePropertiesPrompt = bSalvar
        appexcel.DisplayAlerts = bDisplayAlerts
        If bAdjuntarImpreso Then
            frmESPERA.ProgressBar1.Value = 7
            frmESPERA.lblDetalle = sIdioma(4) & splantilla & " ..."
            frmESPERA.lblDetalle.Refresh
            DoEvents
            appword.Visible = True
            
        End If
        
    End If
    
    appword.Options.SavePropertiesPrompt = bSalvar
    If bActivarWord Then
        frmESPERA.ProgressBar1.Value = 7
        frmESPERA.lblDetalle = sCap(11)
        frmESPERA.lblDetalle.Refresh
        DoEvents
        appword.Visible = True
    End If
        
    If bSesionIniciada Then
        frmESPERA.ProgressBar1.Value = 5
        frmESPERA.lblDetalle = sCap(12)
        FinalizarSesionMail
    End If
    
    If Not g_bCancelarMail = True Then
        basSeguridad.RegistrarAccion AccionesSummit.ACCSeguimientoPedComunicar, "Orden entrega:" & oOrdenEntrega.Anyo & "/" & oOrdenEntrega.NumPedido & "/" & oOrdenEntrega.Numero & " ID: " & oOrdenEntrega.Id & ", pedido: " & oOrdenEntrega.idPedido & ", proveedor: " & oOrdenEntrega.ProveDen & ",TipoComunicacion: " & oOrdenEntrega.Tipo
    End If
   
           
    Unload frmESPERA
    Set oProve = Nothing
    If appword.Visible = False Then
        ' cuando se realizan solo peticiones via web, queda una tarea
        ' de word abierta que no se destruye con appword = nothing
        appword.Quit
    End If
    Set appword = Nothing
    Set CartaWord = Nothing
    Set oPedidoWord = Nothing
    Set oNotificacionPedi = Nothing
    Set oNotificacionesPedi = Nothing
    Set oProves = Nothing
    
    If TipoComu = 3 Or TipoComu = 2 Then
        If Not Cierre Then
            frmRecepcion.Show
        Else
            Unload frmRecepcion
        End If
    Else
        frmSeguimComunic.cmdRestaurar_Click
    End If
    
    Screen.MousePointer = vbNormal
    
    Unload Me
       
    Exit Sub
                    
Error:

    
    Select Case err.Number
        
        Case 5151
            MsgBox err.Description, vbCritical, "Fullstep"
        Case Else
                Resume Next
    End Select
    
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub
Private Sub cmdDot_Click()
On Error GoTo ErrPlantilla

    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sCap(13) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDOT = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub
Private Sub cmdMailDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sCap(13) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtMailDot = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub

Private Sub Form_Load()

    
     On Error GoTo Error:
    
    Me.Height = 5520
    Me.Width = 8520
    
    sFormatoNumber = "#,##0.########"
    
    ''''''''''''''''''''''solo para version del 1-10-2001'''''''''''''''
    chkWord.Visible = False
    chkExcel.Visible = False
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Set oFos = New Scripting.FileSystemObject
    
    Set oProve = oFSGSRaiz.generar_CProveedor
    
    Set oContactos = oFSGSRaiz.Generar_CContactos
        
    If gParametrosGenerales.giMail = 0 Then
        
        txtMailDot.Enabled = False
        cmdMailDot.Enabled = False
        
    End If
    
    
    sdbgPet.Columns(0).DropDownHwnd = sdbddConApe.hWnd
    sdbgPet.Columns(1).DropDownHwnd = sdbddConNom.hWnd
    
    sdbddConApe.AddItem ""
    sdbddConNom.AddItem ""
    
    'Inicializar el array de ficheros
    ReDim sayFileNames(0)
    ReDim Nombres(0)
    
    
    Select Case TipoComu
    Case 0 ' PLANTILLAS PARA emision
        If gParametrosInstalacion.gsPedidoDirEmisionCarta = "" Then
            If gParametrosGenerales.gsPEDDEMICARTADOT = "" Then
                oMensajes.NoValido sCap(15)
            Else
                gParametrosInstalacion.gsPedidoDirEmisionCarta = gParametrosGenerales.gsPEDDEMICARTADOT
                g_GuardarParametrosIns = True
            End If
        End If
        If gParametrosInstalacion.gsPedidoDirEmisionMail = "" Then
            If gParametrosGenerales.gsPEDDEMIMAILDOT = "" Then
                oMensajes.NoValido sCap(17)
            Else
                gParametrosInstalacion.gsPedidoDirEmisionMail = gParametrosGenerales.gsPEDDEMIMAILDOT
                g_GuardarParametrosIns = True
            End If
        End If
        
    Case 1 ' PLANTILLAS PARA ANULACION
        If gParametrosInstalacion.gsPedidoDirAnulacionCarta = "" Then
            If gParametrosGenerales.gsPEDDANUCARTADOT = "" Then
                oMensajes.NoValido sCap(14)
            Else
                gParametrosInstalacion.gsPedidoDirAnulacionCarta = gParametrosGenerales.gsPEDDANUCARTADOT
                g_GuardarParametrosIns = True
            End If
        End If
        If gParametrosGenerales.giMail <> 0 Then
            If gParametrosInstalacion.gsPedidoDirAnulacionMail = "" Then
                If gParametrosGenerales.gsPEDDANUMAILDOT = "" Then
                    oMensajes.NoValido sCap(16)
                Else
                    gParametrosInstalacion.gsPedidoDirAnulacionMail = gParametrosGenerales.gsPEDDANUMAILDOT
                    g_GuardarParametrosIns = True
                End If
            End If
        End If
    Case 2 'PLANTILLAS RECEPCION CORRECTA
        If gParametrosInstalacion.gsPedidoDirRecepCartaOK = "" Then
            If gParametrosGenerales.gsPEDDRECOKCARTADOT = "" Then
                oMensajes.NoValido sCap(22)
            Else
                gParametrosInstalacion.gsPedidoDirRecepCartaOK = gParametrosGenerales.gsPEDDRECOKCARTADOT
                g_GuardarParametrosIns = True
            End If
        End If
        If gParametrosGenerales.giMail <> 0 Then
            If gParametrosInstalacion.gsPedidoDirRecepMailOK = "" Then
                If gParametrosGenerales.gsPEDDRECOKMAILDOT = "" Then
                    oMensajes.NoValido sCap(23)
                Else
                    gParametrosInstalacion.gsPedidoDirRecepMailOK = gParametrosGenerales.gsPEDDRECOKMAILDOT
                    g_GuardarParametrosIns = True
                End If
            End If
        End If
    Case 3 'PLANTILLAS RECEPCION ERRONEA
        If gParametrosInstalacion.gsPedidoDirRecepCartaKO = "" Then
            If gParametrosGenerales.gsPEDDRECKOCARTADOT = "" Then
                oMensajes.NoValido sCap(22)
            Else
                gParametrosInstalacion.gsPedidoDirRecepCartaKO = gParametrosGenerales.gsPEDDRECKOCARTADOT
                g_GuardarParametrosIns = True
            End If
        End If
        If gParametrosGenerales.giMail <> 0 Then
            If gParametrosInstalacion.gsPedidoDirRecepMailKO = "" Then
                If gParametrosGenerales.gsPEDDRECOKMAILDOT = "" Then
                    oMensajes.NoValido sCap(23)
                Else
                    gParametrosInstalacion.gsPedidoDirRecepMailKO = gParametrosGenerales.gsPEDDRECKOMAILDOT
                    g_GuardarParametrosIns = True
                End If
            End If
        End If
    
    End Select
        
    
    ' PLANTILLAS PARA MAIL
    
    Dim sCarpeta As String
    Dim sMarcaPlantillas As String
    Set m_oEmpresas = oFSGSRaiz.Generar_CEmpresas
    m_oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , , , , oOrdenEntrega.Empresa
    If InStr(gParametrosInstalacion.gsPedidoDirEmisionCarta, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
         InStr(gParametrosInstalacion.gsPedidoDirEmisionMail, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
         InStr(gParametrosInstalacion.gsPedidoDirAnulacionCarta, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
         InStr(gParametrosInstalacion.gsPedidoDirAnulacionMail, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
         InStr(gParametrosInstalacion.gsPedidoDirRecepCartaOK, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
         InStr(gParametrosInstalacion.gsPedidoDirRecepMailOK, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
         InStr(gParametrosInstalacion.gsPedidoDirRecepCartaKO, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Or _
         InStr(gParametrosInstalacion.gsPedidoDirRecepMailKO, gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS) > 0 Then
         sCarpeta = m_oEmpresas.Item(1).Carpeta_Plantillas
    End If
    If sCarpeta = "" And gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS <> "" Then
        'Hay que incluir la barra para que te la quite en caso de que encuentre el marcador
        sMarcaPlantillas = "\" & gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    Else
        sMarcaPlantillas = gParametrosGenerales.gsMARCADOR_CARPETA_PLANTILLAS
    End If
    Select Case TipoComu
    Case 0 ' Emisi�n
        frmNotificaPedido.caption = sIdioma(1) & ": " & oOrdenEntrega.ProveCod & ", " & oOrdenEntrega.ProveDen
        chkWord.Visible = True
        chkExcel.Visible = True
        chkAdjPed.Visible = True
        chkWord.Value = Unchecked
        chkExcel.Value = Unchecked
        chkAdjPed.Value = Unchecked
        
        txtDOT = Replace(gParametrosInstalacion.gsPedidoDirEmisionCarta, sMarcaPlantillas, sCarpeta)
        txtMailDot = Replace(gParametrosInstalacion.gsPedidoDirEmisionMail, sMarcaPlantillas, sCarpeta)

    Case 1 ' Anulaci�n
        frmNotificaPedido.caption = sIdioma(0) & ": " & oOrdenEntrega.ProveCod & ", " & oOrdenEntrega.ProveDen
        chkWord.Visible = True
        chkExcel.Visible = True
        chkAdjPed.Visible = False
        chkWord.Value = Unchecked
        chkExcel.Value = Unchecked
        chkExcel.Left = chkWord.Left
        chkWord.Left = chkAdjPed.Left
        
        txtDOT = Replace(gParametrosInstalacion.gsPedidoDirAnulacionCarta, sMarcaPlantillas, sCarpeta)
        txtMailDot = Replace(gParametrosInstalacion.gsPedidoDirAnulacionMail, sMarcaPlantillas, sCarpeta)
        
    Case 2 ' Recepci�n Correcta
        frmNotificaPedido.caption = sIdioma(2) & ": " & oOrdenEntrega.ProveCod & ", " & oOrdenEntrega.ProveDen
        chkWord.Visible = True
        chkWord.Value = vbUnchecked
        chkExcel.Visible = False
        chkAdjPed.Visible = False
        chkWord.Left = chkAdjPed.Left
        
        txtDOT = Replace(gParametrosInstalacion.gsPedidoDirRecepCartaOK, sMarcaPlantillas, sCarpeta)
        txtMailDot = Replace(gParametrosInstalacion.gsPedidoDirRecepMailOK, sMarcaPlantillas, sCarpeta)
        
    Case 3 ' Recepci�n Err�nea
        frmNotificaPedido.caption = sIdioma(3) & ": " & oOrdenEntrega.ProveCod & ", " & oOrdenEntrega.ProveDen
        chkWord.Visible = True
        chkWord.Value = vbChecked
        chkExcel.Visible = False
        chkAdjPed.Visible = False
        chkWord.Left = chkAdjPed.Left
        
        txtDOT = Replace(gParametrosInstalacion.gsPedidoDirRecepCartaKO, sMarcaPlantillas, sCarpeta)
        txtMailDot = Replace(gParametrosInstalacion.gsPedidoDirRecepMailKO, sMarcaPlantillas, sCarpeta)
    End Select
        
        
    If TipoComu = 0 Or TipoComu = 1 Then
    ' para emision o anulaci�n de pedido, en funcion del formato del pedido definido a nivel de proveedor
    ' se cargan las check box por defecto
        Select Case oOrdenEntrega.Formato
        Case "1"
            chkExcel.Value = Unchecked
            chkWord.Value = Checked
        Case "2"
            chkWord.Value = Unchecked
            chkExcel.Value = Checked
        End Select
    End If
        
    
       
    Dim bHabilitarGrid As Boolean
    bHabilitarGrid = True
    'Si hay contacto en PROVE_ERP se utiliza este y se deshabilita el grid
    bHabilitarGrid = Not CargarProveERP
    With sdbgPet
        If bHabilitarGrid Then
            .Backcolor = &H8000000F
            .BackColorEven = &HFFFFFF
            .BackColorOdd = &HFFFFFF
            .Enabled = True
        Else
            .Backcolor = &HC6C3C6
            .BackColorEven = &HC6C3C6
            .BackColorOdd = &HC6C3C6
            .Enabled = False
        End If
    End With
    
    Exit Sub
Error:
    MsgBox err.Description
End Sub

Private Function CargarProveERP() As Boolean
    Dim oProvesERP As CProveERPs
    
    CargarProveERP = False
    
    If NullToStr(oOrdenEntrega.ProveERP) <> "" Then
        Set oProvesERP = oFSGSRaiz.Generar_CProveERPs
        oProvesERP.CargarProveedoresERP oOrdenEntrega.Empresa, oOrdenEntrega.ProveCod, oOrdenEntrega.ProveERP
        If oProvesERP.Count > 0 Then
            If NullToStr(oProvesERP.Item(1).Contacto) <> "" Then
                'A�adir al grid
                sdbgPet.Columns("APE").Value = m_sContactosProveERP
                sdbgPet.Columns("EMAIL").Value = oProvesERP.Item(1).Contacto
                    
                ' contacto con e-mail
                sdbgPet.Columns("MAIL").Value = 1
                sdbgPet.Columns("IMP").Value = 0
                
                CargarProveERP = True
            End If
        End If
    End If
    
    Set oProvesERP = Nothing
End Function

Private Sub Form_Resize()
   Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim i As Integer

On Error GoTo Error:
    
    'Borramos los archivos temporales que hayamos creado
    g_bCancelarMail = False
    i = 0
    While i < UBound(sayFileNames)
        
        If oFos.FileExists(sayFileNames(i)) Then
            oFos.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    
    Set oFos = Nothing
    Set oContactos = Nothing
    Set m_oEmpresas = Nothing
    Me.Visible = False
    Exit Sub

    
Error:
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next

End Sub




Private Sub sdbddConApe_CloseUp()
    
    If Not IsNull(sdbddConApe.Columns(0).Value) Then
        
        sdbgPet.Columns(0).Value = sdbddConApe.Columns(0).Value
        sdbgPet.Columns(1).Value = sdbddConApe.Columns(1).Value
        sdbgPet.Columns(4).Value = sdbddConApe.Columns(2).Value
        sdbgPet.Columns(5).Value = sdbddConApe.Columns(3).Value
        sdbgPet.Columns("TFNO").Value = sdbddConApe.Columns("TFNO").Value
        sdbgPet.Columns("FAX").Value = sdbddConApe.Columns("FAX").Value
        sdbgPet.Columns("TFNO_MOVIL").Value = sdbddConApe.Columns("TFNO_MOVIL").Value

        If sdbddConApe.Columns(2).Value <> "" And gParametrosGenerales.giMail <> 0 Then
        ' contacto con e-mail
            sdbgPet.Columns(2).Value = 1
            sdbgPet.Columns(3).Value = 0
        Else
            sdbgPet.Columns(2).Value = 0
            sdbgPet.Columns(3).Value = 1
        End If
        
    End If
    
End Sub

Private Sub sdbddConApe_DropDown()
    Screen.MousePointer = vbHourglass
    
    oProve.Den = DenProve
    oProve.Cod = CodProve
    
    If bCargarComboDesdeDD Then
        Set oContactos = oProve.CargarTodosLosContactos(DenProve, , , , True)
    Else
        Set oContactos = oProve.CargarTodosLosContactos(, , , , True)
    End If
    
    CargarGridConContactosApe
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddConApe_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddConApe.DataFieldList = "Column 0"
    sdbddConApe.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_NOTIFICAPEDIDO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sCap(1) = Ador(0).Value      '1 Plantilla
        Ador.MoveNext
        sCap(2) = Ador(0).Value      '2
        Ador.MoveNext
        sCap(3) = Ador(0).Value
        Ador.MoveNext
        sCap(4) = Ador(0).Value
        Ador.MoveNext
        sCap(5) = Ador(0).Value
        Ador.MoveNext
        sCap(6) = Ador(0).Value
        Ador.MoveNext
        sCap(7) = Ador(0).Value
        Ador.MoveNext
        sCap(8) = Ador(0).Value
        Ador.MoveNext
        sCap(9) = Ador(0).Value
        Ador.MoveNext
        sCap(10) = Ador(0).Value
        Ador.MoveNext
        sCap(11) = Ador(0).Value
        Ador.MoveNext
        sCap(12) = Ador(0).Value
        Ador.MoveNext
        sCap(13) = Ador(0).Value
        Ador.MoveNext
        sCap(14) = Ador(0).Value
        Ador.MoveNext
        sCap(15) = Ador(0).Value
        Ador.MoveNext
        sCap(16) = Ador(0).Value
        Ador.MoveNext
        sCap(17) = Ador(0).Value
        Ador.MoveNext
        sCap(18) = Ador(0).Value
        Ador.MoveNext
        sCap(19) = Ador(0).Value
        '---------------------------------------
        Ador.MoveNext
        sIdioma(0) = Ador(0).Value
        Ador.MoveNext
        sIdioma(1) = Ador(0).Value
        Ador.MoveNext
        chkWord.caption = Ador(0).Value
        Ador.MoveNext
        chkExcel.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups.Item(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups.Item(1).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbddConApe.Columns(0).caption = Ador(0).Value
        sdbddConNom.Columns(1).caption = Ador(0).Value
        sdbgPet.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbddConApe.Columns(1).caption = Ador(0).Value
        sdbddConNom.Columns(0).caption = Ador(0).Value
        sdbgPet.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sCap(20) = Ador(0).Value
        Ador.MoveNext
        sCap(21) = Ador(0).Value
        Ador.MoveNext
        frmNotificaPedido.Label7.caption = Ador(0).Value
        Ador.MoveNext
        frmNotificaPedido.Label2.caption = Ador(0).Value
        Ador.MoveNext
        frmNotificaPedido.cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        frmNotificaPedido.cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sdbddConApe.Columns(2).caption = Ador(0).Value
        sdbddConNom.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sIdioma(2) = Ador(0).Value
        Ador.MoveNext
        sIdioma(3) = Ador(0).Value
        Ador.MoveNext
        sPedido = Ador(0).Value
        Ador.MoveNext
        sCap(22) = Ador(0).Value
        Ador.MoveNext
        sCap(23) = Ador(0).Value
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        Ador.MoveNext
        chkAdjPed.caption = Ador(0).Value
        Ador.MoveNext
        m_sContactosProveERP = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub
Private Sub CargarGridConContactosApe()

    ''' * Objetivo: Cargar combo con la coleccion de contactos
    
    Dim oCon As CContacto
    
    sdbddConApe.RemoveAll
    If TipoPedi = TipoPedido.Aprovisionamiento Then
        sdbddConApe.Columns(4).caption = sCap(20)
        For Each oCon In oContactos
            sdbddConApe.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Aprovisionador & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.tfnomovil
        Next
    Else
        sdbddConApe.Columns(4).caption = sCap(21)
        For Each oCon In oContactos
            sdbddConApe.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.tfnomovil
        Next
    End If
    
    If sdbddConApe.Rows = 0 Then
        sdbddConApe.AddItem ""
    End If
    
    sdbddConApe.MoveFirst
    
End Sub


Private Sub sdbddConNom_CloseUp()

    If Not IsNull(sdbddConNom.Columns(1).Value) Then
        sdbgPet.Columns(1).Value = sdbddConNom.Columns(0).Value
        sdbgPet.Columns(0).Value = sdbddConNom.Columns(1).Value
        sdbgPet.Columns(4).Value = sdbddConNom.Columns(2).Value
        sdbgPet.Columns(5).Value = sdbddConNom.Columns(3).Value
        sdbgPet.Columns("TFNO").Value = sdbddConNom.Columns("TFNO").Value
'        sdbgPet.Columns("TFNO2").Value = sdbddConNom.Columns("TFNO2").Value
        sdbgPet.Columns("FAX").Value = sdbddConNom.Columns("FAX").Value
        sdbgPet.Columns("TFNO_MOVIL").Value = sdbddConApe.Columns("TFNO_MOVIL").Value
        
        If sdbddConNom.Columns(2).Value <> "" And gParametrosGenerales.giMail <> 0 Then
        ' contacto con e-mail
            sdbgPet.Columns(2).Value = 1
            sdbgPet.Columns(3).Value = 0
        Else
            sdbgPet.Columns(2).Value = 0
            sdbgPet.Columns(3).Value = 1
        End If
        
    End If
    
End Sub

Private Sub sdbddConNom_DropDown()
    Screen.MousePointer = vbHourglass
    
    oProve.Cod = CodProve
    
    If bCargarComboDesdeDD Then
'        Set oContactos = oProve.CargarTodosLosContactos(, sdbgPet.ActiveCell.Value, , True)
        Set oContactos = oProve.CargarTodosLosContactos(, CodProve, , True)
    Else
        Set oContactos = oProve.CargarTodosLosContactos(, , , True)
    End If
    
    CargarGridConContactosNom
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddConNom_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddConNom.DataFieldList = "Column 0"
    sdbddConNom.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub CargarGridConContactosNom()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oCon As CContacto
    
    sdbddConNom.RemoveAll
    
    If TipoPedi = TipoPedido.Aprovisionamiento Then
        sdbddConNom.Columns(4).caption = sCap(20)
        For Each oCon In oContactos
            sdbddConNom.AddItem oCon.nombre & Chr(m_lSeparador) & oCon.Apellidos & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Aprovisionador & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.tfnomovil
        Next
    Else
        sdbddConNom.Columns(4).caption = sCap(21)
        For Each oCon In oContactos
            sdbddConNom.AddItem oCon.nombre & Chr(m_lSeparador) & oCon.Apellidos & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.tfnomovil
        Next
    End If
    
    If sdbddConNom.Rows = 0 Then
        sdbddConNom.AddItem ""
    End If
    
    sdbddConNom.MoveFirst
    
End Sub
Private Sub sdbgPet_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgPet_Change()
    'c
    Select Case sdbgPet.col
    
  
        
    Case 2
        
        If sdbgPet.Columns(2).Value = "1" Or sdbgPet.Columns(2).Value = "-1" Then
            If sdbgPet.Columns("EMAIL").Value = "" Then
                oMensajes.NoValido sCap(18)
                sdbgPet.DataChanged = False
                Exit Sub
            Else
                sdbgPet.Columns(3).Value = 0
            End If
        End If
    
        
    Case 3
        
        If sdbgPet.Columns(3).Value = "1" Or sdbgPet.Columns(3).Value = "-1" Then
            sdbgPet.Columns(2).Value = 0
        End If
        
        
    End Select
    
    
End Sub

Private Sub sdbgPet_InitColumnProps()

    sdbgPet.Columns(0).Locked = True
    sdbgPet.Columns(1).Locked = True
    
   
    If gParametrosGenerales.giMail = 0 Then
        sdbgPet.Columns("MAIL").Locked = True
    End If
    
End Sub
Private Sub Arrange()
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    On Error Resume Next
    
    If Height >= 2550 Then sdbgPet.Height = Height * 0.385
    If Width >= 360 Then sdbgPet.Width = Width - 360
    sdbgPet.Top = Picture1.Height + 265
    sdbgPet.Height = Height - (Picture1.Height + 265) - 1000
    sdbgPet.Groups(0).Width = sdbgPet.Width * 70 / 100
    sdbgPet.Groups(1).Width = sdbgPet.Width * 25 / 100

    cmdAceptar.Left = Me.Width / 2 - 1400
    cmdAceptar.Top = sdbgPet.Height + Picture1.Height + 400
    cmdCancelar.Left = cmdAceptar.Left + 1200
    cmdCancelar.Top = cmdAceptar.Top
End Sub

Private Function DetalleRecepcion(appword As Object) As Object
    Dim docword As Object
    Dim rangeword As Object
    Dim inumitem As Integer
    Dim oPersona As CPersona
    Dim i As Integer
    Dim INUMDEST As Integer
    Dim ayDest() As String
    Dim oDestinos As CDestinos
    Dim oLinea As CLineaPedido
    Dim lIdPerfil As Long
    Dim ADORs As ADODB.Recordset
    Dim sCarpeta As String
    Dim sRemitenteEmpresa As String
    Dim sMarcaPlantillas As String
    On Error GoTo Error:
       
    If gParametrosInstalacion.gsPedRec = "" Then
        If gParametrosGenerales.gsPEDRECDOT = "" Then
            oMensajes.NoValido sCap(1)
            Set DetalleRecepcion = Nothing
            Exit Function
        Else
            gParametrosInstalacion.gsPedRec = gParametrosGenerales.gsPEDRECDOT
            g_GuardarParametrosIns = True
        End If
    End If
    
    CargarCarpetaPlantilla sCarpeta, sMarcaPlantillas, sRemitenteEmpresa
    
    If Right(gParametrosInstalacion.gsPedRec, 3) <> "dot" Then
        oMensajes.NoValido sCap(1) & ": " & Replace(gParametrosInstalacion.gsPedRec, sMarcaPlantillas, sCarpeta)
        Set DetalleRecepcion = Nothing
        Exit Function
    End If
        
    If Not oFos.FileExists(Replace(gParametrosInstalacion.gsPedRec, sMarcaPlantillas, sCarpeta)) Then
        oMensajes.PlantillaNoEncontrada Replace(gParametrosInstalacion.gsPedRec, sMarcaPlantillas, sCarpeta)
        Set DetalleRecepcion = Nothing
        Exit Function
    End If
     
    Set oPersona = Nothing
    Set oPersona = oFSGSRaiz.Generar_CPersona
    oOrdenEntrega.LeerPersonaProceso
    
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    oDestinos.CargarTodosLosDestinos , , , , , , , , , , True, True
    
    Set docword = appword.Documents.Add(Replace(gParametrosInstalacion.gsPedRec, sMarcaPlantillas, sCarpeta))
    
    With docword
        If .Bookmarks.Exists("FECHA") = True Then
            DatoAWord docword, "FECHA", Format(frmRecepcion.txtFecRec.Text, "Short Date")
        End If
        If .Bookmarks.Exists("ANYO") = True Then
            DatoAWord docword, "ANYO", oOrdenEntrega.Anyo
        End If
        If .Bookmarks.Exists("PEDIDO") = True Then
            DatoAWord docword, "PEDIDO", oOrdenEntrega.NumPedido
        End If
        If .Bookmarks.Exists("ORDEN") = True Then
            DatoAWord docword, "ORDEN", oOrdenEntrega.Numero
        End If
        If .Bookmarks.Exists("ALBARAN") = True Then
            DatoAWord docword, "ALBARAN", frmRecepcion.txtalbaran
        End If
        If .Bookmarks.Exists("CIF") = True Then
            DatoAWord docword, "CIF", m_oEmpresas.Item(1).NIF
        End If
        If .Bookmarks.Exists("RAZON_SOCIAL") = True Then
            DatoAWord docword, "RAZON_SOCIAL", m_oEmpresas.Item(1).Den
        End If
        If frmRecepcion.txtComent = "" Then
            .Bookmarks("OBSERVACIONES").Range.Delete
        Else
            If .Bookmarks.Exists("COMENTARIO") = True Then
                DatoAWord docword, "COMENTARIO", frmRecepcion.txtComent
            End If
        End If
        ' DATOS PROVEEDORES
        If .Bookmarks.Exists("COD_PROVE") Then
            DatoAWord docword, "COD_PROVE", oProves.Item(1).Cod
        End If
        If .Bookmarks.Exists("DEN_PROVE") Then
            DatoAWord docword, "DEN_PROVE", oProves.Item(1).Den
        End If
        If .Bookmarks.Exists("DIR_PROVE") Then
            DatoAWord docword, "DIR_PROVE", oProves.Item(1).Direccion
        End If
        If .Bookmarks.Exists("POBL_PROVE") Then
            DatoAWord docword, "POBL_PROVE", oProves.Item(1).Poblacion
        End If
        If .Bookmarks.Exists("CP_PROVE") Then
            DatoAWord docword, "CP_PROVE", oProves.Item(1).cP
        End If
        If .Bookmarks.Exists("PROV_PROVE") Then
            DatoAWord docword, "PROV_PROVE", oProves.Item(1).DenProvi
        End If
        If .Bookmarks.Exists("PAIS_PROVE") Then
            DatoAWord docword, "PAIS_PROVE", oProves.Item(1).DenPais
        End If
        If .Bookmarks.Exists("NIF_PROVE") Then
            DatoAWord docword, "NIF_PROVE", NullToStr(oProves.Item(1).NIF)
        End If
        
        'DATOS DE LA PERSONA QUE REALIZO EL PEDIDO
        If oOrdenEntrega.Persona Is Nothing Then
            .Bookmarks("PERSONA").Range.Delete
        Else
            If .Bookmarks.Exists("NOM_PERSONA") = True Then
                DatoAWord docword, "NOM_PERSONA", oOrdenEntrega.Persona.nombre
            End If
            If .Bookmarks.Exists("APE_PERSONA") = True Then
                DatoAWord docword, "APE_PERSONA", oOrdenEntrega.Persona.Apellidos
            End If
            If .Bookmarks.Exists("FAX_PERSONA") = True Then
                DatoAWord docword, "FAX_PERSONA", oOrdenEntrega.Persona.Fax
            End If
            If .Bookmarks.Exists("TFNO_PERSONA") = True Then
                DatoAWord docword, "TFNO_PERSONA", oOrdenEntrega.Persona.Tfno
            End If
            If .Bookmarks.Exists("MAIL_PERSONA") = True Then
                DatoAWord docword, "MAIL_PERSONA", oOrdenEntrega.Persona.mail
            End If
        End If
        If .Bookmarks.Exists("COD_MON") = True Then
            DatoAWord docword, "COD_MON", gParametrosGenerales.gsMONCEN
        End If
        
    INUMDEST = 0
    ReDim ayDest(0)

    ' Generamos la tabla de procesos
        inumitem = 1
        If .Bookmarks.Exists("ITEM") = True Then
            Set rangeword = .Bookmarks("ITEM").Range
            rangeword.Copy
            .Bookmarks("ITEM").Select
            .Application.Selection.Delete
            frmRecepcion.sdbgPrecios.MoveFirst
            'Rellenamos la tabla para cada linea de recepcion
            For i = 0 To frmRecepcion.sdbgPrecios.Rows - 1
                
                If frmRecepcion.sdbgPrecios.Columns(8).Value <> "" Then
                    .Application.Selection.Paste
                    If .Bookmarks.Exists("NUM_ART") = True Then
                        .Bookmarks("NUM_ART").Range.Text = inumitem
                    End If
                    If .Bookmarks.Exists("ARTICULO") = True Then
                        .Bookmarks("ARTICULO").Range.Text = frmRecepcion.sdbgPrecios.Columns(1).Value
                    End If
                    If .Bookmarks.Exists("COD_DEST") = True Then
                        .Bookmarks("COD_DEST").Range.Text = frmRecepcion.sdbgPrecios.Columns(2).Value
                    End If
                    If .Bookmarks.Exists("COD_UNI") = True Then
                        .Bookmarks("COD_UNI").Range.Text = frmRecepcion.sdbgPrecios.Columns(3).Value
                    End If
                    If .Bookmarks.Exists("PRECIO") = True Then
                        .Bookmarks("PRECIO").Range.Text = Format(frmRecepcion.sdbgPrecios.Columns(4).Value, sFormatoNumber)
                    End If
                    If .Bookmarks.Exists("FEC_ENTREGA") = True Then
                        .Bookmarks("FEC_ENTREGA").Range.Text = Format(frmRecepcion.sdbgPrecios.Columns(5).Value, "Short Date")
                    End If
                    If .Bookmarks.Exists("CANT_PEDIDA") = True Then
                        .Bookmarks("CANT_PEDIDA").Range.Text = Format(frmRecepcion.sdbgPrecios.Columns(6).Value, sFormatoNumber)
                    End If
                    If .Bookmarks.Exists("CANT_RECIBIDA") = True Then
                        .Bookmarks("CANT_RECIBIDA").Range.Text = Format(frmRecepcion.sdbgPrecios.Columns(8).Value, sFormatoNumber)
                    End If
                    If .Bookmarks.Exists("DESTINO") Then
                        If Not BuscarEnArray(ayDest, frmRecepcion.sdbgPrecios.Columns(2).Value) Then
                            ayDest(INUMDEST) = oDestinos.Item(frmRecepcion.sdbgPrecios.Columns("DEST").Value).Cod
                            If oDestinos.Item(frmRecepcion.sdbgPrecios.Columns("DEST").Value).Generico = 1 Then
                                oOrdenEntrega.CargarLineasDePedido True
                                Set oLinea = oOrdenEntrega.LineasPedido.Item(frmRecepcion.sdbgPrecios.Columns(0).Value)
                                oDestinos.Item(ayDest(INUMDEST)).dir = NullToStr(oLinea.Dest_Dir)
                                oDestinos.Item(ayDest(INUMDEST)).POB = NullToStr(oLinea.Dest_Pob)
                                oDestinos.Item(ayDest(INUMDEST)).cP = NullToStr(oLinea.Dest_CP)
                                If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
                                Set ADORs = oDestinos.DevolverTodosLosDestinos(CStr(NullToStr(oLinea.CodDestino)), , True, , , , , , , , , lIdPerfil, True)
                                oDestinos.Item(ayDest(INUMDEST)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den = NullToStr(oLinea.Dest_Den)
                                oDestinos.Item(ayDest(INUMDEST)).Provi = NullToStr(oLinea.Dest_Provi)
                                oDestinos.Item(ayDest(INUMDEST)).Pais = NullToStr(oLinea.Dest_Pai)
                            End If
                            INUMDEST = INUMDEST + 1
                            ReDim Preserve ayDest(UBound(ayDest) + 1)
                        End If
                    End If

                    inumitem = inumitem + 1
                End If
                frmRecepcion.sdbgPrecios.MoveNext
            Next
                 
        End If
        
        If .Bookmarks.Exists("DESTINO") Then
            Set rangeword = .Bookmarks("DESTINO").Range
            rangeword.Copy
            .Bookmarks("DESTINO").Select
            .Application.Selection.Delete
            For INUMDEST = 0 To UBound(ayDest) - 1
                .Application.Selection.Paste
                .Bookmarks("DEST_COD").Range.Text = ayDest(INUMDEST)
                .Bookmarks("DEST_DIR").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).dir)
                .Bookmarks("DEST_POB").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).POB)
                .Bookmarks("DEST_CP").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).cP)
                .Bookmarks("DEST_DEN").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                .Bookmarks("DEST_PROV").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).Provi)
                .Bookmarks("DEST_PAIS").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).Pais)
                
            Next
         End If

    End With
    Set DetalleRecepcion = docword
    Set docword = Nothing
    Set oPersona = Nothing
    Set oDestinos = Nothing
    Exit Function
    
Error:

    If err.Number = 462 Then ' the remote server machine... " han cerrado el word"
           Set appword = Nothing
           Set appword = CreateObject("Word.Application")
           appword.Options.SavePropertiesPrompt = False
           Set docword = appword.Documents.Add(Replace(gParametrosInstalacion.gsPedRec, sMarcaPlantillas, sCarpeta))
    End If

    Resume Next
        
End Function

