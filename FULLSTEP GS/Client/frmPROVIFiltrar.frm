VERSION 5.00
Begin VB.Form frmPROVIFiltrar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar provincia"
   ClientHeight    =   2550
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5400
   Icon            =   "frmPROVIFiltrar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2550
   ScaleWidth      =   5400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   120
      TabIndex        =   6
      Top             =   60
      Width           =   5235
      Begin VB.OptionButton optCOD 
         BackColor       =   &H00808000&
         Caption         =   "Por c�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   0
         Value           =   -1  'True
         Width           =   2310
      End
      Begin VB.TextBox txtCOD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   0
         Top             =   360
         Width           =   795
      End
      Begin VB.CheckBox chkIgualCod 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   1140
         TabIndex        =   1
         Top             =   420
         Width           =   2310
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Height          =   915
      Left            =   120
      TabIndex        =   8
      Top             =   1080
      Width           =   5235
      Begin VB.OptionButton optDEN 
         BackColor       =   &H00808000&
         Caption         =   "Por denominaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Top             =   -60
         Width           =   2310
      End
      Begin VB.TextBox txtDEN 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   100
         TabIndex        =   2
         Top             =   360
         Width           =   2670
      End
      Begin VB.CheckBox chkIgualDen 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3030
         TabIndex        =   3
         Top             =   420
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2790
      TabIndex        =   5
      Top             =   2130
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1590
      TabIndex        =   4
      Top             =   2130
      Width           =   1005
   End
End
Attribute VB_Name = "frmPROVIFiltrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmPROVIFiltrar
''' *** Creacion: 28/12/1998 (Javier Arana)
''' *** Ultima revision: 14/01/1999 (Alfredo Magallon)

Option Explicit
Private Sub Form_Load()

    ''' * Objetivo: Situar el formulario respecto al
    ''' * Objetivo: principal de Provincias
    
    On Error Resume Next
    
    Me.Left = frmPROVI.Left + 500
    Me.Top = frmPROVI.Top + 1000
    CargarRecursos
    
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPROVI
    
    
End Sub
Private Sub optCOD_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    If optCOD.Value = True Then
        optDEN.Value = False
    End If
        
    txtCod.SetFocus
   
End Sub
Private Sub chkIgualDen_Click()
    
    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    txtDen.SetFocus

End Sub
Private Sub chkIgualCod_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    txtCod.SetFocus
    
End Sub
Private Sub optDEN_Click()

    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    If optDEN.Value = True Then
        optCOD.Value = False
    End If
    
    txtDen.SetFocus
    
End Sub

Private Sub cmdAceptar_Click()

    ''' * Objetivo: Aplicar el filtro y descargar
    ''' * Objetivo: el formulario
    
    Screen.MousePointer = vbHourglass
    
    Set frmPROVI.oPaisSeleccionado.Provincias = Nothing
    Set frmPROVI.oPaisSeleccionado.Provincias = oFSGSRaiz.generar_CProvincias
   
        
    frmPROVI.ponerCaption "", 3, False
    
    If optCOD.Value = True And txtCod <> "" Then
        
        frmPROVI.sCriterioCodListado = txtCod
        frmPROVI.sCriterioDenListado = ""
        frmPROVI.bCriterioCoincidenciaTotal = chkIgualCod.Value
        
        If Not chkIgualCod.Value = vbChecked Then
        
            frmPROVI.oPaisSeleccionado.CargarTodasLasProvincias Trim(txtCod), , , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Provi)
            frmPROVI.ponerCaption txtCod, 1, True
            
        Else
        
            frmPROVI.oPaisSeleccionado.CargarTodasLasProvincias Trim(txtCod), , True, , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Provi)
            frmPROVI.ponerCaption txtCod, 1, False
            
        End If
        
    Else
    
        If optDEN.Value = True And txtDen <> "" Then
        
            frmPROVI.sCriterioCodListado = ""
            frmPROVI.sCriterioDenListado = txtDen
            frmPROVI.bCriterioCoincidenciaTotal = chkIgualDen.Value
            
            If Not chkIgualDen.Value = vbChecked Then
                
                frmPROVI.oPaisSeleccionado.CargarTodasLasProvincias , txtDen, , , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Provi)
                frmPROVI.ponerCaption txtDen, 2, True
                
            Else
                
                frmPROVI.oPaisSeleccionado.CargarTodasLasProvincias , txtDen, True, , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Provi)
                frmPROVI.ponerCaption txtDen, 2, False
                
            End If
        
        Else
        
            frmPROVI.sCriterioCodListado = ""
            frmPROVI.sCriterioDenListado = ""
            frmPROVI.bCriterioCoincidenciaTotal = False
            
            frmPROVI.oPaisSeleccionado.CargarTodasLasProvincias , , True, , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Provi)
            
        End If
        
    End If
        
        
    frmPROVI.sdbgProvincias.ReBind
    
    MDI.MostrarFormulario frmPROVI, True
    
    frmPROVI.sdbgProvincias.MoveFirst
    
    Screen.MousePointer = vbNormal
    
    Unload Me

    frmPROVI.sdbgProvincias.SetFocus
    
End Sub
Private Sub cmdCancelar_Click()

    ''' * Objetivo: Cerrar el formulario
    
    MDI.MostrarFormulario frmPROVI, True
    
    Unload Me
    
    frmPROVI.SetFocus
    
    
    
End Sub
Private Sub Form_Activate()

    ''' * Objetivo: Iniciar el formulario
    
    txtCod.SetFocus
    
End Sub
Private Sub txtCOD_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por codigo

    optCOD.Value = True
    optDEN.Value = False
    
End Sub
Private Sub txtDEN_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por denominacion

    optCOD.Value = False
    optDEN.Value = True
    
End Sub




Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVIFILTRAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    Caption = Ador(0).Value
    Ador.MoveNext
    optCOD.Caption = Ador(0).Value
    Ador.MoveNext
    chkIgualCod.Caption = Ador(0).Value
    chkIgualDen.Caption = Ador(0).Value
    Ador.MoveNext
    optDEN.Caption = Ador(0).Value
    Ador.MoveNext
    cmdAceptar.Caption = Ador(0).Value
    Ador.MoveNext
    cmdCancelar.Caption = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub


