VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmOFEAvisoAnya 
   BackColor       =   &H00808000&
   Caption         =   "Realizar peticiones de ofertas"
   ClientHeight    =   4995
   ClientLeft      =   1215
   ClientTop       =   3750
   ClientWidth     =   9270
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOFEAvisoAnya.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4995
   ScaleWidth      =   9270
   Begin SSDataWidgets_B.SSDBGrid sdbgPet 
      Height          =   3900
      Left            =   60
      TabIndex        =   0
      Top             =   600
      Width           =   9180
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   7
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmOFEAvisoAnya.frx":0CB2
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmOFEAvisoAnya.frx":0CCE
      UseGroups       =   -1  'True
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   3
      Groups(0).Width =   6641
      Groups(0).Caption=   "Proveedores"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   2
      Groups(0).Columns(0).Width=   3281
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "CODPROVE"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   3360
      Groups(0).Columns(1).Caption=   "Denominaci�n"
      Groups(0).Columns(1).Name=   "DENPROVE"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(1).Width =   5689
      Groups(1).Caption=   "Contactos"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns(0).Width=   5689
      Groups(1).Columns(0).Caption=   "Apellidos"
      Groups(1).Columns(0).Name=   "APE"
      Groups(1).Columns(0).DataField=   "Column 2"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(2).Width =   5583
      Groups(2).Visible=   0   'False
      Groups(2).Caption=   "Ocultas"
      Groups(2).Columns.Count=   4
      Groups(2).Columns(0).Width=   1058
      Groups(2).Columns(0).Caption=   "CODCON"
      Groups(2).Columns(0).Name=   "CODCON"
      Groups(2).Columns(0).DataField=   "Column 3"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(1).Width=   1191
      Groups(2).Columns(1).Caption=   "OFERTADOWEB"
      Groups(2).Columns(1).Name=   "EMAIL"
      Groups(2).Columns(1).DataField=   "Column 4"
      Groups(2).Columns(1).DataType=   8
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(2).Width=   -873
      Groups(2).Columns(2).Caption=   "EQP"
      Groups(2).Columns(2).Name=   "EQP"
      Groups(2).Columns(2).DataField=   "Column 5"
      Groups(2).Columns(2).DataType=   8
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(3).Width=   -1376
      Groups(2).Columns(3).Caption=   "COM"
      Groups(2).Columns(3).Name=   "COM"
      Groups(2).Columns(3).DataField=   "Column 6"
      Groups(2).Columns(3).DataType=   8
      Groups(2).Columns(3).FieldLen=   256
      _ExtentX        =   16192
      _ExtentY        =   6879
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   9270
      TabIndex        =   4
      Top             =   4500
      Width           =   9270
      Begin VB.CommandButton cmdAceptar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   3300
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "D&Cancelar"
         Height          =   315
         Left            =   4530
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveDen 
      Height          =   1575
      Left            =   1620
      TabIndex        =   6
      Top             =   3000
      Width           =   5055
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmOFEAvisoAnya.frx":0CEA
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmOFEAvisoAnya.frx":0D06
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   5477
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2990
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "EQP"
      Columns(2).Name =   "EQP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "COM"
      Columns(3).Name =   "COM"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   8916
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddConApe 
      Height          =   1515
      Left            =   1140
      TabIndex        =   3
      Top             =   3060
      Width           =   5715
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmOFEAvisoAnya.frx":0D22
      stylesets(1).Name=   "Tan"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmOFEAvisoAnya.frx":0D3E
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   4022
      Columns(0).Caption=   "Apellidos"
      Columns(0).Name =   "APE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3889
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "NOM"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3757
      Columns(2).Caption=   "Mail"
      Columns(2).Name =   "EMAIL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "PORT"
      Columns(4).Name =   "PORT"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   10081
      _ExtentY        =   2672
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveCod 
      Height          =   1635
      Left            =   900
      TabIndex        =   5
      Top             =   3060
      Width           =   5175
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmOFEAvisoAnya.frx":0D5A
      stylesets(1).Name=   "ProvPortal"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmOFEAvisoAnya.frx":0D76
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   2725
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6006
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "EQP"
      Columns(2).Name =   "EQP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "COM"
      Columns(3).Name =   "COM"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   9128
      _ExtentY        =   2884
      _StockProps     =   77
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   8700
      Top             =   3360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label lblTZHoraFin 
      BackColor       =   &H00808000&
      Caption         =   "Label11"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6000
      TabIndex        =   11
      Top             =   135
      Width           =   1215
   End
   Begin VB.Label lblHoraL 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808000&
      Caption         =   "DHora"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4100
      TabIndex        =   10
      Top             =   135
      Width           =   750
   End
   Begin VB.Label lblHoraLimite 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   4925
      TabIndex        =   9
      Top             =   105
      Width           =   885
   End
   Begin VB.Label lblFecLimit 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   2600
      TabIndex        =   8
      Top             =   105
      Width           =   1275
   End
   Begin VB.Label lblFechaLimite 
      BackColor       =   &H00808000&
      Caption         =   "Fecha l�mite de ofertas:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   135
      Width           =   2250
   End
End
Attribute VB_Name = "frmOFEAvisoAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oFos As Scripting.FileSystemObject
Private oProves As CAsignaciones

Private oProve As CProveedor
Private oContactos As CContactos
Private oproce As CProceso
'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private sayFileNames() As String
Private Nombres() As String
Public iNumProves As String 'Contendr� el n�mero de proveedores seleccionados, para el FUP

Public bModificarFecLimit As Boolean  'Variable para saber si se puede modificar o no la fecha l�mite de ofertas
Public bModSubasta As Boolean 'Variable para saber si se puede modificar o no la fecha l�mite en subastas
Public g_bCancelarMail As Boolean
Public Anyo As Integer
Public GMN1 As String
Public Cod As Long
Public vTZHora As Variant

Private moComps As fsgsserver.CCompradores

Private oProvesAsignados As fsgsserver.CAsignaciones
Private m_oOfertasProceso As COfertas


Private Sub cmdAceptar_Click()
EnviarEmails
End Sub

Private Sub cmdCancelar_Click()
    
    Unload Me
    
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_OFEAVISOANYA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.lblFechaLimite.caption = Ador(0).Value
        Ador.MoveNext
       
        Me.caption = Ador(0).Value
        Ador.MoveNext
        
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        
        sdbddConApe.Columns("APE").caption = Ador(0).Value

        Ador.MoveNext
        sdbddConApe.Columns("NOM").caption = Ador(0).Value
        Ador.MoveNext
        sdbddConApe.Columns("EMAIL").caption = Ador(0).Value
        Ador.MoveNext
        sdbddProveCod.Columns("COD").caption = Ador(0).Value '34 Codigo
        sdbddProveDen.Columns("COD").caption = Ador(0).Value
        sdbgPet.Columns("CODPROVE").caption = Ador(0).Value
        Ador.MoveNext
        sdbddProveCod.Columns("DEN").caption = Ador(0).Value '35 Denominaci�n
        sdbddProveDen.Columns("DEN").caption = Ador(0).Value
        sdbgPet.Columns("DENPROVE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Groups(0).caption = Ador(0).Value '
        Ador.MoveNext
        sdbgPet.Groups(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPet.Columns("APE").caption = Ador(0).Value
        Ador.MoveNext
        lblHoraL.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Activate()
    Dim dcListaZonasHorarias As Dictionary
    Dim oProceso As CProceso
    Dim dtFechaFinSub As Date
    Dim dtHoraFinSub As Date
    Dim tzInfo As TIME_ZONE_INFO
    
    'Es posible que la zona horaria haya cambiado en otra pantalla, actualizarla
    If oUsuarioSummit.TimeZone <> vTZHora Then
        vTZHora = oUsuarioSummit.TimeZone
        Set dcListaZonasHorarias = ObtenerZonasHorarias
        Set oProceso = oFSGSRaiz.Generar_CProceso
        oProceso.Anyo = Anyo
        oProceso.GMN1Cod = GMN1
        oProceso.Cod = Cod
        oProceso.CargarDatosGeneralesProceso bSinpres:=True, bSinGRupos:=True, sUsu:=oUsuarioSummit.Cod
            
        If Not oProceso Is Nothing Then
            tzInfo = GetTimeZoneByKey(vTZHora)
            lblTZHoraFin = Mid(tzInfo.DisplayName, 2, 9)
            
            'Fecha l�mite ofertas
            If IsNull(oProceso.FechaMinimoLimOfertas) Then
                lblFecLimit = vbNullString
                lblHoraLimite = vbNullString
            Else
                ConvertirUTCaTZ DateValue(oProceso.FechaMinimoLimOfertas), TimeValue(oProceso.FechaMinimoLimOfertas), vTZHora, dtFechaFinSub, dtHoraFinSub
                    
                lblFecLimit = Format(dtFechaFinSub, "short date")
                lblHoraLimite = Format(dtHoraFinSub, "short time")
            End If
        End If
        
        Set oProceso = Nothing
        Set dcListaZonasHorarias = Nothing
    End If
End Sub

Private Sub Form_Load()
    
    
    On Error GoTo Error:
    
    Me.Width = 9450
    Me.Height = 5640
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos

    PonerFieldSeparator Me

    'Inicializar el array de ficheros
    ReDim sayFileNames(0)
    ReDim Nombres(0)

    Set oFos = New Scripting.FileSystemObject

    Set oProve = oFSGSRaiz.generar_CProveedor
    Set oproce = frmOFEPet.oProcesoSeleccionado


    Set oProvesAsignados = oFSGSRaiz.Generar_CAsignaciones
    
    If frmOFEPet.bComunAsigComp Then
        Set oProvesAsignados = oproce.CargarProveedoresAAvisar(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
    Else
        If frmOFEPet.bComunAsigEqp Then
            Set oProvesAsignados = oproce.CargarProveedoresAAvisar(basOptimizacion.gCodEqpUsuario)
        Else
            Set oProvesAsignados = oproce.CargarProveedoresAAvisar()
        End If
    End If
    
    sdbgPet.Columns("CODPROVE").DropDownHwnd = sdbddProveCod.hWnd
    sdbgPet.Columns("DENPROVE").DropDownHwnd = sdbddProveDen.hWnd
    sdbgPet.Columns("APE").DropDownHwnd = sdbddConApe.hWnd

    sdbddConApe.AddItem ""
    sdbddProveCod.AddItem ""
    sdbddProveDen.AddItem ""



    Exit Sub

Error:
    
    MsgBox err.Description
    
    
End Sub
Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    On Error Resume Next
    
        sdbgPet.Width = Me.Width - 200
        sdbgPet.Height = Me.Height - 1900
        
        sdbgPet.Groups(0).Width = (sdbgPet.Width - 570) * 0.55
        sdbgPet.Groups(1).Width = (sdbgPet.Width - 570) * 0.45
    
    cmdAceptar.Left = Me.Width / 2 - 1400
    cmdCancelar.Left = cmdAceptar.Left + 1200

End Sub


Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim i As Integer


On Error GoTo Error:
    
    'Borramos los archivos temporales que hayamos creado
    
    g_bCancelarMail = False
    Set m_oOfertasProceso = Nothing
    Set oProvesAsignados = Nothing
    
    i = 0
    While i < UBound(sayFileNames)
        
        If oFos.FileExists(sayFileNames(i)) Then
            oFos.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    
    Set oFos = Nothing
    Me.Visible = False
    Exit Sub

Error:
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
End Sub




Private Sub sdbddConApe_CloseUp()

    If sdbgPet.Columns("CODCON").Value = sdbddConApe.Columns("ID").Value Then
        'Se ha seleccionado el mismo contacto que ya teniamos
        Exit Sub
    End If
    
    
    If sdbddConApe.Columns("APE").Value <> "" Then
        
        Screen.MousePointer = vbHourglass
        
        If sdbddConApe.Columns("NOM").Value <> "" Then
            sdbgPet.Columns("APE").Value = sdbddConApe.Columns("APE").Value & ", " & sdbddConApe.Columns("NOM").Value
            sdbgPet.Columns("APE").Text = sdbddConApe.Columns("APE").Value & ", " & sdbddConApe.Columns("NOM").Value
        Else
            sdbgPet.Columns("APE").Value = sdbddConApe.Columns("APE").Value
            sdbgPet.Columns("APE").Text = sdbddConApe.Columns("APE").Value
        End If
        sdbgPet.Columns("CODCON").Value = sdbddConApe.Columns("ID").Value
        sdbgPet.Columns("email").Value = sdbddConApe.Columns("email").Value

        Screen.MousePointer = vbNormal

    End If
    
End Sub

Private Sub sdbddConApe_DropDown()
Dim scodProve As String
Dim oProves As CProveedores
Dim oProve As CProveedor

    sdbddConApe.RemoveAll
    
    If sdbgPet.Columns("CODPROVE").Value = "" Then
        sdbddConApe.RemoveAll
        sdbddConApe.AddItem ""
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    
    scodProve = sdbgPet.Columns("CODPROVE").Value
    
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    Set oProve = oProves.Add(scodProve, "")
    
    
    Set oContactos = Nothing
    Set oContactos = oProve.CargarTodosLosContactos(, , , , True, , , , , , , , , True)
    
    Screen.MousePointer = vbNormal
        
    CargarGridConContactosApe
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
End Sub


Private Sub sdbddConApe_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddConApe.DataFieldList = "Column 0"
    sdbddConApe.DataFieldToDisplay = "Column 0"
    
End Sub
    


Private Sub CargarGridConProveCod()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oProve As CAsignacion
    
    sdbddProveCod.RemoveAll
    
    For Each oProve In oProvesAsignados
        sdbddProveCod.AddItem oProve.CodProve & Chr(m_lSeparador) & oProve.DenProve & Chr(m_lSeparador) & oProve.codEqp & Chr(m_lSeparador) & oProve.CodComp
    Next
    
    If sdbddProveCod.Rows = 0 Then
        sdbddProveCod.AddItem ""
    End If
    
    sdbddProveCod.MoveFirst
    
End Sub
Private Sub CargarGridConProveDen()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oProve As CAsignacion
    
    sdbddProveDen.RemoveAll
    
    For Each oProve In oProvesAsignados
        sdbddProveDen.AddItem oProve.DenProve & Chr(m_lSeparador) & oProve.CodProve & Chr(m_lSeparador) & oProve.codEqp & Chr(m_lSeparador) & oProve.CodComp
    Next
    
    If sdbddProveDen.Rows = 0 Then
        sdbddProveDen.AddItem ""
    End If
    
    sdbddProveDen.MoveFirst
    
End Sub
Private Sub CargarGridConContactosApe()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oCon As CContacto
    
    sdbddConApe.RemoveAll
    
    For Each oCon In oContactos
        sdbddConApe.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & IIf(oCon.Port, "1", "0")
    Next
    
    If sdbddConApe.Rows = 0 Then
        sdbddConApe.AddItem ""
    End If
    
    sdbddConApe.MoveFirst
    
End Sub



Private Sub sdbddConApe_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    Dim sContacto As String

    On Error Resume Next
    sContacto = ""
    sdbddConApe.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddConApe.Rows - 1
            bm = sdbddConApe.GetBookmark(i)
            If sdbddConApe.Columns(1).CellText(bm) = "" Then
                sContacto = sdbddConApe.Columns(0).CellText(bm)
            Else
                sContacto = sdbddConApe.Columns(0).CellText(bm) & ", " & sdbddConApe.Columns(1).CellText(bm)
            End If
            If UCase(Text) = UCase(Mid(sContacto, 1, Len(Text))) Then
                sdbddConApe.Bookmark = bm
                Exit For
            End If
        Next i
    End If


End Sub




Private Sub sdbddProveCod_CloseUp()
    
    If sdbddProveCod.Columns("COD").Value <> "" Then
        sdbgPet.Columns("CODPROVE").Value = sdbddProveCod.Columns("COD").Value
        sdbgPet.Columns("CODPROVE").Text = sdbddProveCod.Columns("COD").Value
        sdbgPet.Columns("DENPROVE").Value = sdbddProveCod.Columns("DEN").Value
        sdbgPet.Columns("DENPROVE").Text = sdbddProveCod.Columns("DEN").Value
        sdbgPet.Columns("EQP").Value = sdbddProveCod.Columns("EQP").Value
        sdbgPet.Columns("COM").Value = sdbddProveCod.Columns("COM").Value
        sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(sdbddProveCod.Columns("COD").Value), "1", "0")
    End If
   
End Sub


Private Sub sdbddProveDen_CloseUp()
    
    If sdbddProveDen.Columns("DEN").Value <> "" Then
        sdbgPet.Columns("CODPROVE").Value = sdbddProveDen.Columns("COD").Value
        sdbgPet.Columns("CODPROVE").Text = sdbddProveDen.Columns("COD").Value
        sdbgPet.Columns("DENPROVE").Value = sdbddProveDen.Columns("DEN").Value
        sdbgPet.Columns("DENPROVE").Text = sdbddProveDen.Columns("DEN").Value
        sdbgPet.Columns("EQP").Value = sdbddProveDen.Columns("EQP").Value
        sdbgPet.Columns("COM").Value = sdbddProveDen.Columns("COM").Value
        sdbgPet_Change
        sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(sdbddProveDen.Columns("COD").Value), "1", "0")

    End If
   
End Sub

Private Sub sdbddProveDen_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveDen.DataFieldList = "Column 0"
    sdbddProveDen.DataFieldToDisplay = "Column 0"
End Sub




Private Sub sdbgPet_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub


Private Sub sdbgPet_Change()
    Dim oProve As CProveedor

    If sdbgPet.Columns("CODPROVE").Value = "" Then
        Exit Sub
    Else
        Set oProve = oProvesAsignados.Item(sdbgPet.Columns("CODPROVE").Value)
        If oProve Is Nothing Then Exit Sub
        If oProve.Contactos Is Nothing Then
            oProve.CargarTodosLosContactos
        End If
    End If
    
    Select Case sdbgPet.col
    
    Case 0, 1
        
        sdbgPet.Columns("APE").Value = ""
        sdbgPet.Columns("CODCON").Value = ""
        sdbgPet.Columns("EMAIL").Value = ""
        

    End Select
    
    'Comprobar el contacto de portal y su email si comunicacion web
    If sdbgPet.Columns("APE").Value <> "" Then

    If Not oProve.Contactos.Item(sdbgPet.Columns("CODCON").Value) Is Nothing Then
        If oProve.Contactos.Item(sdbgPet.Columns("CODCON").Value).Port = False Then
            oMensajes.ContactoWebNoValido
            sdbgPet.CancelUpdate
            sdbgPet.DataChanged = False
            Set oProve = Nothing
            Exit Sub
        End If
    End If
    End If
    Set oProve = Nothing
   
End Sub

Private Sub sdbgPet_InitColumnProps()
    
    sdbgPet.Columns("APE").Locked = True
    
End Sub
Private Sub sdbddProveCod_DropDown()

    Screen.MousePointer = vbHourglass
    
        
    CargarGridConProveCod
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProveCod_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveCod.DataFieldList = "Column 0"
    sdbddProveCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddProveCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveCod.Rows - 1
            bm = sdbddProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddProveCod_ValidateList(Text As String, RtnPassed As Integer)
    ''' * Objetivo: Validar la seleccion (nulo o existente)

    ''' Si es nulo, correcto

    If Text = "" Then
        RtnPassed = True
        sdbgPet.Columns("CODPROVE").Value = ""
        sdbgPet.Columns("CODPROVE").Text = ""
        sdbgPet.Columns("DENPROVE").Value = ""
        sdbgPet.Columns("DENPROVE").Text = ""
        sdbgPet.Columns("OFERTADOWEB").Value = ""
    Else

        ''' Comprobar la existencia en la lista
        If sdbddProveCod.Columns("COD").Value = Text Then
            RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = sdbddProveCod.Columns("COD").Value
            sdbgPet.Columns("CODPROVE").Text = sdbddProveCod.Columns("COD").Value
            sdbgPet.Columns("DENPROVE").Value = sdbddProveCod.Columns("DEN").Value
            sdbgPet.Columns("DENPROVE").Text = sdbddProveCod.Columns("DEN").Value
            sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(sdbddProveCod.Columns("COD").Value), "1", "0")
            Exit Sub
        End If

        Screen.MousePointer = vbHourglass
        If frmOFEPet.bComunAsigComp Then
            Set oProves = oproce.CargarProveedoresAAvisar(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , sdbgPet.ActiveCell.Value, True, OrdAsigPorCodProve)
        Else
            If frmOFEPet.bComunAsigEqp Then
                Set oProves = oproce.CargarProveedoresAAvisar(basOptimizacion.gCodEqpUsuario, , , sdbgPet.ActiveCell.Value, True, OrdAsigPorCodProve)
            Else
                Set oProves = oproce.CargarProveedoresAAvisar(, , , sdbgPet.ActiveCell.Value, True, OrdAsigPorCodProve)
            End If
        End If


        If Not oProves.Item(1) Is Nothing Then
            RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = oProves.Item(1).CodProve
            sdbgPet.Columns("CODPROVE").Text = oProves.Item(1).CodProve
            sdbgPet.Columns("DENPROVE").Value = oProves.Item(1).DenProve
            sdbgPet.Columns("DENPROVE").Text = oProves.Item(1).DenProve
            sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(oProves.Item(1).CodProve), "1", "0")
        Else
            sdbgPet.Columns("CODPROVE").Value = ""
            sdbgPet.Columns("CODPROVE").Text = ""
            sdbgPet.Columns("DENPROVE").Value = ""
            sdbgPet.Columns("DENPROVE").Text = ""
            sdbgPet.Columns("OFERTADOWEB").Value = ""
        End If

    End If

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProveDen_DropDown()

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    
    Screen.MousePointer = vbHourglass
    
    
        
    CargarGridConProveDen
    
    sdbgPet.ActiveCell.SelStart = 0
    sdbgPet.ActiveCell.SelLength = Len(sdbgPet.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddProveDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveDen.Rows - 1
            bm = sdbddProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddProveDen_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)

    ''' Si es nulo, correcto

    If Text = "" Then
        RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = ""
            sdbgPet.Columns("CODPROVE").Text = ""
            sdbgPet.Columns("DENPROVE").Value = ""
            sdbgPet.Columns("DENPROVE").Text = ""
            sdbgPet.Columns("OFERTADOWEB").Value = ""
    Else

        ''' Comprobar la existencia en la lista
        If sdbddProveDen.Columns(0).Value = Text Then
            RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = sdbddProveDen.Columns("COD").Value
            sdbgPet.Columns("CODPROVE").Value = sdbddProveDen.Columns("COD").Value
            sdbgPet.Columns("DENPROVE").Value = sdbddProveDen.Columns("DEN").Value
            sdbgPet.Columns("DENPROVE").Value = sdbddProveDen.Columns("DEN").Value
            sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(sdbddProveDen.Columns("COD").Value), "1", "0")

            Exit Sub
        End If
        Screen.MousePointer = vbHourglass
        If frmOFEPet.bComunAsigComp Then
            Set oProves = oproce.CargarProveedoresAAvisar(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve)
        Else
            If frmOFEPet.bComunAsigEqp Then
                Set oProves = oproce.CargarProveedoresAAvisar(basOptimizacion.gCodEqpUsuario, , , sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve)
            Else
                Set oProves = oproce.CargarProveedoresAAvisar(, , , sdbgPet.ActiveCell.Value, True, OrdAsigPorDenProve)
            End If
        End If

        If Not oProves.Item(1) Is Nothing Then
            RtnPassed = True
            sdbgPet.Columns("CODPROVE").Value = oProves.Item(1).CodProve
            sdbgPet.Columns("CODPROVE").Text = oProves.Item(1).CodProve
            sdbgPet.Columns("DENPROVE").Value = oProves.Item(1).DenProve
            sdbgPet.Columns("DENPROVE").Text = oProves.Item(1).DenProve
            sdbgPet.Columns("OFERTADOWEB").Value = IIf(frmOFEPet.RecibidaOfertaWebDelProveedor(oProves.Item(1).CodProve), "1", "0")
        Else
            sdbgPet.Columns("CODPROVE").Value = ""
            sdbgPet.Columns("CODPROVE").Text = ""
            sdbgPet.Columns("DENPROVE").Value = ""
            sdbgPet.Columns("DENPROVE").Text = ""
            sdbgPet.Columns("OFERTADOWEB").Value = ""
        End If

    End If
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Env�a emails</summary>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 03/06/2011</revision>

Private Sub EnviarEmails()
    Dim errormail As TipoErrorSummit
    Dim sCuerpo As String
    Dim bHTML As Boolean
    Dim vbm As Variant
    Dim oPets As fsgsserver.CPetOfertas
    Dim oPet As fsgsserver.CPetOferta
    Dim oProve As fsgsserver.CProveedor
    Dim sProve As String
    Dim ioAsigs As IAsignaciones
    Dim oProves As fsgsserver.CProveedores
    Dim oCon As fsgsserver.CContacto
    Dim lIdContacto As Variant
    Dim sEqp As Variant
    Dim sComp As Variant
    Dim i As Integer
    Dim sMailSubject As String
    
    Set oPets = oFSGSRaiz.generar_CPetOfertas
    
    sdbgPet.Update
    
    For i = 0 To sdbgPet.Rows - 1
        vbm = sdbgPet.AddItemBookmark(i)
        
        sProve = sdbgPet.Columns("CODPROVE").CellValue(vbm)
        lIdContacto = sdbgPet.Columns("CODCON").CellValue(vbm)
        sEqp = sdbgPet.Columns("EQP").CellValue(vbm)
        sComp = sdbgPet.Columns("COM").CellValue(vbm)
        If Not (lIdContacto = "" Or sProve = "") Then
            If moComps Is Nothing Then
                Set ioAsigs = frmOFEPet.oProcesoSeleccionado
                Set moComps = ioAsigs.DevolverCompradoresAsignados
                Set ioAsigs = Nothing
            End If
                
            Screen.MousePointer = vbHourglass
            Set oProves = oFSGSRaiz.generar_CProveedores
            oProves.Add sProve, ""
            oProves.CargarDatosProveedor (sProve)
            Set oProve = oProves.Item(sProve)
            oProve.CargarTodosLosContactos lIdContacto:=lIdContacto
            Set oCon = oProve.Contactos.Item(CStr(lIdContacto))
                        
            bHTML = SQLBinaryToBoolean(oCon.TipoEmail)
                        
            Dim oCEmailProce As CEmailProce
            Set oCEmailProce = GenerarCEmailProce(oFSGSRaiz, gParametrosGenerales, basPublic.gParametrosInstalacion, oMensajes, basOptimizacion.gTipoDeUsuario, oUsuarioSummit, basParametros.gLongitudesDeCodigos)
            If bHTML Then
                'Genera el cuerpo del mensaje en formato HTML
                sCuerpo = oCEmailProce.GenerarMensajeAvisoHTML(oProve, oCon, sEqp, sComp, frmOFEPet.oProcesoSeleccionado, moComps)
            Else
                'Genera el cuerpo del mensaje en formato TXT
                sCuerpo = oCEmailProce.GenerarMensajeAvisoTXT(oProve, oCon, sEqp, sComp, frmOFEPet.oProcesoSeleccionado, moComps)
            End If
            Set oCEmailProce = Nothing

            If oIdsMail Is Nothing Then
                Set oIdsMail = IniciarSesionMail
            End If
            sMailSubject = ComponerAsuntoEMail(gParametrosInstalacion.gsOFENotifAvisoDespubSubject, frmOFEPet.oProcesoSeleccionado.Anyo, frmOFEPet.oProcesoSeleccionado.GMN1Cod, frmOFEPet.oProcesoSeleccionado.Cod, frmOFEPet.oProcesoSeleccionado.Den)
            errormail = ComponerMensaje(oCon.mail, sMailSubject, sCuerpo, , "frmOFEAvisoAnya", bHTML, _
                                entidadNotificacion:=ProcesoCompra, _
                                tipoNotificacion:=PetOferta, _
                                sToName:=oCon.nombre & " " & oCon.Apellidos, _
                                Anyo:=frmOFEPet.oProcesoSeleccionado.Anyo, _
                                GMN1:=frmOFEPet.oProcesoSeleccionado.GMN1Cod, _
                                Proce:=frmOFEPet.oProcesoSeleccionado.Cod)
            
            
            
            If errormail.NumError <> TESnoerror Then
                basErrores.TratarError errormail
            ElseIf g_bCancelarMail = False And sCuerpo <> "" Then
                Set oPet = oPets.Add(oProve.Cod, oProve.Den, False, True, False, Date, i, oCon.Apellidos, oCon.nombre, oCon.mail, , , , , , tipoNotificacion.TNAvisoAutomatico)
            Else
                g_bCancelarMail = False
            End If
        End If
    Next i
    FinalizarSesionMail
    Dim ioPets As IPeticiones
    
    If oPets.Count > 0 Then
        Set ioPets = frmOFEPet.oProcesoSeleccionado
        errormail = ioPets.RealizarComunicacion(oPets)
        If errormail.NumError <> TESnoerror Then
            basErrores.TratarError errormail
        Else
            basSeguridad.RegistrarAccion accionessummit.ACCPetOfeAvisoDespub, "Anyo:" & CStr(frmOFEPet.sdbcAnyo.Value) & "GMN1:" & CStr(frmOFEPet.sdbcGMN1_4Cod.Value) & "Proce:" & Int(frmOFEPet.sdbcProceCod.Value)
        End If
        Set ioPets = Nothing
        frmOFEPet.cmdRestaurar_Click
    End If
        
    Unload Me
End Sub
