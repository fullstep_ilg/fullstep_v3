VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSolCumplDesglose 
   Caption         =   "DDesglose de material"
   ClientHeight    =   5580
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10200
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolCumplDesglose.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5580
   ScaleWidth      =   10200
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   60
      ScaleHeight     =   405
      ScaleWidth      =   10095
      TabIndex        =   0
      Top             =   5200
      Width           =   10095
      Begin VB.CommandButton cmdSubir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   0
         Picture         =   "frmSolCumplDesglose.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   1050
      End
      Begin VB.CommandButton cmdBajar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1100
         Picture         =   "frmSolCumplDesglose.frx":0FF4
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   1050
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgGrupos 
      Height          =   5100
      Left            =   60
      TabIndex        =   3
      Top             =   60
      Width           =   10110
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   12
      stylesets.count =   5
      stylesets(0).Name=   "Calculado"
      stylesets(0).BackColor=   16766421
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSolCumplDesglose.frx":1336
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "Gris"
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSolCumplDesglose.frx":139D
      stylesets(2).Name=   "Estado"
      stylesets(2).ForeColor=   255
      stylesets(2).BackColor=   16777215
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSolCumplDesglose.frx":13B9
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmSolCumplDesglose.frx":13D5
      stylesets(4).Name=   "Amarillo"
      stylesets(4).ForeColor=   255
      stylesets(4).BackColor=   12648447
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmSolCumplDesglose.frx":13F1
      UseGroups       =   -1  'True
      DividerType     =   0
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   3
      Groups(0).Width =   6112
      Groups(0).Caption=   "DATO"
      Groups(0).Columns.Count=   4
      Groups(0).Columns(0).Width=   3528
      Groups(0).Columns(0).Visible=   0   'False
      Groups(0).Columns(0).Caption=   "ID_CAMPO"
      Groups(0).Columns(0).Name=   "ID_CAMPO"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   6112
      Groups(0).Columns(1).Caption=   "DATO"
      Groups(0).Columns(1).Name=   "DATO"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(2).Width=   1535
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "TIPO"
      Groups(0).Columns(2).Name=   "TIPO"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(3).Width=   4048
      Groups(0).Columns(3).Visible=   0   'False
      Groups(0).Columns(3).Caption=   "CAMPO_GS"
      Groups(0).Columns(3).Name=   "CAMPO_GS"
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   8
      Groups(0).Columns(3).FieldLen=   256
      Groups(1).Width =   3360
      Groups(1).Caption=   "SOLICITANTE@@"
      Groups(1).Columns.Count=   4
      Groups(1).Columns(0).Width=   1111
      Groups(1).Columns(0).Caption=   "VISIBLE_S"
      Groups(1).Columns(0).Name=   "VISIBLE_S"
      Groups(1).Columns(0).DataField=   "Column 4"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Style=   2
      Groups(1).Columns(1).Width=   1111
      Groups(1).Columns(1).Caption=   "ESCRITURA_S"
      Groups(1).Columns(1).Name=   "ESCRITURA_S"
      Groups(1).Columns(1).DataField=   "Column 5"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Style=   2
      Groups(1).Columns(2).Width=   1138
      Groups(1).Columns(2).Caption=   "OBL_S"
      Groups(1).Columns(2).Name=   "OBL_S"
      Groups(1).Columns(2).DataField=   "Column 6"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(2).Style=   2
      Groups(1).Columns(3).Width=   1614
      Groups(1).Columns(3).Visible=   0   'False
      Groups(1).Columns(3).Caption=   "ID"
      Groups(1).Columns(3).Name=   "ID_S"
      Groups(1).Columns(3).DataField=   "Column 7"
      Groups(1).Columns(3).DataType=   8
      Groups(1).Columns(3).FieldLen=   256
      Groups(2).Width =   3360
      Groups(2).Visible=   0   'False
      Groups(2).Caption=   "PROVEEDORES@@"
      Groups(2).Columns.Count=   4
      Groups(2).Columns(0).Width=   1111
      Groups(2).Columns(0).Caption=   "VISIBLE"
      Groups(2).Columns(0).Name=   "VISIBLE_P"
      Groups(2).Columns(0).DataField=   "Column 8"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(0).Style=   2
      Groups(2).Columns(1).Width=   1111
      Groups(2).Columns(1).Caption=   "ESCRITURA"
      Groups(2).Columns(1).Name=   "ESCRITURA_P"
      Groups(2).Columns(1).DataField=   "Column 9"
      Groups(2).Columns(1).DataType=   8
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(1).Style=   2
      Groups(2).Columns(2).Width=   1138
      Groups(2).Columns(2).Caption=   "OBL"
      Groups(2).Columns(2).Name=   "OBL_P"
      Groups(2).Columns(2).DataField=   "Column 10"
      Groups(2).Columns(2).DataType=   8
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(2).Style=   2
      Groups(2).Columns(3).Width=   1614
      Groups(2).Columns(3).Visible=   0   'False
      Groups(2).Columns(3).Caption=   "ID_P"
      Groups(2).Columns(3).Name=   "ID_P"
      Groups(2).Columns(3).DataField=   "Column 11"
      Groups(2).Columns(3).DataType=   8
      Groups(2).Columns(3).FieldLen=   256
      _ExtentX        =   17833
      _ExtentY        =   8996
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSolCumplDesglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oCampoPadre As CFormItem
Public g_bModificar As Boolean

Private m_oSolicitud As CSolicitud

'Variables de idiomas
Private m_sIdiWork As String
Private m_sIdiVisible As String
Private m_sIdiEscritura As String
Private m_sIdiObl As String
Private m_sIdiProveedores As String
Private m_sIdiCampo As String
Private m_sEsEstado As String

Private m_bCtrlNoMoverJunto As Boolean

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOL_CUMPLIMENTACION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value   '1 Cumplimentaci�n de formulario
        Ador.MoveNext
        '2 Configurar por pasos
        Ador.MoveNext
        '3 Configurar por grupos
        Ador.MoveNext
        '4 Usar una configuraci�n espec�fica para proveedores
        Ador.MoveNext
        sdbgGrupos.Columns("DATO").caption = Ador(0).Value '5 Dato
        sdbgGrupos.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        '6 Grupo
        Ador.MoveNext
         '7 Visible
        Ador.MoveNext
         '8 Escritura
        Ador.MoveNext
        '9 Obligatorio
        Ador.MoveNext
        m_sIdiWork = Ador(0).Value '10 Sobre workflow:
        Ador.MoveNext
        m_sIdiProveedores = Ador(0).Value ' 11 -PROVEEDORES-
        sdbgGrupos.Groups(2).caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiVisible = Ador(0).Value  '12 Vis.
        sdbgGrupos.Groups(2).Columns("VISIBLE_P").caption = Ador(0).Value
        sdbgGrupos.Groups(1).Columns("VISIBLE_S").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiEscritura = Ador(0).Value  '13 Esc.
        sdbgGrupos.Groups(2).Columns("ESCRITURA_P").caption = Ador(0).Value
        sdbgGrupos.Groups(1).Columns("ESCRITURA_S").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiObl = Ador(0).Value  '14 Obl.
        sdbgGrupos.Groups(2).Columns("OBL_P").caption = Ador(0).Value
        sdbgGrupos.Groups(1).Columns("OBL_S").caption = Ador(0).Value
        Ador.MoveNext
        sdbgGrupos.Groups(1).caption = Ador(0).Value  '15 Solicitante
        Ador.MoveNext
        m_sIdiCampo = Ador(0).Value ' 16 Campo:
       
        Ador.MoveNext
        '17 No permitir realizar pedidos hasta que haya finalizado el workflow
        Ador.MoveNext
        m_sEsEstado = Ador(0).Value ' 18 Estado
       
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub

''' <summary>
''' Recolocar un campo en la solicitud bajandolo
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub cmdBajar_Click()
    Dim i As Integer
    Dim arrValores() As Variant
    Dim arrValores2() As Variant
    Dim Quien As Integer
    Dim vbm As Variant
    
    Quien = 0
    
    If sdbgGrupos.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgGrupos.AddItemRowIndex(sdbgGrupos.SelBookmarks.Item(0)) = sdbgGrupos.Rows - 1 Then Exit Sub
    
    LockWindowUpdate Me.hWnd
    
    i = 0
    ReDim arrValores(sdbgGrupos.Columns.Count - 1)
    ReDim arrValores2(sdbgGrupos.Columns.Count - 1)
            
    If Not m_bCtrlNoMoverJunto Then
        Select Case sdbgGrupos.Columns("CAMPO_GS").Value
        Case TipoCampoGS.material, TipoCampoGS.Pais
        
            vbm = sdbgGrupos.GetBookmark(1)
            
            If (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                If (sdbgGrupos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Then
                    Quien = 1
                Else
                    Quien = -1
                End If
            ElseIf (sdbgGrupos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Then
                Quien = 1
            End If
            
            sdbgGrupos.MoveNext
            sdbgGrupos.MoveNext
            If (Quien = 0) Then sdbgGrupos.MoveNext
            
            If Not (Quien = -1) Then
                If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 0) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'Pais
                    
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'Provi
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                ElseIf (Quien = 1) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 1) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'den
                    
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'art
                    
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'mat
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    If (Quien = 0) Then sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                End If
                LockWindowUpdate 0&
                Exit Sub
            End If
        Case TipoCampoGS.NuevoCodArticulo, TipoCampoGS.CodArticulo, TipoCampoGS.provincia
            If (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                Quien = 1
            ElseIf (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) Then
                Quien = 11
            End If
            
            sdbgGrupos.MoveNext
            If (Quien = 0) Then sdbgGrupos.MoveNext
            
            If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
            And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            ElseIf (Quien = 0) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                m_bCtrlNoMoverJunto = True
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click
                
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                
                m_bCtrlNoMoverJunto = False
            ElseIf (Quien = 1) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
            And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Then
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            ElseIf (Quien = 1) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Then
                m_bCtrlNoMoverJunto = True
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click 'la den
                
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click 'el art
                
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click 'la mat
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                
                m_bCtrlNoMoverJunto = False
            ElseIf (Quien = 11) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) _
            And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            ElseIf (Quien = 11) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                m_bCtrlNoMoverJunto = True
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click
                
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                
                m_bCtrlNoMoverJunto = False
            Else
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MovePrevious
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            End If
            LockWindowUpdate 0&
            Exit Sub
        Case TipoCampoGS.DenArticulo
            sdbgGrupos.MoveNext
            If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
            And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            ElseIf (Quien = 0) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                m_bCtrlNoMoverJunto = True
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click 'pais
                
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdSubir_Click 'provi
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                
                m_bCtrlNoMoverJunto = False
            Else
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MovePrevious
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            End If
            LockWindowUpdate 0&
            Exit Sub
        End Select
    End If
    
    For i = 0 To sdbgGrupos.Columns.Count - 1
        arrValores(i) = sdbgGrupos.Columns(i).Value
    Next i
    sdbgGrupos.MoveNext
    
    Quien = CInt(IIf(sdbgGrupos.Columns("CAMPO_GS").Value = "", 0, sdbgGrupos.Columns("CAMPO_GS").Value))
        
    For i = 0 To sdbgGrupos.Columns.Count - 1
        arrValores2(i) = sdbgGrupos.Columns(i).Value
        sdbgGrupos.Columns(i).Value = arrValores(i)
    Next i
    
    sdbgGrupos.MovePrevious
    
    For i = 0 To sdbgGrupos.Columns.Count - 1
        sdbgGrupos.Columns(i).Value = arrValores2(i)
    Next i
        
    sdbgGrupos.SelBookmarks.RemoveAll
    sdbgGrupos.MoveNext
    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
    
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos


    Select Case Quien
    Case TipoCampoGS.material
        cmdBajar_Click
        cmdBajar_Click
    Case TipoCampoGS.Pais
        cmdBajar_Click
    End Select
    
    LockWindowUpdate 0&
End Sub

''' <summary>
''' Recolocar un campo en la solicitud subiendolo
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub cmdSubir_Click()
    Dim i As Integer
    Dim arrValores() As Variant
    Dim arrValores2() As Variant
    Dim Quien As Integer
    Dim vbm As Variant
    Dim aux As Integer
    
    Quien = 0
    
    If sdbgGrupos.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgGrupos.AddItemRowIndex(sdbgGrupos.SelBookmarks.Item(0)) = 0 Then Exit Sub
        
    LockWindowUpdate Me.hWnd
    
    i = 0
    ReDim arrValores(sdbgGrupos.Columns.Count - 1)
    ReDim arrValores2(sdbgGrupos.Columns.Count - 1)
                 
    If Not m_bCtrlNoMoverJunto Then
        Select Case sdbgGrupos.Columns("CAMPO_GS").Value
        Case TipoCampoGS.material, TipoCampoGS.Pais
        
            vbm = sdbgGrupos.GetBookmark(1)
            
            If (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                If (sdbgGrupos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Then
                    Quien = 1
                Else
                    Quien = -1
                End If
            ElseIf (sdbgGrupos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Then
                Quien = 11
            End If
            
            sdbgGrupos.MovePrevious
            
            If Not (Quien = -1) Then
                aux = 0
                If (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then aux = 1
                
                If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    If (Quien = 0) Or (aux = 1) Then sdbgGrupos.MovePrevious
                    If (aux = 1) Then sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 0) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click 'provi
                    
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click 'Pais
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                ElseIf (Quien = 1) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    If (aux = 1) Then
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                    End If
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 1) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click 'den
                    
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click 'art
                    
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click 'mat
                                        
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                ElseIf (Quien = 11) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 11) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                                
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                End If
                
                LockWindowUpdate 0&
                Exit Sub
            End If
        Case TipoCampoGS.NuevoCodArticulo, TipoCampoGS.CodArticulo, TipoCampoGS.provincia
            If (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                Quien = 1
            ElseIf (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) Then
                Quien = 11
            End If
            
            sdbgGrupos.MovePrevious
            sdbgGrupos.MovePrevious
                        
            If (Quien = 0 Or Quien = 11) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
            And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MovePrevious
                If (Quien = 0) Then sdbgGrupos.MovePrevious
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            ElseIf (Quien = 0 Or Quien = 11) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                m_bCtrlNoMoverJunto = True
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click
                
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                If (Quien = 0) Then sdbgGrupos.MovePrevious
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MovePrevious
                If (Quien = 0) Then sdbgGrupos.MovePrevious
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                
                m_bCtrlNoMoverJunto = False
            ElseIf (Quien = 1) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) _
            And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MovePrevious
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            ElseIf Quien = 1 And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
                m_bCtrlNoMoverJunto = True
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click 'la den
                
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click 'el art
                
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click 'la mat
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MovePrevious
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                
                m_bCtrlNoMoverJunto = False
            Else
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            End If
            
            LockWindowUpdate 0&
            Exit Sub
        Case TipoCampoGS.DenArticulo
            sdbgGrupos.MovePrevious
            sdbgGrupos.MovePrevious
            sdbgGrupos.MovePrevious
            
            If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
            And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MovePrevious
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            ElseIf (Quien = 0) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                m_bCtrlNoMoverJunto = True
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click
                
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                cmdBajar_Click
                
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MovePrevious
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                
                m_bCtrlNoMoverJunto = False
            Else
                sdbgGrupos.SelBookmarks.RemoveAll
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
            End If
            
            LockWindowUpdate 0&
            Exit Sub
        End Select
    End If
    For i = 0 To sdbgGrupos.Columns.Count - 1
        arrValores(i) = sdbgGrupos.Columns(i).Value
    Next i
    sdbgGrupos.MovePrevious
    
    Quien = CInt(IIf(sdbgGrupos.Columns("CAMPO_GS").Value = "", 0, sdbgGrupos.Columns("CAMPO_GS").Value))
        
    For i = 0 To sdbgGrupos.Columns.Count - 1
        arrValores2(i) = sdbgGrupos.Columns(i).Value
        sdbgGrupos.Columns(i).Value = arrValores(i)
    Next i
    
    sdbgGrupos.MoveNext
    
    For i = 0 To sdbgGrupos.Columns.Count - 1
        sdbgGrupos.Columns(i).Value = arrValores2(i)
    Next i
        
    sdbgGrupos.SelBookmarks.RemoveAll
    sdbgGrupos.MovePrevious
    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
    
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos
    
    Select Case Quien
    Case TipoCampoGS.DenArticulo
        cmdSubir_Click
        cmdSubir_Click
    Case TipoCampoGS.provincia
        cmdSubir_Click
    End Select
    
    LockWindowUpdate 0&
End Sub

Private Sub Form_Load()
    Me.Height = 6090
    Me.Width = 10320
    
    CargarRecursos
    
    PonerFieldSeparator Me

    Me.caption = frmSOLCumplimentacion.caption & ". " & m_sIdiCampo & g_oCampoPadre.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den

    Set m_oSolicitud = oFSGSRaiz.Generar_CSolicitud
    m_oSolicitud.Id = frmSOLCumplimentacion.g_oSolicitud.Id
    
    CargarGridGrupos
    
    If g_bModificar = False Then
        picNavigate.Visible = False
        
        sdbgGrupos.Columns("VISIBLE_P").Locked = True
        sdbgGrupos.Columns("ESCRITURA_P").Locked = True
        sdbgGrupos.Columns("OBL_P").Locked = True
        
        sdbgGrupos.Columns("VISIBLE_S").Locked = True
        sdbgGrupos.Columns("ESCRITURA_S").Locked = True
        sdbgGrupos.Columns("OBL_S").Locked = True
    End If
End Sub

Private Sub Form_Resize()

    If Me.Width < 2000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    sdbgGrupos.Width = Me.Width - 220
    sdbgGrupos.Height = Me.Height - 990
    
    If g_bModificar = False Then
        sdbgGrupos.Height = Me.Height - 590
    Else
        sdbgGrupos.Height = Me.Height - 990
    End If
    
    picNavigate.Top = sdbgGrupos.Top + sdbgGrupos.Height + 80
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oCampoPadre = Nothing
    Set m_oSolicitud = Nothing
End Sub

Private Sub sdbgGrupos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim oConf As CConfCumplimentacion
        
    Set oConf = oFSGSRaiz.Generar_CConfCumplimentacion
        
    oConf.Id = sdbgGrupos.Columns("ID_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value
    
    oConf.Visible = GridCheckToBoolean(sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value)
    oConf.Escritura = GridCheckToBoolean(sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value)
    oConf.Obligatorio = GridCheckToBoolean(sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value)
    
    Set oIBaseDatos = oConf
    teserror = oIBaseDatos.FinalizarEdicionModificando
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        sdbgGrupos.CancelUpdate
        If Me.Visible Then sdbgGrupos.SetFocus
        sdbgGrupos.DataChanged = False
    End If
    
    Set oConf = Nothing
    Set oIBaseDatos = Nothing
        
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbgGrupos_Change()
Dim sCol As String

    If sdbgGrupos.col < 0 Then Exit Sub
    If sdbgGrupos.Grp < 1 Then Exit Sub
                
    sCol = Left(sdbgGrupos.Columns(sdbgGrupos.col).Name, Len(sdbgGrupos.Columns(sdbgGrupos.col).Name) - Len("_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant))
    
    Select Case sCol
        Case "VISIBLE"
            If GridCheckToBoolean(sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) = False Then
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "P" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Then
                        sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
            End If
            
        Case "ESCRITURA"
            If sdbgGrupos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                Exit Sub
            End If
            
            If GridCheckToBoolean(sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) = False Then
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "P" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Then
                        sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
            Else
                If GridCheckToBoolean(sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) = False Then
                    sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                    Exit Sub
                End If
            End If
            
        Case "OBL"
            If sdbgGrupos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                Exit Sub
            End If
            
            If GridCheckToBoolean(sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) = True Then
                If sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = False Or sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = False Then
                    sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                    Exit Sub
                End If
                
            Else
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "P" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Then
                        sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
            End If
    End Select
    
    sdbgGrupos.Update
End Sub

Private Sub sdbgGrupos_RowLoaded(ByVal Bookmark As Variant)
    If sdbgGrupos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
        sdbgGrupos.Columns("DATO").CellStyleSet "Amarillo"
    ElseIf sdbgGrupos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
        sdbgGrupos.Columns("DATO").CellStyleSet "Calculado"
    ElseIf sdbgGrupos.Columns("DATO").CellValue(Bookmark) = m_sEsEstado Then
        sdbgGrupos.Columns("DATO").CellStyleSet "Estado"
    Else
        sdbgGrupos.Columns("DATO").CellStyleSet ""
    End If
End Sub

Private Sub CargarGridGrupos()
Dim oConf As CConfCumplimentacion
Dim sCadena As String
Dim sCadenaS As String
Dim sCadenaP As String
Dim sCadenaPasos As String
Dim lCampo As Long
Dim ogroup As SSDataWidgets_B.Group
Dim oWorkflow As cworkflow
Dim Ador As Ador.Recordset
Dim iTab As Integer
    
    'Carga los pasos del workflow de la solicitud en la y grid:
    Set oWorkflow = oFSGSRaiz.Generar_CWorkflow
    oWorkflow.Id = frmSOLCumplimentacion.g_lWorkFlow
 
    sdbgGrupos.Groups(1).TagVariant = "S"
    sdbgGrupos.Groups(2).TagVariant = "P"
    
    iTab = 2
    
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            'ahora carga los pasos en la grid de grupos.Cada paso ser� un grupo de la grid:
            sdbgGrupos.Groups.Add iTab
            sdbgGrupos.Groups(iTab).caption = Ador.Fields("DEN").Value
            sdbgGrupos.Groups(iTab).TagVariant = "P" & Ador.Fields("ID").Value
            sdbgGrupos.Groups(iTab).Width = 1900
             
            sdbgGrupos.Groups(iTab).Columns.Add 0
            sdbgGrupos.Groups(iTab).Columns(0).Name = "VISIBLE_P" & Ador.Fields("ID").Value
            sdbgGrupos.Groups(iTab).Columns(0).caption = m_sIdiVisible
            sdbgGrupos.Groups(iTab).Columns(0).Style = ssStyleCheckBox
            If g_bModificar = False Then
                sdbgGrupos.Groups(iTab).Columns(0).Locked = True
            End If
            
            sdbgGrupos.Groups(iTab).Columns.Add 1
            sdbgGrupos.Groups(iTab).Columns(1).Name = "ESCRITURA_P" & Ador.Fields("ID").Value
            sdbgGrupos.Groups(iTab).Columns(1).caption = m_sIdiEscritura
            sdbgGrupos.Groups(iTab).Columns(1).Style = ssStyleCheckBox
            If g_bModificar = False Then
                sdbgGrupos.Groups(iTab).Columns(1).Locked = True
            End If
            
            sdbgGrupos.Groups(iTab).Columns.Add 2
            sdbgGrupos.Groups(iTab).Columns(2).Name = "OBL_P" & Ador.Fields("ID").Value
            sdbgGrupos.Groups(iTab).Columns(2).caption = m_sIdiObl
            sdbgGrupos.Groups(iTab).Columns(2).Style = ssStyleCheckBox
            If g_bModificar = False Then
                sdbgGrupos.Groups(iTab).Columns(2).Locked = True
            End If
            
            sdbgGrupos.Groups(iTab).Columns.Add 3
            sdbgGrupos.Groups(iTab).Columns(3).Name = "ID_P" & Ador.Fields("ID").Value
            sdbgGrupos.Groups(iTab).Columns(3).Visible = False
            
            sdbgGrupos.Groups(iTab).Columns(0).Width = 629
            sdbgGrupos.Groups(iTab).Columns(1).Width = 629
            sdbgGrupos.Groups(iTab).Columns(2).Width = 645
            
            iTab = iTab + 1
            Ador.MoveNext
        Wend
        Ador.Close
    End If
    Set Ador = Nothing
    Set oWorkflow = Nothing
    
    'Oculta o muestra el grupo de los proveedores dependiendo de si se muestra o no en la pantalla anterior
    If frmSOLCumplimentacion.chkProve.Value = vbChecked Then
        sdbgGrupos.Groups(iTab).Visible = True
    Else
        sdbgGrupos.Groups(iTab).Visible = False
    End If
    
    'Ahora rellena la grid:
    m_oSolicitud.CargarConfiguracion , g_oCampoPadre.Grupo.Id, , g_oCampoPadre.Id

    sdbgGrupos.RemoveAll
    
    lCampo = 0
    
    For Each oConf In m_oSolicitud.Cumplimentaciones
        If oConf.Campo.Id <> lCampo Then
            lCampo = oConf.Campo.Id
            
            If sCadena <> "" Then sdbgGrupos.AddItem sCadena & sCadenaS & sCadenaPasos & sCadenaP
    
            sCadenaS = ""
            sCadenaP = ""
            sCadenaPasos = ""
            If oConf.EsEstado = 0 Then
                sCadena = oConf.Campo.Id & Chr(m_lSeparador) & oConf.Campo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                sCadena = sCadena & Chr(m_lSeparador) & oConf.Campo.TipoPredef & Chr(m_lSeparador) & oConf.Campo.CampoGS
            Else
                sCadena = oConf.Id & Chr(m_lSeparador) & m_sEsEstado & Chr(m_lSeparador) & oConf.Campo.TipoPredef & Chr(m_lSeparador) & oConf.Campo.CampoGS
            End If
        End If
        
        For Each ogroup In sdbgGrupos.Groups
            If oConf.EsEstado = 0 Then
                If ogroup.TagVariant = "S" And oConf.SolicitanteProv = ConfCumpliment.ConfSolicitante Then
                    sCadenaS = sCadenaS & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Escritura) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio) & Chr(m_lSeparador) & oConf.Id
                    Exit For
                ElseIf ogroup.TagVariant = "P" And oConf.SolicitanteProv = ConfCumpliment.ConfProveedor Then
                    sCadenaP = sCadenaP & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Escritura) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio) & Chr(m_lSeparador) & oConf.Id
                    Exit For
                ElseIf ogroup.TagVariant = "P" & oConf.Paso Then
                    sCadenaPasos = sCadenaPasos & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Escritura) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio) & Chr(m_lSeparador) & oConf.Id
                    Exit For
                End If
            Else
                If oConf.SolicitanteProv = ConfCumpliment.ConfSolicitante Then
                    sCadenaS = sCadenaS & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Escritura) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio) & Chr(m_lSeparador) & oConf.Id
                    Exit For
                ElseIf oConf.SolicitanteProv = ConfCumpliment.ConfProveedor Then
                    sCadenaP = sCadenaP & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Escritura) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio) & Chr(m_lSeparador) & oConf.Id
                    Exit For
                ElseIf oConf.Paso Then
                    sCadenaPasos = sCadenaPasos & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Escritura) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio) & Chr(m_lSeparador) & oConf.Id
                    Exit For
                End If
            End If
        Next
    Next
    
    If sCadena <> "" Then sdbgGrupos.AddItem sCadena & sCadenaS & sCadenaPasos & sCadenaP

    sdbgGrupos.MoveFirst
    
End Sub

Private Sub GuardarOrdenCampos()
Dim oConfs As CConfCumplimentaciones
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim vbm As Variant
Dim ogroup As SSDataWidgets_B.Group
Dim lID As Long

    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:

    Set oConfs = oFSGSRaiz.Generar_CConfCumplimentaciones
    
    For i = 0 To sdbgGrupos.Rows - 1
        vbm = sdbgGrupos.AddItemBookmark(i)

        For Each ogroup In sdbgGrupos.Groups
            If ogroup.TagVariant <> "" Then
                lID = ogroup.Columns("ID_" & ogroup.TagVariant).CellValue(vbm)
                'Guarda en BD solo el orden de los campos que han cambiado
                If NullToDbl0(m_oSolicitud.Cumplimentaciones.Item(CStr(lID)).Orden) <> i + 1 Then
                    m_oSolicitud.Cumplimentaciones.Item(CStr(lID)).Orden = i + 1
                    oConfs.Add lID, m_oSolicitud, m_oSolicitud.Cumplimentaciones.Item(CStr(lID)).Campo, , , , , , , m_oSolicitud.Cumplimentaciones.Item(CStr(lID)).Orden
                End If
            End If
        Next
    Next i

    teserror = oConfs.GuardarOrdenCampos(False)

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Set oConfs = Nothing
End Sub



