VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmESTRCOMPMatPorCom 
   Caption         =   "Materiales por comprador"
   ClientHeight    =   5205
   ClientLeft      =   1860
   ClientTop       =   2430
   ClientWidth     =   5475
   Icon            =   "frmESTRCOMPMatPorCom.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5205
   ScaleWidth      =   5475
   Begin MSComctlLib.TreeView tvwEstrMat 
      Height          =   3600
      Left            =   60
      TabIndex        =   4
      Top             =   1125
      Width           =   5355
      _ExtentX        =   9446
      _ExtentY        =   6350
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   510
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   5475
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   4695
      Width           =   5475
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   6
         Top             =   105
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2280
         TabIndex        =   7
         Top             =   105
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   105
         TabIndex        =   5
         Top             =   105
         Width           =   1005
      End
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   210
      Left            =   120
      Picture         =   "frmESTRCOMPMatPorCom.frx":014A
      ScaleHeight     =   210
      ScaleWidth      =   195
      TabIndex        =   17
      Top             =   1190
      Width           =   200
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1995
      Top             =   -30
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPMatPorCom.frx":0344
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPMatPorCom.frx":03F4
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPMatPorCom.frx":0848
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPMatPorCom.frx":0909
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPMatPorCom.frx":09C9
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPMatPorCom.frx":0A79
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPMatPorCom.frx":0B43
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPMatPorCom.frx":0BFC
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPMatPorCom.frx":0CAC
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraSelComp 
      Height          =   1050
      Left            =   60
      TabIndex        =   9
      Top             =   0
      Width           =   5355
      Begin SSDataWidgets_B.SSDBCombo sdbcCompCod 
         Height          =   285
         Left            =   1170
         TabIndex        =   2
         Top             =   630
         Width           =   1065
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1111
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5001
         Columns(1).Caption=   "Apellido y nombre"
         Columns(1).Name =   "Ape"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 3"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCompDen 
         Height          =   285
         Left            =   2250
         TabIndex        =   3
         Top             =   630
         Width           =   2985
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5424
         Columns(0).Caption=   "Apellido y nombre"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1296
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
         Height          =   285
         Left            =   2250
         TabIndex        =   1
         Top             =   225
         Width           =   2985
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1640
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
         Height          =   285
         Left            =   1170
         TabIndex        =   0
         Top             =   225
         Width           =   1065
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1296
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5345
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 3"
      End
      Begin VB.Label lblEqp 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1170
         TabIndex        =   16
         Top             =   225
         Visible         =   0   'False
         Width           =   4065
      End
      Begin VB.Label lblEqpCod 
         Caption         =   "Equipo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   90
         TabIndex        =   15
         Top             =   255
         Width           =   1005
      End
      Begin VB.Label lblComp 
         Caption         =   "Comprador:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   90
         TabIndex        =   10
         Top             =   660
         Width           =   1020
      End
   End
   Begin MSComctlLib.TreeView tvwEstrMatMod 
      Height          =   3660
      Left            =   60
      TabIndex        =   11
      Top             =   1125
      Visible         =   0   'False
      Width           =   5355
      _ExtentX        =   9446
      _ExtentY        =   6456
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   450
      Left            =   0
      ScaleHeight     =   450
      ScaleWidth      =   5475
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   4245
      Visible         =   0   'False
      Width           =   5475
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1485
         TabIndex        =   14
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2640
         TabIndex        =   13
         Top             =   45
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmESTRCOMPMatPorCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variable para no ejecutar el node check si se ha disparado el mousedown
Private bMouseDown As Boolean
Private SelNode As MSComctlLib.node

'Variables del resize
Private iAltura As Integer
Private iAnchura As Integer

' Variables de seguridad
Private bREqp As Boolean
Private bRMatAsig As Boolean
Private bModif As Boolean

' Variable de la combo
Private CargarComboDesdeEqp As Boolean
Private CargarComboDesdeComp As Boolean
Private RespetarCombo As Boolean

Public oGruposMN1 As CGruposMatNivel1
Public oGruposMN2 As CGruposMatNivel2
Public oGruposMN3 As CGruposMatNivel3
Public oGruposMN4 As CGruposMatNivel4

Private oGruposMN1Seleccionados As CGruposMatNivel1
Private oGruposMN2Seleccionados As CGruposMatNivel2
Private oGruposMN3Seleccionados As CGruposMatNivel3
Private oGruposMN4Seleccionados As CGruposMatNivel4

'Problemas Agustin 19-9-06
'   En el campo Equipo seleccionas, por ejemplo, el equipo de Bilbao y en el campo Comprador seleccionas _
    gaa (Areitioaurtena Abaitua, Gorka). Le das al bot�n Modificar, intentas asignarle, por ejemplo, todo el _
    00-Domusa, le das al bot�n Aceptar y los cambios no se reflejan en la pantalla.
'Veo que ya estaba 00, aunque no seleccionado �l, sino que esta por uno de sus hijos.
Private oGruposMN1SeleccionadosIniciales As CGruposMatNivel1
Private oGruposMN2SeleccionadosIniciales As CGruposMatNivel2
Private oGruposMN3SeleccionadosIniciales As CGruposMatNivel3
'
Private oGruposMN1DesSeleccionados As CGruposMatNivel1
Private oGruposMN2DesSeleccionados As CGruposMatNivel2
Private oGruposMN3DesSeleccionados As CGruposMatNivel3
Private oGruposMN4DesSeleccionados As CGruposMatNivel4
Private oGrupsMN1Asig As CGruposMatNivel1
Private oGrupsMN2Asig As CGruposMatNivel2
Private oGrupsMN3Asig As CGruposMatNivel3
Private oGrupsMN4Asig As CGruposMatNivel4
Private oEqpSeleccionado As CEquipo
Private oComps As CCompradores
Private oEqps As CEquipos
Private oCompSeleccionado As CComprador

Private oIBaseDatos As IBaseDatos

Private sIdiComp As String
Private sIdiMat As String


Private Sub CargarEstructuraMateriales()

If Not oCompSeleccionado Is Nothing Then
    
    tvwEstrMat.Scroll = False
    GenerarEstructuraDeMateriales
    tvwEstrMat.Scroll = True
    
    If bModif Then
        cmdModificar.Enabled = True
    End If
    cmdRestaurar.Enabled = True
End If

If tvwEstrMat.Nodes.Count = 0 Then Exit Sub

ConfigurarInterfazSeguridad tvwEstrMat.Nodes("Raiz")
If Me.Visible Then tvwEstrMat.SetFocus
tvwEstrMat.Nodes("Raiz").Root.Selected = True

End Sub




Private Sub cmdAceptar_Click()

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node
Dim nodx As MSComctlLib.node
Dim oIMaterial As IMaterialAsignado
Dim teserror As TipoErrorSummit
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

teserror.NumError = TESnoerror

Set nodx = tvwEstrMatMod.Nodes(1)
   If nodx.Children = 0 Then Exit Sub
    
   Screen.MousePointer = vbHourglass
   
   Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            scod1 = DevolverCod(nod1) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(nod1)))
            ' No asignado
            If Not nod1.Checked Then
                If Not oGruposMN1.Item(scod1) Is Nothing Then
                    ' Antes si
                    oGruposMN1DesSeleccionados.Add DevolverCod(nod1), ""
                End If
                
                Set nod2 = nod1.Child
                
                While Not nod2 Is Nothing
                    scod2 = DevolverCod(nod2) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(nod2)))
                    ' No asignado
                    If Not nod2.Checked Then
            
                        If Not oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                            ' Antes si
                            oGruposMN2DesSeleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                        End If
                        Set nod3 = nod2.Child
                        While Not nod3 Is Nothing
                            scod3 = DevolverCod(nod3) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(nod3)))
                            If Not nod3.Checked Then
                                
                                If Not oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                    ' Antes si
                                    oGruposMN3DesSeleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                End If
                                Set nod4 = nod3.Child
                                While Not nod4 Is Nothing
                                    
                                    scod4 = DevolverCod(nod4) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(DevolverCod(nod4)))
                                                
                                    If Not nod4.Checked Then
                                        If Not oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                        ' Antes si
                                        oGruposMN4DesSeleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                        End If
                                    Else
                                        If oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                            oGruposMN4Seleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                        End If
                                    End If
                                    
                                    Set nod4 = nod4.Next
                                Wend
                            Else
                                ' Si esta checked GMN3
                                If oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                    'No estaba asignado antes.
                                    oGruposMN3Seleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                Else
                                    If oGruposMN3SeleccionadosIniciales.Item(scod1 & scod2 & scod3) Is Nothing Then
                                        oGruposMN3Seleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                    End If
                                End If
                            End If
                            
                            Set nod3 = nod3.Next
                        Wend
                    Else
                        ' Si esta checked GMN2
                        If oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                            'No estaba asignado antes.
                            oGruposMN2Seleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                        Else
                            If oGruposMN2SeleccionadosIniciales.Item(scod1 & scod2) Is Nothing Then
                                oGruposMN2Seleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                            End If
                        End If
                    End If
                    
                    Set nod2 = nod2.Next
                Wend
                
            Else
                ' Si esta checked GMN1
                If oGruposMN1.Item(scod1) Is Nothing Then
                    'No estaba asignado antes.
                    '   caso primer check a todo
                    oGruposMN1Seleccionados.Add DevolverCod(nod1), ""
                Else
                    'Si estaba antes.
                    '   caso: Pod�a ser que estuviera por alguno de nivel inferior. Luego �l no seleccionado.
                    '   caso: El mismo ya estaba seleccionado
                    If oGruposMN1SeleccionadosIniciales.Item(scod1) Is Nothing Then
                        oGruposMN1Seleccionados.Add DevolverCod(nod1), ""
                    End If
                End If
            End If
            Set nod1 = nod1.Next
        Wend
  
  Set oIMaterial = oCompSeleccionado
  
  Set oIMaterial.GruposMN1 = oGruposMN1DesSeleccionados
  Set oIMaterial.GruposMN2 = oGruposMN2DesSeleccionados
  Set oIMaterial.GruposMN3 = oGruposMN3DesSeleccionados
  Set oIMaterial.GruposMN4 = oGruposMN4DesSeleccionados
  
  teserror = oIMaterial.DesAsignarMaterial
    
  If teserror.NumError <> TESnoerror Then
    Screen.MousePointer = vbNormal
    basErrores.TratarError teserror
    Exit Sub
  End If
  
  Set oIMaterial.GruposMN1 = oGruposMN1Seleccionados
  Set oIMaterial.GruposMN2 = oGruposMN2Seleccionados
  Set oIMaterial.GruposMN3 = oGruposMN3Seleccionados
  Set oIMaterial.GruposMN4 = oGruposMN4Seleccionados
  
  teserror = oIMaterial.AsignarMaterial
    
  If teserror.NumError <> TESnoerror Then
    Screen.MousePointer = vbNormal
    basErrores.TratarError teserror
    Exit Sub
  End If
  
  RegistrarAccion accionessummit.ACCMatPorCompMod, "Cod:" & Trim(sdbcCompCod.Text)
  
  fraSelComp.Enabled = True
  tvwEstrMatMod.Visible = False
  tvwEstrMat.Visible = True
  picNavigate.Visible = True
  picEdit.Visible = False
  
    Set oGruposMN1Seleccionados = Nothing
    Set oGruposMN2Seleccionados = Nothing
    Set oGruposMN3Seleccionados = Nothing
    Set oGruposMN4Seleccionados = Nothing
    Set oGruposMN1SeleccionadosIniciales = Nothing
    Set oGruposMN2SeleccionadosIniciales = Nothing
    Set oGruposMN3SeleccionadosIniciales = Nothing
    Set oGruposMN1DesSeleccionados = Nothing
    Set oGruposMN2DesSeleccionados = Nothing
    Set oGruposMN3DesSeleccionados = Nothing
    Set oGruposMN4DesSeleccionados = Nothing
    Set oGrupsMN1Asig = Nothing
    Set oGrupsMN2Asig = Nothing
    Set oGrupsMN3Asig = Nothing
    Set oGrupsMN4Asig = Nothing

  cmdRestaurar_Click
  Screen.MousePointer = vbNormal
  
End Sub

Private Sub cmdCancelar_Click()
     
  fraSelComp.Enabled = True
  tvwEstrMatMod.Visible = False
  tvwEstrMat.Visible = True
  picNavigate.Visible = True
  picEdit.Visible = False
  
  Set oGruposMN1Seleccionados = Nothing
  Set oGruposMN2Seleccionados = Nothing
  Set oGruposMN3Seleccionados = Nothing
  Set oGruposMN4Seleccionados = Nothing
  Set oGruposMN1SeleccionadosIniciales = Nothing
  Set oGruposMN2SeleccionadosIniciales = Nothing
  Set oGruposMN3SeleccionadosIniciales = Nothing
  Set oGruposMN1DesSeleccionados = Nothing
  Set oGruposMN2DesSeleccionados = Nothing
  Set oGruposMN3DesSeleccionados = Nothing
  Set oGruposMN4DesSeleccionados = Nothing
  Set oGrupsMN1Asig = Nothing
  Set oGrupsMN2Asig = Nothing
  Set oGrupsMN3Asig = Nothing
  Set oGrupsMN4Asig = Nothing

  
End Sub

Private Sub cmdlistado_Click()
    
    If sdbcCompCod.Text = "" And sdbcEqpCod.Text = "" Then
        frmLstESTRCOMPMatPorCom.optTodos.Value = True
    Else
        If Not sdbcEqpCod.Text = "" And sdbcCompCod.Text = "" Then
            frmLstESTRCOMPMatPorCom.optEqp.Value = True

            frmLstESTRCOMPMatPorCom.sdbcEqpCod.Text = sdbcEqpCod.Text
            frmLstESTRCOMPMatPorCom.sdbcEqpCod_Validate False
            frmLstESTRCOMPMatPorCom.sdbcCompCod.Text = ""
            frmLstESTRCOMPMatPorCom.sdbcCompCod_Validate False
        Else

            frmLstESTRCOMPMatPorCom.sdbcEqpCod.Text = sdbcEqpCod.Text
            frmLstESTRCOMPMatPorCom.sdbcEqpCod_Validate False
            frmLstESTRCOMPMatPorCom.optComp.Value = True

            frmLstESTRCOMPMatPorCom.sdbcCompCod.Text = sdbcCompCod.Text
            frmLstESTRCOMPMatPorCom.sdbcCompCod_Validate False
        End If
    End If
    
    frmLstESTRCOMPMatPorCom.Show 1
End Sub

Private Sub cmdModificar_Click()
Dim oIBaseDatos As IBaseDatos
    
    LockWindowUpdate Me.hWnd
    
    'Comprobamos que otro usuario no haya eliminado el comprador
    
    Set oIBaseDatos = oCompSeleccionado
    Screen.MousePointer = vbHourglass
      
    If Not oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
'        oMensajes.DatoEliminado "Comprador"
        oMensajes.DatoEliminado sIdiComp
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        sdbcCompCod.Text = ""
        sdbcCompDen.Text = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        tvwEstrMat.Nodes.clear
        Set oCompSeleccionado = Nothing
        LockWindowUpdate 0&
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    fraSelComp.Enabled = False
    picNavigate.Visible = False
    picEdit.Visible = True
    tvwEstrMat.Visible = False
    tvwEstrMatMod.Visible = True
    
    tvwEstrMatMod.Scroll = False
    GenerarEstructuraMatAsignable
    tvwEstrMatMod.Nodes("Raiz").Root.Selected = True
    tvwEstrMatMod.Scroll = True
    
    Set oGruposMN1DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
    
    Set oGruposMN1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
    
    Dim nodx As MSComctlLib.node
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node
        
    Set oGruposMN1SeleccionadosIniciales = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2SeleccionadosIniciales = oFSGSRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3SeleccionadosIniciales = oFSGSRaiz.Generar_CGruposMatNivel3
    
    Set nodx = tvwEstrMatMod.Nodes(1)
    If nodx.Children > 0 Then
        Set nod1 = nodx.Child
        While Not nod1 Is Nothing
            If nod1.Checked Then
                oGruposMN1SeleccionadosIniciales.Add DevolverCod(nod1), ""
            End If
            
            Set nod2 = nod1.Child
            While Not nod2 Is Nothing
                If nod2.Checked Then
                    oGruposMN2SeleccionadosIniciales.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                End If
                
                Set nod3 = nod2.Child
                While Not nod3 Is Nothing
                    If nod3.Checked Then
                        oGruposMN3SeleccionadosIniciales.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                    End If
                    
                    Set nod3 = nod3.Next
                Wend
                
                Set nod2 = nod2.Next
            Wend
            
            Set nod1 = nod1.Next
        Wend
    End If

    LockWindowUpdate 0&
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdRestaurar_Click()
    
    CargarEstructuraMateriales
    
End Sub


Private Sub Form_Load()

iAltura = 5610
iAnchura = 5595

Me.Height = 5610
Me.Width = 5595

If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If

cmdModificar.Enabled = False
cmdRestaurar.Enabled = False

ConfigurarSeguridad
    PonerFieldSeparator Me

Screen.MousePointer = vbHourglass

If bREqp Then
    
    sdbcEqpCod.Visible = False
    sdbcEqpDen.Visible = False
    lblEqp.Visible = True
    lblEqp.caption = basOptimizacion.gCodEqpUsuario & " - " & oUsuarioSummit.comprador.DenEqp
    sdbcEqpCod.Text = basOptimizacion.gCodEqpUsuario
    Set oEqpSeleccionado = Nothing
    Set oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
  
    oEqpSeleccionado.Cod = basOptimizacion.gCodEqpUsuario
    
Else
    Set oComps = oFSGSRaiz.generar_CCompradores
    
    Set oEqps = oFSGSRaiz.Generar_CEquipos
       
End If
    
CargarRecursos

Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Resize()
Dim iX As Integer
Dim iY As Integer


iX = tvwEstrMat.Width + Me.Width - iAnchura
iY = tvwEstrMat.Height + Me.Height - iAltura
If iX > 0 And iY > 0 Then
    tvwEstrMat.Width = iX
    tvwEstrMat.Height = iY
    tvwEstrMatMod.Width = iX
    tvwEstrMatMod.Height = iY
    iAltura = Me.Height
    iAnchura = Me.Width
End If
cmdAceptar.Left = (tvwEstrMat.Width / 2) - (cmdAceptar.Width / 2) - 300
cmdCancelar.Left = (tvwEstrMat.Width / 2) + 300
End Sub

Private Sub Form_Unload(Cancel As Integer)

'Unload frmSELMaterial

    Set oGruposMN1 = Nothing
    Set oGruposMN2 = Nothing
    Set oGruposMN3 = Nothing
    Set oGruposMN4 = Nothing
    Set oEqpSeleccionado = Nothing
    Set oComps = Nothing
    Set oCompSeleccionado = Nothing
    Set oGruposMN1Seleccionados = Nothing
    Set oGruposMN2Seleccionados = Nothing
    Set oGruposMN3Seleccionados = Nothing
    Set oGruposMN4Seleccionados = Nothing
    Set oGruposMN1SeleccionadosIniciales = Nothing
    Set oGruposMN2SeleccionadosIniciales = Nothing
    Set oGruposMN3SeleccionadosIniciales = Nothing
    Set oGruposMN1DesSeleccionados = Nothing
    Set oGruposMN2DesSeleccionados = Nothing
    Set oGruposMN3DesSeleccionados = Nothing
    Set oGruposMN4DesSeleccionados = Nothing
    Set oGrupsMN1Asig = Nothing
    Set oGrupsMN2Asig = Nothing
    Set oGrupsMN3Asig = Nothing
    Set oGrupsMN4Asig = Nothing
    Me.Visible = False

End Sub

Private Sub GenerarEstructuraDeMateriales()

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Dim oGMN1 As CGrupoMatNivel1
Dim nodx As MSComctlLib.node
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim oIMaterialAsignado As IMaterialAsignado

If oCompSeleccionado Is Nothing Then Exit Sub

tvwEstrMat.Nodes.clear
Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdiMat, "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True

On Error Resume Next

Screen.MousePointer = vbHourglass

Set oIMaterialAsignado = oCompSeleccionado

With oIMaterialAsignado

'************** GruposMN1 ***********
Set oGruposMN1 = .DevolverGruposMN1Asignados(, , , , False, True)
    
        For Each oGMN1 In oGruposMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
            If oGMN1.GruposMatNivel2 Is Nothing Then
                nodx.Image = "GMN1A"
            End If
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod) ' Con la X1X indicamos que el GMN1 ha sido asignado.
            
        Next
        
    If gParametrosGenerales.giNEM <= 1 Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    
    '************** GruposMN2 ***********
    Set oGruposMN2 = .DevolverGruposMN2Asignados(, , , , False, True)
        
        For Each oGMN2 In oGruposMN2
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
            Set nodx = tvwEstrMat.Nodes("GMN1" & scod1)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN2.GMN1Cod) & " - " & oGMN2.GMN1Den, "GMN1")
            End If
            nodx.Tag = "GMN1" & CStr(oGMN2.GMN1Cod)
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
            If oGMN2.GruposMatNivel3 Is Nothing Then
                nodx.Image = "GMN2A"
            End If
            nodx.Tag = "GMN2" & CStr(oGMN2.Cod) ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
            If nodx.Parent.Image = "GMN1" Then
                nodx.Parent.Expanded = True
            End If
        Next
    
    
    If gParametrosGenerales.giNEM <= 2 Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
        
    '************** GruposMN3 ***********
    Set oGruposMN3 = oIMaterialAsignado.DevolverGruposMN3Asignados(, , , , False, True)
        
    If Not oGruposMN3 Is Nothing Then
        For Each oGMN3 In oGruposMN3
            
            scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
            scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
            Set nodx = tvwEstrMat.Nodes("GMN1" & scod1)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN3.GMN1Cod) & " - " & oGMN3.GMN1Den, "GMN1")
            End If
            nodx.Tag = "GMN1" & CStr(oGMN3.GMN1Cod)  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes("GMN2" & scod2)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN3.GMN2Cod) & " - " & oGMN3.GMN2Den, "GMN2")
            End If
            nodx.Tag = "GMN2" & CStr(oGMN3.GMN2Cod)   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
            If oGMN3.GruposMatNivel4 Is Nothing Then
                nodx.Image = "GMN3A"
            End If
            nodx.Tag = "GMN3" & CStr(oGMN3.Cod) ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
            If nodx.Parent.Image = "GMN2" Then
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        Next
    
    End If
    
    If gParametrosGenerales.giNEM <= 3 Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    '************** GruposMN4 ***********
    Set oGruposMN4 = oIMaterialAsignado.DevolverGruposMN4Asignados(, , , , False, True)
    
    If Not oGruposMN4 Is Nothing Then
        
        For Each oGMN4 In oGruposMN4
            scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
            scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
            scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
            scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
            Set nodx = tvwEstrMat.Nodes("GMN1" & scod1)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN4.GMN1Cod) & " - " & oGMN4.GMN1Den, "GMN1")
            End If
            nodx.Tag = "GMN1" & CStr(oGMN4.GMN1Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes("GMN2" & scod2)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN4.GMN2Cod) & " - " & oGMN4.GMN2Den, "GMN2")
            End If
            nodx.Tag = "GMN2" & CStr(oGMN4.GMN2Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes("GMN3" & scod3)
            If nodx Is Nothing Then
                Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN4.GMN3Cod) & " - " & oGMN4.GMN3Den, "GMN3")
            End If
            nodx.Tag = "GMN3" & CStr(oGMN4.GMN3Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4A")
            nodx.Tag = "GMN4" & CStr(oGMN4.Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            If (nodx.Parent.Image) = "GMN3" Then
                nodx.Parent.Parent.Parent.Expanded = True
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        Next
    
    End If
 
 End With
 
 Screen.MousePointer = vbNormal
 
End Sub

''' <summary>Configuraci�n de la seguridad de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorCOMPModificar)) Is Nothing) Then
        bModif = True
        cmdModificar.Visible = True
    Else
        cmdModificar.Visible = False
        cmdListado.Left = cmdRestaurar.Left
        cmdRestaurar.Left = cmdModificar.Left
        
    End If
    
    If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorCOMPRestEquipo)) Is Nothing) Then
        bREqp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorCOMPRestMatComp)) Is Nothing) Then
        bRMatAsig = True
    End If

    cmdListado.Visible = True
End Sub


Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

Select Case Left(node.Tag, 4)

Case "GMN1"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "GMN2"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "GMN3"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
Case "GMN4"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    
End Select

End Function
Private Sub ConfigurarInterfazSeguridad(ByVal nodx As MSComctlLib.node)

End Sub


Private Sub sdbcCompCod_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcCompDen.Text = ""
        tvwEstrMat.Nodes.clear
        RespetarCombo = False
        
        CargarComboDesdeComp = True
        Set oCompSeleccionado = Nothing
            
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        
    End If
    
End Sub


Private Sub sdbcCompCod_Click()
    If Not sdbcCompCod.DroppedDown Then
        sdbcCompCod = ""
        sdbcCompDen = ""
    End If
End Sub

Private Sub sdbcCompCod_CloseUp()
    Dim sCod As String
    
    If sdbcCompCod.Value = "..." Then
        sdbcCompCod.Text = ""
        Exit Sub
    End If
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    If sdbcCompCod.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcCompDen.Text = sdbcCompCod.Columns(1).Text
    sdbcCompCod.Text = sdbcCompCod.Columns(0).Text
    RespetarCombo = False
    
    sCod = oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oEqpSeleccionado.Cod))
    
    Set oCompSeleccionado = oComps.Item(sCod & sdbcCompCod.Columns(0).Text)
        
    CargarComboDesdeComp = False
    
   ' DoEvents
   
    If sdbcCompCod.Value <> "" Then
        CompradorSeleccionado
    End If
    
End Sub

Private Sub sdbcCompCod_DropDown()
Dim EqpCod As String

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    Set oComps = Nothing
    
    Screen.MousePointer = vbHourglass

    Set oComps = oFSGSRaiz.generar_CCompradores
    
    If CargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcCompCod.Text), , , False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, False, False
    End If
    
    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
    'Set oEqpSeleccionado.Compradores = Nothing
    
    If basOptimizacion.gTipoDeUsuario = comprador Then
        
        'Si el usuario es un comprador cargamos todos
        ' los compradores menos a el.
        
''''        If bREqp Then
''''
''''            EqpCod = basOptimizacion.gCodEqpUsuario
''''
''''            For i = 0 To UBound(Codigos.Cod) - 1
''''                If Codigos.Cod(i) <> basOptimizacion.gCodCompradorUsuario Or EqpCod <> basOptimizacion.gCodEqpUsuario Then
''''                    sdbcCompCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
''''                End If
''''            Next
''''
''''        Else
            
            EqpCod = sdbcEqpCod.Text
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
            Next
        
''''        End If
    
    Else
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcCompCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
    
    End If
    
    
    If CargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompCod.AddItem "..."
    End If

    sdbcCompCod.SelStart = 0
    sdbcCompCod.SelLength = Len(sdbcCompCod.Text)
    sdbcCompCod.Refresh

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcCompCod_InitColumnProps()
    sdbcCompCod.DataFieldList = "Column 0"
    sdbcCompCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCompCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCompCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCompCod.Rows - 1
            bm = sdbcCompCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCompCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcCompCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcCompCod_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    
    If sdbcCompCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el comprador
   
    Screen.MousePointer = vbHourglass
    
    oEqpSeleccionado.CargarTodosLosCompradores sdbcCompCod.Text, , , True, , , False
    Set oCompradores = oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompCod.Text = ""
    Else
        RespetarCombo = True
        sdbcCompDen.Text = oCompradores.Item(1).Apel & ", " & oCompradores.Item(1).nombre
        
        sdbcCompCod.Columns(0).Value = sdbcCompCod.Text
        sdbcCompCod.Columns(1).Value = sdbcCompDen.Text
        
        RespetarCombo = False
        Set oCompSeleccionado = oCompradores.Item(1)
        CargarComboDesdeComp = False
        CompradorSeleccionado
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcCompDen_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcCompCod.Text = ""
        tvwEstrMat.Nodes.clear
        RespetarCombo = False
        
        CargarComboDesdeComp = True
        Set oCompSeleccionado = Nothing
            
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        
    End If
    
End Sub


Private Sub sdbcCompDen_Click()
    If Not sdbcCompDen.DroppedDown Then
        sdbcCompCod = ""
        sdbcCompDen = ""
    End If
End Sub

Private Sub sdbcCompDen_CloseUp()
    Dim sCod As String
    
    If sdbcCompDen.Value = "..." Then
        sdbcCompDen.Text = ""
        Exit Sub
    End If
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    If sdbcCompDen.Value = "" Then Exit Sub
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcCompDen.Text = sdbcCompDen.Columns(0).Text
    sdbcCompCod.Text = sdbcCompDen.Columns(1).Text
    RespetarCombo = False
    
    sCod = oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oEqpSeleccionado.Cod))
    
    Set oCompSeleccionado = oComps.Item(sCod & sdbcCompDen.Columns(1).Text)
        
    CargarComboDesdeComp = False
    
    'DoEvents
    
    If sdbcCompDen.Value <> "" Then
        CompradorSeleccionado
    End If
End Sub

Private Sub sdbcCompDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Dim EqpCod As String
    
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
        
    Set oComps = Nothing
    Set oComps = oFSGSRaiz.generar_CCompradores
  
    Screen.MousePointer = vbHourglass
    
    If CargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(Apellidos(sdbcEqpDen.Text)), True, False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, True, False
    End If
    
    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
    Set oEqpSeleccionado.Compradores = Nothing
    
    If basOptimizacion.gTipoDeUsuario = comprador Then
        
        'Si el usuario es un comprador cargamos todos
        ' los compradores menos a el.
        
''''        If bREqp Then
''''
''''            EqpCod = basOptimizacion.gCodEqpUsuario
''''
''''            For i = 0 To UBound(Codigos.Cod) - 1
''''                If Codigos.Cod(i) <> basOptimizacion.gCodCompradorUsuario Or EqpCod <> basOptimizacion.gCodEqpUsuario Then
''''                    sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
''''                End If
''''            Next
''''
''''        Else
            
            EqpCod = sdbcEqpCod.Text
            
            For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
            Next
        
''''        End If
    
    Else
        
        For i = 0 To UBound(Codigos.Cod) - 1
                sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
    
    End If
    
    If CargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompDen.AddItem "..."
    End If

    sdbcCompDen.SelStart = 0
    sdbcCompDen.SelLength = Len(sdbcCompDen.Text)
    sdbcCompDen.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcCompDen_InitColumnProps()
    sdbcCompDen.DataFieldList = "Column 0"
    sdbcCompDen.DataFieldToDisplay = "Column 0"
End Sub



Private Sub sdbcCompDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCompDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCompDen.Rows - 1
            bm = sdbcCompDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCompDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcCompDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcCompDen_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    If sdbcCompDen.Text = "" Then Exit Sub
    If sdbcEqpCod.Text = "" Then Exit Sub
    If oEqpSeleccionado Is Nothing Then Exit Sub
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    
    oEqpSeleccionado.CargarTodosLosCompradores , nombre(sdbcCompDen.Text), Apellidos(sdbcCompDen.Text), True, False, False, False
    Set oCompradores = oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompDen.Text = ""
    Else
        RespetarCombo = True
        sdbcCompCod.Text = oCompradores.Item(1).Cod
        
        sdbcCompDen.Columns(0).Value = sdbcCompDen.Text
        sdbcCompDen.Columns(1).Value = sdbcCompCod.Text
        
        RespetarCombo = False
        Set oCompSeleccionado = oCompradores.Item(1)
        CargarComboDesdeComp = False
        CompradorSeleccionado
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpCod_Change()
     
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpDen.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        tvwEstrMat.Nodes.clear
        RespetarCombo = False
        
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
            
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        
    End If
    
End Sub

Private Sub sdbcEqpCod_Click()
    
    If Not sdbcEqpCod.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpCod.Rows - 1
            bm = sdbcEqpCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcEqpCod_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
   
    oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpCod.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        tvwEstrMat.Nodes.clear
    Else
        RespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        sdbcEqpCod.Columns(0).Value = sdbcEqpCod.Text
        sdbcEqpCod.Columns(1).Value = sdbcEqpDen.Text
        
        RespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
    End If
    
    Set oEquipos = Nothing
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpDen_Change()
      
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpCod.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        tvwEstrMat.Nodes.clear
        RespetarCombo = False
        
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
            
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        
    End If
          
End Sub

Private Sub sdbcEqpDen_Click()
    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpDen_CloseUp()
    
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpDen.Value = "" Then Exit Sub
    
    sdbcCompCod = ""
    sdbcCompDen = ""
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
    tvwEstrMat.Nodes.clear
    
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass

    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(1).Text)
    CargarComboDesdeEqp = False
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    Screen.MousePointer = vbHourglass
    
    sdbcEqpDen.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, True, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpDen.Rows - 1
            bm = sdbcEqpDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass

    oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        tvwEstrMat.Nodes.clear
    Else
        RespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        
        sdbcEqpDen.Columns(0).Value = sdbcEqpDen.Text
        sdbcEqpDen.Columns(1).Value = sdbcEqpCod.Text
        
        RespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
    End If
    
    Set oEquipos = Nothing

    Screen.MousePointer = vbNormal

End Sub

Private Sub tvwEstrMat_NodeClick(ByVal node As MSComctlLib.node)
    
    ConfigurarInterfazSeguridad node
    
End Sub
Private Sub sdbcEqpCod_CloseUp()
    
    If sdbcEqpCod.Value = "..." Then
        sdbcEqpCod.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpCod.Value = "" Then Exit Sub
    
    sdbcCompCod = ""
    sdbcCompDen = ""
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
    tvwEstrMat.Nodes.clear
        
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    RespetarCombo = False
    
    
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(0).Text)
        
    CargarComboDesdeEqp = False

End Sub
Private Sub sdbcEqpCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Set oEqps = Nothing
    
    Screen.MousePointer = vbHourglass
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    sdbcEqpCod.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, False, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh

    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcEqpCod_InitColumnProps()

    sdbcEqpCod.DataFieldList = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub GenerarEstructuraMatAsignable()

Dim oIMaterialComprador As IMaterialAsignado
Dim oGrupsMN1 As CGruposMatNivel1
Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Dim nodx As MSComctlLib.node

On Error GoTo Error

tvwEstrMatMod.Nodes.clear

    Set nodx = tvwEstrMatMod.Nodes.Add(, , "Raiz", sIdiMat, "Raiz")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
Screen.MousePointer = vbHourglass
    
    
If Not bRMatAsig Then

    Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1

    oGrupsMN1.GenerarEstructuraMateriales False
    
    Select Case gParametrosGenerales.giNEM
    
    Case 1
            For Each oGMN1 In oGrupsMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Den, "GMN1")
                nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            Next
            
    Case 2
        For Each oGMN1 In oGrupsMN1
            
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            
            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
            Next
        Next
        
        
    Case 3
        
        For Each oGMN1 In oGrupsMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & CStr(oGMN1.Cod), oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            
            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
                For Each oGMN3 In oGMN2.GruposMatNivel3
                    scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                    Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Den, "GMN3")
                    nodx.Tag = "GMN3" & CStr(oGMN3.Cod)
                Next
            Next
        Next
        
       
    Case 4
        For Each oGMN1 In oGrupsMN1
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
            
            For Each oGMN2 In oGMN1.GruposMatNivel2
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
                    
                    For Each oGMN3 In oGMN2.GruposMatNivel3
                        
                        scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                        Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
                        nodx.Tag = "GMN3" & CStr(oGMN3.Cod)
                                
                                For Each oGMN4 In oGMN3.GruposMatNivel4
                                    scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                    Set nodx = tvwEstrMatMod.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4")
                                    nodx.Tag = "GMN4" & CStr(oGMN4.Cod)
                                Next
                    Next
            Next
        Next
        
    End Select
        
        Set oGrupsMN1 = Nothing
         ' Ahora de esta estructura debemos marcar los materiales que ya estan seleccionados
        
        For Each nodx In tvwEstrMat.Nodes
            If Right(nodx.Image, 1) = "A" Then
                tvwEstrMatMod.Nodes(nodx.key).Checked = True
                If nodx.Visible Then
                    tvwEstrMatMod.Nodes(nodx.key).EnsureVisible
                End If
                DoEvents
            End If
        Next
    
Else
    ' Carga del material asignado al comprador
    
    Set oIMaterialComprador = oUsuarioSummit.comprador
   
   
    With oIMaterialComprador


        '************** GruposMN1 ***********
        Set oGrupsMN1Asig = .DevolverGruposMN1Asignados(, , , , False)
            
        For Each oGMN1 In oGrupsMN1Asig
            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod) ' Con la X1X indicamos que el GMN1 ha sido asignado.
            
        Next
        
        If gParametrosGenerales.giNEM <= 1 Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    
    
        '************** GruposMN2 ***********
        Set oGrupsMN2Asig = .DevolverGruposMN2Asignados(, , , , False)
    
        For Each oGMN2 In oGrupsMN2Asig
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
            Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN2.GMN1Cod) & " - " & oGMN2.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN2.GMN1Cod)
            Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
            nodx.Tag = "GMN2" & CStr(oGMN2.Cod) ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
            
        Next
    
    
        If gParametrosGenerales.giNEM <= 2 Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
    
        '************** GruposMN3 ***********
        Set oGrupsMN3Asig = oIMaterialComprador.DevolverGruposMN3Asignados(, , , , False)
        
        If Not oGrupsMN3Asig Is Nothing Then
            For Each oGMN3 In oGrupsMN3Asig
                scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
                scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN3.GMN1Cod) & " - " & oGMN3.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & CStr(oGMN3.GMN1Cod)  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN3.GMN2Cod) & " - " & oGMN3.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN3.GMN2Cod)   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
                nodx.Tag = "GMN3" & CStr(oGMN3.Cod) ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
                
            Next
    
        End If
    
        If gParametrosGenerales.giNEM <= 3 Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    
        '************** GruposMN4 ***********
        Set oGrupsMN4Asig = oIMaterialComprador.DevolverGruposMN4Asignados(, , , , False)
        
        If Not oGrupsMN4Asig Is Nothing Then
            
            For Each oGMN4 In oGrupsMN4Asig
                
                scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                
                Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN4.GMN1Cod) & " - " & oGMN4.GMN1Den, "GMN1")
                nodx.Tag = "GMN1" & CStr(oGMN4.GMN1Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN4.GMN2Cod) & " - " & oGMN4.GMN2Den, "GMN2")
                nodx.Tag = "GMN2" & CStr(oGMN4.GMN2Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN4.GMN3Cod) & " - " & oGMN4.GMN3Den, "GMN3")
                nodx.Tag = "GMN3" & CStr(oGMN4.GMN3Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4")
                nodx.Tag = "GMN4" & CStr(oGMN4.Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                
            Next
        
        End If
     
     End With
    
     ' Ahora de esta estructura debemos marcar los materiales que ya estan seleccionados
        
    For Each nodx In tvwEstrMatMod.Nodes
        If Right(tvwEstrMat.Nodes(nodx.key).Image, 1) = "A" Then
            nodx.Checked = True
            If tvwEstrMat.Nodes(nodx.key).Visible Then
                nodx.EnsureVisible
            End If
            DoEvents
        End If
    Next

End If

Screen.MousePointer = vbNormal

Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
    
End Sub



Private Sub tvwEstrMatMod_KeyUp(KeyCode As Integer, Shift As Integer)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

If bMouseDown Then
    
    bMouseDown = False
    
    If SelNode.Tag = "Raiz" Then
        SelNode.Checked = False
        DoEvents
        Exit Sub
    End If
                    
    If bRMatAsig Then
        Select Case Left(SelNode.Tag, 4)
            Case "GMN1"
                                
                scod1 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode)))
                If oGrupsMN1Asig.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                End If
                            
            Case "GMN2"
                                
                scod1 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent)))
                scod2 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode)))
                If oGrupsMN2Asig.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN1Asig.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    DoEvents
                    Exit Sub
                End If
                            
            Case "GMN3"
                                    
                scod1 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent)))
                scod2 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent)))
                scod3 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode)))
                If oGrupsMN3Asig.Item(scod1 & scod2 & scod3) Is Nothing _
                    And oGrupsMN2Asig.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN1Asig.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    DoEvents
                    Exit Sub
                End If
                
            Case "GMN4"
                                    
                scod1 = DevolverCod(SelNode.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent.Parent)))
                scod2 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent.Parent)))
                scod3 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode.Parent)))
                scod4 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(DevolverCod(SelNode)))
                If oGrupsMN4Asig.Item(scod1 & scod2 & scod3 & scod4) Is Nothing _
                    And oGrupsMN3Asig.Item(scod1 & scod2 & scod3) Is Nothing _
                    And oGrupsMN2Asig.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN1Asig.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    DoEvents
                    Exit Sub
                End If
                
            End Select
                        
        End If
        
        If SelNode.Checked Then
            MarcarTodosLosHijos SelNode
                           
        Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
                        
        End If
End If

    
End Sub


Private Sub tvwEstrMatMod_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.node
Dim tvhti As TVHITTESTINFO

                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrMatMod.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrMatMod.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento
                             
                bMouseDown = True
                Set SelNode = nod
                             
                End If
            End If
        End If
    End If
    
End Sub

Private Sub tvwEstrMatMod_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
      
If bMouseDown Then
    
    bMouseDown = False
    
    If SelNode.Tag = "Raiz" Then
        SelNode.Checked = False
        DoEvents
        Exit Sub
    End If
                    
    If bRMatAsig Then
        Select Case Left(SelNode.Tag, 4)
            Case "GMN1"
                                
                scod1 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode)))
                If oGrupsMN1Asig.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    Exit Sub
                End If
                            
            Case "GMN2"
                                
                scod1 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent)))
                scod2 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode)))
                If oGrupsMN2Asig.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN1Asig.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    DoEvents
                    Exit Sub
                End If
                            
            Case "GMN3"
                                    
                scod1 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent)))
                scod2 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent)))
                scod3 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode)))
                If oGrupsMN3Asig.Item(scod1 & scod2 & scod3) Is Nothing _
                    And oGrupsMN2Asig.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN1Asig.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    DoEvents
                    Exit Sub
                End If
                
            Case "GMN4"
                                    
                scod1 = DevolverCod(SelNode.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent.Parent)))
                scod2 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent.Parent)))
                scod3 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode.Parent)))
                scod4 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(DevolverCod(SelNode)))
                If oGrupsMN4Asig.Item(scod1 & scod2 & scod3 & scod4) Is Nothing _
                    And oGrupsMN3Asig.Item(scod1 & scod2 & scod3) Is Nothing _
                    And oGrupsMN2Asig.Item(scod1 & scod2) Is Nothing _
                    And oGrupsMN1Asig.Item(scod1) Is Nothing Then
                    SelNode.Checked = Not SelNode.Checked
                    oMensajes.PermisoDenegadoDesAsignarMat
                    DoEvents
                    Exit Sub
                End If
                
            End Select
                        
        End If
        
        If SelNode.Checked Then
            MarcarTodosLosHijos SelNode
                           
        Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
                        
        End If
End If

End Sub

Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.node)

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = True
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = True
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = True
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = True
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub
Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.node)

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = False
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = False
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = False
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = False
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = False
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub

Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.node)
    
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        nodx.Checked = False
        Set nodx = nodx.Parent
    Wend
    DoEvents
End Sub

Private Sub CompradorSeleccionado()
    
    If Not oCompSeleccionado Is Nothing Then
        oCompSeleccionado.codEqp = sdbcEqpCod.Text
       
        CargarEstructuraMateriales
        cmdModificar.Enabled = True
        cmdRestaurar.Enabled = True
    Else
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
    End If
    
End Sub

Private Sub tvwEstrMatMod_NodeCheck(ByVal node As MSComctlLib.node)

If bMouseDown Then
    Exit Sub
End If

bMouseDown = True
Set SelNode = node

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTR_COMPMATPORCOMP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        frmESTRCOMPMatPorCom.caption = Ador(0).Value
        Ador.MoveNext
        lblEqpCod.caption = Ador(0).Value
        Ador.MoveNext
        lblComp.caption = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sdbcEqpCod.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcEqpCod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcEqpDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcEqpDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcCompCod.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcCompCod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcCompDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcCompDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sIdiComp = Ador(0).Value
        Ador.MoveNext
        sIdiMat = Ador(0).Value
        Ador.MoveNext

    End If

    Set Ador = Nothing



End Sub
