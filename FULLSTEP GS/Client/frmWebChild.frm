VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Begin VB.Form frmWebChild 
   Caption         =   "Form1"
   ClientHeight    =   3030
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   4560
   Icon            =   "frmWebChild.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3030
   ScaleWidth      =   4560
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   3480
      Top             =   720
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   415
      Index           =   0
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   420
      TabIndex        =   0
      Top             =   0
      Width           =   415
      Begin VB.PictureBox Picture1 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   415
         Index           =   1
         Left            =   0
         Picture         =   "frmWebChild.frx":014A
         ScaleHeight     =   420
         ScaleWidth      =   420
         TabIndex        =   1
         Top             =   0
         Width           =   415
      End
   End
   Begin SHDocVwCtl.WebBrowser webBrowser1 
      Height          =   4665
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   6195
      ExtentX         =   10927
      ExtentY         =   8229
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
End
Attribute VB_Name = "frmWebChild"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Public g_sRuta As String

Private Sub Form_Load()
    Picture1(0).Width = Me.Width
    Picture1(0).Height = Me.Height
    Picture1(1).Top = (Me.Height - Picture1(1).Height) / 2
    Picture1(1).Left = (Me.Width - Picture1(1).Width) / 2
    Picture1(0).Visible = True
    Picture1(1).Visible = True
    Timer1.Enabled = True
    With webBrowser1
        .Width = Me.Width
        .Height = Me.Height
        .Navigate2 g_sRuta, 4   'que no coja de la cache
    End With
End Sub
Private Sub Form_Resize()
    'Redimensiona el formulario
    If Me.Width < 600 Then Exit Sub
    If Me.Height < 5000 Then Exit Sub
    
    Picture1(0).Width = Me.Width
    Picture1(0).Height = Me.Height
    Picture1(1).Top = (Me.Height - Picture1(1).Height) / 2
    Picture1(1).Left = (Me.Width - Picture1(1).Width) / 2
    
    'redimensiona el width del visor
    webBrowser1.Width = Me.Width - 200
    webBrowser1.Height = Me.Height - 600
    
End Sub
Private Sub Timer1_Timer()
    If webBrowser1.ReadyState = READYSTATE_COMPLETE Then
        Picture1(0).Visible = False
        Picture1(1).Visible = False
        Timer1.Enabled = False
    End If
End Sub
