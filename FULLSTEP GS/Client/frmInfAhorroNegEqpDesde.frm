VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroNegEqpDesde 
   Caption         =   "Informe de ahorros por equipos de compra negociadores en un per�odo"
   ClientHeight    =   5820
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10695
   Icon            =   "frmInfAhorroNegEqpDesde.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5820
   ScaleWidth      =   10695
   Begin VB.PictureBox picLegend 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   9090
      Picture         =   "frmInfAhorroNegEqpDesde.frx":0CB2
      ScaleHeight     =   735
      ScaleWidth      =   1635
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.PictureBox picLegend2 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   9090
      Picture         =   "frmInfAhorroNegEqpDesde.frx":49B4
      ScaleHeight     =   615
      ScaleWidth      =   1635
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   60
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.Frame fraEqp 
      Height          =   555
      Left            =   0
      TabIndex        =   10
      Top             =   690
      Width           =   10695
      Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
         Height          =   285
         Left            =   660
         TabIndex        =   5
         Top             =   180
         Width           =   1215
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1296
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5345
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2143
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 3"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
         Height          =   285
         Left            =   1920
         TabIndex        =   6
         Top             =   180
         Width           =   3795
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1640
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6694
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblEqpCod 
         Caption         =   "Equipo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   60
         TabIndex        =   12
         Top             =   240
         Width           =   555
      End
      Begin VB.Label lblEqp 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   660
         TabIndex        =   11
         Top             =   180
         Visible         =   0   'False
         Width           =   5010
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   0
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   5490
      Width           =   10695
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegEqpDesde.frx":7B52
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegEqpDesde.frx":7B6E
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegEqpDesde.frx":7B8A
      DividerType     =   1
      DividerStyle    =   2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   7
      Columns(0).Width=   2275
      Columns(0).Caption=   "CAPTION"
      Columns(0).Name =   "CAPTION"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "PRESCAPTION"
      Columns(1).Name =   "PRESCAPTION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "PRES"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,#"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "ADJCAPTION"
      Columns(3).Name =   "ADJCAPTION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "ADJ"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,#"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2090
      Columns(5).Caption=   "AHOCAPTION"
      Columns(5).Name =   "AHOCAPTION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "AHO"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "#,#"
      Columns(6).FieldLen=   256
      _ExtentX        =   18865
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   4155
      Left            =   0
      TabIndex        =   7
      Top             =   1320
      Width           =   10695
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   7
      stylesets.count =   2
      stylesets(0).Name=   "Red"
      stylesets(0).BackColor=   4744445
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegEqpDesde.frx":7BA6
      stylesets(1).Name=   "Green"
      stylesets(1).BackColor=   10409634
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegEqpDesde.frx":7BC2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Fecha"
      Columns(0).Name =   "FEC"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   3200
      Columns(1).Caption=   "Fecha"
      Columns(1).Name =   "FECHACORTA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16776960
      Columns(2).Width=   3200
      Columns(2).Caption=   "Referencia"
      Columns(2).Name =   "REF"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3175
      Columns(3).Caption=   "Presupuesto"
      Columns(3).Name =   "PRES"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   15400959
      Columns(4).Width=   3016
      Columns(4).Caption=   "Adjudicado"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   15400959
      Columns(5).Width=   3651
      Columns(5).Caption=   "Ahorro"
      Columns(5).Name =   "AHO"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "Standard"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1852
      Columns(6).Caption=   "%"
      Columns(6).Name =   "PORCEN"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "0.0#\%"
      Columns(6).FieldLen=   256
      _ExtentX        =   18865
      _ExtentY        =   7329
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   4215
      Left            =   0
      OleObjectBlob   =   "frmInfAhorroNegEqpDesde.frx":7BDE
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1320
      Width           =   9855
   End
   Begin VB.Frame fraSel 
      Height          =   675
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   9090
      Begin VB.PictureBox picTipoGrafico 
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   4290
         ScaleHeight     =   435
         ScaleWidth      =   3540
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   150
         Visible         =   0   'False
         Width           =   3540
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
            Height          =   285
            Left            =   720
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   90
            Width           =   1815
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Row.Count       =   5
            Row(0)          =   "Barras 2D"
            Row(1)          =   "Barras 3D"
            Row(2)          =   "Lineas 2D"
            Row(3)          =   "Lineas 3D"
            Row(4)          =   "Tarta"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).Caption=   "TIPO"
            Columns(0).Name =   "TIPO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   3201
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.CommandButton cmdActualizar 
         Height          =   285
         Left            =   8310
         Picture         =   "frmInfAhorroNegEqpDesde.frx":9604
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdCalFecApeDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         Picture         =   "frmInfAhorroNegEqpDesde.frx":968F
         Style           =   1  'Graphical
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecDesde 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   660
         TabIndex        =   0
         Top             =   240
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalFecApeHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3780
         Picture         =   "frmInfAhorroNegEqpDesde.frx":9C19
         Style           =   1  'Graphical
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecHasta 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2640
         TabIndex        =   1
         Top             =   240
         Width           =   1110
      End
      Begin VB.OptionButton optDir 
         Caption         =   "Adj. directa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   390
         Width           =   1335
      End
      Begin VB.OptionButton optTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5685
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   390
         Width           =   945
      End
      Begin VB.OptionButton optReu 
         Caption         =   "Adj. en reuni�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   150
         Value           =   -1  'True
         Width           =   2340
      End
      Begin VB.CommandButton cmdGrid 
         Height          =   285
         Left            =   7920
         Picture         =   "frmInfAhorroNegEqpDesde.frx":A1A3
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   240
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.CommandButton cmdGrafico 
         Height          =   285
         Left            =   7920
         Picture         =   "frmInfAhorroNegEqpDesde.frx":A2ED
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         Height          =   285
         Left            =   8715
         Picture         =   "frmInfAhorroNegEqpDesde.frx":A62F
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMon 
         Height          =   285
         Left            =   6765
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   240
         Width           =   1035
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1164
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1879
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "EQUIV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "SSDBCombo1"
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2160
         TabIndex        =   24
         Top             =   300
         Width           =   450
      End
      Begin VB.Label lblFecDesde 
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   300
         Width           =   555
      End
   End
End
Attribute VB_Name = "frmInfAhorroNegEqpDesde"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ofrmLstAhorroNeg As frmLstINFAhorrosNeg
'Variables para func. combos
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

Private oMonedas As CMonedas
Private dequivalencia As Double
Private oMon As CMoneda

Private ADORs As Ador.Recordset

Private bREqp As Boolean
Private oEqps As CEquipos

Private sIdiTipoGrafico(5) As String
Private sIdiFecha As String
Private sIdiMonCent As String
Private sIdiTotal As String
Private sIdiPresup As String
Private sIdiAdj As String
Private sIdiAhor As String
Private sIdiEqp As String
Private sIdiDetFec As String


Private Sub cmdActualizar_Click()
            
    If txtFecDesde = "" Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then txtFecDesde.SetFocus
        Exit Sub
    End If
    
    If Not IsDate(txtFecDesde) Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then txtFecDesde.SetFocus
        Exit Sub
    End If
    
    If txtFecHasta <> "" Then
        If Not IsDate(txtFecHasta) Then
            oMensajes.NoValido sIdiFecha
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
        If CDate(txtFecDesde) > CDate(txtFecHasta) Then
            oMensajes.FechaDesdeMayorFechaHasta
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    End If
    
    Set ADORs = Nothing
    
    Screen.MousePointer = vbHourglass

    If bREqp Then
        If txtFecHasta = "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoEqpNegDesdeHasta(CDate(txtFecDesde), , optReu, optDir, , , , , basOptimizacion.gCodEqpUsuario)
        Else
            Set ADORs = oGestorInformes.AhorroNegociadoEqpNegDesdeHasta(CDate(txtFecDesde), CDate(txtFecHasta), optReu, optDir, , , , , basOptimizacion.gCodEqpUsuario)
        End If
    Else
        If txtFecHasta = "" Then
            Set ADORs = oGestorInformes.AhorroNegociadoEqpNegDesdeHasta(CDate(txtFecDesde), , optReu, optDir, , , , , sdbcEqpCod)
        Else
            Set ADORs = oGestorInformes.AhorroNegociadoEqpNegDesdeHasta(CDate(txtFecDesde), CDate(txtFecHasta), optReu, optDir, , , , , sdbcEqpCod)
        End If
    End If
    
    BorrarDatosTotales
    
    Screen.MousePointer = vbNormal
    
    If ADORs Is Nothing Then Exit Sub
    
    CargarGrid
    
    If cmdGrid.Visible = True Then
        MSChart1.Visible = True
        MostrarGrafico sdbcTipoGrafico.Value
    End If

        
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub cmdGrafico_Click()
            
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
'        sdbcTipoGrafico = "Barras 3D"
        sdbcTipoGrafico = sIdiTipoGrafico(2)
        MostrarGrafico sIdiTipoGrafico(2)
        picTipoGrafico.Visible = True
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        sdbcMon.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdGrid_Click()
        
        picTipoGrafico.Visible = False
        picLegend.Visible = False
        picLegend2.Visible = False
        
        cmdGrafico.Visible = True
        cmdGrid.Visible = False
        sdbgRes.Visible = True
        sdbcMon.Visible = True
        MSChart1.Visible = False
    
End Sub

Private Sub cmdImprimir_Click()
    Set ofrmLstAhorroNeg = New frmLstINFAhorrosNeg
    ofrmLstAhorroNeg.sOrigen = "frmInfAhorroNegEqpDesde"
  
    ofrmLstAhorroNeg.WindowState = vbNormal
    
    If txtFecDesde <> "" Then
        ofrmLstAhorroNeg.txtFecDesde = txtFecDesde.Text
    End If
    If txtFecHasta <> "" Then
        ofrmLstAhorroNeg.txtFecHasta = txtFecHasta.Text
    End If
    If sdbcEqpCod <> "" Then
        ofrmLstAhorroNeg.sdbcEqpCod = sdbcEqpCod.Text
        ofrmLstAhorroNeg.sdbcEqpCod_Validate False
    End If
    If optReu Then ofrmLstAhorroNeg.optReu = True
    If optDir Then ofrmLstAhorroNeg.optDir = True
    If optTodos Then ofrmLstAhorroNeg.optTodos = True

    ofrmLstAhorroNeg.sdbcMon = sdbcMon.Text
    ofrmLstAhorroNeg.sdbcMon_Validate False
        
    ofrmLstAhorroNeg.Show 1

End Sub

Private Sub Form_Activate()
    
    sdbgRes.SelBookmarks.RemoveAll
End Sub

Private Sub Form_Load()
    
    Me.Height = 6255
    Me.Width = 10815
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
     Set oMonedas = oFSGSRaiz.Generar_CMonedas
   
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        dequivalencia = oMonedas.Item(1).Equiv
    End If
   
    ConfigurarSeguridad
        
    If bREqp Then
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        lblEqp.Visible = True
        lblEqp = basOptimizacion.gCodEqpUsuario & " - " & oUsuarioSummit.comprador.DenEqp
    Else
        Set oEqps = oFSGSRaiz.Generar_CEquipos
     
    End If
    
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & ""
    
End Sub

Private Sub Form_Resize()
    
    If Me.Width > 150 Then
        sdbgRes.Width = Me.Width - 120
        
        sdbgRes.Columns(1).Width = (sdbgRes.Width - 570) * 0.18
        sdbgRes.Columns(2).Width = (sdbgRes.Width - 570) * 0.18
        sdbgRes.Columns(3).Width = (sdbgRes.Width - 570) * 0.18
        sdbgRes.Columns(4).Width = (sdbgRes.Width - 570) * 0.18
        sdbgRes.Columns(5).Width = (sdbgRes.Width - 570) * 0.18
        sdbgRes.Columns(6).Width = (sdbgRes.Width - 570) * 0.1
        
        
        sdbgTotales.Top = Me.Height - 780
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Columns(0).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(1).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(2).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(3).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(4).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(5).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(6).Width = sdbgTotales.Width / 7
        
        MSChart1.Width = Me.Width - 120
        
    End If
    
    If Me.Height > 2100 Then
        sdbgRes.Height = Me.Height - 2040
        MSChart1.Height = Me.Height - 1840
    End If
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oEqps = Nothing
    Set ADORs = Nothing
    Set oMonedas = Nothing
    Set oMon = Nothing
    Me.Visible = False
    
End Sub

Private Sub BorrarDatosTotales()
    sdbgTotales.RemoveAll
    sdbgTotales.Columns(6).CellStyleSet "Normal"
    sdbgTotales.Refresh
End Sub

Private Sub CargarGrid()
Dim dpres As Double
Dim dadj As Double
Dim sFecha As String

    dpres = 0
    dadj = 0
    
    sdbgRes.RemoveAll
    
    sdbgTotales.RemoveAll
    
    While Not ADORs.EOF
        
        If ADORs("ADJDIR").Value = 0 Then
            sFecha = Format(ADORs(0).Value, "short date") & " " & Format(ADORs(0).Value, "short time")
        Else
            sFecha = Format(ADORs(0).Value, "short date")
        End If
        
        sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & sFecha & Chr(m_lSeparador) & ADORs.Fields("REF").Value & Chr(m_lSeparador) & dequivalencia * ADORs(1).Value & Chr(m_lSeparador) & dequivalencia * ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & ADORs(4).Value
        dpres = dpres + dequivalencia * ADORs(1).Value
        dadj = dadj + dequivalencia * ADORs(2).Value
        ADORs.MoveNext
    Wend
    ADORs.Close
    Set ADORs = Nothing

    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & dpres & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & dadj & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & dpres - dadj
    
End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    Select Case Tipo
    
'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                picLegend.Visible = True
                picLegend2.Visible = False
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                picLegend.Visible = False
                picLegend2.Visible = True
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                picLegend.Visible = True
                picLegend2.Visible = False
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
    
End Sub

Private Sub optDir_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optReu_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optTodos_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub sdbcEqpCod_Change()
     
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcEqpDen.Text = ""
        bRespetarCombo = False
        sdbgRes.RemoveAll
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcEqpCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpCod.Rows - 1
            bm = sdbcEqpCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEqpCod_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpCod.Text = ""
        oMensajes.NoValido sIdiEqp
    Else
        bRespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        bRespetarCombo = False
        bCargarComboDesde = False
        sdbgRes.RemoveAll
    End If
    
    Set oEquipos = Nothing

End Sub

Private Sub sdbcEqpDen_Change()
      
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcEqpCod.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
            
        sdbgRes.RemoveAll
        
    End If
          
End Sub
Private Sub sdbcEqpDen_CloseUp()
    
    If sdbcEqpDen.Value = "..." Or sdbcEqpDen = "" Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    bRespetarCombo = False
    MSChart1.Visible = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
    bCargarComboDesde = False
    
    
End Sub
Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcEqpDen.RemoveAll
    
    If bCargarComboDesde Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, True, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpDen.Rows - 1
            bm = sdbcEqpDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
        oMensajes.NoValido sIdiEqp
    Else
        bRespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        bRespetarCombo = False
        bCargarComboDesde = False
        sdbgRes.RemoveAll
    End If
    
    Set oEquipos = Nothing

End Sub


Private Sub sdbcEqpCod_CloseUp()
    
    If sdbcEqpCod.Value = "..." Or sdbcEqpCod.Value = "" Then
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    MSChart1.Visible = False
    sdbgRes.RemoveAll
    BorrarDatosTotales

    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    
End Sub
Private Sub sdbcEqpCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    sdbcEqpCod.RemoveAll
    
    If bCargarComboDesde Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, False, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEqpCod_InitColumnProps()

    sdbcEqpCod.DataFieldList = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbgRes_DblClick()
Dim ADORs As Ador.Recordset
Dim frm As frmInfAhorroNegDetalle
    
    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    
    If bREqp Then
        Set ADORs = oGestorInformes.AhorroNegociadoEqpNegFecha(CDate(sdbgRes.Columns(0).Value), True, , , , , , basOptimizacion.gCodEqpUsuario, optReu, optDir)
    Else
        Set ADORs = oGestorInformes.AhorroNegociadoEqpNegFecha(CDate(sdbgRes.Columns(0).Value), True, , , , , , sdbcEqpCod, optReu, optDir)
    End If
    
    
    If ADORs Is Nothing Then Exit Sub
                
    Set frm = New frmInfAhorroNegDetalle
                
    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                
    frm.caption = sIdiDetFec & "      " & sdbgRes.Columns(0).Text
    frm.sdbgRes.Columns(0).caption = sIdiEqp
    frm.sdbgRes.Columns(0).TagVariant = sIdiEqp
    frm.Fecha = CDate(sdbgRes.Columns(0).Text)
    frm.Eqp = sdbcEqpCod
    frm.SoloDeAdjDir = optDir
    frm.SoloEnReunion = optReu
    frm.dequivalencia = dequivalencia
    While Not ADORs.EOF
        frm.sdbgRes.AddItem ADORs("EQP").Value & Chr(m_lSeparador) & ADORs("DENEQP").Value & Chr(m_lSeparador) & Chr(m_lSeparador) & dequivalencia * ADORs("PRES").Value & Chr(m_lSeparador) & dequivalencia * ADORs("PREC").Value & Chr(m_lSeparador) & dequivalencia * ADORs("AHORRO").Value & Chr(m_lSeparador) & ADORs("AHORROPORC").Value
        ADORs.MoveNext
    Wend
    ADORs.Close
    Set ADORs = Nothing
    
    frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 1000
    frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
                
    frm.Show 1
                         
    sdbgRes.SelBookmarks.RemoveAll
                
End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
    
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
 
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegEqpRestComp)) Is Nothing) And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
        bREqp = True
    End If
End Sub

Private Sub sdbgTotales_RowLoaded(ByVal Bookmark As Variant)
    If sdbgTotales.Columns(6).Value = "" Then Exit Sub
    
    If sdbgTotales.Columns(6).Value < 0 Then
        sdbgTotales.Columns(6).CellStyleSet "Red"
    Else
        If sdbgTotales.Columns(6).Value > 0 Then
            sdbgTotales.Columns(6).CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub sdbcMon_DropDown()
    
    Screen.MousePointer = vbHourglass
    
    sdbcMon.RemoveAll
    
    oMonedas.CargarTodasLasMonedas , , , , , False, True
    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcMon.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMon.Rows - 1
            bm = sdbcMon.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMon.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcMon.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
    End If
    
End Sub


Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_NEGEQPDESDE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value
        Ador.MoveNext
        optReu.caption = Ador(0).Value
        Ador.MoveNext
        optDir.caption = Ador(0).Value '5
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value '8 Denominaci�n
        sdbcEqpCod.Columns(1).caption = Ador(0).Value
        sdbcEqpDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
       
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).Value '10
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        lblEqpCod.caption = Ador(0).Value '15
        Ador.MoveNext
        sdbcEqpCod.Columns(0).caption = Ador(0).Value
        sdbcEqpDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        sIdiPresup = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value '20
        sIdiAdj = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(5).caption = Ador(0).Value
        sIdiAhor = Ador(0).Value
        Ador.MoveNext
        sIdiFecha = Ador(0).Value
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value
        Ador.MoveNext
        sIdiTotal = Ador(0).Value
        Ador.MoveNext
        sIdiEqp = Ador(0).Value
        Ador.MoveNext
        sIdiDetFec = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
       
        Ador.Close
    
    End If
    
    picLegend.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGEQPDESDE_PICLEGEND + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)
    picLegend2.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGEQPDESDE_PICLEGEND2 + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)

    Set Ador = Nothing

End Sub

Private Sub txtFecDesde_Change()
    MSChart1.Visible = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub txtFecHasta_Change()
    MSChart1.Visible = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub
