VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSeguimEstados 
   Caption         =   "DHist�rico de cambios de estado"
   ClientHeight    =   4860
   ClientLeft      =   795
   ClientTop       =   1185
   ClientWidth     =   9075
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSeguimEstados.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4860
   ScaleWidth      =   9075
   Begin SSDataWidgets_B.SSDBGrid sdbgEstados 
      Height          =   2445
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7950
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   5
      stylesets.count =   5
      stylesets(0).Name=   "IntOK"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSeguimEstados.frx":0CB2
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSeguimEstados.frx":0CCE
      stylesets(2).Name=   "ActiveRow"
      stylesets(2).ForeColor=   16777215
      stylesets(2).BackColor=   -2147483647
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSeguimEstados.frx":0CEA
      stylesets(2).AlignmentText=   0
      stylesets(3).Name=   "IntHeader"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmSeguimEstados.frx":0D06
      stylesets(3).AlignmentPicture=   0
      stylesets(4).Name=   "IntKO"
      stylesets(4).BackColor=   255
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmSeguimEstados.frx":0E5E
      AllowUpdate     =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      MaxSelectedRows =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   5530
      Columns(0).Caption=   "Estado"
      Columns(0).Name =   "ESTADO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   2117
      Columns(1).Caption=   "Fecha"
      Columns(1).Name =   "FECHA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      Columns(2).Width=   1746
      Columns(2).Caption=   "Usuario"
      Columns(2).Name =   "USUARIO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3625
      Columns(3).Caption=   "Comentarios"
      Columns(3).Name =   "COMENTARIOS"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   2000
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "ID_EST"
      Columns(4).Name =   "ID_EST"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   14023
      _ExtentY        =   4313
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgModif 
      Height          =   2040
      Left            =   0
      TabIndex        =   1
      Top             =   2790
      Width           =   9075
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      GroupHeaders    =   0   'False
      Col.Count       =   8
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSeguimEstados.frx":0E7A
      stylesets(1).Name=   "Red"
      stylesets(1).ForeColor=   255
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSeguimEstados.frx":0E96
      BevelColorFace  =   192
      AllowUpdate     =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      MaxSelectedRows =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      CaptionAlignment=   0
      Columns.Count   =   8
      Columns(0).Width=   2117
      Columns(0).Caption=   "Fecha"
      Columns(0).Name =   "FECHA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   50
      Columns(0).HasHeadForeColor=   -1  'True
      Columns(0).HeadForeColor=   16777215
      Columns(1).Width=   3149
      Columns(1).Caption=   "Usuario"
      Columns(1).Name =   "USUARIO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasHeadForeColor=   -1  'True
      Columns(1).HeadForeColor=   16777215
      Columns(2).Width=   3519
      Columns(2).Caption=   "DEST"
      Columns(2).Name =   "DEST"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   2000
      Columns(2).HasHeadForeColor=   -1  'True
      Columns(2).HeadForeColor=   16777215
      Columns(3).Width=   2223
      Columns(3).Caption=   "PRECIO"
      Columns(3).Name =   "PRECIO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasHeadForeColor=   -1  'True
      Columns(3).HeadForeColor=   16777215
      Columns(4).Width=   2408
      Columns(4).Caption=   "CANT"
      Columns(4).Name =   "CANT"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasHeadForeColor=   -1  'True
      Columns(4).HeadForeColor=   16777215
      Columns(5).Width=   2117
      Columns(5).Caption=   "FECENTREGA"
      Columns(5).Name =   "FECENTREGA"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).HasHeadForeColor=   -1  'True
      Columns(5).HeadForeColor=   16777215
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "DEST_COD"
      Columns(6).Name =   "DEST_COD"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "DIF"
      Columns(7).Name =   "DIF"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   16007
      _ExtentY        =   3598
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Se han producido modificaciones en los datos basicos de la linea de pedido!"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   285
      Left            =   90
      TabIndex        =   2
      Top             =   2520
      Width           =   7845
   End
End
Attribute VB_Name = "frmSeguimEstados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmSeguimEstados
''' *** Creacion: 27/07/2001

Option Explicit

Public ModoFuncionamiento As ModoSeguimiento
Public sOrigen As String
Public MostrarEstadoIntegracion As Boolean
Public bMostrarModif As Boolean

Private sIdioma(2) As String
Private sIdiEst(18) As String
Private sIdiEstPA(4) As String
Private sIdiSincronizado As String
Private sIdiNoSincronizado As String

Public Anyo As Integer
Public Orden As Long
Public pedido As Long
Public Linea As Long
Public EstOrden As TipoEstadoOrdenEntrega

''' Coleccion de estados por los que pasa una orden de entrega o
''' Coleccion de estados por los que pasa una linea de pedido
Public oLineaPedido As CLineaPedido
Public oOrdenEntrega As COrdenEntrega

Public Adores As Ador.Recordset
Private m_lCont As Long

Private Sub Form_Load()

    CargarRecursos
    
    PonerFieldSeparator Me
    If bMostrarModif Then
        Me.Width = 9195
        Me.Height = 5265
        sdbgModif.Visible = True
        Label1.Visible = True
    Else
        Me.Width = 7950
        Me.Height = 3570
        sdbgModif.Visible = False
        Label1.Visible = False
    End If

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    DoEvents
    
    If sOrigen = "frmSeguimientoOrden" Then
        frmSeguimEstados.caption = sIdioma(0) & " " & Anyo & " - " & pedido & " - " & Orden
        CargarGrid1 Adores
    Else
        If sOrigen = "frmSeguimientoLinea" Then
            If oLineaPedido.ArtCod_Interno = "" Then
                frmSeguimEstados.caption = sIdioma(1) & " " & Anyo & " - " & pedido & " - " & Orden & " - " & oLineaPedido.ArtDen
            Else
                frmSeguimEstados.caption = sIdioma(1) & " " & Anyo & " - " & pedido & " - " & Orden & " - " & oLineaPedido.ArtCod_Interno & " - " & oLineaPedido.ArtDen
            End If
            Set Adores = oLineaPedido.DevolverHistoriaDeLosEstados
            CargarGrid2 Adores
            If bMostrarModif Then
                CargarGrid3
            End If
        End If
    End If
End Sub
Private Sub Form_Resize()
    
    ''' * Objetivo: Adecuar los controles
On Error Resume Next

    If bMostrarModif Then
        sdbgEstados.Height = Height / 2 - 187
        Label1.Top = sdbgEstados.Height + 75
        sdbgModif.Top = Label1.Top + Label1.Height '+ 75
        sdbgModif.Height = Height / 2 - 600
        sdbgEstados.Width = Width - 120
        sdbgModif.Width = Width - 120
        
        sdbgModif.Columns(0).Width = sdbgModif.Width * 21 / 100
        sdbgModif.Columns(1).Width = sdbgModif.Width * 20 / 100
        sdbgModif.Columns(2).Width = sdbgModif.Width * 14 / 100
        sdbgModif.Columns(3).Width = sdbgModif.Width * 14 / 100
        sdbgModif.Columns(4).Width = sdbgModif.Width * 12 / 100
                
    Else
        If Height >= 400 Then sdbgEstados.Height = Height - 400
        If Width >= 120 Then sdbgEstados.Width = Width - 120
    End If
    sdbgEstados.Columns(0).Width = sdbgEstados.Width * 30 / 100
    sdbgEstados.Columns(2).Width = sdbgEstados.Width * 15 / 100
    sdbgEstados.Columns(3).Width = sdbgEstados.Width * 17 / 100
    sdbgEstados.Columns(4).Width = sdbgEstados.Width * 38 / 100 - 335
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SEGUIMESTADOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sIdioma(0) = Ador(0).Value
        Ador.MoveNext
        sIdioma(1) = Ador(0).Value
        Ador.MoveNext
        
        For i = 0 To 3
            sdbgEstados.Columns(i).caption = Ador(0).Value
            Ador.MoveNext
        Next
        sdbgModif.Columns(0).caption = sdbgEstados.Columns(1).caption
        sdbgModif.Columns(1).caption = sdbgEstados.Columns(2).caption
        For i = 1 To 16
            sIdiEst(i) = Ador(0).Value
            Ador.MoveNext
        Next
        sIdiSincronizado = Ador(0).Value
        Ador.MoveNext
        sIdiNoSincronizado = Ador(0).Value
        
        Ador.MoveNext
        sIdiEst(17) = Ador(0).Value  'Emitida
        Ador.MoveNext
        sdbgModif.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgModif.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgModif.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgModif.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        For i = 1 To 4
            sIdiEstPA(i) = Ador(0).Value
            Ador.MoveNext
        Next
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub

''' <summary>Carga Grid1</summary>
''' <param name="Adores">Datos</param>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub CargarGrid1(ByVal Adores As Ador.Recordset)
    Dim DenEstado As String

    'Carga la grid
    
    sdbgEstados.RemoveAll
    
    While Not Adores.EOF
        If ModoFuncionamiento = ModoPedidoEstandar Then
            If Adores("EST") = TipoEstadoOrdenEntrega.PendienteDeAprobacion Then DenEstado = sIdiEst(1)
            If Adores("EST") = TipoEstadoOrdenEntrega.DenegadoParcialAprob Then DenEstado = sIdiEst(2)
            If Adores("EST") = TipoEstadoOrdenEntrega.EmitidoAlProveedor Then DenEstado = sIdiEst(3)
            If Adores("EST") = TipoEstadoOrdenEntrega.AceptadoPorProveedor Then DenEstado = sIdiEst(4)
            If Adores("EST") = TipoEstadoOrdenEntrega.EnCamino Then DenEstado = sIdiEst(5)
            If Adores("EST") = TipoEstadoOrdenEntrega.EnRecepcion Then DenEstado = sIdiEst(6)
            If Adores("EST") = TipoEstadoOrdenEntrega.RecibidoYCerrado Then DenEstado = sIdiEst(7)
            If Adores("EST") = TipoEstadoOrdenEntrega.Anulado Then DenEstado = sIdiEst(8)
            If Adores("EST") = TipoEstadoOrdenEntrega.RechazadoPorProveedor Then DenEstado = sIdiEst(9)
            If Adores("EST") = TipoEstadoOrdenEntrega.DenegadoTotalAprobador Then DenEstado = sIdiEst(10)
        Else
            If Adores("EST") = TipoEstadoPedidoAbierto.PANoVigente Then DenEstado = sIdiEstPA(1)
            If Adores("EST") = TipoEstadoPedidoAbierto.PAAbierto Then DenEstado = sIdiEstPA(2)
            If Adores("EST") = TipoEstadoPedidoAbierto.PACerrado Then DenEstado = sIdiEstPA(3)
            If Adores("EST") = TipoEstadoPedidoAbierto.PAAnulado Then DenEstado = sIdiEstPA(4)
        End If
        
        If MostrarEstadoIntegracion Then
            If Adores("EST").Value = TipoEstadoOrdenEntrega.PendienteDeAprobacion Or _
                Adores("EST").Value = TipoEstadoOrdenEntrega.DenegadoParcialAprob Or _
                Adores("EST").Value = TipoEstadoOrdenEntrega.RecibidoYCerrado Or _
                Adores("EST").Value = TipoEstadoOrdenEntrega.DenegadoTotalAprobador Or _
                Adores("EST").Value = TipoEstadoOrdenEntrega.EnRecepcion Then
                sdbgEstados.AddItem DenEstado & Chr(m_lSeparador) & Adores("FECHA") & Chr(m_lSeparador) & Adores("PER") & Chr(m_lSeparador) & Adores("COMENT") & Chr(m_lSeparador) & Adores("ID")
            Else
                sdbgEstados.AddItem DenEstado & Chr(m_lSeparador) & Adores("FECHA") & Chr(m_lSeparador) & Adores("PER") & Chr(m_lSeparador) & Adores("COMENT") & Chr(m_lSeparador) & Adores("ID")
            End If
        Else
            sdbgEstados.AddItem DenEstado & Chr(m_lSeparador) & Adores("FECHA") & Chr(m_lSeparador) & Adores("PER") & Chr(m_lSeparador) & Adores("COMENT") & Chr(m_lSeparador) & Adores("ID")
        End If
        
        Adores.MoveNext
    Wend
    'Cierra el recordset
    Adores.Close
    Set Adores = Nothing
End Sub

''' <summary>Carga Grid2</summary>
''' <param name="Adores">Datos</param>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub CargarGrid2(ByVal Adores As Ador.Recordset)
    Dim DenoEstado As String
    Dim lID As Long
    Dim i As Integer

    'Carga la grid
    
    sdbgEstados.RemoveAll
    
    If Not Adores.EOF Then
        If EstOrden >= EmitidoAlProveedor Then
            i = 1
            While Not Adores.EOF
                If Adores("EST").Value = tipoestadolineapedido.Aprobada Then lID = i
                i = i + 1
                Adores.MoveNext
            Wend
        Else
            lID = 0
        End If
    
        Adores.MoveFirst
        i = 1
        While Not Adores.EOF
            If ModoFuncionamiento = ModoPedidoEstandar Then
                If Adores("EST").Value = tipoestadolineapedido.PendienteDeAprobar Then DenoEstado = sIdiEst(11)
                If Adores("EST").Value = tipoestadolineapedido.Aprobada Then
                    If EstOrden >= EmitidoAlProveedor Then
                        If lID = i Or lID = 0 Then 's�lo puede haber un estado emitido,los dem�s estar�n aprobados.
                            DenoEstado = sIdiEst(17)   'Emitida
                        Else
                            DenoEstado = sIdiEst(12)   'Aprobada
                        End If
                    Else
                        DenoEstado = sIdiEst(12)   'Aprobada
                    End If
                End If
                If Adores("EST").Value = tipoestadolineapedido.RecibidoParcialmente Then DenoEstado = sIdiEst(13)
                If Adores("EST").Value = tipoestadolineapedido.RecibidoTotalmente Then DenoEstado = sIdiEst(14)
                If Adores("EST").Value = tipoestadolineapedido.Denegada Then DenoEstado = sIdiEst(15)
                If Adores("EST").Value = tipoestadolineapedido.EnEsperaAprobacionLimiteAdjudicacion Then DenoEstado = sIdiEst(16)
            Else
                If Adores("EST") = TipoEstadoPedidoAbierto.PANoVigente Then DenoEstado = sIdiEstPA(1)
                If Adores("EST") = TipoEstadoPedidoAbierto.PAAbierto Then DenoEstado = sIdiEstPA(2)
                If Adores("EST") = TipoEstadoPedidoAbierto.PACerrado Then DenoEstado = sIdiEstPA(3)
                If Adores("EST") = TipoEstadoPedidoAbierto.PAAnulado Then DenoEstado = sIdiEstPA(4)
            End If
            
            sdbgEstados.AddItem DenoEstado & Chr(m_lSeparador) & Adores("FECHA") & Chr(m_lSeparador) & Adores("PER") & Chr(m_lSeparador) & Adores("COMENT") & Chr(m_lSeparador) & Adores("ID")

            i = i + 1
            Adores.MoveNext
        Wend
    End If
    
    'Cierra el recordset
    Adores.Close
    Set Adores = Nothing
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
sOrigen = ""
MostrarEstadoIntegracion = False
bMostrarModif = False

Anyo = 0
Orden = 0
pedido = 0
Linea = 0
EstOrden = 0

Set oLineaPedido = Nothing
Set oOrdenEntrega = Nothing

Set Adores = Nothing

End Sub

Private Sub sdbgEstados_DblClick()
    Dim bModificar As Boolean
    Dim Comentario As String
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    If sdbgEstados.Row = -1 Then Exit Sub
    
    'Muestra el comentario de la l�nea y permite modificarlo.
    
    Select Case sOrigen
        Case "frmSeguimientoOrden"
            bModificar = False
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGModificar)) Is Nothing) Then
                bModificar = True And (Not oOrdenEntrega.EstaDeBajaLogica)
            End If
            
            Comentario = FSGSForm.MostrarFormComenEstado(oGestorIdiomas, NullToStr(sdbgEstados.Columns("COMENTARIOS").Value), Not bModificar, basPublic.gParametrosInstalacion.gIdioma, "EstadoOrden")
            teserror = oOrdenEntrega.ModificarComentarioEstado(Comentario, sdbgEstados.Columns("ID_EST").Value)
            Screen.MousePointer = vbNormal
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
            Set oIBaseDatos = Nothing
            
            ActualizarGrid Comentario
            
        Case "frmSeguimientoLinea"
            Comentario = FSGSForm.MostrarFormComenEstado(oGestorIdiomas, NullToStr(sdbgEstados.Columns("COMENTARIOS").Value), True, basPublic.gParametrosInstalacion.gIdioma, "EstadoLinea")
    End Select
    

End Sub

Public Sub ActualizarGrid(ByVal Comentario As String)
    'Actualiza la grid con el nuevo comentario introducido.
    sdbgEstados.Columns("COMENTARIOS").Value = Comentario
    sdbgEstados.Update
End Sub

Public Sub CargarGrid3()
    Dim sPersona As String
    Dim Vars(0 To 3) As String
    Dim i As Integer
    Dim vDestino As Variant
    Dim vPrecio As Variant
    Dim vCantidad As Variant
    Dim vFechaEntrega As Variant
    Set Adores = oLineaPedido.DevolverHistoriaDeLasModificaciones
    'Carga la grid
    sdbgModif.RemoveAll
    m_lCont = Adores.RecordCount
    If Not Adores.EOF Then
        Adores.MoveFirst
        While Not Adores.EOF
            For i = 0 To UBound(Vars)
                Vars(i) = "0"
            Next i
            If Not IsNull(Adores("NOM").Value) Then
                sPersona = Adores("NOM").Value & " " & Adores("APE").Value
            ElseIf Not IsNull(Adores("APE").Value) Then
                sPersona = Adores("APE").Value
            ElseIf Not IsNull(Adores("PER").Value) Then
                sPersona = Adores("PER").Value
            End If
            
            If vDestino & "" <> "" Then
                If Adores("DEST").Value <> vDestino Then
                    Vars(0) = "1"
                End If
                
                If Adores("PREC_UP").Value <> vPrecio Then
                    Vars(1) = "1"
                End If
                
                If Adores("CANT_PED").Value <> vCantidad Then
                    Vars(2) = "1"
                End If
                
                If NullToStr(Adores("FECENTREGA").Value) <> vFechaEntrega Then
                    Vars(3) = "1"
                End If
            End If
            sdbgModif.AddItem Format(Adores("FECACT").Value, "Short date") & " " & Format(Adores("FECACT").Value, "Short time") & Chr(m_lSeparador) & sPersona & Chr(m_lSeparador) & Adores("DEST").Value & " - " & Adores("DEN").Value & Chr(m_lSeparador) & Adores("PREC_UP").Value & Chr(m_lSeparador) & Adores("CANT_PED").Value & Chr(m_lSeparador) & NullToStr(Adores("FECENTREGA").Value) & Chr(m_lSeparador) & Adores("DEST").Value & _
                                Chr(m_lSeparador) & Vars(0) & "#" & Vars(1) & "#" & Vars(2) & "#" & Vars(3)
            vDestino = Adores("DEST").Value
            vPrecio = Adores("PREC_UP").Value
            vCantidad = Adores("CANT_PED").Value
            vFechaEntrega = NullToStr(Adores("FECENTREGA").Value)
            Adores.MoveNext
        Wend
    End If
    'Cierra el recordset
    Adores.Close
    Set Adores = Nothing
    
End Sub

Private Sub sdbgModif_RowLoaded(ByVal Bookmark As Variant)
Dim Vars() As String
Vars = Split(sdbgModif.Columns("DIF").Value, "#")
If Vars(0) = "1" Then
    sdbgModif.Columns("DEST").CellStyleSet "Red", sdbgModif.Row
End If

If Vars(1) = "1" Then
    sdbgModif.Columns("PRECIO").CellStyleSet "Red", sdbgModif.Row
End If

If Vars(2) = "1" Then
    sdbgModif.Columns("CANT").CellStyleSet "Red", sdbgModif.Row
End If

If Vars(3) = "1" Then
    sdbgModif.Columns("FECENTREGA").CellStyleSet "Red", sdbgModif.Row
End If
End Sub

Private Sub sdbgModif_Scroll(Cancel As Integer)
'sdbgModif.Bookmark = sdbgModif.FirstRow
'm_vDestino = sdbgModif.Columns("DEST_COD").Value
'm_vPrecio = sdbgModif.Columns("PRECIO").Value
'm_vCantidad = sdbgModif.Columns("CANT").Value
'm_vFechaEntrega = sdbgModif.Columns("FECENTREGA").Value
End Sub
