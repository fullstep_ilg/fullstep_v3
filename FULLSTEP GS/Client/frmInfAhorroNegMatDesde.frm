VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroNegMatDesde 
   Caption         =   "Informe de ahorros negociados por material en un per�odo"
   ClientHeight    =   6300
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14640
   Icon            =   "frmInfAhorroNegMatDesde.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6300
   ScaleWidth      =   14640
   Begin VB.CheckBox chkPrecUnitario 
      Caption         =   "S�lo precio unitario"
      Height          =   285
      Left            =   11235
      TabIndex        =   45
      Top             =   225
      Width           =   1680
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgResArt 
      Height          =   3765
      Left            =   0
      TabIndex        =   41
      Top             =   2145
      Width           =   10680
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   14
      stylesets.count =   5
      stylesets(0).Name=   "Blanco"
      stylesets(0).BackColor=   16777215
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegMatDesde.frx":0CB2
      stylesets(1).Name=   "Gris"
      stylesets(1).BackColor=   14671839
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegMatDesde.frx":0CCE
      stylesets(2).Name=   "normal"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegMatDesde.frx":0CEA
      stylesets(3).Name=   "Red"
      stylesets(3).BackColor=   4744445
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmInfAhorroNegMatDesde.frx":0D06
      stylesets(4).Name=   "Green"
      stylesets(4).BackColor=   10409634
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmInfAhorroNegMatDesde.frx":0D22
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   14
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Fecha"
      Columns(0).Name =   "FECULTREU"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   3201
      Columns(1).Caption=   "Articulo"
      Columns(1).Name =   "COD_ART"
      Columns(1).CaptionAlignment=   2
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   2487
      Columns(2).Caption=   "Descripcion"
      Columns(2).Name =   "DEN_ART"
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1640
      Columns(3).Caption=   "Proceso"
      Columns(3).Name =   "PROCE"
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   3200
      Columns(4).Caption=   "Proveedor"
      Columns(4).Name =   "PROVE"
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "FechaInicio"
      Columns(5).Name =   "FechaInicio"
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3228
      Columns(6).Caption=   "FechaFin"
      Columns(6).Name =   "FechaFin"
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "CantAdj"
      Columns(7).Name =   "CANT_ADJ"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   2
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).NumberFormat=   "Standard"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3201
      Columns(8).Caption=   "PRECIO"
      Columns(8).Name =   "PRECIO"
      Columns(8).Alignment=   1
      Columns(8).CaptionAlignment=   2
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).NumberFormat=   "Standard"
      Columns(8).FieldLen=   256
      Columns(8).HasBackColor=   -1  'True
      Columns(8).BackColor=   16777215
      Columns(9).Width=   3201
      Columns(9).Caption=   "Presupuesto"
      Columns(9).Name =   "PRES"
      Columns(9).Alignment=   1
      Columns(9).CaptionAlignment=   2
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).NumberFormat=   "Standard"
      Columns(9).FieldLen=   256
      Columns(9).HasBackColor=   -1  'True
      Columns(9).BackColor=   16777215
      Columns(10).Width=   3016
      Columns(10).Caption=   "Adjudicado"
      Columns(10).Name=   "PREC"
      Columns(10).Alignment=   1
      Columns(10).CaptionAlignment=   2
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).NumberFormat=   "Standard"
      Columns(10).FieldLen=   256
      Columns(10).HasBackColor=   -1  'True
      Columns(10).BackColor=   16777215
      Columns(11).Width=   3201
      Columns(11).Caption=   "Ahorro"
      Columns(11).Name=   "AHO"
      Columns(11).Alignment=   1
      Columns(11).CaptionAlignment=   2
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).NumberFormat=   "Standard"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3201
      Columns(12).Caption=   "%"
      Columns(12).Name=   "PORCEN"
      Columns(12).Alignment=   1
      Columns(12).CaptionAlignment=   2
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).NumberFormat=   "0.0#\%"
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "color"
      Columns(13).Name=   "color"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      _ExtentX        =   18838
      _ExtentY        =   6641
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraDesde 
      Caption         =   "Mostrar resultados desde material"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1410
      Left            =   0
      TabIndex        =   32
      Top             =   720
      Width           =   10650
      Begin SSDataWidgets_B.SSDBCombo sdbcArtiCod 
         Height          =   285
         Left            =   930
         TabIndex        =   13
         Top             =   975
         Width           =   1890
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         Cols            =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TagVariant      =   ""
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2408
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5900
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "Denominaci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3334
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdBorrar 
         Height          =   315
         Left            =   9850
         Picture         =   "frmInfAhorroNegMatDesde.frx":0D3E
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   360
         Width           =   345
      End
      Begin VB.CommandButton cmdSelMat 
         Height          =   315
         Left            =   10230
         Picture         =   "frmInfAhorroNegMatDesde.frx":0DE3
         Style           =   1  'Graphical
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   360
         Width           =   345
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   930
         TabIndex        =   5
         Top             =   210
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
         Height          =   285
         Left            =   5865
         TabIndex        =   7
         Top             =   195
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
         Height          =   285
         Left            =   930
         TabIndex        =   9
         Top             =   570
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
         Height          =   285
         Left            =   5865
         TabIndex        =   11
         Top             =   570
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
         Height          =   285
         Left            =   6825
         TabIndex        =   8
         Top             =   195
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
         Height          =   285
         Left            =   1890
         TabIndex        =   10
         Top             =   570
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
         Height          =   285
         Left            =   6825
         TabIndex        =   12
         Top             =   570
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
         Height          =   285
         Left            =   1890
         TabIndex        =   6
         Top             =   210
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcArtiDen 
         Height          =   285
         Left            =   2850
         TabIndex        =   14
         Top             =   975
         Width           =   4005
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5900
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "Denominaci�n"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2408
         Columns(1).Caption=   "C�d."
         Columns(1).Name =   "C�d."
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7064
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblArticulo 
         Caption         =   "Art�culo:"
         Height          =   210
         Left            =   150
         TabIndex        =   40
         Top             =   1050
         Width           =   795
      End
      Begin VB.Label lblGMN4_4 
         Caption         =   "Gru.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4950
         TabIndex        =   37
         Top             =   615
         Width           =   915
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Comm:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   36
         Top             =   240
         Width           =   795
      End
      Begin VB.Label lblGMN2_4 
         Caption         =   "Fam.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4950
         TabIndex        =   35
         Top             =   240
         Width           =   915
      End
      Begin VB.Label lblGMN3_4 
         Caption         =   "Subfam.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   34
         Top             =   630
         Width           =   795
      End
   End
   Begin VB.PictureBox picTipoGrafico 
      BorderStyle     =   0  'None
      Height          =   435
      Left            =   4320
      ScaleHeight     =   435
      ScaleWidth      =   3525
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   150
      Visible         =   0   'False
      Width           =   3525
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
         Height          =   285
         Left            =   720
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   90
         Width           =   1815
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Row.Count       =   5
         Row(0)          =   "Barras 2D"
         Row(1)          =   "Barras 3D"
         Row(2)          =   "Lineas 2D"
         Row(3)          =   "Lineas 3D"
         Row(4)          =   "Tarta"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "TIPO"
         Columns(0).Name =   "TIPO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3201
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picLegend 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   12915
      Picture         =   "frmInfAhorroNegMatDesde.frx":0E4F
      ScaleHeight     =   735
      ScaleWidth      =   1635
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   15
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.PictureBox picLegend2 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   12915
      Picture         =   "frmInfAhorroNegMatDesde.frx":4B51
      ScaleHeight     =   615
      ScaleWidth      =   1635
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   60
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.CommandButton cmdActualizar 
      Height          =   285
      Left            =   8280
      Picture         =   "frmInfAhorroNegMatDesde.frx":7CEF
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   240
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   -15
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   5925
      Width           =   10650
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegMatDesde.frx":7D7A
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegMatDesde.frx":7D96
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegMatDesde.frx":7DB2
      DividerType     =   1
      DividerStyle    =   2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   7
      Columns(0).Width=   2275
      Columns(0).Caption=   "CAPTION"
      Columns(0).Name =   "CAPTION"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "PRESCAPTION"
      Columns(1).Name =   "PRESCAPTION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "PRES"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,#"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "ADJCAPTION"
      Columns(3).Name =   "ADJCAPTION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "ADJ"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,#"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2090
      Columns(5).Caption=   "AHOCAPTION"
      Columns(5).Name =   "AHOCAPTION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "AHO"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "#,#"
      Columns(6).FieldLen=   256
      _ExtentX        =   18785
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   4110
      Left            =   0
      TabIndex        =   15
      Top             =   1770
      Width           =   10635
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   8
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegMatDesde.frx":7DCE
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4744445
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegMatDesde.frx":7DEA
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegMatDesde.frx":7E06
      AllowUpdate     =   0   'False
      ActiveCellStyleSet=   "Normal"
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Fecha"
      Columns(0).Name =   "FEC"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(0).StyleSet=   "Normal"
      Columns(1).Width=   2302
      Columns(1).Caption=   "Fecha"
      Columns(1).Name =   "FECHACORTA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   2487
      Columns(2).Caption=   "Referencia"
      Columns(2).Name =   "REF"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1640
      Columns(3).Caption=   "Procesos"
      Columns(3).Name =   "NUMPROCE"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   3175
      Columns(4).Caption=   "Presupuesto"
      Columns(4).Name =   "PRES"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16777215
      Columns(5).Width=   3016
      Columns(5).Caption=   "Adjudicado"
      Columns(5).Name =   "ADJ"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "Standard"
      Columns(5).FieldLen=   256
      Columns(5).HasBackColor=   -1  'True
      Columns(5).BackColor=   16777215
      Columns(6).Width=   2831
      Columns(6).Caption=   "Ahorro"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "Standard"
      Columns(6).FieldLen=   256
      Columns(7).Width=   1852
      Columns(7).Caption=   "%"
      Columns(7).Name =   "PORCEN"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   2
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).NumberFormat=   "0.0#\%"
      Columns(7).FieldLen=   256
      _ExtentX        =   18759
      _ExtentY        =   7250
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdGrafico 
      Height          =   285
      Left            =   7890
      Picture         =   "frmInfAhorroNegMatDesde.frx":7E22
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   240
      Width           =   315
   End
   Begin VB.CommandButton cmdImprimir 
      Height          =   285
      Left            =   8670
      Picture         =   "frmInfAhorroNegMatDesde.frx":8164
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   240
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.CommandButton cmdGrid 
      Height          =   285
      Left            =   7890
      Picture         =   "frmInfAhorroNegMatDesde.frx":8266
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   240
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.Frame fraSel 
      Height          =   675
      Left            =   0
      TabIndex        =   22
      Top             =   0
      Width           =   11205
      Begin SSDataWidgets_B.SSDBCombo sdbcFiltro 
         Height          =   300
         Left            =   9300
         TabIndex        =   39
         Top             =   225
         Width           =   1755
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         Cols            =   1
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   3096
         _ExtentY        =   529
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.OptionButton optReu 
         Caption         =   "Adj. en reuni�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   150
         Value           =   -1  'True
         Width           =   2340
      End
      Begin VB.OptionButton optTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5715
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   390
         Width           =   975
      End
      Begin VB.OptionButton optDir 
         Caption         =   "Adj. directa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   390
         Width           =   1350
      End
      Begin VB.TextBox txtFecHasta 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2640
         TabIndex        =   1
         Top             =   240
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalFecApeHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3780
         Picture         =   "frmInfAhorroNegMatDesde.frx":83B0
         Style           =   1  'Graphical
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecDesde 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   660
         TabIndex        =   0
         Top             =   240
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalFecApeDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         Picture         =   "frmInfAhorroNegMatDesde.frx":893A
         Style           =   1  'Graphical
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMon 
         Height          =   285
         Left            =   6750
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   240
         Width           =   1035
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1164
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1879
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "EQUIV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "SSDBCombo1"
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblFecDesde 
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   300
         Width           =   555
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2160
         TabIndex        =   29
         Top             =   300
         Width           =   450
      End
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   3675
      Left            =   0
      OleObjectBlob   =   "frmInfAhorroNegMatDesde.frx":8EC4
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   2250
      Width           =   9855
   End
   Begin MSChart20Lib.MSChart MSChartArt 
      Height          =   3400
      Left            =   0
      OleObjectBlob   =   "frmInfAhorroNegMatDesde.frx":A8EA
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   2190
      Width           =   9855
   End
   Begin MSChart20Lib.MSChart MSChartMat 
      Height          =   3675
      Left            =   0
      OleObjectBlob   =   "frmInfAhorroNegMatDesde.frx":C310
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   2250
      Width           =   9855
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgResMat 
      Height          =   4110
      Left            =   0
      TabIndex        =   44
      Top             =   1770
      Width           =   10665
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   13
      stylesets.count =   3
      stylesets(0).Name=   "normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroNegMatDesde.frx":DD36
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4744445
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroNegMatDesde.frx":DD52
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroNegMatDesde.frx":DD6E
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   13
      Columns(0).Width=   1667
      Columns(0).Caption=   "GMN1"
      Columns(0).Name =   "GMN1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   1561
      Columns(1).Caption=   "GMN2"
      Columns(1).Name =   "GMN2"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   1535
      Columns(2).Caption=   "GMN3"
      Columns(2).Name =   "GMN3"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16777215
      Columns(3).Width=   1693
      Columns(3).Caption=   "GMN4"
      Columns(3).Name =   "GMN4"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   3201
      Columns(4).Caption=   "Material"
      Columns(4).Name =   "Material"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16777215
      Columns(5).Width=   1640
      Columns(5).Caption=   "Procesos"
      Columns(5).Name =   "NUMPROCE"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).HasBackColor=   -1  'True
      Columns(5).BackColor=   16777215
      Columns(6).Width=   3175
      Columns(6).Caption=   "Presupuesto"
      Columns(6).Name =   "PRES"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "Standard"
      Columns(6).FieldLen=   256
      Columns(6).HasBackColor=   -1  'True
      Columns(6).BackColor=   16777215
      Columns(7).Width=   3016
      Columns(7).Caption=   "Adjudicado"
      Columns(7).Name =   "ADJ"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   2
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).NumberFormat=   "Standard"
      Columns(7).FieldLen=   256
      Columns(7).HasBackColor=   -1  'True
      Columns(7).BackColor=   16777215
      Columns(8).Width=   2831
      Columns(8).Caption=   "Ahorro"
      Columns(8).Name =   "AHO"
      Columns(8).Alignment=   1
      Columns(8).CaptionAlignment=   2
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).NumberFormat=   "Standard"
      Columns(8).FieldLen=   256
      Columns(9).Width=   1852
      Columns(9).Caption=   "%"
      Columns(9).Name =   "PORCEN"
      Columns(9).Alignment=   1
      Columns(9).CaptionAlignment=   2
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).NumberFormat=   "0.0#\%"
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Fecha"
      Columns(10).Name=   "FECHA"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).HasBackColor=   -1  'True
      Columns(10).BackColor=   16776960
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "DenMaterial"
      Columns(11).Name=   "DenMaterial"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).HasBackColor=   -1  'True
      Columns(11).BackColor=   16776960
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "Referencia"
      Columns(12).Name=   "REF"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      _ExtentX        =   18812
      _ExtentY        =   7250
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmInfAhorroNegMatDesde"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ofrmLstAhorroNeg As frmLstINFAhorrosNeg
'Variables para func. combos
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

Private oMonedas As CMonedas
Private dequivalencia As Double
Private oMon As CMoneda

Private ADORs As Ador.Recordset

'Variables de seguridad
Private bRMat As Boolean

'Variable de control de flujo de proceso
Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4
'Variables para caso de bRMat
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Public oGruposMN4 As CGruposMatNivel4
Private oICompAsignado As ICompProveAsignados

'Variables utilizadas para el multilenguaje
Private sIdiTipoGrafico(5) As String
Private sIdiFecha As String
Private sIdiMat As String
Private sIdiMonCent As String
Private sIdiTotal As String
Private sIdiPresup As String
Private sIdiAdj As String
Private sIdiAhor As String
Private sIdiDetResult As String
Public sMaterial As String
Public sArticulo As String
Public sFechaFiltro As String
Private iFilaSeleccionada As Integer
Private sDenArticuloAnt As String
Private sCodArticuloAnt As String
Private sColorActual As String


Private Sub ConfigurarNombres()

lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
lblGMN2_4.caption = gParametrosGenerales.gsABR_GMN2 & ":"
lblGMN3_4.caption = gParametrosGenerales.gsabr_GMN3 & ":"
lblGMN4_4.caption = gParametrosGenerales.gsabr_GMN4 & ":"
lblArticulo.caption = sArticulo & ":"
End Sub

Private Sub chkPrecUnitario_Click()
    'Si se descheque esta check se mostraran en el grafico todos los valores de la tabla
    MostrarGraficoArt sdbcTipoGrafico.Value, chkPrecUnitario.Value
End Sub

Private Sub cmdActualizar_Click()
    Dim iNivelAsig As Integer
    
    sdbgRes.RemoveAll
    sdbgResArt.RemoveAll
    sdbgResMat.RemoveAll
    
    If txtFecDesde = "" And sdbcFiltro.Text = sFechaFiltro Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then txtFecDesde.SetFocus
        Exit Sub
    End If
    
    If Not IsDate(txtFecDesde) And sdbcFiltro.Text = sFechaFiltro Then
        oMensajes.NoValido sIdiFecha
        If Me.Visible Then txtFecDesde.SetFocus
        Exit Sub
    End If
    
    If txtFecHasta <> "" And sdbcFiltro.Text = sFechaFiltro Then
        If Not IsDate(txtFecHasta) Then
            oMensajes.NoValido sIdiFecha
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
        
        If CDate(txtFecDesde) > CDate(txtFecHasta) Then
            oMensajes.FechaDesdeMayorFechaHasta
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    Else
        If txtFecHasta = "" And sdbcFiltro.Text = sFechaFiltro Then
            If Not IsDate(txtFecHasta) Then
                oMensajes.NoValido sIdiFecha
                If Me.Visible Then txtFecHasta.SetFocus
                Exit Sub
            End If
        End If
    End If
    
    
    Set ADORs = Nothing
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        If sdbcGMN4_4Cod = "" And sdbcGMN3_4Cod <> "" Then
            Set oICompAsignado = oGMN3Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            'Si el material que tiene asignado el comprador es de nivel 4
            ' no cargamos nada
            If iNivelAsig = 0 Or iNivelAsig > 3 Then
                oMensajes.InfAhorroMatNoValido
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            If sdbcGMN3_4Cod = "" And sdbcGMN2_4Cod <> "" Then
                Set oICompAsignado = oGMN2Seleccionado
            
                iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                If iNivelAsig = 0 Or iNivelAsig > 2 Then
                    oMensajes.InfAhorroMatNoValido
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            Else
                If sdbcGMN2_4Cod = "" And sdbcGMN1_4Cod <> "" Then
                    Set oICompAsignado = oGMN1Seleccionado
                 
                    iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                    If iNivelAsig < 1 Or iNivelAsig > 2 Then
                        oMensajes.InfAhorroMatNoValido
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                Else
                    If sdbcGMN1_4Cod.Value = "" Then
                        oMensajes.InfAhorroMatNoValido
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If
            End If
        End If
    End If
    
    Set ADORs = Nothing
    
    Select Case sdbcFiltro.Text
        Case sMaterial
            Set ADORs = oGestorInformes.AhorroNegociadoMatDesdeHastaMaterial(txtFecDesde, txtFecHasta, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, sdbcGMN4_4Cod.Text, optReu, optDir)
        Case sArticulo
            Set ADORs = oGestorInformes.AhorroNegociadoMatDesdeHastaArticulo(gParametrosInstalacion.giCargaMaximaCombos, txtFecDesde, txtFecHasta, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, sdbcGMN4_4Cod.Text, optReu, optDir, sdbcArtiCod.Text)
        Case sFechaFiltro
            If txtFecHasta <> "" Then
                Set ADORs = oGestorInformes.AhorroNegociadoMatDesdeHasta(CDate(txtFecDesde), CDate(txtFecHasta), sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, sdbcGMN4_4Cod.Text, optReu, optDir)
            Else
                Set ADORs = oGestorInformes.AhorroNegociadoMatDesdeHasta(CDate(txtFecDesde), , sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, sdbcGMN4_4Cod.Text, optReu, optDir)
            End If
    End Select
    
    Screen.MousePointer = vbNormal
    
    If ADORs Is Nothing Then Exit Sub
    
    CargarGrid
    
    If cmdGrid.Visible = True Then
'        MSChart1.Visible = True
'        MostrarGrafico sdbcTipoGrafico.Value
        Select Case sdbcFiltro.Text
            Case sMaterial
                MSChartMat.Visible = True
                MSChartArt.Visible = False
                MSChart1.Visible = False
                MostrarGraficoMat sdbcTipoGrafico.Value
                chkPrecUnitario.Visible = False
            Case sArticulo
                MSChartArt.Visible = True
                MSChartMat.Visible = False
                MSChart1.Visible = False
                sdbcTipoGrafico.Value = sIdiTipoGrafico(3) 'por defecto saldra cargado el grafico en lineas
                MostrarGraficoArt sdbcTipoGrafico.Value
                chkPrecUnitario.Visible = True
                chkPrecUnitario.Value = vbChecked
            Case sFechaFiltro
                MSChart1.Visible = True
                MSChartArt.Visible = False
                MSChartMat.Visible = False
                MostrarGrafico sdbcTipoGrafico.Value
                chkPrecUnitario.Value = False
        End Select
    End If
    
End Sub


Private Sub cmdBorrar_Click()
    sdbcGMN1_4Cod = ""
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub cmdGrafico_Click()
            
        If sdbgRes.Rows = 0 And sdbgResMat.Rows = 0 And sdbgResArt.Rows = 0 Then
            Exit Sub
        End If
    
        Screen.MousePointer = vbHourglass
        sdbcTipoGrafico = sIdiTipoGrafico(2)
        picTipoGrafico.Visible = True
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        sdbgResMat.Visible = False
        sdbgResArt.Visible = False
        Select Case sdbcFiltro.Text
            Case sMaterial
                MSChartMat.Visible = True
                MSChart1.Visible = False
                MSChartArt.Visible = False
                MostrarGraficoMat sIdiTipoGrafico(2)
                chkPrecUnitario.Visible = False
            Case sArticulo
                'Se mostrara el grafico del articulo seleccionado en la grid
                MSChartArt.Visible = True
                MSChartMat.Visible = False
                MSChart1.Visible = False
                sdbcTipoGrafico.Value = sIdiTipoGrafico(3)
                MostrarGraficoArt sIdiTipoGrafico(3), True
                chkPrecUnitario.Visible = True
                chkPrecUnitario.Value = vbChecked
            Case sFechaFiltro
                MSChart1.Visible = True
                MSChartArt.Visible = False
                MSChartMat.Visible = False
                MostrarGrafico sIdiTipoGrafico(2)
                chkPrecUnitario.Visible = False
        End Select
        
        Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdGrid_Click()
        
        picTipoGrafico.Visible = False
        picLegend.Visible = False
        picLegend2.Visible = False
        
        cmdGrafico.Visible = True
        cmdGrid.Visible = False
        chkPrecUnitario.Visible = False
        Select Case sdbcFiltro.Text
            Case sMaterial
                sdbgResMat.Visible = True
                sdbgResMat.Row = iFilaSeleccionada
                sdbgRes.Visible = False
                sdbgResArt.Visible = False
            Case sArticulo
                sdbgResArt.Visible = True
                sdbgResArt.Row = iFilaSeleccionada
                sdbgResMat.Visible = False
                sdbgRes.Visible = False
            Case sFechaFiltro
                sdbgRes.Visible = True
                sdbgResArt.Visible = False
                sdbgResMat.Visible = False
        End Select
        'sdbgRes.Visible = True
        MSChart1.Visible = False
        MSChartArt.Visible = False
        MSChartMat.Visible = False
        
End Sub

Private Sub cmdImprimir_Click()
Set ofrmLstAhorroNeg = New frmLstINFAhorrosNeg
    ofrmLstAhorroNeg.sOrigen = "frmInfAhorroNegMatDesde"
  
    ofrmLstAhorroNeg.WindowState = vbNormal
    
     If txtFecDesde <> "" Then
        ofrmLstAhorroNeg.txtFecDesde = txtFecDesde.Text
    End If
    If txtFecHasta <> "" Then
        ofrmLstAhorroNeg.txtFecHasta = txtFecHasta.Text
    End If
    If optReu Then ofrmLstAhorroNeg.optReu = True
    If optDir Then ofrmLstAhorroNeg.optDir = True
    If optTodos Then ofrmLstAhorroNeg.optTodos = True

    ofrmLstAhorroNeg.sdbcMon = sdbcMon.Text
    ofrmLstAhorroNeg.sdbcMon_Validate False
    
    ' hay que crear los objetos oGMNxSeleccionado para las restricciones por material
    ofrmLstAhorroNeg.PonerMatSeleccionado ("frmInfAhorroNegMatDesde")
    ofrmLstAhorroNeg.sdbcArtiCod.Value = sdbcArtiCod.Text
    ofrmLstAhorroNeg.sdbcArtiDen.Value = sdbcArtiDen.Text
    ofrmLstAhorroNeg.Show 1


End Sub

Private Sub cmdSelMat_Click()
      
    frmSELMAT.sOrigen = "frmInfAhorroNegMatDesde"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Hide
    frmSELMAT.Show 1

End Sub

Private Sub Form_Activate()
    
    sdbgRes.SelBookmarks.RemoveAll
    sdbgResMat.SelBookmarks.RemoveAll
    sdbgResArt.SelBookmarks.RemoveAll
    
End Sub

Private Sub Form_Load()
    Me.Height = 7725
    Me.Width = 13020
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
   
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        dequivalencia = oMonedas.Item(1).Equiv
    End If
       
    ConfigurarSeguridad
    ConfigurarNombres
    
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & ""
    
    sdbcFiltro.Text = sMaterial
    fraDesde.Height = 975
    sdbgResMat.Top = 1770
    sdbgResMat.Height = 9610
    sdbgResMat.Visible = True
    sdbgRes.Visible = False
    sdbgResArt.Visible = False
    MSChart1.Visible = False
    MSChartArt.Visible = False
    MSChartMat.Visible = False
    chkPrecUnitario.Visible = False
    
End Sub

Private Sub Form_Resize()
    
    If Me.Width > 150 Then
        
        sdbgRes.Width = Me.Width - 120
        
        sdbgRes.Columns(1).Width = (sdbgRes.Width - 570) * 0.14
        sdbgRes.Columns(2).Width = (sdbgRes.Width - 570) * 0.195
        sdbgRes.Columns(3).Width = (sdbgRes.Width - 570) * 0.08
        sdbgRes.Columns(4).Width = (sdbgRes.Width - 570) * 0.16
        sdbgRes.Columns(5).Width = (sdbgRes.Width - 570) * 0.16
        sdbgRes.Columns(6).Width = (sdbgRes.Width - 570) * 0.16
        sdbgRes.Columns(7).Width = (sdbgRes.Width - 570) * 0.105
        
        sdbgResMat.Width = Me.Width - 120
        sdbgResMat.Columns("GMN1").Width = (sdbgResMat.Width - 570) * 0.05
        sdbgResMat.Columns("GMN2").Width = (sdbgResMat.Width - 570) * 0.05
        sdbgResMat.Columns("GMN3").Width = (sdbgResMat.Width - 570) * 0.05
        sdbgResMat.Columns("GMN4").Width = (sdbgResMat.Width - 570) * 0.05
        sdbgResMat.Columns("Material").Width = (sdbgResMat.Width - 570) * 0.23
        sdbgResMat.Columns("NUMPROCE").Width = (sdbgResMat.Width - 570) * 0.05
        sdbgResMat.Columns("PRES").Width = (sdbgResMat.Width - 570) * 0.16
        sdbgResMat.Columns("ADJ").Width = (sdbgResMat.Width - 570) * 0.16
        sdbgResMat.Columns("AHO").Width = (sdbgResMat.Width - 570) * 0.15
        sdbgResMat.Columns("PORCEN").Width = (sdbgResMat.Width - 570) * 0.055
        
        sdbgResArt.Width = Me.Width - 120
        sdbgResArt.Columns("COD_ART").Width = (sdbgResArt.Width - 570) * 0.08
        sdbgResArt.Columns("DEN_ART").Width = (sdbgResArt.Width - 570) * 0.15
        sdbgResArt.Columns("PROCE").Width = (sdbgResArt.Width - 570) * 0.07
        sdbgResArt.Columns("PROVE").Width = (sdbgResArt.Width - 570) * 0.16
        sdbgResArt.Columns("FechaInicio").Width = (sdbgResArt.Width - 570) * 0.06
        sdbgResArt.Columns("FechaFin").Width = (sdbgResArt.Width - 570) * 0.06
        sdbgResArt.Columns("CANT_ADJ").Width = (sdbgResArt.Width - 570) * 0.08
        sdbgResArt.Columns("PRECIO").Width = (sdbgResArt.Width - 570) * 0.08
        sdbgResArt.Columns("PRES").Width = (sdbgResArt.Width - 570) * 0.07
        sdbgResArt.Columns("PREC").Width = (sdbgResArt.Width - 570) * 0.07
        sdbgResArt.Columns("AHO").Width = (sdbgResArt.Width - 570) * 0.07
        sdbgResArt.Columns("PORCEN").Width = (sdbgResArt.Width - 570) * 0.05
                
        sdbgTotales.Top = Me.Height - 780
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Columns(0).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(1).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(2).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(3).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(4).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(5).Width = sdbgTotales.Width / 8
        sdbgTotales.Columns(6).Width = sdbgTotales.Width / 8
        
        MSChart1.Width = Me.Width - 120
        MSChartArt.Width = Me.Width - 120
        MSChartMat.Width = Me.Width - 120
        
    End If
    
    If Me.Height > 2500 Then
        sdbgRes.Height = Me.Height - 2600
        sdbgResMat.Height = Me.Height - 2600
        sdbgResArt.Height = Me.Height - 3000
        MSChartArt.Height = Me.Height - 3100
        MSChartMat.Height = Me.Height - 2900
        MSChart1.Height = Me.Height - 2300
    End If
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set ADORs = Nothing
    Set oMonedas = Nothing
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Me.Visible = False
    
End Sub


''' <summary>
''' Cargar la grid con el resultado del informe
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cmdActualizar_click(); Tiempo m�ximo: 0</remarks>
Private Sub CargarGrid()
        
    Dim dpres As Double
    Dim dadj As Double
    Dim sFecha As String
    
    sColorActual = "Blanco"
    
    dpres = 0
    dadj = 0
    
    sdbgRes.RemoveAll
    sdbgResArt.RemoveAll
    sdbgResMat.RemoveAll
    sdbgTotales.RemoveAll
    
    While Not ADORs.EOF

        Select Case sdbcFiltro.Text
        Case sMaterial
            sdbgResMat.AddItem ADORs("GMN1").Value & Chr(m_lSeparador) & ADORs("GMN2").Value & Chr(m_lSeparador) & ADORs("GMN3").Value & Chr(m_lSeparador) & ADORs("GMN4").Value & Chr(m_lSeparador) & ADORs("DENMATERIAL").Value & Chr(m_lSeparador) & dequivalencia * ADORs("NUMPROCE").Value & Chr(m_lSeparador) & dequivalencia * ADORs("PRES").Value & Chr(m_lSeparador) & dequivalencia * ADORs("PREC").Value & Chr(m_lSeparador) & dequivalencia * ADORs("AHORRO").Value & Chr(m_lSeparador) & ADORs("AHORROPORC").Value
        Case sArticulo
            'Se distinguir� cada articulo por un color diferente
            If sCodArticuloAnt <> "" And sCodArticuloAnt <> ADORs("COD_ART").Value Then
                If sColorActual = "Blanco" Then
                    sColorActual = "Gris"
                Else
                    sColorActual = "Blanco"
                End If
            Else
                If sCodArticuloAnt = "" And sDenArticuloAnt <> ADORs("DEN_ART").Value Then
                    If sColorActual = "Blanco" Then
                        sColorActual = "Gris"
                    Else
                        sColorActual = "Blanco"
                    End If
                End If
            End If
            sdbgResArt.AddItem ADORs("FECULTREU").Value & Chr(m_lSeparador) & ADORs("COD_ART").Value & Chr(m_lSeparador) & ADORs("DEN_ART").Value & Chr(m_lSeparador) & ADORs("PROCE").Value & Chr(m_lSeparador) & ADORs("PROVE").Value & Chr(m_lSeparador) & ADORs("FECHAINICIO").Value & Chr(m_lSeparador) & ADORs("FECHAFIN").Value & Chr(m_lSeparador) & ADORs("CANT_ADJ").Value & Chr(m_lSeparador) & dequivalencia * ADORs("PRECIO").Value & Chr(m_lSeparador) & dequivalencia * ADORs("PRES").Value & Chr(m_lSeparador) & dequivalencia * ADORs("PREC").Value & Chr(m_lSeparador) & dequivalencia * ADORs("AHO").Value & Chr(m_lSeparador) & ADORs("PORCEN").Value & Chr(m_lSeparador) & sColorActual
                       
            If sCodArticuloAnt = "" Or sCodArticuloAnt <> ADORs("COD_ART") Or sDenArticuloAnt = "" Or sDenArticuloAnt <> ADORs("DEN_ART").Value Then
                sCodArticuloAnt = NullToStr(ADORs("COD_ART").Value)
                sDenArticuloAnt = NullToStr(ADORs("DEN_ART").Value)
            End If
               
        Case sFechaFiltro
            If ADORs("ADJDIR").Value = 0 Then
                sFecha = Format(ADORs(0).Value, "short date") & " " & Format(ADORs(0).Value, "short time")
            Else
                sFecha = Format(ADORs(0).Value, "short date")
            End If
            sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & sFecha & Chr(m_lSeparador) & ADORs.Fields("REF").Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & dequivalencia * ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & ADORs(5).Value
        End Select
        dpres = dpres + dequivalencia * ADORs("PRES").Value
        dadj = dadj + dequivalencia * ADORs("PREC").Value
        ADORs.MoveNext
    Wend
    If ADORs.RecordCount = gParametrosInstalacion.giCargaMaximaCombos Then
        sdbgResArt.AddItem "" & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..." & Chr(m_lSeparador) & "..."
    End If
    
    ADORs.Close
    Set ADORs = Nothing
    
   sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & dpres & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & dadj & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & dpres - dadj

End Sub
Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    Select Case Tipo
    
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                picLegend.Visible = True
                picLegend.Left = fraDesde.Width + 600
                picLegend2.Visible = False
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                picLegend.Visible = False
                picLegend2.Visible = True
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                picLegend.Visible = True
                picLegend.Left = fraDesde.Width + 600
                picLegend2.Visible = False
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " " '& sdbgRes.Columns(1).Value
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
           
End Sub
Private Sub MostrarGraficoArt(ByVal Tipo As String, Optional ByVal bPrecUnitario As Boolean)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
Dim iNumArt As Integer
Dim sCodArticulo As String
Dim iRow As Integer
    If chkPrecUnitario.Value = vbChecked Then
        picLegend.Visible = False
        picLegend2.Visible = False
    Else
       ' picLegend.Visible = True
        picLegend2.Visible = True
    End If
    If sdbgResArt.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    iNumArt = 0
    sdbgResArt.Row = iFilaSeleccionada
    Dim sDenArticulo As String
    sCodArticulo = sdbgResArt.Columns("COD_ART").Value
    sDenArticulo = sdbgResArt.Columns("DEN_ART").Value
    sdbgResArt.MoveFirst
    For i = 1 To sdbgResArt.Rows
        If sCodArticulo = sdbgResArt.Columns("COD_ART").Value Then
            iNumArt = iNumArt + 1
        End If
        sdbgResArt.MoveNext
    Next
    
    Select Case Tipo
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2) ' "Barras 2D", "Barras 3D"
            'Necesitamos cinco series
            ' Ahorro negativo
            ' Ahorro positivo
            ' Adjudicado
            ' Presupuestado
            'Adjudicado
            If chkPrecUnitario.Value = vbChecked Then
                
                ReDim ar(1 To iNumArt, 1 To 2)
                
                i = 1
                sdbgResArt.MoveFirst
                iRow = sdbgResArt.Row
                
                While iRow <= sdbgResArt.Rows - 1 And i <= iNumArt
                    If sdbgResArt.Columns("COD_ART").Value = sCodArticulo Then
                        ar(i, 1) = Left(sdbgResArt.Columns("FECHA").Text, 10)
                        If sdbgResArt.Columns("PRECIO").Value = "" Then
                            ar(i, 2) = 0
                        Else
                            ar(i, 2) = CDbl(sdbgResArt.Columns("PRECIO").Value)
                        End If
                        i = i + 1
                    End If
                    sdbgResArt.MoveNext
                    iRow = sdbgResArt.Row
                Wend
                
                MSChartArt.ChartData = ar
                
                MSChartArt.Title.Text = sCodArticulo & "-" & sDenArticulo
                MSChartArt.Title.VtFont.Size = 12
                MSChartArt.Title.VtFont.Style = VtFontStyleBold
                MSChartArt.ShowLegend = False
                MSChartArt.Stacking = True
                MSChartArt.Plot.View3d.Rotation = 60
                MSChartArt.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChartArt.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                
                If Tipo = sIdiTipoGrafico(2) Then '"Barras 3D"
                    MSChartArt.chartType = VtChChartType3dBar
                    MSChartArt.SeriesType = VtChSeriesType3dBar
                Else
                    MSChartArt.chartType = VtChChartType2dBar
                    MSChartArt.SeriesType = VtChSeriesType2dBar
                    
                End If
                'Precion Unitario
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdX).Labels
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
            Else
            
                ReDim ar(1 To iNumArt, 1 To 7)
                i = 1
                sdbgResArt.MoveFirst
                iRow = sdbgResArt.Row
                
                While iRow <= sdbgResArt.Rows - 1 And i <= iNumArt
                    If sdbgResArt.Columns("COD_ART").Value = sCodArticulo Then
                        ar(i, 1) = Left(sdbgResArt.Columns("FECHA").Text, 10)
                        If CDbl(sdbgResArt.Columns("PREC").Value) > 0 Then 'Si ahorro +
                            If CDbl(sdbgResArt.Columns("AHO").Value) > CDbl(sdbgResArt.Columns("PREC").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgResArt.Columns("PREC").Value)
                                ar(i, 4) = CDbl(sdbgResArt.Columns("AHO").Value) - CDbl(sdbgResArt.Columns("PREC").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgResArt.Columns("PRES").Value) - CDbl(sdbgResArt.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgResArt.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgResArt.Columns("PREC").Value) - CDbl(sdbgResArt.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgResArt.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else 'Si ahorro-
                            ar(i, 2) = CDbl(sdbgResArt.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgResArt.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgResArt.Columns("AHO").Value)
                        End If
                        i = i + 1
                    End If
                    sdbgResArt.MoveNext
                    iRow = sdbgResArt.Row
                Wend
                
                MSChartArt.ChartData = ar
                MSChartArt.Title.Text = sCodArticulo & "-" & sDenArticulo
                MSChartArt.Title.VtFont.Size = 12
                MSChartArt.Title.VtFont.Style = VtFontStyleBold
                If Tipo = sIdiTipoGrafico(2) Then '"Barras 3D"
                    MSChartArt.chartType = VtChChartType3dBar
                    MSChartArt.SeriesType = VtChSeriesType3dBar
                Else
                    MSChartArt.chartType = VtChChartType2dBar
                    MSChartArt.SeriesType = VtChSeriesType2dBar
                End If
                
                MSChartArt.ShowLegend = False
                MSChartArt.Stacking = True
                MSChartArt.Plot.View3d.Rotation = 60
                MSChartArt.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChartArt.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChartArt.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChartArt.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChartArt.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChartArt.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChartArt.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdX).Labels
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
            End If
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4) ' "Lineas 2D", "Lineas 3D"
            If bPrecUnitario Then
                If Tipo = sIdiTipoGrafico(4) Then ' "Lineas 3D"
                    MSChartArt.chartType = VtChChartType3dLine
                    MSChartArt.SeriesType = VtChSeriesType3dLine
                    MSChartArt.Stacking = False
                Else
                    MSChartArt.chartType = VtChChartType2dLine
                    MSChartArt.SeriesType = VtChSeriesType2dLine
                    MSChartArt.Stacking = False
                End If
                
                ReDim ar(1 To iNumArt, 1 To 2)
                i = 1
                sdbgResArt.MoveFirst
                            
                iRow = sdbgResArt.Row
                While iRow < sdbgResArt.Rows And i <= iNumArt
                    If sdbgResArt.Columns("COD_ART").Value = sCodArticulo Then
                        ar(i, 1) = Left(sdbgResArt.Columns("FECHA").Text, 10)
                        If sdbgResArt.Columns("PRECIO").Value = "" Then
                            ar(i, 2) = 0
                        Else
                            ar(i, 2) = CDbl(sdbgResArt.Columns("PRECIO").Value)
                        End If
                        i = i + 1
                    End If
                    sdbgResArt.MoveNext
                    iRow = sdbgResArt.Row
                Wend
                
                MSChartArt.ChartData = ar
                MSChartArt.Title.Text = sCodArticulo & "-" & sDenArticulo
                MSChartArt.Title.VtFont.Size = 12
                MSChartArt.Title.VtFont.Style = VtFontStyleBold
                MSChartArt.ShowLegend = False
                
                'Precio Unitario
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeAbovePoint
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).DataPointLabel.VtFont.Size = 11
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).DataPointLabel.VtFont.Style = VtFontStyleBold
                
                
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdX).Labels
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
            Else
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                If Tipo = sIdiTipoGrafico(4) Then ' "Lineas 3D"
                    MSChartArt.chartType = VtChChartType3dLine
                    MSChartArt.SeriesType = VtChSeriesType3dLine
                    MSChartArt.Stacking = False
                Else
                    MSChartArt.chartType = VtChChartType2dLine
                    MSChartArt.SeriesType = VtChSeriesType2dLine
                    MSChartArt.Stacking = False
                End If
                
                ReDim ar(1 To iNumArt, 1 To 4)
                i = 1
                sdbgResArt.MoveFirst
                            
                iRow = sdbgResArt.Row
                While iRow < sdbgResArt.Rows - 1 And i <= iNumArt
                    If sdbgResArt.Columns("COD_ART").Value = sCodArticulo Then
                        ar(i, 1) = Left(sdbgResArt.Columns("FECHA").Text, 10)
                        ar(i, 2) = CDbl(sdbgResArt.Columns("PREC").Value)
                        ar(i, 3) = CDbl(sdbgResArt.Columns("PRES").Value)
                        ar(i, 4) = CDbl(sdbgResArt.Columns("AHO").Value)
                        i = i + 1
                    End If
                    sdbgResArt.MoveNext
                    iRow = sdbgResArt.Row
                Wend
                
                MSChartArt.ChartData = ar
                MSChartArt.Title.Text = sCodArticulo & "-" & sDenArticulo
                MSChartArt.Title.VtFont.Size = 12
                MSChartArt.Title.VtFont.Style = VtFontStyleBold
                MSChartArt.ShowLegend = False
                
                'Adjudicado
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
                'Presupuestado
                MSChartArt.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                MSChartArt.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
                'Ahorrado
                MSChartArt.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                MSChartArt.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
                                
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdX).Labels
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
            End If
        Case sIdiTipoGrafico(5) '  "Tarta"
            'Necesitamos cuatro series
            ' Adjudicado positivo +
            ' Presupuesto
            ' Ahorro positivo
            ' Ahorro negativo
            
            If bPrecUnitario Then
                ReDim ar(1 To iNumArt, 1 To 2)
                i = 1
                sdbgResArt.MoveFirst
                iRow = sdbgResArt.Row
            
                While iRow <= sdbgResArt.Rows - 1 And i <= iNumArt
                    If sdbgResArt.Columns("COD_ART").Value = sCodArticulo Then
                        ar(i, 1) = Left(sdbgResArt.Columns("FECHA").Text, 10)
                        If sdbgResArt.Columns("PRECIO").Value = "" Then
                            ar(i, 2) = 0
                        Else
                            ar(i, 2) = CDbl(sdbgResArt.Columns("PRECIO").Value)
                        End If
                        i = i + 1
                    End If
                    sdbgResArt.MoveNext
                    iRow = sdbgResArt.Row
                Wend
                
                MSChartArt.chartType = VtChChartType2dPie
                MSChartArt.SeriesType = VtChSeriesType2dPie
                MSChartArt.ChartData = ar
                MSChartArt.Title.Text = sCodArticulo & "-" & sDenArticulo
                MSChartArt.Title.VtFont.Size = 12
                MSChartArt.Title.VtFont.Style = VtFontStyleBold
                MSChartArt.ShowLegend = False
                MSChartArt.Stacking = True
                MSChartArt.Plot.View3d.Rotation = 60
                MSChartArt.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Adjudicado
'                MSChartArt.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
'                'Ahorro positivo
'                MSChartArt.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
'                'Adjudicado
'                MSChartArt.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
'                'Prespuestado
'                MSChartArt.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
'                'Adjudicado
'                MSChartArt.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdX).Labels
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
            Else
            
                ReDim ar(1 To iNumArt, 1 To 7)
                i = 1
                sdbgResArt.MoveFirst
                iRow = sdbgResArt.Row
            
                While iRow < sdbgResArt.Rows - 1 And i <= iNumArt
                    If sCodArticulo = sdbgResArt.Columns("COD_ART").Value Then
                        ar(i, 1) = Left(sdbgResArt.Columns("FECHA").Text, 10)
                            'Si ahorro +
                        If CDbl(sdbgResArt.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgResArt.Columns("AHO").Value) > CDbl(sdbgResArt.Columns("PREC").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgResArt.Columns("PREC").Value)
                                ar(i, 4) = CDbl(sdbgResArt.Columns("AHO").Value) - CDbl(sdbgResArt.Columns("PREC").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgResArt.Columns("PRES").Value) - CDbl(sdbgResArt.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgResArt.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgResArt.Columns("PREC").Value) - CDbl(sdbgResArt.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgResArt.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgResArt.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgResArt.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgResArt.Columns("AHO").Value)
                        End If
                        i = i + 1
                    End If
                    sdbgResArt.MoveNext
                    iRow = sdbgResArt.Row
                Wend
                MSChartArt.chartType = VtChChartType2dPie
                MSChartArt.SeriesType = VtChSeriesType2dPie
                MSChartArt.ChartData = ar
                MSChartArt.Title.Text = sCodArticulo & "-" & sDenArticulo
                MSChartArt.Title.VtFont.Size = 12
                MSChartArt.Title.VtFont.Style = VtFontStyleBold
                MSChartArt.ShowLegend = False
                MSChartArt.Stacking = True
                MSChartArt.Plot.View3d.Rotation = 60
                MSChartArt.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChartArt.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChartArt.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChartArt.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChartArt.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChartArt.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChartArt.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdX).Labels
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                For Each lbl In MSChartArt.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
        End If
    End Select
End Sub
Private Sub MostrarGraficoMat(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
Dim sGmn1 As String
Dim sGmn2 As String
Dim sGmn3 As String
Dim sGmn4 As String
Dim adoFechas As ADODB.Recordset
    If sdbgResMat.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
     
    sGmn1 = sdbgResMat.Columns("GMN1").Value
    sGmn2 = sdbgResMat.Columns("GMN2").Value
    sGmn3 = sdbgResMat.Columns("GMN3").Value
    sGmn4 = sdbgResMat.Columns("GMN4").Value
    Set adoFechas = oGestorInformes.AhorroNegociadoMatDesdeHastaMaterialFECHA(txtFecDesde, txtFecHasta, sGmn1, sGmn2, sGmn3, sGmn4, optReu, optDir)
   
    Select Case Tipo
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2) ' "Barras 2D", "Barras 3D"
            'Necesitamos cinco series
            ' Ahorro negativo
            ' Ahorro positivo
            ' Adjudicado
            ' Presupuestado
            'Adjudicado
            
            picLegend.Visible = True
            picLegend.Left = fraDesde.Width + 600
            picLegend2.Visible = False
                
            ReDim ar(1 To adoFechas.RecordCount, 1 To 7)
            i = 1
            
            adoFechas.MoveFirst
            While i <= adoFechas.RecordCount
                ar(i, 1) = Left(adoFechas("FECULTREU").Value, 10)
                If CDbl(adoFechas("AHORRO").Value) > 0 Then 'Si ahorro +
                    If CDbl(adoFechas("AHORRO").Value) > CDbl(adoFechas("PREC").Value) Then
                        ar(i, 2) = Null
                        ar(i, 3) = CDbl(adoFechas("PREC").Value)
                        ar(i, 4) = CDbl(adoFechas("AHORRO").Value) - CDbl(adoFechas("PREC").Value)
                        ar(i, 5) = Null
                        ar(i, 6) = CDbl(adoFechas("PRES").Value) - CDbl(adoFechas("AHORRO").Value)
                        ar(i, 7) = Null
                    Else
                        ar(i, 2) = Null
                        ar(i, 3) = Null
                        ar(i, 4) = CDbl(adoFechas("AHORRO").Value)
                        ar(i, 5) = CDbl(adoFechas("PREC").Value) - CDbl(adoFechas("AHORRO").Value)
                        ar(i, 6) = CDbl(adoFechas("AHORRO").Value)
                        ar(i, 7) = Null
                    End If
                Else 'Si ahorro-
                    ar(i, 2) = CDbl(adoFechas("AHORRO").Value)
                    ar(i, 3) = Null
                    ar(i, 4) = Null
                    ar(i, 5) = Null
                    ar(i, 6) = CDbl(adoFechas("PRES").Value)
                    ar(i, 7) = -CDbl(adoFechas("AHORRO").Value)
                End If
                i = i + 1
                adoFechas.MoveNext
            Wend
            
            MSChartMat.ChartData = ar

            If Tipo = sIdiTipoGrafico(2) Then '"Barras 3D"
                MSChartMat.chartType = VtChChartType3dBar
                MSChartMat.SeriesType = VtChSeriesType3dBar
            Else
                MSChartMat.chartType = VtChChartType2dBar
                MSChartMat.SeriesType = VtChSeriesType2dBar
            End If
            
            MSChartMat.ShowLegend = False
            MSChartMat.Stacking = True
            MSChartMat.Plot.View3d.Rotation = 60
            MSChartMat.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
            MSChartMat.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                
            'Ahorro negativo
            MSChartMat.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
            'Adjudicado
            MSChartMat.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
            'Ahorro positivo
            MSChartMat.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
            'Adjudicado
            MSChartMat.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
            'Prespuestado
            MSChartMat.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
            'Adjudicado
            MSChartMat.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
            
            For Each lbl In MSChartMat.Plot.Axis(VtChAxisIdX).Labels
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 10 '12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
            Next
            For Each lbl In MSChartMat.Plot.Axis(VtChAxisIdY).Labels
                lbl.VtFont.Size = 10 '12
                lbl.VtFont.Style = VtFontStyleBold
                lbl.Format = "#.00#"
            Next
            For Each lbl In MSChartMat.Plot.Axis(VtChAxisIdY2).Labels
                lbl.VtFont.Size = 10 ' 12
                lbl.VtFont.Style = VtFontStyleBold
                lbl.Format = "#.00#"
            Next

        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4) ' "Lineas 2D", "Lineas 3D"
            'Necesitamos tres series
            ' Adjudicado
            ' Presupuesto
            ' Ahorro
            picLegend.Visible = False
            picLegend2.Visible = True

            If Tipo = sIdiTipoGrafico(4) Then ' "Lineas 3D"
                MSChartMat.chartType = VtChChartType3dLine
                MSChartMat.SeriesType = VtChSeriesType3dLine
                MSChartMat.Stacking = False
            Else
                MSChartMat.chartType = VtChChartType2dLine
                MSChartMat.SeriesType = VtChSeriesType2dLine
                MSChartMat.Stacking = False
            End If
            
            ReDim ar(1 To adoFechas.RecordCount, 1 To 4)
            i = 1
            adoFechas.MoveFirst
            
            While i <= adoFechas.RecordCount
                ar(i, 1) = Left(adoFechas("FECULTREU").Value, 10)
                ar(i, 2) = CDbl(adoFechas("PREC").Value)
                ar(i, 3) = CDbl(adoFechas("PRES").Value)
                ar(i, 4) = CDbl(adoFechas("AHORRO").Value)
                i = i + 1
                adoFechas.MoveNext
            Wend
            
            MSChartMat.ChartData = ar
            MSChartMat.ShowLegend = False
            
            'Adjudicado
            MSChartMat.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
            'Presupuestado
            MSChartMat.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
            'Ahorrado
            MSChartMat.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                            
            For Each lbl In MSChartMat.Plot.Axis(VtChAxisIdX).Labels
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 10 '12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
            Next
            For Each lbl In MSChartMat.Plot.Axis(VtChAxisIdY).Labels
                lbl.VtFont.Size = 10 '12
                lbl.VtFont.Style = VtFontStyleBold
                lbl.Format = "#.00#"
            Next
            
            For Each lbl In MSChartMat.Plot.Axis(VtChAxisIdY2).Labels
                lbl.VtFont.Size = 10 ' 12
                lbl.VtFont.Style = VtFontStyleBold
                lbl.Format = "#.00#"
            Next
                
        Case sIdiTipoGrafico(5) '  "Tarta"
            'Necesitamos cuatro series
            ' Adjudicado positivo +
            ' Presupuesto
            ' Ahorro positivo
            ' Ahorro negativo
            
            picLegend.Visible = True
            picLegend.Left = fraDesde.Width + 600
            picLegend2.Visible = False
            
            ReDim ar(1 To adoFechas.RecordCount, 1 To 7)
            i = 1
            adoFechas.MoveFirst
            While i <= adoFechas.RecordCount
                 ar(i, 1) = Left(adoFechas("FECULTREU").Value, 10)
                    'Si ahorro +
                If CDbl(sdbgResMat.Columns("AHO").Value) > 0 Then
                    If CDbl(adoFechas("AHORRO").Value) > CDbl(adoFechas("PREC").Value) Then
                        ar(i, 2) = Null
                        ar(i, 3) = CDbl(adoFechas("PREC").Value)
                        ar(i, 4) = CDbl(adoFechas("AHORRO").Value) - CDbl(adoFechas("PREC").Value)
                        ar(i, 5) = Null
                        ar(i, 6) = CDbl(sdbgResMat.Columns("PRES").Value) - CDbl(adoFechas("AHORRO").Value)
                        ar(i, 7) = Null
                    Else
                        ar(i, 2) = Null
                        ar(i, 3) = Null
                        ar(i, 4) = CDbl(adoFechas("AHORRO").Value)
                        ar(i, 5) = CDbl(adoFechas("PREC").Value) - CDbl(adoFechas("AHORRO").Value)
                        ar(i, 6) = CDbl(adoFechas("AHORRO").Value)
                        ar(i, 7) = Null
                    End If
                Else
                'Si ahorro-
                    ar(i, 2) = CDbl(adoFechas("AHORRO").Value)
                    ar(i, 3) = Null
                    ar(i, 4) = Null
                    ar(i, 5) = Null
                    ar(i, 6) = CDbl(adoFechas("PRES").Value)
                    ar(i, 7) = -CDbl(adoFechas("AHORRO").Value)
                End If
                i = i + 1
                adoFechas.MoveNext
            Wend
            MSChartMat.chartType = VtChChartType2dPie
            MSChartMat.SeriesType = VtChSeriesType2dPie
            MSChartMat.ChartData = ar
            MSChartMat.ShowLegend = False
            MSChartMat.Stacking = True
            MSChartMat.Plot.View3d.Rotation = 60
            MSChartMat.Legend.VtFont.Size = 8.25
            'Ahorro negativo
            MSChartMat.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
            'Adjudicado
            MSChartMat.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
            'Ahorro positivo
            MSChartMat.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
            'Adjudicado
            MSChartMat.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
            'Prespuestado
            MSChartMat.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
            'Adjudicado
            MSChartMat.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
             
            For Each lbl In MSChartMat.Plot.Axis(VtChAxisIdX).Labels
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 10 '12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
            Next
            For Each lbl In MSChartMat.Plot.Axis(VtChAxisIdY).Labels
                lbl.VtFont.Size = 10 ' 12
                lbl.VtFont.Style = VtFontStyleBold
                lbl.Format = "#.00#"
            Next
            For Each lbl In MSChartMat.Plot.Axis(VtChAxisIdY2).Labels
                lbl.VtFont.Size = 10 '12
                lbl.VtFont.Style = VtFontStyleBold
                lbl.Format = "#.00#"
            Next
    End Select
End Sub

 Public Sub PonerMatSeleccionadoEnCombos()
 
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
        sdbgResArt.RemoveAll
        sdbgResMat.RemoveAll
    Else
        MSChart1.Visible = False
        MSChartArt.Visible = False
        MSChartMat.Visible = False
    End If
    
    BorrarDatosTotales
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    If Not oGMN1Seleccionado Is Nothing Then
       
        bRespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Den.Text = oGMN1Seleccionado.Den
        bRespetarCombo = False
    Else
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN1_4Den.Text = ""
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    If Not oGMN2Seleccionado Is Nothing Then
    
        bRespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Den.Text = oGMN2Seleccionado.Den
        bRespetarCombo = False
    Else
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN2_4Den.Text = ""
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    If Not oGMN3Seleccionado Is Nothing Then
       
        bRespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Den.Text = oGMN3Seleccionado.Den
        bRespetarCombo = False
    Else
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
    End If
      
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    If Not oGMN4Seleccionado Is Nothing Then
   
        bRespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        sdbcGMN4_4Den.Text = oGMN4Seleccionado.Den
        bRespetarCombo = False
    Else
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
    End If
    
           
End Sub

Private Sub optDir_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optReu_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optTodos_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub sdbcArtiCod_Change()
    
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcArtiDen.Text = ""
        bRespetarCombo = False
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcArtiCod_Click()
    If Not sdbcArtiCod.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiCod_CloseUp()
    If sdbcArtiCod.Value = "..." Then
        sdbcArtiCod.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiCod.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcArtiDen.Text = sdbcArtiCod.Columns(1).Text
    sdbcArtiCod.Text = sdbcArtiCod.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcArtiCod_DropDown()
    Dim oArt As CArticulo
    
    sdbcArtiCod.RemoveAll

    Screen.MousePointer = vbHourglass
    GMN4Seleccionado
    If oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos
    For Each oArt In oGMN4Seleccionado.ARTICULOS
        sdbcArtiCod.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
    Next

    sdbcArtiCod.SelStart = 0
    sdbcArtiCod.SelLength = Len(sdbcArtiCod.Text)
    sdbcArtiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiCod_InitColumnProps()
    sdbcArtiCod.DataFieldList = "Column 0"
    sdbcArtiCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcArtiCod_PositionList(ByVal Text As String)
PositionList sdbcArtiCod, Text
End Sub

Public Sub sdbcArtiCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcArtiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, sdbcArtiCod, , True
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4Seleccionado.ARTICULOS.Count = 0)
            
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiCod.Text = ""
    Else
        bRespetarCombo = True
        sdbcArtiDen.Text = oGMN4Seleccionado.ARTICULOS.Item(1).Den
        
        sdbcArtiCod.Columns(0).Value = sdbcArtiCod.Text
        sdbcArtiCod.Columns(1).Value = sdbcArtiDen.Text
        
        bRespetarCombo = False
        bCargarComboDesde = False
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        Set oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub

Private Sub sdbcArtiDen_Click()
    
    If Not sdbcArtiDen.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiDen_CloseUp()
    
    If sdbcArtiDen.Value = "..." Then
        sdbcArtiDen.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcArtiCod.Text = sdbcArtiDen.Columns(1).Text
    sdbcArtiDen.Text = sdbcArtiDen.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcArtiDen_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcArtiCod.Text = ""
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcArtiDen_DropDown()
    Dim oArt As CArticulo
    
    sdbcArtiDen.RemoveAll
   
    Screen.MousePointer = vbHourglass
    GMN4Seleccionado

    If oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
    Else
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , True
    End If
    
    
    For Each oArt In oGMN4Seleccionado.ARTICULOS
   
        sdbcArtiDen.AddItem oArt.Den & Chr(m_lSeparador) & oArt.Cod
   
    Next

    sdbcArtiDen.SelStart = 0
    sdbcArtiDen.SelLength = Len(sdbcArtiDen.Text)
    sdbcArtiDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiDen_InitColumnProps()
    
    sdbcArtiDen.DataFieldList = "Column 0"
    sdbcArtiDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcArtiDen_PositionList(ByVal Text As String)
PositionList sdbcArtiDen, Text
End Sub

Private Sub sdbcArtiDen_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcArtiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4Seleccionado.ARTICULOS.Count = 0)
            
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiDen.Text = ""
    Else
        bRespetarCombo = True
        sdbcArtiCod.Text = oGMN4Seleccionado.ARTICULOS.Item(1).Cod
        
        sdbcArtiDen.Columns(0).Value = sdbcArtiDen.Text
        sdbcArtiDen.Columns(1).Value = sdbcArtiCod.Text
        
        bRespetarCombo = False
        bCargarComboDesde = False
    End If
    If Not oGMN4Seleccionado Is Nothing Then
       Set oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub


Private Sub sdbcFiltro_CloseUp()
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    sdbgRes.RemoveAll
    sdbgResArt.RemoveAll
    sdbgResMat.RemoveAll
    Select Case sdbcFiltro.Text
        Case sMaterial
            sdbgTotales.Top = Me.Height - 780
            fraDesde.Height = 975
            sdbgResMat.Visible = True
            sdbgResMat.Top = fraDesde.Top + fraDesde.Height + 75 ' 1755
            sdbgResMat.Height = Me.Height - sdbgResMat.Top - sdbgTotales.Height - 500 ' 9610
            sdbgRes.Visible = False
            sdbgResArt.Visible = False
        Case sArticulo
            sdbgTotales.Top = Me.Height - 780
            fraDesde.Height = 1410
            sdbgResArt.Top = fraDesde.Top + fraDesde.Height + 75 ' 2190
            sdbgResArt.Height = Me.Height - sdbgResArt.Top - sdbgTotales.Height - 500 ' 9170
            sdbgResArt.Visible = True
            sdbgResMat.Visible = False
            sdbgRes.Visible = False
        Case sFechaFiltro
            sdbgTotales.Top = Me.Height - 780
            fraDesde.Height = 975
            sdbgRes.Top = fraDesde.Top + fraDesde.Height + 75 '1755
            sdbgRes.Height = Me.Height - sdbgResMat.Top - sdbgTotales.Height - 500 '9610
            sdbgRes.Visible = True
            sdbgResMat.Visible = False
            sdbgResArt.Visible = False
    End Select
    MSChart1.Visible = False
    MSChartArt.Visible = False
    MSChartMat.Visible = False
    picLegend.Visible = False
    picLegend2.Visible = False
    picTipoGrafico.Visible = False
    cmdGrafico.Visible = True
    cmdGrid.Visible = False
    BorrarDatosTotales
End Sub

Private Sub sdbcFiltro_DropDown()
    sdbcFiltro.RemoveAll
    sdbcFiltro.AddItem sMaterial
    sdbcFiltro.MoveNext
    sdbcFiltro.AddItem sArticulo
    sdbcFiltro.MoveNext
    sdbcFiltro.AddItem sFechaFiltro
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not bRespetarCombo Then
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        
        bRespetarCombo = True
        sdbcGMN1_4Cod = ""
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
        bRespetarCombo = False

        sdbgRes.RemoveAll
        sdbgResArt.RemoveAll
        sdbgResMat.RemoveAll
        sdbgTotales.RemoveAll
    End If
End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
       
    If sdbcGMN1_4Den.Value = "....." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
    
    
End Sub

Private Sub sdbcGMN1_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Den.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
   
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , True, False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "....."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub


Private Sub sdbcGMN1_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        
        bRespetarCombo = True
        sdbcGMN1_4Den = ""
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
        bRespetarCombo = False

        sdbgRes.RemoveAll
        MSChart1.Visible = False
        MSChartArt.Visible = False
        MSChartMat.Visible = False
        sdbgResArt.RemoveAll
        sdbgResMat.RemoveAll
        BorrarDatosTotales

    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
        
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
    
       
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
   
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
          
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub
Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
        
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
        bRespetarCombo = False
        GMN1Seleccionado
        Exit Sub
    End If
        
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text Then
        bRespetarCombo = True
        sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
        bRespetarCombo = False
        GMN1Seleccionado
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
        
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        
        Screen.MousePointer = vbHourglass
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False)
        Screen.MousePointer = vbNormal
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
         
            bCargarComboDesde = False
            
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
       
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
            
            bRespetarCombo = True
            sdbcGMN1_4Den = oGMN1.Den
            bRespetarCombo = False
            bCargarComboDesde = False
            
        End If
    
    End If
    
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
        sdbgResArt.RemoveAll
        sdbgResMat.RemoveAll
    End If
    
End Sub

Private Sub sdbcGMN2_4Cod_CloseUp()
    
    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    bRespetarCombo = False
    
    GMN2Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN2_4Cod.RemoveAll
    
    Set oGruposMN2 = Nothing
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
         
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , False, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , False, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    End If
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Cod_InitColumnProps()

    sdbcGMN2_4Cod.DataFieldList = "Column 0"
    sdbcGMN2_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub
Private Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN2 As CGrupoMatNivel2
    Dim oIMAsig As IMaterialAsignado
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
       
        Screen.MousePointer = vbHourglass
        Set oGruposMN2 = Nothing
        Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Screen.MousePointer = vbNormal
        
        If oGruposMN2 Is Nothing Then
            'No existe
            sdbcGMN2_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            If oGruposMN2.Item(1) Is Nothing Then
                'No existe
                sdbcGMN2_4Cod.Text = ""
                oMensajes.NoValido sIdiMat
            Else
                bRespetarCombo = True
                sdbcGMN2_4Den.Text = oGruposMN2.Item(1).Den
                bRespetarCombo = False
                Set oGMN2Seleccionado = oGruposMN2.Item(1)
                bCargarComboDesde = False
            End If
        End If
    Else
        Set oGMN2 = Nothing
        Set oGMN2 = oFSGSRaiz.generar_CGrupoMatNivel2
       
        oGMN2.GMN1Cod = sdbcGMN1_4Cod
        oGMN2.Cod = sdbcGMN2_4Cod
        Set oIBaseDatos = oGMN2
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN2_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            bRespetarCombo = True
            sdbcGMN2_4Den.Text = oGMN2.Den
            bRespetarCombo = False
            Set oGMN2Seleccionado = oGMN2
            bCargarComboDesde = False
        End If
    
    End If
    
    
    Set oGMN2 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN2 = Nothing
    Set oIMAsig = Nothing
    
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
        
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
        sdbgResArt.RemoveAll
        sdbgResMat.RemoveAll
    End If
    
End Sub

Private Sub sdbcGMN2_4Den_CloseUp()
    
    If sdbcGMN2_4Den.Value = "....." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    bRespetarCombo = False
    
    GMN2Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN2_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , Trim(sdbcGMN2_4Den), True, False)
        Else
           
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN2_4Den), True, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , True, False)
        Else
            
             oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    End If
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "....."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()

    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN3_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
        sdbgResArt.RemoveAll
        sdbgResMat.RemoveAll
        
    End If
    
End Sub

Private Sub sdbcGMN3_4Cod_CloseUp()
    
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    
    bRespetarCombo = False
    
    GMN3Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN3_4Cod.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oGruposMN3 = Nothing
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , False, False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    End If
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Cod_InitColumnProps()

    sdbcGMN3_4Cod.DataFieldList = "Column 0"
    sdbcGMN3_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub
Private Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN3 As CGrupoMatNivel3
    Dim oIMAsig As IMaterialAsignado
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    If sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
          
        Screen.MousePointer = vbHourglass
        Set oGruposMN3 = Nothing
        Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Screen.MousePointer = vbNormal
        
        If oGruposMN3 Is Nothing Then
            'No existe
            sdbcGMN3_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            If oGruposMN2.Item(1) Is Nothing Then
                'No existe
                sdbcGMN3_4Cod.Text = ""
                oMensajes.NoValido sIdiMat
            Else
                bRespetarCombo = True
                sdbcGMN3_4Den.Text = oGruposMN3.Item(1).Den
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
    Else
        Set oGMN3 = Nothing
        Set oGMN3 = oFSGSRaiz.generar_CGrupoMatNivel3
       
        oGMN3.GMN1Cod = sdbcGMN1_4Cod
        oGMN3.GMN2Cod = sdbcGMN2_4Cod
        oGMN3.Cod = sdbcGMN3_4Cod
        Set oIBaseDatos = oGMN3
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN3_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            bRespetarCombo = True
            sdbcGMN3_4Den.Text = oGMN3.Den
            bRespetarCombo = False
            bCargarComboDesde = False
        End If
    
    End If
    
    GMN3Seleccionado
    
    Set oGMN3 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN3 = Nothing
    Set oIMAsig = Nothing
    
End Sub
Private Sub sdbcGMN3_4den_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
        sdbgResArt.RemoveAll
        sdbgResMat.RemoveAll
    End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
    
    If sdbcGMN3_4Den.Value = "....." Or sdbcGMN3_4Den.Value = "" Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    bRespetarCombo = False
    
    GMN3Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN3_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN3_4Den.RemoveAll
    
    Set oGruposMN3 = Nothing
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , Trim(sdbcGMN3_4Den), True, False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN3_4Den), True, False
            
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , True, False)
        Else
             
             oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    End If
    
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "....."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()

    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub

Private Sub GMN1Seleccionado()
        
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    sdbcGMN2_4Cod = ""
    sdbcGMN2_4Den = ""
    sdbcGMN3_4Cod = ""
    sdbcGMN3_4Den = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    sdbgRes.RemoveAll
    sdbgResArt.RemoveAll
    sdbgResMat.RemoveAll
    BorrarDatosTotales
    MSChart1.Visible = False
    MSChartArt.Visible = False
    MSChartMat.Visible = False
    Select Case sdbcFiltro.Text
        Case sMaterial
            sdbgResMat.Visible = True
            sdbgResArt.Visible = False
            sdbgRes.Visible = False
        Case sArticulo
            sdbgResArt.Visible = True
            sdbgResMat.Visible = False
            sdbgRes.Visible = False
        Case sFechaFiltro
            sdbgResArt.Visible = False
            sdbgResMat.Visible = False
            sdbgRes.Visible = True
    End Select
End Sub

Private Sub GMN2Seleccionado()
    
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
  
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod.Text
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    sdbcGMN3_4Cod = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    sdbgRes.RemoveAll
    sdbgResMat.RemoveAll
    sdbgResArt.RemoveAll
    BorrarDatosTotales
    MSChart1.Visible = False
    MSChartArt.Visible = False
    MSChartMat.Visible = False
    Select Case sdbcFiltro.Text
         Case sMaterial
             sdbgResMat.Visible = True
             sdbgResArt.Visible = False
             sdbgRes.Visible = False
         Case sArticulo
             sdbgResArt.Visible = True
             sdbgResMat.Visible = False
             sdbgRes.Visible = False
         Case sFechaFiltro
             sdbgResArt.Visible = False
             sdbgResMat.Visible = False
             sdbgRes.Visible = True
     End Select
End Sub
Private Sub GMN3Seleccionado()

    Set oGMN3Seleccionado = Nothing
    
    If sdbcGMN3_4Cod.Text = "" Or sdbcGMN3_4Den.Text = "" Then Exit Sub
     
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
 
    sdbcGMN4_4Cod = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    
    sdbgRes.RemoveAll
    sdbgResArt.RemoveAll
    sdbgResMat.RemoveAll
    BorrarDatosTotales
    MSChart1.Visible = False
    MSChartArt.Visible = False
    MSChartMat.Visible = False
    Select Case sdbcFiltro.Text
        Case sMaterial
            sdbgResMat.Visible = True
            sdbgResArt.Visible = False
            sdbgRes.Visible = False
        Case sArticulo
            sdbgResArt.Visible = True
            sdbgResMat.Visible = False
            sdbgRes.Visible = False
        Case sFechaFiltro
            sdbgResArt.Visible = False
            sdbgResMat.Visible = False
            sdbgRes.Visible = True
    End Select
End Sub

Private Sub GMN4Seleccionado()
    
    Set oGMN4Seleccionado = Nothing
    
    If sdbcGMN4_4Cod.Text = "" Or sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    sdbgRes.RemoveAll
    sdbgResMat.RemoveAll
    sdbgResArt.RemoveAll
    BorrarDatosTotales
    MSChart1.Visible = False
    MSChartArt.Visible = False
    MSChartMat.Visible = False
    Select Case sdbcFiltro.Text
         Case sMaterial
             sdbgResMat.Visible = True
             sdbgResArt.Visible = False
             sdbgRes.Visible = False
         Case sArticulo
             sdbgResArt.Visible = True
             sdbgResMat.Visible = False
             sdbgRes.Visible = False
         Case sFechaFiltro
             sdbgResArt.Visible = False
             sdbgResMat.Visible = False
             sdbgRes.Visible = True
     End Select
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegMatRestMat)) Is Nothing) And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
        bRMat = True
    End If
End Sub
Private Sub BorrarDatosTotales()
    sdbgTotales.RemoveAll
    sdbgTotales.Columns(6).CellStyleSet "Normal"
    sdbgTotales.Refresh
End Sub

Private Sub sdbcGMN4_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
        bRespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        bCargarComboDesde = True
        sdbgResArt.RemoveAll
        sdbgResMat.RemoveAll
        sdbgRes.RemoveAll
                
    End If
    
End Sub

Private Sub sdbcGMN4_4Cod_CloseUp()
    
    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    bRespetarCombo = False
    GMN4Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN4_4Cod.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oGruposMN4 = Nothing
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , False, False
            Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
            Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        End If
    End If
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN4_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4Cod_InitColumnProps()

    sdbcGMN4_4Cod.DataFieldList = "Column 0"
    sdbcGMN4_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub
Private Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN4 As CGrupoMatNivel4
    Dim oIMAsig As IMaterialAsignado
    
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    If sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        
        Screen.MousePointer = vbHourglass
        Set oGruposMN4 = Nothing
        Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , False)
        Screen.MousePointer = vbNormal
        
        If oGruposMN4 Is Nothing Then
            'No existe
            sdbcGMN4_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            If oGruposMN4.Item(1) Is Nothing Then
                'No existe
                sdbcGMN4_4Cod.Text = ""
                oMensajes.NoValido sIdiMat
            Else
                bRespetarCombo = True
                sdbcGMN4_4Den.Text = oGruposMN4.Item(1).Den
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
    Else
        Set oGMN4 = Nothing
        Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
      
        oGMN4.GMN1Cod = sdbcGMN1_4Cod
        oGMN4.GMN2Cod = sdbcGMN2_4Cod
        oGMN4.GMN3Cod = sdbcGMN3_4Cod
        oGMN4.Cod = sdbcGMN4_4Cod
        Set oIBaseDatos = oGMN4
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN4_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            bRespetarCombo = True
            sdbcGMN4_4Den.Text = oGMN4.Den
            bRespetarCombo = False
            bCargarComboDesde = False
        End If
    
    End If
    GMN4Seleccionado
    Set oGMN4 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN4 = Nothing
    Set oIMAsig = Nothing
    
End Sub
Private Sub sdbcGMN4_4Den_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        sdbgResArt.RemoveAll
        sdbgResMat.RemoveAll
        sdbgRes.RemoveAll
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()
    
    If sdbcGMN4_4Den.Value = "....." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    bRespetarCombo = False
    GMN4Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN4_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN4_4Den.RemoveAll
    
    Set oGruposMN4 = Nothing
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
       
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , Trim(sdbcGMN4_4Den), True, False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN4_4Den), True, False
            Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , True, False)
        Else
             
             oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        End If
    End If
    
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "....."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()

    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub
Private Sub sdbcTipoGrafico_Click()
    Select Case sdbcFiltro.Text
        Case sMaterial
            MostrarGraficoMat sdbcTipoGrafico.Value
        Case sArticulo
            MostrarGraficoArt sdbcTipoGrafico.Value, chkPrecUnitario.Value
        Case sFechaFiltro
            MostrarGrafico sdbcTipoGrafico.Value
    End Select
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    Select Case sdbcFiltro.Text
        Case sMaterial
            MostrarGraficoMat sdbcTipoGrafico.Value
        Case sArticulo
            MostrarGraficoArt sdbcTipoGrafico.Value, chkPrecUnitario.Value
        Case sFechaFiltro
            MostrarGrafico sdbcTipoGrafico.Value
    End Select
End Sub

Private Sub sdbgRes_DblClick()
Dim frm As frmInfAhorroNegDetalle
Dim frm2 As frmInfAhorroNegFechaDetalle

    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub

    Set ADORs = Nothing
    
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN4_4Cod <> "" Then
        
         Set ADORs = oGestorInformes.AhorroNegociadoFecha(sdbgRes.Columns(0).Value, sdbcGMN1_4Cod.Value, sdbcGMN2_4Cod.Value, sdbcGMN3_4Cod.Value, sdbcGMN4_4Cod.Value, , , , , , True, , , optReu, optDir)
            
            If ADORs Is Nothing Then Exit Sub
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm2 = New frmInfAhorroNegFechaDetalle
            frm2.sGmn1 = sdbcGMN1_4Cod.Value
            frm2.sGmn2 = sdbcGMN2_4Cod.Value
            frm2.sGmn3 = sdbcGMN3_4Cod.Value
            frm2.sGmn4 = sdbcGMN4_4Cod.Value
            frm2.dequivalencia = dequivalencia
            frm2.sDetalle = "GMN4"
            frm2.sFecha = sdbgRes.Columns(0).Value
            While Not ADORs.EOF
                frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                ADORs.MoveNext
            Wend
            
            frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm2.Show 1
            sdbgRes.SelBookmarks.RemoveAll
            
            ADORs.Close
            Set ADORs = Nothing
            Exit Sub
        
    Else
        If sdbcGMN3_4Cod <> "" Then
            Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbgRes.Columns(0).Value), sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , True, , , , optReu, optDir)
            If ADORs Is Nothing Then Exit Sub
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm = New frmInfAhorroNegDetalle
            frm.dequivalencia = dequivalencia
            frm.sDetalle = "GMN3"
            frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsabr_GMN4
            frm.sdbgRes.Columns(0).TagVariant = "GMN4"
            frm.sFecha = sdbgRes.Columns(0).Value
        Else
            If sdbcGMN2_4Cod <> "" Then
                Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbgRes.Columns(0).Value), sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, , , , , , True, , , , optReu, optDir)
                If ADORs Is Nothing Then Exit Sub
                    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                    Set frm = New frmInfAhorroNegDetalle
                    frm.sDetalle = "GMN2"
                    frm.dequivalencia = dequivalencia
                    frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsabr_GMN3
                    frm.sdbgRes.Columns(0).TagVariant = "GMN3"
                    frm.sFecha = sdbgRes.Columns(0).Value
            Else
                If sdbcGMN1_4Cod <> "" Then
                    
                    Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbgRes.Columns(0).Value), sdbcGMN1_4Cod.Text, , , , , , , True, , , , optReu, optDir)
                    If ADORs Is Nothing Then Exit Sub
                    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                    Set frm = New frmInfAhorroNegDetalle
                    frm.sDetalle = "GMN1"
                    frm.dequivalencia = dequivalencia
                    frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsABR_GMN2
                    frm.sdbgRes.Columns(0).TagVariant = "GMN2"
                    frm.dequivalencia = dequivalencia
                    frm.sFecha = sdbgRes.Columns(0).Value
                Else
                    If bRMat Then
                        Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbgRes.Columns(0).Value), , , , , , , , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, optReu, optDir)
                    Else
                        Set ADORs = oGestorInformes.AhorroNegociadomatFecha(CDate(sdbgRes.Columns(0).Value), , , , , , , , True, , , , optReu, optDir)
                    End If
                    If ADORs Is Nothing Then Exit Sub
                    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                    Set frm = New frmInfAhorroNegDetalle
                    frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsabr_GMN1
                    frm.sdbgRes.Columns(0).TagVariant = "GMN1"
                    frm.dequivalencia = dequivalencia
                    frm.sFecha = sdbgRes.Columns(0).Value
                End If
            End If
        End If
    End If

    frm.caption = sIdiDetResult & "      " & sdbgRes.Columns(0).Value
    frm.Fecha = CDate(sdbgRes.Columns(0).Value)
    frm.SoloDeAdjDir = optDir
    frm.SoloEnReunion = optReu
    frm.sGmn1 = sdbcGMN1_4Cod.Value
    frm.sGmn2 = sdbcGMN2_4Cod.Value
    frm.sGmn3 = sdbcGMN3_4Cod.Value
    frm.sGmn4 = sdbcGMN4_4Cod.Value
    
    If Not ADORs Is Nothing Then
        While Not ADORs.EOF
            frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
            ADORs.MoveNext
        Wend
    End If
    ADORs.Close
    Set ADORs = Nothing
    frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 1000
    frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
    Screen.MousePointer = vbNormal
    frm.Show 1
    
    sdbgRes.SelBookmarks.RemoveAll
            
End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
        
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If

End Sub

Private Sub sdbgResArt_Click()
    Dim sCodArticuloAnterior As String
    
    If sdbgResArt.Rows <= 0 Then Exit Sub
    
    If sdbgResArt.Columns("COD_ART").Value = "..." Then
        'Volver a cargar la lista de articulos con los siguientes articulos
        sdbgResArt.Row = sdbgResArt.Row - 1
        sCodArticuloAnterior = sdbgResArt.Columns("COD_ART").Value
        sDenArticuloAnt = sdbgResArt.Columns("DEN_ART").Value
        Set ADORs = oGestorInformes.AhorroNegociadoMatDesdeHastaArticulo(gParametrosInstalacion.giCargaMaximaCombos, txtFecDesde, txtFecHasta, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, sdbcGMN4_4Cod.Text, optReu, optDir, , sCodArticuloAnterior, sDenArticuloAnt)
        CargarGrid
    Else
        iFilaSeleccionada = sdbgResArt.Row
    End If
End Sub

Private Sub sdbgResArt_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Variant
    
    If sdbgResArt.Columns("AHO").Value < 0 Then
        sdbgResArt.Columns("AHO").CellStyleSet "Red"
        sdbgResArt.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgResArt.Columns("AHO").Value > 0 Then
            sdbgResArt.Columns("AHO").CellStyleSet "Green"
            sdbgResArt.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
    
    For i = 1 To sdbgResArt.Columns.Count - 1
        If sdbgResArt.Columns(i).Name <> "AHO" And sdbgResArt.Columns(i).Name <> "PORCEN" Then
            sdbgResArt.Columns(i).CellStyleSet sdbgResArt.Columns("color").Value
        End If
    Next
   
End Sub

Private Sub sdbgResMat_Click()
    iFilaSeleccionada = sdbgResMat.Row
End Sub

''' <summary>
''' Doble click sobre el grid de ahorros negociados para un material
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema (); Tiempo m�ximo: 0</remarks>

Private Sub sdbgResMat_DblClick()
    'se mostrara el detalle de la estructura de material
    Dim frm As frmInfAhorroNegDetalle
    Dim frm2 As frmInfAhorroNegFechaDetalle

    If sdbgResMat.Rows = 0 Then Exit Sub
    If sdbgResMat.Row < 0 Then Exit Sub

    Set ADORs = Nothing
    
    Screen.MousePointer = vbHourglass
    
    If sdbcGMN4_4Cod <> "" Then
    
            If frmInfAhorroNegMatDesde.sdbcFiltro.Text = frmInfAhorroNegMatDesde.sFechaFiltro Then
                Set ADORs = oGestorInformes.AhorroNegociadoFecha("", sdbgResMat.Columns("GMN1").Value, sdbgResMat.Columns("GMN2").Value, sdbgResMat.Columns("GMN3").Value, sdbgResMat.Columns("GMN4").Value, , , , , , True, , , optReu, optDir)
            Else
                Set ADORs = oGestorInformes.AhorroNegociadoFecha("", sdbgResMat.Columns("GMN1").Value, sdbgResMat.Columns("GMN2").Value, sdbgResMat.Columns("GMN3").Value, sdbgResMat.Columns("GMN4").Value, , , , , , True, , , optReu, optDir, txtFecDesde.Text, txtFecHasta.Text)
            End If
            
            If ADORs Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm2 = New frmInfAhorroNegFechaDetalle
            frm2.dequivalencia = dequivalencia
            frm2.sGmn1 = sdbgResMat.Columns("GMN1").Value
            frm2.sGmn2 = sdbgResMat.Columns("GMN2").Value
            frm2.sGmn3 = sdbgResMat.Columns("GMN3").Value
            frm2.sGmn4 = sdbgResMat.Columns("GMN4").Value
            frm2.sDetalle = "GMN4"
            frm2.sFecha = ""
            
            While Not ADORs.EOF
                frm2.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value & Chr(m_lSeparador) & ADORs("FECULTREU").Value
                ADORs.MoveNext
            Wend
            
            frm2.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm2.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm2.Show 1
            sdbgRes.SelBookmarks.RemoveAll
            
            Set ADORs = Nothing
    Else
    
        If sdbcGMN3_4Cod <> "" Then
                    
            If frmInfAhorroNegMatDesde.sdbcFiltro.Text = frmInfAhorroNegMatDesde.sFechaFiltro Then
                Set ADORs = oGestorInformes.AhorroNegociadomatFecha("", sdbgResMat.Columns("GMN1").Value, sdbgResMat.Columns("GMN2").Value, sdbgResMat.Columns("GMN3").Value, , , , , , True, , , optReu, optDir, txtFecDesde.Text, txtFecHasta.Text)
            Else
                Set ADORs = oGestorInformes.AhorroNegociadomatFecha("", sdbgResMat.Columns("GMN1").Value, sdbgResMat.Columns("GMN2").Value, sdbgResMat.Columns("GMN3").Value, , , , , , True, , , optReu, optDir, txtFecDesde.Text, txtFecHasta.Text)
            End If
            
            If ADORs Is Nothing Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            Set frm = New frmInfAhorroNegDetalle
            frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsabr_GMN4
            frm.sdbgRes.Columns(0).TagVariant = "GMN4"
            'frm.Fecha = ""
            frm.SoloDeAdjDir = optDir
            frm.SoloEnReunion = optReu
            frm.sGmn1 = sdbgResMat.Columns("GMN1").Value
            frm.sGmn2 = sdbgResMat.Columns("GMN2").Value
            frm.sGmn3 = sdbgResMat.Columns("GMN3").Value
            frm.dequivalencia = dequivalencia
            frm.sDetalle = "GMN3"
            frm.sFecha = ""
            frm.sMaterial = sMaterial
            While Not ADORs.EOF
                frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                ADORs.MoveNext
            Wend
            
            frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
            frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
            Screen.MousePointer = vbNormal
            
            frm.Show 1
            sdbgRes.SelBookmarks.RemoveAll
            
            Set ADORs = Nothing
        
        
        Else
        
            If sdbcGMN2_4Cod <> "" Then
            
                If frmInfAhorroNegMatDesde.sdbcFiltro.Text = frmInfAhorroNegMatDesde.sFechaFiltro Then
                    Set ADORs = oGestorInformes.AhorroNegociadomatFecha("", sdbgResMat.Columns("GMN1").Value, sdbgResMat.Columns("GMN2").Value, , , , , , , True, , , optReu, optDir, txtFecDesde.Text, txtFecHasta.Text)
                Else
                    Set ADORs = oGestorInformes.AhorroNegociadomatFecha("", sdbgResMat.Columns("GMN1").Value, sdbgResMat.Columns("GMN2").Value, , , , , , , True, , , optReu, optDir, txtFecDesde.Text, txtFecHasta.Text)
                End If
                
                If ADORs Is Nothing Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                Set frm = New frmInfAhorroNegDetalle
                frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsabr_GMN3
                frm.sdbgRes.Columns(0).TagVariant = "GMN3"
                'frm.Fecha = ""
                frm.SoloDeAdjDir = optDir
                frm.SoloEnReunion = optReu
                frm.sGmn1 = sdbgResMat.Columns("GMN1").Value
                frm.sGmn2 = sdbgResMat.Columns("GMN2").Value
                frm.dequivalencia = dequivalencia
                frm.sDetalle = "GMN2"
                frm.sFecha = ""
                frm.sMaterial = sMaterial
                While Not ADORs.EOF
                    frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                    ADORs.MoveNext
                Wend
                
                frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 450
                frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
                
                Screen.MousePointer = vbNormal
                
                frm.Show 1
                sdbgRes.SelBookmarks.RemoveAll
                
                Set ADORs = Nothing
            Else
                        
                If sdbcGMN1_4Cod <> "" Then
            
                    Set ADORs = oGestorInformes.AhorroNegociadomatFecha("", sdbgResMat.Columns("GMN1").Value, , , , , , , True, , , , optReu, optDir, txtFecDesde.Text, txtFecHasta.Text)
            
                    If ADORs Is Nothing Then Exit Sub
                    sdbgResMat.SelBookmarks.Add sdbgResMat.Bookmark
                    Set frm = New frmInfAhorroNegDetalle
                    frm.sDetalle = "GMN1"
                    frm.dequivalencia = dequivalencia
                    frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsABR_GMN2
                    frm.sdbgRes.Columns(0).TagVariant = "GMN2"
                    frm.dequivalencia = dequivalencia
                    frm.sFecha = "" ' sdbgResMat.Columns(0).Value
                    frm.sMaterial = sMaterial
                Else
                    If bRMat Then
                        Set ADORs = oGestorInformes.AhorroNegociadomatFecha("", sdbgResMat.Columns("GMN1").Value, , , , , , , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, optReu, optDir, txtFecDesde.Text, txtFecHasta.Text)
                    Else
                        Set ADORs = oGestorInformes.AhorroNegociadomatFecha("", sdbgResMat.Columns("GMN1").Value, , , , , , , True, , , , optReu, optDir, txtFecDesde.Text, txtFecHasta.Text)
                    End If
                    If ADORs Is Nothing Then Exit Sub
                    sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
                    Set frm = New frmInfAhorroNegDetalle
                    frm.sdbgRes.Columns(0).caption = basParametros.gParametrosGenerales.gsabr_GMN1
                    frm.sdbgRes.Columns(0).TagVariant = "GMN2"
                    frm.dequivalencia = dequivalencia
                    frm.sMaterial = sMaterial
                End If
            
                frm.caption = sIdiDetResult & "      " & sdbgResMat.Columns("FECHA").Value
               ' frm.Fecha = CDate(sdbgResMat.Columns("FECHA").Value)
                frm.SoloDeAdjDir = optDir
                frm.SoloEnReunion = optReu
                frm.sGmn1 = sdbgResMat.Columns("GMN1").Value
                frm.sGmn2 = sdbgResMat.Columns("GMN2").Value
                frm.sGmn3 = sdbgResMat.Columns("GMN3").Value
                frm.sGmn4 = sdbgResMat.Columns("GMN4").Value
            
                If Not ADORs Is Nothing Then
                    While Not ADORs.EOF
                        frm.sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & dequivalencia * ADORs(5).Value & Chr(m_lSeparador) & ADORs(6).Value
                        ADORs.MoveNext
                    Wend
                End If
                ADORs.Close
                Set ADORs = Nothing
                frm.Top = sdbgResMat.Top + Me.Top + MDI.Top + sdbgResMat.RowTop(sdbgResMat.Row) + sdbgResMat.RowHeight + 1000
                frm.Left = sdbgResMat.Left + Left + 400 + MDI.Left
            
                Screen.MousePointer = vbNormal
                frm.Show 1
            
                sdbgResMat.SelBookmarks.RemoveAll
            
            
            End If
        End If
        
    End If
    
End Sub


Private Sub sdbgResMat_RowLoaded(ByVal Bookmark As Variant)
    
    If sdbgResMat.Columns("AHO").Value < 0 Then
        sdbgResMat.Columns("AHO").CellStyleSet "Red"
        sdbgResMat.Columns("PORCEN").CellStyleSet "Red"
    Else
         If sdbgResMat.Columns("AHO").Value > 0 Then
            sdbgResMat.Columns("AHO").CellStyleSet "Green"
            sdbgResMat.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub sdbgTotales_RowLoaded(ByVal Bookmark As Variant)
If sdbgTotales.Columns(6).Value = "" Then Exit Sub
    
    If sdbgTotales.Columns(6).Value < 0 Then
        sdbgTotales.Columns(6).CellStyleSet "Red"
    Else
        If sdbgTotales.Columns(6).Value > 0 Then
            sdbgTotales.Columns(6).CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
            
End Sub

Private Sub sdbcMon_DropDown()

    sdbcMon.RemoveAll
    
    oMonedas.CargarTodasLasMonedas , , , , , False, True
      
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)
PositionList sdbcMon, Text
End Sub
Private Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
    End If
    
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_NEGMATDESDE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value
        Ador.MoveNext
        optReu.caption = Ador(0).Value
        Ador.MoveNext
        optDir.caption = Ador(0).Value '5
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value '8 Denominaci�n
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        sdbcArtiCod.Columns(1).caption = Ador(0).Value
        sdbcArtiDen.Columns(0).caption = Ador(0).Value
        sdbgResMat.Columns("MATERIAL").caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.RemoveAll '10
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        fraDesde.caption = Ador(0).Value '15
        sIdiMat = Ador(0).Value
        sMaterial = Ador(0).Value
        
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        sdbcArtiCod.Columns(0).caption = Ador(0).Value
        sdbcArtiDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        sdbgResMat.Columns("NUMPROCE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value
        Ador.MoveNext '27
        sdbgRes.Columns(5).caption = Ador(0).Value
        sdbgResMat.Columns("ADJ").caption = Ador(0).Value
        sdbgResArt.Columns("ADJ").caption = Ador(0).Value
        sIdiAdj = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(6).caption = Ador(0).Value
        sdbgResMat.Columns("AHO").caption = Ador(0).Value
        sdbgResArt.Columns("AHO").caption = Ador(0).Value
        sIdiAhor = Ador(0).Value '22 Ahorro
        Ador.MoveNext
        sIdiFecha = Ador(0).Value
        sFechaFiltro = Ador(0).Value
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value
        Ador.MoveNext
        sIdiTotal = Ador(0).Value 'Total
        Ador.MoveNext
        sIdiPresup = Ador(0).Value 'Presupuesto
        sdbgResArt.Columns("PRES").caption = Ador(0).Value
        sdbgResMat.Columns("PRES").caption = Ador(0).Value
        Ador.MoveNext
        sIdiDetResult = Ador(0).Value 'Detalle de resultado con fecha
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
        sdbgResMat.Columns("REF").caption = Ador(0).Value
        Ador.MoveNext
        sArticulo = Ador(0).Value '32
        sdbgResArt.Columns("COD_ART").caption = Ador(0).Value
        Ador.MoveNext
        sdbgResArt.Columns("Proveedor").caption = Ador(0).Value
        Ador.MoveNext
        sdbgResArt.Columns("Proceso").caption = Ador(0).Value
        Ador.MoveNext
        sdbgResArt.Columns("FechaInicio").caption = Ador(0).Value 'fechainicio
        Ador.MoveNext
        sdbgResArt.Columns("FechaFin").caption = Ador(0).Value 'fechafin
        Ador.MoveNext
        sdbgResArt.Columns("Descripcion").caption = Ador(0).Value
        Ador.MoveNext
        sdbgResArt.Columns("CANTADJ").caption = Ador(0).Value '38 Cantidad adj.
        Ador.MoveNext
        sdbgResArt.Columns("PRECIO").caption = Ador(0).Value
        
        sdbgResMat.Columns("GMN1").caption = gParametrosGenerales.gsabr_GMN1
        sdbgResMat.Columns("GMN2").caption = gParametrosGenerales.gsABR_GMN2
        sdbgResMat.Columns("GMN3").caption = gParametrosGenerales.gsabr_GMN3
        sdbgResMat.Columns("GMN4").caption = gParametrosGenerales.gsabr_GMN4
        Ador.MoveNext
        chkPrecUnitario.caption = Ador(0).Value
        
        Ador.Close
    
    End If

    picLegend.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGMATDESDE_PICLEGEND + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)
    picLegend2.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_NEGMATDESDE_PICLEGEND2 + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)

    Set Ador = Nothing

End Sub

Private Sub txtFecDesde_Change()
    MSChart1.Visible = False
    MSChartArt.Visible = False
    MSChartMat.Visible = False
    sdbgResArt.RemoveAll
    sdbgResMat.RemoveAll
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub txtFecHasta_Change()
    MSChart1.Visible = False
    MSChartArt.Visible = False
    MSChartMat.Visible = False
    sdbgResArt.RemoveAll
    sdbgResMat.RemoveAll
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub
