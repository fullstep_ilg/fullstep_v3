VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPRESPorMat 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de presupuestos por material (Opciones)"
   ClientHeight    =   2775
   ClientLeft      =   225
   ClientTop       =   1530
   ClientWidth     =   5910
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPRESPorMat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2775
   ScaleWidth      =   5910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   5910
      TabIndex        =   9
      Top             =   2400
      Width           =   5910
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   4515
         TabIndex        =   10
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2355
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   5850
      _ExtentX        =   10319
      _ExtentY        =   4154
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DSelecci�n"
      TabPicture(0)   =   "frmLstPRESPorMat.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frmSel"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Timer1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "DOrden"
      TabPicture(1)   =   "frmLstPRESPorMat.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "DOpciones"
      TabPicture(2)   =   "frmLstPRESPorMat.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraTipoOrg"
      Tab(2).ControlCount=   1
      Begin VB.Frame fraTipoOrg 
         Height          =   1800
         Left            =   -74850
         TabIndex        =   15
         Top             =   360
         Width           =   5625
         Begin VB.CheckBox chkIncluirItems 
            Caption         =   "DIncluir art�culos"
            Height          =   195
            Left            =   360
            TabIndex        =   7
            Top             =   500
            Width           =   4485
         End
         Begin VB.CheckBox chkIncluirAdj 
            Caption         =   "DIncluir hist�rico de �ltimas adjudicaciones"
            Height          =   255
            Left            =   360
            TabIndex        =   8
            Top             =   830
            Width           =   4485
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4425
         Top             =   120
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1800
         Left            =   -74850
         TabIndex        =   14
         Top             =   360
         Width           =   5625
         Begin VB.OptionButton opOrdDen 
            Caption         =   "DDenominaci�n"
            Height          =   195
            Left            =   450
            TabIndex        =   6
            Top             =   1040
            Width           =   2205
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "DC�digo"
            Height          =   195
            Left            =   450
            TabIndex        =   5
            Top             =   540
            Value           =   -1  'True
            Width           =   2235
         End
      End
      Begin VB.Frame frmSel 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1800
         Left            =   150
         TabIndex        =   12
         Top             =   360
         Width           =   5625
         Begin VB.OptionButton opPais 
            Caption         =   "DListar Rama:"
            Height          =   195
            Left            =   120
            TabIndex        =   2
            Top             =   1230
            Width           =   1590
         End
         Begin VB.OptionButton opTodos 
            Caption         =   "DListado completo"
            Height          =   195
            Left            =   120
            TabIndex        =   1
            Top             =   840
            Value           =   -1  'True
            Width           =   4590
         End
         Begin VB.TextBox txtEstMat 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1905
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   1185
            Width           =   2865
         End
         Begin VB.CommandButton cmdSelMat 
            Height          =   315
            Left            =   5145
            Picture         =   "frmLstPRESPorMat.frx":0D06
            Style           =   1  'Graphical
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   1170
            Width           =   345
         End
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   4815
            Picture         =   "frmLstPRESPorMat.frx":0D72
            Style           =   1  'Graphical
            TabIndex        =   3
            Top             =   1170
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
            Height          =   285
            Left            =   1125
            TabIndex        =   0
            Top             =   240
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin VB.Label lblAnyo 
            Caption         =   "DA�o:"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   120
            TabIndex        =   13
            Top             =   300
            Width           =   525
         End
      End
   End
End
Attribute VB_Name = "frmLstPRESPorMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de seguridad
Public bRMat As Boolean

'Recordset desconectados para los ttx
Public g_adoresDG As Ador.Recordset
Public g_adoresArt As Ador.Recordset

'Anyo seleccionado
Public iAnyoSeleccionado As Integer

' Variables de idioma
Private m_stxtIdiMat As String
Private sEspera(1 To 3) As String
Private m_stxtPag As String
Private m_stxtDe As String
Private m_stxtSeleccion As String
Private m_stxtTitulo As String
Private m_stxtAnio As String
Private m_stxtArticulo As String
Private m_stxtUnidad As String
Private m_stxtCant As String
Private m_stxtPres As String
Private m_stxtObj As String
Private m_stxtAhorro As String
Private m_stxtAhorroPorcen As String
Private m_stxtAdjudicaciones As String
Private m_stxtDest As String
Private m_stxtCodProve As String
Private m_stxtDenProve  As String
Private m_stxtPrec  As String
Private m_stxtFecIni As String
Private m_stxtFecFin As String
Private m_stxtTodosMat As String

'Variables para materiales
Private sGMN1Cod As String
Private sGMN2Cod As String
Private sGMN3Cod As String
Private sGMN4Cod As String


Private Sub chkIncluirItems_Click()
    If chkIncluirItems.Value = vbChecked Then
        chkIncluirAdj.Enabled = True
    Else
        chkIncluirAdj.Value = vbUnchecked
        chkIncluirAdj.Enabled = False
    End If
End Sub

Private Sub cmdBorrar_Click()
    txtEstMat.Text = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    
End Sub

Private Sub cmdObtener_Click()
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim oReport As CRAXDRT.Report
    Dim oCRPresPorMat As CRPresPorMat
    Dim SelectionText As String
    Dim pv As Preview
    Dim bMostrarReport As Boolean
    Dim GroupOrder(1 To 2, 1 To 4) As String
    Dim i As Integer
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Set oCRPresPorMat = GenerarCRPresPorMat
    
    'Verificar:
    If opPais.Value = True Then
        If sGMN1Cod = "" And sGMN2Cod = "" And sGMN3Cod = "" And sGMN4Cod = "" Then
            oMensajes.SeleccioneMaterial
            Set oReport = Nothing
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Set oReport = Nothing
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPRESArt.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Set oReport = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
       
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN1NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN1 & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN2NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN2 & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN3NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN3 & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN4NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN4 & ":" & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & m_stxtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & m_stxtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAnio")).Text = """" & m_stxtAnio & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "ANIO")).Text = """" & iAnyoSeleccionado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPres")).Text = """" & m_stxtPres & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtObj")).Text = """" & m_stxtObj & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAhorro")).Text = """" & m_stxtAhorro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPorcen")).Text = """" & m_stxtAhorroPorcen & """"

    SelectionText = ""
    If sGMN1Cod <> "" Then
        SelectionText = m_stxtIdiMat & ": " & gParametrosGenerales.gsDEN_GMN1 & ":" & sGMN1Cod
    
        If sGMN2Cod <> "" Then
            SelectionText = SelectionText & " - " & gParametrosGenerales.gsDEN_GMN2 & ":" & sGMN2Cod
            
            If sGMN3Cod <> "" Then
                SelectionText = SelectionText & " - " & gParametrosGenerales.gsDEN_GMN3 & ":" & sGMN3Cod
                
                If sGMN4Cod <> "" Then
                    SelectionText = SelectionText & " - " & gParametrosGenerales.gsDEN_GMN4 & ":" & sGMN4Cod
                End If
            End If
        End If
    Else
        SelectionText = m_stxtTodosMat
    End If
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    
    'Grupos:
    GroupOrder(2, 1) = "ORDEN_GRUPO1"  ' Nivel material 1
    GroupOrder(2, 2) = "ORDEN_GRUPO2"  ' Nivel material 2
    GroupOrder(2, 3) = "ORDEN_GRUPO3"  ' Nivel material 3
    GroupOrder(2, 4) = "ORDEN_GRUPO4"  ' Nivel material 4
    If opOrdCod = True Then
        GroupOrder(1, 1) = "{PresGMN1_ttx.GMN1}"
        GroupOrder(1, 2) = "{PresGMN1_ttx.GMN2}"
        GroupOrder(1, 3) = "{PresGMN1_ttx.GMN3}"
        GroupOrder(1, 4) = "{PresGMN1_ttx.GMN4}"
    Else
        GroupOrder(1, 1) = "{PresGMN1_ttx.DEN1}"
        GroupOrder(1, 2) = "{PresGMN1_ttx.DEN2}"
        GroupOrder(1, 3) = "{PresGMN1_ttx.DEN3}"
        GroupOrder(1, 4) = "{PresGMN1_ttx.DEN4}"
    End If
    For i = 1 To UBound(GroupOrder, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, GroupOrder(2, i))).Text = GroupOrder(1, i)
    Next i
    
    bMostrarReport = ListadoPresupuestoPorMaterial(oReport)
    If bMostrarReport = False Then
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sEspera(1)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    
    Set pv = New Preview
    pv.Hide
    pv.caption = m_stxtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set pv = Nothing
    
End Sub

Private Sub cmdSelMat_Click()
        
    frmSELMAT.sOrigen = "frmLstPRESPorMat"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Show 1

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPRES_PORMAT, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        SSTab1.TabCaption(0) = Ador(0).Value      '1 Selecci�n
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value      '2 Orden
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value        '3 &Obtener
        Ador.MoveNext
        '4 Cod
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value   '5 C�digo
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value '6 Denominacion
        Ador.MoveNext
        frmLstPRESPorMat.caption = Ador(0).Value   '7 Listado de presupuestos - Material (Opciones)
        Ador.MoveNext
        'opOrdPres.Caption = Ador(0).Value  '8 Presupuesto
        Ador.MoveNext
        'opOrdObj.Caption = Ador(0).Value  '9 Objetivo
        m_stxtObj = Ador(0).Value
        Ador.MoveNext
        m_stxtIdiMat = Ador(0).Value '10 Material
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value  '11 A�o
        
        Ador.MoveNext
        sEspera(1) = Ador(0).Value  '12 Generando listado de presupuestos - Material
        Ador.MoveNext
        sEspera(2) = Ador(0).Value  '13 Seleccionando registros ...
        Ador.MoveNext
        sEspera(3) = Ador(0).Value  '14 Visualizando listado ...
   
        Ador.MoveNext
        m_stxtPag = Ador(0).Value '15 P�g.
        Ador.MoveNext
        m_stxtDe = Ador(0).Value  '16 /
        Ador.MoveNext
        m_stxtSeleccion = Ador(0).Value  '17 Selecci�n:
        Ador.MoveNext
        m_stxtTitulo = Ador(0).Value '18 Listado de presupuestos - Material
        Ador.MoveNext
        m_stxtAnio = Ador(0).Value  '19 PRESUPUESTOS DEL A�O:
        Ador.MoveNext
        m_stxtPres = Ador(0).Value  '20 Presupuesto
        Ador.MoveNext
        'm_stxtObj = ador(0).Value  '21 Objetivo
        Ador.MoveNext
        m_stxtArticulo = Ador(0).Value  ' 22 Art�culo
        Ador.MoveNext
        m_stxtCant = Ador(0).Value '23 Cantidad
        Ador.MoveNext
        m_stxtUnidad = Ador(0).Value '24 Unidad
        Ador.MoveNext
        m_stxtAhorro = Ador(0).Value '25 Ahorro
        Ador.MoveNext
        m_stxtAhorroPorcen = Ador(0).Value '26 Ahorro%
        
        Ador.MoveNext
        SSTab1.TabCaption(2) = Ador(0).Value '27 Opciones
        
        Ador.MoveNext
        opTodos.caption = Ador(0).Value '28 Listado completo
        Ador.MoveNext
        opPais.caption = Ador(0).Value '29 Listar rama
        
        Ador.MoveNext
        chkIncluirItems.caption = Ador(0).Value '30 Incluir art�culos
        Ador.MoveNext
        chkIncluirAdj.caption = Ador(0).Value '31 Incluir hist�rico de �ltimas adjudicaciones
        
        Ador.MoveNext
        m_stxtAdjudicaciones = Ador(0).Value  '32 �ltimas adjudicaciones
        Ador.MoveNext
        m_stxtDest = Ador(0).Value  '33 Destino
        Ador.MoveNext
        m_stxtCodProve = Ador(0).Value  '34 Proveedor
        Ador.MoveNext
        m_stxtDenProve = Ador(0).Value  '35 Nombre proveedor
        Ador.MoveNext
        m_stxtPrec = Ador(0).Value  '36 Precio
        Ador.MoveNext
        m_stxtFecIni = Ador(0).Value  '37 F.Inicio
        Ador.MoveNext
        m_stxtFecFin = Ador(0).Value  '38 F.Fin
        Ador.MoveNext
        m_stxtTodosMat = Ador(0).Value '39 Todos los materiales
        
        Ador.Close
    End If
    
    Set Ador = Nothing

        
End Sub

Private Sub Form_Load()

    Me.Height = 3180
    Me.Width = 6000
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
    
    txtEstMat.Enabled = False
    
    Timer1.Enabled = False

    CargarAnyos
    
    If chkIncluirItems.Value = vbUnchecked Then
        chkIncluirAdj.Enabled = False
    End If
    If opTodos.Value = True Then
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    End If
    
    ConfigurarSeguridad

End Sub

Private Sub opPais_Click()
    If opPais.Value = True Then
        cmdSelMat.Enabled = True
        cmdBorrar.Enabled = True
    Else
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    End If
End Sub

Private Sub opTodos_Click()
    If opTodos.Value = True Then
        txtEstMat = ""
        cmdSelMat.Enabled = False
        cmdBorrar.Enabled = False
    Else
        cmdSelMat.Enabled = True
        cmdBorrar.Enabled = True
    End If
    
End Sub

Private Sub sdbcAnyo_Click()
        
    iAnyoSeleccionado = Int(sdbcAnyo.Value)
    
End Sub

Private Sub sdbcAnyo_CloseUp()
    
    iAnyoSeleccionado = Int(sdbcAnyo.Value)
    
End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    
End Sub

Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then

    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorMATRestMatComp)) Is Nothing) Then
        bRMat = True
    End If

End If

End Sub

Private Sub Timer1_Timer()
   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub


Private Function ListadoPresupuestoPorMaterial(ByVal oReport As CRAXDRT.Report) As Boolean
Dim SubListado As CRAXDRT.Report
Dim bMostrar As Boolean
Dim sMostrarArt As String
Dim sMostrarAdj As String
Dim sCodComp As String
Dim sCodEqp As String

    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Function
    End If

    'REPORT PRINCIPAL
    sCodComp = ""
    sCodEqp = ""
    If bRMat Then
        sCodComp = oUsuarioSummit.comprador.Cod
        sCodEqp = oUsuarioSummit.comprador.codEqp
    End If
    
    If opTodos.Value = True Then
        sGMN4Cod = ""
        sGMN3Cod = ""
        sGMN2Cod = ""
        sGMN1Cod = ""
    End If
    
    Set g_adoresDG = oGestorInformes.ListadoPresupuestosPorMaterial(iAnyoSeleccionado, sCodEqp, sCodComp, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod)
        
    If g_adoresDG Is Nothing Then
        ListadoPresupuestoPorMaterial = False
        Exit Function

    Else
        oReport.Database.SetDataSource g_adoresDG
    End If
        
    'Art�culos
    If chkIncluirItems.Value = 1 Then
        
        Set g_adoresArt = oGestorInformes.ListadoPresupuestosPorMaterialArt(iAnyoSeleccionado, SQLBinaryToBoolean(chkIncluirAdj.Value), sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, opOrdCod.Value, opOrdDen.Value)
        
        If Not g_adoresArt Is Nothing Then
            sMostrarArt = "S"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMostrarArt")).Text = """" & sMostrarArt & """"
            
            Set SubListado = oReport.OpenSubreport("rptArticulosPres")
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArt")).Text = """" & m_stxtArticulo & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUnidad")).Text = """" & m_stxtUnidad & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCant")).Text = """" & m_stxtCant & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresupuesto")).Text = """" & m_stxtPres & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtObj")).Text = """" & m_stxtObj & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAhorro")).Text = """" & m_stxtAhorro & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcen")).Text = """" & m_stxtAhorroPorcen & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjudicaciones")).Text = """" & m_stxtAdjudicaciones & ":" & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDest")).Text = """" & m_stxtDest & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProv")).Text = """" & m_stxtCodProve & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDenProv")).Text = """" & m_stxtDenProve & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrecio")).Text = """" & m_stxtPrec & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecIni")).Text = """" & m_stxtFecIni & """"
            SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecFin")).Text = """" & m_stxtFecFin & """"
            
            If chkIncluirAdj.Value = 1 Then
                SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtMostrarAdj")).Text = """" & "S" & """"
            Else
                SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtMostrarAdj")).Text = """" & "N" & """"
            End If
        
            SubListado.Database.SetDataSource g_adoresArt
            
        Else
            sMostrarArt = "N"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMostrarArt")).Text = """" & sMostrarArt & """"
        End If
        Set SubListado = Nothing
        
    Else
        sMostrarArt = "N"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMostrarArt")).Text = """" & sMostrarArt & """"
    End If
    
    ListadoPresupuestoPorMaterial = True
End Function


Public Sub PonerMatSeleccionado()

    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
   
    If Not oGMN1Seleccionado Is Nothing Then
         sGMN1Cod = oGMN1Seleccionado.Cod
         
         txtEstMat = sGMN1Cod
    Else
         sGMN1Cod = ""
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
End Sub

Public Sub PonerMatSeleccionadoEnPres()

    Select Case frmPRESArt.sdbgUltraPres.ActiveRow.Band.Index
        Case 0:
            'GMN1
            sGMN1Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("COD").Value
            sGMN2Cod = ""
            sGMN3Cod = ""
            sGMN4Cod = ""
            txtEstMat = sGMN1Cod
            
        Case 1:
            'GMN2
            sGMN1Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN1").Value
            sGMN2Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("COD").Value
            sGMN3Cod = ""
            sGMN4Cod = ""
            txtEstMat = sGMN1Cod & " - " & sGMN2Cod
            
        Case 2:
            'gmn3
            sGMN1Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN1").Value
            sGMN2Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN2").Value
            sGMN3Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("COD").Value
            sGMN4Cod = ""
            txtEstMat = sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod
            
        Case 3:
            'GMN4
            sGMN1Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN1").Value
            sGMN2Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN2").Value
            sGMN3Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN3").Value
            sGMN4Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("COD").Value
            txtEstMat = sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod & " - " & sGMN4Cod
            
        Case 4:
            'gmn4
            sGMN1Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN1").Value
            sGMN2Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN2").Value
            sGMN3Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN3").Value
            sGMN4Cod = frmPRESArt.sdbgUltraPres.ActiveRow.Cells("GMN4").Value
            txtEstMat = sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod & " - " & sGMN4Cod
            
    End Select

    opPais.Value = True
End Sub
