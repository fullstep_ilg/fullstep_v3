VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmArtCentralUnifConfirm 
   Caption         =   "Form1"
   ClientHeight    =   5385
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   9165
   Icon            =   "frmArtCentralUnifConfirm.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5385
   ScaleWidth      =   9165
   Begin VB.PictureBox picBotonera 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   2640
      ScaleHeight     =   375
      ScaleWidth      =   6255
      TabIndex        =   3
      Top             =   4800
      Width           =   6255
      Begin VB.CommandButton cmdVolver 
         Caption         =   "<<  dA�adirMasArticulos           "
         Height          =   315
         Left            =   2520
         TabIndex        =   7
         Top             =   0
         Width           =   2415
      End
      Begin VB.CommandButton cmdFinalizar 
         Caption         =   "&Finalizar"
         Height          =   315
         Left            =   5040
         TabIndex        =   6
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   0
         TabIndex        =   5
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "dEliminar      "
         Height          =   315
         Left            =   1200
         TabIndex        =   4
         Top             =   0
         Width           =   1095
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgArticulos 
      Height          =   3255
      Left            =   120
      TabIndex        =   0
      Top             =   1320
      Width           =   8805
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   6
      stylesets.count =   9
      stylesets(0).Name=   "Atributos"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmArtCentralUnifConfirm.frx":014A
      stylesets(1).Name=   "IntOK"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmArtCentralUnifConfirm.frx":0203
      stylesets(2).Name=   "Normal"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmArtCentralUnifConfirm.frx":021F
      stylesets(3).Name=   "ActiveRow"
      stylesets(3).ForeColor=   16777215
      stylesets(3).BackColor=   -2147483647
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmArtCentralUnifConfirm.frx":023B
      stylesets(3).AlignmentText=   0
      stylesets(4).Name=   "IntHeader"
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmArtCentralUnifConfirm.frx":0257
      stylesets(4).AlignmentText=   0
      stylesets(4).AlignmentPicture=   0
      stylesets(5).Name=   "Adjudica"
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmArtCentralUnifConfirm.frx":03AF
      stylesets(6).Name=   "styEspAdjSi"
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmArtCentralUnifConfirm.frx":0724
      stylesets(7).Name=   "IntKO"
      stylesets(7).ForeColor=   16777215
      stylesets(7).BackColor=   255
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmArtCentralUnifConfirm.frx":07A1
      stylesets(8).Name=   "HayImpuestos"
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmArtCentralUnifConfirm.frx":081E
      stylesets(8).AlignmentText=   2
      stylesets(8).AlignmentPicture=   2
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6959
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1402
      Columns(2).Caption=   "UNIDAD"
      Columns(2).Name =   "UNIDAD"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "UONS"
      Columns(3).Name =   "UONS"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1244
      Columns(4).Caption=   "ESPEC"
      Columns(4).Name =   "ESPEC"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   4
      Columns(4).ButtonsAlways=   -1  'True
      Columns(4).StyleSet=   "styEspAdjSi"
      Columns(5).Width=   1720
      Columns(5).Caption=   "ATRIBUTOS"
      Columns(5).Name =   "ATRIBUTOS"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      Columns(5).ButtonsAlways=   -1  'True
      Columns(5).HasBackColor=   -1  'True
      Columns(5).BackColor=   -2147483633
      Columns(5).StyleSet=   "Atributos"
      _ExtentX        =   15531
      _ExtentY        =   5741
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblArtDen 
      Caption         =   "XXXX-XXXXXXX"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   8775
   End
   Begin VB.Label lblEncabezado 
      Caption         =   "dConfirme los art�culos a unificar bajo :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   8775
   End
End
Attribute VB_Name = "frmArtCentralUnifConfirm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Separadores
Private Const lSeparador = 127
Private m_bModifArti As Boolean
Private m_oArticuloCentral As CArticulo

Private m_bRestrMantUsu As Boolean
Private m_bRestrMantPerf As Boolean
Private m_bModoEdicion As Boolean
Private m_bEnableCancel As Boolean
Private m_bAceptar As Boolean
Public g_sDenominacion, g_scodart As String
Private m_oUonsSeleccionadas As CUnidadesOrganizativas
Private m_oGMNSeleccionada As Object

Public isAlta As Boolean

'idiomas
Private m_sVariasUnidades As String

Public Property Let ModoEdicion(ByVal valor As Boolean)
    m_bModoEdicion = valor
End Property


Public Property Set ArticuloCentral(ByRef oArtCentral As CArticulo)
    Set m_oArticuloCentral = oArtCentral
End Property

Public Property Get ArticuloCentral() As CArticulo
    Set ArticuloCentral = m_oArticuloCentral
End Property

Public Property Let enableCancel(ByVal Var As Boolean)
    m_bEnableCancel = Var
End Property

Public Property Get enableCancel() As Boolean
    enableCancel = m_bEnableCancel
End Property

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ARTCENTRALUNIFCONFIRM, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        lblEncabezado.caption = Ador(0).value
        Ador.MoveNext
        Me.sdbgArticulos.Columns("DEN").caption = Ador(0).value      '3
        Ador.MoveNext
        Me.sdbgArticulos.Columns("UNIDAD").caption = Ador(0).value
        Ador.MoveNext
        Me.sdbgArticulos.Columns("UONS").caption = Ador(0).value
        Ador.MoveNext
        Me.sdbgArticulos.Columns("ESPEC").caption = Ador(0).value
        Ador.MoveNext
        Me.sdbgArticulos.Columns("ATRIBUTOS").caption = Ador(0).value
        Ador.MoveNext
        Me.cmdCancelar.caption = Ador(0).value
        Ador.MoveNext
        Me.cmdFinalizar.caption = Ador(0).value
        Ador.MoveNext
        Me.caption = Ador(0).value
        Ador.MoveNext
        Me.cmdVolver.caption = "<< " & Ador(0).value
        Ador.MoveNext
        Me.cmdDeshacer.caption = Ador(0).value
        Ador.MoveNext
        'idiomas
        m_sVariasUnidades = Ador(0).value
        Ador.Close
    End If
    
    Set Ador = Nothing
    
    'pargen lit
    If m_bModoEdicion Then
        Me.caption = Me.caption & " " & gParametrosGenerales.gsArticuloCentral
    Else
        Me.caption = gParametrosGenerales.gsArticulosPlanta
    End If
End Sub

Private Sub cmdCancelar_Click()
    m_oArticuloCentral.deleteAgregados
    Set m_oArticuloCentral = Nothing
    m_bAceptar = False
    Unload Me
End Sub

Private Sub cmdDeshacer_Click()
    limpiarSeleccion
End Sub

Private Sub cmdFinalizar_Click()
    
    Dim bHayAtributos As Boolean
    bHayAtributos = False
    Dim oArticulo As CArticulo
    For Each oArticulo In m_oArticuloCentral.articulosAgregados
        oArticulo.DevolverTodosLosAtributosDelArticulo
        If oArticulo.ATRIBUTOS.Count > 0 Or oArticulo.esp <> "" Then
            bHayAtributos = True
            Exit For
        End If
    Next
    Set oArticulo = Nothing
        
    If Not bHayAtributos Then
        m_bAceptar = actualizarcentrales(m_oArticuloCentral)
    Else
        frmArtCentralAttr.ArticuloCentral = m_oArticuloCentral
        frmArtCentralAttr.Show
    End If
    Unload Me
End Sub

Private Sub cmdVolver_Click()
    frmArtCentralUnificar.g_scodart = Me.g_scodart
    frmArtCentralUnificar.g_sDenominacion = Me.g_sDenominacion
    Set frmArtCentralUnificar.ArticuloCentral = m_oArticuloCentral
    Set frmArtCentralUnificar.UonsSeleccionadas = m_oUonsSeleccionadas
    Set frmArtCentralUnificar.GMNSeleccionada = m_oGMNSeleccionada
    frmArtCentralUnificar.Show
    Unload Me
End Sub

Private Sub Form_Activate()
    CargarGridArticulos
    Me.lblArtDen.caption = m_oArticuloCentral.CodDen
    
    If m_bModoEdicion Then
        Me.cmdCancelar.Visible = True
        Me.cmdDeshacer.Visible = True
        Me.cmdFinalizar.Visible = True
        Me.cmdVolver.Visible = True
    Else
        Me.cmdCancelar.Visible = False
        Me.cmdDeshacer.Visible = False
        Me.cmdFinalizar.Visible = False
        Me.cmdVolver.Visible = False
    End If
End Sub


Private Sub Form_Initialize()
    m_bModoEdicion = True
End Sub

Private Sub Form_Load()
    CargarRecursos
    PonerFieldSeparator Me
    ConfigurarSeguridad
    If m_bModoEdicion Then
        Me.cmdCancelar.Visible = True
        Me.cmdDeshacer.Visible = True
        Me.cmdFinalizar.Visible = True
        Me.cmdVolver.Visible = True
        Me.lblEncabezado.Visible = True
    Else
        Me.cmdCancelar.Visible = False
        Me.cmdDeshacer.Visible = False
        Me.cmdFinalizar.Visible = False
        Me.cmdVolver.Visible = False
        Me.lblEncabezado.Visible = False
    End If
    Arrange
End Sub

Private Sub Arrange()
    Me.Width = 9405
    Me.Height = 5955
    Me.WindowState = vbNormal
End Sub

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonUsu)) Is Nothing) Then
        m_bRestrMantUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonPerf)) Is Nothing) Then
        m_bRestrMantPerf = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiModificar)) Is Nothing) Then
        m_bModifArti = True
    End If
End Sub


Private Sub CargarGridArticulos()
    Dim Articulo As CArticulo
    sdbgArticulos.RemoveAll
    For Each Articulo In m_oArticuloCentral.articulosAgregados
        Dim sLinea As String
        sLinea = Articulo.Cod & Chr(lSeparador)
        sLinea = sLinea & Articulo.CodDen & Chr(lSeparador)
        sLinea = sLinea & Articulo.CodigoUnidad & Chr(lSeparador)
        sLinea = sLinea & Articulo.NombreUON & Chr(lSeparador)
        sdbgArticulos.AddItem sLinea
    Next
    
End Sub

Public Sub limpiarSeleccion()
    Dim bk As Variant
    For Each bk In Me.sdbgArticulos.SelBookmarks
        Dim Index As Variant
        Index = Me.sdbgArticulos.Columns("COD").CellValue(bk)
        m_oArticuloCentral.deleteArticuloagregado m_oArticuloCentral.articulosAgregados.Item(Index)
    Next
    CargarGridArticulos
End Sub

Private Sub sdbgArticulos_BtnClick()
    Dim teserror As TipoErrorSummit
        Select Case sdbgArticulos.Columns(sdbgArticulos.Col).Name
        Case "ATRIBUTOS"
            
            frmESTRMATAtrib.g_sOrigen = "FRMARTCENTRALUNIFCONFIRM"
            frmESTRMATAtrib.g_bModoEdicion = True
            Set frmESTRMATAtrib.Articulo = m_oArticuloCentral.articulosAgregados.Item(CStr(sdbgArticulos.Columns("COD").value))
            Set frmESTRMATAtrib.g_oAtributos = m_oArticuloCentral.articulosAgregados.Item(CStr(sdbgArticulos.Columns("COD").value)).DevolverTodosLosAtributosDelArticulo
            frmESTRMATAtrib.g_vCodArt = sdbgArticulos.Columns("COD").value
            'no permitir asignar nuevos atributos
            frmESTRMATAtrib.cmdAsignar.Enabled = False
            frmESTRMATAtrib.cmdDesAsignar.Enabled = False
            frmESTRMATAtrib.Show
            
        Case "ESPEC"
            'Si no tiene permisos para modificar los art�culos:
            If m_bModifArti = False Then
                frmArticuloEsp.txtArticuloEsp.Locked = True
                frmArticuloEsp.cmdA�adirEsp.Enabled = False
                frmArticuloEsp.cmdEliminarEsp.Enabled = False
                frmArticuloEsp.cmdModificarEsp.Enabled = False
                frmArticuloEsp.cmdSalvarEsp.Enabled = True
                frmArticuloEsp.cmdAbrirEsp.Enabled = True
                frmArticuloEsp.cmdRestaurar.Visible = False
            End If
            
            Set frmArticuloEsp.g_oArticulo = Me.ArticuloCentral.articulosAgregados.Item(CStr(sdbgArticulos.Columns("COD").value))
            If frmArticuloEsp.g_oArticulo Is Nothing Then Exit Sub
            ''' Pasamos el usuario que realiza la modificaci�n (LOG e Integraci�n
            frmArticuloEsp.g_oArticulo.Usuario = basOptimizacion.gvarCodUsuario
            
            Set frmArticuloEsp.g_oIBaseDatos = frmArticuloEsp.g_oArticulo
            teserror = frmArticuloEsp.g_oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set frmArticuloEsp.g_oIBaseDatos = Nothing
                Set frmArticuloEsp.g_oArticulo = Nothing
                Exit Sub
            End If
            frmArticuloEsp.g_oIBaseDatos.CancelarEdicion

            frmArticuloEsp.g_sOrigen = "frmESTRMAT"
            frmArticuloEsp.g_oArticulo.CargarTodasLasEspecificaciones
            If Not sdbgArticulos.IsAddRow Then
                'Configurar parametros del commondialog
                frmArticuloEsp.cmmdEsp.FLAGS = cdlOFNHideReadOnly
                frmArticuloEsp.caption = sdbgArticulos.Columns("COD").value & " " & sdbgArticulos.Columns("DEN").value

                frmArticuloEsp.g_bRespetarCombo = True
                frmArticuloEsp.txtArticuloEsp.Text = NullToStr(frmArticuloEsp.g_oArticulo.esp)
                frmArticuloEsp.g_bRespetarCombo = False
                frmArticuloEsp.AnyadirEspsALista
                frmArticuloEsp.Show 1
                If Me.ArticuloCentral.articulosAgregados.Item(CStr(sdbgArticulos.Columns("COD").value)).EspAdj = 1 Then
                    sdbgArticulos.Columns("ESPEC").CellStyleSet "styEspAdjSi"
                Else
                    sdbgArticulos.Columns("ESPEC").CellStyleSet ""
                End If
                sdbgArticulos.Refresh
            End If
            If Me.Visible Then sdbgArticulos.SetFocus
            sdbgArticulos.Col = 7
            
    End Select
End Sub

Private Sub sdbgArticulos_RowLoaded(ByVal Bookmark As Variant)
    Dim oArticulo As CArticulo
    Set oArticulo = Nothing
    If Not IsNull(Bookmark) And Not IsEmpty(Bookmark) Then
        Set oArticulo = m_oArticuloCentral.articulosAgregados.Item(Me.sdbgArticulos.Columns("COD").value)
        If Not oArticulo Is Nothing Then
            If oArticulo.EspAdj <> 0 Then
                sdbgArticulos.Columns("ESPEC").CellStyleSet "styEspAdjSi"
            Else
                sdbgArticulos.Columns("ESPEC").CellStyleSet ""
            End If
            If oArticulo.ConAtributos Then
                sdbgArticulos.Columns("ATRIBUTOS").CellStyleSet "Atributos"
            Else
                sdbgArticulos.Columns("ATRIBUTOS").CellStyleSet ""
            End If
        End If
    End If
    If sdbgArticulos.Columns("UONS").value = "Multiples*" Then
       sdbgArticulos.Columns("UONS").value = m_sVariasUnidades
    End If
End Sub

Public Function aceptar() As Boolean
    aceptar = m_bAceptar
End Function

Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas

    Set UonsSeleccionadas = m_oUonsSeleccionadas

End Property

Public Property Set UonsSeleccionadas(oUonsSeleccionadas As CUnidadesOrganizativas)
    If Not oUonsSeleccionadas Is Nothing Then
        Set m_oUonsSeleccionadas = oUonsSeleccionadas
    End If
End Property

Public Property Get GMNSeleccionada() As Object
    Set GMNSeleccionada = m_oGMNSeleccionada
End Property

Public Property Set GMNSeleccionada(oGMNSeleccionada As Object)
    If Not oGMNSeleccionada Is Nothing Then
        Set m_oGMNSeleccionada = oGMNSeleccionada
    End If
End Property
