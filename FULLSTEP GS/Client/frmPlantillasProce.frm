VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{14ACBB92-9C4A-4C45-AFD2-7AE60E71E5B3}#4.0#0"; "IGSplitter40.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPlantillasProce 
   Caption         =   "Plantillas de procesos"
   ClientHeight    =   9075
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11160
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPlantillasProce.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9075
   ScaleWidth      =   11160
   Begin VB.CommandButton cmdEliminarVistas 
      Caption         =   "DEliminar &vistas"
      Height          =   375
      Left            =   6840
      TabIndex        =   40
      Top             =   8520
      Width           =   1600
   End
   Begin VB.CommandButton cmdConfVistas 
      Caption         =   "DConfi&gurar vistas"
      Height          =   375
      Left            =   5160
      TabIndex        =   39
      Top             =   8520
      Width           =   1600
   End
   Begin VB.CommandButton cmdEliminarPlant 
      Caption         =   "D&Eliminar"
      Height          =   375
      Left            =   3480
      TabIndex        =   38
      Top             =   8520
      Width           =   1600
   End
   Begin VB.CommandButton cmdModificarPlant 
      Caption         =   "D&Modificar"
      Height          =   375
      Left            =   1800
      TabIndex        =   37
      Top             =   8520
      Width           =   1600
   End
   Begin VB.PictureBox picPrincipal 
      BackColor       =   &H00808000&
      Height          =   8235
      Left            =   1700
      ScaleHeight     =   8175
      ScaleWidth      =   8940
      TabIndex        =   3
      Top             =   120
      Width           =   9000
      Begin VB.TextBox txtDescr 
         BackColor       =   &H00C8D4D0&
         Height          =   495
         Left            =   60
         Locked          =   -1  'True
         MaxLength       =   500
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   4
         Top             =   360
         Width           =   8800
      End
      Begin TabDlg.SSTab sstabPlantillas 
         Height          =   7215
         Left            =   60
         TabIndex        =   6
         Top             =   900
         Width           =   8805
         _ExtentX        =   15531
         _ExtentY        =   12726
         _Version        =   393216
         Tabs            =   5
         TabsPerRow      =   5
         TabHeight       =   520
         BackColor       =   8421376
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Ambito de datos"
         TabPicture(0)   =   "frmPlantillasProce.frx":014A
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "picNavigateAmbito"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "sdbgPlantillas"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "picAmbitoDatosOfe"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "fraAdminPub"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cmdConfigSubasta"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Grupos"
         TabPicture(1)   =   "frmPlantillasProce.frx":0166
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "sdbgGrupos"
         Tab(1).Control(1)=   "picNavigateGrupos"
         Tab(1).Control(2)=   "txtGrupoDescr"
         Tab(1).ControlCount=   3
         TabCaption(2)   =   "Atributos"
         TabPicture(2)   =   "frmPlantillasProce.frx":0182
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "sdbcSeleccion"
         Tab(2).Control(1)=   "sdbddSigno"
         Tab(2).Control(2)=   "sdbddAmbito"
         Tab(2).Control(3)=   "sdbddAplicarA"
         Tab(2).Control(4)=   "picNavigateAtrib"
         Tab(2).Control(5)=   "sdbgAtribPlant"
         Tab(2).Control(6)=   "cmdSortAsc"
         Tab(2).Control(7)=   "cmdSortDesc"
         Tab(2).ControlCount=   8
         TabCaption(3)   =   "DEspecificaciones"
         TabPicture(3)   =   "frmPlantillasProce.frx":019E
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "cmdDeshacerAtrEsp"
         Tab(3).Control(0).Enabled=   0   'False
         Tab(3).Control(1)=   "cmdModoEdicionAtrEsp"
         Tab(3).Control(1).Enabled=   0   'False
         Tab(3).Control(2)=   "SSSplitterEsp"
         Tab(3).Control(3)=   "Label1"
         Tab(3).ControlCount=   4
         TabCaption(4)   =   "Materiales"
         TabPicture(4)   =   "frmPlantillasProce.frx":01BA
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "picTapa"
         Tab(4).Control(1)=   "fraSoloEstosMateriales"
         Tab(4).Control(2)=   "PicNavigateMateriales"
         Tab(4).Control(3)=   "tvwEstrMat"
         Tab(4).Control(4)=   "ImageList3"
         Tab(4).Control(5)=   "tvwEstrMatMod"
         Tab(4).Control(6)=   "ImageListMod"
         Tab(4).ControlCount=   7
         Begin VB.CommandButton cmdConfigSubasta 
            Caption         =   "..."
            Height          =   285
            Left            =   5160
            TabIndex        =   99
            Top             =   3120
            Width           =   315
         End
         Begin VB.CommandButton cmdDeshacerAtrEsp 
            Caption         =   "&Deshacer"
            Enabled         =   0   'False
            Height          =   345
            Left            =   -73560
            TabIndex        =   98
            TabStop         =   0   'False
            Top             =   5400
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdModoEdicionAtrEsp 
            Caption         =   "&Edici�n"
            Height          =   345
            Left            =   -74640
            TabIndex        =   97
            TabStop         =   0   'False
            Top             =   5400
            Width           =   1005
         End
         Begin VB.PictureBox picTapa 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            Height          =   210
            Left            =   -74760
            Picture         =   "frmPlantillasProce.frx":01D6
            ScaleHeight     =   210
            ScaleWidth      =   195
            TabIndex        =   72
            Top             =   1080
            Width           =   200
         End
         Begin VB.Frame fraSoloEstosMateriales 
            BorderStyle     =   0  'None
            Height          =   495
            Left            =   -74760
            TabIndex        =   70
            Top             =   720
            Width           =   7815
            Begin VB.CheckBox chkSoloEstosMateriales 
               Caption         =   "La plantilla s�lo ser� v�lida para los siguientes materiales"
               Height          =   195
               Left            =   0
               TabIndex        =   71
               Top             =   120
               Width           =   5000
            End
         End
         Begin VB.PictureBox PicNavigateMateriales 
            BorderStyle     =   0  'None
            Height          =   420
            Left            =   -74880
            ScaleHeight     =   420
            ScaleWidth      =   8520
            TabIndex        =   65
            Top             =   5760
            Width           =   8520
            Begin VB.CommandButton cmdCancelarMateriales 
               Caption         =   "&Cancelar"
               Height          =   345
               Left            =   4590
               TabIndex        =   68
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.CommandButton cmdAceptarMateriales 
               Caption         =   "&Aceptar"
               Default         =   -1  'True
               Height          =   345
               Left            =   3420
               TabIndex        =   67
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.CommandButton cmdModificarMateriales 
               Caption         =   "&Modificar"
               Height          =   345
               Left            =   0
               TabIndex        =   66
               Top             =   60
               Width           =   1005
            End
         End
         Begin VB.Frame fraAdminPub 
            Height          =   525
            Left            =   135
            TabIndex        =   61
            Top             =   345
            Width           =   8505
            Begin VB.PictureBox picAdminPub 
               BorderStyle     =   0  'None
               Height          =   350
               Left            =   120
               ScaleHeight     =   345
               ScaleWidth      =   4500
               TabIndex        =   62
               Top             =   150
               Width           =   4500
               Begin VB.CheckBox chkAdminPub 
                  Caption         =   "DProceso de administraci�n p�blica"
                  Height          =   255
                  Left            =   0
                  TabIndex        =   63
                  Top             =   30
                  Width           =   3500
               End
            End
         End
         Begin VB.PictureBox picAmbitoDatosOfe 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   4440
            Left            =   120
            ScaleHeight     =   4440
            ScaleMode       =   0  'User
            ScaleWidth      =   8595
            TabIndex        =   41
            Top             =   2040
            Width           =   8595
            Begin VB.Frame fraSolicitudCompra 
               Caption         =   "dSolicitud de compra"
               Height          =   795
               Index           =   0
               Left            =   30
               TabIndex        =   102
               Top             =   -30
               Width           =   8520
               Begin VB.PictureBox picDatosConf 
                  BorderStyle     =   0  'None
                  Height          =   525
                  Index           =   1
                  Left            =   100
                  ScaleHeight     =   525
                  ScaleWidth      =   6090
                  TabIndex        =   103
                  Top             =   240
                  Width           =   6090
                  Begin VB.CheckBox chkOblSolicitudAdj 
                     Caption         =   "dObligar a indicar solicitud de compra en la validaci�n de la adjudicaci�n"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   105
                     Top             =   270
                     Width           =   10935
                  End
                  Begin VB.CheckBox chkOblSolicitud 
                     Caption         =   "dObligar a indicar solicitud de compra en la validaci�n de la apertura"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   104
                     Top             =   0
                     Width           =   6735
                  End
               End
            End
            Begin VB.Frame fraConfProce 
               Caption         =   "Solicitudes de ofertas"
               Height          =   2685
               Index           =   2
               Left            =   0
               TabIndex        =   42
               Top             =   840
               Width           =   8520
               Begin VB.PictureBox picDatosConf 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  ForeColor       =   &H80000008&
                  Height          =   195
                  Index           =   0
                  Left            =   120
                  ScaleHeight     =   195
                  ScaleWidth      =   7965
                  TabIndex        =   95
                  Top             =   2330
                  Width           =   7965
                  Begin VB.CheckBox chkOblAtrGS 
                     Caption         =   "Respetar la obligatoriedad de los atributos en las ofertas introducidas desde FULLSTEP GS"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   96
                     Top             =   0
                     Width           =   7935
                  End
               End
               Begin VB.PictureBox picDatosConf 
                  BorderStyle     =   0  'None
                  Height          =   200
                  Index           =   4
                  Left            =   105
                  ScaleHeight     =   195
                  ScaleWidth      =   3000
                  TabIndex        =   53
                  Top             =   525
                  Width           =   3000
                  Begin VB.CheckBox chkPrecios 
                     Caption         =   "Pedir varias alternativas de precios"
                     Height          =   195
                     Left            =   15
                     TabIndex        =   54
                     Top             =   0
                     Width           =   3015
                  End
               End
               Begin VB.PictureBox picDatosConf 
                  BorderStyle     =   0  'None
                  Height          =   250
                  Index           =   2
                  Left            =   100
                  ScaleHeight     =   255
                  ScaleWidth      =   3195
                  TabIndex        =   51
                  Top             =   240
                  Width           =   3200
                  Begin VB.CheckBox chkSubasta 
                     Caption         =   "Recepci�n de ofertas en modo subasta"
                     Height          =   195
                     Left            =   10
                     TabIndex        =   52
                     Top             =   15
                     Width           =   5135
                  End
               End
               Begin VB.PictureBox picDatosConf 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  ForeColor       =   &H80000008&
                  Height          =   195
                  Index           =   3
                  Left            =   120
                  ScaleHeight     =   195
                  ScaleWidth      =   4965
                  TabIndex        =   49
                  Top             =   2085
                  Width           =   4965
                  Begin VB.CheckBox chkNoPublicarFinSum 
                     Caption         =   "No publicar fecha de fin de suministro"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   50
                     Top             =   0
                     Width           =   4215
                  End
               End
               Begin VB.PictureBox picDatosConf 
                  BorderStyle     =   0  'None
                  Height          =   1300
                  Index           =   5
                  Left            =   120
                  ScaleHeight     =   1305
                  ScaleWidth      =   6705
                  TabIndex        =   43
                  Top             =   780
                  Width           =   6700
                  Begin VB.CheckBox chkSuministros 
                     Caption         =   "Solicitar cantidades m�ximas de suministro"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   48
                     Top             =   0
                     Width           =   5935
                  End
                  Begin VB.CheckBox chkArchivProce 
                     Caption         =   "Permitir adjuntar archivos a nivel de proceso"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   47
                     Top             =   520
                     Width           =   4155
                  End
                  Begin VB.CheckBox chkArchivGrupo 
                     Caption         =   "Permitir adjuntar archivos a nivel de grupo"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   46
                     Top             =   780
                     Width           =   4155
                  End
                  Begin VB.CheckBox chkArchivItem 
                     Caption         =   "Permitir adjuntar archivos a nivel de item"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   45
                     Top             =   1050
                     Width           =   4155
                  End
                  Begin VB.CheckBox chkOferMonProce 
                     Caption         =   "Ofertar en la moneda del proceso"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   44
                     Top             =   260
                     Width           =   5935
                  End
               End
            End
            Begin VB.Frame fraConfProce 
               Caption         =   "Ponderaci�n"
               Height          =   645
               Index           =   3
               Left            =   0
               TabIndex        =   55
               Top             =   3600
               Width           =   4200
               Begin VB.PictureBox picDatosConf 
                  BorderStyle     =   0  'None
                  Height          =   255
                  Index           =   6
                  Left            =   100
                  ScaleHeight     =   255
                  ScaleWidth      =   3900
                  TabIndex        =   56
                  Top             =   240
                  Width           =   3900
                  Begin VB.CheckBox chkPondAtrib 
                     Caption         =   "Habilitar ponderaci�n en atributos"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   57
                     Top             =   0
                     Width           =   3495
                  End
               End
            End
            Begin VB.Frame fraConfProce 
               Caption         =   "Pedidos"
               Height          =   1005
               Index           =   4
               Left            =   4320
               TabIndex        =   58
               Top             =   3600
               Width           =   4200
               Begin VB.PictureBox picDatosConf 
                  BorderStyle     =   0  'None
                  Height          =   735
                  Index           =   7
                  Left            =   100
                  ScaleHeight     =   735
                  ScaleWidth      =   3855
                  TabIndex        =   59
                  Top             =   240
                  Width           =   3855
                  Begin VB.CheckBox chkUnSoloPedido 
                     Caption         =   "Permitir un s�lo pedido directo"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   60
                     Top             =   0
                     Width           =   3800
                  End
                  Begin VB.CheckBox chkParaPedido 
                     Caption         =   "Plantilla para emitir pedido directo"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   101
                     Top             =   250
                     Width           =   3495
                  End
                  Begin VB.CheckBox chkParaPedidoDef 
                     Caption         =   "Plantilla por defecto"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   100
                     Top             =   510
                     Width           =   3495
                  End
               End
            End
         End
         Begin VB.TextBox txtGrupoDescr 
            BackColor       =   &H00FFFFFF&
            Height          =   1000
            Left            =   -74850
            MaxLength       =   500
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   28
            Top             =   3000
            Width           =   8535
         End
         Begin VB.CommandButton cmdSortDesc 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   -74900
            Picture         =   "frmPlantillasProce.frx":03D0
            Style           =   1  'Graphical
            TabIndex        =   35
            Top             =   1495
            Width           =   380
         End
         Begin VB.CommandButton cmdSortAsc 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   -74900
            Picture         =   "frmPlantillasProce.frx":042A
            Style           =   1  'Graphical
            TabIndex        =   34
            Top             =   840
            Width           =   380
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtribPlant 
            Height          =   3000
            Left            =   -74480
            TabIndex        =   29
            Top             =   840
            Width           =   8150
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   27
            stylesets.count =   8
            stylesets(0).Name=   "Ponderacion"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPlantillasProce.frx":0484
            stylesets(1).Name=   "Yellow"
            stylesets(1).BackColor=   8454143
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmPlantillasProce.frx":05EF
            stylesets(2).Name=   "Gris"
            stylesets(2).BackColor=   13158600
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmPlantillasProce.frx":060B
            stylesets(3).Name=   "Bold"
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmPlantillasProce.frx":0627
            stylesets(4).Name=   "Proceso"
            stylesets(4).BackColor=   7895160
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmPlantillasProce.frx":0643
            stylesets(5).Name=   "Normal"
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "frmPlantillasProce.frx":065F
            stylesets(6).Name=   "Green"
            stylesets(6).BackColor=   7912636
            stylesets(6).HasFont=   -1  'True
            BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(6).Picture=   "frmPlantillasProce.frx":067B
            stylesets(7).Name=   "Azul"
            stylesets(7).BackColor=   16777160
            stylesets(7).HasFont=   -1  'True
            BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(7).Picture=   "frmPlantillasProce.frx":0697
            UseGroups       =   -1  'True
            AllowUpdate     =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   2
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Groups.Count    =   5
            Groups(0).Width =   12012
            Groups(0).Caption=   "Definici�n de atributos"
            Groups(0).HasHeadForeColor=   -1  'True
            Groups(0).HasHeadBackColor=   -1  'True
            Groups(0).HeadForeColor=   16777215
            Groups(0).HeadBackColor=   32896
            Groups(0).Columns.Count=   11
            Groups(0).Columns(0).Width=   503
            Groups(0).Columns(0).Visible=   0   'False
            Groups(0).Columns(0).Caption=   "ID"
            Groups(0).Columns(0).Name=   "ID"
            Groups(0).Columns(0).DataField=   "Column 0"
            Groups(0).Columns(0).DataType=   8
            Groups(0).Columns(0).FieldLen=   256
            Groups(0).Columns(1).Width=   1111
            Groups(0).Columns(1).Visible=   0   'False
            Groups(0).Columns(1).Caption=   "CODGRUPO"
            Groups(0).Columns(1).Name=   "CODGRUPO"
            Groups(0).Columns(1).DataField=   "Column 1"
            Groups(0).Columns(1).DataType=   8
            Groups(0).Columns(1).FieldLen=   256
            Groups(0).Columns(2).Width=   1561
            Groups(0).Columns(2).Caption=   "Interno"
            Groups(0).Columns(2).Name=   "INTERNO"
            Groups(0).Columns(2).CaptionAlignment=   2
            Groups(0).Columns(2).DataField=   "Column 2"
            Groups(0).Columns(2).DataType=   8
            Groups(0).Columns(2).FieldLen=   256
            Groups(0).Columns(2).Style=   2
            Groups(0).Columns(2).HasHeadForeColor=   -1  'True
            Groups(0).Columns(2).HasHeadBackColor=   -1  'True
            Groups(0).Columns(2).HeadForeColor=   16777215
            Groups(0).Columns(2).HeadBackColor=   32896
            Groups(0).Columns(3).Width=   1561
            Groups(0).Columns(3).Caption=   "PEDIDO"
            Groups(0).Columns(3).Name=   "PEDIDO"
            Groups(0).Columns(3).DataField=   "Column 3"
            Groups(0).Columns(3).DataType=   8
            Groups(0).Columns(3).FieldLen=   256
            Groups(0).Columns(3).Style=   2
            Groups(0).Columns(3).HasHeadForeColor=   -1  'True
            Groups(0).Columns(3).HasHeadBackColor=   -1  'True
            Groups(0).Columns(3).HeadForeColor=   16777215
            Groups(0).Columns(3).HeadBackColor=   32896
            Groups(0).Columns(4).Width=   1984
            Groups(0).Columns(4).Caption=   "C�digo"
            Groups(0).Columns(4).Name=   "COD"
            Groups(0).Columns(4).CaptionAlignment=   2
            Groups(0).Columns(4).DataField=   "Column 4"
            Groups(0).Columns(4).DataType=   8
            Groups(0).Columns(4).FieldLen=   256
            Groups(0).Columns(4).ButtonsAlways=   -1  'True
            Groups(0).Columns(4).HasHeadForeColor=   -1  'True
            Groups(0).Columns(4).HasHeadBackColor=   -1  'True
            Groups(0).Columns(4).HasForeColor=   -1  'True
            Groups(0).Columns(4).HasBackColor=   -1  'True
            Groups(0).Columns(4).HeadForeColor=   16777215
            Groups(0).Columns(4).HeadBackColor=   32896
            Groups(0).Columns(4).BackColor=   16777160
            Groups(0).Columns(5).Width=   3096
            Groups(0).Columns(5).Caption=   "Denominaci�n"
            Groups(0).Columns(5).Name=   "NOMBRE"
            Groups(0).Columns(5).CaptionAlignment=   2
            Groups(0).Columns(5).DataField=   "Column 5"
            Groups(0).Columns(5).DataType=   8
            Groups(0).Columns(5).FieldLen=   256
            Groups(0).Columns(5).Locked=   -1  'True
            Groups(0).Columns(5).ButtonsAlways=   -1  'True
            Groups(0).Columns(5).HasHeadForeColor=   -1  'True
            Groups(0).Columns(5).HasHeadBackColor=   -1  'True
            Groups(0).Columns(5).HasForeColor=   -1  'True
            Groups(0).Columns(5).HasBackColor=   -1  'True
            Groups(0).Columns(5).HeadForeColor=   16777215
            Groups(0).Columns(5).HeadBackColor=   32896
            Groups(0).Columns(5).BackColor=   16777160
            Groups(0).Columns(6).Width=   1746
            Groups(0).Columns(6).Caption=   "Descr."
            Groups(0).Columns(6).Name=   "DESCR"
            Groups(0).Columns(6).Alignment=   2
            Groups(0).Columns(6).DataField=   "Column 6"
            Groups(0).Columns(6).DataType=   8
            Groups(0).Columns(6).FieldLen=   256
            Groups(0).Columns(6).Style=   4
            Groups(0).Columns(6).ButtonsAlways=   -1  'True
            Groups(0).Columns(6).HasHeadForeColor=   -1  'True
            Groups(0).Columns(6).HasHeadBackColor=   -1  'True
            Groups(0).Columns(6).HasBackColor=   -1  'True
            Groups(0).Columns(6).HeadForeColor=   16777215
            Groups(0).Columns(6).HeadBackColor=   32896
            Groups(0).Columns(6).BackColor=   16777160
            Groups(0).Columns(6).StyleSet=   "Bold"
            Groups(0).Columns(7).Width=   2064
            Groups(0).Columns(7).Caption=   "Tipo de dato"
            Groups(0).Columns(7).Name=   "TIPO"
            Groups(0).Columns(7).CaptionAlignment=   2
            Groups(0).Columns(7).DataField=   "Column 7"
            Groups(0).Columns(7).DataType=   8
            Groups(0).Columns(7).FieldLen=   256
            Groups(0).Columns(7).Locked=   -1  'True
            Groups(0).Columns(7).HasHeadForeColor=   -1  'True
            Groups(0).Columns(7).HasHeadBackColor=   -1  'True
            Groups(0).Columns(7).HasBackColor=   -1  'True
            Groups(0).Columns(7).HeadForeColor=   16777215
            Groups(0).Columns(7).HeadBackColor=   32896
            Groups(0).Columns(7).BackColor=   16777160
            Groups(0).Columns(8).Width=   1323
            Groups(0).Columns(8).Visible=   0   'False
            Groups(0).Columns(8).Caption=   "PEDIDOORIG"
            Groups(0).Columns(8).Name=   "PEDIDOORIG"
            Groups(0).Columns(8).DataField=   "Column 8"
            Groups(0).Columns(8).DataType=   8
            Groups(0).Columns(8).FieldLen=   256
            Groups(0).Columns(9).Width=   3836
            Groups(0).Columns(9).Visible=   0   'False
            Groups(0).Columns(9).Caption=   "idTIPO"
            Groups(0).Columns(9).Name=   "idTIPO"
            Groups(0).Columns(9).DataField=   "Column 9"
            Groups(0).Columns(9).DataType=   8
            Groups(0).Columns(9).FieldLen=   256
            Groups(0).Columns(10).Width=   3836
            Groups(0).Columns(10).Visible=   0   'False
            Groups(0).Columns(10).Caption=   "ATRIB"
            Groups(0).Columns(10).Name=   "ATRIB"
            Groups(0).Columns(10).DataField=   "Column 10"
            Groups(0).Columns(10).DataType=   8
            Groups(0).Columns(10).FieldLen=   256
            Groups(1).Width =   9869
            Groups(1).Caption=   "Introducci�n"
            Groups(1).HasHeadForeColor=   -1  'True
            Groups(1).HasHeadBackColor=   -1  'True
            Groups(1).HeadForeColor=   16777215
            Groups(1).HeadBackColor=   32896
            Groups(1).Columns.Count=   9
            Groups(1).Columns(0).Width=   900
            Groups(1).Columns(0).Visible=   0   'False
            Groups(1).Columns(0).Caption=   "Libre"
            Groups(1).Columns(0).Name=   "LIBRE"
            Groups(1).Columns(0).CaptionAlignment=   2
            Groups(1).Columns(0).DataField=   "Column 11"
            Groups(1).Columns(0).DataType=   8
            Groups(1).Columns(0).FieldLen=   256
            Groups(1).Columns(0).Style=   2
            Groups(1).Columns(0).HasHeadForeColor=   -1  'True
            Groups(1).Columns(0).HasHeadBackColor=   -1  'True
            Groups(1).Columns(0).HasForeColor=   -1  'True
            Groups(1).Columns(0).HeadForeColor=   16777215
            Groups(1).Columns(0).HeadBackColor=   32896
            Groups(1).Columns(1).Width=   1349
            Groups(1).Columns(1).Caption=   "M�nimo"
            Groups(1).Columns(1).Name=   "MIN"
            Groups(1).Columns(1).Alignment=   1
            Groups(1).Columns(1).DataField=   "Column 12"
            Groups(1).Columns(1).DataType=   8
            Groups(1).Columns(1).FieldLen=   256
            Groups(1).Columns(1).HasHeadForeColor=   -1  'True
            Groups(1).Columns(1).HasHeadBackColor=   -1  'True
            Groups(1).Columns(1).HeadForeColor=   16777215
            Groups(1).Columns(1).HeadBackColor=   32896
            Groups(1).Columns(2).Width=   1376
            Groups(1).Columns(2).Caption=   "M�ximo"
            Groups(1).Columns(2).Name=   "MAX"
            Groups(1).Columns(2).Alignment=   1
            Groups(1).Columns(2).DataField=   "Column 13"
            Groups(1).Columns(2).DataType=   8
            Groups(1).Columns(2).FieldLen=   256
            Groups(1).Columns(2).HasHeadForeColor=   -1  'True
            Groups(1).Columns(2).HasHeadBackColor=   -1  'True
            Groups(1).Columns(2).HeadForeColor=   16777215
            Groups(1).Columns(2).HeadBackColor=   32896
            Groups(1).Columns(3).Width=   1482
            Groups(1).Columns(3).Caption=   "Selecci�n"
            Groups(1).Columns(3).Name=   "SELECCION"
            Groups(1).Columns(3).DataField=   "Column 14"
            Groups(1).Columns(3).DataType=   8
            Groups(1).Columns(3).FieldLen=   256
            Groups(1).Columns(3).Style=   2
            Groups(1).Columns(3).HasHeadForeColor=   -1  'True
            Groups(1).Columns(3).HasHeadBackColor=   -1  'True
            Groups(1).Columns(3).HeadForeColor=   16777215
            Groups(1).Columns(3).HeadBackColor=   32896
            Groups(1).Columns(4).Width=   979
            Groups(1).Columns(4).Name=   "BOT"
            Groups(1).Columns(4).Alignment=   2
            Groups(1).Columns(4).DataField=   "Column 15"
            Groups(1).Columns(4).DataType=   8
            Groups(1).Columns(4).FieldLen=   256
            Groups(1).Columns(4).Style=   4
            Groups(1).Columns(4).ButtonsAlways=   -1  'True
            Groups(1).Columns(4).HasHeadForeColor=   -1  'True
            Groups(1).Columns(4).HasHeadBackColor=   -1  'True
            Groups(1).Columns(4).HeadForeColor=   16777215
            Groups(1).Columns(4).HeadBackColor=   32896
            Groups(1).Columns(4).StyleSet=   "Bold"
            Groups(1).Columns(5).Width=   1244
            Groups(1).Columns(5).Caption=   "Ambito de valor"
            Groups(1).Columns(5).Name=   "AMBITO"
            Groups(1).Columns(5).DataField=   "Column 16"
            Groups(1).Columns(5).DataType=   8
            Groups(1).Columns(5).FieldLen=   256
            Groups(1).Columns(5).Style=   3
            Groups(1).Columns(5).Row.Count=   3
            Groups(1).Columns(5).Col.Count=   2
            Groups(1).Columns(5).Row(0).Col(0)=   "Proceso"
            Groups(1).Columns(5).Row(1).Col(0)=   "Grupo"
            Groups(1).Columns(5).Row(2).Col(0)=   "Item"
            Groups(1).Columns(5).HasHeadForeColor=   -1  'True
            Groups(1).Columns(5).HasHeadBackColor=   -1  'True
            Groups(1).Columns(5).HeadForeColor=   16777215
            Groups(1).Columns(5).HeadBackColor=   32896
            Groups(1).Columns(6).Width=   767
            Groups(1).Columns(6).Visible=   0   'False
            Groups(1).Columns(6).Caption=   "AMBITOORIG"
            Groups(1).Columns(6).Name=   "AMBITOORIG"
            Groups(1).Columns(6).DataField=   "Column 17"
            Groups(1).Columns(6).DataType=   8
            Groups(1).Columns(6).FieldLen=   256
            Groups(1).Columns(7).Width=   1799
            Groups(1).Columns(7).Caption=   "Integracion"
            Groups(1).Columns(7).Name=   "INTEGRACION"
            Groups(1).Columns(7).DataField=   "Column 18"
            Groups(1).Columns(7).DataType=   8
            Groups(1).Columns(7).FieldLen=   256
            Groups(1).Columns(7).HasHeadForeColor=   -1  'True
            Groups(1).Columns(7).HasHeadBackColor=   -1  'True
            Groups(1).Columns(7).HasBackColor=   -1  'True
            Groups(1).Columns(7).HeadForeColor=   16777215
            Groups(1).Columns(7).HeadBackColor=   32896
            Groups(1).Columns(7).BackColor=   16777160
            Groups(1).Columns(8).Width=   1640
            Groups(1).Columns(8).Caption=   "Obligatorio"
            Groups(1).Columns(8).Name=   "OBL"
            Groups(1).Columns(8).CaptionAlignment=   2
            Groups(1).Columns(8).DataField=   "Column 19"
            Groups(1).Columns(8).DataType=   8
            Groups(1).Columns(8).FieldLen=   256
            Groups(1).Columns(8).Style=   2
            Groups(1).Columns(8).HasHeadForeColor=   -1  'True
            Groups(1).Columns(8).HasHeadBackColor=   -1  'True
            Groups(1).Columns(8).HeadForeColor=   16777215
            Groups(1).Columns(8).HeadBackColor=   32896
            Groups(2).Width =   2646
            Groups(2).Caption=   "Preferencia"
            Groups(2).HasHeadForeColor=   -1  'True
            Groups(2).HasHeadBackColor=   -1  'True
            Groups(2).HeadForeColor=   16777215
            Groups(2).HeadBackColor=   32896
            Groups(2).Columns(0).Width=   2646
            Groups(2).Columns(0).Caption=   "Valores bajos"
            Groups(2).Columns(0).Name=   "VALORBAJO"
            Groups(2).Columns(0).CaptionAlignment=   2
            Groups(2).Columns(0).DataField=   "Column 20"
            Groups(2).Columns(0).DataType=   8
            Groups(2).Columns(0).FieldLen=   256
            Groups(2).Columns(0).Style=   2
            Groups(2).Columns(0).HasHeadForeColor=   -1  'True
            Groups(2).Columns(0).HasHeadBackColor=   -1  'True
            Groups(2).Columns(0).HasBackColor=   -1  'True
            Groups(2).Columns(0).HeadForeColor=   16777215
            Groups(2).Columns(0).HeadBackColor=   32896
            Groups(2).Columns(0).BackColor=   16777215
            Groups(3).Width =   8493
            Groups(3).Caption=   "Relaci�n con precio"
            Groups(3).HasHeadForeColor=   -1  'True
            Groups(3).HasHeadBackColor=   -1  'True
            Groups(3).HeadForeColor=   16777215
            Groups(3).HeadBackColor=   32896
            Groups(3).Columns.Count=   3
            Groups(3).Columns(0).Width=   1826
            Groups(3).Columns(0).Caption=   "Operaci�n"
            Groups(3).Columns(0).Name=   "OP"
            Groups(3).Columns(0).CaptionAlignment=   2
            Groups(3).Columns(0).DataField=   "Column 21"
            Groups(3).Columns(0).DataType=   8
            Groups(3).Columns(0).FieldLen=   256
            Groups(3).Columns(0).Style=   3
            Groups(3).Columns(0).Row.Count=   6
            Groups(3).Columns(0).Col.Count=   2
            Groups(3).Columns(0).Row(0).Col(0)=   "+"
            Groups(3).Columns(0).Row(1).Col(0)=   "-"
            Groups(3).Columns(0).Row(2).Col(0)=   "*"
            Groups(3).Columns(0).Row(3).Col(0)=   "/"
            Groups(3).Columns(0).Row(4).Col(0)=   "+ %"
            Groups(3).Columns(0).Row(5).Col(0)=   "- %"
            Groups(3).Columns(0).HasHeadForeColor=   -1  'True
            Groups(3).Columns(0).HasHeadBackColor=   -1  'True
            Groups(3).Columns(0).HasBackColor=   -1  'True
            Groups(3).Columns(0).HeadForeColor=   16777215
            Groups(3).Columns(0).HeadBackColor=   32896
            Groups(3).Columns(0).BackColor=   16777215
            Groups(3).Columns(1).Width=   4471
            Groups(3).Columns(1).Caption=   "Aplicar a precio"
            Groups(3).Columns(1).Name=   "APLIC_PREC"
            Groups(3).Columns(1).CaptionAlignment=   2
            Groups(3).Columns(1).DataField=   "Column 22"
            Groups(3).Columns(1).DataType=   8
            Groups(3).Columns(1).FieldLen=   256
            Groups(3).Columns(1).Style=   3
            Groups(3).Columns(1).Row.Count=   4
            Groups(3).Columns(1).Col.Count=   2
            Groups(3).Columns(1).Row(0).Col(0)=   "Total de la oferta"
            Groups(3).Columns(1).Row(1).Col(0)=   "Total del grupo"
            Groups(3).Columns(1).Row(2).Col(0)=   "Total del item"
            Groups(3).Columns(1).Row(3).Col(0)=   "Unitario del item"
            Groups(3).Columns(1).HasHeadForeColor=   -1  'True
            Groups(3).Columns(1).HasHeadBackColor=   -1  'True
            Groups(3).Columns(1).HasBackColor=   -1  'True
            Groups(3).Columns(1).HeadForeColor=   16777215
            Groups(3).Columns(1).HeadBackColor=   32896
            Groups(3).Columns(1).BackColor=   16777215
            Groups(3).Columns(2).Width=   2196
            Groups(3).Columns(2).Caption=   "Aplicar por defecto"
            Groups(3).Columns(2).Name=   "DEF_PREC"
            Groups(3).Columns(2).CaptionAlignment=   2
            Groups(3).Columns(2).DataField=   "Column 23"
            Groups(3).Columns(2).DataType=   8
            Groups(3).Columns(2).FieldLen=   256
            Groups(3).Columns(2).Style=   2
            Groups(3).Columns(2).HasHeadForeColor=   -1  'True
            Groups(3).Columns(2).HasHeadBackColor=   -1  'True
            Groups(3).Columns(2).HeadForeColor=   16777215
            Groups(3).Columns(2).HeadBackColor=   32896
            Groups(4).Width =   1852
            Groups(4).Caption=   "Ponderaci�n"
            Groups(4).HasHeadForeColor=   -1  'True
            Groups(4).HasHeadBackColor=   -1  'True
            Groups(4).HeadForeColor=   16777215
            Groups(4).HeadBackColor=   32896
            Groups(4).Columns.Count=   3
            Groups(4).Columns(0).Width=   1852
            Groups(4).Columns(0).Name=   "POND"
            Groups(4).Columns(0).DataField=   "Column 24"
            Groups(4).Columns(0).DataType=   8
            Groups(4).Columns(0).FieldLen=   256
            Groups(4).Columns(0).Style=   4
            Groups(4).Columns(0).ButtonsAlways=   -1  'True
            Groups(4).Columns(0).HasHeadForeColor=   -1  'True
            Groups(4).Columns(0).HasHeadBackColor=   -1  'True
            Groups(4).Columns(0).HasForeColor=   -1  'True
            Groups(4).Columns(0).HasBackColor=   -1  'True
            Groups(4).Columns(0).HeadForeColor=   16777215
            Groups(4).Columns(0).HeadBackColor=   32896
            Groups(4).Columns(1).Width=   820
            Groups(4).Columns(1).Visible=   0   'False
            Groups(4).Columns(1).Caption=   "NUEVO"
            Groups(4).Columns(1).Name=   "TipoPond"
            Groups(4).Columns(1).DataField=   "Column 25"
            Groups(4).Columns(1).DataType=   8
            Groups(4).Columns(1).FieldLen=   256
            Groups(4).Columns(2).Width=   1058
            Groups(4).Columns(2).Visible=   0   'False
            Groups(4).Columns(2).Caption=   "HIDDENDESCR"
            Groups(4).Columns(2).Name=   "HIDENDESCR"
            Groups(4).Columns(2).DataField=   "Column 26"
            Groups(4).Columns(2).DataType=   8
            Groups(4).Columns(2).FieldLen=   256
            _ExtentX        =   14376
            _ExtentY        =   5292
            _StockProps     =   79
            BackColor       =   -2147483648
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgPlantillas 
            Height          =   930
            Left            =   120
            TabIndex        =   26
            Top             =   960
            Width           =   8520
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            Col.Count       =   7
            stylesets.count =   2
            stylesets(0).Name=   "Gris"
            stylesets(0).BackColor=   13619151
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPlantillasProce.frx":06B3
            stylesets(1).Name=   "Amarillo"
            stylesets(1).BackColor=   13172735
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmPlantillasProce.frx":06CF
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   7
            Columns(0).Width=   1032
            Columns(0).Caption=   "Usar"
            Columns(0).Name =   "USAR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).Style=   2
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   13619151
            Columns(1).Width=   8916
            Columns(1).Caption=   "Dato"
            Columns(1).Name =   "DATO"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   13619151
            Columns(2).Width=   1323
            Columns(2).Caption=   "Proceso"
            Columns(2).Name =   "PROCE"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   2
            Columns(2).HasBackColor=   -1  'True
            Columns(2).BackColor=   13619151
            Columns(3).Width=   1217
            Columns(3).Caption=   "Grupo"
            Columns(3).Name =   "GRUPO"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Style=   2
            Columns(3).HasBackColor=   -1  'True
            Columns(3).BackColor=   13619151
            Columns(4).Width=   820
            Columns(4).Name =   "BOTONGRUP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Style=   4
            Columns(4).ButtonsAlways=   -1  'True
            Columns(4).HasBackColor=   -1  'True
            Columns(4).BackColor=   13619151
            Columns(5).Width=   1164
            Columns(5).Caption=   "Item"
            Columns(5).Name =   "ITEM"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Style=   2
            Columns(5).HasBackColor=   -1  'True
            Columns(5).BackColor=   13619151
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "INDICE"
            Columns(6).Name =   "INDICE"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            _ExtentX        =   15028
            _ExtentY        =   1640
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.PictureBox picNavigateAtrib 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Left            =   -74850
            ScaleHeight     =   420
            ScaleWidth      =   8520
            TabIndex        =   19
            Top             =   5940
            Width           =   8520
            Begin VB.CommandButton cmdEliAtr 
               Caption         =   "&Eliminar+"
               Height          =   345
               Left            =   1080
               TabIndex        =   24
               TabStop         =   0   'False
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.CommandButton cmdAnyadirAtr 
               Caption         =   "&A�adir"
               Height          =   345
               Left            =   0
               TabIndex        =   23
               TabStop         =   0   'False
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.CommandButton cmdModoEdicionAtr 
               Caption         =   "&Edici�n"
               Height          =   345
               Left            =   7500
               TabIndex        =   22
               TabStop         =   0   'False
               Top             =   60
               Width           =   1005
            End
            Begin VB.CommandButton cmdDeshacerAtr 
               Caption         =   "&Deshacer"
               Enabled         =   0   'False
               Height          =   345
               Left            =   2160
               TabIndex        =   21
               TabStop         =   0   'False
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.CommandButton cmdRestAtr 
               Caption         =   "&Restaurar"
               Height          =   345
               Left            =   1480
               TabIndex        =   20
               TabStop         =   0   'False
               Top             =   60
               Width           =   1005
            End
            Begin VB.CommandButton cmdOrden 
               Caption         =   "&Ordenar"
               Height          =   345
               Index           =   0
               Left            =   0
               TabIndex        =   25
               Top             =   60
               Width           =   1405
            End
         End
         Begin VB.PictureBox picNavigateGrupos 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Left            =   -74850
            ScaleHeight     =   420
            ScaleWidth      =   8520
            TabIndex        =   12
            Top             =   5940
            Width           =   8520
            Begin VB.CommandButton cmdRestGrupo 
               Caption         =   "&Restaurar"
               Height          =   345
               Left            =   0
               TabIndex        =   18
               TabStop         =   0   'False
               Top             =   60
               Width           =   1005
            End
            Begin VB.CommandButton cmdDeshacer 
               Caption         =   "&Deshacer"
               Enabled         =   0   'False
               Height          =   345
               Left            =   2280
               TabIndex        =   17
               TabStop         =   0   'False
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.CommandButton cmdCodigo 
               Caption         =   "C�&digo"
               Height          =   345
               Left            =   3390
               TabIndex        =   16
               TabStop         =   0   'False
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.CommandButton cmdModoEdicion 
               Caption         =   "&Edici�n"
               Height          =   345
               Left            =   7500
               TabIndex        =   15
               TabStop         =   0   'False
               Top             =   60
               Width           =   1005
            End
            Begin VB.CommandButton cmdAnyadir 
               Caption         =   "&A�adir"
               Height          =   345
               Left            =   0
               TabIndex        =   14
               TabStop         =   0   'False
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.CommandButton cmdEliminar 
               Caption         =   "&Eliminar+"
               Height          =   345
               Left            =   1140
               TabIndex        =   13
               TabStop         =   0   'False
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
         End
         Begin VB.PictureBox picNavigateAmbito 
            BorderStyle     =   0  'None
            Height          =   420
            Left            =   120
            ScaleHeight     =   420
            ScaleWidth      =   8520
            TabIndex        =   7
            Top             =   6720
            Width           =   8520
            Begin VB.CommandButton cmdRestaurar 
               Caption         =   "&Restaurar"
               Height          =   345
               Left            =   1080
               TabIndex        =   11
               Top             =   60
               Width           =   1005
            End
            Begin VB.CommandButton cmdMod 
               Caption         =   "&Modificar"
               Height          =   345
               Left            =   0
               TabIndex        =   10
               Top             =   60
               Width           =   1005
            End
            Begin VB.CommandButton cmdAceptar 
               Caption         =   "&Aceptar"
               Height          =   345
               Left            =   3420
               TabIndex        =   9
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.CommandButton cmdCancelar 
               Caption         =   "&Cancelar"
               Height          =   345
               Left            =   4590
               TabIndex        =   8
               Top             =   60
               Visible         =   0   'False
               Width           =   1005
            End
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgGrupos 
            Height          =   2445
            Left            =   -74850
            TabIndex        =   27
            Top             =   450
            Width           =   8820
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   4
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            ForeColorEven   =   0
            RowHeight       =   423
            Columns.Count   =   4
            Columns(0).Width=   3440
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   3
            Columns(0).HasHeadForeColor=   -1  'True
            Columns(0).HasHeadBackColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).HeadForeColor=   16777215
            Columns(0).HeadBackColor=   32896
            Columns(0).BackColor=   16776960
            Columns(1).Width=   10636
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "NOM"
            Columns(1).CaptionAlignment=   0
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   100
            Columns(1).HasHeadForeColor=   -1  'True
            Columns(1).HasHeadBackColor=   -1  'True
            Columns(1).HasForeColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).HeadForeColor=   16777215
            Columns(1).HeadBackColor=   32896
            Columns(1).BackColor=   16777215
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "DESCR"
            Columns(2).Name =   "DESCR"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   500
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "SOBRE"
            Columns(3).Name =   "SOBRE"
            Columns(3).CaptionAlignment=   0
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).HasHeadForeColor=   -1  'True
            Columns(3).HasHeadBackColor=   -1  'True
            Columns(3).HasBackColor=   -1  'True
            Columns(3).HeadForeColor=   16777215
            Columns(3).HeadBackColor=   32896
            Columns(3).BackColor=   16777215
            _ExtentX        =   15557
            _ExtentY        =   4313
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddAplicarA 
            Height          =   765
            Left            =   -73770
            TabIndex        =   30
            Top             =   2430
            Width           =   1515
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPlantillasProce.frx":06EB
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3201
            Columns(0).Name =   "NOMBRE"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   1349
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddAmbito 
            Height          =   765
            Left            =   -74480
            TabIndex        =   31
            Top             =   1350
            Width           =   1515
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPlantillasProce.frx":0707
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3201
            Columns(0).Name =   "NOMBRE"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   1349
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddSigno 
            Height          =   735
            Left            =   -74480
            TabIndex        =   32
            Top             =   2940
            Width           =   1575
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPlantillasProce.frx":0723
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).Name =   "SIGNO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   1296
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcSeleccion 
            Height          =   285
            Left            =   -74880
            TabIndex        =   33
            Top             =   450
            Width           =   8475
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            AllowNull       =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   14949
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin MSComctlLib.TreeView tvwEstrMat 
            Height          =   3600
            Left            =   -74640
            TabIndex        =   64
            Top             =   1200
            Width           =   5355
            _ExtentX        =   9446
            _ExtentY        =   6350
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            HotTracking     =   -1  'True
            ImageList       =   "ImageList3"
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComctlLib.ImageList ImageList3 
            Left            =   -74520
            Top             =   5040
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   16
            ImageHeight     =   16
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   9
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":073F
                  Key             =   "GMN1"
                  Object.Tag             =   "GMN1"
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":07EF
                  Key             =   "Raiz"
                  Object.Tag             =   "Raiz"
               EndProperty
               BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":0C43
                  Key             =   "GMN1A"
                  Object.Tag             =   "GMN1A"
               EndProperty
               BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":0D04
                  Key             =   "GMN2A"
                  Object.Tag             =   "GMN2A"
               EndProperty
               BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":0DC4
                  Key             =   "GMN2"
                  Object.Tag             =   "GMN2"
               EndProperty
               BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":0E74
                  Key             =   "GMN3A"
                  Object.Tag             =   "GMN3A"
               EndProperty
               BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":0F3E
                  Key             =   "GMN3"
                  Object.Tag             =   "GMN3"
               EndProperty
               BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":0FF7
                  Key             =   "GMN4"
                  Object.Tag             =   "GMN4"
               EndProperty
               BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":10A7
                  Key             =   "GMN4A"
                  Object.Tag             =   "GMN4A"
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.TreeView tvwEstrMatMod 
            Height          =   4140
            Left            =   -74400
            TabIndex        =   69
            Top             =   1440
            Width           =   7395
            _ExtentX        =   13044
            _ExtentY        =   7303
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            Checkboxes      =   -1  'True
            HotTracking     =   -1  'True
            ImageList       =   "ImageListMod"
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComctlLib.ImageList ImageListMod 
            Left            =   -74880
            Top             =   5160
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   16
            ImageHeight     =   16
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   9
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":1167
                  Key             =   "GMN1"
                  Object.Tag             =   "GMN1"
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":1217
                  Key             =   "Raiz"
                  Object.Tag             =   "Raiz"
               EndProperty
               BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":166B
                  Key             =   "GMN2"
                  Object.Tag             =   "GMN2"
               EndProperty
               BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":171B
                  Key             =   "GMN1A"
                  Object.Tag             =   "GMN1A"
               EndProperty
               BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":1A6D
                  Key             =   "GMN2A"
                  Object.Tag             =   "GMN2A"
               EndProperty
               BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":1B1D
                  Key             =   "GMN3"
                  Object.Tag             =   "GMN3"
               EndProperty
               BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":1BD6
                  Key             =   "GMN3A"
                  Object.Tag             =   "GMN3A"
               EndProperty
               BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":1C8D
                  Key             =   "GMN4"
                  Object.Tag             =   "GMN4"
               EndProperty
               BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPlantillasProce.frx":1D3D
                  Key             =   "GMN4A"
                  Object.Tag             =   "GMN4A"
               EndProperty
            EndProperty
         End
         Begin SSSplitter.SSSplitter SSSplitterEsp 
            Height          =   5535
            Left            =   -74880
            TabIndex        =   73
            Top             =   480
            Width           =   8415
            _ExtentX        =   14843
            _ExtentY        =   9763
            _Version        =   262144
            PaneTree        =   "frmPlantillasProce.frx":1DED
            Begin VB.PictureBox picSplit1 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   1275
               Left            =   30
               ScaleHeight     =   1275
               ScaleWidth      =   8355
               TabIndex        =   88
               Top             =   30
               Width           =   8355
               Begin VB.TextBox txtProceEsp 
                  Height          =   1035
                  Left            =   120
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   89
                  Top             =   120
                  Width           =   8145
               End
               Begin MSComctlLib.ImageList ImageList2 
                  Left            =   0
                  Top             =   0
                  _ExtentX        =   1005
                  _ExtentY        =   1005
                  BackColor       =   -2147483643
                  ImageWidth      =   16
                  ImageHeight     =   16
                  MaskColor       =   12632256
                  _Version        =   393216
                  BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
                     NumListImages   =   1
                     BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                        Picture         =   "frmPlantillasProce.frx":1E5F
                        Key             =   "ESP"
                        Object.Tag             =   "ESP"
                     EndProperty
                  EndProperty
               End
            End
            Begin VB.PictureBox picSplit2 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   2025
               Left            =   30
               ScaleHeight     =   2025
               ScaleWidth      =   8355
               TabIndex        =   82
               Top             =   1395
               Width           =   8355
               Begin VB.PictureBox picBotonesAtributos 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   615
                  Left            =   0
                  ScaleHeight     =   615
                  ScaleWidth      =   8310
                  TabIndex        =   83
                  Top             =   0
                  Width           =   8310
                  Begin VB.CommandButton cmdSortDesEsp 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   300
                     Left            =   7080
                     Picture         =   "frmPlantillasProce.frx":1FB9
                     Style           =   1  'Graphical
                     TabIndex        =   94
                     Top             =   120
                     Width           =   300
                  End
                  Begin VB.CommandButton cmdSortAscEsp 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   300
                     Left            =   6720
                     Picture         =   "frmPlantillasProce.frx":2013
                     Style           =   1  'Graphical
                     TabIndex        =   93
                     Top             =   120
                     Width           =   300
                  End
                  Begin VB.CommandButton cmdAnyaAtribEsp 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   300
                     Left            =   7560
                     Picture         =   "frmPlantillasProce.frx":206D
                     Style           =   1  'Graphical
                     TabIndex        =   85
                     Top             =   120
                     UseMaskColor    =   -1  'True
                     Width           =   300
                  End
                  Begin VB.CommandButton cmdElimAtribEsp 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   300
                     Left            =   7920
                     Picture         =   "frmPlantillasProce.frx":20EF
                     Style           =   1  'Graphical
                     TabIndex        =   84
                     Top             =   120
                     UseMaskColor    =   -1  'True
                     Width           =   300
                  End
                  Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
                     Height          =   360
                     Left            =   1920
                     TabIndex        =   86
                     Top             =   120
                     Width           =   3015
                     ListAutoValidate=   0   'False
                     ListAutoPosition=   0   'False
                     _Version        =   196617
                     DataMode        =   2
                     ColumnHeaders   =   0   'False
                     stylesets.count =   1
                     stylesets(0).Name=   "Normal"
                     stylesets(0).HasFont=   -1  'True
                     BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "Tahoma"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     stylesets(0).Picture=   "frmPlantillasProce.frx":2181
                     DividerStyle    =   3
                     StyleSet        =   "Normal"
                     ForeColorEven   =   0
                     BackColorOdd    =   16777215
                     RowHeight       =   423
                     Columns.Count   =   2
                     Columns(0).Width=   3200
                     Columns(0).Visible=   0   'False
                     Columns(0).Caption=   "VALOR"
                     Columns(0).Name =   "VALOR"
                     Columns(0).DataField=   "Column 0"
                     Columns(0).DataType=   8
                     Columns(0).FieldLen=   256
                     Columns(1).Width=   3200
                     Columns(1).Caption=   "DESC"
                     Columns(1).Name =   "DESC"
                     Columns(1).DataField=   "Column 1"
                     Columns(1).DataType=   8
                     Columns(1).FieldLen=   256
                     _ExtentX        =   5318
                     _ExtentY        =   635
                     _StockProps     =   77
                  End
                  Begin MSComDlg.CommonDialog cmmdEsp 
                     Left            =   5040
                     Top             =   0
                     _ExtentX        =   847
                     _ExtentY        =   847
                     _Version        =   393216
                     CancelError     =   -1  'True
                     DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
                     MaxFileSize     =   1500
                  End
                  Begin SSDataWidgets_B.SSDBDropDown sdbddAmbitoEspec 
                     Height          =   360
                     Left            =   5520
                     TabIndex        =   90
                     Top             =   240
                     Width           =   3015
                     ListAutoValidate=   0   'False
                     ListAutoPosition=   0   'False
                     _Version        =   196617
                     DataMode        =   2
                     ColumnHeaders   =   0   'False
                     stylesets.count =   1
                     stylesets(0).Name=   "Normal"
                     stylesets(0).HasFont=   -1  'True
                     BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "Tahoma"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     stylesets(0).Picture=   "frmPlantillasProce.frx":219D
                     DividerStyle    =   3
                     StyleSet        =   "Normal"
                     ForeColorEven   =   0
                     BackColorOdd    =   16777215
                     RowHeight       =   423
                     Columns.Count   =   2
                     Columns(0).Width=   3200
                     Columns(0).Visible=   0   'False
                     Columns(0).Caption=   "VALOR"
                     Columns(0).Name =   "VALOR"
                     Columns(0).DataField=   "Column 0"
                     Columns(0).DataType=   8
                     Columns(0).FieldLen=   256
                     Columns(1).Width=   3200
                     Columns(1).Caption=   "DESC"
                     Columns(1).Name =   "DESC"
                     Columns(1).DataField=   "Column 1"
                     Columns(1).DataType=   8
                     Columns(1).FieldLen=   256
                     _ExtentX        =   5318
                     _ExtentY        =   635
                     _StockProps     =   77
                  End
                  Begin SSDataWidgets_B.SSDBDropDown sdbddValidacion 
                     Height          =   360
                     Left            =   5040
                     TabIndex        =   91
                     Top             =   240
                     Width           =   3015
                     ListAutoValidate=   0   'False
                     ListAutoPosition=   0   'False
                     _Version        =   196617
                     DataMode        =   2
                     ColumnHeaders   =   0   'False
                     stylesets.count =   1
                     stylesets(0).Name=   "Normal"
                     stylesets(0).HasFont=   -1  'True
                     BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "Tahoma"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     stylesets(0).Picture=   "frmPlantillasProce.frx":21B9
                     DividerStyle    =   3
                     StyleSet        =   "Normal"
                     ForeColorEven   =   0
                     BackColorOdd    =   16777215
                     RowHeight       =   423
                     Columns.Count   =   2
                     Columns(0).Width=   3200
                     Columns(0).Visible=   0   'False
                     Columns(0).Caption=   "VALOR"
                     Columns(0).Name =   "VALOR"
                     Columns(0).DataField=   "Column 0"
                     Columns(0).DataType=   8
                     Columns(0).FieldLen=   256
                     Columns(1).Width=   3200
                     Columns(1).Caption=   "DESC"
                     Columns(1).Name =   "DESC"
                     Columns(1).DataField=   "Column 1"
                     Columns(1).DataType=   8
                     Columns(1).FieldLen=   256
                     _ExtentX        =   5318
                     _ExtentY        =   635
                     _StockProps     =   77
                  End
                  Begin SSDataWidgets_B.SSDBCombo sdbcGrupoEsp 
                     Height          =   285
                     Left            =   120
                     TabIndex        =   92
                     Top             =   120
                     Width           =   6075
                     DataFieldList   =   "Column 0"
                     ListAutoValidate=   0   'False
                     ListAutoPosition=   0   'False
                     AllowInput      =   0   'False
                     AllowNull       =   0   'False
                     _Version        =   196617
                     DataMode        =   2
                     ForeColorEven   =   0
                     BackColorOdd    =   16777215
                     RowHeight       =   423
                     Columns.Count   =   2
                     Columns(0).Width=   2540
                     Columns(0).Caption=   "Cod"
                     Columns(0).Name =   "COD"
                     Columns(0).DataField=   "Column 0"
                     Columns(0).DataType=   8
                     Columns(0).FieldLen=   256
                     Columns(1).Width=   6482
                     Columns(1).Caption=   "Nombre"
                     Columns(1).Name =   "DEN"
                     Columns(1).DataField=   "Column 1"
                     Columns(1).DataType=   8
                     Columns(1).FieldLen=   256
                     _ExtentX        =   10716
                     _ExtentY        =   503
                     _StockProps     =   93
                     BackColor       =   16777215
                  End
               End
               Begin SSDataWidgets_B.SSDBGrid sdbgAtributosEsp 
                  Height          =   1125
                  Left            =   120
                  TabIndex        =   87
                  Top             =   600
                  Width           =   8205
                  _Version        =   196617
                  DataMode        =   2
                  Col.Count       =   18
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmPlantillasProce.frx":21D5
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   2
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   18
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "IDATRIB"
                  Columns(0).Name =   "IDATRIB"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).HasHeadForeColor=   -1  'True
                  Columns(0).HasHeadBackColor=   -1  'True
                  Columns(0).HeadForeColor=   16777215
                  Columns(0).HeadBackColor=   32896
                  Columns(1).Width=   1455
                  Columns(1).Caption=   "INTERNO"
                  Columns(1).Name =   "INTERNO"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(1).Style=   2
                  Columns(1).HasHeadForeColor=   -1  'True
                  Columns(1).HasHeadBackColor=   -1  'True
                  Columns(1).HeadForeColor=   16777215
                  Columns(1).HeadBackColor=   32896
                  Columns(2).Width=   1349
                  Columns(2).Caption=   "Pedido"
                  Columns(2).Name =   "PEDIDO"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(2).Style=   2
                  Columns(2).HasHeadForeColor=   -1  'True
                  Columns(2).HasHeadBackColor=   -1  'True
                  Columns(2).HeadForeColor=   16777215
                  Columns(2).HeadBackColor=   32896
                  Columns(3).Width=   3519
                  Columns(3).Caption=   "COD"
                  Columns(3).Name =   "COD"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(3).Locked=   -1  'True
                  Columns(3).HasHeadForeColor=   -1  'True
                  Columns(3).HasHeadBackColor=   -1  'True
                  Columns(3).HasBackColor=   -1  'True
                  Columns(3).HeadForeColor=   16777215
                  Columns(3).HeadBackColor=   32896
                  Columns(3).BackColor=   16777160
                  Columns(4).Width=   4577
                  Columns(4).Caption=   "DEN"
                  Columns(4).Name =   "DEN"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(4).Locked=   -1  'True
                  Columns(4).HasHeadForeColor=   -1  'True
                  Columns(4).HasHeadBackColor=   -1  'True
                  Columns(4).HasBackColor=   -1  'True
                  Columns(4).HeadForeColor=   16777215
                  Columns(4).HeadBackColor=   32896
                  Columns(4).BackColor=   16777160
                  Columns(5).Width=   1376
                  Columns(5).Caption=   "Descr."
                  Columns(5).Name =   "DESCR"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(5).Style=   4
                  Columns(5).ButtonsAlways=   -1  'True
                  Columns(5).HasHeadForeColor=   -1  'True
                  Columns(5).HasHeadBackColor=   -1  'True
                  Columns(5).HeadForeColor=   16777215
                  Columns(5).HeadBackColor=   32896
                  Columns(6).Width=   2302
                  Columns(6).Caption=   "Tipo"
                  Columns(6).Name =   "Tipo"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  Columns(6).Locked=   -1  'True
                  Columns(6).HasHeadForeColor=   -1  'True
                  Columns(6).HasHeadBackColor=   -1  'True
                  Columns(6).HasBackColor=   -1  'True
                  Columns(6).HeadForeColor=   16777215
                  Columns(6).HeadBackColor=   32896
                  Columns(6).BackColor=   16777160
                  Columns(7).Width=   3200
                  Columns(7).Visible=   0   'False
                  Columns(7).Caption=   "idTipo"
                  Columns(7).Name =   "idTipo"
                  Columns(7).DataField=   "Column 7"
                  Columns(7).DataType=   8
                  Columns(7).FieldLen=   256
                  Columns(8).Width=   3200
                  Columns(8).Caption=   "Valor"
                  Columns(8).Name =   "VALOR"
                  Columns(8).DataField=   "Column 8"
                  Columns(8).DataType=   8
                  Columns(8).FieldLen=   256
                  Columns(8).HasHeadForeColor=   -1  'True
                  Columns(8).HasHeadBackColor=   -1  'True
                  Columns(8).HeadForeColor=   16777215
                  Columns(8).HeadBackColor=   32896
                  Columns(9).Width=   1773
                  Columns(9).Caption=   "Ambito"
                  Columns(9).Name =   "AMBITO"
                  Columns(9).DataField=   "Column 9"
                  Columns(9).DataType=   8
                  Columns(9).FieldLen=   256
                  Columns(9).HasHeadForeColor=   -1  'True
                  Columns(9).HasHeadBackColor=   -1  'True
                  Columns(9).HeadForeColor=   16777215
                  Columns(9).HeadBackColor=   32896
                  Columns(10).Width=   1773
                  Columns(10).Caption=   "Obligatorio"
                  Columns(10).Name=   "OBLIGATORIO"
                  Columns(10).DataField=   "Column 10"
                  Columns(10).DataType=   8
                  Columns(10).FieldLen=   256
                  Columns(10).Style=   2
                  Columns(10).HasHeadForeColor=   -1  'True
                  Columns(10).HasHeadBackColor=   -1  'True
                  Columns(10).HeadForeColor=   16777215
                  Columns(10).HeadBackColor=   32896
                  Columns(11).Width=   2328
                  Columns(11).Caption=   "Integraci�n"
                  Columns(11).Name=   "INTEGRACION"
                  Columns(11).DataField=   "Column 11"
                  Columns(11).DataType=   8
                  Columns(11).FieldLen=   256
                  Columns(11).Locked=   -1  'True
                  Columns(11).HasHeadForeColor=   -1  'True
                  Columns(11).HasHeadBackColor=   -1  'True
                  Columns(11).HasBackColor=   -1  'True
                  Columns(11).HeadForeColor=   16777215
                  Columns(11).HeadBackColor=   32896
                  Columns(11).BackColor=   16777160
                  Columns(12).Width=   3200
                  Columns(12).Caption=   "Validacion"
                  Columns(12).Name=   "VALIDACION"
                  Columns(12).DataField=   "Column 12"
                  Columns(12).DataType=   8
                  Columns(12).FieldLen=   256
                  Columns(12).HasHeadForeColor=   -1  'True
                  Columns(12).HasHeadBackColor=   -1  'True
                  Columns(12).HeadForeColor=   16777215
                  Columns(12).HeadBackColor=   32896
                  Columns(13).Width=   3200
                  Columns(13).Visible=   0   'False
                  Columns(13).Caption=   "MIN"
                  Columns(13).Name=   "MIN"
                  Columns(13).DataField=   "Column 13"
                  Columns(13).DataType=   8
                  Columns(13).FieldLen=   256
                  Columns(14).Width=   3200
                  Columns(14).Visible=   0   'False
                  Columns(14).Caption=   "MAX"
                  Columns(14).Name=   "MAX"
                  Columns(14).DataField=   "Column 14"
                  Columns(14).DataType=   8
                  Columns(14).FieldLen=   256
                  Columns(15).Width=   3200
                  Columns(15).Visible=   0   'False
                  Columns(15).Caption=   "TIPO_DATOS"
                  Columns(15).Name=   "TIPO_DATOS"
                  Columns(15).DataField=   "Column 15"
                  Columns(15).DataType=   8
                  Columns(15).FieldLen=   256
                  Columns(16).Width=   3200
                  Columns(16).Visible=   0   'False
                  Columns(16).Caption=   "TIPO_INTRO"
                  Columns(16).Name=   "TIPO_INTRO"
                  Columns(16).DataField=   "Column 16"
                  Columns(16).DataType=   8
                  Columns(16).FieldLen=   256
                  Columns(17).Width=   3200
                  Columns(17).Visible=   0   'False
                  Columns(17).Caption=   "HIDENDESCR"
                  Columns(17).Name=   "HIDENDESCR"
                  Columns(17).DataField=   "Column 17"
                  Columns(17).DataType=   8
                  Columns(17).FieldLen=   256
                  _ExtentX        =   14473
                  _ExtentY        =   1984
                  _StockProps     =   79
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
            End
            Begin VB.PictureBox picSplit3 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   1995
               Left            =   30
               ScaleHeight     =   1995
               ScaleWidth      =   8355
               TabIndex        =   74
               Top             =   3510
               Width           =   8355
               Begin VB.PictureBox picNavigateEsp 
                  BorderStyle     =   0  'None
                  Height          =   375
                  Left            =   5880
                  ScaleHeight     =   375
                  ScaleWidth      =   2400
                  TabIndex        =   75
                  Top             =   1440
                  Visible         =   0   'False
                  Width           =   2400
                  Begin VB.CommandButton cmdAbrirEsp 
                     Height          =   300
                     Left            =   1920
                     Picture         =   "frmPlantillasProce.frx":21F1
                     Style           =   1  'Graphical
                     TabIndex        =   80
                     TabStop         =   0   'False
                     Top             =   0
                     UseMaskColor    =   -1  'True
                     Width           =   420
                  End
                  Begin VB.CommandButton cmdSalvarEsp 
                     Height          =   300
                     Left            =   1440
                     Picture         =   "frmPlantillasProce.frx":226D
                     Style           =   1  'Graphical
                     TabIndex        =   79
                     TabStop         =   0   'False
                     Top             =   0
                     UseMaskColor    =   -1  'True
                     Width           =   420
                  End
                  Begin VB.CommandButton cmdEliminarEsp 
                     Height          =   300
                     Left            =   480
                     Picture         =   "frmPlantillasProce.frx":22EE
                     Style           =   1  'Graphical
                     TabIndex        =   78
                     TabStop         =   0   'False
                     Top             =   0
                     UseMaskColor    =   -1  'True
                     Width           =   420
                  End
                  Begin VB.CommandButton cmdAnyadirEsp 
                     Height          =   300
                     Left            =   0
                     Picture         =   "frmPlantillasProce.frx":2371
                     Style           =   1  'Graphical
                     TabIndex        =   77
                     TabStop         =   0   'False
                     Top             =   0
                     UseMaskColor    =   -1  'True
                     Width           =   420
                  End
                  Begin VB.CommandButton cmdModificarEsp 
                     Height          =   300
                     Left            =   960
                     Picture         =   "frmPlantillasProce.frx":23E3
                     Style           =   1  'Graphical
                     TabIndex        =   76
                     TabStop         =   0   'False
                     Top             =   0
                     UseMaskColor    =   -1  'True
                     Width           =   420
                  End
               End
               Begin MSComctlLib.ListView lstvwEsp 
                  Height          =   1065
                  Left            =   120
                  TabIndex        =   81
                  Top             =   120
                  Width           =   8145
                  _ExtentX        =   14367
                  _ExtentY        =   1879
                  View            =   3
                  Arrange         =   1
                  LabelEdit       =   1
                  LabelWrap       =   0   'False
                  HideSelection   =   0   'False
                  HotTracking     =   -1  'True
                  _Version        =   393217
                  Icons           =   "ImageList2"
                  SmallIcons      =   "ImageList2"
                  ForeColor       =   -2147483640
                  BackColor       =   -2147483643
                  BorderStyle     =   1
                  Appearance      =   1
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  NumItems        =   5
                  BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     Text            =   "Fichero"
                     Object.Width           =   3939
                  EndProperty
                  BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   1
                     Text            =   "Tama�o"
                     Object.Width           =   2540
                  EndProperty
                  BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   2
                     Text            =   "Ruta del archivo"
                     Object.Width           =   2540
                  EndProperty
                  BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   3
                     Text            =   "Comentario"
                     Object.Width           =   7272
                  EndProperty
                  BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                     SubItemIndex    =   4
                     Text            =   "Fecha"
                     Object.Width           =   3246
                  EndProperty
               End
            End
         End
         Begin VB.Label Label1 
            BackColor       =   &H00808000&
            Caption         =   "Archivos adjuntos"
            ForeColor       =   &H80000005&
            Height          =   240
            Left            =   -74880
            TabIndex        =   36
            Top             =   1740
            Width           =   1410
         End
      End
      Begin VB.Label lblTitulo 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   5
         Top             =   0
         Width           =   8800
      End
   End
   Begin VB.PictureBox picCrearPlant 
      BorderStyle     =   0  'None
      Height          =   8805
      Left            =   0
      ScaleHeight     =   8805
      ScaleMode       =   0  'User
      ScaleWidth      =   1680
      TabIndex        =   0
      Top             =   120
      Width           =   1675
      Begin VB.CommandButton cmdCrearPlant 
         Caption         =   "&Crear plantilla"
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   8400
         Width           =   1600
      End
      Begin MSComctlLib.ListView lstvwplantillas 
         Height          =   8265
         Left            =   60
         TabIndex        =   2
         Top             =   -15
         Width           =   1605
         _ExtentX        =   2831
         _ExtentY        =   14579
         View            =   2
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         OLEDropMode     =   1
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         OLEDropMode     =   1
         NumItems        =   0
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillasProce.frx":252D
            Key             =   "Plantilla"
            Object.Tag             =   "Plantilla"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPlantillasProce.frx":297F
            Key             =   "PlantillaConVista"
            Object.Tag             =   "PlantillaConVista"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPlantillasProce"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Valores par�metros subasta
Private m_iSubTipo As Integer
Private m_iSubModo As Integer
Private m_vSubDuracion As Variant
Private m_iSubastaEspera As Integer
Private m_bSubPublicar As Boolean
Private m_bSubNotifEventos As Boolean
Private m_bSubastaProve As Boolean
Private m_bSubVerDesdePrimPuja As Boolean
Private m_bSubastaPrecioPuja As Boolean
Private m_bSubastaPujas As Boolean
Private m_bSubastaBajMinPuja As Boolean
Private m_iSubBajMinGanTipo As Integer
Private m_vSubBajMinGanProcVal As Variant
Private m_vSubBajMinGanGrupoVal As Variant
Private m_vSubBajMinGanItemVal As Variant
Private m_bSubBajMinProve As Boolean
Private m_iSubBajMinProveTipo As Integer
Private m_vSubBajMinProveProcVal As Variant
Private m_vSubBajMinProveGrupoVal As Variant
Private m_vSubBajMinProveItemVal As Variant
Private m_dcSubTextosFin As Dictionary

Public g_oPlantillaSeleccionada As CPlantilla
Public g_oIBaseDatos As IBaseDatos
Public g_Accion As AccionesSummit

Public g_lNumID As Long
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean
Public g_sCodGRP As String
Public g_bGuardarEnDefinicion  As Boolean
Public g_ofrmATRIB As frmAtrib
Public g_sOrigen As String
Public g_bSoloRuta As Boolean

'Para las especificaciones
Public g_bCancelarEsp As Boolean
Public g_sComentario As String

Public g_oPlantillas As CPlantillas
Public m_oGrupos As CGrupos
Private m_oAtributoEnEdicion As CAtributo
Private m_oGrupoEnEdicion As CGrupo
Private m_oAtributo As CAtributo
'Seguridad
Private m_bModif As Boolean
Private g_bPermitir As Boolean
Private m_bModifAtrib As Boolean 'Usuario
Public m_bModificarDefAtrib As Boolean 'controlar las columnas q pueden modif en def_atrib
Private m_bMostrarSolicitud As Boolean

Private m_bCargarPrimero As Boolean
Private m_bActualizar As Boolean
Private m_bModoEdicion As Boolean
Public m_bModoEdicionAtr As Boolean
'edu T98
Public m_bModoEdicionMateriales As Boolean

Private m_iIndiListw As Integer

'''IDIOMAS
Private m_sPlantilla As String
Private m_sMensajeMultGrupos As String
Private m_sElGrupo As String
Private m_sCodigo As String
Private m_sConsulta As String
Private m_sEdicion As String
Private m_asDatos(11) As String
Private m_sModif As String
Private m_sDetalle As String
Private m_sA�adir As String
Private m_sIdiOrdenando As String
Private m_sFrmModCod As String
Private m_asAtrib(15) As String
Private m_asMensajes(1) As String
Private m_sMensajeGrupo As String
Private m_sMensajeProce As String
Private m_sIdiSelecAdjunto As String
Private m_sIdiTodosArchivos As String
Private m_sIdiArchivo As String
Private m_sIdiTipoOrig As String
Private m_sIdiGuardar As String
Private m_sIdiElArchivo As String
Private m_skb As String
Private m_sIdiMinutos As String


''' Control de errores
Private m_bModError As Boolean
Private m_bAnyaError As Boolean
Private m_bValError As Boolean

'Variable para controlar qu� dato estamos cambiando en la configuraci�n
Private m_iDatoActual As Integer '0-DEST/1-PAG/2-FECSUM/3-PROVE/4-ESP/5-DIST/6-PRES,etc.
Private m_bModoEdicionPlant As Boolean

'Para las especificaciones:
Private m_aSayFileNames() As String

Private m_ofrmConfigVistas  As frmConfiguracionVista

Private m_bCancelarConfGR As Boolean

Private oCEstructura As CAEstructuraMat
Private sIdiMat As String
Private sIdiMarcaruno As String

'Variable para no ejecutar el node check si se ha disparado el mousedown

Private bMouseDown As Boolean
Private SelNode As MSComctlLib.node

'permisos sobre asignacion de materiales

Private m_bRestringirSoloMaterialesComprador As Boolean
Private m_bRestringirAsignacionAtributosMateriales As Boolean

Public g_sCodGRPEsp As String
Public m_bModoEdicionAtrEsp As Boolean
Private m_sIdiTrue As String
Private m_sIdiFalse As String

Private m_bTieneMateriales As Boolean
Private m_bAccionChkSol As Boolean
' fin de declaraciones


Private Function GruposDefGruposAEliminar(ByVal arGrupos As Variant) As Variant
'****************************************************************************************
'Comprueba si el/los grupo/s que se va/n a eliminar es/son los �nicos que
'tienen alg�n dato definido a nivel de grupo. Si es as� y el dato es
'opcional se mostrar�a un mensaje avisando de que el ambito pasar� a no
'definido y si es obligatorio a �tem.
'Se devuelve un array con los datos opcionales y obligatorios que cambiar�an de �mbito.
'****************************************************************************************

Dim iGrupoDef As Integer
Dim iGrupoEli As Integer
Dim oGrupo As CGrupo
Dim i As Integer
Dim sDatosObl As String
Dim sDatosOpc As String
Dim sDatos As String
Dim vDatos As Variant

    If g_oPlantillaSeleccionada.Destino = EnGrupo Then
        For Each oGrupo In g_oPlantillaSeleccionada.Grupos
            If oGrupo.DefDestino Then
                iGrupoDef = iGrupoDef + 1
                For i = 1 To UBound(arGrupos)
                    If oGrupo.Codigo = arGrupos(i) Then
                        iGrupoEli = iGrupoEli + 1
                    End If
                Next
            End If
        Next
        If iGrupoDef = iGrupoEli Then
            sDatosObl = m_asDatos(0)
            sDatos = "0"
        End If
    End If
    
    iGrupoDef = 0: iGrupoEli = 0
    If g_oPlantillaSeleccionada.FormaPago = EnGrupo Then
        For Each oGrupo In g_oPlantillaSeleccionada.Grupos
            If oGrupo.DefFormaPago Then
                iGrupoDef = iGrupoDef + 1
                For i = 1 To UBound(arGrupos)
                    If oGrupo.Codigo = arGrupos(i) Then
                        iGrupoEli = iGrupoEli + 1
                    End If
                Next
            End If
        Next
        If iGrupoDef = iGrupoEli Then
            If sDatosObl = "" Then
                sDatosObl = m_asDatos(1)
            Else
                sDatosObl = sDatosObl & "," & m_asDatos(1)
            End If
            If sDatos = "" Then
                sDatos = "1"
            Else
                sDatos = sDatos & ",1"
            End If
        End If
    End If
    
    iGrupoDef = 0: iGrupoEli = 0
    If g_oPlantillaSeleccionada.FechaSuministro = EnGrupo Then
        For Each oGrupo In g_oPlantillaSeleccionada.Grupos
            If oGrupo.DefFechasSum Then
                iGrupoDef = iGrupoDef + 1
                For i = 1 To UBound(arGrupos)
                    If oGrupo.Codigo = arGrupos(i) Then
                        iGrupoEli = iGrupoEli + 1
                    End If
                Next
            End If
        Next
        If iGrupoDef = iGrupoEli Then
            If sDatosObl = "" Then
                sDatosObl = m_asDatos(2)
            Else
                sDatosObl = sDatosObl & "," & m_asDatos(2)
            End If
            If sDatos = "" Then
                sDatos = "2"
            Else
                sDatos = sDatos & ",2"
            End If
        End If
    End If
    
    iGrupoDef = 0: iGrupoEli = 0
    If g_oPlantillaSeleccionada.ProveedorAct = EnGrupo Then
        For Each oGrupo In g_oPlantillaSeleccionada.Grupos
            If oGrupo.DefProveActual Then
                iGrupoDef = iGrupoDef + 1
                For i = 1 To UBound(arGrupos)
                    If oGrupo.Codigo = arGrupos(i) Then
                        iGrupoEli = iGrupoEli + 1
                    End If
                Next
            End If
        Next
        If iGrupoDef = iGrupoEli Then
            If sDatosOpc = "" Then
                sDatosOpc = m_asDatos(3)
            Else
                sDatosOpc = sDatosOpc & "," & m_asDatos(3)
            End If
            If sDatos = "" Then
                sDatos = "3"
            Else
                sDatos = sDatos & ",3"
            End If
        End If
    End If
    
    iGrupoDef = 0: iGrupoEli = 0
    If g_oPlantillaSeleccionada.Solicitud = EnGrupo Then
        For Each oGrupo In g_oPlantillaSeleccionada.Grupos
            If oGrupo.DefSolicitud Then
                iGrupoDef = iGrupoDef + 1
                For i = 1 To UBound(arGrupos)
                    If oGrupo.Codigo = arGrupos(i) Then
                        iGrupoEli = iGrupoEli + 1
                    End If
                Next
            End If
        Next
        If iGrupoDef = iGrupoEli Then
            If sDatosOpc = "" Then
                sDatosOpc = m_asDatos(11)
            Else
                sDatosOpc = sDatosOpc & "," & m_asDatos(11)
            End If
            If sDatos = "" Then
                sDatos = "10"
            Else
                sDatos = sDatos & ",10"
            End If
        End If
    End If
    
    iGrupoDef = 0: iGrupoEli = 0
    If g_oPlantillaSeleccionada.EspecGrupo Then
        For Each oGrupo In g_oPlantillaSeleccionada.Grupos
            If oGrupo.DefEspecificaciones Then
                iGrupoDef = iGrupoDef + 1
                For i = 1 To UBound(arGrupos)
                    If oGrupo.Codigo = arGrupos(i) Then
                        iGrupoEli = iGrupoEli + 1
                    End If
                Next
            End If
        Next
        If iGrupoDef = iGrupoEli Then
            If sDatosOpc = "" Then
                sDatosOpc = m_asDatos(4)
            Else
                sDatosOpc = sDatosOpc & "," & m_asDatos(10)
            End If
            If sDatos = "" Then
                sDatos = "4"
            Else
                sDatos = sDatos & ",4"
            End If
        End If
    End If
    
    iGrupoDef = 0: iGrupoEli = 0
    If g_oPlantillaSeleccionada.DistribUON = EnGrupo Then
        For Each oGrupo In g_oPlantillaSeleccionada.Grupos
            If oGrupo.DefDistribUON Then
                iGrupoDef = iGrupoDef + 1
                For i = 1 To UBound(arGrupos)
                    If oGrupo.Codigo = arGrupos(i) Then
                        iGrupoEli = iGrupoEli + 1
                    End If
                Next
            End If
        Next
        If iGrupoDef = iGrupoEli Then
            If sDatosObl = "" Then
                sDatosObl = m_asDatos(5)
            Else
                sDatosObl = sDatosObl & "," & m_asDatos(5)
            End If
            If sDatos = "" Then
                sDatos = "5"
            Else
                sDatos = sDatos & ",5"
            End If
        End If
    End If
    
    If gParametrosGenerales.gbUsarPres1 Then
        iGrupoDef = 0: iGrupoEli = 0
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = EnGrupo Then
            For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                If oGrupo.DefPresAnualTipo1 Then
                    iGrupoDef = iGrupoDef + 1
                    For i = 1 To UBound(arGrupos)
                        If oGrupo.Codigo = arGrupos(i) Then
                            iGrupoEli = iGrupoEli + 1
                        End If
                    Next
                End If
            Next
            If iGrupoDef = iGrupoEli Then
                If gParametrosGenerales.gbOBLPP Then
                    If sDatosObl = "" Then
                        sDatosObl = m_asDatos(6)
                    Else
                        sDatosObl = sDatosObl & "," & m_asDatos(6)
                    End If
                Else
                    If sDatosOpc = "" Then
                        sDatosOpc = m_asDatos(6)
                    Else
                        sDatosOpc = sDatosOpc & "," & m_asDatos(6)
                    End If
                End If
                If sDatos = "" Then
                    sDatos = "6"
                Else
                    sDatos = sDatos & ",6"
                End If
            End If
        End If
    End If
    
    If gParametrosGenerales.gbUsarPres2 Then
        iGrupoDef = 0: iGrupoEli = 0
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = EnGrupo Then
            For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                If oGrupo.DefPresAnualTipo2 Then
                    iGrupoDef = iGrupoDef + 1
                    For i = 1 To UBound(arGrupos)
                        If oGrupo.Codigo = arGrupos(i) Then
                            iGrupoEli = iGrupoEli + 1
                        End If
                    Next
                End If
            Next
            If iGrupoDef = iGrupoEli Then
                If gParametrosGenerales.gbOBLPC Then
                    If sDatosObl = "" Then
                        sDatosObl = m_asDatos(7)
                    Else
                        sDatosObl = sDatosObl & "," & m_asDatos(7)
                    End If
                Else
                    If sDatosOpc = "" Then
                        sDatosOpc = m_asDatos(7)
                    Else
                        sDatosOpc = sDatosOpc & "," & m_asDatos(7)
                    End If
                End If
                If sDatos = "" Then
                    sDatos = "7"
                Else
                    sDatos = sDatos & ",7"
                End If
            End If
        End If
    End If
    
    If gParametrosGenerales.gbUsarPres3 Then
        iGrupoDef = 0: iGrupoEli = 0
        If g_oPlantillaSeleccionada.PresTipo1 = EnGrupo Then
            For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                If oGrupo.DefPresTipo1 Then
                    iGrupoDef = iGrupoDef + 1
                    For i = 1 To UBound(arGrupos)
                        If oGrupo.Codigo = arGrupos(i) Then
                            iGrupoEli = iGrupoEli + 1
                        End If
                    Next
                End If
            Next
            If iGrupoDef = iGrupoEli Then
                If gParametrosGenerales.gbOBLPres3 Then
                    If sDatosObl = "" Then
                        sDatosObl = m_asDatos(8)
                    Else
                        sDatosObl = sDatosObl & "," & m_asDatos(8)
                    End If
                Else
                    If sDatosOpc = "" Then
                        sDatosOpc = m_asDatos(8)
                    Else
                        sDatosOpc = sDatosOpc & "," & m_asDatos(8)
                    End If
                End If
                If sDatos = "" Then
                    sDatos = "8"
                Else
                    sDatos = sDatos & ",8"
                End If
            End If
        End If
    End If
    
    If gParametrosGenerales.gbUsarPres4 Then
        iGrupoDef = 0: iGrupoEli = 0
        If g_oPlantillaSeleccionada.PresTipo2 = EnGrupo Then
            For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                If oGrupo.DefPresTipo2 Then
                    iGrupoDef = iGrupoDef + 1
                    For i = 1 To UBound(arGrupos)
                        If oGrupo.Codigo = arGrupos(i) Then
                            iGrupoEli = iGrupoEli + 1
                        End If
                    Next
                End If
            Next
            If iGrupoDef = iGrupoEli Then
                If gParametrosGenerales.gbOBLPres4 Then
                    If sDatosObl = "" Then
                        sDatosObl = m_asDatos(9)
                    Else
                        sDatosObl = sDatosObl & "," & m_asDatos(9)
                    End If
                Else
                    If sDatosOpc = "" Then
                        sDatosOpc = m_asDatos(9)
                    Else
                        sDatosOpc = sDatosOpc & "," & m_asDatos(9)
                    End If
                End If
                If sDatos = "" Then
                    sDatos = "9"
                Else
                    sDatos = sDatos & ",9"
                End If
            End If
        End If
    End If
    
    ReDim vDatos(2)
    vDatos(0) = sDatosObl
    vDatos(1) = sDatosOpc
    vDatos(2) = sDatos
    
    GruposDefGruposAEliminar = vDatos
        
End Function

Private Sub ModificarConfigPlantilla(ByVal sDatos As String)
'******************************************************************************
'Modifica las propiedades de configuraci�n de la plantilla al eliminar uno
'o varios grupos.
'******************************************************************************
Dim iPos As Integer
Dim sDato As String
Dim oGrupo As CGrupo

    While sDatos <> ""
        iPos = InStr(1, sDatos, ",")
        If iPos = 0 Then
            sDato = sDatos
            sDatos = ""
        Else
            sDato = Left(sDatos, iPos - 1)
            sDatos = Mid(sDatos, iPos + 1)
        End If
        
        Select Case sDato
            Case "0"
                g_oPlantillaSeleccionada.Destino = EnItem
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefDestino = False
                Next
            Case "1"
                g_oPlantillaSeleccionada.FormaPago = EnItem
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefFormaPago = False
                Next
            Case "2"
                g_oPlantillaSeleccionada.FechaSuministro = EnItem
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefFechasSum = False
                Next
            Case "3"
                g_oPlantillaSeleccionada.ProveedorAct = NoDefinido
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefProveActual = False
                Next
            Case "4"
                g_oPlantillaSeleccionada.EspecGrupo = False
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefEspecificaciones = False
                Next
            Case "5"
                g_oPlantillaSeleccionada.DistribUON = EnItem
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefDistribUON = False
                Next
            Case "6"
                If gParametrosGenerales.gbOBLPP Then
                    g_oPlantillaSeleccionada.PresAnualesTipo1 = EnItem
                Else
                    g_oPlantillaSeleccionada.PresAnualesTipo1 = NoDefinido
                End If
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefPresAnualTipo1 = False
                Next
            Case "7"
                If gParametrosGenerales.gbOBLPC Then
                    g_oPlantillaSeleccionada.PresAnualesTipo2 = EnItem
                Else
                    g_oPlantillaSeleccionada.PresAnualesTipo2 = NoDefinido
                End If
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefPresAnualTipo2 = False
                Next
            Case "8"
                If gParametrosGenerales.gbOBLPres3 Then
                    g_oPlantillaSeleccionada.PresTipo1 = EnItem
                Else
                    g_oPlantillaSeleccionada.PresTipo1 = NoDefinido
                End If
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefPresTipo1 = False
                Next
            Case "9"
                If gParametrosGenerales.gbOBLPres4 Then
                    g_oPlantillaSeleccionada.PresTipo2 = EnItem
                Else
                    g_oPlantillaSeleccionada.PresTipo2 = NoDefinido
                End If
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefPresTipo2 = False
                Next
            Case "10"
                g_oPlantillaSeleccionada.Solicitud = NoDefinido
                For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                    oGrupo.DefSolicitud = False
                Next
        End Select
        
    Wend

End Sub


Private Sub chkAdminPub_Click()
    Dim i As Integer
    Dim vbm As Variant
    
    If chkAdminPub.Value = vbChecked Then
        'Dimensiona la grid de grupos haciendo visible el sobre
        sdbgGrupos.Columns("SOBRE").Visible = True
        sdbgGrupos.Columns("COD").Width = sdbgGrupos.Width * 22 / 100
        sdbgGrupos.Columns("NOM").Width = sdbgGrupos.Width * 64 / 100
        
        
        'Elimina los �mbitos de datos correspondientes de la grid de �mbito de datos
        i = sdbgPlantillas.Rows - 1
        While i >= 0
            vbm = sdbgPlantillas.AddItemBookmark(i)
        
            Select Case sdbgPlantillas.Columns("INDICE").CellValue(vbm)
                Case 0
                    'Destino
                    sdbgPlantillas.RemoveItem (sdbgPlantillas.AddItemRowIndex(vbm))
                Case 1
                    'forma de pago
                    sdbgPlantillas.RemoveItem (sdbgPlantillas.AddItemRowIndex(vbm))
                Case 3
                    'Proveedor actual
                    sdbgPlantillas.RemoveItem (sdbgPlantillas.AddItemRowIndex(vbm))
                Case 10
                    'Solicitud de compra
                    sdbgPlantillas.RemoveItem (sdbgPlantillas.AddItemRowIndex(vbm))
            End Select
            i = i - 1
        Wend
        
    Else
        'Dimensiona la grid de grupos haciendo invisible el sobre
        sdbgGrupos.Columns("SOBRE").Visible = False
        sdbgGrupos.Columns("COD").Width = sdbgGrupos.Width * 25 / 100
        sdbgGrupos.Columns("NOM").Width = sdbgGrupos.Width * 68.5 / 100
        
        'Carga otra vez la grid de �mbito de datos
        CargarAmbitoDatos
    End If
    ConfigurarAmbitoDatos chkAdminPub.Value = vbChecked

End Sub

''' <summary>
''' Evento producido al hacer click sobre el check   Respetar la obligatoriedad de los atributos de oferta
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub chkOblAtrGS_Click()

    Dim irespuesta As Integer
    
    If chkOblAtrGS.Enabled = False Then
        Exit Sub
    End If
    If chkOblAtrGS.Value = vbChecked Then
        irespuesta = oMensajes.CheckOblAtribOferta(1)
        If irespuesta = vbYes Then
            g_oPlantillaSeleccionada.ActuProcesos = True
        Else
            g_oPlantillaSeleccionada.ActuProcesos = False
        End If
    Else
        irespuesta = oMensajes.CheckOblAtribOferta(0)
           If irespuesta = vbYes Then
                g_oPlantillaSeleccionada.ActuProcesos = True
           Else
                g_oPlantillaSeleccionada.ActuProcesos = True
           End If
           
    End If

End Sub

Private Sub chkOblSolicitud_Click()
'tarea 3425 - GFA
If Not m_bAccionChkSol Then
    m_bAccionChkSol = True
    If IIf(Me.chkOblSolicitud.Value = 1, True, False) Then
        g_oPlantillaSeleccionada.OblSolCompras = 1
        chkOblSolicitudAdj.Value = 0
    ElseIf Not IIf(Me.chkOblSolicitudAdj.Value = 1, True, False) Then
        g_oPlantillaSeleccionada.OblSolCompras = 0
    End If
    Me.CargarGridPlantillas (True)
    m_bAccionChkSol = False
End If
End Sub

Private Sub chkOblSolicitudAdj_Click()
'tarea 3425 - GFA
If Not m_bAccionChkSol Then
    m_bAccionChkSol = True
    If IIf(Me.chkOblSolicitudAdj.Value = 1, True, False) Then
        g_oPlantillaSeleccionada.OblSolCompras = 2
        chkOblSolicitud.Value = 0
    ElseIf Not IIf(Me.chkOblSolicitud.Value = 1, True, False) Then
        g_oPlantillaSeleccionada.OblSolCompras = 0
    End If
    Me.CargarGridPlantillas (True)
    m_bAccionChkSol = False
End If
End Sub


Private Sub chkParaPedido_Click()
    If chkParaPedido.Value = vbChecked Then
        chkParaPedidoDef.Enabled = True
    Else
        chkParaPedidoDef.Value = vbUnchecked
        chkParaPedidoDef.Enabled = False
    End If
End Sub

Private Sub chkSoloEstosMateriales_Click()
    If chkSoloEstosMateriales.Enabled = False Then
        Exit Sub
    End If
    If chkSoloEstosMateriales.Value = vbChecked Then
        'ConfigurarMateriales True
    Else
        ' eliminar todas las marcas del arbol
        If oCEstructura Is Nothing Then
            Set oCEstructura = CrearCAEstructuraMat(oMensajes)
        End If
        oCEstructura.Desmarcar tvwEstrMatMod, False
    End If

End Sub

Private Sub chkSubasta_Click()
    'Si el proceso es de subasta no se debe permitir seleccionar "Solicitar cantidades m�ximas ..."
    If chkSubasta.Value = vbChecked Then
        chkSuministros.Value = vbUnchecked
    End If
    chkSuministros.Enabled = (chkSubasta.Value = vbUnchecked)
End Sub

Private Sub cmdAbrirEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim FOSFile As Scripting.FileSystemObject
Dim oFos As Scripting.FileSystemObject
Dim oFile As Scripting.File

On Error GoTo Cancelar:

    
    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    Set Item = lstvwEsp.selectedItem

    If Item Is Nothing Then
        oMensajes.SeleccioneFichero

    Else
    
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
            
        'Comprueba si existe el fichero y si existe se borra:
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing
            
        ' Cargamos el contenido en la esp.
        Screen.MousePointer = vbHourglass
        Set oEsp = g_oPlantillaSeleccionada.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        
        If oEsp.DataSize = 0 Then
            oMensajes.NoValido m_sIdiArchivo & "" & oEsp.nombre
            Set oEsp = Nothing
            Exit Sub
        End If
        
        
        If Not IsNull(oEsp.Ruta) Then 'los archivos son vinculados
            Set oFos = New FileSystemObject
            Set oFile = oFos.GetFile(oEsp.Ruta & oEsp.nombre)
            oFile.OpenAsTextStream.ReadAll ''error
    
            If oFos.FolderExists(oEsp.Ruta) And oFos.FileExists(oEsp.Ruta & oEsp.nombre) Then
                ShellExecute MDI.hWnd, "Open", oEsp.Ruta & oEsp.nombre, 0&, oEsp.Ruta, 1
            Else
                oMensajes.ImposibleModificarEsp (oEsp.nombre & " - " & oEsp.Ruta)
            End If
        Else ' los archivos son adjuntos, se leen de base de datos
        
            teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspPlantilla)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Exit Sub
            End If
            
            oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspPlantilla, oEsp.DataSize, sFileName
            
            'Lanzamos la aplicacion
            ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
        End If
    End If
    
    Set oFos = Nothing
    Set oEsp = Nothing
    
    Screen.MousePointer = vbNormal
    Exit Sub

Cancelar:
    If err.Number = 53 Or err.Number = 70 Then
        oMensajes.ImposibleAccederAFichero
    End If
    
    If err.Number = 62 Then
        'El fichero se encuentra vac�o, pero lo abrimos
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
    
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Set oFos = Nothing
    
        
End Sub

''' <summary>
''' Evento producido al hacer click boton aceptar en ambito de datos de plantillas
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdAceptar_Click()
Dim udtTeserror As TipoErrorSummit
Dim oGrupo As CGrupo
Dim i As Integer
Dim vbm As Variant
Dim bIntroSobre As Boolean
Dim irespuesta As Integer
Dim bCargarAtribs As Boolean
Dim oPlantilla As CPlantilla
    
    Screen.MousePointer = vbHourglass
    
    bCargarAtribs = False
    bIntroSobre = False
    
    If ADMIN_OBLIGATORIO Then
        g_oPlantillaSeleccionada.AdminPub = True
        g_oPlantillaSeleccionada.Destino = EnProceso
        g_oPlantillaSeleccionada.FormaPago = EnProceso
        g_oPlantillaSeleccionada.ProveedorAct = NoDefinido
        g_oPlantillaSeleccionada.Solicitud = NoDefinido
        
    Else
        If chkAdminPub.Visible = True Then
            If g_oPlantillaSeleccionada.AdminPub <> SQLBinaryToBoolean(chkAdminPub.Value) Then
                'Si se ha modificado si es de admin p�blica o no:
                If ComprobarActualizarPlantilla = False Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If
            
            If m_oGrupos.Count > 0 Then
                If g_oPlantillaSeleccionada.AdminPub = False And chkAdminPub.Value = vbChecked Then
                    'Si se pasa de un proceso de no adm.p�blica a adm.p�blica y ten�a grupos
                    'cuando acepte pasar� a la pantalla de grupos para que meta las solicitudes
                    bIntroSobre = True
                    g_oPlantillaSeleccionada.Destino = EnProceso
                    g_oPlantillaSeleccionada.FormaPago = EnProceso
                    g_oPlantillaSeleccionada.ProveedorAct = NoDefinido
                    g_oPlantillaSeleccionada.Solicitud = NoDefinido
                    bCargarAtribs = True
                    
                ElseIf g_oPlantillaSeleccionada.AdminPub = True And chkAdminPub.Value = vbUnchecked Then
                    'Si es al contrario se eliminan los sobres de los grupos
                    irespuesta = oMensajes.ProcesoAdministracionPublica(False)
                    If irespuesta = vbNo Then
                        cmdCancelar_Click
                        Exit Sub
                    End If
                    For Each oGrupo In m_oGrupos
                        oGrupo.Sobre = Null
                    Next
                    For i = 0 To sdbgGrupos.Rows - 1
                        vbm = sdbgGrupos.AddItemBookmark(i)
                        sdbgGrupos.Bookmark = vbm
                        sdbgGrupos.Columns("SOBRE").Value = ""
                    Next i
                    sdbgGrupos.MoveFirst
                End If
            Else
                If g_oPlantillaSeleccionada.AdminPub = False And chkAdminPub.Value = vbChecked Then
                    'Se pasa de un proceso de no adm.p�blica a adm.p�blica
                    g_oPlantillaSeleccionada.Destino = EnProceso
                    g_oPlantillaSeleccionada.FormaPago = EnProceso
                    g_oPlantillaSeleccionada.ProveedorAct = NoDefinido
                    g_oPlantillaSeleccionada.Solicitud = NoDefinido
                    bCargarAtribs = True
                End If
            End If
            g_oPlantillaSeleccionada.AdminPub = SQLBinaryToBoolean(chkAdminPub.Value)
        Else
            g_oPlantillaSeleccionada.AdminPub = False
        End If
    End If
    
    
    With g_oPlantillaSeleccionada
        .PedirAlterPrecios = (chkPrecios.Value = vbChecked)
        .NoPublicarFinSum = (chkNoPublicarFinSum.Value = vbChecked)
        .SolicitarCantMax = (chkSuministros.Value = vbChecked)
        .AdjuntarAOfertas = (chkArchivProce.Value = vbChecked)
        .CambiarMonOferta = (chkOferMonProce.Value = vbUnchecked)
        .NoPublicarFinSum = (chkNoPublicarFinSum.Value = vbChecked)
        .UsarPonderacion = (chkPondAtrib.Value = vbChecked)
        .AdjuntarAItems = (chkArchivItem.Value = vbChecked)
        .AdjuntarAGrupos = (chkArchivGrupo.Value = vbChecked)
        .DefUnSoloPedido = (Me.chkUnSoloPedido.Value = vbChecked)
        .ParaPedido = (Me.chkParaPedido.Value = vbChecked)
        .ParaPedidoDef = (Me.chkParaPedidoDef.Value = vbChecked)
        
        If Not .ObligatorioAtribGS And Me.chkOblAtrGS.Value = vbChecked Or .ObligatorioAtribGS And Me.chkOblAtrGS.Value = vbUnchecked Then
            
            RegistrarAccion ACCPlantillasMod, "Cambio del check de obligatoriedad Nuevo valor " & Me.chkOblAtrGS.Value & " Modificacion de los procesos :" & .ActuProcesos
        End If
        .ObligatorioAtribGS = (Me.chkOblAtrGS.Value = vbChecked)
        
        'Datos subasta
        .ModoSubasta = (Me.chkSubasta.Value = vbChecked)
        .Subtipo = m_iSubTipo
        .SubModo = m_iSubModo
        .SubDuracion = m_vSubDuracion
        .MinutosEspera = m_iSubastaEspera
        .SubPublicar = m_bSubPublicar
        .SubNotifEventos = m_bSubNotifEventos
        .MuestraProveedor = m_bSubastaProve
        .SubVerDesdePrimPuja = m_bSubVerDesdePrimPuja
        .MuestraPrecioPuja = m_bSubastaPrecioPuja
        .MuestraDetalle = m_bSubastaPujas
        .BajadaMinimaPujas = m_bSubastaBajMinPuja
        .SubBajMinGanTipo = m_iSubBajMinGanTipo
        .SubBajMinGanProcVal = m_vSubBajMinGanProcVal
        .SubBajMinGanGrupoVal = m_vSubBajMinGanGrupoVal
        .SubBajMinGanItemVal = m_vSubBajMinGanItemVal
        .SubBajMinProve = m_bSubBajMinProve
        .SubBajMinProveTipo = m_iSubBajMinProveTipo
        .SubBajMinProveProcVal = m_vSubBajMinProveProcVal
        .SubBajMinProveGrupoVal = m_vSubBajMinProveGrupoVal
        .SubBajMinProveItemVal = m_vSubBajMinProveItemVal
        Set .SubTextoFin = m_dcSubTextosFin
        'Tarea 3425 - GFA
        If chkOblSolicitud.Value = 1 Then
            .OblSolCompras = 1
        ElseIf chkOblSolicitudAdj.Value = 1 Then
            .OblSolCompras = 2
        Else
            .OblSolCompras = 0
        End If
        
        
    End With
    
    
    cmdMod.Visible = True
    cmdRestaurar.Visible = True
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    cmdCrearPlant.Enabled = True
    cmdModificarPlant.Enabled = True
    cmdEliminarPlant.Enabled = True
    cmdConfVistas.Enabled = True
    cmdEliminarVistas.Enabled = True
    Me.picAmbitoDatosOfe.Enabled = False
    sdbgPlantillas.Columns("USAR").Locked = True
    sdbgPlantillas.Columns("PROCE").Locked = True
    sdbgPlantillas.Columns("GRUPO").Locked = True
    sdbgPlantillas.Columns("BOTONGRUP").Locked = True
    sdbgPlantillas.Columns("ITEM").Locked = True
    picAdminPub.Enabled = False
    
    'Cargar las propiedades
    
    sdbgPlantillas.MoveFirst
    For i = 0 To sdbgPlantillas.Rows - 1
        Select Case sdbgPlantillas.Columns("INDICE").Value
            Case 0
                'Destino
                If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                    g_oPlantillaSeleccionada.Destino = EnProceso
                ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                    g_oPlantillaSeleccionada.Destino = EnGrupo
                ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                    g_oPlantillaSeleccionada.Destino = EnItem
                End If
                If g_oPlantillaSeleccionada.Destino <> EnGrupo Then
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefDestino = False
                    Next
                End If

            Case 1
                'Forma de pago
                If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                    g_oPlantillaSeleccionada.FormaPago = EnProceso
                ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                    g_oPlantillaSeleccionada.FormaPago = EnGrupo
                ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                    g_oPlantillaSeleccionada.FormaPago = EnItem
                End If
                If g_oPlantillaSeleccionada.FormaPago <> EnGrupo Then
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefFormaPago = False
                    Next
                End If
    
            Case 2
                'Fechas de suministro
                If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                    g_oPlantillaSeleccionada.FechaSuministro = EnProceso
                ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                    g_oPlantillaSeleccionada.FechaSuministro = EnGrupo
                ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                    g_oPlantillaSeleccionada.FechaSuministro = EnItem
                End If
                If g_oPlantillaSeleccionada.FechaSuministro <> EnGrupo Then
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefFechasSum = False
                    Next
                End If
    
            Case 3
                'Proveedor actual
                If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                    g_oPlantillaSeleccionada.ProveedorAct = EnProceso
                ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                    g_oPlantillaSeleccionada.ProveedorAct = EnGrupo
                ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                    g_oPlantillaSeleccionada.ProveedorAct = EnItem
                Else
                    g_oPlantillaSeleccionada.ProveedorAct = NoDefinido
                End If
                If g_oPlantillaSeleccionada.ProveedorAct <> EnGrupo Then
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefProveActual = False
                    Next
                End If
    
            Case 4
                'Especificaciones
                If sdbgPlantillas.Columns("USAR").Value <> "-1" Then
                    g_oPlantillaSeleccionada.EspecProce = False
                    g_oPlantillaSeleccionada.EspecGrupo = False
                    g_oPlantillaSeleccionada.EspecItem = False
                Else
                    If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                        g_oPlantillaSeleccionada.EspecProce = True
                    Else
                        g_oPlantillaSeleccionada.EspecProce = False
                    End If
                    If sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                        g_oPlantillaSeleccionada.EspecGrupo = True
                    Else
                        g_oPlantillaSeleccionada.EspecGrupo = False
                    End If
                    If sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                        g_oPlantillaSeleccionada.EspecItem = True
                    Else
                        g_oPlantillaSeleccionada.EspecItem = False
                    End If
                End If
                If Not g_oPlantillaSeleccionada.EspecGrupo Then
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefEspecificaciones = False
                    Next
                End If

            Case 5
                'Distribuci�n en unidades organizativas
                If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                    g_oPlantillaSeleccionada.DistribUON = EnProceso
                ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                    g_oPlantillaSeleccionada.DistribUON = EnGrupo
                ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                    g_oPlantillaSeleccionada.DistribUON = EnItem
                Else
                    g_oPlantillaSeleccionada.DistribUON = NoDefinido
                End If
                If g_oPlantillaSeleccionada.DistribUON <> EnGrupo Then
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefDistribUON = False
                    Next
                End If
    
            Case 6
                'Pres.Anu1
                If gParametrosGenerales.gbUsarPres1 Then
                    If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresAnualesTipo1 = EnProceso
                    ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresAnualesTipo1 = EnGrupo
                    ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresAnualesTipo1 = EnItem
                    Else
                        g_oPlantillaSeleccionada.PresAnualesTipo1 = NoDefinido
                    End If
                    If g_oPlantillaSeleccionada.PresAnualesTipo1 <> EnGrupo Then
                        For Each oGrupo In m_oGrupos
                            oGrupo.DefPresAnualTipo1 = False
                        Next
                    End If
                End If
    
            Case 7
                'Pres.Anu2
                If gParametrosGenerales.gbUsarPres2 Then
                    If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresAnualesTipo2 = EnProceso
                    ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresAnualesTipo2 = EnGrupo
                    ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresAnualesTipo2 = EnItem
                    Else
                        g_oPlantillaSeleccionada.PresAnualesTipo2 = NoDefinido
                    End If
                    If g_oPlantillaSeleccionada.PresAnualesTipo2 <> EnGrupo Then
                        For Each oGrupo In m_oGrupos
                            oGrupo.DefPresAnualTipo2 = False
                        Next
                    End If
                End If
    
            Case 8
                'PresTipo1
                If gParametrosGenerales.gbUsarPres3 Then
                    If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresTipo1 = EnProceso
                    ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresTipo1 = EnGrupo
                    ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresTipo1 = EnItem
                    Else
                        g_oPlantillaSeleccionada.PresTipo1 = NoDefinido
                    End If
                    If g_oPlantillaSeleccionada.PresTipo1 <> EnGrupo Then
                        For Each oGrupo In m_oGrupos
                            oGrupo.DefPresTipo1 = False
                        Next
                    End If
                End If
    
            Case 9
                'PresTipo2
                If gParametrosGenerales.gbUsarPres4 Then
                    If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresTipo2 = EnProceso
                    ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresTipo2 = EnGrupo
                    ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                        g_oPlantillaSeleccionada.PresTipo2 = EnItem
                    Else
                        g_oPlantillaSeleccionada.PresTipo2 = NoDefinido
                    End If
                    If g_oPlantillaSeleccionada.PresTipo2 <> EnGrupo Then
                        For Each oGrupo In m_oGrupos
                            oGrupo.DefPresTipo2 = False
                        Next
                    End If
                End If
    
            Case 10
                'Solicitud de compras
                If sdbgPlantillas.Columns("PROCE").Value = "-1" Then
                    g_oPlantillaSeleccionada.Solicitud = EnProceso
                ElseIf sdbgPlantillas.Columns("GRUPO").Value = "-1" Then
                    g_oPlantillaSeleccionada.Solicitud = EnGrupo
                ElseIf sdbgPlantillas.Columns("ITEM").Value = "-1" Then
                    g_oPlantillaSeleccionada.Solicitud = EnItem
                Else
                    g_oPlantillaSeleccionada.Solicitud = NoDefinido
                End If
                If g_oPlantillaSeleccionada.Solicitud <> EnGrupo Then
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefSolicitud = False
                    Next
                End If
    
        End Select
        sdbgPlantillas.MoveNext
    Next i
    sdbgPlantillas.MoveFirst
    
    
    Set g_oPlantillaSeleccionada.Grupos = m_oGrupos
    
    'Si se marca la plantilla como plantilla para pedido "por defecto"
    'comprobamos si tiene restricciones de material
    'ya que solo puede haber una plantilla por defecto sin material
    g_oPlantillaSeleccionada.ActuPedidoDef = False
    If g_oPlantillaSeleccionada.ParaPedidoDef = True Then
    
        If g_oPlantillaSeleccionada.RestriccionMaterial = False Then
        
            For Each oPlantilla In g_oPlantillas
                If g_oPlantillaSeleccionada.Id <> oPlantilla.Id And oPlantilla.ExisteRestriccionMaterial = False And oPlantilla.ParaPedidoDef = True Then

                    g_oPlantillaSeleccionada.ActuPedidoDef = True
                                        
                    'Como solo puede haber una plantilla "por defecto" sin materiales
                    'una vez encuentro la plantilla y la desmarco "por defecto"
                    'salgo del bucle
                    Exit For
                    
                End If
            Next

        End If
    End If
    
    udtTeserror = g_oPlantillaSeleccionada.GuardarAmbitoDeDatos
    If udtTeserror.NumError <> TESnoerror Then
         TratarError udtTeserror
    End If
    
    If g_oPlantillaSeleccionada.ActuPedidoDef = True Then
        oPlantilla.ParaPedidoDef = False
    End If
    
    lstvwplantillas.ListItems.Item(m_iIndiListw).Selected = True
    m_bModoEdicionPlant = False
    
    If g_oPlantillaSeleccionada.EspecProce = True Or g_oPlantillaSeleccionada.EspecGrupo = True Or g_oPlantillaSeleccionada.EspecItem = True Then
        sstabPlantillas.TabVisible(3) = True
        ConfigurarTabEspecificaciones
    Else
        sstabPlantillas.TabVisible(3) = False
        txtProceEsp.Text = ""
        lstvwEsp.ListItems.clear
        g_oPlantillaSeleccionada.esp = Null
        Set g_oPlantillaSeleccionada.especificaciones = Nothing
    End If
    
    If bCargarAtribs = True Then
        'Si se ha pasado a un proceso de administraci�n p�blica se cargan otra vez los atributos
        CargarGridAtributos
    End If
       
    CargarComboGruposEspDesdeAmbitoDeDatos
    CargarGridEspecificaciones
    
    'Si se ha pasado a un proceso de administraci�n p�blica y ten�a grupos se pasa a la pesta�a
    'de grupos para introducir los sobres
    If bIntroSobre = True Then
        sstabPlantillas.Tab = 1
        cmdModoEdicion_Click
        oMensajes.IntroduzcaSobre
    End If
    
    Screen.MousePointer = vbNormal

End Sub


''' <summary>
''' Configura como se ve el taba de especificaciones
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmd_AceptarClick </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub ConfigurarTabEspecificaciones()
        If g_oPlantillaSeleccionada.EspecProce Then
            SSSplitterEsp.Height = sstabPlantillas.Height - 1000 '* 0.92
            SSSplitterEsp.Width = sstabPlantillas.Width - 250
            
            SSSplitterEsp.Panes.Item(0).LockHeight = False
            SSSplitterEsp.Panes.Item(2).LockHeight = False
            
            SSSplitterEsp.Panes.Item(0).Height = SSSplitterEsp.Height / 4
            SSSplitterEsp.Panes.Item(1).Height = SSSplitterEsp.Height / 2
            SSSplitterEsp.Panes.Item(2).Height = SSSplitterEsp.Height / 4
            
            picSplit1.Height = SSSplitterEsp.Panes.Item(0).Height
            picSplit2.Height = SSSplitterEsp.Panes.Item(1).Height
            picSplit3.Height = SSSplitterEsp.Panes.Item(2).Height
            
                
            If picSplit2.Height > 600 Then
                sdbgAtributosEsp.Height = picSplit2.Height - 600 '* 0.8
            Else
                sdbgAtributosEsp.Height = 0
            End If
            sdbgAtributosEsp.Width = picSplit2.Width - 200
            
            txtProceEsp.Width = picSplit1.Width - 0
            If picSplit1.Height > 200 Then
                txtProceEsp.Height = picSplit1.Height - 200
            Else
                txtProceEsp.Height = 0
            End If

            
            If picSplit3.Height > 575 Then
                lstvwEsp.Height = picSplit3.Height - 120 - 375 - 80
            Else
                lstvwEsp.Height = 0
            End If
            lstvwEsp.Width = picSplit3.Width - 200
            picNavigateEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 80
            picNavigateEsp.Left = lstvwEsp.Width - 2280
            
            
            cmdModoEdicionAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdModoEdicionAtrEsp.Left = SSSplitterEsp.Width - 1000
            
            cmdDeshacerAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdDeshacerAtrEsp.Left = picSplit2.Left + 90
            
            picBotonesAtributos.Width = SSSplitterEsp.Width
            sdbcGrupoEsp.Width = SSSplitterEsp.Width - 2000
            sdbcGrupoEsp.Columns(0).Width = sdbcGrupoEsp.Width / 3
            sdbcGrupoEsp.Columns(1).Width = 2 * sdbcGrupoEsp.Width / 3
            cmdSortAscEsp.Left = sdbcGrupoEsp.Width + 300
            cmdSortDesEsp.Left = cmdSortAscEsp.Left + 350
            cmdAnyaAtribEsp.Left = cmdSortDesEsp.Left + 400
            If Not m_bModifAtrib Then
                cmdAnyaAtribEsp.Visible = False
            Else
                cmdAnyaAtribEsp.Visible = True
            End If
            cmdElimAtribEsp.Left = cmdAnyaAtribEsp.Left + 350
            
        Else
        
            SSSplitterEsp.Panes.Item(0).LockHeight = False
            SSSplitterEsp.Panes.Item(2).LockHeight = False

            SSSplitterEsp.Height = sstabPlantillas.Height - 1000 '* 0.92
            SSSplitterEsp.Width = sstabPlantillas.Width - 250
            
            SSSplitterEsp.Panes.Item(0).Height = 0
            SSSplitterEsp.Panes.Item(1).Height = SSSplitterEsp.Height
            SSSplitterEsp.Panes.Item(2).Height = 0
            
            picSplit1.Height = SSSplitterEsp.Panes.Item(0).Height
            picSplit2.Height = SSSplitterEsp.Panes.Item(1).Height
            picSplit3.Height = SSSplitterEsp.Panes.Item(2).Height
            
                
            If picSplit2.Height > 600 Then
                sdbgAtributosEsp.Height = picSplit2.Height - 600 '* 0.8
            Else
                sdbgAtributosEsp.Height = 0
            End If
            sdbgAtributosEsp.Width = sstabPlantillas.Width - 400
                        
            If picSplit3.Height > 575 Then
                lstvwEsp.Height = picSplit3.Height - 120 - 375 - 80
            Else
                lstvwEsp.Height = 0
            End If
            lstvwEsp.Width = sstabPlantillas.Width - 400
            picNavigateEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 80
            picNavigateEsp.Left = lstvwEsp.Width - 2280
            
            
            cmdModoEdicionAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdModoEdicionAtrEsp.Left = SSSplitterEsp.Width - 1000
            
            cmdDeshacerAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdDeshacerAtrEsp.Left = picSplit2.Left + 90
            
            picBotonesAtributos.Width = SSSplitterEsp.Width
            sdbcGrupoEsp.Width = SSSplitterEsp.Width - 2000
            sdbcGrupoEsp.Columns(0).Width = sdbcGrupoEsp.Width / 3
            sdbcGrupoEsp.Columns(1).Width = 2 * sdbcGrupoEsp.Width / 3
            cmdSortAscEsp.Left = sdbcGrupoEsp.Width + 300
            cmdSortDesEsp.Left = cmdSortAscEsp.Left + 350
            cmdAnyaAtribEsp.Left = cmdSortDesEsp.Left + 400
            If Not m_bModifAtrib Then
                cmdAnyaAtribEsp.Visible = False
            Else
                cmdAnyaAtribEsp.Visible = True
            End If
            cmdElimAtribEsp.Left = cmdAnyaAtribEsp.Left + 350
            
            SSSplitterEsp.Panes.Item(0).LockHeight = True
            SSSplitterEsp.Panes.Item(2).LockHeight = True

        End If

End Sub


''' <summary>
''' Evento producido al hacer click boton aceptar de la pesta�a de materiales
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdAceptarMateriales_Click()
    Dim RecargarGrid As Boolean
    Dim teserror As TipoErrorSummit
    
    'edu Incidencia 7213 de T98
    ' si hay restriccion de material exigir al menos un material marcado
    'edu incidencia 7543
    ' si desmarca todo pero la plantilla tiene mas materiales, no debe dar el error.
    'edu incidencia 7689
    ' lo mismo de la incidencia 7543
    
    RecargarGrid = False
    
    If oCEstructura Is Nothing Then
        Set oCEstructura = CrearCAEstructuraMat(oMensajes)
    End If

    If oCEstructura.ContarMarcas(tvwEstrMatMod) <= 0 Then
        If chkSoloEstosMateriales = vbChecked Then
            If oCEstructura.NoCoincidencias(tvwEstrMat, tvwEstrMatMod) <= 0 Then
                oMensajes.NoValido sIdiMarcaruno '"Se debe marcar al menos un material"
                Exit Sub
            Else
                RecargarGrid = True
            End If
        Else
            If oCEstructura.ContarMarcas(tvwEstrMat, True) <= oCEstructura.ContarMarcas(tvwEstrMatMod, True) Then
                ' no hay restriccion y no quedan materiales, borrar todos
                g_oPlantillaSeleccionada.EliminarMateriales
                RegistrarAccion ACCPlantillasModMat, "Id:" & frmPlantillasProce.g_oPlantillaSeleccionada.Id
                frmPlantillasProce.g_oPlantillaSeleccionada.ExisteRestriccionMaterial
                
                If Not frmPlantillasProce.g_oPlantillaSeleccionada.ExisteRestriccionMaterial And m_bTieneMateriales = True Then
                    teserror = frmPlantillasProce.g_oPlantillaSeleccionada.DesmarcarParaPedidoPorDefecto
                    If teserror.NumError = TESnoerror Then
                        frmPlantillasProce.g_oPlantillaSeleccionada.ParaPedidoDef = False
                        Me.chkParaPedidoDef.Value = vbUnchecked
                    End If
                End If
                
                ConfigurarMateriales False
                Exit Sub
            End If
        End If
    End If

    oCEstructura.GuardarMateriales tvwEstrMat, tvwEstrMatMod, oUsuarioSummit.comprador, frmPlantillasProce.g_oPlantillaSeleccionada, "MODIFICACION"
    RegistrarAccion ACCPlantillasModMat, "Id:" & frmPlantillasProce.g_oPlantillaSeleccionada.Id
    frmPlantillasProce.g_oPlantillaSeleccionada.ExisteRestriccionMaterial

    ConfigurarMateriales False

    If RecargarGrid Then
        ' para que elimine la plantilla de la vista, ya que se han borrado los materiales que coinciden.
        lstvwplantillas.ListItems.Remove (lstvwplantillas.selectedItem.Index)
        SeleccionarPrimeraPlantilla 'edu. por incidencia 199
    End If
    
End Sub

Private Sub SeleccionarPrimeraPlantilla()
    Dim Item As Object
    lstvwplantillas.ListItems.Item(1).Selected = True
    Set Item = lstvwplantillas.selectedItem
    g_lNumID = Right(Item.key, Len(Item.key) - 2)
    CargarGridPlantillas True
End Sub


''' <summary>
''' Abre la ventana de atributos para anyadir un atributo de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click sobre el boton </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdAnyaAtribEsp_Click()

    If ComprobarActualizarPlantilla = False Then
        Exit Sub
    End If
    
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If

    Set g_ofrmATRIB = New frmAtrib
    g_ofrmATRIB.g_sOrigen = "PLANTILLASESP"
        
    g_ofrmATRIB.bRComprador = m_bRestringirAsignacionAtributosMateriales
    
    If g_sCodGRPEsp = "" Then
        If g_oPlantillaSeleccionada.EspecProce Then
            g_ofrmATRIB.sAmbitoAtribEsp = m_asAtrib(4)
            g_ofrmATRIB.iAmbitoAtribEsp = TipoAmbitoProceso.AmbProceso
        Else
            If g_oPlantillaSeleccionada.EspecGrupo Then
                g_ofrmATRIB.sAmbitoAtribEsp = m_asAtrib(5)
                g_ofrmATRIB.iAmbitoAtribEsp = TipoAmbitoProceso.AmbGrupo
            Else
                g_ofrmATRIB.sAmbitoAtribEsp = m_asAtrib(6)
                g_ofrmATRIB.iAmbitoAtribEsp = TipoAmbitoProceso.AmbItem
            End If
        End If
    Else
        If g_oPlantillaSeleccionada.Grupos.Item(CStr(g_sCodGRPEsp)).DefEspecificaciones Then
            g_ofrmATRIB.sAmbitoAtribEsp = m_asAtrib(5)
            g_ofrmATRIB.iAmbitoAtribEsp = TipoAmbitoProceso.AmbGrupo
        Else
            g_ofrmATRIB.sAmbitoAtribEsp = m_asAtrib(6)
            g_ofrmATRIB.iAmbitoAtribEsp = TipoAmbitoProceso.AmbItem
        End If
    End If
    
    If m_bModifAtrib Then
        Screen.MousePointer = vbHourglass
        MDI.MostrarFormulario g_ofrmATRIB
        Screen.MousePointer = vbNormal
    End If
    
    Set m_oAtributo = Nothing

End Sub

Private Sub cmdAnyadir_Click()
    ''' * Objetivo: Situarnos en la fila de adicion
    sdbgGrupos.Scroll 0, sdbgGrupos.Rows - sdbgGrupos.Row
    
    If sdbgGrupos.VisibleRows > 0 Then
        
        If sdbgGrupos.VisibleRows >= sdbgGrupos.Rows Then
            sdbgGrupos.Row = sdbgGrupos.Rows
        Else
            sdbgGrupos.Row = sdbgGrupos.Rows - (sdbgGrupos.Rows - sdbgGrupos.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgGrupos.SetFocus
    
    txtGrupoDescr.Text = ""
    
End Sub

Private Sub cmdAnyadirAtr_Click()

    If ComprobarActualizarPlantilla = False Then
        Exit Sub
    End If
    
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If

    Set g_ofrmATRIB = New frmAtrib
    g_ofrmATRIB.g_sOrigen = "PLANTILLAS"
    g_ofrmATRIB.bRComprador = m_bRestringirAsignacionAtributosMateriales
    
    If m_bModifAtrib Then
        Screen.MousePointer = vbHourglass
        MDI.MostrarFormulario g_ofrmATRIB
        Screen.MousePointer = vbNormal
    End If
    
    Set m_oAtributo = Nothing

End Sub

Private Sub cmdAnyadirEsp_Click()
Dim DataFile As Integer
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim arrFileNames As Variant
Dim iFile As Integer
Dim oFos As Scripting.FileSystemObject
Dim oDrive As Scripting.Drive
Dim sRuta As String
Dim sdrive As String
Dim sdrive2 As String


On Error GoTo Cancelar:

    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
            
    cmmdEsp.DialogTitle = m_sIdiSelecAdjunto

    cmmdEsp.Filter = m_sIdiTodosArchivos & "|*.*"

    cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    cmmdEsp.filename = ""
    cmmdEsp.ShowOpen

    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileName = "" Then Exit Sub
       
    arrFileNames = ExtraerFicheros(sFileName)
       
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.sOrigen = "frmPlantillasProce"
    frmPROCEComFich.chkProcFich.Visible = True
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.chkProcFich.Top
        frmPROCEComFich.chkProcFich.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 1850
    End If
    g_bCancelarEsp = False
    Screen.MousePointer = vbNormal
    frmPROCEComFich.Show 1

    Screen.MousePointer = vbHourglass
    
    If g_bSoloRuta = False Then 'Si es adjunto no pasa por aqui
        If Not g_bCancelarEsp Then
            Set oFos = New Scripting.FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                Set oEsp = oFSGSRaiz.generar_CEspecificacion
                Set oEsp.Plantilla = g_oPlantillaSeleccionada
                oEsp.nombre = sFileTitle
                oEsp.Comentario = g_sComentario
        
                teserror = oEsp.ComenzarEscrituraData(TipoEspecificacion.EspPlantilla)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set oEsp = Nothing
                    Exit Sub
                End If
    
                Dim sAdjunto As String
                Dim ArrayAdjunto() As String
                Dim oFile As File
                Dim bites As Long
                
                Set oFile = oFos.GetFile(sFileName)
                bites = oFile.Size
                oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
                sAdjunto = oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspPlantilla)
                
                'Creamos un array, cada "substring" se asignar�
                'a un elemento del array
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                oEsp.Id = ArrayAdjunto(0)
                oEsp.DataSize = bites

                basSeguridad.RegistrarAccion AccionesSummit.ACCPlantProceEspAnya, "Plantilla:" & g_oPlantillaSeleccionada.Id & "Archivo:" & sFileTitle
                If g_oPlantillaSeleccionada.especificaciones Is Nothing Then
                    Set g_oPlantillaSeleccionada.especificaciones = oFSGSRaiz.Generar_CEspecificaciones
                End If
                g_oPlantillaSeleccionada.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , , oEsp.Comentario, , , , , , g_oPlantillaSeleccionada, oEsp.DataSize
                lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tam", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Rut", oEsp.Ruta
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
            Next iFile
        End If
        lstvwEsp.Refresh
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
        
    Else ' Guardamos la ruta en lugar del archivo
        If Not g_bCancelarEsp Then
            Set oFos = New FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                cmmdEsp.Filter = m_sIdiTodosArchivos & "|*.*"
                sdrive = oFos.GetDriveName(sFileName)
                sRuta = Left(sFileName, Len(sFileName) - Len(sFileTitle))
                sRuta = Right(sRuta, Len(sRuta) - Len(sdrive))
                Set oDrive = oFos.GetDrive(sdrive)
                sdrive2 = oDrive.ShareName
                
                If sdrive2 = "" Then
                    sRuta = sdrive & sRuta
                Else
                    sRuta = sdrive2 & sRuta
                End If
                
                If sRuta = "" Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                Set oEsp = oFSGSRaiz.generar_CEspecificacion
                Set oEsp.Plantilla = g_oPlantillaSeleccionada
                oEsp.nombre = sFileTitle
                oEsp.Comentario = g_sComentario
                oEsp.Ruta = sRuta
                oEsp.DataSize = FileLen(sFileName)
                Set g_oIBaseDatos = oEsp
                
                teserror = g_oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set oEsp = Nothing
                    Set oDrive = Nothing
                    Set oFos = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            
                If g_oPlantillaSeleccionada.especificaciones Is Nothing Then
                    Set g_oPlantillaSeleccionada.especificaciones = oFSGSRaiz.Generar_CEspecificaciones
                End If
            
                g_oPlantillaSeleccionada.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , , oEsp.Comentario, , , , , oEsp.Ruta, g_oPlantillaSeleccionada, oEsp.DataSize
            
                lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tam", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Rut", oEsp.Ruta
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
            
                basSeguridad.RegistrarAccion AccionesSummit.ACCPlantProceEspAnya, "Plantilla:" & g_oPlantillaSeleccionada.Id & "Archivo:" & sFileTitle
            Next
        End If
        
        lstvwEsp.Refresh
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
        Set oFos = Nothing
        Set oDrive = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
        
    End If

    Screen.MousePointer = vbNormal
        
    Exit Sub

Cancelar:
    Screen.MousePointer = vbNormal
    
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        MsgBox err.Description
        
        Close DataFile
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
        Set oFos = Nothing
        Set oDrive = Nothing
    End If
    Set oEsp = Nothing
End Sub




Private Sub cmdCancelar_Click()
    
    Screen.MousePointer = vbHourglass
    If Not g_oIBaseDatos Is Nothing Then
        g_oIBaseDatos.CancelarEdicion
    End If
    
    cmdMod.Visible = True
    cmdRestaurar.Visible = True
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    sdbgPlantillas.CancelUpdate
    
    CargarGridPlantillas True, True
    
    sdbgPlantillas.Columns("USAR").Locked = True
    sdbgPlantillas.Columns("PROCE").Locked = True
    sdbgPlantillas.Columns("GRUPO").Locked = True
    sdbgPlantillas.Columns("BOTONGRUP").Locked = True
    sdbgPlantillas.Columns("ITEM").Locked = True
    Me.picAmbitoDatosOfe.Enabled = False
    picAdminPub.Enabled = False
    
    lstvwplantillas.ListItems.Item(m_iIndiListw).Selected = True
    m_bModoEdicionPlant = False
    
    cmdCrearPlant.Enabled = True
    cmdModificarPlant.Enabled = True
    cmdEliminarPlant.Enabled = True
    cmdConfVistas.Enabled = True
    cmdEliminarVistas.Enabled = True
    
    sdbgPlantillas.Refresh
    
    InicializarValoresSubasta
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdCancelarMateriales_Click()
    ConfigurarMateriales False
End Sub

''' <summary>
''' Evento producido al hacer click boton cod�do para cambiar el codigo de un grupo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub cmdCodigo_Click()
Dim teserror As TipoErrorSummit
    
    ''' * Objetivo: Cambiar de codigo el grupo actual
    
    If sdbgGrupos.Rows = 0 Then Exit Sub
    
    If sdbgGrupos.Columns("COD").Text = "" Then Exit Sub
    
    If sdbgGrupos.SelBookmarks.Count > 1 Then Exit Sub
    
    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
        
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    Screen.MousePointer = vbHourglass
    Set m_oGrupoEnEdicion = g_oPlantillaSeleccionada.Grupos.Item(CStr(sdbgGrupos.Columns("COD").Text))
    Set g_oIBaseDatos = m_oGrupoEnEdicion
    teserror = g_oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        If Me.Visible Then sdbgGrupos.SetFocus
        Exit Sub
    End If
        
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    
    frmMODCOD.caption = m_sFrmModCod
    frmMODCOD.Left = frmPlantillasProce.Left + 500
    frmMODCOD.Top = frmPlantillasProce.Top + 1000
    frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodGRUPOPROCE
    frmMODCOD.txtCodAct.Text = m_oGrupoEnEdicion.Codigo
    Set frmMODCOD.fOrigen = frmPlantillasProce
    g_bCodigoCancelar = False
    Screen.MousePointer = vbNormal
    frmMODCOD.Show 1
    DoEvents
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
        oMensajes.NoValido m_sCodigo
        Set g_oIBaseDatos = Nothing
        Set m_oGrupoEnEdicion = Nothing
        Exit Sub
    End If

    If UCase(g_sCodigoNuevo) = UCase(m_oGrupoEnEdicion.Codigo) Then
        oMensajes.NoValido m_sCodigo
        Set g_oIBaseDatos = Nothing
        Set m_oGrupoEnEdicion = Nothing
        Exit Sub
    End If
    
    ''' Cambiar el codigo
    Screen.MousePointer = vbHourglass
    teserror = g_oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    RegistrarAccion ACCPlantGrupoMod, "Grupo Modificado: " & sdbgGrupos.Columns("COD").Text & " Nuevo Codigo: " & g_sCodigoNuevo
    
    ''' Actualizar los datos
    sdbgGrupos.Columns("COD").Text = g_sCodigoNuevo
    CargarComboGrupos
    CargarComboGruposEsp
            
    
    Set g_oIBaseDatos = Nothing
    Set m_oGrupoEnEdicion = Nothing
    
    cmdRestGrupo_Click
    Screen.MousePointer = vbNormal
    

   
End Sub

''' <summary>Abre el formulario de configuraci�n de subasta.</summary>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: 1sg</remarks>

Private Sub cmdConfigSubasta_Click()
    On Error GoTo Error
    
    Dim oConfSubasta As New frmCONFSubasta
    oConfSubasta.g_sOrigen = "frmPlantillasProce"
    oConfSubasta.g_bModoEdicion = fraConfProce(2).Enabled
    oConfSubasta.g_iSubTipo = m_iSubTipo
    oConfSubasta.g_iSubModo = m_iSubModo
    oConfSubasta.g_vSubDuracion = m_vSubDuracion
    oConfSubasta.g_iSubastaEspera = m_iSubastaEspera
    oConfSubasta.g_bSubPublicar = m_bSubPublicar
    oConfSubasta.g_bSubNotifEventos = m_bSubNotifEventos
    oConfSubasta.g_bSubastaProve = m_bSubastaProve
    oConfSubasta.g_bSubVerDesdePrimPuja = m_bSubVerDesdePrimPuja
    oConfSubasta.g_bSubastaPrecioPuja = m_bSubastaPrecioPuja
    oConfSubasta.g_bSubastaPujas = m_bSubastaPujas
    oConfSubasta.g_bSubastaBajMinPuja = m_bSubastaBajMinPuja
    oConfSubasta.g_iSubBajMinGanTipo = m_iSubBajMinGanTipo
    oConfSubasta.g_vSubBajMinGanProcVal = m_vSubBajMinGanProcVal
    oConfSubasta.g_vSubBajMinGanGrupoVal = m_vSubBajMinGanGrupoVal
    oConfSubasta.g_vSubBajMinGanItemVal = m_vSubBajMinGanItemVal
    oConfSubasta.g_bSubBajMinProve = m_bSubBajMinProve
    oConfSubasta.g_iSubBajMinProveTipo = m_iSubBajMinProveTipo
    oConfSubasta.g_vSubBajMinProveProcVal = m_vSubBajMinProveProcVal
    oConfSubasta.g_vSubBajMinProveGrupoVal = m_vSubBajMinProveGrupoVal
    oConfSubasta.g_vSubBajMinProveItemVal = m_vSubBajMinProveItemVal
    Set oConfSubasta.g_dcSubTextosFin = m_dcSubTextosFin
    
    oConfSubasta.Show vbModal
    If oConfSubasta.g_bOK Then
        m_iSubTipo = oConfSubasta.g_iSubTipo
        m_iSubModo = oConfSubasta.g_iSubModo
        m_vSubDuracion = oConfSubasta.g_vSubDuracion
        m_iSubastaEspera = oConfSubasta.g_iSubastaEspera
        m_bSubPublicar = oConfSubasta.g_bSubPublicar
        m_bSubNotifEventos = oConfSubasta.g_bSubNotifEventos
        m_bSubastaProve = oConfSubasta.g_bSubastaProve
        m_bSubVerDesdePrimPuja = oConfSubasta.g_bSubVerDesdePrimPuja
        m_bSubastaPrecioPuja = oConfSubasta.g_bSubastaPrecioPuja
        m_bSubastaPujas = oConfSubasta.g_bSubastaPujas
        m_bSubastaBajMinPuja = oConfSubasta.g_bSubastaBajMinPuja
        m_iSubBajMinGanTipo = oConfSubasta.g_iSubBajMinGanTipo
        m_vSubBajMinGanProcVal = oConfSubasta.g_vSubBajMinGanProcVal
        m_vSubBajMinGanGrupoVal = oConfSubasta.g_vSubBajMinGanGrupoVal
        m_vSubBajMinGanItemVal = oConfSubasta.g_vSubBajMinGanItemVal
        m_bSubBajMinProve = oConfSubasta.g_bSubBajMinProve
        m_iSubBajMinProveTipo = oConfSubasta.g_iSubBajMinProveTipo
        m_vSubBajMinProveProcVal = oConfSubasta.g_vSubBajMinProveProcVal
        m_vSubBajMinProveGrupoVal = oConfSubasta.g_vSubBajMinProveGrupoVal
        m_vSubBajMinProveItemVal = oConfSubasta.g_vSubBajMinProveItemVal
        Set m_dcSubTextosFin = oConfSubasta.g_dcSubTextosFin
    End If
    
Salir:
    Unload oConfSubasta
    Set oConfSubasta = Nothing
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume Salir
End Sub

Private Sub cmdConfVistas_Click()
    ConfigurarVistas
End Sub

Private Sub cmdCrearPlant_Click()

    If cmdModoEdicion.caption = m_sConsulta Or cmdAceptar.Visible = True Or cmdModoEdicionAtr.caption = m_sConsulta Then
        Exit Sub
    End If
    
    If m_bModif Then
        
        g_Accion = ACCPlantillasAnya
               
        frmPlantillaDetalle.caption = m_sA�adir
    
        frmPlantillaDetalle.WindowState = vbNormal
        frmPlantillaDetalle.Show 1
    
    End If

End Sub

Private Sub cmdDeshacer_Click()
''' * Objetivo: Deshacer la edicion en el Atributo actual
    
    sdbgGrupos.CancelUpdate
    sdbgGrupos.DataChanged = False
        
    If Not m_oGrupoEnEdicion Is Nothing Then
        txtGrupoDescr.Text = NullToStr(m_oGrupoEnEdicion.Descripcion)
        Set g_oIBaseDatos = Nothing
        Set m_oGrupoEnEdicion = Nothing
    Else
        txtGrupoDescr.Text = ""
    End If
    If m_bModoEdicion Then
        sdbgGrupos.Columns("COD").Locked = False
        sdbgGrupos.Columns("NOM").Locked = False
    End If
    cmdAnyadir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    cmdCodigo.Enabled = True
    
    sdbgGrupos.Refresh

End Sub

Private Sub cmdDeshacerAtr_Click()
''' * Objetivo: Deshacer la edicion en el Atributo actual
    
    sdbgAtribPlant.CancelUpdate
    sdbgAtribPlant.DataChanged = False
    
    cmdAnyadirAtr.Enabled = True
    cmdEliAtr.Enabled = True
    cmdDeshacerAtr.Enabled = False
    g_bGuardarEnDefinicion = False
    
    sdbgAtribPlant.Refresh

End Sub


Private Sub cmdDeshacerAtrEsp_Click()

    sdbgAtributosEsp.CancelUpdate
    sdbgAtributosEsp.DataChanged = False
    
    cmdDeshacerAtrEsp.Enabled = False

End Sub

''' <summary>
''' Evento producido al hacer click boton de eliminar un atributo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdEliAtr_Click()
'******************************************************************
'*** Descripci�n: Elimina los atributos seleccionados con opcion  ***
'***                    de eliminar varios a la vez              ***
'******************************************************************
Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim aAux As Variant
Dim iIndice As Integer
Dim inum As Integer
Dim i As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit
  
    If sdbgAtribPlant.Rows = 0 Then Exit Sub
    
    If sdbgAtribPlant.Columns("COD").Text = "" Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    sdbgAtribPlant.GetBookmark sdbgAtribPlant.Row
    
    sdbgAtribPlant.SelBookmarks.Add sdbgAtribPlant.Bookmark
        
    If sdbgAtribPlant.SelBookmarks.Count > 0 Then
    
        Select Case sdbgAtribPlant.SelBookmarks.Count
            Case 0
                Screen.MousePointer = vbNormal
                Exit Sub
            Case 1
                irespuesta = oMensajes.PreguntaEliminar(m_asMensajes(0) & " " & sdbgAtribPlant.Columns("COD").Text & " (" & sdbgAtribPlant.Columns("NOMBRE").Text & ")")
            Case Is > 1
                irespuesta = oMensajes.PreguntaEliminarAtributosPlant(m_asMensajes(1))
        End Select
          
        If irespuesta = vbNo Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        If ComprobarActualizarPlantilla(g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Text))) = False Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        ReDim aIdentificadores(sdbgAtribPlant.SelBookmarks.Count)
        ReDim aBookmarks(sdbgAtribPlant.SelBookmarks.Count)
            
        i = 0
        While i < sdbgAtribPlant.SelBookmarks.Count
            aIdentificadores(i + 1) = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Text)).idAtribProce
            aBookmarks(i + 1) = sdbgAtribPlant.Columns("ATRIB").Text
            sdbgAtribPlant.Bookmark = sdbgAtribPlant.SelBookmarks(i)
            i = i + 1
        Wend
        
        udtTeserror = g_oPlantillaSeleccionada.EliminarAtributos(aIdentificadores)
            If udtTeserror.NumError <> TESnoerror Then
              If udtTeserror.NumError = TESImposibleEliminar Then
                  iIndice = 1
                  aAux = udtTeserror.Arg1
                  inum = UBound(udtTeserror.Arg1, 2)
                  
                  For i = 0 To inum
                      sdbgAtribPlant.SelBookmarks.Remove (aAux(2, i) - iIndice)
                      iIndice = iIndice + 1
                  Next
                    
                  oMensajes.ImposibleEliminacionMultiple 345, udtTeserror.Arg1 'todos los atributos cambiar el 345
                  If sdbgAtribPlant.SelBookmarks.Count > 0 Then
                      sdbgAtribPlant.DeleteSelected
                      sdbgAtribPlant.ReBind
                  End If
              Else
                  TratarError udtTeserror
                  Screen.MousePointer = vbNormal
                  Exit Sub
              End If
            Else
                For i = 1 To sdbgAtribPlant.SelBookmarks.Count
                    If Not m_oAtributo Is Nothing Then
                        If CStr(m_oAtributo.Id) = sdbgAtribPlant.Columns("ATRIB").Text Then
                            Set m_oAtributo = Nothing
                        End If
                    End If
                    RegistrarAccion ACCPlantAtribEli, "Atributo eliminado: " & aBookmarks(i) & " Grupo " & g_sCodGRP
                    g_oPlantillaSeleccionada.ATRIBUTOS.Remove CStr((aBookmarks(i)))
                Next
                sdbgAtribPlant.DeleteSelected
                sdbgAtribPlant.ReBind
                DoEvents
            End If
    End If
    
    sdbgAtribPlant.SelBookmarks.RemoveAll
    
    If sdbgAtribPlant.Row = "-1" Then
        sdbgAtribPlant.RemoveAll
    End If
    
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Elimina atributos de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click sobre el boton </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdElimAtribEsp_Click()

Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim aAux As Variant
Dim iIndice As Integer
Dim inum As Integer
Dim i As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit
  
    If sdbgAtributosEsp.Rows = 0 Then Exit Sub
    
    If sdbgAtributosEsp.Columns("COD").Text = "" Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    sdbgAtributosEsp.GetBookmark sdbgAtributosEsp.Row
    
    sdbgAtributosEsp.SelBookmarks.Add sdbgAtributosEsp.Bookmark
        
    If sdbgAtributosEsp.SelBookmarks.Count > 0 Then
    
        Select Case sdbgAtributosEsp.SelBookmarks.Count
            Case 0
                Screen.MousePointer = vbNormal
                Exit Sub
            Case 1
                irespuesta = oMensajes.PreguntaEliminar(m_asMensajes(0) & " " & sdbgAtributosEsp.Columns("COD").Text & " (" & sdbgAtributosEsp.Columns("DEN").Text & ")")
            Case Is > 1
                irespuesta = oMensajes.PreguntaEliminarAtributosPlant(m_asMensajes(1))
        End Select
          
        If irespuesta = vbNo Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        ReDim aIdentificadores(sdbgAtributosEsp.SelBookmarks.Count)
        ReDim aBookmarks(sdbgAtributosEsp.SelBookmarks.Count)
            
        i = 0
        While i < sdbgAtributosEsp.SelBookmarks.Count
            aIdentificadores(i + 1) = sdbgAtributosEsp.Columns("IDATRIB").Text
            aBookmarks(i + 1) = sdbgAtributosEsp.Columns("IDATRIB").Text
            sdbgAtributosEsp.Bookmark = sdbgAtributosEsp.SelBookmarks(i)
            i = i + 1
        Wend
        
        udtTeserror = g_oPlantillaSeleccionada.EliminarAtributosEspecificacion(aIdentificadores, g_sCodGRPEsp)
            If udtTeserror.NumError <> TESnoerror Then
              If udtTeserror.NumError = TESImposibleEliminar Then
                  iIndice = 1
                  aAux = udtTeserror.Arg1
                  inum = UBound(udtTeserror.Arg1, 2)
                  
                  For i = 0 To inum
                      sdbgAtribPlant.SelBookmarks.Remove (aAux(2, i) - iIndice)
                      iIndice = iIndice + 1
                  Next
                    
                  oMensajes.ImposibleEliminacionMultiple 345, udtTeserror.Arg1 'todos los atributos cambiar el 345
                  If sdbgAtribPlant.SelBookmarks.Count > 0 Then
                      sdbgAtribPlant.DeleteSelected
                      sdbgAtribPlant.ReBind
                  End If
              Else
                  TratarError udtTeserror
                  Screen.MousePointer = vbNormal
                  Exit Sub
              End If
            Else
                For i = 1 To sdbgAtributosEsp.SelBookmarks.Count
                    If Not m_oAtributo Is Nothing Then
                        If CStr(m_oAtributo.Id) = sdbgAtributosEsp.Columns("IDATRIB").Text Then
                            Set m_oAtributo = Nothing
                        End If
                    End If
                    RegistrarAccion ACCPlantAtribEspEli, "Atributo eliminado: " & aBookmarks(i) & " Grupo " & g_sCodGRPEsp
                    g_oPlantillaSeleccionada.AtributosEspecificacion.Remove CStr((aBookmarks(i)))
                Next
                sdbgAtributosEsp.DeleteSelected
                sdbgAtributosEsp.ReBind
                DoEvents
            End If
    End If
    
    sdbgAtribPlant.SelBookmarks.RemoveAll
    
    If sdbgAtribPlant.Row = "-1" Then
        sdbgAtribPlant.RemoveAll
    End If
    
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Evento producido al hacer click boton de eliminar un grupo de la plantilla
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdEliminar_Click()
Dim aIdentificadores As Variant
Dim i As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit
Dim vCambio As Variant
Dim bReordenarSobres As Boolean

    If sdbgGrupos.Rows = 0 Then Exit Sub
    
    If sdbgGrupos.Columns("COD").Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    sdbgGrupos.GetBookmark sdbgGrupos.Row
    
    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
   
    Select Case sdbgGrupos.SelBookmarks.Count
        Case 0
            Screen.MousePointer = vbNormal
            Exit Sub
        Case 1
            irespuesta = oMensajes.PreguntaEliminar(m_sElGrupo & " " & sdbgGrupos.Columns("COD").Value & " (" & sdbgGrupos.Columns("NOM").Value & ")")
        Case Is > 1
            irespuesta = oMensajes.PreguntaEliminarGrupos(m_sMensajeMultGrupos)
    End Select
    
    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If ComprobarActualizarPlantilla = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    ReDim aIdentificadores(sdbgGrupos.SelBookmarks.Count)
    i = 0
    While i < sdbgGrupos.SelBookmarks.Count
        aIdentificadores(i + 1) = sdbgGrupos.Columns("COD").CellValue(sdbgGrupos.SelBookmarks.Item(i))
        i = i + 1
    Wend
    
    vCambio = GruposDefGruposAEliminar(aIdentificadores)
    
    If g_oPlantillaSeleccionada.AdminPub = True Then
        bReordenarSobres = ReordenarSobres
    Else
        bReordenarSobres = False
    End If
    
    If vCambio(0) <> "" Or vCambio(1) <> "" Then
        Screen.MousePointer = vbNormal
        irespuesta = oMensajes.PreguntaElimGrupoCambiarAmbitoDatos(vCambio(0), vCambio(1))
        If irespuesta = vbYes Then
            Screen.MousePointer = vbHourglass
            ModificarConfigPlantilla vCambio(2)
            udtTeserror = g_oPlantillaSeleccionada.EliminarGruposDeBaseDatos(aIdentificadores, True, bReordenarSobres)
            If udtTeserror.NumError <> TESnoerror Then
                  TratarError udtTeserror
                  Screen.MousePointer = vbNormal
                  Exit Sub
            Else
                For i = 1 To sdbgGrupos.SelBookmarks.Count
                    RegistrarAccion ACCPlantGrupoEli, "Grupo Eliminado: " & aIdentificadores(i)
                    g_oPlantillaSeleccionada.Grupos.Remove (aIdentificadores(i))
                Next
                If g_sCodGRP = sdbgGrupos.Columns("COD").Text Then
                    g_sCodGRP = ""
                    CargarGridAtributos
                End If
                sdbgGrupos.DeleteSelected
                sdbgGrupos.ReBind
                txtGrupoDescr.Text = ""
                DoEvents
            End If
            sdbgGrupos.SelBookmarks.RemoveAll
            sdbgGrupos.Refresh
            CargarGridPlantillas True
        Else
            sdbgGrupos.SelBookmarks.RemoveAll
        End If
    Else
        udtTeserror = g_oPlantillaSeleccionada.EliminarGruposDeBaseDatos(aIdentificadores, , bReordenarSobres)
        If udtTeserror.NumError <> TESnoerror Then
              TratarError udtTeserror
              Screen.MousePointer = vbNormal
              Exit Sub
        Else
            For i = 1 To sdbgGrupos.SelBookmarks.Count
                RegistrarAccion ACCPlantGrupoEli, "Grupo Eliminado: " & aIdentificadores(i)
                g_oPlantillaSeleccionada.Grupos.Remove (aIdentificadores(i))
            Next
            If g_sCodGRP = sdbgGrupos.Columns("COD").Text Then
                CargarComboGrupos
                CargarGridAtributos
                CargarEspecificaciones
            End If
            sdbgGrupos.DeleteSelected
            sdbgGrupos.ReBind
            txtGrupoDescr.Text = ""
            DoEvents
        End If
        sdbgGrupos.SelBookmarks.RemoveAll
        sdbgGrupos.Refresh
        CargarGridPlantillas True
    End If
    
    If bReordenarSobres = True Then
        oMensajes.SobresReordenados
    End If
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdEliminarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim oIBAseDatosEnEdicion As IBaseDatos

On Error GoTo Cancelar:

    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
        
    Set Item = lstvwEsp.selectedItem

    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else

        irespuesta = oMensajes.PreguntaEliminar(" " & m_sIdiElArchivo & " " & lstvwEsp.selectedItem.Text)
        If irespuesta = vbNo Then Exit Sub
            Screen.MousePointer = vbHourglass
            Set oEsp = g_oPlantillaSeleccionada.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
            Set oIBAseDatosEnEdicion = oEsp
            teserror = oIBAseDatosEnEdicion.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Set oIBAseDatosEnEdicion = Nothing
            Else
                g_oPlantillaSeleccionada.especificaciones.Remove (CStr(lstvwEsp.selectedItem.Tag))
                basSeguridad.RegistrarAccion AccionesSummit.ACCPlantProceEspEli, "Eliminacion de archivo de especificaciones. Plantilla:" & g_oPlantillaSeleccionada.Id & "Archivo:" & lstvwEsp.selectedItem.Text
                lstvwEsp.ListItems.Remove (CStr(lstvwEsp.selectedItem.key))
                If g_oPlantillaSeleccionada.especificaciones.Count = 0 Then
                    Set g_oPlantillaSeleccionada.especificaciones = Nothing
                End If
            End If
    End If
    
    Set oEsp = Nothing
    Set oIBAseDatosEnEdicion = Nothing
    
    Screen.MousePointer = vbNormal
    
    Exit Sub

Cancelar:
    
    Screen.MousePointer = vbNormal
    Set oEsp = Nothing
    Set oIBAseDatosEnEdicion = Nothing

End Sub

Private Sub cmdEliminarPlant_Click()
    EliminarPlantilla
End Sub

Private Sub cmdEliminarVistas_Click()
    EliminarVistas
End Sub

Private Sub cmdMod_Click()
Dim udtTeserror As TipoErrorSummit

    
    Screen.MousePointer = vbHourglass
    Set g_oIBaseDatos = g_oPlantillaSeleccionada
    udtTeserror = g_oIBaseDatos.IniciarEdicion
    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError udtTeserror
        g_oIBaseDatos.CancelarEdicion
        Set g_oIBaseDatos = Nothing
        Set m_oGrupos = Nothing
        Exit Sub
    End If
    Set m_oGrupos = g_oPlantillaSeleccionada.Grupos
    
    m_iIndiListw = lstvwplantillas.selectedItem.Index 'Indice del elemento del lstvw antes de modificar
                                                      'para evitar que seleccionen otra plantilla cuando estamos modificando
                                                      
    m_bModoEdicionPlant = True
    picAmbitoDatosOfe.Enabled = True
    cmdCrearPlant.Enabled = False
    cmdModificarPlant.Enabled = False
    cmdEliminarPlant.Enabled = False
    cmdConfVistas.Enabled = False
    cmdEliminarVistas.Enabled = False
    cmdMod.Visible = False
    cmdRestaurar.Visible = False
    cmdAceptar.Visible = True
    cmdCancelar.Visible = True
    
    sdbgPlantillas.Columns("PROCE").Locked = False
    sdbgPlantillas.Columns("GRUPO").Locked = False
    sdbgPlantillas.Columns("BOTONGRUP").Locked = False
    sdbgPlantillas.Columns("ITEM").Locked = False
    sdbgPlantillas.Columns("USAR").Locked = False
     
    picAdminPub.Enabled = True
                                                      
    Screen.MousePointer = vbNormal

End Sub




Private Sub cmdModificarEsp_Click()
Dim teserror As TipoErrorSummit
Dim oIBAseDatosEnEdicion As IBaseDatos

    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
        
    If lstvwEsp.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        Screen.MousePointer = vbHourglass
        Set oIBAseDatosEnEdicion = g_oPlantillaSeleccionada.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        teserror = oIBAseDatosEnEdicion.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
        g_Accion = ACCPlantProceEspMod
        Screen.MousePointer = vbNormal
        frmPROCEEspMod.g_sOrigen = "frmPlantillasProce"
        Set frmPROCEEspMod.g_oIBaseDatos = oIBAseDatosEnEdicion
        frmPROCEEspMod.Show 1
    End If

    
    g_Accion = ACCPlantillasCon
    Set oIBAseDatosEnEdicion = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdModificarMateriales_Click()

    If frmPlantillasProce.g_oPlantillaSeleccionada.ExisteRestriccionMaterial Then
        m_bTieneMateriales = True
    Else
        m_bTieneMateriales = False
    End If
    'edu pm98
    ' a peticion de Encarni, permitir la modificacion de los materiales en este formulario.
    'ModificarPlantilla
    ConfigurarMateriales True
End Sub

Private Sub cmdModificarPlant_Click()
    ModificarPlantilla
End Sub

Private Sub cmdModoEdicion_Click()
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
Dim v As Variant
Dim i As Integer
Dim vbm As Variant

    m_bActualizar = True
    
    If Not m_bModoEdicion Then
                
        ' PASAR A MODO EDICION
        
        m_iIndiListw = lstvwplantillas.selectedItem.Index 'Indice del elemento del lstvw antes de modificar
                                                  'para evitar que seleccionen otra plantilla cuando estamos modificando
        lstvwplantillas.Enabled = True
        sdbgGrupos.AllowAddNew = True
        sdbgGrupos.AllowDelete = False
        sdbgGrupos.AllowUpdate = True
        
        cmdModoEdicion.caption = m_sConsulta
        cmdRestGrupo.Visible = False
        
        If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
            cmdCodigo.Visible = True
        End If
        
        cmdAnyadir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
        cmdAnyadir.Enabled = True
        cmdEliminar.Enabled = True
        cmdCodigo.Enabled = True
        cmdDeshacer.Enabled = False
        
        cmdCrearPlant.Enabled = False
        cmdModificarPlant.Enabled = False
        cmdEliminarPlant.Enabled = False
        cmdConfVistas.Enabled = False
        cmdEliminarVistas.Enabled = False
        
        txtGrupoDescr.Locked = False
        
        m_bModoEdicion = True
        
    Else
        
        If sdbgGrupos.DataChanged = True Then
        
            v = sdbgGrupos.ActiveCell.Value
            If Me.Visible Then sdbgGrupos.SetFocus
            If (v <> "") Then
                sdbgGrupos.ActiveCell.Value = v
            End If
            
            If (actualizarYSalir() = True) Then
                Exit Sub
            End If
        Else
           
        End If
        
        'si es un proceso de administraci�n p�blica
        If g_oPlantillaSeleccionada.AdminPub = True Then
            'no deja pasar a consulta hasta que se introduzcan los sobres para todos los grupos.
            For i = 0 To sdbgGrupos.Rows - 1
                vbm = sdbgGrupos.AddItemBookmark(i)
                If sdbgGrupos.Columns("SOBRE").CellValue(vbm) = "" Then
                    oMensajes.IntroduzcaSobre
                    Exit Sub
                End If
            Next i
            sdbgGrupos.MoveFirst
        End If
        
        sdbgGrupos.AllowAddNew = False
        sdbgGrupos.AllowUpdate = False
        sdbgGrupos.AllowDelete = False
                
        cmdAnyadir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdCodigo.Visible = False
        cmdRestGrupo.Visible = True
        txtGrupoDescr.Locked = True
        
        cmdCrearPlant.Enabled = True
        cmdModificarPlant.Enabled = True
        cmdEliminarPlant.Enabled = True
        cmdConfVistas.Enabled = True
        cmdEliminarVistas.Enabled = True
        
        cmdModoEdicion.caption = m_sEdicion
        
        m_bModoEdicion = False
                       
        lstvwplantillas.ListItems.Item(m_iIndiListw).Selected = True
        
    End If
    
    If Me.Visible Then sdbgGrupos.SetFocus

End Sub

Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    If sdbgGrupos.DataChanged = True Then
        m_bValError = False
        m_bAnyaError = False
        m_bModError = False
        If sdbgGrupos.Row = 0 Then
            sdbgGrupos.MoveNext
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgGrupos.MovePrevious
            End If
        Else
            sdbgGrupos.MovePrevious
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgGrupos.MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
End Function

Public Function actualizarYSalirAtrib() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    If sdbgAtribPlant.DataChanged = True Then
        m_bValError = False
        m_bAnyaError = False
        m_bModError = False
        If sdbgAtribPlant.Row = 0 Then
            sdbgAtribPlant.MoveNext
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalirAtrib = True
                Exit Function
            Else
                sdbgAtribPlant.MovePrevious
            End If
        Else
            sdbgAtribPlant.MovePrevious
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalirAtrib = True
                Exit Function
            Else
                sdbgAtribPlant.MoveNext
            End If
        End If
    Else
        actualizarYSalirAtrib = False
    End If
End Function

''' <summary>
''' Pone en edicion o consulta el taba de atributos
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click sobre el boton </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdModoEdicionAtr_Click()
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    Dim i As Integer
     
    If Not m_bModoEdicionAtr Then

        ' PASAR A MODO EDICION
        
        m_iIndiListw = lstvwplantillas.selectedItem.Index 'Indice del elemento del lstvw antes de modificar
                                                  'para evitar que seleccionen otra plantilla cuando estamos modificando
        sdbgAtribPlant.AllowDelete = False
        sdbgAtribPlant.SelectTypeRow = ssSelectionTypeMultiSelectRange
        
        cmdModoEdicionAtr.caption = m_sConsulta
        cmdRestAtr.Visible = False
        cmdOrden(0).Visible = False
        
        If Not m_bModifAtrib Then
            cmdAnyadirAtr.Visible = False
            cmdDeshacerAtr.Left = cmdEliAtr.Left
            cmdEliAtr.Left = cmdAnyadirAtr.Left
        Else
            cmdAnyadirAtr.Visible = True
        End If
        
        cmdEliAtr.Visible = True
        cmdDeshacerAtr.Visible = True
        
        cmdAnyadirAtr.Enabled = True
        cmdEliAtr.Enabled = True
        cmdDeshacerAtr.Enabled = False
        
        cmdCrearPlant.Enabled = False
        cmdModificarPlant.Enabled = False
        cmdEliminarPlant.Enabled = False
        cmdConfVistas.Enabled = False
        cmdEliminarVistas.Enabled = False
        
        sdbcSeleccion.Enabled = False
        sdbddAplicarA.Enabled = True
        sdbddAmbito.Enabled = True
        sdbddSigno.Enabled = True
        
        picNavigateAtrib.Enabled = True
           
        m_bModoEdicionAtr = True
        ProtegerFila
        
    Else
        
        m_bModoEdicionAtr = False
    
        If sdbgAtribPlant.Rows > 1 Then
        
            If sdbgAtribPlant.DataChanged = True Then
            
                v = sdbgAtribPlant.ActiveCell.Value
                If Me.Visible Then sdbgAtribPlant.SetFocus
                If (v <> "") Then
                    sdbgAtribPlant.ActiveCell.Value = v
                End If
                
                If (actualizarYSalirAtrib() = True) Then
                    m_bModoEdicionAtr = True
                    Exit Sub
                End If
            Else
               
            End If
        Else
            sdbgAtribPlant.Update
            CargarGridAtributos
        End If
           
        sdbcSeleccion.Enabled = True
        sdbgAtribPlant.AllowAddNew = False
        sdbgAtribPlant.AllowDelete = False
        sdbgAtribPlant.SelectTypeRow = ssSelectionTypeNone
        
        cmdAnyadirAtr.Visible = False
        cmdEliAtr.Visible = False
        cmdDeshacerAtr.Visible = False
        cmdRestAtr.Visible = True
        cmdOrden(0).Visible = True
        picNavigateAtrib.Enabled = True
        
        cmdCrearPlant.Enabled = True
        cmdModificarPlant.Enabled = True
        cmdEliminarPlant.Enabled = True
        cmdConfVistas.Enabled = True
        cmdEliminarVistas.Enabled = True
        
        For i = 0 To sdbgAtribPlant.Cols - 1
            sdbgAtribPlant.Columns(i).Locked = True
        Next
        sdbgAtribPlant.Columns("BOT").Locked = False
        sdbgAtribPlant.Columns("POND").Locked = False
        
        
        cmdModoEdicionAtr.caption = m_sEdicion
                
        sdbddAplicarA.Enabled = False
        sdbddAmbito.Enabled = False
        sdbddSigno.Enabled = False
        
        Set m_oAtributo = Nothing
                       
    End If
    
    If Me.Visible Then sdbgAtribPlant.SetFocus
    
End Sub


''' <summary>
''' Pone en edicion o consulta el taba de atributos de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click sobre el boton </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdModoEdicionAtrEsp_Click()

Dim v As Variant
    
If Not m_bModoEdicionAtrEsp Then
        m_iIndiListw = lstvwplantillas.selectedItem.Index 'Indice del elemento del lstvw antes de modificar
        'para evitar que seleccionen otra plantilla cuando estamos modificando
        
        cmdCrearPlant.Enabled = False
        cmdModificarPlant.Enabled = False
        cmdEliminarPlant.Enabled = False
        cmdConfVistas.Enabled = False
        cmdEliminarVistas.Enabled = False

        cmdModoEdicionAtrEsp.caption = m_sConsulta
        cmdDeshacerAtrEsp.Enabled = False
                
        sdbddAmbitoEspec.Enabled = True
        sdbddValor.Enabled = True
        sdbddValidacion.Enabled = True
    
        sdbgAtributosEsp.Columns("INTERNO").Locked = False
        sdbgAtributosEsp.Columns("PEDIDO").Locked = False
        sdbgAtributosEsp.Columns("COD").Locked = False
        sdbgAtributosEsp.Columns("DEN").Locked = False
        sdbgAtributosEsp.Columns("TIPO").Locked = False
        sdbgAtributosEsp.Columns("VALOR").Locked = False
        sdbgAtributosEsp.Columns("AMBITO").Locked = False
        sdbgAtributosEsp.Columns("OBLIGATORIO").Locked = False
        sdbgAtributosEsp.Columns("VALIDACION").Locked = False
        
        cmdSortAscEsp.Enabled = True
        cmdSortDesEsp.Enabled = True
        cmdAnyaAtribEsp.Enabled = True
        cmdElimAtribEsp.Enabled = True
        
        cmdAnyadirEsp.Enabled = True
        cmdEliminarEsp.Enabled = True
        cmdModificarEsp.Enabled = True
        
        txtProceEsp.Enabled = True
        
        m_bModoEdicionAtrEsp = True
        
                

Else

        If sdbgAtributosEsp.Rows > 1 Then
        
            If sdbgAtributosEsp.DataChanged = True Then
            
                v = sdbgAtributosEsp.ActiveCell.Value
                If Me.Visible Then sdbgAtributosEsp.SetFocus
                If (actualizarYSalirAtribEsp() = True) Then
                    Exit Sub
                End If
            Else
               
            End If
        Else
            sdbgAtributosEsp.Update
            CargarGridEspecificaciones
        End If

        cmdModoEdicionAtrEsp.caption = m_sEdicion
     
        sdbddAmbitoEspec.Enabled = False
        sdbddValor.Enabled = False
        sdbddValidacion.Enabled = False
        
        sdbgAtributosEsp.Columns("INTERNO").Locked = True
        sdbgAtributosEsp.Columns("PEDIDO").Locked = True
        sdbgAtributosEsp.Columns("COD").Locked = True
        sdbgAtributosEsp.Columns("DEN").Locked = True
        sdbgAtributosEsp.Columns("TIPO").Locked = True
        sdbgAtributosEsp.Columns("VALOR").Locked = True
        sdbgAtributosEsp.Columns("AMBITO").Locked = True
        sdbgAtributosEsp.Columns("OBLIGATORIO").Locked = True
        sdbgAtributosEsp.Columns("VALIDACION").Locked = True
        
        cmdSortAscEsp.Enabled = False
        cmdSortDesEsp.Enabled = False
        cmdAnyaAtribEsp.Enabled = False
        cmdElimAtribEsp.Enabled = False
        
        cmdAnyadirEsp.Enabled = False
        cmdEliminarEsp.Enabled = False
        cmdModificarEsp.Enabled = False
        
        txtProceEsp.Enabled = False
        
        cmdCrearPlant.Enabled = True
        cmdModificarPlant.Enabled = True
        cmdEliminarPlant.Enabled = True
        cmdConfVistas.Enabled = True
        cmdEliminarVistas.Enabled = True
        m_bModoEdicionAtrEsp = False
        cmdDeshacerAtrEsp.Enabled = False
                
End If

End Sub

Private Sub cmdOrden_Click(Index As Integer)

    If Index = 0 Then
    Set frmOrdenAtributos.g_oPlantilla = g_oPlantillaSeleccionada
    frmOrdenAtributos.g_sOrigen = "PLANTILLAS"
    If Not m_bModif Then  'No tiene permisos para guardar el orden
        frmOrdenAtributos.g_bNoModif = True
    End If
    frmOrdenAtributos.Show 1
    Else
    
    End If

End Sub

Private Sub cmdRestAtr_Click()
    CargarGridAtributos
End Sub

Private Sub cmdRestaurar_Click()
    CargarGridPlantillas
End Sub

Private Sub cmdRestGrupo_Click()
    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
    
Dim oGrupo As CGrupo
    
    sdbgGrupos.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    g_oPlantillaSeleccionada.CargarTodosLosGrupos False, OrdPlantillaPorCod
    
    For Each oGrupo In g_oPlantillaSeleccionada.Grupos
        sdbgGrupos.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den & Chr(m_lSeparador) & NullToStr(oGrupo.Descripcion) & Chr(m_lSeparador) & NullToStr(oGrupo.Sobre)
    Next
    
    sdbgGrupos.ReBind
    
    If Me.Visible Then sdbgGrupos.SetFocus
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdSalvarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oFos As Scripting.FileSystemObject

On Error GoTo Cancelar:
    
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub

Set Item = lstvwEsp.selectedItem

If Item Is Nothing Then
    oMensajes.SeleccioneFichero

Else

    cmmdEsp.DialogTitle = m_sIdiGuardar
    cmmdEsp.Filter = m_sIdiTipoOrig & "|*.*"
    cmmdEsp.filename = Item.Text
    cmmdEsp.ShowSave

    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileTitle = "" Then
        oMensajes.NoValido m_sIdiArchivo
        Exit Sub
    End If

    Set oEsp = g_oPlantillaSeleccionada.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        
    If IsNull(oEsp.Ruta) Then 'Los archivos son adjuntos
        
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspPlantilla)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Exit Sub
        End If
        
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspPlantilla, oEsp.DataSize, sFileName
        
    Else ' los archivos son vinculados
        Set oFos = New FileSystemObject
        If oFos.FolderExists(oEsp.Ruta) And oFos.FileExists(oEsp.Ruta & oEsp.nombre) Then
            oFos.CopyFile oEsp.Ruta & oEsp.nombre, sFileName, False
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            oMensajes.ImposibleModificarEsp (oEsp.nombre & " - " & oEsp.Ruta)
            
        End If
        
        Set oFos = Nothing
        Set oEsp = Nothing
    
    End If
End If
Set oEsp = Nothing
Screen.MousePointer = vbNormal
Exit Sub

Cancelar:

    Screen.MousePointer = vbNormal
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
        Set oFos = Nothing
    End If
End Sub

Private Sub cmdSortAsc_Click()
    Dim i As Integer
    Dim iColumnas As Integer
    Dim vdatos1() As Variant
    Dim vdatos2() As Variant
    
    If sdbgAtribPlant.Rows < 2 Then Exit Sub
    If sdbgAtribPlant.Row = 0 Then Exit Sub
    
    'Si estamos en un grupo y el atributo est� definido a nivel de proceso no se desplaza

    If g_sCodGRP <> "" Then
        If sdbgAtribPlant.Columns("CODGRUPO").Text = "" Then Exit Sub
    End If

    If m_bModoEdicionAtr Then sdbgAtribPlant.SelectTypeRow = ssSelectionTypeSingleSelect
    'Actualiza la grid con los cambios de orden
    iColumnas = sdbgAtribPlant.Columns.Count
    ReDim vdatos1(iColumnas - 1)
    ReDim vdatos2(iColumnas - 1)
    
    For i = 0 To iColumnas - 1
        vdatos1(i) = sdbgAtribPlant.Columns(i).Value
    Next i
    
    sdbgAtribPlant.MovePrevious
    
    'Si la siguiente l�nea es de un atributo de proc., estando en un grupo no la desplaza
    If g_sCodGRP <> "" Then
        If sdbgAtribPlant.Columns("CODGRUPO").Text = "" Then
            sdbgAtribPlant.MoveNext
            If m_bModoEdicionAtr Then sdbgAtribPlant.SelectTypeRow = ssSelectionTypeMultiSelectRange
            Exit Sub
        End If
    End If
    
    For i = 0 To iColumnas - 1
        vdatos2(i) = sdbgAtribPlant.Columns(i).Value
    Next i
    For i = 0 To iColumnas - 1
        sdbgAtribPlant.Columns(i).Value = vdatos1(i)
    Next i
    
    sdbgAtribPlant.MoveNext
    For i = 0 To iColumnas - 1
        sdbgAtribPlant.Columns(i).Value = vdatos2(i)
    Next i
    
    sdbgAtribPlant.SelBookmarks.RemoveAll
    sdbgAtribPlant.MovePrevious
    sdbgAtribPlant.SelBookmarks.Add sdbgAtribPlant.Bookmark
    
    If m_bModoEdicionAtr Then sdbgAtribPlant.SelectTypeRow = ssSelectionTypeMultiSelectRange
    'Comprueba los cambios de orden que se han producido y almacena en la base
    'de datos:
    GuardarOrdenAtributos
    
End Sub


''' <summary>
''' Ordenacion de atributos de especificacion hacia arriba
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en el boton </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdSortAscEsp_Click()

Dim i As Long
Dim iColumnas As Integer
Dim vdatos1() As Variant
Dim vdatos2() As Variant
Dim vdatos1T() As Variant
Dim vdatos2T() As Variant
Dim oAtributos As CAtributos
Dim teserror As TipoErrorSummit
Dim vbm As Variant
Dim oatrib As CAtributo

    If sdbgAtributosEsp.Rows < 2 Then Exit Sub
    sdbgAtributosEsp.SelectTypeRow = ssSelectionTypeSingleSelect
            'Desplaza el orden del atributo hacia arriba
            If sdbgAtributosEsp.Row = 0 Then
                sdbgAtributosEsp.SelectTypeRow = ssSelectionTypeMultiSelectRange
                Exit Sub
            End If
            'Actualiza la grid con los cambios de orden
            iColumnas = sdbgAtributosEsp.Columns.Count
            ReDim vdatos1(iColumnas - 1)
            ReDim vdatos2(iColumnas - 1)
            ReDim vdatos1T(iColumnas - 1)
            ReDim vdatos2T(iColumnas - 1)
            
            For i = 0 To iColumnas - 1
                vdatos1(i) = sdbgAtributosEsp.Columns(i).Value
                vdatos1T(i) = sdbgAtributosEsp.Columns(i).Text
            Next i
            
            sdbgAtributosEsp.MovePrevious
    
            For i = 0 To iColumnas - 1
                vdatos2(i) = sdbgAtributosEsp.Columns(i).Value
                 vdatos2T(i) = sdbgAtributosEsp.Columns(i).Text
            Next i
            For i = 0 To iColumnas - 1
                sdbgAtributosEsp.Columns(i).Value = vdatos1(i)
                sdbgAtributosEsp.Columns(i).Text = vdatos1T(i)
            Next i
            
            sdbgAtributosEsp.MoveNext
            For i = 0 To iColumnas - 1
                sdbgAtributosEsp.Columns(i).Value = vdatos2(i)
                sdbgAtributosEsp.Columns(i).Text = vdatos2T(i)
            Next i
            
            sdbgAtributosEsp.SelBookmarks.RemoveAll
            sdbgAtributosEsp.MovePrevious
            sdbgAtributosEsp.SelBookmarks.Add sdbgAtributosEsp.Bookmark
    
    
    
    sdbgAtributosEsp.SelectTypeRow = ssSelectionTypeMultiSelectRange
    'Comprueba los cambios de orden que se han producido y almacena en la base
    'de datos:
    
    Set oAtributos = oFSGSRaiz.Generar_CAtributos
    
        
        For i = 0 To sdbgAtributosEsp.Rows - 1
            vbm = sdbgAtributosEsp.AddItemBookmark(i)
            'Guarda en BD solo el orden de los atributos que han cambiado
                If NullToDbl0(g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Orden) <> (i + 1) Then
                    g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Orden = i + 1
                    oAtributos.Add g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Atrib, "", "", 1, , , , , , , , , , , g_oPlantillaSeleccionada.Id, , , frmPlantillasProce.g_sCodGRPEsp, , , , , , , g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).ambito, , , , , , , , , , , , , g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Orden, , , , , , , , , , , , g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Atrib
                End If
        Next i
    


      teserror = oAtributos.GuardarOrdenAtribEspecificacionPlant

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Unload frmESPERA
        Screen.MousePointer = vbNormal
       Exit Sub
    End If

    'Pongo la fecact a los atributos
    For Each oatrib In oAtributos
        g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(oatrib.Atrib)).FECACT = oatrib.FECACT
    Next
    Set oAtributos = Nothing



End Sub
Private Sub cmdSortDesc_Click()
    Dim i As Integer
    Dim iColumnas As Integer
    Dim vdatos1() As Variant
    Dim vdatos2() As Variant
    
    If sdbgAtribPlant.Rows < 2 Then Exit Sub
    If sdbgAtribPlant.Row = sdbgAtribPlant.Rows - 1 Then Exit Sub
    
    'Si estamos en un grupo y el atributo est� definido a nivel de proceso no se desplaza
    If g_sCodGRP <> "" Then
        If sdbgAtribPlant.Columns("CODGRUPO").Text = "" Then Exit Sub
    End If
    If m_bModoEdicionAtr Then sdbgAtribPlant.SelectTypeRow = ssSelectionTypeSingleSelect
    iColumnas = sdbgAtribPlant.Columns.Count
    ReDim vdatos1(iColumnas - 1)
    ReDim vdatos2(iColumnas - 1)
    
    For i = 0 To iColumnas - 1
        vdatos1(i) = sdbgAtribPlant.Columns(i).Value
    Next i
    
    sdbgAtribPlant.MoveNext
    'Si la siguiente l�nea es de un atributo de proc., estando en un grupo no la desplaza
    If g_sCodGRP <> "" Then
        If sdbgAtribPlant.Columns("CODGRUPO").Text = "" Then
            sdbgAtribPlant.MovePrevious
            If m_bModoEdicionAtr Then sdbgAtribPlant.SelectTypeRow = ssSelectionTypeMultiSelectRange
            Exit Sub
        End If
    End If
    
    For i = 0 To iColumnas - 1
        vdatos2(i) = sdbgAtribPlant.Columns(i).Value
    Next i
    For i = 0 To iColumnas - 1
        sdbgAtribPlant.Columns(i).Value = vdatos1(i)
    Next i
    
    sdbgAtribPlant.MovePrevious
    For i = 0 To iColumnas - 1
        sdbgAtribPlant.Columns(i).Value = vdatos2(i)
    Next i
    
    sdbgAtribPlant.MoveNext
    sdbgAtribPlant.SelBookmarks.Add sdbgAtribPlant.Bookmark
    If m_bModoEdicionAtr Then sdbgAtribPlant.SelectTypeRow = ssSelectionTypeMultiSelectRange
    'Comprueba los cambios de orden que se han producido y almacena en la base
    'de datos:
    GuardarOrdenAtributos
    
End Sub

''' <summary>
''' Ordenacion de atributos de especificacion hacia abajo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en el boton </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub cmdSortDesEsp_Click()

Dim i As Long
Dim iColumnas As Integer
Dim vdatos1() As Variant
Dim vdatos2() As Variant
Dim vdatos1T() As Variant
Dim vdatos2T() As Variant
Dim oAtributos As CAtributos
Dim teserror As TipoErrorSummit
Dim vbm As Variant
Dim oatrib As CAtributo

    If sdbgAtributosEsp.Rows < 2 Then Exit Sub
    sdbgAtributosEsp.SelectTypeRow = ssSelectionTypeSingleSelect
    
    
    
            'Desplaza el orden del atributo hacia abajo

            If sdbgAtributosEsp.Row = sdbgAtributosEsp.Rows - 1 Then
                sdbgAtributosEsp.SelectTypeRow = ssSelectionTypeMultiSelectRange
                Exit Sub
            End If
            iColumnas = sdbgAtributosEsp.Columns.Count
            ReDim vdatos1(iColumnas - 1)
            ReDim vdatos2(iColumnas - 1)
            ReDim vdatos1T(iColumnas - 1)
            ReDim vdatos2T(iColumnas - 1)

            For i = 0 To iColumnas - 1
                vdatos1(i) = sdbgAtributosEsp.Columns(i).Value
                vdatos1T(i) = sdbgAtributosEsp.Columns(i).Text
            Next i

            sdbgAtributosEsp.MoveNext

            For i = 0 To iColumnas - 1
                vdatos2(i) = sdbgAtributosEsp.Columns(i).Value
                vdatos2T(i) = sdbgAtributosEsp.Columns(i).Text
            Next i
            For i = 0 To iColumnas - 1
                sdbgAtributosEsp.Columns(i).Value = vdatos1(i)
                sdbgAtributosEsp.Columns(i).Text = vdatos1T(i)
            Next i

            sdbgAtributosEsp.MovePrevious
            For i = 0 To iColumnas - 1
                sdbgAtributosEsp.Columns(i).Value = vdatos2(i)
                sdbgAtributosEsp.Columns(i).Text = vdatos2T(i)
            Next i

            sdbgAtributosEsp.MoveNext
            sdbgAtributosEsp.SelBookmarks.Add sdbgAtributosEsp.Bookmark
    
    sdbgAtributosEsp.SelectTypeRow = ssSelectionTypeMultiSelectRange
    'Comprueba los cambios de orden que se han producido y almacena en la base
    'de datos:
    
    Set oAtributos = oFSGSRaiz.Generar_CAtributos
    
        
        For i = 0 To sdbgAtributosEsp.Rows - 1
            vbm = sdbgAtributosEsp.AddItemBookmark(i)
            'Guarda en BD solo el orden de los atributos que han cambiado
                If NullToDbl0(g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Orden) <> (i + 1) Then
                    g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Orden = i + 1
                    oAtributos.Add g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Atrib, "a", "a", 1, , , , , , , , , , , g_oPlantillaSeleccionada.Id, , , frmPlantillasProce.g_sCodGRPEsp, , , , , , , g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).ambito, , , , , , , , , , , , , g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Orden, , , , , , , , , , , , g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("idATRIB").CellValue(vbm))).Atrib
                End If
        Next i

      teserror = oAtributos.GuardarOrdenAtribEspecificacionPlant

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Unload frmESPERA
        Screen.MousePointer = vbNormal
       Exit Sub
    End If

    'Pongo la fecact a los atributos
    For Each oatrib In oAtributos
        g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(oatrib.Atrib)).FECACT = oatrib.FECACT
    Next
    Set oAtributos = Nothing




End Sub

''' <summary>
''' Carga del formulario
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub Form_Load()

    Me.Width = 11940
    Me.Height = 8280
        
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    ConfigurarSeguridad
    
    'Inicializar el array de ficheros
    ReDim m_aSayFileNames(0)
    
    cmdModoEdicionAtrEsp.Visible = False
    cmdDeshacerAtrEsp.Visible = False
    
            
    If Not m_bModif Then

        cmdCrearPlant.Visible = False    'Tab de proceso
        cmdMod.Visible = False
        cmdRestaurar.Left = cmdMod.Left
        cmdRestaurar.Visible = False
        
        cmdEliminar.Visible = False   'Tab de grupo
        cmdAnyadir.Visible = False
        cmdDeshacer.Visible = False
        cmdModoEdicion.Visible = False
        cmdCodigo.Visible = False
        
        cmdModoEdicionAtr.Visible = False  'Tab de atributos
        cmdSortAsc.Visible = False
        cmdSortDesc.Visible = False
        sdbgAtribPlant.Left = sdbgAtribPlant.Left - 370
        
        txtProceEsp.Locked = True
        cmdAnyadirEsp.Enabled = False
        cmdEliminarEsp.Enabled = False
        cmdModificarEsp.Enabled = False
        
    End If
    
    txtGrupoDescr.Locked = True
    
    Set g_oPlantillas = oFSGSRaiz.Generar_CPlantillas
    
    g_Accion = ACCPlantillasCon
    
    m_bCargarPrimero = True
    
    lstvwplantillas.ListItems.clear
    
    g_lNumID = 0
    
    CargarGridPlantillas
    
    sdbgAtribPlant.AllowUpdate = True
    
    sdbgPlantillas.Columns("USAR").Locked = True
    sdbgPlantillas.Columns("PROCE").Locked = True
    sdbgPlantillas.Columns("ITEM").Locked = True
    sdbgPlantillas.Columns("GRUPO").Locked = True
    Me.picAmbitoDatosOfe.Enabled = False
    picAdminPub.Enabled = False
    
    
    'Atributos
    sdbgAtribPlant.Columns("INTERNO").Locked = True
    sdbgAtribPlant.Columns("PEDIDO").Locked = True
    sdbgAtribPlant.Columns("COD").Locked = True
    sdbgAtribPlant.Columns("NOMBRE").Locked = True
    sdbgAtribPlant.Columns("TIPO").Locked = True
    sdbgAtribPlant.Columns("LIBRE").Locked = True
    sdbgAtribPlant.Columns("MIN").Locked = True
    sdbgAtribPlant.Columns("MAX").Locked = True
    sdbgAtribPlant.Columns("AMBITO").Locked = True
    sdbgAtribPlant.Columns("SELECCION").Locked = True
    sdbgAtribPlant.Columns("OBL").Locked = True
    sdbgAtribPlant.Columns("INTEGRACION").Locked = True
    sdbgAtribPlant.Columns("VALORBAJO").Locked = True
    sdbgAtribPlant.Columns("OP").Locked = True
    sdbgAtribPlant.Columns("APLIC_PREC").Locked = True
    sdbgAtribPlant.Columns("DEF_PREC").Locked = True
    ''''''''''''''''
    
    
    
    sdbgAtributosEsp.Columns("INTERNO").Locked = True
    sdbgAtributosEsp.Columns("PEDIDO").Locked = True
    sdbgAtributosEsp.Columns("COD").Locked = True
    sdbgAtributosEsp.Columns("DEN").Locked = True
    sdbgAtributosEsp.Columns("TIPO").Locked = True
    sdbgAtributosEsp.Columns("VALOR").Locked = True
    sdbgAtributosEsp.Columns("AMBITO").Locked = True
    sdbgAtributosEsp.Columns("OBLIGATORIO").Locked = True
    sdbgAtributosEsp.Columns("VALIDACION").Locked = True
    
    cmdSortAscEsp.Enabled = False
    cmdSortDesEsp.Enabled = False
    cmdAnyaAtribEsp.Enabled = False
    cmdElimAtribEsp.Enabled = False
    
    cmdAnyadirEsp.Enabled = False
    cmdEliminarEsp.Enabled = False
    cmdModificarEsp.Enabled = False
    
    txtProceEsp.Enabled = False
    
    
    
    m_bModoEdicion = False
    m_bModoEdicionAtr = False
    m_bModificarDefAtrib = False
    m_bModoEdicionAtrEsp = False
    
    
    'Enlazamos las dropdown
        
    sdbddAmbito.AddItem ""
    sdbddSigno.AddItem ""
    sdbddAplicarA.AddItem ""
    sdbgAtribPlant.Columns("AMBITO").DropDownHwnd = sdbddAmbito.hWnd
    sdbgAtribPlant.Columns("OP").DropDownHwnd = sdbddSigno.hWnd
    sdbgAtribPlant.Columns("APLIC_PREC").DropDownHwnd = sdbddAplicarA.hWnd
    sdbddAplicarA.Enabled = False
    sdbddAmbito.Enabled = False
    sdbddSigno.Enabled = False
    
    
    
    sdbddAmbitoEspec.AddItem ""
    sdbddValor.AddItem ""
    sdbddValidacion.AddItem ""
    
    sdbgAtributosEsp.Columns("AMBITO").DropDownHwnd = sdbddAmbitoEspec.hWnd
    sdbgAtributosEsp.Columns("VALIDACION").DropDownHwnd = sdbddValidacion.hWnd
    
    sdbddAmbitoEspec.Enabled = False
    sdbddValor.Enabled = False
    sdbddValidacion.Enabled = False
    
    
    sdbddAmbito.Columns("NOMBRE").Locked = True
    sdbddSigno.Columns("SIGNO").Locked = True
    sdbddAplicarA.Columns("NOMBRE").Locked = True
    
    If g_oPlantillaSeleccionada Is Nothing Then
        sstabPlantillas.TabVisible(0) = True
        sstabPlantillas.TabVisible(1) = False
        sstabPlantillas.TabVisible(2) = False
        sstabPlantillas.TabVisible(3) = False
        sstabPlantillas.TabVisible(4) = False
        cmdRestaurar.Visible = False
        cmdMod.Visible = False
    End If
    
    picNavigateAmbito.Enabled = True
    picNavigateAtrib.Enabled = False
    picNavigateGrupos.Enabled = False

    ' edu PM 98
    ' configurar el formulario en funcion de los permisos de materiales
    
    'ConfigurarMateriales False
    sstabPlantillas.Tab = 0
    If oCEstructura Is Nothing Then
        Set oCEstructura = CrearCAEstructuraMat(oMensajes)
    End If

    oCEstructura.sTituloArbol = sIdiMat
    
    If m_bRestringirSoloMaterialesComprador Then
        oCEstructura.CrearArbolPosiblesGMN oUsuarioSummit.comprador
    End If
End Sub

''' <summary>Inicializa los valores de las variables de par�metros de subasta</summary>
''' <remarks>Llamada desde Form_Load, cmdCancelar_Click</remarks>

Private Sub InicializarValoresSubasta()
    With g_oPlantillaSeleccionada
        m_iSubTipo = .Subtipo
        m_iSubModo = .SubModo
        m_vSubDuracion = .SubDuracion
        m_iSubastaEspera = .MinutosEspera
        m_bSubPublicar = .SubPublicar
        m_bSubNotifEventos = .SubNotifEventos
        m_bSubastaProve = .MuestraProveedor
        m_bSubVerDesdePrimPuja = .SubVerDesdePrimPuja
        m_bSubastaPrecioPuja = .MuestraPrecioPuja
        m_bSubastaPujas = .MuestraDetalle
        m_bSubastaBajMinPuja = .BajadaMinimaPujas
        m_iSubBajMinGanTipo = .SubBajMinGanTipo
        m_vSubBajMinGanProcVal = .SubBajMinGanProcVal
        m_vSubBajMinGanGrupoVal = .SubBajMinGanGrupoVal
        m_vSubBajMinGanItemVal = .SubBajMinGanItemVal
        m_bSubBajMinProve = .SubBajMinProve
        m_iSubBajMinProveTipo = .SubBajMinProveTipo
        m_vSubBajMinProveProcVal = .SubBajMinProveProcVal
        m_vSubBajMinProveGrupoVal = .SubBajMinProveGrupoVal
        m_vSubBajMinProveItemVal = .SubBajMinProveItemVal
        Set m_dcSubTextosFin = .SubTextoFin
    End With
End Sub

Private Sub Form_Resize()

''' * Objetivo: Ajustar el tamanyo y posicion de los controles
''' * Objetivo: al tamanyo del formulario
    
    On Error Resume Next
    If Me.WindowState = 0 Then
        If Me.Width <= 7000 Or Me.Height <= 4500 Then
            Me.Width = 7001
            Me.Height = 4501
        End If
    End If
    If Me.Height <= 360 Then Exit Sub
     
    picPrincipal.Height = Me.Height - 1005
    picPrincipal.Width = (Me.Width - 170) * 0.82
    
    txtDescr.Width = picPrincipal.Width - 200
    lblTitulo.Width = txtDescr.Width
    
    sstabPlantillas.Width = txtDescr.Width
    sstabPlantillas.Height = picPrincipal.Height - 1000
    sdbgPlantillas.Height = sstabPlantillas.Height - picAmbitoDatosOfe.Height - fraAdminPub.Height - picNavigateAmbito.Height - 730
    picAmbitoDatosOfe.Top = sdbgPlantillas.Top + sdbgPlantillas.Height + 45
    cmdConfigSubasta.Top = picAmbitoDatosOfe.Top + fraConfProce(2).Top + 200
    picNavigateAmbito.Top = Me.sstabPlantillas.Height - Me.picNavigateAmbito.Height - 60
    
    
    sdbgPlantillas.Width = sstabPlantillas.Width - 265
    picAmbitoDatosOfe.Width = sdbgPlantillas.Width + 50
    
    
    fraConfProce(2).Width = sdbgPlantillas.Width
    Me.fraSolicitudCompra(0).Width = sdbgPlantillas.Width
    
    If gParametrosGenerales.gbPedidosDirectos Then
        Me.fraConfProce(4).Visible = True
        fraConfProce(3).Width = sdbgPlantillas.Width / 2 - 45
        If gParametrosGenerales.gbUsarPonderacion Then
            fraConfProce(4).Width = sdbgPlantillas.Width / 2
            fraConfProce(4).Left = fraConfProce(3).Width + fraConfProce(3).Left + 45
        Else
            fraConfProce(4).Left = fraConfProce(3).Left
            fraConfProce(4).Width = fraConfProce(2).Width
        End If
    Else
        Me.fraConfProce(4).Visible = False
        fraConfProce(3).Width = sdbgPlantillas.Width
    End If
    
    fraAdminPub.Width = sdbgPlantillas.Width
    
    
    sdbgPlantillas.Columns("USAR").Width = (sdbgPlantillas.Width - 570) * 0.07
    sdbgPlantillas.Columns("DATO").Width = (sdbgPlantillas.Width - 570) * 0.62
    sdbgPlantillas.Columns("PROCE").Width = (sdbgPlantillas.Width - 570) * 0.1
    sdbgPlantillas.Columns("GRUPO").Width = (sdbgPlantillas.Width - 570) * 0.08
    sdbgPlantillas.Columns("BOTONGRUP").Width = (sdbgPlantillas.Width - 570) * 0.06
    sdbgPlantillas.Columns("ITEM").Width = (sdbgPlantillas.Width - 570) * 0.07
    
    
    
    picNavigateAmbito.Width = sdbgPlantillas.Width - 15
    cmdAceptar.Left = picNavigateAmbito.Width / 2 - cmdAceptar.Width - 100
    cmdCancelar.Left = picNavigateAmbito.Width / 2 + 50
            
    sdbgGrupos.Height = sstabPlantillas.Height - txtGrupoDescr.Height - picNavigateGrupos.Height - 680
    sdbgGrupos.Width = sdbgPlantillas.Width
    If sdbgGrupos.Columns("SOBRE").Visible = True Then
        sdbgGrupos.Columns("COD").Width = sdbgGrupos.Width * 22 / 100
        sdbgGrupos.Columns("NOM").Width = sdbgGrupos.Width * 64 / 100
    Else
        sdbgGrupos.Columns("COD").Width = sdbgGrupos.Width * 25 / 100
        sdbgGrupos.Columns("NOM").Width = sdbgGrupos.Width * 68.5 / 100
    End If
    sdbgGrupos.Columns("SOBRE").Width = sdbgGrupos.Width * 10 / 100
    
    txtGrupoDescr.Top = sdbgGrupos.Height + sdbgGrupos.Top + 100
    txtGrupoDescr.Width = sdbgGrupos.Width
    picNavigateGrupos.Top = picNavigateAmbito.Top
    picNavigateGrupos.Width = picNavigateAmbito.Width
    cmdModoEdicion.Left = picNavigateGrupos.Width - cmdModoEdicion.Width - 15
            
    sdbgAtribPlant.Height = sstabPlantillas.Height - 1450
    If m_bModif = True Then
        sdbgAtribPlant.Width = sstabPlantillas.Width - 650
    Else
        sdbgAtribPlant.Width = sstabPlantillas.Width - 250
    End If
    sdbcSeleccion.Width = sdbgAtribPlant.Width
    sdbcSeleccion.Columns(0).Width = sdbcSeleccion.Width / 3
    sdbcSeleccion.Columns(1).Width = 2 * sdbcSeleccion.Width / 3
    picNavigateAtrib.Left = sdbgAtribPlant.Left
    picNavigateAtrib.Width = sdbgAtribPlant.Width
    picNavigateAtrib.Top = picNavigateAmbito.Top
    cmdModoEdicionAtr.Left = picNavigateAtrib.Width - cmdModoEdicionAtr.Width - 15
    
    picCrearPlant.Height = picPrincipal.Height + 495
    picCrearPlant.Width = (Me.Width - 170) * 0.16
    lstvwplantillas.Height = picCrearPlant.Height - 495
    lstvwplantillas.Width = picCrearPlant.Width - 75
    cmdCrearPlant.Top = lstvwplantillas.Height + 95
    picPrincipal.Left = picCrearPlant.Width + 25
    
    If Not g_oPlantillaSeleccionada Is Nothing Then
        If g_oPlantillaSeleccionada.EspecProce Then
            SSSplitterEsp.Panes.Item(0).LockHeight = False
            SSSplitterEsp.Panes.Item(2).LockHeight = False

            SSSplitterEsp.Height = sstabPlantillas.Height - 1000 '* 0.92
            SSSplitterEsp.Width = sstabPlantillas.Width - 250
            
            SSSplitterEsp.Panes.Item(0).Height = SSSplitterEsp.Height / 4
            SSSplitterEsp.Panes.Item(1).Height = SSSplitterEsp.Height / 2
            SSSplitterEsp.Panes.Item(2).Height = SSSplitterEsp.Height / 4
            
            picSplit1.Height = SSSplitterEsp.Panes.Item(0).Height
            picSplit2.Height = SSSplitterEsp.Panes.Item(1).Height
            picSplit3.Height = SSSplitterEsp.Panes.Item(2).Height
            
                
            If picSplit2.Height > 600 Then
                sdbgAtributosEsp.Height = picSplit2.Height - 600 '* 0.8
            Else
                sdbgAtributosEsp.Height = 0
            End If
            sdbgAtributosEsp.Width = picSplit2.Width - 200
            
            sdbgAtributosEsp.Columns("INTERNO").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("PEDIDO").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("COD").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("DEN").Width = sdbgAtributosEsp.Width * 0.25
            sdbgAtributosEsp.Columns("DESCR").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("TIPO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("VALOR").Width = sdbgAtributosEsp.Width * 0.15
            sdbgAtributosEsp.Columns("AMBITO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("OBLIGATORIO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("VALIDACION").Width = sdbgAtributosEsp.Width * 0.1
            
            txtProceEsp.Width = picSplit1.Width - 200
            If picSplit1.Height > 200 Then
                txtProceEsp.Height = picSplit1.Height - 200
            Else
                txtProceEsp.Height = 0
            End If

            
            If picSplit3.Height > 575 Then
                lstvwEsp.Height = picSplit3.Height - 120 - 375 - 80
            Else
                lstvwEsp.Height = 0
            End If
            lstvwEsp.Width = picSplit3.Width - 200
            picNavigateEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 80
            picNavigateEsp.Left = lstvwEsp.Width - 2280
            
            
            cmdModoEdicionAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdModoEdicionAtrEsp.Left = SSSplitterEsp.Width - 1000
            
            cmdDeshacerAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdDeshacerAtrEsp.Left = picSplit2.Left + 90
            
            picBotonesAtributos.Width = SSSplitterEsp.Width
            sdbcGrupoEsp.Width = SSSplitterEsp.Width - 2000
            sdbcGrupoEsp.Columns(0).Width = sdbcGrupoEsp.Width / 3
            sdbcGrupoEsp.Columns(1).Width = 2 * sdbcGrupoEsp.Width / 3
            cmdSortAscEsp.Left = sdbcGrupoEsp.Width + 300
            cmdSortDesEsp.Left = cmdSortAscEsp.Left + 350
            cmdAnyaAtribEsp.Left = cmdSortDesEsp.Left + 400
            If Not m_bModifAtrib Then
                cmdAnyaAtribEsp.Visible = False
            Else
                cmdAnyaAtribEsp.Visible = True
            End If
            cmdElimAtribEsp.Left = cmdAnyaAtribEsp.Left + 350
        Else
        
            SSSplitterEsp.Panes.Item(0).LockHeight = False
            SSSplitterEsp.Panes.Item(2).LockHeight = False

            SSSplitterEsp.Height = sstabPlantillas.Height - 1000 '* 0.92
            SSSplitterEsp.Width = sstabPlantillas.Width - 250
            
            SSSplitterEsp.Panes.Item(0).Height = 0
            SSSplitterEsp.Panes.Item(1).Height = SSSplitterEsp.Height
            SSSplitterEsp.Panes.Item(2).Height = 0
            
            picSplit1.Height = SSSplitterEsp.Panes.Item(0).Height
            picSplit2.Height = SSSplitterEsp.Panes.Item(1).Height
            picSplit3.Height = SSSplitterEsp.Panes.Item(2).Height
            
                
            If picSplit2.Height > 600 Then
                sdbgAtributosEsp.Height = picSplit2.Height - 600 '* 0.8
            Else
                sdbgAtributosEsp.Height = 0
            End If
            sdbgAtributosEsp.Width = picSplit2.Width - 200
            
            sdbgAtributosEsp.Columns("INTERNO").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("PEDIDO").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("COD").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("DEN").Width = sdbgAtributosEsp.Width * 0.25
            sdbgAtributosEsp.Columns("DESCR").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("TIPO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("VALOR").Width = sdbgAtributosEsp.Width * 0.15
            sdbgAtributosEsp.Columns("AMBITO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("OBLIGATORIO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("VALIDACION").Width = sdbgAtributosEsp.Width * 0.1
            
            txtProceEsp.Width = picSplit1.Width - 200
            If picSplit1.Height > 200 Then
                txtProceEsp.Height = picSplit1.Height - 200
            Else
                txtProceEsp.Height = 0
            End If

            
            If picSplit3.Height > 575 Then
                lstvwEsp.Height = picSplit3.Height - 120 - 375 - 80
            Else
                lstvwEsp.Height = 0
            End If
            lstvwEsp.Width = picSplit3.Width - 200
            picNavigateEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 80
            picNavigateEsp.Left = lstvwEsp.Width - 2280
            
            
            cmdModoEdicionAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdModoEdicionAtrEsp.Left = SSSplitterEsp.Width - 1000
            
            cmdDeshacerAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdDeshacerAtrEsp.Left = picSplit2.Left + 90
            
            picBotonesAtributos.Width = SSSplitterEsp.Width
            sdbcGrupoEsp.Width = SSSplitterEsp.Width - 2000
            sdbcGrupoEsp.Columns(0).Width = sdbcGrupoEsp.Width / 3
            sdbcGrupoEsp.Columns(1).Width = 2 * sdbcGrupoEsp.Width / 3
            cmdSortAscEsp.Left = sdbcGrupoEsp.Width + 300
            cmdSortDesEsp.Left = cmdSortAscEsp.Left + 350
            cmdAnyaAtribEsp.Left = cmdSortDesEsp.Left + 400
            If Not m_bModifAtrib Then
                cmdAnyaAtribEsp.Visible = False
            Else
                cmdAnyaAtribEsp.Visible = True
            End If
            
            cmdElimAtribEsp.Left = cmdAnyaAtribEsp.Left + 350
            SSSplitterEsp.Panes.Item(0).LockHeight = True
            SSSplitterEsp.Panes.Item(2).LockHeight = True

            SSSplitterEsp.Panes.Item(0).LockHeight = True
            SSSplitterEsp.Panes.Item(2).LockHeight = True

        End If
        
    Else
        SSSplitterEsp.Panes.Item(0).LockHeight = False
        SSSplitterEsp.Panes.Item(2).LockHeight = False

        SSSplitterEsp.Height = sstabPlantillas.Height - 1000 '* 0.92
        SSSplitterEsp.Width = sstabPlantillas.Width - 250
        
        SSSplitterEsp.Panes.Item(0).Height = SSSplitterEsp.Height / 4
        SSSplitterEsp.Panes.Item(1).Height = SSSplitterEsp.Height / 2
        SSSplitterEsp.Panes.Item(2).Height = SSSplitterEsp.Height / 4
        
        picSplit1.Height = SSSplitterEsp.Panes.Item(0).Height
        picSplit2.Height = SSSplitterEsp.Panes.Item(1).Height
        picSplit3.Height = SSSplitterEsp.Panes.Item(2).Height
        
            
        If picSplit2.Height > 600 Then
            sdbgAtributosEsp.Height = picSplit2.Height - 600 '* 0.8
        Else
            sdbgAtributosEsp.Height = 0
        End If
        sdbgAtributosEsp.Width = picSplit2.Width - 200
        
        sdbgAtributosEsp.Columns("INTERNO").Width = sdbgAtributosEsp.Width * 0.05
        sdbgAtributosEsp.Columns("PEDIDO").Width = sdbgAtributosEsp.Width * 0.05
        sdbgAtributosEsp.Columns("COD").Width = sdbgAtributosEsp.Width * 0.1
        sdbgAtributosEsp.Columns("DEN").Width = sdbgAtributosEsp.Width * 0.25
        sdbgAtributosEsp.Columns("DESCR").Width = sdbgAtributosEsp.Width * 0.05
        sdbgAtributosEsp.Columns("TIPO").Width = sdbgAtributosEsp.Width * 0.1
        sdbgAtributosEsp.Columns("VALOR").Width = sdbgAtributosEsp.Width * 0.15
        sdbgAtributosEsp.Columns("AMBITO").Width = sdbgAtributosEsp.Width * 0.1
        sdbgAtributosEsp.Columns("OBLIGATORIO").Width = sdbgAtributosEsp.Width * 0.1
        sdbgAtributosEsp.Columns("VALIDACION").Width = sdbgAtributosEsp.Width * 0.1
        
        txtProceEsp.Width = picSplit1.Width - 200
        If picSplit1.Height > 200 Then
            txtProceEsp.Height = picSplit1.Height - 200
        Else
            txtProceEsp.Height = 0
        End If

    
        If picSplit3.Height > 575 Then
            lstvwEsp.Height = picSplit3.Height - 120 - 375 - 80
        Else
            lstvwEsp.Height = 0
        End If
        lstvwEsp.Width = picSplit3.Width - 200
        picNavigateEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 80
        picNavigateEsp.Left = lstvwEsp.Width - 2280
        
        
        cmdModoEdicionAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
        
        cmdDeshacerAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
        cmdDeshacerAtrEsp.Left = picSplit2.Left + 90
        
        
    End If
    
    cmdModificarPlant.Top = cmdCrearPlant.Top + 120
    cmdEliminarPlant.Top = cmdModificarPlant.Top
    cmdConfVistas.Top = cmdModificarPlant.Top
    cmdEliminarVistas.Top = cmdModificarPlant.Top
    
    tvwEstrMat.Height = sstabPlantillas.Height - chkSoloEstosMateriales.Height - PicNavigateMateriales.Height - 1200
    tvwEstrMat.Width = sstabPlantillas.Width - 600
    tvwEstrMatMod.Top = tvwEstrMat.Top
    tvwEstrMatMod.Left = tvwEstrMat.Left
    tvwEstrMatMod.Height = tvwEstrMat.Height
    tvwEstrMatMod.Width = tvwEstrMat.Width
    
    PicNavigateMateriales.Left = tvwEstrMat.Left
    PicNavigateMateriales.Width = tvwEstrMat.Width
    PicNavigateMateriales.Top = picNavigateAmbito.Top
    
End Sub


''' <summary>
'''
'''Se carga la grid de plantillas, en la columna INDICE se carga el �ndice del dato
'''para diferenciar los datos entre s�. (0-DEST,1-PAG,2-FECSUM,...) igual que en la
'''apertura. No sirve con leer la row porque los presupuestos pueden aparecer o no
'''dependiendo de los par�metros generales
'''
''' </summary>
''' <returns></returns>
''' <param name="bNoLeerBD">si es false leer los datos de BD, si no se carga la grid con las propiedades de la plantilla </param>
''' <param name="bCancelar">si se llama al m�todo desde "Cancelar" se restauran los valores de los grupos, que pueden haber cambiado. </param>
''' <remarks>Llamada desde: SeleccionarPrimeraPlantilla ,cmdCancelar_Click,cmdEliminar_Click,cmdRestaurar_Click,Form_Load,lstvwplantillas_ItemClick,EliminarPlantilla,SeleccionarPrimeraPlantilla </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Public Sub CargarGridPlantillas(Optional ByVal bNoLeerDeBD As Boolean, Optional ByVal bCancelar As Boolean)
'*********************************************************************************************************************
'Se carga la grid de plantillas, en la columna INDICE se carga el �ndice del dato
'para diferenciar los datos entre s�. (0-DEST,1-PAG,2-FECSUM,...) igual que en la
'apertura. No sirve con leer la row porque los presupuestos pueden aparecer o no
'dependiendo de los par�metros generales
'bNoLeerDeBD, si es false leer los datos de BD, si no se carga la grid con las propiedades de la plantilla
'bCancelar, si se llama al m�todo desde "Cancelar" se restauran los valores de los grupos, que pueden haber cambiado.
'*********************************************************************************************************************

Dim oPlantilla As CPlantilla
Dim oGrupo As CGrupo
Dim bUsar As Boolean
Dim bGrupo As Boolean
Dim bProce As Boolean
Dim bItem As Boolean
Dim sPuntos As String
Dim bprimera As Boolean
Dim sUsar As String
Dim sGrupo As String
Dim sProce As String
Dim sItem As String
           
    If Not bNoLeerDeBD Then
        Screen.MousePointer = vbHourglass
        
        If m_bRestringirSoloMaterialesComprador = False Then
            g_oPlantillas.CargarTodasLasPlantillas 0, False
        Else
            g_oPlantillas.CargarTodasLasPlantillas 0, False, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod
        End If
        If g_lNumID = 0 Then 'Carga todas las plantillas
            For Each oPlantilla In g_oPlantillas

                If oPlantilla.VistasConf = True Then
                    lstvwplantillas.ListItems.Add , "ID" & CStr(oPlantilla.Id), CStr(oPlantilla.nombre), "PlantillaConVista", "PlantillaConVista"
                Else
                    lstvwplantillas.ListItems.Add , "ID" & CStr(oPlantilla.Id), CStr(oPlantilla.nombre), "Plantilla", "Plantilla"
                End If
                If m_bCargarPrimero = True Then 'Seleccionar la primera plantilla
                    g_lNumID = Mid(lstvwplantillas.selectedItem.key, 3)
                    lstvwplantillas.selectedItem.Selected = True
                    m_bCargarPrimero = False
                End If
            Next
        End If
        Screen.MousePointer = vbNormal
    End If
    
    Set g_oPlantillaSeleccionada = g_oPlantillas.Item(CStr(g_lNumID))
    If g_oPlantillaSeleccionada Is Nothing Then
        cmdModificarPlant.Visible = False
        cmdEliminarPlant.Visible = False
        cmdEliminarVistas.Visible = False
        cmdConfVistas.Visible = False
        Exit Sub
    Else
        ' habilitar botones de modificacion en funcion de los permisos
        cmdModificarPlant.Visible = m_bModif
        cmdEliminarPlant.Visible = m_bModif
        cmdEliminarVistas.Visible = m_bModif
        cmdConfVistas.Visible = True
        
        InicializarValoresSubasta
    End If
    
    If g_Accion = ACCPlantillasAnya Then ' si la plantilla es nueva la a�adimos al lstvw
        lstvwplantillas.ListItems.Add , "ID" & CStr(g_oPlantillaSeleccionada.Id), CStr(g_oPlantillaSeleccionada.nombre), "Plantilla", "Plantilla"
        lstvwplantillas.ListItems.Item(lstvwplantillas.ListItems.Count).Selected = True
    End If
    
    
    With g_oPlantillaSeleccionada

            If gParametrosGenerales.gbSubasta Then
                If .ModoSubasta Then
                    chkSubasta.Value = vbChecked
                Else
                    chkSubasta.Value = vbUnchecked
                End If
            Else
                'Mover controles
                picDatosConf(2).Visible = False             'Subasta
                chkSubasta.Visible = False
                
                picDatosConf(4).Top = picDatosConf(2).Top   'Alter. precios
                picDatosConf(5).Top = picDatosConf(4).Top + picDatosConf(4).Height + 55 'Datos ofertas
                picDatosConf(3).Top = picDatosConf(5).Top + picDatosConf(5).Height + 15
                picDatosConf(0).Top = picDatosConf(3).Top + picDatosConf(3).Height + 15
            End If
                                
            If .PedirAlterPrecios Then
                chkPrecios.Value = vbChecked
            Else
                chkPrecios.Value = vbUnchecked
            End If
            
            If .NoPublicarFinSum Then
                chkNoPublicarFinSum.Value = vbChecked
            Else
                chkNoPublicarFinSum.Value = vbUnchecked
            End If
            
            
            If .SolicitarCantMax Then
                chkSuministros.Value = vbChecked
            Else
                chkSuministros.Value = vbUnchecked
            End If
            If .AdjuntarAOfertas Then
                chkArchivProce.Value = vbChecked
            Else
                chkArchivProce.Value = vbUnchecked
            End If
            If .CambiarMonOferta Then
                chkOferMonProce.Value = vbUnchecked
            Else
                chkOferMonProce.Value = vbChecked
            End If
            
            If .NoPublicarFinSum Then
                chkNoPublicarFinSum.Value = vbChecked
            Else
                chkNoPublicarFinSum.Value = vbUnchecked
            End If
                
            If .ObligatorioAtribGS Then
                chkOblAtrGS.Enabled = False
                chkOblAtrGS.Value = vbChecked
                chkOblAtrGS.Enabled = True
            Else
                chkOblAtrGS.Enabled = False
                chkOblAtrGS.Value = vbUnchecked
                chkOblAtrGS.Enabled = True
            End If
                
      
        If gParametrosGenerales.gbUsarPonderacion Then
            If .UsarPonderacion Then
                chkPondAtrib.Value = vbChecked
            Else
                chkPondAtrib.Value = vbUnchecked
            End If
            fraConfProce(3).Visible = True
        Else
            chkPondAtrib.Value = vbUnchecked
            .UsarPonderacion = False
            fraConfProce(3).Visible = False
        End If
        
        If .AdjuntarAItems Then
            chkArchivItem.Value = vbChecked
        Else
            chkArchivItem.Value = vbUnchecked
        End If
        If .AdjuntarAGrupos Then
            chkArchivGrupo.Value = vbChecked
        Else
            chkArchivGrupo.Value = vbUnchecked
        End If
        If .DefUnSoloPedido Then
            Me.chkUnSoloPedido.Value = vbChecked
        Else
            Me.chkUnSoloPedido.Value = vbUnchecked
        End If
        
        If .ParaPedido Then
            Me.chkParaPedido.Value = vbChecked
        Else
            Me.chkParaPedido.Value = vbUnchecked
            chkParaPedidoDef.Enabled = False
        End If
        
        If .ParaPedidoDef Then
            Me.chkParaPedidoDef.Value = vbChecked
        Else
            Me.chkParaPedidoDef.Value = vbUnchecked
        End If
    
    End With
    
    sstabPlantillas.TabVisible(1) = True
    sstabPlantillas.TabVisible(2) = True
    If Not m_bAccionChkSol Then
        cmdRestaurar.Visible = True
    End If
    If m_bModif And Not m_bAccionChkSol Then
        cmdMod.Visible = True
    End If
    
    txtDescr.Text = NullToStr(g_oPlantillaSeleccionada.Descripcion)
    lblTitulo.caption = g_oPlantillaSeleccionada.Fecha & " - " & g_oPlantillaSeleccionada.nombre
    lstvwplantillas.selectedItem.Tag = g_oPlantillaSeleccionada.Id
    
'***************************************************
    If ADMIN_OBLIGATORIO Then
        chkAdminPub.Value = vbChecked
    Else
        If basParametros.gParametrosGenerales.gbAdminPublica = True Then
            chkAdminPub.Value = BooleanToSQLBinary(g_oPlantillaSeleccionada.AdminPub)
            If chkAdminPub.Value = vbChecked Then
                sdbgGrupos.Columns("SOBRE").Visible = True
                sdbgGrupos.Columns("COD").Width = sdbgGrupos.Width * 22 / 100
                sdbgGrupos.Columns("NOM").Width = sdbgGrupos.Width * 64 / 100
            Else
                sdbgGrupos.Columns("SOBRE").Visible = False
                sdbgGrupos.Columns("COD").Width = sdbgGrupos.Width * 25 / 100
                sdbgGrupos.Columns("NOM").Width = sdbgGrupos.Width * 68.5 / 100
            End If
        Else
            chkAdminPub.Value = vbUnchecked
        End If
    End If
    
    ConfigurarAmbitoDatos chkAdminPub.Value = vbChecked
    
    sdbgPlantillas.RemoveAll
    
    'DESTINO
    If chkAdminPub.Value = vbUnchecked Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sPuntos = ""
        If g_oPlantillaSeleccionada.Destino = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.Destino = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.Destino = EnItem Then
            bItem = True
            sItem = "-1"
        End If
        If bCancelar Then
            If bProce Or bItem Then
                For Each oGrupo In m_oGrupos
                    oGrupo.DefDestino = False
                Next
            End If
        End If
        sdbgPlantillas.AddItem "-1" & Chr(m_lSeparador) & "(*) " & m_asDatos(0) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "0"
    
        'FORMA DE PAGO
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sPuntos = ""
        If g_oPlantillaSeleccionada.FormaPago = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.FormaPago = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.FormaPago = EnItem Then
            bItem = True
            sItem = "-1"
        End If
        If bCancelar Then
            If bProce Or bItem Then
                For Each oGrupo In m_oGrupos
                    oGrupo.DefFormaPago = False
                Next
            End If
        End If
        sdbgPlantillas.AddItem "-1" & Chr(m_lSeparador) & "(*) " & m_asDatos(1) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "1"
    End If
    
    'FECHAS DE SUMINISTRO
    bProce = False
    sProce = 0
    bGrupo = False
    sGrupo = 0
    bItem = False
    sItem = 0
    bUsar = True
    sPuntos = ""
    If g_oPlantillaSeleccionada.FechaSuministro = EnProceso Then
        bProce = True
        sProce = "-1"
    End If
    If g_oPlantillaSeleccionada.FechaSuministro = EnGrupo Then
        bGrupo = True
        sGrupo = "-1"
        sPuntos = "..."
    End If
    If g_oPlantillaSeleccionada.FechaSuministro = EnItem Then
        bItem = True
        sItem = "-1"
    End If
    If bCancelar Then
        If bProce Or bItem Then
            For Each oGrupo In m_oGrupos
                oGrupo.DefFechasSum = False
            Next
        End If
    End If
    sdbgPlantillas.AddItem "-1" & Chr(m_lSeparador) & "(*) " & m_asDatos(2) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "2"
    
    'solicitud de compra
    If chkAdminPub.Value = vbUnchecked Then
        If m_bMostrarSolicitud = True Then
            bProce = False
            sProce = 0
            bGrupo = False
            sGrupo = 0
            bItem = False
            sItem = 0
            bUsar = True
            sUsar = "-1"
            sPuntos = ""
            If g_oPlantillaSeleccionada.Solicitud = NoDefinido Then
                bUsar = False
                sUsar = 0
            End If
            If g_oPlantillaSeleccionada.Solicitud = EnProceso Then
                bProce = True
                sProce = "-1"
            End If
            If g_oPlantillaSeleccionada.Solicitud = EnGrupo Then
                bGrupo = True
                sGrupo = "-1"
                sPuntos = "..."
            End If
            If g_oPlantillaSeleccionada.Solicitud = EnItem Then
                bItem = True
                sItem = "-1"
            End If
            If bCancelar Then
                If Not bUsar Or bProce Or bItem Then
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefSolicitud = False
                    Next
                End If
            End If
            'tarea 3376
            'si el check obligar a indicar solicitud compra activado
            sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & IIf(g_oPlantillaSeleccionada.OblSolCompras = 1 Or g_oPlantillaSeleccionada.OblSolCompras = 2, "(*) ", "") & m_asDatos(11) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "10"
            MostrarOcultarFraSolCompra (sUsar)
        End If

        'PROVEEDOR ACTUAL
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.ProveedorAct = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.ProveedorAct = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.ProveedorAct = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.ProveedorAct = EnItem Then
            bItem = True
            sItem = "-1"
        End If
        If bCancelar Then
            If Not bUsar Or bProce Or bItem Then
                For Each oGrupo In m_oGrupos
                    oGrupo.DefProveActual = False
                Next
            End If
        End If
        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(3) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "3"
    End If
    'Tarea 3425 - GFA
    Select Case g_oPlantillaSeleccionada.OblSolCompras
        Case 1
            Me.chkOblSolicitud.Value = vbChecked
        Case 2
            Me.chkOblSolicitudAdj.Value = vbChecked
        Case Else
            Me.chkOblSolicitud.Value = vbUnchecked
            Me.chkOblSolicitudAdj.Value = vbUnchecked
    End Select
        
    
    'ESPECIFICACIONES
    bUsar = False
    sUsar = 0
    sPuntos = ""
    If g_oPlantillaSeleccionada.EspecGrupo Then sPuntos = "..."
    If g_oPlantillaSeleccionada.EspecProce Or g_oPlantillaSeleccionada.EspecGrupo Or g_oPlantillaSeleccionada.EspecItem Then
        bUsar = True
        sUsar = "-1"
        
        If g_oPlantillaSeleccionada.EspecProce Then
            SSSplitterEsp.Panes.Item(0).LockHeight = False
            SSSplitterEsp.Panes.Item(2).LockHeight = False

            SSSplitterEsp.Height = sstabPlantillas.Height - 1000 '* 0.92
            SSSplitterEsp.Width = sstabPlantillas.Width - 250
            
            SSSplitterEsp.Panes.Item(0).Height = SSSplitterEsp.Height / 4
            SSSplitterEsp.Panes.Item(1).Height = SSSplitterEsp.Height / 2
            SSSplitterEsp.Panes.Item(2).Height = SSSplitterEsp.Height / 4
            
            picSplit1.Height = SSSplitterEsp.Panes.Item(0).Height
            picSplit2.Height = SSSplitterEsp.Panes.Item(1).Height
            picSplit3.Height = SSSplitterEsp.Panes.Item(2).Height
            
                
            If picSplit2.Height > 600 Then
                sdbgAtributosEsp.Height = picSplit2.Height - 600 '* 0.8
            Else
                sdbgAtributosEsp.Height = 0
            End If
            sdbgAtributosEsp.Width = picSplit2.Width - 200
            
            sdbgAtributosEsp.Columns("INTERNO").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("PEDIDO").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("COD").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("DEN").Width = sdbgAtributosEsp.Width * 0.25
            sdbgAtributosEsp.Columns("DESCR").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("TIPO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("VALOR").Width = sdbgAtributosEsp.Width * 0.15
            sdbgAtributosEsp.Columns("AMBITO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("OBLIGATORIO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("VALIDACION").Width = sdbgAtributosEsp.Width * 0.1
            
            txtProceEsp.Width = picSplit1.Width - 200
            If picSplit1.Height > 200 Then
                txtProceEsp.Height = picSplit1.Height - 200
            Else
                txtProceEsp.Height = 0
            End If
            
            If picSplit3.Height > 575 Then
                lstvwEsp.Height = picSplit3.Height - 120 - 375 - 80
            Else
                lstvwEsp.Height = 0
            End If
            lstvwEsp.Width = picSplit3.Width - 200
            picNavigateEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 80
            picNavigateEsp.Left = lstvwEsp.Width - 2280
            
            
            cmdModoEdicionAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdDeshacerAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdDeshacerAtrEsp.Left = picSplit2.Left + 90
            
        Else
        
            SSSplitterEsp.Panes.Item(0).LockHeight = False
            SSSplitterEsp.Panes.Item(2).LockHeight = False
            
            SSSplitterEsp.Height = sstabPlantillas.Height - 1000 '* 0.92
            SSSplitterEsp.Width = sstabPlantillas.Width - 250
            
            SSSplitterEsp.Panes.Item(0).Height = 0
            SSSplitterEsp.Panes.Item(1).Height = SSSplitterEsp.Height
            SSSplitterEsp.Panes.Item(2).Height = 0
            
            picSplit1.Height = SSSplitterEsp.Panes.Item(0).Height
            picSplit2.Height = SSSplitterEsp.Panes.Item(1).Height
            picSplit3.Height = SSSplitterEsp.Panes.Item(2).Height
            
                
            If picSplit2.Height > 600 Then
                sdbgAtributosEsp.Height = picSplit2.Height - 600 '* 0.8
            Else
                sdbgAtributosEsp.Height = 0
            End If
            sdbgAtributosEsp.Width = picSplit2.Width - 200
            
            sdbgAtributosEsp.Columns("INTERNO").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("PEDIDO").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("COD").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("DEN").Width = sdbgAtributosEsp.Width * 0.25
            sdbgAtributosEsp.Columns("DESCR").Width = sdbgAtributosEsp.Width * 0.05
            sdbgAtributosEsp.Columns("TIPO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("VALOR").Width = sdbgAtributosEsp.Width * 0.15
            sdbgAtributosEsp.Columns("AMBITO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("OBLIGATORIO").Width = sdbgAtributosEsp.Width * 0.1
            sdbgAtributosEsp.Columns("VALIDACION").Width = sdbgAtributosEsp.Width * 0.1
            
            txtProceEsp.Width = sstabPlantillas.Width - 400
            txtProceEsp.Height = 0
            
            lstvwEsp.Height = 0
            lstvwEsp.Width = sstabPlantillas.Width - 600
            picNavigateEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 80
            picNavigateEsp.Left = lstvwEsp.Width - 2280
            
            
            cmdModoEdicionAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdDeshacerAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
            cmdDeshacerAtrEsp.Left = picSplit2.Left + 90
            
            SSSplitterEsp.Panes.Item(0).LockHeight = True
            SSSplitterEsp.Panes.Item(2).LockHeight = True

        
        End If
        
        sstabPlantillas.TabVisible(3) = True

    Else
        bUsar = True
        sUsar = "-1"
        sstabPlantillas.TabVisible(3) = False
    End If
    If sUsar = 0 Then
         sstabPlantillas.TabVisible(3) = False
    End If
    
    sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(4) & Chr(m_lSeparador) & IIf(g_oPlantillaSeleccionada.EspecProce, "-1", 0) & Chr(m_lSeparador) & IIf(g_oPlantillaSeleccionada.EspecGrupo, "-1", "0") & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & IIf(g_oPlantillaSeleccionada.EspecItem, "-1", "0") & Chr(m_lSeparador) & "4"
    
    'DISTRIBUCION UO
    bProce = False
    sProce = 0
    bGrupo = False
    sGrupo = 0
    bItem = False
    sItem = 0
    bUsar = True
    sUsar = "-1"
    sPuntos = ""
    If g_oPlantillaSeleccionada.DistribUON = NoDefinido Then
        bUsar = False
        sUsar = 0
    End If
    If g_oPlantillaSeleccionada.DistribUON = EnProceso Then
        bProce = True
        sProce = "-1"
    End If
    If g_oPlantillaSeleccionada.DistribUON = EnGrupo Then
        bGrupo = True
        sGrupo = "-1"
        sPuntos = "..."
    End If
    If g_oPlantillaSeleccionada.DistribUON = EnItem Then
        bItem = True
        sItem = "-1"
    End If
    If bCancelar Then
        If Not bUsar Or bProce Or bItem Then
            For Each oGrupo In m_oGrupos
                oGrupo.DefDistribUON = False
            Next
        End If
    End If
    sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & "(*) " & m_asDatos(5) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "5"
    
    'PRESANUALES TIPO1
    If gParametrosGenerales.gbUsarPres1 Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = EnItem Then
            bItem = True
            sItem = "-1"
        End If
        If bCancelar Then
            If Not bUsar Or bProce Or bItem Then
                For Each oGrupo In m_oGrupos
                    oGrupo.DefPresAnualTipo1 = False
                Next
            End If
        End If
        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & IIf(gParametrosGenerales.gbOBLPP = True, "(*) ", "") & m_asDatos(6) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "6"
    End If
    
    'PRESANUALES TIPO2
    If gParametrosGenerales.gbUsarPres2 Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = EnItem Then
            bItem = True
            sItem = "-1"
        End If
        If bCancelar Then
            If Not bUsar Or bProce Or bItem Then
                For Each oGrupo In m_oGrupos
                    oGrupo.DefPresAnualTipo2 = False
                Next
            End If
        End If
        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & IIf(gParametrosGenerales.gbOBLPC = True, "(*) ", "") & m_asDatos(7) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "7"
    End If
    
    'PRES TIPO1
    If gParametrosGenerales.gbUsarPres3 Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.PresTipo1 = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.PresTipo1 = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.PresTipo1 = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.PresTipo1 = EnItem Then
            bItem = True
            sItem = "-1"
        End If
        If bCancelar Then
            If Not bUsar Or bProce Or bItem Then
                For Each oGrupo In m_oGrupos
                    oGrupo.DefPresTipo1 = False
                Next
            End If
        End If
        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & IIf(gParametrosGenerales.gbOBLPres3 = True, "(*) ", "") & m_asDatos(8) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "8"
    End If
    
    'PRES TIPO2
    If gParametrosGenerales.gbUsarPres4 Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.PresTipo2 = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.PresTipo2 = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.PresTipo2 = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.PresTipo2 = EnItem Then
            bItem = True
            sItem = "-1"
        End If
        If bCancelar Then
            If Not bUsar Or bProce Or bItem Then
                For Each oGrupo In m_oGrupos
                    oGrupo.DefPresTipo2 = False
                Next
            End If
        End If
        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & IIf(gParametrosGenerales.gbOBLPres4 = True, "(*) ", "") & m_asDatos(9) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "9"
    End If
    
    bprimera = True
    
    sdbgGrupos.RemoveAll
    txtGrupoDescr.Text = ""
   
    If g_oPlantillaSeleccionada.Grupos Is Nothing Then
        Set g_oPlantillaSeleccionada.Grupos = oFSGSRaiz.Generar_CGrupos
    End If
    
    For Each oGrupo In g_oPlantillaSeleccionada.Grupos
        sdbgGrupos.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den & Chr(m_lSeparador) & NullToStr(oGrupo.Descripcion) & Chr(m_lSeparador) & NullToStr(oGrupo.Sobre)
        If bprimera Then
            txtGrupoDescr.Text = NullToStr(oGrupo.Descripcion)
            bprimera = False
        End If
    Next
    
    If Not m_bModoEdicionAtr Then
        CargarComboGrupos
        CargarGridAtributos
        CargarEspecificaciones
        CargarGridEspecificaciones
    End If
    
    'edu T98.
    'inicializar propiedad RestriccionMaterial de la plantilla de forma que no se acceda a la BDD cada vez que se necesite.
    g_oPlantillaSeleccionada.ExisteRestriccionMaterial
    ConfigurarMateriales False
    
    Me.Refresh
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Carga de cadenas para la pagina
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarRecursos()
Dim Adores As Ador.Recordset

    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_PLANTILLAS_PROCE, basPublic.gParametrosInstalacion.gIdioma)
        
        If Not Adores Is Nothing Then
        
            frmPlantillasProce.caption = Adores(0).Value
            Adores.MoveNext
            cmdCrearPlant.caption = Adores(0).Value
            Adores.MoveNext
            cmdMod.caption = Adores(0).Value
            cmdModificarPlant.caption = Adores(0).Value
            cmdModificarMateriales.caption = Adores(0).Value
            Adores.MoveNext
            cmdRestaurar.caption = Adores(0).Value
            cmdRestGrupo.caption = Adores(0).Value
            'cmdRestaurarPlant.Caption = Adores(0).Value
            cmdRestAtr.caption = Adores(0).Value
            Adores.MoveNext
            cmdAceptar.caption = Adores(0).Value
            cmdAceptarMateriales.caption = Adores(0).Value
            Adores.MoveNext
            cmdCancelar.caption = Adores(0).Value
            cmdCancelarMateriales.caption = Adores(0).Value
            Adores.MoveNext
            sstabPlantillas.TabCaption(0) = Adores(0).Value
            Adores.MoveNext
            sstabPlantillas.TabCaption(1) = Adores(0).Value
            m_sFrmModCod = Adores(0).Value
            Adores.MoveNext
            sstabPlantillas.TabCaption(2) = Adores(0).Value
            Adores.MoveNext
            sdbgPlantillas.Columns(0).caption = Adores(0).Value
            Adores.MoveNext
            sdbgPlantillas.Columns(1).caption = Adores(0).Value
            Adores.MoveNext
            sdbgPlantillas.Columns(2).caption = Adores(0).Value
            m_asAtrib(4) = Adores(0).Value
            Adores.MoveNext
            sdbgPlantillas.Columns(3).caption = Adores(0).Value
            m_asAtrib(5) = Adores(0).Value
            Adores.MoveNext
            sdbgPlantillas.Columns(5).caption = Adores(0).Value
            m_asAtrib(6) = Adores(0).Value
            Adores.MoveNext
            m_asDatos(0) = Adores(0).Value
            Adores.MoveNext
            m_asDatos(1) = Adores(0).Value
            Adores.MoveNext
            m_asDatos(2) = Adores(0).Value
            Adores.MoveNext
            m_asDatos(3) = Adores(0).Value
            Adores.MoveNext
            m_asDatos(4) = Adores(0).Value
            sstabPlantillas.TabCaption(3) = Adores(0).Value 'Especificaciones
            Adores.MoveNext
            m_asDatos(5) = Adores(0).Value
            Adores.MoveNext
            m_asDatos(6) = gParametrosGenerales.gsPlurPres1
            Adores.MoveNext
            m_asDatos(7) = gParametrosGenerales.gsPlurPres2
            Adores.MoveNext
            m_asDatos(8) = gParametrosGenerales.gsPlurPres3
            Adores.MoveNext
            m_asDatos(9) = gParametrosGenerales.gsPlurPres4
            Adores.MoveNext
            m_sModif = Adores(0).Value
            Adores.MoveNext
            m_sDetalle = Adores(0).Value
            Adores.MoveNext
            m_sA�adir = Adores(0).Value
            Adores.MoveNext
            m_sElGrupo = Adores(0).Value
            Adores.MoveNext
            m_sMensajeMultGrupos = Adores(0).Value
            Adores.MoveNext
            sdbgGrupos.Columns("COD").caption = Adores(0).Value
            sdbgAtributosEsp.Columns("COD").caption = Adores(0).Value
            sdbgAtribPlant.Columns("COD").caption = Adores(0).Value
            m_sCodigo = Adores(0).Value
            cmdCodigo.caption = Adores(0).Value  'C�digo
            Adores.MoveNext
            sdbgGrupos.Columns("NOM").caption = Adores(0).Value
            Adores.MoveNext
            cmdAnyadir.caption = Adores(0).Value
            cmdAnyadirAtr.caption = Adores(0).Value
            Adores.MoveNext
            cmdDeshacer.caption = Adores(0).Value
            cmdDeshacerAtr.caption = Adores(0).Value
            cmdDeshacerAtrEsp.caption = Adores(0).Value
            Adores.MoveNext
            m_sEdicion = Adores(0).Value
            cmdModoEdicion.caption = Adores(0).Value
            cmdModoEdicionAtr.caption = Adores(0).Value
            cmdModoEdicionAtrEsp.caption = Adores(0).Value
            Adores.MoveNext
            m_sConsulta = Adores(0).Value
            Adores.MoveNext
            cmdEliminar.caption = Adores(0).Value
            cmdEliAtr.caption = Adores(0).Value
            Adores.MoveNext
            m_sPlantilla = Adores(0).Value
            Adores.MoveNext
            m_sIdiOrdenando = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(0) = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(1) = Adores(0).Value
            lstvwEsp.ColumnHeaders(5).Text = Adores(0).Value  'Fecha
            Adores.MoveNext
            m_asAtrib(2) = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(3) = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(10) = Adores(0).Value
            m_sMensajeProce = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(11) = Adores(0).Value
            m_sMensajeGrupo = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(12) = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(7) = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(8) = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(9) = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("INTERNO").caption = Adores(0).Value
            sdbgAtributosEsp.Columns("INTERNO").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("NOMBRE").caption = Adores(0).Value
            sdbgAtributosEsp.Columns("DEN").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("TIPO").caption = Adores(0).Value
            sdbgAtributosEsp.Columns("TIPO").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Groups(0).caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("LIBRE").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("MAX").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("MIN").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("SELECCION").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("AMBITO").caption = Adores(0).Value
            sdbgAtributosEsp.Columns("AMBITO").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("OBL").caption = Adores(0).Value
            sdbgAtributosEsp.Columns("OBLIGATORIO").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("VALORBAJO").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Groups(2).caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Groups(1).caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Groups(3).caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("OP").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("APLIC_PREC").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("DEF_PREC").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtribPlant.Columns("POND").caption = ""
            sdbgAtribPlant.Groups(4).caption = Adores(0).Value
            Adores.MoveNext
            'EXISTENTE = Adores(0).Value
            Adores.MoveNext
            'NUEVO
            Adores.MoveNext
            m_asMensajes(1) = Adores(0).Value
            Adores.MoveNext
            m_asMensajes(0) = Adores(0).Value
            Adores.MoveNext
            cmdOrden(0).caption = Adores(0).Value
            Adores.MoveNext
            m_asDatos(10) = Adores(0).Value
            Adores.MoveNext
            m_asDatos(11) = Adores(0).Value
            Me.fraSolicitudCompra(0).caption = Adores(0).Value
            Adores.MoveNext
            sdbgGrupos.Columns("SOBRE").caption = Adores(0).Value  'Sobre
            Adores.MoveNext
            chkAdminPub.caption = Adores(0).Value  'Proceso de administraci�n p�blica
            
            Adores.MoveNext
            lstvwEsp.ColumnHeaders(1).Text = Adores(0).Value   'Fichero
            Adores.MoveNext
            lstvwEsp.ColumnHeaders(4).Text = Adores(0).Value  'Comentario
            
            Adores.MoveNext
            m_sIdiSelecAdjunto = Adores(0).Value    '78 Seleccionar archivo adjunto de especificaci�n
            Adores.MoveNext
            m_sIdiTodosArchivos = Adores(0).Value   '79 Todos los archivos
            Adores.MoveNext
            m_sIdiElArchivo = Adores(0).Value     '80 el archivo:
            Adores.MoveNext
            m_sIdiGuardar = Adores(0).Value   '81 Guardar archivo de especificaci�n
            Adores.MoveNext
            m_sIdiTipoOrig = Adores(0).Value  '82 Tipo original
            Adores.MoveNext
            m_sIdiArchivo = Adores(0).Value  '83 Archivo
            
            Adores.MoveNext
            cmdConfVistas.caption = Adores(0).Value '84 Configurar vistas
            Adores.MoveNext
            cmdEliminarVistas.caption = Adores(0).Value '85 Eliminar vistas
            Adores.MoveNext
            cmdEliminarPlant.caption = Adores(0).Value '86 eliminar
            Adores.MoveNext
            sdbgAtribPlant.Columns("DESCR").caption = Adores(0).Value
            sdbgAtributosEsp.Columns("DESCR").caption = Adores(0).Value
            Adores.MoveNext
            m_skb = Adores(0).Value
            Adores.MoveNext
            lstvwEsp.ColumnHeaders(2).Text = Adores(0).Value
            Adores.MoveNext
            lstvwEsp.ColumnHeaders(3).Text = Adores(0).Value
            Adores.MoveNext
            fraConfProce(2).caption = Adores(0).Value
            Adores.MoveNext
            Me.chkPrecios.caption = Adores(0).Value
            Adores.MoveNext
            Me.chkSuministros.caption = Adores(0).Value
            Adores.MoveNext
            Adores.MoveNext
            chkArchivProce.caption = Adores(0).Value
            Adores.MoveNext
            chkArchivGrupo.caption = Adores(0).Value
            Adores.MoveNext
            chkArchivItem.caption = Adores(0).Value
            Adores.MoveNext
            fraConfProce(3).caption = Adores(0).Value
            Adores.MoveNext
            chkPondAtrib.caption = Adores(0).Value
            Adores.MoveNext
            chkUnSoloPedido.caption = Adores(0).Value
            Adores.MoveNext
            chkSubasta.caption = Adores(0).Value
            Adores.MoveNext
            Adores.MoveNext
            Adores.MoveNext
            Adores.MoveNext
            chkNoPublicarFinSum.caption = Adores(0).Value
            Adores.MoveNext
            chkOferMonProce.caption = Adores(0).Value
            Adores.MoveNext
            m_sIdiMinutos = Adores(0).Value
            Adores.MoveNext
            fraConfProce(4).caption = Adores(0).Value
            
            ' edu T98
            Adores.MoveNext
            sIdiMat = Adores(0).Value '109
            sstabPlantillas.TabCaption(4) = Adores(0).Value
            Adores.MoveNext
            sIdiMarcaruno = Adores(0).Value '110 al menos un material
            Adores.MoveNext
            chkSoloEstosMateriales.caption = Adores(0).Value
            Adores.MoveNext
            Adores.MoveNext
            sdbgAtributosEsp.Columns("VALOR").caption = Adores(0).Value
            Adores.MoveNext
            sdbgAtributosEsp.Columns("VALIDACION").caption = Adores(0).Value
            
            Adores.MoveNext
            m_asAtrib(13) = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(14) = Adores(0).Value

            Adores.MoveNext
            m_sIdiTrue = Adores(0).Value
            Adores.MoveNext
            m_sIdiFalse = Adores(0).Value
            
            Adores.MoveNext
            sdbgAtribPlant.Columns("PEDIDO").caption = Adores(0).Value
            sdbgAtributosEsp.Columns("PEDIDO").caption = Adores(0).Value
            Adores.MoveNext
            chkOblAtrGS.caption = Adores(0).Value
            
            Adores.MoveNext
            chkParaPedido.caption = Adores(0).Value
            Adores.MoveNext
            chkParaPedidoDef.caption = Adores(0).Value
            Adores.MoveNext
            m_asAtrib(15) = Adores(0).Value 'Adjudicaci�n
            Adores.MoveNext
            Me.chkOblSolicitud.caption = Adores(0).Value
            Adores.MoveNext
            Me.chkOblSolicitudAdj.caption = Adores(0).Value
            Adores.Close
        End If

    Set Adores = Nothing

End Sub

''' <summary>Configura los controles del formulario en funci�n de los permisos del usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 07/06/2012</revision>

Private Sub ConfigurarSeguridad()
    m_bModif = False
    g_bPermitir = False
    m_bModifAtrib = False

    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
    
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANTILLASModificar)) Is Nothing) Then
            m_bModif = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANTILLASPermitirModifDefinAtrib)) Is Nothing) Then
             g_bPermitir = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBConsultar)) Is Nothing) Then
             m_bModifAtrib = True
        End If
        
        'edu tarea 98
        
        If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANTILLASRestringirSoloMaterialesComprador)) Is Nothing) Then
             m_bRestringirSoloMaterialesComprador = True
        End If
        If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANTILLASRestringirAsignacionAtributosMateriales)) Is Nothing) Then
             m_bRestringirAsignacionAtributosMateriales = True
        End If
    End If
    
    If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
        m_bModif = True
        g_bPermitir = True
        m_bModifAtrib = True
    End If

    'Comprueba si se visualizar�n o no las solicitudes de compras
    m_bMostrarSolicitud = True
    If FSEPConf Then
        m_bMostrarSolicitud = False
    Else
        If basParametros.gParametrosGenerales.gbSolicitudesCompras = False Then
            m_bMostrarSolicitud = False
        End If
    End If
    
    If ADMIN_OBLIGATORIO Then
        sdbgGrupos.Columns("SOBRE").Visible = True
        'No aparece la check de "Proceso de admin.pub" porque ser�n obligatoriamente de admin.p�blica
        picAdminPub.Visible = False
        picAdminPub.Height = 0
        fraAdminPub.Visible = False
        sdbgPlantillas.Top = fraAdminPub.Top
        sdbgPlantillas.Height = 3495
        picDatosConf(2).Visible = False
        cmdConfigSubasta.Visible = False
        
        
    Else
        If basParametros.gParametrosGenerales.gbAdminPublica = True Then
            'Check para indicar si ser� o no un proceso de administraci�n p�blica:
            picAdminPub.Visible = True
            fraAdminPub.Visible = True
            sdbgPlantillas.Top = 960
        Else
            sdbgGrupos.Columns("SOBRE").Visible = False
            picAdminPub.Visible = False
            fraAdminPub.Visible = False
            sdbgPlantillas.Top = fraAdminPub.Top
            sdbgPlantillas.Height = 3495
        End If
    End If
    
        
    If m_bModif = False Then
        cmdModificarPlant.Visible = False
        cmdEliminarPlant.Visible = False
        cmdEliminarVistas.Visible = False
        
        cmdModificarEsp.Visible = False
        cmdModificarMateriales.Visible = False
        cmdModoEdicionAtrEsp.Visible = False
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'si es un proceso de administraci�n p�blica no deja pasar a consulta hasta
    'que se introduzcan los sobres para todos los grupos.
    Dim i As Integer
    Dim vbm As Variant
    Dim FOSFile As Scripting.FileSystemObject

    'Descarga los forms de configuraci�n de vistas si est�n abiertos
    If Not m_ofrmConfigVistas Is Nothing Then
        Unload m_ofrmConfigVistas
        Set m_ofrmConfigVistas = Nothing
    End If
    
    'Borramos los archivos temporales que hayamos creado
    Set FOSFile = New Scripting.FileSystemObject
    i = 0
    While i < UBound(m_aSayFileNames)
        If FOSFile.FileExists(m_aSayFileNames(i)) Then
            FOSFile.DeleteFile m_aSayFileNames(i), True
        End If
        i = i + 1
    Wend
    Set FOSFile = Nothing
    
    If Not g_oPlantillaSeleccionada Is Nothing Then
        If g_oPlantillaSeleccionada.AdminPub = True And m_bModoEdicion = True Then
            For i = 0 To sdbgGrupos.Rows - 1
                vbm = sdbgGrupos.AddItemBookmark(i)
                If sdbgGrupos.Columns("SOBRE").CellValue(vbm) = "" Then
                    oMensajes.IntroduzcaSobre
                    Cancel = True
                    Exit Sub
                End If
            Next i
        End If
    End If
    
    Set g_oPlantillas = Nothing
    Set g_oPlantillaSeleccionada = Nothing
    Set g_oIBaseDatos = Nothing
    Set m_oGrupos = Nothing
    Set m_oAtributo = Nothing
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If
    Me.Visible = False
    
End Sub


''' <summary>
''' Evento producido al hacer click en una de las plantillas de la lista
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en la lista </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Public Sub lstvwplantillas_ItemClick(ByVal Item As MSComctlLib.listItem)

    If cmdModoEdicion.caption = m_sConsulta Or cmdAceptar.Visible = True Or cmdModoEdicionAtr.caption = m_sConsulta Or cmdModoEdicionAtrEsp.caption = m_sConsulta Then
        lstvwplantillas.selectedItem.Selected = False
        lstvwplantillas.ListItems.Item(m_iIndiListw).Selected = True
        Exit Sub
    End If
    
    Set Item = lstvwplantillas.selectedItem
    
    g_lNumID = Right(Item.key, Len(Item.key) - 2)
    
    
    CargarGridPlantillas True

    If sstabPlantillas.Tab = 2 Then
        CargarComboGrupos
        g_sCodGRP = ""
        CargarGridAtributos
        CargarEspecificaciones
        g_sCodGRPEsp = ""
        CargarGridEspecificaciones
    End If
End Sub



Private Sub CargarComboGrupos()
Dim oGrupo As CGrupo

    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub

    'Cargo la combo de grupos
    sdbcSeleccion.RemoveAll
    sdbcSeleccion.AddItem "************" & Chr(m_lSeparador) & m_asAtrib(10)
    If Not g_oPlantillaSeleccionada.Grupos Is Nothing Then
        For Each oGrupo In g_oPlantillaSeleccionada.Grupos
            sdbcSeleccion.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den
        Next
    End If
    g_sCodGRP = ""
    sdbcSeleccion.Columns(0).Value = "************"
    sdbcSeleccion.Text = m_asAtrib(10)


End Sub
Private Sub lstvwplantillas_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim Item As MSComctlLib.listItem

    If Button = 2 Then
        Set Item = lstvwplantillas.selectedItem
        If Not Item Is Nothing Then
            If Not m_bModif Then
                MDI.mnuPopUpPlantillas(1).Visible = False
                MDI.mnuPopUpPlantillas(2).Visible = False
                MDI.mnuPopUpPlantillas(4).Visible = False
                MDI.mnuPopUpPlantillas(5).Visible = False
            Else
                MDI.mnuPopUpPlantillas(1).Visible = True
                MDI.mnuPopUpPlantillas(2).Visible = True
                MDI.mnuPopUpPlantillas(3).Visible = True
                MDI.mnuPopUpPlantillas(4).Visible = True
                MDI.mnuPopUpPlantillas(5).Visible = True
            End If
            PopupMenu MDI.POPUPplantillas
        End If
    End If

End Sub

Private Sub sdbddAmbito_CloseUp()
    If sdbddAmbito.Columns(0).Text = m_asAtrib(5) Then
        If sdbgAtribPlant.Columns("APLIC_PREC").Text = m_asAtrib(12) Then
            sdbgAtribPlant.Columns("APLIC_PREC").Text = ""
        End If
    ElseIf sdbgAtribPlant.Columns("AMBITO").Text = m_asAtrib(6) Then
        If sdbgAtribPlant.Columns("APLIC_PREC").Text <> m_asAtrib(8) And sdbgAtribPlant.Columns("APLIC_PREC").Text <> m_asAtrib(9) Then
            sdbgAtribPlant.Columns("APLIC_PREC").Text = ""
        End If
    End If
End Sub

Private Sub sdbddAmbito_DropDown()
Dim i As Integer

If sdbgAtribPlant.Columns("AMBITO").Locked Then
    sdbddAmbito.Enabled = False
    Exit Sub
End If

sdbddAmbito.RemoveAll

If g_sCodGRP <> "" Then
    If sdbgAtribPlant.Columns("CODGRUPO").Value <> "" Then
        For i = 1 To 2
            sdbddAmbito.AddItem m_asAtrib(i + 4)
            sdbddAmbito.MoveNext
        Next
    Else
        If chkAdminPub.Value = vbChecked Then
            'Si es un proceso de administraci�n p�blica no se podr�n definir atributos a nivel
            'de proceso
            For i = 1 To 2
                sdbddAmbito.AddItem m_asAtrib(i + 4)
                sdbddAmbito.MoveNext
            Next
        Else
            For i = 0 To 2
                sdbddAmbito.AddItem m_asAtrib(i + 4)
                sdbddAmbito.MoveNext
            Next
        End If
    End If
Else
    If chkAdminPub.Value = vbChecked Then
        'Si es un proceso de administraci�n p�blica no se podr�n definir atributos a nivel
        'de proceso
        For i = 1 To 2
            sdbddAmbito.AddItem m_asAtrib(i + 4)
            sdbddAmbito.MoveNext
        Next
    Else
        For i = 0 To 2
            sdbddAmbito.AddItem m_asAtrib(i + 4)
            sdbddAmbito.MoveNext
        Next
    End If
End If

End Sub

Private Sub sdbddAmbito_InitColumnProps()
    sdbddAmbito.DataFieldList = "Column 0"
    sdbddAmbito.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddAmbito_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
Dim i As Long
Dim bm As Variant

    On Error Resume Next
   
    sdbddAmbito.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddAmbito.Rows - 1
            bm = sdbddAmbito.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddAmbito.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtribPlant.Columns("AMBITO").Value = Mid(sdbddAmbito.Columns(0).CellText(bm), 1, Len(Text))
                sdbddAmbito.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddAmbito_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        ''' Comprobar la existencia en la lista
        If g_oPlantillaSeleccionada.AdminPub = True Then
            Select Case UCase(sdbgAtribPlant.Columns("AMBITO").Text)
                Case UCase(m_asAtrib(5)): RtnPassed = True  'Grupo
                Case UCase(m_asAtrib(6)): RtnPassed = True  'Item
                Case Else: sdbgAtribPlant.Columns("AMBITO").Text = ""
            End Select
        Else
            Select Case UCase(sdbgAtribPlant.Columns("AMBITO").Text)
                Case UCase(m_asAtrib(4)): RtnPassed = True  'Proceso
                Case UCase(m_asAtrib(5)): RtnPassed = True  'Grupo
                Case UCase(m_asAtrib(6)): RtnPassed = True  'Item
                Case Else: sdbgAtribPlant.Columns("AMBITO").Text = ""
            End Select
        End If
    End If
End Sub

Private Sub sdbddAplicarA_DropDown()
Dim i As Integer
    
    sdbddAplicarA.RemoveAll

    If sdbgAtribPlant.Columns("APLIC_PREC").Locked Then
        sdbddAplicarA.Enabled = False
        Exit Sub
    End If
    
    If sdbgAtribPlant.Columns("TIPO").Value <> m_asAtrib(0) Then
        sdbddAplicarA.AddItem ""
        sdbddAplicarA.Columns("NOMBRE").DropDownHwnd = sdbddAplicarA.hWnd
        Exit Sub
    End If
    
    If sdbgAtribPlant.Columns("AMBITO").Text = m_asAtrib(4) Then
    
        sdbddAplicarA.AddItem m_asAtrib(12)
        sdbddAplicarA.MoveNext
        For i = 1 To 3
            sdbddAplicarA.AddItem m_asAtrib(i + 6), i
            sdbddAplicarA.MoveNext
        Next
    ElseIf sdbgAtribPlant.Columns("AMBITO").Text = m_asAtrib(5) Then
        For i = 0 To 2
            sdbddAplicarA.AddItem m_asAtrib(i + 7), i
            sdbddAplicarA.MoveNext
        Next
    ElseIf sdbgAtribPlant.Columns("AMBITO").Text = m_asAtrib(6) Then
        For i = 0 To 1
            sdbddAplicarA.AddItem m_asAtrib(i + 8), i
            sdbddAplicarA.MoveNext
        Next
    ElseIf sdbgAtribPlant.Columns("AMBITO").Text = "" Then
        
        sdbddAplicarA.AddItem "", 0 'para que no se bloque el dropdown de aplicar a precio
    
    End If

End Sub

Private Sub sdbddAplicarA_InitColumnProps()
    sdbddAplicarA.DataFieldList = "Column 0"
    sdbddAplicarA.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddAplicarA_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
Dim i As Long
Dim bm As Variant

    On Error Resume Next
   
    sdbddAplicarA.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddAplicarA.Rows - 1
            bm = sdbddAplicarA.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddAplicarA.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtribPlant.Columns("APLIC_PREC").Value = Mid(sdbddAplicarA.Columns(0).CellText(bm), 1, Len(Text))
                sdbddAplicarA.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddAplicarA_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        ''' Comprobar la existencia en la lista
        
        Select Case UCase(sdbgAtribPlant.Columns("APLIC_PREC").Text)
        
            Case UCase(m_asAtrib(12)): RtnPassed = True
            Case UCase(m_asAtrib(7)): RtnPassed = True
            Case UCase(m_asAtrib(8)): RtnPassed = True
            Case UCase(m_asAtrib(9)): RtnPassed = True
            Case Else: sdbgAtribPlant.Columns("APLIC_PREC").Text = ""
        
        End Select
    
    End If
End Sub


Private Sub sdbddSigno_DropDown()
    If sdbgAtribPlant.Columns("OP").Locked Then
        sdbddSigno.Enabled = False
        Exit Sub
    End If

    sdbddSigno.DataFieldToDisplay = "Column 0"
    sdbddSigno.RemoveAll
    sdbddSigno.AddItem "+"
    sdbddSigno.AddItem "-"
    sdbddSigno.AddItem "*"
    sdbddSigno.AddItem "/"
    sdbddSigno.AddItem "+%"
    sdbddSigno.AddItem "-%"
End Sub

Private Sub sdbddSigno_InitColumnProps()
    sdbddSigno.DataFieldList = "Column 0"
End Sub

Private Sub sdbddSigno_PositionList(ByVal Text As String)
 ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddSigno.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddSigno.Rows - 1
            bm = sdbddSigno.GetBookmark(i)
            If UCase(sdbddSigno.Text) = UCase(Mid(sdbddSigno.Columns(0).CellText(bm), 1, Len(sdbddSigno.Text))) Then
                sdbddSigno.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddSigno_ValidateList(Text As String, RtnPassed As Integer)
Dim i As Integer
Dim bm As Variant
    
    If sdbgAtribPlant.Columns("OP").Text = "" Then Exit Sub
    If sdbddSigno.Columns(0).Text <> "" Then

           For i = 0 To sdbddSigno.Rows - 1
            bm = sdbddSigno.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddSigno.Columns(0).CellText(bm), 1, Len(sdbddSigno.Columns(0).Text))) Then
                sdbddSigno.Bookmark = bm
                Exit Sub
            End If
           Next i
           oMensajes.NoValida sdbgAtribPlant.Columns("OP").caption
           sdbgAtribPlant.Columns("OP").Value = ""
    End If
   
End Sub

Private Sub sdbgAtribPlant_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgAtribPlant.Rows = 0) Then
        Exit Sub
    End If
    If Me.Visible Then sdbgAtribPlant.SetFocus
    If sdbgAtribPlant.Row >= sdbgAtribPlant.Rows Then
        sdbgAtribPlant.Bookmark = sdbgAtribPlant.RowBookmark(sdbgAtribPlant.Rows - 1)
    Else
        sdbgAtribPlant.Bookmark = sdbgAtribPlant.RowBookmark(sdbgAtribPlant.Row)
    End If
End Sub

Private Sub sdbgAtribPlant_AfterInsert(RtnDispErrMsg As Integer)
RtnDispErrMsg = 0

End Sub

Private Sub sdbgAtribPlant_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If m_bAnyaError = False And m_bModError = False Then
        cmdAnyadirAtr.Enabled = True
        cmdEliAtr.Enabled = True
        cmdDeshacerAtr.Enabled = False
    End If
End Sub

Private Sub sdbgAtribPlant_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub


''' <summary>
''' Evento de la grid del taba de atributos, comprueba que los valores son correctos
''' </summary>
''' <returns></returns>
''' <param name="Cancel">Cancelacion de la actualizacion </param>
''' <remarks>Llamada desde: Al hacer un cambio en la grid, al cambiar de linea</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtribPlant_BeforeUpdate(Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim bOrdenar As Boolean

    If sdbgAtribPlant.col = -1 And g_sOrigen = "" Then Exit Sub
    
    Cancel = False
    m_bValError = False
    
    If sdbgAtribPlant.Columns("COD").Value = "" Then
        oMensajes.NoValido sdbgAtribPlant.Columns("COD").caption
        Cancel = True
        GoTo Salir
        Exit Sub
    End If
    If Trim(sdbgAtribPlant.Columns("NOMBRE").Value) = "" Then
        oMensajes.NoValido sdbgAtribPlant.Columns("NOMBRE").caption
        Cancel = True
        GoTo Salir
    End If
        
    If Trim(sdbgAtribPlant.Columns("TIPO").Value) = "" Then
        oMensajes.NoValido sdbgAtribPlant.Columns("TIPO").caption
        Cancel = True
        GoTo Salir
    End If
    
    If sdbgAtribPlant.Columns("AMBITO").Text <> "" Then
        If sdbgAtribPlant.Columns("AMBITO").Text <> m_asAtrib(4) Then
            If sdbgAtribPlant.Columns("AMBITO").Text <> m_asAtrib(5) Then
                If sdbgAtribPlant.Columns("AMBITO").Text <> m_asAtrib(6) Then
                    oMensajes.NoValido sdbgAtribPlant.Columns("AMBITO").caption
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
    Else
        sdbgAtribPlant.Columns("AMBITO").Text = m_asAtrib(6)
    End If
       
    
    If sdbgAtribPlant.Columns("MIN").Text <> "" Then
        If sdbgAtribPlant.Columns("LIBRE").Value = "0" Then
            oMensajes.NoValida sdbgAtribPlant.Columns("MIN").caption
            If Me.Visible Then sdbgAtribPlant.SetFocus
            Cancel = True
            GoTo Salir
        End If
        Select Case sdbgAtribPlant.Columns("idTIPO").Value
            Case TiposDeAtributos.TipoFecha
                If Not IsDate(sdbgAtribPlant.Columns("MIN").Text) Then
                   oMensajes.NoValida sdbgAtribPlant.Columns("MIN").caption
                   If Me.Visible Then sdbgAtribPlant.SetFocus
                   Cancel = True
                   GoTo Salir
                End If
            Case TiposDeAtributos.TipoNumerico
                If Not IsNumeric(sdbgAtribPlant.Columns("MIN").Text) Then
                    oMensajes.NoValida sdbgAtribPlant.Columns("MIN").caption
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                End If
            Case Else
                sdbgAtribPlant.Columns("MIN").Text = ""
        End Select
    End If
    
    If sdbgAtribPlant.Columns("MAX").Text <> "" Then
        If sdbgAtribPlant.Columns("LIBRE").Value = "0" Then
            oMensajes.NoValida sdbgAtribPlant.Columns("MAX").caption
            If Me.Visible Then sdbgAtribPlant.SetFocus
            Cancel = True
            GoTo Salir
        End If
        Select Case sdbgAtribPlant.Columns("idTIPO").Value
            Case TiposDeAtributos.TipoFecha
                If Not IsDate(sdbgAtribPlant.Columns("MAX").Text) Then
                   oMensajes.NoValida sdbgAtribPlant.Columns("MAX").caption
                   If Me.Visible Then sdbgAtribPlant.SetFocus
                   Cancel = True
                   GoTo Salir
                End If
            Case TiposDeAtributos.TipoNumerico
                If Not IsNumeric(sdbgAtribPlant.Columns("MAX").Text) Then
                    oMensajes.NoValida sdbgAtribPlant.Columns("MAX").caption
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                End If
            Case Else
                sdbgAtribPlant.Columns("MAX").Text = ""
        End Select
    End If

    If sdbgAtribPlant.Columns("MIN").Value <> "" And sdbgAtribPlant.Columns("MAX").Value <> "" Then
        If sdbgAtribPlant.Columns("idTIPO").Value = TiposDeAtributos.TipoFecha Then
            If CDate(sdbgAtribPlant.Columns("MIN").Value) > CDate(sdbgAtribPlant.Columns("MAX").Value) Then
                oMensajes.NoValida sdbgAtribPlant.Columns("MIN").caption
                If Me.Visible Then sdbgAtribPlant.SetFocus
                Cancel = True
                GoTo Salir
            End If
        Else
            If VarToDbl0(sdbgAtribPlant.Columns("MIN").Value) > VarToDbl0(sdbgAtribPlant.Columns("MAX").Value) Then
                oMensajes.NoValida sdbgAtribPlant.Columns("MIN").caption
                If Me.Visible Then sdbgAtribPlant.SetFocus
                Cancel = True
                GoTo Salir
            End If
        End If
    End If
    
    If sdbgAtribPlant.Columns("CODGRUPO").Value <> "" And UCase(sdbgAtribPlant.Columns("AMBITO").Text) = UCase(m_asAtrib(12)) Then
        oMensajes.NoValido sdbgAtribPlant.Columns("AMBITO").caption
        If Me.Visible Then sdbgAtribPlant.SetFocus
        Cancel = True
        GoTo Salir
    End If

    If sdbgAtribPlant.Columns("APLIC_PREC").Text = "" Then
        sdbgAtribPlant.Columns("DEF_PREC").Text = ""
        sdbgAtribPlant.Columns("OP").Text = ""
    End If
    If sdbgAtribPlant.Columns("OP").Text = "" Then
        sdbgAtribPlant.Columns("DEF_PREC").Text = ""
        sdbgAtribPlant.Columns("APLIC_PREC").Text = ""
    End If

    If sdbgAtribPlant.Columns("APLIC_PREC").Text <> "" Then
    
        'Ambito grupo no puede aplicarse a total oferta
        If UCase(sdbgAtribPlant.Columns("AMBITO").Text) = UCase(m_asAtrib(5)) Then
            If UCase(sdbgAtribPlant.Columns("APLIC_PREC").Text) = UCase(m_asAtrib(10)) Then
                oMensajes.NoValido sdbgAtribPlant.Groups(3).caption
                If Me.Visible Then sdbgAtribPlant.SetFocus
                Cancel = True
                GoTo Salir
            End If
        End If
        'Ambito item s�lo puede ser aplicable a total item y unitario
        If UCase(sdbgAtribPlant.Columns("AMBITO").Text) = UCase(m_asAtrib(6)) Then
            If UCase(sdbgAtribPlant.Columns("APLIC_PREC").Text) <> UCase(m_asAtrib(9)) And UCase(sdbgAtribPlant.Columns("APLIC_PREC").Text) <> UCase(m_asAtrib(8)) Then
                oMensajes.NoValido sdbgAtribPlant.Groups(3).caption
                If Me.Visible Then sdbgAtribPlant.SetFocus
                Cancel = True
                GoTo Salir
            End If
        End If
                
    End If

    If sdbgAtribPlant.Columns("OP").Text <> "" Then
        
        If sdbgAtribPlant.Columns("OP").Value <> "+" And sdbgAtribPlant.Columns("OP").Value <> "-" And _
           sdbgAtribPlant.Columns("OP").Value <> "*" And sdbgAtribPlant.Columns("OP").Value <> "/" And _
           sdbgAtribPlant.Columns("OP").Value <> "-%" And sdbgAtribPlant.Columns("OP").Value <> "+%" Then
                oMensajes.NoValido sdbgAtribPlant.Columns("OP").caption
                If Me.Visible Then sdbgAtribPlant.SetFocus
                Cancel = True
                GoTo Salir
        End If
        
    End If
    
    If sdbgAtribPlant.IsAddRow Then   'Insertar
        If m_bModificarDefAtrib Then
            If g_bPermitir Then
                irespuesta = oMensajes.ModifDefinicionAtrib
                If irespuesta = vbNo Then
                    g_bGuardarEnDefinicion = False
                Else
                    g_bGuardarEnDefinicion = True
                End If
            End If
        End If
        
        m_bModificarDefAtrib = False
            
        Set m_oAtributo = oFSGSRaiz.Generar_CAtributo
        m_oAtributo.Id = sdbgAtribPlant.Columns("ATRIB").Value
        
        
        teserror = g_oPlantillaSeleccionada.AnyadirAtributo(m_oAtributo.Id, g_sCodGRP, g_bGuardarEnDefinicion)
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            cmdDeshacerAtr_Click
            Set m_oAtributo = Nothing
            Exit Sub
        End If
        
        RegistrarAccion ACCPlantAtribAnya, "Atributo A�adido: " & sdbgAtribPlant.Columns("ATRIB").Value & " Grupo " & g_sCodGRP
        
        Set m_oAtributo = Nothing
    
    Else 'Modificar
    
        Set m_oAtributoEnEdicion = Nothing
        Set m_oAtributoEnEdicion = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Text))
        
        
        
'''Ambito

    Dim iError As Integer
    
    If m_oAtributoEnEdicion.pedido Then

        Select Case UCase(sdbgAtribPlant.Columns("AMBITO").Value)
    
        Case UCase(m_asAtrib(4))

                ''Comprobacion de que no existe ese atributo de ese ambito marcado para pedido
                iError = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).ComprobarPermitirMarcarAtributoAPedidoPlant(TipoAmbitoProceso.AmbProceso)
                If iError <> 0 Then
                    
                    teserror.NumError = TESAtributoYaMarcadoAPedido
                    teserror.Arg1 = iError
                    TratarError teserror
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                    
                End If
                                    
        Case UCase(m_asAtrib(5))

                iError = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).ComprobarPermitirMarcarAtributoAPedidoPlant(TipoAmbitoProceso.AmbGrupo)
                If iError <> 0 Then
                    
                    teserror.NumError = TESAtributoYaMarcadoAPedido
                    teserror.Arg1 = iError
                    TratarError teserror
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                    
                End If
            


        Case UCase(m_asAtrib(6))
 
                iError = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).ComprobarPermitirMarcarAtributoAPedidoPlant(TipoAmbitoProceso.AmbItem)
                If iError <> 0 Then
                    
                    teserror.NumError = TESAtributoYaMarcadoAPedido
                    teserror.Arg1 = iError
                    TratarError teserror
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                    
                End If
        
            
        End Select
    End If


'''Ambito
        
        
        'Si se ha modificado el �mbito comprueba si hay vistas configuradas
        Select Case UCase(sdbgAtribPlant.Columns("AMBITO").Text)
            Case UCase(m_asAtrib(4)):
                If m_oAtributoEnEdicion.ambito <> AmbProceso Then
                    If ComprobarActualizarPlantilla(g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Text))) = False Then
                        If Me.Visible Then sdbgAtribPlant.SetFocus
                        Cancel = True
                        GoTo Salir
                    End If
                End If
                m_oAtributoEnEdicion.ambito = AmbProceso
                
            Case UCase(m_asAtrib(5)):
                If m_oAtributoEnEdicion.ambito <> AmbGrupo Then
                    If ComprobarActualizarPlantilla(g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Text))) = False Then
                        If Me.Visible Then sdbgAtribPlant.SetFocus
                        Cancel = True
                        GoTo Salir
                    End If
                End If
                m_oAtributoEnEdicion.ambito = AmbGrupo
                
            Case UCase(m_asAtrib(6)):
                If m_oAtributoEnEdicion.ambito <> AmbItem Then
                    If ComprobarActualizarPlantilla(g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Text))) = False Then
                        If Me.Visible Then sdbgAtribPlant.SetFocus
                        Cancel = True
                        GoTo Salir
                    End If
                End If
                m_oAtributoEnEdicion.ambito = AmbItem
                
            Case "":
                If m_oAtributoEnEdicion.ambito <> AmbItem Then
                    If ComprobarActualizarPlantilla(g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Text))) = False Then
                        If Me.Visible Then sdbgAtribPlant.SetFocus
                        Cancel = True
                        GoTo Salir
                    End If
                End If
                m_oAtributoEnEdicion.ambito = AmbItem
        End Select
        
        If m_bModificarDefAtrib Then
            If g_bPermitir Then
                irespuesta = oMensajes.ModifDefinicionAtrib
                If irespuesta = vbNo Then
                    g_bGuardarEnDefinicion = False
                Else
                    g_bGuardarEnDefinicion = True
                End If
            End If
        End If
        
        m_bModificarDefAtrib = False
       
        If sdbgAtribPlant.Columns("idTIPO").Value = "4" Then
          sdbgAtribPlant.Columns("LIBRE").Value = "0"
          sdbgAtribPlant.Columns("SELECCION").Value = "0"
          sdbgAtribPlant.Columns("MIN").Value = ""
          sdbgAtribPlant.Columns("MAX").Value = ""
        End If
       
        If sdbgAtribPlant.Columns("INTERNO").Value = "0" Or sdbgAtribPlant.Columns("INTERNO").Value = "" Then
            m_oAtributoEnEdicion.interno = False
        Else
            m_oAtributoEnEdicion.interno = True
        End If
    
        If sdbgAtribPlant.Columns("PEDIDO").Value = "0" Or sdbgAtribPlant.Columns("PEDIDO").Value = "" Then
            m_oAtributoEnEdicion.pedido = False
        Else
            m_oAtributoEnEdicion.pedido = True
        End If
    
         
        Select Case UCase(sdbgAtribPlant.Columns("TIPO").Text)
        
            Case UCase(m_asAtrib(0)): m_oAtributoEnEdicion.Tipo = TiposDeAtributos.TipoNumerico
                                      sdbgAtribPlant.Columns("idTIPO").Value = TiposDeAtributos.TipoNumerico
            
            Case UCase(m_asAtrib(1)): m_oAtributoEnEdicion.Tipo = TiposDeAtributos.TipoFecha
                                      sdbgAtribPlant.Columns("idTIPO").Value = TiposDeAtributos.TipoFecha

            Case UCase(m_asAtrib(2)): m_oAtributoEnEdicion.Tipo = TiposDeAtributos.TipoBoolean
                                      sdbgAtribPlant.Columns("idTIPO").Value = TiposDeAtributos.TipoBoolean

            Case UCase(m_asAtrib(3)): m_oAtributoEnEdicion.Tipo = TiposDeAtributos.TipoString
                                      sdbgAtribPlant.Columns("idTIPO").Value = TiposDeAtributos.TipoString
        End Select
            
        m_oAtributoEnEdicion.Maximo = StrToNull(sdbgAtribPlant.Columns("MAX").Value)
        m_oAtributoEnEdicion.Minimo = StrToNull(sdbgAtribPlant.Columns("MIN").Value)
        m_oAtributoEnEdicion.PreferenciaValorBajo = SQLBinaryToBoolean(StrToDbl0(sdbgAtribPlant.Columns("VALORBAJO").Value))
            
        If SQLBinaryToBoolean(StrToDbl0(sdbgAtribPlant.Columns("SELECCION").Value)) Then
            m_oAtributoEnEdicion.TipoIntroduccion = Introselec
        Else
            m_oAtributoEnEdicion.TipoIntroduccion = IntroLibre
        End If
                              
                              
                              
        
        Select Case UCase(sdbgAtribPlant.Columns("AMBITO").Text)
            Case UCase(m_asAtrib(4)): m_oAtributoEnEdicion.ambito = AmbProceso
            Case UCase(m_asAtrib(5)): m_oAtributoEnEdicion.ambito = AmbGrupo
            Case UCase(m_asAtrib(6)): m_oAtributoEnEdicion.ambito = AmbItem
            Case "": m_oAtributoEnEdicion.ambito = AmbItem
        End Select
        
        If sdbgAtribPlant.Columns("OBL").Value = "0" Or sdbgAtribPlant.Columns("OBL").Value = "" Then
            m_oAtributoEnEdicion.Obligatorio = False
        Else
            m_oAtributoEnEdicion.Obligatorio = True
        End If
        
        If sdbgAtribPlant.Columns("OP").Value <> "" Then
            m_oAtributoEnEdicion.PrecioFormula = sdbgAtribPlant.Columns("OP").Value
        Else
            m_oAtributoEnEdicion.PrecioFormula = Null
        End If
            
        bOrdenar = False
        Select Case UCase(sdbgAtribPlant.Columns("APLIC_PREC").Text)
            Case UCase(m_asAtrib(12)):
                If m_oAtributoEnEdicion.PrecioAplicarA <> 0 Or IsNull(m_oAtributoEnEdicion.PrecioAplicarA) Then bOrdenar = True
                m_oAtributoEnEdicion.PrecioAplicarA = 0 'Total de la oferta'
            Case UCase(m_asAtrib(7)):
                If m_oAtributoEnEdicion.PrecioAplicarA <> 1 Or IsNull(m_oAtributoEnEdicion.PrecioAplicarA) Then bOrdenar = True
                m_oAtributoEnEdicion.PrecioAplicarA = 1 'Total del grupo
            Case UCase(m_asAtrib(8)):
                If m_oAtributoEnEdicion.PrecioAplicarA <> 2 Or IsNull(m_oAtributoEnEdicion.PrecioAplicarA) Then bOrdenar = True
                m_oAtributoEnEdicion.PrecioAplicarA = 2 'Total del item
            Case UCase(m_asAtrib(9)):
                If m_oAtributoEnEdicion.PrecioAplicarA <> 3 Or IsNull(m_oAtributoEnEdicion.PrecioAplicarA) Then bOrdenar = True
                m_oAtributoEnEdicion.PrecioAplicarA = 3 ' Precio unitario del item
            Case "":
                If IsNull(m_oAtributoEnEdicion.PrecioAplicarA) Then bOrdenar = True
                m_oAtributoEnEdicion.PrecioAplicarA = Null
        End Select
            
        If sdbgAtribPlant.Columns("OP").Text <> "0" And sdbgAtribPlant.Columns("APLIC_PREC").Text <> "" Then
            If sdbgAtribPlant.Columns("DEF_PREC").Value = "0" Or sdbgAtribPlant.Columns("DEF_PREC").Value = "" Then
                m_oAtributoEnEdicion.PrecioDefecto = 0
            Else
                m_oAtributoEnEdicion.PrecioDefecto = 1
            End If
        Else
            m_oAtributoEnEdicion.PrecioDefecto = Null
        End If
            
        m_oAtributoEnEdicion.TipoPonderacion = sdbgAtribPlant.Columns("TipoPond").Value
        
        m_oAtributoEnEdicion.IDPlantilla = g_oPlantillaSeleccionada.Id
        
        If sdbgAtribPlant.Columns("CODGRUPO").Value <> "" Then
            teserror = g_oPlantillaSeleccionada.ModificarAtributo(sdbgAtribPlant.Columns("ATRIB").Value, g_bGuardarEnDefinicion, sdbgAtribPlant.Columns("CODGRUPO").Value, bOrdenar)
        Else
            teserror = g_oPlantillaSeleccionada.ModificarAtributo(sdbgAtribPlant.Columns("ATRIB").Value, g_bGuardarEnDefinicion, , bOrdenar)
        End If
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgAtribPlant.SetFocus
            m_bValError = True
        End If
        
        RegistrarAccion ACCPlantAtribMod, "Atributo modificado: " & sdbgAtribPlant.Columns("ATRIB").Value & " Grupo " & g_sCodGRP
    
    End If

    sdbgAtribPlant.Columns("CODGRUPO").Text = g_sCodGRP 'para el roadload


Exit Sub

Salir:
        
    m_bValError = Cancel
    If Me.Visible Then sdbgAtribPlant.SetFocus
End Sub

Private Sub sdbgAtribPlant_BtnClick()

    If sdbgAtribPlant.col < 0 Then Exit Sub

    If sdbgAtribPlant.Columns("COD").Text = "" Then Exit Sub
    
    If sdbgAtribPlant.DataChanged = True Then
        sdbgAtribPlant.Update
    End If
    
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "POND" Then
        If sdbgAtribPlant.DataChanged Then
             sdbgAtribPlant.Update
             If m_bValError Then
                 Exit Sub
             End If
         End If
        If sdbgAtribPlant.Columns("idTIPO").Value <> "" Then
            frmPonderacion.g_iTipo = sdbgAtribPlant.Columns("idTIPO").Value
            frmPonderacion.g_sQuien = sdbgAtribPlant.Columns("NOMBRE").Value
            frmPonderacion.g_bModDefAtrib = g_bPermitir
            CargarPonderacion
            If sdbgAtribPlant.DataChanged Then
                sdbgAtribPlant.Update
                If m_bValError Then
                    Exit Sub
                End If
            End If
        End If
    End If
    
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "BOT" Then
        If sdbgAtribPlant.Columns("idTIPO").Value <> 4 Then
            If m_bModoEdicionAtr Then
                If g_sCodGRP <> "" And sdbgAtribPlant.Columns("CODGRUPO").Value = "" Then
                Else
                    If sdbgAtribPlant.Columns("SELECCION").Value = 0 Or sdbgAtribPlant.Columns("SELECCION").Value = "False" Then
                        sdbgAtribPlant.Columns("SELECCION").Value = "-1"
                        sdbgAtribPlant.Columns("BOT").Value = "..."
                        sdbgAtribPlant.Columns("LIBRE").Value = 0
                    End If
                End If
            End If
            If sdbgAtribPlant.Columns("SELECCION").Value = "-1" Or sdbgAtribPlant.Columns("SELECCION").Value = "True" Then
                CargarListaValores
            End If
        End If
    End If
    
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "DESCR" Then
        If Trim(sdbgAtribPlant.Columns("HIDENDESCR").Value) <> "" Then
            frmATRIBDescr.g_bEdicion = False
            frmATRIBDescr.txtDescr.Text = sdbgAtribPlant.Columns("HIDENDESCR").Value
            frmATRIBDescr.Show 1
        Else
            oMensajes.MensajeOKOnly (644)
            Exit Sub
        End If
    End If


End Sub

''' <summary>
''' Evento de la grid del taba de atributos, comprueba que los valores son correctos
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer un cambio en la grid, al cambiar de linea</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtribPlant_Change()
Dim teserror As TipoErrorSummit

    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Locked Then
        sdbgAtribPlant.DataChanged = False
        Exit Sub
    End If
    If cmdDeshacerAtr.Enabled = False Then
        
        cmdAnyadirAtr.Enabled = False
        cmdEliAtr.Enabled = False
        cmdDeshacerAtr.Enabled = True
        cmdCodigo.Enabled = False
        
    End If
    
    If sdbgAtribPlant.Columns("COD").Value = "" Then 'SI COD = "" NO DEJAMOS MODIFICAR
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "INTERNO" Then
            sdbgAtribPlant.Columns("INTERNO").Value = 0
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "PEDIDO" Then
            sdbgAtribPlant.Columns("PEDIDO").Value = 0
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "LIBRE" Then
            sdbgAtribPlant.Columns("LIBRE").Value = 0
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "MINIMO" Then
            sdbgAtribPlant.Columns("MINIMO").Value = ""
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "MAXIMO" Then
            sdbgAtribPlant.Columns("MAXIMO").Value = ""
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "SELECCION" Then
            sdbgAtribPlant.Columns("SELECCION").Value = ""
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "AMBITO" Then
            sdbgAtribPlant.Columns("AMBITO").Value = ""
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "OBL" Then
            sdbgAtribPlant.Columns("OBL").Value = 0
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "VALORBAJO" Then
            sdbgAtribPlant.Columns("VALORBAJO").Value = 0
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "OP" Then
            sdbgAtribPlant.Columns("OP").Value = ""
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "APLIC_PREC" Then
            sdbgAtribPlant.Columns("APLIC_PREC").Value = ""
        End If
        If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "DEF_PREC" Then
            sdbgAtribPlant.Columns("DEF_PREC").Value = 0
        End If
        
        sdbgAtribPlant.CancelUpdate
        Exit Sub
    
    End If

    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "VALORBAJO" Then
        If sdbgAtribPlant.Columns("VALORBAJO").Text = "-1" Then
            m_bModificarDefAtrib = True
        End If
    End If

    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "MINIMO" Then
        m_bModificarDefAtrib = True
    End If
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "MAXIMO" Then
        m_bModificarDefAtrib = True
    End If
    
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "DEF_PREC" Then
        If sdbgAtribPlant.Columns("DEF_PREC").Value = "-1" Or sdbgAtribPlant.Columns("DEF_PREC").Value = "TRUE" Then
            If sdbgAtribPlant.Columns("OP").Value = "" Or sdbgAtribPlant.Columns("APLIC_PREC").Value = "" Then
                 sdbgAtribPlant.Columns("DEF_PREC").Value = 0
                Exit Sub
            End If
        End If
    End If
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "OP" Then
         If sdbgAtribPlant.Columns("OP").Value = "" Then
            sdbgAtribPlant.Columns("DEF_PREC").Value = 0
        End If
    End If
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "APLIC_PREC" Then
         If sdbgAtribPlant.Columns("APLIC_PREC").Value = "" Then
            sdbgAtribPlant.Columns("DEF_PREC").Value = 0
        End If
    End If
    
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "LIBRE" Then
    
        If sdbgAtribPlant.Columns("LIBRE").Value = "-1" Or sdbgAtribPlant.Columns("LIBRE").Value = "TRUE" Then
            sdbgAtribPlant.Columns("SELECCION").Value = 0
            sdbgAtribPlant.Columns("BOT").Value = ""
            If sdbgAtribPlant.Columns("idtipo").Value = "4" Then
                sdbgAtribPlant.Columns("LIBRE").Value = 0
                m_bModificarDefAtrib = False
                Exit Sub
            End If
            If m_bModoEdicionAtr Then
                If BorrarPondeYvaloresLista = False Then
                    sdbgAtribPlant.Columns("SELECCION").Value = "-1"
                    sdbgAtribPlant.Columns("BOT").Value = "..."
                    sdbgAtribPlant.Columns("LIBRE").Value = 0
                    m_bModificarDefAtrib = False
                Else
                    m_bModificarDefAtrib = True
                End If
            End If
            
        Else
            sdbgAtribPlant.Columns("SELECCION").Value = "-1"
            sdbgAtribPlant.Columns("BOT").Value = "..."
            If sdbgAtribPlant.Columns("idtipo").Value = "4" Then
                sdbgAtribPlant.Columns("SELECCION").Value = "0"
                sdbgAtribPlant.Columns("BOT").Value = ""
                m_bModificarDefAtrib = False
            Else
                m_bModificarDefAtrib = True
                If m_bModoEdicionAtr Then
                    CargarListaValores
                End If
            End If
        End If
        
        If sdbgAtribPlant.Columns("LIBRE").Value = "-1" Or sdbgAtribPlant.Columns("LIBRE").Value = "TRUE" Then
            If sdbgAtribPlant.Columns("TIPO").Value = m_asAtrib(1) Or sdbgAtribPlant.Columns("TIPO").Value = m_asAtrib(0) Then
                sdbgAtribPlant.Columns("MIN").Locked = False
                sdbgAtribPlant.Columns("MIN").CellStyleSet "Normal"
                sdbgAtribPlant.Columns("MAX").Locked = False
                sdbgAtribPlant.Columns("MAX").CellStyleSet "Normal"
            Else
                sdbgAtribPlant.Columns("MIN").Locked = True
                sdbgAtribPlant.Columns("MIN").CellStyleSet "proceso"
                sdbgAtribPlant.Columns("MAX").Locked = True
                sdbgAtribPlant.Columns("MAX").CellStyleSet "proceso"
            End If
            
        Else
            sdbgAtribPlant.Columns("MIN").Value = ""
            sdbgAtribPlant.Columns("MAX").Value = ""
            sdbgAtribPlant.Columns("MIN").Locked = True
            sdbgAtribPlant.Columns("MIN").CellStyleSet "proceso"
            sdbgAtribPlant.Columns("MAX").Locked = True
            sdbgAtribPlant.Columns("MAX").CellStyleSet "proceso"
        End If
       sdbgAtribPlant.Refresh
    End If

    If sdbgAtribPlant.col < 0 Then Exit Sub
    
    If sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "SELECCION" Then
    
        If sdbgAtribPlant.Columns("SELECCION").Value = "-1" Then
            sdbgAtribPlant.Columns("LIBRE").Value = 0
            sdbgAtribPlant.Columns("MIN").Value = ""
            sdbgAtribPlant.Columns("MAX").Value = ""
            sdbgAtribPlant.Columns("BOT").Value = "..."
            If sdbgAtribPlant.Columns("idtipo").Value = "4" Then
                sdbgAtribPlant.Columns("SELECCION").Value = "0"
                sdbgAtribPlant.Columns("BOT").Value = ""
                 m_bModificarDefAtrib = False
                Exit Sub
            Else
                m_bModificarDefAtrib = True
            End If
            sdbgAtribPlant.Columns("MIN").Locked = True
            sdbgAtribPlant.Columns("MIN").CellStyleSet "proceso"
            sdbgAtribPlant.Columns("MAX").Locked = True
            sdbgAtribPlant.Columns("MAX").CellStyleSet "proceso"
            If m_bModoEdicionAtr Then
                CargarListaValores
            End If
        Else
        
            If BorrarPondeYvaloresLista = False Then
              sdbgAtribPlant.Columns("SELECCION").Value = "-1"
              sdbgAtribPlant.Columns("LIBRE").Value = 0
              sdbgAtribPlant.Columns("BOT").Value = "..."
              m_bModificarDefAtrib = False
           Else
              If frmLista.sdbgValores.Row > 0 Then
                m_bModificarDefAtrib = True
              End If
           End If
            sdbgAtribPlant.Columns("MIN").Locked = False
            sdbgAtribPlant.Columns("MIN").CellStyleSet "Normal"
            sdbgAtribPlant.Columns("MAX").Locked = False
            sdbgAtribPlant.Columns("MAX").CellStyleSet "Normal"
    
        End If
        sdbgAtribPlant.Refresh
    End If
    
    Dim bDato As Boolean
    
    If (sdbgAtribPlant.Columns(sdbgAtribPlant.col).Name = "PEDIDO") Then
        If Not sdbgAtribPlant.IsAddRow Then
        
            If UCase(sdbgAtribPlant.Columns("AMBITO").Value) = UCase(sdbgAtribPlant.Columns("AMBITOORIG").Value) Then
            
                g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).pedido = sdbgAtribPlant.Columns("PEDIDO").Value
            
                Exit Sub
            Else
            
        
                Select Case UCase(sdbgAtribPlant.Columns("AMBITO").Value)
                
                    Case UCase(m_asAtrib(4))
                    bDato = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).pedido
                    teserror = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).ModificarPedidoPlant(sdbgAtribPlant.Columns("PEDIDO").Value, TipoAmbitoProceso.AmbProceso)
                    
                    Case UCase(m_asAtrib(5))
                    bDato = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).pedido
                    teserror = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).ModificarPedidoPlant(sdbgAtribPlant.Columns("PEDIDO").Value, TipoAmbitoProceso.AmbGrupo)
                    
                    Case UCase(m_asAtrib(6))
                    bDato = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).pedido
                    teserror = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).ModificarPedidoPlant(sdbgAtribPlant.Columns("PEDIDO").Value, TipoAmbitoProceso.AmbItem)
            
                End Select
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    sdbgAtribPlant.Columns("PEDIDO").Value = bDato
                    
                End If
            End If
        End If
    End If
    
    
End Sub

Private Function BorrarPondeYvaloresLista() As Boolean
Dim irespuesta As Integer

    g_oPlantillaSeleccionada.ATRIBUTOS.Item(sdbgAtribPlant.Columns("ATRIB").Value).CargarListaDeValores
    If g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value)).ListaPonderacion.Count > 0 Then

            '''PREGUNTA ELIMINAR LISTA DE VALORES
        irespuesta = oMensajes.PreguntaEliminarListaValores
        
        If irespuesta = vbNo Then
           BorrarPondeYvaloresLista = False
           Exit Function
        End If
    End If
   
''''''''''''PREGUNTA DE CAMBIO DE PONDERACION
 
    If sdbgAtribPlant.Columns("TipoPond").Value <> "4" And sdbgAtribPlant.Columns("TipoPond").Value <> "0" Then
        If sdbgAtribPlant.Columns("TipoPond").Value = "1" And ((sdbgAtribPlant.Columns("idTipo").Value <> "2" And sdbgAtribPlant.Columns("idTipo").Value <> "3") Or (StrToDbl0(sdbgAtribPlant.Columns("idTipo").Value) <> g_oPlantillaSeleccionada.ATRIBUTOS.Item(sdbgAtribPlant.Columns("ATRIB").Value).Tipo)) Then
            irespuesta = oMensajes.PreguntaCambiarPonderacion
            If irespuesta = vbNo Then
               BorrarPondeYvaloresLista = False
               Exit Function
            End If
            sdbgAtribPlant.Columns("TipoPond").Value = "0"
        End If
        If sdbgAtribPlant.Columns("TipoPond").Value = "2" Then
            irespuesta = oMensajes.PreguntaCambiarPonderacion
            If irespuesta = vbNo Then
               BorrarPondeYvaloresLista = False
               Exit Function
            End If
            sdbgAtribPlant.Columns("TipoPond").Value = "0"
        End If
        If sdbgAtribPlant.Columns("TipoPond").Value = "3" And sdbgAtribPlant.Columns("idTipo").Value <> "2" Then
            irespuesta = oMensajes.PreguntaCambiarPonderacion
            If irespuesta = vbNo Then
               BorrarPondeYvaloresLista = False
               Exit Function
            End If
            sdbgAtribPlant.Columns("TipoPond").Value = "0"
    
        End If
        If sdbgAtribPlant.Columns("TipoPond").Value = "5" And sdbgAtribPlant.Columns("idTipo").Value <> "4" Then
            irespuesta = oMensajes.PreguntaCambiarPonderacion
            If irespuesta = vbNo Then
               BorrarPondeYvaloresLista = False
               Exit Function
            End If
            sdbgAtribPlant.Columns("TipoPond").Value = "0"
        End If
    End If
    
    BorrarPondeYvaloresLista = True
    
End Function


Private Sub sdbgAtribPlant_KeyDown(KeyCode As Integer, Shift As Integer)
'*********************************************************************
'*** Descripci�n: Captura la tecla Supr (Delete) y cuando          ***
'***              �sta es presionada se lanza el evento            ***
'***              Click asociado al bot�n eliminar para            ***
'***              borrar de la BD y de la grid las lineas          ***
'***              seleccionadas.                                   ***
'*** Par�metros:  KeyCode ::> Contiene el c�digo interno           ***
'***                          de la tecla pulsada.                 ***
'***              Shift   ::> Es la m�scara, sin uso en esta       ***
'***                          subrutina.                           ***
'*** Valor que devuelve: ----------                                ***
'*********************************************************************
    If KeyCode = vbKeyDelete Then
        If m_bModoEdicionAtr Then
            If cmdEliAtr.Enabled = True Then
                cmdEliAtr_Click
            End If
        End If
    End If
    
End Sub

Private Sub sdbgAtribPlant_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Or KeyAscii = vbKeySpace Then
        sdbgAtribPlant_BtnClick
    End If
    
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgAtribPlant.DataChanged = False Then
            
            sdbgAtribPlant.CancelUpdate
            sdbgAtribPlant.DataChanged = False
            
            If Not m_oGrupoEnEdicion Is Nothing Then
               Set m_oGrupoEnEdicion = Nothing
            End If
                      
            If Not sdbgAtribPlant.IsAddRow Then
                cmdAnyadirAtr.Enabled = True
                cmdEliAtr.Enabled = True
                cmdDeshacerAtr.Enabled = False
            Else
                cmdAnyadirAtr.Enabled = False
                cmdEliAtr.Enabled = False
                cmdDeshacerAtr.Enabled = False
            End If
                                   
        Else
        
            If sdbgAtribPlant.IsAddRow Then
                cmdAnyadirAtr.Enabled = True
                cmdEliAtr.Enabled = True
                cmdDeshacerAtr.Enabled = False
            End If
            
        End If
        
    End If
    
End Sub

Private Sub sdbgAtribPlant_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    If sdbgAtribPlant.Row = -1 Then Exit Sub
    
    If Not sdbgAtribPlant.IsAddRow Then
        
        If sdbgAtribPlant.DataChanged = False Then
            cmdAnyadirAtr.Enabled = True
            cmdEliAtr.Enabled = True
            cmdDeshacerAtr.Enabled = False
        End If
    Else
        If sdbgAtribPlant.DataChanged = True Then cmdDeshacerAtr.Enabled = True

        m_bActualizar = True
        If NullToStr(LastRow) <> NullToStr(sdbgAtribPlant.Bookmark) Then
            sdbgAtribPlant.col = 0 '3
            cmdDeshacerAtr.Enabled = False
        End If
        cmdAnyadirAtr.Enabled = False
        cmdEliAtr.Enabled = False
    End If
    
    
    If NullToStr(LastRow) <> NullToStr(sdbgAtribPlant.Bookmark) And sdbgAtribPlant.DataChanged = False Then
        If m_bModoEdicionAtr Then
            ProtegerFila
        End If
        
        If sdbgAtribPlant.Columns("idTIPO").Value = "4" Then
            sdbgAtribPlant.Columns("MIN").Locked = True
            sdbgAtribPlant.Columns("MAX").Locked = True
            sdbgAtribPlant.Columns("LIBRE").Locked = True
            sdbgAtribPlant.Columns("SELECCION").Locked = True
            sdbgAtribPlant.Columns("MIN").CellStyleSet "Gris", sdbgAtribPlant.Row
            sdbgAtribPlant.Columns("MAX").CellStyleSet "Gris", sdbgAtribPlant.Row
            sdbgAtribPlant.Columns("LIBRE").CellStyleSet "Gris", sdbgAtribPlant.Row
            sdbgAtribPlant.Columns("SELECCION").CellStyleSet "Gris", sdbgAtribPlant.Row
            sdbgAtribPlant.Columns("LIBRE").Value = "0"
        End If
        
        If Not sdbgAtribPlant.Columns("OP").Locked Then
            sdbddSigno.Enabled = True
        Else
            sdbddSigno.Enabled = False
        End If
        If Not sdbgAtribPlant.Columns("AMBITO").Locked Then
            sdbddAmbito.Enabled = True
        Else
            sdbddAmbito.Enabled = False
        End If
            
        If Not sdbgAtribPlant.Columns("APLIC_PREC").Locked Then
            sdbddAplicarA.Enabled = True
        Else
            sdbddAplicarA.Enabled = False
        End If
        
    Else
        If m_bModoEdicionAtr = True Then
            If sdbgAtribPlant.Columns("IDTIPO").Value <> "2" Then
                sdbgAtribPlant.Columns("OP").Locked = True
                sdbgAtribPlant.Columns("APLIC_PREC").Locked = True
            Else
                sdbgAtribPlant.Columns("OP").Locked = False
                sdbgAtribPlant.Columns("APLIC_PREC").Locked = False
            End If
    
            If Not sdbgAtribPlant.Columns("OP").Locked Then
                sdbddSigno.Enabled = True
            Else
                sdbddSigno.Enabled = False
            End If
    
            If Not sdbgAtribPlant.Columns("APLIC_PREC").Locked Then
                sdbddAplicarA.Enabled = True
            Else
                sdbddAplicarA.Enabled = False
            End If
        End If
    End If
    

    
    If m_bModoEdicionAtr = True Then
        If sdbgAtribPlant.Columns("CODGRUPO").Value <> "" Or g_sCodGRP = "" Then
            If sdbgAtribPlant.Columns("OP").Value <> "" And sdbgAtribPlant.Columns("APLIC_PREC").Value <> "" Then
                sdbgAtribPlant.Columns("DEF_PREC").Locked = False
            Else
                sdbgAtribPlant.Columns("DEF_PREC").Locked = True
            End If
        End If
    End If

End Sub

''' <summary>
''' Hace que una fila del grid de atributos no sea modificable
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdModoEdicionAtr_Click,sdbgAtribPlant_RowColChange</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub ProtegerFila()
Dim i As Integer

    If sdbgAtribPlant.Columns("CODGRUPO").Value = "" And g_sCodGRP <> "" Then
        For i = 0 To sdbgAtribPlant.Cols - 1
            sdbgAtribPlant.Columns(i).Locked = True
        Next
        sdbgAtribPlant.Columns("BOT").Locked = False
        sdbgAtribPlant.Columns("POND").Locked = False
    Else
        sdbgAtribPlant.Columns("INTERNO").Locked = False
        sdbgAtribPlant.Columns("PEDIDO").Locked = False
        sdbgAtribPlant.Columns("LIBRE").Locked = False
        sdbgAtribPlant.Columns("MIN").Locked = False
        sdbgAtribPlant.Columns("MAX").Locked = False
        sdbgAtribPlant.Columns("AMBITO").Locked = False
        sdbgAtribPlant.Columns("SELECCION").Locked = False
        sdbgAtribPlant.Columns("OBL").Locked = False
        sdbgAtribPlant.Columns("VALORBAJO").Locked = False
        If (sdbgAtribPlant.Columns("idTIPO").Value = "2" Or sdbgAtribPlant.Columns("idTIPO").Value = "3") And sdbgAtribPlant.Columns("LIBRE").Value = -1 Then
            sdbgAtribPlant.Columns("MIN").Locked = False
            sdbgAtribPlant.Columns("MAX").Locked = False
            If sdbgAtribPlant.Columns("idTIPO").Value = "2" Then
                sdbgAtribPlant.Columns("MIN").Alignment = ssCaptionAlignmentRight
                sdbgAtribPlant.Columns("MAX").Alignment = ssCaptionAlignmentRight
                sdbgAtribPlant.Columns("MIN").CellStyleSet "Normal", sdbgAtribPlant.Row
                sdbgAtribPlant.Columns("MAX").CellStyleSet "Normal", sdbgAtribPlant.Row
            End If
        Else
            sdbgAtribPlant.Columns("MIN").Locked = True
            sdbgAtribPlant.Columns("MAX").Locked = True
        End If
        
        If sdbgAtribPlant.Columns("IDTIPO").Value <> "2" Then
            sdbgAtribPlant.Columns("OP").Locked = True
            sdbgAtribPlant.Columns("APLIC_PREC").Locked = True
            sdbgAtribPlant.Columns("DEF_PREC").Locked = True
            sdbddAplicarA.Enabled = False
        Else
            sdbgAtribPlant.Columns("OP").Locked = False
            sdbgAtribPlant.Columns("APLIC_PREC").Locked = False
            sdbddAplicarA.Enabled = True
        End If
    End If

End Sub

Private Sub sdbgAtribPlant_RowLoaded(ByVal Bookmark As Variant)

    If g_sCodGRP <> "" Then 'Definidos a nivel de grupo + definidos a nivel de proceso
        If sdbgAtribPlant.Columns("CODGRUPO").Text = "" Then
            sdbgAtribPlant.Columns("INTERNO").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("COD").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("NOMBRE").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("TIPO").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("LIBRE").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("SELECCION").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("AMBITO").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("OBL").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("VALORBAJO").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("OP").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("APLIC_PREC").CellStyleSet "Proceso"
            sdbgAtribPlant.Columns("DEF_PREC").CellStyleSet "Proceso"

        Else
            sdbgAtribPlant.Columns("INTERNO").CellStyleSet "Normal"
            sdbgAtribPlant.Columns("COD").CellStyleSet "Azul"
            sdbgAtribPlant.Columns("NOMBRE").CellStyleSet "Azul"
            sdbgAtribPlant.Columns("TIPO").CellStyleSet "Azul"
            sdbgAtribPlant.Columns("LIBRE").CellStyleSet "Normal"
            sdbgAtribPlant.Columns("SELECCION").CellStyleSet "Normal"
            sdbgAtribPlant.Columns("AMBITO").CellStyleSet "Normal"
            sdbgAtribPlant.Columns("OBL").CellStyleSet "Normal"
            sdbgAtribPlant.Columns("VALORBAJO").CellStyleSet "Normal"
        End If
        
    
    Else 'Definidos a nivel de grupo
    
        sdbgAtribPlant.Columns("INTERNO").CellStyleSet "Normal"
        sdbgAtribPlant.Columns("COD").CellStyleSet "Azul"
        sdbgAtribPlant.Columns("NOMBRE").CellStyleSet "Azul"
        sdbgAtribPlant.Columns("TIPO").CellStyleSet "Azul"
        sdbgAtribPlant.Columns("LIBRE").CellStyleSet "Normal"
        sdbgAtribPlant.Columns("SELECCION").CellStyleSet "Normal"
        sdbgAtribPlant.Columns("AMBITO").CellStyleSet "Normal"
        sdbgAtribPlant.Columns("OBL").CellStyleSet "Normal"
        sdbgAtribPlant.Columns("VALORBAJO").CellStyleSet "Normal"
            
    End If
    
    If sdbgAtribPlant.Columns("LIBRE").Value = -1 Then
        If (sdbgAtribPlant.Columns("idTIPO").Value = "2" Or sdbgAtribPlant.Columns("idTIPO").Value = "3") Then
            sdbgAtribPlant.Columns("MIN").CellStyleSet "Normal"
            sdbgAtribPlant.Columns("MAX").CellStyleSet "Normal"
        Else
            sdbgAtribPlant.Columns("MIN").CellStyleSet "Gris"
            sdbgAtribPlant.Columns("MAX").CellStyleSet "Gris"
        End If
    Else
        If sdbgAtribPlant.Columns("SELECCION").Value = -1 Then
           sdbgAtribPlant.Columns("BOT").Value = "..."
        End If
    
        sdbgAtribPlant.Columns("MIN").CellStyleSet "Gris"
        sdbgAtribPlant.Columns("MAX").CellStyleSet "Gris"
    End If
    
     If sdbgAtribPlant.Columns("idTIPO").Value = 4 Then
         sdbgAtribPlant.Columns("LIBRE").CellStyleSet "Gris"
         sdbgAtribPlant.Columns("SELECCION").CellStyleSet "Gris"
         sdbgAtribPlant.Columns("LIBRE").Value = "0"
         sdbgAtribPlant.Columns("SELECCION").Value = "0"
     End If

    If sdbgAtribPlant.Columns("TipoPond").Value <> "0" Then
        sdbgAtribPlant.Columns("POND").CellStyleSet "Ponderacion"
    Else
        sdbgAtribPlant.Columns("POND").CellStyleSet ""
    End If
    
    If Trim(sdbgAtribPlant.Columns("HIDENDESCR").Value) <> "" Then
        sdbgAtribPlant.Columns("DESCR").Value = "..."
    End If
End Sub


''' <summary>
''' Evento producido al cambiar de columna en el grid de atributos de especificaci�n
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtributosEsp_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    If Not sdbgAtributosEsp.IsAddRow Then
        If sdbgAtributosEsp.DataChanged = False Then
            cmdDeshacerAtrEsp.Enabled = False
        End If
    Else
        If sdbgAtributosEsp.DataChanged = True Then cmdDeshacerAtrEsp.Enabled = True
    End If
       
    If NullToStr(LastRow) <> NullToStr(sdbgAtributosEsp.Bookmark) Or sdbgAtributosEsp.col = 8 Then
        If sdbgAtributosEsp.Columns("TIPO_INTRO").Value = 0 Then 'Libre
            If sdbgAtributosEsp.Columns("TIPO_DATOS").Value = TipoBoolean Then
                sdbddValor.RemoveAll
                sdbddValor.AddItem ""
                sdbgAtributosEsp.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                sdbddValor.Enabled = True
            Else
                sdbgAtributosEsp.Columns("VALOR").DropDownHwnd = 0
                sdbddValor.Enabled = False
            
                If sdbgAtributosEsp.Columns("TIPO_DATOS").Value = 1 Then
                    sdbgAtributosEsp.Columns("VALOR").Style = ssStyleEditButton
                End If
            End If
        Else 'Lista
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            sdbgAtributosEsp.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
            sdbddValor.Enabled = True
            sdbddValor.DroppedDown = True
            sdbddValor_DropDown
            sdbddValor.DroppedDown = True
        End If

    End If

End Sub

Private Sub sdbgAtributosEsp_RowLoaded(ByVal Bookmark As Variant)

    If Trim(sdbgAtributosEsp.Columns("HIDENDESCR").Value) <> "" Then
        sdbgAtributosEsp.Columns("DESCR").Value = "..."
    End If

End Sub

Private Sub sdbgGrupos_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgGrupos.Rows = 0) Then
        Exit Sub
    End If
    If Me.Visible Then sdbgGrupos.SetFocus
    sdbgGrupos.Bookmark = sdbgGrupos.RowBookmark(sdbgGrupos.Row)
End Sub

Private Sub sdbgGrupos_AfterInsert(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If m_bAnyaError = False Then
        cmdAnyadir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
End Sub

Private Sub sdbgGrupos_AfterUpdate(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
    If m_bAnyaError = False And m_bModError = False Then
        cmdAnyadir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
End Sub

Private Sub sdbgGrupos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>
''' Evento producido al hacer cambios obre el grid de grupos
''' </summary>
''' <param name="Cancel">Cancelacion de la actualizacion </param>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgGrupos_BeforeUpdate(Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim iNumSobre As Integer
Dim vbm As Variant
Dim iSobre As String
Dim j As Integer
Dim bMayor As Boolean
Dim bEncontrado As Boolean
        
    Cancel = False
    m_bValError = False
    
    If Not sdbgGrupos.IsAddRow Then
        If m_oGrupoEnEdicion Is Nothing Then Exit Sub
    End If
    
    If Not m_bActualizar Then Exit Sub
    
    cmdDeshacer.Enabled = True
    
    If Trim(sdbgGrupos.Columns("COD").Text) = "" Then
        oMensajes.NoValido sdbgGrupos.Columns("COD").caption
        Cancel = True
        GoTo Salir
    End If
        
    If Not NombreDeGrupoValido(sdbgGrupos.Columns("COD").Text) Then
        oMensajes.CodigoGrupoNoValido sdbgGrupos.Columns("COD").caption
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgGrupos.Columns("NOM").Text) = "" Then
        oMensajes.NoValido sdbgGrupos.Columns("NOM").caption
        Cancel = True
        GoTo Salir
    End If
        
    If sdbgGrupos.Columns("SOBRE").Visible = True Then
        If Trim(sdbgGrupos.Columns("SOBRE").Text) = "" Then
            oMensajes.NoValido sdbgGrupos.Columns("SOBRE").caption
            Cancel = True
            GoTo Salir
        End If
        If Not IsNumeric(sdbgGrupos.Columns("SOBRE").Text) Then
            oMensajes.NoValido sdbgGrupos.Columns("SOBRE").caption
            Cancel = True
            GoTo Salir
        End If
        'El n�mero de sobre tiene que ser uno ya existente o el siguiente correlativo
        'al �ltimo n�mero de sobre introducido:
        iNumSobre = 1
        bMayor = True
        If sdbgGrupos.Rows = 1 Then
            If sdbgGrupos.Columns("SOBRE").Value = 1 Then
                bMayor = False
            End If
        Else
            For j = 0 To sdbgGrupos.Rows - 1
                bEncontrado = False
                vbm = sdbgGrupos.AddItemBookmark(j)
                If IsEmpty(vbm) Then
                    iSobre = sdbgGrupos.Columns("SOBRE").Value
                Else
                    If sdbgGrupos.Columns("COD").CellValue(vbm) <> sdbgGrupos.Columns("COD").Value Then
                        iSobre = sdbgGrupos.Columns("SOBRE").CellValue(vbm)
                    Else
                        iSobre = sdbgGrupos.Columns("SOBRE").Value
                    End If
                End If
                If iSobre <> "" Then
                    For i = 0 To sdbgGrupos.Rows - 1
                        vbm = sdbgGrupos.AddItemBookmark(i)
                        If IsEmpty(vbm) Then
                            iSobre = sdbgGrupos.Columns("SOBRE").Value
                        Else
                            If sdbgGrupos.Columns("COD").CellValue(vbm) <> sdbgGrupos.Columns("COD").Value Then
                                iSobre = sdbgGrupos.Columns("SOBRE").CellValue(vbm)
                            Else
                                iSobre = sdbgGrupos.Columns("SOBRE").Value
                            End If
                        End If
                        If iSobre <> "" Then
                            If iSobre > iNumSobre Then
                                bMayor = True
                            End If
                            If iSobre = iNumSobre Then
                                bEncontrado = True
                                bMayor = False
                                Exit For
                            End If
                        End If
                    Next i
                    If bEncontrado = True Then
                        iNumSobre = iNumSobre + 1
                    Else
                        If bMayor = True Then
                            Exit For
                        End If
                    End If
                End If
            Next j
        End If
        If bMayor = True Then
            oMensajes.NoValido sdbgGrupos.Columns("SOBRE").caption
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If sdbgGrupos.IsAddRow Then
        If ComprobarActualizarPlantilla = False Then
            Cancel = True
            GoTo Salir
        End If
        
        Screen.MousePointer = vbHourglass
        Set m_oGrupoEnEdicion = Nothing
        Set m_oGrupoEnEdicion = oFSGSRaiz.generar_cgrupo
        m_oGrupoEnEdicion.Codigo = sdbgGrupos.Columns("COD").Value
        m_oGrupoEnEdicion.Den = sdbgGrupos.Columns("NOM").Value
        m_oGrupoEnEdicion.Descripcion = txtGrupoDescr.Text
        m_oGrupoEnEdicion.IDPlantilla = g_oPlantillaSeleccionada.Id
        'Definir en el nuevo grupo todo lo que la plantilla tenga definido a nivel de grupo
        If g_oPlantillaSeleccionada.Destino = EnGrupo Then
            m_oGrupoEnEdicion.DefDestino = True
        End If
        If g_oPlantillaSeleccionada.FormaPago = EnGrupo Then
            m_oGrupoEnEdicion.DefFormaPago = True
        End If
        If g_oPlantillaSeleccionada.FechaSuministro = EnGrupo Then
            m_oGrupoEnEdicion.DefFechasSum = True
        End If
        If g_oPlantillaSeleccionada.ProveedorAct = EnGrupo Then
            m_oGrupoEnEdicion.DefProveActual = True
        End If
        If g_oPlantillaSeleccionada.EspecGrupo Then
            m_oGrupoEnEdicion.DefEspecificaciones = True
        End If
        If g_oPlantillaSeleccionada.DistribUON = EnGrupo Then
            m_oGrupoEnEdicion.DefDistribUON = True
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = EnGrupo Then
            m_oGrupoEnEdicion.DefPresAnualTipo1 = True
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = EnGrupo Then
            m_oGrupoEnEdicion.DefPresAnualTipo2 = True
        End If
        If g_oPlantillaSeleccionada.PresTipo1 = EnGrupo Then
            m_oGrupoEnEdicion.DefPresTipo1 = True
        End If
        If g_oPlantillaSeleccionada.PresTipo2 = EnGrupo Then
            m_oGrupoEnEdicion.DefPresTipo2 = True
        End If
        If m_bMostrarSolicitud = True Then
            If g_oPlantillaSeleccionada.Solicitud = EnGrupo Then
                m_oGrupoEnEdicion.DefSolicitud = True
            End If
        End If
        If sdbgGrupos.Columns("SOBRE").Visible = True Then
            m_oGrupoEnEdicion.Sobre = sdbgGrupos.Columns("SOBRE").Value
        Else
            m_oGrupoEnEdicion.Sobre = Null
        End If
        Set g_oIBaseDatos = m_oGrupoEnEdicion
        teserror = g_oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            m_bAnyaError = True
            cmdDeshacer_Click
            If Me.Visible Then sdbgGrupos.SetFocus
            Exit Sub
        End If
        If m_bMostrarSolicitud = True Then
            g_oPlantillaSeleccionada.Grupos.Add Nothing, sdbgGrupos.Columns("COD").Text, sdbgGrupos.Columns("NOM").Text, , txtGrupoDescr.Text, m_oGrupoEnEdicion.DefDestino, , m_oGrupoEnEdicion.DefFechasSum, , , m_oGrupoEnEdicion.DefFormaPago, , m_oGrupoEnEdicion.DefProveActual, , m_oGrupoEnEdicion.DefDistribUON, , m_oGrupoEnEdicion.DefPresAnualTipo1, , m_oGrupoEnEdicion.DefPresAnualTipo2, , m_oGrupoEnEdicion.DefPresTipo1, , m_oGrupoEnEdicion.DefPresTipo2, , m_oGrupoEnEdicion.DefEspecificaciones, , , , , m_oGrupoEnEdicion.FECACT, , g_oPlantillaSeleccionada.Id, , , , , , m_oGrupoEnEdicion.DefSolicitud, , StrToNull(sdbgGrupos.Columns("SOBRE").Text)
        Else
            g_oPlantillaSeleccionada.Grupos.Add Nothing, sdbgGrupos.Columns("COD").Text, sdbgGrupos.Columns("NOM").Text, , txtGrupoDescr.Text, m_oGrupoEnEdicion.DefDestino, , m_oGrupoEnEdicion.DefFechasSum, , , m_oGrupoEnEdicion.DefFormaPago, , m_oGrupoEnEdicion.DefProveActual, , m_oGrupoEnEdicion.DefDistribUON, , m_oGrupoEnEdicion.DefPresAnualTipo1, , m_oGrupoEnEdicion.DefPresAnualTipo2, , m_oGrupoEnEdicion.DefPresTipo1, , m_oGrupoEnEdicion.DefPresTipo2, , m_oGrupoEnEdicion.DefEspecificaciones, , , , , m_oGrupoEnEdicion.FECACT, , g_oPlantillaSeleccionada.Id, , , , , , , , StrToNull(sdbgGrupos.Columns("SOBRE").Text)
        End If
        sdbcSeleccion.AddItem sdbgGrupos.Columns("COD").Text & Chr(m_lSeparador) & sdbgGrupos.Columns("NOM").Text
                
        RegistrarAccion ACCPlantGrupoAnya, "Nuevo grupo A�adido: " & sdbgGrupos.Columns("COD").Text
        sdbcGrupoEsp.AddItem sdbgGrupos.Columns("COD").Text & Chr(m_lSeparador) & sdbgGrupos.Columns("NOM").Text
    Else 'Modificar
        
        If m_oGrupoEnEdicion.Sobre <> sdbgGrupos.Columns("SOBRE").Value Then
            If ComprobarActualizarPlantilla = False Then
                Cancel = True
                GoTo Salir
            End If
        End If
        
        m_bModError = False
        
        Screen.MousePointer = vbHourglass
        Set g_oIBaseDatos = m_oGrupoEnEdicion
        m_oGrupoEnEdicion.Den = sdbgGrupos.Columns("NOM").Value
        m_oGrupoEnEdicion.Descripcion = txtGrupoDescr.Text
        If sdbgGrupos.Columns("SOBRE").Visible = True Then
            m_oGrupoEnEdicion.Sobre = sdbgGrupos.Columns("SOBRE").Value
        Else
            m_oGrupoEnEdicion.Sobre = Null
        End If
        
        teserror = g_oIBaseDatos.FinalizarEdicionModificando
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            If Me.Visible Then sdbgGrupos.SetFocus
            sdbgGrupos.DataChanged = False
            m_bModError = True
        Else
            CargarComboGrupos
            CargarComboGruposEsp
            cmdDeshacer.Enabled = False
        End If
                
        Set g_oIBaseDatos = Nothing
        
        RegistrarAccion ACCPlantGrupoMod, "Grupo Modificado: " & m_oGrupoEnEdicion.Den
        
    End If
    
    'Set m_oGrupoEnEdicion = Nothing
    
    Screen.MousePointer = vbNormal
    Exit Sub

Salir:
        
    m_bValError = Cancel
    If Me.Visible Then sdbgGrupos.SetFocus
    
End Sub

Private Sub sdbgGrupos_Change()
''' * Objetivo: Iniciar la edicion, si no hay errores
''' * Objetivo: Atencion si han cambiado los datos
    
Dim teserror As TipoErrorSummit

    If cmdDeshacer.Enabled = False Then
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
        cmdCodigo.Enabled = False
    End If
    
    If g_oPlantillaSeleccionada.Grupos.Item(CStr(sdbgGrupos.Columns("COD").Value)) Is Nothing Then Exit Sub
        
    If sdbgGrupos.IsAddRow Then Exit Sub
            
    Screen.MousePointer = vbHourglass
    Set m_oGrupoEnEdicion = Nothing
    Set m_oGrupoEnEdicion = g_oPlantillaSeleccionada.Grupos.Item(CStr(sdbgGrupos.Columns("COD").Text))
      
    Set g_oIBaseDatos = m_oGrupoEnEdicion
    teserror = g_oIBaseDatos.IniciarEdicion
    If teserror.NumError = TESInfModificada Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        sdbgGrupos.DataChanged = False
        sdbgGrupos.Columns("COD").Value = m_oGrupoEnEdicion.Codigo
        sdbgGrupos.Columns("NOM").Value = m_oGrupoEnEdicion.Den
        If sdbgGrupos.Columns("SOBRE").Visible = True Then
            sdbgGrupos.Columns("SOBRE").Value = m_oGrupoEnEdicion.Sobre
        End If
        teserror.NumError = TESnoerror
    End If
        
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        If Me.Visible Then sdbgGrupos.SetFocus
    End If
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgGrupos_HeadClick(ByVal ColIndex As Integer)
''' * Objetivo: Ordenar el grid segun la columna
Dim oGrupo As CGrupo
Dim sHeadCaption As String
Dim bprimera As Boolean
    
    If m_bModoEdicion Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgGrupos.Columns(ColIndex).caption
    sdbgGrupos.Columns(ColIndex).caption = m_sIdiOrdenando & "..."

    Select Case ColIndex
        Case 0
            g_oPlantillaSeleccionada.CargarTodosLosGrupos False, OrdPlantillaPorCod
        Case 1
            g_oPlantillaSeleccionada.CargarTodosLosGrupos False, OrdPlantillaPorDen
        Case 3
            g_oPlantillaSeleccionada.CargarTodosLosGrupos False, OrdPlantillaPorSobre
    End Select
    
    sdbgGrupos.Columns(ColIndex).caption = sHeadCaption
    
    sdbgGrupos.RemoveAll
    
    bprimera = True
          
    For Each oGrupo In g_oPlantillaSeleccionada.Grupos
        sdbgGrupos.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den & Chr(m_lSeparador) & NullToStr(oGrupo.Descripcion) & Chr(m_lSeparador) & NullToStr(oGrupo.Sobre)
        If bprimera Then
            txtGrupoDescr.Text = NullToStr(oGrupo.Descripcion)
            bprimera = False
        End If
    Next
    
    sdbgGrupos.ReBind

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgGrupos_InitColumnProps()
    sdbgGrupos.Columns("COD").FieldLen = basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE
End Sub

Private Sub sdbgGrupos_KeyDown(KeyCode As Integer, Shift As Integer)
'*********************************************************************
'*** Descripci�n: Captura la tecla Supr (Delete) y cuando          ***
'***              �sta es presionada se lanza el evento            ***
'***              Click asociado al bot�n eliminar para            ***
'***              borrar de la BD y de la grid las lineas          ***
'***              seleccionadas.                                   ***
'*** Par�metros:  KeyCode ::> Contiene el c�digo interno           ***
'***                          de la tecla pulsada.                 ***
'***              Shift   ::> Es la m�scara, sin uso en esta       ***
'***                          subrutina.                           ***
'*** Valor que devuelve: ----------                                ***
'*********************************************************************
    If KeyCode = vbKeyDelete Then
        If m_bModoEdicion Then
            If cmdEliminar.Enabled = True Then
                cmdEliminar_Click
            End If
        End If
    End If
End Sub

Private Sub sdbgGrupos_KeyPress(KeyAscii As Integer)
    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgGrupos.DataChanged = False Then
            
            sdbgGrupos.CancelUpdate
            sdbgGrupos.DataChanged = False
            
            If Not m_oGrupoEnEdicion Is Nothing Then
                g_oIBaseDatos.CancelarEdicion
                Set g_oIBaseDatos = Nothing
                Set m_oGrupoEnEdicion = Nothing
            End If
           
            If Not sdbgGrupos.IsAddRow Then
                cmdAnyadir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
            Else
                cmdAnyadir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = False
            End If
                                   
        Else
        
            If sdbgGrupos.IsAddRow Then
           
            End If
                
            cmdAnyadir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
            cmdCodigo.Enabled = True
            
        End If
        
    End If

End Sub

Private Sub sdbgGrupos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
   
    If Not sdbgGrupos.IsAddRow Then
        sdbgGrupos.Columns("COD").Locked = True
        If sdbgGrupos.DataChanged = False Then
            cmdAnyadir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
            cmdCodigo.Enabled = True
        End If
        
        If Not g_oPlantillaSeleccionada.Grupos.Item(sdbgGrupos.Columns("COD").Text) Is Nothing Then 'cuando deshacemos
            txtGrupoDescr.Text = NullToStr(g_oPlantillaSeleccionada.Grupos.Item(sdbgGrupos.Columns("COD").Text).Descripcion)
        End If
    Else
        If sdbgGrupos.DataChanged = True Then cmdDeshacer.Enabled = True
        sdbgGrupos.Columns("COD").Locked = False
        m_bActualizar = True
        If Not IsNull(LastRow) Then
            If NullToStr(LastRow) <> NullToStr(sdbgGrupos.Bookmark) Then
                sdbgGrupos.col = 0
                cmdDeshacer.Enabled = False
            End If
            txtGrupoDescr.Text = ""
        End If
    End If
    
     m_bActualizar = True
        
End Sub

Private Sub sdbgPlantillas_BtnClick()

    If sdbgPlantillas.Columns("BOTONGRUP").Value <> "..." Then Exit Sub
    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
    If g_oPlantillaSeleccionada.Grupos.Count = 0 Then Exit Sub
    
    m_iDatoActual = sdbgPlantillas.Columns("INDICE").Value
    
    Set m_oGrupos = g_oPlantillaSeleccionada.Grupos

    If cmdAceptar.Visible Then
        frmPlantillasProceGrupo.g_bModoEdicion = True
    Else
        frmPlantillasProceGrupo.g_bModoEdicion = False
    End If
    frmPlantillasProceGrupo.g_lIndice = sdbgPlantillas.Columns("INDICE").Value
    
    frmPlantillasProceGrupo.Show vbModal

End Sub

Private Sub sdbgPlantillas_Change()
Dim oGrupo As CGrupo
Dim bGrupoNoDef As Boolean

    If Not m_bModoEdicionPlant Then Exit Sub
    If m_bCancelarConfGR = True Then Exit Sub
    
    m_iDatoActual = sdbgPlantillas.Columns("INDICE").Value
    
    Select Case sdbgPlantillas.Columns("INDICE").Value
        
        Case "0", "1", "2" 'DEST,PAG,FECSUM
            Select Case sdbgPlantillas.col
                Case 0 'NO DEF
                        sdbgPlantillas.Columns("USAR").Value = "-1"
                        sdbgPlantillas.Update
                        Exit Sub
                Case 2 'PROCESO
                        sdbgPlantillas.Columns("PROCE").Value = "-1"
                        sdbgPlantillas.Columns("GRUPO").Value = "0"
                        sdbgPlantillas.Columns("BOTONGRUP").Value = ""
                        sdbgPlantillas.Columns("ITEM").Value = "0"
                        bGrupoNoDef = True
                Case 3 'GRUPO
                        If sdbgPlantillas.Columns("GRUPO").Value = "0" Then
                            sdbgPlantillas.Columns("GRUPO").Value = "-1"
                            Exit Sub
                        End If
                        sdbgPlantillas.Columns("GRUPO").Value = "-1"
                        sdbgPlantillas.Columns("BOTONGRUP").Value = "..."
                        sdbgPlantillas.Columns("PROCE").Value = "0"
                        sdbgPlantillas.Columns("ITEM").Value = "0"
                        sdbgPlantillas_BtnClick
                        bGrupoNoDef = False
                Case 5 'ITEM
                        sdbgPlantillas.Columns("ITEM").Value = "-1"
                        sdbgPlantillas.Columns("PROCE").Value = "0"
                        sdbgPlantillas.Columns("GRUPO").Value = "0"
                        sdbgPlantillas.Columns("BOTONGRUP").Value = ""
                        bGrupoNoDef = True
            End Select
        
        Case "3", "5", "6", "7", "8", "9", "10" 'PROVE,DIST,PRESUP,SOLICIT
            Select Case sdbgPlantillas.col
                Case 0 'NO DEF
                        If sdbgPlantillas.Columns("USAR").Value = "-1" Then
                            sdbgPlantillas.Columns("ITEM").Value = "-1"
                        End If
                        If sdbgPlantillas.Columns("INDICE").Value = "5" Then 'DIST
                            sdbgPlantillas.Columns("USAR").Value = "-1"
                            sdbgPlantillas.Update
                            Exit Sub
                        End If
                        If sdbgPlantillas.Columns("INDICE").Value = "6" Then 'PANU1
                            If gParametrosGenerales.gbOBLPP Then
                                sdbgPlantillas.Columns("USAR").Value = "-1"
                                sdbgPlantillas.Update
                                Exit Sub
                            End If
                        End If
                        If sdbgPlantillas.Columns("INDICE").Value = "7" Then 'PANU2
                            If gParametrosGenerales.gbOBLPC Then
                                sdbgPlantillas.Columns("USAR").Value = "-1"
                                sdbgPlantillas.Update
                                Exit Sub
                            End If
                        End If
                        If sdbgPlantillas.Columns("INDICE").Value = "8" Then 'PRES1
                            If gParametrosGenerales.gbOBLPres3 Then
                                sdbgPlantillas.Columns("USAR").Value = "-1"
                                sdbgPlantillas.Update
                                Exit Sub
                            End If
                        End If
                        If sdbgPlantillas.Columns("INDICE").Value = "9" Then 'PRES2
                            If gParametrosGenerales.gbOBLPres4 Then
                                sdbgPlantillas.Columns("USAR").Value = "-1"
                                sdbgPlantillas.Update
                                Exit Sub
                            End If
                        End If
                        
                        bGrupoNoDef = True
                        'si la fila seleccionada es Solicitud de compra mostraru o cultar frame correspondiente seg�n estado de check
                        'Tarea 3376
                        If sdbgPlantillas.Columns("USAR").Value = "10" Then
                            MostrarOcultarFraSolCompra (sdbgPlantillas.Columns.Item("Usar").Value)
                        End If
                Case 2 'PROCESO
                        sdbgPlantillas.Columns("USAR").Value = "-1"
                        If sdbgPlantillas.Columns("PROCE").Value = "0" Then
                            If sdbgPlantillas.Columns("INDICE").Value = "5" Then 'DIST
                                sdbgPlantillas.Columns("PROCE").Value = "-1"
                                sdbgPlantillas.Update
                                Exit Sub
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "6" Then 'PANU1
                                If gParametrosGenerales.gbOBLPP Then
                                    sdbgPlantillas.Columns("PROCE").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "7" Then 'PANU2
                                If gParametrosGenerales.gbOBLPC Then
                                    sdbgPlantillas.Columns("PROCE").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "8" Then 'PRES1
                                If gParametrosGenerales.gbOBLPres3 Then
                                    sdbgPlantillas.Columns("PROCE").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "9" Then 'PRES2
                                If gParametrosGenerales.gbOBLPres4 Then
                                    sdbgPlantillas.Columns("PROCE").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If

                            sdbgPlantillas.Columns("USAR").Value = "0"
                        End If
                        sdbgPlantillas.Columns("GRUPO").Value = "0"
                        sdbgPlantillas.Columns("BOTONGRUP").Value = ""
                        sdbgPlantillas.Columns("ITEM").Value = "0"
                        bGrupoNoDef = True
                        
                Case 3 'GRUPO
                        sdbgPlantillas.Columns("USAR").Value = "-1"
                        If sdbgPlantillas.Columns("GRUPO").Value = "0" Then
                            If sdbgPlantillas.Columns("INDICE").Value = "5" Then 'DIST
                                sdbgPlantillas.Columns("GRUPO").Value = "-1"
                                sdbgPlantillas.Update
                                Exit Sub
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "6" Then 'PANU1
                                If gParametrosGenerales.gbOBLPP Then
                                    sdbgPlantillas.Columns("GRUPO").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "7" Then 'PANU2
                                If gParametrosGenerales.gbOBLPC Then
                                    sdbgPlantillas.Columns("GRUPO").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "8" Then 'PRES1
                                If gParametrosGenerales.gbOBLPres3 Then
                                    sdbgPlantillas.Columns("GRUPO").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "9" Then 'PRES2
                                If gParametrosGenerales.gbOBLPres4 Then
                                    sdbgPlantillas.Columns("GRUPO").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If

                            sdbgPlantillas.Columns("USAR").Value = "0"
                            sdbgPlantillas.Columns("BOTONGRUP").Value = ""
                            bGrupoNoDef = True
                        Else
                            sdbgPlantillas.Columns("BOTONGRUP").Value = "..."
                            bGrupoNoDef = False
                            sdbgPlantillas_BtnClick
                        End If
                        sdbgPlantillas.Columns("PROCE").Value = "0"
                        sdbgPlantillas.Columns("ITEM").Value = "0"
                        
                Case 5 'ITEM
                        sdbgPlantillas.Columns("USAR").Value = "-1"
                        If sdbgPlantillas.Columns("ITEM").Value = "0" Then
                            If sdbgPlantillas.Columns("INDICE").Value = "5" Then 'DIST
                                sdbgPlantillas.Columns("ITEM").Value = "-1"
                                sdbgPlantillas.Update
                                Exit Sub
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "6" Then 'PANU1
                                If gParametrosGenerales.gbOBLPP Then
                                    sdbgPlantillas.Columns("ITEM").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "7" Then 'PANU2
                                If gParametrosGenerales.gbOBLPC Then
                                    sdbgPlantillas.Columns("ITEM").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "8" Then 'PRES1
                                If gParametrosGenerales.gbOBLPres3 Then
                                    sdbgPlantillas.Columns("ITEM").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            If sdbgPlantillas.Columns("INDICE").Value = "9" Then 'PRES2
                                If gParametrosGenerales.gbOBLPres4 Then
                                    sdbgPlantillas.Columns("ITEM").Value = "-1"
                                    sdbgPlantillas.Update
                                    Exit Sub
                                End If
                            End If
                            sdbgPlantillas.Columns("USAR").Value = "0"
                        End If
                        sdbgPlantillas.Columns("GRUPO").Value = "0"
                        sdbgPlantillas.Columns("BOTONGRUP").Value = ""
                        sdbgPlantillas.Columns("PROCE").Value = 0
                        bGrupoNoDef = True
            End Select
            If sdbgPlantillas.Columns("USAR").Value = "0" Then
                sdbgPlantillas.Columns("PROCE").Value = "0"
                sdbgPlantillas.Columns("ITEM").Value = "0"
                sdbgPlantillas.Columns("GRUPO").Value = "0"
                sdbgPlantillas.Columns("BOTONGRUP").Value = ""
                sdbgPlantillas.Columns("USAR").Value = "0"
            End If
        
        Case "4" 'ESPEC
            Select Case sdbgPlantillas.col
                    Case 0
                        If sdbgPlantillas.Columns("USAR").Value = "-1" Then
                            sdbgPlantillas.Columns("ITEM").Value = "-1"
                        End If
                        bGrupoNoDef = True
                    Case 2 'PROCESO
                        sdbgPlantillas.Columns("USAR").Value = "-1"
                        If sdbgPlantillas.Columns("ITEM").Value = "0" And sdbgPlantillas.Columns("PROCE").Value = "0" And sdbgPlantillas.Columns("GRUPO").Value = "0" Then
                            sdbgPlantillas.Columns("USAR").Value = "0"
                        End If
                        bGrupoNoDef = True
                    Case 3 'GRUPO
                        sdbgPlantillas.Columns("USAR").Value = "-1"
                        If sdbgPlantillas.Columns("GRUPO").Value = "0" Then
                            sdbgPlantillas.Columns("BOTONGRUP").Value = ""
                            If sdbgPlantillas.Columns("ITEM").Value = "0" And sdbgPlantillas.Columns("PROCE").Value = "0" And sdbgPlantillas.Columns("GRUPO").Value = "0" Then
                                sdbgPlantillas.Columns("USAR").Value = "0"
                            End If
                        Else
                            sdbgPlantillas.Columns("BOTONGRUP").Value = "..."
                        End If
                        sdbgPlantillas_BtnClick
                        bGrupoNoDef = False
                    Case 5 'ITEM
                        sdbgPlantillas.Columns("USAR").Value = "-1"
                        If sdbgPlantillas.Columns("ITEM").Value = "0" And sdbgPlantillas.Columns("PROCE").Value = "0" And sdbgPlantillas.Columns("GRUPO").Value = "0" Then
                            sdbgPlantillas.Columns("USAR").Value = "0"
                        End If
                        bGrupoNoDef = True
            End Select
            If sdbgPlantillas.Columns("USAR").Value = "0" Then
                sdbgPlantillas.Columns("PROCE").Value = "0"
                sdbgPlantillas.Columns("ITEM").Value = "0"
                sdbgPlantillas.Columns("GRUPO").Value = "0"
                sdbgPlantillas.Columns("BOTONGRUP").Value = ""
                sdbgPlantillas.Columns("USAR").Value = "0"
            End If
    
    End Select
    
    If bGrupoNoDef Then
        Select Case sdbgPlantillas.Columns("INDICE").Value
            Case "0"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefDestino = False
                    Next
            Case "1"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefFormaPago = False
                    Next
            Case "2"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefFechasSum = False
                    Next
            Case "3"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefProveActual = False
                    Next
            Case "4"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefEspecificaciones = False
                    Next
            Case "5"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefDistribUON = False
                    Next
            Case "6"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefPresAnualTipo1 = False
                    Next
            Case "7"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefPresAnualTipo2 = False
                    Next
            Case "8"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefPresTipo1 = False
                    Next
            Case "9"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefPresTipo2 = False
                    Next
            Case "10"
                    For Each oGrupo In m_oGrupos
                        oGrupo.DefSolicitud = False
                    Next
                    If sdbgPlantillas.Columns("usar").Value = 0 Then
                        Me.fraSolicitudCompra(0).Visible = False
                    Else
                        Me.fraSolicitudCompra(0).Visible = True
                    End If
        End Select
    End If

    sdbgPlantillas.Update

End Sub


Private Sub sdbgPlantillas_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    If Not m_bModoEdicionPlant Then Exit Sub
    
    If IsNull(LastRow) Or IsEmpty(LastRow) Then Exit Sub

    If sdbgPlantillas.col = "-1" Then Exit Sub

    If CStr(LastRow) <> CStr(sdbgPlantillas.Bookmark) Then

        Select Case sdbgPlantillas.Columns("INDICE").CellValue(LastRow)
            Case "0" 'DEST
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If
                
            Case "1" 'PAG
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If

            Case "2" 'FECSUM
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If
                
            Case "3" 'PROVE (opcional)
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If
                
            Case "4" 'ESP
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If
                
            Case "5" 'DISTIB
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If

            'PRESUP
            Case "6"
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If
            
            Case "7"
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If

            Case "8"
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If
                
            Case "9"
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If
                
            Case "10"
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
                Else
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                End If
       End Select
    Else
        If sdbgPlantillas.Columns(sdbgPlantillas.col).Name = "DATO" Then
            sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = True
        Else
            'Si la fila est� bloqueada se fuerza el change, si no no hace falta.
            If sdbgPlantillas.Columns(sdbgPlantillas.col).Locked Then
                If sdbgPlantillas.Columns(sdbgPlantillas.col).Name <> "BOTONGRUP" Then
                    sdbgPlantillas.Columns(sdbgPlantillas.col).Locked = False
                    If sdbgPlantillas.Columns(sdbgPlantillas.col).Value = "0" Then
                        sdbgPlantillas.Columns(sdbgPlantillas.col).Value = "-1"
                    Else
                        If sdbgPlantillas.Columns(sdbgPlantillas.col).Value = "-1" Then
                            sdbgPlantillas.Columns(sdbgPlantillas.col).Value = "0"
                        End If
                    End If
                    sdbgPlantillas_Change
                End If
            End If
        End If
    End If

End Sub

Private Sub sdbgplantillas_RowLoaded(ByVal Bookmark As Variant)
    
    If sdbgPlantillas.Columns(0).Value = "-1" Then
        sdbgPlantillas.Columns(0).CellStyleSet ("Amarillo")
        sdbgPlantillas.Columns(1).CellStyleSet ("Amarillo")
    Else
        sdbgPlantillas.Columns(0).CellStyleSet ("proceso")
        sdbgPlantillas.Columns(1).CellStyleSet ("proceso")
    End If

    
End Sub

Public Function ModificarPlantilla()
Dim udtTeserror As TipoErrorSummit

    If cmdModoEdicion.caption = m_sConsulta Or cmdAceptar.Visible = True Or cmdModoEdicionAtr.caption = m_sConsulta Then
        Exit Function
    End If

    Screen.MousePointer = vbHourglass
    
    udtTeserror.NumError = TESnoerror
    g_Accion = ACCPlantillasMod
    
    Set g_oPlantillaSeleccionada = g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag))
    InicializarValoresSubasta
    
    g_oPlantillaSeleccionada.ExisteRestriccionMaterial
    
    Set g_oIBaseDatos = g_oPlantillaSeleccionada
    udtTeserror = g_oIBaseDatos.IniciarEdicion
    If udtTeserror.NumError = TESnoerror Then
         frmPlantillaDetalle.caption = m_sModif & " " & g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag)).Fecha & " - " & g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag)).nombre
         frmPlantillaDetalle.txtDescr = NullToStr(g_oPlantillaSeleccionada.Descripcion)
         frmPlantillaDetalle.txtFecha = g_oPlantillaSeleccionada.Fecha 'g_oPlantillas.Item(CStr(lstvwplantillas.SelectedItem.Tag)).Fecha
         frmPlantillaDetalle.txtNombre = g_oPlantillaSeleccionada.nombre
         
         If g_oPlantillaSeleccionada.ParaPedido Then
            frmPlantillaDetalle.chkParaPedido.Value = vbChecked
         Else
            frmPlantillaDetalle.chkParaPedido.Value = vbUnchecked
            frmPlantillaDetalle.chkParaPedidoDef.Enabled = False
         End If
        
         If g_oPlantillaSeleccionada.ParaPedidoDef Then
            frmPlantillaDetalle.chkParaPedidoDef.Value = vbChecked
         Else
            frmPlantillaDetalle.chkParaPedidoDef.Value = vbUnchecked
         End If

         Screen.MousePointer = vbNormal
         frmPlantillaDetalle.Show 1
    Else
         TratarError udtTeserror
    End If
    
    Screen.MousePointer = vbNormal

End Function

Public Function DetalleVista()

    If cmdModoEdicion.caption = m_sConsulta Or cmdAceptar.Visible = True Or cmdModoEdicionAtr.caption = m_sConsulta Then
        Exit Function
    End If
    
    g_Accion = ACCPlantillasCon
    
    frmPlantillaDetalle.caption = m_sDetalle & " " & g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag)).Fecha & " - " & g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag)).nombre
    frmPlantillaDetalle.txtDescr = NullToStr(g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag)).Descripcion)
    frmPlantillaDetalle.txtNombre = g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag)).nombre
    frmPlantillaDetalle.txtFecha = g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag)).Fecha
    
    If g_oPlantillaSeleccionada.ParaPedido Then
        frmPlantillaDetalle.chkParaPedido.Value = vbChecked
    Else
        frmPlantillaDetalle.chkParaPedido.Value = vbUnchecked
    End If
    
    If g_oPlantillaSeleccionada.ParaPedidoDef Then
        frmPlantillaDetalle.chkParaPedidoDef.Value = vbChecked
    Else
        frmPlantillaDetalle.chkParaPedidoDef.Value = vbUnchecked
    End If

    frmPlantillaDetalle.bOcultarMateriales = True
    Screen.MousePointer = vbNormal
    frmPlantillaDetalle.Show 1

End Function

Public Function EliminarPlantilla()
Dim udtTeserror As TipoErrorSummit
Dim irespuesta As Integer

On Error GoTo Cancelar:

    If cmdModoEdicion.caption = m_sConsulta Or cmdAceptar.Visible = True Or cmdModoEdicionAtr.caption = m_sConsulta Then
        Exit Function
    End If
    
    g_Accion = ACCPlantillasEli
    
    irespuesta = oMensajes.PreguntaEliminar(m_sPlantilla & " " & lstvwplantillas.selectedItem.Text)
    If irespuesta = vbNo Then Exit Function
    Set g_oPlantillaSeleccionada = g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag))
    Set g_oIBaseDatos = g_oPlantillaSeleccionada
    
    udtTeserror = g_oIBaseDatos.EliminarDeBaseDatos
    If udtTeserror.NumError <> TESnoerror Then
        basErrores.TratarError udtTeserror
    Else
        g_oPlantillas.Remove (CStr(lstvwplantillas.selectedItem.Tag))
        basSeguridad.RegistrarAccion AccionesSummit.ACCPlantillasEli, lstvwplantillas.selectedItem.Tag
        lstvwplantillas.ListItems.Remove (CStr(lstvwplantillas.selectedItem.key))
               
    End If
    
    If lstvwplantillas.ListItems.Count > 0 Then
        lstvwplantillas.selectedItem.Selected = True
        lstvwplantillas.selectedItem.EnsureVisible
        g_lNumID = Mid(lstvwplantillas.selectedItem.key, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE)
        CargarGridPlantillas
    Else
        txtDescr.Text = ""
        lblTitulo.caption = ""
        sdbgPlantillas.RemoveAll
        sstabPlantillas.TabVisible(1) = False
        sstabPlantillas.TabVisible(2) = False
        sstabPlantillas.TabVisible(3) = False
        cmdRestaurar.Visible = False
        cmdMod.Visible = False
        cmdModificarPlant.Visible = False
        cmdEliminarPlant.Visible = False
        cmdConfVistas.Visible = False
        cmdEliminarVistas.Visible = False
    End If
    
    Set g_oIBaseDatos = Nothing
    
    Exit Function
    
Cancelar:
    
    Set g_oIBaseDatos = Nothing
    
End Function


''' <summary>
''' Resize del splitter
''' </summary>
''' <param name="BorderPanes">Es la coleccion de elementos del splitter</param>
''' <returns></returns>
''' <remarks>Llamada desde;Evento Tiempo m�ximo 0</remarks>

Private Sub SSSplitterEsp_Resize(ByVal BorderPanes As SSSplitter.Panes)

        If Not g_oPlantillaSeleccionada Is Nothing Then
 
            If g_oPlantillaSeleccionada.EspecProce Then
                picSplit1.Height = SSSplitterEsp.Panes.Item(0).Height
                picSplit2.Height = SSSplitterEsp.Panes.Item(1).Height
                picSplit3.Height = SSSplitterEsp.Panes.Item(2).Height
                
                If picSplit2.Height > 600 Then
                    sdbgAtributosEsp.Height = picSplit2.Height - 600 '* 0.8
                Else
                    sdbgAtributosEsp.Height = 0
                End If
                sdbgAtributosEsp.Width = picSplit2.Width - 200
                
                sdbgAtributosEsp.Columns("INTERNO").Width = sdbgAtributosEsp.Width * 0.05
                sdbgAtributosEsp.Columns("PEDIDO").Width = sdbgAtributosEsp.Width * 0.05
                sdbgAtributosEsp.Columns("COD").Width = sdbgAtributosEsp.Width * 0.1
                sdbgAtributosEsp.Columns("DEN").Width = sdbgAtributosEsp.Width * 0.2
                sdbgAtributosEsp.Columns("DESCR").Width = sdbgAtributosEsp.Width * 0.05
                sdbgAtributosEsp.Columns("TIPO").Width = sdbgAtributosEsp.Width * 0.1
                sdbgAtributosEsp.Columns("VALOR").Width = sdbgAtributosEsp.Width * 0.15
                sdbgAtributosEsp.Columns("AMBITO").Width = sdbgAtributosEsp.Width * 0.1
                sdbgAtributosEsp.Columns("OBLIGATORIO").Width = sdbgAtributosEsp.Width * 0.1
                sdbgAtributosEsp.Columns("VALIDACION").Width = sdbgAtributosEsp.Width * 0.1
                
                
                txtProceEsp.Width = picSplit1.Width - 200
                If picSplit1.Height > 200 Then
                    txtProceEsp.Height = picSplit1.Height - 200
                Else
                    txtProceEsp.Height = 0
                End If
                
                If picSplit3.Height > 575 Then
                    lstvwEsp.Height = picSplit3.Height - 120 - 375 - 80
                Else
                    lstvwEsp.Height = 0
                End If
                lstvwEsp.Width = picSplit3.Width - 200
                picNavigateEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 80
                picNavigateEsp.Left = lstvwEsp.Width - 2280
                
                
                cmdModoEdicionAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
                cmdModoEdicionAtrEsp.Left = SSSplitterEsp.Width - 1000
                
                cmdDeshacerAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
                cmdDeshacerAtrEsp.Left = picSplit2.Left + 90
                
                picBotonesAtributos.Width = SSSplitterEsp.Width
                sdbcGrupoEsp.Width = SSSplitterEsp.Width - 2000
                sdbcGrupoEsp.Columns(0).Width = sdbcGrupoEsp.Width / 3
                sdbcGrupoEsp.Columns(1).Width = 2 * sdbcGrupoEsp.Width / 3
                cmdSortAscEsp.Left = sdbcGrupoEsp.Width + 300
                cmdSortDesEsp.Left = cmdSortAscEsp.Left + 350
                cmdAnyaAtribEsp.Left = cmdSortDesEsp.Left + 400
                If Not m_bModifAtrib Then
                    cmdAnyaAtribEsp.Visible = False
                Else
                    cmdAnyaAtribEsp.Visible = True
                End If
                cmdElimAtribEsp.Left = cmdAnyaAtribEsp.Left + 350
            Else
            
                If picSplit2.Height > 600 Then
                    sdbgAtributosEsp.Height = picSplit2.Height - 600 '* 0.8
                Else
                    sdbgAtributosEsp.Height = 0
                End If
                sdbgAtributosEsp.Width = picSplit2.Width - 200
                
                sdbgAtributosEsp.Columns("INTERNO").Width = sdbgAtributosEsp.Width * 0.05
                sdbgAtributosEsp.Columns("PEDIDO").Width = sdbgAtributosEsp.Width * 0.05
                sdbgAtributosEsp.Columns("COD").Width = sdbgAtributosEsp.Width * 0.1
                sdbgAtributosEsp.Columns("DEN").Width = sdbgAtributosEsp.Width * 0.2
                sdbgAtributosEsp.Columns("DESCR").Width = sdbgAtributosEsp.Width * 0.05
                sdbgAtributosEsp.Columns("TIPO").Width = sdbgAtributosEsp.Width * 0.1
                sdbgAtributosEsp.Columns("VALOR").Width = sdbgAtributosEsp.Width * 0.15
                sdbgAtributosEsp.Columns("AMBITO").Width = sdbgAtributosEsp.Width * 0.1
                sdbgAtributosEsp.Columns("OBLIGATORIO").Width = sdbgAtributosEsp.Width * 0.1
                sdbgAtributosEsp.Columns("VALIDACION").Width = sdbgAtributosEsp.Width * 0.1
                
                If picSplit3.Height > 575 Then
                    lstvwEsp.Height = picSplit3.Height - 120 - 375 - 80
                Else
                    lstvwEsp.Height = 0
                End If
                lstvwEsp.Width = sstabPlantillas.Width - 600
                picNavigateEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 80
                picNavigateEsp.Left = lstvwEsp.Width - 2280
                
                
                cmdModoEdicionAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
                cmdModoEdicionAtrEsp.Left = SSSplitterEsp.Width - 1000
                
                cmdDeshacerAtrEsp.Top = SSSplitterEsp.Top + SSSplitterEsp.Height + 50
                cmdDeshacerAtrEsp.Left = picSplit2.Left + 90
                
                picBotonesAtributos.Width = SSSplitterEsp.Width
                sdbcGrupoEsp.Width = SSSplitterEsp.Width - 2000
                sdbcGrupoEsp.Columns(0).Width = sdbcGrupoEsp.Width / 3
                sdbcGrupoEsp.Columns(1).Width = 2 * sdbcGrupoEsp.Width / 3
                cmdSortAscEsp.Left = sdbcGrupoEsp.Width + 300
                cmdSortDesEsp.Left = cmdSortAscEsp.Left + 350
                cmdAnyaAtribEsp.Left = cmdSortDesEsp.Left + 400
                If Not m_bModifAtrib Then
                    cmdAnyaAtribEsp.Visible = False
                Else
                    cmdAnyaAtribEsp.Visible = True
                End If
                cmdElimAtribEsp.Left = cmdAnyaAtribEsp.Left + 350
            End If
        
        End If
        

End Sub


''' <summary>
''' Evento producido al hacer click en los tabs
''' </summary>
''' <param name="PreviousTab">Tab Anterior </param>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>


Private Sub sstabPlantillas_Click(PreviousTab As Integer)

    Select Case sstabPlantillas.Tab
    
        Case 0
        
            If m_bModoEdicion Then
                sstabPlantillas.Tab = 1
                Exit Sub
            End If
            If m_bModoEdicionAtr Then
                sstabPlantillas.Tab = 2
                Exit Sub
            End If
            If m_bModoEdicionAtrEsp Then
                sstabPlantillas.Tab = 3
                Exit Sub
            End If
            
            If m_bModoEdicionMateriales Then
                sstabPlantillas.Tab = 4
                Exit Sub
            End If
            picNavigateAmbito.Enabled = True
            picNavigateGrupos.Enabled = False
            picNavigateAtrib.Enabled = False
            picNavigateEsp.Enabled = False
            picNavigateEsp.Visible = False
            MaterialesVisible False
            cmdModoEdicionAtrEsp.Visible = False
            cmdDeshacerAtrEsp.Visible = False
            
            
        Case 1
        
            If m_bModoEdicionPlant Then
                sstabPlantillas.Tab = 0
                Exit Sub
            End If
            If m_bModoEdicionAtr Then
                sstabPlantillas.Tab = 2
                Exit Sub
            End If
            If m_bModoEdicionAtrEsp Then
                sstabPlantillas.Tab = 3
                Exit Sub
            End If
            
            If m_bModoEdicionMateriales Then
                sstabPlantillas.Tab = 4
                Exit Sub
            End If
            picNavigateAmbito.Enabled = False
            picNavigateGrupos.Enabled = True
            picNavigateAtrib.Enabled = False
            picNavigateEsp.Enabled = False
            picNavigateEsp.Visible = False
            MaterialesVisible False
            cmdModoEdicionAtrEsp.Visible = False
            cmdDeshacerAtrEsp.Visible = False
        
        Case 2
            
            If m_bModoEdicion Then
               sstabPlantillas.Tab = 1
               Exit Sub
            End If
            If m_bModoEdicionPlant Then
                sstabPlantillas.Tab = 0
                Exit Sub
            End If
            If m_bModoEdicionAtrEsp Then
                sstabPlantillas.Tab = 3
                Exit Sub
            End If
            If m_bModoEdicionMateriales Then
                sstabPlantillas.Tab = 4
                Exit Sub
            End If
            If Not gParametrosGenerales.gbUsarPonderacion Then
                sdbgAtribPlant.Groups.Item(4).Visible = False
            End If
            picNavigateAmbito.Enabled = False
            picNavigateGrupos.Enabled = False
            picNavigateAtrib.Enabled = True
            picNavigateEsp.Enabled = False
            picNavigateEsp.Visible = False
            MaterialesVisible False
            cmdModoEdicionAtrEsp.Visible = False
            cmdDeshacerAtrEsp.Visible = False
            
        Case 3
            
            If m_bModoEdicion Then
               sstabPlantillas.Tab = 1
               Exit Sub
            End If
            If m_bModoEdicionPlant Then
                sstabPlantillas.Tab = 0
                Exit Sub
            End If
            If m_bModoEdicionAtr Then
                sstabPlantillas.Tab = 2
                Exit Sub
            End If
            If m_bModoEdicionMateriales Then
                sstabPlantillas.Tab = 4
                Exit Sub
            End If
            picNavigateAmbito.Enabled = False
            picNavigateGrupos.Enabled = False
            picNavigateAtrib.Enabled = False
            picNavigateEsp.Visible = True
            picNavigateEsp.Enabled = True
            MaterialesVisible False
            If m_bModif Then cmdModoEdicionAtrEsp.Visible = True
            If m_bModif Then cmdDeshacerAtrEsp.Visible = True
            
        Case 4 ' Materiales
        
            If m_bModoEdicion Then
               sstabPlantillas.Tab = 1
               Exit Sub
            End If
            If m_bModoEdicionPlant Then
                sstabPlantillas.Tab = 0
                Exit Sub
            End If
            If m_bModoEdicionAtr Then
                sstabPlantillas.Tab = 2
                Exit Sub
            End If
            If m_bModoEdicionAtrEsp Then
                sstabPlantillas.Tab = 3
                Exit Sub
            End If
            If m_bModoEdicionMateriales Then
                Exit Sub
            End If
            picNavigateAmbito.Enabled = False
            picNavigateGrupos.Enabled = False
            picNavigateAtrib.Enabled = False
            picNavigateEsp.Visible = True
            picNavigateEsp.Enabled = True
            ConfigurarMateriales False
            cmdModoEdicionAtrEsp.Visible = False
            cmdDeshacerAtrEsp.Visible = False
    End Select
    
End Sub

Private Sub ConfigurarMateriales(ModoEdicion As Boolean)
    
    'edu Incidencia 7216
    'edu Recuparado codigo antiguo para incidencia 7315

    'edu Incidencia 7543. Vuelta a empezar.
    ' si el usuario tiene restriccion y la plantilla no, cancelar la modificacion

    If g_oPlantillaSeleccionada Is Nothing Then
        Exit Sub
    End If
        
    If m_bRestringirSoloMaterialesComprador And g_oPlantillaSeleccionada.RestriccionMaterial = False Then
        ModoEdicion = False
    End If
    
    'edu incidencia 7513.
    'Utilizar permiso  de modificacion de plantillas
    
    If Not m_bModif Then
        ModoEdicion = False
    End If
    
    fraSoloEstosMateriales.Visible = True

    If ModoEdicion Then
        
        ' incidencia 7545
        picCrearPlant.Enabled = False
        
        tvwEstrMat.Visible = False
        
        If oCEstructura Is Nothing Then
            Set oCEstructura = CrearCAEstructuraMat(oMensajes)
        End If
    
        Select Case m_bRestringirSoloMaterialesComprador
        Case True
        
            'edu incidencia 7598. quitar tembleque
            LockWindowUpdate Me.hWnd
            
            oCEstructura.GenerarEstructuraDePlantilla g_oPlantillaSeleccionada, tvwEstrMat
            oCEstructura.GenerarEstructuraDeComprador oUsuarioSummit.comprador, tvwEstrMatMod
            
            'edu 98. Tapar checkbox del elemento raiz del arbol
            picTapa.Top = tvwEstrMatMod.Top + 60
            picTapa.Left = tvwEstrMatMod.Left + 60
            picTapa.Visible = True
            tvwEstrMatMod.Visible = True ' edu. Si el control no es visible las marcas no se visualizan.
    
            tvwEstrMatMod.Scroll = False
            tvwEstrMatMod.Refresh
            
            If oCEstructura.TrasladarMarcas(tvwEstrMat, tvwEstrMatMod) <= 0 Then
                LockWindowUpdate 0&
                ConfigurarMateriales False
                Exit Sub
            End If
   
            LockWindowUpdate 0&
            
        Case False

            oCEstructura.GenerarEstructuraDePlantilla g_oPlantillaSeleccionada, tvwEstrMat
            oCEstructura.GenerarEstructuraMateriales , tvwEstrMatMod
            tvwEstrMatMod.Visible = True ' edu. Si el control no es visible las marcas no se visualizan.
            tvwEstrMatMod.Scroll = False
            tvwEstrMatMod.Refresh
            oCEstructura.TrasladarMarcas tvwEstrMat, tvwEstrMatMod
            oCEstructura.MemorizarEstructura tvwEstrMatMod
        End Select

        'Si no quedan materiales
        If g_oPlantillaSeleccionada.RestriccionMaterial = True Then ' Tiene materiales asignados
            chkSoloEstosMateriales.Value = vbChecked
        End If
        'edu 98. Tapar checkbox del elemento raiz del arbol
        picTapa.Top = tvwEstrMatMod.Top + 60
        picTapa.Left = tvwEstrMatMod.Left + 60
        picTapa.Visible = True
        'chkSoloEstosMateriales.Value = vbUnchecked
        tvwEstrMatMod.Visible = True

        
        cmdModificarMateriales.Visible = False
        cmdAceptarMateriales.Visible = True
        cmdCancelarMateriales.Visible = True

        If m_bRestringirSoloMaterialesComprador Then
            fraSoloEstosMateriales.Enabled = False
        Else
            fraSoloEstosMateriales.Enabled = True
        End If
        
    Else
            
        ' incidencia 7545
        picCrearPlant.Enabled = True

        tvwEstrMatMod.Visible = False
        picTapa.Visible = False

        If g_oPlantillaSeleccionada.RestriccionMaterial = True Then ' Tiene materiales asignados
            CheckMateriales vbChecked
            CargarEstructuraMateriales
            tvwEstrMat.Visible = True
        Else
            CheckMateriales vbUnchecked
            tvwEstrMat.Visible = False
        End If


        cmdModificarMateriales.Visible = m_bModif

        cmdAceptarMateriales.Visible = False
        cmdCancelarMateriales.Visible = False
        picTapa.Visible = False

        fraSoloEstosMateriales.Enabled = False

    End If

    m_bModoEdicionMateriales = ModoEdicion
    
End Sub

Sub MaterialesVisible(modo As Boolean)

    tvwEstrMat.Visible = modo
    cmdModificarMateriales.Visible = modo
    cmdAceptarMateriales.Visible = modo
    cmdCancelarMateriales.Visible = modo
    picTapa.Visible = modo

    fraSoloEstosMateriales.Visible = modo

End Sub

' marcar automaticamente el check de materiales evitando que se dispare el evento check.

Sub CheckMateriales(modo As Variant)

    If chkSoloEstosMateriales.Value = modo Then
        Exit Sub
    End If
    If chkSoloEstosMateriales.Enabled = True Then
        chkSoloEstosMateriales.Enabled = False
        chkSoloEstosMateriales.Value = modo
        chkSoloEstosMateriales.Enabled = True
    Else
        chkSoloEstosMateriales.Value = modo
    End If
    
    

End Sub

''' <summary>
''' Carga el gri de atributos de oferta
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdAceptar_Click;cmdEliminar_Click;cmdModoEdicionAtr_Click;cmdRestAtr_Click;CargarGridPlantillas;lstvwplantillas_ItemClick  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarGridAtributos()
Dim oatrib As CAtributo
Dim sTipo As String
Dim sAmbito As String
Dim sLibre As String
Dim sSeleccion As String
Dim sAplicarA As String
Dim sBoton As String
Dim sInterno As String
Dim sValorBajo As String
Dim sDefPrec As String
Dim sObl As String
Dim sPedido As String
Dim sIntegracion As String

    sdbgAtribPlant.RemoveAll
    
    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
    
    If Not gParametrosGenerales.gbPedidosDirectos Then
        sdbgAtribPlant.Columns("PEDIDO").Visible = False
    End If
    
    'Comprobacion de si hay integracion de pedidos o de adjudicacion para mostrar la columna de integracion
    If Not g_oPlantillaSeleccionada.HayIntegracionPedAdj Then
       sdbgAtribPlant.Columns("INTEGRACION").Visible = False
    End If
    
    
    g_oPlantillaSeleccionada.CargarTodosLosAtributos (g_sCodGRP)
    
    
    If g_oPlantillaSeleccionada.ATRIBUTOS.Count = 0 Then Exit Sub
    
    For Each oatrib In g_oPlantillaSeleccionada.ATRIBUTOS
        
        Select Case oatrib.Tipo
            Case 1: sTipo = m_asAtrib(3) 'texto
            Case 2: sTipo = m_asAtrib(0) 'numerico
            Case 3: sTipo = m_asAtrib(1) 'fecha
            Case 4: sTipo = m_asAtrib(2) 'si/no
        End Select
        
        Select Case oatrib.ambito
            Case 1: sAmbito = m_asAtrib(4)
            Case 2: sAmbito = m_asAtrib(5)
            Case 3: sAmbito = m_asAtrib(6)
        End Select
        
        Select Case oatrib.PrecioAplicarA
            Case Null: sAplicarA = ""
            Case 0: sAplicarA = m_asAtrib(12)
            Case 1: sAplicarA = m_asAtrib(7)
            Case 2: sAplicarA = m_asAtrib(8)
            Case 3: sAplicarA = m_asAtrib(9)
    
        End Select
        
        sLibre = 0
        sSeleccion = 0
        sBoton = ""
        If oatrib.TipoIntroduccion = 0 Then
            sLibre = "-1"
            sBoton = ""
        Else
            If oatrib.TipoIntroduccion = 1 And oatrib.Tipo <> TipoBoolean Then
                sSeleccion = "-1"
                sBoton = "..."
            End If
        End If
        
        If oatrib.interno = True Then
            sInterno = "-1"
        Else
            sInterno = 0
        End If
        
        If oatrib.pedido = True Then
            sPedido = "-1"
        Else
            sPedido = 0
        End If
        
        If oatrib.Obligatorio = True Then
            sObl = "-1"
        Else
            sObl = 0
        End If
        If oatrib.PreferenciaValorBajo = True Then
            sValorBajo = "-1"
        Else
            sValorBajo = 0
        End If
        If oatrib.PrecioDefecto = 1 Then
            sDefPrec = "-1"
        Else
            sDefPrec = 0
        End If
        
        Select Case oatrib.ConfIntegracionAtributo
         Case TipoIntegracionAtributo.NoTiene:              sIntegracion = ""
         Case TipoIntegracionAtributo.Adjudicacion:         sIntegracion = "Adjudicacion"
         Case TipoIntegracionAtributo.pedido:               sIntegracion = "Pedido"
         Case TipoIntegracionAtributo.AdjudicacionPedido:   sIntegracion = "Adjudicacion/Pedido"
        
        End Select
       
        If oatrib.codgrupo = g_sCodGRP Then
        
            sdbgAtribPlant.AddItem oatrib.idAtribProce & Chr(m_lSeparador) & oatrib.codgrupo & Chr(m_lSeparador) & sInterno & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & IIf(IsNull(oatrib.Descripcion), "", "...") & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & sLibre & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & sSeleccion & Chr(m_lSeparador) & sBoton & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sIntegracion & Chr(m_lSeparador) & sObl & Chr(m_lSeparador) & sValorBajo & Chr(m_lSeparador) & oatrib.PrecioFormula & Chr(m_lSeparador) & sAplicarA & Chr(m_lSeparador) & sDefPrec & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.TipoPonderacion & Chr(m_lSeparador) & oatrib.Descripcion
            
        ElseIf IsNull(oatrib.codgrupo) Then
        
            sdbgAtribPlant.AddItem oatrib.idAtribProce & Chr(m_lSeparador) & oatrib.codgrupo & Chr(m_lSeparador) & sInterno & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & IIf(IsNull(oatrib.Descripcion), "", "...") & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & sLibre & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & sSeleccion & Chr(m_lSeparador) & sBoton & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sIntegracion & Chr(m_lSeparador) & sObl & Chr(m_lSeparador) & sValorBajo & Chr(m_lSeparador) & oatrib.PrecioFormula & Chr(m_lSeparador) & sAplicarA & Chr(m_lSeparador) & sDefPrec & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.TipoPonderacion & Chr(m_lSeparador) & oatrib.Descripcion
            
        End If
        
        sAplicarA = ""
    
    Next
    
    sdbgAtribPlant.SplitterPos = 1
    
    sdbgAtribPlant.MoveFirst

End Sub




Private Sub txtGrupoDescr_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit

    If sdbgGrupos.IsAddRow Then
    '    sdbgGrupos.Columns("DESCR").Text = txtGrupoDescr.Text
    Else
        
        m_bModError = False
        
        Set m_oGrupoEnEdicion = Nothing
        Set m_oGrupoEnEdicion = g_oPlantillaSeleccionada.Grupos.Item(CStr(sdbgGrupos.Columns("COD").Text))
          
        Set g_oIBaseDatos = m_oGrupoEnEdicion
        
        teserror = g_oIBaseDatos.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
                
            TratarError teserror
            sdbgGrupos.DataChanged = False
            sdbgGrupos.Columns("COD").Value = m_oGrupoEnEdicion.Codigo
            sdbgGrupos.Columns("NON").Value = m_oGrupoEnEdicion.Den
            txtGrupoDescr.Text = m_oGrupoEnEdicion.Descripcion
            
            teserror.NumError = TESnoerror
                
        End If
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgGrupos.SetFocus
        End If
        
        m_oGrupoEnEdicion.Den = sdbgGrupos.Columns("NOM").Value
        m_oGrupoEnEdicion.Descripcion = txtGrupoDescr.Text
          
        Screen.MousePointer = vbHourglass
        teserror = g_oIBaseDatos.FinalizarEdicionModificando
        Screen.MousePointer = vbNormal
                    
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgGrupos.SetFocus
            sdbgGrupos.DataChanged = False
            m_bModError = True
        Else
            cmdDeshacer.Enabled = False
        End If
                    
        Set g_oIBaseDatos = Nothing
        
        m_bActualizar = False ' para que no salte el BeforeUpdate
        
        Exit Sub
        
    End If

End Sub

Public Sub CargarListaValores()
Dim oLista As CValorPond
    
    frmLista.g_sOrigen = "PLANTILLAS"
    
    If Not m_oAtributo Is Nothing Then
        If sdbgAtribPlant.DataChanged = True Then
            sdbgAtribPlant.Update
        End If
        Set m_oAtributo = Nothing
    End If
    
    
    If g_sCodGRP <> "" And sdbgAtribPlant.Columns("CODGRUPO").Value = "" Then
        frmLista.g_bEdicion = False
    Else
        frmLista.g_bEdicion = m_bModoEdicionAtr
    End If
    
    Set frmLista.g_oAtributo = g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").Value))
    
    frmLista.sdbgValores.RemoveAll
    
    If Not sdbgAtribPlant.IsAddRow Then
    
        g_oPlantillaSeleccionada.ATRIBUTOS.Item(sdbgAtribPlant.Columns("ATRIB").Value).CargarListaDeValores
        frmLista.g_iIndiCole = 1
        Set frmLista.g_oPondIntermedia = Nothing
        Set frmLista.g_oPondIntermedia = oFSGSRaiz.Generar_CValoresPond
        For Each oLista In g_oPlantillaSeleccionada.ATRIBUTOS.Item(sdbgAtribPlant.Columns("ATRIB").Value).ListaPonderacion
            frmLista.sdbgValores.AddItem oLista.ValorLista & Chr(m_lSeparador) & frmLista.g_iIndiCole & Chr(m_lSeparador) & oLista.IDAtributo & Chr(m_lSeparador) & oLista.ValorPond & Chr(m_lSeparador) & oLista.FechaActualizacion
            frmLista.g_oPondIntermedia.Add oLista.IDAtributo, oLista.AnyoProce, oLista.GMN1Proce, , , , oLista.ValorLista, , , , oLista.FechaActualizacion, frmLista.g_iIndiCole
            frmLista.g_iIndiCole = frmLista.g_iIndiCole + 1
        Next
    
    End If
        If Not g_bPermitir Then
        frmLista.chkGuardarDef.Visible = False
    End If
    
    frmLista.Show 1
        
End Sub

Public Sub CargarPonderacion()
    
    frmPonderacion.g_sOrigen = "PLANTILLAS"
    
    g_oPlantillaSeleccionada.ATRIBUTOS.Item(sdbgAtribPlant.Columns("ATRIB").Value).CargarPonderacion
    Set frmPonderacion.g_oAtributo = g_oPlantillaSeleccionada.ATRIBUTOS.Item(sdbgAtribPlant.Columns("ATRIB").Value)
    If sdbgAtribPlant.Columns("TipoPond").Value <> "" Then
        frmPonderacion.g_oAtributo.TipoPonderacion = sdbgAtribPlant.Columns("TipoPond").Value
    End If
    
    If g_sCodGRP <> "" And sdbgAtribPlant.Columns("CODGRUPO").Value = "" Then
        frmPonderacion.g_bEdicion = False
        frmPonderacion.g_bAtribNoModif = True
        frmPonderacion.cmdModif1.Visible = False
    Else
        frmPonderacion.g_bEdicion = m_bModoEdicionAtr
    End If
    frmPonderacion.g_bError = False
    If g_bPermitir And m_bModoEdicionAtr Then
        If sdbgAtribPlant.Columns("TipoPond").Value <> "2" Then
            If Not frmPonderacion.g_bAtribNoModif Then
                frmPonderacion.chkDef1.Visible = True
            End If
        Else
            If g_oPlantillaSeleccionada.ATRIBUTOS.Item(sdbgAtribPlant.Columns("ATRIB").Value).TipoIntroduccion = 1 Then
                If Not frmPonderacion.g_bAtribNoModif Then
                    frmPonderacion.chkDef1.Visible = True
                End If
            End If
        End If
    End If
    
    frmPonderacion.WindowState = vbNormal

    If Not frmPonderacion.g_bError Then
        frmPonderacion.Show 1
    Else
        Unload frmPonderacion
    End If
End Sub

Private Sub sdbcSeleccion_Click()
    sdbcSeleccion.Value = ""
End Sub

Private Sub sdbcSeleccion_CloseUp()

    If sdbcSeleccion.Columns(0).Value = "************" Then
        g_sCodGRP = ""
        sdbcSeleccion.Value = sdbcSeleccion.Columns(1).Value
    Else
        sdbcSeleccion.Value = sdbcSeleccion.Columns(0).Value
        If sdbcSeleccion.Columns(1).Value <> "" Then
            sdbcSeleccion.Text = sdbcSeleccion.Columns(0).Value & " - " & sdbcSeleccion.Columns(1).Value
        End If
        g_sCodGRP = sdbcSeleccion.Columns(0).Value
    End If
    CargarGridAtributos 'Tengo q cargar la coleccion cuando
End Sub

Private Sub sdbcSeleccion_InitColumnProps()
    sdbcSeleccion.DataFieldList = "Column 0"
    sdbcSeleccion.DataFieldToDisplay = "Column 0"
End Sub
'fin nuevo
Private Sub sdbcSeleccion_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcSeleccion.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcSeleccion.Rows - 1
            bm = sdbcSeleccion.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcSeleccion.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcSeleccion.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Private Function ReordenarSobres() As Boolean
    Dim i As Integer
    Dim oGrupo As CGrupo
    Dim lSobre As Long
    Dim j As Integer
    Dim vbm As Variant
    Dim vReordenar() As Variant
    Dim iReordenar As Integer
    Dim bMayor As Boolean
    Dim bReordenar As Boolean
    Dim vCodigo() As Variant
    Dim k As Variant
    Dim bBorra As Boolean
    
    'si es de administraci�n p�blica comprueba si habr� que reordenar los sobres para
    'que queden consecutivos
    
    bReordenar = False
    iReordenar = 1
    
    For i = 0 To sdbgGrupos.SelBookmarks.Count - 1
        bMayor = False
        lSobre = sdbgGrupos.Columns("SOBRE").CellValue(sdbgGrupos.SelBookmarks.Item(i))
        
        For j = 0 To sdbgGrupos.Rows - 1
            vbm = sdbgGrupos.AddItemBookmark(j)
            If sdbgGrupos.Columns("COD").CellValue(vbm) <> sdbgGrupos.Columns("COD").CellValue(sdbgGrupos.SelBookmarks.Item(i)) Then
                If sdbgGrupos.Columns("SOBRE").CellValue(vbm) = lSobre Then
                    'Se recorre los bookmarks para comprobar que no est� tambi�n seleccionado para borrar
                    bBorra = False
                    For k = 0 To sdbgGrupos.SelBookmarks.Count - 1
                        If sdbgGrupos.Columns("COD").CellValue(vbm) = sdbgGrupos.Columns("COD").CellValue(sdbgGrupos.SelBookmarks.Item(k)) Then
                            bBorra = True
                        End If
                    Next k
                    If bBorra = True Then
                        bMayor = True
                    Else
                        bMayor = False
                        Exit For
                    End If
                ElseIf sdbgGrupos.Columns("SOBRE").CellValue(vbm) > lSobre Then
                    'Si es mayor
                    bMayor = True
                End If
            End If
        Next j
        
        If bMayor = True Then
            ReDim Preserve vReordenar(iReordenar)
            vReordenar(iReordenar) = lSobre
            ReDim Preserve vCodigo(iReordenar)
            vCodigo(iReordenar) = sdbgGrupos.Columns("COD").CellValue(sdbgGrupos.SelBookmarks.Item(i))
            iReordenar = iReordenar + 1
            bReordenar = True
        End If
    Next i
    
    If bReordenar = True Then
        'Reordena,si es necesario,los sobres de cada grupo despu�s de haber eliminado alg�n grupo
        For i = 1 To UBound(vReordenar)
            For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                If oGrupo.Sobre > vReordenar(i) And oGrupo.Codigo <> vCodigo(i) Then
                    oGrupo.Sobre = oGrupo.Sobre - 1
                End If
            Next
        Next i
        
        ReordenarSobres = True
    Else
        ReordenarSobres = False
    End If
        
End Function

Private Sub CargarAmbitoDatos()
'*********************************************************************************************************************
'Se carga la grid de plantillas, en la columna INDICE se carga el �ndice del dato
'para diferenciar los datos entre s�. (0-DEST,1-PAG,2-FECSUM,...) igual que en la
'apertura. No sirve con leer la row porque los presupuestos pueden aparecer o no
'dependiendo de los par�metros generales
'*********************************************************************************************************************

Dim bUsar As Boolean
Dim bGrupo As Boolean
Dim bProce As Boolean
Dim bItem As Boolean
Dim sPuntos As String
Dim sUsar As String
Dim sGrupo As String
Dim sProce As String
Dim sItem As String
        
    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
    
    sdbgPlantillas.RemoveAll
    
    'DESTINO
    bProce = False
    sProce = 0
    bGrupo = False
    sGrupo = 0
    bItem = False
    sItem = 0
    bUsar = True
    sPuntos = ""
    If g_oPlantillaSeleccionada.Destino = EnProceso Then
        bProce = True
        sProce = "-1"
    End If
    If g_oPlantillaSeleccionada.Destino = EnGrupo Then
        bGrupo = True
        sGrupo = "-1"
        sPuntos = "..."
    End If
    If g_oPlantillaSeleccionada.Destino = EnItem Then
        bItem = True
        sItem = "-1"
    End If

    sdbgPlantillas.AddItem "-1" & Chr(m_lSeparador) & m_asDatos(0) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "0"
    
    'FORMA DE PAGO
    bProce = False
    sProce = 0
    bGrupo = False
    sGrupo = 0
    bItem = False
    sItem = 0
    bUsar = True
    sPuntos = ""
    If g_oPlantillaSeleccionada.FormaPago = EnProceso Then
        bProce = True
        sProce = "-1"
    End If
    If g_oPlantillaSeleccionada.FormaPago = EnGrupo Then
        bGrupo = True
        sGrupo = "-1"
        sPuntos = "..."
    End If
    If g_oPlantillaSeleccionada.FormaPago = EnItem Then
        bItem = True
        sItem = "-1"
    End If
    
    sdbgPlantillas.AddItem "-1" & Chr(m_lSeparador) & m_asDatos(1) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "1"
    
    'FECHAS DE SUMINISTRO
    bProce = False
    sProce = 0
    bGrupo = False
    sGrupo = 0
    bItem = False
    sItem = 0
    bUsar = True
    sPuntos = ""
    If g_oPlantillaSeleccionada.FechaSuministro = EnProceso Then
        bProce = True
        sProce = "-1"
    End If
    If g_oPlantillaSeleccionada.FechaSuministro = EnGrupo Then
        bGrupo = True
        sGrupo = "-1"
        sPuntos = "..."
    End If
    If g_oPlantillaSeleccionada.FechaSuministro = EnItem Then
        bItem = True
        sItem = "-1"
    End If
    
    sdbgPlantillas.AddItem "-1" & Chr(m_lSeparador) & m_asDatos(2) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "2"
    
    'solicitud de compra
    If m_bMostrarSolicitud = True Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.Solicitud = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.Solicitud = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.Solicitud = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.Solicitud = EnItem Then
            bItem = True
            sItem = "-1"
        End If

        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(11) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "10"
    End If

    'PROVEEDOR ACTUAL
    bProce = False
    sProce = 0
    bGrupo = False
    sGrupo = 0
    bItem = False
    sItem = 0
    bUsar = True
    sUsar = "-1"
    sPuntos = ""
    If g_oPlantillaSeleccionada.ProveedorAct = NoDefinido Then
        bUsar = False
        sUsar = 0
    End If
    If g_oPlantillaSeleccionada.ProveedorAct = EnProceso Then
        bProce = True
        sProce = "-1"
    End If
    If g_oPlantillaSeleccionada.ProveedorAct = EnGrupo Then
        bGrupo = True
        sGrupo = "-1"
        sPuntos = "..."
    End If
    If g_oPlantillaSeleccionada.ProveedorAct = EnItem Then
        bItem = True
        sItem = "-1"
    End If

    sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(3) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "3"
    
    'ESPECIFICACIONES
    bUsar = False
    sUsar = 0
    sPuntos = ""
    If g_oPlantillaSeleccionada.EspecGrupo Then sPuntos = "..."
    If g_oPlantillaSeleccionada.EspecProce Then
        bUsar = True
        sUsar = "-1"
        ElseIf g_oPlantillaSeleccionada.EspecGrupo Then
            bUsar = True
            sUsar = "-1"
            ElseIf g_oPlantillaSeleccionada.EspecItem Then
                bUsar = True
                sUsar = "-1"
    End If
    
    sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(4) & Chr(m_lSeparador) & IIf(g_oPlantillaSeleccionada.EspecProce, "-1", 0) & Chr(m_lSeparador) & IIf(g_oPlantillaSeleccionada.EspecGrupo, "-1", "0") & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & IIf(g_oPlantillaSeleccionada.EspecItem, "-1", "0") & Chr(m_lSeparador) & "4"
    
    'DISTRIBUCION UO
    bProce = False
    sProce = 0
    bGrupo = False
    sGrupo = 0
    bItem = False
    sItem = 0
    bUsar = True
    sUsar = "-1"
    sPuntos = ""
    If g_oPlantillaSeleccionada.DistribUON = NoDefinido Then
        bUsar = False
        sUsar = 0
    End If
    If g_oPlantillaSeleccionada.DistribUON = EnProceso Then
        bProce = True
        sProce = "-1"
    End If
    If g_oPlantillaSeleccionada.DistribUON = EnGrupo Then
        bGrupo = True
        sGrupo = "-1"
        sPuntos = "..."
    End If
    If g_oPlantillaSeleccionada.DistribUON = EnItem Then
        bItem = True
        sItem = "-1"
    End If
    
    sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(5) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "5"
    
    'PRESANUALES TIPO1
    If gParametrosGenerales.gbUsarPres1 Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo1 = EnItem Then
            bItem = True
            sItem = "-1"
        End If

        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(6) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "6"
    End If
    
    'PRESANUALES TIPO2
    If gParametrosGenerales.gbUsarPres2 Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.PresAnualesTipo2 = EnItem Then
            bItem = True
            sItem = "-1"
        End If
        
        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(7) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "7"
    End If
    
    'PRES TIPO1
    If gParametrosGenerales.gbUsarPres3 Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.PresTipo1 = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.PresTipo1 = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.PresTipo1 = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.PresTipo1 = EnItem Then
            bItem = True
            sItem = "-1"
        End If

        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(8) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "8"
    End If
    
    'PRES TIPO2
    If gParametrosGenerales.gbUsarPres4 Then
        bProce = False
        sProce = 0
        bGrupo = False
        sGrupo = 0
        bItem = False
        sItem = 0
        bUsar = True
        sUsar = "-1"
        sPuntos = ""
        If g_oPlantillaSeleccionada.PresTipo2 = NoDefinido Then
            bUsar = False
            sUsar = 0
        End If
        If g_oPlantillaSeleccionada.PresTipo2 = EnProceso Then
            bProce = True
            sProce = "-1"
        End If
        If g_oPlantillaSeleccionada.PresTipo2 = EnGrupo Then
            bGrupo = True
            sGrupo = "-1"
            sPuntos = "..."
        End If
        If g_oPlantillaSeleccionada.PresTipo2 = EnItem Then
            bItem = True
            sItem = "-1"
        End If

        sdbgPlantillas.AddItem sUsar & Chr(m_lSeparador) & m_asDatos(9) & Chr(m_lSeparador) & sProce & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sPuntos & Chr(m_lSeparador) & sItem & Chr(m_lSeparador) & "9"
    End If
    
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub GuardarOrdenAtributos()
    Dim oAtributos As CAtributos
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim vbm As Variant
    Dim iNumAtribProc As Integer
    
    'Comprueba los cambios de orden que se han producido y almacena en la base
    'de datos:
    iNumAtribProc = 0
    
    Set oAtributos = oFSGSRaiz.Generar_CAtributos
    
    For i = 0 To sdbgAtribPlant.Rows - 1
        vbm = sdbgAtribPlant.AddItemBookmark(i)
        
        If g_sCodGRP <> "" And sdbgAtribPlant.Columns("CODGRUPO").CellValue(vbm) = "" Then
            'Es un atributo de proceso y estamos en un grupo.No hace nada
            iNumAtribProc = iNumAtribProc + 1
        Else
            'Guarda en BD solo el orden de los atributos que han cambiado
            If NullToDbl0(g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").CellValue(vbm))).OrdenAtrib) <> (i + 1 - iNumAtribProc) Then
                g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").CellValue(vbm))).OrdenAtrib = i + 1 - iNumAtribProc
                
                oAtributos.Add g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").CellValue(vbm))).Id, g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").CellValue(vbm))).Cod, g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").CellValue(vbm))).Den, g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").CellValue(vbm))).Tipo, , , , , , , , , , , g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").CellValue(vbm))).IDPlantilla, , , , g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").CellValue(vbm))).idAtribProce, , , , , , , , , , , , , , , , , , , , g_oPlantillaSeleccionada.ATRIBUTOS.Item(CStr(sdbgAtribPlant.Columns("ATRIB").CellValue(vbm))).OrdenAtrib
            End If
        End If
    Next i
    
    teserror = oAtributos.GuardarOrdenAtribPlantilla
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Unload frmESPERA
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    Set oAtributos = Nothing
End Sub



Private Sub txtProceEsp_Change()
        
    g_Accion = ACCPlantProceEspMod
    
End Sub

Private Sub txtProceEsp_LostFocus()
    'Forzar el validate porque a veces no salta
    If g_Accion = ACCPlantProceEspMod Then
        txtProceEsp_Validate False
    End If
End Sub

Private Sub txtProceEsp_Validate(Cancel As Boolean)
    Dim teserror As TipoErrorSummit

    If g_Accion = ACCPlantProceEspMod Then
    
        If StrComp(NullToStr(g_oPlantillaSeleccionada.esp), txtProceEsp.Text, vbTextCompare) <> 0 Then
            g_oPlantillaSeleccionada.esp = StrToNull(txtProceEsp.Text)
            
            teserror = g_oPlantillaSeleccionada.ModificarEspecificaciones
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Cancel = True
                g_Accion = ACCPlantillasCon
                Exit Sub
            End If
            
            'Registrar accion
            basSeguridad.RegistrarAccion AccionesSummit.ACCPlantProceEspMod, "Plantilla:" & g_oPlantillaSeleccionada.Id
        End If

    End If
    
    g_Accion = ACCPlantillasCon

End Sub


''' <summary>
''' Carga el tab de especificaciones
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:cmdAceptar_Click;cmdEliminar_Click;CargarGridPlantillas;lstvwplantillas_ItemClick</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarEspecificaciones()
Dim oEsp As CEspecificacion
    
'***********************************************************************************
' A�ade a la lista de especificaciones los archivos adjuntos de la plantilla
'************************************************************************************

    'Carga las especificaciones
    txtProceEsp.Text = NullToStr(g_oPlantillaSeleccionada.esp)
    
    lstvwEsp.ListItems.clear
    
    g_oPlantillaSeleccionada.CargarTodasLasEspecificaciones
    
    If Not g_oPlantillaSeleccionada.especificaciones Is Nothing Then
        For Each oEsp In g_oPlantillaSeleccionada.especificaciones
            lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tam", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Rut", NullToStr(oEsp.Ruta)
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
        Next
    End If
    
    
    'g_oPlantillaSeleccionada.CargarGridEspecificaciones
    CargarComboGruposEsp
    
    Set oEsp = Nothing
End Sub



Public Function EliminarVistas()
Dim udtTeserror As TipoErrorSummit
Dim irespuesta As Integer

On Error GoTo Cancelar

    If cmdModoEdicion.caption = m_sConsulta Or cmdAceptar.Visible = True Or cmdModoEdicionAtr.caption = m_sConsulta Then
        Exit Function
    End If

    g_Accion = ACCPlantVistasEli

    irespuesta = oMensajes.PreguntaEliminarVistas(lstvwplantillas.selectedItem.Text)
    If irespuesta = vbNo Then Exit Function
    
    Set g_oPlantillaSeleccionada = g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag))
    
    udtTeserror = g_oPlantillaSeleccionada.EliminarVistas()
    If udtTeserror.NumError <> TESnoerror Then
        basErrores.TratarError udtTeserror
    Else
        'Le pone el icono de plantilla sin vistas
        lstvwplantillas.selectedItem.Icon = "Plantilla"
        lstvwplantillas.selectedItem.SmallIcon = "Plantilla"
        g_oPlantillas.Item(CStr(lstvwplantillas.selectedItem.Tag)).VistasConf = False
        g_oPlantillaSeleccionada.VistasConf = False
        
        basSeguridad.RegistrarAccion AccionesSummit.ACCPlantVistasEli, lstvwplantillas.selectedItem.Tag
    End If

    Exit Function
    
Cancelar:
    Exit Function
End Function


Public Sub ConfigurarVistas()
    'Muestra el formulario para configurar las vistas de la plantilla
    
    If cmdModoEdicion.caption = m_sConsulta Or cmdAceptar.Visible = True Or cmdModoEdicionAtr.caption = m_sConsulta Then
        Exit Sub
    End If

    'Abre una nueva ventana de configuraci�n de vistas:
    If Not m_ofrmConfigVistas Is Nothing Then
        Unload m_ofrmConfigVistas
        Set m_ofrmConfigVistas = Nothing
    End If

    Set m_ofrmConfigVistas = New frmConfiguracionVista
    Set m_ofrmConfigVistas.g_oPlantillaSeleccionada = g_oPlantillaSeleccionada
    'edu Incidencia 7513
    'aplicar restriccion para modificar plantillas a formulario de vistas
    m_ofrmConfigVistas.m_bModif = m_bModif
    m_ofrmConfigVistas.Show

End Sub

Private Function ComprobarActualizarPlantilla(Optional ByVal oatrib As CAtributo) As Boolean
    Dim bActualizar As Boolean
    Dim irespuesta As Integer
    Dim bPreguntar As Boolean
    
    bActualizar = True
    bPreguntar = False
    
    'Si hay vistas configuradas deber� hacer las comprobaciones correspondientes:
    If g_oPlantillaSeleccionada.VistasConf = True Then
        If oatrib Is Nothing Then
            bPreguntar = True
        Else
            If g_oPlantillaSeleccionada.ExisteAtribVisibleEnvista(oatrib.ambito, oatrib.idAtribProce) = True Then
                bPreguntar = True
            Else
                bPreguntar = False
            End If
        End If
        
        If bPreguntar = True Then
            If g_oPlantillaSeleccionada.ExistenProcAsociados = True Then
                'Existen procesos abiertos desde la plantilla
                irespuesta = oMensajes.PreguntaModificarPlantilla
                If irespuesta = vbNo Then
                    bActualizar = False
                End If
                
            Else
                'Si no existe ning�n proceso abierto desde la plantilla le indica que deber�
                'reconfigurar las vistas existentes
                irespuesta = oMensajes.PreguntaReconfigurarVista
                If irespuesta = vbNo Then
                    bActualizar = False
                End If
            End If
        End If
    End If
    
    ComprobarActualizarPlantilla = bActualizar
End Function

Public Sub RestaurarConfig(ByVal lIndice As Long, Optional ByVal bEsp As Boolean)
'******************************************************************************
'Restaura la configuraci�n anterior del dato actual. Si se trata
'de las especificaciones la config viene dada por las propiedades del proceso
'Si se restaura a grupo y ninguno de los grupos tiene el dato definido
'se muestra la ventana y se modifica la variable que lo controla.
'bEsp, indica si el dato son las especificaciones
'******************************************************************************
Dim iConfigAnterior As Integer
Dim oGrupo As CGrupo
Dim bESGr As Boolean

    m_bCancelarConfGR = True

    If bEsp Then
        'Son las especificaciones:
        bESGr = False
        For Each oGrupo In m_oGrupos
            If oGrupo.DefEspecificaciones = True Then
                bESGr = True
                Exit For
            End If
        Next
        If bESGr = False Then
            sdbgPlantillas.Columns("GRUPO").Value = "0"
            sdbgPlantillas.Columns("BOTONGRUP").Value = " "
            
            If sdbgPlantillas.Columns("PROCE").Value = "0" And sdbgPlantillas.Columns("ITEM").Value = "0" Then
                sdbgPlantillas.Columns("USAR").Value = "0"
            End If
        End If
    
    Else
        Select Case lIndice
            Case 0 'DEST
                iConfigAnterior = g_oPlantillaSeleccionada.Destino

            Case 1 'PAG
                iConfigAnterior = g_oPlantillaSeleccionada.FormaPago
            
            Case 2 'FECSUM
                iConfigAnterior = g_oPlantillaSeleccionada.FechaSuministro
                    
            Case 3 'PROVE (no obligatorio)
                iConfigAnterior = g_oPlantillaSeleccionada.ProveedorAct
            
            Case 5 'DIST UON (no obligatorio)
                iConfigAnterior = g_oPlantillaSeleccionada.DistribUON
                    
            Case 6 'PRES ANU1 (no obligatorio)
                iConfigAnterior = g_oPlantillaSeleccionada.PresAnualesTipo1
                    
            Case 7 'PRES ANU2 (no obligatorio)
                iConfigAnterior = g_oPlantillaSeleccionada.PresAnualesTipo2
                
            Case 8 'PRES1 (no obligatorio)
                iConfigAnterior = g_oPlantillaSeleccionada.PresTipo1
                        
            Case 9 'PRES2 (no obligatorio)
                iConfigAnterior = g_oPlantillaSeleccionada.PresTipo2
            
            '''Solicitud de compra
            Case 10:
                iConfigAnterior = g_oPlantillaSeleccionada.Solicitud
        End Select
        
        Select Case iConfigAnterior
            Case 0
                sdbgPlantillas.Columns("USAR").Value = "0"
                sdbgPlantillas.Columns("PROCE").Value = "0"
                sdbgPlantillas.Columns("GRUPO").Value = "0"
                sdbgPlantillas.Columns("BOTONGRUP").Value = " "
                sdbgPlantillas.Columns("ITEM").Value = "0"

            Case 1
                sdbgPlantillas.Columns("USAR").Value = "-1"
                sdbgPlantillas.Columns("PROCE").Value = "-1"
                sdbgPlantillas.Columns("GRUPO").Value = "0"
                sdbgPlantillas.Columns("BOTONGRUP").Value = " "
                sdbgPlantillas.Columns("ITEM").Value = "0"

            Case 2
                sdbgPlantillas.Columns("USAR").Value = "-1"
                sdbgPlantillas.Columns("PROCE").Value = "0"
                sdbgPlantillas.Columns("GRUPO").Value = "-1"
                sdbgPlantillas.Columns("BOTONGRUP").Value = "..."
                sdbgPlantillas.Columns("ITEM").Value = "0"
                
            Case 3
                sdbgPlantillas.Columns("USAR").Value = "-1"
                sdbgPlantillas.Columns("PROCE").Value = "0"
                sdbgPlantillas.Columns("GRUPO").Value = "0"
                sdbgPlantillas.Columns("BOTONGRUP").Value = " "
                sdbgPlantillas.Columns("ITEM").Value = "-1"

        End Select
    End If
    
    m_bCancelarConfGR = False
    
    sdbgPlantillas.Columns("USAR").Locked = False
    sdbgPlantillas.Columns("DATO").Locked = True
    sdbgPlantillas.Columns("PROCE").Locked = False
    sdbgPlantillas.Columns("GRUPO").Locked = False
    sdbgPlantillas.Columns("BOTONGRUP").Locked = False
    sdbgPlantillas.Columns("ITEM").Locked = False
    sdbgPlantillas.Update
    
End Sub




Private Sub picDatosConf_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    g_iContadorBloqueo = 0
End Sub

''' <summary>
''' Configura los controles del ambito de datos
''' </summary>
''' <param name="bAdminPub">caracteristica del proceso si es de admin publica </param>
''' <returns></returns>
''' <remarks>Llamada desde:chkAdminPub_click;CargarGridPlantillas;</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub ConfigurarAmbitoDatos(ByVal bAdminPub As Boolean)

    If bAdminPub Then
        chkSubasta.Value = vbUnchecked
        chkPrecios.Value = vbUnchecked
        chkSuministros.Value = vbUnchecked
        chkOferMonProce.Value = vbChecked
        chkArchivProce.Value = vbUnchecked
        picDatosConf(2).Visible = False 'Subasta
        Me.cmdConfigSubasta.Visible = False
        picDatosConf(4).Visible = False 'Alter precios
        chkSuministros.Visible = False
        chkOferMonProce.Visible = False
        chkArchivProce.Visible = False
        picDatosConf(5).Top = picDatosConf(2).Top  'Datos ofertas
        picDatosConf(5).Height = 620
        chkArchivGrupo.Top = 0
        chkArchivItem.Top = chkArchivGrupo.Top + 315
        picDatosConf(3).Top = picDatosConf(5).Top + picDatosConf(5).Height
        picDatosConf(0).Top = picDatosConf(3).Top + picDatosConf(3).Height + 100
        picDatosConf(0).Left = picDatosConf(3).Left
        fraConfProce(2).Height = picDatosConf(5).Height + 800 'frame opciones configuracion datos
        fraConfProce(3).Top = fraConfProce(2).Top + fraConfProce(2).Height  'fraPond
        fraConfProce(4).Top = fraConfProce(2).Top + fraConfProce(2).Height  'frame pond
    Else
        picDatosConf(2).Visible = True
        Me.cmdConfigSubasta.Visible = True
        picDatosConf(4).Visible = True
        chkSuministros.Visible = True
        chkArchivProce.Visible = True
        chkOferMonProce.Visible = True
        chkArchivGrupo.Top = chkArchivProce.Top + 260
        chkArchivItem.Top = chkArchivGrupo.Top + 260
        picDatosConf(5).Height = 1300
        picDatosConf(5).Top = picDatosConf(4).Top + picDatosConf(4).Height + 55
        picDatosConf(3).Top = picDatosConf(5).Top + picDatosConf(5).Height
        picDatosConf(0).Top = picDatosConf(3).Top + picDatosConf(3).Height + 55
        picDatosConf(0).Left = picDatosConf(3).Left
        fraConfProce(2).Height = 2590 'Frame opciones configuracion
        fraConfProce(3).Top = fraConfProce(2).Top + fraConfProce(2).Height  'frame pond
        fraConfProce(4).Top = fraConfProce(2).Top + fraConfProce(2).Height  'frame pond
    End If

End Sub


Public Sub CargarEstructuraMateriales()

    If oCEstructura Is Nothing Then
        Set oCEstructura = CrearCAEstructuraMat(oMensajes)
    End If
    
    oCEstructura.GenerarEstructuraDePlantilla frmPlantillasProce.g_oPlantillaSeleccionada, tvwEstrMat

End Sub

Private Sub tvwEstrMatMod_KeyUp(KeyCode As Integer, Shift As Integer)
    
    If bMouseDown Then
        bMouseDown = False
        oCEstructura.MarcarNodo SelNode, m_bRestringirSoloMaterialesComprador
    End If
    
End Sub

Private Sub tvwEstrMatMod_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.node
Dim tvhti As TVHITTESTINFO

                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrMatMod.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrMatMod.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento
                             
                bMouseDown = True
                Set SelNode = nod
                             
                End If
            End If
        End If
    End If
    
End Sub

Private Sub tvwEstrMatMod_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

    If bMouseDown Then
        bMouseDown = False
        oCEstructura.MarcarNodo SelNode, m_bRestringirSoloMaterialesComprador
        'edu Nuevo requisito:
        ' si se desmarcan todos los nodos desmarcar el check de restriccion
        If SelNode.Checked = False Then
            If oCEstructura.ContarMarcas(tvwEstrMatMod) <= 0 Then
                'edu 2008-02-20: excepcion al requisito anterior:
                'EXCEPTO si el usuario tiene restriccion de material.
                If Not m_bRestringirSoloMaterialesComprador Then
                    CheckMateriales vbUnchecked
                End If
            End If
        Else
            'edu Nuevo requisito:
            ' si se marcan todos los nodos marcar el check de restriccion
            'edu 2008-02-26: excepcion al requisito anterior:
            'INCLUSIVE si el usuario tiene restriccion de material.
            If Not m_bRestringirSoloMaterialesComprador Then
                CheckMateriales vbChecked
            End If
        End If
        DoEvents
    End If

End Sub


Private Sub tvwEstrMatMod_NodeCheck(ByVal node As MSComctlLib.node)

    If bMouseDown Then
        Exit Sub
    End If
    
    bMouseDown = True
    Set SelNode = node

End Sub





''' <summary>
''' Carga el grid de los atributos de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:sdbcGrupoEsp_CloseUp;cmdModoEdicionAtrEsp_Click;CargarGridPlantillas;lstvwplantillas_ItemClick;CargarEspecificaciones</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarGridEspecificaciones()
Dim oatrib As CAtributo
Dim sTipo As String
Dim sAmbito As String
Dim sLibre As String
Dim sSeleccion As String
Dim sBoton As String
Dim sInterno As String
Dim sObl As String
Dim sPedido As String
Dim sValidacion As String
Dim sIntegracion As String

    sdbgAtributosEsp.RemoveAll
    
    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
    
    
    g_oPlantillaSeleccionada.CargarTodosLosAtributosEspecificacion (g_sCodGRPEsp)
        
    
    If g_oPlantillaSeleccionada.AtributosEspecificacion.Count = 0 Then Exit Sub
    
    If Not gParametrosGenerales.gbPedidosDirectos Then
        sdbgAtributosEsp.Columns("PEDIDO").Visible = False
    End If
    
    'Comprobacion de si hay integracion de pedidos o de adjudicacion para mostrar la columna de integracion
    If Not g_oPlantillaSeleccionada.HayIntegracionPedAdj Then
       sdbgAtributosEsp.Columns("INTEGRACION").Visible = False
    End If
    
    sdbgAtributosEsp.Columns("VALOR").DropDownHwnd = 0
    
    For Each oatrib In g_oPlantillaSeleccionada.AtributosEspecificacion
        
        Select Case oatrib.Tipo
            Case 1: sTipo = m_asAtrib(3) 'texto
            Case 2: sTipo = m_asAtrib(0) 'numerico
            Case 3: sTipo = m_asAtrib(1) 'fecha
            Case 4: sTipo = m_asAtrib(2) 'si/no
        End Select
        
        Select Case oatrib.ambito
            Case 1: sAmbito = m_asAtrib(4)
            Case 2: sAmbito = m_asAtrib(5)
            Case 3: sAmbito = m_asAtrib(6)
        End Select
        
        Select Case oatrib.Validacion
            Case 0: sValidacion = m_asAtrib(13)
            Case 1: sValidacion = m_asAtrib(14)
            Case 2: sValidacion = m_asAtrib(15)
        End Select

        sLibre = 0
        sSeleccion = 0
        sBoton = ""
        If oatrib.TipoIntroduccion = 0 Then
            sLibre = "-1"
            sBoton = ""
        Else
            If oatrib.TipoIntroduccion = 1 And oatrib.Tipo <> TipoBoolean Then
                sSeleccion = "-1"
                sBoton = "..."
            End If
        End If
        
        If oatrib.interno = True Then
            sInterno = "-1"
        Else
            sInterno = 0
        End If
        If oatrib.Obligatorio = True Then
            sObl = "-1"
        Else
            sObl = 0
        End If
        
        If oatrib.pedido = True Then
            sPedido = "-1"
        Else
            sPedido = 0
        End If
        
        Select Case oatrib.ConfIntegracionAtributo
         Case TipoIntegracionAtributo.NoTiene:              sIntegracion = ""
         Case TipoIntegracionAtributo.Adjudicacion:         sIntegracion = "Adjudicacion"
         Case TipoIntegracionAtributo.pedido:               sIntegracion = "Pedido"
         Case TipoIntegracionAtributo.AdjudicacionPedido:   sIntegracion = "Adjudicacion/Pedido"
        
        End Select
                    
        Select Case oatrib.Tipo
            Case 1: sdbgAtributosEsp.AddItem oatrib.Atrib & Chr(m_lSeparador) & sInterno & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & IIf(IsNull(oatrib.Descripcion), "", "...") & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.valorText & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sObl & Chr(m_lSeparador) & sIntegracion & Chr(m_lSeparador) & sValidacion & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Descripcion
            Case 2: sdbgAtributosEsp.AddItem oatrib.Atrib & Chr(m_lSeparador) & sInterno & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & IIf(IsNull(oatrib.Descripcion), "", "...") & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.valorNum & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sObl & Chr(m_lSeparador) & sIntegracion & Chr(m_lSeparador) & sValidacion & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Descripcion
            Case 3: sdbgAtributosEsp.AddItem oatrib.Atrib & Chr(m_lSeparador) & sInterno & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & IIf(IsNull(oatrib.Descripcion), "", "...") & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.valorFec & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sObl & Chr(m_lSeparador) & sIntegracion & Chr(m_lSeparador) & sValidacion & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Descripcion
            Case 4:
                    If IsNull(oatrib.valorBool) Then
                        sdbgAtributosEsp.AddItem oatrib.Atrib & Chr(m_lSeparador) & sInterno & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & IIf(IsNull(oatrib.Descripcion), "", "...") & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sObl & Chr(m_lSeparador) & sIntegracion & Chr(m_lSeparador) & sValidacion & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Descripcion
                    Else
                        If oatrib.valorBool Then
                            sdbgAtributosEsp.AddItem oatrib.Atrib & Chr(m_lSeparador) & sInterno & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & IIf(IsNull(oatrib.Descripcion), "", "...") & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & m_sIdiTrue & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sObl & Chr(m_lSeparador) & sIntegracion & Chr(m_lSeparador) & sValidacion & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Descripcion
                        Else
                            sdbgAtributosEsp.AddItem oatrib.Atrib & Chr(m_lSeparador) & sInterno & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & IIf(IsNull(oatrib.Descripcion), "", "...") & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & m_sIdiFalse & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sObl & Chr(m_lSeparador) & sIntegracion & Chr(m_lSeparador) & sValidacion & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Descripcion
                        End If
                    End If

        End Select
            
    Next
    
    sdbgAtributosEsp.SplitterPos = 1
    
    sdbgAtributosEsp.MoveFirst

End Sub

''' <summary>
''' Carga el combo del tab de especificaciones
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:cmdCodigo_Click();sdbgGrupos_BeforeUpdate;sdbgGrupos_BeforeUpdate;CargarEspecificaciones</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarComboGruposEsp()
Dim oGrupo As CGrupo

    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub

    'Cargo la combo de grupos
    sdbcGrupoEsp.RemoveAll
    If g_oPlantillaSeleccionada.EspecProce Or g_oPlantillaSeleccionada.EspecGrupo Or g_oPlantillaSeleccionada.EspecItem Then
        sdbcGrupoEsp.AddItem "************" & Chr(m_lSeparador) & m_asAtrib(10)
    End If
        
    If Not g_oPlantillaSeleccionada.Grupos Is Nothing Then
        If g_oPlantillaSeleccionada.EspecGrupo Or g_oPlantillaSeleccionada.EspecItem Then
            For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                sdbcGrupoEsp.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den
            Next
        End If
    End If
    
    If g_oPlantillaSeleccionada.EspecProce Or g_oPlantillaSeleccionada.EspecGrupo Or g_oPlantillaSeleccionada.EspecItem Then
        g_sCodGRPEsp = ""
        sdbcGrupoEsp.Columns(0).Value = "************"
        sdbcGrupoEsp.Text = m_asAtrib(10)
    Else
        sdbcGrupoEsp.MoveFirst
        g_sCodGRPEsp = sdbcGrupoEsp.Columns(0).Value
        sdbcGrupoEsp.Text = sdbcGrupoEsp.Columns(1).Value
    End If


End Sub

''' <summary>
''' Carga el combo del tab de especificaciones
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:cmdCodigo_Click();sdbgGrupos_BeforeUpdate;sdbgGrupos_BeforeUpdate;CargarEspecificaciones</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarComboGruposEspDesdeAmbitoDeDatos()
Dim oGrupo As CGrupo
Dim sSeleccion As String
Dim bEsta As Boolean

    
   If g_oPlantillaSeleccionada Is Nothing Then Exit Sub

   sSeleccion = sdbcGrupoEsp.Columns(0).Value
    'Cargo la combo de grupos
    sdbcGrupoEsp.RemoveAll
    If g_oPlantillaSeleccionada.EspecProce Or g_oPlantillaSeleccionada.EspecGrupo Or g_oPlantillaSeleccionada.EspecItem Then
        sdbcGrupoEsp.AddItem "************" & Chr(m_lSeparador) & m_asAtrib(10)
        If sSeleccion = "************" Then
            bEsta = True
        End If
    End If
        
    If Not g_oPlantillaSeleccionada.Grupos Is Nothing Then
        If g_oPlantillaSeleccionada.EspecGrupo Or g_oPlantillaSeleccionada.EspecItem Then
            For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                sdbcGrupoEsp.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den
                If sSeleccion = oGrupo.Codigo Then
                    bEsta = True
                End If
            Next
        End If
    End If
        
    If Not bEsta Then
        sdbcGrupoEsp.MoveFirst
        If sdbcGrupoEsp.Columns(0).Value = "************" Then
            g_sCodGRPEsp = ""
            sdbcGrupoEsp.Text = m_asAtrib(10)
        Else
            g_sCodGRPEsp = sdbcGrupoEsp.Columns(0).Value
            sdbcGrupoEsp.Text = sdbcGrupoEsp.Columns(1).Value
        End If
        
        CargarGridEspecificaciones
    End If


End Sub

''' <summary>
''' CLick sobre ek campo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbcGrupoEsp_Click()
    sdbcGrupoEsp.Value = ""
End Sub


''' <summary>
''' Clouseup del combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbcGrupoEsp_CloseUp()

    If sdbcGrupoEsp.Columns(0).Value = "************" Then
        g_sCodGRPEsp = ""
        sdbcGrupoEsp.Value = sdbcGrupoEsp.Columns(1).Value
    Else
        sdbcGrupoEsp.Value = sdbcGrupoEsp.Columns(0).Value
        If sdbcGrupoEsp.Columns(1).Value <> "" Then
            sdbcGrupoEsp.Text = sdbcGrupoEsp.Columns(0).Value & " - " & sdbcGrupoEsp.Columns(1).Value
        End If
        g_sCodGRPEsp = sdbcGrupoEsp.Columns(0).Value
    End If
    CargarGridEspecificaciones 'Tengo q cargar la coleccion cuando
End Sub

''' <summary>
''' Inicializacion de los datos del combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>


Private Sub sdbcGrupoEsp_InitColumnProps()
    sdbcSeleccion.DataFieldList = "Column 0"
    sdbcSeleccion.DataFieldToDisplay = "Column 0"
End Sub

''' <summary>
''' Posicionamiento en el combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbcGrupoEsp_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcGrupoEsp.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcGrupoEsp.Rows - 1
            bm = sdbcGrupoEsp.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGrupoEsp.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGrupoEsp.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

''' <summary>
''' DropDown del combo de ambito
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddAmbitoEspec_DropDown()

If sdbgAtributosEsp.Columns("AMBITO").Locked Then
    sdbddAmbitoEspec.Enabled = False
    Exit Sub
End If

sdbddAmbitoEspec.RemoveAll

If g_sCodGRPEsp <> "" Then
        If chkAdminPub.Value = vbChecked Then
            'Si es un proceso de administraci�n p�blica no se podr�n definir atributos a nivel
            'de proceso
            If g_oPlantillaSeleccionada.EspecGrupo = True Then
                sdbddAmbitoEspec.AddItem 1 & Chr(m_lSeparador) & m_asAtrib(5)
                sdbddAmbitoEspec.MoveNext
            End If
            If g_oPlantillaSeleccionada.EspecItem = True Then
                sdbddAmbitoEspec.AddItem 2 & Chr(m_lSeparador) & m_asAtrib(6)
                sdbddAmbitoEspec.MoveNext
            End If
        Else
            If g_oPlantillaSeleccionada.EspecGrupo = True Then
                If g_oPlantillaSeleccionada.Grupos.Item(CStr(g_sCodGRPEsp)).DefEspecificaciones = True Then
                    sdbddAmbitoEspec.AddItem 1 & Chr(m_lSeparador) & m_asAtrib(5)
                    sdbddAmbitoEspec.MoveNext
                End If
            End If
        
            If g_oPlantillaSeleccionada.EspecItem = True Then
                sdbddAmbitoEspec.AddItem 2 & Chr(m_lSeparador) & m_asAtrib(6)
                sdbddAmbitoEspec.MoveNext
            End If
        End If
Else
    If chkAdminPub.Value = vbChecked Then
        'Si es un proceso de administraci�n p�blica no se podr�n definir atributos a nivel
        'de proceso
            If g_oPlantillaSeleccionada.EspecGrupo = True Then
                sdbddAmbitoEspec.AddItem 1 & Chr(m_lSeparador) & m_asAtrib(5)
                sdbddAmbitoEspec.MoveNext
            End If
        
            If g_oPlantillaSeleccionada.EspecItem = True Then
                sdbddAmbitoEspec.AddItem 2 & Chr(m_lSeparador) & m_asAtrib(6)
                sdbddAmbitoEspec.MoveNext
            End If
    Else
        If g_oPlantillaSeleccionada.EspecProce Then
                sdbddAmbitoEspec.AddItem 0 & Chr(m_lSeparador) & m_asAtrib(4)
                sdbddAmbitoEspec.MoveNext
        
            If g_oPlantillaSeleccionada.EspecGrupo = True Then
                sdbddAmbitoEspec.AddItem 1 & Chr(m_lSeparador) & m_asAtrib(5)
                sdbddAmbitoEspec.MoveNext
            End If
        
            If g_oPlantillaSeleccionada.EspecItem = True Then
                sdbddAmbitoEspec.AddItem 2 & Chr(m_lSeparador) & m_asAtrib(6)
                sdbddAmbitoEspec.MoveNext
            End If
        
        Else
            If g_oPlantillaSeleccionada.EspecGrupo = True Then
                sdbddAmbitoEspec.AddItem 1 & Chr(m_lSeparador) & m_asAtrib(5)
                sdbddAmbitoEspec.MoveNext
            End If
        
            If g_oPlantillaSeleccionada.EspecItem = True Then
                sdbddAmbitoEspec.AddItem 2 & Chr(m_lSeparador) & m_asAtrib(6)
                sdbddAmbitoEspec.MoveNext
            End If
        
        End If
    End If
End If

End Sub

''' <summary>
''' Inicializacion de columnas del combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddAmbitoEspec_InitColumnProps()
    sdbddAmbitoEspec.DataFieldList = "Column 0"
    sdbddAmbitoEspec.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Posicionamiento  en el combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>


Private Sub sdbddAmbitoEspec_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
Dim i As Long
Dim bm As Variant

    On Error Resume Next
   
    sdbddAmbitoEspec.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddAmbitoEspec.Rows - 1
            bm = sdbddAmbitoEspec.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddAmbitoEspec.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributosEsp.Columns("AMBITO").Value = Mid(sdbddAmbitoEspec.Columns(0).CellText(bm), 1, Len(Text))
                sdbddAmbitoEspec.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>
''' Validacion de la seleccion realizada en el combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddAmbitoEspec_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        ''' Comprobar la existencia en la lista
        If g_oPlantillaSeleccionada.AdminPub = True Then
            Select Case UCase(sdbgAtributosEsp.Columns("AMBITO").Text)
                Case UCase(m_asAtrib(5)): RtnPassed = True  'Grupo
                Case UCase(m_asAtrib(6)): RtnPassed = True  'Item
                Case Else: sdbgAtribPlant.Columns("AMBITO").Text = ""
            End Select
        Else
            Select Case UCase(sdbgAtributosEsp.Columns("AMBITO").Text)
                Case UCase(m_asAtrib(4)): RtnPassed = True  'Proceso
                Case UCase(m_asAtrib(5)): RtnPassed = True  'Grupo
                Case UCase(m_asAtrib(6)): RtnPassed = True  'Item
                Case Else: sdbgAtributosEsp.Columns("AMBITO").Text = ""
            End Select
        End If
    End If
    
        
End Sub






''' <summary>
''' Evento producido en el campo del valor del atributo al hacer click para expandir la lista de valores del atributo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en la lista de valores de un atributo </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_DropDown()

Dim oLista As CValorPond
Dim iIndice As Integer


    If sdbgAtributosEsp.Columns("VALOR").Locked Then
        sdbddValor.DroppedDown = False
        Exit Sub
    End If
    
    
iIndice = 1

sdbddValor.RemoveAll

    Select Case sdbgAtributosEsp.Columns("idtipo").Value
    
    Case 1, 2, 3
            g_oPlantillaSeleccionada.AtributosEspecificacion.Item(sdbgAtributosEsp.Columns("IDATRIB").Value).CargarListaDeValoresDef
            For Each oLista In g_oPlantillaSeleccionada.AtributosEspecificacion.Item(sdbgAtributosEsp.Columns("IDATRIB").Value).ListaPonderacion
                sdbddValor.AddItem iIndice & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
            'End If
        
    Case 4
        sdbddValor.AddItem "" & Chr(m_lSeparador) & ""
        sdbddValor.AddItem 1 & Chr(m_lSeparador) & m_sIdiTrue
        sdbddValor.AddItem 0 & Chr(m_lSeparador) & m_sIdiFalse
        
        
    End Select


End Sub

''' <summary>
''' Inicializacion de las columnas del combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Posicionamiento en la lista del combo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en la lista de valores de un atributo </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributosEsp.Columns("VALOR").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

''' <summary>
''' Validacion de la seleccion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en la lista de valores de un atributo </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
Dim bExiste As Boolean
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim oatrib As CAtributo

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        
        Set oatrib = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(sdbgAtributosEsp.Columns("IDATRIB").Value)
        If oatrib Is Nothing Then Exit Sub

        If oatrib.TipoIntroduccion = Introselec Then
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributosEsp.Columns("VALOR").Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If oatrib.Tipo = TipoBoolean Then
                If UCase(m_sIdiTrue) = UCase(sdbgAtributosEsp.Columns("VALOR").Text) Then
                    bExiste = True
                End If
                If UCase(m_sIdiFalse) = UCase(sdbgAtributosEsp.Columns("VALOR").Text) Then
                    bExiste = True
                End If

            End If
        End If
        If Not bExiste Then
            sdbgAtributosEsp.Columns("VALOR").Text = ""
            oMensajes.NoValido sdbgAtributosEsp.Columns("VALOR").caption
            RtnPassed = False
            m_bModError = True
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub





''Funciones grid atributos especificacion---------------

''' <summary>
''' Funcion que salta al hacer un delete en el grid de atributos de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtributosEsp_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgAtributosEsp.Rows = 0) Then
        Exit Sub
    End If
    If Me.Visible Then sdbgAtributosEsp.SetFocus
    If sdbgAtributosEsp.Row >= sdbgAtributosEsp.Rows Then
        sdbgAtributosEsp.Bookmark = sdbgAtributosEsp.RowBookmark(sdbgAtributosEsp.Rows - 1)
    Else
        sdbgAtributosEsp.Bookmark = sdbgAtributosEsp.RowBookmark(sdbgAtributosEsp.Row)
    End If
End Sub


''' <summary>
''' Funcion que salta al hacer una insercion en el grid de atributos de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtributosEsp_AfterInsert(RtnDispErrMsg As Integer)
RtnDispErrMsg = 0
    
End Sub

''Funciones grid atributos especificacion---------------

''' <summary>
''' Funcion que salta al hacer una modificacion en el grid de atributos de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtributosEsp_AfterUpdate(RtnDispErrMsg As Integer)

    RtnDispErrMsg = 0
    If m_bAnyaError = False And m_bModError = False Then
        cmdDeshacerAtrEsp.Enabled = False
    End If
    
End Sub

''' <summary>
''' Funcion que salta antes de hacer un delete en el grid de atributos de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtributosEsp_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub



''' <summary>
''' Evento de la grid del taba de atributos de especificacion, comprueba que los valores son correctos
''' </summary>
''' <returns></returns>
''' <param name="Cancel">Cancelacion de la actualizacion </param>
''' <remarks>Llamada desde: Al hacer un cambio en la grid, al cambiar de linea</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtributosEsp_BeforeUpdate(Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim m_oAtributoAnyadir As CAtributo

    If sdbgAtributosEsp.col = -1 And g_sOrigen = "" Then Exit Sub
    
    Cancel = False
    m_bValError = False
    
    If sdbgAtributosEsp.Columns("COD").Value = "" Then
        oMensajes.NoValido sdbgAtributosEsp.Columns("COD").caption
        Cancel = True
        GoTo Salir
        Exit Sub
    End If
    If Trim(sdbgAtributosEsp.Columns("DEN").Value) = "" Then
        oMensajes.NoValido sdbgAtributosEsp.Columns("DEN").caption
        Cancel = True
        GoTo Salir
    End If
        
    If Trim(sdbgAtributosEsp.Columns("TIPO").Value) = "" Then
        oMensajes.NoValido sdbgAtributosEsp.Columns("TIPO").caption
        Cancel = True
        GoTo Salir
    End If
    
    If sdbgAtributosEsp.Columns("AMBITO").Text <> "" Then
        If sdbgAtributosEsp.Columns("AMBITO").Text <> m_asAtrib(4) Then
            If sdbgAtributosEsp.Columns("AMBITO").Text <> m_asAtrib(5) Then
                If sdbgAtributosEsp.Columns("AMBITO").Text <> m_asAtrib(6) Then
                    oMensajes.NoValido sdbgAtributosEsp.Columns("AMBITO").caption
                    If Me.Visible Then sdbgAtributosEsp.SetFocus
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
    Else
        sdbgAtributosEsp.Columns("AMBITO").Text = m_asAtrib(6)
    End If

If sdbgAtributosEsp.Columns("Valor").Text <> "" Then

    Select Case UCase(sdbgAtributosEsp.Columns("TIPO_DATOS").Text)

        Case 2 'Numero
                    If (Not IsNumeric(sdbgAtributosEsp.Columns("Valor").Text)) Then
                        oMensajes.AtributoValorNoValido ("TIPO2")
                        Cancel = True
                        GoTo Salir

                    Else
                        If sdbgAtributosEsp.Columns("MIN").Text <> "" And sdbgAtributosEsp.Columns("MAX").Text <> "" Then
                            If StrToDbl0(sdbgAtributosEsp.Columns("MIN").Text) > StrToDbl0(sdbgAtributosEsp.Columns("Valor").Text) Or StrToDbl0(sdbgAtributosEsp.Columns("MAX").Text) < StrToDbl0(sdbgAtributosEsp.Columns("Valor").Text) Then
                                oMensajes.ValorEntreMaximoYMinimo sdbgAtributosEsp.Columns("MIN").Text, sdbgAtributosEsp.Columns("MAX").Text
                                Cancel = True
                                GoTo Salir
                            End If
                        End If
                    
                    End If



        Case 3 'Fecha
                        If (Not IsDate(sdbgAtributosEsp.Columns("Valor").Text) And sdbgAtributosEsp.Columns("Valor").Text <> "") Then
                            oMensajes.AtributoValorNoValido ("TIPO3")
                            Cancel = True
                            GoTo Salir

                        Else
                            If sdbgAtributosEsp.Columns("MIN").Text <> "" And sdbgAtributosEsp.Columns("MAX").Text <> "" Then
                                If CDate(sdbgAtributosEsp.Columns("MIN").Text) > CDate(sdbgAtributosEsp.Columns("Valor").Text) Or CDate(sdbgAtributosEsp.Columns("MAX").Text) < CDate(sdbgAtributosEsp.Columns("Valor").Text) Then
                                 oMensajes.ValorEntreMaximoYMinimo sdbgAtributosEsp.Columns("MIN").Text, sdbgAtributosEsp.Columns("MAX").Text
                                    'sdbgatributos.CancelUpdate
                                    Cancel = True
                                    GoTo Salir

                                End If
                            End If
                    End If
    End Select

End If

    
    If sdbgAtributosEsp.IsAddRow Then   'Insertar
        
        m_bModificarDefAtrib = False
            
        Set m_oAtributoAnyadir = oFSGSRaiz.Generar_CAtributo
        m_oAtributoAnyadir.Id = sdbgAtributosEsp.Columns("idATRIB").Value
        
        Select Case CInt(sdbgAtributosEsp.Columns("TIPO_DATOS").Text)
        
            Case 1: m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoString
            Case 2: m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoNumerico
            Case 3: m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoFecha
            Case 4: m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoBoolean
                          
        End Select
                    
        Select Case StrToDbl0(sdbgAtributosEsp.Columns("TIPO_DATOS").Value)
            
            Case 1: m_oAtributoAnyadir.valorText = sdbgAtributosEsp.Columns("Valor").Value
            Case 2:
                    If sdbgAtributosEsp.Columns("Valor").Value = "" Then
                        m_oAtributoAnyadir.valorNum = Null
                    Else
                        m_oAtributoAnyadir.valorNum = sdbgAtributosEsp.Columns("Valor").Value
                    End If
            Case 3:
                    m_oAtributoAnyadir.valorFec = sdbgAtributosEsp.Columns("Valor").Value
                    If sdbgAtributosEsp.Columns("Valor").Value = "" Then
                        m_oAtributoAnyadir.valorFec = Null
                    Else
                        m_oAtributoAnyadir.valorFec = sdbgAtributosEsp.Columns("Valor").Value
                    End If
            Case 4:
                Select Case sdbgAtributosEsp.Columns("Valor").Value
                    Case m_sIdiTrue: m_oAtributoAnyadir.valorBool = 1
                    Case m_sIdiFalse: m_oAtributoAnyadir.valorBool = 0
                    Case Else: m_oAtributoAnyadir.valorBool = ""
                End Select
        End Select
                
        Select Case UCase(sdbgAtributosEsp.Columns("AMBITO").Value)
    
            Case UCase(m_asAtrib(4))
                m_oAtributoAnyadir.ambito = TipoAmbitoProceso.AmbProceso
            Case UCase(m_asAtrib(5))
                m_oAtributoAnyadir.ambito = TipoAmbitoProceso.AmbGrupo
            Case UCase(m_asAtrib(6))
                m_oAtributoAnyadir.ambito = TipoAmbitoProceso.AmbItem
        End Select
                
                
        teserror = g_oPlantillaSeleccionada.AnyadirAtributoEspecificacion(m_oAtributoAnyadir.Id, g_sCodGRPEsp)
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            cmdDeshacerAtr_Click
            Set m_oAtributoAnyadir = Nothing
            Exit Sub
        End If
        
        RegistrarAccion ACCPlantAtribEspAnya, "Atributo a�adido: " & m_oAtributoAnyadir.Id & " Grupo " & g_sCodGRPEsp
                    
        Set m_oAtributoAnyadir = Nothing
    
    Else 'Modificar
    
        Set m_oAtributoEnEdicion = Nothing
        Set m_oAtributoEnEdicion = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Text))
        
    Dim bDato As Boolean
    
    If m_oAtributoEnEdicion.pedido Then

        Select Case UCase(sdbgAtributosEsp.Columns("AMBITO").Value)
    
        Case UCase(m_asAtrib(4))
                
            If m_oAtributoEnEdicion.ambito <> TipoAmbitoProceso.AmbProceso Then

                g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).IDPlantilla = g_oPlantillaSeleccionada.Id
                If sdbcGrupoEsp.Columns(0).Value <> "************" Then
                    bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).pedido
                    teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarPedidoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("PEDIDO").Value), sdbcGrupoEsp.Columns(0).Value, TipoAmbitoProceso.AmbProceso)
                Else
                    bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).pedido
                    teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarPedidoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("PEDIDO").Value), , TipoAmbitoProceso.AmbProceso)
                End If
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                End If

                ''Comprobacion de que no existe ese atributo de ese ambito marcado para pedido
            End If
     
            
                    
                                    
        Case UCase(m_asAtrib(5))

            If m_oAtributoEnEdicion.ambito <> TipoAmbitoProceso.AmbGrupo And m_oAtributoEnEdicion.ambito <> TipoAmbitoProceso.AmbItem Then
            
                g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).IDPlantilla = g_oPlantillaSeleccionada.Id
                If sdbcGrupoEsp.Columns(0).Value <> "************" Then
                    bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).pedido
                    teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarPedidoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("PEDIDO").Value), sdbcGrupoEsp.Columns(0).Value, TipoAmbitoProceso.AmbGrupo)
                Else
                    bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).pedido
                    teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarPedidoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("PEDIDO").Value), , TipoAmbitoProceso.AmbGrupo)
                End If
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                End If
            
            End If

        Case UCase(m_asAtrib(6))
        If m_oAtributoEnEdicion.ambito <> TipoAmbitoProceso.AmbItem And m_oAtributoEnEdicion.ambito <> TipoAmbitoProceso.AmbGrupo Then
                g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).IDPlantilla = g_oPlantillaSeleccionada.Id
                If sdbcGrupoEsp.Columns(0).Value <> "************" Then
                    bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).pedido
                    teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarPedidoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("PEDIDO").Value), sdbcGrupoEsp.Columns(0).Value, TipoAmbitoProceso.AmbItem)
                Else
                    bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).pedido
                    teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarPedidoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("PEDIDO").Value), , TipoAmbitoProceso.AmbItem)
                End If
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    If Me.Visible Then sdbgAtribPlant.SetFocus
                    Cancel = True
                    GoTo Salir
                End If
        
        End If
            
        End Select
    End If
        
               
        If sdbgAtributosEsp.Columns("INTERNO").Value = "0" Or sdbgAtributosEsp.Columns("INTERNO").Value = "" Then
            m_oAtributoEnEdicion.interno = False
        Else
            m_oAtributoEnEdicion.interno = True
        End If
    
                                       
        Select Case UCase(sdbgAtributosEsp.Columns("AMBITO").Text)
            Case UCase(m_asAtrib(4)): m_oAtributoEnEdicion.ambito = AmbProceso
            Case UCase(m_asAtrib(5)): m_oAtributoEnEdicion.ambito = AmbGrupo
            Case UCase(m_asAtrib(6)): m_oAtributoEnEdicion.ambito = AmbItem
            Case "": m_oAtributoEnEdicion.ambito = AmbItem
        End Select
        
        If sdbgAtributosEsp.Columns("OBLIGATORIO").Value = "0" Or sdbgAtributosEsp.Columns("OBLIGATORIO").Value = "" Then
            m_oAtributoEnEdicion.Obligatorio = False
        Else
            m_oAtributoEnEdicion.Obligatorio = True
        End If
        
        Select Case UCase(sdbgAtributosEsp.Columns("VALIDACION").Text)
            Case UCase(m_asAtrib(13)): m_oAtributoEnEdicion.Validacion = TValidacionAtrib.Apertura
            Case UCase(m_asAtrib(14)): m_oAtributoEnEdicion.Validacion = TValidacionAtrib.Publicacion
            Case UCase(m_asAtrib(15)): m_oAtributoEnEdicion.Validacion = TValidacionAtrib.TVAdjudicacion
        End Select
        
        Select Case sdbgAtributosEsp.Columns("TIPO_DATOS").Value
            Case 1: m_oAtributoEnEdicion.valorText = sdbgAtributosEsp.Columns("Valor").Text
                
            Case 2: m_oAtributoEnEdicion.valorNum = sdbgAtributosEsp.Columns("Valor").Text
            Case 3: m_oAtributoEnEdicion.valorFec = sdbgAtributosEsp.Columns("Valor").Text
            Case 4:
                    Select Case sdbgAtributosEsp.Columns("Valor").Text
                        Case m_sIdiTrue: m_oAtributoEnEdicion.valorBool = 1
                        Case m_sIdiFalse: m_oAtributoEnEdicion.valorBool = 0
                        Case Else: m_oAtributoEnEdicion.valorBool = ""
                    End Select
        End Select

        
        m_oAtributoEnEdicion.IDPlantilla = g_oPlantillaSeleccionada.Id
        
        If g_sCodGRPEsp <> "" Then
            teserror = g_oPlantillaSeleccionada.ModificarAtributoEspecificacion(sdbgAtributosEsp.Columns("IDATRIB").Value, g_sCodGRPEsp)
        Else
            teserror = g_oPlantillaSeleccionada.ModificarAtributoEspecificacion(sdbgAtributosEsp.Columns("IDATRIB").Value, "")
        End If
        
        m_oAtributoEnEdicion.IDPlantilla = ""
        
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgAtribPlant.SetFocus
            m_bValError = True
        End If
    
        RegistrarAccion ACCPlantAtribEspMod, "Atributo modificado: " & sdbgAtributosEsp.Columns("IDATRIB").Value & " Grupo " & g_sCodGRPEsp
    End If

    sdbgAtributosEsp.Columns("VALOR").DropDownHwnd = 0

Exit Sub

Salir:
        
    m_bValError = Cancel
    If Me.Visible Then sdbgAtribPlant.SetFocus
End Sub

''' <summary>
''' Evento de la grid del tab de atributos al hacer click sobre una columna de tipo boton
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer un cambio en la grid,</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub sdbgAtributosEsp_BtnClick()

    If sdbgAtributosEsp.col < 0 Then Exit Sub

    If sdbgAtributosEsp.Columns("COD").Text = "" Then Exit Sub
    
    If sdbgAtributosEsp.DataChanged = True Then
        sdbgAtributosEsp.Update
    End If
        
    If sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Name = "DESCR" Then
        If Trim(sdbgAtributosEsp.Columns("HIDENDESCR").Value) <> "" Then
            frmATRIBDescr.g_bEdicion = False
            frmATRIBDescr.txtDescr.Text = sdbgAtributosEsp.Columns("HIDENDESCR").Value
            frmATRIBDescr.Show 1
        Else
            oMensajes.MensajeOKOnly (644)
            Exit Sub
        End If
    End If
            
    
    If sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Name = "VALOR" Then
    
        If Not m_bModoEdicionAtrEsp Then
            frmATRIBDescr.g_bEdicion = False
        Else
            frmATRIBDescr.g_bEdicion = True
        End If
        frmATRIBDescr.caption = sdbgAtributosEsp.Columns("COD").Value & ": " & sdbgAtributosEsp.Columns("DEN").Value
        frmATRIBDescr.txtDescr.Text = sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Value
        frmATRIBDescr.g_sOrigen = "PLANT_ATRIB_ESP_VAL"
        frmATRIBDescr.Show 1
        
        
        If sdbgAtributosEsp.DataChanged Then
                'Set Parent.g_oAtributoEnEdicion = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value))
            Set m_oAtributoEnEdicion = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value))
            sdbgAtributosEsp.Update
        End If

    End If



End Sub

''' <summary>
''' Evento de la grid del tab de atributos al hacer algun cambio sobre un campo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer un cambio en la grid,</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub sdbgAtributosEsp_Change()
Dim teserror As TipoErrorSummit
Dim bDato As Boolean
Dim iAmbito As TipoAmbitoProceso


   If cmdDeshacerAtrEsp.Enabled = False Then
        cmdDeshacerAtrEsp.Enabled = True
    End If

    If sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Locked Then
        sdbgAtributosEsp.DataChanged = False
        Exit Sub
    End If
    
    If sdbgAtributosEsp.Columns("COD").Value = "" Then 'SI COD = "" NO DEJAMOS MODIFICAR
        If sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Name = "INTERNO" Then
            sdbgAtributosEsp.Columns("INTERNO").Value = 0
        End If
        If sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Name = "AMBITO" Then
            sdbgAtributosEsp.Columns("AMBITO").Value = ""
        End If
        If sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Name = "OBLIGATORIO" Then
            sdbgAtributosEsp.Columns("OBL").Value = 0
        End If
                
        sdbgAtributosEsp.CancelUpdate
        Exit Sub
    
    End If

        
    If sdbgAtributosEsp.col < 0 Then Exit Sub
    

    
    If (sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Name = "INTERNO") Then
        If Not sdbgAtributosEsp.IsAddRow Then
            g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).IDPlantilla = g_oPlantillaSeleccionada.Id
            If sdbcGrupoEsp.Columns(0).Value <> "************" Then
                bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).interno
                teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarinternoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("INTERNO").Value), sdbcGrupoEsp.Columns(0).Value)
            Else
                bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).interno
                teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarinternoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("INTERNO").Value))
            End If
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                sdbgAtributosEsp.Columns("INTERNO").Value = BooleanToSQLBinary(bDato)
            End If
                sdbgAtributosEsp.Update
            Exit Sub
        End If
    End If


    If (sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Name = "PEDIDO") Then
        If Not sdbgAtributosEsp.IsAddRow Then
        
               Select Case UCase(sdbgAtributosEsp.Columns("AMBITO").Text)
                Case UCase(m_asAtrib(4)): iAmbito = AmbProceso
                Case UCase(m_asAtrib(5)): iAmbito = AmbGrupo
                Case UCase(m_asAtrib(6)): iAmbito = AmbItem
                Case "": iAmbito = AmbItem
            End Select
        
            g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).IDPlantilla = g_oPlantillaSeleccionada.Id
            If sdbcGrupoEsp.Columns(0).Value <> "************" Then
                bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).pedido
                teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarPedidoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("PEDIDO").Value), sdbcGrupoEsp.Columns(0).Value, iAmbito)
            Else
                bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).pedido
                teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarPedidoEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("PEDIDO").Value), , iAmbito)
            End If
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                sdbgAtributosEsp.Columns("PEDIDO").Value = BooleanToSQLBinary(bDato)
            End If
            sdbgAtributosEsp.Update
            Exit Sub
        End If
    End If

    If (sdbgAtributosEsp.Columns(sdbgAtributosEsp.col).Name = "OBLIGATORIO") Then
        If Not sdbgAtributosEsp.IsAddRow Then
            g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).IDPlantilla = g_oPlantillaSeleccionada.Id
            If sdbcGrupoEsp.Columns(0).Value <> "************" Then
                bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).Obligatorio
                teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarObligatorioEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("OBLIGATORIO").Value), sdbcGrupoEsp.Columns(0).Value)
            Else
                bDato = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).Obligatorio
                teserror = g_oPlantillaSeleccionada.AtributosEspecificacion.Item(CStr(sdbgAtributosEsp.Columns("IDATRIB").Value)).ModificarObligatorioEspecificacionPlant(GridCheckToBoolean(sdbgAtributosEsp.Columns("OBLIGATORIO").Value))
            End If
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                sdbgAtributosEsp.Columns("OBLIGATORIO").Value = BooleanToSQLBinary(bDato)
            End If
            sdbgAtributosEsp.Update
            Exit Sub
        End If
    End If
''
End Sub


''' <summary>
''' Evento del combo de la columna validacion de la grid del tab de atributos
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub sdbddValidacion_DropDown()

If sdbgAtributosEsp.Columns("VALIDACION").Locked Then
    sdbddValidacion.Enabled = False
    Exit Sub
End If

sdbddValidacion.RemoveAll

sdbddValidacion.AddItem 0 & Chr(m_lSeparador) & m_asAtrib(13)
sdbddValidacion.MoveNext
sdbddValidacion.AddItem 1 & Chr(m_lSeparador) & m_asAtrib(14)
sdbddValidacion.MoveNext
sdbddValidacion.AddItem 2 & Chr(m_lSeparador) & m_asAtrib(15)
sdbddValidacion.MoveNext

End Sub

''' <summary>
''' Incializacion de las columnas del combo de la columna validacion de la grid del tab de atributos de especificacion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValidacion_InitColumnProps()
    sdbddValidacion.DataFieldList = "Column 0"
    sdbddValidacion.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Posicionamiento en el combo de la columna validacion de la grid del tab de atributos de especificacion
''' </summary>
''' <returns></returns>
''' <param name="Texto">Valor en el que posicionarse</param>
''' <remarks>Llamada desde: </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValidacion_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
Dim i As Long
Dim bm As Variant

    On Error Resume Next
   
    sdbddValidacion.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValidacion.Rows - 1
            bm = sdbddValidacion.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValidacion.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributosEsp.Columns("VALIDACION").Value = Mid(sdbddValidacion.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValidacion.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>
''' Valida la lista del combo
''' </summary>
''' <returns></returns>
''' <param name="Texto">Valor en el que posicionarse</param>
''' <param name="RtnPassed"></param>
''' <remarks>Llamada desde: </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValidacion_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
        ''' Comprobar la existencia en la lista
            Select Case UCase(sdbgAtributosEsp.Columns("VALIDACION").Text)
                Case UCase(m_asAtrib(13)): RtnPassed = True  'Apertura
                Case UCase(m_asAtrib(14)): RtnPassed = True  'Publicacion
                Case UCase(m_asAtrib(15)): RtnPassed = True 'Adjudicaci�n
                Case Else: sdbgAtributosEsp.Columns("VALIDACION").Text = ""
            End Select
    End If
    
        
End Sub


''' <summary>
'''  Evita el bug de la grid de Infragistics moviendonos a una fila adyacente y regresando luego a la actual en caso de  que no haya error.
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdModoEdicionAtrEsp_Click </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Public Function actualizarYSalirAtribEsp() As Boolean

    If sdbgAtributosEsp.DataChanged = True Then
        m_bValError = False
        m_bAnyaError = False
        m_bModError = False
        If sdbgAtributosEsp.Row = 0 Then
            sdbgAtributosEsp.MoveNext
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalirAtribEsp = True
                Exit Function
            Else
                sdbgAtributosEsp.MovePrevious
            End If
        Else
            sdbgAtributosEsp.MovePrevious
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalirAtribEsp = True
                Exit Function
            Else
                sdbgAtributosEsp.MoveNext
            End If
        End If
    Else
        actualizarYSalirAtribEsp = False
    End If
End Function

Public Sub MostrarOcultarFraSolCompra(ByVal bMostrar As Boolean)
    Me.fraSolicitudCompra(0).Visible = bMostrar
End Sub
