VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRMatPorCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub Listado(oReport As CRAXDRT.Report, ByVal bREqp As Boolean, ByVal sCodComp As String, ByVal sCodEqp As String, _
                    ByVal Ordcod As Boolean, ByVal porEqp As Boolean, ByVal Todos As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim i As Integer
    Dim OrderRpt(1 To 2, 1 To 1) As String
    Dim sRecordSelectionFormulaRpt As String
    Dim SubListado As CRAXDRT.Report
    Dim sOrden As String
    
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    sRecordSelectionFormulaRpt = " {PER.BAJALOG} = 0 "
    
    If Todos = True Then
        If bREqp Then
            sRecordSelectionFormulaRpt = sRecordSelectionFormulaRpt & " AND {COM.EQP} = '" & DblQuote(sCodEqp) & "'"
        End If
    Else
        If porEqp = True Then
            sRecordSelectionFormulaRpt = sRecordSelectionFormulaRpt & " AND {COM.EQP} = '" & DblQuote(sCodEqp) & "'"
        Else
            sRecordSelectionFormulaRpt = sRecordSelectionFormulaRpt & " AND {COM.EQP} = '" & DblQuote(sCodEqp) & "'" & " AND {COM.COD} = '" & DblQuote(sCodComp) & "'"
        End If
    End If
        
    
    
    ' Clasificación
    OrderRpt(2, 1) = "GRUPO1"
    If Ordcod = True Then
        sOrden = "COD"
        OrderRpt(1, 1) = "{COM.EQP}&{COM.COD}"
    Else
        sOrden = "DEN"
        OrderRpt(1, 1) = "{EQP.DEN}&{COM.APE}"
    End If
        
        
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
    
    For i = 1 To UBound(OrderRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, OrderRpt(2, i))).Text = OrderRpt(1, i)
    Next i
    
    oReport.RecordSelectionFormula = sRecordSelectionFormulaRpt
        
    Set SubListado = oReport.OpenSubreport("rptMAT")
    
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next

    'formula field para Clasificación grupos
    For i = 1 To 3
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "ORD_M" & i)).Text = "{MAT_RESTR;1." & sOrden & i & "}"
    Next i

End Sub

