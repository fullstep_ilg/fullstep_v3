VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmRecepGral 
   Caption         =   "DRecepciones de pedidos"
   ClientHeight    =   8490
   ClientLeft      =   855
   ClientTop       =   1590
   ClientWidth     =   11820
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRecepGral.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   8490
   ScaleWidth      =   11820
   Begin VB.PictureBox PicCritSel 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   3735
      Left            =   0
      ScaleHeight     =   3705
      ScaleWidth      =   11805
      TabIndex        =   5
      Top             =   0
      Width           =   11835
      Begin VB.CheckBox chkPedDirec 
         Caption         =   "Pedidos negociados"
         Height          =   225
         Left            =   5940
         TabIndex        =   53
         Top             =   240
         Width           =   1725
      End
      Begin VB.CheckBox chkPedAprov 
         Caption         =   "Pedidos de catalogo"
         Height          =   225
         Left            =   8115
         TabIndex        =   52
         Top             =   240
         Width           =   1965
      End
      Begin VB.CheckBox chkPedErp 
         Caption         =   "Pedidos ERP"
         Height          =   225
         Left            =   10140
         TabIndex        =   51
         Top             =   240
         Width           =   1200
      End
      Begin VB.TextBox txtPedidoExt 
         Height          =   285
         Left            =   1230
         MaxLength       =   100
         TabIndex        =   23
         Top             =   1545
         Width           =   1860
      End
      Begin VB.TextBox txtOrdEntrega 
         Height          =   285
         Left            =   3165
         TabIndex        =   22
         Top             =   795
         Width           =   765
      End
      Begin VB.TextBox txtPedido 
         Height          =   285
         Left            =   2310
         TabIndex        =   21
         Top             =   795
         Width           =   795
      End
      Begin VB.CommandButton cmdBuscarProve 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5535
         Picture         =   "frmRecepGral.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   1170
         Width           =   315
      End
      Begin VB.CommandButton cmdCalFecHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5235
         Picture         =   "frmRecepGral.frx":01D7
         Style           =   1  'Graphical
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   2190
         Width           =   315
      End
      Begin VB.CommandButton cmdCalFecDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2430
         Picture         =   "frmRecepGral.frx":0761
         Style           =   1  'Graphical
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   2190
         Width           =   315
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   4080
         TabIndex        =   17
         Top             =   2190
         Width           =   1110
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   1230
         TabIndex        =   16
         Top             =   2190
         Width           =   1110
      End
      Begin VB.CommandButton cmdActualizar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11415
         Picture         =   "frmRecepGral.frx":0CEB
         Style           =   1  'Graphical
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   90
         Width           =   315
      End
      Begin VB.CommandButton cmlistado 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11415
         Picture         =   "frmRecepGral.frx":0D76
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   495
         Width           =   315
      End
      Begin VB.TextBox txtalbaran 
         Height          =   285
         Left            =   7170
         MaxLength       =   100
         TabIndex        =   13
         Top             =   795
         Width           =   1950
      End
      Begin VB.CheckBox chkParcial 
         Caption         =   "Mostrar �rdenes parcialmente recibidas"
         Height          =   225
         Left            =   6450
         TabIndex        =   12
         Top             =   2085
         Width           =   3495
      End
      Begin VB.CheckBox chkTotal 
         Caption         =   "Mostrar �rdenes totalmente recibidas"
         Height          =   225
         Left            =   6450
         TabIndex        =   11
         Top             =   2325
         Width           =   3495
      End
      Begin VB.TextBox txtFacNum 
         Height          =   285
         Left            =   4080
         TabIndex        =   10
         Top             =   3300
         Width           =   1635
      End
      Begin VB.CommandButton cmdCalFacHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5205
         Picture         =   "frmRecepGral.frx":0DFB
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2850
         Width           =   315
      End
      Begin VB.TextBox txtFacHasta 
         Height          =   285
         Left            =   4080
         TabIndex        =   8
         Top             =   2850
         Width           =   1065
      End
      Begin VB.CommandButton cmdCalFacDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2430
         Picture         =   "frmRecepGral.frx":110D
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   2850
         Width           =   315
      End
      Begin VB.TextBox txtFacDesde 
         Height          =   285
         Left            =   1230
         TabIndex        =   6
         Top             =   2850
         Width           =   1110
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   1230
         TabIndex        =   24
         Top             =   795
         Width           =   1020
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmRecepGral.frx":141F
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HeadStyleSet=   "Normal"
         Columns(0).StyleSet=   "Normal"
         _ExtentX        =   1799
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
         Height          =   285
         Left            =   2325
         TabIndex        =   25
         Top             =   1170
         Width           =   3180
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmRecepGral.frx":143B
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4048
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HeadStyleSet=   "Normal"
         Columns(0).StyleSet=   "Normal"
         Columns(1).Width=   1931
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HeadStyleSet=   "Normal"
         Columns(1).StyleSet=   "Normal"
         _ExtentX        =   5609
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
         Height          =   285
         Left            =   1230
         TabIndex        =   26
         Top             =   1170
         Width           =   1020
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmRecepGral.frx":1457
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1931
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HeadStyleSet=   "Normal"
         Columns(0).StyleSet=   "Normal"
         Columns(1).Width=   4048
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HeadStyleSet=   "Normal"
         Columns(1).StyleSet=   "Normal"
         _ExtentX        =   1799
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEmpresa 
         Height          =   285
         Left            =   1230
         TabIndex        =   27
         Top             =   165
         Width           =   4500
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   -2147483640
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2487
         Columns(0).Caption=   "CIF"
         Columns(0).Name =   "CIF"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6747
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7937
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAprov 
         Height          =   285
         Left            =   7170
         TabIndex        =   28
         Top             =   1170
         Width           =   3855
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   -2147483640
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2593
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7382
         Columns(1).Caption=   "Denominacion"
         Columns(1).Name =   "Denominacion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Per"
         Columns(2).Name =   "Per"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6800
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcReceptor 
         Height          =   285
         Left            =   7170
         TabIndex        =   29
         Top             =   1545
         Width           =   3855
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6509
         Columns(1).Caption=   "Denominacion"
         Columns(1).Name =   "Denominacion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Per"
         Columns(2).Name =   "Per"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6800
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcFacAnyo 
         Height          =   285
         Left            =   1230
         TabIndex        =   30
         Top             =   3300
         Width           =   1020
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1799
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoPedido 
         Height          =   285
         Left            =   4080
         TabIndex        =   45
         Top             =   1545
         Width           =   1755
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   9
         Columns(0).Width=   1958
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   4604
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   1931
         Columns(2).Caption=   "Concepto"
         Columns(2).Name =   "INV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1773
         Columns(3).Caption=   "Almacen"
         Columns(3).Name =   "ALMAC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1693
         Columns(4).Caption=   "Recepci�n"
         Columns(4).Name =   "RECEP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "INV_"
         Columns(5).Name =   "INV_"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "ALMAC_"
         Columns(6).Name =   "ALMAC_"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "RECEP_"
         Columns(7).Name =   "RECEP_"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID"
         Columns(8).Name =   "ID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   3096
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483642
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEstadoFactura 
         Height          =   285
         Left            =   7170
         TabIndex        =   46
         Top             =   2850
         Width           =   3855
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6350
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         _ExtentX        =   6800
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483642
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblTipoPedido 
         Caption         =   "Tipo pedido:"
         ForeColor       =   &H00000000&
         Height          =   435
         Left            =   3180
         TabIndex        =   50
         Top             =   1530
         Width           =   960
      End
      Begin VB.Label lblFacHasta 
         Caption         =   "Hasta:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   3030
         TabIndex        =   49
         Top             =   2910
         Width           =   645
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Hasta:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   3030
         TabIndex        =   48
         Top             =   2220
         Width           =   645
      End
      Begin VB.Label lblEstFac 
         Caption         =   "Estado:"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   6120
         TabIndex        =   47
         Top             =   2865
         Width           =   960
      End
      Begin VB.Label lblSelec 
         Caption         =   "Seleccione los criterios de b�squeda"
         Height          =   255
         Left            =   6180
         TabIndex        =   44
         Top             =   210
         Visible         =   0   'False
         Width           =   4815
      End
      Begin VB.Label LblPedExt 
         Caption         =   "N�m.pedido externo:"
         ForeColor       =   &H00000000&
         Height          =   435
         Left            =   60
         TabIndex        =   43
         Top             =   1530
         Width           =   1200
      End
      Begin VB.Label lblNumOrden 
         Caption         =   "(a�o/cesta/pedido)"
         Height          =   225
         Left            =   4080
         TabIndex        =   42
         Top             =   855
         Width           =   1635
      End
      Begin VB.Label lblAnyo 
         Caption         =   "Pedido:"
         Height          =   225
         Left            =   60
         TabIndex        =   41
         Top             =   810
         Width           =   975
      End
      Begin VB.Label lblProve 
         Caption         =   "Proveedor:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   75
         TabIndex        =   40
         Top             =   1200
         Width           =   870
      End
      Begin VB.Label lblFecDesde 
         Caption         =   "Fecha entrega desde:"
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   45
         TabIndex        =   39
         Top             =   2160
         Width           =   1080
      End
      Begin VB.Label lblAlbaran 
         Caption         =   "Albar�n:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   6000
         TabIndex        =   38
         Top             =   825
         Width           =   1020
      End
      Begin VB.Label lblEmpresa 
         Caption         =   "Empresa:"
         Height          =   255
         Left            =   75
         TabIndex        =   37
         Top             =   195
         Width           =   945
      End
      Begin VB.Label lblAprov 
         Caption         =   "Aprovisionador:"
         Height          =   255
         Left            =   6000
         TabIndex        =   36
         Top             =   1185
         Width           =   1155
      End
      Begin VB.Label lblReceptor 
         Caption         =   "Receptor:"
         Height          =   255
         Left            =   6030
         TabIndex        =   35
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label lblDatosReceptor 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   7170
         TabIndex        =   34
         Top             =   1560
         Visible         =   0   'False
         Width           =   3855
      End
      Begin VB.Line Line2 
         X1              =   5880
         X2              =   5880
         Y1              =   0
         Y2              =   2670
      End
      Begin VB.Label lblFacNum 
         AutoSize        =   -1  'True
         Caption         =   "N�m factura:"
         Height          =   195
         Left            =   3030
         TabIndex        =   33
         Top             =   3330
         Width           =   945
      End
      Begin VB.Label lblFacAnyo 
         AutoSize        =   -1  'True
         Caption         =   "A�o factura:"
         Height          =   195
         Left            =   60
         TabIndex        =   32
         Top             =   3330
         Width           =   915
      End
      Begin VB.Line Line3 
         X1              =   -30
         X2              =   11350
         Y1              =   630
         Y2              =   630
      End
      Begin VB.Line Line4 
         X1              =   0
         X2              =   11350
         Y1              =   1980
         Y2              =   1980
      End
      Begin VB.Line Line5 
         X1              =   0
         X2              =   11350
         Y1              =   2670
         Y2              =   2670
      End
      Begin VB.Label lblFacDesde 
         Caption         =   "Fecha factura desde:"
         ForeColor       =   &H00000000&
         Height          =   375
         Left            =   60
         TabIndex        =   31
         Top             =   2790
         Width           =   1080
      End
      Begin VB.Line Line1 
         X1              =   11350
         X2              =   11350
         Y1              =   0
         Y2              =   3690
      End
   End
   Begin VB.PictureBox picNavigate 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   11850
      TabIndex        =   3
      Top             =   8145
      Width           =   11850
      Begin VB.CommandButton cmdRecepcion 
         Caption         =   "&Recepci�n"
         Enabled         =   0   'False
         Height          =   345
         Left            =   0
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   0
         Width           =   1125
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   0
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   0
         Width           =   1125
      End
      Begin VB.CommandButton CmdEdicion 
         Caption         =   "&Edici�n"
         Default         =   -1  'True
         Height          =   345
         Left            =   10635
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   0
         Width           =   1125
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgOrdenes 
      Height          =   4365
      Left            =   0
      TabIndex        =   0
      Top             =   3765
      Width           =   11805
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   15
      stylesets.count =   1
      stylesets(0).Name=   "Tahoma"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmRecepGral.frx":1473
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      MaxSelectedRows =   1
      HeadStyleSet    =   "Tahoma"
      StyleSet        =   "Tahoma"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterPos     =   1
      Columns.Count   =   15
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "EMPRESA"
      Columns(1).Name =   "EMPRESA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777152
      Columns(2).Width=   3200
      Columns(2).Caption=   "PED"
      Columns(2).Name =   "PED"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16777152
      Columns(3).Width=   3201
      Columns(3).Caption=   "Proveedor"
      Columns(3).Name =   "Proveedor"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777152
      Columns(4).Width=   3200
      Columns(4).Caption=   "Fecha"
      Columns(4).Name =   "Fecha"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16777152
      Columns(5).Width=   3200
      Columns(5).Caption=   "Estado"
      Columns(5).Name =   "Estado"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(5).HasBackColor=   -1  'True
      Columns(5).BackColor=   16777152
      Columns(6).Width=   3200
      Columns(6).Caption=   "FecAcep"
      Columns(6).Name =   "FecAcep"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).HasBackColor=   -1  'True
      Columns(6).BackColor=   16777152
      Columns(7).Width=   1482
      Columns(7).Name =   "CERRAR"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   4
      Columns(7).ButtonsAlways=   -1  'True
      Columns(8).Width=   2223
      Columns(8).Caption=   "Incorrecta"
      Columns(8).Name =   "Incorrecta"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(8).Style=   2
      Columns(9).Width=   3200
      Columns(9).Caption=   "TIPOPEDIDO"
      Columns(9).Name =   "TIPOPEDIDO"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).HasBackColor=   -1  'True
      Columns(9).BackColor=   16777152
      Columns(10).Width=   3200
      Columns(10).Caption=   "APROV"
      Columns(10).Name=   "APROV"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(10).Style=   1
      Columns(10).HasBackColor=   -1  'True
      Columns(10).BackColor=   16777152
      Columns(11).Width=   3200
      Columns(11).Caption=   "RECEPTOR"
      Columns(11).Name=   "RECEPTOR"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).Locked=   -1  'True
      Columns(11).Style=   1
      Columns(11).HasBackColor=   -1  'True
      Columns(11).BackColor=   16777152
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "COD_APROV"
      Columns(12).Name=   "COD_APROV"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "COD_RECEP"
      Columns(13).Name=   "COD_RECEP"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "IDTIPOPEDIDO"
      Columns(14).Name=   "IDTIPOPEDIDO"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      _ExtentX        =   20823
      _ExtentY        =   7699
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmRecepGral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables para func. combos
Private bRespetarCombo As Boolean
Private RespetarComboProve As Boolean
Private bCargarComboDesde As Boolean
Private bRespetarUpdate As Boolean

'Variables para permisos
Private bCons As Boolean
Private bMod As Boolean
Private bRMat As Boolean
Private bRUsuAprov As Boolean
Private m_bPedDirec As Boolean
Private m_bPedAprov As Boolean
Private m_bPedErp As Boolean
Private m_bREmpresa As Boolean

'Proveedor seleccionado
Public oProveSeleccionado As CProveedor
'variable para la coleccion de proveedores
Private oProves As CProveedores
'variable para la coleccion de ordenes de entrega
Private oOrdenesEntrega As COrdenesEntrega

'Variable de control de flujo de proceso
Private bModoEdicion As Boolean
Private bModError As Boolean
Private Accion As accionessummit
Private oLineaEnEdicion As COrdenEntrega
Private oIBaseDatos As IBaseDatos

' Variables para textos
Private sFecDesde As String
Private sFecHasta As String
Private sPedido As String
Private sOrden As String
Private sTextosEstados() As String
Private sProve As String
Private sIdiConsulta As String
Private sIdiEdicion As String
Private m_sEmpresa As String
Private m_sTituloRecep As String
Private m_arrConcep(2) As String
Private m_arrRecep(2) As String
Private m_arrAlmac(2) As String
Private m_sCerrar As String
Private m_sReabrir As String

Private m_bRespetarComboEmpresa As Boolean
Private m_bCargarComboDesde As Boolean
Private m_bRespetarComboUsu As Boolean
Private m_bRespetarComboRecep As Boolean

Private m_oEmpresas As CEmpresas
Private m_oTiposPedido As CTiposPedido
Private m_oTipoPedido As CTipoPedido

Private m_bProvMat As Boolean
Private m_bPersUO As Boolean

Private m_arrTiposPedido(3) As Boolean 'Array que contiene los tipos de pedidos marcados en el filtro. 0-Directos, 1-Aprovisionamiento, 2-Erp ,3-A false siempre, se usa en frmSeguimiento para los pedidos de Erp, aqui lo ponemos a false pq nos hace falta pasarlo a la funcion BuscarTodasLasOrdenes a la que tambi�n se le llama desde frmSeguimiento.

'Para cargar datos desde el Historico
Public iAnyo As Integer 'PEDIDO.ANYO
Public lNumPedido As Long 'PEDIDO.NUM
Public lNumOrden As Long 'ORDEN_ENTREGA.NUM

Private bVienesDeSeguimiento As Boolean
Public lIdOrden As Long 'ORDEN_ENTREGA.ID

'Contiene el c�digo de la persona conectada, si administrador = ""
Private m_sPersona As String


Function BuscarProveedor(CodProve As String) As CProveedor
Dim oProves As CProveedores
Dim oProve As CProveedor

    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado sProve
        Set oProves = Nothing
        Exit Function
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    Set BuscarProveedor = oProve
    Set oProve = Nothing
    Set oProves = Nothing
End Function

''' <summary>
''' Redimensionar los controles de pantalla
''' </summary>
''' <remarks>Llamada desde: Form_Resize ; Tiempo m�ximo: 0</remarks>
Private Sub Arrange()

On Error Resume Next

    PicCritSel.Width = Me.Width - 120
    sdbgOrdenes.Width = Me.Width - 120
   
    sdbgOrdenes.Top = PicCritSel.Height + 5
    sdbgOrdenes.Height = Me.Height - PicCritSel.Height - 935
    
    If sdbgOrdenes.Rows > 0 Then
        Dim vbm As Variant
        vbm = sdbgOrdenes.Bookmark
        sdbgOrdenes.Bookmark = 0
    End If
    
    picNavigate.Width = Me.Width - 120
    picNavigate.Top = PicCritSel.Height + sdbgOrdenes.Height + 75
    
    CmdEdicion.Left = picNavigate.Width - CmdEdicion.Width
    sdbgOrdenes.Columns("EMPRESA").Width = sdbgOrdenes.Width * 8 / 100
    sdbgOrdenes.Columns("PED").Width = sdbgOrdenes.Width * 8 / 100
    sdbgOrdenes.Columns("Proveedor").Width = sdbgOrdenes.Width * 13 / 100
    sdbgOrdenes.Columns("Fecha").Width = sdbgOrdenes.Width * 6 / 100
    sdbgOrdenes.Columns("Estado").Width = sdbgOrdenes.Width * 15 / 100
    sdbgOrdenes.Columns("FecAcep").Width = sdbgOrdenes.Width * 6 / 100
    sdbgOrdenes.Columns("CERRAR").Width = sdbgOrdenes.Width * 4 / 100
    sdbgOrdenes.Columns("Incorrecta").Width = sdbgOrdenes.Width * 6 / 100
    sdbgOrdenes.Columns("TIPOPEDIDO").Width = sdbgOrdenes.Width * 15 / 100
    sdbgOrdenes.Columns("APROV").Width = sdbgOrdenes.Width * 12 / 100
    sdbgOrdenes.Columns("RECEPTOR").Width = sdbgOrdenes.Width * 12 / 100
    
    
    If sdbgOrdenes.Rows > 0 Then
        'Recup�ro el bookmark y si no se ve la fila la pongo la priemra visible
        sdbgOrdenes.Bookmark = vbm
        If sdbgOrdenes.AddItemRowIndex(sdbgOrdenes.Bookmark) < sdbgOrdenes.AddItemRowIndex(sdbgOrdenes.FirstRow) Or sdbgOrdenes.AddItemRowIndex(sdbgOrdenes.Bookmark) > sdbgOrdenes.AddItemRowIndex(sdbgOrdenes.FirstRow) + sdbgOrdenes.VisibleRows - 1 Then
            sdbgOrdenes.FirstRow = sdbgOrdenes.Bookmark
        End If
    End If
    
End Sub


Public Sub CargarProveedorConBusqueda()

Dim oProves As CProveedores

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    RespetarComboProve = True
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveDen.Text = oProves.Item(1).Den
    RespetarComboProve = False

End Sub




Private Sub ProveedorSeleccionado()
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
   
    
End Sub


''' <summary>
''' Carga los a�os en los combos
''' </summary>
''' <param name=""></param>
''' <returns></returns>
''' <remarks>Llamada desde:Form_load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub CargarAnyos()
    Dim iAnyoActual As Integer
    Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        sdbcFacAnyo.AddItem iInd
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub

Private Sub chkParcial_Click()
If chkParcial.Value = False And chkTotal.Value = False Then
    txtalbaran.Enabled = False
Else
    txtalbaran.Enabled = True
    
    End If
If chkParcial.Value = False Then
    chkTotal.Value = False
End If
End Sub

Private Sub chkPedAprov_Click()
    If chkPedAprov.Value = vbChecked Then
        m_arrTiposPedido(1) = True
    Else
        m_arrTiposPedido(1) = False
    End If

End Sub

Private Sub chkPedDirec_Click()
    If chkPedDirec.Value = vbChecked Then
        m_arrTiposPedido(0) = True
    Else
        m_arrTiposPedido(0) = False
    End If
End Sub

Private Sub chkPedErp_Click()
    If chkPedErp.Value = vbChecked Then
        m_arrTiposPedido(2) = True
    Else
        m_arrTiposPedido(2) = False
    End If
End Sub

Private Sub chkTotal_Click()
    If chkParcial.Value = vbUnchecked And chkTotal.Value = vbUnchecked Then
        txtalbaran.Enabled = False
    Else
        txtalbaran.Enabled = True
    End If
End Sub

''' <summary>
''' Recargar el grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,3</remarks>
Public Sub cmdActualizar_Click()
    Dim Estado As TipoEstadoOrdenEntrega
    Dim EstadoHasta As TipoEstadoOrdenEntrega
    Dim iPedido As Double
    Dim iOrdenEntrega As Double
    Dim bConCriterios As Boolean
    Dim sPer As String
    Dim lEmpresa As Long
    Dim sReceptor As String
    Dim arFiltros(1 To 5) As Variant
    Dim arTipoPed(0 To 1) As Variant
    
    LockWindowUpdate Me.hWnd

    sdbgOrdenes.RemoveAll
    
    If Not IsDate(txtFecDesde) And txtFecDesde <> "" Then
        oMensajes.NoValido sFecDesde
        LockWindowUpdate 0&
        Exit Sub
    End If
    
    If Not IsDate(txtFecHasta) And txtFecHasta <> "" Then
        oMensajes.NoValido sFecHasta
        LockWindowUpdate 0&
        Exit Sub
    End If

    If Trim(txtPedido) <> "" Then
        If Not IsNumeric(txtPedido) Then
            oMensajes.NoValido sPedido
            LockWindowUpdate 0&
            Exit Sub
        End If
    End If

    If Trim(txtOrdEntrega) <> "" Then
        If Not IsNumeric(txtOrdEntrega) Then
            oMensajes.NoValido sOrden
            LockWindowUpdate 0&
            Exit Sub
        End If
    End If

    If m_bREmpresa = True And Trim(sdbcEmpresa.Text) = "" Then
        m_oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        If m_oEmpresas.Count = 0 Then
            oMensajes.UnidadUOSinEmpresa
        Else
            oMensajes.SeleccionarEmpresa
        End If
        If Me.Visible Then sdbcEmpresa.SetFocus
        LockWindowUpdate 0&
        Exit Sub
    End If
    
    If txtPedido.Text = "" Then
        iPedido = 0
    Else
        iPedido = CDbl(txtPedido)
    End If
    
    If txtOrdEntrega.Text = "" Then
        iOrdenEntrega = 0
    Else
        iOrdenEntrega = CDbl(txtOrdEntrega)
    End If
    
    If txtFecDesde <> "" Or txtFecHasta <> "" Or Trim(txtPedido) <> "" Or Trim(txtOrdEntrega) <> "" Or Trim(txtPedidoExt) <> "" Or txtalbaran <> "" Or Trim(sdbcProveCod) <> "" Then
        bConCriterios = True
    Else
        bConCriterios = False
    End If
    
    Estado = TipoEstadoOrdenEntrega.EmitidoAlProveedor
    EstadoHasta = TipoEstadoOrdenEntrega.EnCamino
    If chkTotal.Value = vbChecked Or chkParcial.Value = vbChecked Then
        If chkParcial.Value = vbUnchecked And chkTotal.Value = vbChecked Then
            Estado = TipoEstadoOrdenEntrega.RecibidoYCerrado
            EstadoHasta = TipoEstadoOrdenEntrega.RecibidoYCerrado
        ElseIf chkParcial.Value = vbChecked And chkTotal.Value = vbUnchecked Then
            Estado = TipoEstadoOrdenEntrega.EnRecepcion
            EstadoHasta = TipoEstadoOrdenEntrega.EnRecepcion
        ElseIf chkTotal.Value = vbChecked And chkParcial.Value = vbChecked Then
            Estado = TipoEstadoOrdenEntrega.EnRecepcion
            EstadoHasta = TipoEstadoOrdenEntrega.RecibidoYCerrado
        End If
    End If
        
    If sdbcEmpresa.Text <> "" Then
        lEmpresa = sdbcEmpresa.Columns("ID").Value
    End If
    
    If bRUsuAprov = True Then
        sReceptor = basOptimizacion.gCodPersonaUsuario
    ElseIf sdbcReceptor.Text <> "" Then
        sReceptor = sdbcReceptor.Columns("COD").Value
    End If
    
    If sdbcAprov.Text <> "" Then
        sPer = sdbcAprov.Columns("COD").Value
    End If
    'EPB
    If Not IsDate(txtFacDesde.Text) And txtFacDesde.Text <> "" Then
        oMensajes.NoValido sFecDesde
        LockWindowUpdate 0&
        Exit Sub
    End If
    
    If Not IsDate(txtFacHasta.Text) And txtFacHasta.Text <> "" Then
        oMensajes.NoValido sFecHasta
        LockWindowUpdate 0&
        Exit Sub
    End If
    
    If m_oTipoPedido Is Nothing Then
        arTipoPed(0) = 0
    Else
        arTipoPed(0) = m_oTipoPedido.Id
    End If

    If txtFacDesde.Text <> "" Then
        arFiltros(1) = txtFacDesde.Text
    End If
    If txtFacHasta.Text <> "" Then
        arFiltros(2) = txtFacHasta.Text
    End If
    If sdbcFacAnyo.Text <> "" Then
        arFiltros(3) = sdbcFacAnyo.Text
    End If
    If txtFacNum.Text <> "" Then
        arFiltros(4) = txtFacNum.Text
    End If
    If sdbcEstadoFactura.Text <> "" Then
        arFiltros(5) = sdbcEstadoFactura.Columns("ID").Value
    End If
    
    Screen.MousePointer = vbHourglass
    
    'oOrdenesEntrega.BuscarTodasLasOrdenes gParametrosInstalacion.giCargaMaximaCombos, val(sdbcAnyo), iPedido, iOrdenEntrega, txtPedidoExt, , , Trim(txtFecDesde), Trim(txtFecHasta), TipoEstadoOrdenEntrega.EmitidoAlProveedor, bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , Trim(sdbcProveCod), , , , , , , , , , sPer, txtalbaran, EstadoHasta, txtFecDesde, txtFecHasta, , , , , , , , , , , , , , , , lEmpresa, sReceptor, , arTipoPed, arFiltros, True, , , m_arrTiposPedido
    oOrdenesEntrega.BuscarTodasLasOrdenes gParametrosInstalacion.giCargaMaximaCombos, val(sdbcAnyo), iPedido, iOrdenEntrega, txtPedidoExt, , , Trim(txtFecDesde), Trim(txtFecHasta), Estado, bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , Trim(sdbcProveCod), , , , , , , , , , sPer, txtalbaran, EstadoHasta, txtFecDesde, txtFecHasta, , , , , , , , , , , , , , , , lEmpresa, sReceptor, , arTipoPed, arFiltros, True, , , m_arrTiposPedido

    If oOrdenesEntrega.Count = 0 Then
        sdbgOrdenes.RemoveAll
        Screen.MousePointer = vbNormal
        cmdRecepcion.Enabled = False
        If (chkParcial.Value = vbUnchecked And chkTotal.Value = vbUnchecked) And bConCriterios Then
            If oMensajes.RepetirBusquedaRecepcion Then
                chkTotal.Value = vbChecked
                chkParcial.Value = vbChecked
                EstadoHasta = TipoEstadoOrdenEntrega.RecibidoYCerrado
                oOrdenesEntrega.BuscarTodasLasOrdenes gParametrosInstalacion.giCargaMaximaCombos, val(sdbcAnyo), iPedido, iOrdenEntrega, txtPedidoExt, , , Trim(txtFecDesde), Trim(txtFecHasta), TipoEstadoOrdenEntrega.EmitidoAlProveedor, bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , Trim(sdbcProveCod), , , , , , , , , , sPer, txtalbaran, EstadoHasta, txtFecDesde, txtFecHasta, , , , , , , , , , , , , , , , lEmpresa, sReceptor, , arTipoPed, arFiltros, True, , , m_arrTiposPedido
                If oOrdenesEntrega.Count = 0 Then
                    LockWindowUpdate 0&
                    Exit Sub
                End If
            End If
        Else
            LockWindowUpdate 0&
            Exit Sub
        End If
    End If
    
    CmdEdicion.Enabled = True
    cmdRecepcion.Enabled = True
    
    CargarOrdenes oOrdenesEntrega, sReceptor
    
    LockWindowUpdate 0&
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Carga las �rdenes en el grid</summary>
''' <param name="oOrdenesEntrega">Ordenes de entrega</param>
''' <remarks>Llamada desde: cmdActualizar_Click, CargaDesdeseguimiento</remarks>
''' <revision>LTG 17/07/2012</revision>

Private Sub CargarOrdenes(ByVal oOrdenesEntrega As COrdenesEntrega, Optional ByVal sReceptor As String)
    Dim oOrdenEntrega As COrdenEntrega
    Dim sDenEstado As String
    Dim sPersona As String
    Dim sRecep As String
    Dim b As Boolean
    Dim sLinea As String
    Dim sPedido As String
    Dim sCerrar As String
    
    For Each oOrdenEntrega In oOrdenesEntrega
        sDenEstado = sTextosEstados(oOrdenEntrega.Estado)
        If oOrdenEntrega.Incorrecta Then
            b = 1
        Else
            b = 0
        End If
        
        sPersona = ""
        If Not oOrdenEntrega.Persona Is Nothing Then
            sPersona = NullToStr(oOrdenEntrega.Persona.nombre) & " " & NullToStr(oOrdenEntrega.Persona.Apellidos)
        End If
        
        sRecep = ""
        If Not oOrdenEntrega.Receptor Is Nothing Then
            sReceptor = NullToStr(oOrdenEntrega.Receptor.nombre) & " " & NullToStr(oOrdenEntrega.Receptor.Apellidos)
        End If
            
         sPedido = oOrdenEntrega.Anyo & "/" & oOrdenEntrega.NumPedido & "/" & oOrdenEntrega.Numero
            
        If oOrdenEntrega.Estado < TipoEstadoOrdenEntrega.RecibidoYCerrado Then
            sCerrar = m_sCerrar
        Else
            sCerrar = m_sReabrir
        End If
        
        sLinea = oOrdenEntrega.Id & Chr(m_lSeparador) & oOrdenEntrega.EmpresaDen & Chr(m_lSeparador) & sPedido & Chr(m_lSeparador) & oOrdenEntrega.ProveCod
        sLinea = sLinea & Chr(m_lSeparador) & oOrdenEntrega.FechaEmision & Chr(m_lSeparador) & sDenEstado & Chr(m_lSeparador) & oOrdenEntrega.FechaEstado
        sLinea = sLinea & Chr(m_lSeparador) & sCerrar & Chr(m_lSeparador) & b & Chr(m_lSeparador) & oOrdenEntrega.TipoDePedido.Den & Chr(m_lSeparador) & sPersona
        sLinea = sLinea & Chr(m_lSeparador) & sReceptor & Chr(m_lSeparador) & oOrdenEntrega.Persona.Cod & Chr(m_lSeparador) & oOrdenEntrega.Receptor.Cod
        sLinea = sLinea & Chr(m_lSeparador) & oOrdenEntrega.TipoDePedido.Id
                        
        sdbgOrdenes.AddItem sLinea
    Next
    
    Set oOrdenEntrega = Nothing
End Sub

Private Sub cmdBuscarProve_Click()
    frmPROVEBuscar.sOrigen = "frmRecepGral"
    frmPROVEBuscar.bREqp = False
    If bRMat Or m_bProvMat Then
        frmPROVEBuscar.bRMat = True
    End If
    frmPROVEBuscar.CodGMN1 = ""
    frmPROVEBuscar.Show 1
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub


Private Sub cmdDeshacer_Click()

    sdbgOrdenes.CancelUpdate
    sdbgOrdenes.DataChanged = False
    
    If Not oLineaEnEdicion Is Nothing Then
        oIBaseDatos.CancelarEdicion
        Set oIBaseDatos = Nothing
        Set oLineaEnEdicion = Nothing
    End If
    
    cmdDeshacer.Enabled = False
    
    Accion = ACCRecepcionPedConsulta
    
    If Me.Visible Then sdbgOrdenes.SetFocus
            

End Sub

Private Sub cmdEdicion_Click()
    
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
       
    If bModoEdicion Then
        cmdRecepcion.Visible = True
        bModError = False
        sdbgOrdenes.Update
        
        If Not bModError Then
            CmdEdicion.caption = sIdiEdicion
            cmdDeshacer.Enabled = False
            
            sdbgOrdenes.Update
            sdbgOrdenes.Columns("Incorrecta").Locked = True
            bModoEdicion = False
            PicCritSel.Enabled = True
            Accion = ACCRecepcionPedConsulta
            cmdActualizar_Click
        End If
        
    Else
        CmdEdicion.caption = sIdiConsulta
        cmdDeshacer.Visible = True
        cmdRecepcion.Visible = False
        
        PicCritSel.Enabled = False
        sdbgOrdenes.Columns("Incorrecta").Locked = False
        
        bModoEdicion = True
        
        Accion = ACCRecepcionPedModIncorr
        
    End If
    

End Sub

''' <summary>
''' Carga la pantalla para recepcionar
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdRecepcion_Click()
Dim oRecepciones As cRecepciones
Dim oOrdenSeleccionada As COrdenEntrega
Dim oProve As CProveedor
   
    If sdbgOrdenes.Columns("ID").Value = "" Then Exit Sub
    
        
    ' habr� que limpiar campos de la ventana o hacerlo en el form load

    ' el metodo DevolverRecepcionesDeOrden tiene que tener un order by , con tipo fecha en descendente
    Set oOrdenSeleccionada = Nothing
    Set oOrdenSeleccionada = oOrdenesEntrega.Item(sdbgOrdenes.Columns("ID").Value)
    
    If oOrdenSeleccionada Is Nothing Then Exit Sub
    
    Set oProve = BuscarProveedor(oOrdenSeleccionada.ProveCod)
    oOrdenSeleccionada.ProveDen = oProve.Den
    
    Set oRecepciones = oOrdenSeleccionada.DevolverTodasLasRecepciones(, True)
     
    If oRecepciones Is Nothing Then
        Set oProve = Nothing
        Exit Sub
    Else
        If frmRecepcion.Accion <> 0 Then
            Set frmRecepcion.oOrdenEntrega = oOrdenSeleccionada
            frmRecepcion.Show
        Else
            If oRecepciones.Count > 0 Then
                frmRecepcion.Accion = ACCRecepcionPedConsulta
                frmRecepcion.bRMat = bRMat
                If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Directo) And oOrdenSeleccionada.Tipo = Directo Then
                ' Restricciones si sentido integraci�n es solo de entrada,tambien para el Administrador
                    frmRecepcion.bMod = False
                Else
                    frmRecepcion.bMod = bMod
                End If
                Set frmRecepcion.oOrdenEntrega = oOrdenSeleccionada
                frmRecepcion.caption = m_sTituloRecep & " " & sdbgOrdenes.Columns("PED").Value
                frmRecepcion.sdbcFecRec.Columns("ID").Value = oRecepciones.Item(1).Id
                frmRecepcion.sdbcFecRec = oRecepciones.Item(1).FechaRec
                frmRecepcion.sdbcFecRec_Validate False
                frmRecepcion.bCargarComboDesde = False
                frmRecepcion.bActualizaSeguimiento = bVienesDeSeguimiento
                frmRecepcion.SetFocus
                Set oProve = Nothing
                Set oRecepciones = Nothing
                Set oOrdenSeleccionada = Nothing

            Else
                If (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Directo) And oOrdenSeleccionada.Tipo = Directo) Or (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Aprov) And oOrdenSeleccionada.Tipo = Aprovisionamiento) Then
                    ' Restricciones si sentido integraci�n es solo de entrada,tambien para el Administrador
                    Exit Sub
                End If
                
                'Antes de dar de alta una recepci�n se comprobar� que el pedido est� integrado.
                If Not ComprobarPedidoIntegrado(oOrdenSeleccionada) Then
                    oMensajes.MensajeOKOnly 1115
                    Exit Sub
                End If
                                                
                If oOrdenSeleccionada.AlgunoNoRecepAutom = False Then
                    oMensajes.MensajeOKOnly 1275
                    Exit Sub
                End If
                
                'Permisos de solo consulta, se puede meter una recepci�n cuando el pedido no tiene ninguna, no deber�a dejar
                If Not bMod Then
                    oMensajes.SinRecepciones
                    Exit Sub
                End If
                If oOrdenSeleccionada.Estado >= RecibidoYCerrado Then
                    oMensajes.SinRecepciones
                    Exit Sub
                End If
                
                frmRecepcion.Accion = ACCRecepcionPedAnya
                frmRecepcion.bRMat = bRMat
                frmRecepcion.bMod = bMod
                Set frmRecepcion.oOrdenEntrega = oOrdenSeleccionada
                frmRecepcion.caption = m_sTituloRecep & " " & sdbgOrdenes.Columns("PED").Value
                frmRecepcion.bActualizaSeguimiento = bVienesDeSeguimiento
                frmRecepcion.Show
                If frmRecepcion.cmdAceptar.Visible = False Then
                    Unload frmRecepcion 'Si por la vigencia no se puede dar alta
                Else
                    frmRecepcion.SetFocus
                End If
                Set oProve = Nothing
                Set oRecepciones = Nothing
                Set oOrdenSeleccionada = Nothing

            End If
        End If
    End If



End Sub

Private Sub cmlistado_Click()
    frmLstRecepciones.WindowState = vbNormal
    frmLstRecepciones.sdbcAnyo.Text = Me.sdbcAnyo.Text
    frmLstRecepciones.sdbcFacAnyo.Text = sdbcFacAnyo.Text
    frmLstRecepciones.txtalbaran = txtalbaran
    frmLstRecepciones.txtFecDesde = txtFecDesde
    frmLstRecepciones.txtFecHasta = txtFecHasta
    frmLstRecepciones.txtFacDesde = txtFacDesde
    frmLstRecepciones.txtFacHasta = txtFacHasta
    frmLstRecepciones.txtOrdEntrega = txtOrdEntrega
    frmLstRecepciones.txtPedido = txtPedido
    frmLstRecepciones.txtPedidoExt = txtPedidoExt
    frmLstRecepciones.txtFacNum = txtFacNum
    frmLstRecepciones.chkParcial.Value = chkParcial.Value
    frmLstRecepciones.chkPedAprov.Value = chkPedAprov.Value
    frmLstRecepciones.chkPedDirec.Value = chkPedDirec.Value
    frmLstRecepciones.chkPedErp.Value = chkPedErp.Value
    frmLstRecepciones.chkTotal.Value = chkTotal.Value
    frmLstRecepciones.ChkSinRecep.Value = vbChecked
    frmLstRecepciones.chkItems.Value = vbChecked
    If sdbcProveCod.Text <> "" Then
        frmLstRecepciones.sdbcProveCod.Text = sdbcProveCod.Text
        frmLstRecepciones.sdbcProveCod_Validate False
    End If
    
    If sdbcEmpresa.Text <> "" Then
        frmLstRecepciones.sdbcEmpresa.Text = sdbcEmpresa.Text
        frmLstRecepciones.sdbcEmpresa_Validate False
    End If
    
    If sdbcAprov.Text <> "" Then
        frmLstRecepciones.sdbcAprov.Text = sdbcAprov.Text
        frmLstRecepciones.sdbcAprov.Columns("COD").Value = sdbcAprov.Columns("COD").Value
        frmLstRecepciones.sdbcAprov_Validate False
    End If
    
    If sdbcReceptor.Text <> "" Then
        frmLstRecepciones.sdbcReceptor.Text = sdbcReceptor.Text
        frmLstRecepciones.sdbcReceptor.Columns("COD").Value = sdbcReceptor.Columns("COD").Value
        frmLstRecepciones.sdbcReceptor_Validate False
    End If
    
    If sdbcTipoPedido.Text <> "" Then
        frmLstRecepciones.sdbcTipoPedido.Text = sdbcTipoPedido.Text
        frmLstRecepciones.sdbcTipoPedido_Validate False
        frmLstRecepciones.sdbcTipoPedido.Columns("ID").Value = sdbcTipoPedido.Columns("ID").Value
        Set frmLstRecepciones.m_oTipoPedido = m_oTiposPedido.Item(sdbcTipoPedido.Columns("ID").Value)
    End If

    If sdbcEstadoFactura.Text <> "" Then
        frmLstRecepciones.sdbcEstadoFactura.Text = sdbcEstadoFactura.Text
        frmLstRecepciones.sdbcEstadoFactura_Validate False
        frmLstRecepciones.sdbcEstadoFactura.Columns("ID").Value = sdbcEstadoFactura.Columns("ID").Value
    End If
    
    frmLstRecepciones.Show vbModal
End Sub

''' <summary>
''' Descargar la pantalla de recepciones y en caso de que se hayan hecho cambios en las recepciones reflejarlas en esta pantalla
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Form_Activate()
    If (frmRecepcion.Accion = ACCRecepcionPedAnya) Or (frmRecepcion.Accion = ACCRecepcionPedConsulta) Then
        cmdActualizar_Click
        frmRecepcion.Accion = 0
    End If
       
    Unload frmRecepcion
End Sub

''' <summary>
''' Carga la pantalla. Si se viene de frmSeguimiento, carga recepciones de una orden concreta
''' </summary>
''' <remarks>Llamada desde: frmSeguimiento  MDI ; Tiempo m�ximo: 0,1</remarks>
Private Sub Form_Load()
Dim i As Integer

    CargarRecursos
    
    PonerFieldSeparator Me
    
    Accion = ACCRecepcionPedConsulta
    
    ConfigurarSeguridad
        
    Me.Width = 11940
    Me.Top = 1900
    Me.Left = 2300
    If (bMod Or bCons) Then
        Me.Height = 8895
    Else
        Me.Height = 8565
    End If
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarAnyos
            
    bModoEdicion = False
                 
    Set oProves = oFSGSRaiz.generar_CProveedores
    Set oOrdenesEntrega = oFSGSRaiz.Generar_COrdenesEntrega
    Set m_oEmpresas = oFSGSRaiz.Generar_CEmpresas
    
    
    'Carga la empresa por defecto que tenga asignada el usuario:
    If m_bREmpresa = True Then
        sdbcEmpresa.AllowInput = False
        m_oEmpresas.CargarTodasLasEmpresasDesde , , , , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        If m_oEmpresas.Count > 0 Then
            sdbcEmpresa.Text = m_oEmpresas.Item(1).Den
            sdbcEmpresa_Validate False
        End If
    End If

    If bRUsuAprov Then
        sdbcReceptor.Visible = False
        lblDatosReceptor.Visible = True
                
        lblDatosReceptor.caption = basOptimizacion.gCodPersonaUsuario & " - " & oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos
    End If
    
    'Cargo los tipos de pedido del combo
    Set m_oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    m_oTiposPedido.CargarTodosLosTiposPedidos gParametrosInstalacion.gIdioma
    
    For i = 0 To 3
        m_arrTiposPedido(i) = False
    Next
    
    lblSelec.Visible = False
    
    If FSEPConf Then
       chkPedDirec.Value = vbUnchecked
       chkPedAprov.Value = vbChecked
       chkPedErp.Value = vbUnchecked
       chkPedDirec.Visible = False
       chkPedAprov.Visible = False
       chkPedErp.Visible = False
       lblSelec.Visible = True
    Else
        'Si solo hay un tipo de pedido configurado o el usuario s�lo tiene permiso para uno de los tipos no se ver�n los checks.
        If (gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP) Or (m_bPedDirec And Not m_bPedAprov And Not m_bPedErp) Then
            chkPedAprov.Value = vbUnchecked
            chkPedDirec.Value = vbChecked
            chkPedErp.Value = vbUnchecked
            chkPedDirec.Visible = False
            chkPedAprov.Visible = False
            chkPedErp.Visible = False
            lblSelec.Visible = True
        'Si solo hay un tipo de pedido configurado o el usuario s�lo tiene permiso para uno de los tipos no se ver�n los checks.
        ElseIf (gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosERP) Or (m_bPedAprov And Not m_bPedDirec And Not m_bPedErp) Then
            chkPedAprov.Value = vbChecked
            chkPedDirec.Value = vbUnchecked
            chkPedErp.Value = vbUnchecked
            chkPedDirec.Visible = False
            chkPedAprov.Visible = False
            chkPedErp.Visible = False
            lblSelec.Visible = True
        'Si solo hay un tipo de pedido configurado o el usuario s�lo tiene permiso para uno de los tipos no se ver�n los checks.
        ElseIf (gParametrosGenerales.gbPedidosERP And Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov) Or (m_bPedErp And Not m_bPedDirec And Not m_bPedAprov) Then
            chkPedAprov.Value = vbUnchecked
            chkPedErp.Value = vbChecked
            chkPedDirec.Value = vbUnchecked
            chkPedDirec.Visible = False
            chkPedAprov.Visible = False
            chkPedErp.Visible = False
            lblSelec.Visible = True
        'Si hay alg�n tipo de pedido no configurado o el usuario no tiene permiso para �l, no se ver�.
        ElseIf (gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP) Or (m_bPedDirec And m_bPedAprov And Not m_bPedErp) Then
            chkPedDirec.Visible = True
            chkPedAprov.Visible = True
            chkPedErp.Visible = False
            chkPedDirec.Left = chkPedDirec.Left + 250
            chkPedAprov.Left = chkPedAprov.Left + 450
        'Si hay alg�n tipo de pedido no configurado o el usuario no tiene permiso para �l, no se ver�.
        ElseIf (gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP) Or (m_bPedDirec And Not m_bPedAprov And m_bPedErp) Then
            chkPedDirec.Visible = True
            chkPedAprov.Visible = False
            chkPedErp.Visible = True
            chkPedDirec.Left = chkPedDirec.Left + 800
            chkPedErp.Left = chkPedAprov.Left + 1450
        'Si hay alg�n tipo de pedido no configurado o el usuario no tiene permiso para �l, no se ver�.
        ElseIf (Not gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP) Or (Not m_bPedDirec And m_bPedAprov And m_bPedErp) Then
            chkPedDirec.Visible = False
            chkPedAprov.Visible = True
            chkPedErp.Visible = True
            chkPedAprov.Left = chkPedDirec.Left + 500
            chkPedErp.Left = chkPedAprov.Left + 2700
        End If
    End If
    
    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
        m_sPersona = oUsuarioSummit.Persona.Cod
    Else
        m_sPersona = ""
    End If
    
    bVienesDeSeguimiento = False
    CargaDesdeSeguimiento
End Sub

''' <summary>
''' Configura la seguridad del formulario
''' </summary>
''' <remarks>Llamada desde=form_load; Tiempo m�ximo=0seg.</remarks>
''' <revision>LTG 04/04/2012</revision>

Private Sub ConfigurarSeguridad()
    
    m_bProvMat = False
    m_bPersUO = False

    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
    
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECConsultar)) Is Nothing) Then
            bCons = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECModificar)) Is Nothing) Then
            bMod = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECRestMat)) Is Nothing) And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
            bRMat = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECRestUsuAprov)) Is Nothing) Then
            bRUsuAprov = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECSoloAprov)) Is Nothing Then
            m_bPedAprov = True
        End If

        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECSoloDirectos)) Is Nothing Then
            m_bPedDirec = True
        End If
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECErp)) Is Nothing Then
            m_bPedErp = True
        End If
        
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECRestEmpresaUsu)) Is Nothing Then
            m_bREmpresa = True
        End If
        
        If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECRestProvMatComp)) Is Nothing) Then
            m_bProvMat = True
        End If
        
        If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestUsuUON)) Is Nothing) Then
            m_bPersUO = True
        End If
        
    Else
        bMod = True
        bCons = True
    End If

    If bMod Or bCons Then
        CmdEdicion.Visible = bMod
    Else
        picNavigate.Visible = False
    End If
    
    If Not bMod Then
        Me.sdbgOrdenes.Columns("CERRAR").Visible = False
    End If

    CmdEdicion.Enabled = False
    cmdRecepcion.Enabled = False

    If gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso Then
        OcultarFacturas
    End If
End Sub

Private Sub OcultarFacturas()

lblFacDesde.Visible = False
txtFacDesde.Visible = False
cmdCalFacDesde.Visible = False
lblFacHasta.Visible = False
txtFacHasta.Visible = False
cmdCalFacHasta.Visible = False
lblEstFac.Visible = False
sdbcEstadoFactura.Visible = False
lblFacAnyo.Visible = False
sdbcFacAnyo.Visible = False
lblFacNum.Visible = False
txtFacNum.Visible = False
PicCritSel.Height = PicCritSel.Height - 1020

End Sub

''' <summary>
''' Carga los idiomas del formulario.
''' </summary>
''' <remarks>Llamada desde=Form_load; Tiempo m�ximo=0,2</remarks>
Private Sub CargarRecursos()
Dim sAnyo As String

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_RECEPGRAL, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value '1
        Ador.MoveNext
        LblPedExt.caption = Ador(0).Value
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value & ":"
        sFecDesde = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value
        lblFacHasta.caption = Ador(0).Value
        sFecHasta = Ador(0).Value
        Ador.MoveNext
        chkParcial.caption = Ador(0).Value '5
        Ador.MoveNext
        chkTotal.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value & ":"
        
        sAnyo = Ador(0).Value
        Ador.MoveNext
        'lblNumPed.caption = ador(0).Value '8
        sPedido = Ador(0).Value
        Ador.MoveNext
        lblNumOrden.caption = Ador(0).Value
        sOrden = Ador(0).Value
        Ador.MoveNext
        lblProve.caption = Ador(0).Value & ":" '10
        sProve = Ador(0).Value
        Ador.MoveNext
                    
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        sdbcAprov.Columns(0).caption = Ador(0).Value
        sdbcReceptor.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        sdbcAprov.Columns(1).caption = Ador(0).Value
        sdbcReceptor.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgOrdenes.caption = Ador(0).Value
        Ador.MoveNext
        'sdbgOrdenes.Columns(2).caption = sAnyo '14
        'sdbgOrdenes.Columns(3).caption = ador(0).Value
        Ador.MoveNext
        sdbgOrdenes.Columns("PED").caption = Ador(0).Value
        Ador.MoveNext
        sdbgOrdenes.Columns("Proveedor").caption = sProve
        sdbgOrdenes.Columns("Fecha").caption = Ador(0).Value
        Ador.MoveNext
        sdbgOrdenes.Columns("Estado").caption = Ador(0).Value
        Ador.MoveNext
        sdbgOrdenes.Columns("FecAcep").caption = Ador(0).Value
        Ador.MoveNext
        
        lblAlbaran.caption = Ador(0).Value
        Ador.MoveNext
        ReDim sTextosEstados(2 To 6)
        sTextosEstados(3) = Ador(0).Value
        Ador.MoveNext
        sTextosEstados(4) = Ador(0).Value
        Ador.MoveNext
        sTextosEstados(5) = Ador(0).Value
        Ador.MoveNext
        sTextosEstados(6) = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        sIdiConsulta = Ador(0).Value
        Ador.MoveNext
        CmdEdicion.caption = Ador(0).Value
        sIdiEdicion = Ador(0).Value
        Ador.MoveNext
        sdbgOrdenes.Columns("Incorrecta").caption = Ador(0).Value
        Ador.MoveNext
        chkPedDirec.caption = Ador(0).Value
        Ador.MoveNext
        chkPedAprov.caption = Ador(0).Value
        Ador.MoveNext
        cmdRecepcion.caption = Ador(0).Value
        
        Ador.MoveNext
        sdbgOrdenes.Columns("EMPRESA").caption = Ador(0).Value
        m_sEmpresa = Ador(0).Value
        lblEmpresa.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblAprov.caption = Ador(0).Value & ":"
        sdbgOrdenes.Columns("APROV").caption = Ador(0).Value
        Ador.MoveNext
        lblReceptor.caption = Ador(0).Value & ":"
        sdbgOrdenes.Columns("RECEPTOR").caption = Ador(0).Value
        
        Ador.MoveNext
        sdbcEmpresa.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcEmpresa.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        m_sTituloRecep = Ador(0).Value
        Ador.MoveNext
        Me.cmdActualizar.ToolTipText = Ador(0).Value
        Ador.MoveNext
        Me.cmlistado.ToolTipText = Ador(0).Value
        
        Ador.MoveNext
        sdbgOrdenes.Columns("TIPOPEDIDO").caption = Ador(0).Value
        lblTipoPedido.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblFacDesde.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblFacAnyo.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblFacNum.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbcTipoPedido.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoPedido.Columns("DEN").caption = Ador(0).Value
        sdbcEstadoFactura.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoPedido.Columns("INV").caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoPedido.Columns("ALMAC").caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoPedido.Columns("RECEP").caption = Ador(0).Value
        Ador.MoveNext
        m_arrConcep(0) = Ador(0).Value
        Ador.MoveNext
        m_arrConcep(1) = Ador(0).Value
        Ador.MoveNext
        m_arrConcep(2) = Ador(0).Value
        Ador.MoveNext
        m_arrAlmac(1) = Ador(0).Value
        m_arrRecep(1) = Ador(0).Value
        Ador.MoveNext
        m_arrAlmac(0) = Ador(0).Value
        Ador.MoveNext
        m_arrAlmac(2) = Ador(0).Value
        m_arrRecep(2) = Ador(0).Value
        Ador.MoveNext
        m_arrRecep(0) = Ador(0).Value
        Ador.MoveNext
        lblSelec.caption = Ador(0).Value
        Ador.MoveNext
        sTextosEstados(2) = Ador(0).Value
        Ador.MoveNext
        lblEstFac.caption = Ador(0).Value & ":"
        Ador.MoveNext
        Ador.MoveNext
        m_sCerrar = Ador(0).Value
        Ador.MoveNext
        m_sReabrir = Ador(0).Value
        Ador.Close
        
    End If

    Set Ador = Nothing

End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Set oProves = Nothing
    Set oOrdenesEntrega = Nothing
    Set m_oEmpresas = Nothing
    Set m_oTipoPedido = Nothing
    
    Accion = ACCRecepcionPedConsulta
    Me.Visible = False
End Sub

Private Sub sdbcAnyo_Change()
    sdbgOrdenes.RemoveAll
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcProveCod = ""
        sdbcProveDen = ""
        txtPedido = ""
        txtOrdEntrega = ""
        bRespetarCombo = False
    End If

End Sub

Private Sub sdbcAnyo_Click()
    txtPedido = ""
    txtOrdEntrega = ""
    sdbgOrdenes.RemoveAll

End Sub


Private Sub sdbcProveCod_Change()
    
    If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveDen = ""
        Set oProveSeleccionado = Nothing
        RespetarComboProve = False
   End If
   sdbgOrdenes.RemoveAll

End Sub

Private Sub sdbcProveCod_Click()
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
    sdbgOrdenes.RemoveAll
End Sub

Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
      
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcProveCod.RemoveAll
     
    Screen.MousePointer = vbHourglass
    
    If sdbcProveCod.Text <> "" Then
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveCod.Text, , , , , , , , , , , , False, , , , , , , , , , , , , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , False, , , , , , , , , , , , , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If
       
    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveCod.Rows - 1
            bm = sdbcProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub


Private Sub sdbcProveCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    If sdbcProveCod.Text = sdbcProveCod.Columns(0).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
        RespetarComboProve = False
        Exit Sub
    End If
    
    If sdbcProveCod.Text = sdbcProveDen.Columns(1).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
        RespetarComboProve = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proveedor
    Screen.MousePointer = vbHourglass
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Text = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Text = sdbcProveDen.Text
            
        RespetarComboProve = False
        
        Screen.MousePointer = vbHourglass
        ProveedorSeleccionado
        Screen.MousePointer = vbNormal
    End If

End Sub

Private Sub sdbcProveDen_Change()

    If Not RespetarComboProve Then
        RespetarComboProve = True
        sdbcProveCod = ""
        RespetarComboProve = False
        Set oProveSeleccionado = Nothing
    End If
    
    sdbgOrdenes.RemoveAll
End Sub

Private Sub sdbcProveDen_Click()
      If Not sdbcProveDen.DroppedDown Then
        sdbcProveDen = ""
        sdbcProveCod = ""
    End If
  
    sdbgOrdenes.RemoveAll
End Sub

Private Sub sdbcProveDen_CloseUp()
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
   
    sdbcProveDen.RemoveAll
        
    Screen.MousePointer = vbHourglass
    
    If sdbcProveDen.Text = "" Then
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , , , , , True, , , , , , , , , , , , , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    Else
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , sdbcProveDen.Text, , , , , , , , , , , True, , , , , , , , , , , , , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    End If
        
    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveDen.Refresh

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbgOrdenes_AfterUpdate(RtnDispErrMsg As Integer)
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim v As Variant
    Dim b As Boolean
     
    If bRespetarUpdate Then Exit Sub
    If oLineaEnEdicion Is Nothing Then
        DoEvents
        sdbgOrdenes_Change
        DoEvents
    End If
    
    DoEvents
    bModError = False
    
    ''' Modificamos en la base de datos
       
    If sdbgOrdenes.Columns("Incorrecta").Value = "-1" Or sdbgOrdenes.Columns("Incorrecta").Value = "1" Then
        b = True
    Else
        b = False
    End If
    teserror = oLineaEnEdicion.ModificarRecepcionIncorrecta(b)
       
    If teserror.NumError <> TESnoerror Then
    
        v = sdbgOrdenes.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgOrdenes.SetFocus
        bModError = True
        sdbgOrdenes.ActiveCell.Value = v
        Accion = ACCRecepcionPedConsulta
        sdbgOrdenes.DataChanged = False
        
    Else
        
        ''' Registro de acciones
        basSeguridad.RegistrarAccion accionessummit.ACCRecepcionPedModIncorr, "Pedido: " & oOrdenesEntrega.Item(sdbgOrdenes.Columns("ID").Value).Anyo & "/" & oOrdenesEntrega.Item(sdbgOrdenes.Columns("ID").Value).NumPedido & "/" & oOrdenesEntrega.Item(sdbgOrdenes.Columns("ID").Value).Numero & " Id Orden:" & oOrdenesEntrega.Item(sdbgOrdenes.Columns("ID").Value).Id & "Recepcion Incorrecta:" & oOrdenesEntrega.Item(sdbgOrdenes.Columns("ID").Value).Incorrecta
        Accion = ACCRecepcionPedConsulta
        cmdDeshacer.Enabled = False
        Set oIBaseDatos = Nothing
        Set oLineaEnEdicion = Nothing
        
    End If
    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' Segun el caso que sea. Si la celda es aprovador o receptor envia el foco a otro formulario. Si la celda es cerrar, cierra la orden tras preguntar.
''' </summary>
''' <remarks>Llamada desde: Sistema; Tiempo m�ximo:0,1seg.</remarks>
Private Sub sdbgOrdenes_BtnClick()
    Dim oPer As CPersona
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    
    'Muestra el detalle del aprovisionador o del receptor::
    If sdbgOrdenes.Columns(sdbgOrdenes.col).Name = "APROV" Or sdbgOrdenes.Columns(sdbgOrdenes.col).Name = "RECEPTOR" Then
        If sdbgOrdenes.Columns(sdbgOrdenes.col).Name = "APROV" Then
            'Detalle del aprovisionador
            If sdbgOrdenes.Columns("COD_APROV").Value = "" Then Exit Sub
            Set oPer = oFSGSRaiz.Generar_CPersona
            oPer.Cod = Trim(sdbgOrdenes.Columns("COD_APROV").Value)
        Else
            'Detalle del receptor
            If sdbgOrdenes.Columns("COD_RECEP").Value = "" Then Exit Sub
            Set oPer = oFSGSRaiz.Generar_CPersona
            oPer.Cod = Trim(sdbgOrdenes.Columns("COD_RECEP").Value)
        End If
        
        teserror = oPer.CargarTodosLosDatos
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Exit Sub
        End If
        
        Set frmESTRORGPersona.frmOrigen = Me
        frmESTRORGPersona.txtCod = oPer.Cod
        frmESTRORGPersona.txtNom = NullToStr(oPer.nombre)
        frmESTRORGPersona.txtApel = oPer.Apellidos
        frmESTRORGPersona.txtCargo = NullToStr(oPer.Cargo)
        frmESTRORGPersona.txtFax = NullToStr(oPer.Fax)
        frmESTRORGPersona.txtMail = NullToStr(oPer.mail)
        frmESTRORGPersona.txtTfno = NullToStr(oPer.Tfno)
        frmESTRORGPersona.txtTfno2 = NullToStr(oPer.Tfno2)
        frmESTRORGPersona.sdbcEquipo.Text = NullToStr(oPer.codEqp)
        If NullToStr(oPer.codEqp) <> "" Then
            frmESTRORGPersona.chkFunCompra.Value = vbChecked
        End If
        Screen.MousePointer = vbNormal
        frmESTRORGPersona.Show vbModal
        
        Set oPer = Nothing
        Exit Sub
    End If
    
    If sdbgOrdenes.Columns(sdbgOrdenes.col).Name = "CERRAR" Then
    
        Set oLineaEnEdicion = Nothing
        Set oLineaEnEdicion = oOrdenesEntrega.Item(sdbgOrdenes.Columns("ID").Value)
                        
        If oLineaEnEdicion.Estado < RecibidoYCerrado Then
        
            'tarea 10414: No permitir cerrar un pedido de tipo recepci�n obligatoria si no tiene recepciones en todas sus lineas
            If oLineaEnEdicion.TipoDePedido.CodRecep = ObligatorioRececpionar Then
                If Not oLineaEnEdicion.TieneRecepcionEnTodasLasLineas Then
                    oMensajes.ImposibleCerrarOrdenEntrega (1)
                    Exit Sub
                End If
            End If
        
            irespuesta = oMensajes.PreguntaEstadoCerrado(sdbgOrdenes.Columns("PED").Value)
            If irespuesta = vbYes Then
                oLineaEnEdicion.CambiaEstadoPersona = m_sPersona
            
                teserror = oLineaEnEdicion.CerrarSinRecepcionar
                If teserror.NumError = TESnoerror Then
                    sdbgOrdenes.Columns("ESTADO").Value = sTextosEstados(oLineaEnEdicion.Estado)
                    sdbgOrdenes.Columns("FecAcep").Value = oLineaEnEdicion.FechaEstado
                    sdbgOrdenes.Columns("CERRAR").Value = m_sReabrir
                    bRespetarUpdate = True
                    sdbgOrdenes.Update
                    bRespetarUpdate = False
                End If
            End If
        Else
            irespuesta = oMensajes.PreguntaEstadoReabrir(sdbgOrdenes.Columns("PED").Value)
            If irespuesta = vbYes Then
                oLineaEnEdicion.CambiaEstadoPersona = m_sPersona
                teserror = oLineaEnEdicion.Reabrir
                If teserror.NumError = TESnoerror Then
                    sdbgOrdenes.Columns("ESTADO").Value = sTextosEstados(oLineaEnEdicion.Estado)
                    sdbgOrdenes.Columns("FecAcep").Value = oLineaEnEdicion.FechaEstado
                    sdbgOrdenes.Columns("CERRAR").Value = m_sCerrar
                    bRespetarUpdate = True
                    sdbgOrdenes.Update
                    bRespetarUpdate = False
                End If
            End If
        End If
    End If
End Sub


Private Sub sdbgOrdenes_Change()
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    Dim teserror As TipoErrorSummit
    Dim b As Boolean
    
    
    If bModoEdicion Then
    
        'DoEvents
        
        
        Set oLineaEnEdicion = Nothing
        
        'Set oLineaEnEdicion = oOrdenesEntrega.Item(CStr(sdbgOrdenes.AddItemRowIndex(sdbgOrdenes.Bookmark)))
        Set oLineaEnEdicion = oOrdenesEntrega.Item(sdbgOrdenes.Columns("ID").Value)
        
        If oLineaEnEdicion.Estado = AceptadoPorProveedor Or oLineaEnEdicion.Estado = EnCamino Then
            oMensajes.OrdenEntregaNoModificable
            sdbgOrdenes.CancelUpdate
            Exit Sub
        End If
                     
        Set oIBaseDatos = oLineaEnEdicion
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
            
            TratarError teserror
            sdbgOrdenes.DataChanged = False
            If oLineaEnEdicion.Incorrecta Then
                b = 1
            Else
                b = 0
            End If
            sdbgOrdenes.Columns("Incorrecta").Value = b
            teserror.NumError = TESnoerror
            
        End If
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgOrdenes.SetFocus
        Else
            Accion = ACCRecepcionPedModIncorr
            cmdDeshacer.Enabled = True
        End If
         
    End If

    
End Sub

''' <summary>
''' Doble click sobre el grid, nos lleva a la ventana de detalle de recepci�n frmREcepcion
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer doble_click; Tiempo m�ximo:0</remarks>

Private Sub sdbgOrdenes_DblClick()
Dim oRecepciones As cRecepciones
Dim oOrdenSeleccionada As COrdenEntrega
Dim oProve As CProveedor
    
    If bModoEdicion Then Exit Sub
    
    If sdbgOrdenes.Rows = 0 Then Exit Sub
    
    If sdbgOrdenes.Columns("ID").Value = "" Then Exit Sub
    
    If Not (bMod Or bCons) Then
        oMensajes.SinRecepciones
        Exit Sub
    End If
        
    ' habr� que limpiar campos de la ventana o hacerlo en el form load

    ' el metodo DevolverRecepcionesDeOrden tiene que tener un order by , con tipo fecha en descendente
    Set oOrdenSeleccionada = Nothing
    Set oOrdenSeleccionada = oOrdenesEntrega.Item(sdbgOrdenes.Columns("ID").Value)
    
    If oOrdenSeleccionada Is Nothing Then Exit Sub
    
    Set oProve = BuscarProveedor(oOrdenSeleccionada.ProveCod)
    oOrdenSeleccionada.ProveDen = oProve.Den
    
    Set oRecepciones = oOrdenSeleccionada.DevolverTodasLasRecepciones(, True)
     
    If oRecepciones Is Nothing Then
        Set oProve = Nothing
        Exit Sub
    Else
        If frmRecepcion.Accion <> 0 Then
            frmRecepcion.Show
        Else
            If oRecepciones.Count > 0 Then
                frmRecepcion.Accion = ACCRecepcionPedConsulta
                frmRecepcion.bRMat = bRMat
                If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Directo) And oOrdenSeleccionada.Tipo = Directo Then
                ' Restricciones si sentido integraci�n es solo de entrada,tambien para el Administrador
                    frmRecepcion.bMod = False
                Else
                    frmRecepcion.bMod = bMod
                End If
                Set frmRecepcion.oOrdenEntrega = oOrdenSeleccionada
                frmRecepcion.caption = m_sTituloRecep & " " & sdbgOrdenes.Columns("PED").Value
                frmRecepcion.sdbcFecRec.Columns("ID").Value = oRecepciones.Item(1).Id
                frmRecepcion.sdbcFecRec = oRecepciones.Item(1).FechaRec
                frmRecepcion.sdbcFecRec_Validate False
                frmRecepcion.bCargarComboDesde = False
                frmRecepcion.bActualizaSeguimiento = bVienesDeSeguimiento
                frmRecepcion.SetFocus
                Set oProve = Nothing
                Set oRecepciones = Nothing
                Set oOrdenSeleccionada = Nothing

            Else
                If (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Directo) And oOrdenSeleccionada.Tipo = Directo) Or (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Aprov) And oOrdenSeleccionada.Tipo = Aprovisionamiento) Then
                ' Restricciones si sentido integraci�n es solo de entrada,tambien para el Administrador
                    Exit Sub
                End If
                
                'Antes de dar de alta una recepci�n se comprobar� que el pedido est� integrado.
                If Not ComprobarPedidoIntegrado(oOrdenSeleccionada) Then
                    oMensajes.MensajeOKOnly 1115
                    Exit Sub
                End If
                
                If oOrdenSeleccionada.AlgunoNoRecepAutom = False Then
                    oMensajes.MensajeOKOnly 1275
                    Exit Sub
                End If
                
                'Permisos de solo consulta, se puede meter una recepci�n cuando el pedido no tiene ninguna, no deber�a dejar
                If Not bMod Then
                    oMensajes.SinRecepciones
                    Exit Sub
                End If
                If oOrdenSeleccionada.Estado >= RecibidoYCerrado Then
                    oMensajes.SinRecepciones
                    Exit Sub
                End If
                
                frmRecepcion.Accion = ACCRecepcionPedAnya
                frmRecepcion.bRMat = bRMat
                frmRecepcion.bMod = bMod
                Set frmRecepcion.oOrdenEntrega = oOrdenSeleccionada
                frmRecepcion.caption = m_sTituloRecep & " " & sdbgOrdenes.Columns("PED").Value
                frmRecepcion.bActualizaSeguimiento = bVienesDeSeguimiento
                frmRecepcion.Show
                If frmRecepcion.cmdAceptar.Visible = False Then
                    Unload frmRecepcion 'Si por la vigencia no se puede dar alta
                Else
                    frmRecepcion.SetFocus
                End If
                Set oProve = Nothing
                Set oRecepciones = Nothing
                Set oOrdenSeleccionada = Nothing
            End If
        End If
    End If


End Sub


Private Sub sdbgOrdenes_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgOrdenes.DataChanged = False Then
            
            sdbgOrdenes.CancelUpdate
            sdbgOrdenes.DataChanged = False
            
            If Not oLineaEnEdicion Is Nothing Then
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
                Set oLineaEnEdicion = Nothing
            End If
           
                cmdDeshacer.Visible = False
                Accion = ACCRecepcionPedConsulta
         
        End If
    
    End If

End Sub

Private Sub txtalbaran_Change()
    sdbgOrdenes.RemoveAll
End Sub

Private Sub txtFecDesde_Change()
    sdbgOrdenes.RemoveAll
End Sub

Private Sub txtFecHasta_Change()
sdbgOrdenes.RemoveAll
End Sub

Private Sub txtOrdEntrega_Change()
    sdbgOrdenes.RemoveAll
End Sub

Private Sub txtPedido_Change()
    sdbgOrdenes.RemoveAll
End Sub

Private Sub txtPedidoExt_Change()
    sdbgOrdenes.RemoveAll
End Sub

Private Sub sdbcEmpresa_Change()
    If Not m_bRespetarComboEmpresa Then
       m_bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcEmpresa_CloseUp()
    If sdbcEmpresa.Value = "..." Or sdbcEmpresa.Text = "" Then
        sdbcEmpresa.Text = ""
        Exit Sub
    End If
    
    m_bRespetarComboEmpresa = True
    sdbcEmpresa.Value = sdbcEmpresa.Columns(1).Value
    m_bRespetarComboEmpresa = False
    
    m_bCargarComboDesde = False
End Sub

Private Sub sdbcEmpresa_DropDown()
    Dim oEmpresa As CEmpresa
    
    Screen.MousePointer = vbHourglass
    
    sdbcEmpresa.RemoveAll
    
    If m_bCargarComboDesde Then
        If m_bREmpresa = True Then
            m_oEmpresas.CargarTodasLasEmpresasDesde sdbcEmpresa.Text, , , , True, True, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        Else
            m_oEmpresas.CargarTodasLasEmpresasDesde sdbcEmpresa.Text, , , , True, True
        End If
    Else
        If m_bREmpresa = True Then
            m_oEmpresas.CargarTodasLasEmpresasDesde , , , , True, , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
        Else
            m_oEmpresas.CargarTodasLasEmpresasDesde , , , , True
        End If
    End If
    
    For Each oEmpresa In m_oEmpresas
        sdbcEmpresa.AddItem oEmpresa.nif & Chr(m_lSeparador) & oEmpresa.Den & Chr(m_lSeparador) & oEmpresa.Id
    Next
        
    sdbcEmpresa.SelStart = 0
    sdbcEmpresa.SelLength = Len(sdbcEmpresa.Text)
    sdbcEmpresa.Refresh
    
    m_bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEmpresa_InitColumnProps()
    sdbcEmpresa.DataFieldList = "Column 0"
    sdbcEmpresa.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcEmpresa_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEmpresa.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEmpresa.Rows - 1
            bm = sdbcEmpresa.GetBookmark(i)
            If UCase(sdbcEmpresa.Text) = UCase(Mid(sdbcEmpresa.Columns(0).CellText(bm), 1, Len(sdbcEmpresa.Text))) Then
                sdbcEmpresa.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbcEmpresa_Validate(Cancel As Boolean)
    If sdbcEmpresa.Text = "" Then
        m_bCargarComboDesde = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    m_oEmpresas.CargarTodasLasEmpresasDesde , Trim(sdbcEmpresa.Text)

    If m_oEmpresas.Count = 0 Then
        sdbcEmpresa.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sEmpresa

    Else
        m_bRespetarComboEmpresa = True

        sdbcEmpresa.Columns(0).Value = m_oEmpresas.Item(1).nif
        sdbcEmpresa.Columns(1).Value = m_oEmpresas.Item(1).Den
        sdbcEmpresa.Columns(2).Value = m_oEmpresas.Item(1).Id
        
        m_bRespetarComboEmpresa = False

        m_bCargarComboDesde = False
    End If

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcAprov_Change()
    If Not m_bRespetarComboUsu Then
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcAprov_CloseUp()
   
    If sdbcAprov.Value = "..." Then
        sdbcAprov.Text = ""
        Exit Sub
    End If
    
    If sdbcAprov.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    m_bRespetarComboUsu = True
    sdbcAprov.Text = sdbcAprov.Columns(1).Text
    m_bRespetarComboUsu = False
    
    bCargarComboDesde = False
End Sub


Private Sub sdbcAprov_DropDown()
    Dim oRes As Ador.Recordset
    Dim i As Integer
        
    sdbcAprov.RemoveAll
    
    'Cargamos los codigos
    Screen.MousePointer = vbHourglass

    If bCargarComboDesde Then
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(Trim(sdbcAprov.Text), False, TipoOrdenacionPersonas.OrdPorCod, m_bPersUO, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True)
    Else
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(, False, TipoOrdenacionPersonas.OrdPorCod, m_bPersUO, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True)
    End If

    While Not oRes.EOF
        sdbcAprov.AddItem oRes("CODPER").Value & Chr(m_lSeparador) & oRes("NOM").Value & " " & oRes("APE").Value & Chr(m_lSeparador) & oRes("USUCOD").Value
        oRes.MoveNext
    Wend

    oRes.Close
    Set oRes = Nothing
    
    sdbcAprov.SelStart = 0
    sdbcAprov.SelLength = Len(sdbcAprov.Text)
    sdbcAprov.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcAprov_InitColumnProps()
    sdbcAprov.DataFieldList = "Column 0"
    sdbcAprov.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcAprov_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcAprov.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcAprov.Rows - 1
            bm = sdbcAprov.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcAprov.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcAprov.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcAprov_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oRes As Ador.Recordset
    
    
    If sdbcAprov.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el usuario
    
    Screen.MousePointer = vbHourglass
    Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(sdbcAprov.Columns(0).Value, True, , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True)
    
    bExiste = Not (oRes.EOF)
    
    If Not bExiste Then
        sdbcAprov.Text = ""
    Else
        m_bRespetarComboUsu = True
        sdbcAprov.Columns(0).Value = oRes("CODPER").Value
        sdbcAprov.Columns(1).Value = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcAprov.Columns(2).Value = oRes("USUCOD").Value
        m_bRespetarComboUsu = False
        bCargarComboDesde = False
    End If
    
    oRes.Close
    Set oRes = Nothing
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcReceptor_Change()
    If Not m_bRespetarComboRecep Then
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcReceptor_CloseUp()
   
    If sdbcReceptor.Value = "..." Then
        sdbcReceptor.Text = ""
        Exit Sub
    End If
    
    If sdbcReceptor.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    m_bRespetarComboRecep = True
    sdbcReceptor.Text = sdbcReceptor.Columns(1).Text
    m_bRespetarComboRecep = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcReceptor_DropDown()
    Dim oRes As Ador.Recordset

    sdbcReceptor.RemoveAll
    
    'Cargamos los codigos
    Screen.MousePointer = vbHourglass

    If bCargarComboDesde Then
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(Trim(sdbcReceptor.Text), False, TipoOrdenacionPersonas.OrdPorCod, m_bPersUO, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , True)
    Else
        Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(, False, TipoOrdenacionPersonas.OrdPorCod, m_bPersUO, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , True)
    End If

    While Not oRes.EOF
        sdbcReceptor.AddItem oRes("CODPER").Value & Chr(m_lSeparador) & oRes("NOM").Value & " " & oRes("APE").Value & Chr(m_lSeparador) & oRes("USUCOD").Value
        oRes.MoveNext
    Wend

    oRes.Close
    Set oRes = Nothing
    
    sdbcReceptor.SelStart = 0
    sdbcReceptor.SelLength = Len(sdbcReceptor.Text)
    sdbcReceptor.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcReceptor_InitColumnProps()
    sdbcReceptor.DataFieldList = "Column 0"
    sdbcReceptor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcReceptor_PositionList(ByVal Text As String)
     ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcReceptor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcReceptor.Rows - 1
            bm = sdbcReceptor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcReceptor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcReceptor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcReceptor_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oRes As Ador.Recordset
    
    
    If sdbcReceptor.Text = "" Then
        bCargarComboDesde = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el usuario
    
    Screen.MousePointer = vbHourglass
    Set oRes = oGestorSeguridad.DevolverUsuariosDeTipoPersona(sdbcReceptor.Columns(0).Value, True, , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , True)
    
    bExiste = Not (oRes.EOF)
    
    If Not bExiste Then
        sdbcReceptor.Text = ""
    Else
        m_bRespetarComboRecep = True
        'sdbcReceptor.Text = oRes("CODPER").Value
        sdbcReceptor.Columns(0).Value = oRes("CODPER").Value
        sdbcReceptor.Columns(1).Value = oRes("NOM").Value & " " & oRes("APE").Value
        sdbcReceptor.Columns(2).Value = oRes("USUCOD").Value
        m_bRespetarComboRecep = False
        bCargarComboDesde = False
    End If
    
    oRes.Close
    Set oRes = Nothing
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcTipoPedido_CloseUp()
    bRespetarCombo = True
    sdbcTipoPedido.Value = sdbcTipoPedido.Columns(0).Text & " - " & sdbcTipoPedido.Columns(1).Text
    Set m_oTipoPedido = m_oTiposPedido.Item(sdbcTipoPedido.Columns("ID").Value)
    bRespetarCombo = False
End Sub
Private Sub sdbcTipoPedido_Change()
    
    If Not bRespetarCombo Then
        RespetarComboProve = True
        Set m_oTipoPedido = Nothing
        RespetarComboProve = False
   End If
   sdbgOrdenes.RemoveAll

End Sub

Private Sub sdbcTipoPedido_DropDown()
    Dim oTipoPedido As CTipoPedido

    Screen.MousePointer = vbHourglass

    sdbcTipoPedido.RemoveAll
       
    For Each oTipoPedido In m_oTiposPedido
        sdbcTipoPedido.AddItem oTipoPedido.Cod & Chr(m_lSeparador) & oTipoPedido.Den & _
        Chr(m_lSeparador) & m_arrConcep(oTipoPedido.CodConcep) & Chr(m_lSeparador) & m_arrAlmac(oTipoPedido.CodAlmac) & Chr(m_lSeparador) & m_arrRecep(oTipoPedido.CodRecep) & _
        Chr(m_lSeparador) & oTipoPedido.CodConcep & Chr(m_lSeparador) & oTipoPedido.CodAlmac & Chr(m_lSeparador) & oTipoPedido.CodRecep & _
        Chr(m_lSeparador) & oTipoPedido.indice
    Next

    sdbcTipoPedido.SelStart = 0
    sdbcTipoPedido.SelLength = Len(sdbcTipoPedido.Text)
    sdbcTipoPedido.Refresh


    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFacDesde_Click()
    AbrirFormCalendar Me, txtFacDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFacHasta_Click()
    AbrirFormCalendar Me, txtFacHasta
End Sub

''' <summary>
''' Carga el combo de estados de pago con los posibles estados que puede tener un pago de una factura
''' </summary>
''' <remarks>Tiempo m�ximo=0,1seg.</remarks>
Private Sub sdbcEstadoFactura_DropDown()
    Dim oFacturas As CFacturas
    Dim rs As Recordset
    Screen.MousePointer = vbHourglass
    
    sdbcEstadoFactura.RemoveAll
    Set oFacturas = oFSGSRaiz.Generar_CFacturas
    
    Set rs = oFacturas.devolverTipoEstados(gParametrosInstalacion.gIdioma)

    While Not rs.EOF
        sdbcEstadoFactura.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("DEN").Value
        rs.MoveNext
    Wend
    Set oFacturas = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>
''' Almacena el valor del estado de una factura seleccionado
''' </summary>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbcEstadoFactura_CloseUp()
    If sdbcEstadoFactura.Value = "0" Or Trim(sdbcEstadoFactura.Value) = "" Then
        sdbcTipoPedido.Text = ""
        Exit Sub
    End If
    sdbcEstadoFactura.Text = sdbcEstadoFactura.Columns(1).Value
End Sub

Private Sub sdbcEstadoFactura_InitColumnProps()
    sdbcEstadoFactura.DataFieldList = "Column 0"
    sdbcEstadoFactura.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>
''' Valida que el Estado de una factura introducida sea el correcto
''' </summary>
''' <param name="Cancel">Propio del evento</param>
''' <remarks>Tiempo m�ximo=0seg.</remarks>
Private Sub sdbcEstadoFactura_Validate(Cancel As Boolean)
Dim i As Integer
Dim bm As Variant
Dim bEncontrado As Boolean
    
    If sdbcEstadoFactura.Text <> "" Then
        sdbcEstadoFactura.MoveFirst
        For i = 0 To sdbcEstadoFactura.Rows - 1
         bm = sdbcEstadoFactura.GetBookmark(i)
         If UCase(sdbcEstadoFactura.Text) = UCase(Mid(sdbcEstadoFactura.Columns("DEN").CellText(bm), 1, Len(sdbcEstadoFactura.Text))) Then
             sdbcEstadoFactura.Bookmark = bm
             sdbcEstadoFactura.Text = sdbcEstadoFactura.Columns("DEN").Value
             bEncontrado = True
             Exit Sub
         End If
        Next i
        sdbcEstadoFactura.Text = ""
        sdbcEstadoFactura.Bookmark = -1
        End If
End Sub

''' <summary>
''' Posicionarse en el combo segun la seleccion
''' </summary>
''' <param name="Text">Valor de la celda</param>
''' <remarks>Tiempo m�ximo=0</remarks>
Private Sub sdbcEstadoFactura_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEstadoFactura.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEstadoFactura.Rows - 1
            bm = sdbcEstadoFactura.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEstadoFactura.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcEstadoFactura.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>
''' Se comprueba si el pedido est� integrado.
''' </summary>
''' <param name="oOrdenSeleccionada">Orden seleccionada</param>
''' <returns>Devuelve true si el pedido est� integrado y false si no.</returns>
''' <remarks>Se le llama desde sdbgOrdenes_DblClick() y cmdRecepcion_Click(); Tiempo m�ximo=0'3</remarks>
Private Function ComprobarPedidoIntegrado(ByVal oOrdenSeleccionada As COrdenEntrega) As Boolean
    ComprobarPedidoIntegrado = True
    If (basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_directo) And basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Directo) And (oOrdenSeleccionada.Tipo = Directo Or oOrdenSeleccionada.Tipo = PedidosPM)) Or _
       (basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_Aprov) And basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Aprov) And (oOrdenSeleccionada.Tipo = Aprovisionamiento Or oOrdenSeleccionada.Tipo = PedidosPM)) Then
        If Not oOrdenSeleccionada.PedidoIntegrado Then
            ComprobarPedidoIntegrado = False
        End If
    End If
    
End Function

''' <summary>
''' Carga las recepciones de una orden seleccionada en frmSeguimiento
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,1</remarks>
Private Sub CargaDesdeSeguimiento()
    Dim arTipoPed(0 To 1) As Variant
    
    If lIdOrden > 0 Then
        Screen.MousePointer = vbHourglass
        
        Me.sdbcAnyo.Value = iAnyo
        Me.txtPedido.Text = lNumPedido
        Me.txtOrdEntrega.Text = lNumOrden
        
        arTipoPed(0) = 0
        
        oOrdenesEntrega.BuscarTodasLasOrdenes gParametrosInstalacion.giCargaMaximaCombos, , , , , , , , , , bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , arTipoPed, , True, , , m_arrTiposPedido, lIdOrden
        
        CmdEdicion.Enabled = True
        cmdRecepcion.Enabled = True
        
        CargarOrdenes oOrdenesEntrega
        
        bVienesDeSeguimiento = True
        
        lIdOrden = 0
        
        Me.chkParcial.Value = vbChecked
        Me.chkTotal.Value = vbChecked
        
        Screen.MousePointer = vbNormal
    End If
End Sub
