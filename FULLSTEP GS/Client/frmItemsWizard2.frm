VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmItemsWizard2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Valores para completar los �tems"
   ClientHeight    =   8310
   ClientLeft      =   1980
   ClientTop       =   3405
   ClientWidth     =   6795
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmItemsWizard2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8310
   ScaleWidth      =   6795
   Begin VB.PictureBox picOtros 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2385
      Left            =   0
      ScaleHeight     =   2385
      ScaleWidth      =   6885
      TabIndex        =   27
      Top             =   5400
      Width           =   6885
      Begin VB.TextBox txtFecFin 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   4830
         TabIndex        =   34
         Top             =   1620
         Width           =   1125
      End
      Begin VB.CommandButton cmdCalFecFin 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6000
         Picture         =   "frmItemsWizard2.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   1620
         Width           =   315
      End
      Begin VB.TextBox txtFecIni 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1455
         TabIndex        =   32
         Top             =   1620
         Width           =   1125
      End
      Begin VB.CommandButton cmdCalFecIni 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2610
         Picture         =   "frmItemsWizard2.frx":123C
         Style           =   1  'Graphical
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   1620
         Width           =   315
      End
      Begin VB.CheckBox chkProve 
         Caption         =   "Introducir proveedor de la �ltima adjudicaci�n"
         ForeColor       =   &H00000000&
         Height          =   360
         Left            =   150
         TabIndex        =   30
         Top             =   1185
         Width           =   5500
      End
      Begin VB.CommandButton cmdBorrarSolicit 
         Height          =   285
         Left            =   5620
         Picture         =   "frmItemsWizard2.frx":17C6
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   1980
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscarSolicit 
         Height          =   285
         Left            =   6000
         Picture         =   "frmItemsWizard2.frx":186B
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   1980
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcDestCod 
         Height          =   285
         Left            =   1455
         TabIndex        =   35
         Top             =   120
         Width           =   1125
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   9128
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1984
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcDestDen 
         Height          =   285
         Left            =   2595
         TabIndex        =   36
         Top             =   120
         Width           =   3705
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   9049
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2566
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6535
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPagoCod 
         Height          =   285
         Left            =   1455
         TabIndex        =   37
         Top             =   480
         Width           =   1125
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1984
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPagoDen 
         Height          =   285
         Left            =   2595
         TabIndex        =   38
         Top             =   480
         Width           =   3705
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6773
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2355
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6535
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
         Height          =   285
         Left            =   1455
         TabIndex        =   39
         Top             =   840
         Width           =   1125
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1984
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
         Height          =   285
         Left            =   2595
         TabIndex        =   40
         Top             =   840
         Width           =   3705
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6429
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2963
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6535
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.Line Line4 
         BorderColor     =   &H8000000C&
         BorderWidth     =   2
         X1              =   0
         X2              =   6780
         Y1              =   30
         Y2              =   30
      End
      Begin VB.Label lblPago 
         Caption         =   "Forma de pago"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   150
         TabIndex        =   47
         Top             =   480
         Width           =   1185
      End
      Begin VB.Label lblProve 
         Caption         =   "Proveedor"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   150
         TabIndex        =   46
         Top             =   840
         Width           =   1140
      End
      Begin VB.Label lblFecFin 
         BackStyle       =   0  'Transparent
         Caption         =   "Fin suministro"
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   3675
         TabIndex        =   45
         Top             =   1665
         Width           =   1095
      End
      Begin VB.Label lblFecIni 
         BackStyle       =   0  'Transparent
         Caption         =   "Inicio suministro"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   150
         TabIndex        =   44
         Top             =   1665
         Width           =   1275
      End
      Begin VB.Label lblSolicitud 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1455
         TabIndex        =   42
         Top             =   1980
         Width           =   4095
      End
      Begin VB.Label lblDest 
         Caption         =   "Destino"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   150
         TabIndex        =   41
         Top             =   120
         Width           =   900
      End
      Begin VB.Label lblLitSolicit 
         Caption         =   "DSolicitud:"
         Height          =   255
         Left            =   150
         TabIndex        =   43
         Top             =   2025
         Width           =   1575
      End
   End
   Begin VB.PictureBox picPres 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3045
      Left            =   0
      ScaleHeight     =   3045
      ScaleWidth      =   6885
      TabIndex        =   17
      Top             =   2370
      Width           =   6885
      Begin VB.CheckBox chkPres 
         Caption         =   "Introducir precio de la �ltima adjudicaci�n"
         ForeColor       =   &H00000000&
         Height          =   405
         Left            =   2880
         TabIndex        =   21
         Top             =   105
         Width           =   3720
      End
      Begin VB.TextBox txtPres 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1455
         TabIndex        =   20
         Top             =   405
         Width           =   1110
      End
      Begin VB.CheckBox chkPresPlanif 
         Caption         =   "DIntroducir presupuesto unitario planificado"
         Height          =   300
         Left            =   2880
         TabIndex        =   19
         Top             =   480
         Width           =   3720
      End
      Begin VB.TextBox txtPresUniItem 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   3150
         TabIndex        =   18
         Top             =   855
         Visible         =   0   'False
         Width           =   1830
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGridEscalados 
         Height          =   1290
         Left            =   270
         TabIndex        =   22
         Top             =   1635
         Visible         =   0   'False
         Width           =   6435
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   5
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmItemsWizard2.frx":18F8
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6403
         Columns(1).Caption=   "Cantidades directas"
         Columns(1).Name =   "DIR"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).NumberFormat=   "standard"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Caption=   "Cantidad inicial"
         Columns(2).Name =   "INI"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).NumberFormat=   "standard"
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Caption=   "Cantidad final"
         Columns(3).Name =   "FIN"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "standard"
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   3200
         Columns(4).Caption=   "Presupuesto Unitario"
         Columns(4).Name =   "PRES"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).NumberFormat=   "#,##0.00####################"
         Columns(4).FieldLen=   256
         _ExtentX        =   11351
         _ExtentY        =   2275
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00808080&
         BorderWidth     =   2
         X1              =   0
         X2              =   6780
         Y1              =   30
         Y2              =   30
      End
      Begin VB.Label lblValPres 
         Caption         =   "Valor:"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   495
         TabIndex        =   26
         Top             =   435
         Width           =   765
      End
      Begin VB.Label lblPres 
         BackStyle       =   0  'Transparent
         Caption         =   "Presupuesto"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   90
         TabIndex        =   25
         Top             =   90
         Width           =   1095
      End
      Begin VB.Label LabelPresUni 
         AutoSize        =   -1  'True
         Caption         =   "Presupuestos unitarios escalados:"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   270
         TabIndex        =   24
         Top             =   1215
         Visible         =   0   'False
         Width           =   2445
      End
      Begin VB.Label lblPresUniItem 
         Caption         =   "Presupuesto unitario para el item:"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   495
         TabIndex        =   23
         Top             =   855
         Visible         =   0   'False
         Width           =   2535
      End
   End
   Begin VB.PictureBox picCantidad 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   765
      Left            =   0
      ScaleHeight     =   765
      ScaleWidth      =   6885
      TabIndex        =   12
      Top             =   1590
      Width           =   6885
      Begin VB.CheckBox chkCant 
         Caption         =   "Introducir autom�ticamente la cantidad estimada para el art�culo"
         ForeColor       =   &H00000000&
         Height          =   405
         Left            =   2880
         TabIndex        =   14
         Top             =   330
         Width           =   3645
      End
      Begin VB.TextBox txtCant 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1455
         TabIndex        =   13
         Top             =   390
         Width           =   1110
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00808080&
         BorderWidth     =   2
         X1              =   0
         X2              =   6780
         Y1              =   30
         Y2              =   30
      End
      Begin VB.Label lblValCant 
         Caption         =   "Valor:"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   510
         TabIndex        =   16
         Top             =   420
         Width           =   765
      End
      Begin VB.Label lblCant 
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   90
         TabIndex        =   15
         Top             =   150
         Width           =   1275
      End
   End
   Begin VB.PictureBox picUni 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   0
      ScaleHeight     =   1215
      ScaleWidth      =   6885
      TabIndex        =   6
      Top             =   360
      Width           =   6885
      Begin VB.OptionButton opUni 
         Caption         =   "Valor"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   195
         TabIndex        =   8
         Top             =   420
         Width           =   975
      End
      Begin VB.OptionButton opUniDef 
         Caption         =   "Introducir la unidad por defecto"
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   195
         TabIndex        =   7
         Top             =   855
         Width           =   5000
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcUnidadCod 
         Height          =   285
         Left            =   1455
         TabIndex        =   9
         Top             =   390
         Width           =   1125
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1984
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcUnidadDen 
         Height          =   285
         Left            =   2595
         TabIndex        =   10
         Top             =   390
         Width           =   3705
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6985
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2540
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6535
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00808080&
         BorderWidth     =   2
         X1              =   0
         X2              =   6780
         Y1              =   30
         Y2              =   30
      End
      Begin VB.Label lblUni 
         BackStyle       =   0  'Transparent
         Caption         =   "Unidad"
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   90
         TabIndex        =   11
         Top             =   75
         Width           =   1275
      End
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   6885
      TabIndex        =   5
      Top             =   7800
      Width           =   6885
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   1050
         TabIndex        =   1
         Top             =   100
         Width           =   1095
      End
      Begin VB.CommandButton cmdContinuar 
         Caption         =   "           >>"
         Default         =   -1  'True
         Height          =   315
         Left            =   3465
         TabIndex        =   3
         Top             =   100
         Width           =   1095
      End
      Begin VB.CommandButton cmdFinalizar 
         Caption         =   "&Finalizar"
         Height          =   315
         Left            =   4680
         TabIndex        =   4
         Top             =   100
         Width           =   1095
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "<<             "
         Height          =   315
         Left            =   2250
         TabIndex        =   2
         Top             =   100
         Width           =   1095
      End
   End
   Begin VB.Label lblSel 
      Caption         =   "Introduzca los valores para completar los items"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   90
      TabIndex        =   0
      Top             =   45
      Width           =   5535
   End
End
Attribute VB_Name = "frmItemsWizard2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bDestRespetarCombo As Boolean
Private m_bDestCargarComboDesde As Boolean
Private m_bPagoRespetarCombo As Boolean
Private m_bPagoCargarComboDesde As Boolean
Private m_bProveRespetarCombo As Boolean
Private m_bUniRespetarCombo As Boolean
Private m_bUniCargarComboDesde As Boolean

Private m_oDestinos As CDestinos
Private m_oPagos As CPagos
Private m_oProves As CProveedores
Private m_oUnidades As CUnidades

Private m_sIdiPres As String
Private m_sIdiCant As String
Private m_sIdiDestino As String
Private m_sIdiPago As String
Private m_sIdiFecIni As String
Private m_sIdiFecFin As String
Private m_sIdiUni As String
Private m_sPresUniItem As String

Public g_bRDest As Boolean
Private m_bRDestPerf As Boolean
Private m_lIdPerfil As Long

Private m_bCancelar As Boolean

Private m_vValores() As Variant
Private m_vValoresEsc() As Variant
'Obviar el Load si volvemos desde el Wizard3 o 4
Private m_bRetorno As Boolean

Public bRestProvMatComp As Boolean
Public bPermProcMultiMaterial As Boolean
Private g_ogrupo As CGrupo
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String
''' <summary>Carga los textos de los controles en el idioma de la instalaci�n</summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0</remarks>
''' <revison>LTG 03/02/2012</revison>

Private Sub CargarRecursos()
    Dim Adores As Ador.Recordset

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_ITEMS_WIZARD2, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Adores Is Nothing Then
        Me.caption = Adores(0).Value
        Adores.MoveNext
        lblSel.caption = Adores(0).Value
        Adores.MoveNext
        lblUni.caption = Adores(0).Value & ":"
        m_sIdiUni = Adores(0).Value
        Adores.MoveNext
        opUni.caption = Adores(0).Value & ":"
        lblValCant.caption = Adores(0).Value & ":"
        lblValPres.caption = Adores(0).Value & ":"
        Adores.MoveNext
        opUniDef.caption = Adores(0).Value
        Adores.MoveNext
        lblCant.caption = Adores(0).Value & ":"
        m_sIdiCant = Adores(0).Value
        Adores.MoveNext
        chkCant.caption = Adores(0).Value
        Adores.MoveNext
        lblPres.caption = Adores(0).Value & ":"
        m_sIdiPres = Adores(0).Value
        Adores.MoveNext
        chkPres.caption = Adores(0).Value
        Adores.MoveNext
        lblDest.caption = Adores(0).Value & ":"
        m_sIdiDestino = Adores(0).Value
        Adores.MoveNext
        lblPago.caption = Adores(0).Value & ":"
        m_sIdiPago = Adores(0).Value
        Adores.MoveNext
        lblProve.caption = Adores(0).Value & ":"
        Adores.MoveNext
        lblFecIni.caption = Adores(0).Value & ":"
        m_sIdiFecIni = Adores(0).Value
        Adores.MoveNext
        lblFecFin.caption = Adores(0).Value & ":"
        m_sIdiFecFin = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("COD").caption = Adores(0).Value
        sdbcDestDen.Columns("COD").caption = Adores(0).Value
        sdbcPagoCod.Columns("COD").caption = Adores(0).Value
        sdbcPagoDen.Columns("COD").caption = Adores(0).Value
        sdbcProveCod.Columns("COD").caption = Adores(0).Value
        sdbcProveDen.Columns("COD").caption = Adores(0).Value
        sdbcUnidadCod.Columns("COD").caption = Adores(0).Value
        sdbcUnidadDen.Columns("COD").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("DEN").caption = Adores(0).Value
        sdbcDestDen.Columns("DEN").caption = Adores(0).Value
        sdbcPagoCod.Columns("DEN").caption = Adores(0).Value
        sdbcPagoDen.Columns("DEN").caption = Adores(0).Value
        sdbcProveCod.Columns("DEN").caption = Adores(0).Value
        sdbcProveDen.Columns("DEN").caption = Adores(0).Value
        sdbcUnidadCod.Columns("DEN").caption = Adores(0).Value
        sdbcUnidadDen.Columns("DEN").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("DIR").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("POB").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("CP").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("PAIS").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("PROVI").caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.MoveNext
        cmdFinalizar.caption = Adores(0).Value
        Adores.MoveNext
        chkProve.caption = Adores(0).Value
        Adores.MoveNext
        lblLitSolicit.caption = Adores(0).Value & ":"
        Adores.MoveNext
        chkPresPlanif.caption = Adores(0).Value
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("INI").caption = Adores(0).Value
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("FIN").caption = Adores(0).Value
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("DIR").caption = Adores(0).Value
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("PREC").caption = Adores(0).Value
        Adores.MoveNext
        m_sPresUniItem = Adores(0).Value
        lblPresUniItem.caption = Adores(0).Value & ":"
        Adores.MoveNext
        LabelPresUni.caption = Adores(0).Value & ":"
        Adores.Close
        
    End If
    
    Set Adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "CargarRecursos", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Sub chkPres_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPres.Value = 1 Then
        chkPresPlanif.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "chkPres_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub chkPresPlanif_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPresPlanif.Value = 1 Then
        chkPres.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "chkPresPlanif_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub cmdBorrarSolicit_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblSolicitud.caption = ""
    lblSolicitud.Tag = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "cmdBorrarSolicit_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdBuscarSolicit_Click()
    'LLama al formulario de b�squeda de solicitudes
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSolicitudBuscar.g_sOrigen = "frmItemsWizard2"
    frmSolicitudBuscar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "cmdBuscarSolicit_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecFin_Click()
    AbrirFormCalendar Me, txtFecFin, "FIN"
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecIni_Click()
    AbrirFormCalendar Me, txtFecIni, "INI"
End Sub

Public Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCancelar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Comprobar q los datos son correctos (cant 0 admisible) y lanzar el wizard de presupuestos.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub cmdContinuar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If opUniDef.Value = False Then
        If sdbcUnidadCod.Value = "" Then
            oMensajes.NoValido m_sIdiUni
            Exit Sub
        End If
    End If
    
    If Not IsNumeric(txtCant.Text) Then
        If txtCant.Text = "" And frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
            'en modo escalados se permite valor en blanco
        Else
            oMensajes.NoValido m_sIdiCant
            Exit Sub
        End If
    End If

    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        'Con escalados
        If Not ComprobarEscaladosFormulario() Then
            Exit Sub
        End If
        
    Else
        'Sin escalados
        If txtPres.Text = "" Or Not IsNumeric(txtPres.Text) Then
            oMensajes.NoValido m_sIdiPres
            Exit Sub
        End If
    End If


    If sdbcDestCod.Visible Then
        If sdbcDestCod.Value = "" Then
            oMensajes.NoValido m_sIdiDestino
            Exit Sub
        End If
    End If
    
    If sdbcPagoCod.Visible Then
        If sdbcPagoCod.Value = "" Then
            oMensajes.NoValido m_sIdiPago
            Exit Sub
        End If
    End If
    
    If txtFecIni.Visible Then
        If txtFecIni.Text = "" Or Not IsDate(txtFecIni.Text) Then
            oMensajes.NoValido m_sIdiFecIni
            Exit Sub
        End If
    End If
    
    If txtFecFin.Visible Then
        If txtFecFin.Text = "" Or Not IsDate(txtFecFin.Text) Then
            oMensajes.NoValido m_sIdiFecFin
            Exit Sub
        End If
    End If
    
    If txtFecIni.Visible And txtFecFin.Visible Then
        If CDate(txtFecIni.Text) > CDate(txtFecFin.Text) Then
            oMensajes.FechaDesdeMayorFechaHasta 2
            Exit Sub
        End If
        
        If frmPROCE.txtFecNec.Text <> "" Then
            If ((frmPROCE.g_oProcesoSeleccionado.FechaNecModificadaPorUsu = False) And (CDate(txtFecIni.Text) < CDate(frmPROCE.txtFecNec.Text))) Then
                frmPROCE.txtFecNec.Text = txtFecIni.Text
            ElseIf ((frmPROCE.g_oProcesoSeleccionado.FechaNecModificadaPorUsu = True) And (CDate(txtFecIni.Text) < CDate(frmPROCE.txtFecNec.Text))) Then
                oMensajes.FechaMenorQueNecesidad frmPROCE.m_sIdiFecNec, frmPROCE.txtFecNec.Text
                Exit Sub
            End If
        End If
    End If
    
    ReDim m_vValores(17)
    m_vValores(0) = sdbcUnidadCod.Value
    m_vValores(1) = sdbcUnidadDen.Value
    m_vValores(2) = opUniDef.Value
    m_vValores(3) = txtCant.Text
    m_vValores(4) = chkCant.Value
    m_vValores(5) = txtPres.Text
    m_vValores(6) = chkPres.Value
    m_vValores(7) = sdbcDestCod.Value
    m_vValores(8) = sdbcDestDen.Value
    m_vValores(9) = sdbcPagoCod.Value
    m_vValores(10) = sdbcPagoDen.Value
    m_vValores(11) = sdbcProveCod.Value
    m_vValores(12) = sdbcProveDen.Value
    m_vValores(13) = txtFecIni.Text
    m_vValores(14) = txtFecFin.Text
    m_vValores(15) = chkProve.Value
    m_vValores(16) = lblSolicitud.caption
    m_vValores(17) = chkPresPlanif.Value
    
    frmItemsWizard.g_vValores = m_vValores
    
    
    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        ReDim m_vValoresEsc(frmPROCE.g_oGrupoSeleccionado.Escalados.Count)
        Dim bm As Variant 'Bookmark
        Dim i As Long
        For i = 0 To SSDBGridEscalados.Rows - 1
        
            bm = SSDBGridEscalados.AddItemBookmark(i)
            m_vValoresEsc(i) = SSDBGridEscalados.Columns("PRES").CellValue(bm)
        Next i
        frmItemsWizard.g_vValoresEsc = m_vValoresEsc
        
    End If
    
    

    SetItemProperties
    
    m_bCancelar = False
    Unload Me
    
    If frmPROCE.sdbgItems.Columns("DIST").Visible Then
        If Not frmItemsWizard.g_oDistsNivel1 Is Nothing Or Not frmItemsWizard.g_oDistsNivel2 Is Nothing Or Not frmItemsWizard.g_oDistsNivel3 Is Nothing Then 'hide wizard1
            frmItemsWizard3.MostrarDistribSeleccionada
        End If
        frmItemsWizard3.Show 1
    Else
        If gParametrosGenerales.gbUsarPres1 And ((frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1)) Then
            frmItemsWizard4.g_iTipoPres = 1
            frmItemsWizard4.Show 1
        Else
            If gParametrosGenerales.gbUsarPres2 And ((frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2)) Then
                frmItemsWizard4.g_iTipoPres = 2
                frmItemsWizard4.Show 1
            Else
                If gParametrosGenerales.gbUsarPres3 And ((frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo1)) Then
                    frmItemsWizard4.g_iTipoPres = 3
                    frmItemsWizard4.Show 1
                Else
                    If gParametrosGenerales.gbUsarPres4 And ((frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo2)) Then
                        frmItemsWizard4.g_iTipoPres = 4
                        frmItemsWizard4.Show 1
                    End If
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "cmdContinuar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Configura la pantalla segun los parametros y configuraci�n del proceso
''' </summary>
''' <remarks>Llamada desde: load; Tiempo m�ximo</remarks>
''' <revision>LTG 03/02/2012</revision>

Private Sub ConfigurarControles()
    Dim bOcultaUnidad As Boolean
    Dim iSeparador As Integer
    Dim iVSep As Integer
    Dim bOtrosVisible As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iSeparador = 180
    iVSep = 75
    
    ' si se obliga a usar la unidad de medida, no aparecen los controles para las unidades
    If gParametrosGenerales.gbOBLUnidadMedida Then
        picUni.Visible = False
        opUniDef.Value = True ' se toma como si se sustituyeran por la unidad de medida
        bOcultaUnidad = True
    ElseIf frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        picUni.Visible = False
        opUni.Value = True ' se toma como si se sustituyeran por la unidad de medida del combo
        bOcultaUnidad = True
    End If
    
    If Not frmPROCE.sdbgItems.Columns("DEST").Visible Then
        lblDest.Visible = False
        sdbcDestCod.Visible = False
        sdbcDestDen.Visible = False
    End If
    
    If Not frmPROCE.sdbgItems.Columns("PAG").Visible Then
        lblPago.Visible = False
        sdbcPagoCod.Visible = False
        sdbcPagoDen.Visible = False
    End If

    If Not frmPROCE.sdbgItems.Columns("PROVE").Visible Then
        lblProve.Visible = False
        sdbcProveCod.Visible = False
        sdbcProveDen.Visible = False
        chkProve.Visible = False
    End If

    If Not frmPROCE.sdbgItems.Columns("INI").Visible Then
        lblFecIni.Visible = False
        lblFecFin.Visible = False
        cmdCalFecIni.Visible = False
        cmdCalFecFin.Visible = False
        txtFecIni.Visible = False
        txtFecFin.Visible = False
    End If
    
    If Not frmPROCE.sdbgItems.Columns("SOLICITUD").Visible Then
        lblLitSolicit.Visible = False
        lblSolicitud.Visible = False
        cmdBorrarSolicit.Visible = False
        cmdBuscarSolicit.Visible = False
    Else
        If frmPROCE.sdbgItems.Columns("SOLICITUD").Locked Then
            lblLitSolicit.Visible = False
            lblSolicitud.Visible = False
            cmdBorrarSolicit.Visible = False
            cmdBuscarSolicit.Visible = False
        End If
    End If
    
    bOtrosVisible = (lblDest.Visible Or lblPago.Visible Or lblProve.Visible Or lblFecIni.Visible Or lblLitSolicit.Visible)
    picOtros.Visible = bOtrosVisible
    
    'Escalados
    'Grid de escalados
    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        CargarEscalados

        'Me.Width = 5670
        Dim nFilas As Long
        nFilas = Me.SSDBGridEscalados.Rows
        If nFilas > 4 Then nFilas = 4
        Me.SSDBGridEscalados.Height = 290 + (nFilas * Me.SSDBGridEscalados.RowHeight)
    
        'Ajustar las columnas del grid
        Dim Scroll As Long
            Scroll = 200
        Dim Marcadores As Long
            Marcadores = 380
        
        If frmPROCE.g_oGrupoSeleccionado.TipoEscalados = ModoRangos Then
            Me.SSDBGridEscalados.Columns("INI").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.3
            Me.SSDBGridEscalados.Columns("FIN").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.3
            Me.SSDBGridEscalados.Columns("PRES").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.4
        Else
            Me.SSDBGridEscalados.Columns("DIR").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
            Me.SSDBGridEscalados.Columns("PRES").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
        End If
         
        'Calculamos las  dimensiones del form
        chkPres.Top = chkPresPlanif.Top
        chkPres.Left = Me.SSDBGridEscalados.Left
        chkPres.Width = 3000
        chkPresPlanif.Height = chkPres.Height
        chkPresPlanif.Width = 3000
        chkPresPlanif.Left = chkPresPlanif.Left + 1000
        chkPresPlanif.Value = vbUnchecked
        chkPres.Value = vbUnchecked
        
        lblValPres.Visible = False
        txtPres.Visible = False
        
        lblPresUniItem.Visible = True
        txtPresUniItem.Visible = True
        lblPresUniItem.Left = chkPres.Left
        lblPresUniItem.Top = chkPres.Top + chkPres.Height + iSeparador
        txtPresUniItem.Top = lblPresUniItem.Top
        txtPresUniItem.Left = lblPresUniItem.Left + lblPresUniItem.Width + iSeparador
        
        LabelPresUni.Visible = True
        LabelPresUni.Top = lblPresUniItem.Top + lblPresUniItem.Height + iSeparador
        
        SSDBGridEscalados.Visible = True
        SSDBGridEscalados.Top = LabelPresUni.Top + LabelPresUni.Height + (iSeparador / 2)
        
        picPres.Height = SSDBGridEscalados.Top + SSDBGridEscalados.Height + iVSep
    Else
        picPres.Height = chkPresPlanif.Top + chkPresPlanif.Height + iVSep
    End If
    
        
    If Not frmPROCE.sdbgItems.Columns("DEST").Visible And Not frmPROCE.sdbgItems.Columns("PAG").Visible _
    And Not frmPROCE.sdbgItems.Columns("PROVE").Visible And Not frmPROCE.sdbgItems.Columns("INI").Visible _
    And (Not frmPROCE.sdbgItems.Columns("SOLICITUD").Visible Or frmPROCE.sdbgItems.Columns("SOLICITUD").Locked) _
    And bOcultaUnidad Then
        picCantidad.Top = picUni.Top
        picPres.Top = picCantidad.Top + picCantidad.Height
        picNavigate.Top = picPres.Top + picPres.Height
    Else
        If bOcultaUnidad Then ' no se ve la unidad de medida
            picCantidad.Top = picUni.Top
        End If
            
        picPres.Top = picCantidad.Top + picCantidad.Height
        picOtros.Top = picPres.Top + picPres.Height
        picNavigate.Top = picOtros.Top + picOtros.Height

        If bOtrosVisible Then
            If Not frmPROCE.sdbgItems.Columns("DEST").Visible Then
                lblLitSolicit.Top = lblFecIni.Top
                lblSolicitud.Top = txtFecIni.Top
                cmdBorrarSolicit.Top = txtFecIni.Top
                cmdBuscarSolicit.Top = txtFecIni.Top
                
                lblFecIni.Top = chkProve.Top + 50
                lblFecFin.Top = lblFecIni.Top
                txtFecIni.Top = lblFecIni.Top - 45
                txtFecFin.Top = lblFecFin.Top - 45
                cmdCalFecIni.Top = txtFecIni.Top - 15
                cmdCalFecFin.Top = txtFecIni.Top - 15
                
                lblProve.Top = lblPago.Top
                sdbcProveCod.Top = sdbcPagoCod.Top
                sdbcProveDen.Top = sdbcPagoDen.Top
                chkProve.Top = sdbcProveCod.Top + 280
                
                lblPago.Top = lblDest.Top
                sdbcPagoCod.Top = sdbcDestCod.Top
                sdbcPagoDen.Top = sdbcDestCod.Top
            End If
        
            If Not frmPROCE.sdbgItems.Columns("PAG").Visible Then
                lblLitSolicit.Top = lblFecIni.Top
                lblSolicitud.Top = txtFecIni.Top
                cmdBorrarSolicit.Top = txtFecIni.Top
                cmdBuscarSolicit.Top = txtFecIni.Top
                
                lblFecIni.Top = chkProve.Top + 50
                lblFecFin.Top = lblFecIni.Top
                txtFecIni.Top = lblFecIni.Top - 45
                txtFecFin.Top = lblFecFin.Top - 45
                cmdCalFecIni.Top = txtFecIni.Top - 15
                cmdCalFecFin.Top = txtFecIni.Top - 15
                
                lblProve.Top = lblPago.Top
                sdbcProveCod.Top = sdbcPagoCod.Top
                sdbcProveDen.Top = sdbcPagoDen.Top
                chkProve.Top = sdbcProveCod.Top + 280
            End If
            
            If Not frmPROCE.sdbgItems.Columns("PROVE").Visible Then
                lblLitSolicit.Top = chkProve.Top
                lblSolicitud.Top = chkProve.Top
                cmdBorrarSolicit.Top = chkProve.Top
                cmdBuscarSolicit.Top = chkProve.Top
                
                lblFecIni.Top = lblProve.Top + 50
                lblFecFin.Top = lblProve.Top
                txtFecIni.Top = lblProve.Top - 45
                txtFecFin.Top = lblProve.Top - 45
                cmdCalFecIni.Top = lblProve.Top - 15
                cmdCalFecFin.Top = lblProve.Top - 15
            End If
            
            If Not frmPROCE.sdbgItems.Columns("INI").Visible Then
                lblLitSolicit.Top = lblFecIni.Top
                lblSolicitud.Top = txtFecIni.Top
                cmdBorrarSolicit.Top = txtFecIni.Top
                cmdBuscarSolicit.Top = txtFecIni.Top
            End If
            
            picOtros.Height = lblSolicitud.Top + lblSolicitud.Height + iVSep
                
            If Not frmPROCE.sdbgItems.Columns("SOLICITUD").Visible Or frmPROCE.sdbgItems.Columns("SOLICITUD").Locked Then
                picOtros.Height = picOtros.Height - lblSolicitud.Height
            Else
                picOtros.Height = lblSolicitud.Top + lblSolicitud.Height
            End If
            
            picNavigate.Top = picOtros.Top + picOtros.Height
        Else
            picNavigate.Top = picOtros.Top
        End If
        
        If Not frmPROCE.sdbgItems.Columns("DEST").Visible And Not frmPROCE.sdbgItems.Columns("PAG").Visible _
            And Not frmPROCE.sdbgItems.Columns("PROVE").Visible And Not frmPROCE.sdbgItems.Columns("INI").Visible _
            And (Not frmPROCE.sdbgItems.Columns("SOLICITUD").Visible Or frmPROCE.sdbgItems.Columns("SOLICITUD").Locked) Then
            Line4.Visible = False
        End If
    End If
    
    
    'Si no est�n definidos ni los presupuestos ni la distribuci�n a nivel de �tem (o lo est�n pero no se usan los presup)
    If Not (frmPROCE.sdbgItems.Columns("DIST").Visible Or frmPROCE.sdbgItems.Columns("PRESUP").Visible) Then
        cmdContinuar.Visible = False
        cmdFinalizar.Left = cmdContinuar.Left
        cmdFinalizar.Default = True
    Else
        'Si la distribuci�n o alg�n presupuesto est� a nivel de �tem y es obligatorio (o a nivel de grupo pero no para el grupo del �tem)
        If frmPROCE.sdbgItems.Columns("DIST").Visible Then
            cmdFinalizar.Visible = False
        Else
            If (gParametrosGenerales.gbUsarPres1 And (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1)) And (gParametrosGenerales.gbOBLPP)) Then
                cmdFinalizar.Visible = False
            Else
                If (gParametrosGenerales.gbUsarPres2 And (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2)) And (gParametrosGenerales.gbOBLPC)) Then
                    cmdFinalizar.Visible = False
                Else
                    If (gParametrosGenerales.gbUsarPres3 And (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo1)) And (gParametrosGenerales.gbOBLPres3)) Then
                        cmdFinalizar.Visible = False
                    Else
                        If (gParametrosGenerales.gbUsarPres4 And (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo2)) And (gParametrosGenerales.gbOBLPres4)) Then
                            cmdFinalizar.Visible = False
                        End If
                    End If
                End If
            End If
        End If
    End If

    If gParametrosInstalacion.gbPresupuestoPlanificado = True Then
        chkPresPlanif.Value = 1
    Else
        chkPres.Value = 1
    End If
    
    Me.Height = picNavigate.Top + picNavigate.Height + 400
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "ConfigurarControles", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Comprobar q los datos son correctos (cant 0 admisible) y grabar en el proceso si todo es correcto.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo</remarks>
''' <revision>LTG 03/02/2012</revision>

Private Sub cmdFinalizar_Click()
    Dim teserror As TipoErrorSummit
    Dim bItems As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.SSDBGridEscalados.Update
    If frmItemsWizard.g_oItems Is Nothing Then Exit Sub
    
    If opUniDef.Value = False Then
        If sdbcUnidadCod.Value = "" Then
            oMensajes.NoValido m_sIdiUni
            Exit Sub
        End If
    End If
    
    If Not IsNumeric(txtCant.Text) Then
        If txtCant.Text = "" And frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
            'en modo escalados se permite valor en blanco
        Else
            oMensajes.NoValido m_sIdiCant
            Exit Sub
        End If
    End If
        
    If txtPresUniItem.Text <> "" Then
        If Not IsNumeric(txtPresUniItem.Text) Then
            oMensajes.NoValido m_sPresUniItem
            Exit Sub
        End If
    End If

    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        'Con escalados
        If Not ComprobarEscaladosFormulario() Then
            Exit Sub
        End If
        
    Else
        'Sin escalados
        If txtPres.Text = "" Or Not IsNumeric(txtPres.Text) Then
            oMensajes.NoValido m_sIdiPres
            Exit Sub
        End If
    End If

    If sdbcDestCod.Visible Then
        If sdbcDestCod.Value = "" Then
            oMensajes.NoValido m_sIdiDestino
            Exit Sub
        End If
    End If
    
    If sdbcPagoCod.Visible Then
        If sdbcPagoCod.Value = "" Then
            oMensajes.NoValido m_sIdiPago
            Exit Sub
        End If
    End If
    
    If txtFecIni.Visible Then
        If txtFecIni.Text = "" Or Not IsDate(txtFecIni.Text) Then
            oMensajes.NoValido m_sIdiFecIni
            Exit Sub
        End If
    End If
    
    If txtFecFin.Visible Then
        If txtFecFin.Text = "" Or Not IsDate(txtFecFin.Text) Then
            oMensajes.NoValido m_sIdiFecFin
            Exit Sub
        End If
    End If
    
    If txtFecIni.Visible And txtFecFin.Visible Then
        If CDate(txtFecIni.Text) > CDate(txtFecFin.Text) Then
            oMensajes.FechaDesdeMayorFechaHasta 2
            Exit Sub
        End If
    
        If frmPROCE.txtFecNec.Text <> "" Then
            If ((frmPROCE.g_oProcesoSeleccionado.FechaNecModificadaPorUsu = False) And (CDate(txtFecIni.Text) < CDate(frmPROCE.txtFecNec.Text))) Then
                frmPROCE.txtFecNec.Text = txtFecIni.Text
            ElseIf ((frmPROCE.g_oProcesoSeleccionado.FechaNecModificadaPorUsu = True) And (CDate(txtFecIni.Text) < CDate(frmPROCE.txtFecNec.Text))) Then
                oMensajes.FechaMenorQueNecesidad frmPROCE.m_sIdiFecNec, frmPROCE.txtFecNec.Text
                Exit Sub
            End If
        End If
    End If
    
    SetItemProperties
            
    If Not gParametrosGenerales.gbPresupuestosAut Then
        'Si alguno de los presupuestos est� en proce o grupo reasignar presupuestos
        '**PRESANU1
        If Not frmItemsWizard.g_bPresAnu1OK Then
            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso Then 'PRESANU1 en PROCE
                If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 Then
                    frmItemsWizard.ReasignarPresup 1, True
                End If
            Else
                If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo Then 'PRESANU1 en GRUPO
                    If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 Then
                        frmItemsWizard.ReasignarPresup 1, False
                    End If
                End If
            End If
         End If
        '**PRESANU2
        If Not frmItemsWizard.g_bPresAnu2OK Then
            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso Then 'PRESANU2 en PROCE
                If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                    frmItemsWizard.ReasignarPresup 2, True
                End If
            Else
                If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo Then 'PRESANU2 en GRUPO
                    If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 Then
                        frmItemsWizard.ReasignarPresup 2, False
                    End If
                End If
            End If
        End If
        '***PRES1
        If Not frmItemsWizard.g_bPres1OK Then
            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Then 'PRES1 en PROCE
                If frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                    frmItemsWizard.ReasignarPresup 3, True
                End If
            Else
                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then 'PRES1 en GRUPO
                    If frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                        frmItemsWizard.ReasignarPresup 3, False
                    End If
                End If
            End If
        End If
        '***PRES2
        If Not frmItemsWizard.g_bPres2OK Then
            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Then 'PRES1 en PROCE
                If frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                    frmItemsWizard.ReasignarPresup 4, True
                End If
            Else
                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo Then 'PRES1 en GRUPO
                    If frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 Then
                        frmItemsWizard.ReasignarPresup 4, False
                    End If
                End If
            End If
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    If Not IsEmpty(frmPROCE.g_oProcesoSeleccionado.Plantilla) And Not IsNull(frmPROCE.g_oProcesoSeleccionado.Plantilla) Then
        If frmPROCE.g_oProcesoSeleccionado.DefEspItems Then
            frmPROCE.m_bAtribItemPlant = True
        End If
    End If
    
    teserror = frmItemsWizard.AnyadirMultiplesItems(chkCant.Value, chkPres.Value, frmItemsWizard.HayUON, frmItemsWizard.HayPresup, chkProve.Value, chkPresPlanif.Value)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        
        If teserror.NumError = TESVolumenAdjDirSuperado Then
            If frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then basErrores.TratarError teserror
            If Not frmPROCE.g_oProcesoSeleccionado.PermSaltarseVolMaxAdjDir Then
                m_bCancelar = True
                Unload Me
                Exit Sub
            End If
        Else
            basErrores.TratarError teserror
            m_bCancelar = True
            Unload Me
            Exit Sub
        End If
    End If
    
    frmItemsWizard.AnyadirItemsAGridApertura
    
    If frmItemsWizard.g_oItems.Count > 1 Then bItems = True
    
    
    'DPD -> Escalados
    'Comprobar la colecci�n de items que no se han podido a�adir y mostrar mensaje
    'Este caso s�lo ocurre cuando hay escalados y se intenta agregar art�culos con unidades distintas a la del escalado
    ' y el par�maetro de obligar a utilizar la unidad del art�culo nos impide cambiar la unidad.
    ' Por lo tanto puede haber art�culos que no se puedan a�adir
    
    Dim noInsItem As Boolean
    
    If Not frmItemsWizard.g_oItemsNoValidos Is Nothing Then
        If frmItemsWizard.g_oItemsNoValidos.Count > 0 Then
            noInsItem = True
        End If
    End If
    
    
    m_bCancelar = True
    Unload Me
    
    If noInsItem Then
        
        oMensajes.ItemsNoAnyadidosAProce frmItemsWizard.g_oUniNoValidos, frmItemsWizard.g_oGrupoSeleccionado.UnidadEscalado
    Else
        oMensajes.ItemsAnyadidosAProceOK bItems
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "cmdFinalizar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
        
End Sub

''' <summary> establece las propiedades de los nuevos items </summary>
''' <remarks>Llamada desde: cmdContinuar_Click y cmdFinalizar_Click; Tiempo m�ximo</remarks>
''' <revision>LTG 03/02/2012</revision>

Private Sub SetItemProperties()
    Dim i As Long
    Dim oEscalados As CEscalados
    Dim oEscalado As CEscalado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oEscalados = oFSGSRaiz.Generar_CEscalados
    
    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        'Cargamos al colecci�n de escalados
        Dim Id As Long
        Dim bm As Variant 'Bookmark
        oEscalados.modo = frmPROCE.g_oGrupoSeleccionado.TipoEscalados
        For i = 0 To SSDBGridEscalados.Rows - 1
            bm = SSDBGridEscalados.AddItemBookmark(i)
            If CStr(SSDBGridEscalados.Columns("ID").CellValue(bm)) = "" Then
                Id = 0
            Else
                Id = SSDBGridEscalados.Columns("ID").CellValue(bm)
            End If
                
            If frmPROCE.g_oGrupoSeleccionado.TipoEscalados = ModoRangos Then
                Set oEscalado = oEscalados.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(bm), SSDBGridEscalados.Columns("FIN").CellValue(bm), SSDBGridEscalados.Columns("PRES").CellValue(bm))
            Else
                Set oEscalado = oEscalados.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(bm), Null, SSDBGridEscalados.Columns("PRES").CellValue(bm))
            End If
        Next i
    End If
    
    With frmItemsWizard.g_oItems
        For i = 0 To .Count - 1
            If sdbcUnidadCod.Value <> "" Then
                .Item(CStr(i)).UniCod = sdbcUnidadCod.Value
            End If
            .Item(CStr(i)).Cantidad = txtCant.Text
            
            If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
                'Con escalados
                Set .Item(CStr(i)).Escalados = oEscalados
                If txtPresUniItem.Text <> "" Then .Item(CStr(i)).Precio = txtPresUniItem.Text
            Else
                'Sin escalados
                .Item(CStr(i)).Precio = txtPres.Text
            End If
            If IsNumeric(.Item(CStr(i)).Precio) And IsNumeric(.Item(CStr(i)).Cantidad) Then
                .Item(CStr(i)).Presupuesto = .Item(CStr(i)).Precio * .Item(CStr(i)).Cantidad
            Else
                .Item(CStr(i)).Presupuesto = Null
            End If
            
            If sdbcDestCod.Visible Then
                .Item(CStr(i)).DestCod = sdbcDestCod.Value
            End If
            If sdbcPagoCod.Visible Then
                .Item(CStr(i)).PagCod = sdbcPagoCod.Value
            End If
            If sdbcProveCod.Visible Then
                .Item(CStr(i)).ProveAct = StrToNull(sdbcProveCod.Value)
            End If
            If txtFecIni.Visible Then
                .Item(CStr(i)).FechaInicioSuministro = txtFecIni.Text
            End If
            If txtFecFin.Visible Then
                .Item(CStr(i)).FechaFinSuministro = txtFecFin.Text
            End If
            If lblSolicitud.Visible = True Then
                If lblSolicitud.caption = "" Then
                    .Item(CStr(i)).SolicitudId = Null
                Else
                    .Item(CStr(i)).SolicitudId = lblSolicitud.Tag
                End If
            End If
            
            If gParametrosGenerales.gbSubasta And frmPROCE.g_oProcesoSeleccionado.ModoSubasta Then
                'Si el proceso procede de plantilla y es de subasta
                If Not frmPROCE.m_oPlantillaSeleccionada Is Nothing Then
                    If frmPROCE.m_oPlantillaSeleccionada.ModoSubasta Then
                        If frmPROCE.m_oPlantillaSeleccionada.BajadaMinimaPujas Then
                            .Item(CStr(i)).MinPujGanador = frmPROCE.m_oPlantillaSeleccionada.SubBajMinGanItemVal
                        End If
                        If frmPROCE.m_oPlantillaSeleccionada.SubBajMinProve Then
                            .Item(CStr(i)).MinPujProve = frmPROCE.m_oPlantillaSeleccionada.SubBajMinProveItemVal
                        End If
                    End If
                Else
                    .Item(CStr(i)).MinPujGanador = gParametrosInstalacion.gCPSubBajMinGanItemVal
                    .Item(CStr(i)).MinPujProve = gParametrosInstalacion.gCPSubBajMinProveItemVal
                End If
            End If
        Next
    End With
    
    Set oEscalado = Nothing
    Set oEscalados = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "SetItemProperties", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Private Sub cmdVolver_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCancelar = False
    Unload Me
    frmItemsWizard.SeleccionarArticulos
    frmItemsWizard.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "cmdVolver_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga la pantalla. Se ocupa de cargar, ocultar y habilitar combos, de cargar el arbol de proceso, etc.
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bDescargarFrm = False
m_bActivado = False
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
    Set m_oPagos = oFSGSRaiz.generar_CPagos
    Set m_oProves = oFSGSRaiz.generar_CProveedores
    Set m_oUnidades = oFSGSRaiz.Generar_CUnidades
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If Not oUsuarioSummit.perfil Is Nothing Then m_lIdPerfil = oUsuarioSummit.perfil.Id
    m_bRDestPerf = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDestPerf)) Is Nothing)
    
    ConfigurarControles
    m_bCancelar = True
    
    If Not m_bRetorno Then
        If Not gParametrosGenerales.gbOBLUnidadMedida Then
            If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
                'Unidad del escalado
                sdbcUnidadCod.Value = frmPROCE.g_oGrupoSeleccionado.UnidadEscalado
                sdbcUnidadCod.Enabled = False
                sdbcUnidadDen.Enabled = False
                opUniDef.Enabled = False
            Else
                sdbcUnidadCod.Value = gParametrosGenerales.gsUNIDEF
            End If
            sdbcUnidadCod_Validate False
            
        End If
        If frmPROCE.sdbgItems.Columns("DEST").Visible Then
            sdbcDestCod.Value = gParametrosInstalacion.gsDestino
            sdbcDestCod_Validate False
        End If
        If frmPROCE.sdbgItems.Columns("PAG").Visible Then
            sdbcPagoCod.Value = gParametrosInstalacion.gsFormaPago
            sdbcPagoCod_Validate False
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oDestinos = Nothing
    Set m_oPagos = Nothing
    Set m_oProves = Nothing
    Set m_oUnidades = Nothing
    m_bRetorno = False

    If m_bCancelar Then
        frmItemsWizard.g_bCancelar = m_bCancelar
        Unload frmItemsWizard
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub opUni_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        sdbcUnidadCod.Enabled = False
        sdbcUnidadDen.Enabled = False
    Else
        sdbcUnidadCod.Enabled = True
        sdbcUnidadDen.Enabled = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "opUni_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub opUniDef_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadCod.Value = ""
    sdbcUnidadDen.Value = ""
    sdbcUnidadCod.Enabled = False
    sdbcUnidadDen.Enabled = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "opUniDef_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcDestCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcDestCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestCod.DroppedDown Then
        sdbcDestCod.Value = ""
        sdbcDestDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcDestCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCod.Value = "..." Then
        sdbcDestCod.Text = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
    sdbcDestCod.Text = sdbcDestCod.Columns(0).Text
    m_bDestRespetarCombo = False
    
    m_bDestCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Cargar el combo Destinos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcDestCod_DropDown()
    Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcDestCod.RemoveAll
    
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , , g_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , , g_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , g_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , g_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    End If
    
    If ADORs Is Nothing Then
        sdbcDestCod.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestCod.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestCod.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestCod.Rows = 0 Then
        sdbcDestCod.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcDestCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestCod.DataFieldList = "Column 0"
    sdbcDestCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcDestCod_PositionList(ByVal Text As String)
PositionList sdbcDestCod, Text
End Sub

''' <summary>
''' Validar lo q se ha puesto en el combo
''' </summary>
''' <param name="Cancel">Cancela pq no es valido</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcDestCod_Validate(Cancel As Boolean)
    Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCod.Text = "" Then Exit Sub

    If sdbcDestCod.Text = sdbcDestCod.Columns(0).Text Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
        m_bDestRespetarCombo = False
        Exit Sub
    End If

    If sdbcDestCod.Text = sdbcDestDen.Columns(1).Text Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
        m_bDestRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    ''' Solo continuamos si existe el destino
    If Not oUsuarioSummit.Persona Is Nothing Then
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , True, g_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
    Else
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , True, g_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
    End If
    
    If ADORs Is Nothing Then
        sdbcDestCod.Text = ""
        Screen.MousePointer = vbNormal
    ElseIf ADORs.EOF Then
        sdbcDestCod.Value = ""
        Screen.MousePointer = vbNormal
        ADORs.Close
        Set ADORs = Nothing
    Else
        m_bDestRespetarCombo = True
        sdbcDestDen.Value = ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value

        sdbcDestCod.Columns(0).Text = sdbcDestCod.Text
        sdbcDestCod.Columns(1).Text = sdbcDestDen.Text

        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = False
        ADORs.Close
        Set ADORs = Nothing
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcDestDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestCod.Text = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcDestDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestDen.DroppedDown Then
        sdbcDestDen.Value = ""
        sdbcDestCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcDestDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestDen.Value = "..." Then
        sdbcDestDen.Text = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestCod.Text = sdbcDestDen.Columns(1).Text
    sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
    m_bDestRespetarCombo = False
    
    m_bDestCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Cargar el combo Denom Destinos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcDestDen_DropDown()
Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcDestDen.RemoveAll
    
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestDen.Value), , , g_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestDen.Value), , , g_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , g_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , , m_bRDestPerf, m_lIdPerfil)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , g_bRDest, , , , , , , m_bRDestPerf, m_lIdPerfil)
        End If
    End If
    
    If ADORs Is Nothing Then
        sdbcDestDen.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestDen.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestDen.AddItem ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestDen.Rows = 0 Then
        sdbcDestDen.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcDestDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestDen.DataFieldList = "Column 0"
    sdbcDestDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcDestDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcDestDen_PositionList(ByVal Text As String)
PositionList sdbcDestDen, Text
End Sub

Private Sub sdbcPagoCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Text = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcPagoCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcPagoCod.DroppedDown Then
        sdbcPagoCod.Value = ""
        sdbcPagoDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcPagoCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPagoCod.Value = "..." Then
        sdbcPagoCod.Text = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoDen.Text = sdbcPagoCod.Columns(1).Text
    sdbcPagoCod.Text = sdbcPagoCod.Columns(0).Text
    m_bPagoRespetarCombo = False
    
    m_bPagoCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcPagoCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Long
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcPagoCod.RemoveAll
    
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbcPagoCod.Value), , , False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
    End If
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoCod.AddItem "..."
    End If

    sdbcPagoCod.SelStart = 0
    sdbcPagoCod.SelLength = Len(sdbcPagoCod.Text)
    sdbcPagoCod.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcPagoCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPagoCod.DataFieldList = "Column 0"
    sdbcPagoCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcPagoCod_PositionList(ByVal Text As String)
PositionList sdbcPagoCod, Text
End Sub

Private Sub sdbcPagoCod_Validate(Cancel As Boolean)
Dim oPagos As CPagos
Dim bExiste As Boolean
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oPagos = oFSGSRaiz.generar_CPagos
    If sdbcPagoCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    oPagos.CargarTodosLosPagos sdbcPagoCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPagos.Count = 0)
    
    If Not bExiste Then
        sdbcPagoCod.Text = ""
    Else
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Text = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcPagoCod.Columns(0).Value = sdbcPagoCod.Text
        sdbcPagoCod.Columns(1).Value = sdbcPagoDen.Text
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = False
    End If
    
    Set oPagos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcPagoDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoCod.Text = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcPagoDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcPagoDen.DroppedDown Then
        sdbcPagoDen.Value = ""
        sdbcPagoCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcPagoDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPagoDen.Value = "..." Then
        sdbcPagoDen.Text = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoCod.Text = sdbcPagoDen.Columns(1).Text
    sdbcPagoDen.Text = sdbcPagoDen.Columns(0).Text
    m_bPagoRespetarCombo = False
    
    m_bPagoCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcPagoDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Set m_oPagos = Nothing
    Set m_oPagos = oFSGSRaiz.generar_CPagos
     
    sdbcPagoDen.RemoveAll
    
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , CStr(sdbcPagoDen.Value), True, False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
    End If
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoDen.AddItem "..."
    End If

    sdbcPagoDen.SelStart = 0
    sdbcPagoDen.SelLength = Len(sdbcPagoDen.Text)
    sdbcPagoDen.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcPagoDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPagoDen.DataFieldList = "Column 0"
    sdbcPagoDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcPagoDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcPagoDen_PositionList(ByVal Text As String)
PositionList sdbcPagoDen, Text
End Sub

Private Sub sdbcProveCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bProveRespetarCombo Then
        m_bProveRespetarCombo = True
        sdbcProveDen.Text = ""
        m_bProveRespetarCombo = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod.Value = ""
        sdbcProveDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
      
    If sdbcProveCod.Value = "" Then Exit Sub
    
    m_bProveRespetarCombo = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    m_bProveRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll
    
    If bRestProvMatComp Then
        m_oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, Trim(sdbcProveCod.Text)
    Else
        m_oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProveCod.Text), , , , False
    End If
   
    Codigos = m_oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    '    If Not m_oProves.EOF Then
    If m_oProves.Count = gParametrosInstalacion.giCargaMaximaCombos Then
        sdbcProveCod.AddItem "..."
    End If
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcProveCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el proveedor
    If bRestProvMatComp Then
        m_oProves.BuscarProveedoresConMatDelCompDesde 1, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, Trim(sdbcProveCod.Text)
    Else
        m_oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
    End If

    bExiste = Not (m_oProves.Count = 0)
    If bExiste Then bExiste = (UCase(m_oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        m_bProveRespetarCombo = True
        sdbcProveDen.Text = m_oProves.Item(1).Den
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Text
        m_bProveRespetarCombo = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcProveDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bProveRespetarCombo Then
        m_bProveRespetarCombo = True
        sdbcProveCod.Text = ""
        m_bProveRespetarCombo = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod.Value = ""
        sdbcProveDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    m_bProveRespetarCombo = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    m_bProveRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll
    If Not bPermProcMultiMaterial Then
        If bRestProvMatComp Then
            m_oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcProveDen.Value)
        Else
            m_oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveDen.Value, , True, False
        End If
    Else
        m_oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , , , , , , , , , , , , , , , , , , frmItemsWizard.g_oGMN4Seleccionado.GMN1Cod, frmItemsWizard.g_oGMN4Seleccionado.GMN2Cod, frmItemsWizard.g_oGMN4Seleccionado.GMN3Cod, frmItemsWizard.g_oGMN4Seleccionado.Cod
    End If
    Codigos = m_oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
'    If Not m_oProves.EOF Then
    If m_oProves.Count = gParametrosInstalacion.giCargaMaximaCombos Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveCod.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcProveDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcProveDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Private Sub sdbcUnidadCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bUniRespetarCombo Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = ""
        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcUnidadCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcUnidadCod.DroppedDown Then
        sdbcUnidadCod.Value = ""
        sdbcUnidadDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcUnidadCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadCod.Value = "..." Then
        sdbcUnidadCod.Text = ""
        Exit Sub
    End If
    
    m_bUniRespetarCombo = True
    sdbcUnidadDen.Text = sdbcUnidadCod.Columns(1).Text
    sdbcUnidadCod.Text = sdbcUnidadCod.Columns(0).Text
    m_bUniRespetarCombo = False
    
    m_bUniCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcUnidadCod_DropDown()
Dim oUni As CUnidad

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadCod.RemoveAll

    Screen.MousePointer = vbHourglass
    If m_bUniCargarComboDesde Then
        m_oUnidades.CargarTodasLasUnidades CStr(sdbcUnidadCod.Value)
    Else
        m_oUnidades.CargarTodasLasUnidades
    End If

    For Each oUni In m_oUnidades
        sdbcUnidadCod.AddItem oUni.Cod & Chr(m_lSeparador) & oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    Next

    If sdbcUnidadCod.Rows = 0 Then
        sdbcUnidadCod.AddItem ""
    End If

    Set oUni = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcUnidadCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadCod.DataFieldList = "Column 0"
    sdbcUnidadCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcUnidadCod_PositionList(ByVal Text As String)
PositionList sdbcUnidadCod, Text
End Sub

Private Sub sdbcUnidadCod_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadCod.Text = "" Then Exit Sub

    If sdbcUnidadCod.Text = sdbcUnidadCod.Columns(0).Text Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = sdbcUnidadCod.Columns(1).Text
        m_bUniRespetarCombo = False
        Exit Sub
    End If

    If sdbcUnidadCod.Text = sdbcUnidadDen.Columns(1).Text Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = sdbcUnidadDen.Columns(0).Text
        m_bUniRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el destino
    m_oUnidades.CargarTodasLasUnidades CStr(sdbcUnidadCod.Value), , True
    If m_oUnidades.Count = 0 Then
        sdbcUnidadCod.Text = ""
        Screen.MousePointer = vbNormal
    Else
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = NullToStr(m_oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)

        sdbcUnidadCod.Columns(0).Text = sdbcUnidadCod.Text
        sdbcUnidadCod.Columns(1).Text = sdbcUnidadDen.Text

        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcUnidadDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bUniRespetarCombo Then
        m_bUniRespetarCombo = True
        sdbcUnidadCod.Text = ""
        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcUnidadDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcUnidadDen.DroppedDown Then
        sdbcUnidadDen.Value = ""
        sdbcUnidadCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcUnidadDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadDen.Value = "..." Then
        sdbcUnidadDen.Text = ""
        Exit Sub
    End If
    
    m_bUniRespetarCombo = True
    sdbcUnidadCod.Text = sdbcUnidadDen.Columns(1).Text
    sdbcUnidadDen.Text = sdbcUnidadDen.Columns(0).Text
    m_bUniRespetarCombo = False
    
    m_bUniCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcUnidadDen_DropDown()
Dim oUni As CUnidad
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If m_bUniCargarComboDesde Then
        m_oUnidades.CargarTodasLasUnidades , CStr(sdbcUnidadDen.Value), , True
    Else
        m_oUnidades.CargarTodasLasUnidades , , , True
    End If

    For Each oUni In m_oUnidades
        sdbcUnidadDen.AddItem oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oUni.Cod
    Next

    If sdbcUnidadDen.Rows = 0 Then
        sdbcUnidadDen.AddItem ""
    End If

    Set oUni = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcUnidadDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadDen.DataFieldList = "Column 0"
    sdbcUnidadDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "sdbcUnidadDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcUnidadDen_PositionList(ByVal Text As String)
PositionList sdbcUnidadDen, Text
End Sub

Public Sub MostrarValoresSeleccionados()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bRetorno = True
    opUniDef.Value = frmItemsWizard.g_vValores(2)
    m_bUniRespetarCombo = True
    sdbcUnidadCod.Value = frmItemsWizard.g_vValores(0)
    sdbcUnidadDen.Value = frmItemsWizard.g_vValores(1)
    m_bUniRespetarCombo = False
    txtCant.Text = frmItemsWizard.g_vValores(3)
    chkCant.Value = frmItemsWizard.g_vValores(4)
    txtPres.Text = frmItemsWizard.g_vValores(5)
    chkPres.Value = frmItemsWizard.g_vValores(6)
    m_bDestRespetarCombo = True
    sdbcDestCod.Value = frmItemsWizard.g_vValores(7)
    sdbcDestDen.Value = frmItemsWizard.g_vValores(8)
    m_bDestRespetarCombo = False
    m_bPagoRespetarCombo = True
    sdbcPagoCod.Value = frmItemsWizard.g_vValores(9)
    sdbcPagoDen.Value = frmItemsWizard.g_vValores(10)
    m_bPagoRespetarCombo = False
    m_bProveRespetarCombo = True
    sdbcProveCod.Value = frmItemsWizard.g_vValores(11)
    sdbcProveDen.Value = frmItemsWizard.g_vValores(12)
    chkProve.Value = frmItemsWizard.g_vValores(15)
    m_bProveRespetarCombo = False
    txtFecIni.Text = frmItemsWizard.g_vValores(13)
    txtFecFin.Text = frmItemsWizard.g_vValores(14)
    lblSolicitud.caption = frmItemsWizard.g_vValores(16)
    chkPresPlanif.Value = frmItemsWizard.g_vValores(17)
    
    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        If UBound(frmItemsWizard.g_vValores) > 0 Then
            Dim i As Long
            Dim bm As Variant 'Bookmark
            For i = 0 To SSDBGridEscalados.Rows - 1
                SSDBGridEscalados.Bookmark = i
                SSDBGridEscalados.Columns("PRES").Value = m_vValoresEsc(i)
            Next i
            SSDBGridEscalados.Bookmark = 0
        
        End If
    End If
    
    
    
    m_bRetorno = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "MostrarValoresSeleccionados", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Public Sub PonerSolicitudSeleccionada(ByVal oSolic As CInstancia)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblSolicitud.caption = oSolic.Id & " " & oSolic.DescrBreve
    lblSolicitud.Tag = oSolic.Id
    
    Set oSolic = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "PonerSolicitudSeleccionada", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub




'--<summary>
'--Carga los escalados en el grid
'--</summary>
'--<remarks>Llamada desde Form-Load</remarks>
'--<revision>DPD 09/08/2011</revision>
Sub CargarEscalados()


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set g_ogrupo = frmPROCE.g_oGrupoSeleccionado
g_ogrupo.CargarEscalados

If g_ogrupo.TipoEscalados = ModoRangos Then
    SSDBGridEscalados.Columns("INI").caption = SSDBGridEscalados.Columns("INI").caption & " (" & g_ogrupo.UnidadEscalado & ")"
    SSDBGridEscalados.Columns("FIN").caption = SSDBGridEscalados.Columns("FIN").caption & " (" & g_ogrupo.UnidadEscalado & ")"
Else
    SSDBGridEscalados.Columns("DIR").caption = SSDBGridEscalados.Columns("DIR").caption & " (" & g_ogrupo.UnidadEscalado & ")"
End If

If Not g_ogrupo.Escalados Is Nothing Then

    Dim cEsc As CEscalado
    Dim i As Long
    Dim Linea As String
    For Each cEsc In g_ogrupo.Escalados
    
        Linea = cEsc.Id & Chr(m_lSeparador)
    
        'Set cEsc = Me.g_oGrupoSeleccionado.Escalados.Item(i)
        Linea = Linea & cEsc.Inicial & Chr(m_lSeparador) & cEsc.Inicial & Chr(m_lSeparador)
        
        If g_ogrupo.TipoEscalados = ModoRangos Then
            Linea = Linea & cEsc.final & Chr(m_lSeparador)
        Else
            Linea = Linea & "" & Chr(m_lSeparador)
        End If
        
        Linea = Linea & cEsc.Presupuesto
        
        Me.SSDBGridEscalados.AddItem Linea

    Next
End If

If g_ogrupo.TipoEscalados = ModoRangos Then
    ModoARangos
Else
    ModoADirecto
End If

Set g_ogrupo = Nothing
Set cEsc = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "CargarEscalados", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub



'Establece el modo de escalado en Rangos
Sub ModoARangos()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSDBGridEscalados.Columns("DIR").Visible = False
    SSDBGridEscalados.Columns("INI").Visible = True
    SSDBGridEscalados.Columns("FIN").Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "ModoARangos", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub
'Establece el modo de escalado en Cantidades directas
Sub ModoADirecto()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSDBGridEscalados.Columns("DIR").Visible = True
    SSDBGridEscalados.Columns("INI").Visible = False
    SSDBGridEscalados.Columns("FIN").Visible = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "ModoADirecto", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

'--<summary>
'--Comprueba que se han introducido correctamente los presupuestos unitarios necesarios
'--En caso de encontrar un valor no  v�lido muestra un mensaje, y se posiciona en la celda en modo edici�n
'--</summary>
'--<remarks>Click Bot�n Aceptar</remarks>
'--<revision>DPD 08/08/2011</revision>

Function ComprobarEscaladosFormulario() As Boolean
    Dim vPres As Variant
    Dim i As Long
    Dim bm As Variant ' Bookmark
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.SSDBGridEscalados.Update
    
    For i = 0 To Me.SSDBGridEscalados.Rows
        'Comprobamos los presupuestos
        bm = SSDBGridEscalados.AddItemBookmark(i)
        vPres = SSDBGridEscalados.Columns("PRES").CellValue(bm)
        If (Not IsNumeric(vPres)) And vPres <> "" Then
            oMensajes.valorCampoNoValido
            SSDBGridEscalados.Bookmark = bm
            SSDBGridEscalados.Col = SSDBGridEscalados.Columns("PRES").Position
            SSDBGridEscalados.Columns("PRES").Selected = True
            ComprobarEscaladosFormulario = False
            Exit Function
        End If
    Next i

    ComprobarEscaladosFormulario = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard2", "ComprobarEscaladosFormulario", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

