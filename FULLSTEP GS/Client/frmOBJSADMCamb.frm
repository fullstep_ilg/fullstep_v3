VERSION 5.00
Begin VB.Form frmOBJSADMCamb 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cambio de contrase�a para el servidor"
   ClientHeight    =   3420
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4590
   Icon            =   "frmOBJSADMCamb.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3420
   ScaleWidth      =   4590
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picADM 
      BackColor       =   &H00808000&
      Height          =   2835
      Left            =   60
      ScaleHeight     =   2775
      ScaleWidth      =   4395
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   60
      Width           =   4455
      Begin VB.TextBox txtAUsuActual 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   0
         Top             =   120
         Width           =   2235
      End
      Begin VB.TextBox txtAUsuNuevoConf 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   3
         Top             =   1500
         Width           =   2235
      End
      Begin VB.TextBox txtAUsuNuevo 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   1080
         Width           =   2235
      End
      Begin VB.TextBox txtAContrNuevaConf 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   5
         Top             =   2340
         Width           =   2235
      End
      Begin VB.TextBox txtAContrNueva 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   4
         Top             =   1920
         Width           =   2235
      End
      Begin VB.TextBox txtAContrActual 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2040
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   540
         Width           =   2235
      End
      Begin VB.Label lblAUsuNuevoConf 
         BackColor       =   &H00808000&
         Caption         =   "Confirmaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1560
         Width           =   1875
      End
      Begin VB.Label lblAUsuNuevo 
         BackColor       =   &H00808000&
         Caption         =   "Nuevo usuario:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   1140
         Width           =   1875
      End
      Begin VB.Label lblAContrNuevaConf 
         BackColor       =   &H00808000&
         Caption         =   "Confirmaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   2400
         Width           =   1875
      End
      Begin VB.Label lblAContrNueva 
         BackColor       =   &H00808000&
         Caption         =   "Nueva contrase�a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1980
         Width           =   1875
      End
      Begin VB.Line Line2 
         X1              =   120
         X2              =   4260
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Label lblAContrActual 
         BackColor       =   &H00808000&
         Caption         =   "Contrase�a actual:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   600
         Width           =   1875
      End
      Begin VB.Label lblAUsuActual 
         BackColor       =   &H00808000&
         Caption         =   "Usuario actual:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   1875
      End
   End
   Begin VB.PictureBox picButtons 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   675
      Left            =   0
      ScaleHeight     =   675
      ScaleWidth      =   4590
      TabIndex        =   15
      Top             =   2745
      Width           =   4590
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2220
         TabIndex        =   7
         Top             =   270
         Width           =   1215
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   960
         TabIndex        =   6
         Top             =   270
         Width           =   1155
      End
   End
End
Attribute VB_Name = "frmOBJSADMCamb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Multilenguaje

Private sIdiConfUid As String
Private sIdiConfPwd As String
Private sIdiUsuario As String
Private sIdiCorrecto As String
Private sIdiNuevaLic As String
Private sIdiIncluirLic As String
Private sIdiGenLic As String
Private sIdiPwdAct As String

''' <summary>
''' Evento que se genera al clicar en el bot�n de aceptar, cambiando la contrase�a en caso de cumplir los requerimientos al comprobar que todo es correcto
''' </summary>
''' <remarks>
''' Llamada desde: Autom�tica, siempre que se clicke el bot�n de aceptar
''' Tiempo m�ximo: 0,3 seg</remarks>
Private Sub cmdAceptar_Click()

    Dim teserror As TipoErrorSummit
    Dim oCrypt1 As Object, oCrypt2 As Object
    Dim pwdcrypt As String
    Dim dfechahoracrypt As String
    Dim oldusu As String
    Dim oldpwdenc As String
    Dim oldpwd As String
    Dim newusu As String
    Dim newpwdenc As String
    Dim newpwd As String
    Dim fecpwd As String
    Dim PathMisDocumentos As String * 260
    Dim ret As Long
    Dim filename As String
    Dim fso As New FileSystemObject, fil As TextStream
    Dim mensaje As String
    Dim sPassword As String
    
    ''' Comprobaci�n de igualdad
    
    If txtAUsuNuevo.Text <> txtAUsuNuevoConf.Text Then
        oMensajes.NoValida sIdiConfUid
        Exit Sub
    End If
    If txtAContrNueva.Text <> txtAContrNuevaConf.Text Then
        oMensajes.NoValida sIdiConfPwd
        Exit Sub
    End If
    
    ''' Datos del usuario
    
    Dim oGestorSeguridad As CGestorSeguridad
    Dim oUsuario As CUsuario
    
    Set oGestorSeguridad = oFSGSRaiz.generar_cgestorseguridad
    Set oUsuario = oFSGSRaiz.generar_cusuario
    
    Screen.MousePointer = vbHourglass
    Set oUsuario = oGestorSeguridad.DevolverBaseUsuarioOBJ
    Screen.MousePointer = vbNormal
     
    If txtAUsuActual.Text <> oUsuario.Cod Then
        oMensajes.NoValido sIdiUsuario
        Exit Sub
    End If
    
    Dim CodResul As Integer
    Dim oWebSvc As FSGSLibrary.CWebService
    Set oWebSvc = New FSGSLibrary.CWebService
    CodResul = oWebSvc.LlamarWebServiceCambioPWD(gParametrosGenerales, 2, 1, txtAUsuActual.Text, FSGSLibrary.SustituirCaracteresInvalidos(txtAContrActual, False), txtAUsuNuevo.Text, txtAContrNueva.Text, _
        oUsuarioSummit.fecpwd, oMensajes)
    Set oWebSvc = Nothing

    If CodResul = 0 Then
        txtAContrActual.Text = txtAContrNueva.Text

        ret = SHGetFolderPath(0, CSIDL_TYPES.CSIDL_PERSONAL, 0, 0, PathMisDocumentos)
                
        If ret = S_OK Then
        
            filename = "FSOS.LIC"
            PathMisDocumentos = Left$(PathMisDocumentos, InStr(PathMisDocumentos, Chr$(0)) - 1)

            filename = Trim(PathMisDocumentos) & "\" & filename
            
            Set fil = fso.CreateTextFile(filename)
            
            dfechahoracrypt = Format(Now, "DD\/MM\/YYYY HH\:NN\:SS")
            
            fil.WriteLine dfechahoracrypt
            
            ''' Encriptaci�n login
            sPassword = FSGSLibrary.EncriptarAES("", Me.txtAUsuNuevo.Text, True, dfechahoracrypt, 2)
            fil.WriteLine sPassword

                       
            ''' Encriptaci�n de la contrase�a
            sPassword = FSGSLibrary.EncriptarAES("", Me.txtAContrNueva.Text, True, dfechahoracrypt, 2)
            fil.WriteLine sPassword
            
            fil.Close
                
            mensaje = sIdiNuevaLic & " " & Chr(13) & Chr(13) & filename & Chr(13) & Chr(13)
            mensaje = mensaje & sIdiIncluirLic
            
            Beep

            MsgBox mensaje, vbInformation, sIdiGenLic
            
        End If
        
    Else
        txtAContrNuevaConf.Text = ""
        txtAContrNueva.Text = ""
        oMensajes.NoValida sIdiPwdAct
    End If
        
End Sub

Private Sub cmdCancelar_Click()
    
    Unload Me
    
End Sub

Private Sub Form_Load()

    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    CargarRecursos
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_OBJS_ADMCAMB, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    caption = Ador(0).Value
    Ador.MoveNext
    
    lblAUsuActual.caption = Ador(0).Value
    Ador.MoveNext
    lblAContrActual.caption = Ador(0).Value
    Ador.MoveNext
    lblAUsuNuevo.caption = Ador(0).Value
    Ador.MoveNext
    lblAUsuNuevoConf.caption = Ador(0).Value
    Ador.MoveNext
    lblAContrNueva.caption = Ador(0).Value
    Ador.MoveNext
    lblAContrNuevaConf.caption = Ador(0).Value
    Ador.MoveNext
    
    cmdAceptar.caption = Ador(0).Value
    Ador.MoveNext
    cmdCancelar.caption = Ador(0).Value
    Ador.MoveNext
    
  
    sIdiConfUid = Ador(0).Value
    Ador.MoveNext
    sIdiConfPwd = Ador(0).Value
    Ador.MoveNext
    sIdiUsuario = Ador(0).Value
    Ador.MoveNext
    sIdiCorrecto = Ador(0).Value
    Ador.MoveNext
    sIdiNuevaLic = Ador(0).Value
    Ador.MoveNext
    sIdiIncluirLic = Ador(0).Value
    Ador.MoveNext
    sIdiGenLic = Ador(0).Value
    Ador.MoveNext
    sIdiPwdAct = Ador(0).Value
    
    
    
    
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub

