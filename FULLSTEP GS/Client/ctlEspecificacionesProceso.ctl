VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{14ACBB92-9C4A-4C45-AFD2-7AE60E71E5B3}#4.0#0"; "IGSplitter40.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.UserControl ctlEspecificacionesProceso 
   ClientHeight    =   10590
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   18345
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ScaleHeight     =   11425.61
   ScaleMode       =   0  'User
   ScaleWidth      =   18345
   Begin SSSplitter.SSSplitter SSSplitterEsp 
      Height          =   9255
      Left            =   0
      TabIndex        =   1
      Top             =   360
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   16325
      _Version        =   262144
      PaneTree        =   "ctlEspecificacionesProceso.ctx":0000
      Begin VB.PictureBox picSplit3 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3825
         Left            =   30
         ScaleHeight     =   3825
         ScaleWidth      =   8355
         TabIndex        =   13
         Top             =   5400
         Width           =   8355
         Begin VB.PictureBox picBotonesAdjuntos 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   0
            ScaleHeight     =   495
            ScaleWidth      =   8295
            TabIndex        =   14
            Top             =   3240
            Width           =   8295
            Begin VB.CommandButton cmdAnyadirEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   6530
               Picture         =   "ctlEspecificacionesProceso.ctx":0072
               Style           =   1  'Graphical
               TabIndex        =   19
               Top             =   120
               UseMaskColor    =   -1  'True
               Width           =   300
            End
            Begin VB.CommandButton cmdEliminarEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   6890
               Picture         =   "ctlEspecificacionesProceso.ctx":00D3
               Style           =   1  'Graphical
               TabIndex        =   18
               Top             =   120
               UseMaskColor    =   -1  'True
               Width           =   300
            End
            Begin VB.CommandButton cmdModificarEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   7250
               Picture         =   "ctlEspecificacionesProceso.ctx":0159
               Style           =   1  'Graphical
               TabIndex        =   17
               Top             =   120
               UseMaskColor    =   -1  'True
               Width           =   300
            End
            Begin VB.CommandButton cmdSalvarEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   7610
               Picture         =   "ctlEspecificacionesProceso.ctx":02A3
               Style           =   1  'Graphical
               TabIndex        =   16
               Top             =   120
               UseMaskColor    =   -1  'True
               Width           =   300
            End
            Begin VB.CommandButton cmdAbrirEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   7970
               Picture         =   "ctlEspecificacionesProceso.ctx":0324
               Style           =   1  'Graphical
               TabIndex        =   15
               Top             =   120
               UseMaskColor    =   -1  'True
               Width           =   300
            End
         End
         Begin MSComctlLib.ListView lstvwEsp 
            Height          =   3015
            Left            =   0
            TabIndex        =   20
            Top             =   360
            Width           =   8265
            _ExtentX        =   14579
            _ExtentY        =   5318
            View            =   3
            Arrange         =   1
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            HotTracking     =   -1  'True
            _Version        =   393217
            Icons           =   "ImageList1"
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Fichero"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Tamanyo"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Comentario"
               Object.Width           =   6588
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Fecha"
               Object.Width           =   2867
            EndProperty
         End
         Begin VB.Label lblAdjuntos 
            Caption         =   "Datos Adjuntos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   0
            TabIndex        =   21
            Top             =   0
            Width           =   2895
         End
      End
      Begin VB.PictureBox picSplit2 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3495
         Left            =   30
         ScaleHeight     =   3495
         ScaleWidth      =   8355
         TabIndex        =   4
         Top             =   1815
         Width           =   8355
         Begin VB.PictureBox picBotonesAtributos 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   0
            ScaleHeight     =   615
            ScaleWidth      =   8310
            TabIndex        =   5
            Top             =   0
            Width           =   8310
            Begin VB.CommandButton cmdOrden 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   1
               Left            =   7080
               Picture         =   "ctlEspecificacionesProceso.ctx":03A0
               Style           =   1  'Graphical
               TabIndex        =   9
               Top             =   240
               Width           =   300
            End
            Begin VB.CommandButton cmdOrden 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   0
               Left            =   6720
               Picture         =   "ctlEspecificacionesProceso.ctx":03FA
               Style           =   1  'Graphical
               TabIndex        =   8
               Top             =   240
               Width           =   300
            End
            Begin VB.CommandButton cmdElimOEAtrib 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   7920
               Picture         =   "ctlEspecificacionesProceso.ctx":0454
               Style           =   1  'Graphical
               TabIndex        =   7
               Top             =   240
               UseMaskColor    =   -1  'True
               Width           =   300
            End
            Begin VB.CommandButton cmdAnyaOEAtrib 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   7560
               Picture         =   "ctlEspecificacionesProceso.ctx":04E6
               Style           =   1  'Graphical
               TabIndex        =   6
               Top             =   240
               UseMaskColor    =   -1  'True
               Width           =   300
            End
            Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
               Height          =   360
               Left            =   1920
               TabIndex        =   10
               Top             =   120
               Width           =   3015
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               stylesets.count =   1
               stylesets(0).Name=   "Normal"
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "ctlEspecificacionesProceso.ctx":0568
               DividerStyle    =   3
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "VALOR"
               Columns(0).Name =   "VALOR"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Caption=   "DESC"
               Columns(1).Name =   "DESC"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5318
               _ExtentY        =   635
               _StockProps     =   77
            End
            Begin MSComDlg.CommonDialog cmmdEsp 
               Left            =   5040
               Top             =   0
               _ExtentX        =   847
               _ExtentY        =   847
               _Version        =   393216
               CancelError     =   -1  'True
               DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
               MaxFileSize     =   1500
            End
            Begin VB.Label lblAtributos 
               Caption         =   "DOtros datos:"
               Height          =   375
               Left            =   120
               TabIndex        =   11
               Top             =   360
               Width           =   1455
            End
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
            Height          =   2805
            Left            =   120
            TabIndex        =   12
            Top             =   600
            Width           =   8205
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   21
            stylesets.count =   2
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "ctlEspecificacionesProceso.ctx":0584
            stylesets(1).Name=   "styAzul"
            stylesets(1).BackColor=   16777152
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "ctlEspecificacionesProceso.ctx":05A0
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   21
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ANYO"
            Columns(0).Name =   "ANYO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "GMN1"
            Columns(1).Name =   "GMN1"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "PROCE"
            Columns(2).Name =   "PROCE"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "GRUPO"
            Columns(3).Name =   "GRUPO"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "ATRIB"
            Columns(4).Name =   "ATRIB"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "Item"
            Columns(5).Name =   "Item"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "AMBITO"
            Columns(6).Name =   "AMBITO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   1323
            Columns(7).Caption=   "Interno"
            Columns(7).Name =   "Interno"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   11
            Columns(7).FieldLen=   256
            Columns(7).Style=   2
            Columns(8).Width=   1376
            Columns(8).Caption=   "Pedido"
            Columns(8).Name =   "Pedido"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   11
            Columns(8).FieldLen=   256
            Columns(8).Style=   2
            Columns(9).Width=   2487
            Columns(9).Caption=   "C�digo"
            Columns(9).Name =   "COD"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(9).Locked=   -1  'True
            Columns(9).HasHeadForeColor=   -1  'True
            Columns(9).HasHeadBackColor=   -1  'True
            Columns(9).HasBackColor=   -1  'True
            Columns(9).HeadBackColor=   -2147483633
            Columns(9).BackColor=   16777152
            Columns(10).Width=   6535
            Columns(10).Caption=   "Nombre"
            Columns(10).Name=   "DEN"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   200
            Columns(10).Locked=   -1  'True
            Columns(10).HasHeadForeColor=   -1  'True
            Columns(10).HasHeadBackColor=   -1  'True
            Columns(10).HasBackColor=   -1  'True
            Columns(10).HeadBackColor=   -2147483633
            Columns(10).BackColor=   16777152
            Columns(11).Width=   6218
            Columns(11).Caption=   "Valor"
            Columns(11).Name=   "VALOR"
            Columns(11).CaptionAlignment=   0
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   32000
            Columns(11).HasHeadForeColor=   -1  'True
            Columns(11).HasHeadBackColor=   -1  'True
            Columns(11).HeadBackColor=   -2147483633
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "MIN"
            Columns(12).Name=   "MIN"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "MAX"
            Columns(13).Name=   "MAX"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Visible=   0   'False
            Columns(14).Caption=   "TIPO_DATOS"
            Columns(14).Name=   "TIPO_DATOS"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(15).Width=   3200
            Columns(15).Visible=   0   'False
            Columns(15).Caption=   "TIPO_INTRO"
            Columns(15).Name=   "TIPO_INTRO"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(16).Width=   3200
            Columns(16).Visible=   0   'False
            Columns(16).Caption=   "GRUPOID"
            Columns(16).Name=   "GRUPOID"
            Columns(16).DataField=   "Column 16"
            Columns(16).DataType=   8
            Columns(16).FieldLen=   256
            Columns(17).Width=   3200
            Columns(17).Visible=   0   'False
            Columns(17).Caption=   "OBLIG"
            Columns(17).Name=   "OBLIG"
            Columns(17).DataField=   "Column 17"
            Columns(17).DataType=   11
            Columns(17).FieldLen=   256
            Columns(18).Width=   3200
            Columns(18).Visible=   0   'False
            Columns(18).Caption=   "VALIDACION"
            Columns(18).Name=   "VALIDACION"
            Columns(18).DataField=   "Column 18"
            Columns(18).DataType=   2
            Columns(18).FieldLen=   256
            Columns(19).Width=   3200
            Columns(19).Visible=   0   'False
            Columns(19).Caption=   "ID_A"
            Columns(19).Name=   "ID"
            Columns(19).DataField=   "Column 19"
            Columns(19).DataType=   8
            Columns(19).FieldLen=   256
            Columns(20).Width=   3200
            Columns(20).Visible=   0   'False
            Columns(20).Caption=   "LISTA_EXTERNA"
            Columns(20).Name=   "LISTA_EXTERNA"
            Columns(20).DataField=   "Column 20"
            Columns(20).DataType=   11
            Columns(20).FieldLen=   256
            _ExtentX        =   14473
            _ExtentY        =   4948
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.PictureBox picSplit1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1695
         Left            =   30
         ScaleHeight     =   1695
         ScaleWidth      =   8355
         TabIndex        =   2
         Top             =   30
         Width           =   8355
         Begin VB.TextBox txtEspecificacion 
            Height          =   1530
            Left            =   0
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Top             =   0
            Width           =   8310
         End
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ctlEspecificacionesProceso.ctx":05BC
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblEsp 
      Alignment       =   2  'Center
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Especificaciones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   292
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8415
   End
End
Attribute VB_Name = "ctlEspecificacionesProceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public g_oAtributos As CAtributos
Private m_oAtribEnEdicion As CAtributo
Public m_bRespetarEspec As Boolean


Private g_oObjetoSeleccionado As Object
' proceso o grupo en curso

'Variables de Textos de idioma
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sIdiMayor As String
Private m_sIdiManor As String
Private m_sIdiEntre As String
Private m_sIdiTexto As String
Private m_sIdiNumerico As String
Private m_sIdiBoolean As String
Private m_sIdiInterno As String
Private m_sIdiCodigo As String
Private m_sIdiDenominacion As String
Private m_sIdiValor As String
Private m_sIdiPedido As String
Private m_sIdiFecha As String
Private m_sIdiAtributosEsp As String
Private m_sIdiAtributosEspProceso As String
Private m_sIdiAtributosEspGrupo As String

Private m_bSalirDeEdicion As Boolean

Public Property Get ObjetoSeleccionado() As Object
    ObjetoSeleccionado = g_oObjetoSeleccionado
End Property

Public Property Let ObjetoSeleccionado(ByRef objeto As Object)
    Set g_oObjetoSeleccionado = objeto
End Property

Public Property Get ListaEsp() As Object
    Set ListaEsp = lstvwEsp
End Property

Public Property Let ListaEsp(ByRef objeto As Object)
   Set lstvwEsp = objeto
End Property

Public Property Get GridAtributosEsp() As Object
    Set GridAtributosEsp = sdbgAtributos
End Property

Public Property Let GridAtributosEsp(ByRef objeto As Object)
   Set sdbgAtributos = objeto
End Property


Public Property Get txtEsp() As String
    txtEsp = txtEspecificacion.Text
End Property


Public Property Let txtEsp(Texto As String)
    m_bRespetarEspec = True
    txtEspecificacion.Text = Texto
    m_bRespetarEspec = False
End Property

Public Sub txtEsp_validate(Cancel As Boolean)
    txtEspecificacion_Validate Cancel
End Sub

Public Property Get SalirDeEdicion() As Boolean
    SalirDeEdicion = m_bSalirDeEdicion
End Property

Public Property Let SalirDeEdicion(Value As Boolean)
    m_bSalirDeEdicion = Value
End Property


Private Sub cmdAbrirEsp_Click()
    Dim Item As MSComctlLib.listItem
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
    Dim node As MSComctlLib.node
    Dim FOSFile As Scripting.FileSystemObject
    Dim arrData() As Byte
    
    On Error GoTo Cancelar:

    Set node = Parent.tvwProce.selectedItem
    
    If node Is Nothing Then Exit Sub
    
    If node.Tag = "Especificaciones" Then
        'ESPECIFICACIONES DE PROCESO
        If g_oObjetoSeleccionado Is Nothing Then Exit Sub
    
        Screen.MousePointer = vbHourglass
        
        Set Item = lstvwEsp.selectedItem
    
        If Item Is Nothing Then
            oMensajes.SeleccioneFichero
        Else
            sFileName = FSGSLibrary.DevolverPathFichTemp
            sFileName = sFileName & Item.Text
            sFileTitle = Item.Text
            
            'Comprueba si existe el fichero y si existe se borra:
            Set FOSFile = New Scripting.FileSystemObject
            If FOSFile.FileExists(sFileName) Then
                FOSFile.DeleteFile sFileName, True
            End If
            Set FOSFile = Nothing
            
            ' Cargamos el contenido en la esp.
            Screen.MousePointer = vbHourglass
            Set oEsp = g_oObjetoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
            teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspProceso)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Set node = Nothing
                Exit Sub
            End If
               
            oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspProceso, oEsp.DataSize, sFileName
        
            Parent.AnyadirTemporal sFileName

            'Lanzamos la aplicacion
            ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
        End If
    
        g_oObjetoSeleccionado.ActualizadoProceso
        Set oEsp = Nothing

    Else
        'ESPECIFICACIONES DE GRUPO
        If g_oObjetoSeleccionado Is Nothing Then Exit Sub
    
        Screen.MousePointer = vbHourglass
    
        Set Item = lstvwEsp.selectedItem
    
        If Item Is Nothing Then
            oMensajes.SeleccioneFichero
        Else
            sFileName = FSGSLibrary.DevolverPathFichTemp
            sFileName = sFileName & Item.Text
            sFileTitle = Item.Text
            
            'Comprueba si existe el fichero y si existe se borra:
            Set FOSFile = New Scripting.FileSystemObject
            If FOSFile.FileExists(sFileName) Then
                FOSFile.DeleteFile sFileName, True
            End If
            Set FOSFile = Nothing
            
            ' Cargamos el contenido en la esp.
            Screen.MousePointer = vbHourglass
            Set oEsp = g_oObjetoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
            teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspGrupo)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Set node = Nothing
                Exit Sub
            End If
    
            oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspGrupo, oEsp.DataSize, sFileName
                        
            Parent.AnyadirTemporal sFileName
    
            'Lanzamos la aplicacion
            ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
        End If
    
        g_oObjetoSeleccionado.ActualizadoGrupo
        Set oEsp = Nothing
    End If
    
    Set node = Nothing
    Screen.MousePointer = vbNormal
    Exit Sub

Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
    'If DataFile <> 0 Then
    '    Close DataFile
    '    DataFile = 0
    'End If

    Set oEsp = Nothing
    Set node = Nothing

End Sub

Private Sub cmdAnyadirEsp_Click()
    Dim DataFile As Integer
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
    Dim node As MSComctlLib.node
    Dim arrFileNames As Variant
    Dim iFile As Integer
    Dim oFos As Scripting.FileSystemObject
    Dim sAdjunto As String
    Dim ArrayAdjunto() As String
    Dim oFile As File
    Dim bites As Long
    
    On Error GoTo Cancelar:

    If g_oObjetoSeleccionado Is Nothing Then Exit Sub
    Screen.MousePointer = vbHourglass
    
    Set node = Parent.tvwProce.selectedItem
    
    If node Is Nothing Then
        Exit Sub
    End If
    
    If node.Tag = "Especificaciones" Then
        'ESPECIFICACIONES DE PROCESO
            
        cmmdEsp.DialogTitle = Parent.m_sIdiSelecAdjunto
    
        cmmdEsp.Filter = Parent.m_sIdiTodosArchivos & "|*.*"
        cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    
        cmmdEsp.filename = ""
        cmmdEsp.ShowOpen
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileName = "" Then
            'Parent.DesbloquearProceso
            Set node = Nothing
            Exit Sub
        End If
    
    
        arrFileNames = ExtraerFicheros(sFileName)
    
        ' Ahora obtenemos el comentario para la especificacion
        frmPROCEComFich.chkProcFich.Visible = False
        If UBound(arrFileNames) = 1 Then
            frmPROCEComFich.lblFich = sFileTitle
        Else
            frmPROCEComFich.lblFich = ""
            frmPROCEComFich.Label1.Visible = False
            frmPROCEComFich.lblFich.Visible = False
            frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
            frmPROCEComFich.txtCom.Height = 2300
        End If
        
        frmPROCEComFich.sOrigen = "frmPROCE"
        Parent.g_bCancelarEsp = False
        Screen.MousePointer = vbNormal
        frmPROCEComFich.Show 1
        If g_oObjetoSeleccionado Is Nothing Then Exit Sub
        Screen.MousePointer = vbHourglass
        
        If Not Parent.g_bCancelarEsp Then
            Set oFos = New Scripting.FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                Set oEsp = oFSGSRaiz.generar_CEspecificacion
                Set oEsp.proceso = g_oObjetoSeleccionado
                oEsp.nombre = sFileTitle
                oEsp.Comentario = Parent.g_sComentario
        
                teserror = oEsp.ComenzarEscrituraData(TipoEspecificacion.EspProceso)
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set oEsp = Nothing
                    Set node = Nothing
                    Exit Sub
                End If
        
                oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                sFileName = FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
                Set oFile = oFos.GetFile(sFileName)
                bites = oFile.Size
                oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
                sAdjunto = oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspProceso)
                
                'Creamos un array, cada "substring" se asignar�
                'a un elemento del array
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                oEsp.Id = ArrayAdjunto(0)
                oEsp.DataSize = bites
                oEsp.Fecha = Date & " " & Time
        
                basSeguridad.RegistrarAccion accionessummit.ACCProceEspAnya, "Anyo:" & Parent.sdbcAnyo.Value & "GMN1:" & Parent.sdbcGMN1_4Cod.Value & "Cod:" & Parent.sdbcProceCod.Value & "Archivo:" & sFileTitle
                If g_oObjetoSeleccionado.especificaciones Is Nothing Then
                    Set g_oObjetoSeleccionado.especificaciones = oFSGSRaiz.Generar_CEspecificaciones
                End If
                g_oObjetoSeleccionado.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, g_oObjetoSeleccionado, , oEsp.Comentario, , , , , , , oEsp.DataSize, TipoEspecificacion.EspProceso
                lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & Parent.m_skb
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
        
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
            Next
            ImagenNodoEspecificaciones
   
        End If
        lstvwEsp.Refresh
        Set oEsp = Nothing
        Screen.MousePointer = vbHourglass
        g_oObjetoSeleccionado.ActualizadoProceso
        Set node = Nothing
        Screen.MousePointer = vbNormal
        

    Else
        'ESPECIFICACIONES DE GRUPO
        If g_oObjetoSeleccionado Is Nothing Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        Set Parent.m_oIBAseDatosEnEdicion = g_oObjetoSeleccionado
        teserror = Parent.m_oIBAseDatosEnEdicion.IniciarEdicion(False)
        If teserror.NumError <> TESnoerror Then
            'Parent.DesbloquearProceso
            basErrores.TratarError teserror
            Set Parent.m_oIBAseDatosEnEdicion = Nothing
            Set node = Nothing
            Exit Sub
        End If
    
        cmmdEsp.DialogTitle = Parent.m_sIdiSelecAdjunto
    
        cmmdEsp.Filter = Parent.m_sIdiTodosArchivos & "|*.*"
        cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    
        cmmdEsp.filename = ""
        cmmdEsp.ShowOpen

    
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileName = "" Then
            'Parent.DesbloquearProceso
            Set Parent.m_oIBAseDatosEnEdicion = Nothing
            Set node = Nothing
            Exit Sub
        End If
    
        arrFileNames = ExtraerFicheros(sFileName)
        
        ' Ahora obtenemos el comentario para la especificacion
        frmPROCEComFich.chkProcFich.Visible = False
        If UBound(arrFileNames) = 1 Then
            frmPROCEComFich.lblFich = sFileTitle
        Else
            frmPROCEComFich.lblFich = ""
            frmPROCEComFich.Label1.Visible = False
            frmPROCEComFich.lblFich.Visible = False
            frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
            frmPROCEComFich.txtCom.Height = 2300
        End If
        frmPROCEComFich.sOrigen = "frmPROCE"
        Parent.g_bCancelarEsp = False
        Screen.MousePointer = vbNormal
        frmPROCEComFich.Show 1
        If g_oObjetoSeleccionado Is Nothing Then Exit Sub
        Screen.MousePointer = vbHourglass
        
        If Not Parent.g_bCancelarEsp Then
            Set oFos = New Scripting.FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                Set oEsp = oFSGSRaiz.generar_CEspecificacion
                Set oEsp.Grupo = g_oObjetoSeleccionado
                oEsp.nombre = sFileTitle
                oEsp.Comentario = Parent.g_sComentario
        
                teserror = oEsp.ComenzarEscrituraData(TipoEspecificacion.EspGrupo)
                If teserror.NumError <> TESnoerror Then
                    'Parent.DesbloquearProceso
                    basErrores.TratarError teserror
                    Set oEsp = Nothing
                    Set Parent.m_oIBAseDatosEnEdicion = Nothing
                    Set node = Nothing
                    Exit Sub
                End If
        
                Set oFile = oFos.GetFile(sFileName)
                bites = oFile.Size
                oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
                sAdjunto = oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspGrupo)
                
                'Creamos un array, cada "substring" se asignar�
                'a un elemento del array
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                oEsp.Id = ArrayAdjunto(0)
                oEsp.DataSize = bites
                oEsp.Fecha = Date & " " & Time
        
                basSeguridad.RegistrarAccion accionessummit.ACCProceGrupoEspAnya, "Anyo:" & Parent.sdbcAnyo.Value & "GMN1:" & Parent.sdbcGMN1_4Cod.Value & "Proceso:" & Parent.sdbcProceCod.Value & "Grupo:" & g_oObjetoSeleccionado.Codigo & "Archivo:" & sFileTitle
                If g_oObjetoSeleccionado.especificaciones Is Nothing Then
                    Set g_oObjetoSeleccionado.especificaciones = oFSGSRaiz.Generar_CEspecificaciones
                End If
                g_oObjetoSeleccionado.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , , oEsp.Comentario, , , , g_oObjetoSeleccionado, , , oEsp.DataSize, TipoEspecificacion.EspProceso
                lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & Parent.m_skb
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
        
                lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
            Next
            ImagenNodoEspecificaciones

        
        End If
        lstvwEsp.Refresh
        Set oEsp = Nothing
        'Parent.DesbloquearProceso
        Screen.MousePointer = vbHourglass
        Set Parent.m_oIBAseDatosEnEdicion = Nothing
        g_oObjetoSeleccionado.ActualizadoGrupo
        Set node = Nothing
        Screen.MousePointer = vbNormal
    
    End If
    Exit Sub

Cancelar:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Close DataFile
    End If
    Set oEsp = Nothing
    Set Parent.m_oIBAseDatosEnEdicion = Nothing
    Set node = Nothing
    Screen.MousePointer = vbNormal
End Sub



Private Sub cmdAnyaOEAtrib_Click()
    sdbgAtributos.Columns("Valor").Style = 0
    sdbgAtributos.Columns("Valor").DropDownHwnd = 0
    
    Parent.AplicarCambios
    

    If Not Parent.g_ofrmATRIB Is Nothing Then
        Unload Parent.g_ofrmATRIB
        Set Parent.g_ofrmATRIB = Nothing
    End If

    Set Parent.g_ofrmATRIB = New frmAtrib
    
    If frmPROCE.g_oGrupoSeleccionado Is Nothing Then
        Parent.g_ofrmATRIB.g_sOrigen = "ATRIB_ESP"
    Else
        Parent.g_ofrmATRIB.g_sOrigen = "ATRIB_ESP_GRUPO"
    End If
    Parent.g_ofrmATRIB.Seleccionar Me


End Sub

Private Sub cmdEliminarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim node As MSComctlLib.node

On Error GoTo Cancelar:
    
    If g_oObjetoSeleccionado Is Nothing Then Exit Sub
    
    'If Not Parent.BloquearProceso Then Exit Sub
    
    Set Item = lstvwEsp.selectedItem

    If Item Is Nothing Then
            oMensajes.SeleccioneFichero
    Else

            irespuesta = oMensajes.PreguntaEliminar(" " & Parent.m_sIdiElArchivo & " " & lstvwEsp.selectedItem.Text)
            If irespuesta = vbNo Then
                Exit Sub
            End If
    
            Set node = Parent.tvwProce.selectedItem
            
            If node Is Nothing Then
                Exit Sub
            End If
            
            If node.Tag = "Especificaciones" Then
                'ESPECIFICACIONES DE PROCESO
                
                Screen.MousePointer = vbHourglass
                Set oEsp = g_oObjetoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
                Set Parent.m_oIBAseDatosEnEdicion = oEsp
                teserror = Parent.m_oIBAseDatosEnEdicion.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set node = Nothing
                    Set oEsp = Nothing
                    Set Parent.m_oIBAseDatosEnEdicion = Nothing
                Else
                    g_oObjetoSeleccionado.especificaciones.Remove (CStr(lstvwEsp.selectedItem.Tag))
                    basSeguridad.RegistrarAccion accionessummit.ACCProceEspEli, "Anyo:" & Parent.sdbcAnyo.Value & "GMN1:" & Parent.sdbcGMN1_4Cod.Value & "Cod:" & Parent.sdbcProceCod.Value & "Archivo:" & lstvwEsp.selectedItem.Text
                    lstvwEsp.ListItems.Remove (CStr(lstvwEsp.selectedItem.key))
                    ImagenNodoEspecificaciones
                    g_oObjetoSeleccionado.ActualizadoProceso
                End If
            
            Else
                'ESPECIFICACIONES DE GRUPO
                Screen.MousePointer = vbHourglass
                Set oEsp = g_oObjetoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
                Set Parent.m_oIBAseDatosEnEdicion = oEsp
                teserror = Parent.m_oIBAseDatosEnEdicion.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set node = Nothing
                    Set oEsp = Nothing
                    Set Parent.m_oIBAseDatosEnEdicion = Nothing
                Else
                    g_oObjetoSeleccionado.especificaciones.Remove (CStr(lstvwEsp.selectedItem.Tag))
                    basSeguridad.RegistrarAccion accionessummit.ACCProceGrupoEspEli, "Anyo:" & Parent.sdbcAnyo.Value & "GMN1:" & Parent.sdbcGMN1_4Cod.Value & "Proceso:" & Parent.sdbcProceCod.Value & "Grupo:" & g_oObjetoSeleccionado.Codigo & "Archivo:" & lstvwEsp.selectedItem.Text
                    lstvwEsp.ListItems.Remove (CStr(lstvwEsp.selectedItem.key))
                    Me.ImagenNodoEspecificaciones
                    g_oObjetoSeleccionado.ActualizadoGrupo
                End If
            
            End If
            
    End If
    
    Set oEsp = Nothing
    Set node = Nothing
    'Parent.DesbloquearProceso
    Set Parent.m_oIBAseDatosEnEdicion = Nothing
    Screen.MousePointer = vbNormal
    Exit Sub

Cancelar:
    
    Set oEsp = Nothing
    Set Parent.m_oIBAseDatosEnEdicion = Nothing
    Set node = Nothing

    Screen.MousePointer = vbNormal

End Sub


Private Sub cmdElimOEAtrib_Click()
    Dim lID As Long
    Dim i As Integer
    Dim vbook As Variant
    Dim teserror As TipoErrorSummit
    Dim oAtributoEliminado As CAtributo
    Dim oAtributo As CAtributo
    Dim iAnyo As Integer
    Dim sGMN1 As String
    Dim iProce As Long
    Dim sGrupo As String
    Dim iAmbito As Integer
    Dim irespuesta As Integer
    
    sdbgAtributos.Columns("Valor").DropDownHwnd = 0
    
    If sdbgAtributos.DataChanged Then
        sdbgAtributos.Update
    End If
    
    If sdbgAtributos.Rows = 0 Then Exit Sub
    
    If sdbgAtributos.Row = -1 Then Exit Sub
    
    Parent.AplicarCambios
    
    
    Select Case sdbgAtributos.SelBookmarks.Count
        Case 0
            Exit Sub
        Case 1
            If sdbgAtributos.Columns("GRUPO").CellValue(sdbgAtributos.SelBookmarks(0)) <> "" Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiAtributosEsp & ": " & sdbgAtributos.Columns("COD").CellValue(sdbgAtributos.SelBookmarks(0)) & " (" & sdbgAtributos.Columns("DEN").CellValue(sdbgAtributos.SelBookmarks(0)) & "), " & m_sIdiAtributosEspGrupo & ": " & sdbgAtributos.Columns("GRUPO").CellValue(sdbgAtributos.SelBookmarks(0)))
            Else
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiAtributosEsp & ": " & sdbgAtributos.Columns("COD").CellValue(sdbgAtributos.SelBookmarks(0)) & " (" & sdbgAtributos.Columns("DEN").CellValue(sdbgAtributos.SelBookmarks(0)) & "), " & m_sIdiAtributosEspProceso)
            End If
        Case Is > 1
            irespuesta = oMensajes.PreguntaEliminar(m_sIdiAtributosEsp)
    End Select
    
    If irespuesta = vbNo Then
        Exit Sub
    End If
    
    Dim aIdentificadores As Variant
    ReDim aIdentificadores(sdbgAtributos.SelBookmarks.Count)
    
    i = 0
    While i < sdbgAtributos.SelBookmarks.Count
        If i = 0 Then 'Recupero el anyo, gmn1, proce y grupo
            iAnyo = sdbgAtributos.Columns("ANYO").CellValue(sdbgAtributos.SelBookmarks(i))
            sGMN1 = sdbgAtributos.Columns("GMN1").CellValue(sdbgAtributos.SelBookmarks(i))
            iProce = sdbgAtributos.Columns("PROCE").CellValue(sdbgAtributos.SelBookmarks(i))
            sGrupo = sdbgAtributos.Columns("GRUPOID").CellValue(sdbgAtributos.SelBookmarks(i))

            iAmbito = sdbgAtributos.Columns("AMBITO").CellValue(sdbgAtributos.SelBookmarks(i))
        End If
        aIdentificadores(i) = sdbgAtributos.Columns("ATRIB").CellValue(sdbgAtributos.SelBookmarks(i))
        i = i + 1
    Wend
    If iAmbito = 1 Then
        teserror = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.EliminarAtributosDeEspecificacion(aIdentificadores, iAnyo, sGMN1, iProce, sGrupo)
    Else 'Ambito de Grupo
        If sGrupo = "" Then
            If iAmbito = 2 Then
                If sdbgAtributos.Columns("GRUPO").CellValue(sdbgAtributos.SelBookmarks(i)) = Parent.g_oGrupoSeleccionado.Codigo Then
                    sGrupo = Parent.g_oGrupoSeleccionado.Id
                End If
            End If
        End If

        teserror = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.EliminarAtributosDeEspecificacion(aIdentificadores, iAnyo, sGMN1, iProce, sGrupo)
    End If
    
    For i = 0 To sdbgAtributos.SelBookmarks.Count - 1
        vbook = sdbgAtributos.SelBookmarks(i)
        lID = sdbgAtributos.Columns("ATRIB").CellValue(vbook)
        'Actualizar Los ordenes
        If iAmbito = 1 Then
            Set oAtributoEliminado = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(lID))
            For Each oAtributo In Parent.g_oProcesoSeleccionado.AtributosEspecificacion
                If oAtributo.Orden > oAtributoEliminado.Orden Then
                    oAtributo.Orden = oAtributo.Orden - 1
                End If
            Next
            Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Remove CStr(lID)
        Else
            Set oAtributoEliminado = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(lID))
            For Each oAtributo In Parent.g_oGrupoSeleccionado.AtributosEspecificacion
                If oAtributo.Orden > oAtributoEliminado.Orden Then
                    oAtributo.Orden = oAtributo.Orden - 1
                End If
            Next
            Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Remove CStr(lID)
        End If

    Next

    sdbgAtributos.DeleteSelected
    ImagenNodoEspecificaciones
End Sub

Private Sub cmdModificarEsp_Click()
Dim node As MSComctlLib.node
Dim teserror As TipoErrorSummit

    If g_oObjetoSeleccionado Is Nothing Then Exit Sub
    
    Parent.m_udtOrigBloqueo = ModificarEsp
    
    Set node = Parent.tvwProce.selectedItem
    
    If node Is Nothing Then
        Exit Sub
    End If
    
    If node.Tag = "Especificaciones" Then
        'ESPECIFICACIONES DE PROCESO
    
        If lstvwEsp.selectedItem Is Nothing Then
            oMensajes.SeleccioneFichero
        Else
            Screen.MousePointer = vbHourglass
            Set Parent.m_oIBAseDatosEnEdicion = g_oObjetoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
            teserror = Parent.m_oIBAseDatosEnEdicion.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set node = Nothing
                Set Parent.m_oIBAseDatosEnEdicion = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            Parent.Accion = ACCProceEspMod
            Screen.MousePointer = vbNormal
            frmPROCEEspMod.g_sOrigen = "frmPROCE"
            Set frmPROCEEspMod.g_oIBaseDatos = Parent.m_oIBAseDatosEnEdicion
            frmPROCEEspMod.Show 1
        End If

    Else
        'ESPECIFICACIONES DE GRUPO
        If g_oObjetoSeleccionado Is Nothing Then
            'Parent.DesbloquearProceso
            Exit Sub
        End If
        
        If lstvwEsp.selectedItem Is Nothing Then
            oMensajes.SeleccioneFichero
        Else
            Screen.MousePointer = vbHourglass
            Set Parent.m_oIBAseDatosEnEdicion = g_oObjetoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
            teserror = Parent.m_oIBAseDatosEnEdicion.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set Parent.m_oIBAseDatosEnEdicion = Nothing
                Set node = Nothing
                Screen.MousePointer = vbNormal
                'Parent.DesbloquearProceso
                Exit Sub
            End If
            Parent.Accion = ACCProceGrupoEspMod
            Screen.MousePointer = vbNormal
            frmPROCEEspMod.g_sOrigen = "frmPROCEG"
            Set frmPROCEEspMod.g_oIBaseDatos = Parent.m_oIBAseDatosEnEdicion
            frmPROCEEspMod.Show 1
        End If
    
    End If
    Screen.MousePointer = vbNormal
    'Parent.Accion = ACCProceCon
    Set Parent.m_oIBAseDatosEnEdicion = Nothing
    'Parent.DesbloquearProceso
    Set node = Nothing
    
End Sub


Private Sub cmdOrden_Click(Index As Integer)
Dim i As Long
Dim iColumnas As Integer
Dim vdatos1() As Variant
Dim vdatos2() As Variant
Dim vdatos1T() As Variant
Dim vdatos2T() As Variant
Dim oAtributos As CAtributos
Dim teserror As TipoErrorSummit
Dim vbm As Variant
Dim oatrib As CAtributo

    If sdbgAtributos.Rows < 2 Then Exit Sub
    sdbgAtributos.SelectTypeRow = ssSelectionTypeSingleSelect
    Select Case Index
        Case 0
            'Desplaza el orden del atributo hacia arriba
            If sdbgAtributos.Row = 0 Then
                sdbgAtributos.SelectTypeRow = ssSelectionTypeMultiSelectRange
                Exit Sub
            End If
            'Actualiza la grid con los cambios de orden
            iColumnas = sdbgAtributos.Columns.Count
            ReDim vdatos1(iColumnas - 1)
            ReDim vdatos2(iColumnas - 1)
            ReDim vdatos1T(iColumnas - 1)
            ReDim vdatos2T(iColumnas - 1)
            
            For i = 0 To iColumnas - 1
                vdatos1(i) = sdbgAtributos.Columns(i).Value
                vdatos1T(i) = sdbgAtributos.Columns(i).Text
            Next i
            
            sdbgAtributos.MovePrevious
    
            For i = 0 To iColumnas - 1
                vdatos2(i) = sdbgAtributos.Columns(i).Value
                 vdatos2T(i) = sdbgAtributos.Columns(i).Text
            Next i
            For i = 0 To iColumnas - 1
                sdbgAtributos.Columns(i).Value = vdatos1(i)
                sdbgAtributos.Columns(i).Text = vdatos1T(i)
            Next i
            
            sdbgAtributos.MoveNext
            For i = 0 To iColumnas - 1
                sdbgAtributos.Columns(i).Value = vdatos2(i)
                sdbgAtributos.Columns(i).Text = vdatos2T(i)
            Next i
            
            sdbgAtributos.SelBookmarks.RemoveAll
            sdbgAtributos.MovePrevious
            sdbgAtributos.SelBookmarks.Add sdbgAtributos.Bookmark
    
        Case 1
            'Desplaza el orden del atributo hacia abajo
            
            If sdbgAtributos.Row = sdbgAtributos.Rows - 1 Then
                sdbgAtributos.SelectTypeRow = ssSelectionTypeMultiSelectRange
                Exit Sub
            End If
            iColumnas = sdbgAtributos.Columns.Count
            ReDim vdatos1(iColumnas - 1)
            ReDim vdatos2(iColumnas - 1)
            ReDim vdatos1T(iColumnas - 1)
            ReDim vdatos2T(iColumnas - 1)
            
            For i = 0 To iColumnas - 1
                vdatos1(i) = sdbgAtributos.Columns(i).Value
                vdatos1T(i) = sdbgAtributos.Columns(i).Text
            Next i
            
            sdbgAtributos.MoveNext
            
            For i = 0 To iColumnas - 1
                vdatos2(i) = sdbgAtributos.Columns(i).Value
                vdatos2T(i) = sdbgAtributos.Columns(i).Text
            Next i
            For i = 0 To iColumnas - 1
                sdbgAtributos.Columns(i).Value = vdatos1(i)
                sdbgAtributos.Columns(i).Text = vdatos1T(i)
            Next i
            
            sdbgAtributos.MovePrevious
            For i = 0 To iColumnas - 1
                sdbgAtributos.Columns(i).Value = vdatos2(i)
                sdbgAtributos.Columns(i).Text = vdatos2T(i)
            Next i
            
            sdbgAtributos.MoveNext
            sdbgAtributos.SelBookmarks.Add sdbgAtributos.Bookmark
    
    End Select
    sdbgAtributos.SelectTypeRow = ssSelectionTypeMultiSelectRange
    'Comprueba los cambios de orden que se han producido y almacena en la base
    'de datos:
    
    Set oAtributos = oFSGSRaiz.Generar_CAtributos
    
        
        For i = 0 To sdbgAtributos.Rows - 1
            vbm = sdbgAtributos.AddItemBookmark(i)
            'Guarda en BD solo el orden de los atributos que han cambiado
            If sdbgAtributos.Columns("AMBITO").Value = 1 Then
                If NullToDbl0(Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).Orden) <> (i + 1) Then
                    Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).Orden = i + 1
                    oAtributos.AddAtrEsp Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).AnyoProce, Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).GMN1Proce, Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).CodProce, Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).Atrib, 1, , , , , Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).Orden
                End If
            Else
                If NullToDbl0(Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).Orden) <> (i + 1) Then
                    Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).Orden = i + 1
                    oAtributos.AddAtrEsp Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).AnyoProce, Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).GMN1Proce, Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).CodProce, Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).Atrib, 2, Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).codgrupo, , , , Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").CellValue(vbm))).Orden
                End If
            End If
            
        Next i
    


      teserror = oAtributos.GuardarOrdenAtribEspecificacion

''
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Unload frmESPERA
        Screen.MousePointer = vbNormal
       Exit Sub
    End If

    'Pongo la fecact a los atributos
    For Each oatrib In oAtributos
        If sdbgAtributos.Columns("AMBITO").Value = 1 Then
            Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(oatrib.Atrib)).FECACT = oatrib.FECACT
        Else
            Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(oatrib.Atrib)).FECACT = oatrib.FECACT
        End If
    Next
    Set oAtributos = Nothing


End Sub

Private Sub cmdSalvarEsp_Click()
    Dim Item As MSComctlLib.listItem
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
    Dim node As MSComctlLib.node
    Dim arrData() As Byte
    
    On Error GoTo Cancelar:

    Set node = Parent.tvwProce.selectedItem
    
    If node Is Nothing Then Exit Sub
    
    If g_oObjetoSeleccionado Is Nothing Then Exit Sub

    If node.Tag = "Especificaciones" Then
        'ESPECIFICACIONES DE PROCESO
    
        Set Item = lstvwEsp.selectedItem
    
        If Item Is Nothing Then
            oMensajes.SeleccioneFichero
        Else
            cmmdEsp.DialogTitle = Parent.m_sIdiGuardar
            cmmdEsp.Filter = Parent.m_sIdiTipoOrig & "|*.*"
            cmmdEsp.filename = Item.Text
            cmmdEsp.ShowSave
    
            sFileName = cmmdEsp.filename
            sFileTitle = cmmdEsp.FileTitle
            If sFileTitle = "" Then
                oMensajes.NoValido Parent.m_sIdiArchivo
                Set node = Nothing
                Exit Sub
            End If
    
            ' Cargamos el contenido en la esp.
            Screen.MousePointer = vbHourglass
            Set oEsp = g_oObjetoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
            teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspProceso)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Set node = Nothing
                Exit Sub
            End If
           
            oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspProceso, oEsp.DataSize, sFileName
        End If
    
    Else
        If g_oObjetoSeleccionado Is Nothing Then Exit Sub
        
        Screen.MousePointer = vbNormal
    
        Set Item = lstvwEsp.selectedItem
    
        If Item Is Nothing Then
            oMensajes.SeleccioneFichero
        Else
            cmmdEsp.DialogTitle = Parent.m_sIdiGuardar
            cmmdEsp.Filter = Parent.m_sIdiTipoOrig & "|*.*"
            cmmdEsp.filename = Item.Text
            cmmdEsp.ShowSave
    
            sFileName = cmmdEsp.filename
            sFileTitle = cmmdEsp.FileTitle
            If sFileTitle = "" Then
                oMensajes.NoValido Parent.m_sIdiArchivo
                Set node = Nothing
                Exit Sub
            End If
       
            ' Cargamos el contenido en la esp.
            Screen.MousePointer = vbHourglass
            Set oEsp = g_oObjetoSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
            teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspGrupo)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Set node = Nothing
                Exit Sub
            End If
    
            oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspGrupo, oEsp.DataSize, sFileName
        End If
    End If
    
    Set oEsp = Nothing
    Set node = Nothing
    Screen.MousePointer = vbNormal
    Exit Sub

Cancelar:

    Screen.MousePointer = vbNormal
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        'Close DataFile
        g_oObjetoSeleccionado.ActualizadoProceso
        Set oEsp = Nothing
        Set node = Nothing
    End If
End Sub

Public Sub lblEsp_Caption(Texto As String)
    lblEsp.caption = Texto
End Sub

Public Sub cmmdEsp_DialogTitle(Texto As String)
    cmmdEsp.DialogTitle = Texto
End Sub

Public Sub lblAdjuntos_Caption(Texto As String)
    lblAdjuntos.caption = Texto
End Sub


Public Sub lstvwesp_ColumnHeaders(columna As Integer, Texto As String)
    lstvwEsp.ColumnHeaders(columna).Text = Texto
End Sub



Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns("Valor").Value = Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond

    If Text = "" Then
        If sdbgAtributos.Columns("OBLIG").Value Then ' es un atributo oblig
            RtnPassed = False
            m_bSalirDeEdicion = False
            oMensajes.NoValido sdbgAtributos.Columns("VALOR").caption
        Else
            RtnPassed = True
        End If
        Exit Sub
    End If
    
    Dim bExiste As Boolean
    bExiste = False
    
    Select Case sdbgAtributos.Columns("AMBITO").Value
    Case 1
        Select Case sdbgAtributos.Columns("TIPO_DATOS").Value
        Case 1, 2, 3
            Set oAtributo = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value)
                
                If oAtributo Is Nothing Then
                    'MsgBox m_sMensajes(17), vbInformation, "FULLSTEP"
                Else
                Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value).CargarListaDeValores
                For Each oLista In Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value).ListaPonderacion
                    If oLista.ValorLista = Text Then
                        bExiste = True
                        Exit For
                    End If
                Next
                End If
            
        Case 4
            
            If Text = "" Or Text = m_sIdiTrue Or Text = m_sIdiFalse Then
                bExiste = True
            End If
        End Select
   Case 2
        Select Case sdbgAtributos.Columns("TIPO_DATOS").Value
        Case 1, 2, 3
            Set oAtributo = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value)
                
                If oAtributo Is Nothing Then
                    'MsgBox m_sMensajes(17), vbInformation, "FULLSTEP"
                Else
                Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value).CargarListaDeValores
                For Each oLista In Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value).ListaPonderacion
                    If oLista.ValorLista = Text Then
                        bExiste = True
                        Exit For
                    End If
                Next
                End If
            
        Case 4
            
            If Text = "" Or Text = m_sIdiTrue Or Text = m_sIdiFalse Then
                bExiste = True
            End If
        End Select
   End Select
   
    If Not bExiste Then
        sdbgAtributos.Columns("VALOR").Text = ""

        oMensajes.NoValido sdbgAtributos.Columns("VALOR").caption
        RtnPassed = False
        m_bSalirDeEdicion = False
        Exit Sub
    End If
    RtnPassed = True
 

End Sub

''' <summary>
''' Evento que salta al hacer click en el boton de un atruto de texto largo
''' </summary>
''' <param name="Cancel">Click en un atributo de tipo texto largo</param>
''' <returns></returns>
''' <remarks>Llamada desde;Evento Tiempo m�ximo</remarks>

Private Sub sdbgAtributos_BtnClick()
    If sdbgAtributos.Col < 0 Then Exit Sub
    
    If sdbgAtributos.Columns(sdbgAtributos.Col).Name = "VALOR" Then
    
        If Parent.cmdModoEdicionAtriEsp.caption = Parent.m_sIdiEdicion Then
            frmATRIBDescr.g_bEdicion = False
        Else
            frmATRIBDescr.g_bEdicion = True
        End If
        frmATRIBDescr.caption = sdbgAtributos.Columns("COD").Value & ": " & sdbgAtributos.Columns("DEN").Value
        frmATRIBDescr.txtDescr.Text = sdbgAtributos.Columns(sdbgAtributos.Col).Value
        frmATRIBDescr.g_sOrigen = "ATRIB_ESP"
        frmATRIBDescr.Show 1
        
        
        If sdbgAtributos.DataChanged Then
            If Parent.g_oGrupoSeleccionado Is Nothing Then
                Set Parent.g_oAtributoEnEdicion = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value))
            Else
                Set Parent.g_oAtributoEnEdicion = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value))
            End If
            sdbgAtributos.Update
        End If

    End If
    
End Sub

Private Sub sdbgAtributos_RowLoaded(ByVal Bookmark As Variant)
If sdbgAtributos.Columns("LISTA_EXTERNA").Value Then
    sdbgAtributos.Columns("VALOR").CellStyleSet "styAzul"
End If
End Sub

Private Sub sdbgatributos_Scroll(Cancel As Integer)
    sdbgAtributos.Columns("Valor").DropDownHwnd = 0
End Sub

Private Sub SSSplitterEsp_Resize(ByVal BorderPanes As SSSplitter.Panes)
    ArrangeSplit
End Sub

Private Sub txtEspecificacion_LostFocus()
    'Forzar el validate porque a veces no salta
    If Parent.Accion = ACCProceEspMod Or Parent.Accion = ACCProceGrupoEspMod Then
        txtEsp_validate False
    End If
End Sub

Public Sub MostrarBotones()
    cmdAnyadirEsp.Visible = True
    cmdModificarEsp.Visible = True
    cmdEliminarEsp.Visible = True
End Sub
Public Sub OcultarBotones()
    cmdAnyadirEsp.Visible = False
    cmdModificarEsp.Visible = False
    cmdEliminarEsp.Visible = False
End Sub

Public Sub MostrarBotonesAtrEsp()
    cmdAnyaOEAtrib.Visible = True
    cmdElimOEAtrib.Visible = True
    cmdOrden(0).Visible = True
    cmdOrden(1).Visible = True
End Sub
Public Sub OcultarBotonesAtrEsp()
    cmdAnyaOEAtrib.Visible = False
    cmdElimOEAtrib.Visible = False
    cmdOrden(0).Visible = False
    cmdOrden(1).Visible = False
End Sub

Public Sub HabilitarBotones(ByVal modo As Boolean)
    
    cmdAnyaOEAtrib.Enabled = modo
    cmdElimOEAtrib.Enabled = modo
    
    cmdAnyadirEsp.Enabled = modo
    cmdModificarEsp.Enabled = modo
    cmdEliminarEsp.Enabled = modo
    cmdOrden(0).Enabled = modo
    cmdOrden(1).Enabled = modo

    sdbgAtributos.Columns("INTERNO").Locked = Not modo
    sdbgAtributos.Columns("PEDIDO").Locked = Not modo
    sdbgAtributos.Columns("VALOR").Locked = Not modo
    sdbgAtributos_RowColChange sdbgAtributos.Row, sdbgAtributos.Col
    txtEspecificacion.Locked = Not modo
End Sub

Public Sub ConfigurarCommonDialog()
    cmmdEsp.FLAGS = cdlOFNHideReadOnly
End Sub

Function MostrarCommonDialog(titulo As String, filtro As String, valido As String, Optional ByVal bOpen As Boolean = False) As String
    'jpa Incidencia 149
    cmmdEsp.CancelError = True

    On Error Resume Next
    'cmmdEsp.FileTitle = ""
    cmmdEsp.DialogTitle = titulo
    cmmdEsp.Filter = filtro
    cmmdEsp.filename = ValidFilename(valido)
    If bOpen Then
        cmmdEsp.ShowOpen
    Else
        cmmdEsp.ShowSave
    End If

    If err.Number = cdlCancel Then
        ' The user canceled.
        MostrarCommonDialog = ""
        Exit Function
    End If
    On Error GoTo 0
    
    If cmmdEsp.FileTitle = "" Then
        MostrarCommonDialog = ""
        Exit Function
    End If

    MostrarCommonDialog = cmmdEsp.filename
    'fin jpa Incidencia 149
    
End Function

Public Sub PrepararAnyadir()
    txtEsp = ""
End Sub


    
Public Sub LimpiarEspecificaciones()
    lstvwEsp.ListItems.clear
End Sub

Public Sub AnyadirEspecificaciones(especificaciones As Object)

    Dim oEsp As CEspecificacion
    lstvwEsp.ListItems.clear

    For Each oEsp In especificaciones
        lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & Parent.m_skb
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next

    Set oEsp = Nothing
    
End Sub

Public Sub LimpiarAtributos()
    sdbgAtributos.RemoveAll
End Sub

Public Sub Arrange(W As Integer, h As Integer)
        
        If Me Is Nothing Then
            Exit Sub
        End If
        
        Width = W
        Height = h * 1.08
        
        lblEsp.Width = Width
        
        SSSplitterEsp.Height = (Height - lblEsp.Height)
        SSSplitterEsp.Width = Width
        
        SSSplitterEsp.Top = lblEsp.Height
        SSSplitterEsp.Left = 0
        
        SSSplitterEsp.Panes.Item(0).Height = SSSplitterEsp.Height / 3
        SSSplitterEsp.Panes.Item(1).Height = SSSplitterEsp.Height / 3
        SSSplitterEsp.Panes.Item(2).Height = SSSplitterEsp.Height / 3
        
        ArrangeSplit
        
        
        
End Sub
Public Sub ArrangeSplit()

    If sdbgAtributos.Rows > 0 Then
        Dim vbm As Variant
        vbm = sdbgAtributos.Bookmark
        sdbgAtributos.Bookmark = 0
    End If
    
        picSplit1.Height = SSSplitterEsp.Panes.Item(0).Height
        picSplit2.Height = SSSplitterEsp.Panes.Item(1).Height
        picSplit3.Height = SSSplitterEsp.Panes.Item(2).Height
        
        picSplit1.Width = SSSplitterEsp.Panes.Item(0).Width
        picSplit2.Width = SSSplitterEsp.Panes.Item(1).Width
        picSplit3.Width = SSSplitterEsp.Panes.Item(2).Width
        
        If picSplit1.Width > 400 Then
            txtEspecificacion.Width = picSplit1.Width - 400
        Else
            txtEspecificacion.Width = 0
        End If
        txtEspecificacion.Left = 200
        txtEspecificacion.Top = 200
        If picSplit1.Height > 400 Then
            txtEspecificacion.Height = picSplit1.Height - 400
        Else
            txtEspecificacion.Height = 0
        End If
        
        If picSplit3.Width > 400 Then
            lstvwEsp.Width = picSplit3.Width - 400
        Else
            lstvwEsp.Width = 0
        End If
        lstvwEsp.Left = 200
        
        If picSplit2.Width > 400 Then
            sdbgAtributos.Width = picSplit2.Width - 400
        Else
            sdbgAtributos.Width = 0
        End If
        sdbgAtributos.Left = 200
        sdbgAtributos.Top = 450
        If picSplit2.Height > 550 Then
            sdbgAtributos.Height = picSplit2.Height - 550
        Else
            sdbgAtributos.Height = 0
        End If
        
        picBotonesAtributos.Top = -200
        picBotonesAtributos.Left = 80
        If picSplit2.Width > 320 Then
            picBotonesAtributos.Width = picSplit2.Width - 320
        Else
            picBotonesAtributos.Width = 0
        End If
        
        picBotonesAtributos.Left = 80
        cmdElimOEAtrib.Left = picBotonesAtributos.Width - 300
        cmdAnyaOEAtrib.Left = picBotonesAtributos.Width - 650
        cmdOrden(1).Left = picBotonesAtributos.Width - 1150
        cmdOrden(0).Left = picBotonesAtributos.Width - 1500
        
        If picSplit3.Height > 1000 Then
            lstvwEsp.Height = picSplit3.Height - 1000
        Else
            lstvwEsp.Height = 0
        End If
        
        lblAdjuntos.Top = 100
        lblAdjuntos.Left = 200
        
        picBotonesAdjuntos.Left = picSplit3.Width - picBotonesAdjuntos.Width - 200
        picBotonesAdjuntos.Top = picSplit3.Height - 600
        
        lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.2
        lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.12
        lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.48
        lstvwEsp.ColumnHeaders(4).Width = lstvwEsp.Width * 0.195
        If sdbgAtributos.Width > 600 Then
            sdbgAtributos.Columns("Interno").Width = (sdbgAtributos.Width - 600) * (3 / 44)
            sdbgAtributos.Columns("Pedido").Width = (sdbgAtributos.Width - 600) * (3 / 44)
            sdbgAtributos.Columns("COD").Width = (sdbgAtributos.Width - 600) * (8 / 44)
            sdbgAtributos.Columns("DEN").Width = (sdbgAtributos.Width - 600) * (19 / 44)
            sdbgAtributos.Columns("VALOR").Width = (sdbgAtributos.Width - 600) * (11 / 44)
        Else
            sdbgAtributos.Columns("Interno").Width = 0
            sdbgAtributos.Columns("Pedido").Width = 0
            sdbgAtributos.Columns("COD").Width = 0
            sdbgAtributos.Columns("DEN").Width = 0
            sdbgAtributos.Columns("VALOR").Width = 0
        
        End If
     If sdbgAtributos.Rows > 0 Then
        'Recup�ro el bookmark y si no se ve la fila la pongo la priemra visible
        sdbgAtributos.Bookmark = vbm
        If sdbgAtributos.AddItemRowIndex(sdbgAtributos.Bookmark) < sdbgAtributos.AddItemRowIndex(sdbgAtributos.FirstRow) Or sdbgAtributos.AddItemRowIndex(sdbgAtributos.Bookmark) > sdbgAtributos.AddItemRowIndex(sdbgAtributos.FirstRow) + sdbgAtributos.VisibleRows - 1 Then
            sdbgAtributos.FirstRow = sdbgAtributos.Bookmark
        End If
    End If
    
    DoEvents
    sdbgAtributos.Columns("Valor").DropDownHwnd = 0
        
End Sub

Public Sub txtEsp_Text(Texto As String)
    txtEspecificacion.Text = Texto
End Sub


Public Function cmmdEsp_FileTitle() As String
    cmmdEsp_FileTitle = cmmdEsp.FileTitle
End Function


' recibe el atributo seleccionado en frmATRIB

Public Sub TransferirAtributoSeleccionado(ByVal miAtributo As CAtributo)
    If g_oObjetoSeleccionado.AtributosEspecificacion Is Nothing Then
        Set g_oObjetoSeleccionado.AtributosEspecificacion = oFSGSRaiz.Generar_CAtributos
    End If

    sdbgAtributos.AddNew
    sdbgAtributos.Columns("COD").Text = miAtributo.Cod
    sdbgAtributos.Columns("DEN").Value = miAtributo.Den
    sdbgAtributos.Columns("VALOR").Value = miAtributo.valor
    sdbgAtributos.Columns("ID").Value = miAtributo.Id
    sdbgAtributos.Columns("MAX").Value = miAtributo.Maximo
    sdbgAtributos.Columns("MIN").Value = miAtributo.Minimo
    sdbgAtributos.Columns("TIPO_INTRO").Value = miAtributo.TipoIntroduccion
    FuerzaPosicion
    sdbgAtributos.Update
    

End Sub


Public Function actualizarYSalirAtrib() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error.
    
    Dim bError As Boolean
    
    If sdbgAtributos.DataChanged = True Then
        bError = False
        If sdbgAtributos.Row = 0 Then
            sdbgAtributos.MoveNext
            DoEvents
            If bError Then
                actualizarYSalirAtrib = True
                Exit Function
            Else
                sdbgAtributos.MovePrevious
            End If
        Else
            sdbgAtributos.MovePrevious
            DoEvents
            If bError Then
                actualizarYSalirAtrib = True
                Exit Function
            Else
                sdbgAtributos.MoveNext
            End If
        End If
    Else
        actualizarYSalirAtrib = False
    End If
End Function



    
' eventos del grid de atributos
    
    
Private Sub sdbgAtributos_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
   If IsEmpty(sdbgAtributos.RowBookmark(sdbgAtributos.Row)) Then
        sdbgAtributos.Bookmark = sdbgAtributos.RowBookmark(sdbgAtributos.Row - 1)
    Else
        sdbgAtributos.Bookmark = sdbgAtributos.RowBookmark(sdbgAtributos.Row)
    End If
    
End Sub

Private Sub sdbgatributos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgatributos_AfterUpdate(RtnDispErrMsg As Integer)
    ImagenNodoEspecificaciones
End Sub

''' <summary>
''' Evento que salta al hacer cambios en la grid
''' </summary>
''' <param name="Cancel">Cancelacion del cambio</param>
''' <returns></returns>
''' <remarks>Llamada desde;Evento Tiempo m�ximo</remarks>

Private Sub sdbgatributos_BeforeUpdate(Cancel As Integer)

Dim teserror As TipoErrorSummit
Dim m_oAtributoAnyadir As CAtributo
Dim m_oAtribEnEdicion As CAtributo


If Not sdbgAtributos.DataChanged Then Exit Sub

Cancel = False

If sdbgAtributos.Columns("Valor").Text <> "" Then

    If SQLBinaryToBoolean(StrToDbl0(sdbgAtributos.Columns("TIPO_INTRO").Value)) Then
        sdbddValor_ValidateList sdbgAtributos.Columns("Valor").Text, 0
    End If
    Select Case UCase(sdbgAtributos.Columns("TIPO_DATOS").Text)

        Case 2 'Numero
                    If (Not IsNumeric(sdbgAtributos.Columns("Valor").Text)) Then
                        oMensajes.AtributoValorNoValido ("TIPO2")
                        Cancel = True
                        GoTo Salir

                    Else
                        If sdbgAtributos.Columns("MIN").Text <> "" And sdbgAtributos.Columns("MAX").Text <> "" Then
                            If StrToDbl0(sdbgAtributos.Columns("MIN").Text) > StrToDbl0(sdbgAtributos.Columns("Valor").Text) Or StrToDbl0(sdbgAtributos.Columns("MAX").Text) < StrToDbl0(sdbgAtributos.Columns("Valor").Text) Then
                                oMensajes.ValorEntreMaximoYMinimo sdbgAtributos.Columns("MIN").Text, sdbgAtributos.Columns("MAX").Text
                                Cancel = True
                                GoTo Salir
                            End If
                        End If
                    
                    End If



        Case 3 'Fecha
                        If (Not IsDate(sdbgAtributos.Columns("Valor").Text) And sdbgAtributos.Columns("Valor").Text <> "") Then
                            oMensajes.AtributoValorNoValido ("TIPO3")
                            Cancel = True
                            GoTo Salir

                        Else
                            If sdbgAtributos.Columns("MIN").Text <> "" And sdbgAtributos.Columns("MAX").Text <> "" Then
                                If CDate(sdbgAtributos.Columns("MIN").Text) > CDate(sdbgAtributos.Columns("Valor").Text) Or CDate(sdbgAtributos.Columns("MAX").Text) < CDate(sdbgAtributos.Columns("Valor").Text) Then
                                    oMensajes.ValorEntreMaximoYMinimo sdbgAtributos.Columns("MIN").Text, sdbgAtributos.Columns("MAX").Text
                                    Cancel = True
                                    GoTo Salir
                                End If
                            End If
                    End If
    End Select

End If
If sdbgAtributos.IsAddRow Then
         
    Set m_oAtributoAnyadir = oFSGSRaiz.Generar_CAtributo
    m_oAtributoAnyadir.Id = sdbgAtributos.Columns("ATRIB").Value
    
    m_oAtributoAnyadir.AnyoProce = Parent.g_oProcesoSeleccionado.Anyo
    m_oAtributoAnyadir.CodProce = Parent.g_oProcesoSeleccionado.Cod
    m_oAtributoAnyadir.GMN1Proce = Parent.g_oProcesoSeleccionado.GMN1Cod
    m_oAtributoAnyadir.codgrupo = sdbgAtributos.Columns("GRUPO").Value
    m_oAtributoAnyadir.idGrupo = sdbgAtributos.Columns("GRUPOID").Value
    
    m_oAtributoAnyadir.Item = sdbgAtributos.Columns("Item").Value
    m_oAtributoAnyadir.Atrib = sdbgAtributos.Columns("ATRIB").Value
    m_oAtributoAnyadir.Cod = sdbgAtributos.Columns("COD").Value
    m_oAtributoAnyadir.Den = sdbgAtributos.Columns("DEN").Value
    
    m_oAtributoAnyadir.Orden = sdbgAtributos.Rows
    

    m_oAtributoAnyadir.ambito = sdbgAtributos.Columns("AMBITO").Value

    
    Select Case CInt(sdbgAtributos.Columns("TIPO_DATOS").Text)
    
        Case 1: m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoString
        Case 2: m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoNumerico
        Case 3: m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoFecha
        Case 4: m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoBoolean
                      
    End Select
    

    If SQLBinaryToBoolean(StrToDbl0(sdbgAtributos.Columns("TIPO_INTRO").Value)) Then
        m_oAtributoAnyadir.TipoIntroduccion = Introselec
    Else
        m_oAtributoAnyadir.TipoIntroduccion = IntroLibre
    End If
    m_oAtributoAnyadir.Maximo = StrToNull(sdbgAtributos.Columns("MAX").Value)
    m_oAtributoAnyadir.Minimo = StrToNull(sdbgAtributos.Columns("MIN").Value)

    
    If sdbgAtributos.Columns("INTERNO").Value = "0" Or sdbgAtributos.Columns("INTERNO").Value = "" Then
        m_oAtributoAnyadir.interno = False
    Else
        m_oAtributoAnyadir.interno = True
    End If
    

        Select Case StrToDbl0(sdbgAtributos.Columns("TIPO_DATOS").Value)
            
            Case 1: m_oAtributoAnyadir.valorText = sdbgAtributos.Columns("Valor").Value
            Case 2:
                    If sdbgAtributos.Columns("Valor").Value = "" Then
                        m_oAtributoAnyadir.valorNum = Null
                    Else
                        m_oAtributoAnyadir.valorNum = sdbgAtributos.Columns("Valor").Value
                    End If
            Case 3:
                    m_oAtributoAnyadir.valorFec = sdbgAtributos.Columns("Valor").Value
                    If sdbgAtributos.Columns("Valor").Value = "" Then
                        m_oAtributoAnyadir.valorFec = Null
                    Else
                        m_oAtributoAnyadir.valorFec = sdbgAtributos.Columns("Valor").Value
                    End If
            Case 4:
                Select Case sdbgAtributos.Columns("Valor").Value
                    Case m_sIdiTrue: m_oAtributoAnyadir.valorBool = 1
                    Case m_sIdiFalse: m_oAtributoAnyadir.valorBool = 0

                End Select
        End Select

    

    m_oAtributoAnyadir.FECACT = sdbgAtributos.Columns("ID_A").Value  'Fecha Modificaci�n Atributo
    
    teserror = m_oAtributoAnyadir.AnyadirAtributoEspecificacion
            



    If m_oAtributoAnyadir.ambito = AmbProceso Then
        If Parent.g_oProcesoSeleccionado.AtributosEspecificacion Is Nothing Then
            Set Parent.g_oProcesoSeleccionado.AtributosEspecificacion = oFSGSRaiz.Generar_CAtributos
        End If
        Parent.g_oProcesoSeleccionado.AtributosEspecificacion.AddAtrEsp m_oAtributoAnyadir.AnyoProce, m_oAtributoAnyadir.GMN1Proce, m_oAtributoAnyadir.CodProce, m_oAtributoAnyadir.Atrib, m_oAtributoAnyadir.ambito, m_oAtributoAnyadir.codgrupo, m_oAtributoAnyadir.Item, SQLBinaryToBoolean(m_oAtributoAnyadir.interno), SQLBinaryToBoolean(m_oAtributoAnyadir.pedido), m_oAtributoAnyadir.Orden, m_oAtributoAnyadir.valorNum, m_oAtributoAnyadir.valorText, m_oAtributoAnyadir.valorFec, SQLBinaryToBoolean(m_oAtributoAnyadir.valorBool), m_oAtributoAnyadir.Cod, m_oAtributoAnyadir.Den, m_oAtributoAnyadir.Descripcion, m_oAtributoAnyadir.Tipo, m_oAtributoAnyadir.Minimo, m_oAtributoAnyadir.Maximo, m_oAtributoAnyadir.TipoIntroduccion, m_oAtributoAnyadir.FECACT
    Else 'AmbGrupo
        If Parent.g_oGrupoSeleccionado.AtributosEspecificacion Is Nothing Then
            Set Parent.g_oGrupoSeleccionado.AtributosEspecificacion = oFSGSRaiz.Generar_CAtributos
        End If
        Parent.g_oGrupoSeleccionado.AtributosEspecificacion.AddAtrEsp m_oAtributoAnyadir.AnyoProce, m_oAtributoAnyadir.GMN1Proce, m_oAtributoAnyadir.CodProce, m_oAtributoAnyadir.Atrib, m_oAtributoAnyadir.ambito, m_oAtributoAnyadir.codgrupo, m_oAtributoAnyadir.Item, SQLBinaryToBoolean(m_oAtributoAnyadir.interno), SQLBinaryToBoolean(m_oAtributoAnyadir.pedido), m_oAtributoAnyadir.Orden, m_oAtributoAnyadir.valorNum, m_oAtributoAnyadir.valorText, m_oAtributoAnyadir.valorFec, SQLBinaryToBoolean(m_oAtributoAnyadir.valorBool), m_oAtributoAnyadir.Cod, m_oAtributoAnyadir.Den, m_oAtributoAnyadir.Descripcion, m_oAtributoAnyadir.Tipo, m_oAtributoAnyadir.Minimo, m_oAtributoAnyadir.Maximo, m_oAtributoAnyadir.TipoIntroduccion, m_oAtributoAnyadir.FECACT, m_oAtributoAnyadir.idGrupo
    
    End If

Else  'No es addrow
    'Realizar las comprobaciones necesarias para ver si el texto introducido es correcto
    'sdbgatributos.CancelUpdate
    Dim valor As Variant
    
    If sdbgAtributos.Col <> -1 Then

    If sdbgAtributos.Columns("AMBITO").Value = AmbProceso Then
        Set m_oAtribEnEdicion = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value)
    Else
        Set m_oAtribEnEdicion = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value)
    End If
    
    If Parent.g_oProcesoSeleccionado.Estado > ConItemsSinValidar Then
        If Not IsNull(Parent.g_oProcesoSeleccionado.Plantilla) Then
            If sdbgAtributos.Columns("OBLIG").Value Then ' es un atributo oblig
                If sdbgAtributos.Columns("VALIDACION").Value = TValidacionAtrib.Apertura Then
                    If IsNull(sdbgAtributos.Columns("VALOR").Value) Or sdbgAtributos.Columns("VALOR").Value = "" Then
                        oMensajes.ImposibleEliminarValorAtribOblig
                        Select Case sdbgAtributos.Columns("TIPO_DATOS").Value
                            Case 1: sdbgAtributos.Columns("Valor").Text = m_oAtribEnEdicion.valorText
                                
                            Case 2: sdbgAtributos.Columns("Valor").Text = m_oAtribEnEdicion.valorNum
                            
                            Case 3: sdbgAtributos.Columns("Valor").Text = m_oAtribEnEdicion.valorFec
                            Case 4:
                                    Select Case m_oAtribEnEdicion.valorBool
                                        Case 1: sdbgAtributos.Columns("Valor").Text = m_sIdiTrue
                                        Case 0: sdbgAtributos.Columns("Valor").Text = m_sIdiFalse
                                        Case True: sdbgAtributos.Columns("Valor").Text = m_sIdiTrue
                                        Case False: sdbgAtributos.Columns("Valor").Text = m_sIdiFalse
                                    End Select
                        End Select
                        Cancel = True
                        GoTo Salir
                    End If
                End If
            End If
        End If
    End If

    If Parent.g_oProcesoSeleccionado.AtributosEspValidados Then
        If Not IsNull(Parent.g_oProcesoSeleccionado.Plantilla) Then
            If sdbgAtributos.Columns("OBLIG").Value Then  ' es un atributo oblig
                If IsNull(sdbgAtributos.Columns("VALOR").Value) Or sdbgAtributos.Columns("VALOR").Value = "" Then
                    oMensajes.ImposibleEliminarValorAtribOblig
                    Select Case sdbgAtributos.Columns("TIPO_DATOS").Value
                        Case 1: sdbgAtributos.Columns("Valor").Text = m_oAtribEnEdicion.valorText
                            
                        Case 2: sdbgAtributos.Columns("Valor").Text = m_oAtribEnEdicion.valorNum
                        
                        Case 3: sdbgAtributos.Columns("Valor").Text = m_oAtribEnEdicion.valorFec
                        Case 4:
                                Select Case m_oAtribEnEdicion.valorBool
                                    Case 1: sdbgAtributos.Columns("Valor").Text = m_sIdiTrue
                                    Case 0: sdbgAtributos.Columns("Valor").Text = m_sIdiFalse
                                    Case True: sdbgAtributos.Columns("Valor").Text = m_sIdiTrue
                                    Case False: sdbgAtributos.Columns("Valor").Text = m_sIdiFalse
                                End Select
                    End Select
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
    End If
    
    Select Case sdbgAtributos.Columns("TIPO_DATOS").Value
        Case 1: valor = sdbgAtributos.Columns("Valor").Text
            
        Case 2: valor = sdbgAtributos.Columns("Valor").Text
        Case 3: valor = sdbgAtributos.Columns("Valor").Text
        Case 4:
                Select Case sdbgAtributos.Columns("Valor").Text
                    Case m_sIdiTrue: valor = 1
                    Case m_sIdiFalse: valor = 0
                End Select
    End Select

    teserror = m_oAtribEnEdicion.ModificarValorEspecificacion(valor)
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        sdbgAtributos.Columns("Valor").Text = valor
        Cancel = True
    End If

    End If
    
End If
    sdbgAtributos.Columns("Valor").DropDownHwnd = 0
Exit Sub
Salir:
    m_bSalirDeEdicion = False
    If Parent.Visible Then sdbgAtributos.SetFocus
End Sub

Public Sub FuerzaPosicion()
    sdbgatributos_Change
End Sub

Private Sub sdbgatributos_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos

    Dim teserror As TipoErrorSummit
    Dim bDato As Boolean
    
If (sdbgAtributos.Columns(sdbgAtributos.Col).Name = "Interno") Then
    If Not sdbgAtributos.IsAddRow Then
        If sdbgAtributos.Columns("AMBITO").Value = 1 Then
            bDato = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value)).interno
            teserror = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value)).ModificarInternoEspecificacion(sdbgAtributos.Columns("Interno").Value)
        Else
            bDato = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value)).interno
            teserror = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value)).ModificarInternoEspecificacion(sdbgAtributos.Columns("Interno").Value)
        End If
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            sdbgAtributos.Columns("Interno").Value = bDato
        End If
        sdbgAtributos.Update
        Exit Sub
    End If
End If

If (sdbgAtributos.Columns(sdbgAtributos.Col).Name = "Pedido") Then
    If Not sdbgAtributos.IsAddRow Then
        If sdbgAtributos.Columns("AMBITO").Value = 1 Then
            bDato = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value)).pedido
            teserror = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value)).ModificarPedidoEspecificacion(sdbgAtributos.Columns("Pedido").Value)
        Else
            bDato = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value)).pedido
            teserror = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(CStr(sdbgAtributos.Columns("ATRIB").Value)).ModificarPedidoEspecificacion(sdbgAtributos.Columns("Pedido").Value)
        End If
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            sdbgAtributos.Columns("Pedido").Value = bDato
        End If
        sdbgAtributos.Update
        Exit Sub
    End If
End If


End Sub

Private Sub sdbgAtributos_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape Then

        If sdbgAtributos.DataChanged = False Then

            sdbgAtributos.CancelUpdate
            sdbgAtributos.DataChanged = False

            If Not m_oAtribEnEdicion Is Nothing Then
                Set m_oAtribEnEdicion = Nothing
            End If

        End If

    End If

End Sub

''' <summary>
''' El grid tiene una columna q en unas lineas es combo y en otras no es combo. Aqui se establece el DropDownHwnd, pero solo si la columna es editable
''' Hay q hacerlo solo si editable pq algo le pasa al VB6 con columnas en unas lineas combo en otras no combo q, a veces, si usas scroll le da por
''' perder los datos en pantalla y bd. Parece q no procesa bien eso de tanta asignaci�n al DropDownHwnd poniendo y quitando.
''' </summary>
''' <param name="LastRow">Ultima fila</param>
''' <param name="LastCol">Ultima columna</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    'Si esta en modo edici�n salimos
    
    If sdbgAtributos.Columns("VALOR").Locked And cmdAnyaOEAtrib.Enabled = False Then
        If sdbgAtributos.Columns("TIPO_INTRO").Value = 0 And sdbgAtributos.Columns("TIPO_DATOS").Value = 1 Then
            sdbgAtributos.Columns("Valor").Style = ssStyleEditButton
        Else
            sdbgAtributos.Columns("Valor").Style = ssStyleEdit
        End If
        Exit Sub
    End If
    If sdbgAtributos.Columns("TIPO_INTRO").Value = 0 Then 'Libre
        If sdbgAtributos.Columns("TIPO_DATOS").Value = TipoBoolean Then
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            sdbgAtributos.Columns("Valor").DropDownHwnd = sdbddValor.hWnd
            sdbddValor.Enabled = True
            sdbgAtributos.Columns("Valor").Locked = False
        Else
            sdbgAtributos.Columns("Valor").DropDownHwnd = 0
            sdbddValor.Enabled = False
            sdbgAtributos.Columns("Valor").Locked = False
            If sdbgAtributos.Columns("TIPO_DATOS").Value = 1 Then
                sdbgAtributos.Columns("Valor").Style = ssStyleEditButton
            Else
                sdbgAtributos.Columns("Valor").Style = ssStyleEdit
            End If
        End If
    Else 'Lista
        sdbddValor.RemoveAll
        sdbddValor.AddItem ""
        sdbgAtributos.Columns("Valor").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
        sdbddValor.DroppedDown = True
        sdbddValor_DropDown
        sdbddValor.DroppedDown = True
    End If
    If sdbgAtributos.Columns("LISTA_EXTERNA").Value Then
        sdbgAtributos.Columns("Valor").DropDownHwnd = 0
        sdbddValor.Enabled = False
        sdbgAtributos.Columns("Valor").Locked = True
    End If
End Sub
Private Sub sdbddValor_DropDown()

    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1


    If sdbgAtributos.Rows = 0 Then Exit Sub
    
    If sdbgAtributos.Columns("VALOR").Locked Then
        sdbddValor.DroppedDown = False
        Exit Sub
    End If
    sdbddValor.RemoveAll
    sdbddValor.DroppedDown = True
    
    Select Case sdbgAtributos.Columns("AMBITO").Value
    Case 1
        Select Case sdbgAtributos.Columns("TIPO_DATOS").Value
        Case 1, 2, 3
            If Not Parent.g_oProcesoSeleccionado Is Nothing Then
                If Not Parent.g_oProcesoSeleccionado.AtributosEspecificacion Is Nothing Then
                    Set oAtributo = Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value)
                
                    If oAtributo Is Nothing Then
                        'MsgBox m_sMensajes(17), vbInformation, "FULLSTEP"
                    Else
                        Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value).CargarListaDeValores
                        For Each oLista In Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value).ListaPonderacion
                            sdbddValor.AddItem iIndice & Chr(9) & oLista.ValorLista
                            iIndice = iIndice + 1
                        Next
                    End If
                End If
            End If
        Case 4
            sdbddValor.AddItem "" & Chr(9) & ""
            sdbddValor.AddItem 1 & Chr(9) & m_sIdiTrue
            sdbddValor.AddItem 0 & Chr(9) & m_sIdiFalse
        End Select
   Case 2
        Select Case sdbgAtributos.Columns("TIPO_DATOS").Value
        Case 1, 2, 3
            If Not Parent.g_oGrupoSeleccionado Is Nothing Then
                If Not Parent.g_oGrupoSeleccionado.AtributosEspecificacion Is Nothing Then
                    Set oAtributo = Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value)
                    
                    If oAtributo Is Nothing Then
                        'MsgBox m_sMensajes(17), vbInformation, "FULLSTEP"
                    Else
                        Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value).CargarListaDeValores
                        For Each oLista In Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Item(sdbgAtributos.Columns("ATRIB").Value).ListaPonderacion
                            sdbddValor.AddItem iIndice & Chr(9) & oLista.ValorLista
                            iIndice = iIndice + 1
                        Next
                    End If
                End If
            End If
        Case 4
            sdbddValor.AddItem "" & Chr(9) & ""
            sdbddValor.AddItem 1 & Chr(9) & m_sIdiTrue
            sdbddValor.AddItem 0 & Chr(9) & m_sIdiFalse
        End Select
   End Select

End Sub




Sub CargarRecursos()

Dim Ador As Ador.Recordset

On Error Resume Next
    
Set Ador = oGestorIdiomas.DevolverTextosDelModulo(CTL_ESPECIFICACIONES, basPublic.gParametrosInstalacion.gIdioma)
If Not Ador Is Nothing Then
    m_sIdiTrue = Ador(0).Value
    Ador.MoveNext
    m_sIdiFalse = Ador(0).Value
    Ador.MoveNext
    m_sIdiMayor = Ador(0).Value
    Ador.MoveNext
    m_sIdiManor = Ador(0).Value
    Ador.MoveNext
    m_sIdiEntre = Ador(0).Value
    Ador.MoveNext
    m_sIdiTexto = Ador(0).Value
    Ador.MoveNext
    m_sIdiNumerico = Ador(0).Value
    Ador.MoveNext
    m_sIdiBoolean = Ador(0).Value
    Ador.MoveNext
    m_sIdiInterno = Ador(0).Value
    Ador.MoveNext
    m_sIdiCodigo = Ador(0).Value
    Ador.MoveNext
    m_sIdiDenominacion = Ador(0).Value
    Ador.MoveNext
    m_sIdiValor = Ador(0).Value
    Ador.MoveNext
    m_sIdiPedido = Ador(0).Value
    Ador.MoveNext
    m_sIdiFecha = Ador(0).Value
    Ador.MoveNext
    m_sIdiAtributosEsp = Ador(0).Value
    Ador.MoveNext
    m_sIdiAtributosEspProceso = Ador(0).Value
    Ador.MoveNext
    m_sIdiAtributosEspGrupo = Ador(0).Value
    Ador.MoveNext
    lblAtributos = Ador(0).Value
End If

    sdbgAtributos.Columns("Interno").caption = m_sIdiInterno
    sdbgAtributos.Columns("Pedido").caption = m_sIdiPedido
    sdbgAtributos.Columns("COD").caption = m_sIdiCodigo
    sdbgAtributos.Columns("DEN").caption = m_sIdiDenominacion
    sdbgAtributos.Columns("VALOR").caption = m_sIdiValor
   
    
End Sub

    
Private Sub txtEspecificacion_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit

    If Parent.Accion = ACCProceEspMod Then
    
     Set Parent.m_oIBAseDatosEnEdicion = g_oObjetoSeleccionado
        If StrComp(NullToStr(g_oObjetoSeleccionado.esp), txtEspecificacion.Text, vbTextCompare) <> 0 Then
    
            g_oObjetoSeleccionado.esp = StrToNull(txtEspecificacion.Text)
            teserror = g_oObjetoSeleccionado.ActualizarEspecificaciones
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Cancel = True
                Exit Sub
            End If
            g_oObjetoSeleccionado.ActualizadoProceso
            'Registrar Parent.Accion
            basSeguridad.RegistrarAccion accionessummit.ACCProceEspMod, "Anyo:" & Parent.sdbcAnyo.Value & "GMN1:" & Parent.sdbcGMN1_4Cod.Value & "Cod:" & Parent.sdbcProceCod.Value
            
            ImagenNodoEspecificaciones
            
        End If
        m_bRespetarEspec = False
    End If

    If Parent.Accion = ACCProceGrupoEspMod Then
    
        Set Parent.m_oIBAseDatosEnEdicion = g_oObjetoSeleccionado
    
        If StrComp(NullToStr(g_oObjetoSeleccionado.esp), txtEspecificacion.Text, vbTextCompare) <> 0 Then
    
            g_oObjetoSeleccionado.esp = StrToNull(txtEspecificacion.Text)
            teserror = g_oObjetoSeleccionado.ActualizarEspecificaciones
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Cancel = True
                'Parent.Accion = ACCProceCon
                Set Parent.m_oIBAseDatosEnEdicion = Nothing
                'Parent.DesbloquearProceso
                Exit Sub
            End If
            g_oObjetoSeleccionado.ActualizadoGrupo
            'Registrar Parent.Accion
            basSeguridad.RegistrarAccion accionessummit.ACCProceGrupoEspMod, "Anyo:" & Parent.sdbcAnyo.Value & "GMN1:" & Parent.sdbcGMN1_4Cod.Value & "Cod:" & Parent.sdbcProceCod.Value
            
            
            ImagenNodoEspecificaciones

            
    
        End If
        m_bRespetarEspec = False
        Set Parent.m_oIBAseDatosEnEdicion = Nothing
    End If

    'Parent.Accion = ACCProceCon
    'Parent.DesbloquearProceso

End Sub

Private Sub UserControl_Initialize()
    CargarRecursos
    
    sdbgAtributos.Columns("COD").Locked = True
    sdbgAtributos.Columns("DEN").Locked = True
    HabilitarBotones False
    ConfigurarCommonDialog
    ' Arrange

End Sub

Sub RestaurarEspecificaciones(objeto As Object, Optional CargarEspecificacionesGenerales As Boolean = False)


    objeto.CargarEspGeneral



    txtEsp = NullToStr(objeto.esp)
    
    'Cargamos las especificaciones del proceso
    
    objeto.CargarTodasLasEspecificaciones True, False
    
    ObjetoSeleccionado = objeto
    
    If Not objeto.especificaciones Is Nothing Then
        If objeto.especificaciones.Count = 0 Then
            Set objeto.especificaciones = Nothing
            LimpiarEspecificaciones
        Else
            AnyadirEspecificaciones objeto.especificaciones
        End If
    End If

    If Parent.g_oGrupoSeleccionado Is Nothing Then
    If Not Parent.g_oProcesoSeleccionado.AtributosEspecificacion Is Nothing Then
        If Parent.g_oProcesoSeleccionado.AtributosEspecificacion.Count = 0 Then
           Set Parent.g_oProcesoSeleccionado.AtributosEspecificacion = Nothing
            LimpiarAtributos
       Else
           MostrarAtributosEsp Parent.g_oProcesoSeleccionado.AtributosEspecificacion
        End If
    End If
    Else
    If Not Parent.g_oGrupoSeleccionado.AtributosEspecificacion Is Nothing Then
        If Parent.g_oGrupoSeleccionado.AtributosEspecificacion.Count = 0 Then
           Set Parent.g_oGrupoSeleccionado.AtributosEspecificacion = Nothing
            LimpiarAtributos
       Else
           MostrarAtributosEsp Parent.g_oGrupoSeleccionado.AtributosEspecificacion
        End If
    End If
    End If
    
End Sub

''' <summary>
''' A�ade los atributos de especificacion al grid
''' </summary>
''' <param name="AtributosEsp">Atributos de especificacion</param>
''' <returns></returns>
''' <remarks>Llamada desde; ctlEspecificacionesProceso.RestaurarEspecificaciones Tiempo m�ximo</remarks>
Public Sub MostrarAtributosEsp(AtributosEsp As CAtributos)
    Dim oAtr As CAtributo
    Dim i As Integer
    

    sdbgAtributos.RemoveAll
    sdbgAtributos.Columns("Valor").DropDownHwnd = 0

    If Not AtributosEsp Is Nothing Then
        For i = 1 To AtributosEsp.Count
            Set oAtr = AtributosEsp.Item(i)
            
            Dim sCod As String
            If oAtr.Obligatorio Then
                sCod = "(*) " & oAtr.Cod
            Else
                sCod = oAtr.Cod
            End If
            Select Case oAtr.Tipo
                Case 1
                    sdbgAtributos.AddItem oAtr.AnyoProce & vbTab & oAtr.GMN1Proce & vbTab & oAtr.CodProce & vbTab & oAtr.codgrupo & vbTab & oAtr.Atrib & vbTab & " " & vbTab & oAtr.ambito & vbTab & oAtr.interno & vbTab & oAtr.pedido & vbTab & sCod & vbTab & oAtr.Den & vbTab & oAtr.valorText & vbTab & oAtr.Minimo & vbTab & oAtr.Maximo & vbTab & oAtr.Tipo & vbTab & oAtr.TipoIntroduccion & vbTab & oAtr.idGrupo & vbTab & oAtr.Obligatorio & vbTab & oAtr.Validacion & vbTab & vbTab & SQLBinaryToBoolean(oAtr.ListaExterna)

                Case 2
                    sdbgAtributos.AddItem oAtr.AnyoProce & vbTab & oAtr.GMN1Proce & vbTab & oAtr.CodProce & vbTab & oAtr.codgrupo & vbTab & oAtr.Atrib & vbTab & " " & vbTab & oAtr.ambito & vbTab & oAtr.interno & vbTab & oAtr.pedido & vbTab & sCod & vbTab & oAtr.Den & vbTab & oAtr.valorNum & vbTab & oAtr.Minimo & vbTab & oAtr.Maximo & vbTab & oAtr.Tipo & vbTab & oAtr.TipoIntroduccion & vbTab & oAtr.idGrupo & vbTab & oAtr.Obligatorio & vbTab & oAtr.Validacion & vbTab & vbTab & SQLBinaryToBoolean(oAtr.ListaExterna)
                Case 3
                    sdbgAtributos.AddItem oAtr.AnyoProce & vbTab & oAtr.GMN1Proce & vbTab & oAtr.CodProce & vbTab & oAtr.codgrupo & vbTab & oAtr.Atrib & vbTab & " " & vbTab & oAtr.ambito & vbTab & oAtr.interno & vbTab & oAtr.pedido & vbTab & sCod & vbTab & oAtr.Den & vbTab & oAtr.valorFec & vbTab & oAtr.Minimo & vbTab & oAtr.Maximo & vbTab & oAtr.Tipo & vbTab & oAtr.TipoIntroduccion & vbTab & oAtr.idGrupo & vbTab & oAtr.Obligatorio & vbTab & oAtr.Validacion & vbTab & vbTab & SQLBinaryToBoolean(oAtr.ListaExterna)
                Case 4
                    If IsNull(oAtr.valorBool) Then
                        sdbgAtributos.AddItem oAtr.AnyoProce & vbTab & oAtr.GMN1Proce & vbTab & oAtr.CodProce & vbTab & oAtr.codgrupo & vbTab & oAtr.Atrib & vbTab & " " & vbTab & oAtr.ambito & vbTab & oAtr.interno & vbTab & oAtr.pedido & vbTab & sCod & vbTab & oAtr.Den & vbTab & vbTab & oAtr.Minimo & vbTab & oAtr.Maximo & vbTab & oAtr.Tipo & vbTab & oAtr.TipoIntroduccion & vbTab & oAtr.idGrupo & vbTab & oAtr.Obligatorio & vbTab & oAtr.Validacion & vbTab & vbTab & SQLBinaryToBoolean(oAtr.ListaExterna)
                    Else
                        If oAtr.valorBool Then
                            sdbgAtributos.AddItem oAtr.AnyoProce & vbTab & oAtr.GMN1Proce & vbTab & oAtr.CodProce & vbTab & oAtr.codgrupo & vbTab & oAtr.Atrib & vbTab & " " & vbTab & oAtr.ambito & vbTab & oAtr.interno & vbTab & oAtr.pedido & vbTab & sCod & vbTab & oAtr.Den & vbTab & m_sIdiTrue & vbTab & oAtr.Minimo & vbTab & oAtr.Maximo & vbTab & oAtr.Tipo & vbTab & oAtr.TipoIntroduccion & vbTab & oAtr.idGrupo & vbTab & oAtr.Obligatorio & vbTab & oAtr.Validacion & vbTab & vbTab & SQLBinaryToBoolean(oAtr.ListaExterna)
                        Else
                            sdbgAtributos.AddItem oAtr.AnyoProce & vbTab & oAtr.GMN1Proce & vbTab & oAtr.CodProce & vbTab & oAtr.codgrupo & vbTab & oAtr.Atrib & vbTab & " " & vbTab & oAtr.ambito & vbTab & oAtr.interno & vbTab & oAtr.pedido & vbTab & sCod & vbTab & oAtr.Den & vbTab & m_sIdiFalse & vbTab & oAtr.Minimo & vbTab & oAtr.Maximo & vbTab & oAtr.Tipo & vbTab & oAtr.TipoIntroduccion & vbTab & oAtr.idGrupo & vbTab & oAtr.Obligatorio & vbTab & oAtr.Validacion & vbTab & vbTab & SQLBinaryToBoolean(oAtr.ListaExterna)
                        End If
                    End If
            End Select
        Next
    End If
End Sub

Public Sub ImagenNodoEspecificaciones()



        If frmPROCE.g_oGrupoSeleccionado Is Nothing Then
            If Trim(txtEspecificacion.Text) = "" Then
                If g_oObjetoSeleccionado.especificaciones Is Nothing Then
                    Parent.tvwProce.Nodes.Item("Especificaciones").Image = "Especificacion"
                    If Not g_oObjetoSeleccionado.AtributosEspecificacion Is Nothing Then
                        If g_oObjetoSeleccionado.AtributosEspecificacion.Count > 0 Then
                            Parent.tvwProce.Nodes.Item("Especificaciones").Image = "EspecificacionAsig"
                        End If
                    End If
                Else
                    If g_oObjetoSeleccionado.especificaciones.Count = 0 Then
                        Parent.tvwProce.Nodes.Item("Especificaciones").Image = "Especificacion"
                        If Not g_oObjetoSeleccionado.AtributosEspecificacion Is Nothing Then
                            If g_oObjetoSeleccionado.AtributosEspecificacion.Count > 0 Then
                                Parent.tvwProce.Nodes.Item("Especificaciones").Image = "EspecificacionAsig"
                            End If
                        End If
                    Else
                        Parent.tvwProce.Nodes.Item("Especificaciones").Image = "EspecificacionAsig"
                    End If
                End If
            Else
                Parent.tvwProce.Nodes.Item("Especificaciones").Image = "EspecificacionAsig"
            End If
    Else
            If Trim(txtEspecificacion.Text) = "" Then
                If g_oObjetoSeleccionado.especificaciones Is Nothing Then
                    Parent.tvwProce.Nodes.Item("EspecG" & g_oObjetoSeleccionado.Codigo).Image = "Especificacion"
                    If Not g_oObjetoSeleccionado.AtributosEspecificacion Is Nothing Then
                        If g_oObjetoSeleccionado.AtributosEspecificacion.Count > 0 Then
                            Parent.tvwProce.Nodes.Item("EspecG" & g_oObjetoSeleccionado.Codigo).Image = "EspecificacionAsig"
                        End If
                    End If
                Else
                    If g_oObjetoSeleccionado.especificaciones.Count = 0 Then
                        Parent.tvwProce.Nodes.Item("EspecG" & g_oObjetoSeleccionado.Codigo).Image = "Especificacion"
                        If Not g_oObjetoSeleccionado.AtributosEspecificacion Is Nothing Then
                            If g_oObjetoSeleccionado.AtributosEspecificacion.Count > 0 Then
                                Parent.tvwProce.Nodes.Item("EspecG" & g_oObjetoSeleccionado.Codigo).Image = "EspecificacionAsig"
                            End If
                        End If
                    Else
                        Parent.tvwProce.Nodes.Item("EspecG" & g_oObjetoSeleccionado.Codigo).Image = "EspecificacionAsig"
                    End If
                End If
            Else
                Parent.tvwProce.Nodes.Item("EspecG" & g_oObjetoSeleccionado.Codigo).Image = "EspecificacionAsig"
            End If
    End If

End Sub

