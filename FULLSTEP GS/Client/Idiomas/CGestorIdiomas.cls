VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGestorIdiomas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private adoCon As New ADODB.Connection

Public Function DevolverTextosDelModulo(ByVal FormId As ModuleName, ByVal Idioma As String, Optional ByVal ControlId As Variant) As ADODB.Recordset
Dim adores As ADODB.Recordset
Dim sDen As String

    sDen = "TEXT_" & Idioma
    
    sConsulta = "SELECT " & sDen & " FROM TEXTOS WHERE MODULO=" & FormId
    If Not IsMissing(ControlId) Then
        If IsNumeric(ControlId) Then
            sConsulta = sConsulta & " AND ID = " & ControlId
        Else
            Set DevolverTextosDelModulo = Nothing
            Exit Function
        End If
    End If
    sConsulta = sConsulta & " ORDER BY MODULO,ID"
    Set adores = New ADODB.Recordset
    
    adores.Open sConsulta, adoCon, adOpenForwardOnly, adLockReadOnly
    
    Set adores.ActiveConnection = Nothing
    
    Set DevolverTextosDelModulo = adores
End Function

Public Function DevolverTextosIdiomasDelModulo(ByVal FormId As ModuleName, ByVal Idiomas As Variant, Optional ByVal ControlId As Variant) As ADODB.Recordset
    Dim adores As ADODB.Recordset
    Dim sDen As String
    Dim i As Integer
    
    For i = 0 To UBound(Idiomas)
        sDen = sDen & "TEXT_" & Idiomas(i) & ","
    Next
    sDen = Left(sDen, Len(sDen) - 1)
    
    sConsulta = "SELECT " & sDen & " FROM TEXTOS WHERE MODULO=" & FormId
    If Not IsMissing(ControlId) Then
        If IsNumeric(ControlId) Then
            sConsulta = sConsulta & " AND ID = " & ControlId
        Else
            Set DevolverTextosIdiomasDelModulo = Nothing
            Exit Function
        End If
    End If
    sConsulta = sConsulta & " ORDER BY MODULO,ID"
    Set adores = New ADODB.Recordset
    
    adores.Open sConsulta, adoCon, adOpenForwardOnly, adLockReadOnly
    
    Set adores.ActiveConnection = Nothing
    
    Set DevolverTextosIdiomasDelModulo = adores
End Function

Public Sub Inicializar(ByVal sBD As String)
Dim adores As ADODB.Recordset

    On Error GoTo error:
    
    'Create the connection.
    adoCon.Open "Provider=Microsoft.Jet.OLEDB.4.0;" & _
    "Data Source=" & App.Path & "\" & sBD & ";"
    
    adoCon.CursorLocation = adUseClient
    
    Exit Sub

error:
    
    Err.Raise 10000, "FSGIdiomas", "Cannot connect to " & sBD
    
    

End Sub


Private Sub Class_Terminate()
    
    adoCon.Close
    
End Sub

Public Function DevolverTextosTotalProveedores(ByVal FormId As ModuleName) As ADODB.Recordset
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Function que devuelve un recordset con una fila con los idiomas que tiene la denominacion de la variable de calidad de nivel 0
'*** Par�metros de entrada: FormId:=ID del modulo = 356
'*** Parametros salida:= Recordset con los nombres de la var_cal0 en los distintos idiomas de la aplicacion
'*** Llamada desde: frmVarCalidad.frm
'*** Tiempo m�ximo: 0,10 seg
'************************************************************

'    Dim inicio As Double, final As Double, tiempoTranscurrido As Double
'    inicio = Timer
    Dim adores As ADODB.Recordset
    Dim sConsulta As String
        
    sConsulta = "SELECT TEXT_SPA, TEXT_ENG, TEXT_GER, TEXT_FRA FROM TEXTOS WHERE MODULO=" & FormId & " AND ID = 15"
    Set adores = New ADODB.Recordset
    
    adores.Open sConsulta, adoCon, adOpenForwardOnly, adLockReadOnly
    
    Set adores.ActiveConnection = Nothing
    
    Set DevolverTextosTotalProveedores = adores
    
'    final = Timer
'    tiempoTranscurrido = (final - inicio)
    

End Function
