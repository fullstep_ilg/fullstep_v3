VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfEvolPres 
   Caption         =   "Informe de evoluci�n de presupuesto"
   ClientHeight    =   5805
   ClientLeft      =   150
   ClientTop       =   2130
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfEvoluPres.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5805
   ScaleWidth      =   11880
   Begin VB.Frame fraAnyoPres 
      Caption         =   "Considerar el presupuesto en los siguientes a�os"
      Height          =   615
      Left            =   6150
      TabIndex        =   48
      Top             =   1770
      Width           =   5685
      Begin SSDataWidgets_B.SSDBCombo sdbcADesdePres 
         Height          =   285
         Left            =   870
         TabIndex        =   49
         Top             =   240
         Width           =   975
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1720
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAHastaPres 
         Height          =   285
         Left            =   2850
         TabIndex        =   50
         Top             =   240
         Width           =   975
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1720
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.Label lblHastaPres 
         Caption         =   "Hasta:"
         Height          =   225
         Left            =   2130
         TabIndex        =   52
         Top             =   270
         Width           =   660
      End
      Begin VB.Label lblDesdePres 
         Caption         =   "Desde:"
         Height          =   255
         Left            =   150
         TabIndex        =   51
         Top             =   270
         Width           =   705
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   5745
      Top             =   120
   End
   Begin VB.PictureBox picLegend 
      BorderStyle     =   0  'None
      Height          =   885
      Left            =   10050
      Picture         =   "frmInfEvoluPres.frx":0CB2
      ScaleHeight     =   885
      ScaleWidth      =   1635
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.PictureBox picTipoGrafico 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      FillStyle       =   0  'Solid
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   6555
      ScaleHeight     =   405
      ScaleWidth      =   2145
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   180
      Visible         =   0   'False
      Width           =   2145
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
         Height          =   285
         Left            =   60
         TabIndex        =   40
         TabStop         =   0   'False
         Top             =   30
         Width           =   1755
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Row.Count       =   5
         Row(0)          =   "Barras 2D"
         Row(1)          =   "Barras 3D"
         Row(2)          =   "Lineas 2D"
         Row(3)          =   "Lineas 3D"
         Row(4)          =   "Tarta"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "TIPO"
         Columns(0).Name =   "TIPO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3096
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   3405
      Left            =   0
      TabIndex        =   32
      Top             =   2400
      Width           =   11835
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   17
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfEvoluPres.frx":565C
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4744445
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfEvoluPres.frx":5678
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409635
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfEvoluPres.frx":5694
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   17
      Columns(0).Width=   873
      Columns(0).Caption=   "Anyo"
      Columns(0).Name =   "ANYO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   926
      Columns(1).Caption=   "Gmn1"
      Columns(1).Name =   "GMN1"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   847
      Columns(2).Caption=   "Cod"
      Columns(2).Name =   "COD"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2170
      Columns(3).Caption=   "Descr."
      Columns(3).Name =   "DESCR"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1244
      Columns(4).Caption=   "FecPres."
      Columns(4).Name =   "FECPRES"
      Columns(4).CaptionAlignment=   0
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   1138
      Columns(5).Caption=   "FecNec."
      Columns(5).Name =   "FECNEC"
      Columns(5).CaptionAlignment=   0
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "EstadoId"
      Columns(6).Name =   "ESTADOID"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   2963
      Columns(7).Caption=   "Est."
      Columns(7).Name =   "ESTADODEN"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   2593
      Columns(8).Caption=   "Abierto"
      Columns(8).Name =   "ABIERTO"
      Columns(8).Alignment=   1
      Columns(8).CaptionAlignment=   2
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).NumberFormat=   "Standard"
      Columns(8).FieldLen=   256
      Columns(9).Width=   2646
      Columns(9).Caption=   "Consumido"
      Columns(9).Name =   "CONSUMIDO"
      Columns(9).Alignment=   1
      Columns(9).CaptionAlignment=   2
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).NumberFormat=   "Standard"
      Columns(9).FieldLen=   256
      Columns(10).Width=   2487
      Columns(10).Caption=   "Adjudicado"
      Columns(10).Name=   "ADJUDICADO"
      Columns(10).Alignment=   1
      Columns(10).CaptionAlignment=   2
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).NumberFormat=   "Standard"
      Columns(10).FieldLen=   256
      Columns(11).Width=   3254
      Columns(11).Caption=   "Ahorrado"
      Columns(11).Name=   "AHORRADO"
      Columns(11).Alignment=   1
      Columns(11).CaptionAlignment=   2
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).NumberFormat=   "Standard"
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "FECLIMIOFE"
      Columns(12).Name=   "FECLIMIOFE"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "FECAPE"
      Columns(13).Name=   "FECAPE"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "COM"
      Columns(14).Name=   "COM"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "NOM"
      Columns(15).Name=   "NOM"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "APE"
      Columns(16).Name=   "APE"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      _ExtentX        =   20876
      _ExtentY        =   6006
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraVolumenes 
      Caption         =   "Vol�menes"
      Height          =   975
      Left            =   6135
      TabIndex        =   18
      Top             =   780
      Width           =   5700
      Begin VB.TextBox txtAhorrado 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   3082
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4260
         Locked          =   -1  'True
         TabIndex        =   36
         Top             =   600
         Width           =   1100
      End
      Begin VB.TextBox txtAdjudicado 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   3082
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4260
         Locked          =   -1  'True
         TabIndex        =   34
         Top             =   225
         Width           =   1100
      End
      Begin VB.TextBox txtConsumido 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   3082
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2255
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   600
         Width           =   1130
      End
      Begin VB.TextBox txtObjetivo 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2255
         Locked          =   -1  'True
         TabIndex        =   28
         Top             =   225
         Width           =   1130
      End
      Begin VB.TextBox txtAbierto 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   3082
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   660
         Locked          =   -1  'True
         TabIndex        =   26
         Top             =   600
         Width           =   1130
      End
      Begin VB.TextBox txtPresupuesto 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   3082
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   660
         Locked          =   -1  'True
         TabIndex        =   25
         Top             =   225
         Width           =   1130
      End
      Begin VB.Label lblAhorrado 
         Caption         =   "Ahorrado"
         Height          =   255
         Left            =   3435
         TabIndex        =   35
         Top             =   645
         Width           =   690
      End
      Begin VB.Label lblAdjudicado 
         Caption         =   "Adjudicado"
         Height          =   255
         Left            =   3435
         TabIndex        =   33
         Top             =   270
         Width           =   780
      End
      Begin VB.Label lblAbierto 
         Caption         =   "Abierto"
         Height          =   255
         Left            =   105
         TabIndex        =   31
         Top             =   645
         Width           =   615
      End
      Begin VB.Label lblCons 
         Caption         =   "Cons."
         Height          =   255
         Left            =   1830
         TabIndex        =   29
         Top             =   645
         Width           =   465
      End
      Begin VB.Label lblObj 
         Caption         =   "Obj."
         Height          =   255
         Left            =   1830
         TabIndex        =   27
         Top             =   270
         Width           =   495
      End
      Begin VB.Label lblPres 
         Caption         =   "Pres."
         Height          =   255
         Left            =   105
         TabIndex        =   24
         Top             =   270
         Width           =   645
      End
   End
   Begin VB.Frame fraResultado 
      Caption         =   "Procesos asignados. Total=N"
      Height          =   855
      Left            =   0
      TabIndex        =   17
      Top             =   1500
      Width           =   6090
      Begin VB.Label txtParCerrados 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   4120
         TabIndex        =   47
         Top             =   480
         Width           =   410
      End
      Begin VB.Label lblParCerrados 
         Caption         =   "Parc. Cerrados"
         Height          =   240
         Left            =   2800
         TabIndex        =   46
         Top             =   480
         Width           =   1320
      End
      Begin VB.Label txtAnulados 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   5490
         TabIndex        =   45
         Top             =   480
         Width           =   420
      End
      Begin VB.Label txtAdjudicados 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   5490
         TabIndex        =   44
         Top             =   240
         Width           =   420
      End
      Begin VB.Label txtNegociacion 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   4120
         TabIndex        =   43
         Top             =   240
         Width           =   410
      End
      Begin VB.Label txtSelProve 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2250
         TabIndex        =   42
         Top             =   480
         Width           =   410
      End
      Begin VB.Label txtSinValidar 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   2250
         TabIndex        =   41
         Top             =   240
         Width           =   410
      End
      Begin VB.Label lblAnulados 
         Caption         =   "Anulados"
         Height          =   240
         Left            =   4635
         TabIndex        =   23
         Top             =   480
         Width           =   750
      End
      Begin VB.Label lblAdjudicados 
         Caption         =   "Adjudicados"
         Height          =   225
         Left            =   4635
         TabIndex        =   22
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblNegociacion 
         Caption         =   "En negociaci�n"
         Height          =   240
         Left            =   2800
         TabIndex        =   21
         Top             =   240
         Width           =   1320
      End
      Begin VB.Label lblSelProve 
         Caption         =   "En selecci�n de proveedores"
         Height          =   240
         Left            =   135
         TabIndex        =   20
         Top             =   480
         Width           =   2200
      End
      Begin VB.Label lblSinValidar 
         Caption         =   "Sin validar"
         Height          =   180
         Left            =   135
         TabIndex        =   19
         Top             =   240
         Width           =   1485
      End
   End
   Begin VB.Frame fraMostrarDesde 
      Caption         =   "Mostrar resultados desde presupuesto"
      Height          =   675
      Left            =   0
      TabIndex        =   13
      Top             =   780
      Width           =   6090
      Begin VB.CommandButton cmdSelParCon 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5655
         Picture         =   "frmInfEvoluPres.frx":56B0
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdBorrar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5310
         Picture         =   "frmInfEvoluPres.frx":571C
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   240
         Width           =   315
      End
      Begin VB.Label lblParCon 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   5115
      End
   End
   Begin VB.Frame fraSel 
      Caption         =   "Proyecto"
      Height          =   615
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9990
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9555
         Picture         =   "frmInfEvoluPres.frx":57C1
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   225
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.CommandButton cmdGrafico 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8775
         Picture         =   "frmInfEvoluPres.frx":58C3
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   225
         Width           =   315
      End
      Begin VB.CommandButton cmdActualizar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9165
         Picture         =   "frmInfEvoluPres.frx":5C05
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   225
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyoDesde 
         Height          =   285
         Left            =   570
         TabIndex        =   3
         Top             =   210
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMesDesde 
         Height          =   285
         Left            =   1380
         TabIndex        =   4
         Top             =   210
         Width           =   1260
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2222
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyoHasta 
         Height          =   285
         Left            =   3150
         TabIndex        =   5
         Top             =   210
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMesHasta 
         Height          =   285
         Left            =   3960
         TabIndex        =   6
         Top             =   210
         Width           =   1260
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2222
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.CommandButton cmdGrid 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8775
         Picture         =   "frmInfEvoluPres.frx":5C90
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   225
         Visible         =   0   'False
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMon 
         Height          =   285
         Left            =   7545
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   225
         Width           =   1140
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1164
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1879
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "EQUIV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   2011
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblAnyoDesde 
         Caption         =   "Desde:"
         Height          =   255
         Left            =   60
         TabIndex        =   12
         Top             =   240
         Width           =   555
      End
      Begin VB.Label lblAnyoHasta 
         Caption         =   "Hasta:"
         Height          =   225
         Left            =   2670
         TabIndex        =   11
         Top             =   240
         Width           =   450
      End
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   3405
      Left            =   0
      OleObjectBlob   =   "frmInfEvoluPres.frx":5DDA
      TabIndex        =   37
      TabStop         =   0   'False
      Top             =   2400
      Width           =   11835
   End
   Begin VB.PictureBox picLegend2 
      BorderStyle     =   0  'None
      Height          =   765
      Left            =   10050
      Picture         =   "frmInfEvoluPres.frx":7800
      ScaleHeight     =   765
      ScaleWidth      =   1635
      TabIndex        =   39
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1635
   End
End
Attribute VB_Name = "frmInfEvolPres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables para func. combos
Private bRespetarCombo As Boolean
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

Private oMonedas As CMonedas
Private dequivalencia As Double

Private arResultados As Ador.Recordset
Private arResultados2() As Variant

'Origen de la llamda al formulario
Public iConcepto As Integer
Private iConcepto2 As Integer

' variables para listado

Public ofrmLstInfEvolPres1 As frmLstINFEvolPres
Public ofrmLstInfEvolPres2 As frmLstINFEvolPres

'Variables para partidas
Private sParCon1 As String
Private sParCon2 As String
Private sParCon3 As String
Private sParCon4 As String

'Variables para proyectos
Private sProy1 As String
Private sProy2 As String
Private sProy3 As String
Private sProy4 As String
'Variables para UO
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String

'MultiLenguaje
Private sIdiAnyoInic As String
Private sIdiMonCent As String
Private sIdiTotal As String
Private sIdiAdj As String
Private sIdiAhor As String
Private sIdiConsum As String
Private sestado(10) As String
Private sIdiTipoGrafico(5) As String
Private sIdiMeses(12) As String
Private sMensajeSeleccion As String
Private sIdiCargando As String
Private sIdiSeleccionando As String
Private sIdiFechas As String
Private sIdiProceAsig As String

'Variable de seguridad
Private bRMat As Boolean
Private m_bRuo As Boolean

'Variables para Procesos Asignados
Private proceSinValidar As Long
Private proceEnSelProve As Long
Private proceEnNeg As Long
Private proceAdj As Long
Private proceAnul As Long
Private proceParCerrados As Long

'Variables para Vol�menes
Private volAbierto As Double
Private volConsum As Double
Private volAdj As Double
Private volAho As Double

Private m_sLblUO As String
'variables de Resize
Private bload As Boolean

Private sIdiAnyo As String

Private Sub Arrange()

    If Me.Width > 1000 And Me.Height > 3000 Then

        'Grid resultados
        sdbgRes.Height = Me.Height - 2805
        sdbgRes.Width = Me.Width - 135
        MSChart1.Width = sdbgRes.Width
        MSChart1.Height = sdbgRes.Height
        
        sdbgRes.Columns("FECPRES").Width = sdbgRes.Width * 0.082
        sdbgRes.Columns("FECNEC").Width = sdbgRes.Width * 0.082
        sdbgRes.Columns("DESCR").Width = sdbgRes.Width * 0.15
        sdbgRes.Columns("ABIERTO").Width = sdbgRes.Width * 0.1
        sdbgRes.Columns("CONSUMIDO").Width = sdbgRes.Width * 0.1
        sdbgRes.Columns("ADJUDICADO").Width = sdbgRes.Width * 0.1
        If Me.WindowState = vbMaximized Then
            sdbgRes.Columns("AHORRADO").Width = sdbgRes.Width * 0.075
        Else
            sdbgRes.Columns("AHORRADO").Width = sdbgRes.Width * 0.071
        End If
    End If
    
End Sub


Private Function AsignarYContarEstado(ByVal IdEstado As Integer)
    
    Select Case IdEstado
        
        Case 1, 2:  AsignarYContarEstado = sestado(1)
                    proceSinValidar = proceSinValidar + 1
    
        Case 3, 4:  AsignarYContarEstado = sestado(2)
                    proceEnSelProve = proceEnSelProve + 1
        
        Case 5:     AsignarYContarEstado = sestado(3)
                    proceEnNeg = proceEnNeg + 1
    
        Case 6, 7:  AsignarYContarEstado = sestado(4)
                    proceEnNeg = proceEnNeg + 1
    
        Case 8, 9:  AsignarYContarEstado = sestado(5)
                    proceEnNeg = proceEnNeg + 1
    
        Case 10:    AsignarYContarEstado = sestado(6)
                    proceEnNeg = proceEnNeg + 1
    
        Case 12:    AsignarYContarEstado = sestado(7)
                    proceAdj = proceAdj + 1
    
        Case 13:    AsignarYContarEstado = sestado(8)
                    proceAdj = proceAdj + 1
        
        Case 20:    AsignarYContarEstado = sestado(9)
                    proceAnul = proceAnul + 1
   
        Case 11:    AsignarYContarEstado = sestado(10)
                    proceParCerrados = proceParCerrados + 1
    
    End Select


End Function

Private Sub CargarGrid()
    Dim sLinea As String

    sdbgRes.RemoveAll
    
    Dim i As Integer
    i = 0
    
    While Not arResultados.EOF
        sLinea = arResultados("ANYO").Value & Chr(m_lSeparador) & NullToStr(arResultados("GMN1").Value) & Chr(m_lSeparador) & arResultados("PROCE").Value & Chr(m_lSeparador) & NullToStr(arResultados("DEN").Value) & Chr(m_lSeparador) & arResultados("FECPRES").Value & Chr(m_lSeparador) & arResultados("FECNEC").Value & Chr(m_lSeparador) & arResultados("EST").Value & Chr(m_lSeparador) & AsignarYContarEstado(arResultados("EST").Value) & Chr(m_lSeparador) & dequivalencia * NullToDbl0(arResultados("ABIERTO").Value) & Chr(m_lSeparador) & dequivalencia * NullToDbl0(arResultados("CONSUMIDO").Value)
        sLinea = sLinea & Chr(m_lSeparador) & dequivalencia * NullToDbl0(arResultados("ADJUDICADO").Value) & Chr(m_lSeparador) & dequivalencia * NullToDbl0(arResultados("AHORRO").Value) & Chr(m_lSeparador) & arResultados("FECLIMOFE").Value & Chr(m_lSeparador) & arResultados("FECAPE").Value & Chr(m_lSeparador) & NullToStr(arResultados("COM").Value) & Chr(m_lSeparador) & NullToStr(arResultados("NOM").Value) & Chr(m_lSeparador) & NullToStr(arResultados("APE").Value)
        sdbgRes.AddItem sLinea
        
        volConsum = volConsum + (dequivalencia * NullToDbl0(arResultados("CONSUMIDO").Value))
        volAdj = volAdj + (dequivalencia * NullToDbl0(arResultados("ADJUDICADO").Value))
        volAbierto = volAbierto + (dequivalencia * NullToDbl0(arResultados("ABIERTO").Value))
        i = i + 1
        arResultados.MoveNext
    Wend
    
    arResultados.Close
    Set arResultados = Nothing
    
        
End Sub

Private Sub CargarAnyos()
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)

    For iInd = iAnyoActual - 10 To iAnyoActual + 10

        sdbcAnyoDesde.AddItem iInd
        sdbcAnyoHasta.AddItem iInd
        sdbcADesdePres.AddItem iInd
        sdbcAHastaPres.AddItem iInd
        
    Next
    
    sdbcAnyoDesde.Text = iAnyoActual
    sdbcAnyoDesde.ListAutoPosition = True
    sdbcAnyoDesde.Scroll 1, 7
    sdbcAnyoHasta.Text = iAnyoActual
    sdbcAnyoHasta.ListAutoPosition = True
    sdbcAnyoHasta.Scroll 1, 7
    sdbcADesdePres.Scroll 1, 7
    sdbcAHastaPres.Scroll 1, 7
    
    sdbcMesDesde.Text = "1"
    sdbcMesDesde.ListAutoPosition = True
    sdbcMesHasta.Text = Month(Date) - 1
    sdbcMesHasta.ListAutoPosition = True

    sdbcMesDesde.AddItem sIdiMeses(1)
    sdbcMesDesde.AddItem sIdiMeses(2)
    sdbcMesDesde.AddItem sIdiMeses(3)
    sdbcMesDesde.AddItem sIdiMeses(4)
    sdbcMesDesde.AddItem sIdiMeses(5)
    sdbcMesDesde.AddItem sIdiMeses(6)
    sdbcMesDesde.AddItem sIdiMeses(7)
    sdbcMesDesde.AddItem sIdiMeses(8)
    sdbcMesDesde.AddItem sIdiMeses(9)
    sdbcMesDesde.AddItem sIdiMeses(10)
    sdbcMesDesde.AddItem sIdiMeses(11)
    sdbcMesDesde.AddItem sIdiMeses(12)

    sdbcMesHasta.AddItem sIdiMeses(1)
    sdbcMesHasta.AddItem sIdiMeses(2)
    sdbcMesHasta.AddItem sIdiMeses(3)
    sdbcMesHasta.AddItem sIdiMeses(4)
    sdbcMesHasta.AddItem sIdiMeses(5)
    sdbcMesHasta.AddItem sIdiMeses(6)
    sdbcMesHasta.AddItem sIdiMeses(7)
    sdbcMesHasta.AddItem sIdiMeses(8)
    sdbcMesHasta.AddItem sIdiMeses(9)
    sdbcMesHasta.AddItem sIdiMeses(10)
    sdbcMesHasta.AddItem sIdiMeses(11)
    sdbcMesHasta.AddItem sIdiMeses(12)

    sdbcMesDesde.MoveFirst
    sdbcMesDesde.Text = sdbcMesDesde.Columns(0).Value

    sdbcMesHasta.MoveFirst

    For iInd = 1 To Month(Date) - 2
        sdbcMesHasta.MoveNext
    Next

    sdbcMesHasta.Text = sdbcMesHasta.Columns(0).Value
    
End Sub


Private Sub CargarRecursos()

    Dim Ador As Ador.Recordset
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFEVOLPRES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        If iConcepto = 2 Then
            sMensajeSeleccion = gParametrosGenerales.gsSingPres2
            If InStr(1, Ador(0).Value, "PROY2") = 1 Then
                caption = Replace(Ador(0).Value, "PROY2", gParametrosGenerales.gsSingPres2)
            Else
                caption = Replace(Ador(0).Value, "PROY2", LCase(gParametrosGenerales.gsSingPres2))
            End If
            Ador.MoveNext
            fraSel.caption = gParametrosGenerales.gsSingPres2
         Else
            Ador.MoveNext
            sMensajeSeleccion = gParametrosGenerales.gsSingPres1
            If InStr(1, Ador(0).Value, "PROY1") = 1 Then
                caption = Replace(Ador(0).Value, "PROY1", gParametrosGenerales.gsSingPres1)
            Else
                caption = Replace(Ador(0).Value, "PROY1", LCase(gParametrosGenerales.gsSingPres1))
            End If
            fraSel.caption = gParametrosGenerales.gsSingPres1
        End If
        
        
        Ador.MoveNext
        lblAnyoDesde.caption = Ador(0).Value
        lblDesdePres.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyoHasta.caption = Ador(0).Value
        lblHastaPres.caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value
        sdbgRes.Columns("DESCR").caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        
        If iConcepto = 2 Then
            If InStr(1, Ador(0).Value, "PROY2") = 1 Then
                fraMostrarDesde.caption = Replace(Ador(0).Value, "PROY2", gParametrosGenerales.gsSingPres2)
            Else
                fraMostrarDesde.caption = Replace(Ador(0).Value, "PROY2", LCase(gParametrosGenerales.gsSingPres2))
            End If
            Ador.MoveNext
        Else
            Ador.MoveNext
            If InStr(1, Ador(0).Value, "PROY1") = 1 Then
                fraMostrarDesde.caption = Replace(Ador(0).Value, "PROY1", gParametrosGenerales.gsSingPres1)
            Else
                fraMostrarDesde.caption = Replace(Ador(0).Value, "PROY1", LCase(gParametrosGenerales.gsSingPres1))
            End If
        End If
        
        Ador.MoveNext
        sIdiProceAsig = Ador(0).Value
        fraResultado.caption = sIdiProceAsig
        Ador.MoveNext
        lblSinValidar.caption = Ador(0).Value
        Ador.MoveNext
        lblSelProve.caption = Ador(0).Value
        Ador.MoveNext
        lblNegociacion.caption = Ador(0).Value
        Ador.MoveNext
        lblAdjudicados.caption = Ador(0).Value
        Ador.MoveNext
        lblAnulados.caption = Ador(0).Value
        Ador.MoveNext
        sIdiTotal = Ador(0).Value
        Ador.MoveNext
        fraVolumenes.caption = Ador(0).Value
        Ador.MoveNext
        lblPres.caption = Ador(0).Value
        Ador.MoveNext
        lblAbierto.caption = Ador(0).Value
        sdbgRes.Columns("ABIERTO").caption = Ador(0).Value
        Ador.MoveNext
        lblObj.caption = Ador(0).Value
        Ador.MoveNext
        lblCons.caption = Ador(0).Value
        Ador.MoveNext
        lblAdjudicado.caption = Ador(0).Value
        sdbgRes.Columns("ADJUDICADO").caption = Ador(0).Value
        sIdiAdj = Ador(0).Value

        Ador.MoveNext
        lblAhorrado.caption = Ador(0).Value
        sdbgRes.Columns("AHORRADO").caption = Ador(0).Value
        sIdiAhor = Ador(0).Value
 
        Ador.MoveNext
        
        sdbgRes.Columns("ANYO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        'sdbgRes.Columns("DESCR").Caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns("FECPRES").caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns("FECNEC").caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns("ESTADODEN").caption = Ador(0).Value
        Ador.MoveNext
        sIdiConsum = Ador(0).Value
        sdbgRes.Columns("CONSUMIDO").caption = Ador(0).Value
        Ador.MoveNext
                
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        sestado(1) = Ador(0).Value
        Ador.MoveNext
        sestado(2) = Ador(0).Value
        Ador.MoveNext
        sestado(3) = Ador(0).Value
        Ador.MoveNext
        sestado(4) = Ador(0).Value
        Ador.MoveNext
        sestado(5) = Ador(0).Value
        Ador.MoveNext
        sestado(6) = Ador(0).Value
        Ador.MoveNext
        sestado(7) = Ador(0).Value
        Ador.MoveNext
        sestado(8) = Ador(0).Value
        Ador.MoveNext
        sestado(9) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(1) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(2) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(3) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(4) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(5) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(6) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(7) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(8) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(9) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(10) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(11) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(12) = Ador(0).Value
      
        Ador.MoveNext
        sIdiCargando = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiFechas = Ador(0).Value
        Ador.MoveNext
        sestado(10) = Ador(0).Value
        Ador.MoveNext
        lblParCerrados.caption = Ador(0).Value
        Ador.MoveNext
        fraAnyoPres.caption = Ador(0).Value
        Ador.Close
    
    End If

    Set Ador = Nothing
    
    picLegend.Picture = LoadResPicture(FSClientResource.FRM_INFEVOLPRES_PICLEGEND + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)
    picLegend2.Picture = LoadResPicture(FSClientResource.FRM_INFEVOLPRES_PICLEGEND2 + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)


End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
        If iConcepto = 1 Then
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto1RestMat)) Is Nothing Then
                bRMat = True
            End If
            
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto1RestUO)) Is Nothing) Then
                m_bRuo = True
            End If
        ElseIf iConcepto = 2 Then
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto2RestMat)) Is Nothing Then
                bRMat = True
            End If
            
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto2RestUO)) Is Nothing) Then
                m_bRuo = True
            End If
        End If
    End If
End Sub


Private Sub InicializarValores()
    proceSinValidar = 0
    proceEnSelProve = 0
    proceEnNeg = 0
    proceAdj = 0
    proceAnul = 0
    proceParCerrados = 0
    volAbierto = 0
    volConsum = 0
    volAdj = 0
    volAho = 0
End Sub


Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer

    sdbcTipoGrafico.Visible = True

    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If

    Select Case Tipo

'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)

                'Necesitamos ocho series
                ' Adjudicado
                ' Abierto
                ' Adjudicado
                ' Consumido
                ' Ahorro positivo
                ' Adjudicado
                ' Ahorro positivo
                ' Ahorro negativo

                picLegend.Visible = True
                picLegend2.Visible = False

                ReDim ar(1 To sdbgRes.Rows, 1 To 9)
                i = 1


                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        'If sdbgRes.Columns("AHORRADO").Value <> "" Then
                        
                            ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value & " " & sdbgRes.Columns(2).Value
    
                            'Si ahorro +
                            If CDbl(sdbgRes.Columns("AHORRADO").Value) > 0 Then
                                If CDbl(sdbgRes.Columns("AHORRADO").Value) > CDbl(sdbgRes.Columns("ADJUDICADO").Value) Then
                                    ar(i, 2) = Null
                                    ar(i, 3) = Null
                                    ar(i, 4) = CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                                    ar(i, 5) = CDbl(sdbgRes.Columns("AHORRADO").Value) - CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                                    ar(i, 6) = CDbl(sdbgRes.Columns("CONSUMIDO").Value) - CDbl(sdbgRes.Columns("AHORRADO").Value)
                                    ar(i, 7) = Null
                                    ar(i, 8) = CDbl(sdbgRes.Columns("ABIERTO").Value) - CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                                    ar(i, 9) = Null
                                
                                ElseIf CDbl(sdbgRes.Columns("AHORRADO").Value) < CDbl(sdbgRes.Columns("ADJUDICADO").Value) Then
                                    ar(i, 2) = Null
                                    ar(i, 3) = CDbl(sdbgRes.Columns("AHORRADO").Value)
                                    ar(i, 4) = CDbl(sdbgRes.Columns("ADJUDICADO").Value) - CDbl(sdbgRes.Columns("AHORRADO").Value)
                                    ar(i, 5) = Null
                                    ar(i, 6) = CDbl(sdbgRes.Columns("CONSUMIDO").Value) - CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                                    ar(i, 7) = Null
                                    ar(i, 8) = CDbl(sdbgRes.Columns("ABIERTO").Value) - CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                                    ar(i, 9) = Null
                                End If
                            'Si ahorro-
                            ElseIf CDbl(sdbgRes.Columns("AHORRADO").Value) < 0 Then
                                
                                If CDbl(sdbgRes.Columns("ADJUDICADO").Value) > CDbl(sdbgRes.Columns("CONSUMIDO").Value) Then
                                    ar(i, 2) = -CDbl(sdbgRes.Columns("AHORRADO").Value)
                                    ar(i, 3) = Null
                                    ar(i, 4) = Null
                                    ar(i, 5) = Null
                                    ar(i, 6) = CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                                    ar(i, 7) = Null
                                    ar(i, 8) = CDbl(sdbgRes.Columns("ADJUDICADO").Value) - CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                                    ar(i, 9) = CDbl(sdbgRes.Columns("ABIERTO").Value) - CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                                Else
                                    ar(i, 2) = -CDbl(sdbgRes.Columns("AHORRADO").Value)
                                    ar(i, 3) = Null
                                    ar(i, 4) = Null
                                    ar(i, 5) = Null
                                    ar(i, 6) = CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                                    ar(i, 7) = Null
                                    ar(i, 8) = CDbl(sdbgRes.Columns("ABIERTO").Value) - CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                                    ar(i, 9) = CDbl(sdbgRes.Columns("ADJUDICADO").Value) - CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                                End If
                            
                            Else 'Ahorro Cero
                                    ar(i, 2) = Null
                                    ar(i, 3) = Null
                                    ar(i, 4) = CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                                    ar(i, 5) = Null
                                    ar(i, 6) = Null
                                    ar(i, 7) = Null
                                    ar(i, 8) = CDbl(sdbgRes.Columns("ABIERTO").Value) - CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                                    ar(i, 9) = Null
                            End If
                        'End If
                        i = i + 1
                        sdbgRes.MoveNext
                Wend

                MSChart1.ChartData = ar
               
               '"Barras 3D"
                If Tipo = sIdiTipoGrafico(2) Then

                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else

                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar

                End If

                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine

                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Consumido
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Abierto
                MSChart1.Plot.SeriesCollection.Item(7).DataPoints.Item(-1).Brush.FillColor.Set 0, 128, 255
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(8).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115


                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels

                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal

                Next

                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next

                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next

'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)

                'Necesitamos cuatro series
                ' Adjudicado
                ' Consumido
                ' Abierto
                ' Ahorro

                picLegend.Visible = False
                picLegend2.Visible = True

'               If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If

                ReDim ar(1 To sdbgRes.Rows, 1 To 5)

                i = 1

                sdbgRes.MoveFirst

                While i <= sdbgRes.Rows
                    'If sdbgRes.Columns("AHORRADO").Value <> "" Then
                        ar(i, 1) = sdbgRes.Columns(0).Text & " " & sdbgRes.Columns(1).Value & " " & sdbgRes.Columns(2).Value
                        ar(i, 2) = CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                        ar(i, 3) = CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                        ar(i, 4) = CDbl(sdbgRes.Columns("AHORRADO").Value)
                        ar(i, 5) = CDbl(sdbgRes.Columns("ABIERTO").Value)
                    'End If
                    i = i + 1
                    sdbgRes.MoveNext
                Wend

                MSChart1.ChartData = ar

                MSChart1.ShowLegend = False

                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Consumido
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Abierto
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 0, 128, 255

                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels

                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal

                Next

                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next

                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next

'        Case "Tarta"
        Case sIdiTipoGrafico(5)

                'Necesitamos cinco series
                ' Adjudicado
                ' Consumido
                ' Abierto
                ' Ahorro positivo
                ' Ahorro negativo

                picLegend.Visible = True
                picLegend2.Visible = False

                ReDim ar(1 To sdbgRes.Rows, 1 To 6)
                i = 1

                sdbgRes.MoveFirst

                While i <= sdbgRes.Rows
                    'If sdbgRes.Columns("AHORRADO").Value <> "" Then
                        ar(i, 1) = sdbgRes.Columns(0).Text & " "
                            'Si ahorro +
                            If CDbl(sdbgRes.Columns("AHORRADO").Value) > 0 Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("AHORRADO").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("ABIERTO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                                
                            ElseIf CDbl(sdbgRes.Columns("AHORRADO").Value) < 0 Then
                            'Si ahorro-
                                ar(i, 2) = CDbl(sdbgRes.Columns("AHORRADO").Value)
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("ABIERTO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                            Else
                            'Ahorro 0
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("ABIERTO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("CONSUMIDO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("ADJUDICADO").Value)
                            End If
                     '   End If
                        i = i + 1
                        sdbgRes.MoveNext
                Wend


                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Abierto
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 128, 255
                'Consumido
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels

                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal

                Next

                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next

                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next

    End Select

End Sub

Public Sub MostrarParConSeleccionada()
    sParCon1 = frmSELPresAnuUON.g_sPRES1
    sParCon2 = frmSELPresAnuUON.g_sPRES2
    sParCon3 = frmSELPresAnuUON.g_sPRES3
    sParCon4 = frmSELPresAnuUON.g_sPRES4
    m_sUON1 = frmSELPresAnuUON.g_sUON1
    m_sUON2 = frmSELPresAnuUON.g_sUON2
    m_sUON3 = frmSELPresAnuUON.g_sUON3

    m_sLblUO = ""
    If m_sUON1 <> "" Then
        m_sLblUO = "(" & m_sUON1
        If m_sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & m_sUON2
            If m_sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & m_sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If

    If sParCon4 <> "" Then
        lblParCon.caption = m_sLblUO & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " - " & sParCon4 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sParCon3 <> "" Then
        lblParCon.caption = m_sLblUO & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sParCon2 <> "" Then
        lblParCon.caption = m_sLblUO & sParCon1 & " - " & sParCon2 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sParCon1 <> "" Then
        lblParCon.caption = m_sLblUO & sParCon1 & " " & frmSELPresAnuUON.g_sDenPres
    Else
        lblParCon.caption = m_sLblUO
    End If


    
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    
    BorrarDatosFrames

End Sub

Public Sub MostrarProySeleccionado()
    
    sProy1 = frmSELPresAnuUON.g_sPRES1
    sProy2 = frmSELPresAnuUON.g_sPRES2
    sProy3 = frmSELPresAnuUON.g_sPRES3
    sProy4 = frmSELPresAnuUON.g_sPRES4
    m_sUON1 = frmSELPresAnuUON.g_sUON1
    m_sUON2 = frmSELPresAnuUON.g_sUON2
    m_sUON3 = frmSELPresAnuUON.g_sUON3

    m_sLblUO = ""
    If m_sUON1 <> "" Then
        m_sLblUO = "(" & m_sUON1
        If m_sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & m_sUON2
            If m_sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & m_sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If

    If sProy4 <> "" Then
        lblParCon.caption = m_sLblUO & sProy1 & " - " & sProy2 & " - " & sProy3 & " - " & sProy4 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sProy3 <> "" Then
        lblParCon.caption = m_sLblUO & sProy1 & " - " & sProy2 & " - " & sProy3 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sProy2 <> "" Then
        lblParCon.caption = m_sLblUO & sProy1 & " - " & sProy2 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sProy1 <> "" Then
        lblParCon.caption = m_sLblUO & sProy1 & " " & frmSELPresAnuUON.g_sDenPres
    Else
        lblParCon.caption = m_sLblUO
    End If

    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    
    BorrarDatosFrames
    
End Sub

Private Sub cmdActualizar_Click()
    Dim FechaDesde As Date
    Dim FechaHasta As Date

    FechaDesde = CDate("01/" & CStr(sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1) & "/" & sdbcAnyoDesde)
    If sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1 = 12 Then
        FechaHasta = CDate("01/01/" & CStr(sdbcAnyoHasta.Value + 1))
    Else
        FechaHasta = CDate("01/" & CStr(sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 2) & "/" & sdbcAnyoHasta)
    End If
    If FechaDesde > DateAdd("d", -1, FechaHasta) Then
        oMensajes.NoValido sIdiFechas
        Exit Sub
    End If
    If lblParCon = "" Then
        oMensajes.FaltanDatos (sMensajeSeleccion)
        Exit Sub
    End If
    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    If sdbcADesdePres.Text <> "" Then
        If Len(sdbcADesdePres.Text) <> 4 Then
            oMensajes.NoValido sIdiAnyo
            If Me.Visible Then sdbcADesdePres.SetFocus
            Exit Sub
        Else
            If Not IsNumeric(sdbcADesdePres.Text) Then
                oMensajes.NoValido sIdiAnyo
                If Me.Visible Then sdbcADesdePres.SetFocus
                Exit Sub
            End If
        End If
        If sdbcAHastaPres.Text = "" Then
            bRespetarCombo = True
            sdbcAHastaPres.Text = sdbcADesdePres.Text
            bRespetarCombo = False
        End If
    End If
    If sdbcAHastaPres.Text <> "" Then
        If Len(sdbcAHastaPres.Text) <> 4 Then
            oMensajes.NoValido sIdiAnyo
            If Me.Visible Then sdbcAHastaPres.SetFocus
            Exit Sub
        Else
            If Not IsNumeric(sdbcAHastaPres.Text) Then
                oMensajes.NoValido sIdiAnyo
                If Me.Visible Then sdbcAHastaPres.SetFocus
                Exit Sub
            End If
        End If
        If sdbcADesdePres.Text = "" Then
            bRespetarCombo = True
            sdbcADesdePres.Text = sdbcAHastaPres.Text
            bRespetarCombo = False
        End If
    End If
    If val(sdbcADesdePres.Text) > val(sdbcAHastaPres.Text) Then
        oMensajes.PeriodoNoValido
        If Me.Visible Then sdbcAHastaPres.SetFocus
        Exit Sub
    End If
    
        Screen.MousePointer = vbHourglass
    
        frmESPERA.Show
        frmESPERA.lblGeneral.caption = Me.caption
        frmESPERA.Top = Me.Height / 2 - frmESPERA.Height / 2
        frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
        frmESPERA.Frame2.caption = ""
        frmESPERA.Frame3.caption = ""
        frmESPERA.ProgressBar2.Value = 10
        frmESPERA.ProgressBar1.Value = 1
        frmESPERA.lblContacto = sIdiCargando
        frmESPERA.lblDetalle = sIdiSeleccionando
        frmESPERA.Show
        Timer1.Enabled = True
        DoEvents
        
        If iConcepto = 1 Then
    
            If sProy4 <> "" Then
                Set arResultados = oGestorInformes.InfEvolucionConcepto1(oSesionSummit.Id, (sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sProy1, sProy2, sProy3, sProy4, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , True, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
            Else
                If sProy3 <> "" Then
                    Set arResultados = oGestorInformes.InfEvolucionConcepto1(oSesionSummit.Id, val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sProy1, sProy2, sProy3, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , True, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
                Else
                    If sProy2 <> "" Then
                        Set arResultados = oGestorInformes.InfEvolucionConcepto1(oSesionSummit.Id, val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sProy1, sProy2, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , True, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
                    Else
                        If sProy1 <> "" Then
                            Set arResultados = oGestorInformes.InfEvolucionConcepto1(oSesionSummit.Id, val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sProy1, , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , True, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
                        End If
                    End If
                End If
            End If
    
        Else
    
            If sParCon4 <> "" Then
                Set arResultados = oGestorInformes.InfEvolucionConcepto2(oSesionSummit.Id, val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, sParCon2, sParCon3, sParCon4, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , True, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
            Else
                If sParCon3 <> "" Then
                    Set arResultados = oGestorInformes.InfEvolucionConcepto2(oSesionSummit.Id, val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, sParCon2, sParCon3, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , True, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
                Else
                    If sParCon2 <> "" Then
                        Set arResultados = oGestorInformes.InfEvolucionConcepto2(oSesionSummit.Id, val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, sParCon2, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , True, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
                    Else
                        If sParCon1 <> "" Then
                            Set arResultados = oGestorInformes.InfEvolucionConcepto2(oSesionSummit.Id, val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , True, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
                        End If
                    End If
                End If
            End If
    
        End If
    
        Timer1.Enabled = False

        Unload frmESPERA
                
        On Error GoTo Error:
            
        If arResultados.EOF Or arResultados Is Nothing Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        InicializarValores
        
        CargarGrid
    
        If cmdGrid.Visible = True Then
            MSChart1.Visible = True
            MostrarGrafico sdbcTipoGrafico.Value
        End If
    
        CargarDatosProcesos
            
        CargarDatosVolumenes
        
        Screen.MousePointer = vbNormal
    
    
    
    Exit Sub

Error:
        If cmdGrid.Visible = True Then
            sdbgRes.RemoveAll
            BorrarDatosFrames
            cmdGrid_Click
        End If
        Screen.MousePointer = vbNormal
        
    

End Sub

Private Sub cmdGrafico_Click()
            
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass

        sdbcTipoGrafico.Value = sIdiTipoGrafico(2)
        picTipoGrafico.Visible = True
        MostrarGrafico sIdiTipoGrafico(2)
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal
        
End Sub

Private Sub cmdGrid_Click()
    
    picTipoGrafico.Visible = False
    picLegend.Visible = False
    picLegend2.Visible = False
        
    cmdGrafico.Visible = True
    cmdGrid.Visible = False
    sdbgRes.Visible = True
    MSChart1.Visible = False


End Sub

Private Sub cmdImprimir_Click()
Dim iInd As Integer
    If iConcepto = 1 Then
                
            Set ofrmLstInfEvolPres1 = New frmLstINFEvolPres
            ofrmLstInfEvolPres1.iConcepto = iConcepto
            ofrmLstInfEvolPres1.sOrigen = "frmInfEvolpres"
            ofrmLstInfEvolPres1.sProy1 = sProy1
            ofrmLstInfEvolPres1.sProy2 = sProy2
            ofrmLstInfEvolPres1.sProy3 = sProy3
            ofrmLstInfEvolPres1.sProy4 = sProy4
            ofrmLstInfEvolPres1.sUON1 = m_sUON1
            ofrmLstInfEvolPres1.sUON2 = m_sUON2
            ofrmLstInfEvolPres1.sUON3 = m_sUON3
            
            ofrmLstInfEvolPres1.txtParCon = lblParCon
            ofrmLstInfEvolPres1.sdbcAnyoDesde = sdbcAnyoDesde
            ofrmLstInfEvolPres1.sdbcAnyoHasta = sdbcAnyoHasta
            
            ofrmLstInfEvolPres1.sdbcMesDesde.MoveFirst
            For iInd = 1 To sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark)
                ofrmLstInfEvolPres1.sdbcMesDesde.MoveNext
            Next
            ofrmLstInfEvolPres1.sdbcMesDesde.Text = ofrmLstInfEvolPres1.sdbcMesDesde.Columns(0).Value
            
            ofrmLstInfEvolPres1.sdbcMesHasta.MoveFirst
            For iInd = 1 To sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark)
                ofrmLstInfEvolPres1.sdbcMesHasta.MoveNext
            Next
            ofrmLstInfEvolPres1.sdbcMesHasta.Text = ofrmLstInfEvolPres1.sdbcMesHasta.Columns(0).Value
            
            ofrmLstInfEvolPres1.sdbcADesdePres = sdbcADesdePres
            ofrmLstInfEvolPres1.sdbcAHastaPres = sdbcAHastaPres
            ofrmLstInfEvolPres1.cargarmoneda sdbcMon
            ofrmLstInfEvolPres1.sdbcMon.Text = sdbcMon
            ofrmLstInfEvolPres1.sdbcMon_Validate False
            ofrmLstInfEvolPres1.Show 1
    
    Else

            Set ofrmLstInfEvolPres2 = New frmLstINFEvolPres
            ofrmLstInfEvolPres2.iConcepto = iConcepto
            ofrmLstInfEvolPres2.sOrigen = "frmInfEvolpres"
            ofrmLstInfEvolPres2.txtParCon = lblParCon
            ofrmLstInfEvolPres2.sParCon1 = sParCon1
            ofrmLstInfEvolPres2.sParCon2 = sParCon2
            ofrmLstInfEvolPres2.sParCon3 = sParCon3
            ofrmLstInfEvolPres2.sParCon4 = sParCon4
            ofrmLstInfEvolPres2.sUON1 = m_sUON1
            ofrmLstInfEvolPres2.sUON2 = m_sUON2
            ofrmLstInfEvolPres2.sUON3 = m_sUON3
            ofrmLstInfEvolPres2.sdbcAnyoDesde = sdbcAnyoDesde
            ofrmLstInfEvolPres2.sdbcAnyoHasta = sdbcAnyoHasta
            ofrmLstInfEvolPres2.sdbcMesDesde.MoveFirst
            For iInd = 1 To sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark)
                ofrmLstInfEvolPres2.sdbcMesDesde.MoveNext
            Next
            ofrmLstInfEvolPres2.sdbcMesDesde.Text = ofrmLstInfEvolPres2.sdbcMesDesde.Columns(0).Value
            
            ofrmLstInfEvolPres2.sdbcMesHasta.MoveFirst
            For iInd = 1 To sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark)
                ofrmLstInfEvolPres2.sdbcMesHasta.MoveNext
            Next
            ofrmLstInfEvolPres2.sdbcMesHasta.Text = ofrmLstInfEvolPres2.sdbcMesHasta.Columns(0).Value
            ofrmLstInfEvolPres2.sdbcADesdePres = sdbcADesdePres
            ofrmLstInfEvolPres2.sdbcAHastaPres = sdbcAHastaPres
            ofrmLstInfEvolPres2.cargarmoneda sdbcMon
            ofrmLstInfEvolPres2.sdbcMon = sdbcMon
            ofrmLstInfEvolPres2.sdbcMon_Validate False
            ofrmLstInfEvolPres2.Show 1
    End If
    
    
End Sub

Private Sub cmdSelParCon_Click()
    
    If sdbcAnyoDesde = "" Then
        oMensajes.NoValido sIdiAnyoInic
        If Me.Visible Then sdbcAnyoDesde.SetFocus
        Exit Sub
    End If

    If iConcepto = 1 Then
        frmSELPresAnuUON.sOrigen = "InfEvolPres1"
        frmSELPresAnuUON.bRUO = m_bRuo
        frmSELPresAnuUON.g_iTipoPres = 1
        frmSELPresAnuUON.bMostrarBajas = True
        frmSELPresAnuUON.Show 1

    ElseIf iConcepto = 2 Then
        frmSELPresAnuUON.sOrigen = "InfEvolPres2"
        frmSELPresAnuUON.bRUO = m_bRuo
        frmSELPresAnuUON.bMostrarBajas = True
        frmSELPresAnuUON.g_iTipoPres = 2
        frmSELPresAnuUON.Show 1
    End If

   
End Sub

Private Sub CargarDatosVolumenes()
    
    If iConcepto = 1 Then
        arResultados2 = oGestorInformes.DevolverPresYObjPorProy(val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sProy1, sProy2, sProy3, sProy4)
    Else
        arResultados2 = oGestorInformes.DevolverPresYObjPorPar(val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, sParCon2, sParCon3, sParCon4)
    End If
    
    If arResultados2(0) <> 0 Then
        txtPresupuesto = Format(arResultados2(0) * dequivalencia, "#,###,###,###.00")
    Else
        txtPresupuesto = 0
    End If
    
    If arResultados2(1) <> 0 Then
        txtObjetivo = CStr(Format(arResultados2(1) * 100, "##0.00")) + "%"
    Else
        txtObjetivo = "0%"
    End If
    If volConsum <> 0 Then
        txtConsumido = Format(volConsum, "#,###,###,###.00")
    Else
        txtConsumido = 0
    End If
    
    If volAdj <> 0 Then
        txtAdjudicado = Format(volAdj, "#,###,###,###.00")
    Else
        txtAdjudicado = 0
    End If
    
    volAho = volConsum - volAdj
    
    If volAho <> 0 Then
        txtAhorrado = Format(volAho, "#,###,###,###.00")
    Else
        txtAhorrado = 0
    End If
    
    If txtAhorrado > 0 Then
        txtAhorrado.BackColor = RGB(163, 214, 158)
    ElseIf txtAhorrado < 0 Then
        txtAhorrado.BackColor = RGB(253, 100, 72)
    End If

    If volAbierto > 0 Then
        txtAbierto = Format(volAbierto, "#,###,###,###.00")
    Else
        txtAbierto = 0
    End If
End Sub
Private Sub CargarDatosProcesos()
     txtSinValidar = proceSinValidar
     txtSelProve = proceEnSelProve
     txtNegociacion = proceEnNeg
     txtAdjudicados = proceAdj
     txtAnulados = proceAnul
     txtParCerrados = proceParCerrados
     fraResultado.caption = sIdiProceAsig & ". " & sIdiTotal & "=" & sdbgRes.Rows
End Sub

Private Sub BorrarDatosFrames()
    txtSinValidar = ""
    txtSelProve = ""
    txtNegociacion = ""
    txtAdjudicados = ""
    txtAnulados = ""
    txtParCerrados = ""
    txtPresupuesto = ""
    txtAbierto = ""
    txtObjetivo = ""
    txtConsumido = ""
    txtAdjudicado = ""
    txtAhorrado = ""
    txtAhorrado.BackColor = RGB(255, 255, 255)
    fraResultado.caption = sIdiProceAsig
End Sub
Private Sub cmdBorrar_Click()
    lblParCon = ""
    If iConcepto = 1 Then
        sProy1 = ""
        sProy2 = ""
        sProy3 = ""
        sProy4 = ""
    Else
        sParCon1 = ""
        sParCon2 = ""
        sParCon3 = ""
        sParCon4 = ""
    End If
End Sub

Private Sub Form_Activate()
    sdbgRes.SelBookmarks.RemoveAll
End Sub



Private Sub Form_Load()
    Dim oMon As CMoneda
    
    bload = True
    Me.Height = 6210
    Me.Width = 12000
    bload = False
    
    Me.Top = 0
    Me.Left = 0
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    ConfigurarSeguridad
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    CargarAnyos
    
    InicializarValores
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    
    
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        dequivalencia = oMonedas.Item(1).Equiv
    End If
    

End Sub

Private Sub Form_Resize()
    If Not bload Then Arrange
End Sub




Private Sub Form_Unload(Cancel As Integer)
Dim sConcepto As String
Set oMonedas = Nothing
Set arResultados = Nothing
Set ofrmLstInfEvolPres1 = Nothing
Set ofrmLstInfEvolPres2 = Nothing

If iConcepto2 = 1 Then
    sConcepto = gParametrosGenerales.gsSingPres1
Else
    sConcepto = gParametrosGenerales.gsSingPres2
End If

oGestorInformes.BorrarTemporal sConcepto, oSesionSummit.Id
Me.Visible = False
    
End Sub





Private Sub lblParCon_Change()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    
    BorrarDatosFrames
End Sub

Private Sub sdbcAnyoDesde_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosFrames
    
End Sub
Private Sub sdbcAnyoDesde_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosFrames
End Sub
Private Sub sdbcAnyoHasta_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosFrames
End Sub
Private Sub sdbcAnyoHasta_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosFrames
End Sub
Private Sub sdbcMesDesde_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosFrames
End Sub
Private Sub sdbcMesDesde_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosFrames
End Sub
Private Sub sdbcMesHasta_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosFrames
End Sub
Private Sub sdbcMesHasta_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosFrames
End Sub
Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
    sdbgRes.RemoveAll
    BorrarDatosFrames
        
End Sub
Private Sub sdbcMon_DropDown()

    Dim oMon As CMoneda
    
    sdbcMon.RemoveAll
    
    oMonedas.CargarTodasLasMonedas , , , , , False, True
    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next

    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcMon.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMon.Rows - 1
            bm = sdbcMon.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMon.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcMon.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
    End If
    
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbgRes_DblClick()
       
    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    frmInfEvolPresProceDetalle.iConcepto = iConcepto
    frmInfEvolPresProceDetalle.EvolAnyo = sdbgRes.Columns("ANYO").Value
    frmInfEvolPresProceDetalle.EvolGmn1 = sdbgRes.Columns("GMN1").Value
    frmInfEvolPresProceDetalle.EvolProce = sdbgRes.Columns("COD").Value
    frmInfEvolPresProceDetalle.g_sProy1 = sProy1
    frmInfEvolPresProceDetalle.g_sProy2 = sProy2
    frmInfEvolPresProceDetalle.g_sProy3 = sProy3
    frmInfEvolPresProceDetalle.g_sProy4 = sProy4
    frmInfEvolPresProceDetalle.g_sParCon1 = sParCon1
    frmInfEvolPresProceDetalle.g_sParCon2 = sParCon2
    frmInfEvolPresProceDetalle.g_sParCon3 = sParCon3
    frmInfEvolPresProceDetalle.g_sParCon4 = sParCon4
    frmInfEvolPresProceDetalle.g_sUON1 = m_sUON1
    frmInfEvolPresProceDetalle.g_sUON2 = m_sUON2
    frmInfEvolPresProceDetalle.g_sUON3 = m_sUON3
    frmInfEvolPresProceDetalle.g_dEquivalencia = dequivalencia
    
    If sdbgRes.Columns("ESTADOID").Value = 11 Then
        frmInfEvolPresProceDetalle.bParCerrado = True
    Else
        frmInfEvolPresProceDetalle.bParCerrado = False
    End If
   
    Screen.MousePointer = vbNormal

    frmInfEvolPresProceDetalle.Show 1
    
    iConcepto2 = iConcepto ' iConcepto pierde el valor despues de cerrar el formulario detalle

End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
    
    If sdbgRes.Columns("AHORRADO").Value <> "" Then
        If sdbgRes.Columns("AHORRADO").Value < 0 Then
            sdbgRes.Columns("AHORRADO").CellStyleSet "Red"
        Else
            If sdbgRes.Columns("AHORRADO").Value > 0 Then
                sdbgRes.Columns("AHORRADO").CellStyleSet "Green"
            End If
        End If
    End If
    
End Sub

Private Sub Timer1_Timer()

   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub

Private Sub sdbcADesdePres_Change()
    If Not bRespetarCombo Then
        If cmdGrafico.Visible Then
            sdbgRes.RemoveAll
        Else
            MSChart1.Visible = False
        End If
    End If
End Sub

Private Sub sdbcADesdePres_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
End Sub

Private Sub sdbcADesdePres_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    sdbcAHastaPres.Text = sdbcADesdePres.Text
End Sub

Private Sub sdbcADesdePres_Validate(Cancel As Boolean)
    bRespetarCombo = True
    If sdbcADesdePres.Text = "" Then
        sdbcAHastaPres.Text = ""
    End If
    bRespetarCombo = False
End Sub

Private Sub sdbcAHastaPres_Change()
    If Not bRespetarCombo Then
        If cmdGrafico.Visible Then
            sdbgRes.RemoveAll
        Else
            MSChart1.Visible = False
        End If
    End If

End Sub

Private Sub sdbcAHastaPres_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
End Sub

Private Sub sdbcAHastaPres_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
End Sub


Private Sub sdbcAHastaPres_Validate(Cancel As Boolean)
    bRespetarCombo = True
    If sdbcAHastaPres.Text = "" Then
        sdbcAHastaPres.Text = sdbcADesdePres.Text
    End If
    bRespetarCombo = False
End Sub

