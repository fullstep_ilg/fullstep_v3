VERSION 5.00
Begin VB.Form frmESTConfig 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "DConfiguraci�n de campos"
   ClientHeight    =   6240
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   5505
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   5505
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstCampos 
      Height          =   6135
      Left            =   60
      Style           =   1  'Checkbox
      TabIndex        =   0
      Top             =   60
      Width           =   5415
   End
End
Attribute VB_Name = "frmESTConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''''''''''''''''''''''''''''''  VARIABLES PRIVADAS  ''''''''''''''''''''''''''''''
Private m_stxtCampos(2 To 34) As String
Private m_bCargando As Boolean

''' <summary>
''' Cargar multidioma para Campos de visor procesos
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_EST_CONFIG, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value
        For i = 2 To 34
            Ador.MoveNext
            m_stxtCampos(i) = Ador(0).Value
        Next i
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()
    'Me.Left = frmEST.Left + Me.Width / 2
    'Me.Top = frmEST.Top + Me.Height / 4
    
    CargarRecursos
    
    m_bCargando = True
    CargarListaCampos
    m_bCargando = False
End Sub

''' <summary>
''' Cargar Lista Campos de visor procesos
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarListaCampos()
    Dim i As Integer
    '1 Configuracion de campos
    For i = 2 To 34
        lstCampos.AddItem m_stxtCampos(i)
    Next i
    'El campo 26 va separado del resto
    If Not Col_GsToERP_Visible() Then lstCampos.RemoveItem (24)
           
    For i = 0 To 23
        Select Case i
            Case 0  'A�o
                lstCampos.Selected(i) = frmEST.g_oConfVisor.AnyoVisible
            Case 1  'GMN1
                lstCampos.Selected(i) = frmEST.g_oConfVisor.GMN1Visible
            Case 2  'Proce
                lstCampos.Selected(i) = frmEST.g_oConfVisor.ProceVisible
            Case 3  'Descr
                lstCampos.Selected(i) = frmEST.g_oConfVisor.DescrVisible
            Case 4  'Estado
                lstCampos.Selected(i) = frmEST.g_oConfVisor.EstadoVisible
            Case 5  'Responsable
                lstCampos.Selected(i) = frmEST.g_oConfVisor.ResponsableVisible
            Case 6  'AdjDir
                lstCampos.Selected(i) = frmEST.g_oConfVisor.AdjDirVisible
            Case 7   'P
                lstCampos.Selected(i) = frmEST.g_oConfVisor.PVisible
            Case 8  'O
                lstCampos.Selected(i) = frmEST.g_oConfVisor.OVisible
            Case 9  'W
                lstCampos.Selected(i) = frmEST.g_oConfVisor.WVisible
            Case 10  'R
                lstCampos.Selected(i) = frmEST.g_oConfVisor.RVisible
            Case 11  'FecApe
                lstCampos.Selected(i) = frmEST.g_oConfVisor.FecApeVisible
            Case 12  'FecValApertura    (Fec.Val.Apertura) Fecha validaci�n apertura
                lstCampos.Selected(i) = frmEST.g_oConfVisor.FecValAperturaVisible
            Case 13  'FecLimOfe
                lstCampos.Selected(i) = frmEST.g_oConfVisor.FecLimOfeVisible
            Case 14  'FecPres
                lstCampos.Selected(i) = frmEST.g_oConfVisor.FecPresVisible
            Case 15  'FecCierre
                lstCampos.Selected(i) = frmEST.g_oConfVisor.FecCierreVisible
            Case 16  'FecNec
                lstCampos.Selected(i) = frmEST.g_oConfVisor.FecNecVisible
            Case 17  'FecIni
                lstCampos.Selected(i) = frmEST.g_oConfVisor.FecIniVisible
            Case 18  'FecFin
                lstCampos.Selected(i) = frmEST.g_oConfVisor.FecFinVisible
            Case 19  'Pres
                lstCampos.Selected(i) = frmEST.g_oConfVisor.PresVisible
            Case 20  'Consumido
                lstCampos.Selected(i) = frmEST.g_oConfVisor.ConsumidoVisible
            Case 21  'importe
                lstCampos.Selected(i) = frmEST.g_oConfVisor.ImporteVisible
            Case 22  'Ahorro
                lstCampos.Selected(i) = frmEST.g_oConfVisor.AhorroVisible
            Case 23  'PorcentajeAhorro  (% Ahorro)     % Ahorro del proceso
                lstCampos.Selected(i) = frmEST.g_oConfVisor.PorcentajeAhorroVisible
        End Select
    Next i
                
    'El campo 24 va separado del resto
    If Col_GsToERP_Visible() Then
        'existe
        For i = 24 To 32
            Select Case i
                Case 24  'gstoerp
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.gsToErpVisible
                Case 25  'moneda
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.MonedaVisible
                Case 26  '(Cambio)   Cambio de la moneda del proceso
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.CambioVisible
                Case 27  'Dest
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.DestinoVisible
                Case 28  'destden
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.DestinoDenVisible
                Case 29  'pago
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.PagoVisible
                Case 30  'Prove
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.ProveedorVisible
                Case 31  'proveDen
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.ProveedorDenVisible
                Case 32  'vinculada
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.SolicVinculadaVisible
            End Select
        Next i
    Else
        'No existe
        For i = 24 To 31
            Select Case i
                Case 24  'moneda
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.MonedaVisible
                Case 25  '(Cambio)   Cambio de la moneda del proceso
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.CambioVisible
                Case 26  'Dest
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.DestinoVisible
                Case 27  'destden
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.DestinoDenVisible
                Case 28  'pago
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.PagoVisible
                Case 29  'Prove
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.ProveedorVisible
                Case 30  'proveDen
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.ProveedorDenVisible
                Case 31  'vinculada
                    lstCampos.Selected(i) = frmEST.g_oConfVisor.SolicVinculadaVisible
            End Select
        Next i
    End If
End Sub

''' <summary>
''' Oculta o muestra el campo en la grid
''' </summary>
''' <param name="Item"> campo en la grid</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub lstCampos_ItemCheck(Item As Integer)
    Dim bVisible As Boolean
    Dim sColumna As String
    Dim iPos As Integer
    Dim iIndex As Integer
    Dim i As Integer
    Dim bNoScrollar As Boolean
    
    If m_bCargando = True Then Exit Sub
    
    bVisible = lstCampos.Selected(Item)
    frmEST.g_oConfVisor.HayCambios = True
    
    If Item <= 23 Then
        Select Case Item
            Case 0  'A�o
                frmEST.g_oConfVisor.AnyoVisible = bVisible: sColumna = "ANYO"
            Case 1  'GMN1
                frmEST.g_oConfVisor.GMN1Visible = bVisible: sColumna = "GMN1"
            Case 2  'Proce
                frmEST.g_oConfVisor.ProceVisible = bVisible: sColumna = "COD"
            Case 3  'Descr
                frmEST.g_oConfVisor.DescrVisible = bVisible: sColumna = "DESCR"
            Case 4  'Estado
                frmEST.g_oConfVisor.EstadoVisible = bVisible: sColumna = "EST"
            Case 5  'Responsable
                frmEST.g_oConfVisor.ResponsableVisible = bVisible: sColumna = "RESP"
            Case 6  'AdjDir
                frmEST.g_oConfVisor.AdjDirVisible = bVisible: sColumna = "ADJDIR"
            Case 7  'P
                frmEST.g_oConfVisor.PVisible = bVisible: sColumna = "P"
            Case 8  'O
                frmEST.g_oConfVisor.OVisible = bVisible: sColumna = "O"
            Case 9  'W
                frmEST.g_oConfVisor.WVisible = bVisible: sColumna = "W"
            Case 10  'R
                frmEST.g_oConfVisor.RVisible = bVisible: sColumna = "R"
            Case 11  'FecApe
                frmEST.g_oConfVisor.FecApeVisible = bVisible: sColumna = "FECAPE"
            Case 12  'FecValApertura
                frmEST.g_oConfVisor.FecValAperturaVisible = bVisible: sColumna = "FECVALAPERTURA"
            Case 13  'FecLimOfe
                frmEST.g_oConfVisor.FecLimOfeVisible = bVisible: sColumna = "FECLIMOFE"
            Case 14  'FecPres
                frmEST.g_oConfVisor.FecPresVisible = bVisible: sColumna = "FECPRES"
            Case 15  'FecCierre
                frmEST.g_oConfVisor.FecCierreVisible = bVisible: sColumna = "FECCIERRE"
            Case 16  'FecNec
                frmEST.g_oConfVisor.FecNecVisible = bVisible: sColumna = "FECNEC"
            Case 17  'FecIni
                frmEST.g_oConfVisor.FecIniVisible = bVisible: sColumna = "FECINI"
            Case 18  'FecFin
                frmEST.g_oConfVisor.FecFinVisible = bVisible: sColumna = "FECFIN"
            Case 19  'Pres
                frmEST.g_oConfVisor.PresVisible = bVisible: sColumna = "PRES"
            Case 20  'Consumido
                frmEST.g_oConfVisor.ConsumidoVisible = bVisible: sColumna = "CONS"
            Case 21  'importe
                frmEST.g_oConfVisor.ImporteVisible = bVisible: sColumna = "IMP"
            Case 22  'Ahorro
                frmEST.g_oConfVisor.AhorroVisible = bVisible: sColumna = "AHORRO"
            Case 23  'PorcentajeAhorro
                frmEST.g_oConfVisor.PorcentajeAhorroVisible = bVisible: sColumna = "PORCENTAJEAHORRO"
        End Select
    Else
        If Col_GsToERP_Visible() Then
            Select Case Item
                Case 24  'gsToErp
                    frmEST.g_oConfVisor.gsToErpVisible = bVisible: sColumna = "GSTOERP"
                Case 25  'moneda
                    frmEST.g_oConfVisor.MonedaVisible = bVisible: sColumna = "MON"
                Case 26  '(Cambio)
                    frmEST.g_oConfVisor.CambioVisible = bVisible: sColumna = "CAMBIO"
                Case 27  'Dest
                    frmEST.g_oConfVisor.DestinoVisible = bVisible: sColumna = "DEST"
                Case 28  'destden
                    frmEST.g_oConfVisor.DestinoDenVisible = bVisible: sColumna = "DESTDEN"
                Case 29  'pago
                    frmEST.g_oConfVisor.PagoVisible = bVisible: sColumna = "PAGO"
                Case 30  'Prove
                    frmEST.g_oConfVisor.ProveedorVisible = bVisible: sColumna = "PROVE"
                Case 31  'proveDen
                    frmEST.g_oConfVisor.ProveedorDenVisible = bVisible: sColumna = "PROVEDEN"
                Case 32  'vinculada
                    frmEST.g_oConfVisor.SolicVinculadaVisible = bVisible: sColumna = "VINCULADA"
            End Select
        Else
            Select Case Item
                Case 24  'moneda
                    frmEST.g_oConfVisor.MonedaVisible = bVisible: sColumna = "MON"
                Case 25  '(Cambio)
                    frmEST.g_oConfVisor.CambioVisible = bVisible: sColumna = "CAMBIO"
                Case 26  'Dest
                    frmEST.g_oConfVisor.DestinoVisible = bVisible: sColumna = "DEST"
                Case 27  'destden
                    frmEST.g_oConfVisor.DestinoDenVisible = bVisible: sColumna = "DESTDEN"
                Case 28  'pago
                    frmEST.g_oConfVisor.PagoVisible = bVisible: sColumna = "PAGO"
                Case 29  'Prove
                    frmEST.g_oConfVisor.ProveedorVisible = bVisible: sColumna = "PROVE"
                Case 30  'proveDen
                    frmEST.g_oConfVisor.ProveedorDenVisible = bVisible: sColumna = "PROVEDEN"
                Case 31  'vinculada
                    frmEST.g_oConfVisor.SolicVinculadaVisible = bVisible: sColumna = "VINCULADA"
            End Select
        End If
    End If
    
    'pongo visible o invisible la columna
    frmEST.sdbgProcesos.Columns(sColumna).Visible = bVisible
    
    'si se pone visible y es la anterior en posici�n a la primera que hay visible hay que scrollar la grid una posici�n
    If bVisible = True Then
        iIndex = frmEST.sdbgProcesos.ColContaining(1)   '�ndice de la primera columna visible
        iPos = frmEST.sdbgProcesos.Columns(iIndex).Position
        
        bNoScrollar = False
        For i = frmEST.sdbgProcesos.Columns(sColumna).Position + 1 To iPos - 1
            If frmEST.sdbgProcesos.Columns(frmEST.sdbgProcesos.ColPosition(i)).Visible = True Then
                bNoScrollar = True
                Exit For
            End If
        Next i
        
        If Not bNoScrollar Then
            frmEST.sdbgProcesos.Scroll -1, 0
        End If
    End If
End Sub


'ESTADO DEL PROCESO RESPECTO AL TRASPASO DE LAS ADJUDICACIONES
Private Function Col_GsToERP_Visible() As Boolean

    'If gParametrosIntegracion.gaExportar(EntidadIntegracion.Adj) Or gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Adj) duda

    If Not (gParametrosGenerales.gbTraspasoAdjERP And gParametrosIntegracion.gaExportar(EntidadIntegracion.Adj)) Then
        'si esto no se cumple el usuario no lo ver� en la lista de campos a mostrar en el visor)
        Col_GsToERP_Visible = False   'no vera la columna
    Else
        Col_GsToERP_Visible = True   'vera la columna
    End If
End Function


