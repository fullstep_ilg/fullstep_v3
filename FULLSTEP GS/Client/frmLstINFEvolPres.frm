VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstINFEvolPres 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de evoluci�n de partida"
   ClientHeight    =   4065
   ClientLeft      =   2205
   ClientTop       =   5445
   ClientWidth     =   7140
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstINFEvolPres.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4065
   ScaleWidth      =   7140
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      Height          =   375
      Left            =   5820
      TabIndex        =   14
      Top             =   3705
      Width           =   1335
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3675
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7155
      _ExtentX        =   12621
      _ExtentY        =   6482
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstINFEvolPres.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "FraTipo"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Timer1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "fraAnyoPres"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      Begin VB.Frame fraAnyoPres 
         Caption         =   "Considerar el presupuesto en los siguientes a�os"
         Height          =   855
         Left            =   135
         TabIndex        =   16
         Top             =   1710
         Width           =   6885
         Begin SSDataWidgets_B.SSDBCombo sdbcADesdePres 
            Height          =   285
            Left            =   1110
            TabIndex        =   17
            Top             =   330
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAHastaPres 
            Height          =   285
            Left            =   3360
            TabIndex        =   18
            Top             =   330
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin VB.Label lblHastaPres 
            Caption         =   "Hasta:"
            Height          =   255
            Left            =   2610
            TabIndex        =   20
            Top             =   360
            Width           =   660
         End
         Begin VB.Label lblDesdePres 
            Caption         =   "Desde:"
            Height          =   255
            Left            =   360
            TabIndex        =   19
            Top             =   360
            Width           =   705
         End
      End
      Begin VB.Timer Timer1 
         Interval        =   2000
         Left            =   4380
         Top             =   75
      End
      Begin VB.Frame FraTipo 
         Caption         =   "Filtro por presupuesto"
         Height          =   855
         Left            =   135
         TabIndex        =   10
         Top             =   2640
         Width           =   6870
         Begin VB.TextBox txtParCon 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   165
            Locked          =   -1  'True
            TabIndex        =   15
            Top             =   360
            Width           =   5730
         End
         Begin VB.CommandButton cmdSelParCon 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   6360
            Picture         =   "frmLstINFEvolPres.frx":0CCE
            Style           =   1  'Graphical
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   345
            Width           =   345
         End
         Begin VB.CommandButton cmdBorrar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5970
            Picture         =   "frmLstINFEvolPres.frx":0D3A
            Style           =   1  'Graphical
            TabIndex        =   11
            Top             =   345
            Width           =   345
         End
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   135
         TabIndex        =   1
         Top             =   375
         Width           =   6870
         Begin VB.CheckBox chkItems 
            Caption         =   "Incluir items"
            Height          =   285
            Left            =   4635
            TabIndex        =   13
            Top             =   750
            Visible         =   0   'False
            Width           =   1815
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMon 
            Height          =   285
            Left            =   1170
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   735
            Width           =   1140
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1164
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3651
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   1879
            Columns(2).Caption=   "Equivalencia"
            Columns(2).Name =   "EQUIV"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   2011
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyoDesde 
            Height          =   285
            Left            =   1170
            TabIndex        =   3
            Top             =   300
            Width           =   825
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1455
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMesDesde 
            Height          =   285
            Left            =   1980
            TabIndex        =   4
            Top             =   300
            Width           =   1260
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2222
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyoHasta 
            Height          =   285
            Left            =   4635
            TabIndex        =   5
            Top             =   300
            Width           =   825
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1455
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMesHasta 
            Height          =   285
            Left            =   5445
            TabIndex        =   6
            Top             =   300
            Width           =   1260
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2222
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin VB.Label lblMon 
            Caption         =   "Moneda:"
            Height          =   240
            Left            =   240
            TabIndex        =   9
            Top             =   750
            Width           =   915
         End
         Begin VB.Label lblAnyoHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   3810
            TabIndex        =   8
            Top             =   330
            Width           =   750
         End
         Begin VB.Label lblAnyoDesde 
            Caption         =   "Desde:"
            Height          =   195
            Left            =   270
            TabIndex        =   7
            Top             =   330
            Width           =   780
         End
      End
   End
End
Attribute VB_Name = "frmLstINFEvolPres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de restricciones
Public bRMat As Boolean

'Private arResultados() As Variant
Private Adores As Ador.Recordset

'Vbles. para la selecci�n de proyecto
Public sProy1 As String
Public sProy2 As String
Public sProy3 As String
Public sProy4 As String
Private bRespetarCombo As Boolean
'Vbles. para la selecci�n de partidas contables
Public sParCon1 As String
Public sParCon2 As String
Public sParCon3 As String
Public sParCon4 As String
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String
Private m_sLblUO As String
Private m_iNivelUO As Integer
Public bRUO As Boolean
' Origen llamada a formulario
Public iConcepto As Integer
Public sOrigen As String

'Variable para listado
Private sSeleccion As String
Private oFos As FileSystemObject
Private RepPath As String

'Variables para func. combos
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean
Private sMonedaOrig As String
Private sMoneda As String
Private oMonedas As CMonedas
Private oMon As CMoneda
Private dequivalencia As Double


'Multilenguaje
Private sEstados(1 To 14) As String
Private sIdiMonCent As String
Private sIdiMeses(12) As String
Private sIdiAnyoInic As String
Private sIdiMon As String
Private sMensajeSeleccion As String

Private stxtDesde As String
Private stxtHasta As String
Private stxtPres As String
Private stxtitems As String
Private stxtMon As String
Private stxtTitulo As String
Private FormulaRpt(1, 1 To 7) As String      'formulas textos RPT
Private stxtAnyo As String
Private stxtProceCod As String
Private stxtProceDen As String
Private stxtFecPres As String
Private stxtFecNec As String
Private stxtEstado As String
Private stxtAbierto As String
Private stxtConsumido As String
Private stxtAdjudicado As String
Private stxtAhorrado As String
Private stxtArticulo As String
Private stxtDescr As String
Private stxtPresUni As String
Private stxtCantidad As String
Private stxtPresTotal As String
Private stxtPorcen As String
Private stxtTotal As String
Private stxtFecLimiOfe As String
Private stxtFecAper As String
Private stxtResponsable As String
Private stxtSeleccion As String
'Private stxtTotal As String

Private sIdiGenerando As String
Private sIdiSeleccionando As String
Private sIdiVisualizando As String

Public Function cargarmoneda(mon As String)
    Dim oMonedas As CMonedas
    Dim oMon As CMoneda
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    
    oMonedas.CargarTodasLasMonedas mon, , True, , , , True
    sdbcMon.RemoveAll
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next

End Function

Private Sub ObtenerListadoEvolPres()
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
    Dim SubListado As CRAXDRT.Report
    Dim oCRInfEvolPres As CRInformes
    'Dim ListadoPorItems As Boolean
    Dim RepPath As String
    Dim i As Integer
    Dim Ador As Ador.Recordset
    Dim RecordSelFormula As String

    Set oCRInfEvolPres = GenerarCRInformes
    sSeleccion = GenerarTextoSeleccion

    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If

    If iConcepto = 1 Then
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptEvolucionPres1_1.rpt"
    Else
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptEvolucionPres2_1.rpt"
    End If

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing

    Screen.MousePointer = vbHourglass

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)

   '*********** FORMULA FIELDS REPORT
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & FormulaRpt(1, 1) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & FormulaRpt(1, 2) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSELECCION")).Text = """" & stxtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTITULO")).Text = """" & stxtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAnyo")).Text = """" & stxtAnyo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGmn1")).Text = """" & "GMN1" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProceCod")).Text = """" & stxtProceCod & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProceDen")).Text = """" & stxtProceDen & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecPres")).Text = """" & stxtFecPres & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecNec")).Text = """" & stxtFecNec & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecLimiOfe")).Text = """" & stxtFecLimiOfe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecAper")).Text = """" & stxtFecAper & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtResponsable")).Text = """" & stxtResponsable & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEstado")).Text = """" & stxtEstado & """"
    oReport.ParameterFields(crs_ParameterIndex(oReport, "EQUIV")).SetCurrentValue CDbl(sdbcMon.Columns(2).Value), 7
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAbierto")).Text = """" & stxtAbierto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtConsum")).Text = """" & stxtConsumido & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAdjudicado")).Text = """" & stxtAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAhorrado")).Text = """" & stxtAhorrado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPorcenAho")).Text = """" & stxtPorcen & """"
    
    'Textos de los estados
    For i = 1 To 14
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst" & i)).Text = """" & sEstados(i) & """"
    Next i
            
    If chkItems.Value = vbChecked Then
        Set SubListado = oReport.OpenSubreport("rptITEMS")
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SUPRIMIR")).Text = """" & "N" & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArt")).Text = """" & stxtArticulo & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDescr")).Text = """" & stxtDescr & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresUni")).Text = """" & stxtPresUni & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCant")).Text = """" & stxtCantidad & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPresTotal")).Text = """" & stxtPresTotal & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtConsum")).Text = """" & stxtConsumido & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAdjudicado")).Text = """" & stxtAdjudicado & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAhorrado")).Text = """" & stxtAhorrado & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcenAho")).Text = """" & stxtPorcen & """"
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPorcenAho")).Text = """" & stxtPorcen & """"
                    
        Set SubListado = Nothing
    
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SUPRIMIR")).Text = """" & "S" & """"
    End If
    
    
    If sOrigen = "LstInfEvolPres1" Or sOrigen = "LstInfEvolPres3" Or iConcepto = 1 Then
        Set Adores = oGestorInformes.InfEvolucionConcepto1(oSesionSummit.Id, val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sProy1, sProy2, sProy3, sProy4, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , False, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), sUON1, sUON2, sUON3)
    Else
        If sOrigen = "LstInfEvolPres2" Or sOrigen = "LstInfEvolPres4" Or iConcepto = 2 Then
            Set Adores = oGestorInformes.InfEvolucionConcepto2(oSesionSummit.Id, val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, sParCon2, sParCon3, sParCon4, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bRMat, , False, val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), sUON1, sUON2, sUON3)
        End If
    End If

    oCRInfEvolPres.ListadoEvolPresupuesto oReport, chkItems.Value, oSesionSummit.Id, iConcepto
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show

    Timer1.Enabled = True
    pv.caption = stxtTitulo
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False

    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set oCRInfEvolPres = Nothing


End Sub

Private Sub ConfigurarSeguridad()
    
    If oUsuarioSummit.Tipo <> Administrador Then
    
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador Then
                              
            If iConcepto = 1 Then
            
                If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto1RestMat)) Is Nothing Then
                    bRMat = True
                End If
                If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto1RestUO)) Is Nothing Then
                    bRUO = True
                End If
            
            ElseIf iConcepto = 2 Then
                
                If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto2RestMat)) Is Nothing Then
                    bRMat = True
                End If
                If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto2RestUO)) Is Nothing Then
                    bRUO = True
                End If
            
            End If
              
        End If
        
    End If
    If bRUO Then
        m_iNivelUO = 0
        If CStr(basOptimizacion.gUON3Usuario) <> "" Then
            m_iNivelUO = 3
        ElseIf CStr(basOptimizacion.gUON2Usuario) <> "" Then
            m_iNivelUO = 2
        ElseIf CStr(basOptimizacion.gUON1Usuario) <> "" Then
            m_iNivelUO = 1
        End If
        sUON1 = NullToStr(basOptimizacion.gUON1Usuario)
        sUON2 = NullToStr(basOptimizacion.gUON2Usuario)
        sUON3 = NullToStr(basOptimizacion.gUON3Usuario)
        m_sLblUO = ""
        If sUON1 <> "" Then
            m_sLblUO = "(" & sUON1
            If sUON2 <> "" Then
                m_sLblUO = m_sLblUO & " - " & sUON2
                If sUON3 <> "" Then
                    m_sLblUO = m_sLblUO & " - " & sUON3 & ") "
                Else
                    m_sLblUO = m_sLblUO & ") "
                End If
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        End If
        txtParCon.Text = m_sLblUO
    End If
        
End Sub

Public Sub MostrarProySeleccionado()
Dim iNivel As Integer
Dim i As Integer

    
    sProy1 = frmSELPresAnuUON.g_sPRES1
    sProy2 = frmSELPresAnuUON.g_sPRES2
    sProy3 = frmSELPresAnuUON.g_sPRES3
    sProy4 = frmSELPresAnuUON.g_sPRES4
    sUON1 = frmSELPresAnuUON.g_sUON1
    sUON2 = frmSELPresAnuUON.g_sUON2
    sUON3 = frmSELPresAnuUON.g_sUON3
    
    m_sLblUO = ""
    If sUON1 <> "" Then
        m_sLblUO = "(" & sUON1
        If sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & sUON2
            If sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If
    
    iNivel = 1
    If sProy4 <> "" Then
        txtParCon = m_sLblUO & sProy1 & " - " & sProy2 & " - " & sProy3 & " - " & sProy4 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 4
    ElseIf sProy3 <> "" Then
        txtParCon = m_sLblUO & sProy1 & " - " & sProy2 & " - " & sProy3 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 3
    ElseIf sProy2 <> "" Then
        txtParCon = m_sLblUO & sProy1 & " - " & sProy2 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 2
    ElseIf sProy1 <> "" Then
        txtParCon = m_sLblUO & sProy1 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 1
    Else
        txtParCon = m_sLblUO
    End If
End Sub

Private Function GenerarTextoSeleccion() As String

    Dim sUnion As String

    sSeleccion = ""
    sUnion = ""

    sSeleccion = sSeleccion & stxtDesde & " " & sdbcMesDesde
    sSeleccion = sSeleccion & " " & Format(val(sdbcAnyoDesde), "0000")
    sSeleccion = sSeleccion & " " & stxtHasta & " " & sdbcMesHasta
    sSeleccion = sSeleccion & " " & Format(val(sdbcAnyoHasta), "0000")
 
    sSeleccion = sSeleccion & "; " & stxtPres & ": " & txtParCon

    If chkItems Then
        sSeleccion = sSeleccion & "; " & stxtitems
    End If

    If sdbcMon <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtMon & ": " & Trim(sdbcMon)
    End If

    GenerarTextoSeleccion = sSeleccion

End Function

'

Public Sub MostrarParConSeleccionada()
    
Dim iNivel As Integer
Dim i As Integer
    
    sParCon1 = frmSELPresAnuUON.g_sPRES1
    sParCon2 = frmSELPresAnuUON.g_sPRES2
    sParCon3 = frmSELPresAnuUON.g_sPRES3
    sParCon4 = frmSELPresAnuUON.g_sPRES4
    sUON1 = frmSELPresAnuUON.g_sUON1
    sUON2 = frmSELPresAnuUON.g_sUON2
    sUON3 = frmSELPresAnuUON.g_sUON3
    
    m_sLblUO = ""
    If sUON1 <> "" Then
        m_sLblUO = "(" & sUON1
        If sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & sUON2
            If sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = m_sLblUO & ") "
        End If
    End If
    
    iNivel = 1
    If sParCon4 <> "" Then
        txtParCon = m_sLblUO & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " - " & sParCon4 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 4
    ElseIf sParCon3 <> "" Then
        txtParCon = m_sLblUO & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 3
    ElseIf sParCon2 <> "" Then
        txtParCon = m_sLblUO & sParCon1 & " - " & sParCon2 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 2
    ElseIf sParCon1 <> "" Then
        txtParCon = m_sLblUO & sParCon1 & " " & frmSELPresAnuUON.g_sDenPres
        iNivel = 1
    Else
        txtParCon = m_sLblUO
    End If

    
End Sub


Private Sub CargarAnyos()
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyoDesde.AddItem iInd
        sdbcAnyoHasta.AddItem iInd
        sdbcADesdePres.AddItem iInd
        sdbcAHastaPres.AddItem iInd
        
    Next
    
    sdbcAnyoDesde.Text = iAnyoActual
    sdbcAnyoDesde.ListAutoPosition = True
    sdbcAnyoDesde.Scroll 1, 7
    sdbcAnyoHasta.Text = iAnyoActual
    sdbcAnyoHasta.ListAutoPosition = True
    sdbcAnyoHasta.Scroll 1, 7
    sdbcADesdePres.Scroll 1, 7
    sdbcAHastaPres.Scroll 1, 7

    sdbcMesDesde.Text = "1"
    sdbcMesDesde.ListAutoPosition = True
    sdbcMesHasta.Text = Month(Date) - 1
    sdbcMesHasta.ListAutoPosition = True
    
    sdbcMesDesde.AddItem sIdiMeses(1)
    sdbcMesDesde.AddItem sIdiMeses(2)
    sdbcMesDesde.AddItem sIdiMeses(3)
    sdbcMesDesde.AddItem sIdiMeses(4)
    sdbcMesDesde.AddItem sIdiMeses(5)
    sdbcMesDesde.AddItem sIdiMeses(6)
    sdbcMesDesde.AddItem sIdiMeses(7)
    sdbcMesDesde.AddItem sIdiMeses(8)
    sdbcMesDesde.AddItem sIdiMeses(9)
    sdbcMesDesde.AddItem sIdiMeses(10)
    sdbcMesDesde.AddItem sIdiMeses(11)
    sdbcMesDesde.AddItem sIdiMeses(12)
    
    sdbcMesHasta.AddItem sIdiMeses(1)
    sdbcMesHasta.AddItem sIdiMeses(2)
    sdbcMesHasta.AddItem sIdiMeses(3)
    sdbcMesHasta.AddItem sIdiMeses(4)
    sdbcMesHasta.AddItem sIdiMeses(5)
    sdbcMesHasta.AddItem sIdiMeses(6)
    sdbcMesHasta.AddItem sIdiMeses(7)
    sdbcMesHasta.AddItem sIdiMeses(8)
    sdbcMesHasta.AddItem sIdiMeses(9)
    sdbcMesHasta.AddItem sIdiMeses(10)
    sdbcMesHasta.AddItem sIdiMeses(11)
    sdbcMesHasta.AddItem sIdiMeses(12)
    
    sdbcMesDesde.MoveFirst
    sdbcMesDesde.Text = sdbcMesDesde.Columns(0).Value
    
    sdbcMesHasta.MoveFirst
    
    For iInd = 1 To Month(Date) - 2 '
        sdbcMesHasta.MoveNext
    Next
    
    sdbcMesHasta.Text = sdbcMesHasta.Columns(0).Value
    
    
    
End Sub



Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTINFEVOLPRES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
               
        If iConcepto = 2 Then
            sMensajeSeleccion = gParametrosGenerales.gsSingPres2
            stxtPres = gParametrosGenerales.gsSingPres2
            If InStr(1, Ador(0).Value, "PROY2") = 1 Then
                caption = Replace(Ador(0).Value, "PROY2", gParametrosGenerales.gsSingPres2)
            Else
                caption = Replace(Ador(0).Value, "PROY2", LCase(gParametrosGenerales.gsSingPres2))
            End If
            stxtTitulo = caption
            Ador.MoveNext
         Else
            sMensajeSeleccion = gParametrosGenerales.gsSingPres1
            stxtPres = gParametrosGenerales.gsSingPres1
            Ador.MoveNext
            If InStr(1, Ador(0).Value, "PROY1") = 1 Then
                caption = Replace(Ador(0).Value, "PROY1", gParametrosGenerales.gsSingPres1)
            Else
                caption = Replace(Ador(0).Value, "PROY1", LCase(gParametrosGenerales.gsSingPres1))
            End If
            stxtTitulo = caption
        End If
        
        
        Ador.MoveNext
        SSTab1.caption = Ador(0).Value
        stxtSeleccion = Ador(0).Value
        Ador.MoveNext
        lblAnyoDesde.caption = Ador(0).Value
        lblDesdePres.caption = Ador(0).Value
        stxtDesde = Ador(0).Value
        Ador.MoveNext
        lblAnyoHasta.caption = Ador(0).Value
        lblHastaPres.caption = Ador(0).Value
        stxtHasta = Ador(0).Value
        Ador.MoveNext
        lblMon.caption = Ador(0).Value
        Ador.MoveNext
        chkItems.caption = Ador(0).Value
        Ador.MoveNext
        If iConcepto = 2 Then
            If InStr(1, Ador(0).Value, "PROY2") = 1 Then
                FraTipo.caption = Replace(Ador(0).Value, "PROY2", gParametrosGenerales.gsSingPres2)
            Else
                FraTipo.caption = Replace(Ador(0).Value, "PROY2", LCase(gParametrosGenerales.gsSingPres2))
            End If
            Ador.MoveNext
        Else
            Ador.MoveNext
            If InStr(1, Ador(0).Value, "PROY1") = 1 Then
                FraTipo.caption = Replace(Ador(0).Value, "PROY1", gParametrosGenerales.gsSingPres1)
            Else
                FraTipo.caption = Replace(Ador(0).Value, "PROY1", LCase(gParametrosGenerales.gsSingPres1))
            End If
        End If
        
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value
        Ador.MoveNext
        
        For i = 1 To 12
            sIdiMeses(i) = Ador(0).Value
            Ador.MoveNext
        Next
        
        sIdiAnyoInic = Ador(0).Value
        Ador.MoveNext
        sIdiMon = Ador(0).Value
        stxtMon = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        stxtitems = Ador(0).Value
        Ador.MoveNext
        
        
        ' FORMULAS RPT
        For i = 1 To 2
            FormulaRpt(1, i) = Ador(0).Value
            Ador.MoveNext
        Next
        
        'Report
        stxtProceCod = Ador(0).Value
        Ador.MoveNext
        stxtProceDen = Ador(0).Value
        Ador.MoveNext
        stxtFecPres = Ador(0).Value
        Ador.MoveNext
        stxtFecNec = Ador(0).Value
        Ador.MoveNext
        stxtEstado = Ador(0).Value
        Ador.MoveNext
        stxtAbierto = Ador(0).Value
        Ador.MoveNext
        stxtConsumido = Ador(0).Value
        Ador.MoveNext
        stxtAdjudicado = Ador(0).Value
        Ador.MoveNext
        stxtAhorrado = Ador(0).Value
        Ador.MoveNext
        
        'SubReport
        stxtArticulo = Ador(0).Value
        Ador.MoveNext
        stxtDescr = Ador(0).Value
        Ador.MoveNext
        stxtPresUni = Ador(0).Value
        Ador.MoveNext
        stxtCantidad = Ador(0).Value
        Ador.MoveNext
        stxtPresTotal = Ador(0).Value
        Ador.MoveNext
        stxtPorcen = Ador(0).Value
        Ador.MoveNext
        
        If iConcepto = 1 Then
            If InStr(1, Ador(0).Value, "PROY1") = 1 Then
                sIdiGenerando = Replace(Ador(0).Value, "PROY1", gParametrosGenerales.gsSingPres1)
            Else
                sIdiGenerando = Replace(Ador(0).Value, "PROY1", LCase(gParametrosGenerales.gsSingPres1))
            End If
            Ador.MoveNext
        Else
            Ador.MoveNext
            If InStr(1, Ador(0).Value, "PROY2") = 1 Then
                sIdiGenerando = Replace(Ador(0).Value, "PROY2", gParametrosGenerales.gsSingPres2)
            Else
                sIdiGenerando = Replace(Ador(0).Value, "PROY2", LCase(gParametrosGenerales.gsSingPres2))
            End If
        End If
        
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext
        stxtAnyo = Ador(0).Value
        Ador.MoveNext
        stxtitems = Ador(0).Value
        Ador.MoveNext
        
        sEstados(1) = Ador(0).Value
        sEstados(2) = Ador(0).Value
        Ador.MoveNext
        sEstados(3) = Ador(0).Value
        sEstados(4) = Ador(0).Value
        Ador.MoveNext
        sEstados(5) = Ador(0).Value
        Ador.MoveNext
        sEstados(6) = Ador(0).Value
        sEstados(7) = Ador(0).Value
        Ador.MoveNext
        sEstados(8) = Ador(0).Value
        sEstados(9) = Ador(0).Value
        Ador.MoveNext
        sEstados(10) = Ador(0).Value
        Ador.MoveNext
        sEstados(11) = Ador(0).Value
        Ador.MoveNext
        sEstados(12) = Ador(0).Value
        Ador.MoveNext
        sEstados(13) = Ador(0).Value
        Ador.MoveNext
        
        stxtFecAper = Ador(0).Value
        Ador.MoveNext
        
        stxtFecLimiOfe = Ador(0).Value
        Ador.MoveNext
        stxtResponsable = Ador(0).Value
        Ador.MoveNext
        stxtTotal = Ador(0).Value
        Ador.MoveNext
        sEstados(14) = Ador(0).Value
        Ador.MoveNext
        fraAnyoPres.caption = Ador(0).Value
        
        Ador.Close
    
    End If

    Set Ador = Nothing


End Sub

Private Sub cmdObtener_Click()
  Dim iAnyoIni As Integer
Dim iAnyoFin As Integer
Dim iMesIni As Integer
Dim iMesFin As Integer

'If bRMat Then
  
    If sdbcMon = "" Then
        oMensajes.NoValido sIdiMon
        If Me.Visible Then sdbcMon.SetFocus
        Exit Sub
    End If
    
    iAnyoIni = val(sdbcAnyoDesde)
    iAnyoFin = val(sdbcAnyoHasta)
    If iAnyoFin < iAnyoIni Then
        oMensajes.PeriodoNoValido
        If Me.Visible Then sdbcAnyoDesde.SetFocus
        Exit Sub
    Else
        iMesIni = sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1
        iMesFin = sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1
        If (iAnyoIni = iAnyoFin) And (iMesIni > iMesFin) Then
            oMensajes.PeriodoNoValido
            If Me.Visible Then sdbcMesDesde.SetFocus
            Exit Sub
        End If
    End If
    
    If txtParCon.Text <> "" Then
        Screen.MousePointer = vbHourglass
        ObtenerListadoEvolPres
        Screen.MousePointer = vbNormal
    Else
        oMensajes.FaltanDatos (sMensajeSeleccion)
        Exit Sub
    End If


End Sub

Private Sub cmdSelParCon_Click()
    If sdbcAnyoDesde = "" Then
        oMensajes.NoValido sIdiAnyoInic
        If Me.Visible Then sdbcAnyoDesde.SetFocus
        Exit Sub
    End If
    
    If iConcepto = 1 Then
        If sOrigen = "Listados" Then
            frmSELPresAnuUON.sOrigen = "LstInfEvolPres3"
        Else
            frmSELPresAnuUON.sOrigen = "LstInfEvolPres1"
        End If
        frmSELPresAnuUON.bRUO = bRUO
        frmSELPresAnuUON.g_iTipoPres = 1
        frmSELPresAnuUON.bMostrarBajas = True
        frmSELPresAnuUON.Show 1
    
    ElseIf iConcepto = 2 Then
        If sOrigen = "Listados" Then
            frmSELPresAnuUON.sOrigen = "LstInfEvolPres4"
        Else
            frmSELPresAnuUON.sOrigen = "LstInfEvolPres2"
        End If
                
        frmSELPresAnuUON.bRUO = bRUO
        frmSELPresAnuUON.g_iTipoPres = 2
        frmSELPresAnuUON.bMostrarBajas = True
        frmSELPresAnuUON.Show 1
    End If

End Sub

Private Sub Form_Load()
Dim i As Integer
    
    ConfigurarSeguridad
    
    Me.Height = 4470
    Me.Width = 7260
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        
        sMonedaOrig = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        dequivalencia = oMonedas.Item(1).Equiv
    End If
    
    CargarAnyos
    
    chkItems.Value = vbUnchecked

End Sub

Private Sub Form_Unload(Cancel As Integer)
    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""
    sProy1 = ""
    sProy2 = ""
    sProy3 = ""
    sProy4 = ""
    
    oGestorInformes.BorrarTemporal iConcepto, oSesionSummit.Id
End Sub

Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        sMoneda = ""
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    sMoneda = sdbcMon.Columns(1).Text
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
        
End Sub
Private Sub sdbcMon_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    sdbcMon.RemoveAll
    
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedas , , , , , False, True

    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    

    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcMon.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMon.Rows - 1
            bm = sdbcMon.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMon.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcMon.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
        sMoneda = ""
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
        
    End If
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub cmdBorrar_Click()
    txtParCon.Text = ""
End Sub


Private Sub sdbcADesdePres_CloseUp()
    sdbcAHastaPres.Text = sdbcADesdePres.Text
End Sub

Private Sub sdbcADesdePres_Validate(Cancel As Boolean)
    bRespetarCombo = True
    If sdbcADesdePres.Text = "" Then
        sdbcAHastaPres.Text = ""
    End If
    bRespetarCombo = False
End Sub

Private Sub sdbcAHastaPres_Validate(Cancel As Boolean)
    bRespetarCombo = True
    If sdbcAHastaPres.Text = "" Then
        sdbcAHastaPres.Text = sdbcADesdePres.Text
    End If
    bRespetarCombo = False
End Sub

Private Sub Timer1_Timer()
    If frmESPERA.ProgressBar2.Value < 90 Then
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
    End If
    If frmESPERA.ProgressBar1.Value < 10 Then
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    End If
End Sub
