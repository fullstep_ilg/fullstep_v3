VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmCONFSubasta 
   BackColor       =   &H00808000&
   Caption         =   "Conversi�n de proceso a modo subasta"
   ClientHeight    =   7710
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10215
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCONFSubasta.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7710
   ScaleWidth      =   10215
   Begin VB.PictureBox picTextoFin 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   2295
      Left            =   0
      ScaleHeight     =   2295
      ScaleWidth      =   10215
      TabIndex        =   34
      Top             =   4800
      Width           =   10215
      Begin TabDlg.SSTab stabTextoFin 
         Height          =   1935
         Left            =   120
         TabIndex        =   25
         Top             =   240
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   3413
         _Version        =   393216
         Tabs            =   1
         TabHeight       =   520
         BackColor       =   8421376
         TabCaption(0)   =   "Tab 0"
         TabPicture(0)   =   "frmCONFSubasta.frx":014A
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "picTab(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         Begin VB.PictureBox picTab 
            BorderStyle     =   0  'None
            Height          =   1335
            Index           =   0
            Left            =   120
            ScaleHeight     =   1335
            ScaleWidth      =   9735
            TabIndex        =   36
            Top             =   480
            Width           =   9735
            Begin VB.TextBox txtTextoFin 
               Height          =   1215
               Index           =   0
               Left            =   0
               MultiLine       =   -1  'True
               TabIndex        =   26
               Top             =   0
               Width           =   9735
            End
         End
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   10200
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Label lblTextoFin 
         BackColor       =   &H00808000&
         Caption         =   "Texto de fin de subasta:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   120
         TabIndex        =   35
         Top             =   0
         Width           =   2025
      End
   End
   Begin VB.PictureBox picBotones 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   0
      ScaleHeight     =   615
      ScaleWidth      =   10215
      TabIndex        =   29
      Top             =   7080
      Width           =   10215
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   3975
         TabIndex        =   27
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   5235
         TabIndex        =   28
         Top             =   120
         Width           =   1005
      End
   End
   Begin VB.PictureBox picControles 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   4815
      Left            =   0
      ScaleHeight     =   4815
      ScaleWidth      =   10215
      TabIndex        =   0
      Top             =   0
      Width           =   10215
      Begin VB.CheckBox chkBajadaMin 
         BackColor       =   &H00808000&
         Caption         =   "Establecer bajada m�nima de pujas ganadoras"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   2400
         Width           =   4635
      End
      Begin VB.Frame fraPujasProve 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         ForeColor       =   &H00FFFFFF&
         Height          =   2055
         Left            =   5160
         TabIndex        =   38
         Top             =   2640
         Width           =   5055
         Begin VB.TextBox txtBajadaProcProv 
            Height          =   285
            Left            =   3600
            TabIndex        =   22
            Top             =   960
            Width           =   1170
         End
         Begin VB.TextBox txtBajadaGrupoProv 
            Height          =   285
            Left            =   3600
            TabIndex        =   23
            Top             =   1320
            Width           =   1170
         End
         Begin VB.TextBox txtBajadaItemProv 
            Height          =   285
            Left            =   3600
            TabIndex        =   24
            Top             =   1680
            Width           =   1170
         End
         Begin VB.OptionButton rdbImporteProve 
            BackColor       =   &H00808000&
            Caption         =   "Importe fijo"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   240
            TabIndex        =   21
            Top             =   480
            Width           =   2535
         End
         Begin VB.OptionButton rdbPorcentProve 
            BackColor       =   &H00808000&
            Caption         =   "Porcentaje"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   240
            TabIndex        =   20
            Top             =   120
            Width           =   2295
         End
         Begin VB.Label lblPorcent6 
            BackColor       =   &H00808000&
            Caption         =   "%"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   4800
            TabIndex        =   50
            Top             =   1680
            Width           =   255
         End
         Begin VB.Label lblPorcent5 
            BackColor       =   &H00808000&
            Caption         =   "%"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   4800
            TabIndex        =   49
            Top             =   1320
            Width           =   255
         End
         Begin VB.Label lblPorcent4 
            BackColor       =   &H00808000&
            Caption         =   "%"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   4800
            TabIndex        =   48
            Top             =   960
            Width           =   255
         End
         Begin VB.Label lblBajadaProcProv 
            BackColor       =   &H00808000&
            Caption         =   "Bajada m�nima en el importe total del proceso:"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   120
            TabIndex        =   44
            Top             =   960
            Width           =   3465
         End
         Begin VB.Label lblBajadaGrupoProv 
            BackColor       =   &H00808000&
            Caption         =   "Bajada m�nima en el importe total del grupo:"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   120
            TabIndex        =   43
            Top             =   1320
            Width           =   3465
         End
         Begin VB.Label lblBajadaItemProv 
            BackColor       =   &H00808000&
            Caption         =   "Bajada m�nima en el precio de cada �tem:"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   120
            TabIndex        =   42
            Top             =   1680
            Width           =   3465
         End
      End
      Begin VB.Frame fraPujasGan 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         ForeColor       =   &H00FFFFFF&
         Height          =   2055
         Left            =   0
         TabIndex        =   37
         Top             =   2640
         Width           =   5055
         Begin VB.TextBox txtBajadaProc 
            Height          =   285
            Left            =   3600
            TabIndex        =   16
            Top             =   960
            Width           =   1170
         End
         Begin VB.TextBox txtBajadaGrupo 
            Height          =   285
            Left            =   3600
            TabIndex        =   17
            Top             =   1320
            Width           =   1170
         End
         Begin VB.TextBox txtBajadaItem 
            Height          =   285
            Left            =   3600
            TabIndex        =   18
            Top             =   1680
            Width           =   1170
         End
         Begin VB.OptionButton rdbImporteGan 
            BackColor       =   &H00808000&
            Caption         =   "Importe fijo"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   120
            TabIndex        =   15
            Top             =   480
            Width           =   2655
         End
         Begin VB.OptionButton rdbPorcentGan 
            BackColor       =   &H00808000&
            Caption         =   "Porcentaje"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   120
            TabIndex        =   14
            Top             =   120
            Width           =   2655
         End
         Begin VB.Label lblPorcent3 
            BackColor       =   &H00808000&
            Caption         =   "%"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   4800
            TabIndex        =   47
            Top             =   1680
            Width           =   255
         End
         Begin VB.Label lblPorcent2 
            BackColor       =   &H00808000&
            Caption         =   "%"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   4800
            TabIndex        =   46
            Top             =   1320
            Width           =   255
         End
         Begin VB.Label lblPorcent1 
            BackColor       =   &H00808000&
            Caption         =   "%"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   4800
            TabIndex        =   45
            Top             =   960
            Width           =   255
         End
         Begin VB.Label lblBajadaProc 
            BackColor       =   &H00808000&
            Caption         =   "Bajada m�nima en el importe total del proceso:"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   120
            TabIndex        =   41
            Top             =   960
            Width           =   3465
         End
         Begin VB.Label lblBajadaGrupo 
            BackColor       =   &H00808000&
            Caption         =   "Bajada m�nima en el importe total del grupo:"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   120
            TabIndex        =   40
            Top             =   1320
            Width           =   3465
         End
         Begin VB.Label lblBajadaItem 
            BackColor       =   &H00808000&
            Caption         =   "Bajada m�nima en el precio de cada �tem:"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   120
            TabIndex        =   39
            Top             =   1680
            Width           =   3465
         End
      End
      Begin VB.TextBox txtDuracion 
         Height          =   285
         Left            =   1920
         TabIndex        =   3
         Top             =   600
         Width           =   735
      End
      Begin MSComCtl2.UpDown udDuracion 
         Height          =   290
         Left            =   2640
         TabIndex        =   4
         Top             =   610
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   -1  'True
      End
      Begin VB.CheckBox chkPublicar 
         BackColor       =   &H00808000&
         Caption         =   "Publicar y comunicar a los proveedores seleccionados"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   4395
      End
      Begin VB.CheckBox chkPrecio 
         BackColor       =   &H00808000&
         Caption         =   "Mostrar el precio de la puja ganadora"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   5160
         TabIndex        =   11
         Top             =   1560
         Width           =   3435
      End
      Begin VB.CheckBox chkGanador 
         BackColor       =   &H00808000&
         Caption         =   "Mostrar el nombre del proveedor ganador"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   1560
         Width           =   3435
      End
      Begin VB.TextBox txtMinutos 
         Height          =   285
         Left            =   7440
         TabIndex        =   6
         Top             =   600
         Width           =   810
      End
      Begin VB.CheckBox chkBajadaMinProv 
         BackColor       =   &H00808000&
         Caption         =   "Establecer bajada m�nima para pujas de un mismo proveedor"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   5280
         TabIndex        =   19
         Top             =   2400
         Width           =   4755
      End
      Begin VB.CheckBox chkNotificar 
         BackColor       =   &H00808000&
         Caption         =   "Enviar emails de notificaci�n de eventos"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   5160
         TabIndex        =   8
         Top             =   1080
         Width           =   4395
      End
      Begin VB.CheckBox chkInfoPuja 
         BackColor       =   &H00808000&
         Caption         =   "No mostrar informaci�n antes de la primera puja"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   1920
         Width           =   4515
      End
      Begin VB.CheckBox chkDetalle 
         BackColor       =   &H00808000&
         Caption         =   "Mostrar el detalle de pujas"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   5160
         TabIndex        =   12
         Top             =   1920
         Width           =   3435
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoSubasta 
         Height          =   285
         Left            =   1440
         TabIndex        =   1
         Top             =   120
         Width           =   3345
         ScrollBars      =   0
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5900
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcModoSubasta 
         Height          =   285
         Left            =   6480
         TabIndex        =   2
         Top             =   120
         Width           =   3345
         ScrollBars      =   0
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5900
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcDuracionSubasta 
         Height          =   285
         Left            =   3000
         TabIndex        =   5
         Top             =   600
         Width           =   1785
         ScrollBars      =   0
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3149
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.Label lblTipoSubasta 
         BackColor       =   &H00808000&
         Caption         =   "Tipo de subasta:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   120
         TabIndex        =   33
         Top             =   120
         Width           =   1425
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   10200
         Y1              =   480
         Y2              =   480
      End
      Begin VB.Label lblDuracion 
         BackColor       =   &H00808000&
         Caption         =   "Duraci�n de la subasta:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   120
         TabIndex        =   32
         Top             =   600
         Width           =   1800
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   10200
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Label lblMinutos 
         BackColor       =   &H00808000&
         Caption         =   "Minutos de espera para cierre"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   5160
         TabIndex        =   31
         Top             =   600
         Width           =   2265
      End
      Begin VB.Line Line8 
         BorderColor     =   &H00FFFFFF&
         X1              =   5115
         X2              =   5115
         Y1              =   2280
         Y2              =   4755
      End
      Begin VB.Line Line6 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   10200
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Line Line7 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   10200
         Y1              =   4755
         Y2              =   4755
      End
      Begin VB.Label lblModoSubasta 
         BackColor       =   &H00808000&
         Caption         =   "Modo subasta:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   5160
         TabIndex        =   30
         Top             =   120
         Width           =   1425
      End
   End
End
Attribute VB_Name = "frmCONFSubasta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const m_lSeparador = 127
Private Const m_lAlturaSep = 100
Private Const m_lAnchuraSep = 100
Private Const m_lAlturaFormCaption = 200
Private Const m_lAnchuraFormMin = 10335
Private Const m_lAlturaFormMin = 8115

'Valores par�metros
Public g_iSubTipo As Integer
Public g_iSubModo As Integer
Public g_vSubDuracion As Variant
Public g_iSubastaEspera As Integer
Public g_bSubPublicar As Boolean
Public g_bSubNotifEventos As Boolean
Public g_bSubastaProve As Boolean
Public g_bSubVerDesdePrimPuja As Boolean
Public g_bSubastaPrecioPuja As Boolean
Public g_bSubastaPujas As Boolean
Public g_bSubastaBajMinPuja As Boolean
Public g_iSubBajMinGanTipo As Integer
Public g_vSubBajMinGanProcVal As Variant
Public g_vSubBajMinGanGrupoVal As Variant
Public g_vSubBajMinGanItemVal As Variant
Public g_bSubBajMinProve As Boolean
Public g_iSubBajMinProveTipo As Integer
Public g_vSubBajMinProveProcVal As Variant
Public g_vSubBajMinProveGrupoVal As Variant
Public g_vSubBajMinProveItemVal As Variant
Public g_dcSubTextosFin As Dictionary

Public g_bModoEdicion As Boolean
Public g_sOrigen As String
Public g_bOK As Boolean
Public g_bCancelarReg As Boolean
Public g_sComentario As String

'Textos
Private msDescSubastaProceso As String
Private msDescSubastaGrupo As String
Private msDescSubastaItem As String
Private msDuracionDias As String
Private msDuracionHoras As String
Private msDuracionMinutos As String
Private msTipoSubasta As String
Private msModoSubasta As String
Private msBajadaProc As String
Private msBajadaGrupo As String
Private msBajadaItem As String
Private msSubastaInglesa As String
Private msSubastaSobreCerrado As String
Private msProceso As String
Private msGrupo As String
Private msItem As String
Private msSelectArchivos As String
Private msTodosArchivos As String
Private msMinutosEspera As String
Private msArchivo As String
Private msTama�o As String
Private msComentario As String
Private msFecha As String
Private msKb As String

Private Sub chkBajadaMin_Click()
    Dim bEnabled As Boolean
    If chkBajadaMin.Value = vbChecked Then bEnabled = True
    txtBajadaProc.Enabled = bEnabled
    txtBajadaGrupo.Enabled = bEnabled
    txtBajadaItem.Enabled = bEnabled
End Sub

Private Sub chkBajadaMinProv_Click()
    Dim bEnabled As Boolean
    If chkBajadaMinProv.Value = vbChecked Then bEnabled = True
    txtBajadaProcProv.Enabled = bEnabled
    txtBajadaGrupoProv.Enabled = bEnabled
    txtBajadaItemProv.Enabled = bEnabled
End Sub

Private Sub cmdAceptar_Click()
    If ComprobarDatos Then
        If ActualizarDatosParametros Then
            g_bOK = True
            Me.Hide
        End If
    End If
End Sub

Private Sub cmdCancelar_Click()
    g_bOK = False
    Me.Hide
End Sub

''' <summary>Comprueba que los datos introducidos son correctos</summary>
''' <remarks>Llamada desde cmdAceptar_Click</remarks>

Private Function ComprobarDatos() As Boolean
    On Error GoTo Error
    
    ComprobarDatos = False
    
    If sdbcTipoSubasta.Text = "" Then
        oMensajes.NoValido msTipoSubasta
        If Me.Visible Then sdbcTipoSubasta.SetFocus
        Exit Function
    End If
    
    If sdbcModoSubasta.Text = "" Then
        oMensajes.NoValido msModoSubasta
        If Me.Visible Then sdbcModoSubasta.SetFocus
        Exit Function
    End If
    
    If Not ComprobarCantidad(txtMinutos, msMinutosEspera) Then Exit Function
    If Not ComprobarCantidad(txtBajadaProc, msBajadaProc) Then Exit Function
    If Not ComprobarCantidad(txtBajadaGrupo, msBajadaGrupo) Then Exit Function
    If Not ComprobarCantidad(txtBajadaItem, msBajadaItem) Then Exit Function
    If Not ComprobarCantidad(txtBajadaProcProv, msBajadaProc) Then Exit Function
    If Not ComprobarCantidad(txtBajadaGrupoProv, msBajadaGrupo) Then Exit Function
    If Not ComprobarCantidad(txtBajadaItemProv, msBajadaItem) Then Exit Function
        
    ComprobarDatos = True
    
    Exit Function
Error:
    ComprobarDatos = False
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
End Function

''' <summary>Devuelve Si el valor de una caja de texto es num�rico o no</summary>
''' <param name="oTextBox">Caja de texto.</param>
''' <param name="sMensaje">Mensaje a mostrar en caso de que no sea correcto.</param>
''' <returns>Booleano indicando si el valor es correcto</returns>
''' <remarks>Llamada desde ComprobarDatos</remarks>

Private Function ComprobarCantidad(ByVal oTextBox As TextBox, ByVal sMensaje As String) As Boolean
    ComprobarCantidad = True
    If Len(Trim(oTextBox.Text)) > 0 Then
        If Not IsNumeric(oTextBox.Text) Then
            ComprobarCantidad = False
            oMensajes.NoValido sMensaje
        End If
    End If
End Function

''' <summary>Actualiza los datos</summary>
''' <remarks>Llamada desde cmdAceptar_Click</remarks>

Private Function ActualizarDatosParametros() As Boolean
    Dim i As Integer
    Dim sTexto As String
    
    On Error GoTo Error
      
    ActualizarDatosParametros = False
    
    g_iSubTipo = sdbcTipoSubasta.Columns(1).Value
    g_iSubModo = sdbcModoSubasta.Columns(1).Value
    'La duraci�n se guarda en minutos
    If txtDuracion.Text <> vbNullString Then
        Select Case sdbcDuracionSubasta.Columns(1).Value
            Case DuracionSubasta.Dias
                g_vSubDuracion = CInt(StrToDbl0(txtDuracion.Text)) * 24 * 60
            Case DuracionSubasta.Horas
                g_vSubDuracion = CInt(StrToDbl0(txtDuracion.Text)) * 60
            Case DuracionSubasta.Minutos
                g_vSubDuracion = CInt(StrToDbl0(txtDuracion.Text))
        End Select
    Else
        g_vSubDuracion = Null
    End If
    g_iSubastaEspera = CInt(StrToDbl0(txtMinutos.Text))
    g_bSubPublicar = ObtenerValorCheck(chkPublicar)
    g_bSubNotifEventos = ObtenerValorCheck(chkNotificar)
    g_bSubastaProve = ObtenerValorCheck(chkGanador)
    g_bSubVerDesdePrimPuja = ObtenerValorCheck(chkInfoPuja)
    g_bSubastaPrecioPuja = ObtenerValorCheck(chkPrecio)
    g_bSubastaPujas = ObtenerValorCheck(chkDetalle)
    g_bSubastaBajMinPuja = ObtenerValorCheck(chkBajadaMin)
    If rdbPorcentGan.Value Then
        g_iSubBajMinGanTipo = TipoBajadaSubasta.PorPorcentaje
    Else
        g_iSubBajMinGanTipo = TipoBajadaSubasta.PorImporte
    End If
    If txtBajadaProc.Text <> vbNullString Then
        g_vSubBajMinGanProcVal = StrToDbl0(txtBajadaProc.Text)
    Else
        g_vSubBajMinGanProcVal = Null
    End If
    If txtBajadaGrupo.Text <> vbNullString Then
        g_vSubBajMinGanGrupoVal = StrToDbl0(txtBajadaGrupo.Text)
    Else
        g_vSubBajMinGanGrupoVal = Null
    End If
    If txtBajadaItem.Text <> vbNullString Then
        g_vSubBajMinGanItemVal = StrToDbl0(txtBajadaItem.Text)
    Else
        g_vSubBajMinGanItemVal = Null
    End If
    
    g_bSubBajMinProve = ObtenerValorCheck(chkBajadaMinProv)
    If rdbPorcentProve.Value Then
        g_iSubBajMinProveTipo = TipoBajadaSubasta.PorPorcentaje
    Else
        g_iSubBajMinProveTipo = TipoBajadaSubasta.PorImporte
    End If
    If txtBajadaProcProv.Text <> vbNullString Then
        g_vSubBajMinProveProcVal = StrToDbl0(txtBajadaProcProv.Text)
    Else
        g_vSubBajMinProveProcVal = Null
    End If
    If txtBajadaGrupoProv.Text <> vbNullString Then
        g_vSubBajMinProveGrupoVal = StrToDbl0(txtBajadaGrupoProv.Text)
    Else
        g_vSubBajMinProveGrupoVal = Null
    End If
    If txtBajadaItemProv.Text <> vbNullString Then
        g_vSubBajMinProveItemVal = StrToDbl0(txtBajadaItemProv.Text)
    Else
        g_vSubBajMinProveItemVal = Null
    End If
    
    For i = 0 To txtTextoFin.Count - 1
        If g_dcSubTextosFin Is Nothing Then Set g_dcSubTextosFin = New Dictionary
                       
        If g_dcSubTextosFin.Exists(txtTextoFin(i).Tag) Then
            g_dcSubTextosFin(txtTextoFin(i).Tag) = txtTextoFin(i).Text
        Else
            If txtTextoFin(i).Text <> vbNullString Then
                g_dcSubTextosFin.Add txtTextoFin(i).Tag, txtTextoFin(i).Text
            End If
        End If
    Next
    
    ActualizarDatosParametros = True
    
    Exit Function
Error:
    Screen.MousePointer = vbNormal
    ActualizarDatosParametros = False
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
End Function

''' <summary>Se ejecuta al cargarse el formulario.</summary>

Private Sub Form_Load()
    Dim lMeTop As Long
    Dim lMeLeft As Long
    Dim i As Integer
    
    lMeTop = MDI.ScaleHeight / 2 - Me.Height / 2
    lMeLeft = MDI.ScaleWidth / 2 - Me.Width / 2
    If lMeTop < 0 Then lMeTop = Screen.Height / 2 - Me.Height / 2
    If lMeLeft < 0 Then lMeLeft = Screen.Width / 2 - Me.Width / 2
    Me.Top = lMeTop
    Me.Left = lMeLeft
    Me.Height = AlturaFormulario
    
    PonerFieldSeparator Me
    CargarRecursos
    CargarComboTipoSubasta
    CargarComboModoSubasta
    CargarComboDuracionSubasta
    ConfigurarTabIdiomas
    CargarDatosParametros
    
    picControles.Enabled = g_bModoEdicion
    Line3.Visible = g_bModoEdicion
    picBotones.Visible = g_bModoEdicion
    For i = 0 To picTab.Count - 1
        picTab(i).Enabled = g_bModoEdicion
    Next
    
    stabTextoFin.Tab = 0
    DoEvents
End Sub

Private Sub ConfigurarTabIdiomas()
    Dim oIdiomas As CIdiomas
    Dim oGestorParametros As CGestorParametros
    Dim i As Integer
    Dim sIdiomaUsu As String
    Dim k As Integer
    
    On Error GoTo Error
    
    'Obtener el idioma del usuario
    If Not IsEmpty(oUsuarioSummit.idioma) Then
        sIdiomaUsu = oUsuarioSummit.idioma
    Else
        sIdiomaUsu = gParametrosGenerales.gIdioma
    End If
    
    Set oGestorParametros = oFSGSRaiz.generar_CGestorParametros
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    'Creaci�n de los tabs
    'Graficamente hecho para 3 idiomas. As�,cuenta los idiomas que hay y muestra todos(incluso mas de 3 idiomas)sin superponerse'
    stabTextoFin.TabsPerRow = oIdiomas.Count
    stabTextoFin.Tabs = oIdiomas.Count
    For i = 1 To stabTextoFin.Tabs
        stabTextoFin.Tab = i - 1
        
        If i > 1 Then
            Load picTab(i - 1)
            Load txtTextoFin(i - 1)
            
            picTab(i - 1).Visible = False
            txtTextoFin(i - 1).Visible = True
        End If
        
        Set txtTextoFin(i - 1).Container = picTab(i - 1)
        Set picTab(i - 1).Container = stabTextoFin
    Next
    
    'Asignaci�n del caption a los tabs
    k = oIdiomas.Count
    For i = oIdiomas.Count To 1 Step -1
        If sIdiomaUsu <> oIdiomas.Item(i).Cod Then
            stabTextoFin.Tab = k - 1
            stabTextoFin.caption = oIdiomas.Item(i).Den
            
            txtTextoFin(k - 1).Tag = oIdiomas.Item(i).Cod
            
            k = k - 1
        Else
            'La primera pesta�a ser� la del idioma del usuario
            stabTextoFin.Tab = 0
            stabTextoFin.caption = oIdiomas.Item(i).Den
            
            txtTextoFin(0).Tag = oIdiomas.Item(i).Cod
        End If
    Next
    
fin:
    Set oGestorParametros = Nothing
    Set oIdiomas = Nothing
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume fin
End Sub
    


''' <summary>Carga los textos de los controles</summary>
''' <remarks>Llamada desde Form_Load</remarks>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    
    On Error GoTo Error

    'Cargar textos
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFSUBASTA, basPublic.gParametrosInstalacion.gIdioma)
    If Not Ador.EOF Then
        Me.caption = Ador(0).Value
        cmdAceptar.caption = TextoSiguiente(Ador)
        cmdCancelar.caption = TextoSiguiente(Ador)
        msTipoSubasta = TextoSiguiente(Ador)
        lblTipoSubasta.caption = msTipoSubasta & ":"
        chkPublicar.caption = TextoSiguiente(Ador)
        msModoSubasta = TextoSiguiente(Ador)
        lblModoSubasta.caption = msModoSubasta & ":"
        lblDuracion.caption = TextoSiguiente(Ador)
        msDuracionDias = TextoSiguiente(Ador)
        msDuracionHoras = TextoSiguiente(Ador)
        msDuracionMinutos = TextoSiguiente(Ador)
        msDescSubastaProceso = TextoSiguiente(Ador)
        msDescSubastaGrupo = TextoSiguiente(Ador)
        msDescSubastaItem = TextoSiguiente(Ador)
        chkNotificar.caption = TextoSiguiente(Ador)
        chkInfoPuja.caption = TextoSiguiente(Ador)
        chkDetalle.caption = TextoSiguiente(Ador)
        msMinutosEspera = TextoSiguiente(Ador)
        lblMinutos.caption = msMinutosEspera
        chkGanador.caption = TextoSiguiente(Ador)
        chkPrecio.caption = TextoSiguiente(Ador)
        chkBajadaMin.caption = TextoSiguiente(Ador)
        msBajadaProc = TextoSiguiente(Ador)
        msBajadaGrupo = TextoSiguiente(Ador)
        msBajadaItem = TextoSiguiente(Ador)
        chkBajadaMinProv.caption = TextoSiguiente(Ador)
        lblBajadaProc.caption = msBajadaProc
        lblBajadaGrupo.caption = msBajadaGrupo
        lblBajadaItem.caption = msBajadaItem
        lblBajadaProcProv.caption = msBajadaProc
        lblBajadaGrupoProv.caption = msBajadaGrupo
        lblBajadaItemProv.caption = msBajadaItem
        msSubastaInglesa = TextoSiguiente(Ador)
        msSubastaSobreCerrado = TextoSiguiente(Ador)
        msProceso = TextoSiguiente(Ador)
        msGrupo = TextoSiguiente(Ador)
        msItem = TextoSiguiente(Ador)
        msSelectArchivos = TextoSiguiente(Ador)
        msTodosArchivos = TextoSiguiente(Ador)
        msArchivo = TextoSiguiente(Ador)
        msTama�o = TextoSiguiente(Ador)
        msComentario = TextoSiguiente(Ador)
        msFecha = TextoSiguiente(Ador)
        msKb = TextoSiguiente(Ador)
        lblTextoFin.caption = TextoSiguiente(Ador)
        rdbPorcentGan.caption = TextoSiguiente(Ador)
        rdbImporteGan.caption = TextoSiguiente(Ador)
        rdbPorcentProve.caption = rdbPorcentGan.caption
        rdbImporteProve.caption = rdbImporteGan.caption
    End If

fin:
    Ador.Close
    Set Ador = Nothing
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume fin
End Sub

''' <summary>devuelve valor del texto siguiente registro del recordset pasado como par�metro</summary>
''' <param name="Ador">Recordset en el que buscar.</param>
''' <returns>El valor del texto en el campo 0</returns>
''' <remarks>Llamada desde Form_Load</remarks>

Private Function TextoSiguiente(ByVal Ador As ADODB.Recordset) As String
    Ador.MoveNext
    TextoSiguiente = Ador(0).Value
End Function

''' <summary>Carga los datos de los par�metros en el formulario</summary>
''' <remarks>Llamada desde Form_Load</remarks>

Private Sub CargarDatosParametros()
    Dim iSel As Integer
    Dim i As Integer
    
    On Error GoTo Error
    
    sdbcTipoSubasta.Columns(1).Value = g_iSubTipo
    Select Case g_iSubTipo
        Case TipoSubasta.Inglesa
            sdbcTipoSubasta.Value = msSubastaInglesa
        Case TipoSubasta.SobreCerrado
            sdbcTipoSubasta.Value = msSubastaSobreCerrado
    End Select
    sdbcModoSubasta.Columns(1).Value = g_iSubModo
    Select Case g_iSubModo
        Case TipoAmbitoProceso.AmbProceso
            sdbcModoSubasta.Value = msDescSubastaProceso
        Case TipoAmbitoProceso.AmbGrupo
            sdbcModoSubasta.Value = msDescSubastaGrupo
        Case TipoAmbitoProceso.AmbItem
            sdbcModoSubasta.Value = msDescSubastaItem
    End Select
    If Not (IsEmpty(g_vSubDuracion) Or IsNull(g_vSubDuracion)) Then
        If g_vSubDuracion < 60 Then
            'La duraci�n la ponemos en minutos
            CargarListaValoresDuracion DuracionSubasta.Minutos
            udDuracion.Value = g_vSubDuracion
            sdbcDuracionSubasta.Columns(1).Value = DuracionSubasta.Minutos
            sdbcDuracionSubasta.Value = msDuracionMinutos
        ElseIf g_vSubDuracion >= 60 And g_vSubDuracion < 1440 Then
            'La duraci�n la ponemos en horas
            CargarListaValoresDuracion DuracionSubasta.Horas
            udDuracion.Value = g_vSubDuracion / 60
            sdbcDuracionSubasta.Columns(1).Value = DuracionSubasta.Horas
            sdbcDuracionSubasta.Value = msDuracionHoras
        Else
            'La duraci�n se pone en d�as
            CargarListaValoresDuracion DuracionSubasta.Dias
            udDuracion.Value = g_vSubDuracion / 1440  '(60*24)
            sdbcDuracionSubasta.Columns(1).Value = DuracionSubasta.Dias
            sdbcDuracionSubasta.Value = msDuracionDias
        End If
    Else
        txtDuracion.Text = vbNullString
    End If
    txtMinutos.Text = CInt(g_iSubastaEspera)
    MostrarValorCheck chkPublicar, g_bSubPublicar
    MostrarValorCheck chkNotificar, g_bSubNotifEventos
    MostrarValorCheck chkGanador, g_bSubastaProve
    MostrarValorCheck chkInfoPuja, g_bSubVerDesdePrimPuja
    MostrarValorCheck chkPrecio, g_bSubastaPrecioPuja
    MostrarValorCheck chkDetalle, g_bSubastaPujas
    MostrarValorCheck chkBajadaMin, g_bSubastaBajMinPuja
    Select Case g_iSubBajMinGanTipo
        Case TipoBajadaSubasta.PorPorcentaje
            rdbPorcentGan.Value = True
        Case TipoBajadaSubasta.PorImporte
            rdbImporteGan.Value = True
    End Select
    If Not (IsEmpty(g_vSubBajMinGanProcVal) Or IsNull(g_vSubBajMinGanProcVal)) Then txtBajadaProc.Text = CDbl(g_vSubBajMinGanProcVal)
    If Not (IsEmpty(g_vSubBajMinGanGrupoVal) Or IsNull(g_vSubBajMinGanGrupoVal)) Then txtBajadaGrupo.Text = CDbl(g_vSubBajMinGanGrupoVal)
    If Not (IsEmpty(g_vSubBajMinGanItemVal) Or IsNull(g_vSubBajMinGanItemVal)) Then txtBajadaItem.Text = CDbl(g_vSubBajMinGanItemVal)
    MostrarValorCheck chkBajadaMinProv, g_bSubBajMinProve
    Select Case g_iSubBajMinProveTipo
        Case TipoBajadaSubasta.PorPorcentaje
            rdbPorcentProve.Value = True
        Case TipoBajadaSubasta.PorImporte
            rdbImporteProve.Value = True
    End Select
    If Not (IsEmpty(g_vSubBajMinProveProcVal) Or IsNull(g_vSubBajMinProveProcVal)) Then txtBajadaProcProv.Text = CDbl(g_vSubBajMinProveProcVal)
    If Not (IsEmpty(g_vSubBajMinProveGrupoVal) Or IsNull(g_vSubBajMinProveGrupoVal)) Then txtBajadaGrupoProv.Text = CDbl(g_vSubBajMinProveGrupoVal)
    If Not (IsEmpty(g_vSubBajMinProveItemVal) Or IsNull(g_vSubBajMinProveItemVal)) Then txtBajadaItemProv.Text = CDbl(g_vSubBajMinProveItemVal)
    
    'Cargar Textos fin
    If Not g_dcSubTextosFin Is Nothing Then
        For i = 0 To txtTextoFin.Count - 1
            If g_dcSubTextosFin.Exists(txtTextoFin(i).Tag) Then
                txtTextoFin(i).Text = g_dcSubTextosFin(txtTextoFin(i).Tag)
            End If
        Next
    End If
        
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
End Sub

''' <summary>Establece el valor de un checkbox</summary>
''' <param name="oCheck">Control Checkbox</param>
''' <param name="bValue">Valor a asignar</param>
''' <remarks>Llamada desde Form_Load</remarks>

Private Sub MostrarValorCheck(ByVal oCheck As CheckBox, ByVal bValue As Boolean)
    If bValue Then
        oCheck.Value = vbChecked
    Else
        oCheck.Value = vbUnchecked
    End If
End Sub

''' <summary>Devuelve un booleano en funci�n del valor de un checkbox</summary>
''' <param name="oCheck">Control Checkbox</param>
''' <returns>Booleano en funci�n de si el Checkbox est� chequeado o no</returns>
''' <remarks>Llamada desde Form_Load</remarks>

Private Function ObtenerValorCheck(ByVal oCheck As CheckBox) As Boolean
    If oCheck.Value = vbChecked Then
        ObtenerValorCheck = True
    Else
        ObtenerValorCheck = False
    End If
End Function

Private Sub Form_Resize()
    If Not Me.WindowState = vbMinimized Then
        Arrange Me.ScaleHeight
    End If
End Sub

''' <summary>Calcula la altura del formulario en funci�n de si son visibles o no las opciones avanzadas</summary>
''' <returns>La nueva altura del formulario calculada</returns>
''' <remarks>Llamada desde Form_Resize y cmdOpcionesAv_Click</remarks>

Private Function AlturaFormulario() As Long
    If g_bModoEdicion Then
        AlturaFormulario = picBotones.Top + picBotones.Height + 2 * m_lAlturaSep + m_lAlturaFormCaption
    Else
        AlturaFormulario = picTextoFin.Top + picTextoFin.Height + m_lAlturaSep + m_lAlturaFormCaption
    End If
End Function

''' <summary>Implementa el redimensionamiento del formulario</summary>
''' <remarks>Llamada desde cmdOpcionesAv_Click</remarks>

Private Sub Arrange(ByVal lFormHeight As Long)
    Dim i As Integer
    
    If Me.Width < m_lAnchuraFormMin Then Me.Width = m_lAnchuraFormMin
    If g_bModoEdicion Then
        If Me.Height < m_lAlturaFormMin Then Me.Height = m_lAlturaFormMin
    Else
        If Me.Height < (m_lAlturaFormMin - picBotones.Height) Then Me.Height = m_lAlturaFormMin - picBotones.Height
    End If

    picControles.Width = Me.Width
    picBotones.Width = Me.Width
    picTextoFin.Top = picControles.Height
    picTextoFin.Width = Me.Width
    If g_bModoEdicion Then
        picTextoFin.Height = Me.Height - m_lAlturaFormCaption - picControles.Height - picBotones.Height - m_lAlturaSep
    Else
        picTextoFin.Height = Me.Height - m_lAlturaFormCaption - picControles.Height - m_lAlturaSep
    End If
    picBotones.Top = picTextoFin.Top + picTextoFin.Height
    
    stabTextoFin.Height = picTextoFin.Height - stabTextoFin.Top - (2 * m_lAlturaSep)
    stabTextoFin.Width = Me.Width - (3 * m_lAnchuraSep)
    
    For i = 0 To txtTextoFin.Count - 1
        picTab(i).Width = stabTextoFin.Width - (2 * m_lAnchuraSep)
        picTab(i).Height = stabTextoFin.Height - (3 * m_lAlturaSep) - stabTextoFin.TabHeight
        txtTextoFin(i).Width = picTab(i).Width
        txtTextoFin(i).Height = picTab(i).Height
    Next
    
    Line1.X1 = 0
    Line1.X2 = Me.Width
    Line2.X1 = 0
    Line2.X2 = Me.Width
    Line3.X1 = 0
    Line3.X2 = Me.Width
    Line3.Y1 = stabTextoFin.Top + stabTextoFin.Height + m_lAlturaSep
    Line3.Y2 = Line3.Y1
    Line6.X1 = 0
    Line6.X2 = Me.Width
    Line7.X1 = 0
    Line7.X2 = Me.Width
    Line8.Y1 = Line6.Y1
    Line8.Y2 = Line7.Y1
    'Line8.X1 = txtBajadaProc.Left + txtBajadaProc.Width + m_lAnchuraSep
    'Line8.X2 = Line8.X1
       
    lblTipoSubasta.Left = m_lAnchuraSep
'    lblModoSubasta
'    lblDuracion
'    lblMinutos
    chkPublicar.Left = m_lAnchuraSep
    'chkNotificar
'    chkGanador
'    chkPrecio
'    chkInfoPuja
'    chkDetalle
    chkBajadaMin.Left = m_lAnchuraSep
    lblBajadaProc.Left = m_lAnchuraSep
'    txtBajadaProc
    lblBajadaGrupo.Left = m_lAnchuraSep
'    txtBajadaGrupo
    lblBajadaItem.Left = m_lAnchuraSep
'    txtBajadaItem
'    chkBajadaMinProv
'    lblBajadaProcProv
'    txtBajadaProcProv
'    lblBajadaGrupoProv
'    txtBajadaGrupoProv
'    lblBajadaItemProv
'    txtBajadaItemProv
    cmdAceptar.Top = m_lAlturaSep
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = Me.Width / 2 - (cmdAceptar.Width + cmdCancelar.Width + m_lAnchuraSep) / 2
    cmdCancelar.Left = cmdAceptar.Left + cmdAceptar.Width + m_lAnchuraSep
End Sub

''' <summary>Cierre de objetos globales</summary>

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    
    g_sOrigen = vbNullString
End Sub

Private Sub rdbImporteGan_Click()
    If rdbImporteGan.Value Then
        lblPorcent1.Visible = False
        lblPorcent2.Visible = False
        lblPorcent3.Visible = False
    End If
End Sub

Private Sub rdbImporteProve_Click()
    If rdbImporteProve.Value Then
        lblPorcent4.Visible = False
        lblPorcent5.Visible = False
        lblPorcent6.Visible = False
    End If
End Sub

Private Sub rdbPorcentGan_Click()
    If rdbPorcentGan.Value Then
        lblPorcent1.Visible = True
        lblPorcent2.Visible = True
        lblPorcent3.Visible = True
    End If
End Sub

Private Sub rdbPorcentProve_Click()
    If rdbPorcentProve.Value Then
        lblPorcent4.Visible = True
        lblPorcent5.Visible = True
        lblPorcent6.Visible = True
    End If
End Sub

''' <summary>Carga los items de duraci�n de la subasta en funci�n del valor del combo de duraci�n de subasta</summary>

Private Sub sdbcDuracionSubasta_CloseUp()
    If sdbcDuracionSubasta.Value = "" Then
        'Cambiar la lista de valores posibles de la duraci�n
        CargarListaValoresDuracion sdbcDuracionSubasta.Columns(1).Value
    End If
End Sub

''' <summary>Carga los valores de duraci�n de subasta</summary>
''' <remarks>Llamada desde sdbcDuracionSubasta_CloseUp y CargarDatosParametros</remarks>

Private Sub CargarListaValoresDuracion(ByVal iDuracion As Integer)
    Dim i As Integer
    Dim iMaxItem As Integer
    
    Select Case iDuracion
        Case DuracionSubasta.Dias
            iMaxItem = 365
        Case DuracionSubasta.Horas
            iMaxItem = 23
        Case DuracionSubasta.Minutos
            iMaxItem = 59
    End Select
    
    udDuracion.min = 1
    udDuracion.Max = iMaxItem
'    For i = 1 To iMaxItem
'        lstDuracion.AddItem i
'    Next
End Sub

Private Sub sdbcDuracionSubasta_InitColumnProps()
    With sdbcDuracionSubasta
        .DataFieldList = "Column 0, Column 1"
        .DataFieldToDisplay = "Column 0"
    End With
End Sub

Private Sub sdbcDuracionSubasta_PositionList(ByVal Text As String)
    PositionList sdbcDuracionSubasta, Text
End Sub

Private Sub sdbcModoSubasta_InitColumnProps()
    With sdbcModoSubasta
        .DataFieldList = "Column 0, Column 1"
        .DataFieldToDisplay = "Column 0"
    End With
End Sub

Private Sub sdbcModoSubasta_PositionList(ByVal Text As String)
    PositionList sdbcModoSubasta, Text
End Sub

Private Sub sdbcTipoSubasta_InitColumnProps()
    With sdbcTipoSubasta
        .DataFieldList = "Column 0, Column 1"
        .DataFieldToDisplay = "Column 0"
    End With
End Sub

Private Sub sdbcTipoSubasta_PositionList(ByVal Text As String)
    PositionList sdbcTipoSubasta, Text
End Sub

Private Sub PositionList(ByRef sdbcControl As SSDBCombo, ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    With sdbcControl
        .MoveFirst
    
        If Text <> "" Then
            For i = 0 To .Rows - 1
                bm = .GetBookmark(i)
                If UCase(Text) = UCase(Mid(.Columns(0).CellText(bm), 1, Len(Text))) Then
                    .Bookmark = bm
                    Exit For
                End If
            Next i
        End If
    End With
End Sub

''' <summary>Carga el combo de modos de subasta</summary>

Private Sub CargarComboModoSubasta()
    On Error GoTo Error
    
    With sdbcModoSubasta
        .AddItem msDescSubastaProceso & Chr(m_lSeparador) & CStr(TipoAmbitoProceso.AmbProceso)
        .AddItem msDescSubastaGrupo & Chr(m_lSeparador) & CStr(TipoAmbitoProceso.AmbGrupo)
        .AddItem msDescSubastaItem & Chr(m_lSeparador) & CStr(TipoAmbitoProceso.AmbItem)
        
        .ListAutoPosition = True
    End With
    
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
End Sub

''' <summary>Carga el combo de tipos de subasta</summary>

Private Sub CargarComboTipoSubasta()
    On Error GoTo Error
    
    With sdbcTipoSubasta
        .AddItem msSubastaInglesa & Chr(m_lSeparador) & CStr(TipoSubasta.Inglesa)
        .AddItem msSubastaSobreCerrado & Chr(m_lSeparador) & CStr(TipoSubasta.SobreCerrado)
         
        .ListAutoPosition = True
    End With
    
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
End Sub

''' <summary>Carga el combo de duraci�n de subasta</summary>

Private Sub CargarComboDuracionSubasta()
    On Error GoTo Error
    
    With sdbcDuracionSubasta
        .AddItem msDuracionDias & Chr(m_lSeparador) & CStr(DuracionSubasta.Dias)
        .AddItem msDuracionHoras & Chr(m_lSeparador) & CStr(DuracionSubasta.Horas)
        .AddItem msDuracionMinutos & Chr(m_lSeparador) & CStr(DuracionSubasta.Minutos)
        
        .ListAutoPosition = True
    End With
    
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
End Sub

Private Sub stabTextoFin_Click(PreviousTab As Integer)
    picTab(PreviousTab).Visible = False
    
    picTab(stabTextoFin.Tab).Visible = True
    picTab(stabTextoFin.Tab).Left = 120
    
    If txtTextoFin(stabTextoFin.Tab).Enabled And Me.Visible Then txtTextoFin(stabTextoFin.Tab).SetFocus
End Sub

Private Sub udDuracion_Change()
    txtDuracion.Text = udDuracion.Value
End Sub
