VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstDEST 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de destinos (Opciones)"
   ClientHeight    =   3135
   ClientLeft      =   2745
   ClientTop       =   2295
   ClientWidth     =   6135
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstDEST.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3135
   ScaleWidth      =   6135
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   6135
      TabIndex        =   5
      Top             =   2760
      Width           =   6135
      Begin VB.CommandButton cmdObtener 
         Caption         =   "D&Obtener"
         Height          =   375
         Left            =   4800
         TabIndex        =   3
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2745
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   4842
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DSelecci�n"
      TabPicture(0)   =   "frmLstDEST.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "DOpciones"
      TabPicture(1)   =   "frmLstDEST.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(1)=   "Frame4"
      Tab(1).ControlCount=   2
      Begin VB.Frame Frame3 
         Height          =   2220
         Left            =   120
         TabIndex        =   8
         Top             =   330
         Width           =   5895
         Begin VB.Frame frmUO 
            Caption         =   "Filtro por unidad organizativa"
            Height          =   735
            Left            =   180
            TabIndex        =   9
            Top             =   180
            Width           =   5295
            Begin VB.CommandButton cmdEstruc 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   4860
               Picture         =   "frmLstDEST.frx":0CEA
               Style           =   1  'Graphical
               TabIndex        =   12
               TabStop         =   0   'False
               Top             =   300
               Width           =   345
            End
            Begin VB.TextBox txtEstOrg 
               BackColor       =   &H80000018&
               Enabled         =   0   'False
               Height          =   285
               Left            =   120
               Locked          =   -1  'True
               TabIndex        =   11
               TabStop         =   0   'False
               Top             =   300
               Width           =   4335
            End
            Begin VB.CommandButton cmdBorrar 
               Height          =   315
               Left            =   4500
               Picture         =   "frmLstDEST.frx":0D56
               Style           =   1  'Graphical
               TabIndex        =   10
               Top             =   300
               Width           =   315
            End
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
            Height          =   285
            Left            =   285
            TabIndex        =   13
            Top             =   1170
            Width           =   1035
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "DCod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "DDenominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
            Height          =   285
            Left            =   1365
            TabIndex        =   14
            Top             =   1170
            Width           =   2835
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "DDenominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "DCod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5001
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
            Height          =   285
            Left            =   285
            TabIndex        =   15
            Top             =   1605
            Width           =   1035
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "DCod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "DDenominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
            Height          =   285
            Left            =   1365
            TabIndex        =   16
            Top             =   1605
            Width           =   2835
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "DDenominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "DCod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5001
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.Frame Frame4 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -74895
         TabIndex        =   7
         Top             =   1590
         Width           =   5895
         Begin VB.CheckBox chkIncluirDetalle 
            Caption         =   "DIncluir detalles"
            Height          =   195
            Left            =   750
            TabIndex        =   2
            Top             =   240
            Width           =   3465
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "DOrden"
         Height          =   1095
         Left            =   -74895
         TabIndex        =   6
         Top             =   405
         Width           =   5895
         Begin VB.OptionButton opOrdCod 
            Caption         =   "DC�digo"
            Height          =   195
            Left            =   780
            TabIndex        =   0
            Top             =   330
            Value           =   -1  'True
            Width           =   1515
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "DDenominaci�n"
            Height          =   195
            Left            =   780
            TabIndex        =   1
            Top             =   675
            Width           =   1515
         End
      End
   End
End
Attribute VB_Name = "frmLstDEST"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oPaises As CPaises
Private oPaisSeleccionado As CPais
Private PaiCargarComboDesde As Boolean
Private PaiRespetarCombo As Boolean
Private ProviRespetarCombo As Boolean
Private ProviCargarComboDesde As Boolean
'Variables de idioma
Private sIdiSelTodos As String
Private sIdiSelDest As String
Private sIdiSelDest1 As String
Private sIdiTitulo As String
Private txtCod As String
Private txtDe As String
Private txtDen As String
Private txtDIR As String
Private txtNulPai As String
Private txtNulPro As String
Private txtPag As String
Private txtPais As String
Private txtPOB As String
Private txtPos As String
Private txtProvi As String
Private txtSeleccion As String

Public bRUO As Boolean
Private m_iNivelUO As Integer
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String
Private SelecUON(0 To 2) As Variant

Private m_oIdiomas As CIdiomas


Private Sub cmdBorrar_Click()
    txtEstOrg.Text = ""
    
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
End Sub

Private Sub cmdEstruc_Click()
    frmSELUO.sOrigen = "frmLstDEST"
    frmSELUO.bRUO = bRUO
    frmSELUO.bMostrarBajas = True
    frmSELUO.Show 1
End Sub
Public Sub MostrarUOSeleccionada()

    If frmSELUO.sUON3 <> "" Then
        txtEstOrg.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sUON3 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = frmSELUO.sUON3

        Exit Sub
    End If
    
    If frmSELUO.sUON2 <> "" Then
        txtEstOrg.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = ""
        Exit Sub
    End If
    
    If frmSELUO.sUON1 <> "" Then
        txtEstOrg.Text = frmSELUO.sUON1 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = ""
        sUON3 = ""
        Exit Sub
    End If
    
    txtEstOrg.Text = ""
    txtEstOrg.Refresh
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""

End Sub

Private Sub sdbcPaiCod_Change()
    
    If Not PaiRespetarCombo Then
    
        PaiRespetarCombo = True
        sdbcPaiDen.Text = ""
        PaiRespetarCombo = False
        
        PaiCargarComboDesde = True
        Set oPaisSeleccionado = Nothing
      
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
End Sub

Private Sub sdbcPaiCod_CloseUp()
    
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    PaiRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    PaiRespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set oPaisSeleccionado = oPaises.Item(sdbcPaiCod.Columns(0).Text)
    
    PaiCargarComboDesde = False
        
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPaiCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    Set oPaises = Nothing
    Set oPaises = oFSGSRaiz.Generar_CPaises
   
    sdbcPaiCod.RemoveAll
    
    If PaiCargarComboDesde Then
        oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcPaiCod.Text), , , False
    Else
        oPaises.CargarTodosLosPaises , , , , , False
    End If
    
    Codigos = oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If PaiCargarComboDesde And Not oPaises.EOF Then
        sdbcPaiCod.AddItem "..."
    End If

    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPaiCod_InitColumnProps()

    sdbcPaiCod.DataFieldList = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcPaiCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiCod.Rows - 1
            bm = sdbcPaiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcPaiCod_Validate(Cancel As Boolean)

    Dim oPaises As CPaises
    Dim bExiste As Boolean
      
    Set oPaises = oFSGSRaiz.Generar_CPaises
  
    If sdbcPaiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    
    Screen.MousePointer = vbHourglass

    oPaises.CargarTodosLosPaises sdbcPaiCod.Text, , True, , False
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiCod.Text = ""
    Else
        PaiRespetarCombo = True
        sdbcPaiDen.Text = oPaises.Item(1).Den
        
        sdbcPaiCod.Columns(0).Value = sdbcPaiCod.Text
        sdbcPaiCod.Columns(1).Value = sdbcPaiDen.Text
        
        PaiRespetarCombo = False
        Set oPaisSeleccionado = oPaises.Item(1)
        PaiCargarComboDesde = False
    End If
    
    Set oPaises = Nothing
    
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcPaiDen_Change()
    
   
    If Not PaiRespetarCombo Then
    
        PaiRespetarCombo = True
        sdbcPaiCod.Text = ""
        PaiRespetarCombo = False
        PaiCargarComboDesde = True
        Set oPaisSeleccionado = Nothing
        
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""

End Sub
Private Sub sdbcPaiDen_CloseUp()
    
    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    PaiRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    PaiRespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set oPaisSeleccionado = oPaises.Item(sdbcPaiDen.Columns(1).Text)
    
    PaiCargarComboDesde = False
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcPaiDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
   
    Screen.MousePointer = vbHourglass
    Set oPaises = Nothing
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    sdbcPaiDen.RemoveAll
    
    If PaiCargarComboDesde Then
        oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPaiDen.Text), True, False
    Else
        oPaises.CargarTodosLosPaises , , , True, , False
    End If
    
    Codigos = oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If PaiCargarComboDesde And Not oPaises.EOF Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiDen.Refresh

    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcPaiDen_InitColumnProps()

    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcPaiDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiDen.Rows - 1
            bm = sdbcPaiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcPaiDen_Validate(Cancel As Boolean)

    Dim oPaises As CPaises
    Dim bExiste As Boolean
    
    Set oPaises = oFSGSRaiz.Generar_CPaises
  
    If sdbcPaiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    
    Screen.MousePointer = vbHourglass
    

    oPaises.CargarTodosLosPaises , sdbcPaiDen.Text, True, , , False
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiDen.Text = ""
    Else
        PaiRespetarCombo = True
        sdbcPaiCod.Text = oPaises.Item(1).Cod
        sdbcPaiDen.Columns(1).Value = sdbcPaiCod.Text
        sdbcPaiDen.Columns(0).Value = sdbcPaiDen.Text
        
        PaiRespetarCombo = False
        Set oPaisSeleccionado = oPaises.Item(1)
        PaiCargarComboDesde = False
    End If
    
    Set oPaises = Nothing

    Screen.MousePointer = vbNormal
    
End Sub


Private Sub cmdObtener_Click()

    ''' * Objetivo: Obtener un listado de destinos
    Dim Srpt As CRAXDRT.Report
    Dim oReport As CRAXDRT.Report
    Dim oCRDestinos As CRDestinos
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim sOcultarCampos As String
    Dim sSeleccion As String
    Dim i As Integer
    Dim IncluirTodosGenerales As String
    Dim IncluirUON1 As String
    Dim IncluirUON2 As String
    Dim IncluirUON3 As String
    Dim oIdi As CIdioma
    
    Set oCRDestinos = GenerarCRDestinos
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptDestinos.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    sSeleccion = ""
    If txtEstOrg.Text = "" Then
        sSeleccion = sIdiSelTodos & " " & txtEstOrg
    End If
    
    IncluirUON1 = "N"
    IncluirUON2 = "N"
    IncluirUON3 = "N"
    IncluirTodosGenerales = "N"
    If bRUO Then
        Select Case gParametrosGenerales.giNEO
    
            Case 1
                    If sUON1 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    End If
            Case 2
                    If m_iNivelUO > 0 And sUON1 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    ElseIf m_iNivelUO > 1 And sUON2 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    End If
            Case 3
                    If m_iNivelUO > 0 And sUON1 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    ElseIf m_iNivelUO > 1 And sUON2 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    ElseIf m_iNivelUO > 2 And sUON3 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    End If
        End Select
    End If
    
    If sUON1 <> "" Then
        If sUON2 <> "" Then
            If sUON3 <> "" Then
                sSeleccion = sUON1 & "- " & sUON2 & " - " & sUON3
                IncluirUON3 = "S"
            Else
                sSeleccion = sUON1 & "- " & sUON2
                IncluirUON2 = "S"
                IncluirUON3 = "S"
            End If
        Else
            sSeleccion = sUON1
            IncluirUON1 = "S"
            IncluirUON2 = "S"
            IncluirUON3 = "S"
        End If
    Else
        sSeleccion = sIdiSelTodos
        IncluirUON1 = "S"
        IncluirUON2 = "S"
        IncluirUON3 = "S"
        IncluirTodosGenerales = "S"
    End If
    
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
        
    If chkIncluirDetalle.Value = vbUnchecked Then
        sOcultarCampos = "S"
    Else
        sOcultarCampos = "N"
    End If
   
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "UON0NOM")).Text = """" & gParametrosGenerales.gsDEN_UON0 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "UON1NOM")).Text = """" & gParametrosGenerales.gsDEN_UON1 & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "UON2NOM")).Text = """" & gParametrosGenerales.gsDEN_UON2 & ":" & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "UON3NOM")).Text = """" & gParametrosGenerales.gsDEN_UON3 & ":" & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & sIdiTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & txtSeleccion & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "IncluirTodosGenerales")).Text = """" & IncluirTodosGenerales & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "IncluirUON1")).Text = """" & IncluirUON1 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "IncluirUON2")).Text = """" & IncluirUON2 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "IncluirUON3")).Text = """" & IncluirUON3 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "IDI")).Text = """" & gParametrosInstalacion.gIdioma & """"

    For i = 0 To 3
        Set Srpt = Nothing
        Set Srpt = oReport.OpenSubreport("rptDest_UON" & i)
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtNulPai")).Text = """" & txtNulPai & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtNulPro")).Text = """" & txtNulPro & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtCod")).Text = """" & txtCod & """"
        For Each oIdi In m_oIdiomas
            Srpt.FormulaFields(crs_FormulaIndex(Srpt, "TIT" & oIdi.Cod)).Text = """" & txtDen & " (" & oIdi.Den & ")" & """"
        Next
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtDir")).Text = """" & txtDIR & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtPais")).Text = """" & txtPais & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtPos")).Text = """" & txtPos & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtProvi")).Text = """" & txtProvi & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "txtPob")).Text = """" & txtPOB & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "OcultarCampos")).Text = """" & sOcultarCampos & """"
        Srpt.FormulaFields(crs_FormulaIndex(Srpt, "IDI")).Text = """" & gParametrosInstalacion.gIdioma & """"
       
    Next i
   
    oCRDestinos.Listado oReport, opOrdDen.Value, sdbcPaiCod.Text, sdbcProviCod.Text, sUON1, sUON2, sUON3, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, sdbcPaiCod.Text, sdbcProviCod.Text 'pasarle las uo
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sIdiTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    pv.Show
    Unload Me

    Screen.MousePointer = vbNormal
    
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTDEST, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        SSTab1.TabCaption(0) = Ador(0).Value      '1
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value     '2
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value     '3
        Ador.MoveNext
        chkIncluirDetalle.caption = Ador(0).Value
        Ador.MoveNext
        Frame1.caption = Ador(0).Value '5
        Ador.MoveNext
        frmLstDEST.caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value '7 C�digo
        sdbcPaiCod.Columns(0).caption = Ador(0).Value
        sdbcProviCod.Columns(0).caption = Ador(0).Value
        sdbcPaiDen.Columns(1).caption = Ador(0).Value
        sdbcProviDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value '8 Denominaci�n
        sdbcPaiDen.Columns(0).caption = Ador(0).Value
        sdbcProviDen.Columns(0).caption = Ador(0).Value
        sdbcPaiCod.Columns(1).caption = Ador(0).Value
        sdbcProviCod.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        'opPais.Caption = Ador(0).Value
        Ador.MoveNext
        'opProvi.Caption = Ador(0).Value '10
        Ador.MoveNext
       ' opTodos.Caption = Ador(0).Value
        'Idiomas del RPT
        Ador.MoveNext
        sIdiSelTodos = Ador(0).Value '200
        Ador.MoveNext
        sIdiSelDest = Ador(0).Value 'destinos del pais
        Ador.MoveNext
        sIdiTitulo = Ador(0).Value
        Ador.MoveNext
        txtCod = Ador(0).Value
        Ador.MoveNext
        txtDe = Ador(0).Value
        Ador.MoveNext
        txtDen = Ador(0).Value '205
        Ador.MoveNext
        txtDIR = Ador(0).Value
        Ador.MoveNext
        txtNulPai = Ador(0).Value 'destinos sin especificar pais
        Ador.MoveNext
        txtNulPro = Ador(0).Value 'destinos sin especificar provincia
        Ador.MoveNext
        txtPag = Ador(0).Value
        Ador.MoveNext
        txtPais = Ador(0).Value '210
        Ador.MoveNext
        txtPOB = Ador(0).Value
        Ador.MoveNext
        txtPos = Ador(0).Value
        Ador.MoveNext
        txtProvi = Ador(0).Value
        Ador.MoveNext
        txtSeleccion = Ador(0).Value '214
        Ador.MoveNext
        sIdiSelDest1 = Ador(0).Value
        Ador.MoveNext
        frmUO.caption = Ador(0).Value

        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


Private Sub Form_Load()

    Me.Width = 6255
    Me.Height = 3540
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    ConfigurarSeguridad
    
    cmdObtener.Enabled = True
    
    If gParametrosInstalacion.gsPais <> "" Then
        sdbcPaiCod.Text = gParametrosInstalacion.gsPais
    Else
        If gParametrosGenerales.gsPAIDEF <> "" Then
            sdbcPaiCod.Text = gParametrosGenerales.gsPAIDEF
            gParametrosInstalacion.gsPais = gParametrosGenerales.gsPAIDEF
            g_GuardarParametrosIns = True
        End If
    End If
    
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    
    sdbcPaiCod_Validate False
    
End Sub


Private Sub sdbcProviCod_Change()
    
'    opProvi.Value = True
    
    If Not ProviRespetarCombo Then
    
        ProviRespetarCombo = True
        sdbcProviDen.Text = ""
        ProviRespetarCombo = False
        
        ProviCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcProviCod_CloseUp()
    
    If sdbcProviCod.Value = "..." Then
        sdbcProviCod.Text = ""
        Exit Sub
    End If
    
    ProviRespetarCombo = True
    sdbcProviDen.Text = sdbcProviCod.Columns(1).Text
    sdbcProviCod.Text = sdbcProviCod.Columns(0).Text
    ProviRespetarCombo = False
    
    ProviCargarComboDesde = False
        
End Sub

Private Sub sdbcProviCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    sdbcProviCod.RemoveAll
        
    If Not oPaisSeleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        
        If ProviCargarComboDesde Then
            oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProviCod.Text), , , False
        Else
            oPaisSeleccionado.CargarTodasLasProvincias , , , , False
        End If
        
        Codigos = oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
        
        If ProviCargarComboDesde And Not oPaisSeleccionado.Provincias.EOF Then
            sdbcProviCod.AddItem "..."
        End If

    End If
    
    sdbcProviCod.SelStart = 0
    sdbcProviCod.SelLength = Len(sdbcProviCod.Text)
    sdbcProviCod.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcProviCod_InitColumnProps()

    sdbcProviCod.DataFieldList = "Column 0"
    sdbcProviCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProviCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProviCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProviCod.Rows - 1
            bm = sdbcProviCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProviCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProviCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcProviCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProviCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    Screen.MousePointer = vbHourglass
    
    If Not oPaisSeleccionado Is Nothing Then
        
        oPaisSeleccionado.CargarTodasLasProvincias sdbcProviCod.Text, , True, , False
    
        bExiste = Not (oPaisSeleccionado.Provincias.Count = 0)
        
    Else
    
        bExiste = False
        
    End If
    
        
    If Not bExiste Then
        sdbcProviCod.Text = ""
    Else
        ProviRespetarCombo = True
        sdbcProviDen.Text = oPaisSeleccionado.Provincias.Item(1).Den
        sdbcProviCod.Columns(0).Value = sdbcProviCod.Text
        sdbcProviCod.Columns(1).Value = sdbcProviDen.Text
        
        ProviRespetarCombo = False
        ProviCargarComboDesde = False
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcProviDen_Change()
    
    If Not ProviRespetarCombo Then
    
        ProviRespetarCombo = True
        sdbcProviCod.Text = ""
        ProviRespetarCombo = False
        ProviCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcProviDen_CloseUp()
    
    If sdbcProviDen.Value = "..." Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If
    
    ProviRespetarCombo = True
    sdbcProviCod.Text = sdbcProviDen.Columns(1).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(0).Text
    ProviRespetarCombo = False
    
    ProviCargarComboDesde = False
        
End Sub
Private Sub sdbcProviDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcProviDen.RemoveAll
    
    If Not oPaisSeleccionado Is Nothing Then
        
        Screen.MousePointer = vbHourglass
        
        If ProviCargarComboDesde Then
            oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProviDen.Text), True, False
        Else
            oPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If
        
        Codigos = oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
        
        If ProviCargarComboDesde And Not oPaisSeleccionado.Provincias.EOF Then
            sdbcProviDen.AddItem "..."
        End If

    End If
    
    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
    sdbcProviCod.Refresh

    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcProviDen_InitColumnProps()

    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcProviDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcProviCod.DroppedDown = False
        sdbcProviCod.Text = ""
        sdbcProviCod.RemoveAll
        sdbcProviDen.DroppedDown = False
        sdbcProviDen.Text = ""
        sdbcProviDen.RemoveAll
    End If
End Sub

Private Sub sdbcProviDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProviDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    If Not oPaisSeleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        
        oPaisSeleccionado.CargarTodasLasProvincias , sdbcProviDen.Text, True, , False
        bExiste = Not (oPaisSeleccionado.Provincias.Count = 0)
        
    Else
        bExiste = False
    End If
    
        
    If Not bExiste Then
        sdbcProviDen.Text = ""
    Else
        ProviRespetarCombo = True
        sdbcProviCod.Text = oPaisSeleccionado.Provincias.Item(1).Cod
        sdbcProviDen.Text = oPaisSeleccionado.Provincias.Item(1).Den
        
        sdbcProviDen.Columns(1).Value = sdbcProviCod.Text
        sdbcProviDen.Columns(0).Value = sdbcProviDen.Text
        
        ProviRespetarCombo = False
        ProviCargarComboDesde = False
    End If
    Screen.MousePointer = vbNormal

End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    bRUO = False
        
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestUO)) Is Nothing) Then
        bRUO = True
    End If

    If bRUO Then
        m_iNivelUO = 0
        If CStr(basOptimizacion.gUON3Usuario) <> "" Then
            m_iNivelUO = 3
        ElseIf CStr(basOptimizacion.gUON2Usuario) <> "" Then
            m_iNivelUO = 2
        ElseIf CStr(basOptimizacion.gUON1Usuario) <> "" Then
            m_iNivelUO = 1
        End If
    End If
End Sub


