VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmItemsWizard4 
   Caption         =   "Asignaci�n de presupuestos"
   ClientHeight    =   6780
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9465
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmItemsWizard4.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6780
   ScaleWidth      =   9465
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2350
      ScaleHeight     =   495
      ScaleWidth      =   4695
      TabIndex        =   7
      Top             =   6300
      Width           =   4700
      Begin VB.CommandButton cmdVolver 
         Caption         =   "<<             "
         Height          =   315
         Left            =   1200
         TabIndex        =   11
         Top             =   100
         Width           =   1095
      End
      Begin VB.CommandButton cmdFinalizar 
         Caption         =   "&Finalizar"
         Height          =   315
         Left            =   3600
         TabIndex        =   10
         Top             =   100
         Width           =   1095
      End
      Begin VB.CommandButton cmdContinuar 
         Caption         =   "           >>"
         Default         =   -1  'True
         Height          =   315
         Left            =   2400
         TabIndex        =   9
         Top             =   100
         Width           =   1095
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   0
         TabIndex        =   8
         Top             =   100
         Width           =   1095
      End
   End
   Begin VB.PictureBox picSepar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   500
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   9345
      TabIndex        =   0
      Top             =   350
      Width           =   9400
      Begin VB.TextBox txtPorcenAsig 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1400
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   60
         Width           =   700
      End
      Begin VB.TextBox txtPorcenPend 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4700
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   60
         Width           =   700
      End
      Begin VB.Label lblAsignado 
         Caption         =   "Asignado:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Left            =   100
         TabIndex        =   6
         Top             =   90
         Width           =   1200
      End
      Begin VB.Label lblPorcen 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Index           =   1
         Left            =   2200
         TabIndex        =   5
         Top             =   100
         Width           =   250
      End
      Begin VB.Label lblPrendiente 
         Caption         =   "Pendiente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Left            =   3500
         TabIndex        =   4
         Top             =   90
         Width           =   1200
      End
      Begin VB.Label lblPorcen 
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Index           =   2
         Left            =   5500
         TabIndex        =   3
         Top             =   100
         Width           =   240
      End
   End
   Begin TabDlg.SSTab SSTabPresupuestos 
      Height          =   5350
      Left            =   0
      TabIndex        =   12
      Top             =   900
      Width           =   9400
      _ExtentX        =   16589
      _ExtentY        =   9446
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Seleccione la unidad organizativa"
      TabPicture(0)   =   "frmItemsWizard4.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblEstrorg"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "ImageList1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "ImageList2"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "tvwestrorg"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Seleccione el presupuesto"
      TabPicture(1)   =   "frmItemsWizard4.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdOtras"
      Tab(1).Control(1)=   "picPresup"
      Tab(1).Control(2)=   "tvwestrPres"
      Tab(1).Control(3)=   "sdbcAnyo"
      Tab(1).Control(4)=   "lblOtras"
      Tab(1).Control(5)=   "lblUO"
      Tab(1).ControlCount=   6
      Begin VB.CommandButton cmdOtras 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   -66135
         TabIndex        =   29
         Top             =   405
         Width           =   375
      End
      Begin VB.PictureBox picPresup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Left            =   -74850
         ScaleHeight     =   825
         ScaleWidth      =   9045
         TabIndex        =   13
         Top             =   4300
         Width           =   9100
         Begin VB.PictureBox picPorcen 
            BorderStyle     =   0  'None
            Height          =   400
            Left            =   750
            ScaleHeight     =   405
            ScaleWidth      =   7995
            TabIndex        =   24
            Top             =   360
            Width           =   8000
            Begin VB.TextBox txtPorcenAsignar 
               Alignment       =   1  'Right Justify
               BackColor       =   &H80000018&
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1260
               TabIndex        =   26
               TabStop         =   0   'False
               Top             =   50
               Width           =   975
            End
            Begin MSComctlLib.Slider Slider1 
               Height          =   375
               Left            =   4320
               TabIndex        =   25
               Top             =   20
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   661
               _Version        =   393216
               Max             =   100
            End
            Begin VB.Label lblAsignar 
               Caption         =   "Asignar"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Left            =   0
               TabIndex        =   28
               Top             =   110
               Width           =   675
            End
            Begin VB.Label lblPorcen 
               Caption         =   "%"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00400000&
               Height          =   225
               Index           =   3
               Left            =   2350
               TabIndex        =   27
               Top             =   110
               Width           =   240
            End
         End
         Begin VB.TextBox txtPresupuesto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2010
            Locked          =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   0
            Width           =   2655
         End
         Begin VB.TextBox txtObj 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6420
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   0
            Width           =   975
         End
         Begin VB.Line Line1 
            X1              =   0
            X2              =   8880
            Y1              =   330
            Y2              =   330
         End
         Begin VB.Label lblPresupuesto 
            Caption         =   "Presupuesto:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00400000&
            Height          =   225
            Left            =   750
            TabIndex        =   17
            Top             =   30
            Width           =   1140
         End
         Begin VB.Label lblObjetivo 
            Caption         =   "Objetivo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00400000&
            Height          =   225
            Left            =   5310
            TabIndex        =   16
            Top             =   60
            Width           =   1035
         End
      End
      Begin MSComctlLib.TreeView tvwestrorg 
         Height          =   4500
         Left            =   150
         TabIndex        =   18
         Top             =   735
         Width           =   9100
         _ExtentX        =   16060
         _ExtentY        =   7938
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageList ImageList2 
         Left            =   0
         Top             =   500
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   6
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":0CEA
               Key             =   "Raiz"
               Object.Tag             =   "Raiz"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":17B4
               Key             =   "PRES1"
               Object.Tag             =   "PRES1"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":1C06
               Key             =   "PRES2"
               Object.Tag             =   "PRES2"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":2058
               Key             =   "PRES3"
               Object.Tag             =   "PRES3"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":24AA
               Key             =   "PRES4"
               Object.Tag             =   "PRES4"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":28FC
               Key             =   "PRESA"
               Object.Tag             =   "PRESA"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView tvwestrPres 
         Height          =   3500
         Left            =   -74850
         TabIndex        =   19
         Top             =   735
         Width           =   9100
         _ExtentX        =   16060
         _ExtentY        =   6165
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList2"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   -67905
         TabIndex        =   20
         Top             =   390
         Width           =   840
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1482
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "2002"
         BackColor       =   16777215
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   500
         Top             =   500
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   12
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":2C4E
               Key             =   "UON0"
               Object.Tag             =   "UON0"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":2CEC
               Key             =   "UON1"
               Object.Tag             =   "UON1"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":2D9C
               Key             =   "UON2"
               Object.Tag             =   "UON2"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":2E4C
               Key             =   "UON3"
               Object.Tag             =   "UON3"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":2EDE
               Key             =   "UON1A"
               Object.Tag             =   "UON1A"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":2F9E
               Key             =   "UON2A"
               Object.Tag             =   "UON2A"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":3060
               Key             =   "UON3A"
               Object.Tag             =   "UON3A"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":3120
               Key             =   "UON2D"
               Object.Tag             =   "UON2D"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":31E2
               Key             =   "UON1D"
               Object.Tag             =   "UON1D"
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":32A4
               Key             =   "UON3D"
               Object.Tag             =   "UON3D"
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":3366
               Key             =   "UON0A"
               Object.Tag             =   "UON0A"
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemsWizard4.frx":3409
               Key             =   "UON0D"
               Object.Tag             =   "UON0D"
            EndProperty
         EndProperty
      End
      Begin VB.Label lblOtras 
         Caption         =   "Others -->"
         Height          =   195
         Left            =   -66990
         TabIndex        =   30
         Top             =   450
         Width           =   825
      End
      Begin VB.Label lblEstrorg 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Unidad organizativa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   150
         TabIndex        =   22
         Top             =   390
         Width           =   9100
      End
      Begin VB.Label lblUO 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Unidad organizativa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -74850
         TabIndex        =   21
         Top             =   390
         Width           =   7800
      End
   End
   Begin VB.Label lblSel 
      Caption         =   "Seleccione la asignaci�n de presupuestos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   60
      TabIndex        =   23
      Top             =   50
      Width           =   6600
   End
End
Attribute VB_Name = "frmItemsWizard4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Para el popup de Otras
Private WithEvents cP As cPopupMenu
Attribute cP.VB_VarHelpID = -1
Private m_arTag As Variant

'Restricci�n UO
Private m_bRuo As Boolean
Private m_bRuoRama As Boolean 'De momento no se usa, siempre es false
Private m_iUOBase As Long
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String

'Abierto original, no se modifica
Private m_dblPorcenAsig As Double
'Si al entrar el pendiente es distino de 0 y de 100
'los porcentajes se calculan en relaci�n al nuevo abierto
Private m_bPorcenAsigAbierto As Boolean 'Si True hay que recalcular

' Unidades organizativas
Private m_oUnidadesOrgN1 As CUnidadesOrgNivel1
Private m_oUnidadesOrgN2 As CUnidadesOrgNivel2
Private m_oUnidadesOrgN3 As CUnidadesOrgNivel3

'Variables para la estructura de presupuestos
Private m_oPresupuestos1 As CPresProyectosNivel1
Private m_oPresupuestos2 As CPresContablesNivel1
Private m_oPresupuestos3 As CPresConceptos3Nivel1
Private m_oPresupuestos4 As CPresConceptos4Nivel1

'Para el boton otras
Private m_oPresupuestos1Otros As CPresProyectosNivel4
Private m_oPresupuestos2Otros As CPresContablesNivel4
Private m_oPresupuestos3Otros As CPresConceptos3Nivel4
Private m_oPresupuestos4Otros As CPresConceptos4Nivel4

'Tipo de presupuesto
Public g_iTipoPres As Integer


Private m_bRespetarPorcen As Boolean
Private m_stexto As String

Private m_bCancelar As Boolean
Public g_bSinPermisos As Boolean

Public msPresupPlural As String
Public m_bRestringirMultiplePres As Boolean

Private sCodArts As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub Arrange()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    SSTabPresupuestos.Height = Me.Height - 1835
    SSTabPresupuestos.Width = Me.Width - 125
    tvwestrorg.Height = SSTabPresupuestos.Height - 870
    tvwestrorg.Width = SSTabPresupuestos.Width - 300
    tvwestrPres.Height = tvwestrorg.Height - 905
    tvwestrPres.Width = tvwestrorg.Width
    lblEstrorg.Width = tvwestrorg.Width
    'lblUO.Width = tvwestrPres.Width
    picSepar.Width = SSTabPresupuestos.Width
    picPresup.Top = tvwestrPres.Top + tvwestrPres.Height + 50
    picPresup.Width = tvwestrPres.Width
    picNavigate.Top = SSTabPresupuestos.Top + SSTabPresupuestos.Height + 50
    picNavigate.Left = Me.Width / 2 - picNavigate.Width / 2
    sdbcAnyo.Top = lblUO.Top
    'sdbcAnyo.Left = lblUO.Left + lblUO.Width - sdbcAnyo.Width
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub

Private Sub CargarAnyos()
Dim iAnyoActual As Integer
Dim iInd As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "CargarAnyos", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub


Private Sub OcultarBotones()

'Si alguno de los presupuestos siguientes es obligatorio y est� a nivel de �tem no se puede finalizar
'Si ninguno de los presupuestos siguiente est� a nivel de �tem no se continua

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_iTipoPres
        Case 1
            If Not gParametrosGenerales.gbUsarPres2 And Not gParametrosGenerales.gbUsarPres3 And Not gParametrosGenerales.gbUsarPres4 Then
                cmdContinuar.Visible = False
                cmdFinalizar.Left = cmdContinuar.Left
                cmdFinalizar.Default = True
            Else
                If gParametrosGenerales.gbUsarPres2 Then
                    If (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2) Then
                        If gParametrosGenerales.gbUsarPres3 Then
                            If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo1) Then
                                If gParametrosGenerales.gbUsarPres4 Then
                                    If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo2) Then
                                        cmdContinuar.Visible = False
                                        cmdFinalizar.Left = cmdContinuar.Left
                                        cmdFinalizar.Default = True
                                     Else
                                        If gParametrosGenerales.gbOBLPres4 Then
                                            cmdFinalizar.Visible = False
                                        End If
                                    End If
                                Else
                                    cmdContinuar.Visible = False
                                    cmdFinalizar.Left = cmdContinuar.Left
                                    cmdFinalizar.Default = True
                                End If
                            Else
                                If gParametrosGenerales.gbOBLPres3 Then
                                    cmdFinalizar.Visible = False
                                End If
                            End If
                        Else
                            If gParametrosGenerales.gbUsarPres4 Then
                                If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo2) Then
                                    cmdContinuar.Visible = False
                                    cmdFinalizar.Left = cmdContinuar.Left
                                    cmdFinalizar.Default = True
                                 Else
                                    If gParametrosGenerales.gbOBLPres4 Then
                                        cmdFinalizar.Visible = False
                                    End If
                                End If
                            Else
                                cmdContinuar.Visible = False
                                cmdFinalizar.Left = cmdContinuar.Left
                                cmdFinalizar.Default = True
                            End If
                        End If
                    Else
                        If gParametrosGenerales.gbOBLPres3 Then
                            cmdFinalizar.Visible = False
                        End If
                    End If
                Else
                    If gParametrosGenerales.gbUsarPres3 Then
                        If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo1) Then
                            If gParametrosGenerales.gbUsarPres4 Then
                                If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo2) Then
                                    cmdContinuar.Visible = False
                                    cmdFinalizar.Left = cmdContinuar.Left
                                    cmdFinalizar.Default = True
                                 Else
                                    If gParametrosGenerales.gbOBLPres4 Then
                                        cmdFinalizar.Visible = False
                                    End If
                                End If
                            Else
                                cmdContinuar.Visible = False
                                cmdFinalizar.Left = cmdContinuar.Left
                                cmdFinalizar.Default = True
                            End If
                        Else
                            If gParametrosGenerales.gbOBLPres3 Then
                                cmdFinalizar.Visible = False
                            End If
                        End If
                    Else
                        If gParametrosGenerales.gbUsarPres4 Then
                            If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo2) Then
                                cmdContinuar.Visible = False
                                cmdFinalizar.Left = cmdContinuar.Left
                                cmdFinalizar.Default = True
                             Else
                                If gParametrosGenerales.gbOBLPres4 Then
                                    cmdFinalizar.Visible = False
                                End If
                            End If
                        Else
                            cmdContinuar.Visible = False
                            cmdFinalizar.Left = cmdContinuar.Left
                            cmdFinalizar.Default = True
                        End If
                    End If
                End If
            End If
            
        Case 2
            If Not gParametrosGenerales.gbUsarPres3 And Not gParametrosGenerales.gbUsarPres4 Then
                cmdContinuar.Visible = False
                cmdFinalizar.Left = cmdContinuar.Left
                cmdFinalizar.Default = True
            Else
                If gParametrosGenerales.gbUsarPres3 Then
                    If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo1) Then
                        If gParametrosGenerales.gbUsarPres4 Then
                            If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo2) Then
                                cmdContinuar.Visible = False
                                cmdFinalizar.Left = cmdContinuar.Left
                                cmdFinalizar.Default = True
                             Else
                                If gParametrosGenerales.gbOBLPres4 Then
                                    cmdFinalizar.Visible = False
                                End If
                            End If
                        Else
                            cmdContinuar.Visible = False
                            cmdFinalizar.Left = cmdContinuar.Left
                            cmdFinalizar.Default = True
                        End If
                    Else
                        If gParametrosGenerales.gbOBLPres3 Then
                            cmdFinalizar.Visible = False
                        End If
                    End If
                Else
                    If gParametrosGenerales.gbUsarPres4 Then
                        If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo2) Then
                            cmdContinuar.Visible = False
                            cmdFinalizar.Left = cmdContinuar.Left
                            cmdFinalizar.Default = True
                         Else
                            If gParametrosGenerales.gbOBLPres4 Then
                                cmdFinalizar.Visible = False
                            End If
                        End If
                    Else
                        cmdContinuar.Visible = False
                        cmdFinalizar.Left = cmdContinuar.Left
                        cmdFinalizar.Default = True
                    End If
                End If
            End If
                            
        Case 3
            If gParametrosGenerales.gbUsarPres4 Then
                If (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = NoDefinido Or frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Or frmPROCE.g_oGrupoSeleccionado.DefPresTipo2) Then
                    cmdContinuar.Visible = False
                    cmdFinalizar.Left = cmdContinuar.Left
                    cmdFinalizar.Default = True
                 Else
                    If gParametrosGenerales.gbOBLPres4 Then
                        cmdFinalizar.Visible = False
                    End If
                End If
            Else
                cmdContinuar.Visible = False
                cmdFinalizar.Left = cmdContinuar.Left
                cmdFinalizar.Default = True
            End If
        
        Case 4
            cmdContinuar.Visible = False
            cmdFinalizar.Left = cmdContinuar.Left
            cmdFinalizar.Default = True
        
    End Select

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "OcultarBotones", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCancelar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdContinuar_Click()
            
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCancelar = False
    
    Select Case g_iTipoPres
        Case 1
            If gParametrosGenerales.gbOBLPP Then
                If txtPorcenAsig.Text <> 100 Then
                    oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                    Exit Sub
                End If
            Else
                If txtPorcenAsig.Text <> 0 Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                End If
            End If
            Unload Me
            
            '**PRESANU2
            If (gParametrosGenerales.gbUsarPres2 And (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2))) Then
                frmItemsWizard4.g_iTipoPres = 2
                frmItemsWizard4.Show 1
            Else
                'Si est� en proce o grupo reasignar presupuestos
                If gParametrosGenerales.gbUsarPres2 And Not gParametrosGenerales.gbPresupuestosAut Then
                    If Not frmItemsWizard.g_bPresAnu2OK Then
                        If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso Then 'PRESANU2 en PROCE
                            If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                                frmItemsWizard.ReasignarPresup 2, True
                            End If
                        Else
                            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo Then 'PRESANU2 en GRUPO
                                If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 Then
                                    frmItemsWizard.ReasignarPresup 2, False
                                End If
                            End If
                        End If
                    End If
                End If
                '***PRES1
                If (gParametrosGenerales.gbUsarPres3 And (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo1))) Then
                    frmItemsWizard4.g_iTipoPres = 3
                    frmItemsWizard4.Show 1
                Else
                    If gParametrosGenerales.gbUsarPres3 And Not gParametrosGenerales.gbPresupuestosAut Then
                        If Not frmItemsWizard.g_bPres1OK Then
                        'Si est� en proce o grupo reasignar presupuestos
                            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Then 'PRES1 en PROCE
                                If frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                                    frmItemsWizard.ReasignarPresup 3, True
                                End If
                            Else
                                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then 'PRES1 en GRUPO
                                    If frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                                        frmItemsWizard.ReasignarPresup 3, False
                                    End If
                                End If
                            End If
                        End If
                    End If
                    '***PRES2
                    If (gParametrosGenerales.gbUsarPres4 And (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo2))) Then
                        frmItemsWizard4.g_iTipoPres = 4
                        frmItemsWizard4.Show 1
                    End If
                End If
            End If
    
    
        Case 2
            If gParametrosGenerales.gbOBLPC Then
                If txtPorcenAsig.Text <> 100 Then
                    oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                    Exit Sub
                End If
            Else
                If txtPorcenAsig.Text <> 0 Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                End If
            End If
            Unload Me
            
            '***PRES1
            If (gParametrosGenerales.gbUsarPres3 And (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo1))) Then
                frmItemsWizard4.g_iTipoPres = 3
                frmItemsWizard4.Show 1
            Else
                'Si est� en proce o grupo reasignar presupuestos
                If gParametrosGenerales.gbUsarPres3 And Not gParametrosGenerales.gbPresupuestosAut Then
                    If Not frmItemsWizard.g_bPresAnu2OK Then
                        If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Then 'PRES1 en PROCE
                            If frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                                frmItemsWizard.ReasignarPresup 3, True
                            End If
                        Else
                            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then 'PRES1 en GRUPO
                                If frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                                    frmItemsWizard.ReasignarPresup 3, False
                                End If
                            End If
                        End If
                    End If
                End If
            
                '***PRES2
                If (gParametrosGenerales.gbUsarPres4 And (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo2))) Then
                    frmItemsWizard4.g_iTipoPres = 4
                    frmItemsWizard4.Show 1
                End If
            End If
    
        Case 3
            If gParametrosGenerales.gbOBLPres3 Then
                If txtPorcenAsig.Text <> 100 Then
                    oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                    Exit Sub
                End If
            Else
                If txtPorcenAsig.Text <> 0 Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                End If
            End If
            Unload Me
            
            '***PRES2
            If (gParametrosGenerales.gbUsarPres4 And (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnItem Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo2))) Then
                frmItemsWizard4.g_iTipoPres = 4
                frmItemsWizard4.Show 1
            End If
    
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "cmdContinuar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Finaliza la insercion de datos del item
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdFinalizr_click sistema; Tiempo m�ximo:0</remarks>

Private Sub cmdFinalizar_Click()
Dim teserror As TipoErrorSummit
Dim bItems As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCancelar = True
    
    Select Case g_iTipoPres
        Case 1
                If gParametrosGenerales.gbOBLPP Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                Else
                    If txtPorcenAsig.Text <> 0 Then
                        If txtPorcenAsig.Text <> 100 Then
                            oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                            Exit Sub
                        End If
                    End If
                End If
                                                
                If Not gParametrosGenerales.gbPresupuestosAut Then
                    'Si alguno de los presupuestos est� en proce o grupo reasignar presupuestos
                    '**PRESANU2
                    If gParametrosGenerales.gbUsarPres2 Then
                        If Not frmItemsWizard.g_bPresAnu2OK Then
                            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso Then 'PRESANU2 en PROCE
                                If frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                                    frmItemsWizard.ReasignarPresup 2, True
                                End If
                            Else
                                If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo Then 'PRESANU2 en GRUPO
                                    If frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 Then
                                        frmItemsWizard.ReasignarPresup 2, False
                                    End If
                                End If
                            End If
                        End If
                    End If
                    '***PRES1
                    If gParametrosGenerales.gbUsarPres3 Then
                        If Not frmItemsWizard.g_bPres1OK Then
                            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Then 'PRES1 en PROCE
                                If frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                                    frmItemsWizard.ReasignarPresup 3, True
                                End If
                            Else
                                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then 'PRES1 en GRUPO
                                    If frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                                        frmItemsWizard.ReasignarPresup 3, False
                                    End If
                                End If
                            End If
                        End If
                    End If
                    '***PRES2
                    If gParametrosGenerales.gbUsarPres4 Then
                        If Not frmItemsWizard.g_bPres2OK Then
                            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Then 'PRES1 en PROCE
                                If frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                                    frmItemsWizard.ReasignarPresup 4, True
                                End If
                            Else
                                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo Then 'PRES1 en GRUPO
                                    If frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 Then
                                        frmItemsWizard.ReasignarPresup 4, False
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
                
        Case 2
    
                If gParametrosGenerales.gbOBLPC Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                Else
                    If txtPorcenAsig.Text <> 0 Then
                        If txtPorcenAsig.Text <> 100 Then
                            oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                            Exit Sub
                        End If
                    End If
                End If
                                                
                If Not gParametrosGenerales.gbPresupuestosAut Then
                    'Si alguno de los presupuestos est� en proce o grupo reasignar presupuestos
                    '***PRES1
                    If gParametrosGenerales.gbUsarPres3 Then
                        If Not frmItemsWizard.g_bPres1OK Then
                            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso Then 'PRES1 en PROCE
                                If frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                                    frmItemsWizard.ReasignarPresup 3, True
                                End If
                            Else
                                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo Then 'PRES1 en GRUPO
                                    If frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                                        frmItemsWizard.ReasignarPresup 3, False
                                    End If
                                End If
                            End If
                        End If
                    End If
                    '***PRES2
                    If gParametrosGenerales.gbUsarPres4 Then
                        If Not frmItemsWizard.g_bPres2OK Then
                            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Then 'PRES1 en PROCE
                                If frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                                    frmItemsWizard.ReasignarPresup 4, True
                                End If
                            Else
                                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo Then 'PRES1 en GRUPO
                                    If frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 Then
                                        frmItemsWizard.ReasignarPresup 4, False
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
                
        Case 3
    
                If gParametrosGenerales.gbOBLPres3 Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                Else
                    If txtPorcenAsig.Text <> 0 Then
                        If txtPorcenAsig.Text <> 100 Then
                            oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                            Exit Sub
                        End If
                    End If
                End If
                                                
                If Not gParametrosGenerales.gbPresupuestosAut Then
                    'Si alguno de los presupuestos est� en proce o grupo reasignar presupuestos
                    '***PRES2
                    If gParametrosGenerales.gbUsarPres4 Then
                        If Not frmItemsWizard.g_bPres2OK Then
                            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso Then 'PRES1 en PROCE
                                If frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                                    frmItemsWizard.ReasignarPresup 4, True
                                End If
                            Else
                                If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnGrupo Then 'PRES1 en GRUPO
                                    If frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 Then
                                        frmItemsWizard.ReasignarPresup 4, False
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
    
        Case 4
    
                If gParametrosGenerales.gbOBLPres4 Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                Else
                    If txtPorcenAsig.Text <> 0 Then
                        If txtPorcenAsig.Text <> 100 Then
                            oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                            Exit Sub
                        End If
                    End If
                End If
    
    End Select
    
    If Not IsEmpty(frmPROCE.g_oProcesoSeleccionado.Plantilla) And Not IsNull(frmPROCE.g_oProcesoSeleccionado.Plantilla) Then
            If frmPROCE.g_oProcesoSeleccionado.DefEspItems Then
                frmPROCE.m_bAtribItemPlant = True
            End If
    End If
    
    Screen.MousePointer = vbHourglass
    teserror = frmItemsWizard.AnyadirMultiplesItems(frmItemsWizard.g_vValores(4), frmItemsWizard.g_vValores(6), frmItemsWizard.HayUON, frmItemsWizard.HayPresup, frmItemsWizard.g_vValores(15), frmItemsWizard.g_vValores(17))
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        
        If teserror.NumError = TESVolumenAdjDirSuperado Then
            If frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then basErrores.TratarError teserror
            If Not frmPROCE.g_oProcesoSeleccionado.PermSaltarseVolMaxAdjDir Then
                m_bCancelar = True
                Unload Me
                Exit Sub
            End If
        Else
            basErrores.TratarError teserror
            m_bCancelar = True
            Unload Me
            Exit Sub
        End If
    End If
    
    frmItemsWizard.AnyadirItemsAGridApertura
    
    If frmItemsWizard.g_oItems.Count > 1 Then bItems = True
    
    m_bCancelar = True
    Unload Me
    
    oMensajes.ItemsAnyadidosAProceOK bItems

'    If frmPROCE.txtPresGlob.Text <> "" Then
'        If frmPROCE.lblPresGrupo <> "" Then
'            If CDbl(frmPROCE.lblPresGrupo) > CDbl(frmPROCE.txtPresGlob.Text) Then
'                oMensajes.PresActualMayorGlobal frmPROCE.txtPresGlob.Text
'            End If
'        End If
'    End If
        
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "cmdFinalizar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub cmdVolver_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCancelar = False
    
    Select Case g_iTipoPres
    
        Case 1
            If gParametrosGenerales.gbOBLPP Then
                If txtPorcenAsig.Text <> 100 Then
                    oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                    Exit Sub
                End If
            Else
                If txtPorcenAsig.Text <> 0 Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                End If
            End If
            If frmPROCE.sdbgItems.Columns("DIST").Visible Then
                frmItemsWizard3.MostrarDistribSeleccionada
                Unload Me
                frmItemsWizard3.Show 1
            Else
                frmItemsWizard2.MostrarValoresSeleccionados
                Unload Me
                frmItemsWizard2.Show 1
            End If
        
        Case 2
            If gParametrosGenerales.gbOBLPC Then
                If txtPorcenAsig.Text <> 100 Then
                    oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                    Exit Sub
                End If
            Else
                If txtPorcenAsig.Text <> 0 Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                End If
            End If
            If gParametrosGenerales.gbUsarPres1 And ((frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1)) Then
                Unload Me
                frmItemsWizard4.g_iTipoPres = 1
                frmItemsWizard4.Show 1
            Else
                If frmPROCE.sdbgItems.Columns("DIST").Visible Then
                    frmItemsWizard3.MostrarDistribSeleccionada
                    Unload Me
                    frmItemsWizard3.Show 1
                Else
                    frmItemsWizard2.MostrarValoresSeleccionados
                    Unload Me
                    frmItemsWizard2.Show 1
                End If
            End If
        
        Case 3
            If gParametrosGenerales.gbOBLPres3 Then
                If txtPorcenAsig.Text <> 100 Then
                    oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                    Exit Sub
                End If
            Else
                If txtPorcenAsig.Text <> 0 Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                End If
            End If
            If gParametrosGenerales.gbUsarPres2 And ((frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2)) Then
                Unload Me
                frmItemsWizard4.g_iTipoPres = 2
                frmItemsWizard4.Show 1
            Else
                If gParametrosGenerales.gbUsarPres1 And ((frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1)) Then
                    Unload Me
                    frmItemsWizard4.g_iTipoPres = 1
                    frmItemsWizard4.Show 1
                Else
                    If frmPROCE.sdbgItems.Columns("DIST").Visible Then
                        frmItemsWizard3.MostrarDistribSeleccionada
                        Unload Me
                        frmItemsWizard3.Show 1
                    Else
                        frmItemsWizard2.MostrarValoresSeleccionados
                        Unload Me
                        frmItemsWizard2.Show 1
                    End If
                End If
            End If
        
        Case 4
            If gParametrosGenerales.gbOBLPres4 Then
                If txtPorcenAsig.Text <> 100 Then
                    oMensajes.AsignarPresup100Obl txtPorcenAsig.Text
                    Exit Sub
                End If
            Else
                If txtPorcenAsig.Text <> 0 Then
                    If txtPorcenAsig.Text <> 100 Then
                        oMensajes.AsignarPresup0100Obl txtPorcenAsig.Text
                        Exit Sub
                    End If
                End If
            End If
            If gParametrosGenerales.gbUsarPres3 And ((frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresTipo1)) Then
                Unload Me
                frmItemsWizard4.g_iTipoPres = 3
                frmItemsWizard4.Show 1
            Else
                If gParametrosGenerales.gbUsarPres2 And ((frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2)) Then
                    Unload Me
                    frmItemsWizard4.g_iTipoPres = 2
                    frmItemsWizard4.Show 1
                Else
                    If gParametrosGenerales.gbUsarPres1 And ((frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnItem) Or (frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnGrupo And Not frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1)) Then
                        Unload Me
                        frmItemsWizard4.g_iTipoPres = 1
                        frmItemsWizard4.Show 1
                    Else
                        If frmPROCE.sdbgItems.Columns("DIST").Visible Then
                            frmItemsWizard3.MostrarDistribSeleccionada
                            Unload Me
                            frmItemsWizard3.Show 1
                        Else
                            frmItemsWizard2.MostrarValoresSeleccionados
                            Unload Me
                            frmItemsWizard2.Show 1
                        End If
                    End If
                End If
            End If
        
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "cmdVolver_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bDescargarFrm = False
m_bActivado = False
    Set cP = New cPopupMenu
    cP.hWndOwner = Me.hWnd

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarAnyos

    CargarRecursos
    
    Select Case g_iTipoPres
    Case 1
        m_bRuo = frmPROCE.g_bRUOPresAnuTipo1
    Case 2
        m_bRuo = frmPROCE.g_bRUOPresAnuTipo2
    Case 3
        m_bRuo = frmPROCE.g_bRUOPresTipo1
    Case 4
        m_bRuo = frmPROCE.g_bRUOPresTipo2
    End Select
    
    g_bSinPermisos = False
    
    CrearStringArticulos
    
    ConfigurarTab0
    
    If Not g_bSinPermisos Then
        
        m_bRestringirMultiplePres = frmItemsWizard.g_oItems.RestriccionMultiplePres
        
        Cargar
    Else
        RefrescarTotales
    End If
    OcultarBotones
    
    m_bCancelar = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub ConfigurarTab0()
Dim arPresup As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_iUOBase = 0
    If m_bRuo Or m_bRuoRama Then
        If (NullToStr(basOptimizacion.gUON3Usuario) <> "") Then
            m_iUOBase = 3
            m_sUON3 = NullToStr(basOptimizacion.gUON3Usuario)
            m_sUON2 = NullToStr(basOptimizacion.gUON2Usuario)
            m_sUON1 = NullToStr(basOptimizacion.gUON1Usuario)
        ElseIf (NullToStr(basOptimizacion.gUON2Usuario) <> "") Then
            m_iUOBase = 2
            m_sUON2 = NullToStr(basOptimizacion.gUON2Usuario)
            m_sUON1 = NullToStr(basOptimizacion.gUON1Usuario)
        ElseIf (NullToStr(basOptimizacion.gUON1Usuario) <> "") Then
            m_iUOBase = 1
            m_sUON1 = basOptimizacion.gUON1Usuario
        End If
        
    End If
    
    Select Case g_iTipoPres
    Case 1
        Set m_oPresupuestos1 = oFSGSRaiz.generar_CPresProyectosNivel1
        If gParametrosGenerales.gbOblAsigPresUonArtAper Then
            arPresup = m_oPresupuestos1.ExistenPresupuestos(m_sUON1, m_sUON2, m_sUON3, m_bRuo, m_bRuoRama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
        Else
            arPresup = m_oPresupuestos1.ExistenPresupuestos(m_sUON1, m_sUON2, m_sUON3, m_bRuo, m_bRuoRama)
        End If
    Case 2
        Set m_oPresupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel1
        If gParametrosGenerales.gbOblAsigPresUonArtAper Then
            arPresup = m_oPresupuestos2.ExistenPresupuestos(m_sUON1, m_sUON2, m_sUON3, m_bRuo, m_bRuoRama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
        Else
            arPresup = m_oPresupuestos2.ExistenPresupuestos(m_sUON1, m_sUON2, m_sUON3, m_bRuo, m_bRuoRama)
        End If
    Case 3
        Set m_oPresupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel1
        If gParametrosGenerales.gbOblAsigPresUonArtAper Then
            arPresup = m_oPresupuestos3.ExistenPresupuestos(m_sUON1, m_sUON2, m_sUON3, m_bRuo, m_bRuoRama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
        Else
            arPresup = m_oPresupuestos3.ExistenPresupuestos(m_sUON1, m_sUON2, m_sUON3, m_bRuo, m_bRuoRama)
        End If
    
    Case 4
        Set m_oPresupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel1
        If gParametrosGenerales.gbOblAsigPresUonArtAper Then
            arPresup = m_oPresupuestos4.ExistenPresupuestos(m_sUON1, m_sUON2, m_sUON3, m_bRuo, m_bRuoRama, True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
        Else
            arPresup = m_oPresupuestos4.ExistenPresupuestos(m_sUON1, m_sUON2, m_sUON3, m_bRuo, m_bRuoRama)
        End If
    End Select
    
    Select Case arPresup(0)
    Case 0
        'No hay ningun presupuesto para las ramas permitidas
        oMensajes.MensajeOKOnly 728
        g_bSinPermisos = True
        SSTabPresupuestos.TabVisible(0) = True
        SSTabPresupuestos.TabVisible(1) = False
    Case 1
        SSTabPresupuestos.TabVisible(1) = True
        SSTabPresupuestos.TabVisible(0) = False
        m_sUON1 = NullToStr(arPresup(1))
        m_sUON2 = NullToStr(arPresup(2))
        m_sUON3 = NullToStr(arPresup(3))
        If m_sUON3 <> "" Then
            If m_oUnidadesOrgN3 Is Nothing Then
                Set m_oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
            End If
            lblUO.caption = m_sUON3 & " - " & m_oUnidadesOrgN3.DevolverDenominacion(m_sUON1, m_sUON2, m_sUON3)
        Else
            If m_sUON2 <> "" Then
                If m_oUnidadesOrgN2 Is Nothing Then
                    Set m_oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
                End If
                lblUO.caption = m_sUON2 & " - " & m_oUnidadesOrgN2.DevolverDenominacion(m_sUON1, m_sUON2)
            Else
                If m_oUnidadesOrgN1 Is Nothing Then
                    Set m_oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
                End If
                lblUO.caption = m_sUON1 & " - " & m_oUnidadesOrgN1.DevolverDenominacion(m_sUON1)
            End If
        End If
    Case Else
        SSTabPresupuestos.TabVisible(1) = False
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "ConfigurarTab0", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Sub Cargar(Optional ByVal Anyo As String)
Dim oPresupuestos As Object
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    GenerarEstructuraOrg False
    If SSTabPresupuestos.TabVisible(0) Then 'Se carga el arbol de UON s�lo si la presta�a est� visible
        Select Case g_iTipoPres
        Case 1
            If frmItemsWizard.g_oPresupuestos1Nivel4 Is Nothing Then
                Set frmItemsWizard.g_oPresupuestos1Nivel4 = oFSGSRaiz.Generar_CPresProyectosNivel4
            End If
            Set oPresupuestos = frmItemsWizard.g_oPresupuestos1Nivel4
        Case 2
            If frmItemsWizard.g_oPresupuestos2Nivel4 Is Nothing Then
                Set frmItemsWizard.g_oPresupuestos2Nivel4 = oFSGSRaiz.Generar_CPresContablesNivel4
            End If
            Set oPresupuestos = frmItemsWizard.g_oPresupuestos2Nivel4
        Case 3
            If frmItemsWizard.g_oPresupuestos3Nivel4 Is Nothing Then
                Set frmItemsWizard.g_oPresupuestos3Nivel4 = oFSGSRaiz.Generar_CPresConceptos3Nivel4
            End If
            Set oPresupuestos = frmItemsWizard.g_oPresupuestos3Nivel4
        Case 4
            If frmItemsWizard.g_oPresupuestos4Nivel4 Is Nothing Then
                Set frmItemsWizard.g_oPresupuestos4Nivel4 = oFSGSRaiz.Generar_CPresConceptos4Nivel4
            End If
            Set oPresupuestos = frmItemsWizard.g_oPresupuestos4Nivel4
        End Select
        If basOptimizacion.gTipoDeUsuario <> Administrador Then
            BuscarUONUsuario
        End If

    Else
        Select Case g_iTipoPres
        Case 1
            If frmItemsWizard.g_oPresupuestos1Nivel4 Is Nothing Then
                Set frmItemsWizard.g_oPresupuestos1Nivel4 = oFSGSRaiz.Generar_CPresProyectosNivel4
            End If
            GenerarEstructuraPresupuestosTipo1 False, True
            Set oPresupuestos = frmItemsWizard.g_oPresupuestos1Nivel4
        Case 2
            If frmItemsWizard.g_oPresupuestos2Nivel4 Is Nothing Then
                Set frmItemsWizard.g_oPresupuestos2Nivel4 = oFSGSRaiz.Generar_CPresContablesNivel4
            End If
            GenerarEstructuraPresupuestosTipo2 False, True
            Set oPresupuestos = frmItemsWizard.g_oPresupuestos2Nivel4
        Case 3
            If frmItemsWizard.g_oPresupuestos3Nivel4 Is Nothing Then
                Set frmItemsWizard.g_oPresupuestos3Nivel4 = oFSGSRaiz.Generar_CPresConceptos3Nivel4
            End If
            GenerarEstructuraPresupuestosTipo3 False
            Set oPresupuestos = frmItemsWizard.g_oPresupuestos3Nivel4
        Case 4
            If frmItemsWizard.g_oPresupuestos4Nivel4 Is Nothing Then
                Set frmItemsWizard.g_oPresupuestos4Nivel4 = oFSGSRaiz.Generar_CPresConceptos4Nivel4
            End If
            GenerarEstructuraPresupuestosTipo4 False
            Set oPresupuestos = frmItemsWizard.g_oPresupuestos4Nivel4
        End Select
    End If
    If oPresupuestos.Count > 0 Then
        m_dblPorcenAsig = 1
    Else
        m_dblPorcenAsig = 0
    End If
    
    If g_iTipoPres < 3 Then
        If oPresupuestos.Count = 0 Then
            If Anyo = "" Then
                sdbcAnyo.Value = frmPROCE.sdbcAnyo.Value
            Else
                sdbcAnyo.Value = Anyo
            End If
        Else
            sdbcAnyo.Value = oPresupuestos.Item(1).Anyo
        End If
    Else
        sdbcAnyo.Visible = False
    End If
    
    RefrescarTotales
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "Cargar", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub
Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3
' Otras
Dim nodx As node
Dim bUON0 As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    tvwestrorg.Nodes.clear
    
    Set m_oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set m_oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set m_oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
       
         
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And m_bRuo Then
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
                Else
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama)
                End If
            Case 2
                If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts
                Else
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama
                End If
            Case 3
                If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama, , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts
                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts
                Else
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen, , , , , , , m_bRuoRama
                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRuo, bOrdenadoPorDen
                End If
        End Select
               
    Else
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
                Else
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                End If
            Case 2
                If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts
                Else
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                End If
            Case 3
                If gParametrosGenerales.gbOblAsigPresUonArtAper Then
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts
                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , , True, 3, frmPROCE.sdbcAnyo.Value, frmPROCE.sdbcGMN1_4Cod.Value, frmPROCE.sdbcProceCod.Value, , , sCodArts
                Else
                    bUON0 = m_oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen)
                    m_oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                    m_oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen
                End If
        End Select
        

    End If
    
        
   '************************************************************
    'Generamos la estructura arborea
    Dim iPresup As Integer
    Dim sKeyNodo As String
    
    ' Unidades organizativas
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    If bUON0 Then
        If ComprobarSiEstaAsignado() Then
            nodx.Image = "UON0A"
            nodx.Expanded = True
        Else
            nodx.Image = "UON0D"
        End If
    Else
        nodx.Image = "UON0"
    End If
    nodx.Tag = "UON0"
    nodx.Expanded = True
    
    iPresup = 0
    sKeyNodo = ""
    
    For Each oUON1 In m_oUnidadesOrgN1
    
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        If ComprobarSiEstaAsignado(oUON1.Cod) Then
            nodx.Image = "UON1A"
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        ElseIf oUON1.ConPresup Then
            nodx.Image = "UON1D"
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        End If
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
    Next
    
    For Each oUON2 In m_oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        If ComprobarSiEstaAsignado(oUON2.CodUnidadOrgNivel1, oUON2.Cod) Then
            nodx.Image = "UON2A"
            nodx.Parent.Expanded = True
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        ElseIf oUON2.ConPresup Then
            nodx.Image = "UON2D"
            nodx.Parent.Expanded = True
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        End If
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
    Next
    
    For Each oUON3 In m_oUnidadesOrgN3

        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        If ComprobarSiEstaAsignado(oUON3.CodUnidadOrgNivel1, oUON3.CodUnidadOrgNivel2, oUON3.Cod) Then
            nodx.Image = "UON3A"
            nodx.Parent.Parent.Expanded = True
            nodx.Parent.Expanded = True
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        ElseIf oUON3.ConPresup Then
            nodx.Image = "UON3D"
            nodx.Parent.Parent.Expanded = True
            nodx.Parent.Expanded = True
            nodx.Expanded = True
            iPresup = iPresup + 1
            sKeyNodo = nodx.key
        End If
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
    Next
    
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    If iPresup = 1 Then 'Solo hay una org compras seleccionable
        tvwestrorg.Nodes(sKeyNodo).Selected = True
        tvwestrorg_NodeClick tvwestrorg.Nodes(sKeyNodo)
        SSTabPresupuestos.Tab = 1
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "GenerarEstructuraOrg", err, Erl, , m_bActivado)
        Exit Sub
    End If
        
End Sub
Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function
    
    Select Case Left(node.Tag, 5)
        Case "PRES1", "PRES2", "PRES3", "PRES4"
            iPosicion = InStr(5, node.Tag, "-%$")
            DevolverCod = Mid(node.Tag, 6, iPosicion - 6)
    End Select
    
    Select Case Left(node.Tag, 4)
        Case "UON1", "UON2", "UON3"
            DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "DevolverCod", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Public Function DevolverDen(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer
Dim iPos3 As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function

    'posici�n del primer separador
    iPosicion = InStr(1, node.Tag, "-%$")
    'posici�n del segundo separador
    iPosicion = InStr(iPosicion + 1, node.Tag, "-%$")
    'posici�n del tercer separador
    iPos3 = InStr(iPosicion + 1, node.Tag, "-%$")
    iPosicion = iPosicion + 3
    DevolverDen = Mid(node.Tag, iPosicion, iPos3 - iPosicion)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "DevolverDen", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function
    
    'posici�n del primer separador
    iPosicion = InStr(6, node.Tag, "-%$")
    DevolverId = val(Right(node.Tag, Len(node.Tag) - (iPosicion + 2)))
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "DevolverId", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Function DevolverPorcentaje(ByVal nodx As MSComctlLib.node) As Double
Dim dPorcen As Double
Dim sCod As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sCod = DevolverCodigoColeccion(nodx)

    Select Case g_iTipoPres
    Case 1
        If frmItemsWizard.g_oPresupuestos1Nivel4.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = frmItemsWizard.g_oPresupuestos1Nivel4.Item(sCod).Porcen
        End If
    Case 2
        If frmItemsWizard.g_oPresupuestos2Nivel4.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = frmItemsWizard.g_oPresupuestos2Nivel4.Item(sCod).Porcen
        End If
    Case 3
        If frmItemsWizard.g_oPresupuestos3Nivel4.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = frmItemsWizard.g_oPresupuestos3Nivel4.Item(sCod).Porcen
        End If
    Case 4
        If frmItemsWizard.g_oPresupuestos4Nivel4.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = frmItemsWizard.g_oPresupuestos4Nivel4.Item(sCod).Porcen
        End If
    End Select
    
    DevolverPorcentaje = dPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "DevolverPorcentaje", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Public Sub MostrarDatosBarraInf()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim vImporte As Variant
Dim vObjetivo As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwestrPres.selectedItem
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 5)
        
            Case "Raiz "
                
                txtPresupuesto.Text = ""
                txtObj.Text = ""
                
            Case "PRES1"
            
                Select Case g_iTipoPres
                    Case 1
                        scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx)))
                        txtPresupuesto.Text = DblToStr(m_oPresupuestos1.Item(scod1).importe)
                        txtObj.Text = Format(DblToStr(m_oPresupuestos1.Item(scod1).Objetivo), "0.0#\%")
                    Case 2
                        scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx)))
                        txtPresupuesto.Text = DblToStr(m_oPresupuestos2.Item(scod1).importe)
                        txtObj.Text = Format(DblToStr(m_oPresupuestos2.Item(scod1).Objetivo), "0.0#\%")
                    Case 3
                        scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(DevolverCod(nodx)))
                        txtPresupuesto.Text = DblToStr(m_oPresupuestos3.Item(scod1).importe)
                        txtObj.Text = Format(DblToStr(m_oPresupuestos3.Item(scod1).Objetivo), "0.0#\%")
                    Case 4
                        scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx)))
                        txtPresupuesto.Text = DblToStr(m_oPresupuestos4.Item(scod1).importe)
                        txtObj.Text = Format(DblToStr(m_oPresupuestos4.Item(scod1).Objetivo), "0.0#\%")
                
                End Select
                
            Case "PRES2"
            
                Select Case g_iTipoPres
                    Case 1
                        scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx.Parent)))
                        scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).importe
                        vObjetivo = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).Objetivo
                    Case 2
                        scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent)))
                        scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe
                        vObjetivo = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Objetivo
                    Case 3
                        scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(DevolverCod(nodx.Parent)))
                        scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).importe
                        vObjetivo = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).Objetivo
                    Case 4
                        scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent)))
                        scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).importe
                        vObjetivo = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).Objetivo
                
                End Select
                txtPresupuesto.Text = DblToStr(vImporte)
                txtObj.Text = Format(DblToStr(vObjetivo), "0.0#\%")
            
            Case "PRES3"
            
                Select Case g_iTipoPres
                    Case 1
                        scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(DevolverCod(nodx.Parent)))
                        scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).importe
                        vObjetivo = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).Objetivo
                    Case 2
                        scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent)))
                        scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe
                        vObjetivo = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Objetivo
                    Case 3
                        scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(DevolverCod(nodx.Parent)))
                        scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).PresConceptos3Nivel3.Item(scod1 & scod2 & scod3).importe
                        vObjetivo = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).PresConceptos3Nivel3.Item(scod1 & scod2 & scod3).Objetivo
                    Case 4
                        scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx.Parent)))
                        scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).importe
                        vObjetivo = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).Objetivo
                End Select
                txtPresupuesto.Text = DblToStr(vImporte)
                txtObj.Text = Format(DblToStr(vObjetivo), "0.0#\%")
            
            Case "PRES4"
            
                Select Case g_iTipoPres
                    Case 1
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(DevolverCod(nodx.Parent)))
                        scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).PresProyectosNivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                        vObjetivo = m_oPresupuestos1.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).PresProyectosNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                    Case 2
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx.Parent)))
                        scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                        vObjetivo = m_oPresupuestos2.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                    Case 3
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(DevolverCod(nodx.Parent)))
                        scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).PresConceptos3Nivel3.Item(scod1 & scod2 & scod3).PresConceptos3Nivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                        vObjetivo = m_oPresupuestos3.Item(scod1).PresConceptos3Nivel2.Item(scod1 & scod2).PresConceptos3Nivel3.Item(scod1 & scod2 & scod3).PresConceptos3Nivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                    Case 4
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                        scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(DevolverCod(nodx.Parent.Parent)))
                        scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(DevolverCod(nodx.Parent)))
                        scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(DevolverCod(nodx)))
                        vImporte = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).PresConceptos4Nivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                        vObjetivo = m_oPresupuestos4.Item(scod1).PresConceptos4Nivel2.Item(scod1 & scod2).PresConceptos4Nivel3.Item(scod1 & scod2 & scod3).PresConceptos4Nivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                End Select
                txtPresupuesto.Text = DblToStr(vImporte)
                txtObj.Text = Format(DblToStr(vObjetivo), "0.0#\%")
        
        End Select
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "MostrarDatosBarraInf", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Function QuitarPorcentaje(ByVal sTexto As String) As String
Dim i As Long
Dim sAux As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sAux = sTexto
    
    If sTexto = "" Then Exit Function
    
    i = Len(sTexto)
    
    If i = 1 Then Exit Function
    
    sTexto = Left(sTexto, Len(sTexto) - 1)
    
    While i > 1
        
        If Mid(sTexto, Len(sTexto), 1) <> "(" Then
            sTexto = Left(sTexto, Len(sTexto) - 1)
            i = i - 1
        Else
            sTexto = Left(sTexto, Len(sTexto) - 2)
            i = -1
        End If
    
    Wend
    
    If i = 0 Or i = 1 Then
        sTexto = sAux
    End If
    
    QuitarPorcentaje = sTexto
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "QuitarPorcentaje", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function



Private Sub GenerarEstructuraPresupuestosTipo1(ByVal bOrdenadoPorDen As Boolean, Optional ByVal bBuscarEnOtrosAnyos As Boolean = False)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oPRES1 As CPresProyNivel1
Dim oPRES2 As CPresProyNivel2
Dim oPRES3 As CPresProyNivel3
Dim oPRES4 As CPresProyNivel4
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwestrPres.Nodes.clear

    Set nodx = tvwestrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres1, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    Set m_oPresupuestos1 = oFSGSRaiz.generar_CPresProyectosNivel1

    If SSTabPresupuestos.TabVisible(0) = True Then
        
        Set nodo = tvwestrorg.selectedItem
        If nodo Is Nothing Then Exit Sub
        
        Select Case Left(nodo.Tag, 4)
            Case "UON3"
                m_sUON1 = DevolverCod(nodo.Parent.Parent)
                m_sUON2 = DevolverCod(nodo.Parent)
                m_sUON3 = DevolverCod(nodo)
                m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, m_sUON1, m_sUON2, m_sUON3, bBuscarEnOtrosAnyos, False
            Case "UON2"
                m_sUON1 = DevolverCod(nodo.Parent)
                m_sUON2 = DevolverCod(nodo)
                m_sUON3 = ""
                m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, m_sUON1, m_sUON2, , bBuscarEnOtrosAnyos, False
            Case "UON1"
                m_sUON1 = DevolverCod(nodo)
                m_sUON2 = ""
                m_sUON3 = ""
                m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, m_sUON1, , , bBuscarEnOtrosAnyos, False
            Case "UON0"
                m_sUON1 = ""
                m_sUON2 = ""
                m_sUON3 = ""
                m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, , , , bBuscarEnOtrosAnyos, False
        End Select
    Else
         m_oPresupuestos1.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, m_sUON1, m_sUON2, m_sUON3, bBuscarEnOtrosAnyos, False
    End If

    'Ponemos en la combo el a�o cuyos presupuestos aparecen en pantalla y
    'lo a�adimos a la combo si no se encuentra ya en ella.
    If Not m_oPresupuestos1.Item(1) Is Nothing Then
        sdbcAnyo.Value = m_oPresupuestos1.Item(1).Anyo
        If (m_oPresupuestos1.Item(1).Anyo < Year(Date) - 10) Or (m_oPresupuestos1.Item(1).Anyo > Year(Date) + 10) Then
            If Not ExisteAnyoEnCombo(CStr(m_oPresupuestos1.Item(1).Anyo)) Then
                sdbcAnyo.AddItem sdbcAnyo.Text
            End If
        End If
    End If

    Select Case gParametrosGenerales.giNEPP

        Case 1

                For Each oPRES1 In m_oPresupuestos1
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
                Next

        Case 2

                For Each oPRES1 In m_oPresupuestos1
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresProyectosNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
                    Next
                Next

        Case 3

                For Each oPRES1 In m_oPresupuestos1
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresProyectosNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
     
                            For Each oPRES3 In oPRES2.PresProyectosNivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPRES3.Cod))
                                Set nodx = tvwestrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                            Next
                    Next
                Next

        Case 4

                For Each oPRES1 In m_oPresupuestos1
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresProyectosNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresProyectosNivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPRES3.Cod))
                                Set nodx = tvwestrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                                
                                For Each oPRES4 In oPRES3.PresProyectosNivel4
                                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(oPRES4.Cod))
                                    Set nodx = tvwestrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                    nodx.Tag = "PRES4" & oPRES4.Cod & "-%$" & oPRES4.Id & "-%$" & oPRES4.Den & "-%$" & oPRES4.importe
                                Next
                            Next
                    Next
                Next

    End Select
    
    'Marcar los presupuestos asignados
    Dim lIndO As Long
    lIndO = 1
    Set m_oPresupuestos1Otros = oFSGSRaiz.Generar_CPresProyectosNivel4
    
    For Each oPRES4 In frmItemsWizard.g_oPresupuestos1Nivel4
        If oPRES4.Anyo = sdbcAnyo.Value And NullToStr(oPRES4.UON1) = m_sUON1 And NullToStr(oPRES4.UON2) = m_sUON2 And NullToStr(oPRES4.UON3) = m_sUON3 Then
                Select Case oPRES4.IDNivel
                    Case 1
                        scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                        Set nodo = tvwestrPres.Nodes.Item("PRES1" & scod1)
                        nodo.Image = "PRESA"
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                        nodo.Expanded = True

                    Case 2
                        scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                        scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES4.CodPRES2))
                        Set nodo = tvwestrPres.Nodes.Item("PRES2" & scod1 & scod2)
                        nodo.Image = "PRESA"
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                        nodo.Parent.Expanded = True

                    Case 3
                        scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                        scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES4.CodPRES2))
                        scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPRES4.CodPRES3))
                        Set nodo = tvwestrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                        nodo.Image = "PRESA"
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                        nodo.Parent.Parent.Expanded = True
                        nodo.Parent.Expanded = True

                    Case 4
                        scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                        scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPRES4.CodPRES2))
                        scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPRES4.CodPRES3))
                        scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(oPRES4.Cod))
                        Set nodo = tvwestrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                        nodo.Image = "PRESA"
                        nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                        nodo.Parent.Parent.Parent.Expanded = True
                        nodo.Parent.Parent.Expanded = True
                        nodo.Parent.Expanded = True

                End Select
        Else
            m_oPresupuestos1Otros.Add oPRES4.Anyo, oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, oPRES4.Objetivo, lIndO, oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True
            lIndO = lIndO + 1
        End If

    Next
    ConfigurarEtiquetas lIndO
    Set oPRES1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    Set nodo = Nothing
    Set nodx = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "GenerarEstructuraPresupuestosTipo1", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub
Private Function ConfigurarEtiquetas(ByVal lIndice As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next

        If lIndice = 1 Then
            cmdOtras.Visible = False
            lblOtras.Visible = False
            lblUO.Width = tvwestrPres.Width
            sdbcAnyo.Left = lblUO.Left + lblUO.Width - sdbcAnyo.Width
        Else
            cmdOtras.Visible = True
            lblOtras.Visible = True
            lblUO.Width = tvwestrPres.Width - 1400
            sdbcAnyo.Left = lblUO.Left + lblUO.Width - sdbcAnyo.Width
        End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "ConfigurarEtiquetas", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Function ExisteAnyoEnCombo(ByVal sAnyo As String) As Boolean
'***********************************************************************************
'*** Descripci�n: Comprueba la existencia en la combo de a�os (sdbcAnyo) del a�o ***
'***              que le es pasado en el argumento de tipo string.               ***
'***                                                                             ***
'*** Par�metros : sAnyo ::>> Es de tipo string y especifica el a�o cuya          ***
'***                         existencia en la combo se est� comprobando.         ***
'***                                                                             ***
'*** Valor que devuelve: TRUE: Si el a�o existe en la combo.                     ***
'***                     FALSE: Si el a�o no existe en la combo.                 ***
'***********************************************************************************
    Dim i As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcAnyo.MoveFirst
    For i = 1 To sdbcAnyo.Rows
        If sdbcAnyo.Columns.Item(0).Value = sAnyo Then
            ExisteAnyoEnCombo = True
            Exit Function
        End If
        sdbcAnyo.MoveNext
    Next i
    ExisteAnyoEnCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "ExisteAnyoEnCombo", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
Private Sub GenerarEstructuraPresupuestosTipo2(ByVal bOrdenadoPorDen As Boolean, Optional ByVal bBuscarEnOtrosAnyos As Boolean = False)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oPRES1 As CPresConNivel1
Dim oPRES2 As CPresconNivel2
Dim oPRES3 As CPresConNivel3
Dim oPRES4 As CPresconNivel4
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwestrPres.Nodes.clear

    Set nodx = tvwestrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres2, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    Set m_oPresupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel1

    If SSTabPresupuestos.TabVisible(0) = True Then
        
        Set nodo = tvwestrorg.selectedItem
        If nodo Is Nothing Then Exit Sub
        
        Select Case Left(nodo.Tag, 4)
            Case "UON3"
                m_sUON1 = DevolverCod(nodo.Parent.Parent)
                m_sUON2 = DevolverCod(nodo.Parent)
                m_sUON3 = DevolverCod(nodo)
                m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, m_sUON1, m_sUON2, m_sUON3, bBuscarEnOtrosAnyos, False
            Case "UON2"
                m_sUON1 = DevolverCod(nodo.Parent)
                m_sUON2 = DevolverCod(nodo)
                m_sUON3 = ""
                m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, m_sUON1, m_sUON2, , bBuscarEnOtrosAnyos, False
            Case "UON1"
                m_sUON1 = DevolverCod(nodo)
                m_sUON2 = ""
                m_sUON3 = ""
                m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, m_sUON1, , , bBuscarEnOtrosAnyos, False
            Case "UON0"
                m_sUON1 = ""
                m_sUON2 = ""
                m_sUON3 = ""
                m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, , , , bBuscarEnOtrosAnyos, False
        End Select
    Else
        m_oPresupuestos2.GenerarEstructuraPresupuestos sdbcAnyo.Value, sdbcAnyo.Value, bOrdenadoPorDen, m_sUON1, m_sUON2, m_sUON3, bBuscarEnOtrosAnyos, False
    End If

    'Ponemos en la combo el a�o cuyos presupuestos aparecen en pantalla y
    'lo a�adimos a la combo si no se encuentra ya en ella.
    If Not m_oPresupuestos2.Item(1) Is Nothing Then
        sdbcAnyo.Text = m_oPresupuestos2.Item(1).Anyo
        If (m_oPresupuestos2.Item(1).Anyo < Year(Date) - 10) Or (m_oPresupuestos2.Item(1).Anyo > Year(Date) + 10) Then
            If Not ExisteAnyoEnCombo(CStr(m_oPresupuestos2.Item(1).Anyo)) Then
                sdbcAnyo.AddItem sdbcAnyo.Text
            End If
        End If
    End If

    Select Case gParametrosGenerales.giNEPC

        Case 1

                For Each oPRES1 In m_oPresupuestos2
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
                Next

        Case 2

                For Each oPRES1 In m_oPresupuestos2
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresContablesNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
                    Next
                Next

        Case 3

                For Each oPRES1 In m_oPresupuestos2
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresContablesNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresContablesNivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES3.Cod))
                                Set nodx = tvwestrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                            Next
                    Next
                Next

        Case 4

                For Each oPRES1 In m_oPresupuestos2
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresContablesNivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresContablesNivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES3.Cod))
                                Set nodx = tvwestrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                                
                                For Each oPRES4 In oPRES3.PresContablesNivel4
                                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(oPRES4.Cod))
                                    Set nodx = tvwestrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                    nodx.Tag = "PRES4" & oPRES4.Cod & "-%$" & oPRES4.Id & "-%$" & oPRES4.Den & "-%$" & oPRES4.importe
                                Next
                            Next
                    Next
                Next

    End Select
    
    'Marcar los presupuestos asignados
    Dim lIndO As Long
    lIndO = 1
    Set m_oPresupuestos2Otros = oFSGSRaiz.Generar_CPresContablesNivel4
    For Each oPRES4 In frmItemsWizard.g_oPresupuestos2Nivel4
        If oPRES4.Anyo = sdbcAnyo.Value And NullToStr(oPRES4.UON1) = m_sUON1 And NullToStr(oPRES4.UON2) = m_sUON2 And NullToStr(oPRES4.UON3) = m_sUON3 Then
            Select Case oPRES4.IDNivel
                Case 1
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPRES4.CodPRES1))
                    Set nodo = tvwestrPres.Nodes.Item("PRES1" & scod1)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Expanded = True
                    
                Case 2
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES4.CodPRES2))
                    Set nodo = tvwestrPres.Nodes.Item("PRES2" & scod1 & scod2)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Parent.Expanded = True
                
                Case 3
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES4.CodPRES3))
                    Set nodo = tvwestrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                
                Case 4
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES4.CodPRES3))
                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(oPRES4.Cod))
                    Set nodo = tvwestrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Parent.Parent.Parent.Expanded = True
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                    
            End Select
        Else
            m_oPresupuestos2Otros.Add oPRES4.Anyo, oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, oPRES4.Objetivo, lIndO, oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True
            lIndO = lIndO + 1
        End If
    Next
    ConfigurarEtiquetas lIndO
    Set oPRES1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    Set nodo = Nothing
    Set nodx = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "GenerarEstructuraPresupuestosTipo2", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Sub GenerarEstructuraPresupuestosTipo3(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oPRES1 As CPresConcep3Nivel1
Dim oPRES2 As CPresConcep3Nivel2
Dim oPRES3 As CPresConcep3Nivel3
Dim oPRES4 As CPresConcep3Nivel4
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwestrPres.Nodes.clear

    Set nodx = tvwestrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres3, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    Set m_oPresupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel1

    If SSTabPresupuestos.TabVisible(0) = True Then
        
        Set nodo = tvwestrorg.selectedItem
        If nodo Is Nothing Then Exit Sub
        
        Select Case Left(nodo.Tag, 4)
            Case "UON3"
                m_sUON1 = DevolverCod(nodo.Parent.Parent)
                m_sUON2 = DevolverCod(nodo.Parent)
                m_sUON3 = DevolverCod(nodo)
                m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, m_sUON1, m_sUON2, m_sUON3, False
            Case "UON2"
                m_sUON1 = DevolverCod(nodo.Parent)
                m_sUON2 = DevolverCod(nodo)
                m_sUON3 = ""
                m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, m_sUON1, m_sUON2, , False
            Case "UON1"
                m_sUON1 = DevolverCod(nodo)
                m_sUON2 = ""
                m_sUON3 = ""
                m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, m_sUON1, , , False
            Case "UON0"
                m_sUON1 = ""
                m_sUON2 = ""
                m_sUON3 = ""
                m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, , , , False
        End Select
    Else
        m_oPresupuestos3.GenerarEstructuraPresupuestosConceptos3 bOrdenadoPorDen, m_sUON1, m_sUON2, m_sUON3, False
    End If

    Select Case gParametrosGenerales.giNEP3

        Case 1

                For Each oPRES1 In m_oPresupuestos3
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
                Next

        Case 2

                For Each oPRES1 In m_oPresupuestos3
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos3Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
                    Next
                Next

        Case 3

                For Each oPRES1 In m_oPresupuestos3
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos3Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresConceptos3Nivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPRES3.Cod))
                                Set nodx = tvwestrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                            Next
                    Next
                Next

        Case 4

                For Each oPRES1 In m_oPresupuestos3
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos3Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresConceptos3Nivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPRES3.Cod))
                                Set nodx = tvwestrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                                
                                For Each oPRES4 In oPRES3.PresConceptos3Nivel4
                                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34 - Len(oPRES4.Cod))
                                    Set nodx = tvwestrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                    nodx.Tag = "PRES4" & oPRES4.Cod & "-%$" & oPRES4.Id & "-%$" & oPRES4.Den & "-%$" & oPRES4.importe
                                Next
                            Next
                    Next
                Next

    End Select
    
    Dim lIndO As Long
    lIndO = 1
    Set m_oPresupuestos3Otros = oFSGSRaiz.Generar_CPresConceptos3Nivel4
    For Each oPRES4 In frmItemsWizard.g_oPresupuestos3Nivel4
        If NullToStr(oPRES4.UON1) = m_sUON1 And NullToStr(oPRES4.UON2) = m_sUON2 And NullToStr(oPRES4.UON3) = m_sUON3 Then
            Select Case oPRES4.IDNivel
                Case 1
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES4.CodPRES1))
                    Set nodo = tvwestrPres.Nodes.Item("PRES1" & scod1)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Expanded = True
                    
                Case 2
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES4.CodPRES2))
                    Set nodo = tvwestrPres.Nodes.Item("PRES2" & scod1 & scod2)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Parent.Expanded = True
                
                Case 3
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPRES4.CodPRES3))
                    Set nodo = tvwestrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                
                Case 4
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPRES4.CodPRES3))
                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34 - Len(oPRES4.Cod))
                    Set nodo = tvwestrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Parent.Parent.Parent.Expanded = True
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                    
            End Select
        Else
            m_oPresupuestos3Otros.Add oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, oPRES4.Objetivo, lIndO, oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True
            lIndO = lIndO + 1
        End If
    Next
    ConfigurarEtiquetas lIndO
    Set oPRES1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    Set nodo = Nothing
    Set nodx = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "GenerarEstructuraPresupuestosTipo3", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub GenerarEstructuraPresupuestosTipo4(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oPRES1 As CPresConcep4Nivel1
Dim oPRES2 As CPresConcep4Nivel2
Dim oPRES3 As CPresConcep4Nivel3
Dim oPRES4 As CPresConcep4Nivel4
Dim nodx As MSComctlLib.node
Dim nodo As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    tvwestrPres.Nodes.clear

    Set nodx = tvwestrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres4, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    Set m_oPresupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel1

    If SSTabPresupuestos.TabVisible(0) = True Then
        
        Set nodo = tvwestrorg.selectedItem
        If nodo Is Nothing Then Exit Sub
        
        Select Case Left(nodo.Tag, 4)
            Case "UON3"
                m_sUON1 = DevolverCod(nodo.Parent.Parent)
                m_sUON2 = DevolverCod(nodo.Parent)
                m_sUON3 = DevolverCod(nodo)
                m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, m_sUON1, m_sUON2, m_sUON3, False
            Case "UON2"
                m_sUON1 = DevolverCod(nodo.Parent)
                m_sUON2 = DevolverCod(nodo)
                m_sUON3 = ""
                m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, m_sUON1, m_sUON2, , False
            Case "UON1"
                m_sUON1 = DevolverCod(nodo)
                m_sUON2 = ""
                m_sUON3 = ""
                m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, m_sUON1, , , False
            Case "UON0"
                m_sUON1 = ""
                m_sUON2 = ""
                m_sUON3 = ""
                m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, , , , False
        End Select
    Else
        m_oPresupuestos4.GenerarEstructuraPresupuestosConceptos4 bOrdenadoPorDen, m_sUON1, m_sUON2, m_sUON3, False
    End If

    Select Case gParametrosGenerales.giNEP4

        Case 1

                For Each oPRES1 In m_oPresupuestos4
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
                Next

        Case 2

                For Each oPRES1 In m_oPresupuestos4
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos4Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
                    Next
                Next

        Case 3

                For Each oPRES1 In m_oPresupuestos4
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos4Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresConceptos4Nivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPRES3.Cod))
                                Set nodx = tvwestrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                            Next
                    Next
                Next

        Case 4

                For Each oPRES1 In m_oPresupuestos4
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES1.Cod))
                    Set nodx = tvwestrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod & "-%$" & oPRES1.Id & "-%$" & oPRES1.Den & "-%$" & oPRES1.importe
    
                    For Each oPRES2 In oPRES1.PresConceptos4Nivel2
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES2.Cod))
                        Set nodx = tvwestrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod & "-%$" & oPRES2.Id & "-%$" & oPRES2.Den & "-%$" & oPRES2.importe
    
                            For Each oPRES3 In oPRES2.PresConceptos4Nivel3
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPRES3.Cod))
                                Set nodx = tvwestrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod & "-%$" & oPRES3.Id & "-%$" & oPRES3.Den & "-%$" & oPRES3.importe
                                
                                For Each oPRES4 In oPRES3.PresConceptos4Nivel4
                                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(oPRES4.Cod))
                                    Set nodx = tvwestrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                    nodx.Tag = "PRES4" & oPRES4.Cod & "-%$" & oPRES4.Id & "-%$" & oPRES4.Den & "-%$" & oPRES4.importe
                                Next
                            Next
                    Next
                Next

    End Select
    
    'Marcar los presupuestos asignados, al proceso o al grupo
    Dim lIndO As Long
    lIndO = 1
    Set m_oPresupuestos4Otros = oFSGSRaiz.Generar_CPresConceptos4Nivel4
    For Each oPRES4 In frmItemsWizard.g_oPresupuestos4Nivel4
        If NullToStr(oPRES4.UON1) = m_sUON1 And NullToStr(oPRES4.UON2) = m_sUON2 And NullToStr(oPRES4.UON3) = m_sUON3 Then
            Select Case oPRES4.IDNivel
                Case 1
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES4.CodPRES1))
                    Set nodo = tvwestrPres.Nodes.Item("PRES1" & scod1)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Expanded = True
                    
                Case 2
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES4.CodPRES2))
                    Set nodo = tvwestrPres.Nodes.Item("PRES2" & scod1 & scod2)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Parent.Expanded = True
                
                Case 3
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPRES4.CodPRES3))
                    Set nodo = tvwestrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                
                Case 4
                    scod1 = oPRES4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPRES4.CodPRES1))
                    scod2 = oPRES4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPRES4.CodPRES2))
                    scod3 = oPRES4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPRES4.CodPRES3))
                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(oPRES4.Cod))
                    Set nodo = tvwestrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                    nodo.Image = "PRESA"
                    nodo.Text = nodo.Text & "(" & oPRES4.Porcen * 100 & "%)"
                    nodo.Parent.Parent.Parent.Expanded = True
                    nodo.Parent.Parent.Expanded = True
                    nodo.Parent.Expanded = True
                    
            End Select
        Else
            m_oPresupuestos4Otros.Add oPRES4.CodPRES1, oPRES4.CodPRES2, oPRES4.CodPRES3, oPRES4.Cod, oPRES4.Den, oPRES4.importe, oPRES4.Objetivo, lIndO, oPRES4.UON1, oPRES4.UON2, oPRES4.UON3, oPRES4.Id, oPRES4.IDNivel, oPRES4.Porcen, True
            lIndO = lIndO + 1
        End If
    Next
    ConfigurarEtiquetas lIndO
    Set oPRES1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    Set nodx = Nothing
    Set nodo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "GenerarEstructuraPresupuestosTipo4", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub CargarRecursos()
Dim Adores As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_ITEMS_WIZARD4, basPublic.gParametrosInstalacion.gIdioma)

    If Not Adores Is Nothing Then
        Me.caption = Adores(0).Value
        Adores.MoveNext
        lblSel.caption = Adores(0).Value
        Adores.MoveNext
        lblAsignado.caption = Adores(0).Value
        Adores.MoveNext
        lblPrendiente.caption = Adores(0).Value
        Adores.MoveNext
        SSTabPresupuestos.TabCaption(0) = Adores(0).Value
        Adores.MoveNext
        SSTabPresupuestos.TabCaption(1) = Adores(0).Value
        Adores.MoveNext
        lblUO.caption = Adores(0).Value
        Adores.MoveNext
        lblPresupuesto.caption = Adores(0).Value
        Adores.MoveNext
        lblObjetivo.caption = Adores(0).Value
        Adores.MoveNext
        lblAsignar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.MoveNext
        cmdFinalizar.caption = Adores(0).Value
        Adores.MoveNext
        lblOtras.caption = Adores(0).Value

        Adores.Close

    End If
    
    Set Adores = Nothing

    Select Case g_iTipoPres
        Case 1
                Me.caption = Me.caption & " " & gParametrosGenerales.gsSingPres1
                msPresupPlural = gParametrosGenerales.gsPlurPres1
        Case 2
                Me.caption = Me.caption & " " & gParametrosGenerales.gsSingPres2
                msPresupPlural = gParametrosGenerales.gsPlurPres2
        Case 3
                Me.caption = Me.caption & " " & gParametrosGenerales.gsSingPres3
                msPresupPlural = gParametrosGenerales.gsPlurPres3
        Case 4
                Me.caption = Me.caption & " " & gParametrosGenerales.gsSingPres4
                msPresupPlural = gParametrosGenerales.gsPlurPres4
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "CargarRecursos", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Private Sub Form_Resize()

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_dblPorcenAsig = 0
    m_bPorcenAsigAbierto = False
    
    Set m_oUnidadesOrgN1 = Nothing
    Set m_oUnidadesOrgN2 = Nothing
    Set m_oUnidadesOrgN3 = Nothing
    
    Set m_oPresupuestos1 = Nothing
    Set m_oPresupuestos2 = Nothing
    Set m_oPresupuestos3 = Nothing
    Set m_oPresupuestos4 = Nothing
    
    Set m_oPresupuestos1Otros = Nothing
    Set m_oPresupuestos2Otros = Nothing
    Set m_oPresupuestos3Otros = Nothing
    Set m_oPresupuestos4Otros = Nothing

    m_sUON1 = ""
    m_sUON2 = ""
    m_sUON3 = ""
    m_bRuo = False
    m_bRuoRama = False

    If m_bCancelar Then
        frmItemsWizard.g_bCancelar = m_bCancelar
        Unload frmItemsWizard
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcAnyo_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtObj.Text = ""
    txtPresupuesto.Text = ""
    m_bRespetarPorcen = True
    txtPorcenAsignar.Text = ""
    m_bRespetarPorcen = True
    Slider1.Value = 0
    m_bRespetarPorcen = False

    Screen.MousePointer = vbHourglass
    Select Case g_iTipoPres
        Case 1
                GenerarEstructuraPresupuestosTipo1 False
        Case 2
                GenerarEstructuraPresupuestosTipo2 False
        Case 3
                GenerarEstructuraPresupuestosTipo3 False
        Case 4
                GenerarEstructuraPresupuestosTipo4 False
    End Select
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "sdbcAnyo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Slider1_Change()
Dim nodx As MSComctlLib.node
Dim dPorcen As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwestrPres.selectedItem
    
    If nodx Is Nothing Then
        Slider1.Value = 0
        Exit Sub
    End If
    
    If nodx.Parent Is Nothing Then
        Slider1.Value = 0
        Exit Sub
    End If
    
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If Slider1.Value >= Slider1.Max Then
        dPorcen = CDec(DevolverPorcentaje(tvwestrPres.selectedItem) * 100)
        dPorcen = dPorcen + (100 - CDec(m_dblPorcenAsig * 100))
    Else
        dPorcen = Slider1.Value
    End If
    txtPorcenAsignar.Text = dPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "Slider1_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub SSTabPresupuestos_Click(PreviousTab As Integer)
Dim nodx As MSComctlLib.node
Dim sImage As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
    
    If SSTabPresupuestos.Tab = 1 Then
        If tvwestrorg.selectedItem Is Nothing Then
            If SSTabPresupuestos.TabVisible(0) = True Then
                SSTabPresupuestos.Tab = 0
            End If
        Else 'Si esta asignado y es de a�o, pongo e el combo el a�o del presupuesto asignado
            If g_iTipoPres < 3 Then
                sImage = tvwestrorg.selectedItem.Image
                If Right(sImage, 1) = "A" Then
                    sdbcAnyo.Value = DevolverAnyoAsignado
                End If
            End If
        End If
        
        Screen.MousePointer = vbHourglass
        Select Case g_iTipoPres
            Case 1
                    GenerarEstructuraPresupuestosTipo1 False, True
            Case 2
                    GenerarEstructuraPresupuestosTipo2 False, True
            Case 3
                    GenerarEstructuraPresupuestosTipo3 False
            Case 4
                    GenerarEstructuraPresupuestosTipo4 False
        End Select
        Screen.MousePointer = vbNormal

        txtObj.Text = ""
        txtPresupuesto.Text = ""
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
    
    Else
        Set nodx = tvwestrorg.selectedItem
        
        If nodx Is Nothing Then Exit Sub
        
        ActualizarEstrOrg nodx
        
'        tvwestrorg.SetFocus
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "SSTabPresupuestos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub ActualizarEstrOrg(ByVal node As MSComctlLib.node)
Dim oPres1Niv4 As CPresProyNivel4
Dim oPres2Niv4 As CPresconNivel4
Dim oPres3Niv4 As CPresConcep3Nivel4
Dim oPres4Niv4 As CPresConcep4Nivel4
Dim bConPres As Boolean
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case Left(node.Tag, 4)
        
        Case "UON0"
        
                    If node.Image = "UON0A" Then
                        bConPres = True
                    Else
                        bConPres = False
                    End If
                    
                    Select Case g_iTipoPres
                        Case 1
                            If bConPres Then
                                For Each oPres1Niv4 In frmItemsWizard.g_oPresupuestos1Nivel4
                                    If NullToStr(oPres1Niv4.UON1) = m_sUON1 And NullToStr(oPres1Niv4.UON2) = m_sUON2 And NullToStr(oPres1Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON0D"
                            Else
                                For Each oPres1Niv4 In frmItemsWizard.g_oPresupuestos1Nivel4
                                    If NullToStr(oPres1Niv4.UON1) = m_sUON1 And NullToStr(oPres1Niv4.UON2) = m_sUON2 And NullToStr(oPres1Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON0A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 2
                            If bConPres Then
                                For Each oPres2Niv4 In frmItemsWizard.g_oPresupuestos2Nivel4
                                    If NullToStr(oPres2Niv4.UON1) = m_sUON1 And NullToStr(oPres2Niv4.UON2) = m_sUON2 And NullToStr(oPres2Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON0D"
                            Else
                                For Each oPres2Niv4 In frmItemsWizard.g_oPresupuestos2Nivel4
                                    If NullToStr(oPres2Niv4.UON1) = m_sUON1 And NullToStr(oPres2Niv4.UON2) = m_sUON2 And NullToStr(oPres2Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON0A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 3
                            If bConPres Then
                                For Each oPres3Niv4 In frmItemsWizard.g_oPresupuestos3Nivel4
                                    If NullToStr(oPres3Niv4.UON1) = m_sUON1 And NullToStr(oPres3Niv4.UON2) = m_sUON2 And NullToStr(oPres3Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON0D"
                            Else
                                For Each oPres3Niv4 In frmItemsWizard.g_oPresupuestos3Nivel4
                                    If NullToStr(oPres3Niv4.UON1) = m_sUON1 And NullToStr(oPres3Niv4.UON2) = m_sUON2 And NullToStr(oPres3Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON0A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 4
                            If bConPres Then
                                For Each oPres4Niv4 In frmItemsWizard.g_oPresupuestos4Nivel4
                                    If NullToStr(oPres4Niv4.UON1) = m_sUON1 And NullToStr(oPres4Niv4.UON2) = m_sUON2 And NullToStr(oPres4Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON0D"
                            Else
                                For Each oPres4Niv4 In frmItemsWizard.g_oPresupuestos4Nivel4
                                    If NullToStr(oPres4Niv4.UON1) = m_sUON1 And NullToStr(oPres4Niv4.UON2) = m_sUON2 And NullToStr(oPres4Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON0A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                    End Select
            
            Case "UON1"
                    If node.Image = "UON1A" Then
                        bConPres = True
                    Else
                        bConPres = False
                    End If
                    Select Case g_iTipoPres
                        Case 1
                            If bConPres Then
                                For Each oPres1Niv4 In frmItemsWizard.g_oPresupuestos1Nivel4
                                    If NullToStr(oPres1Niv4.UON1) = m_sUON1 And NullToStr(oPres1Niv4.UON2) = m_sUON2 And NullToStr(oPres1Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON1D"
                            Else
                                For Each oPres1Niv4 In frmItemsWizard.g_oPresupuestos1Nivel4
                                    If NullToStr(oPres1Niv4.UON1) = m_sUON1 And NullToStr(oPres1Niv4.UON2) = m_sUON2 And NullToStr(oPres1Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON1A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 2
                            If bConPres Then
                                For Each oPres2Niv4 In frmItemsWizard.g_oPresupuestos2Nivel4
                                    If NullToStr(oPres2Niv4.UON1) = m_sUON1 And NullToStr(oPres2Niv4.UON2) = m_sUON2 And NullToStr(oPres2Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON1D"
                            Else
                                For Each oPres2Niv4 In frmItemsWizard.g_oPresupuestos2Nivel4
                                    If NullToStr(oPres2Niv4.UON1) = m_sUON1 And NullToStr(oPres2Niv4.UON2) = m_sUON2 And NullToStr(oPres2Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON1A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 3
                            If bConPres Then
                                For Each oPres3Niv4 In frmItemsWizard.g_oPresupuestos3Nivel4
                                    If NullToStr(oPres3Niv4.UON1) = m_sUON1 And NullToStr(oPres3Niv4.UON2) = m_sUON2 And NullToStr(oPres3Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON1D"
                            Else
                                For Each oPres3Niv4 In frmItemsWizard.g_oPresupuestos3Nivel4
                                    If NullToStr(oPres3Niv4.UON1) = m_sUON1 And NullToStr(oPres3Niv4.UON2) = m_sUON2 And NullToStr(oPres3Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON1A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 4
                            If bConPres Then
                                For Each oPres4Niv4 In frmItemsWizard.g_oPresupuestos4Nivel4
                                    If NullToStr(oPres4Niv4.UON1) = m_sUON1 And NullToStr(oPres4Niv4.UON2) = m_sUON2 And NullToStr(oPres4Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON1D"
                            Else
                                For Each oPres4Niv4 In frmItemsWizard.g_oPresupuestos4Nivel4
                                    If NullToStr(oPres4Niv4.UON1) = m_sUON1 And NullToStr(oPres4Niv4.UON2) = m_sUON2 And NullToStr(oPres4Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON1A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                    End Select
                    
            
            Case "UON2"
                    If node.Image = "UON2A" Then
                        bConPres = True
                    Else
                        bConPres = False
                    End If
                    
                    Select Case g_iTipoPres
                        Case 1
                            If bConPres Then
                                For Each oPres1Niv4 In frmItemsWizard.g_oPresupuestos1Nivel4
                                    If NullToStr(oPres1Niv4.UON1) = m_sUON1 And NullToStr(oPres1Niv4.UON2) = m_sUON2 And NullToStr(oPres1Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON2D"
                            Else
                                For Each oPres1Niv4 In frmItemsWizard.g_oPresupuestos1Nivel4
                                    If NullToStr(oPres1Niv4.UON1) = m_sUON1 And NullToStr(oPres1Niv4.UON2) = m_sUON2 And NullToStr(oPres1Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON2A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 2
                            If bConPres Then
                                For Each oPres2Niv4 In frmItemsWizard.g_oPresupuestos2Nivel4
                                    If NullToStr(oPres2Niv4.UON1) = m_sUON1 And NullToStr(oPres2Niv4.UON2) = m_sUON2 And NullToStr(oPres2Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON2D"
                            Else
                                For Each oPres2Niv4 In frmItemsWizard.g_oPresupuestos2Nivel4
                                    If NullToStr(oPres2Niv4.UON1) = m_sUON1 And NullToStr(oPres2Niv4.UON2) = m_sUON2 And NullToStr(oPres2Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON2A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 3
                            If bConPres Then
                                For Each oPres3Niv4 In frmItemsWizard.g_oPresupuestos3Nivel4
                                    If NullToStr(oPres3Niv4.UON1) = m_sUON1 And NullToStr(oPres3Niv4.UON2) = m_sUON2 And NullToStr(oPres3Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON2D"
                            Else
                                For Each oPres3Niv4 In frmItemsWizard.g_oPresupuestos3Nivel4
                                    If NullToStr(oPres3Niv4.UON1) = m_sUON1 And NullToStr(oPres3Niv4.UON2) = m_sUON2 And NullToStr(oPres3Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON2A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 4
                            If bConPres Then
                                For Each oPres4Niv4 In frmItemsWizard.g_oPresupuestos4Nivel4
                                    If NullToStr(oPres4Niv4.UON1) = m_sUON1 And NullToStr(oPres4Niv4.UON2) = m_sUON2 And NullToStr(oPres4Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON2D"
                            Else
                                For Each oPres4Niv4 In frmItemsWizard.g_oPresupuestos4Nivel4
                                    If NullToStr(oPres4Niv4.UON1) = m_sUON1 And NullToStr(oPres4Niv4.UON2) = m_sUON2 And NullToStr(oPres4Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON2A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                    End Select
                    
                
            Case "UON3"
                    If node.Image = "UON3A" Then
                        bConPres = True
                    Else
                        bConPres = False
                    End If
                    
                    Select Case g_iTipoPres
                        Case 1
                            If bConPres Then
                                For Each oPres1Niv4 In frmItemsWizard.g_oPresupuestos1Nivel4
                                    If NullToStr(oPres1Niv4.UON1) = m_sUON1 And NullToStr(oPres1Niv4.UON2) = m_sUON2 And NullToStr(oPres1Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON3D"
                            Else
                                For Each oPres1Niv4 In frmItemsWizard.g_oPresupuestos1Nivel4
                                    If NullToStr(oPres1Niv4.UON1) = m_sUON1 And NullToStr(oPres1Niv4.UON2) = m_sUON2 And NullToStr(oPres1Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON3A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 2
                            If bConPres Then
                                For Each oPres2Niv4 In frmItemsWizard.g_oPresupuestos2Nivel4
                                    If NullToStr(oPres2Niv4.UON1) = m_sUON1 And NullToStr(oPres2Niv4.UON2) = m_sUON2 And NullToStr(oPres2Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON3D"
                            Else
                                For Each oPres2Niv4 In frmItemsWizard.g_oPresupuestos2Nivel4
                                    If NullToStr(oPres2Niv4.UON1) = m_sUON1 And NullToStr(oPres2Niv4.UON2) = m_sUON2 And NullToStr(oPres2Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON3A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 3
                            If bConPres Then
                                For Each oPres3Niv4 In frmItemsWizard.g_oPresupuestos3Nivel4
                                    If NullToStr(oPres3Niv4.UON1) = m_sUON1 And NullToStr(oPres3Niv4.UON2) = m_sUON2 And NullToStr(oPres3Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON3D"
                            Else
                                For Each oPres3Niv4 In frmItemsWizard.g_oPresupuestos3Nivel4
                                    If NullToStr(oPres3Niv4.UON1) = m_sUON1 And NullToStr(oPres3Niv4.UON2) = m_sUON2 And NullToStr(oPres3Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON3A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                        Case 4
                            If bConPres Then
                                For Each oPres4Niv4 In frmItemsWizard.g_oPresupuestos4Nivel4
                                    If NullToStr(oPres4Niv4.UON1) = m_sUON1 And NullToStr(oPres4Niv4.UON2) = m_sUON2 And NullToStr(oPres4Niv4.UON3) = m_sUON3 Then
                                        Exit Sub
                                    End If
                                Next
                                node.Image = "UON3D"
                            Else
                                For Each oPres4Niv4 In frmItemsWizard.g_oPresupuestos4Nivel4
                                    If NullToStr(oPres4Niv4.UON1) = m_sUON1 And NullToStr(oPres4Niv4.UON2) = m_sUON2 And NullToStr(oPres4Niv4.UON3) = m_sUON3 Then
                                        node.Image = "UON3A"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        
                    End Select
                                    
        End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "ActualizarEstrOrg", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub
Public Sub ConfigurarInterfazSeguridadUON(ByVal node As MSComctlLib.node)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblEstrorg.caption = node.Text
    lblUO.caption = lblEstrorg.caption
    
    If Right(node.Image, 1) = "A" Or Right(node.Image, 1) = "D" Then
        SSTabPresupuestos.TabVisible(1) = True
    Else
        SSTabPresupuestos.TabVisible(1) = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "ConfigurarInterfazSeguridadUON", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Private Sub tvwestrorg_Collapse(ByVal node As MSComctlLib.node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfazSeguridadUON node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "tvwestrorg_Collapse", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub tvwestrorg_NodeClick(ByVal node As MSComctlLib.node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfazSeguridadUON node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "tvwestrorg_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub tvwEstrPres_NodeClick(ByVal node As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfazPresup node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "tvwEstrPres_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub ConfigurarInterfazPresup(ByVal node As MSComctlLib.node)
Dim dPorcen As Double
Dim dPorcentajeHermano As Double
Dim nodoHermano As MSComctlLib.node
Dim dSumaPorcenHijos As Double
Dim sCod As String

    'Configuramos seg�n nivel de de distribuci�n m�nimo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_iTipoPres
    
        Case 1
            Select Case gParametrosGenerales.giNIVPP
                Case 2
                    If Mid(node.Image, 5, 1) = "1" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 3
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 4
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Or Mid(node.Image, 5, 1) = "3" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case Else
                    picPorcen.Enabled = True
            End Select
    
        Case 2
            Select Case gParametrosGenerales.giNIVPC
                Case 2
                    If Mid(node.Image, 5, 1) = "1" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 3
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 4
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Or Mid(node.Image, 5, 1) = "3" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case Else
                    picPorcen.Enabled = True
            End Select
    
        Case 3
            Select Case gParametrosGenerales.giNIVPres3
                Case 2
                    If Mid(node.Image, 5, 1) = "1" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 3
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 4
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Or Mid(node.Image, 5, 1) = "3" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case Else
                    picPorcen.Enabled = True
            End Select
    
        Case 4
            Select Case gParametrosGenerales.giNIVPres4
                Case 2
                    If Mid(node.Image, 5, 1) = "1" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 3
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case 4
                    If Mid(node.Image, 5, 1) = "1" Or Mid(node.Image, 5, 1) = "2" Or Mid(node.Image, 5, 1) = "3" Then
                        picPorcen.Enabled = False
                        Exit Sub
                    Else
                        picPorcen.Enabled = True
                    End If
                Case Else
                    picPorcen.Enabled = True
            End Select
    
    End Select
    
    MostrarDatosBarraInf
    
    m_stexto = node.Text
    Slider1.Max = 100
    If node.Image = "PRESA" Then
        dPorcen = DevolverPorcentaje(node)
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = CDec(dPorcen * 100)
        m_bRespetarPorcen = True
        If dPorcen < 32767 Then
            Slider1.Value = Int(CDec(dPorcen * 100))
        End If
        
       If Slider1.Value = 0 Then
            If Int(100 - CDec(m_dblPorcenAsig * 100)) > 1 Then
                Slider1.Max = Int(100 - CDec(m_dblPorcenAsig * 100))
            Else
                Slider1.Max = 100
            End If
        Else
            Slider1.Max = Slider1.Value + Int(100 - CDec(m_dblPorcenAsig * 100))
        End If
        m_bRespetarPorcen = False
    Else
    
        If m_bRestringirMultiplePres And node.Image <> "Raiz" And val(txtPorcenPend.Text) <> 0 And val(txtPorcenPend.Text) <> 100 Then
            oMensajes.ImposibleImputarMultiplesPresupuestos (msPresupPlural)
            picPorcen.Enabled = False
            Exit Sub
        End If
        
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        
        If Int(100 - CDec(m_dblPorcenAsig * 100)) > 1 Then
            Slider1.Max = Int(100 - CDec(m_dblPorcenAsig * 100))
        Else
            Slider1.Max = 100
        End If

        m_bRespetarPorcen = False
    End If
    
    If picPorcen.Enabled And node.Image <> "PRESA" Then
        Dim scod1 As String
        Dim scod2 As String
        Dim scod3 As String
        Dim scod4 As String
        Dim oPres1Niv4 As CPresProyNivel4
        Dim oPres2Niv4 As CPresconNivel4
        Dim oPres3Niv4 As CPresConcep3Nivel4
        Dim oPres4Niv4 As CPresConcep4Nivel4
        Dim nodo As MSComctlLib.node
        
        If CDec(m_dblPorcenAsig) < 1 And SusPadresNoTienenAsignaciones(node) Then
            dSumaPorcenHijos = SumaDeLosHijos(node)
            If dSumaPorcenHijos > 0 Then
                'Si los hijos tienen porcentajes hay que poner esa suma
                LimpiarHijos node
                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
            Else
                Slider1.Value = Slider1.Max
            End If
        Else
            Select Case g_iTipoPres
            
                Case 1
                    If frmItemsWizard.g_oPresupuestos1Nivel4.Count < 2 Then
                        If frmItemsWizard.g_oPresupuestos1Nivel4.Count = 0 Then
                            Slider1.Value = Slider1.Max
                        Else
                        'Tenemos que quitar el presupuesto al que lo lleva ahora para asignarselo al nuevo
                                
                            For Each oPres1Niv4 In frmItemsWizard.g_oPresupuestos1Nivel4
                                If oPres1Niv4.Anyo = sdbcAnyo.Value And NullToStr(oPres1Niv4.UON1) = m_sUON1 And NullToStr(oPres1Niv4.UON2) = m_sUON2 And NullToStr(oPres1Niv4.UON3) = m_sUON3 Then
                                    Select Case oPres1Niv4.IDNivel
                                        Case 1
                                            scod1 = oPres1Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres1Niv4.CodPRES1))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES1" & scod1)
                                            nodo.Image = "PRES1"
                                        Case 2
                                            scod1 = oPres1Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres1Niv4.CodPRES1))
                                            scod2 = oPres1Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPres1Niv4.CodPRES2))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES2" & scod1 & scod2)
                                            nodo.Image = "PRES2"
                                        Case 3
                                            scod1 = oPres1Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres1Niv4.CodPRES1))
                                            scod2 = oPres1Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPres1Niv4.CodPRES2))
                                            scod3 = oPres1Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPres1Niv4.CodPRES3))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                                            nodo.Image = "PRES3"
                                        Case 4
                                            scod1 = oPres1Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres1Niv4.CodPRES1))
                                            scod2 = oPres1Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(oPres1Niv4.CodPRES2))
                                            scod3 = oPres1Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(oPres1Niv4.CodPRES3))
                                            scod4 = oPres1Niv4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(oPres1Niv4.Cod))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                                            nodo.Image = "PRES4"
                                    End Select
                                    dPorcentajeHermano = DevolverPorcentaje(nodo)
                                    nodo.Text = QuitarPorcentaje(nodo.Text)
                                    AsignarPres 0, nodo
                                    txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                                Else
                                    'hay uno al 100% pero en otro a�o.
                                    'Solo se cambia si el usuario tiene permisos
                                    sCod = EsPresupDelUsuario(oPres1Niv4)
                                    If sCod <> "" Then
                                        m_dblPorcenAsig = m_dblPorcenAsig - oPres1Niv4.Porcen
                                        Set frmItemsWizard.g_oPresupuestos1Nivel4 = oFSGSRaiz.Generar_CPresProyectosNivel4
                                        Set m_oPresupuestos1Otros = oFSGSRaiz.Generar_CPresProyectosNivel4
                                        ConfigurarEtiquetas 1
                                        Slider1.Max = 100
                                        Slider1.Value = Slider1.Max
                                    End If
                                    
                                End If
                                
                            Next
                        
                        End If
                    Else
                        'Hay m�s de un presupuesto asignado. Si el nodo solo tiene un hermano asignado, o un padre entonces hay que poner ese valor en el nuevo nodo pulsado
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            ' Si tiene asignaciones m�s abajo hay que poner la suma de esos
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                dPorcentajeHermano = DevolverPorcentaje(nodoHermano)
                                nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                                nodoHermano.Image = "PRES" & Mid(nodoHermano.Tag, 5, 1)
                                AsignarPres 0, nodoHermano
                                txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                            End If
                        Else
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                Slider1.Max = 100
                            End If
                        End If
                    
                    End If
            
                Case 2
                    If frmItemsWizard.g_oPresupuestos2Nivel4.Count < 2 Then
                        If frmItemsWizard.g_oPresupuestos2Nivel4.Count = 0 Then
                            Slider1.Value = Slider1.Max
                        Else
                        'Tenemos que quitar el presupuesto al que lo lleva ahora para asignarselo al nuevo
                            For Each oPres2Niv4 In frmItemsWizard.g_oPresupuestos2Nivel4
                                If oPres2Niv4.Anyo = sdbcAnyo.Value And NullToStr(oPres2Niv4.UON1) = m_sUON1 And NullToStr(oPres2Niv4.UON2) = m_sUON2 And NullToStr(oPres2Niv4.UON3) = m_sUON3 Then
                                    Select Case oPres2Niv4.IDNivel
                                        Case 1
                                            scod1 = oPres2Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(oPres2Niv4.CodPRES1))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES1" & scod1)
                                            nodo.Image = "PRES1"
                                        Case 2
                                            scod1 = oPres2Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPres2Niv4.CodPRES1))
                                            scod2 = oPres2Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPres2Niv4.CodPRES2))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES2" & scod1 & scod2)
                                            nodo.Image = "PRES2"
                                        
                                        Case 3
                                            scod1 = oPres2Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPres2Niv4.CodPRES1))
                                            scod2 = oPres2Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPres2Niv4.CodPRES2))
                                            scod3 = oPres2Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPres2Niv4.CodPRES3))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                                            nodo.Image = "PRES3"
                                        
                                        Case 4
                                            scod1 = oPres2Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oPres2Niv4.CodPRES1))
                                            scod2 = oPres2Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPres2Niv4.CodPRES2))
                                            scod3 = oPres2Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPres2Niv4.CodPRES3))
                                            scod4 = oPres2Niv4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(oPres2Niv4.Cod))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                                            nodo.Image = "PRES4"
                                            
                                    End Select
                                    dPorcentajeHermano = DevolverPorcentaje(nodo)
                                    nodo.Text = QuitarPorcentaje(nodo.Text)
                                    AsignarPres 0, nodo
                                    txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                               
                                Else
                                    'hay uno al 100% pero en otro a�o.... que liada.
                                    sCod = EsPresupDelUsuario(oPres2Niv4)
                                    If sCod <> "" Then
                                        m_dblPorcenAsig = m_dblPorcenAsig - oPres2Niv4.Porcen
                                        Set frmItemsWizard.g_oPresupuestos2Nivel4 = oFSGSRaiz.Generar_CPresContablesNivel4
                                        Set m_oPresupuestos2Otros = oFSGSRaiz.Generar_CPresContablesNivel4
                                        ConfigurarEtiquetas 1
                                        Slider1.Max = 100
                                        Slider1.Value = Slider1.Max
                                    End If
                                End If
                               
                            Next
                        End If
                    Else
                    'Hay m�s de un presupuesto asignado. Si el nodo solo tiene un hermano asignado, o un padre entonces hay que poner ese valor en el nuevo nodo pulsado
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            ' Si tiene asignaciones m�s abajo hay que poner la suma de esos
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                dPorcentajeHermano = DevolverPorcentaje(nodoHermano)
                                nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                                nodoHermano.Image = "PRES" & Mid(nodoHermano.Tag, 5, 1)
                                AsignarPres 0, nodoHermano
                                txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                            End If
                        Else
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                Slider1.Max = 100
                            End If
                        End If
                    
                    End If
            
                Case 3
                    If frmItemsWizard.g_oPresupuestos3Nivel4.Count < 2 Then
                        If frmItemsWizard.g_oPresupuestos3Nivel4.Count = 0 Then
                            Slider1.Value = Slider1.Max
                        Else
                        'Aqu� es cuando la liamos... tenemos que quitar el presupuesto al que lo lleva ahora para asignarselo al nuevo
                            For Each oPres3Niv4 In frmItemsWizard.g_oPresupuestos3Nivel4
                                If NullToStr(oPres3Niv4.UON1) = m_sUON1 And NullToStr(oPres3Niv4.UON2) = m_sUON2 And NullToStr(oPres3Niv4.UON3) = m_sUON3 Then
                                    Select Case oPres3Niv4.IDNivel
                                        Case 1
                                            scod1 = oPres3Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPres3Niv4.CodPRES1))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES1" & scod1)
                                            nodo.Image = "PRES1"
                                        Case 2
                                            scod1 = oPres3Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPres3Niv4.CodPRES1))
                                            scod2 = oPres3Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPres3Niv4.CodPRES2))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES2" & scod1 & scod2)
                                            nodo.Image = "PRES2"
                                        Case 3
                                            scod1 = oPres3Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPres3Niv4.CodPRES1))
                                            scod2 = oPres3Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPres3Niv4.CodPRES2))
                                            scod3 = oPres3Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPres3Niv4.CodPRES3))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                                            nodo.Image = "PRES3"
                                        Case 4
                                            scod1 = oPres3Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31 - Len(oPres3Niv4.CodPRES1))
                                            scod2 = oPres3Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32 - Len(oPres3Niv4.CodPRES2))
                                            scod3 = oPres3Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33 - Len(oPres3Niv4.CodPRES3))
                                            scod4 = oPres3Niv4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34 - Len(oPres3Niv4.Cod))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                                            nodo.Image = "PRES4"
                                    End Select
                                    dPorcentajeHermano = DevolverPorcentaje(nodo)
                                    nodo.Text = QuitarPorcentaje(nodo.Text)
                                    AsignarPres 0, nodo
                                    txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                               
                                Else
                                    ''hay uno pero en otra UO, si no tiene permiso no le dejamos
                                    sCod = EsPresupDelUsuario(oPres3Niv4)
                                    If sCod <> "" Then
                                        m_dblPorcenAsig = m_dblPorcenAsig - oPres3Niv4.Porcen
                                        Set frmItemsWizard.g_oPresupuestos3Nivel4 = oFSGSRaiz.Generar_CPresConceptos3Nivel4
                                        Set m_oPresupuestos3Otros = oFSGSRaiz.Generar_CPresConceptos3Nivel4
                                        ConfigurarEtiquetas 1
                                        Slider1.Max = 100
                                        Slider1.Value = Slider1.Max
                                    End If
                                End If
                            Next
                        End If
                    Else
                    'Hay m�s de un presupuesto asignado. Si el nodo solo tiene un hermano asignado, o un padre entonces hay que poner ese valor en el nuevo nodo pulsado
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            ' Si tiene asignaciones m�s abajo hay que poner la suma de esos
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                dPorcentajeHermano = DevolverPorcentaje(nodoHermano)
                                nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                                nodoHermano.Image = "PRES" & Mid(nodoHermano.Tag, 5, 1)
                                AsignarPres 0, nodoHermano
                                txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                            End If
                        Else
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                Slider1.Max = 100
                            End If
                        End If
                    
                    End If
                Case 4
                    If frmItemsWizard.g_oPresupuestos4Nivel4.Count < 2 Then
                        If frmItemsWizard.g_oPresupuestos4Nivel4.Count = 0 Then
                            Slider1.Value = Slider1.Max
                        Else
                        'Aqu� es cuando la liamos... tenemos que quitar el presupuesto al que lo lleva ahora para asignarselo al nuevo
                            For Each oPres4Niv4 In frmItemsWizard.g_oPresupuestos4Nivel4
                                If NullToStr(oPres4Niv4.UON1) = m_sUON1 And NullToStr(oPres4Niv4.UON2) = m_sUON2 And NullToStr(oPres4Niv4.UON3) = m_sUON3 Then
                                    Select Case oPres4Niv4.IDNivel
                                        Case 1
                                            scod1 = oPres4Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPres4Niv4.CodPRES1))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES1" & scod1)
                                            nodo.Image = "PRES1"
                                        Case 2
                                            scod1 = oPres4Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPres4Niv4.CodPRES1))
                                            scod2 = oPres4Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPres4Niv4.CodPRES2))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES2" & scod1 & scod2)
                                            nodo.Image = "PRES2"
                                        Case 3
                                            scod1 = oPres4Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPres4Niv4.CodPRES1))
                                            scod2 = oPres4Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPres4Niv4.CodPRES2))
                                            scod3 = oPres4Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPres4Niv4.CodPRES3))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES3" & scod1 & scod2 & scod3)
                                            nodo.Image = "PRES3"
                                        Case 4
                                            scod1 = oPres4Niv4.CodPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41 - Len(oPres4Niv4.CodPRES1))
                                            scod2 = oPres4Niv4.CodPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42 - Len(oPres4Niv4.CodPRES2))
                                            scod3 = oPres4Niv4.CodPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43 - Len(oPres4Niv4.CodPRES3))
                                            scod4 = oPres4Niv4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44 - Len(oPres4Niv4.Cod))
                                            Set nodo = tvwestrPres.Nodes.Item("PRES4" & scod1 & scod2 & scod3 & scod4)
                                            nodo.Image = "PRES4"
                                    End Select
                                    dPorcentajeHermano = DevolverPorcentaje(nodo)
                                    nodo.Text = QuitarPorcentaje(nodo.Text)
                                    AsignarPres 0, nodo
                                    txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                               
                                Else
                                    'hay uno al 100% pero en otro a�o
                                    sCod = EsPresupDelUsuario(oPres4Niv4)
                                    If sCod <> "" Then
                                        m_dblPorcenAsig = m_dblPorcenAsig - oPres4Niv4.Porcen
                                        Set frmItemsWizard.g_oPresupuestos4Nivel4 = oFSGSRaiz.Generar_CPresConceptos4Nivel4
                                        Set m_oPresupuestos4Otros = oFSGSRaiz.Generar_CPresConceptos4Nivel4
                                        ConfigurarEtiquetas 1
                                        Slider1.Max = 100
                                        Slider1.Value = Slider1.Max
                                    End If
                                End If
                               
                            Next
                        End If
                    Else
                    'Hay m�s de un presupuesto asignado. Si el nodo solo tiene un hermano asignado, o un padre entonces hay que poner ese valor en el nuevo nodo pulsado
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            ' Si tiene asignaciones m�s abajo hay que poner la suma de esos
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                dPorcentajeHermano = DevolverPorcentaje(nodoHermano)
                                nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                                nodoHermano.Image = "PRES" & Mid(nodoHermano.Tag, 5, 1)
                                AsignarPres 0, nodoHermano
                                txtPorcenAsignar.Text = CDec(dPorcentajeHermano * 100)
                            End If
                        Else
                            dSumaPorcenHijos = SumaDeLosHijos(node)
                            If dSumaPorcenHijos > 0 Then
                                'Si los hijos tienen porcentajes hay que poner esa suma
                                LimpiarHijos node
                                txtPorcenAsignar.Text = CDec(dSumaPorcenHijos * 100)
                            Else
                                Slider1.Max = 100
                            End If
                        End If
                    
                    End If
            End Select
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "ConfigurarInterfazPresup", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub
Private Sub txtPorcenAsignar_Change()
Dim nodx As MSComctlLib.node
Dim dPorcen As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwestrPres.selectedItem
    If nodx Is Nothing Then
        txtPorcenAsignar.Text = ""
        Exit Sub
    End If
    
    If nodx.Parent Is Nothing Then
        txtPorcenAsignar.Text = ""
        Exit Sub
    End If
    
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If

    dPorcen = DevolverPorcentaje(nodx)
    
    If Trim(txtPorcenAsignar.Text) = "" Then
        If dPorcen <> 0 Then
            m_bRespetarPorcen = True
            Slider1.Value = 0
            m_stexto = QuitarPorcentaje(m_stexto)
            tvwestrPres.selectedItem.Text = m_stexto
            tvwestrPres.selectedItem.Image = Left(tvwestrPres.selectedItem.Tag, 5)
            m_bRespetarPorcen = False
            
            AsignarPres 0, tvwestrPres.selectedItem
            
            RefrescarTotales
        End If
        Exit Sub
    End If

    If Not IsNumeric(txtPorcenAsignar.Text) Then
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = Left(txtPorcenAsignar.Text, Len(txtPorcenAsignar.Text) - 1)
        m_bRespetarPorcen = False
        Exit Sub
    End If

    m_stexto = nodx.Text
    If Mid(nodx.Image, 5, 1) = "A" Then
        m_stexto = QuitarPorcentaje(m_stexto)
    End If

    If txtPorcenAsignar.Text <= 0 Then
        m_bRespetarPorcen = True
        txtPorcenAsignar.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        tvwestrPres.selectedItem.Text = m_stexto
        tvwestrPres.selectedItem.Image = Left(tvwestrPres.selectedItem.Tag, 5)
        
        AsignarPres 0, tvwestrPres.selectedItem
        
        RefrescarTotales
    Else
        If txtPorcenAsignar.Text <> CDec(dPorcen * 100) Then
            m_bRespetarPorcen = True
            If txtPorcenAsignar.Text > 100 Then
                While txtPorcenAsignar.Text > 100
                    m_bRespetarPorcen = True
                    txtPorcenAsignar.Text = Left(txtPorcenAsignar.Text, Len(txtPorcenAsignar.Text) - 1)
                Wend
            Else
                If txtPorcenAsignar.Text < 32767 Then
                    Slider1.Value = Int(txtPorcenAsignar.Text)
                End If
            End If
            m_bRespetarPorcen = False
    
            tvwestrPres.selectedItem.Text = m_stexto & " (" & txtPorcenAsignar.Text & "%" & ")"
            tvwestrPres.selectedItem.Image = "PRESA"
    
            AsignarPres CDec(txtPorcenAsignar.Text / 100), tvwestrPres.selectedItem
            
            RefrescarTotales
            
            If CDbl(txtPorcenAsig.Text) > 100 Then
                txtPorcenAsignar.Text = Left(txtPorcenAsignar.Text, Len(txtPorcenAsignar.Text) - 1)
            End If
            
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "txtPorcenAsignar_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub AsignarPres(ByVal dblPorcen As Double, ByVal nodx As MSComctlLib.node)
'*******************************************************************************************
'M�todo que a�ade, modifica o elimina de la coleccion de presupuestos la selecci�n actual.
'*******************************************************************************************

Dim lIdNodo As Long
Dim iNivel As Integer
Dim sPres1 As String
Dim sPRES2 As String
Dim sPRES3 As String
Dim sPRES4 As String
Dim sDen As String
Dim oPres1Niv4 As CPresProyNivel4
Dim oPres2Niv4 As CPresconNivel4
Dim oPres3Niv4 As CPresConcep3Nivel4
Dim oPres4Niv4 As CPresConcep4Nivel4
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nodx Is Nothing Then Exit Sub
    
    lIdNodo = DevolverId(nodx)

    sCod = DevolverCodigoColeccion(nodx)
    
    Select Case Left(nodx.Tag, 5)
        Case "PRES1"
                    sPres1 = DevolverCod(nodx)
                    sPRES2 = ""
                    sPRES3 = ""
                    sPRES4 = ""
                    iNivel = 1
        Case "PRES2"
                    sPres1 = DevolverCod(nodx.Parent)
                    sPRES2 = DevolverCod(nodx)
                    sPRES3 = ""
                    sPRES4 = ""
                    iNivel = 2
        Case "PRES3"
                    sPres1 = DevolverCod(nodx.Parent.Parent)
                    sPRES2 = DevolverCod(nodx.Parent)
                    sPRES3 = DevolverCod(nodx)
                    sPRES4 = ""
                    iNivel = 3
        Case "PRES4"
                    sPres1 = DevolverCod(nodx.Parent.Parent.Parent)
                    sPRES2 = DevolverCod(nodx.Parent.Parent)
                    sPRES3 = DevolverCod(nodx.Parent)
                    sPRES4 = DevolverCod(nodx)
                    iNivel = 4
    End Select
    
    Select Case g_iTipoPres
        Case 1
                Set oPres1Niv4 = frmItemsWizard.g_oPresupuestos1Nivel4.Item(sCod)
                If oPres1Niv4 Is Nothing Then
                    'No existe
                    If dblPorcen <> 0 Then
                        sDen = DevolverDen(nodx)
                        frmItemsWizard.g_oPresupuestos1Nivel4.Add sdbcAnyo.Value, sPres1, sPRES2, sPRES3, sPRES4, sDen, , , , StrToNull(m_sUON1), StrToNull(m_sUON2), StrToNull(m_sUON3), lIdNodo, iNivel, dblPorcen, True
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                Else
                    'Existe
                    m_dblPorcenAsig = m_dblPorcenAsig - oPres1Niv4.Porcen
                    If dblPorcen = 0 Then
                        frmItemsWizard.g_oPresupuestos1Nivel4.Remove sCod
                    Else
                        oPres1Niv4.Porcen = dblPorcen
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                End If
                
        Case 2
                Set oPres2Niv4 = frmItemsWizard.g_oPresupuestos2Nivel4.Item(sCod)
                If oPres2Niv4 Is Nothing Then
                    'No existe
                    If dblPorcen <> 0 Then
                        sDen = DevolverDen(nodx)
                        frmItemsWizard.g_oPresupuestos2Nivel4.Add sdbcAnyo.Value, sPres1, sPRES2, sPRES3, sPRES4, sDen, , , , StrToNull(m_sUON1), StrToNull(m_sUON2), StrToNull(m_sUON3), lIdNodo, iNivel, dblPorcen, True
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                Else
                    'Existe
                    m_dblPorcenAsig = m_dblPorcenAsig - oPres2Niv4.Porcen
                    If dblPorcen = 0 Then
                        frmItemsWizard.g_oPresupuestos2Nivel4.Remove sCod
                    Else
                        oPres2Niv4.Porcen = dblPorcen
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                End If
        Case 3
                Set oPres3Niv4 = frmItemsWizard.g_oPresupuestos3Nivel4.Item(sCod)
                If oPres3Niv4 Is Nothing Then
                    'No existe
                    If dblPorcen <> 0 Then
                        sDen = DevolverDen(nodx)
                        frmItemsWizard.g_oPresupuestos3Nivel4.Add sPres1, sPRES2, sPRES3, sPRES4, sDen, , , , StrToNull(m_sUON1), StrToNull(m_sUON2), StrToNull(m_sUON3), lIdNodo, iNivel, dblPorcen, True
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                Else
                    'Existe
                    m_dblPorcenAsig = m_dblPorcenAsig - oPres3Niv4.Porcen
                    If dblPorcen = 0 Then
                        frmItemsWizard.g_oPresupuestos3Nivel4.Remove sCod
                    Else
                        oPres3Niv4.Porcen = dblPorcen
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                End If
        Case 4
                
                Set oPres4Niv4 = frmItemsWizard.g_oPresupuestos4Nivel4.Item(sCod)
                If oPres4Niv4 Is Nothing Then
                    'No existe
                    If dblPorcen <> 0 Then
                        sDen = DevolverDen(nodx)
                        frmItemsWizard.g_oPresupuestos4Nivel4.Add sPres1, sPRES2, sPRES3, sPRES4, sDen, , , , StrToNull(m_sUON1), StrToNull(m_sUON2), StrToNull(m_sUON3), lIdNodo, iNivel, dblPorcen, True
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                Else
                    'Existe
                    m_dblPorcenAsig = m_dblPorcenAsig - oPres4Niv4.Porcen
                    If dblPorcen = 0 Then
                        frmItemsWizard.g_oPresupuestos4Nivel4.Remove sCod
                    Else
                        oPres4Niv4.Porcen = dblPorcen
                        m_dblPorcenAsig = m_dblPorcenAsig + dblPorcen
                    End If
                End If

    End Select

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "AsignarPres", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Function DevolverCodigoColeccion(nodx As MSComctlLib.node) As String
Dim sCod As String
Dim sPres1 As String
Dim sPRES2 As String
Dim sPRES3 As String
Dim sPRES4 As String
Dim sTag As String
Dim iLongP1 As Integer
Dim iLongP2 As Integer
Dim iLongP3 As Integer
Dim iLongP4 As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbcAnyo.Visible Then
    sCod = sdbcAnyo.Text & "%UON0-%$"
Else
    sCod = "%UON0-%$"
End If
    
If m_sUON1 <> "" Then
    sCod = sCod & "%UON1-%$" & m_sUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(m_sUON1))
    If m_sUON2 <> "" Then
        sCod = sCod & "%UON2-%$" & m_sUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(m_sUON2))
        If m_sUON3 <> "" Then
            sCod = sCod & "%UON3-%$" & m_sUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(m_sUON3))
        End If
    End If
End If
    
Select Case Left(nodx.Tag, 5)
Case "PRES1"
    sPres1 = Left(nodx.Tag, InStr(1, nodx.Tag, "-%$") - 1)
    sPres1 = Right(sPres1, Len(sPres1) - 5)
    sPRES2 = ""
    sPRES3 = ""
    sPRES4 = ""
Case "PRES2"
    sTag = nodx.Parent.Tag
    sPres1 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPres1 = Right(sPres1, Len(sPres1) - 5)
    sPRES2 = Left(nodx.Tag, InStr(1, nodx.Tag, "-%$") - 1)
    sPRES2 = Right(sPRES2, Len(sPRES2) - 5)
Case "PRES3"
    sTag = nodx.Parent.Parent.Tag
    sPres1 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPres1 = Right(sPres1, Len(sPres1) - 5)
    sTag = nodx.Parent.Tag
    sPRES2 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPRES2 = Right(sPRES2, Len(sPRES2) - 5)
    sPRES3 = Left(nodx.Tag, InStr(1, nodx.Tag, "-%$") - 1)
    sPRES3 = Right(sPRES3, Len(sPRES3) - 5)
Case "PRES4"
    sTag = nodx.Parent.Parent.Parent.Tag
    sPres1 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPres1 = Right(sPres1, Len(sPres1) - 5)
    sTag = nodx.Parent.Parent.Tag
    sPRES2 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPRES2 = Right(sPRES2, Len(sPRES2) - 5)
    sTag = nodx.Parent.Tag
    sPRES3 = Left(sTag, InStr(1, sTag, "-%$") - 1)
    sPRES3 = Right(sPRES3, Len(sPRES3) - 5)
    sPRES4 = Left(nodx.Tag, InStr(1, nodx.Tag, "-%$") - 1)
    sPRES4 = Right(sPRES4, Len(sPRES4) - 5)
End Select
Select Case g_iTipoPres
Case 1
    iLongP1 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1
    iLongP2 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2
    iLongP3 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3
    iLongP4 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4
Case 2
    iLongP1 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON1
    iLongP2 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON2
    iLongP3 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON3
    iLongP4 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON4
Case 3
    iLongP1 = basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP31
    iLongP2 = basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP32
    iLongP3 = basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP33
    iLongP4 = basParametros.gLongitudesDeCodigos.giLongCodPRESCONCEP34
Case 4
    iLongP1 = basParametros.gLongitudesDeCodigos.giLongCodPRESConcep41
    iLongP2 = basParametros.gLongitudesDeCodigos.giLongCodPRESConcep42
    iLongP3 = basParametros.gLongitudesDeCodigos.giLongCodPRESConcep43
    iLongP4 = basParametros.gLongitudesDeCodigos.giLongCodPRESConcep44
End Select

sCod = sCod & "%PRES1-%$" & sPres1 & Mid$("                         ", 1, iLongP1 - Len(sPres1))
sCod = sCod & "%PRES2-%$" & sPRES2 & Mid$("                         ", 1, iLongP2 - Len(sPRES2))
sCod = sCod & "%PRES3-%$" & sPRES3 & Mid$("                         ", 1, iLongP3 - Len(sPRES3))
sCod = sCod & "%PRES4-%$" & sPRES4 & Mid$("                         ", 1, iLongP4 - Len(sPRES4))

DevolverCodigoColeccion = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "DevolverCodigoColeccion", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Sub RefrescarTotales()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtPorcenAsig.Text = Format(CDec(m_dblPorcenAsig * 100), "#,##0.00#")
    txtPorcenPend.Text = Format(100 - CDec(m_dblPorcenAsig * 100), "#,##0.00#")
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "RefrescarTotales", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Function SoloUnHermanoAsignado(ByVal node As MSComctlLib.node) As Object
Dim lCount As Long
Dim nod As MSComctlLib.node
Dim nodoHermano As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
lCount = 0

Set nodoHermano = Nothing

Set nod = node.FirstSibling
While Not nod Is Nothing
    If Mid(nod.Image, 5, 1) = "A" And nod.key <> node.key Then
        Set nodoHermano = Nothing
        Set nodoHermano = nod
        lCount = lCount + 1
    End If
    Set nod = nod.Next
Wend

'La modificamos para que compruebe lo mismo con el padre
If nodoHermano Is Nothing Then
    Set nod = node.Parent
    While Not nod Is Nothing
        If Mid(nod.Image, 5, 1) = "A" And nod.key <> node.key Then
            Set nodoHermano = Nothing
            Set nodoHermano = nod
        End If
        Set nod = nod.Parent
    Wend
End If

Set SoloUnHermanoAsignado = nodoHermano
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "SoloUnHermanoAsignado", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Function SumaDeLosHijos(ByVal node As MSComctlLib.node) As Double
Dim nodo As MSComctlLib.node
Dim dSuma As Double

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
dSuma = 0
If node Is Nothing Then Exit Function
Set nodo = Nothing
Set nodo = node.Child
While Not nodo Is Nothing
    If Mid(nodo.Image, 5, 1) = "A" Then
        dSuma = dSuma + DevolverPorcentaje(nodo)
    End If
    dSuma = dSuma + SumaDeLosHijos(nodo)
    Set nodo = nodo.Next
Wend

SumaDeLosHijos = dSuma
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "SumaDeLosHijos", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function
Private Sub LimpiarHijos(ByVal node As MSComctlLib.node)
Dim nodo As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If node Is Nothing Then Exit Sub
Set nodo = node.Child
While Not nodo Is Nothing
    If Mid(nodo.Image, 5, 1) = "A" Then
        nodo.Text = QuitarPorcentaje(nodo.Text)
        AsignarPres 0, nodo
        nodo.Image = "PRES" & Mid(nodo.Tag, 5, 1)
    End If
    LimpiarHijos nodo
    Set nodo = nodo.Next
Wend
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "LimpiarHijos", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub
Private Function SusPadresNoTienenAsignaciones(ByVal node As MSComctlLib.node) As Boolean
Dim b As Boolean
Dim nod As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
b = True
Set nod = node.Parent
If Not nod Is Nothing Then
    If Mid(nod.Image, 5, 1) = "A" Then
       b = False
    Else
       b = SusPadresNoTienenAsignaciones(nod)
    End If
End If

SusPadresNoTienenAsignaciones = b
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "SusPadresNoTienenAsignaciones", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function


Private Sub cmdOtras_Click()
Dim oPres As Object
Dim oPresupuestos As Object
Dim scod1 As String
Dim sDen As String
Dim i As Integer


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case g_iTipoPres
Case 1
    Set oPresupuestos = m_oPresupuestos1Otros
Case 2
    Set oPresupuestos = m_oPresupuestos2Otros
Case 3
    Set oPresupuestos = m_oPresupuestos3Otros
Case 4
    Set oPresupuestos = m_oPresupuestos4Otros
End Select

ReDim m_arTag(1 To 1)

cP.clear

i = 1
For Each oPres In oPresupuestos

    If NullToStr(oPres.UON3) <> "" Then
        scod1 = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        scod1 = scod1 & oPres.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPres.UON2))
        scod1 = scod1 & oPres.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPres.UON3))
        If m_bRuo Then
            If m_oUnidadesOrgN3 Is Nothing Then
                Set m_oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
            End If
            sDen = m_oUnidadesOrgN3.DevolverDenominacion(oPres.UON1, oPres.UON2, oPres.UON3)
        Else
            sDen = m_oUnidadesOrgN3.Item(scod1).Den
        End If
    ElseIf NullToStr(oPres.UON2) <> "" Then
        scod1 = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        scod1 = scod1 & oPres.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPres.UON2))
        If m_bRuo Then
            If m_oUnidadesOrgN2 Is Nothing Then
                Set m_oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
            End If
            sDen = m_oUnidadesOrgN2.DevolverDenominacion(oPres.UON1, oPres.UON2)
        Else
            sDen = m_oUnidadesOrgN2.Item(scod1).Den
        End If
    ElseIf NullToStr(oPres.UON1) <> "" Then
        scod1 = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        If m_bRuo Then
            If m_oUnidadesOrgN1 Is Nothing Then
                Set m_oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
            End If
            sDen = m_oUnidadesOrgN1.DevolverDenominacion(oPres.UON1)
        Else
            sDen = m_oUnidadesOrgN1.Item(scod1).Den
        End If
    Else
        sDen = gParametrosGenerales.gsDEN_UON0
    End If
    
    If g_iTipoPres < 3 Then
        sDen = sDen & " - " & oPres.Anyo & " - " & oPres.Den & " (" & oPres.Porcen * 100 & "%)"
    Else
        sDen = sDen & " - " & oPres.Den & " (" & oPres.Porcen * 100 & "%)"
    End If
    cP.AddItem sDen, , i, , , , , i
    m_arTag(i) = oPres.indice
    i = i + 1
    ReDim Preserve m_arTag(1 To i)
Next


If i > 1 Then
    cP.ShowPopupMenu cmdOtras.Left, cmdOtras.Top + 850
End If
Set oPresupuestos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "cmdOtras_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub
Private Sub BuscarUONUsuario()
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim sCod As String
Dim oPres As Object
Dim nodx As MSComctlLib.node
Dim iBase As Integer
Dim bBusca As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bBusca = True

    If Not NoHayParametro(basOptimizacion.gUON1Usuario) Then
        scod1 = basOptimizacion.gUON1Usuario & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(basOptimizacion.gUON1Usuario))
        iBase = 1
        If m_oUnidadesOrgN1.Item(scod1) Is Nothing Then bBusca = False
    End If
    If Not NoHayParametro(basOptimizacion.gUON2Usuario) And bBusca Then
        scod2 = scod1 & basOptimizacion.gUON2Usuario & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(basOptimizacion.gUON2Usuario))
        iBase = 2
        If m_oUnidadesOrgN2.Item(scod2) Is Nothing Then bBusca = False
    End If
    If Not NoHayParametro(basOptimizacion.gUON3Usuario) And bBusca Then
        scod3 = scod2 & basOptimizacion.gUON3Usuario & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(basOptimizacion.gUON3Usuario))
        iBase = 3
        If m_oUnidadesOrgN3.Item(scod3) Is Nothing Then bBusca = False
    End If

    If scod3 <> "" And bBusca Then
        If tvwestrorg.Nodes("UON3" & scod3).Image = "UON3A" Then
            tvwestrorg.Nodes("UON3" & scod3).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON3" & scod3)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
    End If
    If scod2 <> "" And bBusca Then
        If m_bRuoRama Or (Not m_bRuoRama And iBase <= 2) Then
        If tvwestrorg.Nodes("UON2" & scod2).Image = "UON2A" Then
            tvwestrorg.Nodes("UON2" & scod2).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON2" & scod2)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
        End If
    End If
    If scod1 <> "" And bBusca Then
        If m_bRuoRama Or (Not m_bRuoRama And iBase <= 1) Then
            If tvwestrorg.Nodes("UON1" & scod1).Image = "UON1A" Then
                tvwestrorg.Nodes("UON1" & scod1).Selected = True
                tvwestrorg_NodeClick tvwestrorg.Nodes("UON1" & scod1)
    '            If g_iTipoPres < 3 Then
    '                sdbcAnyo.Value = oPres.Anyo
    '            End If
                SSTabPresupuestos.Tab = 1
                SSTabPresupuestos_Click 0
                Exit Sub
            End If
        End If
    End If
    If tvwestrorg.Nodes("UON0").Image = "UON0A" Then
        tvwestrorg.Nodes("UON0").Selected = True
        tvwestrorg_NodeClick tvwestrorg.Nodes("UON0")
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
        SSTabPresupuestos.Tab = 1
        SSTabPresupuestos_Click 0
        Exit Sub
    End If
    'Si ni su UO ni sus padres est�n asignadas entonces muestra la que est� asinada al 100%
    Select Case g_iTipoPres
    Case 1
        If frmItemsWizard.g_oPresupuestos1Nivel4.Count = 1 Then
            Set oPres = frmItemsWizard.g_oPresupuestos1Nivel4.Item(1)
        End If
    Case 2
        If frmItemsWizard.g_oPresupuestos2Nivel4.Count = 1 Then
            Set oPres = frmItemsWizard.g_oPresupuestos2Nivel4.Item(1)
        End If
    Case 3
        If frmItemsWizard.g_oPresupuestos3Nivel4.Count = 1 Then
            Set oPres = frmItemsWizard.g_oPresupuestos3Nivel4.Item(1)
        End If
    Case 4
        If frmItemsWizard.g_oPresupuestos4Nivel4.Count = 1 Then
            Set oPres = frmItemsWizard.g_oPresupuestos4Nivel4.Item(1)
        End If
    End Select
    If Not oPres Is Nothing Then
        sCod = EsPresupDelUsuario(oPres)
        If sCod <> "" Then
            Set nodx = tvwestrorg.Nodes.Item(sCod)
            nodx.Selected = True
            tvwestrorg_NodeClick nodx
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Set oPres = Nothing
            Exit Sub
        End If
    End If
    Set oPres = Nothing
    'Si no est� asignado muestro la suya o su padre que tenga presupuestos
    If scod3 <> "" And bBusca Then
        If tvwestrorg.Nodes("UON3" & scod3).Image = "UON3D" Then
            tvwestrorg.Nodes("UON3" & scod3).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON3" & scod3)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
    End If
    If scod2 <> "" And bBusca Then
        If tvwestrorg.Nodes("UON2" & scod2).Image = "UON2D" Then
            tvwestrorg.Nodes("UON2" & scod2).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON2" & scod2)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
    End If
    If scod1 <> "" And bBusca Then
        If tvwestrorg.Nodes("UON1" & scod1).Image = "UON1D" Then
            tvwestrorg.Nodes("UON1" & scod1).Selected = True
            tvwestrorg_NodeClick tvwestrorg.Nodes("UON1" & scod1)
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
            SSTabPresupuestos.Tab = 1
            SSTabPresupuestos_Click 0
            Exit Sub
        End If
    End If
    If tvwestrorg.Nodes("UON0").Image = "UON0D" Then
        tvwestrorg.Nodes("UON0").Selected = True
        tvwestrorg_NodeClick tvwestrorg.Nodes("UON0")
'            If g_iTipoPres < 3 Then
'                sdbcAnyo.Value = oPres.Anyo
'            End If
        SSTabPresupuestos.Tab = 1
        SSTabPresupuestos_Click 0
        Exit Sub
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "BuscarUONUsuario", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Private Function EsPresupDelUsuario(ByVal oPres As Object) As String
Dim sCod As String
Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If NullToStr(oPres.UON3) <> "" Then
        sCod = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        sCod = sCod & oPres.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPres.UON2))
        sCod = sCod & oPres.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPres.UON3))
        If m_bRuo Then
            If m_oUnidadesOrgN3.Item(sCod) Is Nothing Then
                EsPresupDelUsuario = ""
                Exit Function
            Else
                Set nodx = tvwestrorg.Nodes.Item("UON3" & sCod)
                If Right(CStr(nodx.Image), 1) <> "A" Then
                    EsPresupDelUsuario = ""
                    Set nodx = Nothing
                    Exit Function
                End If
                Set nodx = Nothing
            End If
        End If
        EsPresupDelUsuario = "UON3" & sCod

    ElseIf NullToStr(oPres.UON2) <> "" Then
        sCod = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        sCod = sCod & oPres.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPres.UON2))
        If m_bRuo Then
            If m_oUnidadesOrgN2.Item(sCod) Is Nothing Then
                EsPresupDelUsuario = ""
                Exit Function
            Else
                Set nodx = tvwestrorg.Nodes.Item("UON2" & sCod)
                If Right(CStr(nodx.Image), 1) <> "A" Then
                    EsPresupDelUsuario = ""
                    Set nodx = Nothing
                    Exit Function
                End If
                Set nodx = Nothing
            End If
        End If
        EsPresupDelUsuario = "UON2" & sCod
    ElseIf NullToStr(oPres.UON1) <> "" Then
        sCod = oPres.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPres.UON1))
        If m_bRuo Then
            If m_oUnidadesOrgN1.Item(sCod) Is Nothing Then
                EsPresupDelUsuario = ""
                Exit Function
            Else
                Set nodx = tvwestrorg.Nodes.Item("UON1" & sCod)
                If Right(CStr(nodx.Image), 1) <> "A" Then
                    EsPresupDelUsuario = ""
                    Set nodx = Nothing
                    Exit Function
                End If
                Set nodx = Nothing
            End If
        End If
        EsPresupDelUsuario = "UON1" & sCod
    Else
        If m_bRuo And Not m_bRuoRama Then
            If NullToStr(basOptimizacion.gUON1Usuario) <> "" Then
                EsPresupDelUsuario = ""
                Exit Function
            End If
        End If
        EsPresupDelUsuario = "UON0"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "EsPresupDelUsuario", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Sub cP_Click(ItemNumber As Long)
Dim sTag As String
Dim oPres As Object
Dim sCod As String
Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sTag = CStr(m_arTag(ItemNumber))
    
    Select Case g_iTipoPres
    Case 1
        Set oPres = m_oPresupuestos1Otros.Item(sTag)
    Case 2
        Set oPres = m_oPresupuestos2Otros.Item(sTag)
    Case 3
        Set oPres = m_oPresupuestos3Otros.Item(sTag)
    Case 4
        Set oPres = m_oPresupuestos4Otros.Item(sTag)
    End Select
    
    sCod = EsPresupDelUsuario(oPres)
    If sCod = "" Then
        oMensajes.MensajeOKOnly 728
        Exit Sub
    Else
        Set nodx = tvwestrorg.Nodes.Item(sCod)
    End If
    
    nodx.Selected = True
    tvwestrorg_NodeClick nodx
    
    If g_iTipoPres < 3 Then
        sdbcAnyo.Value = oPres.Anyo
    End If
    SSTabPresupuestos_Click 0
    
    Set oPres = Nothing


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "cP_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Function DevolverAnyoAsignado() As Integer
Dim nodo As MSComctlLib.node
Dim oPres As Object

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set nodo = tvwestrorg.selectedItem

Select Case Left(nodo.Tag, 4)
    Case "UON3"
        m_sUON1 = DevolverCod(nodo.Parent.Parent)
        m_sUON2 = DevolverCod(nodo.Parent)
        m_sUON3 = DevolverCod(nodo)
    Case "UON2"
        m_sUON1 = DevolverCod(nodo.Parent)
        m_sUON2 = DevolverCod(nodo)
        m_sUON3 = ""
    Case "UON1"
        m_sUON1 = DevolverCod(nodo)
        m_sUON2 = ""
        m_sUON3 = ""
    Case "UON0"
        m_sUON1 = ""
        m_sUON2 = ""
        m_sUON3 = ""
End Select

Select Case g_iTipoPres
Case 1
    For Each oPres In frmItemsWizard.g_oPresupuestos1Nivel4
        If NullToStr(oPres.UON1) = m_sUON1 And NullToStr(oPres.UON2) = m_sUON2 And NullToStr(oPres.UON3) = m_sUON3 Then
            DevolverAnyoAsignado = oPres.Anyo
            Exit Function
        End If
    Next
Case 2
    For Each oPres In frmItemsWizard.g_oPresupuestos2Nivel4
        If NullToStr(oPres.UON1) = m_sUON1 And NullToStr(oPres.UON2) = m_sUON2 And NullToStr(oPres.UON3) = m_sUON3 Then
            DevolverAnyoAsignado = oPres.Anyo
            Exit Function
        End If
    Next
End Select
DevolverAnyoAsignado = sdbcAnyo.Value
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "DevolverAnyoAsignado", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Function ComprobarSiEstaAsignado(Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String) As Boolean
Dim oPres As Object
Dim oPresups As Object

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
ComprobarSiEstaAsignado = False
Select Case g_iTipoPres
Case 1
    If frmItemsWizard.g_oPresupuestos1Nivel4 Is Nothing Then Exit Function
    If frmItemsWizard.g_oPresupuestos1Nivel4.Count = 0 Then Exit Function
    Set oPresups = frmItemsWizard.g_oPresupuestos1Nivel4
Case 2
    If frmItemsWizard.g_oPresupuestos2Nivel4 Is Nothing Then Exit Function
    If frmItemsWizard.g_oPresupuestos2Nivel4.Count = 0 Then Exit Function
    Set oPresups = frmItemsWizard.g_oPresupuestos2Nivel4
Case 3
    If frmItemsWizard.g_oPresupuestos3Nivel4 Is Nothing Then Exit Function
    If frmItemsWizard.g_oPresupuestos3Nivel4.Count = 0 Then Exit Function
    Set oPresups = frmItemsWizard.g_oPresupuestos3Nivel4
Case 4
    If frmItemsWizard.g_oPresupuestos4Nivel4 Is Nothing Then Exit Function
    If frmItemsWizard.g_oPresupuestos4Nivel4.Count = 0 Then Exit Function
    Set oPresups = frmItemsWizard.g_oPresupuestos4Nivel4
End Select

For Each oPres In oPresups
    If sUON3 <> "" Then
        If sUON1 = oPres.UON1 And sUON2 = oPres.UON2 And sUON3 = oPres.UON3 Then
            If m_bRuo Then
                Select Case m_iUOBase
                Case 1
                    If sUON1 = basOptimizacion.gUON1Usuario Then
                        ComprobarSiEstaAsignado = True
                        Set oPresups = Nothing
                        Exit Function
                    End If
                Case 2
                    If sUON1 = basOptimizacion.gUON1Usuario And sUON2 = basOptimizacion.gUON2Usuario Then
                        ComprobarSiEstaAsignado = True
                        Set oPresups = Nothing
                        Exit Function
                    End If
                Case 3
                    If sUON1 = basOptimizacion.gUON1Usuario And sUON2 = basOptimizacion.gUON2Usuario And sUON3 = basOptimizacion.gUON3Usuario Then
                        ComprobarSiEstaAsignado = True
                        Set oPresups = Nothing
                        Exit Function
                    End If
                Case 0
                    ComprobarSiEstaAsignado = True
                    Set oPresups = Nothing
                    Exit Function
                End Select
            Else
                ComprobarSiEstaAsignado = True
                Set oPresups = Nothing
                Exit Function
            End If
        End If
    ElseIf sUON2 <> "" Then
        If oPres.UON1 = sUON1 And oPres.UON2 = sUON2 And IsNull(oPres.UON3) Then
            If m_bRuo Then
                Select Case m_iUOBase
                Case 1
                    If sUON1 = basOptimizacion.gUON1Usuario Then
                        ComprobarSiEstaAsignado = True
                        Set oPresups = Nothing
                        Exit Function
                    End If
                Case 2
                    If sUON1 = basOptimizacion.gUON1Usuario And sUON2 = basOptimizacion.gUON2Usuario Then
                        ComprobarSiEstaAsignado = True
                        Set oPresups = Nothing
                        Exit Function
                    End If
                Case 3
                    If m_bRuoRama Then
                        If sUON1 = basOptimizacion.gUON1Usuario And sUON2 = basOptimizacion.gUON2Usuario Then
                            ComprobarSiEstaAsignado = True
                            Set oPresups = Nothing
                            Exit Function
                        End If
                    End If
                Case 0
                    ComprobarSiEstaAsignado = True
                    Set oPresups = Nothing
                    Exit Function
                End Select
            Else
                ComprobarSiEstaAsignado = True
                Set oPresups = Nothing
                Exit Function
            End If
        End If
    ElseIf sUON1 <> "" Then
        If oPres.UON1 = sUON1 And IsNull(oPres.UON2) And IsNull(oPres.UON3) Then
            If m_bRuo Then
                Select Case m_iUOBase
                Case 1
                    If sUON1 = basOptimizacion.gUON1Usuario Then
                        ComprobarSiEstaAsignado = True
                        Set oPresups = Nothing
                        Exit Function
                    End If
                Case 2
                    If m_bRuoRama Then
                        If sUON1 = basOptimizacion.gUON1Usuario Then
                            ComprobarSiEstaAsignado = True
                            Set oPresups = Nothing
                            Exit Function
                        End If
                    End If
                Case 3
                    If m_bRuoRama Then
                        If sUON1 = basOptimizacion.gUON1Usuario Then
                            ComprobarSiEstaAsignado = True
                            Set oPresups = Nothing
                            Exit Function
                        End If
                    End If
                Case 0
                    ComprobarSiEstaAsignado = True
                    Set oPresups = Nothing
                    Exit Function
                End Select
            Else
                ComprobarSiEstaAsignado = True
                Set oPresups = Nothing
                Exit Function
            End If
        End If
    Else
        If IsNull(oPres.UON1) And IsNull(oPres.UON2) And IsNull(oPres.UON3) Then
            If m_bRuo Then
                If m_iUOBase = 0 Then
                    ComprobarSiEstaAsignado = True
                    Set oPresups = Nothing
                    Exit Function
                Else
                    If m_bRuoRama Then
                        ComprobarSiEstaAsignado = True
                        Set oPresups = Nothing
                        Exit Function
                    End If
                End If
            Else
                ComprobarSiEstaAsignado = True
                Set oPresups = Nothing
                Exit Function
            End If
        End If
    End If
Next

Set oPresups = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "ComprobarSiEstaAsignado", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Sub CrearStringArticulos()
Dim i As Integer
Dim oItem As CItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sCodArts = ""
    i = 0
    For Each oItem In frmItemsWizard.g_oItems
        i = i + 1
        sCodArts = sCodArts & "'" & Trim(oItem.ArticuloCod) & "'"
        If i < Me.Count Then
             sCodArts = sCodArts & ","
        End If
    Next
    sCodArts = Mid(sCodArts, 1, Len(sCodArts) - 1)
    Set oItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemsWizard4", "CrearStringArticulos", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub
