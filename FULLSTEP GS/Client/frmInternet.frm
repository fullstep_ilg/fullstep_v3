VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmInternet 
   Caption         =   "frmInternet"
   ClientHeight    =   4800
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6315
   Icon            =   "frmInternet.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4800
   ScaleWidth      =   6315
   Begin VB.Timer Timer1 
      Interval        =   2000
      Left            =   0
      Top             =   0
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6120
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmInternet.frx":014A
            Key             =   "contratos"
            Object.Tag             =   "contratos"
         EndProperty
      EndProperty
   End
   Begin SHDocVwCtl.WebBrowser webVisor 
      Height          =   4665
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Visible         =   0   'False
      Width           =   6195
      ExtentX         =   10927
      ExtentY         =   8229
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
End
Attribute VB_Name = "frmInternet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Public g_sNombre As String
Public g_sRuta As String
Public g_sOPCO As String
Public g_sFamilia As String
Public g_sArticulos As String
Public g_sProveedores As String
Public g_sProceso As String
Public g_sFormKey As String

Dim sURL As String
Dim lseg As Long
Public g_bVisor As Boolean
Public g_sFileLogin As String
Public g_sPathContrato As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
''' <summary>
''' Evento que salta cuando se activa el formulario y en el que pasaremos al visor la direcci�n a mostrar
''' </summary>
''' <remarks>Tiempo maximo 0 sec</remarks>
Private Sub Form_Activate()
       
    'Timer1.Enabled = False
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
     Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
    Screen.MousePointer = vbHourglass
    Me.caption = g_sNombre
    
    sURL = g_sRuta
    webVisor.Navigate2 sURL, 4 'que no coja de la cache
    If InStr(1, LCase(sURL), "contratos", vbTextCompare) <> 0 Then
        Dim Ador As Ador.Recordset
        Me.Icon = Me.ImageList1.ListImages("contratos").Picture
        Set Ador = oGestorIdiomas.DevolverTextosDelModulo(279, basPublic.gParametrosInstalacion.gIdioma)
        If Not Ador Is Nothing Then
            Me.caption = "" & Ador(0).Value       'Contratos
        End If
    End If
       
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmInternet", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmInternet", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Evento que Redimensiona el formulario
''' </summary>
''' <remarks>Tiempo maximo 0 sec</remarks>
Private Sub Form_Resize()
    'Redimensiona el formulario
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Width < 600 Then Exit Sub
    If Me.Height < 5000 Then Exit Sub
    
    'redimensiona el width del visor
    webVisor.Width = Me.Width - 200
    webVisor.Height = Me.Height - 600
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub


''' <summary>
''' Evento que se lanza cuando el control se dispone a navegar a una URL, en el aprovecharemos para mostrar un formulario de espera
''' </summary>
''' <remarks>Llamada desde el propio control webVisor</remarks>
Private Sub webVisor_BeforeNavigate2(ByVal pDisp As Object, URL As Variant, FLAGS As Variant, TargetFrameName As Variant, PostData As Variant, Headers As Variant, Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    lseg = 0
    frmADJCargar.Show
    frmADJCargar.Refresh
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmInternet", "webVisor_BeforeNavigate2", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Evento que se lanza cuando el estado de la carga de la pagn se dispone a navegar a una URL, en el aprovecharemos para mostrar un formulario de espera
''' </summary>
''' <remarks>Llamada desde el propio control webVisor</remarks>
Private Sub webVisor_ProgressChange(ByVal Progress As Long, ByVal ProgressMax As Long)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Progress = -1 Then
        Unload frmADJCargar
        Screen.MousePointer = vbNormal
    Else
        Dim oForm As Form
        For Each oForm In Forms
            If oForm.Name = "frmADJCargar" Then
                oForm.SetFocus
                webVisor.Visible = True
                Exit Sub
            End If
        Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmInternet", "webVisor_ProgressChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Timer1_Timer()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lseg = lseg + 1
    If lseg = 1200 Then
        Select Case g_sOrigen
        Case "frmAdj"
        Case Else
            Unload Me
        End Select
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmInternet", "Timer1_Timer", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim fso As New FileSystemObject
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Select Case g_sOrigen
        Case "frmCONTRWizard"
            If g_sFileLogin <> "" Then
              If fso.FileExists(g_sFileLogin) Then
                fso.DeleteFile g_sFileLogin, True
              End If
            End If
            If g_sPathContrato <> "" Then
                If fso.FolderExists(g_sPathContrato) Then
                    fso.DeleteFolder g_sPathContrato, True
                End If
            End If
        Case "frmCONFGEN"
            frmCONFGEN.CargarDatosPlantillas
        Case "frmADJ", "frmRESREU"
            If g_sOPCO = "1" Then
                frmPEDIDOS.SetFocus
            End If
            g_sOPCO = ""
    End Select
    
    Set fso = Nothing
    If Not g_FormulariosInternet Is Nothing Then
        If g_FormulariosInternet.Exists(g_sOrigen & "|" & g_sFormKey) Then
            Set g_FormulariosInternet.Item(g_sOrigen & "|" & g_sFormKey) = Nothing
            g_FormulariosInternet.Remove g_sOrigen & "|" & g_sFormKey
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmInternet", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

