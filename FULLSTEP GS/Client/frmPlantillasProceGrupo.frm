VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPlantillasProceGrupo 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DSeleccione los grupos"
   ClientHeight    =   2595
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPlantillasProceGrupo.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2595
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1125
      TabIndex        =   3
      Top             =   2240
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2265
      TabIndex        =   2
      Top             =   2240
      Width           =   1005
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "Ce&rrar"
      Height          =   315
      Left            =   1865
      TabIndex        =   1
      Top             =   2240
      Visible         =   0   'False
      Width           =   1005
   End
   Begin MSComctlLib.ListView lstGrupos 
      Height          =   2085
      Left            =   75
      TabIndex        =   0
      Top             =   75
      Width           =   4500
      _ExtentX        =   7938
      _ExtentY        =   3678
      View            =   2
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmPlantillasProceGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_lIndice As Long
Public g_bModoEdicion As Boolean

Private m_bCargando As Boolean
Private m_bCargarLista As Boolean

Private oGrupoAnteriores As CGrupos

Private m_bAceptar As Boolean
Private m_bCancelar As Boolean
Private m_bCerrar As Boolean



''' <summary>
''' Click del boton Aceptar
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento del sistema Tiempo m�ximo: 0,3</remarks>

Private Sub cmdAceptar_Click()
Dim oGrupo As CGrupo
Dim irespuesta As Integer
Dim bSalir As Boolean

    If frmPlantillasProce.sdbgPlantillas.col = "-1" Then Exit Sub
    
    bSalir = False
    
    Select Case g_lIndice
        Case 0 'DEST
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefDestino = True Then
                        bSalir = True
                        Exit For
                    End If
                Next

        Case 1 'PAG
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefFormaPago = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
        
        Case 2 'FECSUM
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefFechasSum = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                
        Case 3 'PROVE (no obligatorio)
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefProveActual = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
        
        Case 4 'ESP (no obligatorio)
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefEspecificaciones = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
        
        Case 5 'DIST UON (no obligatorio)
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefDistribUON = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                
        Case 6 'PRES ANU1 (no obligatorio)
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefPresAnualTipo1 = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                
        Case 7 'PRES ANU2 (no obligatorio)
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefPresAnualTipo2 = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
            
        Case 8 'PRES1 (no obligatorio)
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefPresTipo1 = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
                    
        Case 9 'PRES2 (no obligatorio)
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefPresTipo2 = True Then
                        bSalir = True
                        Exit For
                    End If
                Next
        
        '''Solicitud de compra
        Case 10:
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    If oGrupo.DefSolicitud = True Then
                        bSalir = True
                        Exit For
                    End If
                Next

    End Select
    
    Set oGrupo = Nothing
    
    If bSalir = True Then
        m_bAceptar = True
        Unload Me
    End If
    
End Sub

''' <summary>
''' Click del boton cancelar
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento del sistema Tiempo m�ximo: 0,31</remarks>

Private Sub cmdCancelar_Click()
    Dim oGrupo As CGrupo
    Dim bRestaurar As Boolean
    
    'Restaura la configuraci�n como estaba antes:
    
    bRestaurar = True
    Select Case g_lIndice
        Case 0 'DEST
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefDestino = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefDestino
                If oGrupo.DefDestino = True Then bRestaurar = False
            Next
        
        Case 1 ' PAGO
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefFormaPago = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefFormaPago
                If oGrupo.DefFormaPago = True Then bRestaurar = False
            Next
            
        Case 2 'FECHAS
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefFechasSum = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefFechasSum
                If oGrupo.DefFechasSum = True Then bRestaurar = False
            Next
            
        Case 3 'PROVE
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefProveActual = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefProveActual
                If oGrupo.DefProveActual = True Then bRestaurar = False
            Next
            
        Case 4 'ESP
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefEspecificaciones = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefEspecificaciones
            Next
            frmPlantillasProce.RestaurarConfig g_lIndice, True
            m_bCancelar = True
            Unload Me
            Exit Sub
            
        Case 5 'DIST UON
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefDistribUON = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefDistribUON
                If oGrupo.DefDistribUON = True Then bRestaurar = False
            Next
            
        Case 6 'PRES ANUAL 1
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefPresAnualTipo1 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresAnualTipo1
                If oGrupo.DefPresAnualTipo1 = True Then bRestaurar = False
            Next
            
        Case 7 'PRES ANUAL 2
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefPresAnualTipo2 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresAnualTipo2
                If oGrupo.DefPresAnualTipo2 = True Then bRestaurar = False
            Next
            
        Case 8 'PRES 1
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefPresTipo1 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresTipo1
                If oGrupo.DefPresTipo1 = True Then bRestaurar = False
            Next
            
        Case 9 'PRES 2
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefPresTipo2 = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresTipo2
                If oGrupo.DefPresTipo2 = True Then bRestaurar = False
            Next
            
        Case 10  'SOLICITUD DE COMPRAS
            For Each oGrupo In frmPlantillasProce.m_oGrupos
                oGrupo.DefSolicitud = oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefSolicitud
                If oGrupo.DefSolicitud = True Then bRestaurar = False
            Next
            
    End Select
            
    If bRestaurar = True Then
        frmPlantillasProce.RestaurarConfig g_lIndice
    End If
    
    m_bCancelar = True
    Unload Me
End Sub
''' <summary>
''' Click del boton cerrar
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento del sistema Tiempo m�ximo: 0,31</remarks>

Private Sub cmdCerrar_Click()
    m_bCerrar = True
    Unload Me
End Sub

Private Sub Form_Activate()
    'La carga hay que hacerla as�,porque sino el width de la columna del listview se pone por defecto muy peque�o,
    'y no se puede cambiar.Para que funcione debe rellenarse el listview cuando est� ya cargado el form.
    If m_bCargarLista = True Then
        DoEvents
        m_bCargando = True
        MostrarGrupos
        m_bCargando = False
        m_bCargarLista = False
    End If
End Sub

''' <summary>
''' Load del formulario
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento del sistema Tiempo m�ximo: 0,1</remarks>

Private Sub Form_Load()
    Dim oGrupo As CGrupo
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    m_bAceptar = False
    m_bCancelar = False
    m_bCerrar = False
    
    If g_bModoEdicion = True Then
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        cmdCerrar.Visible = False
        
        'Almacena en una clase los valores que tenemos al comienzo,por si se cancela:
        Set oGrupoAnteriores = oFSGSRaiz.Generar_CGrupos
    
        Select Case g_lIndice
            Case 0 'DEST
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefDestino = oGrupo.DefDestino
                Next
            
            Case 1 ' PAGO
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefFormaPago = oGrupo.DefFormaPago
                Next
                
            Case 2 'FECHAS
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefFechasSum = oGrupo.DefFechasSum
                Next
                
            Case 3 'PROVE
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefProveActual = oGrupo.DefProveActual
                Next
                
            Case 4 'ESP
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefEspecificaciones = oGrupo.DefEspecificaciones
                Next
            
            Case 5 'DIST UON
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefDistribUON = oGrupo.DefDistribUON
                Next
                
            Case 6 'PRES ANUAL 1
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresAnualTipo1 = oGrupo.DefPresAnualTipo1
                Next
                
            Case 7 'PRES ANUAL 2
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresAnualTipo2 = oGrupo.DefPresAnualTipo2
                Next
                
            Case 8 'PRES 1
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresTipo1 = oGrupo.DefPresTipo1
                Next
                
            Case 9 'PRES 2
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefPresTipo2 = oGrupo.DefPresTipo2
                Next
                
                
            Case 10  'SOLICITUD DE COMPRAS
                For Each oGrupo In frmPlantillasProce.m_oGrupos
                    oGrupoAnteriores.Add Nothing, oGrupo.Codigo, oGrupo.Den
                    oGrupoAnteriores.Item(CStr(oGrupo.Codigo)).DefSolicitud = oGrupo.DefSolicitud
                Next
                
        End Select
    
    Else
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        cmdCerrar.Visible = True
    End If
    
    m_bCargarLista = True
    
    
End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCE_GRUPOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    Me.caption = Ador(0).Value
    Ador.MoveNext
    cmdAceptar.caption = Ador(0).Value
    Ador.MoveNext
    cmdCancelar.caption = Ador(0).Value
    Ador.MoveNext
    cmdCerrar.caption = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing


End Sub


Private Sub MostrarGrupos()
    Dim oGrupo As CGrupo
    Dim i As Integer
    Dim bChequeado As Boolean
    
    lstGrupos.ListItems.Clear
    
    i = 1
    
    For Each oGrupo In frmPlantillasProce.m_oGrupos
        
         Select Case g_lIndice
            Case 0 'DEST
                bChequeado = oGrupo.DefDestino
            
            Case 1 ' PAGO
                bChequeado = oGrupo.DefFormaPago
                
            Case 2 'FECHAS
                bChequeado = oGrupo.DefFechasSum
            
            Case 3 'PROVE
                bChequeado = oGrupo.DefProveActual
                
            Case 4 'ESP
                bChequeado = oGrupo.DefEspecificaciones
            
            Case 5 'DIST UON
                bChequeado = oGrupo.DefDistribUON
            
            Case 6 'PRES ANUAL 1
                bChequeado = oGrupo.DefPresAnualTipo1
                
            Case 7 'PRES ANUAL 2
                bChequeado = oGrupo.DefPresAnualTipo2
                
            Case 8 'PRES 1
                bChequeado = oGrupo.DefPresTipo1
            
            Case 9 'PRES 2
                bChequeado = oGrupo.DefPresTipo2
                
            Case 10  'SOLICITUD DE COMPRAS
                bChequeado = oGrupo.DefSolicitud
                
        End Select
        
        'A�ado a la lista:
        lstGrupos.ListItems.Add , "GR" & CStr(i), oGrupo.Codigo & " - " & oGrupo.Den
        lstGrupos.ListItems(i).Checked = bChequeado
        lstGrupos.ListItems.Item("GR" & CStr(i)).Tag = oGrupo.Codigo
        
        i = i + 1
    Next
    
    Set oGrupo = Nothing
End Sub

''' <summary>
''' Descarga del formulario
''' Se comprueba en el que haya seleccionado algun grupo de la lista para que no se pueda ir por la X del formulario sin marcar
''' </summary>
''' <param name="Cancel">Cancelacion de la descarga del formulario</param>
''' <returns></returns>
''' <remarks>Llamada desde: cmdAceptar_Click,cmdCancelar_click,cmdCerrar_click Tiempo m�ximo: 0,3</remarks>

Private Sub Form_Unload(Cancel As Integer)

    If Not m_bAceptar And Not m_bCancelar And Not m_bCerrar Then
        If g_bModoEdicion = True Then
            'Cancel = True
            cmdCancelar_Click
            'Cancel = False
        Else
            cmdCerrar_Click
        End If

    Else
        If g_bModoEdicion = True Then
            Set oGrupoAnteriores = Nothing
        End If
    End If
End Sub

Private Sub lstGrupos_ItemCheck(ByVal Item As MSComctlLib.ListItem)
Dim oGrupo As CGrupo

    'si est� en modo consulta que no deje modificar el listbox de grupos
    If g_bModoEdicion = False And m_bCargando = False Then
        Item.Checked = Not Item.Checked
        Exit Sub
    End If
    
    If lstGrupos.ListItems.Count = 0 Then
        Item.Checked = Not Item.Checked
        Exit Sub
    End If
    
    Set oGrupo = frmPlantillasProce.m_oGrupos.Item(CStr(Item.Tag))
    If oGrupo Is Nothing Then Exit Sub

    Select Case g_lIndice

        Case 0 'Destino
                If Item.Checked Then
                    oGrupo.DefDestino = True
                Else
                    oGrupo.DefDestino = False
                End If

        Case 1 'Forma de pago
                If Item.Checked Then
                    oGrupo.DefFormaPago = True
                Else
                    oGrupo.DefFormaPago = False
                End If

        Case 2 'Fechas de suministro
                If Item.Checked Then
                    oGrupo.DefFechasSum = True
                Else
                    oGrupo.DefFechasSum = False
                End If

        Case 3 'Proveedor actual
                If Item.Checked Then
                    oGrupo.DefProveActual = True
                Else
                    oGrupo.DefProveActual = False
                End If

        Case 4 'Especificaciones
                If Item.Checked Then
                    oGrupo.DefEspecificaciones = True
                Else
                    oGrupo.DefEspecificaciones = False
                End If

        Case 5 'Dist UON
                If Item.Checked Then
                    oGrupo.DefDistribUON = True
                Else
                    oGrupo.DefDistribUON = False
                End If

        Case 6 'Presupuesto anual tipo1
                If Item.Checked Then
                    oGrupo.DefPresAnualTipo1 = True
                Else
                    oGrupo.DefPresAnualTipo1 = False
                End If

        Case 7 'Presupuesto anual tipo2
                If Item.Checked Then
                    oGrupo.DefPresAnualTipo2 = True
                Else
                    oGrupo.DefPresAnualTipo2 = False
                End If

        Case 8 'Presupuesto tipo1
                If Item.Checked Then
                    oGrupo.DefPresTipo1 = True
                Else
                    oGrupo.DefPresTipo1 = False
                End If

        Case 9 'Presupuesto tipo2
                If Item.Checked Then
                    oGrupo.DefPresTipo2 = True
                Else
                    oGrupo.DefPresTipo2 = False
                End If

        Case 10  'Solicitud de compras
                If Item.Checked Then
                    oGrupo.DefSolicitud = True
                Else
                    oGrupo.DefSolicitud = False
                End If
    End Select

    Set oGrupo = Nothing

End Sub
