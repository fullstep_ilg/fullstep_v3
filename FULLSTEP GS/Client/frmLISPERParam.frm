VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmLISPERParam 
   Caption         =   "Form1"
   ClientHeight    =   3555
   ClientLeft      =   195
   ClientTop       =   3840
   ClientWidth     =   6225
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLISPERParam.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3555
   ScaleWidth      =   6225
   Begin VB.Frame frameValores 
      Caption         =   "Introduzca valores"
      Height          =   3015
      Left            =   60
      TabIndex        =   0
      Top             =   90
      Width           =   6135
      Begin SSDataWidgets_B.SSDBGrid sdbgParam 
         Height          =   2655
         Left            =   60
         TabIndex        =   1
         Top             =   285
         Width           =   6015
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   5
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmLISPERParam.frx":014A
         UseGroups       =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         CellNavigation  =   1
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Groups(0).Width =   9843
         Groups(0).Caption=   "Par�metros"
         Groups(0).HasHeadForeColor=   -1  'True
         Groups(0).HasHeadBackColor=   -1  'True
         Groups(0).HeadForeColor=   16777215
         Groups(0).HeadBackColor=   8421504
         Groups(0).Columns.Count=   5
         Groups(0).Columns(0).Width=   5001
         Groups(0).Columns(0).Caption=   "Nombre"
         Groups(0).Columns(0).Name=   "NOM"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(0).Locked=   -1  'True
         Groups(0).Columns(0).HasBackColor=   -1  'True
         Groups(0).Columns(0).BackColor=   16777147
         Groups(0).Columns(1).Width=   4815
         Groups(0).Columns(1).Caption=   "Valor"
         Groups(0).Columns(1).Name=   "VAL"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(2).Width=   4657
         Groups(0).Columns(2).Visible=   0   'False
         Groups(0).Columns(2).Caption=   "NAME"
         Groups(0).Columns(2).Name=   "NAME"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         Groups(0).Columns(3).Width=   4895
         Groups(0).Columns(3).Visible=   0   'False
         Groups(0).Columns(3).Caption=   "SUBRPT"
         Groups(0).Columns(3).Name=   "SUBRPT"
         Groups(0).Columns(3).DataField=   "Column 3"
         Groups(0).Columns(3).DataType=   8
         Groups(0).Columns(3).FieldLen=   256
         Groups(0).Columns(4).Width=   2514
         Groups(0).Columns(4).Visible=   0   'False
         Groups(0).Columns(4).Caption=   "Parametro"
         Groups(0).Columns(4).Name=   "VAL_PARAM"
         Groups(0).Columns(4).DataField=   "Column 4"
         Groups(0).Columns(4).DataType=   8
         Groups(0).Columns(4).FieldLen=   256
         _ExtentX        =   10610
         _ExtentY        =   4683
         _StockProps     =   79
         BackColor       =   8421376
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown ssdbValoresParam 
      Height          =   735
      Left            =   300
      TabIndex        =   3
      Top             =   1905
      Width           =   2295
      ListAutoValidate=   0   'False
      MaxDropDownItems=   5
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      Row.Count       =   1
      Col.Count       =   4
      Row(0).Col(0)   =   " "
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2646
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   3519
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   1296
      _StockProps     =   77
   End
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      Height          =   375
      Left            =   4860
      TabIndex        =   2
      Top             =   3140
      Width           =   1335
   End
End
Attribute VB_Name = "frmLISPERParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public oReport As CRAXDRT.Report
Private NumParam As Integer
Dim adoRecordset As ADODB.Recordset
Dim vEstructura As Variant
Private m_bEsUltima As Boolean


Private Sub cmdObtener_Click()
    ObtenerInforme
End Sub


Private Sub Form_Load()
    
    Me.Width = 6315
    Me.Height = 3930
    
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    CargarRecursos
      
    PonerFieldSeparator Me
                                  
           
    
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LISPER_PARAM, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        
        frameValores.caption = ador(0).Value
        ador.MoveNext
        sdbgParam.Groups(0).caption = ador(0).Value
        ador.MoveNext
        cmdObtener.caption = ador(0).Value
        ador.MoveNext
        sdbgParam.Groups(0).Columns(0).caption = ador(0).Value
        ador.MoveNext
        sdbgParam.Groups(0).Columns(1).caption = ador(0).Value
        
        ador.Close
    
    End If

   Set ador = Nothing

End Sub

Private Sub ObtenerInforme()
    '''Obtiene el informe
    Dim pv As Preview
    Dim i As Integer
    Dim sSubRpt As CRAXDRT.Report
    Dim intTipo As Integer
       
        
    On Error GoTo Error
    
    'Comprueba que se han introducido todos los par�metros del informe
    For i = 0 To NumParam - 1
        sdbgParam.Row = i
        If sdbgParam.Columns("VAL").Value = "" And Left(sdbgParam.Columns("NOM").Text, 3) = "(*)" Then
            oMensajes.IntroducirPar�metros
            Exit Sub
        End If
    Next i
   

    'Introduce el valor en los par�metros
    For i = 0 To NumParam - 1
        sdbgParam.Row = i
        
        'Comprueba si el par�metro es del informe o de alg�n subreport
        If sdbgParam.Columns("SUBRPT").Value = "" Then
            intTipo = oReport.ParameterFields(crs_ParameterIndex(oReport, sdbgParam.Columns(2).Value)).ValueType
            Select Case intTipo
                Case 7 'Num�rico
                    If sdbgParam.Columns("VAL_PARAM").Value <> "" Then
                        oReport.ParameterFields(crs_ParameterIndex(oReport, sdbgParam.Columns(2).Value)).SetCurrentValue CLng(sdbgParam.Columns("VAL_PARAM").Value), intTipo
                    Else
                        oReport.ParameterFields(crs_ParameterIndex(oReport, sdbgParam.Columns(2).Value)).SetCurrentValue IIf(sdbgParam.Columns(1).Value = "", 0, CLng(sdbgParam.Columns(1).Value)), intTipo
                    End If
                    
                Case 10  'Fecha
                    If sdbgParam.Columns("VAL_PARAM").Value <> "" Then
                        oReport.ParameterFields(crs_ParameterIndex(oReport, sdbgParam.Columns(2).Value)).SetCurrentValue CDate(sdbgParam.Columns("VAL_PARAM").Value), intTipo
                    Else
                        oReport.ParameterFields(crs_ParameterIndex(oReport, sdbgParam.Columns(2).Value)).SetCurrentValue CDate(sdbgParam.Columns(1).Value), intTipo
                    End If
                    
                Case Else
                   If sdbgParam.Columns("VAL_PARAM").Value <> "" Then
                        oReport.ParameterFields(crs_ParameterIndex(oReport, sdbgParam.Columns(2).Value)).SetCurrentValue sdbgParam.Columns("VAL_PARAM").Value, intTipo
                   Else
                        oReport.ParameterFields(crs_ParameterIndex(oReport, sdbgParam.Columns(2).Value)).SetCurrentValue sdbgParam.Columns(1).Value, intTipo
                   End If
                    
            End Select
            
        Else  'El par�metro pertenece a un subreport
            Set sSubRpt = oReport.OpenSubreport(sdbgParam.Columns("SUBRPT").Value)
            intTipo = sSubRpt.ParameterFields(crs_ParameterIndex(sSubRpt, sdbgParam.Columns(2).Value)).ValueType
            Select Case intTipo
                Case 7  'Num�rico
                    sSubRpt.ParameterFields(crs_ParameterIndex(sSubRpt, sdbgParam.Columns(2).Value)).SetCurrentValue CLng(sdbgParam.Columns(1).Value), intTipo
                Case 10  'Fecha
                    sSubRpt.ParameterFields(crs_ParameterIndex(sSubRpt, sdbgParam.Columns(2).Value)).SetCurrentValue CDate(sdbgParam.Columns(1).Value), intTipo
                Case Else
                    sSubRpt.ParameterFields(crs_ParameterIndex(sSubRpt, sdbgParam.Columns(2).Value)).SetCurrentValue sdbgParam.Columns(1).Value, intTipo
            End Select
            Set sSubRpt = Nothing
        End If
    Next i
    
    Screen.MousePointer = vbHourglass
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Unload Me
    
    Set pv = New Preview
    pv.Hide
    pv.caption = Me.caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Exit Sub
    
Error:
    Screen.MousePointer = vbNormal
    If err.Number = 13 Then
        oMensajes.NoValido (sdbgParam.Columns(0).Value)
    End If
    
End Sub


''' <summary>
''' Carga los parametros del reporte en la pantalla
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: frmLisper.ObtenerInforme Tiempo m�ximo: 0,1</remarks>
Public Sub CargarParametros()
    Dim intSubRpt As Integer
    Dim i  As Integer
    Dim intSub As Integer
    Dim sSubRpt As CRAXDRT.Report
    Dim bExiste As Boolean
    
    '''Carga el grid con los par�metros del informe
    
    sdbgParam.RemoveAll
    
    For i = 1 To oReport.ParameterFields.Count
        sdbgParam.AddItem oReport.ParameterFields.Item(i).Prompt & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oReport.ParameterFields.Item(i).ParameterFieldName
    Next i
    
    NumParam = i - 1  'N�mero de par�metros del informe
    
    'Si existen subreports carga tambi�n sus par�metros en la grid
    For i = 1 To oReport.FormulaFields.Count
        If oReport.FormulaFields(i).FormulaFieldName = "NUMSUB" Then
            bExiste = True
            Exit For
        End If
    Next i
    
    If bExiste = False Then
        intSubRpt = 0
    Else
        intSubRpt = oReport.FormulaFields(crs_FormulaIndex(oReport, "NUMSUB")).Text
    End If
        
    If intSubRpt > 0 Then
        For i = 1 To intSubRpt
            Set sSubRpt = oReport.OpenSubreport("SubRpt" & i)
            
            For intSub = 1 To sSubRpt.ParameterFields.Count
                If Mid(sSubRpt.ParameterFields(intSub).ParameterFieldName, 1, 3) <> "Pm-" Then 'Que no sea un link del subreport
                    sdbgParam.AddItem sSubRpt.ParameterFields.Item(intSub).Prompt & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & sSubRpt.ParameterFields.Item(intSub).ParameterFieldName & Chr(m_lSeparador) & "SubRpt" & i
                End If
            Next intSub
            
            NumParam = NumParam + intSub - 1  'N�mero de par�metros del informe
            Set sSubRpt = Nothing
        Next i
        
    End If
       
    sdbgParam.Col = 1
End Sub

Private Sub Form_Resize()

On Error Resume Next

    frameValores.Height = Me.Height - 945
    frameValores.Width = Me.Width - 210
    sdbgParam.Height = frameValores.Height - 360
    sdbgParam.Width = frameValores.Width - 240
    cmdObtener.Left = Me.Width - cmdObtener.Width - 150
    cmdObtener.Top = frameValores.Height + 125
    sdbgParam.Groups(0).Width = sdbgParam.Width - 240
    sdbgParam.Columns(0).Width = (sdbgParam.Groups(0).Width) / 2
    sdbgParam.Columns(1).Width = (sdbgParam.Groups(0).Width) / 2.1
End Sub


''' <summary>
''' Control del tabulador para la ultima columna del grid
''' </summary>
''' <param name="KeyCode">Codigo de la tecla presionada</param>
''' <param name="Shift">Si esta apretada la tecla shift</param>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbgParam_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyTab Then
        If sdbgParam.Row + 1 = oReport.ParameterFields.Count Then
            m_bEsUltima = True
            sdbgParam.Update
            If m_bEsUltima Then
                If Me.Visible Then cmdObtener.SetFocus
            End If
        End If
        
    End If
End Sub



''' <summary>
''' Evento al moverse de columna o de fila en la grid
''' </summary>
''' <param name="lastRow">Fila desde la que se mueve</param>
''' <param name="LastCol">Columna desde la que se mueve</param>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Public Sub sdbgParam_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    If oReport.ParameterFields.Item(sdbgParam.Row + 1).NumberOfDefaultValues > 0 Then
       
    ssdbValoresParam.RemoveAll
    ssdbValoresParam.AddItem "" & Chr(m_lSeparador) & ""
    sdbgParam.Columns("VAL").DropDownHwnd = ssdbValoresParam.hWnd
    ssdbValoresParam.Enabled = True
    ssdbValoresParam.DroppedDown = True
    ssdbValoresParam_DropDown
    ssdbValoresParam.DroppedDown = True
    
Else
    ''Comprobar estructura NOMBRE_TABLA.NOMBRE_CAMPO
    If (Comprobar_estructura(oReport.ParameterFields.Item(sdbgParam.Row + 1).ParameterFieldName) > 0) Then
        ReDim Preserve vEstructura(2)
        Dim oGestorListadosPers As cGestorListadosPers
        Set oGestorListadosPers = oFSGSRaiz.Generar_CGestorListadosPers
    
        Set adoRecordset = New ADODB.Recordset
        Set adoRecordset = oGestorListadosPers.DevolverDatosTablaIndefinida(vEstructura(0), vEstructura(1), vEstructura(2))
        Set oGestorListadosPers = Nothing
            
        If (Not adoRecordset Is Nothing) Then
           
            ssdbValoresParam.RemoveAll
            ssdbValoresParam.AddItem "" & Chr(m_lSeparador) & ""
            sdbgParam.Columns("VAL").DropDownHwnd = ssdbValoresParam.hWnd
            ssdbValoresParam.Enabled = True
            ssdbValoresParam.DroppedDown = True
            ssdbValoresParam_DropDown
            ssdbValoresParam.DroppedDown = True
            
        Else
            sdbgParam.Columns("VAL").Style = ssStyleEdit
            sdbgParam.Columns("VAL").DropDownHwnd = 0
            sdbgParam.Columns("VAL").Locked = False
        End If
    Else
        sdbgParam.Columns("VAL").Style = ssStyleEdit
        sdbgParam.Columns("VAL").DropDownHwnd = 0
        sdbgParam.Columns("VAL").Locked = False
    End If
       
End If
   
End Sub

''' <summary>
''' Evento de close up de un combo en la grid
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub ssdbValoresParam_CloseUp()
Dim sDato As String

sdbgParam.Columns("VAL_PARAM").Value = ssdbValoresParam.Columns("COD").Value
sDato = ssdbValoresParam.Columns("COD").Value
If (ssdbValoresParam.Columns("DEN").Value <> "") Then
    sDato = sDato & "-" & ssdbValoresParam.Columns("DEN").Value
End If
'sdbgParam.Columns("VAL").DropDownHwnd = 0
sdbgParam.Columns("VAL").Value = sDato

End Sub

''' <summary>
''' Evento de drop down de un combo en la grid
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub ssdbValoresParam_DropDown()
Dim i As Integer

ssdbValoresParam.RemoveAll
  
If oReport.ParameterFields.Item(sdbgParam.Row + 1).NumberOfDefaultValues > 0 Then
    'Cargar la lista de valores
    For i = 1 To oReport.ParameterFields.Item(sdbgParam.Row + 1).NumberOfDefaultValues
        ssdbValoresParam.AddItem oReport.ParameterFields.Item(sdbgParam.Row + 1).GetNthDefaultValue(i)
    Next i
    ssdbValoresParam.Columns("DEN").Visible = False
ElseIf (Comprobar_estructura(oReport.ParameterFields.Item(sdbgParam.Row + 1).ParameterFieldName) > 0) Then
    ReDim Preserve vEstructura(2)
    If vEstructura(2) <> "" Then
        ssdbValoresParam.Columns("DEN").Visible = True
    Else
        ssdbValoresParam.Columns("DEN").Visible = False
        ssdbValoresParam.Columns("COD").Width = sdbgParam.Columns(1).Width - 250
    End If
  
    If Not adoRecordset Is Nothing Then
        adoRecordset.MoveFirst
        Do While Not adoRecordset.EOF
            If vEstructura(2) <> "" Then
                ssdbValoresParam.AddItem adoRecordset.Fields.Item(0) & Chr(m_lSeparador) & adoRecordset.Fields.Item(1)
            Else
                ssdbValoresParam.AddItem adoRecordset.Fields.Item(0)
            End If
        
            adoRecordset.MoveNext
        Loop
        'Set adoRecordset = Nothing
      
   End If
End If

End Sub

Private Function Comprobar_estructura(sVal As String) As Integer
    vEstructura = Split(sVal, ".")
    Comprobar_estructura = UBound(vEstructura)
End Function

Private Sub ssdbValoresParam_InitColumnProps()
    ssdbValoresParam.DataFieldList = "Column 0"
End Sub

''' <summary>
''' Validacion del valor de la combo
''' </summary>
''' <param name="Text">Texto seleccion</param>
''' <param name="RtnPassed">si es valido o no</param>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub ssdbValoresParam_ValidateList(Text As String, RtnPassed As Integer)
   
    If Text = "" Then
        RtnPassed = True
    Else
        Dim sDato As String
        Dim i As Integer
        Dim bValido As Boolean
        
        
        bValido = False
        
        
        If oReport.ParameterFields.Item(sdbgParam.Row + 1).NumberOfDefaultValues > 0 Then
            For i = 1 To oReport.ParameterFields.Item(sdbgParam.Row + 1).NumberOfDefaultValues
                If oReport.ParameterFields.Item(sdbgParam.Row + 1).GetNthDefaultValue(i) = Text Then
                    sdbgParam.Columns("VAL_PARAM").Value = Text
                    bValido = True
                    Exit For
                End If
            Next i
            
            If Not bValido Then
                RtnPassed = False
                m_bEsUltima = False
            End If

        
        ElseIf (Comprobar_estructura(oReport.ParameterFields.Item(sdbgParam.Row + 1).ParameterFieldName) > 0) Then
            If (ssdbValoresParam.Columns("DEN").Visible) Then
                 If Not adoRecordset Is Nothing Then
                     adoRecordset.MoveFirst
                     
                     For i = 1 To adoRecordset.RecordCount
                     
                        If Text <> "" Then
                     
                            If adoRecordset.Fields.Item(0).Value = Text Then
                        
                                sdbgParam.Columns("VAL").Value = sdbgParam.Columns("VAL").Value & "-" & adoRecordset.Fields.Item(1).Value
                                sdbgParam.Columns("VAL_PARAM").Value = adoRecordset.Fields.Item(0).Value
                                bValido = True
                                Exit For
                            Else
                                If adoRecordset.Fields.Item(0).Value & "-" & adoRecordset.Fields.Item(1).Value = Text Then
                                    sdbgParam.Columns("VAL").Value = adoRecordset.Fields.Item(0).Value & "-" & adoRecordset.Fields.Item(1).Value
                                    sdbgParam.Columns("VAL_PARAM").Value = adoRecordset.Fields.Item(0).Value
                                    bValido = True
                                    Exit For
                                        
                                End If
                            End If
                        End If
                        adoRecordset.MoveNext
                     Next
                     
                     If Not bValido Then
                        RtnPassed = False
                        m_bEsUltima = False
                     End If
                   
                End If
                
            End If
        End If
        
        

    End If

End Sub
