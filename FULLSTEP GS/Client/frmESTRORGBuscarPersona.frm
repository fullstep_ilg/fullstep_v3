VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRORGBuscarPersona 
   BackColor       =   &H00808000&
   Caption         =   "Buscar persona"
   ClientHeight    =   3885
   ClientLeft      =   1155
   ClientTop       =   1470
   ClientWidth     =   9255
   Icon            =   "frmESTRORGBuscarPersona.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3885
   ScaleWidth      =   9255
   Begin VB.CommandButton cmdCargar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8850
      Picture         =   "frmESTRORGBuscarPersona.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Cargar"
      Top             =   900
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPersonas 
      Height          =   2055
      Left            =   120
      TabIndex        =   4
      Top             =   1320
      Width           =   9090
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   8
      stylesets.count =   1
      stylesets(0).Name=   "ActiveRowBlue"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8388608
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmESTRORGBuscarPersona.frx":01D6
      UseGroups       =   -1  'True
      AllowUpdate     =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Groups.Count    =   3
      Groups(0).Width =   4366
      Groups(0).Caption=   "Unidades organizativas"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   4
      Groups(0).Columns(0).Width=   1058
      Groups(0).Columns(0).Caption=   "UON1"
      Groups(0).Columns(0).Name=   "UON1"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).BackColor=   16449500
      Groups(0).Columns(1).Width=   1058
      Groups(0).Columns(1).Caption=   "UON2"
      Groups(0).Columns(1).Name=   "UON2"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).BackColor=   16449500
      Groups(0).Columns(2).Width=   1058
      Groups(0).Columns(2).Caption=   "UON3"
      Groups(0).Columns(2).Name=   "UON3"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(2).HasBackColor=   -1  'True
      Groups(0).Columns(2).BackColor=   16449500
      Groups(0).Columns(3).Width=   1191
      Groups(0).Columns(3).Caption=   "Dep."
      Groups(0).Columns(3).Name=   "DEP"
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   8
      Groups(0).Columns(3).FieldLen=   256
      Groups(0).Columns(3).HasBackColor=   -1  'True
      Groups(0).Columns(3).BackColor=   16449500
      Groups(1).Width =   8123
      Groups(1).Caption=   "Personas"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns.Count=   3
      Groups(1).Columns(0).Width=   1164
      Groups(1).Columns(0).Caption=   "C�digo"
      Groups(1).Columns(0).Name=   "CODPER"
      Groups(1).Columns(0).DataField=   "Column 4"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(1).Width=   4736
      Groups(1).Columns(1).Caption=   "Apellidos"
      Groups(1).Columns(1).Name=   "APE"
      Groups(1).Columns(1).DataField=   "Column 5"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(2).Width=   2223
      Groups(1).Columns(2).Caption=   "Nombre"
      Groups(1).Columns(2).Name=   "NOM"
      Groups(1).Columns(2).DataField=   "Column 6"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(2).Width =   3200
      Groups(2).Caption=   "Funci�n compradora"
      Groups(2).HasHeadForeColor=   -1  'True
      Groups(2).HasHeadBackColor=   -1  'True
      Groups(2).HeadForeColor=   16777215
      Groups(2).HeadBackColor=   8421504
      Groups(2).Columns(0).Width=   3201
      Groups(2).Columns(0).Caption=   "Equipo"
      Groups(2).Columns(0).Name=   "Equipo"
      Groups(2).Columns(0).DataField=   "Column 7"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      TabNavigation   =   1
      _ExtentX        =   16034
      _ExtentY        =   3625
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCancelar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4705
      TabIndex        =   6
      Top             =   3480
      Width           =   1125
   End
   Begin VB.CommandButton cmdAceptar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3325
      TabIndex        =   5
      Top             =   3480
      Width           =   1125
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1080
      Left            =   120
      ScaleHeight     =   1020
      ScaleWidth      =   8565
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   120
      Width           =   8625
      Begin VB.CheckBox ChkFunCompra 
         BackColor       =   &H00808000&
         Caption         =   "Funci�n Compradora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   2475
         TabIndex        =   11
         Top             =   600
         Width           =   2340
      End
      Begin VB.TextBox txtCod 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1215
         MaxLength       =   50
         TabIndex        =   2
         Top             =   540
         Width           =   735
      End
      Begin VB.TextBox txtApe 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1215
         MaxLength       =   150
         TabIndex        =   0
         Top             =   120
         Width           =   4155
      End
      Begin VB.TextBox txtNom 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   6825
         MaxLength       =   50
         TabIndex        =   1
         Top             =   120
         Width           =   1575
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEquipo 
         Height          =   285
         Left            =   5775
         TabIndex        =   13
         Top             =   540
         Width           =   2625
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2487
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3625
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "Denominaci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4630
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblEquipo 
         BackColor       =   &H00808000&
         Caption         =   "Equipo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   4875
         TabIndex        =   12
         Top             =   600
         Width           =   765
      End
      Begin VB.Label lblCod 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   120
         TabIndex        =   10
         Top             =   600
         Width           =   750
      End
      Begin VB.Label lblNom 
         BackColor       =   &H00808000&
         Caption         =   "Nombre:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   5700
         TabIndex        =   9
         Top             =   195
         Width           =   1050
      End
      Begin VB.Label lblApel 
         BackColor       =   &H00808000&
         Caption         =   "Apellidos:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   120
         TabIndex        =   8
         Top             =   180
         Width           =   1125
      End
   End
End
Attribute VB_Name = "frmESTRORGBuscarPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private oPersonasEncontradas As CPersonas

Private CargarComboDesdeEqp As Boolean
Private m_bBajaLog As Boolean

Private Sub cmdAceptar_Click()

If sdbgPersonas.Rows = 0 Then
    Exit Sub
End If

Dim nodx As node
On Error GoTo Error

Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("PERS" & CStr(sdbgPersonas.Columns(4).Value))
nodx.Parent.Expanded = True
nodx.Selected = True

frmESTRORG.ConfigurarInterfazSeguridad nodx

Unload Me
If frmESTRORG.Visible Then frmESTRORG.tvwestrorg.SetFocus

Exit Sub

Error:
    oMensajes.DatosModificados

End Sub

Private Sub cmdCancelar_Click()

Unload Me

End Sub

Private Sub cmdCargar_Click()
    Dim Adores As Ador.Recordset
    
    If Me.Visible Then cmdAceptar.SetFocus
    
    Screen.MousePointer = vbHourglass
    
    Set Adores = oPersonasEncontradas.CargarTodasLasPersonas2(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod), Trim(txtNom), Trim(txtApe), False, gParametrosGenerales.giNEO + 1, , , , True, False, ChkFunCompra.Value, Me.sdbcEquipo.Text, , m_bBajaLog)
    
    Screen.MousePointer = vbNormal
    
    'Carga la grid
    CargarGrid Adores
    
    'Cierra el recordset
    Adores.Close
    Set Adores = Nothing
End Sub
Private Sub CargarGrid(ByVal Adores As Ador.Recordset)
    'Carga la grid con las personas

    sdbgPersonas.RemoveAll
    
    While Not Adores.EOF
        sdbgPersonas.AddItem Adores("UON1") & Chr(m_lSeparador) & Adores("UON2") & Chr(m_lSeparador) & Adores("UON3") & Chr(m_lSeparador) & Adores("DEP") & Chr(m_lSeparador) & Adores("COD") & Chr(m_lSeparador) & Adores("APE") & Chr(m_lSeparador) & Adores("NOM") & Chr(m_lSeparador) & Adores("EQP")
        Adores.MoveNext
    Wend
    
End Sub
Private Sub Form_Load()
    
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    Set oPersonasEncontradas = oFSGSRaiz.Generar_CPersonas
        
    CargarRecursos
    
    If FSEPConf Then
        ChkFunCompra.Visible = False
        sdbcEquipo.Visible = False
        lblEquipo.Visible = False
        sdbgPersonas.Groups(2).Visible = False
        sdbgPersonas.Width = sdbgPersonas.Groups(0).Width + sdbgPersonas.Groups(1).Width
        lblNom.Left = 120
        lblNom.Width = 1050
        lblNom.Top = 650
        lblNom.Height = 270
        txtNom.Left = 1215
        txtNom.Width = 1575
        txtNom.Top = 600
        txtNom.Height = 285
        lblCod.Left = 3000
        lblCod.Width = 750
        lblCod.Top = 650
        lblCod.Height = 270
        txtCod.Left = 3900
        txtCod.Width = 735
        txtCod.Top = 600
        txtCod.Height = 285
        frmESTRORGBuscarPersona.Width = 7900
        Picture1.Width = 7200
        cmdCargar.Left = 7385
        cmdAceptar.Left = 2500
        cmdCancelar.Left = 3800
    End If
    
    If frmESTRORG.chkBajaLog.Value = vbChecked Then
        m_bBajaLog = True
    Else
        m_bBajaLog = False
    End If
    
    PonerFieldSeparator Me
End Sub

Private Sub Form_Resize()
    
    If Me.Height < 2000 Then Exit Sub
    If Me.Width < 1000 Then Exit Sub
    sdbgPersonas.Left = Picture1.Left
    sdbgPersonas.Top = Picture1.Height + 250
    sdbgPersonas.Width = Me.Width - 300
    sdbgPersonas.Groups.Item(0).Width = sdbgPersonas.Width / 4
    sdbgPersonas.Groups.Item(1).Width = sdbgPersonas.Width / 4 * 2
    sdbgPersonas.Groups.Item(2).Width = sdbgPersonas.Width / 4
    If FSEPConf Then
       sdbgPersonas.Groups.Item(0).Width = sdbgPersonas.Width / 3
       sdbgPersonas.Groups.Item(1).Width = sdbgPersonas.Width / 3 * 2
    End If
    sdbgPersonas.Height = Me.Height - Picture1.Height - 1200
    cmdAceptar.Top = sdbgPersonas.Height + Picture1.Height + 400
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = sdbgPersonas.Width / 2 - cmdAceptar.Width - 100
    cmdCancelar.Left = sdbgPersonas.Width / 2 + 100
End Sub

Private Sub Form_Unload(Cancel As Integer)

Set oPersonasEncontradas = Nothing

End Sub

Private Sub sdbcEquipo_Change()
    CargarComboDesdeEqp = True
End Sub

Private Sub sdbcEquipo_Click()
    If Not sdbcEquipo.DroppedDown Then
        sdbcEquipo = ""
    End If
End Sub


Private Sub sdbcEquipo_CloseUp()
    If sdbcEquipo.Value = "..." Then
        sdbcEquipo.Text = ""
        Exit Sub
    End If
End Sub


Private Sub sdbcEquipo_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim oEqps As CEquipos
    
    Screen.MousePointer = vbHourglass
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    sdbcEquipo.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEquipo.Text), , False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, False, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEquipo.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEquipo.AddItem "..."
    End If

    sdbcEquipo.SelStart = 0
    sdbcEquipo.SelLength = Len(sdbcEquipo.Text)
    sdbcEquipo.Refresh

    Set oEqps = Nothing
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcEquipo_InitColumnProps()
    sdbcEquipo.DataFieldList = "Column 0"
    'sdbcEquipo.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcEquipo_PositionList(ByVal Text As String)
        ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEquipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEquipo.Rows - 1
            bm = sdbcEquipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEquipo.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEquipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEquipo_Validate(Cancel As Boolean)
    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEquipo.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
   
    oEquipos.CargarTodosLosEquipos sdbcEquipo.Text, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEquipo.Text = ""
    Else
        CargarComboDesdeEqp = False
    End If
    
    Set oEquipos = Nothing
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbgPersonas_DblClick()
    cmdAceptar_Click
End Sub

Private Sub sdbgPersonas_HeadClick(ByVal ColIndex As Integer)
Dim oDep As CDepAsociado
Dim Adores As Ador.Recordset

Screen.MousePointer = vbHourglass

Set oPersonasEncontradas = Nothing
Set oPersonasEncontradas = oFSGSRaiz.Generar_CPersonas
    
Select Case ColIndex
    
    Case 4 ' Por codigo
            
        Set Adores = oPersonasEncontradas.CargarTodasLasPersonas2(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod), Trim(txtNom), Trim(txtApe), False, , True, , , , False, ChkFunCompra.Value, sdbcEquipo.Text, , m_bBajaLog)
             
    Case 5 ' Por nombre
                        
        Set Adores = oPersonasEncontradas.CargarTodasLasPersonas2(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod), Trim(txtNom), Trim(txtApe), False, , , True, , , False, ChkFunCompra.Value, sdbcEquipo.Text, , m_bBajaLog)

    Case 6 ' Por apellidos
                
        Set Adores = oPersonasEncontradas.CargarTodasLasPersonas2(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod), Trim(txtNom), Trim(txtApe), False, , , , True, , False, ChkFunCompra.Value, sdbcEquipo.Text, , m_bBajaLog)
            
    
    Case 3, 2, 1, 0 ' Por departamento
                
        Set Adores = oPersonasEncontradas.CargarTodasLasPersonas2(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod), Trim(txtNom), Trim(txtApe), False, , , , , True, False, ChkFunCompra.Value, sdbcEquipo.Text, , m_bBajaLog)
    
    Case 7
        Set Adores = oPersonasEncontradas.CargarTodasLasPersonas2(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod), Trim(txtNom), Trim(txtApe), False, , , , , False, False, ChkFunCompra.Value, sdbcEquipo.Text, True, m_bBajaLog)
        
End Select

CargarGrid Adores

Adores.Close
Set Adores = Nothing
Screen.MousePointer = vbNormal

End Sub


Private Sub sdbgPersonas_InitColumnProps()
    
    Select Case gParametrosGenerales.giNEO
        
        Case 0
            
            sdbgPersonas.Columns(0).Visible = False
            sdbgPersonas.Columns(1).Visible = False
            sdbgPersonas.Columns(2).Visible = False
            
        Case 1
            sdbgPersonas.Columns(1).Visible = False
            sdbgPersonas.Columns(2).Visible = False
        Case 2
            sdbgPersonas.Columns(2).Visible = False
    End Select
    
    sdbgPersonas.Columns(0).caption = gParametrosGenerales.gsABR_UON1 & "."
    sdbgPersonas.Columns(1).caption = gParametrosGenerales.gsABR_UON2 & "."
    sdbgPersonas.Columns(2).caption = gParametrosGenerales.gsABR_UON3 & "."
   
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRORG_BUSCARPERSONA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        lblApel.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblNom.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblCod.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbgPersonas.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Groups(1).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgPersonas.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.MoveNext
        sdbcEquipo.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcEquipo.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        
        ChkFunCompra.caption = Ador(0).Value
        sdbgPersonas.Groups(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns(7).caption = Ador(0).Value
        lblEquipo.caption = Ador(0).Value
        Ador.MoveNext
        
        
        Ador.Close
    
    End If

    Set Ador = Nothing



End Sub



