VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELPROVEResp 
   BackColor       =   &H00808000&
   Caption         =   "DSelecci�n del comprador responsable del proceso"
   ClientHeight    =   4980
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6705
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELPROVEResp.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4980
   ScaleWidth      =   6705
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   2040
      TabIndex        =   2
      Top             =   4600
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3240
      TabIndex        =   1
      Top             =   4600
      Width           =   1005
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPROVEResp.frx":0CB2
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPROVEResp.frx":1106
            Key             =   "Comprador"
            Object.Tag             =   "Comprador"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPROVEResp.frx":121A
            Key             =   "Equipo"
            Object.Tag             =   "Equipo"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwEstrComp 
      Height          =   4395
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   7752
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSELPROVEResp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_oProcesoSeleccionado As CProceso
Public g_oOrigen As Form
Public g_bValida As Boolean
Public g_udtAccion As accionessummit

Private m_oEquipos As CEquipos
'Variables de idiomas
Private m_sRaiz As String
Private m_sSubject As String
Private m_sEstados(12) As String

'Variables de seguridad
Private m_bREqp As Boolean
Private m_bRuo As Boolean
Private m_bRPerfUO As Boolean
Private m_bRDep As Boolean
Private m_bRMat As Boolean
Public m_bPermProcMultimaterial As Boolean

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim oTextosWebtext As CTextosWEBTEXT

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELPROVERESP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        m_sRaiz = Ador(0).Value  'Equipos de compra
        Ador.MoveNext
        m_sEstados(1) = Ador(0).Value   '6 Pendiente de validar apertura
        Ador.MoveNext
        m_sEstados(2) = Ador(0).Value   '7 Pendiente de asig. prove.
        Ador.MoveNext
        m_sEstados(3) = Ador(0).Value   '8 Pendiente de enviar pet.
        Ador.MoveNext
        m_sEstados(4) = Ador(0).Value   '9 En recepci�n de ofertas
        Ador.MoveNext
        m_sEstados(5) = Ador(0).Value   '10 Pend. de comunicar obj.
        Ador.MoveNext
        m_sEstados(6) = Ador(0).Value   '11 Pendiente de adjudicar
        Ador.MoveNext
        m_sEstados(7) = Ador(0).Value   '12 Pend. de notificar las adj.
        Ador.MoveNext
        m_sEstados(8) = Ador(0).Value   '13 Adjudicado y notificado
        Ador.MoveNext
        m_sEstados(9) = Ador(0).Value   '14 Anulado
        Ador.MoveNext
        m_sEstados(10) = Ador(0).Value  '15 Cerrado
        Ador.MoveNext
        m_sEstados(11) = Ador(0).Value  '16 Abierto
        Ador.MoveNext
        m_sEstados(12) = Ador(0).Value  '17 Parcialmente cerrado
        
        Ador.Close
                
        Set oTextosWebtext = oFSGSRaiz.Generar_CTextosWEBTEXT
        m_sSubject = oTextosWebtext.DevolverTextoWEBTEXT(102, 2, basPublic.gParametrosInstalacion.gIdioma)
        Set oTextosWebtext = Nothing
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Aceptar selecci�n del comprador responsable del proceso
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim sCom As String
    Dim sEqp As String
    Dim oIAsigs As IAsignaciones
    Dim errormail As TipoErrorSummit
    Dim sCuerpo As String
    Dim sIdioma As String
    Dim oPer As CPersona
    Dim oPersonas As CPersonas
    Dim bAsignar As Boolean
    Dim oCom As CComprador
    Dim sCod As String
    Dim IMatAsig As IMaterialAsignado
    Dim oGruposMAt4 As CGruposMatNivel4
    Dim iRes As Integer
    Dim sTZ As String
    Dim sSubject As String
    Dim oTextosWebtext As CTextosWEBTEXT
    Dim sSubjectForm As String

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    If tvwEstrComp.selectedItem Is Nothing Then
        oMensajes.SeleccioneCompradorResponsable
        Exit Sub
    End If
    
    If Left(tvwEstrComp.selectedItem.Tag, 3) <> "COM" Then
        oMensajes.SeleccioneCompradorResponsable
        Exit Sub
    End If
   
    sCom = DevolverCod(tvwEstrComp.selectedItem)
    sEqp = DevolverCod(tvwEstrComp.selectedItem.Parent)
    
    'si seleccionamos el mismo responsable que hay ahora no hace nada
    If Not g_oProcesoSeleccionado.responsable Is Nothing Then
        If g_oProcesoSeleccionado.responsable.Cod <> "" Then
            If g_oProcesoSeleccionado.responsable.Cod = sCom And g_oProcesoSeleccionado.responsable.codEqp = sEqp Then
                Unload Me
                Exit Sub
            End If
        End If
    End If
    If (g_oOrigen.Name = "frmPROCE" And g_bValida) Or g_oProcesoSeleccionado.Estado > validado Then
        bAsignar = False
        If gParametrosInstalacion.gbSeleccionPositiva = True Then
            iRes = oMensajes.PreguntaModificarResponsable(iTipoMod:=1)
        End If
        If iRes = vbYes Then
            sCod = sEqp & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(sEqp))
            sCod = sCod & sCom
            Set oCom = m_oEquipos.Item(sEqp).Compradores(sCod)
            Set IMatAsig = oCom
            
            Dim oMat As CGrupoMatNivel4
            If m_bPermProcMultimaterial Then
                Set oGruposMAt4 = oFSGSRaiz.Generar_CGruposMatNivel4
                For Each oMat In g_oProcesoSeleccionado.MaterialProce
                     Set oGruposMAt4 = IMatAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, oMat.GMN1Cod, oMat.GMN2Cod, oMat.GMN3Cod, oMat.Cod, , True)
                     If oGruposMAt4.Count > 0 Then
                        Exit For
                     End If
                Next
            Else
                Set oGruposMAt4 = IMatAsig.DevolverGruposMN4Visibles(g_oProcesoSeleccionado.MaterialProce.Item(1).GMN1Cod, g_oProcesoSeleccionado.MaterialProce.Item(1).GMN2Cod, g_oProcesoSeleccionado.MaterialProce.Item(1).GMN3Cod, g_oProcesoSeleccionado.MaterialProce.Item(1).Cod, , True)
            End If
           
            If oGruposMAt4.Count = 0 Then
                iRes = oMensajes.PreguntaModificarResponsable(iTipoMod:=2)
                If iRes = vbYes Then
                    bAsignar = True
                End If
            Else
                bAsignar = True
            End If
        End If
    End If
    Screen.MousePointer = vbHourglass
    
    'Guarda en BD el nuevo responsable:
    Set oIAsigs = g_oProcesoSeleccionado
    teserror = oIAsigs.AsignarResponsable(sEqp, sCom, bAsignar)
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Set oIAsigs = Nothing
        Exit Sub
    End If
    
    Set oIAsigs = Nothing
    RegistrarAccion g_udtAccion, "Proceso:" & CStr(g_oProcesoSeleccionado.Anyo) & " / " & CStr(g_oProcesoSeleccionado.GMN1Cod) & " / " & CStr(g_oProcesoSeleccionado.Cod) & "  Responsable: " & sEqp & "; " & sCom
    
    'Carga los datos del comprador
    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    oPersonas.CargarTodasLasPersonas sCom, , , True, 10, , , , , , , True, False
    Set oPer = oPersonas.Item(1)
    Set oPersonas = Nothing
    
    'Modifica la clase cProcesoSeleccionado del form origen y el bot�n de responsable
    If Not g_oOrigen Is Nothing Then
        If (g_oOrigen.Name = "frmPROCE" And g_bValida) Then g_oOrigen.g_bAsignarResp = bAsignar
        g_oOrigen.ResponsableSustituido oPer
    End If
    
    'Si el responsable seleccionado es distinto del usuario que est� realizando el cambio le env�a un email
    If Not (basOptimizacion.gvarCodUsuario = sCom And basOptimizacion.gCodEqpUsuario = sEqp) Then
        If oPer.mail <> "" And Not IsNull(oPer.mail) Then
            If IsNull(oPer.Usuario) Then
                sIdioma = gParametrosGenerales.gIdioma
            Else
                sIdioma = oGestorParametros.DevolverIdiomaUsuario(oPer.Usuario)
                If sIdioma = "" Then
                    sIdioma = gParametrosGenerales.gIdioma
                End If
            End If
            If sIdioma <> "" Then
                'Obtener la zona horaria para las fechas
                sTZ = ObtenerTZFechas(oPer.Usuario)
                
                'Obtiene el subject del email en el idioma del usuario,no en el de la aplicaci�n
                If sIdioma <> basPublic.gParametrosInstalacion.gIdioma Then
                    Set oTextosWebtext = oFSGSRaiz.Generar_CTextosWEBTEXT
                    m_sSubject = oTextosWebtext.DevolverTextoWEBTEXT(102, 2, sIdioma)
                    Set oTextosWebtext = Nothing
                End If
                'Obtiene el subject De partede : ### del email en el idioma del usuario,no en el de la aplicaci�n
                sSubjectForm = gSubjectMultiIdiomaSMTP
                If sIdioma <> basPublic.gParametrosInstalacion.gIdioma Then
                    Set oTextosWebtext = oFSGSRaiz.Generar_CTextosWEBTEXT
                    sSubjectForm = oTextosWebtext.DevolverTextoWEBFSWSTEXT(180, 1, sIdioma)
                    Set oTextosWebtext = Nothing
                End If
                'Genera el cuerpo del email:
                Dim oCEmailProce As CEmailProce
                Set oCEmailProce = GenerarCEmailProce(oFSGSRaiz, gParametrosGenerales, basPublic.gParametrosInstalacion, oMensajes, basOptimizacion.gTipoDeUsuario, oUsuarioSummit, basParametros.gLongitudesDeCodigos)
                sCuerpo = oCEmailProce.GenerarMensajeNotificResponsable(sIdioma, sTZ, m_bDescargarFrm, m_bActivado, g_oProcesoSeleccionado, m_sEstados)
                Set oCEmailProce = Nothing
                
                If oIdsMail Is Nothing Then Set oIdsMail = IniciarSesionMail
                'Env�a o muestra el email
                sSubject = ComponerAsuntoEMail(m_sSubject, g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oProcesoSeleccionado.Den)
                errormail = ComponerMensaje(oPer.mail, sSubject, sCuerpo, , "frmSELPROVEResp", , , , sSubjectForm, _
                                                entidadNotificacion:=entidadNotificacion.Solicitud, _
                                                Anyo:=g_oProcesoSeleccionado.Anyo, _
                                                GMN1:=g_oProcesoSeleccionado.GMN1Cod, _
                                                Proce:=g_oProcesoSeleccionado.Cod)
                If errormail.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError errormail
                    FinalizarSesionMail
                    Exit Sub
                End If
                FinalizarSesionMail
            End If
        End If
    End If
        
    Set oPer = Nothing
    Screen.MousePointer = vbNormal
    Unload Me

    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

'<summary>Devuelve la zona horaria del usuario. Si no existe devuelve la zona horaria del usuario conectado</summary>
'<returns>Objeto de tipo GSServerModel.EMail con los datos del correo a mostrar</returns>
'<remarks>Llamada desde: cmdAceptar_Click  </remarks>
    
Private Function ObtenerTZFechas(ByVal Usuario As Variant) As String
    Dim oUsuario  As CUsuario
    Dim sTZ As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsNull(Usuario) And Not IsEmpty(Usuario) Then
        Set oUsuario = oFSGSRaiz.generar_cusuario
        oUsuario.Cod = Usuario
        oUsuario.ExpandirUsuario
        If Not IsNull(oUsuario.TimeZone) And Not IsEmpty(oUsuario.TimeZone) Then
            sTZ = oUsuario.TimeZone
        End If
        
        Set oUsuario = Nothing
    End If
    
    If sTZ = vbNullString Then sTZ = oUsuarioSummit.TimeZone
    
    ObtenerTZFechas = sTZ
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "ObtenerTZFechas", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function


Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Width = 6795
    Me.Height = 5460
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    m_bRMat = False
    m_bRDep = False
    m_bRuo = False
    m_bREqp = False
        
    CargarRecursos
    
    ConfigurarSeguridad

    GenerarEstructuraCompras False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 08/01/2013</revision>

Private Sub ConfigurarSeguridad()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case g_oOrigen.Name
    
        Case "frmPROCE"
            m_bRDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestResponsableDep)) Is Nothing)
            m_bREqp = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestResponsableEqp)) Is Nothing)
            m_bRMat = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestResponsableMat)) Is Nothing)
            m_bRuo = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestResponsableUO)) Is Nothing)
            m_bRPerfUO = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestResponsablePerfUO)) Is Nothing)
            
        Case "frmADJ"
            m_bRDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestResponsableDep)) Is Nothing)
            m_bREqp = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestResponsableEqp)) Is Nothing)
            m_bRMat = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestResponsableMat)) Is Nothing)
            m_bRuo = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjRestResponsableUO)) Is Nothing)
            m_bRPerfUO = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPRestResponsablePerfUO)) Is Nothing)
            
        Case "frmRESREU"
            m_bRDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREURestResponsableDep)) Is Nothing)
            m_bREqp = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREURestResponsableEqp)) Is Nothing)
            m_bRMat = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREURestResponsableMat)) Is Nothing)
            m_bRuo = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREURestResponsableUO)) Is Nothing)
            m_bRPerfUO = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREURestResponsablePerfUO)) Is Nothing)
            
        Case "frmSELPROVE"
            m_bRDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestResponsableDep)) Is Nothing)
            m_bREqp = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestResponsableEqp)) Is Nothing)
            m_bRMat = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestResponsableMat)) Is Nothing)
            m_bRuo = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestResponsableUO)) Is Nothing)
            m_bRPerfUO = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestResponsablePerfUON)) Is Nothing)
            
        Case "frmOFEPet"
            m_bRDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestResponsableDep)) Is Nothing)
            m_bREqp = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestResponsableEqp)) Is Nothing)
            m_bRMat = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestResponsableMat)) Is Nothing)
            m_bRuo = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestResponsableUO)) Is Nothing)
            m_bRPerfUO = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestResponsablePerfUON)) Is Nothing)
            
        Case "frmOFERec"
            m_bRDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestResponsableDep)) Is Nothing)
            m_bREqp = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestResponsableEqp)) Is Nothing)
            m_bRMat = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestResponsableMat)) Is Nothing)
            m_bRuo = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestResponsableUO)) Is Nothing)
            m_bRPerfUO = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestResponsablePerfUON)) Is Nothing)
                
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub GenerarEstructuraCompras(ByVal OrdenadoPorDen As Boolean)
    Dim oEquipo As CEquipo
    Dim nodx As node
    Dim oCom As CComprador
    Dim sCod As String
    Dim nody As node
    Dim lIdPerfil As Long
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Set m_oEquipos = Nothing
    Set m_oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If m_bREqp And oUsuarioSummit.Tipo = TIpoDeUsuario.comprador Then
        m_oEquipos.CargarTodosLosEquipos oUsuarioSummit.comprador.codEqp, , True, OrdenadoPorDen, False
    Else
        m_oEquipos.CargarTodosLosEquipos , , , OrdenadoPorDen, False
    End If
    
    Set nodx = tvwEstrComp.Nodes.Add(, , "Raiz", m_sRaiz, "Raiz")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    If Not oUsuarioSummit.perfil Is Nothing Then lIdPerfil = oUsuarioSummit.perfil.Id
    
    For Each oEquipo In m_oEquipos
        'If Not m_bPermProcMultimaterial Then
        If g_oProcesoSeleccionado.MaterialProce.Count = 1 Then
            oEquipo.DevolverTodosLosCompradores , , , , , OrdenadoPorDen, False, , m_bRuo, m_bRDep, m_bRMat, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, g_oProcesoSeleccionado.MaterialProce.Item(1).GMN1Cod, g_oProcesoSeleccionado.MaterialProce.Item(1).GMN2Cod, g_oProcesoSeleccionado.MaterialProce.Item(1).GMN3Cod, g_oProcesoSeleccionado.MaterialProce.Item(1).Cod, m_bRPerfUO, lIdPerfil
            If oEquipo.Compradores.Count > 0 Then
                Set nodx = tvwEstrComp.Nodes.Add("Raiz", tvwChild, "EQP" & CStr(oEquipo.Cod), CStr(oEquipo.Cod) & " - " & oEquipo.Den, "Equipo")
                nodx.Tag = "EQP" & CStr(oEquipo.Cod)

                For Each oCom In oEquipo.Compradores
                    sCod = oCom.codEqp & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oCom.codEqp))
                    Set nody = tvwEstrComp.Nodes.Add("EQP" & CStr(oCom.codEqp), tvwChild, "COM" & sCod & CStr(oCom.Cod), CStr(oCom.Cod) & " - " & oCom.Apel & " , " & oCom.nombre, "Comprador")
                    nody.Tag = "COM" & CStr(oCom.Cod)
                Next
            End If
        Else
        
            Dim oMat As CGrupoMatNivel4
            Set oMat = oFSGSRaiz.Generar_CGrupoMatNivel4
            
            oEquipo.DevolverTodosLosCompradoresProceso , , , , , OrdenadoPorDen, False, , m_bRuo, m_bRDep, m_bRMat, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, m_bRPerfUO, lIdPerfil
                        
            If oEquipo.Compradores.Count > 0 Then
                
                Set nodx = tvwEstrComp.Nodes.Add("Raiz", tvwChild, "EQP" & CStr(oEquipo.Cod), CStr(oEquipo.Cod) & " - " & oEquipo.Den, "Equipo")
                nodx.Tag = "EQP" & CStr(oEquipo.Cod)
                    
                For Each oCom In oEquipo.Compradores
                    sCod = oCom.codEqp & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oCom.codEqp))
                    Set nody = tvwEstrComp.Nodes.Add("EQP" & CStr(oCom.codEqp), tvwChild, "COM" & sCod & CStr(oCom.Cod), CStr(oCom.Cod) & " - " & oCom.Apel & " , " & oCom.nombre, "Comprador")
                    nody.Tag = "COM" & CStr(oCom.Cod)
                Next
            End If
        End If
    Next

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "GenerarEstructuraCompras", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub Form_Resize()
    'Redimensiona el form
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Width < 1500 Then Exit Sub
    If Me.Height < 1500 Then Exit Sub
    
    tvwEstrComp.Width = Me.Width - 300
    tvwEstrComp.Height = Me.Height - 1065
    
    cmdAceptar.Top = tvwEstrComp.Top + tvwEstrComp.Height + 85
    cmdCancelar.Top = cmdAceptar.Top
    
    cmdAceptar.Left = tvwEstrComp.Width / 2 - cmdAceptar.Width - 50
    cmdCancelar.Left = tvwEstrComp.Width / 2 + 50
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "Form_Resize", err, Erl)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oProcesoSeleccionado = Nothing
    Set g_oOrigen = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Function DevolverCod(ByVal nodx As node) As Variant
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Function
   
    Select Case Left(nodx.Tag, 3)
    
        Case "EQP"
            DevolverCod = Right(nodx.Tag, Len(nodx.Tag) - 3)
            
        Case "COM"
            DevolverCod = Right(nodx.Tag, Len(nodx.Tag) - 3)
        
    End Select

    Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELPROVEResp", "DevolverCod", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function
