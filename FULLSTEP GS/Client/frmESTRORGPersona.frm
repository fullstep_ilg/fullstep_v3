VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRORGPersona 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5610
   ClientLeft      =   3030
   ClientTop       =   2820
   ClientWidth     =   3930
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRORGPersona.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5610
   ScaleWidth      =   3930
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picFunComp 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   330
      Left            =   75
      ScaleHeight     =   330
      ScaleWidth      =   2880
      TabIndex        =   22
      Top             =   3840
      Width           =   2880
      Begin VB.CheckBox chkFunCompra 
         BackColor       =   &H00808000&
         Caption         =   "Funci�n Compradora"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   75
         TabIndex        =   23
         Top             =   45
         Width           =   2490
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   75
      ScaleHeight     =   420
      ScaleWidth      =   3435
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   4860
      Width           =   3435
      Begin VB.CommandButton cmdCancelar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1725
         TabIndex        =   10
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   585
         TabIndex        =   9
         Top             =   45
         Width           =   1005
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4590
      Left            =   1470
      ScaleHeight     =   4590
      ScaleWidth      =   2400
      TabIndex        =   18
      Top             =   105
      Width           =   2400
      Begin VB.TextBox txtMail 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   100
         TabIndex        =   7
         Top             =   3375
         Width           =   2220
      End
      Begin VB.TextBox txtTfno2 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   20
         TabIndex        =   5
         Top             =   2460
         Width           =   1755
      End
      Begin VB.TextBox txtCod 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         TabIndex        =   0
         Top             =   135
         Width           =   1110
      End
      Begin VB.TextBox txtCargo 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   50
         TabIndex        =   3
         Top             =   1515
         Width           =   2220
      End
      Begin VB.TextBox txtNom 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   50
         TabIndex        =   1
         Top             =   595
         Width           =   2220
      End
      Begin VB.TextBox txtApel 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   100
         TabIndex        =   2
         Top             =   1055
         Width           =   2220
      End
      Begin VB.TextBox txtTfno 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   20
         TabIndex        =   4
         Top             =   1975
         Width           =   1755
      End
      Begin VB.TextBox txtFax 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   60
         MaxLength       =   20
         TabIndex        =   6
         Top             =   2910
         Width           =   1740
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEquipo 
         Height          =   285
         Left            =   0
         TabIndex        =   8
         Top             =   4200
         Width           =   2220
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         BevelColorFace  =   13226710
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3281
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3201
         Columns(1).Caption=   "CodEqp"
         Columns(1).Name =   "COD_EQP"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3916
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 3"
      End
   End
   Begin VB.Label lblEquipo 
      BackColor       =   &H00808000&
      Caption         =   "Equipo"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   450
      TabIndex        =   21
      Top             =   4360
      Width           =   765
   End
   Begin VB.Label lblTfno2 
      BackColor       =   &H00808000&
      Caption         =   "Tfno. 2:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   20
      Top             =   2580
      Width           =   1050
   End
   Begin VB.Label lblCod 
      BackColor       =   &H00808000&
      Caption         =   "C�digo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   19
      Top             =   300
      Width           =   1050
   End
   Begin VB.Label lblMail 
      BackColor       =   &H00808000&
      Caption         =   "E-mail:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   16
      Top             =   3540
      Width           =   1050
   End
   Begin VB.Label lblFax 
      BackColor       =   &H00808000&
      Caption         =   "Fax:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   15
      Top             =   3060
      Width           =   1050
   End
   Begin VB.Label lblTfno 
      BackColor       =   &H00808000&
      Caption         =   "Tfno. 1:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   14
      Top             =   2160
      Width           =   1050
   End
   Begin VB.Label lblApel 
      BackColor       =   &H00808000&
      Caption         =   "Apellidos:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   13
      Top             =   1200
      Width           =   1050
   End
   Begin VB.Label lblNom 
      BackColor       =   &H00808000&
      Caption         =   "Nombre:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   12
      Top             =   780
      Width           =   1050
   End
   Begin VB.Label lblCargo 
      BackColor       =   &H00808000&
      Caption         =   "Cargo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   11
      Top             =   1680
      Width           =   1050
   End
End
Attribute VB_Name = "frmESTRORGPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'HBA

Private sIdiCod As String
Private sIdiApel As String
Private sIdiEliminar As String
Private sIdiDetalle As String
Private sIdiA�adir As String
Private sIdiModificar As String
Public frmOrigen As Form
' Variables de seguridad
Private bREqp As Boolean
'Private bRMatAsig As Boolean
Private bModif As Boolean

Private oEqps As CEquipos
Private oEqpSeleccionado As CEquipo
Private CargarComboDesdeEqp As Boolean
Private CargarComboDesdeComp As Boolean
Private RespetarCombo As Boolean


Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String
Private Sub chkFunCompra_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        If Not bModif Then Exit Sub
        
        If chkFunCompra.Value = vbChecked Then
            If bREqp Then
                If IsEmpty(basOptimizacion.gCodEqpUsuario) Then
                    chkFunCompra.Enabled = False
                    sdbcEquipo.Enabled = False
                Else
                    If oEqps Is Nothing Then
                        Set oEqps = oFSGSRaiz.Generar_CEquipos
                        oEqps.CargarTodosLosEquiposDesde 1, basOptimizacion.gCodEqpUsuario, , False, False
                    End If
                    If oEqps.Count > 0 Then
                        sdbcEquipo.RemoveAll
                        sdbcEquipo.Text = oEqps.Item(basOptimizacion.gCodEqpUsuario).Cod & " - " & oEqps.Item(basOptimizacion.gCodEqpUsuario).Den
                        sdbcEquipo.MoveFirst
                    Else
                        chkFunCompra.Value = vbUnchecked
                    End If
                End If
            End If
        Else
            sdbcEquipo.Text = ""
        End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "chkFunCompra_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim sEquipo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Screen.MousePointer = vbHourglass
teserror.NumError = TESnoerror
Screen.MousePointer = vbNormal

Select Case frmESTRORG.Accion

Case ACCPerAnya
        
        '******* Validar Datos
        If Trim(txtCod) = "" Then
'            oMensajes.NoValido "Codigo"
            oMensajes.NoValido sIdiCod
            If Me.Visible Then txtCod.SetFocus
            Exit Sub
        End If
        
        If Trim(txtApel) = "" Then
'            oMensajes.NoValido "Apellidos"
            oMensajes.NoValido sIdiApel
            If Me.Visible Then txtApel.SetFocus
            Exit Sub
        End If
        
        frmESTRORG.oPersonaSeleccionada.Cod = Trim(txtCod)
        frmESTRORG.oPersonaSeleccionada.nombre = StrToNull(Trim(txtNom))
        frmESTRORG.oPersonaSeleccionada.Apellidos = Trim(txtApel)
        frmESTRORG.oPersonaSeleccionada.Cargo = StrToNull(Trim(txtCargo))
        frmESTRORG.oPersonaSeleccionada.Fax = StrToNull(Trim(txtFax))
        frmESTRORG.oPersonaSeleccionada.mail = StrToNull(Trim(txtMail))
        frmESTRORG.oPersonaSeleccionada.Tfno = StrToNull(Trim(txtTfno))
        frmESTRORG.oPersonaSeleccionada.Tfno2 = StrToNull(Trim(txtTfno2))
        
        If chkFunCompra.Value = vbChecked Then
            If sdbcEquipo.Text = "" Then
                sEquipo = Mid(lblEquipo, 1, Len(lblEquipo) - 1)
                oMensajes.FaltanDatos sEquipo 'lblEquipo.Caption
                Exit Sub
            End If
            If bREqp Then
                frmESTRORG.oPersonaSeleccionada.codEqp = basOptimizacion.gCodEqpUsuario
            Else
                frmESTRORG.oPersonaSeleccionada.codEqp = StrToNull(Trim(sdbcEquipo.Columns(0).Text))
            End If
        Else
            frmESTRORG.oPersonaSeleccionada.codEqp = Null
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = frmESTRORG.oIBaseDatos.AnyadirABaseDatos
        Screen.MousePointer = vbNormal
        
        If teserror.NumError = TESnoerror Then
            AnyadirPersonaAEstructura
        Else
            TratarError teserror
            Exit Sub
        End If
    
 
 Case ACCPerMod
           
        If Trim(txtApel) = "" Then
'            oMensajes.NoValido "Apellidos"
            oMensajes.NoValido sIdiApel
            If Me.Visible Then txtNom.SetFocus
            Exit Sub
        End If
        
        frmESTRORG.oPersonaSeleccionada.nombre = StrToNull(Trim(txtNom))
        frmESTRORG.oPersonaSeleccionada.Apellidos = Trim(txtApel)
        frmESTRORG.oPersonaSeleccionada.Cargo = StrToNull(Trim(txtCargo))
        frmESTRORG.oPersonaSeleccionada.Fax = StrToNull(Trim(txtFax))
        frmESTRORG.oPersonaSeleccionada.mail = StrToNull(Trim(txtMail))
        frmESTRORG.oPersonaSeleccionada.Tfno = StrToNull(Trim(txtTfno))
        frmESTRORG.oPersonaSeleccionada.Tfno2 = StrToNull(Trim(txtTfno2))
        
        If chkFunCompra.Value = vbChecked Then
            If sdbcEquipo.Text = "" Then
                oMensajes.FaltanDatos lblEquipo.caption
                Exit Sub
            End If
            If bREqp Then
                frmESTRORG.oPersonaSeleccionada.codEqp = basOptimizacion.gCodEqpUsuario
            Else
                If frmESTRORG.oPersonaSeleccionada.codEqp = StrToNull(Trim(sdbcEquipo.Columns(1).Value)) Then
                    frmESTRORG.oPersonaSeleccionada.codEqp = StrToNull(Trim(sdbcEquipo.Columns(1).Value))
                Else
                    frmESTRORG.oPersonaSeleccionada.codEqp = StrToNull(Trim(sdbcEquipo.Columns(0).Value))
                End If
            End If
        Else
            frmESTRORG.oPersonaSeleccionada.codEqp = Null
        End If
            
        Screen.MousePointer = vbHourglass
        teserror = frmESTRORG.oIBaseDatos.FinalizarEdicionModificando
        Screen.MousePointer = vbNormal
        
        If teserror.NumError = TESnoerror Then
            ModificarPersonaEnEstructura
        Else
            Screen.MousePointer = vbNormal
            TratarError teserror
            'oIBaseDatos.CancelarEdicion
            'Set oIBaseDatos = Nothing
            Exit Sub
        End If
        
End Select

Set frmESTRORG.oIBaseDatos = Nothing

Screen.MousePointer = vbHourglass
frmESTRORG.Accion = accionessummit.ACCUOCon

frmESTRORG.tvwestrorg_NodeClick frmESTRORG.tvwestrorg.selectedItem

Screen.MousePointer = vbNormal

Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
If frmOrigen Is Nothing Then

    Select Case frmESTRORG.Accion
        
        Case ACCPerDet
                picDatos.Enabled = False
                picEdit.Visible = False
                picFunComp.Enabled = False
                Me.Height = Me.Height - 300
                caption = sIdiDetalle
        Case ACCPerAnya
                If Me.Visible Then txtCod.SetFocus
                caption = sIdiA�adir
        Case ACCPerMod
                txtCod.Enabled = False
                If Me.Visible Then txtNom.SetFocus
                caption = sIdiModificar
    End Select
Else
    picDatos.Enabled = False
    picFunComp.Enabled = False
    picEdit.Visible = False
    Me.Height = Me.Height - 300
    caption = sIdiDetalle

End If

If FSEPConf Then
    chkFunCompra.Visible = False
    sdbcEquipo.Visible = False
    lblEquipo.Visible = False
    frmESTRORGPersona.Height = 5250
    picEdit.Top = 4200
    picDatos.Height = 4000
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True

Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPER
CargarRecursos

    PonerFieldSeparator Me

If frmOrigen Is Nothing Then
    Select Case frmESTRORG.Accion
    
        Case ACCPerMod, ACCPerEli
            
            txtCod = frmESTRORG.oPersonaSeleccionada.Cod
            txtNom = NullToStr(frmESTRORG.oPersonaSeleccionada.nombre)
            txtApel = frmESTRORG.oPersonaSeleccionada.Apellidos
            txtCargo = NullToStr(frmESTRORG.oPersonaSeleccionada.Cargo)
            txtTfno = NullToStr(frmESTRORG.oPersonaSeleccionada.Tfno)
            txtTfno2 = NullToStr(frmESTRORG.oPersonaSeleccionada.Tfno2)
            txtFax = NullToStr(frmESTRORG.oPersonaSeleccionada.Fax)
            txtMail = NullToStr(frmESTRORG.oPersonaSeleccionada.mail)
            If Not IsNull(frmESTRORG.oPersonaSeleccionada.codEqp) And Not IsEmpty(frmESTRORG.oPersonaSeleccionada.codEqp) Then
                chkFunCompra.Value = vbChecked
                sdbcEquipo.Enabled = True
                Set oEqpSeleccionado = Nothing
                Set oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
        
                oEqpSeleccionado.Cod = frmESTRORG.oPersonaSeleccionada.codEqp
                sdbcEquipo.AddItem frmESTRORG.oPersonaSeleccionada.codEqp & "  " & frmESTRORG.oPersonaSeleccionada.DenEqp & Chr(m_lSeparador) & frmESTRORG.oPersonaSeleccionada.codEqp
                RespetarCombo = True
                sdbcEquipo.Text = sdbcEquipo.Columns(0).Value
                RespetarCombo = False
            Else
                chkFunCompra.Value = Unchecked
                sdbcEquipo.Enabled = False
                sdbcEquipo.Columns(0).Value = ""
            End If
    End Select
    ConfigurarSeguridad

End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bModif = False
    bREqp = False
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPModificar)) Is Nothing) Then
        bModif = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPRestEquipo)) Is Nothing) Then
        bREqp = True
    End If
    
    If Not bModif Then
        chkFunCompra.Enabled = False
        sdbcEquipo.Enabled = False
    Else
        chkFunCompra.Enabled = True
        sdbcEquipo.Enabled = True
        If bREqp Then
            sdbcEquipo.AllowInput = False
            If IsEmpty(basOptimizacion.gCodEqpUsuario) Then
                chkFunCompra.Enabled = False
                sdbcEquipo.Enabled = False
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False
If frmOrigen Is Nothing Then
    Select Case frmESTRORG.Accion
    
        Case ACCPerMod, ACCPerEli
            
                frmESTRORG.oIBaseDatos.CancelarEdicion
    End Select
    
    frmESTRORG.Accion = ACCUOCon
    
    Set frmESTRORG.oPersonaSeleccionada = Nothing
    Set frmESTRORG.oIBaseDatos = Nothing
Else
    Set frmOrigen = Nothing
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub sdbcEquipo_DropDown()
 Dim Codigos As TipoDatosCombo
 Dim i As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcEquipo.Enabled Then
       If Not IsNull(frmESTRORG.oPersonaSeleccionada.codEqp) Then
          sdbcEquipo.Text = frmESTRORG.oPersonaSeleccionada.codEqp & " - " & frmESTRORG.oPersonaSeleccionada.DenEqp
       Else
          sdbcEquipo.Text = ""
       End If
       Exit Sub
    End If
    If bREqp Then
        sdbcEquipo.DroppedDown = False
        Exit Sub
    End If
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    Screen.MousePointer = vbHourglass
    
    sdbcEquipo.RemoveAll
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEquipo.Columns(0).Value), False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, True, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEquipo.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEquipo.AddItem "..."
    End If

    sdbcEquipo.SelStart = 0
    sdbcEquipo.SelLength = Len(sdbcEquipo.Columns(0).Value)
    sdbcEquipo.Refresh
    
    Screen.MousePointer = vbNormal

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "sdbcEquipo_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub sdbcEquipo_Change()
      
    If Not RespetarCombo Then
    
        RespetarCombo = False
        sdbcEquipo.Columns(0).Text = ""
        sdbcEquipo.Columns(1).Text = ""
        'ChKFunCompra.Value = Unchecked
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
        
    End If
          
End Sub

Private Sub sdbcEquipo_KeyPress(KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bREqp Then KeyAscii = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "sdbcEquipo_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcEquipo_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcEquipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEquipo.Rows - 1
            bm = sdbcEquipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEquipo.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEquipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "sdbcEquipo_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcEquipo_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If bREqp Then Exit Sub
    If Not sdbcEquipo.DroppedDown Then
        sdbcEquipo = ""
    End If
    
    
    
    chkFunCompra.Value = vbChecked
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "sdbcEquipo_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcEquipo_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEquipo.Value = "..." Then
        sdbcEquipo.Columns(0).Value = ""
        sdbcEquipo.Columns(1).Value = ""
        Exit Sub
    End If
    
    If sdbcEquipo.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcEquipo.Columns(0).Value = sdbcEquipo.Columns(0).Text
    sdbcEquipo.Columns(1).Value = sdbcEquipo.Columns(1).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass

    Set oEqpSeleccionado = oEqps.Item(sdbcEquipo.Columns(0).Text)
    CargarComboDesdeEqp = False
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "sdbcEquipo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub sdbcEquipo_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcEquipo.DataFieldList = "Column 0"
    sdbcEquipo.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "sdbcEquipo_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub sdbcEquipo_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    Dim sbuscar As String
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcEquipo.Text = sdbcEquipo.Columns(0).Text Or bREqp Then Exit Sub

    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEquipo.Columns(0).Value = "" Then
        sdbcEquipo.Text = ""
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    
    If sdbcEquipo.Columns(0).Text = "" Then
       sbuscar = "NULL"
    Else
        sbuscar = sdbcEquipo.Columns(0).Text
    End If
       
    oEquipos.CargarTodosLosEquipos sbuscar, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEquipo.Text = ""
    Else
        RespetarCombo = True
        sdbcEquipo.Text = oEquipos.Item(1).Cod & " - " & oEquipos.Item(1).Den
        sdbcEquipo.Columns(0).Value = oEquipos.Item(1).Cod
        sdbcEquipo.Columns(1).Value = oEquipos.Item(1).Den
        
        RespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
    
    
        
    End If
    
    Set oEquipos = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "sdbcEquipo_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub AnyadirPersonaAEstructura()
Dim nodx As node
Dim node As node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set node = frmESTRORG.tvwestrorg.selectedItem
If Not IsNull(frmESTRORG.oPersonaSeleccionada.codEqp) Then
    Set nodx = frmESTRORG.tvwestrorg.Nodes.Add(node.key, tvwChild, "PERS" & Trim(txtCod), Trim(txtCod) & " - " & Trim(txtApel) & " " & Trim(txtNom), "PerCesta")
Else
    Set nodx = frmESTRORG.tvwestrorg.Nodes.Add(node.key, tvwChild, "PERS" & Trim(txtCod), Trim(txtCod) & " - " & Trim(txtApel) & " " & Trim(txtNom), "Persona")
End If
nodx.Tag = "PER" & Mid(node.Tag, 4, 1) & Trim(txtCod)
nodx.EnsureVisible
Set nodx = Nothing
Set node = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "AnyadirPersonaAEstructura", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
Private Sub ModificarPersonaEnEstructura()
Dim nodx As node
 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set nodx = frmESTRORG.tvwestrorg.selectedItem

nodx.Text = Trim(txtCod) & " - " & Trim(txtApel) & " " & Trim(txtNom)

If Not IsNull(StrToNull(Trim(sdbcEquipo.Value))) Then
    nodx.Image = "PerCesta"
Else
    nodx.Image = "Persona"
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "ModificarPersonaEnEstructura", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub



Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRORG_PERSONA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        lblCod.caption = Ador(0).Value
        Ador.MoveNext
        lblNom.caption = Ador(0).Value
        Ador.MoveNext
        lblApel.caption = Ador(0).Value
        Ador.MoveNext
        lblCargo.caption = Ador(0).Value
        Ador.MoveNext
        lblTfno.caption = Ador(0).Value
        Ador.MoveNext
        lblTfno2.caption = Ador(0).Value
        Ador.MoveNext
        lblFax.caption = Ador(0).Value
        Ador.MoveNext
        lblMail.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiCod = Ador(0).Value
        Ador.MoveNext
        sIdiApel = Ador(0).Value
        Ador.MoveNext
        sIdiEliminar = Ador(0).Value
        Ador.MoveNext
        sIdiDetalle = Ador(0).Value
        Ador.MoveNext
        sIdiA�adir = Ador(0).Value
        Ador.MoveNext
        sIdiModificar = Ador(0).Value
        Ador.MoveNext
        lblEquipo.caption = Ador(0).Value
        Ador.MoveNext
        chkFunCompra.caption = Ador(0).Value
        Ador.MoveNext
        sdbcEquipo.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcEquipo.Columns(1).caption = Ador(0).Value
        
        Ador.Close
    
    End If

    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGPersona", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub





