VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroApliMat 
   Caption         =   "Informe de ahorros aplicados por material"
   ClientHeight    =   5505
   ClientLeft      =   45
   ClientTop       =   1860
   ClientWidth     =   11715
   Icon            =   "frmInfAhorroApliMat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5505
   ScaleWidth      =   11715
   Begin VB.PictureBox picTipoGrafico 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      FillStyle       =   0  'Solid
      ForeColor       =   &H80000008&
      Height          =   465
      Left            =   5220
      ScaleHeight     =   465
      ScaleWidth      =   3660
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   150
      Visible         =   0   'False
      Width           =   3660
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
         Height          =   285
         Left            =   630
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   90
         Width           =   1755
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Row.Count       =   5
         Row(0)          =   "Barras 2D"
         Row(1)          =   "Barras 3D"
         Row(2)          =   "Lineas 2D"
         Row(3)          =   "Lineas 3D"
         Row(4)          =   "Tarta"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "TIPO"
         Columns(0).Name =   "TIPO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3096
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picLegend 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   10110
      Picture         =   "frmInfAhorroApliMat.frx":0CB2
      ScaleHeight     =   735
      ScaleWidth      =   1635
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.PictureBox picLegend2 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   10095
      Picture         =   "frmInfAhorroApliMat.frx":11AC
      ScaleHeight     =   615
      ScaleWidth      =   1635
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   60
      Visible         =   0   'False
      Width           =   1635
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   0
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   5460
      Width           =   11715
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroApliMat.frx":1659
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroApliMat.frx":1675
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroApliMat.frx":1691
      DividerType     =   1
      DividerStyle    =   2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   7
      Columns(0).Width=   2275
      Columns(0).Caption=   "CAPTION"
      Columns(0).Name =   "CAPTION"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "PRESCAPTION"
      Columns(1).Name =   "PRESCAPTION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "PRES"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,#"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "ADJCAPTION"
      Columns(3).Name =   "ADJCAPTION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "ADJ"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,#"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2090
      Columns(5).Caption=   "AHOCAPTION"
      Columns(5).Name =   "AHOCAPTION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "AHO"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "#,#"
      Columns(6).FieldLen=   256
      _ExtentX        =   20664
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   2865
      Left            =   0
      TabIndex        =   15
      Top             =   1680
      Width           =   11655
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   9
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroApliMat.frx":16AD
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4744445
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroApliMat.frx":16C9
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroApliMat.frx":16E5
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "Normal"
      SplitterPos     =   2
      SplitterVisible =   -1  'True
      Columns.Count   =   9
      Columns(0).Width=   1799
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   2937
      Columns(1).Caption=   "Denominación"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16776960
      Columns(2).Width=   2884
      Columns(2).Caption=   "Presupuesto total"
      Columns(2).Name =   "PRESTOT"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   15400959
      Columns(3).Width=   2752
      Columns(3).Caption=   "Consumido"
      Columns(3).Name =   "PRES"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   15400959
      Columns(4).Width=   2752
      Columns(4).Caption=   "Adjudicado"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   15400959
      Columns(5).Width=   2699
      Columns(5).Caption=   "Ahorro"
      Columns(5).Name =   "AHO"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "Standard"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1376
      Columns(6).Caption=   "%"
      Columns(6).Name =   "PORCEN"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "0.0#\%"
      Columns(6).FieldLen=   256
      Columns(7).Width=   1323
      Columns(7).Caption=   "Obj."
      Columns(7).Name =   "OBJ"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   2
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).NumberFormat=   "0.0#\%"
      Columns(7).FieldLen=   256
      Columns(7).HasBackColor=   -1  'True
      Columns(7).BackColor=   15400959
      Columns(8).Width=   1085
      Columns(8).Caption=   "Hist..."
      Columns(8).Name =   "HIST"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Style=   4
      Columns(8).ButtonsAlways=   -1  'True
      _ExtentX        =   20558
      _ExtentY        =   5054
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   3735
      Left            =   0
      OleObjectBlob   =   "frmInfAhorroApliMat.frx":1701
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   1680
      Width           =   11715
   End
   Begin VB.Frame fraResMat 
      Caption         =   "Mostrar resultados desde material"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   0
      TabIndex        =   30
      Top             =   660
      Width           =   11730
      Begin VB.CommandButton cmdBorrar 
         Height          =   315
         Left            =   10110
         Picture         =   "frmInfAhorroApliMat.frx":3127
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   360
         Width           =   345
      End
      Begin VB.CommandButton cmdSelMat 
         Height          =   315
         Left            =   10485
         Picture         =   "frmInfAhorroApliMat.frx":31CC
         Style           =   1  'Graphical
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   360
         Width           =   345
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   1005
         TabIndex        =   7
         Top             =   210
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
         Height          =   285
         Left            =   5925
         TabIndex        =   9
         Top             =   195
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
         Height          =   285
         Left            =   1005
         TabIndex        =   11
         Top             =   570
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
         Height          =   285
         Left            =   5925
         TabIndex        =   13
         Top             =   570
         Width           =   945
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1667
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
         Height          =   285
         Left            =   6885
         TabIndex        =   10
         Top             =   195
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
         Height          =   285
         Left            =   1965
         TabIndex        =   12
         Top             =   570
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
         Height          =   285
         Left            =   6885
         TabIndex        =   14
         Top             =   570
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
         Height          =   285
         Left            =   1965
         TabIndex        =   8
         Top             =   210
         Width           =   2985
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5265
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblGMN3_4 
         Caption         =   "Subfam.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   35
         Top             =   630
         Width           =   765
      End
      Begin VB.Label lblGMN2_4 
         Caption         =   "Fam.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5190
         TabIndex        =   34
         Top             =   240
         Width           =   735
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Comm:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   150
         TabIndex        =   33
         Top             =   240
         Width           =   765
      End
      Begin VB.Label lblGMN4_4 
         Caption         =   "Gru.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5190
         TabIndex        =   32
         Top             =   615
         Width           =   735
      End
   End
   Begin VB.Frame fraSel 
      Height          =   615
      Left            =   0
      TabIndex        =   22
      Top             =   30
      Width           =   10095
      Begin VB.OptionButton optDir 
         Caption         =   "Adj. directa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5250
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   360
         Width           =   1560
      End
      Begin VB.OptionButton optTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6840
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   360
         Width           =   1200
      End
      Begin VB.OptionButton optReu 
         Caption         =   "Adj. en reunión"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5250
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   120
         Value           =   -1  'True
         Width           =   2790
      End
      Begin VB.CommandButton cmdActualizar 
         Height          =   285
         Left            =   9345
         Picture         =   "frmInfAhorroApliMat.frx":3238
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   210
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyoDesde 
         Height          =   285
         Left            =   570
         TabIndex        =   0
         Top             =   210
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMesDesde 
         Height          =   285
         Left            =   1380
         TabIndex        =   1
         Top             =   210
         Width           =   1260
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2222
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyoHasta 
         Height          =   285
         Left            =   3150
         TabIndex        =   2
         Top             =   210
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMesHasta 
         Height          =   285
         Left            =   3960
         TabIndex        =   3
         Top             =   210
         Width           =   1260
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2222
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMon 
         Height          =   285
         Left            =   8085
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   210
         Width           =   795
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1164
         Columns(0).Caption=   "Código"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1879
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "EQUIV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1402
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "SSDBCombo1"
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdGrafico 
         Height          =   285
         Left            =   8955
         Picture         =   "frmInfAhorroApliMat.frx":32C3
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   210
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         Height          =   285
         Left            =   8955
         Picture         =   "frmInfAhorroApliMat.frx":3605
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   210
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         Height          =   285
         Left            =   9720
         Picture         =   "frmInfAhorroApliMat.frx":374F
         Style           =   1  'Graphical
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   210
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.Label lblAnyoHasta 
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2670
         TabIndex        =   29
         Top             =   240
         Width           =   450
      End
      Begin VB.Label lblAnyoDesde 
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   60
         TabIndex        =   28
         Top             =   240
         Width           =   555
      End
   End
End
Attribute VB_Name = "frmInfAhorroApliMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables para func. combos
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

Private oMonedas As CMonedas
Private dequivalencia As Double
Private oMon As CMoneda

Private arResultados() As Variant

'Variables de seguridad
Private bRMat As Boolean

Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4
'Variables para caso de bRMat
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4
' variable para listado
Public ofrmLstApliMat As frmLstINFAhorrosApl
Private oICompAsignado As ICompProveAsignados

Private sIdiMat As String
Private sIdiTotal As String
Private sIdiPresup As String
Private sIdiAdj As String
Private sIdiAhor As String
Private sIdiMonCent As String
Private sIdiTipoGrafico(5) As String
Private sIdiMeses(12) As String
Private sIdiHistRes As String

 Public Sub PonerMatSeleccionadoEnCombos()
    
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    
    BorrarDatosTotales
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    If Not oGMN1Seleccionado Is Nothing Then
     
        bRespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Den.Text = oGMN1Seleccionado.Den
        bRespetarCombo = False
    Else
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN1_4Den.Text = ""
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    If Not oGMN2Seleccionado Is Nothing Then
        
        bRespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Den.Text = oGMN2Seleccionado.Den
        bRespetarCombo = False
    Else
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN2_4Den.Text = ""
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    If Not oGMN3Seleccionado Is Nothing Then
      
        bRespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Den.Text = oGMN3Seleccionado.Den
        bRespetarCombo = False
    Else
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    If Not oGMN4Seleccionado Is Nothing Then
       
        bRespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        sdbcGMN4_4Den.Text = oGMN4Seleccionado.Den
        bRespetarCombo = False
    Else
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
    End If
    
           
End Sub


Private Sub cmdActualizar_Click()
Dim vacio As Integer
Dim iNivelAsig As Integer
        
    'Comprueba que la fecha de inicio no sea mayor que la de fin
    If val(sdbcAnyoDesde) > val(sdbcAnyoHasta) Then
        sdbgRes.RemoveAll
        oMensajes.PeriodoNoValido
        If Me.Visible Then sdbcAnyoDesde.SetFocus
        Exit Sub
    Else
        If val(sdbcAnyoDesde) = val(sdbcAnyoHasta) Then
            If sdbcMesHasta.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1 > sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1 Then
                sdbgRes.RemoveAll
                oMensajes.PeriodoNoValido
                If Me.Visible Then sdbcMesDesde.SetFocus
                Exit Sub
            End If
        End If
    End If

    Screen.MousePointer = vbHourglass
    
    If bRMat Then
        
        Select Case gParametrosGenerales.giNEM
    
            Case 1
                    If sdbcGMN1_4Cod = "" Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido sIdiMat
                        Exit Sub
                    End If
            Case 2
                    If sdbcGMN2_4Cod = "" And sdbcGMN1_4Cod <> "" Then
                        Set oICompAsignado = oGMN1Seleccionado
                     
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        If iNivelAsig < 1 Or iNivelAsig > 2 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                    End If
            Case 3
                    If sdbcGMN3_4Cod = "" And sdbcGMN2_4Cod <> "" Then
                        Set oICompAsignado = oGMN2Seleccionado
                       
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        If iNivelAsig = 0 Or iNivelAsig > 2 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                    Else
                        If sdbcGMN2_4Cod = "" And sdbcGMN1_4Cod <> "" Then
                            Set oICompAsignado = oGMN1Seleccionado
                    
                            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                            If iNivelAsig < 1 Or iNivelAsig > 2 Then
                                Screen.MousePointer = vbNormal
                                oMensajes.InfAhorroMatNoValido
                                Exit Sub
                            End If
                        End If
                    End If
            Case 4
                
                If sdbcGMN4_4Cod = "" And sdbcGMN3_4Cod <> "" Then
                    Set oICompAsignado = oGMN3Seleccionado
                    iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                    'Si el material que tiene asignado el comprador es de nivel 4
                    ' no cargamos nada
                    If iNivelAsig = 0 Or iNivelAsig > 3 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.InfAhorroMatNoValido
                        Exit Sub
                    End If
                Else
                    If sdbcGMN3_4Cod = "" And sdbcGMN2_4Cod <> "" Then
                        Set oICompAsignado = oGMN2Seleccionado
                      
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        If iNivelAsig = 0 Or iNivelAsig > 2 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                    Else
                        If sdbcGMN2_4Cod = "" And sdbcGMN1_4Cod <> "" Then
                            Set oICompAsignado = oGMN1Seleccionado
                           
                            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                            If iNivelAsig < 1 Or iNivelAsig > 2 Then
                                Screen.MousePointer = vbNormal
                                oMensajes.InfAhorroMatNoValido
                                Exit Sub
                            End If
                        Else
                            If sdbcGMN1_4Cod.Value = "" Then
                                Screen.MousePointer = vbNormal
                                oMensajes.InfAhorroMatNoValido
                                Exit Sub
                            End If
                            
                        End If
                    End If
                End If
        
        End Select
    
    End If
    
    arResultados = oGestorInformes.AhorroAplicadoMaterial(val(sdbcAnyoDesde), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, sdbcGMN4_4Cod.Text, optReu, optDir, True)
    
    Screen.MousePointer = vbNormal
    
    On Error GoTo Error:
        
    If LBound(arResultados, 2) < 0 Then
        Exit Sub
    End If
    
    CargarGrid
        
    If cmdGrid.Visible = True Then
        MSChart1.Visible = True
        MostrarGrafico sdbcTipoGrafico.Value
    End If
    
    Exit Sub

Error:
    vacio = 0
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & vacio & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & vacio & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & vacio
    Exit Sub
    
End Sub

Private Sub CargarGrid()
Dim i As Integer
Dim dpres As Double
Dim dadj As Double

    sdbgRes.RemoveAll
    sdbgTotales.RemoveAll
    
    dpres = 0
    dadj = 0
      
    i = 0
    
    While i <= UBound(arResultados, 2)
        
        sdbgRes.AddItem arResultados(0, i) & Chr(m_lSeparador) & arResultados(1, i) & Chr(m_lSeparador) & dequivalencia * arResultados(2, i) & Chr(m_lSeparador) & dequivalencia * arResultados(4, i) & Chr(m_lSeparador) & dequivalencia * arResultados(5, i) & Chr(m_lSeparador) & dequivalencia * arResultados(6, i) & Chr(m_lSeparador) & arResultados(7, i) & Chr(m_lSeparador) & arResultados(3, i)
        dpres = dpres + dequivalencia * arResultados(4, i)
        dadj = dadj + dequivalencia * arResultados(5, i)
        i = i + 1
    
    Wend
    
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & dpres & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & dadj & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & dpres - dadj
    
    Erase arResultados

End Sub

Private Sub cmdBorrar_Click()
    sdbcGMN1_4Cod = ""
End Sub

Private Sub cmdGrafico_Click()
            
    If sdbgRes.Rows = 0 Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
'       MostrarGrafico "Barras 3D"
    sdbcTipoGrafico = sIdiTipoGrafico(2)
    MostrarGrafico sIdiTipoGrafico(2)
    picTipoGrafico.Visible = True
    cmdGrafico.Visible = False
    cmdGrid.Visible = True
    sdbgRes.Visible = False
    MSChart1.Visible = True
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdImprimir_Click()
Dim iInd As Integer

    Set ofrmLstApliMat = New frmLstINFAhorrosApl
    ofrmLstApliMat.sOrigen = "frmInfAhorroApliMat"
    ofrmLstApliMat.PonerMatSeleccionado ("frmInfAhorroApliMat")
    ofrmLstApliMat.WindowState = vbNormal
    
    ofrmLstApliMat.sdbcAnyoDesde.Text = sdbcAnyoDesde.Text
    ofrmLstApliMat.sdbcAnyoHasta.Text = sdbcAnyoHasta.Text
    ofrmLstApliMat.sdbcMesDesde.MoveFirst
    For iInd = 1 To sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark)
        ofrmLstApliMat.sdbcMesDesde.MoveNext
    Next
    ofrmLstApliMat.sdbcMesDesde.Text = ofrmLstApliMat.sdbcMesDesde.Columns(0).Value
    
    ofrmLstApliMat.sdbcMesHasta.MoveFirst
    For iInd = 1 To sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark)
        ofrmLstApliMat.sdbcMesHasta.MoveNext
    Next
    ofrmLstApliMat.sdbcMesHasta.Text = ofrmLstApliMat.sdbcMesHasta.Columns(0).Value
    
    ofrmLstApliMat.optReu.Value = optReu.Value
    ofrmLstApliMat.optDir.Value = optDir.Value
    ofrmLstApliMat.optTodos.Value = optTodos.Value
    
    ofrmLstApliMat.sdbcMon.Text = sdbcMon.Text
    ofrmLstApliMat.sdbcMon_Validate False
    ofrmLstApliMat.Show 1
    ' hay que crear los objetos oGMNxSeleccionado para las restricciones por material
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    Set oMonedas = Nothing
    Set oMon = Nothing
    Set oICompAsignado = Nothing
    Set oGruposMN1 = Nothing
    Set oGruposMN2 = Nothing
    Set oGruposMN3 = Nothing
    Set oGruposMN4 = Nothing
    Me.Visible = False
    
End Sub

Private Sub optDir_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optReu_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optTodos_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub


Private Sub sdbcAnyoDesde_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoDesde_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoHasta_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoHasta_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not bRespetarCombo Then
    
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        
        bRespetarCombo = True
        sdbcGMN1_4Cod = ""
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
        bRespetarCombo = False

        sdbgRes.RemoveAll
        BorrarDatosTotales
    End If
End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
       
    If sdbcGMN1_4Den.Value = "....." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
    
    
End Sub

Private Sub sdbcGMN1_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Den.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
   
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
          
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , True, False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
           
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "....."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcMesDesde_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesDesde_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesHasta_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesHasta_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub


Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_InitColumnProps()
    
    sdbcTipoGrafico.DataFieldList = "Column 0"
    sdbcTipoGrafico.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    Select Case Tipo
    
'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                picLegend.Visible = True
                picLegend2.Visible = False
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                picLegend.Visible = False
                picLegend2.Visible = True
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                picLegend.Visible = True
                picLegend2.Visible = False
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
    
End Sub

Private Sub cmdGrid_Click()
    
    picTipoGrafico.Visible = False
    cmdGrafico.Visible = True
    picLegend.Visible = False
    picLegend2.Visible = False
        
    cmdGrid.Visible = False
    sdbgRes.Visible = True
    MSChart1.Visible = False

End Sub

Private Sub cmdSelMat_Click()
      
    frmSELMAT.sOrigen = "frmInfAhorroApliMat"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Hide
    frmSELMAT.Show 1

End Sub

Private Sub Form_Load()
    
    Me.Width = 11835
    Me.Height = 6195
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    CargarAnyos
    ConfigurarSeguridad
    ConfigurarNombres
    sdbgRes.ScrollBars = ssScrollBarsVertical
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        dequivalencia = oMonedas.Item(1).Equiv
    End If
       
End Sub
Private Sub ConfigurarNombres()

lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
lblGMN2_4.caption = gParametrosGenerales.gsABR_GMN2 & ":"
lblGMN3_4.caption = gParametrosGenerales.gsabr_GMN3 & ":"
lblGMN4_4.caption = gParametrosGenerales.gsabr_GMN4 & ":"
End Sub

Private Sub BorrarDatosTotales()
    
    sdbgTotales.RemoveAll
    sdbgTotales.Columns(6).CellStyleSet "Normal"
    sdbgTotales.Refresh

End Sub

Private Sub CargarAnyos()
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyoDesde.AddItem iInd
        sdbcAnyoHasta.AddItem iInd
    Next
    
    sdbcAnyoDesde.Text = iAnyoActual
    sdbcAnyoDesde.ListAutoPosition = True
    sdbcAnyoDesde.Scroll 1, 7
    sdbcAnyoHasta.Text = iAnyoActual
    sdbcAnyoHasta.ListAutoPosition = True
    sdbcAnyoHasta.Scroll 1, 7
    sdbcMesDesde.Text = "1"
    sdbcMesDesde.ListAutoPosition = True
    sdbcMesHasta.Text = Month(Date) - 1
    sdbcMesHasta.ListAutoPosition = True
       
    sdbcMesDesde.AddItem sIdiMeses(1)
    sdbcMesDesde.AddItem sIdiMeses(2)
    sdbcMesDesde.AddItem sIdiMeses(3)
    sdbcMesDesde.AddItem sIdiMeses(4)
    sdbcMesDesde.AddItem sIdiMeses(5)
    sdbcMesDesde.AddItem sIdiMeses(6)
    sdbcMesDesde.AddItem sIdiMeses(7)
    sdbcMesDesde.AddItem sIdiMeses(8)
    sdbcMesDesde.AddItem sIdiMeses(9)
    sdbcMesDesde.AddItem sIdiMeses(10)
    sdbcMesDesde.AddItem sIdiMeses(11)
    sdbcMesDesde.AddItem sIdiMeses(12)
    
    sdbcMesHasta.AddItem sIdiMeses(1)
    sdbcMesHasta.AddItem sIdiMeses(2)
    sdbcMesHasta.AddItem sIdiMeses(3)
    sdbcMesHasta.AddItem sIdiMeses(4)
    sdbcMesHasta.AddItem sIdiMeses(5)
    sdbcMesHasta.AddItem sIdiMeses(6)
    sdbcMesHasta.AddItem sIdiMeses(7)
    sdbcMesHasta.AddItem sIdiMeses(8)
    sdbcMesHasta.AddItem sIdiMeses(9)
    sdbcMesHasta.AddItem sIdiMeses(10)
    sdbcMesHasta.AddItem sIdiMeses(11)
    sdbcMesHasta.AddItem sIdiMeses(12)
    
    sdbcMesDesde.MoveFirst
    sdbcMesDesde.Text = sdbcMesDesde.Columns(0).Value
    
    sdbcMesHasta.MoveFirst
    
    For iInd = 1 To Month(Date) - 2
        sdbcMesHasta.MoveNext
    Next
    
    sdbcMesHasta.Text = sdbcMesHasta.Columns(0).Value
    
End Sub

Private Sub Form_Resize()
Dim W As Double
    
    If Me.Width > 150 Then
            
        W = sdbgRes.Width
        
        sdbgRes.Width = Me.Width - 150
        
        sdbgRes.Columns(0).Width = (sdbgRes.Width) * (sdbgRes.Columns(0).Width / W)
        sdbgRes.Columns(1).Width = (sdbgRes.Width) * (sdbgRes.Columns(1).Width / W)
        sdbgRes.Columns(2).Width = (sdbgRes.Width) * (sdbgRes.Columns(2).Width / W)
        sdbgRes.Columns(3).Width = (sdbgRes.Width) * (sdbgRes.Columns(3).Width / W)
        sdbgRes.Columns(4).Width = (sdbgRes.Width) * (sdbgRes.Columns(4).Width / W)
        sdbgRes.Columns(5).Width = (sdbgRes.Width) * (sdbgRes.Columns(5).Width / W)
        sdbgRes.Columns(6).Width = (sdbgRes.Width) * (sdbgRes.Columns(6).Width / W)
        sdbgRes.Columns(7).Width = (sdbgRes.Width) * (sdbgRes.Columns(7).Width / W)
        sdbgRes.Columns(8).Width = (sdbgRes.Width) * (sdbgRes.Columns(8).Width / W)
        
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Top = Me.Height - 780
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Columns(0).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(1).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(2).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(3).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(4).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(5).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(6).Width = sdbgTotales.Width / 7
        MSChart1.Width = Me.Width - 150
        
    End If
    
    If Me.Height > 2565 Then
        sdbgRes.Height = Me.Height - 2565
        MSChart1.Height = Me.Height - 2265
    End If
    

        
End Sub

Private Sub sdbcGMN1_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        
        bRespetarCombo = True
        sdbcGMN1_4Den = ""
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
        bRespetarCombo = False
        
        sdbgRes.RemoveAll
        MSChart1.Visible = False
        BorrarDatosTotales
    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
    
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
        
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
           
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub
Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
        
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
        bRespetarCombo = False
        GMN1Seleccionado
        Exit Sub
    End If
        
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text Then
        bRespetarCombo = True
        sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
        bRespetarCombo = False
        GMN1Seleccionado
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
        
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
       
        Screen.MousePointer = vbHourglass
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False)
        Screen.MousePointer = vbNormal
        
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
           
            bCargarComboDesde = False
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
      
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
            
            bRespetarCombo = True
            sdbcGMN1_4Den = oGMN1.Den
            bRespetarCombo = False
            bCargarComboDesde = False
            
        End If
    
    End If
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
        BorrarDatosTotales
    End If
    
End Sub

Private Sub sdbcGMN2_4Cod_CloseUp()
    
    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    bRespetarCombo = False
    
    GMN2Seleccionado
    bCargarComboDesde = False
   
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
    
    sdbcGMN2_4Cod.RemoveAll
    
    Set oGruposMN2 = Nothing
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , False, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
        
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , False, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    End If
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Cod_InitColumnProps()

    sdbcGMN2_4Cod.DataFieldList = "Column 0"
    sdbcGMN2_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub
Private Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN2 As CGrupoMatNivel2
    Dim oIMAsig As IMaterialAsignado

    If oGMN1Seleccionado Is Nothing Then Exit Sub
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub

    If sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If

    ''' Solo continuamos si existe el grupo


    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador

        Screen.MousePointer = vbHourglass
        Set oGruposMN2 = Nothing
        Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Screen.MousePointer = vbNormal

        If oGruposMN2 Is Nothing Then
            'No existe
            sdbcGMN2_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            If oGruposMN2.Item(1) Is Nothing Then
                'No existe
                sdbcGMN2_4Cod.Text = ""
                oMensajes.NoValido sIdiMat
            Else
                bRespetarCombo = True
                sdbcGMN2_4Den.Text = oGruposMN2.Item(1).Den
                bRespetarCombo = False
                Set oGMN2Seleccionado = oGruposMN2.Item(1)
                bCargarComboDesde = False
            End If
        End If
    Else
        Set oGMN2 = Nothing
        Set oGMN2 = oFSGSRaiz.generar_CGrupoMatNivel2

        oGMN2.GMN1Cod = sdbcGMN1_4Cod
        oGMN2.Cod = sdbcGMN2_4Cod
        Set oIBaseDatos = oGMN2

        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal

        If Not bExiste Then
            sdbcGMN2_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            bRespetarCombo = True
            sdbcGMN2_4Den.Text = oGMN2.Den
            bRespetarCombo = False
            Set oGMN2Seleccionado = oGMN2
            bCargarComboDesde = False
        End If

    End If


    Set oGMN2 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN2 = Nothing
    Set oIMAsig = Nothing
    
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
        BorrarDatosTotales
    
    End If
    
End Sub

Private Sub sdbcGMN2_4Den_CloseUp()
    
    If sdbcGMN2_4Den.Value = "....." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    bRespetarCombo = False
    
    GMN2Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN2_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
        
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , Trim(sdbcGMN2_4Den), True, False)
        Else
           
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN2_4Den), True, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , True, False)
        Else
            
             oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    End If
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "....."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()

    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub
Private Sub sdbcGMN3_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
        BorrarDatosTotales
    
    End If
    
End Sub

Private Sub sdbcGMN3_4Cod_CloseUp()
    
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    bRespetarCombo = False
    
    GMN3Seleccionado
    bCargarComboDesde = False
   
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN3_4Cod.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oGruposMN3 = Nothing
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , False, False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    End If
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Cod_InitColumnProps()

    sdbcGMN3_4Cod.DataFieldList = "Column 0"
    sdbcGMN3_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub
Private Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN3 As CGrupoMatNivel3
    Dim oIMAsig As IMaterialAsignado
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    If sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
      
        Screen.MousePointer = vbHourglass
        Set oGruposMN3 = Nothing
        Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Screen.MousePointer = vbNormal
        
        If oGruposMN3 Is Nothing Then
            'No existe
            sdbcGMN3_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            If oGruposMN2.Item(1) Is Nothing Then
                'No existe
                sdbcGMN3_4Cod.Text = ""
                oMensajes.NoValido sIdiMat
            Else
                bRespetarCombo = True
                sdbcGMN3_4Den.Text = oGruposMN3.Item(1).Den
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
    Else
        Set oGMN3 = Nothing
        Set oGMN3 = oFSGSRaiz.generar_CGrupoMatNivel3
       
        oGMN3.GMN1Cod = sdbcGMN1_4Cod
        oGMN3.GMN2Cod = sdbcGMN2_4Cod
        oGMN3.Cod = sdbcGMN3_4Cod
        Set oIBaseDatos = oGMN3
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN3_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            bRespetarCombo = True
            sdbcGMN3_4Den.Text = oGMN3.Den
            bRespetarCombo = False
            bCargarComboDesde = False
        End If
    
    End If
    
    GMN3Seleccionado
    
    Set oGMN3 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN3 = Nothing
    Set oIMAsig = Nothing
    
End Sub
Private Sub sdbcGMN3_4den_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbgRes.RemoveAll
        BorrarDatosTotales
    
    End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
    
    If sdbcGMN3_4Den.Value = "....." Or sdbcGMN3_4Den.Value = "" Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    bRespetarCombo = False
    
    GMN3Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN3_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN3_4Den.RemoveAll
    
    Set oGruposMN3 = Nothing
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , Trim(sdbcGMN3_4Den), True, False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN3_4Den), True, False
            
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , True, False)
        Else
             
             oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    End If
    
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "....."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()

    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub

Private Sub sdbcGMN4_4Cod_InitColumnProps()

    sdbcGMN4_4Cod.DataFieldList = "Column 0"
    sdbcGMN4_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub
Private Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN4 As CGrupoMatNivel4
    Dim oIMAsig As IMaterialAsignado
    
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    If sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text Then
        bRespetarCombo = True
        sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
        bRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        
        Screen.MousePointer = vbHourglass
        Set oGruposMN4 = Nothing
        Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , False)
        Screen.MousePointer = vbNormal
        
        If oGruposMN4 Is Nothing Then
            'No existe
            sdbcGMN4_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            If oGruposMN4.Item(1) Is Nothing Then
                'No existe
                sdbcGMN4_4Cod.Text = ""
                oMensajes.NoValido sIdiMat
            Else
                bRespetarCombo = True
                sdbcGMN4_4Den.Text = oGruposMN4.Item(1).Den
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
    Else
        Set oGMN4 = Nothing
        Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
     
        oGMN4.GMN1Cod = sdbcGMN1_4Cod
        oGMN4.GMN2Cod = sdbcGMN2_4Cod
        oGMN4.GMN3Cod = sdbcGMN3_4Cod
        oGMN4.Cod = sdbcGMN4_4Cod
        Set oIBaseDatos = oGMN4
        
        Screen.MousePointer = vbHourglass
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        Screen.MousePointer = vbNormal
        
        If Not bExiste Then
            sdbcGMN4_4Cod.Text = ""
            oMensajes.NoValido sIdiMat
        Else
            bRespetarCombo = True
            sdbcGMN4_4Den.Text = oGMN4.Den
            bRespetarCombo = False
            bCargarComboDesde = False
        End If
    
    End If
    
    GMN4Seleccionado
    
    Set oGMN4 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN4 = Nothing
    Set oIMAsig = Nothing
    
End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()

    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub

Private Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
   
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    bRespetarCombo = True
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    bRespetarCombo = False
    
    sdbcGMN2_4Cod = ""
    sdbcGMN2_4Den = ""
    sdbcGMN3_4Cod = ""
    sdbcGMN3_4Den = ""
    
    BorrarDatosTotales
    sdbgRes.RemoveAll
    MSChart1.Visible = False
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2

    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod.Text
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    sdbcGMN3_4Cod = ""
    BorrarDatosTotales
    sdbgRes.RemoveAll
    MSChart1.Visible = False
   
End Sub
Private Sub GMN3Seleccionado()

    Set oGMN3Seleccionado = Nothing
    
    If sdbcGMN3_4Cod.Text = "" Or sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
  
    sdbcGMN4_4Cod = ""
    BorrarDatosTotales
    sdbgRes.RemoveAll
    MSChart1.Visible = False
    
End Sub

Private Sub GMN4Seleccionado()
    
    Set oGMN4Seleccionado = Nothing
    
    If sdbcGMN4_4Cod.Text = "" Or sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN3_4Cod
    
    BorrarDatosTotales
    sdbgRes.RemoveAll
    MSChart1.Visible = False
    
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplMatRestMat)) Is Nothing) And oUsuarioSummit.Tipo = TipoDeUsuario.comprador Then
        bRMat = True
    End If
End Sub

Private Sub sdbgRes_BtnClick()
Dim frm As frmInfAhorroApliDet
Dim ADORs As Ador.Recordset
        
        Screen.MousePointer = vbHourglass
        
        If sdbcGMN3_4Cod <> "" Then
            Set ADORs = oGestorInformes.AhorroAplicadoMaterialHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, sdbgRes.Columns(0).Text, optReu.Value, optDir.Value)
        Else
            If sdbcGMN2_4Cod <> "" Then
                Set ADORs = oGestorInformes.AhorroAplicadoMaterialHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbgRes.Columns(0).Text, , optReu.Value, optDir.Value)
            Else
                If sdbcGMN1_4Cod <> "" Then
                    Set ADORs = oGestorInformes.AhorroAplicadoMaterialHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sdbcGMN1_4Cod.Text, sdbgRes.Columns(0).Text, , , optReu.Value, optDir.Value)
                Else
                    Set ADORs = oGestorInformes.AhorroAplicadoMaterialHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sdbgRes.Columns(0).Text, , , , optReu.Value, optDir.Value)
                End If
            End If
        End If
        
        If Not ADORs Is Nothing Then
            Set frm = New frmInfAhorroApliDet
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            frm.caption = sIdiHistRes
    
            While Not ADORs.EOF
                frm.sdbgRes.AddItem ADORs(0).Value & " - " & ADORs(1).Value & Chr(m_lSeparador) & dequivalencia * ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & ADORs(5).Value
                ADORs.MoveNext
            Wend
            ADORs.Close
            Set ADORs = Nothing
        End If
            
        frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 1000
        frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
        
        Screen.MousePointer = vbNormal
        
        frm.Show 1
        sdbgRes.SelBookmarks.RemoveAll
        
End Sub

Private Sub sdbgRes_DblClick()
    
    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    
    If sdbcGMN3_4Cod <> "" Then
        bRespetarCombo = True
        sdbcGMN4_4Cod = sdbgRes.Columns(0).Value
        sdbcGMN4_4Den = sdbgRes.Columns(1).Value
        bRespetarCombo = False
        GMN4Seleccionado
            
    Else
        If sdbcGMN2_4Cod <> "" Then
            bRespetarCombo = True
            sdbcGMN3_4Cod = sdbgRes.Columns(0).Value
            sdbcGMN3_4Den = sdbgRes.Columns(1).Value
            bRespetarCombo = False
            GMN3Seleccionado
        Else
            If sdbcGMN1_4Cod <> "" Then
                bRespetarCombo = True
                sdbcGMN2_4Cod = sdbgRes.Columns(0).Value
                sdbcGMN2_4Den = sdbgRes.Columns(1).Value
                bRespetarCombo = False
                GMN2Seleccionado
            Else
                bRespetarCombo = True
                sdbcGMN1_4Cod = sdbgRes.Columns(0).Value
                sdbcGMN1_4Den = sdbgRes.Columns(1).Value
                bRespetarCombo = False
                GMN1Seleccionado
            End If
        End If
    End If
    
    Screen.MousePointer = vbNormal
    cmdActualizar_Click
    
End Sub

Private Sub sdbcGMN4_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        
        sdbgRes.RemoveAll
        BorrarDatosTotales
    
    End If
    
End Sub


Private Sub sdbcGMN4_4Cod_Click()
If sdbcGMN4_4Cod.DroppedDown Then Exit Sub
    
    If sdbcGMN4_4Cod.Value = "" Or sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    sdbcGMN4_4Cod.ListAutoPosition = False
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Value
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Value
    sdbcGMN4_4Cod.ListAutoPosition = True
    DoEvents
    GMN4Seleccionado
End Sub

Private Sub sdbcGMN4_4Cod_CloseUp()
    
    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    bRespetarCombo = False
    GMN4Seleccionado
    bCargarComboDesde = False
   
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN4_4Cod.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oGruposMN3 = Nothing
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
        
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , False, False
            Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
            Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        End If
    End If
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN4_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4Cod_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        Set oGMN4Seleccionado = Nothing
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
          
    End If
End Sub

Private Sub sdbcGMN4_4Den_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        sdbgRes.RemoveAll
        BorrarDatosTotales

    End If
    
End Sub


Private Sub sdbcGMN4_4Den_Click()
If sdbcGMN4_4Den.DroppedDown Then Exit Sub
    
    If sdbcGMN4_4Den.Value = "" Or sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    sdbcGMN4_4Den.ListAutoPosition = False
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Value
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Value
    sdbcGMN4_4Den.ListAutoPosition = True
    DoEvents
    GMN4Seleccionado
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()
    
    If sdbcGMN4_4Den.Value = "....." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    bRespetarCombo = False
    
    GMN4Seleccionado
    bCargarComboDesde = False
        
End Sub

Private Sub sdbcGMN4_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN4_4Den.RemoveAll
    
    Set oGruposMN4 = Nothing
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , Trim(sdbcGMN4_4Den), True, False)
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN4_4Den), True, False
            Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN4 = Nothing
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , True, False)
        Else
             
             oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        End If
    End If
    
    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "....."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4Den_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        Set oGMN4Seleccionado = Nothing
        sdbcGMN4_4Cod.Text = ""
        sdbcGMN4_4Den.Text = ""
        
    End If
End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
        
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If

End Sub

Private Sub sdbgTotales_RowLoaded(ByVal Bookmark As Variant)
    If sdbgTotales.Columns(6).Value = "" Then Exit Sub
    
    If sdbgTotales.Columns(6).Value < 0 Then
        sdbgTotales.Columns(6).CellStyleSet "Red"
    Else
        If sdbgTotales.Columns(6).Value > 0 Then
            sdbgTotales.Columns(6).CellStyleSet "Green"
        End If
    End If
End Sub
Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
       
End Sub

Private Sub sdbcMon_DropDown()
   
    sdbcMon.RemoveAll
    
    oMonedas.CargarTodasLasMonedas , , , , , False, True
    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next

    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)
PositionList sdbcMon, Text
End Sub
Private Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
    End If
    
End Sub



Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_APLIMAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        lblAnyoDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyoHasta.caption = Ador(0).Value
        Ador.MoveNext
        optReu.caption = Ador(0).Value
        Ador.MoveNext
        optDir.caption = Ador(0).Value '5
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        sdbgRes.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value '8 Denominación
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.RemoveAll '10
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        fraResMat.caption = Ador(0).Value '15
        sIdiMat = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value '16 Cod
        sdbcGMN1_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
        sIdiPresup = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value
        sIdiAdj = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(5).caption = Ador(0).Value
        sIdiAhor = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(7).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(8).caption = Ador(0).Value
        Ador.MoveNext
        sIdiTotal = Ador(0).Value
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(1) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(2) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(3) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(4) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(5) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(6) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(7) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(8) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(9) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(10) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(11) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(12) = Ador(0).Value
        Ador.MoveNext
        sIdiHistRes = Ador(0).Value
       
        Ador.Close
    
    End If


    picLegend.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_APLIMAT_PICLEGEND + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)
    picLegend2.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_APLIMAT_PICLEGEND2 + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)


    Set Ador = Nothing



End Sub








