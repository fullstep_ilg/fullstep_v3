VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSelProveERP 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Selecci�n de Proveedor ERP"
   ClientHeight    =   1785
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7140
   Icon            =   "frmSelProveERP.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1785
   ScaleWidth      =   7140
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame 
      Caption         =   "Proveedor emisor de la factura"
      Height          =   1065
      Left            =   150
      TabIndex        =   2
      Top             =   150
      Width           =   6825
      Begin VB.CommandButton cmdBuscar1 
         Height          =   285
         Left            =   6390
         Picture         =   "frmSelProveERP.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   300
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
         Height          =   285
         Left            =   150
         TabIndex        =   4
         Top             =   300
         Width           =   1455
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2566
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
         Height          =   285
         Left            =   1740
         TabIndex        =   5
         Top             =   300
         Width           =   4575
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 1"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 0"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   8070
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCodERP 
         Height          =   285
         Left            =   2505
         TabIndex        =   7
         Top             =   660
         Width           =   3810
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   -2147483640
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2593
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5212
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3016
         Columns(2).Caption=   "CAMPO1"
         Columns(2).Name =   "CAMPO1"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   6720
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo de proveedor en el ERP:"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   2550
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "cmdAceptar"
      Height          =   315
      Left            =   150
      TabIndex        =   1
      Top             =   1350
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "cmdCancelar"
      Height          =   315
      Left            =   1230
      TabIndex        =   0
      Top             =   1350
      Width           =   1005
   End
End
Attribute VB_Name = "frmSelProveERP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sOrigen As String
Private bRespetarComboProve As Boolean
Private m_bProvMat As Boolean
Private oProves As CProveedores
Private oProveSeleccionado As CProveedor
Public m_bModif As Boolean
Private m_bCargarComboDesde As Boolean
Private bCargarComboDesde As Boolean
Private m_bRespetarComboCodERP As Boolean
Private m_oProvesERP As CProveERPs

'Separadores
Private Const m_lSeparador = 127

Private bActivado As Boolean
Private sCodProveAnt As String
Public m_lEmpresa As Long

Public m_bDescargarFrm As Boolean

Dim m_bActivado As Boolean

Private m_bUnload As Boolean

Private m_sMsgError As String
Public Sub CargarProveedorConBusqueda()

Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim vlb As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oProves = frmPROVEBuscar.oProveEncontrados
Set frmPROVEBuscar.oProveEncontrados = Nothing
sdbcProveCod = oProves.Item(1).Cod
sdbcProveCod_Validate False

'GFA - Tenemos que verificar que el proveedor seleccionado desde la b�squeda est� entre los que se puede seleccionar
'ya que en la b�squeda aparecen todos los proveedores
oProves.CargarTodosLosProveedoresDesde3 1, sdbcProveCod.Text, , , , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
Codigos = oProves.DevolverLosCodigos
vlb = False
For i = 0 To UBound(Codigos.Cod) - 1
    If Codigos.Cod(i) = sdbcProveCod.Text Then
        vlb = True
    End If
Next
If Not vlb Then
    sdbcProveCod.Text = ""
    sdbcProveDen.Text = ""
    sdbcCodERP.Text = ""
Else
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbcProveCod_Validate False
    If sCodProveAnt <> sdbcProveCod.Text Then
        sCodProveAnt = sdbcProveCod.Text
        sdbcCodERP.Text = ""
        sdbcCodERP_DropDown
        sdbcCodERP_CloseUp
    End If
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "CargarProveedorConBusqueda", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub cmdCancelar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
bActivado = True
m_bActivado = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSelProveERP", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcCodERP_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarComboCodERP And bActivado Then
       m_bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcCodERP_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Click sobre la combo de c�digos ERP.
''' </summary>
''' <param name></param>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub sdbcCodERP_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcCodERP.DroppedDown Then
        sdbcCodERP = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcCodERP_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' CloseUp de la combo de c�digos ERP.
''' </summary>
''' <param name></param>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub sdbcCodERP_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bRespetarComboCodERP = True
    
    sdbcCodERP.Value = sdbcCodERP.Columns(0).Value
    
    m_bRespetarComboCodERP = False
    m_bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcCodERP_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub


''' <summary>
''' Evento que salta cuando se despliega la combo de Provedores ERP.
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisado por ngo</remarks>
Private Sub sdbcCodERP_DropDown()
Dim oProveERP As CProveERP
Dim sCampo As String

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Text = "" Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass

    sdbcCodERP.RemoveAll

    Set m_oProvesERP = Nothing
    Set m_oProvesERP = oFSGSRaiz.Generar_CProveERPs
     
    If m_bCargarComboDesde Then
        m_oProvesERP.CargarProveedoresERP m_lEmpresa, sdbcProveCod.Text, sdbcCodERP.Text, , , False
    Else
        m_oProvesERP.CargarProveedoresERP m_lEmpresa, sdbcProveCod.Text, , , , False
    End If
    
    For Each oProveERP In m_oProvesERP
        If gParametrosGenerales.gbCampo1ERPAct = True Then
            sCampo = NullToStr(oProveERP.Campo1)
        ElseIf gParametrosGenerales.gbCampo2ERPAct Then
            sCampo = NullToStr(oProveERP.Campo2)
        ElseIf gParametrosGenerales.gbCampo3ERPAct Then
            sCampo = NullToStr(oProveERP.Campo3)
        ElseIf gParametrosGenerales.gbCampo4ERPAct Then
            sCampo = NullToStr(oProveERP.Campo4)
        End If
    
        sdbcCodERP.AddItem oProveERP.Cod & Chr(m_lSeparador) & oProveERP.Den '& Chr(m_lSeparador) & sCampo
    Next

    sdbcCodERP.SelStart = 0
    sdbcCodERP.SelLength = Len(sdbcCodERP.Text)
    sdbcCodERP.Refresh

    m_bCargarComboDesde = False

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcCodERP_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcCodERP_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcCodERP.DataFieldList = "Column 0"
    sdbcCodERP.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcCodERP_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Se posiciona en la combo de Proveedores ERP seg�n la selecci�n.
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>

Private Sub sdbcCodERP_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcCodERP.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCodERP.Rows - 1
            bm = sdbcCodERP.GetBookmark(i)
            If UCase(sdbcCodERP.Text) = UCase(Mid(sdbcCodERP.Columns(0).CellText(bm), 1, Len(sdbcCodERP.Text))) Then
                sdbcCodERP.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcCodERP_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Validate de la combo de Proveedores ERP.
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
''' <remarks>Revisado por ngo</remarks>
Private Sub sdbcCodERP_Validate(Cancel As Boolean)

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcCodERP.Text = "" Then
        m_bCargarComboDesde = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    m_oProvesERP.CargarProveedoresERP , sdbcProveCod.Text, sdbcCodERP.Text, , , True

    If m_oProvesERP.Count = 0 Then
        sdbcCodERP.Text = ""
        Screen.MousePointer = vbNormal
    Else
        m_bRespetarComboCodERP = True

        sdbcCodERP.Columns(0).Value = m_oProvesERP.Item(1).Cod
        sdbcCodERP.Columns(1).Value = m_oProvesERP.Item(1).Den
                
        m_bRespetarComboCodERP = False

        m_bCargarComboDesde = False
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcCodERP_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdAceptar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case UCase(sOrigen)
    Case UCase("FRMPEDIDOS")
        frmPEDIDOS.txtCodPedidoERPFact.Text = sdbcCodERP.Text
    Case UCase("frmSeguimiento")
        frmSeguimiento.sdbgSeguimiento.Columns("COD_PROVE_ERP_FACTURA").Value = sdbcCodERP.Text
    Case UCase("frmProveDetalle")
        frmProveDetalle.lblCodERPFactText.caption = sdbcCodERP.Text
End Select

Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub cmdBuscar1_Click()


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmPROVEBuscar.sOrigen = "frmSelProveERP"
    frmPROVEBuscar.bRMat = m_bProvMat
    frmSelProveERP.Hide
    frmPROVEBuscar.Hide
    frmPROVEBuscar.Show 1
    frmSelProveERP.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "cmdBuscar1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub




Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    m_bDescargarFrm = False
    m_bActivado = False
    m_bUnload = False

bActivado = False
Screen.MousePointer = vbHourglass

m_bCargarComboDesde = False

'Configurar Seguridad
If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestProvMatComp)) Is Nothing) Then
    m_bProvMat = True
Else
    m_bProvMat = False
End If

CargarRecursos

PonerFieldSeparator Me

Set oProves = oFSGSRaiz.Generar_CProveedores
Set m_oProvesERP = oFSGSRaiz.Generar_CProveERPs
Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub CargarRecursos()
'''M�todo para traducci�n de literales en funci�n del idioma de la aplicaci�n
   Dim Ador As Ador.Recordset
   
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELPROVEERP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value '1
        Ador.MoveNext
        Label1.caption = Ador(0).Value & ":"
        Ador.MoveNext
        Frame.caption = Ador(0).Value & ":"
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        sdbcCodERP.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        sdbcCodERP.Columns(1).caption = Ador(0).Value
        
        Ador.Close
    End If
    Set Ador = Nothing
    
    If gParametrosGenerales.gbCampo1ERPAct = True Then
        sdbcCodERP.Columns("CAMPO1").caption = NullToStr(gParametrosGenerales.gsCampo1ERP)
    ElseIf gParametrosGenerales.gbCampo2ERPAct Then
        sdbcCodERP.Columns("CAMPO1").caption = NullToStr(gParametrosGenerales.gsCampo2ERP)
    ElseIf gParametrosGenerales.gbCampo3ERPAct Then
        sdbcCodERP.Columns("CAMPO1").caption = NullToStr(gParametrosGenerales.gsCampo3ERP)
    ElseIf gParametrosGenerales.gbCampo4ERPAct Then
        sdbcCodERP.Columns("CAMPO1").caption = NullToStr(gParametrosGenerales.gsCampo4ERP)
    Else  'Si no est� activado ninguno no muestra el campo
        sdbcCodERP.Columns("CAMPO1").Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oProveSeleccionado = Nothing
Set oProves = Nothing
Set m_oProvesERP = Nothing
Set frmSelProveERP = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcProveCod_LostFocus()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sdbcProveCod_CloseUp
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSelProveERP", "sdbcProveCod_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveDen_CloseUp()


    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    
    ProveedorSeleccionado sdbcProveDen.Columns(1).Text
    
    If sCodProveAnt <> sdbcProveCod.Text Then
        sCodProveAnt = sdbcProveCod.Text
        sdbcCodERP.Text = ""
        sdbcCodERP_DropDown
        sdbcCodERP_CloseUp
    End If
    bRespetarComboProve = False
    
    DoEvents
    
    
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ProveedorSeleccionado(sProveCod As String)

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    oProves.CargarDatosProveedor (sProveCod)
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sProveCod)
    
    sdbcCodERP.Enabled = True
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "ProveedorSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveDen_Change()

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboProve And bActivado Then
    
        bRespetarComboProve = True
        sdbcProveCod.Text = ""
        sdbcCodERP.Text = ""
        sdbcCodERP.Enabled = False
        
        bRespetarComboProve = False
        
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveDen_Click()

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub sdbcProveDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll
    'If bCargarComboDesde Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveDen.Text), , True, , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    'Else
    '    oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , , True, , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    'End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveCod.Refresh

    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_LostFocus()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sdbcProveDen_CloseUp
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSelProveERP", "sdbcProveDen_LostFocus", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveDen_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcProveCod_Change()

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not bRespetarComboProve And bActivado Then
    
        bRespetarComboProve = True
        sdbcProveDen.Text = ""
        sdbcCodERP.Text = ""
        sdbcCodERP.Enabled = False
        bRespetarComboProve = False
        bCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcProveCod_Click()

     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
     End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub sdbcProveCod_CloseUp()

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    bRespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    If sCodProveAnt <> sdbcProveCod.Text Then
        sCodProveAnt = sdbcProveCod.Text
        sdbcCodERP.Text = ""
        sdbcCodERP_DropDown
        sdbcCodERP_CloseUp
    End If
    bRespetarComboProve = False
    
    DoEvents
    
    ProveedorSeleccionado sdbcProveCod.Text
    
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub sdbcProveCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll
    
    'If bCargarComboDesde Then
        oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProveCod.Text), , , , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    'Else
    '    oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , , , , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
    'End If


    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If

    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    bCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub sdbcProveCod_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub sdbcProveCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveCod.Rows - 1
            bm = sdbcProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveCod_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el proveedor
    oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , , m_bProvMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario

    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        bRespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Text
        
        bRespetarComboProve = False
        ProveedorSeleccionado sdbcProveCod.Text
    End If
    bCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelProveERP", "sdbcProveCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
