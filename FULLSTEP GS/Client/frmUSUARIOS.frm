VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Begin VB.Form frmUSUARIOS 
   Caption         =   "Usuarios"
   ClientHeight    =   10620
   ClientLeft      =   1755
   ClientTop       =   1305
   ClientWidth     =   15420
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmUSUARIOS.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10620
   ScaleWidth      =   15420
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6600
      Top             =   4260
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":014A
            Key             =   "GrupoDeUsuarios"
            Object.Tag             =   "GrupoDeUsuarios"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":049C
            Key             =   "Usuario"
            Object.Tag             =   "Usuario"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":07EE
            Key             =   "Candado"
            Object.Tag             =   "Candado"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":0C40
            Key             =   "Usuarios"
            Object.Tag             =   "Usuarios"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":0F92
            Key             =   "Seguridad"
            Object.Tag             =   "Seguridad"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   26
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":13E4
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":1494
            Key             =   "DepartamentoBaja"
            Object.Tag             =   "DepartamentoBaja"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":17E6
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":1896
            Key             =   "UON1BAJA"
            Object.Tag             =   "UON1BAJA"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":1BE8
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":1F7B
            Key             =   "UON3BAJA"
            Object.Tag             =   "UON3BAJA"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":22CD
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":237D
            Key             =   "UON3ASIG"
            Object.Tag             =   "UON3ASIG"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":26CF
            Key             =   "UON1ASIG"
            Object.Tag             =   "UON1ASIG"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":2A21
            Key             =   "UON2ASIG"
            Object.Tag             =   "UON2ASIG"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":2D73
            Key             =   "UON2BAJA"
            Object.Tag             =   "UON2BAJA"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":30C5
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3164
            Key             =   "UON0_lit"
            Object.Tag             =   "UON0_lit"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":32BE
            Key             =   "UON0ASIG"
            Object.Tag             =   "UON0ASIG"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3610
            Key             =   "UON1CC"
            Object.Tag             =   "UON1CC"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3712
            Key             =   "UON2CC"
            Object.Tag             =   "UON2CC"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3814
            Key             =   "UON3CC"
            Object.Tag             =   "UON3CC"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3915
            Key             =   "UON4CC"
            Object.Tag             =   "UON4CC"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3A16
            Key             =   "UON4"
            Object.Tag             =   "UON4"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3B17
            Key             =   "SELLO"
            Object.Tag             =   "SELLO"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3BC7
            Key             =   "SELLOBAJA"
            Object.Tag             =   "SELLOBAJA"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3C77
            Key             =   "SELLOCC"
            Object.Tag             =   "SELLOCC"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":3D79
            Key             =   "UON1CCRED"
            Object.Tag             =   "UON1CCRED"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":4142
            Key             =   "UON2CCRED"
            Object.Tag             =   "UON2CCRED"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":450B
            Key             =   "UON3CCRED"
            Object.Tag             =   "UON3CCRED"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmUSUARIOS.frx":48D1
            Key             =   "UON4CCRED"
            Object.Tag             =   "UON4CCRED"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab sstabUsu 
      Height          =   13605
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   24075
      _ExtentX        =   42466
      _ExtentY        =   23998
      _Version        =   393216
      Style           =   1
      Tabs            =   13
      Tab             =   1
      TabsPerRow      =   13
      TabHeight       =   520
      TabMaxWidth     =   3528
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Usuarios"
      TabPicture(0)   =   "frmUSUARIOS.frx":4C97
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "picNavigateUsu"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "tvwUsu"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "DAcceso a compras FSGS"
      TabPicture(1)   =   "frmUSUARIOS.frx":4CB3
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "tvwsegur"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "fraAcciones"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lstSegurIdAcciones"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "picNavigateSegur"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "picEdit"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).ControlCount=   5
      TabCaption(2)   =   "DAcceso a solicitudes FSWS"
      TabPicture(2)   =   "frmUSUARIOS.frx":4CCF
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraFSWS"
      Tab(2).Control(1)=   "picEditFSWS"
      Tab(2).Control(2)=   "picNavigateFSWS"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "DAcceso a aprovisionamiento FSEP"
      TabPicture(3)   =   "frmUSUARIOS.frx":4CEB
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraFSEP"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "picEditFSEP"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "picNavigateFSEP"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).ControlCount=   3
      TabCaption(4)   =   "DAcceso a calidad FSQA"
      TabPicture(4)   =   "frmUSUARIOS.frx":4D07
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "ssTabQA"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "DAcceso a control de gasto FSSM"
      TabPicture(5)   =   "frmUSUARIOS.frx":4D23
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "sstabSM"
      Tab(5).Control(1)=   "picNavigateFSSM"
      Tab(5).Control(2)=   "picEditFSSM"
      Tab(5).ControlCount=   3
      TabCaption(6)   =   "Destinos para pedidos"
      TabPicture(6)   =   "frmUSUARIOS.frx":4D3F
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "SSTabCN"
      Tab(6).ControlCount=   1
      TabCaption(7)   =   "DAcceso a la red de colaboraci�n FSRC"
      TabPicture(7)   =   "frmUSUARIOS.frx":4D5B
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "picNavigateDest"
      Tab(7).Control(1)=   "picEditDest"
      Tab(7).Control(2)=   "fraDestinos"
      Tab(7).ControlCount=   3
      TabCaption(8)   =   "DPerfil"
      TabPicture(8)   =   "frmUSUARIOS.frx":4D77
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "wPerfil"
      Tab(8).ControlCount=   1
      TabCaption(9)   =   "Acceso a FSIS"
      TabPicture(9)   =   "frmUSUARIOS.frx":4D93
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "picNavigateFSIS"
      Tab(9).Control(1)=   "fraFSIS"
      Tab(9).Control(2)=   "picEditFSIS"
      Tab(9).ControlCount=   3
      TabCaption(10)  =   "Acceso a FSIM"
      TabPicture(10)  =   "frmUSUARIOS.frx":4DAF
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "fraFSIM"
      Tab(10).Control(1)=   "picNavigateFSIM"
      Tab(10).Control(2)=   "picEditFSIM"
      Tab(10).ControlCount=   3
      TabCaption(11)  =   "DAcceso a FSCM"
      TabPicture(11)  =   "frmUSUARIOS.frx":4DCB
      Tab(11).ControlEnabled=   0   'False
      Tab(11).Control(0)=   "fraFSCM"
      Tab(11).Control(1)=   "picNavigateFSCM"
      Tab(11).Control(2)=   "picEditFSCM"
      Tab(11).ControlCount=   3
      TabCaption(12)  =   "DSeguridad de solicitudes"
      TabPicture(12)  =   "frmUSUARIOS.frx":4DE7
      Tab(12).ControlEnabled=   0   'False
      Tab(12).Control(0)=   "picEditFSSOL"
      Tab(12).Control(1)=   "picNavigateFSSOL"
      Tab(12).Control(2)=   "fraFSSOL"
      Tab(12).ControlCount=   3
      Begin VB.PictureBox picEdit 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   90
         ScaleHeight     =   420
         ScaleWidth      =   9855
         TabIndex        =   312
         Top             =   5800
         Width           =   9855
         Begin VB.CommandButton cmdGS 
            Caption         =   "&Cancelar"
            Height          =   345
            Index           =   4
            Left            =   3780
            TabIndex        =   314
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdGS 
            Caption         =   "&Aceptar"
            Height          =   345
            Index           =   3
            Left            =   2625
            TabIndex        =   313
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigateSegur 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   60
         ScaleHeight     =   405
         ScaleWidth      =   9885
         TabIndex        =   308
         TabStop         =   0   'False
         Top             =   5800
         Width           =   9885
         Begin VB.CommandButton cmdGS 
            Caption         =   "&Modificar"
            Height          =   345
            Index           =   0
            Left            =   75
            TabIndex        =   311
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdGS 
            Caption         =   "&Restaurar"
            Height          =   345
            Index           =   1
            Left            =   1185
            TabIndex        =   310
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdGS 
            Caption         =   "&Listado"
            Height          =   345
            Index           =   2
            Left            =   2280
            TabIndex        =   309
            Top             =   30
            Width           =   1005
         End
      End
      Begin VB.Frame fraFSWS 
         Height          =   780
         Left            =   -74880
         TabIndex        =   305
         Top             =   480
         Width           =   11655
         Begin VB.PictureBox picFSWS 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   405
            Left            =   120
            ScaleHeight     =   405
            ScaleWidth      =   11295
            TabIndex        =   306
            Top             =   240
            Width           =   11295
            Begin VB.CheckBox chkPM 
               Caption         =   "DPermitir ver las notificaciones de PM"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   0
               TabIndex        =   307
               Top             =   0
               Value           =   1  'Checked
               Width           =   9210
            End
         End
      End
      Begin VB.PictureBox picNavigateFSIS 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   -74760
         ScaleHeight     =   375
         ScaleWidth      =   1815
         TabIndex        =   303
         Top             =   3840
         Width           =   1815
         Begin VB.CommandButton cmdModificarFSIS 
            Caption         =   "DModificar"
            Height          =   345
            Left            =   0
            TabIndex        =   304
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEditFSWS 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -71520
         ScaleHeight     =   375
         ScaleWidth      =   4695
         TabIndex        =   161
         Top             =   9360
         Visible         =   0   'False
         Width           =   4695
         Begin VB.CommandButton cmdWS 
            Caption         =   "DCancelar"
            Height          =   345
            Index           =   2
            Left            =   1155
            TabIndex        =   163
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdWS 
            Caption         =   "&DAceptar"
            Height          =   345
            Index           =   1
            Left            =   0
            TabIndex        =   162
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigateFSWS 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -74760
         ScaleHeight     =   375
         ScaleWidth      =   3150
         TabIndex        =   159
         Top             =   9360
         Width           =   3150
         Begin VB.CommandButton cmdWS 
            Caption         =   "DModificar"
            Height          =   345
            Index           =   0
            Left            =   0
            TabIndex        =   160
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigateUsu 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   -74940
         ScaleHeight     =   420
         ScaleWidth      =   7560
         TabIndex        =   151
         TabStop         =   0   'False
         Top             =   5340
         Width           =   7560
         Begin VB.CommandButton cmdUsu 
            Caption         =   "&Codigo"
            Height          =   345
            Index           =   3
            Left            =   3225
            TabIndex        =   158
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdUsu 
            Caption         =   "&Listado"
            Height          =   345
            Index           =   6
            Left            =   6390
            TabIndex        =   157
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdUsu 
            Caption         =   "&A�adir"
            Height          =   345
            Index           =   0
            Left            =   60
            TabIndex        =   156
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdUsu 
            Caption         =   "&Modificar"
            Height          =   345
            Index           =   1
            Left            =   1110
            TabIndex        =   155
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdUsu 
            Caption         =   "&Eliminar"
            Height          =   345
            Index           =   2
            Left            =   2160
            TabIndex        =   154
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdUsu 
            Caption         =   "&Buscar"
            Height          =   345
            Index           =   4
            Left            =   4290
            TabIndex        =   153
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdUsu 
            Caption         =   "&Restaurar"
            Height          =   345
            Index           =   5
            Left            =   5340
            TabIndex        =   152
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.ListBox lstSegurIdAcciones 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   840
         Left            =   6180
         TabIndex        =   150
         Top             =   3240
         Visible         =   0   'False
         Width           =   2370
      End
      Begin VB.Frame fraAcciones 
         Caption         =   "Accciones disponibles"
         Height          =   5130
         Left            =   4200
         TabIndex        =   146
         Top             =   360
         Width           =   5745
         Begin VB.CommandButton cmdGS 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   8.25
               Charset         =   0
               Weight          =   900
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   0
            TabIndex        =   147
            Top             =   90
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.Timer Timer2 
            Enabled         =   0   'False
            Interval        =   50
            Left            =   1000
            Top             =   1000
         End
         Begin MSComctlLib.ListView lstvwSegur 
            Height          =   3585
            Left            =   180
            TabIndex        =   148
            Top             =   270
            Visible         =   0   'False
            Width           =   5430
            _ExtentX        =   9578
            _ExtentY        =   6324
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   -1  'True
            HideColumnHeaders=   -1  'True
            Checkboxes      =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "ACC"
               Object.Width           =   2540
            EndProperty
         End
         Begin MSComctlLib.ListView lstvwSegurReadOnly 
            Height          =   3585
            Left            =   180
            TabIndex        =   149
            Top             =   270
            Width           =   5430
            _ExtentX        =   9578
            _ExtentY        =   6324
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            HideColumnHeaders=   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "ACC"
               Object.Width           =   2540
            EndProperty
         End
      End
      Begin VB.PictureBox picNavigateFSEP 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -74880
         ScaleHeight     =   375
         ScaleWidth      =   1335
         TabIndex        =   144
         Top             =   9510
         Width           =   1335
         Begin VB.CommandButton cmdEP 
            Caption         =   "DModificar"
            Height          =   345
            Index           =   0
            Left            =   0
            TabIndex        =   145
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEditFSEP 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -71520
         ScaleHeight     =   375
         ScaleWidth      =   4695
         TabIndex        =   141
         Top             =   9510
         Visible         =   0   'False
         Width           =   4695
         Begin VB.CommandButton cmdEP 
            Caption         =   "DCancelar"
            Height          =   345
            Index           =   2
            Left            =   1155
            TabIndex        =   143
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdEP 
            Caption         =   "&DAceptar"
            Height          =   345
            Index           =   1
            Left            =   0
            TabIndex        =   142
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigateFSSM 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -74880
         ScaleHeight     =   375
         ScaleWidth      =   2895
         TabIndex        =   122
         Top             =   7380
         Width           =   2895
         Begin VB.CommandButton cmdModificarFSSM 
            Caption         =   "DModificar"
            Height          =   345
            Left            =   0
            TabIndex        =   123
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEditFSSM 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -69600
         ScaleHeight     =   375
         ScaleWidth      =   4695
         TabIndex        =   119
         Top             =   7380
         Visible         =   0   'False
         Width           =   4695
         Begin VB.CommandButton cmdCancelarFSSM 
            Caption         =   "DCancelar"
            Height          =   345
            Left            =   1155
            TabIndex        =   121
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdAceptarFSSM 
            Caption         =   "&DAceptar"
            Height          =   345
            Left            =   0
            TabIndex        =   120
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.Frame fraDestinos 
         Height          =   5955
         Left            =   -74880
         TabIndex        =   110
         Top             =   480
         Width           =   9825
         Begin VB.PictureBox PicDestPorDefecto 
            BorderStyle     =   0  'None
            Height          =   765
            Left            =   120
            ScaleHeight     =   765
            ScaleWidth      =   5295
            TabIndex        =   113
            Top             =   4620
            Width           =   5295
            Begin SSDataWidgets_B.SSDBCombo sdbcDestDen 
               Height          =   285
               Left            =   840
               TabIndex        =   114
               Top             =   420
               Width           =   4335
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   7646
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcDestCod 
               Height          =   285
               Left            =   0
               TabIndex        =   115
               Top             =   420
               Width           =   855
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1499
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblDestino 
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   0
               TabIndex        =   117
               Top             =   420
               Visible         =   0   'False
               Width           =   5160
            End
            Begin VB.Label lblDestinoDef 
               Caption         =   "DDestino por defecto para pedidos:"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   0
               TabIndex        =   116
               Top             =   150
               Width           =   5145
            End
         End
         Begin VB.PictureBox PicDestinos 
            BorderStyle     =   0  'None
            Height          =   3720
            Left            =   120
            ScaleHeight     =   3720
            ScaleWidth      =   9495
            TabIndex        =   111
            Top             =   480
            Width           =   9495
            Begin SSDataWidgets_B.SSDBGrid sdbgDestinos 
               Height          =   3705
               Left            =   0
               Negotiate       =   -1  'True
               TabIndex        =   112
               Top             =   0
               Width           =   9405
               _Version        =   196617
               DataMode        =   2
               Col.Count       =   8
               stylesets.count =   4
               stylesets(0).Name=   "Amarillo2"
               stylesets(0).BackColor=   11599871
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmUSUARIOS.frx":4E03
               stylesets(1).Name=   "Gris"
               stylesets(1).BackColor=   12632256
               stylesets(1).HasFont=   -1  'True
               BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(1).Picture=   "frmUSUARIOS.frx":4E1F
               stylesets(2).Name=   "Normal"
               stylesets(2).HasFont=   -1  'True
               BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(2).Picture=   "frmUSUARIOS.frx":4E3B
               stylesets(3).Name=   "Amarillo"
               stylesets(3).BackColor=   -2147483636
               stylesets(3).HasFont=   -1  'True
               BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(3).Picture=   "frmUSUARIOS.frx":4E57
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   3
               SelectByCell    =   -1  'True
               StyleSet        =   "Amarillo"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   8
               Columns(0).Width=   1402
               Columns(0).Caption=   "Emisi�n"
               Columns(0).Name =   "EMISION"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   11
               Columns(0).FieldLen=   256
               Columns(0).Style=   2
               Columns(0).StyleSet=   "Amarillo2"
               Columns(1).Width=   1693
               Columns(1).Caption=   "Recepci�n"
               Columns(1).Name =   "RECEPCION"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   11
               Columns(1).FieldLen=   256
               Columns(1).Style=   2
               Columns(1).StyleSet=   "Amarillo2"
               Columns(2).Width=   1773
               Columns(2).Caption=   "COD"
               Columns(2).Name =   "COD"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(2).Locked=   -1  'True
               Columns(2).StyleSet=   "Amarillo2"
               Columns(3).Width=   4128
               Columns(3).Caption=   "DEN"
               Columns(3).Name =   "DEN"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(3).Locked=   -1  'True
               Columns(3).StyleSet=   "Amarillo2"
               Columns(4).Width=   4366
               Columns(4).Caption=   "DIR"
               Columns(4).Name =   "DIR"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(4).Locked=   -1  'True
               Columns(4).StyleSet=   "Amarillo2"
               Columns(5).Width=   3201
               Columns(5).Caption=   "POB"
               Columns(5).Name =   "POB"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               Columns(5).Locked=   -1  'True
               Columns(5).StyleSet=   "Amarillo2"
               Columns(6).Width=   3201
               Columns(6).Caption=   "CP"
               Columns(6).Name =   "CP"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               Columns(6).Locked=   -1  'True
               Columns(6).StyleSet=   "Amarillo2"
               Columns(7).Width=   3200
               Columns(7).Visible=   0   'False
               Columns(7).Caption=   "GENERICO"
               Columns(7).Name =   "GENERICO"
               Columns(7).DataField=   "Column 7"
               Columns(7).DataType=   11
               Columns(7).FieldLen=   256
               _ExtentX        =   16589
               _ExtentY        =   6535
               _StockProps     =   79
               BackColor       =   16777215
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Label lblDestinoUp 
            Caption         =   "DDestino para pedidos:"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   120
            TabIndex        =   118
            Top             =   240
            Width           =   5145
         End
      End
      Begin VB.PictureBox picEditDest 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -70560
         ScaleHeight     =   375
         ScaleWidth      =   4695
         TabIndex        =   107
         Top             =   7260
         Visible         =   0   'False
         Width           =   4695
         Begin VB.CommandButton cmdAceptarDest 
            Caption         =   "&DAceptar"
            Height          =   345
            Left            =   0
            TabIndex        =   109
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdCancelarDest 
            Caption         =   "DCancelar"
            Height          =   345
            Left            =   1155
            TabIndex        =   108
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigateDest 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -74520
         ScaleHeight     =   375
         ScaleWidth      =   2895
         TabIndex        =   105
         Top             =   7260
         Width           =   2895
         Begin VB.CommandButton cmdModifDest 
            Caption         =   "DModificar"
            Height          =   345
            Left            =   0
            TabIndex        =   106
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.Frame fraFSIS 
         Caption         =   "Acceso FSIS"
         Enabled         =   0   'False
         Height          =   3255
         Left            =   -74760
         TabIndex        =   103
         Top             =   480
         Width           =   11535
         Begin VB.CheckBox chkRelanzar 
            Caption         =   "Relanzar integraci�n"
            Height          =   615
            Left            =   240
            TabIndex        =   104
            Top             =   480
            Width           =   8175
         End
      End
      Begin VB.PictureBox picEditFSIS 
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   -70680
         ScaleHeight     =   615
         ScaleWidth      =   2175
         TabIndex        =   99
         Top             =   3840
         Visible         =   0   'False
         Width           =   2175
         Begin VB.CommandButton cmdAceptarFSIS 
            Caption         =   "D&Aceptar"
            Height          =   345
            Left            =   0
            TabIndex        =   101
            Top             =   0
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdCancelarFSIS 
            Caption         =   "DCancelar"
            Height          =   345
            Left            =   1080
            TabIndex        =   100
            Top             =   0
            Visible         =   0   'False
            Width           =   1005
         End
      End
      Begin VB.Frame fraFSEP 
         Height          =   9015
         Left            =   -74880
         TabIndex        =   67
         Top             =   480
         Width           =   9735
         Begin VB.PictureBox picFSEP 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   8775
            Left            =   60
            ScaleHeight     =   8775
            ScaleWidth      =   9555
            TabIndex        =   68
            Top             =   120
            Width           =   9555
            Begin VB.CheckBox chkFSEP 
               Caption         =   "DRestringir los pedidos de cat�logo a la empresa del usuario"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   12
               Left            =   120
               TabIndex        =   98
               Top             =   960
               Width           =   8940
            End
            Begin VB.CheckBox chkFSEP 
               Caption         =   "DPermitir realizar pedidos de cat�logo libres"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   97
               Top             =   360
               Width           =   8940
            End
            Begin VB.CheckBox chkFSEP 
               Caption         =   "DPermitir modificar los precios en la emisi�n de pedidos de cat�logo "
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   11
               Left            =   120
               TabIndex        =   96
               Top             =   660
               Width           =   8940
            End
            Begin VB.CheckBox chkFSEP 
               Caption         =   "dRestringir la emisi�n  de pedidios a art�culos de la unidad organizativa del usuario"
               Height          =   195
               Index           =   13
               Left            =   120
               TabIndex        =   95
               Top             =   1260
               Width           =   8955
            End
            Begin VB.CheckBox chkFSEP 
               Caption         =   "DPermitir emitir pedidos desde Pedido Abierto"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   94
               Top             =   1560
               Width           =   8940
            End
            Begin VB.CheckBox chkFSEP 
               Caption         =   "DRestringir la emisi�n de pedidos contra Pedido Abierto a los Pedidos Abiertos de la Empresa del usuario"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   93
               Top             =   1860
               Width           =   8940
            End
            Begin VB.PictureBox picPres 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   1155
               Index           =   0
               Left            =   120
               ScaleHeight     =   1155
               ScaleWidth      =   9045
               TabIndex        =   88
               Top             =   2160
               Width           =   9045
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "DPermitir seleccionar presupuestos por concepto 1 de las unidades organizativas situadas por encima del usuario"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Index           =   3
                  Left            =   0
                  TabIndex        =   92
                  Top             =   0
                  Width           =   8940
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "DPermitir seleccionar presupuestos por concepto 2 de las unidades organizativas situadas por encima del usuario"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Index           =   5
                  Left            =   0
                  TabIndex        =   91
                  Top             =   300
                  Width           =   8940
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "DPermitir seleccionar presupuestos por concepto 3 de las unidades organizativas situadas por encima del usuario"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Index           =   6
                  Left            =   0
                  TabIndex        =   90
                  Top             =   600
                  Width           =   8940
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "DPermitir seleccionar presupuestos por concepto 4 de las unidades organizativas situadas por encima del usuario"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Index           =   7
                  Left            =   0
                  TabIndex        =   89
                  Top             =   900
                  Width           =   8940
               End
            End
            Begin VB.PictureBox picPres 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   1155
               Index           =   1
               Left            =   120
               ScaleHeight     =   1155
               ScaleWidth      =   9015
               TabIndex        =   83
               Top             =   3360
               Width           =   9015
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "DRestringir la selecci�n de presupuestos por concepto 1 a la unidad organizativa del usuario"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Index           =   4
                  Left            =   0
                  TabIndex        =   87
                  Top             =   0
                  Width           =   8940
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "DRestringir la selecci�n de presupuestos por concepto 2 a la unidad organizativa del usuario"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Index           =   8
                  Left            =   0
                  TabIndex        =   86
                  Top             =   300
                  Width           =   8940
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "DRestringir la selecci�n de presupuestos por concepto 3 a la unidad organizativa del usuario"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Index           =   9
                  Left            =   0
                  TabIndex        =   85
                  Top             =   600
                  Width           =   8940
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "DRestringir la selecci�n de presupuestos por concepto 4 a la unidad organizativa del usuario"
                  ForeColor       =   &H00000000&
                  Height          =   195
                  Index           =   10
                  Left            =   0
                  TabIndex        =   84
                  Top             =   900
                  Width           =   8940
               End
            End
            Begin VB.PictureBox picPres 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   4215
               Index           =   2
               Left            =   0
               ScaleHeight     =   4215
               ScaleWidth      =   9225
               TabIndex        =   69
               Top             =   4560
               Width           =   9225
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir modificar el porcentaje de desv�o de la recepci�n en la emisi�n de pedidos"
                  Height          =   195
                  Index           =   27
                  Left            =   120
                  TabIndex        =   315
                  Top             =   3840
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir activar el bloqueo de albaranes para facturaci�n"
                  Height          =   195
                  Index           =   19
                  Left            =   120
                  TabIndex        =   82
                  Top             =   1470
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir recepcionar pedidos de los centros de coste del usuario"
                  Height          =   195
                  Index           =   17
                  Left            =   120
                  TabIndex        =   81
                  Top             =   870
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir reabrir pedidos"
                  Height          =   195
                  Index           =   16
                  Left            =   120
                  TabIndex        =   80
                  Top             =   570
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir cerrar pedidos"
                  Height          =   195
                  Index           =   15
                  Left            =   120
                  TabIndex        =   79
                  Top             =   285
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir ver los pedidos de los centros de coste del usuario"
                  Height          =   195
                  Index           =   14
                  Left            =   120
                  TabIndex        =   78
                  Top             =   0
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "DPermitir anular recepciones"
                  Height          =   195
                  Index           =   18
                  Left            =   120
                  TabIndex        =   77
                  Top             =   1170
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dRestringir el centro de aprovisionamiento al usuario"
                  Height          =   195
                  Index           =   20
                  Left            =   120
                  TabIndex        =   76
                  Top             =   1770
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir anular pedidos"
                  Height          =   195
                  Index           =   21
                  Left            =   120
                  TabIndex        =   75
                  Top             =   2070
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir ver pedidos borrados en el seguimiento de pedidos"
                  Height          =   195
                  Index           =   22
                  Left            =   120
                  TabIndex        =   74
                  Top             =   2370
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir borrar l�neas de pedido"
                  Height          =   195
                  Index           =   23
                  Left            =   120
                  TabIndex        =   73
                  Top             =   2670
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir deshacer el borrado de l�neas de pedido"
                  Height          =   225
                  Index           =   24
                  Left            =   120
                  TabIndex        =   72
                  Top             =   2970
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir borrar pedidos"
                  Height          =   225
                  Index           =   25
                  Left            =   120
                  TabIndex        =   71
                  Top             =   3270
                  Width           =   8955
               End
               Begin VB.CheckBox chkFSEP 
                  Caption         =   "dPermitir deshacer el borrado de pedidos"
                  Height          =   225
                  Index           =   26
                  Left            =   120
                  TabIndex        =   70
                  Top             =   3570
                  Width           =   8955
               End
            End
         End
      End
      Begin VB.Frame fraFSIM 
         Enabled         =   0   'False
         Height          =   1875
         Left            =   -74760
         TabIndex        =   62
         Top             =   480
         Width           =   12315
         Begin VB.CheckBox chkFSIM 
            Caption         =   "DAlta de facturas"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   66
            Top             =   420
            Width           =   10995
         End
         Begin VB.CheckBox chkFSIM 
            Caption         =   "DSeguimiento de facturas"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   65
            Top             =   720
            Width           =   10995
         End
         Begin VB.CheckBox chkFSIM 
            Caption         =   "DVisor de autofacturas"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   64
            Top             =   1020
            Width           =   10995
         End
         Begin VB.CheckBox chkFSIM 
            Caption         =   "DPermitir ver las notificaciones de IM"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   63
            Top             =   1320
            Width           =   10995
         End
      End
      Begin VB.PictureBox picNavigateFSIM 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   -74760
         ScaleHeight     =   375
         ScaleWidth      =   1605
         TabIndex        =   60
         Top             =   7980
         Width           =   1600
         Begin VB.CommandButton cmdModificarFSIM 
            Caption         =   "DModificar"
            Height          =   345
            Left            =   0
            TabIndex        =   61
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEditFSIM 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   -70080
         ScaleHeight     =   375
         ScaleWidth      =   3435
         TabIndex        =   57
         Top             =   7980
         Visible         =   0   'False
         Width           =   3435
         Begin VB.CommandButton cmdAceptarFSIM 
            Caption         =   "DAceptar"
            Height          =   345
            Left            =   60
            TabIndex        =   59
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdCancelarFSIM 
            Caption         =   "DCancelar"
            Height          =   345
            Left            =   1440
            TabIndex        =   58
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.Frame fraFSSOL 
         Enabled         =   0   'False
         Height          =   8835
         Left            =   -74760
         TabIndex        =   13
         Top             =   480
         Width           =   12435
         Begin VB.Frame fraFSSOLAcciones 
            BorderStyle     =   0  'None
            Height          =   5895
            Left            =   120
            TabIndex        =   37
            Top             =   120
            Width           =   11175
            Begin VB.CheckBox chkSOL 
               Caption         =   "DPermitir crear escenarios para el proveedor"
               Height          =   255
               Index           =   19
               Left            =   0
               TabIndex        =   56
               Top             =   3120
               Width           =   8055
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir las empresas de la solicitud a la Empresa del usuario"
               Height          =   255
               Index           =   8
               Left            =   0
               TabIndex        =   55
               Top             =   2520
               Width           =   9210
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir la edici�n de escenarios compartidos no creados por el usuario a escenarios del departamento del usuario"
               Height          =   195
               Index           =   16
               Left            =   0
               TabIndex        =   54
               Top             =   5220
               Width           =   10300
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir la edici�n de escenarios compartidos no creados por el usuario a escenarios de unidad organizativa del usuario"
               Height          =   195
               Index           =   15
               Left            =   0
               TabIndex        =   53
               Top             =   4920
               Width           =   10000
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir la creaci�n de escenarios para otros usuaruios a usuarios del departamento del usuario"
               Height          =   195
               Index           =   14
               Left            =   0
               TabIndex        =   52
               Top             =   4620
               Width           =   8415
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir la creaci�n de escenarios para otros usuaruios a usuarios de la unidad organizativa del usuario"
               Height          =   195
               Index           =   13
               Left            =   0
               TabIndex        =   51
               Top             =   4320
               Width           =   8415
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir los conceptos presupuestarios a la unidad organizativa del usuario"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   17
               Left            =   0
               TabIndex        =   50
               Top             =   5520
               Width           =   9210
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DPermitir crear escenarios"
               Height          =   255
               Index           =   9
               Left            =   0
               TabIndex        =   49
               Top             =   2820
               Width           =   8055
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DOcultar escenario por defecto"
               Height          =   195
               Index           =   12
               Left            =   0
               TabIndex        =   48
               Top             =   4020
               Width           =   8415
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DPermitir editar escenarios compartidos por otros usuarios"
               Height          =   195
               Index           =   11
               Left            =   0
               TabIndex        =   47
               Top             =   3720
               Width           =   7935
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DPermitir crear escenarios para otros usuarios"
               Height          =   255
               Index           =   10
               Left            =   0
               TabIndex        =   46
               Top             =   3420
               Width           =   8055
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir los destinos a los de la unidad organizativa del usuario"
               Height          =   255
               Index           =   5
               Left            =   0
               TabIndex        =   45
               Top             =   1620
               Width           =   9210
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir el material asignado al usuario"
               Height          =   255
               Index           =   4
               Left            =   0
               TabIndex        =   44
               Top             =   1320
               Width           =   9210
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir los proveedores al material del usuario"
               Height          =   255
               Index           =   3
               Left            =   0
               TabIndex        =   43
               Top             =   1020
               Width           =   9210
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir los traslados a personas del departamento del usuario"
               Height          =   255
               Index           =   2
               Left            =   0
               TabIndex        =   42
               Top             =   720
               Width           =   9210
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir los traslados a personas de la unidad organizativa del usuario"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   0
               TabIndex        =   41
               Top             =   420
               Width           =   9210
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DRestringir las solicitudes referenciadas a las abiertas en la unidad organizativa del usuario"
               Height          =   255
               Index           =   6
               Left            =   0
               TabIndex        =   40
               Top             =   1920
               Width           =   9210
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DPermitir visualizar los precios de las �ltimas adjudicaciones para los art�culos"
               Height          =   255
               Index           =   7
               Left            =   0
               TabIndex        =   39
               Top             =   2220
               Width           =   9210
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DPermitir realizar traslados"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   0
               TabIndex        =   38
               Top             =   120
               Width           =   9210
            End
         End
         Begin VB.Frame fraFSSOLConcepPresup 
            BorderStyle     =   0  'None
            Height          =   2775
            Left            =   120
            TabIndex        =   14
            Top             =   5880
            Width           =   9135
            Begin VB.CommandButton cmdLimpiarPres 
               Height          =   285
               Index           =   1
               Left            =   7935
               Picture         =   "frmUSUARIOS.frx":4E73
               Style           =   1  'Graphical
               TabIndex        =   26
               Top             =   930
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarPres 
               Height          =   285
               Index           =   1
               Left            =   8295
               Picture         =   "frmUSUARIOS.frx":5204
               Style           =   1  'Graphical
               TabIndex        =   25
               Top             =   930
               Width           =   315
            End
            Begin VB.CommandButton cmdLimpiarPres 
               Height          =   285
               Index           =   2
               Left            =   7920
               Picture         =   "frmUSUARIOS.frx":5270
               Style           =   1  'Graphical
               TabIndex        =   24
               Top             =   1335
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarPres 
               Height          =   285
               Index           =   2
               Left            =   8295
               Picture         =   "frmUSUARIOS.frx":5601
               Style           =   1  'Graphical
               TabIndex        =   23
               Top             =   1335
               Width           =   315
            End
            Begin VB.CommandButton cmdLimpiarPres 
               Height          =   285
               Index           =   3
               Left            =   7935
               Picture         =   "frmUSUARIOS.frx":566D
               Style           =   1  'Graphical
               TabIndex        =   22
               Top             =   1740
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarPres 
               Height          =   285
               Index           =   3
               Left            =   8295
               Picture         =   "frmUSUARIOS.frx":59FE
               Style           =   1  'Graphical
               TabIndex        =   21
               Top             =   1740
               Width           =   315
            End
            Begin VB.CommandButton cmdLimpiarPres 
               Height          =   285
               Index           =   0
               Left            =   7935
               Picture         =   "frmUSUARIOS.frx":5A6A
               Style           =   1  'Graphical
               TabIndex        =   20
               Top             =   540
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarPres 
               Height          =   285
               Index           =   0
               Left            =   8295
               Picture         =   "frmUSUARIOS.frx":5DFB
               Style           =   1  'Graphical
               TabIndex        =   19
               Top             =   540
               Width           =   315
            End
            Begin VB.CheckBox chkSOL 
               Caption         =   "DActivar sustituci�n temporal"
               Height          =   285
               Index           =   18
               Left            =   120
               TabIndex        =   18
               Top             =   2040
               Width           =   4425
            End
            Begin VB.TextBox txtSustituto 
               Height          =   285
               Left            =   1935
               Locked          =   -1  'True
               TabIndex        =   17
               Top             =   2340
               Width           =   5952
            End
            Begin VB.CommandButton cmdLimpiarSust 
               Height          =   285
               Left            =   7935
               Picture         =   "frmUSUARIOS.frx":5E67
               Style           =   1  'Graphical
               TabIndex        =   16
               Top             =   2340
               Width           =   315
            End
            Begin VB.CommandButton cmdBuscarSust 
               Height          =   285
               Left            =   8295
               Picture         =   "frmUSUARIOS.frx":61F8
               Style           =   1  'Graphical
               TabIndex        =   15
               Top             =   2340
               Width           =   315
            End
            Begin VB.Label lblLitPres1 
               BackStyle       =   0  'Transparent
               Caption         =   "Cuenta de gasto"
               ForeColor       =   &H00000000&
               Height          =   375
               Left            =   180
               TabIndex        =   36
               Top             =   570
               Width           =   1905
            End
            Begin VB.Label lblPres 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Index           =   0
               Left            =   2115
               TabIndex        =   35
               Top             =   540
               Width           =   5805
            End
            Begin VB.Label lblLitPres4 
               BackStyle       =   0  'Transparent
               Caption         =   "Partida contable"
               ForeColor       =   &H00000000&
               Height          =   375
               Left            =   180
               TabIndex        =   34
               Top             =   1770
               Width           =   1905
            End
            Begin VB.Label lblPres 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Index           =   3
               Left            =   2115
               TabIndex        =   33
               Top             =   1740
               Width           =   5805
            End
            Begin VB.Label lblLitPres3 
               BackStyle       =   0  'Transparent
               Caption         =   "Proyecto:"
               ForeColor       =   &H00000000&
               Height          =   375
               Left            =   180
               TabIndex        =   32
               Top             =   1380
               Width           =   1905
            End
            Begin VB.Label lblPres 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Index           =   2
               Left            =   2115
               TabIndex        =   31
               Top             =   1335
               Width           =   5805
            End
            Begin VB.Label lblPres 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Index           =   1
               Left            =   2115
               TabIndex        =   30
               Top             =   930
               Width           =   5805
            End
            Begin VB.Label lblLitPres2 
               BackStyle       =   0  'Transparent
               Caption         =   "Centro de coste:"
               ForeColor       =   &H00000000&
               Height          =   375
               Left            =   180
               TabIndex        =   29
               Top             =   975
               Width           =   1905
            End
            Begin VB.Label lblDefecto 
               BackStyle       =   0  'Transparent
               Caption         =   "DValores por defecto para conceptos presupuestarios"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   120
               TabIndex        =   28
               Top             =   240
               Width           =   8505
            End
            Begin VB.Label lblSustituir 
               Caption         =   "DSustituir por:"
               Height          =   255
               Left            =   180
               TabIndex        =   27
               Top             =   2385
               Width           =   1695
            End
         End
      End
      Begin VB.PictureBox picNavigateFSSOL 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   -74760
         ScaleHeight     =   375
         ScaleWidth      =   1605
         TabIndex        =   11
         Top             =   9630
         Width           =   1600
         Begin VB.CommandButton cmdModificarFSSOL 
            Caption         =   "DModificar"
            Height          =   345
            Left            =   0
            TabIndex        =   12
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEditFSSOL 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   -70080
         ScaleHeight     =   375
         ScaleWidth      =   3435
         TabIndex        =   8
         Top             =   9600
         Visible         =   0   'False
         Width           =   3435
         Begin VB.CommandButton cmdAceptarFSSOL 
            Caption         =   "DAceptar"
            Height          =   345
            Left            =   60
            TabIndex        =   10
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdCancelarFSSOL 
            Caption         =   "DCancelar"
            Height          =   345
            Left            =   1440
            TabIndex        =   9
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.Frame fraFSCM 
         Enabled         =   0   'False
         Height          =   780
         Left            =   -74880
         TabIndex        =   6
         Top             =   480
         Width           =   11655
         Begin VB.CheckBox chkCM 
            Caption         =   "DPermitir ver las notificaciones de PM"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   240
            Value           =   1  'Checked
            Width           =   9210
         End
      End
      Begin VB.PictureBox picNavigateFSCM 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -74640
         ScaleHeight     =   375
         ScaleWidth      =   3150
         TabIndex        =   4
         Top             =   9240
         Width           =   3150
         Begin VB.CommandButton cmdCM 
            Caption         =   "DModificar"
            Height          =   345
            Index           =   0
            Left            =   0
            TabIndex        =   5
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEditFSCM 
         BorderStyle     =   0  'None
         Height          =   380
         Left            =   -71400
         ScaleHeight     =   375
         ScaleWidth      =   4695
         TabIndex        =   1
         Top             =   9240
         Visible         =   0   'False
         Width           =   4695
         Begin VB.CommandButton cmdCM 
            Caption         =   "&DAceptar"
            Height          =   345
            Index           =   1
            Left            =   0
            TabIndex        =   3
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdCM 
            Caption         =   "DCancelar"
            Height          =   345
            Index           =   2
            Left            =   1155
            TabIndex        =   2
            Top             =   0
            Width           =   1005
         End
      End
      Begin MSComctlLib.TreeView tvwUsu 
         Height          =   4980
         Left            =   -74850
         TabIndex        =   102
         Top             =   360
         Width           =   7335
         _ExtentX        =   12938
         _ExtentY        =   8784
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin TabDlg.SSTab sstabSM 
         Height          =   6495
         Left            =   -74880
         TabIndex        =   124
         Top             =   360
         Width           =   12975
         _ExtentX        =   22886
         _ExtentY        =   11456
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabHeight       =   520
         TabCaption(0)   =   "DControl presupuestario"
         TabPicture(0)   =   "frmUSUARIOS.frx":6264
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraArboles"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "DActivos"
         TabPicture(1)   =   "frmUSUARIOS.frx":6280
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraActivos"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraArboles 
            Height          =   5955
            Left            =   120
            TabIndex        =   130
            Top             =   360
            Width           =   10455
            Begin VB.Timer Timer 
               Enabled         =   0   'False
               Interval        =   5
               Left            =   6600
               Top             =   5400
            End
            Begin TabDlg.SSTab sstabEstrorg 
               Height          =   5415
               Left            =   45
               TabIndex        =   131
               Top             =   180
               Width           =   10200
               _ExtentX        =   17992
               _ExtentY        =   9551
               _Version        =   393216
               Style           =   1
               TabHeight       =   520
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Imputaci�n de pedidos de aprovisionamiento"
               TabPicture(0)   =   "frmUSUARIOS.frx":629C
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "FrameFSSMTab0"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).ControlCount=   1
               TabCaption(1)   =   "Control de importes"
               TabPicture(1)   =   "frmUSUARIOS.frx":62B8
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "chkNotifDisponible"
               Tab(1).Control(1)=   "FrameFSSMTab1"
               Tab(1).ControlCount=   2
               TabCaption(2)   =   "DGesti�n de facturas"
               TabPicture(2)   =   "frmUSUARIOS.frx":62D4
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "PicTapa2"
               Tab(2).Control(1)=   "tvwestrorg_GesFac"
               Tab(2).ControlCount=   2
               Begin VB.Frame FrameFSSMTab1 
                  Enabled         =   0   'False
                  Height          =   5055
                  Left            =   -74880
                  TabIndex        =   138
                  Top             =   630
                  Width           =   9975
                  Begin UltraGrid.SSUltraGrid ssImportes 
                     Height          =   4965
                     Left            =   0
                     TabIndex        =   139
                     Top             =   90
                     Width           =   9900
                     _ExtentX        =   17463
                     _ExtentY        =   8758
                     _Version        =   131072
                     GridFlags       =   17040384
                     ImageList       =   "ImageList2"
                     Images          =   "frmUSUARIOS.frx":62F0
                     LayoutFlags     =   67108884
                     BorderStyle     =   6
                     RowConnectorStyle=   2
                     BorderStyleCaption=   5
                     InterBandSpacing=   0
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "Small Fonts"
                        Size            =   6.75
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Caption         =   "ssImportes"
                  End
               End
               Begin VB.Frame FrameFSSMTab0 
                  Enabled         =   0   'False
                  Height          =   4935
                  Left            =   120
                  TabIndex        =   134
                  Top             =   420
                  Width           =   9975
                  Begin VB.PictureBox PicTapa 
                     Appearance      =   0  'Flat
                     BackColor       =   &H80000005&
                     BorderStyle     =   0  'None
                     ForeColor       =   &H80000008&
                     Height          =   255
                     Left            =   25
                     Picture         =   "frmUSUARIOS.frx":6322
                     ScaleHeight     =   255
                     ScaleWidth      =   240
                     TabIndex        =   135
                     Top             =   25
                     Width           =   240
                  End
                  Begin MSComctlLib.TreeView tvwestrorg_Con 
                     Height          =   4890
                     Left            =   30
                     TabIndex        =   136
                     Top             =   0
                     Width           =   9945
                     _ExtentX        =   17542
                     _ExtentY        =   8625
                     _Version        =   393217
                     HideSelection   =   0   'False
                     LabelEdit       =   1
                     Style           =   7
                     HotTracking     =   -1  'True
                     ImageList       =   "ImageList2"
                     Appearance      =   1
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "Tahoma"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                  End
                  Begin MSComctlLib.TreeView tvwestrorg_Modif 
                     Height          =   4890
                     Left            =   0
                     TabIndex        =   137
                     Top             =   0
                     Width           =   9945
                     _ExtentX        =   17542
                     _ExtentY        =   8625
                     _Version        =   393217
                     HideSelection   =   0   'False
                     LabelEdit       =   1
                     Style           =   7
                     Checkboxes      =   -1  'True
                     HotTracking     =   -1  'True
                     ImageList       =   "ImageList2"
                     Appearance      =   1
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "Tahoma"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                  End
               End
               Begin VB.PictureBox PicTapa2 
                  Appearance      =   0  'Flat
                  BackColor       =   &H80000005&
                  BorderStyle     =   0  'None
                  ForeColor       =   &H80000008&
                  Height          =   255
                  Left            =   -74850
                  Picture         =   "frmUSUARIOS.frx":651C
                  ScaleHeight     =   255
                  ScaleWidth      =   240
                  TabIndex        =   133
                  Top             =   465
                  Width           =   240
               End
               Begin VB.CheckBox chkNotifDisponible 
                  Caption         =   "DRecibir notificaci�n Control del Disponible"
                  Enabled         =   0   'False
                  Height          =   195
                  Left            =   -74880
                  TabIndex        =   132
                  Top             =   420
                  Width           =   9015
               End
               Begin MSComctlLib.TreeView tvwestrorg_GesFac 
                  Height          =   4890
                  Left            =   -74880
                  TabIndex        =   140
                  Top             =   420
                  Width           =   9945
                  _ExtentX        =   17542
                  _ExtentY        =   8625
                  _Version        =   393217
                  HideSelection   =   0   'False
                  LabelEdit       =   1
                  Style           =   7
                  HotTracking     =   -1  'True
                  ImageList       =   "ImageList2"
                  Appearance      =   1
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
            End
         End
         Begin VB.Frame fraActivos 
            Enabled         =   0   'False
            Height          =   5955
            Left            =   -74880
            TabIndex        =   125
            Top             =   360
            Width           =   10455
            Begin VB.CheckBox chkConsultaActivos 
               Caption         =   "DConsulta de activos"
               Height          =   195
               Left            =   480
               TabIndex        =   129
               Top             =   600
               Width           =   2415
            End
            Begin VB.CheckBox chkAltaActivos 
               Caption         =   "DAlta de nuevos activos"
               Height          =   195
               Left            =   480
               TabIndex        =   128
               Top             =   960
               Width           =   2415
            End
            Begin VB.CheckBox chkModificarActivos 
               Caption         =   "DModificaci�n de activos"
               Height          =   195
               Left            =   480
               TabIndex        =   127
               Top             =   1320
               Width           =   2415
            End
            Begin VB.CheckBox chkEliminarActivos 
               Caption         =   "DEliminaci�n de activos"
               Height          =   195
               Left            =   480
               TabIndex        =   126
               Top             =   1680
               Width           =   2415
            End
         End
      End
      Begin TabDlg.SSTab ssTabQA 
         Height          =   8580
         Left            =   -74880
         TabIndex        =   164
         Top             =   480
         Width           =   9660
         _ExtentX        =   17039
         _ExtentY        =   15134
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "DVariables de calidad"
         TabPicture(0)   =   "frmUSUARIOS.frx":6716
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "sdbgVariables"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "DRestricciones"
         TabPicture(1)   =   "frmUSUARIOS.frx":6732
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "picEditFSQA(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "picNavigateFSQA(0)"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "picFSQA(1)"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).ControlCount=   3
         TabCaption(2)   =   "DPermisos"
         TabPicture(2)   =   "frmUSUARIOS.frx":674E
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "picFSQA(2)"
         Tab(2).Control(1)=   "picNavigateFSQA(1)"
         Tab(2).Control(2)=   "picEditFSQA(1)"
         Tab(2).ControlCount=   3
         TabCaption(3)   =   "DUnidadesNegocio"
         TabPicture(3)   =   "frmUSUARIOS.frx":676A
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "fraPermisosUndNegUsuRead"
         Tab(3).Control(1)=   "cmdCancelarFSQA(2)"
         Tab(3).Control(2)=   "cmdAceptarFSQA(2)"
         Tab(3).Control(3)=   "cmdModificarFSQA(2)"
         Tab(3).Control(4)=   "tvwUndNegUsuRead"
         Tab(3).ControlCount=   5
         Begin VB.PictureBox picEditFSQA 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   1
            Left            =   -71560
            ScaleHeight     =   375
            ScaleWidth      =   4815
            TabIndex        =   226
            Top             =   8295
            Visible         =   0   'False
            Width           =   4815
            Begin VB.CommandButton cmdAceptarFSQA 
               Caption         =   "&DAceptar"
               Height          =   345
               Index           =   1
               Left            =   0
               TabIndex        =   228
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdCancelarFSQA 
               Caption         =   "DCancelar"
               Height          =   345
               Index           =   1
               Left            =   1080
               TabIndex        =   227
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picNavigateFSQA 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   1
            Left            =   -74880
            ScaleHeight     =   375
            ScaleWidth      =   1995
            TabIndex        =   224
            Top             =   8295
            Width           =   2000
            Begin VB.CommandButton cmdModificarFSQA 
               Caption         =   "DModificar"
               Height          =   345
               Index           =   1
               Left            =   0
               TabIndex        =   225
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picFSQA 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   7860
            Index           =   2
            Left            =   -74800
            ScaleHeight     =   7860
            ScaleWidth      =   9345
            TabIndex        =   196
            Top             =   375
            Width           =   9350
            Begin VB.Frame fraQA 
               Caption         =   "DParametros generales"
               Height          =   2895
               Index           =   5
               Left            =   0
               TabIndex        =   214
               Top             =   60
               Width           =   9200
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir modificar el modo de traspaso de proveedores al panel de calidad"
                  Height          =   255
                  Index           =   13
                  Left            =   240
                  TabIndex        =   223
                  Top             =   290
                  Width           =   8000
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir modificar el contacto para certificados autom�ticos"
                  Height          =   255
                  Index           =   14
                  Left            =   240
                  TabIndex        =   222
                  Top             =   560
                  Width           =   8000
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir modificar los par�metros generales de notificaciones por cambios en materiales GS"
                  Height          =   255
                  Index           =   15
                  Left            =   240
                  TabIndex        =   221
                  Top             =   820
                  Width           =   8055
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir modificar el periodo de validez de los certificados expirados"
                  Height          =   255
                  Index           =   16
                  Left            =   240
                  TabIndex        =   220
                  Top             =   1120
                  Width           =   7455
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir modificar el par�metro general de notificaci�n al proveedor por cambio de calificaci�n total"
                  Height          =   195
                  Index           =   17
                  Left            =   240
                  TabIndex        =   219
                  Top             =   1440
                  Width           =   8775
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir modificar los par�metros generales de notificaci�n de proximidad de fechas de fin de una no conformidad"
                  Height          =   315
                  Index           =   18
                  Left            =   240
                  TabIndex        =   218
                  Top             =   1680
                  Width           =   8775
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermiso modificar el nivel o niveles de unidades de negocio a mostrar por defecto al proveedor en sus calificaciones"
                  Height          =   255
                  Index           =   19
                  Left            =   240
                  TabIndex        =   217
                  Top             =   2000
                  Width           =   8775
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "dPermitir modificar el par�metro de selecci�n de proveedores por material en el panel de proveedores"
                  Height          =   255
                  Index           =   20
                  Left            =   240
                  TabIndex        =   216
                  Top             =   2280
                  Width           =   8775
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir ver las notificaciones de QA"
                  ForeColor       =   &H00000000&
                  Height          =   255
                  Index           =   35
                  Left            =   240
                  TabIndex        =   215
                  Top             =   2550
                  Width           =   9210
               End
            End
            Begin VB.Frame fraQA 
               Caption         =   "DMantenimientos"
               Height          =   1665
               Index           =   6
               Left            =   0
               TabIndex        =   208
               Top             =   3000
               Width           =   9200
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir gestionar el mantenimiento de unidades de negocio de QA"
                  Height          =   255
                  Index           =   23
                  Left            =   240
                  TabIndex        =   213
                  Top             =   820
                  Width           =   7695
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir gestionar el Mantenimiento de materiales del QA"
                  Height          =   255
                  Index           =   21
                  Left            =   240
                  TabIndex        =   212
                  Top             =   290
                  Width           =   7335
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir gestionar el mantenimiento de proveedores de QA"
                  Height          =   255
                  Index           =   22
                  Left            =   240
                  TabIndex        =   211
                  Top             =   560
                  Width           =   7695
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir gestionar el mantenimiento de objetivos y suelos de variables de calidad"
                  Height          =   255
                  Index           =   24
                  Left            =   240
                  TabIndex        =   210
                  Top             =   1080
                  Width           =   7695
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir gestionar el mantenimiento de niveles de escalaci�n"
                  Height          =   255
                  Index           =   25
                  Left            =   240
                  TabIndex        =   209
                  Top             =   1340
                  Width           =   7695
               End
            End
            Begin VB.Frame fraQA 
               Caption         =   "DNo Conformidades"
               Height          =   1000
               Index           =   8
               Left            =   0
               TabIndex        =   204
               Top             =   6720
               Width           =   9200
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir cerrar noconformidades aunque las acciones no est�n finalizadas"
                  Height          =   255
                  Index           =   32
                  Left            =   240
                  TabIndex        =   207
                  Top             =   240
                  Width           =   7335
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir reabrir noconformidades cerradas"
                  Height          =   255
                  Index           =   33
                  Left            =   240
                  TabIndex        =   206
                  Top             =   480
                  Width           =   5685
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir revisar el cierre de no conformidades"
                  Height          =   255
                  Index           =   34
                  Left            =   240
                  TabIndex        =   205
                  Top             =   720
                  Width           =   6135
               End
            End
            Begin VB.Frame fraQA 
               Caption         =   "DCertificados"
               Height          =   1920
               Index           =   7
               Left            =   0
               TabIndex        =   197
               Top             =   4740
               Width           =   9200
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir Enviar certificado"
                  Height          =   255
                  Index           =   31
                  Left            =   240
                  TabIndex        =   203
                  Top             =   1560
                  Width           =   8000
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir Renovar certificado"
                  Height          =   255
                  Index           =   30
                  Left            =   240
                  TabIndex        =   202
                  Top             =   1320
                  Width           =   8000
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir solicitar certificado"
                  Height          =   255
                  Index           =   29
                  Left            =   240
                  TabIndex        =   201
                  Top             =   1080
                  Width           =   8000
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir modificar el per�odo de antelaci�n para considerar los certificados pr�ximos a caducar"
                  Height          =   255
                  Index           =   26
                  Left            =   240
                  TabIndex        =   200
                  Top             =   290
                  Width           =   8000
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir publicar/despublicar certificados de otros usuarios"
                  Height          =   255
                  Index           =   27
                  Left            =   240
                  TabIndex        =   199
                  Top             =   560
                  Width           =   8000
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DPermitir modificar los datos de los certificados de otros usuarios"
                  Height          =   255
                  Index           =   28
                  Left            =   240
                  TabIndex        =   198
                  Top             =   820
                  Width           =   8000
               End
            End
         End
         Begin VB.PictureBox picEditFSQA 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   0
            Left            =   -71560
            ScaleHeight     =   375
            ScaleWidth      =   4815
            TabIndex        =   193
            Top             =   6835
            Visible         =   0   'False
            Width           =   4815
            Begin VB.CommandButton cmdCancelarFSQA 
               Caption         =   "DCancelar"
               Height          =   345
               Index           =   0
               Left            =   1170
               TabIndex        =   195
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdAceptarFSQA 
               Caption         =   "&DAceptar"
               Height          =   345
               Index           =   0
               Left            =   0
               TabIndex        =   194
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picNavigateFSQA 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   0
            Left            =   -74800
            ScaleHeight     =   375
            ScaleWidth      =   1995
            TabIndex        =   191
            Top             =   6835
            Width           =   2000
            Begin VB.CommandButton cmdModificarFSQA 
               Caption         =   "DModificar"
               Height          =   345
               Index           =   0
               Left            =   30
               TabIndex        =   192
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picFSQA 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   6280
            Index           =   1
            Left            =   -74800
            ScaleHeight     =   6285
            ScaleWidth      =   9345
            TabIndex        =   172
            Top             =   500
            Width           =   9350
            Begin VB.Frame fraQA 
               Caption         =   "DRestringir la seleccion de revisores de no conformidades"
               Height          =   975
               Index           =   3
               Left            =   0
               TabIndex        =   188
               Top             =   4110
               Width           =   9200
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA A su unidad organizativa"
                  Height          =   315
                  Index           =   9
                  Left            =   240
                  TabIndex        =   190
                  Top             =   270
                  Width           =   4815
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA A su departamento"
                  Height          =   315
                  Index           =   10
                  Left            =   240
                  TabIndex        =   189
                  Top             =   570
                  Width           =   4815
               End
            End
            Begin VB.Frame fraQA 
               Caption         =   "DRestringir la consulta de no conformidades"
               Height          =   1095
               Index           =   2
               Left            =   0
               TabIndex        =   184
               Top             =   2925
               Width           =   9200
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA las realizadas en su unidad organizativa"
                  Height          =   255
                  Index           =   7
                  Left            =   240
                  TabIndex        =   187
                  Top             =   520
                  Width           =   8655
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA las realizadas en su departamento"
                  Height          =   255
                  Index           =   8
                  Left            =   240
                  TabIndex        =   186
                  Top             =   780
                  Width           =   8295
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA las realizadas por el usuario"
                  Height          =   255
                  Index           =   6
                  Left            =   240
                  TabIndex        =   185
                  Top             =   250
                  Width           =   8625
               End
            End
            Begin VB.Frame fraQA 
               Caption         =   "DRestringir los proveedores"
               Height          =   1300
               Index           =   0
               Left            =   0
               TabIndex        =   180
               Top             =   0
               Width           =   9200
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA aquellos que tengan al menos un contacto del departamento de calidad"
                  Height          =   255
                  Index           =   0
                  Left            =   240
                  TabIndex        =   183
                  Top             =   300
                  Width           =   7935
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "Al material del usuario"
                  Height          =   255
                  Index           =   1
                  Left            =   240
                  TabIndex        =   182
                  Top             =   600
                  Width           =   7575
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DAl equipo del usuario"
                  Height          =   255
                  Index           =   2
                  Left            =   240
                  TabIndex        =   181
                  Top             =   900
                  Width           =   8415
               End
            End
            Begin VB.Frame fraQA 
               Caption         =   "DRestringir la consulta de certificados"
               Height          =   1425
               Index           =   1
               Left            =   0
               TabIndex        =   176
               Top             =   1400
               Width           =   9200
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA los solicitados en su unidad de negocio"
                  Height          =   255
                  Index           =   36
                  Left            =   240
                  TabIndex        =   316
                  Top             =   1080
                  Width           =   8415
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA los solicitados en su departamento"
                  Height          =   255
                  Index           =   5
                  Left            =   240
                  TabIndex        =   179
                  Top             =   810
                  Width           =   8415
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA los solicitados en su unidad organizativa"
                  Height          =   255
                  Index           =   4
                  Left            =   240
                  TabIndex        =   178
                  Top             =   540
                  Width           =   8415
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA los solicititados por el usuario"
                  Height          =   255
                  Index           =   3
                  Left            =   240
                  TabIndex        =   177
                  Top             =   270
                  Width           =   8415
               End
            End
            Begin VB.Frame fraQA 
               Caption         =   "DRestringir la selecci�n de notificados internos de las no conformidades"
               Height          =   975
               Index           =   4
               Left            =   0
               TabIndex        =   173
               Top             =   5160
               Width           =   9200
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA A su unidad organizativa"
                  CausesValidation=   0   'False
                  Height          =   255
                  Index           =   12
                  Left            =   240
                  TabIndex        =   175
                  Top             =   600
                  Width           =   4095
               End
               Begin VB.CheckBox chkQA 
                  Caption         =   "DA A su departamento"
                  CausesValidation=   0   'False
                  Height          =   255
                  Index           =   11
                  Left            =   240
                  TabIndex        =   174
                  Top             =   300
                  Width           =   4095
               End
            End
         End
         Begin VB.CommandButton cmdModificarFSQA 
            Caption         =   "DModificar"
            Height          =   345
            Index           =   2
            Left            =   -74760
            TabIndex        =   170
            Top             =   5880
            Width           =   1005
         End
         Begin VB.CommandButton cmdAceptarFSQA 
            Caption         =   "&DAceptar"
            Height          =   345
            Index           =   2
            Left            =   -71640
            TabIndex        =   169
            Top             =   5880
            Width           =   1005
         End
         Begin VB.CommandButton cmdCancelarFSQA 
            Caption         =   "DCancelar"
            Height          =   345
            Index           =   2
            Left            =   -70560
            TabIndex        =   168
            Top             =   5880
            Width           =   1005
         End
         Begin VB.Frame fraPermisosUndNegUsuRead 
            Caption         =   "Frame1"
            Height          =   5055
            Left            =   -70440
            TabIndex        =   165
            Top             =   480
            Width           =   4695
            Begin VB.ListBox lstPermisosUndNegUsu 
               Height          =   4335
               Left            =   240
               Style           =   1  'Checkbox
               TabIndex        =   167
               Top             =   360
               Width           =   4335
            End
            Begin VB.ListBox lstPermisosUndNegUsuRead 
               Height          =   4545
               Left            =   240
               TabIndex        =   166
               Top             =   270
               Width           =   4335
            End
         End
         Begin MSComctlLib.TreeView tvwUndNegUsuRead 
            Height          =   5055
            Left            =   -74760
            TabIndex        =   171
            Top             =   600
            Width           =   4095
            _ExtentX        =   7223
            _ExtentY        =   8916
            _Version        =   393217
            HideSelection   =   0   'False
            Style           =   7
            ImageList       =   "ImageList2"
            Appearance      =   1
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgVariables 
            Height          =   4710
            Left            =   120
            TabIndex        =   229
            Top             =   480
            Width           =   9405
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   9
            stylesets.count =   6
            stylesets(0).Name=   "Nivel1"
            stylesets(0).ForeColor=   16777215
            stylesets(0).BackColor=   8421504
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmUSUARIOS.frx":6786
            stylesets(0).AlignmentText=   0
            stylesets(1).Name=   "Nivel2"
            stylesets(1).BackColor=   13619151
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmUSUARIOS.frx":67A2
            stylesets(1).AlignmentText=   0
            stylesets(2).Name=   "Nivel3"
            stylesets(2).BackColor=   13417139
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmUSUARIOS.frx":67BE
            stylesets(2).AlignmentText=   0
            stylesets(3).Name=   "Normal"
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmUSUARIOS.frx":67DA
            stylesets(4).Name=   "Nivel4"
            stylesets(4).BackColor=   12566463
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmUSUARIOS.frx":67F6
            stylesets(4).AlignmentText=   0
            stylesets(5).Name=   "Nivel5"
            stylesets(5).BackColor=   11513775
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "frmUSUARIOS.frx":6812
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            SelectByCell    =   -1  'True
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   9
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "NIVEL"
            Columns(1).Name =   "NIVEL"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   5
            Columns(2).Width=   10451
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   150
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   2540
            Columns(3).Caption=   "CONS"
            Columns(3).Name =   "CONS"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Style=   2
            Columns(4).Width=   2540
            Columns(4).Caption=   "MOD"
            Columns(4).Name =   "MOD"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Style=   2
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "IDNIVEL1"
            Columns(5).Name =   "IDNIVEL1"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "IDNIVEL2"
            Columns(6).Name =   "IDNIVEL2"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "IDNIVEL3"
            Columns(7).Name =   "IDNIVEL3"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "IDNIVEL4"
            Columns(8).Name =   "IDNIVEL4"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            _ExtentX        =   16589
            _ExtentY        =   8308
            _StockProps     =   79
            BackColor       =   16777215
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin MSComctlLib.TreeView tvwsegur 
         Height          =   5010
         Left            =   60
         TabIndex        =   230
         Top             =   420
         Width           =   3930
         _ExtentX        =   6932
         _ExtentY        =   8837
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin TabDlg.SSTab SSTabCN 
         Height          =   8400
         Left            =   -74880
         TabIndex        =   231
         Top             =   780
         Width           =   10500
         _ExtentX        =   18521
         _ExtentY        =   14817
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "DPermisos"
         TabPicture(0)   =   "frmUSUARIOS.frx":682E
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "picFSCN(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "picNavigateFSCN(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "picEditFSCN(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "DRestricciones"
         TabPicture(1)   =   "frmUSUARIOS.frx":684A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "picFSCN(1)"
         Tab(1).Control(1)=   "picNavigateFSCN(1)"
         Tab(1).Control(2)=   "picEditFSCN(1)"
         Tab(1).ControlCount=   3
         Begin VB.PictureBox picEditFSCN 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   0
            Left            =   3440
            ScaleHeight     =   375
            ScaleWidth      =   4815
            TabIndex        =   297
            Top             =   6840
            Visible         =   0   'False
            Width           =   4815
            Begin VB.CommandButton cmdCancelarCN 
               Caption         =   "DCancelar"
               Height          =   345
               Index           =   0
               Left            =   1080
               TabIndex        =   299
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdAceptarCN 
               Caption         =   "&DAceptar"
               Height          =   345
               Index           =   0
               Left            =   0
               TabIndex        =   298
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picNavigateFSCN 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   0
            Left            =   200
            ScaleHeight     =   375
            ScaleWidth      =   1995
            TabIndex        =   295
            Top             =   6840
            Width           =   2000
            Begin VB.CommandButton cmdModificarCN 
               Caption         =   "DModificar"
               Height          =   345
               Index           =   0
               Left            =   0
               TabIndex        =   296
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picFSCN 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   6280
            Index           =   0
            Left            =   200
            ScaleHeight     =   6285
            ScaleWidth      =   10185
            TabIndex        =   288
            Top             =   480
            Width           =   10185
            Begin VB.Frame fraCNPermis 
               Caption         =   "DOrganizaci�n de la red de colaboraci�n"
               Height          =   975
               Index           =   0
               Left            =   0
               TabIndex        =   292
               Top             =   0
               Width           =   10155
               Begin VB.CheckBox chkCNPOrgRedCat 
                  Caption         =   "DPermitir organizar los contenidos de la red. Gesti�n de categor�as"
                  Height          =   315
                  Left            =   240
                  TabIndex        =   294
                  Top             =   300
                  Width           =   8535
               End
               Begin VB.CheckBox chkCNPOrgRedGr 
                  Caption         =   "DPermitir organizar los miembros de la red. Gesti�n de grupos corporativos"
                  Height          =   315
                  Left            =   240
                  TabIndex        =   293
                  Top             =   570
                  Width           =   8535
               End
            End
            Begin VB.Frame fraCNPermis 
               Caption         =   "DUso de la red de colaboraci�n"
               Height          =   975
               Index           =   1
               Left            =   0
               TabIndex        =   289
               Top             =   1050
               Width           =   10155
               Begin VB.CheckBox chkCNPUsoRedMensUrg 
                  Caption         =   "DPermitir crear mensajes urgentes"
                  CausesValidation=   0   'False
                  Height          =   255
                  Left            =   240
                  TabIndex        =   291
                  Top             =   600
                  Width           =   4095
               End
               Begin VB.CheckBox chkCNPUsoRedMens 
                  Caption         =   "DPermitir crear temas, noticias y eventos"
                  CausesValidation=   0   'False
                  Height          =   255
                  Left            =   240
                  TabIndex        =   290
                  Top             =   300
                  Width           =   4095
               End
            End
         End
         Begin VB.Frame Frame1 
            Caption         =   "Frame1"
            Height          =   5055
            Left            =   -70440
            TabIndex        =   285
            Top             =   480
            Width           =   4695
            Begin VB.ListBox List1 
               Height          =   4545
               Left            =   240
               TabIndex        =   287
               Top             =   240
               Width           =   4335
            End
            Begin VB.ListBox List2 
               Height          =   4335
               Left            =   240
               Style           =   1  'Checkbox
               TabIndex        =   286
               Top             =   360
               Width           =   4335
            End
         End
         Begin VB.CommandButton cmdCancelarFSQA 
            Caption         =   "DCancelar"
            Height          =   345
            Index           =   3
            Left            =   -70560
            TabIndex        =   284
            Top             =   5880
            Width           =   1005
         End
         Begin VB.CommandButton cmdAceptarFSQA 
            Caption         =   "&DAceptar"
            Height          =   345
            Index           =   3
            Left            =   -71640
            TabIndex        =   283
            Top             =   5880
            Width           =   1005
         End
         Begin VB.CommandButton cmdModificarFSQA 
            Caption         =   "DModificar"
            Height          =   345
            Index           =   3
            Left            =   -74760
            TabIndex        =   282
            Top             =   5880
            Width           =   1005
         End
         Begin VB.PictureBox picFSCN 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   6280
            Index           =   1
            Left            =   -74880
            ScaleHeight     =   6285
            ScaleWidth      =   10275
            TabIndex        =   267
            Top             =   480
            Width           =   10275
            Begin VB.Frame fraCNRestric 
               Caption         =   "DEnv�o de mensajes. Estructura de compras"
               Height          =   735
               Index           =   2
               Left            =   0
               TabIndex        =   280
               Top             =   2520
               Width           =   10275
               Begin VB.CheckBox chkCNREqpUsu 
                  Caption         =   "DRestringir el env�o a miembros del equipo de compras del usuario"
                  Height          =   315
                  Left            =   240
                  TabIndex        =   281
                  Top             =   300
                  Width           =   7215
               End
            End
            Begin VB.Frame fraCNRestric 
               Caption         =   "DOrganizaci�n de la red"
               Height          =   1785
               Index           =   3
               Left            =   0
               TabIndex        =   275
               Top             =   3360
               Width           =   10275
               Begin VB.OptionButton optOrgRed4 
                  Caption         =   "DSin restricciones en la gesti�n de categor�as y/o grupos"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   279
                  Top             =   1440
                  Width           =   8415
               End
               Begin VB.OptionButton optOrgRed3 
                  Height          =   255
                  Left            =   240
                  TabIndex        =   278
                  Top             =   1080
                  Width           =   9975
               End
               Begin VB.OptionButton optOrgRed2 
                  Caption         =   "DRestringir la gesti�n de categor�as y/o grupos de la red a las categor�as y/o grupos dados de alta en el departamento del usuario"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   277
                  Top             =   720
                  Width           =   9855
               End
               Begin VB.OptionButton optOrgRed1 
                  Caption         =   "DRestringir la gesti�n de categor�as y/o grupos de la red a las categor�as y/o grupos de la red dados de alta por el usuario"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   276
                  Top             =   360
                  Width           =   9055
               End
            End
            Begin VB.Frame fraCNRestric 
               Caption         =   "DEnv�o de mensajes. Proveedores"
               Height          =   1300
               Index           =   1
               Left            =   0
               TabIndex        =   271
               Top             =   1080
               Width           =   10275
               Begin VB.CheckBox chkCNRProvCon 
                  Caption         =   "DRestringir el env�o de mensajes a proveedores con contacto de calidad"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   274
                  Top             =   900
                  Width           =   8415
               End
               Begin VB.CheckBox chkCNRProEqp 
                  Caption         =   "DRestringir el env�o de mensajes a proveedores del equipo de compras del usuario"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   273
                  Top             =   600
                  Width           =   7575
               End
               Begin VB.CheckBox chkCNRProvMat 
                  Caption         =   "DRestringir el env�o de mensajes a proveedores del material del usuario"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   272
                  Top             =   300
                  Width           =   7935
               End
            End
            Begin VB.Frame fraCNRestric 
               Caption         =   "DEnv�o de mensajes. Estructura organizativa"
               Height          =   975
               Index           =   0
               Left            =   0
               TabIndex        =   268
               Top             =   0
               Width           =   10275
               Begin VB.CheckBox chkCNREstrOrgDep 
                  Caption         =   "DRestringir el env�o a miembros de la red de colaboraci�n del departamento del usuario"
                  Height          =   315
                  Left            =   240
                  TabIndex        =   270
                  Top             =   570
                  Width           =   7815
               End
               Begin VB.CheckBox chkCNREstrOrgUON 
                  Caption         =   "DRestringir el env�o a miembros de la red de colaboraci�n de la unidad organizativa del usuario"
                  Height          =   315
                  Left            =   240
                  TabIndex        =   269
                  Top             =   300
                  Width           =   7935
               End
            End
         End
         Begin VB.PictureBox picNavigateFSCN 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   1
            Left            =   -74800
            ScaleHeight     =   375
            ScaleWidth      =   1995
            TabIndex        =   265
            Top             =   6840
            Width           =   2000
            Begin VB.CommandButton cmdModificarCN 
               Caption         =   "DModificar"
               Height          =   345
               Index           =   1
               Left            =   0
               TabIndex        =   266
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picEditFSCN 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   1
            Left            =   -71560
            ScaleHeight     =   375
            ScaleWidth      =   4815
            TabIndex        =   262
            Top             =   6840
            Visible         =   0   'False
            Width           =   4815
            Begin VB.CommandButton cmdAceptarCN 
               Caption         =   "&DAceptar"
               Height          =   345
               Index           =   1
               Left            =   0
               TabIndex        =   264
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdCancelarCN 
               Caption         =   "DCancelar"
               Height          =   345
               Index           =   1
               Left            =   1080
               TabIndex        =   263
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picFSQA 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   7440
            Index           =   3
            Left            =   -74800
            ScaleHeight     =   7440
            ScaleWidth      =   9345
            TabIndex        =   237
            Top             =   375
            Width           =   9350
            Begin VB.Frame FraPermisosQA 
               Caption         =   "DMantenimientos"
               Height          =   1425
               Index           =   4
               Left            =   0
               TabIndex        =   257
               Top             =   2685
               Width           =   9200
               Begin VB.CheckBox chkQAMantenimientos 
                  Caption         =   "DPermitir gestionar el mantenimiento de objetivos y suelos de variables de calidad"
                  Height          =   255
                  Index           =   4
                  Left            =   240
                  TabIndex        =   261
                  Top             =   1080
                  Width           =   7695
               End
               Begin VB.CheckBox chkQAMantenimientos 
                  Caption         =   "DPermitir gestionar el mantenimiento de proveedores de QA"
                  Height          =   255
                  Index           =   5
                  Left            =   240
                  TabIndex        =   260
                  Top             =   560
                  Width           =   7695
               End
               Begin VB.CheckBox chkQAMantenimientos 
                  Caption         =   "DPermitir gestionar el Mantenimiento de materiales del QA"
                  Height          =   255
                  Index           =   6
                  Left            =   240
                  TabIndex        =   259
                  Top             =   290
                  Width           =   7335
               End
               Begin VB.CheckBox chkQAMantenimientos 
                  Caption         =   "DPermitir gestionar el mantenimiento de unidades de negocio de QA"
                  Height          =   255
                  Index           =   7
                  Left            =   240
                  TabIndex        =   258
                  Top             =   820
                  Width           =   7695
               End
            End
            Begin VB.Frame FraPermisosQA 
               Caption         =   "DParametros generales"
               Height          =   2440
               Index           =   5
               Left            =   0
               TabIndex        =   249
               Top             =   175
               Width           =   9200
               Begin VB.CheckBox Check1 
                  Caption         =   "DPermiso modificar el nivel o niveles de unidades de negocio a mostrar por defecto al proveedor en sus calificaciones"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   256
                  Top             =   2000
                  Width           =   8775
               End
               Begin VB.CheckBox Check2 
                  Caption         =   "DPermitir modificar los par�metros generales de notificaci�n de proximidad de fechas de fin de una no conformidad"
                  Height          =   315
                  Left            =   240
                  TabIndex        =   255
                  Top             =   1680
                  Width           =   8775
               End
               Begin VB.CheckBox Check3 
                  Caption         =   "DPermitir modificar el par�metro general de notificaci�n al proveedor por cambio de calificaci�n total"
                  Height          =   195
                  Left            =   240
                  TabIndex        =   254
                  Top             =   1440
                  Width           =   8775
               End
               Begin VB.CheckBox Check4 
                  Caption         =   "DPermitir modificar el periodo de validez de los certificados expirados"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   253
                  Top             =   1120
                  Width           =   7455
               End
               Begin VB.CheckBox Check5 
                  Caption         =   "DPermitir modificar los par�metros generales de notificaciones por cambios en materiales GS"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   252
                  Top             =   820
                  Width           =   8055
               End
               Begin VB.CheckBox Check6 
                  Caption         =   "DPermitir modificar el contacto para certificados autom�ticos"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   251
                  Top             =   560
                  Width           =   8000
               End
               Begin VB.CheckBox Check7 
                  Caption         =   "DPermitir modificar el modo de traspaso de proveedores al panel de calidad"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   250
                  Top             =   290
                  Width           =   8000
               End
            End
            Begin VB.Frame FraPermisosQA 
               Caption         =   "DCertificados"
               Height          =   1920
               Index           =   6
               Left            =   0
               TabIndex        =   242
               Top             =   4185
               Width           =   9200
               Begin VB.CheckBox chkAccionesCertificado 
                  Caption         =   "DPermitir Enviar certificado"
                  Height          =   255
                  Index           =   3
                  Left            =   240
                  TabIndex        =   248
                  Top             =   1560
                  Width           =   8000
               End
               Begin VB.CheckBox chkAccionesCertificado 
                  Caption         =   "DPermitir Renovar certificado"
                  Height          =   255
                  Index           =   4
                  Left            =   240
                  TabIndex        =   247
                  Top             =   1320
                  Width           =   8000
               End
               Begin VB.CheckBox chkAccionesCertificado 
                  Caption         =   "DPermitir solicitar certificado"
                  Height          =   255
                  Index           =   5
                  Left            =   240
                  TabIndex        =   246
                  Top             =   1080
                  Width           =   8000
               End
               Begin VB.CheckBox Check8 
                  Caption         =   "DPermitir modificar el per�odo de antelaci�n para considerar los certificados pr�ximos a caducar"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   245
                  Top             =   290
                  Width           =   8000
               End
               Begin VB.CheckBox Check9 
                  Caption         =   "DPermitir publicar/despublicar certificados de otros usuarios"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   244
                  Top             =   560
                  Width           =   8000
               End
               Begin VB.CheckBox Check10 
                  Caption         =   "DPermitir modificar los datos de los certificados de otros usuarios"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   243
                  Top             =   820
                  Width           =   8000
               End
            End
            Begin VB.Frame FraPermisosQA 
               Caption         =   "DNo Conformidades"
               Height          =   1000
               Index           =   7
               Left            =   0
               TabIndex        =   238
               Top             =   6180
               Width           =   9200
               Begin VB.CheckBox Check11 
                  Caption         =   "DPermitir revisar el cierre de no conformidades"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   241
                  Top             =   720
                  Width           =   6135
               End
               Begin VB.CheckBox Check12 
                  Caption         =   "DPermitir reabrir noconformidades cerradas"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   240
                  Top             =   480
                  Width           =   5685
               End
               Begin VB.CheckBox Check13 
                  Caption         =   "DPermitir cerrar noconformidades aunque las acciones no est�n finalizadas"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   239
                  Top             =   240
                  Width           =   7335
               End
            End
         End
         Begin VB.PictureBox picNavigateFSQA 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   3
            Left            =   -74880
            ScaleHeight     =   375
            ScaleWidth      =   1995
            TabIndex        =   235
            Top             =   7875
            Width           =   2000
            Begin VB.CommandButton cmdModificarFSQA 
               Caption         =   "DModificar"
               Height          =   345
               Index           =   5
               Left            =   0
               TabIndex        =   236
               Top             =   0
               Width           =   1005
            End
         End
         Begin VB.PictureBox picEditFSQA 
            BorderStyle     =   0  'None
            Height          =   380
            Index           =   3
            Left            =   -71560
            ScaleHeight     =   375
            ScaleWidth      =   4815
            TabIndex        =   232
            Top             =   7875
            Visible         =   0   'False
            Width           =   4815
            Begin VB.CommandButton cmdCancelarFSQA 
               Caption         =   "DCancelar"
               Height          =   345
               Index           =   5
               Left            =   1080
               TabIndex        =   234
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdAceptarFSQA 
               Caption         =   "&DAceptar"
               Height          =   345
               Index           =   5
               Left            =   0
               TabIndex        =   233
               Top             =   0
               Width           =   1005
            End
         End
         Begin MSComctlLib.TreeView TreeView1 
            Height          =   5055
            Left            =   -74760
            TabIndex        =   300
            Top             =   600
            Width           =   4095
            _ExtentX        =   7223
            _ExtentY        =   8916
            _Version        =   393217
            HideSelection   =   0   'False
            Style           =   7
            ImageList       =   "ImageList2"
            Appearance      =   1
         End
         Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
            Height          =   4710
            Left            =   -74880
            TabIndex        =   301
            Top             =   480
            Width           =   9405
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   9
            stylesets.count =   6
            stylesets(0).Name=   "Nivel1"
            stylesets(0).ForeColor=   16777215
            stylesets(0).BackColor=   8421504
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmUSUARIOS.frx":6866
            stylesets(0).AlignmentText=   0
            stylesets(1).Name=   "Nivel2"
            stylesets(1).BackColor=   13619151
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmUSUARIOS.frx":6882
            stylesets(1).AlignmentText=   0
            stylesets(2).Name=   "Nivel3"
            stylesets(2).BackColor=   13417139
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmUSUARIOS.frx":689E
            stylesets(2).AlignmentText=   0
            stylesets(3).Name=   "Nivel4"
            stylesets(3).BackColor=   12566463
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmUSUARIOS.frx":68BA
            stylesets(3).AlignmentText=   0
            stylesets(4).Name=   "Normal"
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmUSUARIOS.frx":68D6
            stylesets(5).Name=   "Nivel5"
            stylesets(5).BackColor=   11513775
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "frmUSUARIOS.frx":68F2
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            SelectByCell    =   -1  'True
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   9
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "NIVEL"
            Columns(1).Name =   "NIVEL"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   5
            Columns(2).Width=   10451
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   150
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   2540
            Columns(3).Caption=   "CONS"
            Columns(3).Name =   "CONS"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Style=   2
            Columns(4).Width=   2540
            Columns(4).Caption=   "MOD"
            Columns(4).Name =   "MOD"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Style=   2
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "IDNIVEL1"
            Columns(5).Name =   "IDNIVEL1"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "IDNIVEL2"
            Columns(6).Name =   "IDNIVEL2"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "IDNIVEL3"
            Columns(7).Name =   "IDNIVEL3"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "IDNIVEL4"
            Columns(8).Name =   "IDNIVEL4"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            _ExtentX        =   16589
            _ExtentY        =   8308
            _StockProps     =   79
            BackColor       =   16777215
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin SHDocVwCtl.WebBrowser wPerfil 
         Height          =   7785
         Left            =   -74880
         TabIndex        =   302
         Top             =   480
         Width           =   13515
         ExtentX         =   23839
         ExtentY         =   13732
         ViewMode        =   0
         Offline         =   0
         Silent          =   0
         RegisterAsBrowser=   0
         RegisterAsDropTarget=   1
         AutoArrange     =   0   'False
         NoClientEdge    =   0   'False
         AlignLeft       =   0   'False
         NoWebView       =   0   'False
         HideFileNames   =   0   'False
         SingleClick     =   0   'False
         SingleSelection =   0   'False
         NoFolders       =   0   'False
         Transparent     =   0   'False
         ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
         Location        =   ""
      End
   End
End
Attribute VB_Name = "frmUSUARIOS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Const NUM_ACCIONES = 100

'VARIABLES PUBLICAS
Public Accion As AccionesSummit
Public g_oIBaseDatos As IBaseDatos
Public g_oUsuario As CUsuario
Public g_SelNode As MSComctlLib.node 'Nodo para recoger el recibido en frmUSUARIOS.tvwestrorg_Con_MouseUp y poder usarlo en frmUSUARIOSSelGestores
'para el cambio de c�digo
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean
Public m_bRespetarComboDest As Boolean
Public m_bDescargarFrm As Boolean

'VARIABLES PRIVADAS
Private m_bActivado As Boolean
Private m_sConsulta As String
Private m_sModificacion As String
Private m_sAlta As String
Private m_sPresup As String
Private m_bValorAnteriorCeldaModif As Boolean
Private m_bValorAnteriorCeldaCon As Boolean
Private m_bValorAnteriorCeldaAlta As Boolean
Private m_bValorAnteriorCeldaPresup As Boolean
Private m_sModoArbolIPA As String   '"C o M"    Modo del arbol de Imputacion de pedidos de aprovisionamiento
Private m_sModoTabEstrOrg As String   '"C o M"    Modo C o M del Tab sstabEstrorg
Private m_oUsuarios As CUsuarios
Private m_sCaption As String
Private m_oPerfil As CPerfil
Private m_oPerfiles As CPerfiles
Private m_oPerfilBaseSeleccionado As CPerfil
Private m_oPlantillaDeAccionesDeSeguridad As CAcciones
Private m_oPlantillaDeAccionesCargadas As CAcciones 'Para controlar los indices cuando no se muestran  todas
Private m_oAccionesAAnyadir As CAcciones
Private m_oAccionesAEliminar As CAcciones
Private m_oAccionesIniciales As CAcciones
Private m_bCargarComboDesde As Boolean
Private m_oDestinos As CDestinos
Private m_bRespetarChange As Boolean
Private InterDep As InterDepAcciones
Private WithEvents cP As cPopupMenu
Attribute cP.VB_VarHelpID = -1
'para el resize
Private m_iIteracion As Integer
'para el Multilenguaje
Private m_sIdiPersonalizado As String '(Personalizado)
Private m_sIdiA�aUsu As String 'A�adir Usuario
Private m_sIdiUsuarioCod As String 'Usuario (C�digo)
Private m_sIdiCodigo As String 'C�digo
Private m_sIdiEliminar As String 'Se va a eliminar el usuario:
Private m_sIdiContinuar As String '�Continuar?
Private m_sIdiModifUsu As String 'Modificar Usuario
Private m_sIdiDatUsu As String 'Datos del usuario
Private m_sIdiDetalle As String 'Detalle
Private m_sIdiUsuarios As String 'Usuarios
Private m_sIdiUsuario As String
Private m_sAcceso As String
Private m_oIdiomas As CIdiomas
Private m_sAsignarPartidasGestor As String
Private m_bNotifDisponibleAnt As Boolean
Private mNode As node
Private m_sEstructuraUnidadesNegocio As String '"C o M"    Modo del arbol de Unidades de negocio para el usuario
Private m_oPermisosUndNegUsuarioIniciales As CUnidadesNegQA
Private m_oPermisosUndNegUsuarioAgregar As CUnidadesNegQA
Private m_oPermisosUndNegUsuarioEliminar As CUnidadesNegQA
Private m_oRecargaPermisosUndNegUsuario As Boolean
Private m_colBookmarks As Collection
Private m_sPermisosEscalacion(0 To 11) As String
Private m_sNivelesEscalacion As String
Private m_sMsgError As String

Private Sub ReiniciarTvw()
    Dim i As Integer
    For i = 1 To tvwsegur.Nodes.Count
        tvwsegur.Nodes(i).Expanded = False
    Next i
    tvwsegur.Nodes(1).Expanded = True
    If Me.Visible Then tvwsegur.SetFocus
End Sub

''' Revisado por: blp. Fecha: 23/10/2012
''' Carga el treeview de usuarios
''' Llamada desde frmUSUARIOS. M�x. 0,5 seg.
Private Sub RefrescarTvw()
    Dim nodx As node
    Dim sDenPerfil As String
    Dim m_bActivar As Boolean
    Dim sDenAcceso As String

    If g_oUsuario Is Nothing Then Exit Sub
    
    m_bActivar = False
    
    If Accion <> ACCusuEli Then
        If Not g_oUsuario.Perfil Is Nothing Then
            sDenPerfil = " / " & g_oUsuario.Perfil.Den
            
            sDenAcceso = ""
            If g_oUsuario.Perfil.FSGS Then
                sDenAcceso = sDenAcceso & "GS;"
            End If
            If g_oUsuario.Perfil.FSEP And gParametrosGenerales.gbPedidosAprov = True Then
                sDenAcceso = sDenAcceso & "EP;"
            End If
            If g_oUsuario.Perfil.FSPM Then
                If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso Then
                    sDenAcceso = sDenAcceso & "PM;"
                End If
            End If
            If g_oUsuario.Perfil.FSQA Then
                If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
                    sDenAcceso = sDenAcceso & "QA;"
                End If
            End If
            If gParametrosGenerales.gsAccesoFSSM <> TipoAccesoFSSM.SinAcceso Then
                'Incidencia 31800.4 2008 14
                'sDenAcceso = sDenAcceso & "SM;"
                If g_oUsuario.Perfil.FSEP And gParametrosGenerales.gbPedidosAprov Then
                    sDenAcceso = sDenAcceso & "SM;"
                End If
            End If
            If g_oUsuario.Perfil.FSIM Then
                If gParametrosGenerales.gsAccesoFSIM <> TipoAccesoFSIM.SinAcceso Then
                    sDenAcceso = sDenAcceso & "IM;"
                End If
            End If
            If g_oUsuario.Perfil.FSCM Then
                If gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.SinAcceso Then
                    sDenAcceso = sDenAcceso & "CM;"
                End If
            End If
            If g_oUsuario.Perfil.FSIS And gParametrosGenerales.gbIntegracion Then
                sDenAcceso = sDenAcceso & "IS;"
            End If
            If g_oUsuario.Perfil.FSBI Then
                If gParametrosGenerales.gsAccesoFSBI <> TipoAccesoFSBI.SinAcceso Then
                    sDenAcceso = sDenAcceso & "BI;"
                End If
            End If
        Else
            sDenPerfil = ""
            
            sDenAcceso = ""
            If g_oUsuario.AccesoFSGS = True Then
                sDenAcceso = sDenAcceso & "GS;"
            End If
            If g_oUsuario.AccesoFSEP = True And gParametrosGenerales.gbPedidosAprov = True Then
                sDenAcceso = sDenAcceso & "EP;"
            End If
            If g_oUsuario.AccesoFSWS = True Then
                If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso Then
                    sDenAcceso = sDenAcceso & "PM;"
                End If
            End If
            If g_oUsuario.AccesoFSQA = True Then
                If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
                    sDenAcceso = sDenAcceso & "QA;"
                End If
            End If
            If gParametrosGenerales.gsAccesoFSSM <> TipoAccesoFSSM.SinAcceso Then
                'Incidencia 31800.4 2008 14
                'sDenAcceso = sDenAcceso & "SM;"
                If g_oUsuario.AccesoFSEP And gParametrosGenerales.gbPedidosAprov Then
                    sDenAcceso = sDenAcceso & "SM;"
                End If
            End If
            If g_oUsuario.AccesoFSIM = True Then
                If gParametrosGenerales.gsAccesoFSIM <> TipoAccesoFSIM.SinAcceso Then
                    sDenAcceso = sDenAcceso & "IM;"
                End If
            End If
            If g_oUsuario.AccesoFSContratos = True Then
                If gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.SinAcceso Then
                    sDenAcceso = sDenAcceso & "CM;"
                End If
            End If
            If g_oUsuario.AccesoFSINT And gParametrosGenerales.gbIntegracion Then
                sDenAcceso = sDenAcceso & "IS;"
            End If
            If g_oUsuario.AccesoFSBI Then
                If gParametrosGenerales.gsAccesoFSBI <> TipoAccesoFSBI.SinAcceso Then
                    sDenAcceso = sDenAcceso & "BI;"
                End If
            End If
        End If

        'Quitar el �ltimo ; de la l�nea del usuario
        If sDenAcceso <> "" Then
            sDenAcceso = Mid(sDenAcceso, 1, Len(sDenAcceso) - 1)
            sDenAcceso = " / " & m_sAcceso & sDenAcceso
        End If
        
        Me.caption = m_sIdiUsuario & " " & g_oUsuario.Cod
    End If
    
    
    Select Case Accion
    
        Case ACCUsuAnya
            Set nodx = tvwUsu.Nodes.Add("Root", tvwChild, "Cod" & g_oUsuario.Cod, g_oUsuario.Cod & ", " & g_oUsuario.Persona.Apellidos & ", " & g_oUsuario.Persona.nombre & ", " & g_oUsuario.Persona.DenDep & sDenPerfil & sDenAcceso, "Usuario")
            If g_oUsuario.Tipo = TIpoDeUsuario.comprador Then
                nodx.Tag = "Comprador"
            Else
                nodx.Tag = "Persona"
            End If
            m_bActivar = True
            nodx.Selected = True
                
        Case ACCUsuMod
            Set nodx = tvwUsu.selectedItem
            g_oUsuario.ExpandirUsuario
            nodx.Text = g_oUsuario.Cod & ", " & g_oUsuario.Persona.Apellidos & ", " & g_oUsuario.Persona.nombre & ", " & g_oUsuario.Persona.DenDep & sDenPerfil & sDenAcceso
            If g_oUsuario.Tipo = TIpoDeUsuario.comprador Then
                nodx.Tag = "Comprador"
            Else
                nodx.Tag = "Persona"
            End If
            nodx.key = "Cod" & g_oUsuario.Cod
            m_bActivar = True
        
        Case ACCusuEli
            tvwUsu.Nodes.Remove tvwUsu.selectedItem.key
            
    End Select
    
    Accion = accusucon
    If m_bActivar Then
        cmdUsu(1).Enabled = True
        cmdUsu(2).Enabled = True
        cmdUsu(3).Enabled = True
    End If
    tvwUsu_NodeClick tvwUsu.selectedItem
    
End Sub

''' <summary>Genera la estructura del �rbol de usuarios</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 08/01/2013</revision>

Public Sub GenerarEstructuraUsuarios()
    Dim oUsuario As CUsuario
    Dim nodx As node
    Dim sDenPerfil As String
    Dim sDenAcceso As String
    
    tvwUsu.Nodes.clear
    
    Set nodx = tvwUsu.Nodes.Add(, , "Root", m_sIdiUsuarios, "Usuarios")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    nodx.Selected = True
    
    If Not m_oUsuarios Is Nothing Then
        
        For Each oUsuario In m_oUsuarios
            
            If Not oUsuario.Perfil Is Nothing Then
                sDenPerfil = " / " & oUsuario.Perfil.Den
                
                sDenAcceso = ""
                If oUsuario.Perfil.FSGS Then
                    sDenAcceso = sDenAcceso & "GS;"
                End If
                If oUsuario.Perfil.FSEP And gParametrosGenerales.gbPedidosAprov Then
                    sDenAcceso = sDenAcceso & "EP;"
                End If
                If oUsuario.Perfil.FSPM Then
                    If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso Then
                        sDenAcceso = sDenAcceso & "PM;"
                    End If
                End If
                If oUsuario.Perfil.FSQA Then
                    If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
                        sDenAcceso = sDenAcceso & "QA;"
                    End If
                End If
                If gParametrosGenerales.gsAccesoFSSM <> TipoAccesoFSSM.SinAcceso Then
                    'Incidencia 31800.4 2008 14
                    'sDenAcceso = sDenAcceso & "SM;"
                    If oUsuario.Perfil.FSEP = True And gParametrosGenerales.gbPedidosAprov = True Then
                        sDenAcceso = sDenAcceso & "SM;"
                    End If
                End If
                If oUsuario.Perfil.FSIM Then
                    If gParametrosGenerales.gsAccesoFSIM <> TipoAccesoFSIM.SinAcceso Then
                        sDenAcceso = sDenAcceso & "IM;"
                    End If
                End If
                If oUsuario.Perfil.FSCM Then
                    If gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.SinAcceso Then
                        sDenAcceso = sDenAcceso & "CM;"
                    End If
                End If
                If oUsuario.Perfil.FSIS And gParametrosGenerales.gbIntegracion Then
                    sDenAcceso = sDenAcceso & "IS;"
                End If
                If oUsuario.Perfil.FSBI And gParametrosGenerales.gsAccesoFSBI = AccesoFSBI Then
                    sDenAcceso = sDenAcceso & "BI;"
                End If
            Else
                sDenPerfil = ""
                
                sDenAcceso = ""
                If oUsuario.AccesoFSGS Then
                    sDenAcceso = sDenAcceso & "GS;"
                End If
                If oUsuario.AccesoFSEP And gParametrosGenerales.gbPedidosAprov Then
                    sDenAcceso = sDenAcceso & "EP;"
                End If
                If oUsuario.AccesoFSWS Then
                    If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso Then
                        sDenAcceso = sDenAcceso & "PM;"
                    End If
                End If
                If oUsuario.AccesoFSQA Then
                    If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
                        sDenAcceso = sDenAcceso & "QA;"
                    End If
                End If
                If gParametrosGenerales.gsAccesoFSSM <> TipoAccesoFSSM.SinAcceso Then
                    'Incidencia 31800.4 2008 14
                    'sDenAcceso = sDenAcceso & "SM;"
                    If oUsuario.AccesoFSEP = True And gParametrosGenerales.gbPedidosAprov = True Then
                        sDenAcceso = sDenAcceso & "SM;"
                    End If
                End If
                If oUsuario.AccesoFSIM Then
                    If gParametrosGenerales.gsAccesoFSIM <> TipoAccesoFSIM.SinAcceso Then
                        sDenAcceso = sDenAcceso & "IM;"
                    End If
                End If
                If oUsuario.AccesoFSContratos Then
                    If gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.SinAcceso Then
                        sDenAcceso = sDenAcceso & "CM;"
                    End If
                End If
                If oUsuario.AccesoFSINT And gParametrosGenerales.gbIntegracion Then
                    sDenAcceso = sDenAcceso & "IS;"
                End If
                If oUsuario.AccesoFSBI And gParametrosGenerales.gsAccesoFSBI = AccesoFSBI Then
                    sDenAcceso = sDenAcceso & "BI;"
                End If
            End If
            
            If sDenAcceso <> "" Then
                sDenAcceso = Mid(sDenAcceso, 1, Len(sDenAcceso) - 1)
                sDenAcceso = " / " & m_sAcceso & sDenAcceso
            End If
            
            Set nodx = tvwUsu.Nodes.Add("Root", tvwChild, "Cod" & oUsuario.Cod, oUsuario.Cod & ", " & oUsuario.Persona.Apellidos & ", " & oUsuario.Persona.nombre & ", " & oUsuario.Persona.DenDep & sDenPerfil & sDenAcceso, "Usuario")
            Select Case oUsuario.Tipo
                Case TIpoDeUsuario.comprador
                    nodx.Tag = "Comprador"
                Case TIpoDeUsuario.Persona
                    nodx.Tag = "Persona"
            End Select
                         
        Next
    
    End If
    
    Set nodx = Nothing
End Sub

''' <summary>
''' Cargar los permisos/restricciones del menu gs
''' </summary>
''' <param name="iAccion">menu gs</param>
''' <param name="iNumAccionesEnLista">numero de acciones del menu gs</param>
''' <remarks>Llamada desde: tvwSegur_NodeClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub LlenarListaConPlantillaDeAcciones(ByVal iAccion As Integer, ByVal iNumAccionesEnLista As Integer)
Dim oAccion As CAccion
Dim iIndice As Integer

    lstvwSegur.ListItems.clear
    lstSegurIdAcciones.clear
    Set m_oPlantillaDeAccionesCargadas = Nothing
    Set m_oPlantillaDeAccionesCargadas = oFSGSRaiz.Generar_CAcciones
    
    For iIndice = 0 To iNumAccionesEnLista - 1
    
        Set oAccion = m_oPlantillaDeAccionesDeSeguridad.Item(CStr(iAccion + iIndice))
        If oAccion Is Nothing Then
            'Exit For: Se excluyen las acciones con uo -> la de orden 12027  se exclu�a y cortaba
        Else
            If NoCargarAccion(oAccion.Id) Then
            Else
                lstvwSegur.ListItems.Add Text:=oAccion.Den, key:="A" & CStr(oAccion.Id)
                lstSegurIdAcciones.AddItem oAccion.Id
                m_oPlantillaDeAccionesCargadas.Add oAccion.Id, oAccion.Den
                m_oPlantillaDeAccionesCargadas.Item(CStr(oAccion.Id)).Orden = oAccion.Orden
                Set oAccion = Nothing
            End If
        
        End If

    Next

End Sub

Private Sub MarcarAccionesDelUsuario()
Dim iIndice As Integer
Dim iNumDeAcciones As Integer

iNumDeAcciones = lstSegurIdAcciones.ListCount
 
For iIndice = 1 To iNumDeAcciones
    
   If Not m_oAccionesIniciales Is Nothing Then
        
        If Not m_oAccionesIniciales.Item(lstSegurIdAcciones.List(iIndice - 1)) Is Nothing Then
            lstvwSegur.ListItems(iIndice).Checked = True
        End If
    
    End If
    
Next
    
End Sub


''' <summary>
''' Evento que ocurre cuando se hace click en el control
''' </summary>
''' <remarks>Se utiliza para chequear automaticamente la opci�n
''' de consulta de activos cuando se marca la opci�n de alta de activos</remarks>
Private Sub chkAltaActivos_Click()
    If CBool(chkAltaActivos.Value) Then
        chkConsultaActivos.Value = 1
    End If
End Sub

Private Sub chkCNPUsoRedMensUrg_Click()
    If chkCNPUsoRedMensUrg.Value = 1 Then
        chkCNPUsoRedMens.Value = 1
    End If
End Sub

Private Sub chkCNPUsoRedMens_Click()
    If chkCNPUsoRedMens.Value = 0 Then
        chkCNPUsoRedMensUrg.Value = 0
    End If
End Sub

''' <summary>
''' Evento que ocurre cuando se hace click en el control
''' </summary>
''' <remarks>Se utiliza para desmarcar automaticamente las opciones de alta, modificaci�n y eliminaci�n de activos cuando se desmarca la
''' opci�n de consulta de activos</remarks>
Private Sub chkConsultaActivos_Click()
    If chkConsultaActivos.Value = 0 Then
        chkAltaActivos.Value = 0
        chkModificarActivos.Value = 0
        chkEliminarActivos.Value = 0
    Else
        If CBool(chkAltaActivos.Value) Or CBool(chkModificarActivos.Value) Or CBool(chkEliminarActivos.Value) Then
            chkConsultaActivos.Value = 1
        End If
    End If
End Sub

''' <summary>
''' Evento que ocurre cuando se hace click en el control
''' </summary>
''' <remarks>Se utiliza para chequear automaticamente la opci�n
''' de consulta de activos cuando se marca la opci�n de eliminar activos</remarks>
Private Sub chkEliminarActivos_Click()
    If CBool(chkEliminarActivos.Value) Then
        chkConsultaActivos.Value = 1
    End If
End Sub





''' <summary>
''' Evento que ocurre cuando se hace click en el control
''' </summary>
''' <remarks>Se utiliza para chequear automaticamente la opci�n
''' de consulta de activos cuando se marca la opci�n de modificar activos</remarks>
Private Sub chkModificarActivos_Click()
    If CBool(chkModificarActivos.Value) Then
        chkConsultaActivos.Value = 1
    End If
End Sub

Private Sub chkSOL_Click(Index As Integer)
    Select Case Index
        Case 13 'Restringir la creaci�n de escenarios para otros usuaruios a usuarios de la unidad organizativa del usuario
            If Not chkSOL(13).Value = 1 Then
                chkSOL(14).Value = 0
            End If
        Case 14 'Restringir la creaci�n de escenarios para otros usuaruios a usuarios del departamento del usuario
            If chkSOL(14).Value = 1 Then
                chkSOL(13).Value = 1
            End If
        Case 15 'Restringir la edici�n de escenarios compartidos no creados por el usuario a escenarios de unidad organizativa del usuario
            If Not chkSOL(15).Value = 1 Then
                chkSOL(16).Value = 0
            End If
        Case 16 'Restringir la edici�n de escenarios compartidos no creados por el usuario a escenarios del departamento del usuario
            If chkSOL(16).Value = 1 Then
                chkSOL(15).Value = 1
            End If
        Case 18 'Activar sustituci�n temporal
            If chkSOL(18).Value = vbChecked Then
                cmdBuscarSust.Enabled = True
                cmdLimpiarSust.Enabled = True
            Else
                txtSustituto.Text = ""
                txtSustituto.Tag = ""
                cmdBuscarSust.Enabled = False
                cmdLimpiarSust.Enabled = False
            End If
    End Select
End Sub

''' <summary>
''' Guarda los datos de CN del usuario
''' </summary>
''' <param name="Index">Le indica la pesta�a dentro de CN en la que estamos</param>
''' <remarks>Llamada desde Boton Aceptar de cualquiera de las pesta�as de CN; Tiempo m�ximo < 1 seg</remarks>
''' <revision>JVS 26/01/2012</revision>
Private Sub cmdAceptarCN_Click(Index As Integer)
Dim teserror As TipoErrorSummit
    
    Screen.MousePointer = vbHourglass
    
    If Index = 0 Then
        g_oUsuario.CNPermitirGestionCategorias = Me.chkCNPOrgRedCat.Value
        g_oUsuario.CNPermitirGestionGrupos = Me.chkCNPOrgRedGr.Value
        g_oUsuario.CNPermitirCrearMens = Me.chkCNPUsoRedMens.Value
        g_oUsuario.CNPermitirCrearMensUrgentes = Me.chkCNPUsoRedMensUrg.Value
                            
        teserror.NumError = TESnoerror
        teserror = g_oUsuario.ModificarPermisosFSCN()
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            Exit Sub
        End If
    Else
        g_oUsuario.CNRestrEnvioUON = Me.chkCNREstrOrgUON.Value
        g_oUsuario.CNRestrEnvioDep = Me.chkCNREstrOrgDep.Value
        g_oUsuario.CNRestrEnvioProveedMat = Me.chkCNRProvMat.Value
        g_oUsuario.CNRestrEnvioProveedEqp = Me.chkCNRProEqp.Value
        g_oUsuario.CNRestrEnvioProveedCon = Me.chkCNRProvCon.Value
        g_oUsuario.CNRestrEnvioEqpUsu = Me.chkCNREqpUsu.Value
        If Me.optOrgRed1.Value Then
            g_oUsuario.CNRestrOrganizarRedUsu = True
            g_oUsuario.CNRestrOrganizarRedDep = False
            g_oUsuario.CNRestrOrganizarRedUO = False
        ElseIf Me.optOrgRed2.Value Then
            g_oUsuario.CNRestrOrganizarRedDep = True
            g_oUsuario.CNRestrOrganizarRedUsu = False
            g_oUsuario.CNRestrOrganizarRedUO = False
        ElseIf Me.optOrgRed3.Value Then
            g_oUsuario.CNRestrOrganizarRedUO = True
            g_oUsuario.CNRestrOrganizarRedUsu = False
            g_oUsuario.CNRestrOrganizarRedDep = False
        ElseIf Me.optOrgRed4.Value Then
            g_oUsuario.CNRestrOrganizarRedUO = False
            g_oUsuario.CNRestrOrganizarRedUsu = False
            g_oUsuario.CNRestrOrganizarRedDep = False
        End If
        
        teserror.NumError = TESnoerror
        teserror = g_oUsuario.ModificarRestriccionesFSCN()
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            Exit Sub
        End If
            
    End If
    
    ModoConsulta sstabUsu.Tab, SSTabCN.Tab
        
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdAceptarDest_Click()
    Dim teserror As TipoErrorSummit
    Dim vbm As Variant
    Dim i As Integer
    Dim oDen As CMultiidiomas
    Dim oDestinos As CDestinos
       
    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    With sdbgDestinos
        'Destinos
        Set oDen = oFSGSRaiz.Generar_CMultiidiomas
        Set oDestinos = oFSGSRaiz.Generar_CDestinos
        .MoveFirst
        For i = 0 To .Rows - 1
            vbm = .AddItemBookmark(i)
            If .Columns("EMISION").CellValue(vbm) Or .Columns("RECEPCION").CellValue(vbm) Then
                oDestinos.Add CStr(.Columns("COD").CellValue(vbm)), oDen, , , , , , , , , , , , , , .Columns("EMISION").CellValue(vbm), .Columns("RECEPCION").CellValue(vbm)
            End If
        Next i
    End With
        
    Set g_oUsuario.DestinosParaPedidos = oDestinos
    teserror = g_oUsuario.AnyadirDestinosParaPedidos()
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        Exit Sub
    End If
    
    'Destino por defecto
    If sdbcDestCod.Value = "" Then
        Set g_oUsuario.Destino = Nothing
    Else
        Set g_oUsuario.Destino = oDestinos.Item(CStr(sdbcDestCod.Columns(0).Value))
    End If
    
    Set oDestinos = Nothing
    
    teserror.NumError = TESnoerror
    teserror = g_oUsuario.ModificarAccesoFSDest
    
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            Exit Sub
        End If
    
    ModoConsulta (sstabUsu.Tab)
    
    Screen.MousePointer = vbNormal
    Exit Sub
Error:
    Screen.MousePointer = vbNormal
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmUSUARIOS", "cmdAceptarDest_Click", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub

''' <summary>
''' Guarda las unidades de negocio asociadas al usuario seleccionadas en el arbol
''' </summary>
''' <remarks>Llamada desde Boton Aceptar; Tiempo m�ximo < 1seg </remarks>

Private Sub AceptarFSQAUnidadesNeg()
    Dim teserror As TipoErrorSummit
    
    teserror.NumError = TESnoerror
    
    Screen.MousePointer = vbHourglass
    
    LockWindowUpdate Me.hWnd
          
    teserror = m_oPermisosUndNegUsuarioAgregar.AniadirUnidades(g_oUsuario.Cod)
            
    If teserror.NumError <> TESnoerror Then
        LockWindowUpdate 0&
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        
        Set m_oPermisosUndNegUsuarioAgregar = Nothing
        Set m_oPermisosUndNegUsuarioEliminar = Nothing
        Exit Sub
    End If
    Set m_oPermisosUndNegUsuarioAgregar = Nothing
    
    teserror = m_oPermisosUndNegUsuarioEliminar.EliminarUnidades(g_oUsuario.Cod)
            
    If teserror.NumError <> TESnoerror Then
        LockWindowUpdate 0&
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        
        Set m_oPermisosUndNegUsuarioAgregar = Nothing
        Set m_oPermisosUndNegUsuarioEliminar = Nothing
        Exit Sub
    End If
    Set m_oPermisosUndNegUsuarioEliminar = Nothing
      
    LockWindowUpdate 0&
    Screen.MousePointer = vbNormal
    
    m_oPermisosUndNegUsuarioIniciales.PermisosUndNegUsuIniciales g_oUsuario.Cod
End Sub

Private Sub cmdAceptarFSIM_Click()
    Dim teserror As TipoErrorSummit
    Screen.MousePointer = vbHourglass
    
    With g_oUsuario
        .FSIMAltaFacturas = chkFSIM(0).Value
        .FSIMSeguimientoFacturas = chkFSIM(1).Value
        .FSIMVisorAutofacturas = chkFSIM(2).Value
        .FSIMNotificacionesIM = chkFSIM(3).Value
    End With
    
    teserror.NumError = TESnoerror
    teserror = g_oUsuario.ModificarPermisosFSIM
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        Exit Sub
    End If

    ModoConsulta (sstabUsu.Tab)
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdAceptarFSIS_Click()
    Dim teserror As TipoErrorSummit
    
    g_oUsuario.INTPermisoRelanzarIntegracion = Me.chkRelanzar.Value
    
    teserror = g_oUsuario.modificarPermisosFSIS()
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        Exit Sub
    End If

    ModoConsulta (sstabUsu.Tab)
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdAceptarFSSOL_Click()
    Dim teserror As TipoErrorSummit
    Dim bSustituir As Boolean
    Dim oSolicitud As CSolicitud
    Dim bElimPresupFav As Boolean
    
    If chkSOL(18).Value = vbChecked Then
        If Trim(txtSustituto.Text) = "" Then
            oMensajes.SustitucionTempActivada
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    Set oSolicitud = oFSGSRaiz.Generar_CSolicitud
    'Si se activa la restricci�n de que solo se puedan seleccionar destinos de su unidad organizativa, eliminar de las solicitudes favoritas los destinos que no pertenezcan a la unidad organizativa.
    If g_oUsuario.FSWSRestrDestUO <> CBool(chkSOL(5).Value) Then
        If chkSOL(5).Value = vbChecked Then
            oSolicitud.EliminarSolicitudesFavoritasDestinos_NO_UON (g_oUsuario.Cod)
        End If
    End If
    'Si se activa la restricci�n de que solo se puedan seleccionar Personas de la unidad organizativa del usuario, eliminar de las solicitudes favoritas las personas que no pertenezcan a la unidad organizativa.
    If g_oUsuario.FSWSTrasladosUO <> CBool(chkSOL(1).Value) Then
        If chkSOL(1).Value = vbChecked Then
            oSolicitud.EliminarSolicitudesFavoritasPersonas_NO_UON_DEP g_oUsuario.Cod, g_oUsuario.Persona.UON1, g_oUsuario.Persona.UON2, g_oUsuario.Persona.UON3
        End If
    End If
    'Si se activa la restricci�n de que solo se puedan seleccionar Personas del departamento del usuario, eliminar de las solicitudes favoritas las personas que no pertenezcan a ese departamento.
    If g_oUsuario.FSWSTrasladosDEP <> CBool(chkSOL(2).Value) Then
        If chkSOL(2).Value = vbChecked Then
            oSolicitud.EliminarSolicitudesFavoritasPersonas_NO_UON_DEP g_oUsuario.Cod, g_oUsuario.Persona.UON1, g_oUsuario.Persona.UON2, g_oUsuario.Persona.UON3, g_oUsuario.Persona.CodDep
        End If
    End If
    'Si se activa la restriccion de "Restringir los proveedores al material del usuario"
    If g_oUsuario.FSWSRestrProvMat <> CBool(chkSOL(3).Value) Then
        If chkSOL(3).Value = vbChecked Then
            oSolicitud.EliminarSolicitudesFavoritasProveedores g_oUsuario.Cod
        End If
    End If
    'Si se activa la restriccion de "Restringir el material al asignado al usuario"
    If g_oUsuario.FSWSRestrMatUsu <> CBool(chkSOL(4).Value) Then
        If chkSOL(4).Value = vbChecked Then
            oSolicitud.EliminarSolicitudesFavoritasMaterial g_oUsuario.Cod
        End If
    End If
    'Si se activa la restriccion de las unidades organizativas
    If g_oUsuario.FSWSRestrRefAbiertasUO <> CBool(chkSOL(6).Value) Then
        If chkSOL(6).Value = vbChecked Then
            oSolicitud.EliminarSolicitudesFavoritasUONs g_oUsuario.Cod
        End If
    End If

    'Si se activa la restriccion de "Restringir los conceptos presupuestarios a la unidad organizativa del usuario"
    If g_oUsuario.FSWSPresUO <> CBool(chkSOL(17).Value) Then
        If chkSOL(17).Value = vbChecked Then
            oSolicitud.EliminarSolicitudesFavoritasPRES g_oUsuario.Cod
            bElimPresupFav = True
        End If
    End If
    
    Set oSolicitud = Nothing
    
    g_oUsuario.FSWSPresUO = chkSOL(17).Value
    g_oUsuario.FSPMTraslados = chkSOL(0).Value
    g_oUsuario.FSWSTrasladosUO = chkSOL(1).Value
    g_oUsuario.FSWSTrasladosDEP = chkSOL(2).Value
    g_oUsuario.FSWSRestrProvMat = chkSOL(3).Value
    g_oUsuario.FSWSRestrMatUsu = chkSOL(4).Value
    g_oUsuario.FSWSRestrDestUO = chkSOL(5).Value
    g_oUsuario.FSWSRestrRefAbiertasUO = chkSOL(6).Value
    g_oUsuario.FSWSVisualizarUltADJ = chkSOL(7).Value
    g_oUsuario.FSPMRestringirEmpresasUSU = chkSOL(8).Value
    g_oUsuario.FSPMCrearEscenarios = chkSOL(9).Value
    g_oUsuario.FSPMCrearEscenariosParaOtros = chkSOL(10).Value
    g_oUsuario.FSPMPermitirCrearEscenariosProveedores = chkSOL(19).Value
    g_oUsuario.FSPMEditarEscenariosCompartidos = chkSOL(11)
    g_oUsuario.FSPMOcultarEscenarioDefecto = chkSOL(12)
    'RESTRICCIONES ESCENARIOS
    g_oUsuario.FSPMRestringirCrearEscenariosUsuariosUONUsu = chkSOL(13)
    g_oUsuario.FSPMRestringirCrearEscenariosUsuariosDEPUsu = chkSOL(14)
    g_oUsuario.FSPMRestringirEdicionEscenariosUONUsu = chkSOL(15)
    g_oUsuario.FSPMRestringirEdicionEscenariosDEPUsu = chkSOL(16)
    
    If NullToStr(g_oUsuario.FSWSSustitucion) <> txtSustituto.Tag Then
        bSustituir = True
    End If
    If txtSustituto.Text = "" Then
        g_oUsuario.FSWSSustitucion = Null
    Else
        g_oUsuario.FSWSSustitucion = txtSustituto.Tag
    End If
    
    If lblPres(0).caption = "" Then
        g_oUsuario.FSWSPres1 = Null
    Else
        g_oUsuario.FSWSPres1 = lblPres(0).Tag
    End If
    
    If lblPres(1).caption = "" Then
        g_oUsuario.FSWSPres2 = Null
    Else
        g_oUsuario.FSWSPres2 = lblPres(1).Tag
    End If
    
    If lblPres(2).caption = "" Then
        g_oUsuario.FSWSPres3 = Null
    Else
        g_oUsuario.FSWSPres3 = lblPres(2).Tag
    End If
    
    If lblPres(3).caption = "" Then
        g_oUsuario.FSWSPres4 = Null
    Else
        g_oUsuario.FSWSPres4 = lblPres(3).Tag
    End If
    teserror.NumError = TESnoerror
    teserror = g_oUsuario.ModificarAccesoFSWS(bSustituir)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        Exit Sub
    End If
    'Si se activa la restriccion de "Restringir los conceptos presupuestarios a la unidad organizativa del usuario"
    If bElimPresupFav Then g_oUsuario.EliminarPresupuestosFavoritosRestrUO
    
    ModoConsulta (sstabUsu.Tab)
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdCancelarCN_Click(Index As Integer)
    ModoConsulta sstabUsu.Tab, SSTabCN.Tab
    
    If g_oUsuario Is Nothing Then Exit Sub
    
    CargarFSCN

End Sub

Private Sub cmdCancelarDest_Click()

    ModoConsulta (sstabUsu.Tab)

    If g_oUsuario Is Nothing Then Exit Sub
    
    g_oUsuario.ExpandirUsuario
    CargarFSDirectos
    
End Sub

Private Sub cmdCancelarFSIM_Click()
    ModoConsulta (sstabUsu.Tab)
    If g_oUsuario Is Nothing Then Exit Sub
    g_oUsuario.ExpandirUsuario
    CargarFSIM
End Sub

Private Sub cmdCancelarFSIS_Click()
    If g_oUsuario Is Nothing Then Exit Sub
    g_oUsuario.ExpandirUsuario
    cargarFSIS
    
    ModoConsulta (sstabUsu.Tab)
End Sub

''' <summary>
''' Ocurre cuando se hace click en el control
''' </summary>
''' <remarks>Se utiliza para cargar datos relacionados con la gesti�n SM. Tiempo m�ximo > 1 seg.</remarks>
Private Sub cmdCancelarFSSM_Click()
    If g_oUsuario Is Nothing Then Exit Sub
    chkNotifDisponible.Value = BooleanToSQLBinary(m_bNotifDisponibleAnt)
    g_oUsuario.ExpandirUsuario
    m_sModoArbolIPA = "C"
    m_sModoTabEstrOrg = "C"
    CargarFSSM_ControlImportes
    CargarFSSM_PermisosActivos
    
    ModoConsulta (sstabUsu.Tab)
End Sub

Private Sub cmdCancelarFSSOL_Click()
    ModoConsulta (sstabUsu.Tab)
    If g_oUsuario Is Nothing Then Exit Sub
    g_oUsuario.ExpandirUsuario
    CargarFSSOL
End Sub

Private Sub cmdCM_Click(Index As Integer)
    Dim teserror As TipoErrorSummit
    
    Select Case Index
        Case 0  'Modificar
            ModoEdicion (sstabUsu.Tab)
        
        Case 1  'Aceptar
            Screen.MousePointer = vbHourglass
                                    
            g_oUsuario.FSCMPermitirVisorNotificaciones = chkCM.Value
            
            teserror.NumError = TESnoerror
            teserror = g_oUsuario.ModificarAccesoFSCM
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Exit Sub
            End If
            
            ModoConsulta (sstabUsu.Tab)
            Screen.MousePointer = vbNormal
            
        Case 2  'Cancelar
            ModoConsulta (sstabUsu.Tab)
            If g_oUsuario Is Nothing Then Exit Sub
            g_oUsuario.ExpandirUsuario
            CargarFSCM
    End Select
End Sub

Private Sub cmdEP_Click(Index As Integer)
Dim teserror As TipoErrorSummit

Select Case Index
    Case 0  'Modificar
        ModoEdicion (sstabUsu.Tab)
        
    Case 1  'Aceptar
        Screen.MousePointer = vbHourglass
         'Pedido libre
        If chkFSEP(0).Visible = False Then
            g_oUsuario.PedidoLibre = False
        Else
            g_oUsuario.PedidoLibre = chkFSEP(0).Value
        End If
        'Modificar precios en EP
        If chkFSEP(11).Visible = False Then
            g_oUsuario.ModificarPrecios = False
        Else
            g_oUsuario.ModificarPrecios = chkFSEP(11).Value
        End If
        'Pedido a otras empresas
        If chkFSEP(12).Visible = False Then
            g_oUsuario.OtrasEmp = False
        Else
            g_oUsuario.OtrasEmp = (chkFSEP(12).Value = vbChecked)
        End If
        'Permitir anular Recepciones en EP
        If chkFSEP(18).Visible = False Then
            g_oUsuario.AnularRecepciones = False
        Else
            g_oUsuario.AnularRecepciones = chkFSEP(18).Value
        End If
        'Pedidos  de los CC de otros usuarios
        If chkFSEP(14).Visible = False Then
            g_oUsuario.VerPedidosCCImputables = False
        Else
            g_oUsuario.VerPedidosCCImputables = chkFSEP(14).Value
        End If
        'Pedidos  de los CC de otros usuarios
        If chkFSEP(17).Visible = False Then
            g_oUsuario.RecepcionarPedidosCCImputables = False
        Else
            g_oUsuario.RecepcionarPedidosCCImputables = chkFSEP(17).Value
        End If
        'Permitir activar el bloqueo de albaranes para facturaci�n
        If chkFSEP(19).Visible = False Then
            g_oUsuario.BloqueoAlbaranesFacturacion = False
        Else
            g_oUsuario.BloqueoAlbaranesFacturacion = chkFSEP(19).Value
        End If
        'Permitir activar la emisi�n de articulos a la uon del usuario.
        If chkFSEP(13).Visible = False Then
            g_oUsuario.RestringirPedidiosArtUONUsuario = False
        Else
            g_oUsuario.RestringirPedidiosArtUONUsuario = chkFSEP(13).Value
        End If
        'Permiso cerrar Pedidos
        g_oUsuario.PermisoCerrarPedidos = chkFSEP(15).Value
        'Permiso Reabrir Pedidos
        g_oUsuario.PermisoReabrirPedidos = chkFSEP(16).Value
        
        g_oUsuario.EPPermitirAnularPedidos = chkFSEP(21).Value
        g_oUsuario.EPPermitirVerPedidosBorradosSeguimiento = chkFSEP(22).Value
        g_oUsuario.EPPermitirBorrarLineasPedido = chkFSEP(23).Value
        g_oUsuario.EPPermitirDeshacerBorradoLineasPedido = chkFSEP(24).Value
        g_oUsuario.EPPermitirBorrarPedidos = chkFSEP(25).Value
        g_oUsuario.EPPermitirDeshacerBorradoPedidos = chkFSEP(26).Value
        g_oUsuario.EPPermitirModificarPorcentajeDesvio = chkFSEP(27).Value
        
        'Permitir emitir pedidos desde Pedido Abierto
        If chkFSEP(1).Visible = False Then
            g_oUsuario.EmitirPedAbierto = False
        Else
            g_oUsuario.EmitirPedAbierto = chkFSEP(1).Value
        End If
        'Restringir la emisi�n de pedidos contra Pedido Abierto a los Pedidos Abiertos de la Empresa del usuario
        If chkFSEP(2).Visible = False Then
            g_oUsuario.RestrEmiContraPedAbiertoEmpUsu = False
        Else
            g_oUsuario.RestrEmiContraPedAbiertoEmpUsu = chkFSEP(2).Value
        End If
        'Permitir seleccionar presupuestos por concepto 1 de las unidades organizativas situadas por encima del usuario
        If chkFSEP(3).Visible = False Then
            g_oUsuario.PermitSelPres1UOEncimaUsu = False
        Else
            g_oUsuario.PermitSelPres1UOEncimaUsu = chkFSEP(3).Value
        End If
        'Permitir seleccionar presupuestos por concepto 2 de las unidades organizativas situadas por encima del usuario
        If chkFSEP(5).Visible = False Then
            g_oUsuario.PermitSelPres2UOEncimaUsu = False
        Else
            g_oUsuario.PermitSelPres2UOEncimaUsu = chkFSEP(5).Value
        End If
        'Permitir seleccionar presupuestos por concepto 3 de las unidades organizativas situadas por encima del usuario
        If chkFSEP(6).Visible = False Then
            g_oUsuario.PermitSelPres3UOEncimaUsu = False
        Else
            g_oUsuario.PermitSelPres3UOEncimaUsu = chkFSEP(6).Value
        End If
        'Permitir seleccionar presupuestos por concepto 4 de las unidades organizativas situadas por encima del usuario
        If chkFSEP(7).Visible = False Then
            g_oUsuario.PermitSelPres4UOEncimaUsu = False
        Else
            g_oUsuario.PermitSelPres4UOEncimaUsu = chkFSEP(7).Value
        End If
        'Restringir la selecci�n de presupuestos por concepto 1 a la unidad organizativa del usuario
        If chkFSEP(4).Visible = False Then
            g_oUsuario.RestrSelPres1UOUsu = False
        Else
            g_oUsuario.RestrSelPres1UOUsu = chkFSEP(4).Value
        End If
         'Restringir la selecci�n de presupuestos por concepto 2 a la unidad organizativa del usuario
        If chkFSEP(8).Visible = False Then
            g_oUsuario.RestrSelPres2UOUsu = False
        Else
            g_oUsuario.RestrSelPres2UOUsu = chkFSEP(8).Value
        End If
        'Restringir la selecci�n de presupuestos por concepto 3 a la unidad organizativa del usuario
        If chkFSEP(9).Visible = False Then
            g_oUsuario.RestrSelPres3UOUsu = False
        Else
            g_oUsuario.RestrSelPres3UOUsu = chkFSEP(9).Value
        End If
        'Restringir la selecci�n de presupuestos por concepto 4 a la unidad organizativa del usuario
        If chkFSEP(10).Visible = False Then
            g_oUsuario.RestrSelPres4UOUsu = False
        Else
            g_oUsuario.RestrSelPres4UOUsu = chkFSEP(10).Value
        End If
        If Not chkFSEP(20).Visible Then
            g_oUsuario.RestrSelCentroAprovUsu = False
        Else
            g_oUsuario.RestrSelCentroAprovUsu = chkFSEP(20).Value
        End If
        teserror.NumError = TESnoerror
        teserror = g_oUsuario.ModificarAccesoFSEP
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            Exit Sub
        End If
        ModoConsulta (sstabUsu.Tab)
        Screen.MousePointer = vbNormal

    Case 2  'Cancelar
        ModoConsulta (sstabUsu.Tab)
        If g_oUsuario Is Nothing Then Exit Sub
        g_oUsuario.ExpandirUsuario

End Select
End Sub

Private Sub cmdGS_Click(Index As Integer)
Dim sCadena As String
Dim iAccion As Integer
Dim nodx As node
Dim oAccion As CAccion
Dim teserror As TipoErrorSummit

Select Case Index
    Case 0  'Modificar
        If Not g_oUsuario.ExpandirUsuario Then Exit Sub
        ModoEdicion (sstabUsu.Tab)
        If g_oUsuario.Acciones Is Nothing Then
            If g_oUsuario.Perfil Is Nothing Then
                Set m_oAccionesIniciales = oFSGSRaiz.Generar_CAcciones
            Else
                If g_oUsuario.Perfil.Acciones Is Nothing Then
                    Set m_oAccionesIniciales = oFSGSRaiz.Generar_CAcciones
                Else
                    Set m_oAccionesIniciales = g_oUsuario.Perfil.Acciones
                End If
            End If
        Else
            Set m_oAccionesIniciales = g_oUsuario.Acciones
            Set nodx = tvwsegur.selectedItem
            sCadena = Mid(nodx.Tag, 4, 7)
            iAccion = Int(val(sCadena))
            cmdGS(5).Visible = False
        End If
        Set m_oAccionesAAnyadir = oFSGSRaiz.Generar_CAcciones
        Set m_oAccionesAEliminar = oFSGSRaiz.Generar_CAcciones
        On Error Resume Next
        tvwSegur_NodeClick tvwsegur.selectedItem
        On Error GoTo 0
        
    Case 1  'Restaurar
        'Al poner la variable a false la proxima vez que accedamos alas acciones del usuario, este volvera a cargarlas de la BD.
        Screen.MousePointer = vbHourglass
        If tvwUsu.selectedItem Is Nothing Then
            lstSegurIdAcciones.clear
            Set m_oAccionesIniciales = Nothing
            Set m_oAccionesIniciales = g_oUsuario.Acciones
        Else
            lstSegurIdAcciones.clear
            lstvwSegur.ListItems.clear
            lstvwSegurReadOnly.ListItems.clear
            tvwUsu_NodeClick tvwUsu.selectedItem
            tvwSegur_NodeClick tvwsegur.selectedItem
        End If
        If g_oUsuario.Perfil Is Nothing Then
            If g_oUsuario.Acciones Is Nothing Then
                g_oUsuario.CargarAcciones basPublic.gParametrosInstalacion.gIdioma
            End If
            Set m_oAccionesIniciales = g_oUsuario.Acciones
        Else
            g_oUsuario.Perfil.CargarAcciones basPublic.gParametrosInstalacion.gIdioma
            Set m_oAccionesIniciales = Nothing
            Set m_oAccionesIniciales = g_oUsuario.Perfil.Acciones
        End If
        Screen.MousePointer = vbNormal
        
    Case 2  'Listado
        Set nodx = tvwUsu.selectedItem
            'NodoSeleccionado nodx
        If Not nodx Is Nothing Then
            If nodx.Tag = "Raiz" Then
                Exit Sub
            Else
                frmLstUSUARIOS.opUsuario = True
                frmLstUSUARIOS.sdbcUsuCod.Text = DevolverCod(nodx)
                frmLstUSUARIOS.chkAcc.Value = Checked
           End If
           Set nodx = Nothing
        End If
        DoEvents
        frmLstUSUARIOS.Show vbModal
    Case 3  'Aceptar
        DoEvents
        lstvwSegur_LostFocus
        Screen.MousePointer = vbHourglass
        For Each oAccion In m_oAccionesAEliminar
            m_oAccionesIniciales.Remove CStr(oAccion.Id)
        Next
        For Each oAccion In m_oAccionesAAnyadir
            m_oAccionesIniciales.Add oAccion.Id, oAccion.Den
        Next
        If Not m_oAccionesIniciales Is Nothing Then
            If Not ValidarSeguimiento Then
                oMensajes.MensajeOKOnly 1112, Exclamation
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            If Not ValidarRecepcion Then
                oMensajes.MensajeOKOnly 1113, Exclamation
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
        teserror = g_oUsuario.AsignarAcciones(m_oAccionesIniciales)
        If Not g_oUsuario.Presups5Niv0 Is Nothing Then
            teserror = g_oUsuario.AsignarPresups5Niv0(g_oUsuario.Presups5Niv0)
        End If
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            cmdGS_Click 4
            Exit Sub
        End If
        '******* Registrar accion ***************
        If gParametrosGenerales.gbACTIVLOG Then
            oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, AccionesSummit.ACCUsuModSeg, ""
        End If
        '*****************************************
        'vuelve a cargar la colecci�n con los cambios,para que se muestren ordenados
        g_oUsuario.CargarAcciones basPublic.gParametrosInstalacion.gIdioma
        Set m_oAccionesIniciales = g_oUsuario.Acciones
        Set m_oAccionesAAnyadir = Nothing
        Set m_oAccionesAEliminar = Nothing
        ModoConsulta (sstabUsu.Tab)
        Accion = accusucon
        tvwSegur_NodeClick tvwsegur.selectedItem
        Screen.MousePointer = vbNormal
        
    Case 4  'Cancelar
        DoEvents
        ModoConsulta (sstabUsu.Tab)
        g_oUsuario.ExpandirUsuario
        If g_oUsuario.Acciones Is Nothing Then
            If g_oUsuario.Perfil Is Nothing Then
                Set m_oAccionesIniciales = oFSGSRaiz.Generar_CAcciones
            Else
                If g_oUsuario.Perfil.Acciones Is Nothing Then
                    Set m_oAccionesIniciales = oFSGSRaiz.Generar_CAcciones
                Else
                    Set m_oAccionesIniciales = g_oUsuario.Perfil.Acciones
                End If
            End If
        Else
            Set m_oAccionesIniciales = g_oUsuario.Acciones
        End If
        Set g_oUsuario.Presups5Niv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
        g_oUsuario.Presups5Niv0.CargarPresupuestosConceptos5 , , , , , , , , g_oUsuario.Cod
        tvwSegur_NodeClick tvwsegur.selectedItem
        Set m_oAccionesAAnyadir = Nothing
        Set m_oAccionesAEliminar = Nothing
     Case 5 'Arboles
        If g_oUsuario Is Nothing Then Exit Sub
        MostrarFormPermisosCC oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, g_oUsuario.Cod, (Accion <> ACCUsuModSeg), g_oUsuario.Presups5Niv0, MDI.ScaleWidth, MDI.ScaleHeight

End Select
End Sub

Private Sub cmdModifDest_Click()
    picEditDest.Visible = True
    picNavigateDest.Visible = False
    ModoEdicion (sstabUsu.Tab)
End Sub

Private Sub cmdModificarCN_Click(Index As Integer)
    ModoEdicion sstabUsu.Tab, SSTabCN.Tab
End Sub

Private Sub cmdModificarFSIM_Click()
    ModoEdicion (sstabUsu.Tab)
End Sub

Private Sub cmdModificarFSIS_Click()
    ModoEdicion (sstabUsu.Tab)
    cmdAceptarFSIS.Visible = True
    cmdCancelarFSIS.Visible = True
End Sub

Private Sub cmdModificarFSSM_Click()
    m_bNotifDisponibleAnt = SQLBinaryToBoolean(chkNotifDisponible.Value)
    ModoEdicion (sstabUsu.Tab)
End Sub

Private Sub cmdModificarFSSOL_Click()
    ModoEdicion (sstabUsu.Tab)
End Sub

Public Sub cmdUsu_Click(Index As Integer)
Dim nodx As node
Dim teserror As TipoErrorSummit
Dim dfechahoracrypt As Date
Dim bAccesoGs As Boolean
Dim irespuesta As Integer
Dim nombre As String
Dim iEstadoBaja As Integer
Dim i As Integer
Dim sAux As String
Select Case Index
    Case 0  'A�adir*****************************************************************************
        ''' <summary>Evento que se genera al clicar la opcion de anyadir nuevo usuario</summary>
        Accion = ACCUsuAnya
        Set nodx = tvwUsu.selectedItem
        Set g_oUsuario = Nothing
        frmUSUPERSWinSec.caption = m_sIdiA�aUsu
        frmUSUPERSWinSec.Show vbModal
        RefrescarTvw
        Set nodx = Nothing
    Case 1  'Modificar**************************************************************************
        ''' Evento que se genera cuando se clica la opcion del menu emergente de modificar usuario, cargando los datos del mismo y abriendo el formulario
        DoEvents
        Set nodx = tvwUsu.selectedItem
        If Not nodx Is Nothing Then
            Accion = ACCUsuMod
            Set g_oUsuario = Nothing
            Set g_oUsuario = oFSGSRaiz.generar_cusuario
            g_oUsuario.Cod = DevolverCod(nodx)
            Set g_oIBaseDatos = g_oUsuario
            teserror = g_oIBaseDatos.IniciarEdicion
            If teserror.NumError = TESnoerror Then
                dfechahoracrypt = g_oUsuario.fecpwd
                If Not (g_oUsuario.Perfil Is Nothing) Then
                    bAccesoGs = g_oUsuario.Perfil.FSGS
                Else
                    bAccesoGs = g_oUsuario.AccesoFSGS
                End If
                If basParametros.gParametrosGenerales.giWinSecurity = UsuariosLocales And bAccesoGs Then
                    teserror = g_oUsuario.RetrieveUserProperties
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Accion = accusucon
                        Set nodx = Nothing
                        Exit Sub
                    End If
                End If
                frmUSUPERSWinSec.caption = m_sIdiModifUsu
                frmUSUPERSWinSec.Show vbModal
            Else
                Set g_oUsuario = Nothing
                Set g_oIBaseDatos = Nothing
            End If
        End If
        RefrescarTvw
        Set nodx = Nothing
    Case 2  'Eliminar**************************************************************************
        If Not g_oUsuario Is Nothing Then
            Set g_oIBaseDatos = g_oUsuario
            g_oUsuario.ExpandirUsuario
            nombre = g_oUsuario.Persona.Apellidos & ", " & g_oUsuario.Persona.nombre
            'Si hay admin pub comprobamos que no tenga sobres cerrados
            If gParametrosGenerales.gbAdminPublica Then
                If g_oUsuario.ExistenSobresCerradosDeUsuario Then
                    oMensajes.ImposibleEliminarUsuarioConSobres
                    Exit Sub
                End If
            End If
            irespuesta = MsgBox(m_sIdiEliminar & " " & g_oUsuario.Cod & " (" & nombre & ")" & vbCrLf & m_sIdiContinuar, vbQuestion + vbYesNo, "FullStep")
            If irespuesta = vbYes Then
                Screen.MousePointer = vbHourglass
                teserror = g_oIBaseDatos.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
                Accion = ACCusuEli
                RefrescarTvw
                If Me.Visible Then tvwUsu.SetFocus
                Screen.MousePointer = vbNormal
            End If
        End If
    
    Case 3  'Codig0******************************************************************************************************
        ''' * Objetivo: Cambiar de codigo el usuario
        ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
        If g_oUsuario Is Nothing Then Exit Sub
        Set g_oIBaseDatos = g_oUsuario
        teserror = g_oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
        g_oIBaseDatos.CancelarEdicion
        ''' Activacion del formulario de cambio de codigo
        ''' para conocer el nuevo codigo
        frmMODCOD.caption = m_sIdiUsuarioCod
        frmMODCOD.Left = frmUSUARIOS.Left + 500
        frmMODCOD.Top = frmUSUARIOS.Top + 1000
        
        frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodUSU
        frmMODCOD.txtCodAct.Text = g_oUsuario.Cod
        Set frmMODCOD.fOrigen = frmUSUARIOS
        g_bCodigoCancelar = False
        frmMODCOD.Show 1
        DoEvents
        If g_bCodigoCancelar = True Then Exit Sub
        
        ''' Comprobar validez del codigo
        If g_sCodigoNuevo = "" Then
            oMensajes.NoValido m_sIdiCodigo
            Set g_oIBaseDatos = Nothing
            Exit Sub
        End If
            
        If UCase(g_sCodigoNuevo) = UCase(g_oUsuario.Cod) Then
            oMensajes.NoValido m_sIdiCodigo
            Set g_oIBaseDatos = Nothing
            Exit Sub
        End If
             
        irespuesta = oMensajes.PreguntarCambiarCodigoUsuario()
        If irespuesta = vbNo Then Exit Sub
        
        For i = 1 To Len(g_sCodigoNuevo)
            If InStr(1, sAuxMatrixCodigoValido, Mid(g_sCodigoNuevo, i, 1)) = 0 Then
                oMensajes.CodUsuarioNoValido (Mid(g_sCodigoNuevo, i, 1))
                Set g_oIBaseDatos = Nothing
                Exit Sub
            End If
        Next
                 
        sAux = g_sCodigoNuevo & Format(g_oUsuario.fecpwd, "DD\/MM\/YYYY HH\:NN\:SS")
        For i = 1 To Len(sAux)
            If InStr(1, sAuxMatrix, Mid(sAux, i, 1)) = 0 Then
                oMensajes.CodUsuarioNoValido (Mid(sAux, i, 1))
                Set g_oIBaseDatos = Nothing
                Exit Sub
            End If
        Next
        
        ''' Cambiar el codigo
        iEstadoBaja = g_oUsuario.ComprobarUsuarioBaja(g_oUsuario.Persona.Cod, g_sCodigoNuevo)
        If iEstadoBaja <> 0 Then
            oMensajes.CodigoUsarioBaja
            Screen.MousePointer = vbNormal
            Set g_oIBaseDatos = Nothing
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        g_oUsuario.Usuario = basOptimizacion.gvarCodUsuario
        teserror = g_oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        Screen.MousePointer = vbNormal
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
        Accion = ACCUsuMod
        RefrescarTvw
        Set g_oIBaseDatos = Nothing
        
    Case 4  'Buscar*************************************************************************************
        frmUSUBuscar.g_sOrigen = "frmUSUARIOS"
        frmUSUBuscar.Show 1
        
    Case 5  'Restaurar usuario**************************************************************************
        Screen.MousePointer = vbHourglass
        tvwUsu.Nodes.clear
        Me.caption = m_sIdiUsuarios
        Set m_oUsuarios = oGestorSeguridad.DevolverTodosLosUsuarios(, , , True)
        GenerarEstructuraUsuarios
        Set m_oUsuarios = Nothing
        cmdUsu(0).Enabled = True
        cmdUsu(1).Enabled = False
        cmdUsu(2).Enabled = False
        cmdUsu(3).Enabled = False
        If Me.Visible Then tvwUsu.SetFocus
        Screen.MousePointer = vbNormal
    
    Case 6  'Listado Usuario*******************************************************************************
         ''' * Objetivo: Obtener un listado de usuarios
        Set nodx = tvwUsu.selectedItem
        If Not nodx Is Nothing Then
            If nodx.Tag = "Raiz" Then
                frmLstUSUARIOS.opTodos = True
            Else
                frmLstUSUARIOS.opUsuario.Value = True
                frmLstUSUARIOS.sdbcUsuCod.Value = DevolverCod(nodx)
            End If
            Set nodx = Nothing
        End If
        frmLstUSUARIOS.Show vbModal
End Select

End Sub

Private Sub cmdWS_Click(Index As Integer)
    Dim teserror As TipoErrorSummit
    
    Select Case Index
        Case 0  'Modificar
            ModoEdicion (sstabUsu.Tab)
        
        Case 1  'Aceptar
            Screen.MousePointer = vbHourglass
                                    
            g_oUsuario.FSPMPermitirVisorNotificaciones = chkPM.Value
            
            teserror.NumError = TESnoerror
            teserror = g_oUsuario.ModificarAccesoFSPM
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Exit Sub
            End If
            
            ModoConsulta (sstabUsu.Tab)
            Screen.MousePointer = vbNormal
            
        Case 2  'Cancelar
            ModoConsulta (sstabUsu.Tab)
            If g_oUsuario Is Nothing Then Exit Sub
            g_oUsuario.ExpandirUsuario
            CargarFSWS
    End Select
End Sub

Private Sub Form_Activate()
    On Error GoTo Error
    
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
    
    If Not m_bActivado Then m_bActivado = True

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUARIOS", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub lstPermisosUndNegUsu_ItemCheck(Item As Integer)
    Dim Vinculado As Boolean
    Dim i As Integer
    Vinculado = False
    'Seg�n las opciones checkeadas y hasta que acepte los cambios haremos un mantenimiento de 3 variables globales.
    '1)Las opciones checkeadas inicialmente al cargarse la p�gina
    '2)Las opciones que se eliminan de las opciones iniciales
    '3)Las opciones que se agregan de las opciones iniciales
    'Tener en cuenta las siguientes restricciones:
    'Si Emitir no conf.=>Consultar no conf.
    'Si Modificar no conf.=>Consultar y Emitir no conf.
    'Si Reabrir no conf.=>Consultar no conf.
    'Si Mantener obj. y suelos=>Consultar obj. y suelos
    'Si Mod. punt. panel calidad=>Consultar punt. panel calidad
    'EN BASE DE DATOS NO SE GUARDA PERMISO DE CONSULTA SI TIENE OTRO DE MAYOR GRADO(EMITIR, MODIFICAR,REABRIR)
    If Not m_oRecargaPermisosUndNegUsuario Then
        Select Case Item
            Case 0 'Emitir
                If lstPermisosUndNegUsu.Selected(0) Then
                    lstPermisosUndNegUsu.Selected(1) = True
                    'Se ha checkeado Emitir no conf. Para asegurarnos, primero lo eliminamos de ELIMINAR por si esta.
                    'Luego, si no esta en las iniciales y tampoco en AGREGAR, la agregamos a AGREGAR
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1)
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2)
                                                   
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 1
                        End If
                    End If
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 2
                        End If
                    End If
                Else
                    'Se ha descheckeado Emitir no conf. Lo agregamos a ELIMINAR. Aunque no este en INICIALES, es por asegurarnos.
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 1
                    End If
                    'La quitamos de AGREGAR
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1)
                    End If
                    
                    'Si no tiene permiso de Emitir tampoco de Modificar, ni Reabrir NC de otros usuarios
                    lstPermisosUndNegUsu.Selected(2) = False
                    If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9) Is Nothing Then
                        If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9) Is Nothing Then
                            m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 9
                        End If
                    Else
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9)
                    End If
                    
                    lstPermisosUndNegUsu.Selected(3) = False
                    If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3) Is Nothing Then
                        If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3) Is Nothing Then
                            m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 3
                        End If
                    Else
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3)
                    End If
                    
                    'Si descheckeamos Emitir, descheckeamos Reabrir y Modificar pero dejamos checkeado Consultar
                    'quitamos Consultar de ELIMINAR.
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2)
                    'Como tras desseleccionar se queda checkeado Consultar, lo agregamos a AGREGAR si no esta en INICIALES
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 2
                        End If
                    End If
                End If
                
            Case 1
                If lstPermisosUndNegUsu.Selected(1) Then
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2)
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                        m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 2
                    End If
                Else
                    If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                        If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                            m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 2
                        End If
                    Else
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2)
                    End If
                
                    If lstPermisosUndNegUsu.Selected(0) Then
                        If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                            If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                                m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 1
                            End If
                        Else
                            m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1)
                        End If
                    End If
                
                    If lstPermisosUndNegUsu.Selected(2) Then
                        If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9) Is Nothing Then
                            If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9) Is Nothing Then
                                m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 9
                            End If
                        Else
                            m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9)
                        End If
                    End If
                    
                    If lstPermisosUndNegUsu.Selected(3) Then
                        If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3) Is Nothing Then
                            If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3) Is Nothing Then
                                m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 3
                            End If
                        Else
                            m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3)
                        End If
                    End If
                    
                    lstPermisosUndNegUsu.Selected(0) = False
                    lstPermisosUndNegUsu.Selected(2) = False
                    lstPermisosUndNegUsu.Selected(3) = False
                End If
                
            Case 2
               If lstPermisosUndNegUsu.Selected(2) Then
                    lstPermisosUndNegUsu.Selected(0) = True
                    lstPermisosUndNegUsu.Selected(1) = True
                    
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9)
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1)
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2)
                                                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 9
                        End If
                    End If
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 1
                        End If
                    End If
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 2
                        End If
                    End If
                    
                Else
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 9
                    End If
                        
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9) Is Nothing Then
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 9)
                    End If
                    
                    If lstPermisosUndNegUsu.Selected(3) = False Then
                        If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                            If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                                m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 1
                            End If
                        Else
                            If Not m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                                m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1)
                            End If
                        End If
                    End If
                End If
                
            Case 3
               If lstPermisosUndNegUsu.Selected(3) Then
                    lstPermisosUndNegUsu.Selected(0) = True
                    lstPermisosUndNegUsu.Selected(1) = True
                    
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3)
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1)
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2)
                                                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 3
                        End If
                    End If
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 1
                        End If
                    End If
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 2) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 2
                        End If
                    End If
                Else
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 3
                    End If
                        
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3) Is Nothing Then
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 3)
                    End If
                    
                    If lstPermisosUndNegUsu.Selected(2) = False Then
                        If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                            If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                                m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 1
                            End If
                        Else
                            If Not m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1) Is Nothing Then
                                m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 1)
                            End If
                        End If
                    End If
                End If
            
            Case 4
                If lstPermisosUndNegUsu.Selected(4) Then
                    lstPermisosUndNegUsu.Selected(5) = True
                    
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 4)
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5)
                                                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 4) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 4) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 4
                        End If
                    End If
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 5
                        End If
                    End If
                Else
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 4) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 4
                    End If
                        
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 4) Is Nothing Then
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 4)
                    End If
                              
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5)
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 5
                        End If
                    End If
                End If
            
            Case 5
                If lstPermisosUndNegUsu.Selected(5) Then
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5)
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 5
                        End If
                    End If
                Else
                    lstPermisosUndNegUsu.Selected(4) = False
                    
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 4) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 4
                    End If
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 4) Is Nothing Then
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 4)
                    End If
                    
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 5
                    End If
                    
                    m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 5)
                End If
            
            Case 6
                If lstPermisosUndNegUsu.Selected(6) Then
                    If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 6) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 6)
                    Else
                        m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 6
                    End If
                Else
                    If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 6) Is Nothing Then
                        If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 6) Is Nothing Then
                            m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 6
                        End If
                    Else
                         m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 6)
                    End If
                End If
                
            Case 7
                If lstPermisosUndNegUsu.Selected(7) Then
                    lstPermisosUndNegUsu.Selected(8) = True
                    
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 7)
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8)
                                                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 7) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 7) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 7
                        End If
                    End If
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 8
                        End If
                    End If
                Else
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 7) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 7
                    End If
                        
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 7) Is Nothing Then
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 7)
                    End If
                              
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8)
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 8
                        End If
                    End If
                End If
            
            Case 8
                If lstPermisosUndNegUsu.Selected(8) Then
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8)
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 8
                        End If
                    End If
                Else
                    lstPermisosUndNegUsu.Selected(7) = False
                    
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 7) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 7
                    End If
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 7) Is Nothing Then
                        m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 7)
                    End If
                    
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 8
                    End If
                    
                    m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 8)
                    
                End If
            Case 9  'Recibir aviso de expiraci�n de certificados de proveedores de la unidad de negocio
                If lstPermisosUndNegUsu.Selected(9) Then
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 10)
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 10) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 10) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 10
                        End If
                    End If
                Else
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 10) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 10
                    End If
                    
                    m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 10)
                End If
            Case 10 'Recibir aviso de conversi�n autom�tica de proveedor en real en la unidad de negocio
                If lstPermisosUndNegUsu.Selected(10) Then
                    m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 11)
                    
                    If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 11) Is Nothing Then
                        If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 11) Is Nothing Then
                            m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 11
                        End If
                    End If
                Else
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 11) Is Nothing Then
                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, 11
                    End If
                    
                    m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & 11)
                End If
            Case Else
                '12 - Recibir avisos de Escalaciones de Proveedores de Nivel 1
                '13 - Consultar Escalaciones de Proveedores de Nivel 1 (*)
                '14 - Aprobar/Rechazar Escalaciones de Proveedores de Nivel 1
                '15 - Recibir avisos de Escalaciones de Proveedores de Nivel 2
                '16 - Consultar Escalaciones de Proveedores de Nivel 2 (*)
                '17 - Aprobar/Rechazar Escalaciones de Proveedores de Nivel 2
                '18 - Recibir avisos de Escalaciones de Proveedores de Nivel 3
                '19 - Consultar Escalaciones de Proveedores de Nivel 3 (*)
                '20 - Aprobar/Rechazar Escalaciones de Proveedores de Nivel 3
                '21 - Recibir avisos de Escalaciones de Proveedores de Nivel 4
                '22 - Consultar Escalaciones de Proveedores de Nivel 4 (*)
                '23 - Aprobar/Rechazar Escalaciones de Proveedores de Nivel 4
                For i = 0 To UBound(m_sPermisosEscalacion)
                    If lstPermisosUndNegUsu.Text = m_sPermisosEscalacion(i) Then
                        If lstPermisosUndNegUsu.Selected(Item) Then
                            'En caso de checkear recibir o aprobar/rechazar, se marcar� autom�ticamente el check de consulta
                            Dim j As Integer
                            j = 0
                            Select Case i
                                Case 0, 3, 6, 9
                                    j = 1
                                Case 2, 5, 8, 11
                                    j = -1
                            End Select
                            If j <> 0 Then
                                m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12 + j))
                                If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12 + j)) Is Nothing Then
                                    If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12 + j)) Is Nothing Then
                                        m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, (i + 12 + j)
                                    End If
                                End If
                                lstPermisosUndNegUsu.Selected(lstPermisosUndNegUsu.ListIndex + j) = True
                            End If
                            m_oPermisosUndNegUsuarioEliminar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12))
                            If m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12)) Is Nothing Then
                                If m_oPermisosUndNegUsuarioAgregar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12)) Is Nothing Then
                                    m_oPermisosUndNegUsuarioAgregar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, (i + 12)
                                End If
                            End If
                        Else
                            j = 0
                            Select Case i
                                Case 1, 4, 7, 10
                                    j = 1
                            End Select
                            If j <> 0 Then
                                If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12 + j)) Is Nothing Then
                                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12 + j)) Is Nothing Then
                                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, (i + 12 + j)
                                    End If
                                Else
                                    m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12 + j))
                                End If
                                lstPermisosUndNegUsu.Selected(lstPermisosUndNegUsu.ListIndex + j) = False
                                If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12 - j)) Is Nothing Then
                                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12 - j)) Is Nothing Then
                                        m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, (i + 12 - j)
                                    End If
                                Else
                                    m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12 - j))
                                End If
                                lstPermisosUndNegUsu.Selected(lstPermisosUndNegUsu.ListIndex - j) = False
                            End If
                            If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12)) Is Nothing Then
                                If m_oPermisosUndNegUsuarioEliminar.Item(Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12)) Is Nothing Then
                                    m_oPermisosUndNegUsuarioEliminar.Add Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)), 0, 0, "", "", False, (i + 12)
                                End If
                            Else
                                m_oPermisosUndNegUsuarioAgregar.Remove (Mid(tvwUndNegUsuRead.selectedItem.key, 4, Len(tvwUndNegUsuRead.selectedItem.key)) & "#" & (i + 12))
                            End If
                        End If
                    End If
                Next i
        End Select
    End If
End Sub

Private Sub sdbgDestinos_BeforeRowColChange(Cancel As Integer)
    If Not picEditDest.Visible Then Cancel = True
End Sub

Private Sub sdbgDestinos_Change()
    Dim vbm As Variant
    Dim bValor As Boolean
    Dim bEnEmision As Boolean

    With sdbgDestinos
        If (.Columns(.col).Name <> "EMISION") And (.Columns(.col).Name <> "RECEPCION") Then Exit Sub
        If .Columns("GENERICO").Value And .Columns(.col).Name = "EMISION" Then
            .Columns("EMISION").Value = False
            Exit Sub
        End If
        bValor = .Columns(.col).Value
        bEnEmision = (.Columns(.col).Name = "EMISION")
        If Not m_colBookmarks Is Nothing Then
            If m_colBookmarks.Count > 0 Then
                LockWindowUpdate Me.hWnd
                
                For Each vbm In m_colBookmarks
                    .Bookmark = vbm
                    
                    If bEnEmision Then
                        .Columns("EMISION").Value = bValor
                    Else
                        .Columns("RECEPCION").Value = bValor
                    End If
                Next
                
                LockWindowUpdate 0&
                Set m_colBookmarks = Nothing
                
                .SelBookmarks.RemoveAll
            End If
        End If
        
        'Si hemos desmarcado la check, y era el destino por defecto, entonces lo vaciamos.
        If Not .Columns("EMISION").Value Then
            If sdbcDestCod.Value = .Columns("COD").Value Then
                sdbcDestCod.Value = ""
            End If
        End If
        
        .Update
    End With
End Sub

''' <summary>Guarda los datos de QA del usuario</summary>
''' <param name="Index">Le indica la pesta�a dentro de QA en la que estamos</param>
''' <remarks>Llamada desde Boton Aceptar de cualquiera de las pesta�as de QA; Tiempo m�ximo < 1 seg</remarks>
Private Sub cmdAceptarFSQA_Click(Index As Integer)
    Dim teserror As TipoErrorSummit
    
    Screen.MousePointer = vbHourglass
    
    With g_oUsuario
        If Index = 0 Then
            .QARestCertifDep = chkQA(5).Value
            .QARestCertifUO = chkQA(4).Value
            .QARestCertifUsu = chkQA(3).Value
            .QARestCertifUnidNegocio = chkQA(36).Value
            .QARestNoConformDep = chkQA(8).Value
            .QARestNoConformUO = chkQA(7).Value
            .QARestNoConformUsu = chkQA(6).Value
            .QARestProvDepCalidad = chkQA(0).Value
            .QARestProvEquipo = chkQA(2).Value
            .QARestProvMaterial = chkQA(1).Value
            .QArestselecRevUO = Me.chkQA(9).Value
            .QARestselecRevDepart = Me.chkQA(10).Value
            .QARestrNotifUO = Me.chkQA(11).Value
            .QARestrNotifDEP = Me.chkQA(12).Value
                        
            teserror.NumError = TESnoerror
            teserror = .ModificarRestriccionesFSQA()
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Exit Sub
            End If
        Else
            If Index = 2 Then
                AceptarFSQAUnidadesNeg
            Else
                .QAMantenimientoMat = chkQA(21).Value
                .QAMantenimientoProv = chkQA(22).Value
                .QAModifProvCalidad = chkQA(13).Value
                .QAModifNotifCambiosMatGS = chkQA(15).Value
                .QAModifNotifCambiosCalif = chkQA(17).Value
                .QACertifModifExpirar = chkQA(26)
                .QACertifPubOtrosUsu = chkQA(27).Value
                .QACertifModifOtrosUsu = chkQA(28).Value
                .QARevCierreNoConfor = chkQA(34).Value
                .QAMantUnidadesNegocio = chkQA(23).Value
                .QACierreNoConf = Me.chkQA(32).Value
                .QAReabrirNoConf = Me.chkQA(33).Value
                .QAContactoCertAutom = Me.chkQA(14).Value
                .QAPermitirSolicitarCertificados = Me.chkQA(29).Value
                .QAPermitirRenovarCertificados = Me.chkQA(30).Value
                .QAPermitirEnviarCertificados = Me.chkQA(31).Value
                .QAMantenimientoObjetivosySuelos = chkQA(24).Value
                .QAMantenimientoProveEsc = chkQA(25).Value
                .QAPeriodoValidezCertificado = Me.chkQA(16).Value
                .QAModifNotifProximidadNC = Me.chkQA(18).Value
                .QAModificacionNivelesDefectoUNQA = Me.chkQA(19).Value
                .QAPermitirModificarFiltroMaterial = Me.chkQA(20).Value
                .QAPermitirVisorNotificaciones = Me.chkQA(35).Value
                teserror.NumError = TESnoerror
                teserror = .ModificarPermisosFSQA()
                
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
            End If
        End If
    End With
    
    ModoConsulta sstabUsu.Tab, ssTabQA.Tab
        
    Screen.MousePointer = vbNormal
    
End Sub




Private Sub cmdBuscarPres_Click(Index As Integer)
    
    Select Case Index
        Case 0
            frmSELPresAnuUON.sOrigen = "frmUsuarios"
            frmSELPresAnuUON.g_iTipoPres = 1
            frmSELPresAnuUON.Show vbModal
            
        Case 1
            frmSELPresAnuUON.sOrigen = "frmUsuarios"
            frmSELPresAnuUON.g_iTipoPres = 2
            frmSELPresAnuUON.Show vbModal
            
        Case 2
            frmSELPresUO.sOrigen = "frmUsuarios"
            frmSELPresUO.g_iTipoPres = 3
            frmSELPresUO.Show vbModal
            
        Case 3
            frmSELPresUO.sOrigen = "frmUsuarios"
            frmSELPresUO.g_iTipoPres = 4
            frmSELPresUO.Show vbModal
    End Select
    
End Sub

Private Sub cmdBuscarSust_Click()
    frmSOLSelPersona.g_sOrigen = "frmUsuarios"
    frmSOLSelPersona.Show vbModal
End Sub


Private Sub cmdCancelarFSQA_Click(Index As Integer)
    
    ModoConsulta sstabUsu.Tab, ssTabQA.Tab
    
    If g_oUsuario Is Nothing Then Exit Sub
    
    g_oUsuario.ExpandirUsuario
    CargarFSQA

    Set m_oPermisosUndNegUsuarioAgregar = Nothing
    Set m_oPermisosUndNegUsuarioEliminar = Nothing
End Sub

Private Sub cmdLimpiarPres_Click(Index As Integer)
    
    lblPres(Index).caption = ""
    
End Sub

Private Sub cmdLimpiarSust_Click()
    txtSustituto.Text = ""
    txtSustituto.Tag = ""
End Sub


Private Sub cmdModificarFSQA_Click(Index As Integer)
    ModoEdicion sstabUsu.Tab, ssTabQA.Tab
End Sub







''' Revisado por: blp. Fecha: 07/03/2012
''' <summary>
''' Carga del formulario de usuarios
''' </summary>
''' <remarks>Llamada desde evento Load del formulario. M�x. 0,3 seg.</remarks>
Private Sub Form_Load()
    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    m_sModoArbolIPA = "C"
    m_sModoTabEstrOrg = "C"
    m_sEstructuraUnidadesNegocio = "C"
       
    Me.Width = 10890    'incluyendo ahora FSSM
    Me.Height = 8100
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
   
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    CargarRecursos
    PonerFieldSeparator Me
    
    Screen.MousePointer = vbHourglass
    InterDep = oGestorSeguridad.DevolverInterDepAcciones(gParametrosGenerales.gbPymes)
    Screen.MousePointer = vbNormal
        
    Accion = accusucon
    m_sCaption = caption
    
    With sstabUsu
        .TabVisible(1) = False
        .TabVisible(2) = False
        .TabVisible(3) = False
        .TabVisible(4) = False
        .TabVisible(5) = False
        .TabVisible(6) = False
        .TabVisible(7) = False
        .TabVisible(8) = False
        .TabVisible(9) = False
        .TabVisible(10) = False
        .TabVisible(11) = False
        .TabVisible(12) = False
    End With
    
    picNavigateSegur.Enabled = False
    picEdit.Enabled = False
    picNavigateUsu.Enabled = True
    cmdGS(2).Enabled = False
    cmdGS(0).Enabled = False
    cmdGS(1).Enabled = False
    cmdUsu(0).Enabled = True
    cmdUsu(3).Enabled = False
    cmdUsu(1).Enabled = False
    cmdUsu(2).Enabled = False
    
    fraAcciones.Visible = False
        
    Set m_oUsuarios = oGestorSeguridad.DevolverTodosLosUsuarios(, , , True)
    GenerarEstructuraUsuarios
    Set m_oUsuarios = Nothing
        
    If FSEPConf Then
        GenerarEstructuraAccionesFSEP tvwsegur
    Else
        GenerarEstructuraAcciones tvwsegur
    End If
    Set m_oPlantillaDeAccionesDeSeguridad = oGestorSeguridad.DevolverAccionesDeSeguridad(basPublic.gParametrosInstalacion.gIdioma, True)
    
    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
        
    If Not basParametros.gParametrosGenerales.gbPedidoLibre Then
        chkFSEP(0).Visible = False
        chkFSEP(27).Top = chkFSEP(26).Top
        chkFSEP(26).Top = chkFSEP(25).Top
        chkFSEP(25).Top = chkFSEP(24).Top
        chkFSEP(24).Top = chkFSEP(23).Top
        chkFSEP(23).Top = chkFSEP(22).Top
        chkFSEP(22).Top = chkFSEP(21).Top
        chkFSEP(21).Top = chkFSEP(20).Top
        chkFSEP(20).Top = chkFSEP(19).Top
        chkFSEP(13).Top = chkFSEP(19).Top
        chkFSEP(19).Top = chkFSEP(17).Top
        chkFSEP(17).Top = chkFSEP(14).Top
        chkFSEP(14).Top = chkFSEP(16).Top
        chkFSEP(16).Top = chkFSEP(15).Top
        chkFSEP(15).Top = chkFSEP(18).Top
        chkFSEP(18).Top = chkFSEP(11).Top
        chkFSEP(11).Top = chkFSEP(12).Top
        chkFSEP(12).Top = chkFSEP(0).Top
    End If
    If Not basParametros.gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS Then
        chkFSEP(22).Visible = False
        chkFSEP(23).Visible = False
        chkFSEP(24).Visible = False
        chkFSEP(25).Visible = False
        chkFSEP(26).Visible = False
        chkFSEP(27).Top = chkFSEP(22).Top
    End If
    lblDefecto.Visible = (basParametros.gParametrosGenerales.gbUsarPres1 Or basParametros.gParametrosGenerales.gbUsarPres2 Or _
                          basParametros.gParametrosGenerales.gbUsarPres3 Or basParametros.gParametrosGenerales.gbUsarPres4)

    If gParametrosGenerales.gbActPedAbierto Then
        chkFSEP(1).Visible = True
        chkFSEP(2).Top = chkFSEP(1).Top + chkFSEP(1).Height + 105
    Else
        chkFSEP(1).Visible = False
        chkFSEP(2).Top = chkFSEP(1).Top
    End If
    picPres(0).Top = chkFSEP(2).Top + chkFSEP(2).Height + 55
    picPres(1).Top = picPres(0).Top + picPres(0).Height + 55
    picPres(2).Top = picPres(1).Top + picPres(1).Height + 55
    chkFSEP(27).Visible = True

    
    If Not basParametros.gParametrosGenerales.gbUsarPres1 Then
        lblPres(0).Visible = False
        lblLitPres1.Visible = False
        cmdLimpiarPres(0).Visible = False
        cmdBuscarPres(0).Visible = False
        
        lblSustituir.Top = chkSOL(18).Top
        txtSustituto.Top = chkSOL(18).Top
        cmdLimpiarSust.Top = chkSOL(18).Top
        cmdBuscarSust.Top = chkSOL(18).Top
        chkSOL(18).Top = lblPres(3).Top
        
        lblPres(3).Top = lblPres(2).Top
        lblLitPres4.Top = lblLitPres3.Top
        cmdLimpiarPres(3).Top = cmdLimpiarPres(2).Top
        cmdBuscarPres(3).Top = cmdBuscarPres(2).Top
        
        lblPres(2).Top = lblPres(1).Top
        lblLitPres3.Top = lblLitPres2.Top
        cmdLimpiarPres(2).Top = cmdLimpiarPres(1).Top
        cmdBuscarPres(2).Top = cmdBuscarPres(1).Top
        
        lblPres(1).Top = lblPres(0).Top
        lblLitPres2.Top = lblLitPres1.Top
        cmdLimpiarPres(1).Top = cmdLimpiarPres(0).Top
        cmdBuscarPres(1).Top = cmdBuscarPres(0).Top
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres2 Then
        lblPres(1).Visible = False
        lblLitPres2.Visible = False
        cmdLimpiarPres(1).Visible = False
        cmdBuscarPres(1).Visible = False
        
        lblSustituir.Top = chkSOL(18).Top
        txtSustituto.Top = chkSOL(18).Top
        cmdLimpiarSust.Top = chkSOL(18).Top
        cmdBuscarSust.Top = chkSOL(18).Top
        chkSOL(18).Top = lblPres(3).Top
        
        lblPres(3).Top = lblPres(2).Top
        lblLitPres4.Top = lblLitPres3.Top
        cmdLimpiarPres(3).Top = cmdLimpiarPres(2).Top
        cmdBuscarPres(3).Top = cmdBuscarPres(2).Top
        
        lblPres(2).Top = lblPres(1).Top
        lblLitPres3.Top = lblLitPres2.Top
        cmdLimpiarPres(2).Top = cmdLimpiarPres(1).Top
        cmdBuscarPres(2).Top = cmdBuscarPres(1).Top
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres3 Then
        lblPres(2).Visible = False
        lblLitPres3.Visible = False
        cmdLimpiarPres(2).Visible = False
        cmdBuscarPres(2).Visible = False
        
        lblSustituir.Top = chkSOL(18).Top
        txtSustituto.Top = chkSOL(18).Top
        cmdLimpiarSust.Top = chkSOL(18).Top
        cmdBuscarSust.Top = chkSOL(18).Top
        chkSOL(18).Top = lblPres(3).Top
        
        lblPres(3).Top = lblPres(2).Top
        lblLitPres4.Top = lblLitPres3.Top
        cmdLimpiarPres(3).Top = cmdLimpiarPres(2).Top
        cmdBuscarPres(3).Top = cmdBuscarPres(2).Top
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres4 Then
        lblPres(3).Visible = False
        lblLitPres4.Visible = False
        cmdLimpiarPres(3).Visible = False
        cmdBuscarPres(3).Visible = False
        
        lblSustituir.Top = chkSOL(18).Top
        txtSustituto.Top = chkSOL(18).Top
        cmdLimpiarSust.Top = chkSOL(18).Top
        cmdBuscarSust.Top = chkSOL(18).Top
        chkSOL(18).Top = lblPres(3).Top
    End If

    If Not basParametros.gParametrosGenerales.gbAccesoQACertificados Then
        fraQA(1).Visible = False
        fraQA(3).Top = fraQA(2).Top
        fraQA(2).Top = fraQA(1).Top
        fraQA(7).Visible = False
        fraQA(8).Top = fraQA(7).Top
    End If
    If Not basParametros.gParametrosGenerales.gbAccesoQANoConformidad Then
        fraQA(2).Visible = False
        fraQA(3).Visible = False
        fraQA(8).Visible = False
    Else
        If basParametros.gParametrosGenerales.gbFSQARevisor Then
            Me.fraQA(3).Visible = basParametros.gParametrosGenerales.gbAccesoQANoConformidad
        Else
            Me.fraQA(3).Visible = False
        End If
    End If
    
    'Check "Permitir ver los pedidos de los centros de coste del usuario
    chkFSEP(14).Visible = (gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM)
    chkFSEP(17).Visible = (gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM)
    If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.SinAcceso Then
        chkFSEP(19).Top = chkFSEP(14).Top
    End If
    'Permitir activar el bloqueo de albaranes para facturaci�n
    chkFSEP(19).Visible = (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM)
    chkFSEP(20).Visible = gParametrosGenerales.gbUsarOrgCompras
    
    Me.ssTabQA.TabVisible(2) = True
    
    If Not gParametrosGenerales.gbOblProveEqp Then chkQA(2).Visible = False

    Set cP = New cPopupMenu
    cP.hWndOwner = Me.hWnd
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmUSUARIOS", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' Revisado por: blp. FEcha: 17/07/2012
''' <summary>
''' Evento que se lanza al modificar en pantalla el tama�o del formulario y redimensiona los controles
''' </summary>
''' <remarks>Llamada desde el evento. Max. 0,1 seg.</remarks>
Private Sub Form_Resize()
    Dim i As Integer
    
    On Error Resume Next
    
    If Me.Height < 3000 Then Exit Sub
    If Me.Width < 3000 Then Exit Sub
    
    sstabUsu.Height = Me.Height - 500
    sstabUsu.Width = Me.Width - 200
    
    tvwUsu.Height = sstabUsu.Height - 990
    tvwUsu.Width = sstabUsu.Width - 220
    
    picNavigateUsu.Top = sstabUsu.Height - 500
    picNavigateSegur.Top = picNavigateUsu.Top + 50
    picEdit.Top = picNavigateSegur.Top
    
    tvwsegur.Height = sstabUsu.Height - 1000
    tvwsegur.Width = (35 * (sstabUsu.Width - 300)) / 100
    fraAcciones.Left = tvwsegur.Width + 210
    
    fraAcciones.Width = (65 * (sstabUsu.Width - 300)) / 100
    fraAcciones.Height = tvwsegur.Height + 100
    
    lstvwSegur.Width = fraAcciones.Width - 315
    lstvwSegur.ColumnHeaders(1).Width = Screen.Width
    lstvwSegurReadOnly.Width = fraAcciones.Width - 315
    lstvwSegurReadOnly.ColumnHeaders(1).Width = Screen.Width
    
    lstvwSegur.Height = fraAcciones.Height - 395
    lstvwSegurReadOnly.Height = fraAcciones.Height - 395
            
    picNavigateSegur.Width = sstabUsu.Width - 100
    picNavigateUsu.Width = picNavigateSegur.Width
    picEdit.Width = picNavigateSegur.Width
    cmdGS(3).Top = 30
    cmdGS(4).Top = 30
    cmdGS(3).Left = (Me.Width / 2) - (cmdGS(3).Width / 2) - cmdGS(4).Width + 300
    cmdGS(4).Left = cmdGS(3).Left + cmdGS(3).Width + 100
    
    fraFSWS.Height = sstabUsu.Height - 920
    fraFSWS.Width = sstabUsu.Width - 330
    
    sstabSM.Height = tvwUsu.Height
    sstabSM.Width = tvwUsu.Width
    
    fraArboles.Height = sstabSM.Height - 920
    fraArboles.Width = sstabSM.Width - 330
    
    fraActivos.Height = fraArboles.Height
    fraActivos.Width = fraArboles.Width

    fraFSEP.Height = fraFSWS.Height
    fraFSEP.Width = fraFSWS.Width
    
    fraDestinos.Height = fraFSWS.Height
    fraDestinos.Width = fraFSWS.Width
        
    'EP alto
    picFSEP.Top = (fraFSEP.Height - picFSEP.Height) - 120
    PicDestPorDefecto.Top = fraDestinos.Height - PicDestPorDefecto.Height - 120
    PicDestinos.Height = PicDestPorDefecto.Top - 480
    picFSEP.Top = fraDestinos.Top
    sdbgDestinos.Height = PicDestinos.Height
    'EP ancho
    picFSEP.Width = fraFSEP.Width - 480
    PicDestinos.Width = picFSEP.Width + 240
    sdbgDestinos.Width = PicDestinos.Width
    sdbgDestinos.Columns("COD").Width = 1000
    sdbgDestinos.Columns("DEN").Width = 2340 * 1.925
    sdbgDestinos.Columns("DIR").Width = 2475 * 1.925
    sdbgDestinos.Columns("POB").Width = 1814 * 1.925
    sdbgDestinos.Columns("CP").Width = 1814 * 1.925
    
    picEditFSWS.Top = fraFSWS.Top + fraFSWS.Height + 20
    picEditFSSM.Top = sstabSM.Top + sstabSM.Height + 20
    picEditFSEP.Top = picEditFSWS.Top
    picEditDest.Top = picEditFSWS.Top
    picNavigateFSWS.Top = picEditFSWS.Top
    picNavigateFSSM.Top = picEditFSSM.Top
    picNavigateFSEP.Top = picEditFSWS.Top
    picNavigateDest.Top = picEditFSWS.Top
    picNavigateFSIS.Top = picEditFSWS.Top
    
    picEditFSWS.Left = (sstabUsu.Width / 3) + 120
    picEditFSSM.Left = picEditFSWS.Left
    picEditFSEP.Left = picEditFSWS.Left
    picEditFSQA(0).Left = picEditFSWS.Left
    picEditFSQA(1).Left = picEditFSWS.Left
    picEditFSCN(0).Left = picEditFSWS.Left
    picEditFSCN(1).Left = picEditFSWS.Left
    picEditDest.Left = picEditFSWS.Left
    
    ssTabQA.Width = sstabUsu.Width - 395
    ssTabQA.Height = sstabUsu.Height - 780
    sdbgVariables.Width = ssTabQA.Width - 300
    sdbgVariables.Height = ssTabQA.Height - 600
    sdbgVariables.Columns("CONS").Width = 1450
    sdbgVariables.Columns("MOD").Width = 1450
    If sdbgVariables.Columns("MOD").Visible = True Then
        sdbgVariables.Columns("DEN").Width = sdbgVariables.Width - 3500
    Else
        sdbgVariables.Columns("DEN").Width = sdbgVariables.Width - 2050
    End If
    
    tvwUndNegUsuRead.Width = (ssTabQA.Width * 0.5) - 300
    tvwUndNegUsuRead.Height = ssTabQA.Height - 1200
    
     With fraPermisosUndNegUsuRead
        .Left = tvwUndNegUsuRead.Left + tvwUndNegUsuRead.Width + 300
        .Top = tvwUndNegUsuRead.Top - 120
        .Width = (ssTabQA.Width * 0.5) - 600
        .Height = tvwUndNegUsuRead.Height + 120
    End With
    
    With lstPermisosUndNegUsu
        .Left = 120
        .Top = 300
        .Width = fraPermisosUndNegUsuRead.Width - 300
        .Height = fraPermisosUndNegUsuRead.Height - 420
    End With
        
    With lstPermisosUndNegUsuRead
        .Left = 120
        .Top = 300
        .Width = fraPermisosUndNegUsuRead.Width - 300
        .Height = fraPermisosUndNegUsuRead.Height - 420
    End With
    
    cmdModificarFSQA(2).Top = tvwUndNegUsuRead.Top + tvwUndNegUsuRead.Height + 100
    cmdAceptarFSQA(2).Top = tvwUndNegUsuRead.Top + tvwUndNegUsuRead.Height + 100
    cmdCancelarFSQA(2).Top = tvwUndNegUsuRead.Top + tvwUndNegUsuRead.Height + 100
    cmdAceptarFSQA(2).Left = (ssTabQA.Width) / 2 - 300
    cmdCancelarFSQA(2).Left = cmdAceptarFSQA(2).Left + cmdAceptarFSQA(2).Width + 200
    
    sstabEstrorg.Width = fraArboles.Width - 300
    sstabEstrorg.Height = fraArboles.Height - 600
    
    FrameFSSMTab0.Width = sstabEstrorg.Width - 160
    FrameFSSMTab0.Height = sstabEstrorg.Height - 520
        
    tvwestrorg_Con.Width = FrameFSSMTab0.Width
    tvwestrorg_Con.Height = FrameFSSMTab0.Height
    
    tvwestrorg_Modif.Width = FrameFSSMTab0.Width
    tvwestrorg_Modif.Height = FrameFSSMTab0.Height
    
    tvwestrorg_GesFac.Width = FrameFSSMTab0.Width
    tvwestrorg_GesFac.Height = FrameFSSMTab0.Height
    
    FrameFSSMTab1.Width = FrameFSSMTab0.Width
    FrameFSSMTab1.Height = FrameFSSMTab0.Height
    ssImportes.Width = FrameFSSMTab1.Width
    ssImportes.Height = FrameFSSMTab1.Height
    
    picFSQA(1).Width = ssTabQA.Width - 400
    For i = fraQA.LBound To fraQA.UBound
        fraQA(i).Width = ssTabQA.Width - 470
    Next i
        
    picEditFSQA(0).Top = ssTabQA.Height - 500
    picNavigateFSQA(0).Top = picEditFSQA(0).Top
    
    picFSQA(2).Width = ssTabQA.Width - 400

    picEditFSCN(1).Top = SSTabCN.Height - 500
    picNavigateFSCN(1).Top = picEditFSCN(1).Top
    
    picEditFSCN(0).Top = SSTabCN.Height - 500
    picNavigateFSCN(0).Top = picEditFSCN(0).Top

    wPerfil.Height = sstabUsu.Height - 800
    wPerfil.Width = sstabUsu.Width - 400
    
    fraFSIS.Width = fraFSWS.Width
    fraFSIS.Height = fraFSWS.Height
    picEditFSIS.Top = picEditFSEP.Top
    picEditFSIS.Left = fraFSIS.Width / 2 - 30
    
    fraFSIM.Width = fraFSWS.Width
    fraFSIM.Height = fraFSWS.Height
    picNavigateFSIM.Top = picEditFSEP.Top
    picEditFSIM.Top = picEditFSEP.Top
    picEditFSIM.Left = fraFSIM.Width / 2 - 30
    
    fraFSCM.Width = fraFSWS.Width
    fraFSCM.Height = fraFSWS.Height
    picNavigateFSCM.Top = picEditFSEP.Top
    picEditFSCM.Top = picEditFSEP.Top
    picEditFSCM.Left = fraFSIM.Width / 2 - 30
    
    fraFSSOL.Width = fraFSWS.Width
    fraFSSOL.Height = fraFSWS.Height
    picNavigateFSSOL.Top = picEditFSEP.Top
    picEditFSSOL.Top = picEditFSEP.Top
    picEditFSSOL.Left = fraFSSOL.Width / 2 - 30
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo Error
    
    If m_bDescargarFrm Then
         m_bDescargarFrm = False
         oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
    
    m_iIteracion = 0
    Set g_oUsuario = Nothing
    Set m_oUsuarios = Nothing
    Set g_oIBaseDatos = Nothing
    Set m_oPerfil = Nothing
    Set m_oPerfiles = Nothing
    Set m_oPerfilBaseSeleccionado = Nothing
    Set m_oPlantillaDeAccionesDeSeguridad = Nothing
    Set m_oPlantillaDeAccionesCargadas = Nothing
    Set m_oAccionesAAnyadir = Nothing
    Set m_oAccionesAEliminar = Nothing
    Set m_oAccionesIniciales = Nothing
    Set m_oDestinos = Nothing
    Set m_oPermisosUndNegUsuarioIniciales = Nothing
    Set m_oPermisosUndNegUsuarioAgregar = Nothing
    Set m_oPermisosUndNegUsuarioEliminar = Nothing
    Set m_colBookmarks = Nothing
    
    g_sCodigoNuevo = ""
    g_bCodigoCancelar = False
    Me.Visible = False
    
    Exit Sub
Error:
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmUSUARIOS", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub

Private Sub lstvwSegur_LostFocus()
Dim iIndice As Integer

If Accion = AccionesSummit.ACCUsuModSeg Then

    For iIndice = 1 To lstvwSegur.ListItems.Count
        
        If lstvwSegur.ListItems(iIndice).Checked Then
            If m_oAccionesIniciales.Item(Replace(lstvwSegur.ListItems(iIndice).key, "A", "")) Is Nothing Then
                m_oAccionesAAnyadir.Add Int(val(Replace(lstvwSegur.ListItems(iIndice).key, "A", ""))), lstvwSegur.ListItems(iIndice).Text
            End If
        
        Else
     
            If Not m_oAccionesIniciales.Item(Replace(lstvwSegur.ListItems(iIndice).key, "A", "")) Is Nothing Then
                m_oAccionesAEliminar.Add Int(val(Replace(lstvwSegur.ListItems(iIndice).key, "A", ""))), lstvwSegur.ListItems(iIndice).Text
            End If
        
        End If
        
    Next

End If
 

End Sub

Private Sub lstvwSegur_ItemCheck(ByVal Item As MSComctlLib.listItem)
    Dim blnEvitoInterdependencia_de_acciones As Boolean
    Dim bFound As Boolean
    Dim bRepeated As Boolean
    Dim bLimit As Boolean
    
    Dim iMaxLimit As Integer
    Dim iMinLimit As Integer
    Dim iIndex As Integer
    Dim iIndexAux As Integer
    
    Dim iStatusSearched As Integer
    
    If Item.Checked = True Then
        iStatusSearched = 1
        'aqui ha puesto el check
        If Replace(lstvwSegur.ListItems(Item.Index).key, "A", "") = CStr(AccionesDeSeguridad.PRESConcepto5CrearArboles) Then
            Dim valor
            Select Case lstvwSegur.ListItems.Count
            Case 1
                valor = lstvwSegur.Top + 50
            Case 2
                valor = lstvwSegur.Top + cmdGS(5).Height
            End Select
            cmdGS(5).Move (lstvwSegur.Width - cmdGS(5).Width), valor
            cmdGS(5).Visible = True
        End If
    Else
        iStatusSearched = 0
    End If
    
     If blnEvitoInterdependencia_de_acciones Then Exit Sub
     
    ''' Interdependencia de acciones
    
    bFound = False
    bRepeated = False
    
    ''' B�squeda de la acci�n en el array
    
    iMaxLimit = UBound(InterDep.AccionCausa)
    iMinLimit = 1
    
    While (Not bFound) And (Not bRepeated)
    
        ''' Miramos los l�mites, si no est� movemos el l�mite adecuado
        
        If InterDep.AccionCausa(iMinLimit) = Replace(lstvwSegur.ListItems(Item.Index).key, "A", "") Then
            iIndex = iMinLimit
            bFound = True
        Else
            If InterDep.AccionCausa(iMaxLimit) = Replace(lstvwSegur.ListItems(Item.Index).key, "A", "") Then
                iIndex = iMaxLimit
                bFound = True
            Else
                iIndex = (iMaxLimit + iMinLimit) \ 2
                If InterDep.AccionCausa(iIndex) = Replace(lstvwSegur.ListItems(Item.Index).key, "A", "") Then
                    bFound = True
                Else
                    If InterDep.AccionCausa(iIndex) < Replace(lstvwSegur.ListItems(Item.Index).key, "A", "") Then
                        iMinLimit = iIndex
                    Else
                        iMaxLimit = iIndex
                    End If
                End If
                If (iMaxLimit + iMinLimit) \ 2 = iIndex Then bRepeated = True
            End If
        End If
    Wend
    
    If Not bFound Then Exit Sub
    
    ''' B�squeda del comienzo de la acci�n
    
    bFound = True
    
    While bFound = True
            
        iIndex = iIndex - 1
        
        If iIndex > 0 And iIndex <= UBound(InterDep.AccionCausa) Then
            If InterDep.AccionCausa(iIndex) < Replace(lstvwSegur.ListItems(Item.Index).key, "A", "") Then
                bFound = False
            End If
        Else
            bFound = False
        End If

    Wend
    
    iIndex = iIndex + 1
    
    ''' B�squeda del estado correcto
    
    bFound = True
    
    If iStatusSearched = 0 Then
        If InterDep.EstadoCausa(iIndex) = 1 Then
            ''' Como est�n en orden ya sabemos que no est� con estado 0
            bFound = False
        End If
    Else
        If InterDep.EstadoCausa(iIndex) = 0 Then
        
            ''' Buscamos el estado 1, cuidando el tema del fin del array
            
            bFound = False
            bLimit = False
            
            While Not bFound And Not bLimit
            
                If iIndex < UBound(InterDep.EstadoCausa) Then
                    iIndex = iIndex + 1
                    If InterDep.AccionCausa(iIndex) = Replace(lstvwSegur.ListItems(Item.Index).key, "A", "") And InterDep.EstadoCausa(iIndex) = 1 Then
                        bFound = True
                    End If
                Else
                    bLimit = True
                End If
                    
            Wend
                
        End If
    End If
     
    If Not bFound Then Exit Sub
    
        
    ''' Poner las acciones en el estado correspondiente
    
    While InterDep.AccionCausa(iIndex) = Replace(lstvwSegur.ListItems(Item.Index).key, "A", "") And InterDep.EstadoCausa(iIndex) = iStatusSearched
    
        ''' B�squeda de la acci�n en la lista
        
        For iIndexAux = 1 To lstSegurIdAcciones.ListCount
                    
            If Replace(lstvwSegur.ListItems(iIndexAux).key, "A", "") = InterDep.AccionConsecuencia(iIndex) Then
                
                
                If InterDep.EstadoConsecuencia(iIndex) = 1 Then
                    lstvwSegur.ListItems(iIndexAux).Checked = True
                Else
                    lstvwSegur.ListItems(iIndexAux).Checked = False
                End If
                
                Exit For
                
            End If
            
        Next iIndexAux
            
        iIndex = iIndex + 1
        If iIndex > UBound(InterDep.AccionCausa) Then
            Exit Sub
        End If
    Wend
    

End Sub

Private Sub sdbcDestDen_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim ADORs As Ador.Recordset

    If sdbcDestCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el destino
    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(Trim(sdbcDestDen.Columns(1).Text), , True)

    If ADORs Is Nothing Then
        bExiste = False
    Else
        bExiste = Not (ADORs.RecordCount = 0)
    End If
    
    If bExiste Then bExiste = (UCase(ADORs(0).Value) = UCase(Trim(sdbcDestCod.Text)))
    
    If Not bExiste Then
        sdbcDestCod.Text = ""
        
    Else
        m_bRespetarComboDest = True
        sdbcDestDen.Text = ADORs(1).Value
        
        sdbcDestCod.Columns(0).Value = sdbcDestCod.Text
        sdbcDestCod.Columns(1).Value = sdbcDestDen.Text
        
        m_bRespetarComboDest = False
        
    End If

    If Not ADORs Is Nothing Then
        ADORs.Close
        Set ADORs = Nothing
    End If
    
    m_bCargarComboDesde = False
End Sub

Private Sub sdbgDestinos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bk As Variant
    
    With sdbgDestinos
        If .col = -1 Then
            Set m_colBookmarks = New Collection
            For Each bk In .SelBookmarks
                m_colBookmarks.Add bk
            Next
        Else
            If (.Columns(.col).Name = "EMISION") Or (.Columns(.col).Name = "RECEPCION") Then
                If .SelBookmarks.Count = 0 And Not m_colBookmarks Is Nothing Then
                    For Each bk In m_colBookmarks
                        .SelBookmarks.Add bk
                    Next
                End If
            Else
                Set m_colBookmarks = Nothing
            End If
        End If
    End With
End Sub

Private Sub sdbgDestinos_RowLoaded(ByVal Bookmark As Variant)
If sdbgDestinos.Columns("GENERICO").Value Then
    sdbgDestinos.Columns("EMISION").CellStyleSet "Gris"
End If
End Sub

Private Sub sdbgVariables_Change()
    Dim vb As Variant
    Dim lID1 As Long
    Dim lID2 As Long
    Dim lID3 As Long
    Dim lID4 As Long
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    
    If m_bRespetarChange = True Then Exit Sub
    m_bRespetarChange = True
    If sdbgVariables.Columns(sdbgVariables.col).Name = "CONS" Then
        teserror = g_oUsuario.ModificarPermisoVariable(sdbgVariables.Columns("ID").Value, sdbgVariables.Columns("NIVEL").Value, True, GridCheckToBoolean(sdbgVariables.ActiveCell.Value))
        If teserror.NumError = TESnoerror Then
            If sdbgVariables.ActiveCell.Value <> "0" Then
                Select Case sdbgVariables.Columns("NIVEL").Value
                    Case 1
                        MarcarVariablesHijas "IDNIVEL1", "CONS"
                    Case 2
                        MarcarVariablesHijas "IDNIVEL2", "CONS"
                    Case 3
                        MarcarVariablesHijas "IDNIVEL3", "CONS"
                    Case 4
                        MarcarVariablesHijas "IDNIVEL4", "CONS"
                End Select
            Else
                If g_oUsuario.AccesoFSQA And gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
                    Select Case sdbgVariables.Columns("NIVEL").Value
                    Case 2
                        vb = sdbgVariables.Bookmark
                        lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                        sdbgVariables.MovePrevious
                        While Not (sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID1 Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        sdbgVariables.Bookmark = vb
                    Case 3
                        vb = sdbgVariables.Bookmark
                        lID2 = sdbgVariables.Columns("IDNIVEL2").Value
                        lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                        sdbgVariables.MovePrevious
                        While Not (sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2" Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        While Not (sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1" Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        sdbgVariables.Bookmark = vb
                    Case 4
                        vb = sdbgVariables.Bookmark
                        lID3 = sdbgVariables.Columns("IDNIVEL3").Value
                        lID2 = sdbgVariables.Columns("IDNIVEL2").Value
                        lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                        sdbgVariables.MovePrevious
                        While Not (sdbgVariables.Columns("ID").Value = lID3 And sdbgVariables.Columns("NIVEL").Value = "3") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID3 And sdbgVariables.Columns("NIVEL").Value = "3" Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        While Not (sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2" Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        While Not (sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1" Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        sdbgVariables.Bookmark = vb
                    Case 5
                        vb = sdbgVariables.Bookmark
                        lID4 = sdbgVariables.Columns("IDNIVEL4").Value
                        lID3 = sdbgVariables.Columns("IDNIVEL3").Value
                        lID2 = sdbgVariables.Columns("IDNIVEL2").Value
                        lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                        sdbgVariables.MovePrevious
                        While Not (sdbgVariables.Columns("ID").Value = lID4 And sdbgVariables.Columns("NIVEL").Value = "4") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID4 And sdbgVariables.Columns("NIVEL").Value = "4" Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        While Not (sdbgVariables.Columns("ID").Value = lID3 And sdbgVariables.Columns("NIVEL").Value = "3") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID3 And sdbgVariables.Columns("NIVEL").Value = "3" Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        While Not (sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2" Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        While Not (sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                            sdbgVariables.MovePrevious
                        Wend
                        If sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1" Then
                            sdbgVariables.Columns("MOD").Value = "0"
                        End If
                        sdbgVariables.Bookmark = vb
                    End Select
                    sdbgVariables.Columns("MOD").Value = "0"
                End If
            End If
        Else
            vb = sdbgVariables.ActiveCell.Value
            sdbgVariables.ActiveCell.Value = IIf(vb = "0", "1", "0")
        End If
        sdbgVariables.Update
    ElseIf sdbgVariables.Columns(sdbgVariables.col).Name = "MOD" Then
        teserror = g_oUsuario.ModificarPermisoVariable(sdbgVariables.Columns("ID").Value, sdbgVariables.Columns("NIVEL").Value, False, GridCheckToBoolean(sdbgVariables.ActiveCell.Value))
        If teserror.NumError = TESnoerror Then
            If sdbgVariables.ActiveCell.Value <> "0" Then
                sdbgVariables.Columns("CONS").Value = "1"
                Select Case sdbgVariables.Columns("NIVEL").Value
                Case 1
                    vb = sdbgVariables.Bookmark
                    lID1 = sdbgVariables.Columns("ID").Value
                    sdbgVariables.MoveNext
                    i = sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark)
                    While sdbgVariables.Columns("IDNIVEL1").Value = lID1 And i < sdbgVariables.Rows
                        sdbgVariables.Columns("CONS").Value = "1"
                        sdbgVariables.Columns("MOD").Value = "1"
                        sdbgVariables.MoveNext
                        i = i + 1
                    Wend
                    sdbgVariables.Bookmark = vb
                Case 2
                    vb = sdbgVariables.Bookmark
                    lID2 = sdbgVariables.Columns("ID").Value
                    lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                    sdbgVariables.MoveNext
                    i = sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark)
                    While sdbgVariables.Columns("IDNIVEL2").Value = lID2 And sdbgVariables.Columns("IDNIVEL1").Value = lID1 And i < sdbgVariables.Rows
                        sdbgVariables.Columns("CONS").Value = "1"
                        sdbgVariables.Columns("MOD").Value = "1"
                        sdbgVariables.MoveNext
                        i = i + 1
                    Wend
                    sdbgVariables.Bookmark = vb
                Case 3
                    vb = sdbgVariables.Bookmark
                    lID3 = sdbgVariables.Columns("ID").Value
                    lID2 = sdbgVariables.Columns("IDNIVEL2").Value
                    lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                    sdbgVariables.MoveNext
                    i = sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark)
                    While sdbgVariables.Columns("IDNIVEL3").Value = lID3 And sdbgVariables.Columns("IDNIVEL2").Value = lID2 And sdbgVariables.Columns("IDNIVEL1").Value = lID1 And i < sdbgVariables.Rows
                        sdbgVariables.Columns("CONS").Value = "1"
                        sdbgVariables.Columns("MOD").Value = "1"
                        sdbgVariables.MoveNext
                        i = i + 1
                    Wend
                    sdbgVariables.Bookmark = vb
                Case 4
                    vb = sdbgVariables.Bookmark
                    lID4 = sdbgVariables.Columns("ID").Value
                    lID3 = sdbgVariables.Columns("IDNIVEL3").Value
                    lID2 = sdbgVariables.Columns("IDNIVEL2").Value
                    lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                    sdbgVariables.MoveNext
                    i = sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark)
                    While sdbgVariables.Columns("IDNIVEL4").Value = lID4 And sdbgVariables.Columns("IDNIVEL3").Value = lID3 And sdbgVariables.Columns("IDNIVEL2").Value = lID2 And sdbgVariables.Columns("IDNIVEL1").Value = lID1 And i < sdbgVariables.Rows
                        sdbgVariables.Columns("CONS").Value = "1"
                        sdbgVariables.Columns("MOD").Value = "1"
                        sdbgVariables.MoveNext
                        i = i + 1
                    Wend
                    sdbgVariables.Bookmark = vb
                End Select
            Else
                Select Case sdbgVariables.Columns("NIVEL").Value
                Case 2
                    vb = sdbgVariables.Bookmark
                    lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                    sdbgVariables.MovePrevious
                    While Not (sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID1 Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    sdbgVariables.Bookmark = vb
                Case 3
                    vb = sdbgVariables.Bookmark
                    lID2 = sdbgVariables.Columns("IDNIVEL2").Value
                    lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                    sdbgVariables.MovePrevious
                    While Not (sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2" Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    While Not (sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1" Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    sdbgVariables.Bookmark = vb
                Case 4
                    vb = sdbgVariables.Bookmark
                    lID3 = sdbgVariables.Columns("IDNIVEL3").Value
                    lID2 = sdbgVariables.Columns("IDNIVEL2").Value
                    lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                    sdbgVariables.MovePrevious
                    While Not (sdbgVariables.Columns("ID").Value = lID3 And sdbgVariables.Columns("NIVEL").Value = "3") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID3 And sdbgVariables.Columns("NIVEL").Value = "3" Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    While Not (sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2" Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    While Not (sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1" Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    sdbgVariables.Bookmark = vb
                Case 5
                    vb = sdbgVariables.Bookmark
                    lID4 = sdbgVariables.Columns("IDNIVEL4").Value
                    lID3 = sdbgVariables.Columns("IDNIVEL3").Value
                    lID2 = sdbgVariables.Columns("IDNIVEL2").Value
                    lID1 = sdbgVariables.Columns("IDNIVEL1").Value
                    sdbgVariables.MovePrevious
                    While Not (sdbgVariables.Columns("ID").Value = lID4 And sdbgVariables.Columns("NIVEL").Value = "4") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID4 And sdbgVariables.Columns("NIVEL").Value = "4" Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    While Not (sdbgVariables.Columns("ID").Value = lID3 And sdbgVariables.Columns("NIVEL").Value = "3") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID3 And sdbgVariables.Columns("NIVEL").Value = "3" Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    While Not (sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID2 And sdbgVariables.Columns("NIVEL").Value = "2" Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    While Not (sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1") And sdbgVariables.AddItemRowIndex(sdbgVariables.Bookmark) > 0
                        sdbgVariables.MovePrevious
                    Wend
                    If sdbgVariables.Columns("ID").Value = lID1 And sdbgVariables.Columns("NIVEL").Value = "1" Then
                        sdbgVariables.Columns("MOD").Value = "0"
                    End If
                    sdbgVariables.Bookmark = vb
                End Select
            End If
        Else
            vb = sdbgVariables.ActiveCell.Value
            sdbgVariables.ActiveCell.Value = IIf(vb = "0", "1", "0")
        End If
        sdbgVariables.Update
    End If
    m_bRespetarChange = False
End Sub

''' <summary>Marca las variables hijas de una var. dada</summary>
''' <param name="sColNivel">Col. que indica el nivel de la var. que se ha chequeado</param>
''' <param name="sColCheck">Columna que se ha chequeado</param>
''' <param name="PreviousTab">PreviousTab</param>
''' <remarks>Se ejecuta al hacer click en alguna de las pesta�as de sstabEstrorg; Tiempo m�ximo < 1 seg.</remarks>

Private Sub MarcarVariablesHijas(ByVal sColNivel As String, ByVal sColCheck As String)
    Dim iRow As Integer
    Dim vbm As Variant
    Dim lID As Long
    
    With sdbgVariables
        lID = .Columns("ID").Value
        
        vbm = .Bookmark
        .MoveNext
        iRow = .AddItemRowIndex(sdbgVariables.Bookmark)
        While .Columns(sColNivel).Value = lID And iRow < .Rows
            .Columns("CONS").Value = "1"
            .MoveNext
            iRow = iRow + 1
        Wend
        .Bookmark = vbm
    End With
End Sub

Private Sub sdbgVariables_RowLoaded(ByVal Bookmark As Variant)
        
    Select Case sdbgVariables.Columns("NIVEL").Value
    Case 1
        sdbgVariables.Columns("DEN").CellStyleSet "Nivel1"
    Case 2
        sdbgVariables.Columns("DEN").CellStyleSet "Nivel2"
    Case 3
        sdbgVariables.Columns("DEN").CellStyleSet "Nivel3"
    Case 4
        sdbgVariables.Columns("DEN").CellStyleSet "Nivel4"
    Case 5
        sdbgVariables.Columns("DEN").CellStyleSet "Nivel5"
    End Select
    
End Sub

Private Sub ssImportes_BeforeCellActivate(ByVal Cell As UltraGrid.SSCell, ByVal Cancel As UltraGrid.SSReturnBoolean)
    m_bValorAnteriorCeldaModif = ssImportes.ActiveRow.Cells("C_MODIF").Value
    m_bValorAnteriorCeldaCon = ssImportes.ActiveRow.Cells("C_CONSULTA").Value
    m_bValorAnteriorCeldaAlta = ssImportes.ActiveRow.Cells("C_ALTA").Value
    m_bValorAnteriorCeldaPresup = ssImportes.ActiveRow.Cells("C_PRESUP").Value
End Sub

Private Sub ssImportes_CellChange(ByVal Cell As UltraGrid.SSCell)
Dim bValorActualCeldaModif As Boolean
Dim bValorActualCeldaCon As Boolean
Dim bValorActualCeldaAlta As Boolean
Dim bValorActualCeldaPresup As Boolean
Dim bActualizarRow As Boolean
bActualizarRow = False

    If Not picEditFSSM.Visible Then
        ssImportes.PerformAction ssKeyActionExitEditMode
        If Cell.Value Then
           Cell.Value = False
        Else
            Cell.Value = True
        End If
        ssImportes.PerformAction ssKeyActionEnterEditMode
    Else
        ssImportes.PerformAction ssKeyActionExitEditMode
        bValorActualCeldaModif = ssImportes.ActiveRow.Cells("C_MODIF").Value
        bValorActualCeldaCon = ssImportes.ActiveRow.Cells("C_CONSULTA").Value
        bValorActualCeldaAlta = ssImportes.ActiveRow.Cells("C_ALTA").Value
        bValorActualCeldaPresup = ssImportes.ActiveRow.Cells("C_PRESUP").Value
        Dim sNivel As String
        Dim bSeleccionarHijos As Boolean
        
        'If Cell.Value Then
        
            bSeleccionarHijos = False
            bActualizarRow = True
            If ssImportes.ActiveRow.HasChild Then
               
                sNivel = "UON" & ssImportes.ActiveRow.Band.Index & "CC"
                If ssImportes.ActiveRow.Band.Index = 0 Then
                    bSeleccionarHijos = True
                ElseIf ssImportes.ActiveRow.Cells(sNivel).Value = 0 Then
                    bSeleccionarHijos = True
                End If
                
            End If
    
            If bSeleccionarHijos Then
                RecorrerBandasySeleccionarCC ssImportes.ActiveRow, ssImportes.ActiveCell.Column.key, Cell.Value
            End If
            
        Dim oParent As SSRow
        If ssImportes.ActiveRow.Band.Index > 0 Then
            Set oParent = ssImportes.ActiveRow.GetParent
            While Not oParent Is Nothing
                oParent.Cells(ssImportes.ActiveCell.Column.key).Value = ActualizarPadre(oParent, ssImportes.ActiveCell.Column.key)
                If oParent.Band.Index <> 0 Then
                    Set oParent = oParent.GetParent
                Else
                    Set oParent = Nothing
                End If
            Wend
        End If
            
        'End If
        
        Dim bActualizarConsulta As Boolean
        bActualizarConsulta = False
        Dim sAccion(3) As String
        
            
        'si se desmarca consulta hay que desmarcar automaticamente modificaci�n.
        If m_bValorAnteriorCeldaCon And Not bValorActualCeldaCon Then
            If (ssImportes.ActiveRow.Cells("C_ALTA").Value = True) Then
                sAccion(1) = "C_ALTA"
            End If
            If (ssImportes.ActiveRow.Cells("C_MODIF").Value = True) Then
                sAccion(2) = "C_MODIF"
            End If
            If (ssImportes.ActiveRow.Cells("C_PRESUP").Value = True) Then
                sAccion(3) = "C_PRESUP"
            End If
            ssImportes.ActiveRow.Cells("C_MODIF").Value = ssImportes.ActiveRow.Cells("C_CONSULTA").Value
            ssImportes.ActiveRow.Cells("C_ALTA").Value = False
            ssImportes.ActiveRow.Cells("C_PRESUP").Value = False
            ssImportes.PerformAction ssKeyActionEnterEditMode
            ssImportes.Refresh ssFireInitializeRow
        End If
        
        'Si se marca modificaci�n se tiene que marcar automaticamente consulta
        If Not m_bValorAnteriorCeldaModif And bValorActualCeldaModif Then
            ssImportes.ActiveRow.Cells("C_CONSULTA").Value = ssImportes.ActiveRow.Cells("C_MODIF").Value
            ssImportes.PerformAction ssKeyActionEnterEditMode
            ssImportes.Refresh ssFireInitializeRow
            bActualizarConsulta = True
        End If
        
        'Si se marca ALTA se tiene que marcar automaticamente consulta
        If Not m_bValorAnteriorCeldaAlta And bValorActualCeldaAlta Then
            ssImportes.ActiveRow.Cells("C_CONSULTA").Value = True
            ssImportes.PerformAction ssKeyActionEnterEditMode
            ssImportes.Refresh ssFireInitializeRow
            bActualizarConsulta = True
        End If
        
        
        'Si se marca PRESUPUESTACION se tiene que marcar automaticamente consulta
        If Not m_bValorAnteriorCeldaPresup And bValorActualCeldaPresup Then
            ssImportes.ActiveRow.Cells("C_CONSULTA").Value = True
            ssImportes.PerformAction ssKeyActionEnterEditMode
            bActualizarConsulta = True
            ssImportes.Refresh ssFireInitializeRow
        End If
        
        If bActualizarConsulta Then
            sAccion(0) = "C_CONSULTA"
        End If
        
        Dim i As Byte
        

        
        If ssImportes.ActiveRow.Band.Index > 0 Then
            Set oParent = ssImportes.ActiveRow.GetParent
            While Not oParent Is Nothing
                For i = 0 To UBound(sAccion)
                    If sAccion(i) <> "" Then
                        oParent.Cells(sAccion(i)).Value = ActualizarPadre(oParent, sAccion(i))
                    End If
                Next i
                
                If oParent.Band.Index <> 0 Then
                    Set oParent = oParent.GetParent
                Else
                    Set oParent = Nothing
                End If
            Wend
        End If

    End If
End Sub

''' Revisado por: blp. FEcha: 17/07/2012
''' <summary>
''' vac�a de contenido el caption de ssImportes al pasar a la pesta�a de Control de importes
''' </summary>
''' <param name="PreviousTab">PreviousTab</param>
''' <remarks>Se ejecuta al hacer click en alguna de las pesta�as de sstabEstrorg; Tiempo m�ximo < 1 seg.</remarks>
Private Sub sstabEstrorg_Click(PreviousTab As Integer)
    Select Case sstabEstrorg.Tab
        Case 0  'Imputaci�n de pedidos de aprovisionamiento ----------------
            cmdModificarFSSM.Enabled = True
        Case 1  'Control de importes   -------------------------------------
            ssImportes.caption = ""
            cmdModificarFSSM.Enabled = True
        Case 2
            cmdModificarFSSM.Enabled = False
    End Select
End Sub

''' <summary>
''' Cargar el grid de control de importes de SM
''' </summary>
''' <remarks>Llamada desde: sstabUsu_Click; AceptarFSSM_Tab1; cmdCancelarFSSM_Click; sstabEstrorg_Click. Tiempo m�ximo < 1 seg.</remarks>
Private Sub CargarFSSM_ControlImportes()
Dim oRow As SSRow

    ssImportes.caption = ""
    ssImportes.Layouts.clear
    Set ssImportes.DataSource = Nothing
    Set ssImportes.DataSource = g_oUsuario.DevolverRecordsetJerarquico()
    

    Set oRow = ssImportes.GetRow(ssChildRowFirst)
    oRow.Cells("C_CONSULTA").Value = ActualizarGrid(oRow, "C_CONSULTA")
    oRow.Cells("C_ALTA").Value = ActualizarGrid(oRow, "C_ALTA")
    oRow.Cells("C_MODIF").Value = ActualizarGrid(oRow, "C_MODIF")
    oRow.Cells("C_PRESUP").Value = ActualizarGrid(oRow, "C_PRESUP")

    ssImportes.ExpandAll
End Sub

''' <summary>
''' Lee los valores de permisos de usuario para la gesti�n de activos
''' </summary>
''' <remarks>Llamada desde: sstabUsu_Click; cmdCancelarFSSM_Click</remarks>
Private Sub CargarFSSM_PermisosActivos()
    chkAltaActivos.Value = BooleanToSQLBinary(g_oUsuario.ActivoAlta)
    chkModificarActivos.Value = BooleanToSQLBinary(g_oUsuario.ActivoModif)
    chkEliminarActivos.Value = BooleanToSQLBinary(g_oUsuario.ActivoBaja)
    'chkConsultaActivos.Value se tiene que establecer en ultimo lugar ya que depende de los otros tres en los eventos click
    chkConsultaActivos.Value = BooleanToSQLBinary(g_oUsuario.ActivoConsulta)
    chkNotifDisponible.Value = BooleanToSQLBinary(g_oUsuario.Notif_Disponible)
End Sub

''' <summary>
''' En caso de se haga click en el tab de calidad si es el de indice 3 se carga el arbol de unidadesde negocio
''' </summary>
''' <param name="PreviousTab">Indice del tab en el que se hace click</param>
Private Sub ssTabQA_Click(PreviousTab As Integer)
    Select Case ssTabQA.Tab
        Case 3
            GenerarEstructuraUnidadesNegocio m_sEstructuraUnidadesNegocio
            
            'Vacio la lista de permisos al entrar para que no aparezcan los ultimos cargados
            lstPermisosUndNegUsuRead.clear
            lstPermisosUndNegUsuRead.Visible = True
            lstPermisosUndNegUsu.Visible = False
            
            Set m_oPermisosUndNegUsuarioIniciales = oFSGSRaiz.Generar_CUnidadesNegQA
                        
            m_oPermisosUndNegUsuarioIniciales.PermisosUndNegUsuIniciales g_oUsuario.Cod
    End Select
End Sub

''' Revisado por: blp. Fecha: 30/04/2012
''' <summary>
''' Menu contextual del panel
''' </summary>
''' <param name="Button">Bot�n del rat�n que ha lanzado el evento</param>
''' <param name="Shift">
''' Integer that indicates whether an auxiliary key is pressed during the Mouse event
''' 0 (none), 1 (Shift), 2 (Ctrl), 4 (Alt), or the sum of any combination of those keys
''' </param>
''' <param name="x">Horizontal position of the mouse pointer from the internal left edge of the control or form receiving the event</param>
''' <param name="y">Vertical position of the mouse pointer from the internal top edge of the control or form receiving the event</param>
''' <remarks>
''' Llamada desde evento. M�ximo 0,3 seg.</remarks>
Private Sub tvwestrorg_GesFac_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim nodx As node
    If Button = 2 Then
        Set nodx = tvwestrorg_GesFac.selectedItem
        Set g_SelNode = tvwestrorg_GesFac.selectedItem
        If Not nodx Is Nothing Then
            ''La forma m�s r�pida y sencilla de saber si el nodo seleccionado es centro de coste o no
            '(lo cual implica mostrar o no el men� contextual para centro de coste)
            'es mirar la imagen que se le ha dado, y que contiene, caso de ser centro de coste, una doble "C".
            'Si cambiase el nombre de la imagen este sistema dejar�a de funcionar.
            Select Case InStr(1, nodx.Image, "CC")
                'Case "UON0", "UON1", "UON2", "UON3", "UON4":
                Case Is > 1:
                    'PopupMenu mnuGestorPartidas
                    cargarMenu X, Y + 1600
            End Select
        End If
    End If
End Sub

Private Sub tvwUndNegUsuRead_NodeClick(ByVal node As MSComctlLib.node)
    CargarListaPermisosUnidadesNegocio node.key
End Sub

Private Sub CargarListaPermisosUnidadesNegocio(ByVal nodeKey As String)
    Dim i As Integer
    Dim sNivel() As String
    'Borramos los niveles de escalacion porque tendremos que crearlos despu�s en fincion del nivel de las unidades de negocio
    If lstPermisosUndNegUsu.ListCount > 11 Then
        For i = (lstPermisosUndNegUsu.ListCount - 1) To 11 Step -1
            lstPermisosUndNegUsu.RemoveItem (i)
        Next i
    End If
    'Agregamos los permisos en funci�n de la configuracion de los niveles de escalaci�n
    m_sNivelesEscalacion = m_oPermisosUndNegUsuarioIniciales.SacarNivelEscalacion(Mid(nodeKey, 3, 1))
    'Cargamos los controles en el listbox en funcion de los niveles
    sNivel = Split(m_sNivelesEscalacion, "#")
    For i = 0 To UBound(sNivel)
        Select Case sNivel(i)
            Case 1
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(0)
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(1)
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(2)
            Case 2
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(3)
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(4)
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(5)
            Case 3
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(6)
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(7)
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(8)
            Case 4
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(9)
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(10)
                lstPermisosUndNegUsu.AddItem m_sPermisosEscalacion(11)
        End Select
    Next i
    If m_sEstructuraUnidadesNegocio = "C" Then
        'Modo consulta
        
        lstPermisosUndNegUsuRead.clear

        For i = 8 To 1 Step -1
            If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#" & i) Is Nothing Then
                Select Case i
                    Case Is < 3
                        lstPermisosUndNegUsuRead.AddItem lstPermisosUndNegUsu.List(i - 1), 0
                    Case Else
                        lstPermisosUndNegUsuRead.AddItem lstPermisosUndNegUsu.List(i), 0
                End Select
            End If
        Next
        
        If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#9") Is Nothing Then lstPermisosUndNegUsuRead.AddItem lstPermisosUndNegUsu.List(2), 2
        If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#10") Is Nothing Then lstPermisosUndNegUsuRead.AddItem lstPermisosUndNegUsu.List(9), lstPermisosUndNegUsuRead.ListCount
        If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#11") Is Nothing Then lstPermisosUndNegUsuRead.AddItem lstPermisosUndNegUsu.List(10), lstPermisosUndNegUsuRead.ListCount
        
        For i = 0 To UBound(m_sPermisosEscalacion)
            If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#" & (i + 12)) Is Nothing Then
                lstPermisosUndNegUsuRead.AddItem m_sPermisosEscalacion(i), (lstPermisosUndNegUsuRead.ListCount - 1) + 1
            End If
        Next i
    Else
        'Modo edici�n
        
        If tvwUndNegUsuRead.selectedItem Is Nothing Then
            lstPermisosUndNegUsu.Enabled = True
        Else
            lstPermisosUndNegUsu.Enabled = False
        End If
        
        m_oRecargaPermisosUndNegUsuario = True
        lstPermisosUndNegUsu.Enabled = True
        
        'Primero limpio la lista de permisos checkeados
        For i = 0 To lstPermisosUndNegUsu.ListCount - 1
            lstPermisosUndNegUsu.Selected(i) = False
        Next
        
        For i = 11 To 1 Step -1
            If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#" & i) Is Nothing Then
                If Not m_oPermisosUndNegUsuarioEliminar Is Nothing Then
                    'Si el permiso esta en los iniciales
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#" & i) Is Nothing Then
                        'Si no esta en el array para eliminar lo selecciono
                        Select Case i
                            Case Is < 3
                                lstPermisosUndNegUsu.Selected(i - 1) = True
                            Case 9
                                lstPermisosUndNegUsu.Selected(2) = True
                            Case 10
                                lstPermisosUndNegUsu.Selected(9) = True
                            Case 11
                                lstPermisosUndNegUsu.Selected(10) = True
                            Case Else
                                lstPermisosUndNegUsu.Selected(i) = True
                        End Select
                    End If
                Else
                    'El permiso esta en los iniciales. Lo selecciono
                    Select Case i
                        Case Is < 3
                            lstPermisosUndNegUsu.Selected(i - 1) = True
                        Case 9
                            lstPermisosUndNegUsu.Selected(2) = True
                        Case 10
                            lstPermisosUndNegUsu.Selected(9) = True
                        Case 11
                            lstPermisosUndNegUsu.Selected(10) = True
                        Case Else
                            lstPermisosUndNegUsu.Selected(i) = True
                    End Select
                End If
            Else
                'El permiso no esta en las iniciales. Tengo que ver si esta en el array de agregar
                If Not m_oPermisosUndNegUsuarioAgregar Is Nothing Then
                    If Not m_oPermisosUndNegUsuarioAgregar.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#" & i) Is Nothing Then
                        'Esta en agregar, lo selecciono
                        Select Case i
                            Case Is < 3
                                lstPermisosUndNegUsu.Selected(i - 1) = True
                            Case 9
                                lstPermisosUndNegUsu.Selected(2) = True
                            Case 10
                                lstPermisosUndNegUsu.Selected(9) = True
                            Case 11
                                lstPermisosUndNegUsu.Selected(10) = True
                            Case Else
                                lstPermisosUndNegUsu.Selected(i) = True
                        End Select
                    End If
                End If
            End If
        Next
        Dim j As Integer
        Dim b As Boolean
        'Niveles de escalaci�n
        For i = 0 To UBound(m_sPermisosEscalacion)
            b = False
            If Not m_oPermisosUndNegUsuarioIniciales.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#" & (i + 12)) Is Nothing Then
                If Not m_oPermisosUndNegUsuarioEliminar Is Nothing Then
                    If m_oPermisosUndNegUsuarioEliminar.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#" & (i + 12)) Is Nothing Then
                        b = True
                    End If
                Else
                    b = True
                End If
            Else
                'El permiso no esta en las iniciales. Tengo que ver si esta en el array de agregar
                If Not m_oPermisosUndNegUsuarioAgregar Is Nothing Then
                    If Not m_oPermisosUndNegUsuarioAgregar.Item(Mid(nodeKey, 4, Len(nodeKey)) & "#" & (i + 12)) Is Nothing Then
                        b = True
                    End If
                End If
            End If
            
            If b Then
                For j = 11 To lstPermisosUndNegUsu.ListCount
                    If m_sPermisosEscalacion(i) = lstPermisosUndNegUsu.List(j) Then
                        lstPermisosUndNegUsu.Selected(j) = True
                    End If
                Next j
            End If
        Next i
        m_oRecargaPermisosUndNegUsuario = False
    End If
End Sub

''' Revisado por: jvs. Fecha: 15/02/2012
''' <summary>
''' Ocurre cuando se activa un tab en el control SSTab
''' </summary>
''' <param name="PreviousTab">Indice del tab anterior</param>
''' <remarks>Llamada desde evento click
''' Se utiliza para cargar los datos relacionados con cada tab y visualizar la informaci�n pertinente.
''' M�x. 0,3 seg.</remarks>
Private Sub sstabUsu_Click(PreviousTab As Integer)
    Select Case sstabUsu.Tab
    
        Case 0  'Usuarios
            picNavigateUsu.Enabled = True
            picNavigateSegur.Enabled = False
            picEdit.Enabled = False
            cmdGS(2).Enabled = False
            cmdGS(0).Enabled = False
            cmdGS(1).Enabled = False
            
            cmdUsu(4).Enabled = True
            cmdUsu(5).Enabled = True
            cmdUsu(6).Enabled = True
            cmdUsu(0).Enabled = True
            cmdUsu(3).Enabled = True
            cmdUsu(1).Enabled = True
            cmdUsu(2).Enabled = True
            
            ActivarTabulacionUsu
            Set m_oAccionesIniciales = Nothing
            Accion = ACCUsuMod
            RefrescarTvw
        
            fraAcciones.Visible = False
            picEdit.Visible = False
            
        Case 1  'FSGS
            Screen.MousePointer = vbHourglass
            picNavigateUsu.Enabled = False
            picNavigateSegur.Enabled = True
            picEdit.Enabled = False
            cmdGS(2).Enabled = True
            cmdGS(0).Enabled = True
            cmdGS(1).Enabled = True
            
            cmdUsu(4).Enabled = False
            cmdUsu(5).Enabled = False
            cmdUsu(6).Enabled = False
            cmdUsu(0).Enabled = False
            cmdUsu(3).Enabled = False
            cmdUsu(1).Enabled = False
            cmdUsu(2).Enabled = False
            
            Accion = accusucon
            
            DesactivarTabulacionUsu
            If g_oUsuario.Perfil Is Nothing Then
                If g_oUsuario.Acciones Is Nothing Then
                    g_oUsuario.CargarAcciones basPublic.gParametrosInstalacion.gIdioma
                End If
                Set m_oAccionesIniciales = g_oUsuario.Acciones
            End If
            
            tvwsegur.Nodes(1).Selected = True
            If m_oAccionesIniciales Is Nothing Then
                Set m_oAccionesIniciales = oFSGSRaiz.Generar_CAcciones
            End If
            
            cmdGS_Click 1
            
            Screen.MousePointer = vbNormal
        
            lstvwSegurReadOnly.ListItems.clear
            fraAcciones.Visible = True
            picEdit.Visible = False
            ReiniciarTvw
                    
        Case 2  'FSWS
            CargarFSWS
            fraAcciones.Visible = False
            picEdit.Visible = False
            
        Case 3  'FSEP
            'Muestra las propiedades correspondientes al acceso al FSEP
            chkFSEP(0).Value = BooleanToSQLBinary(g_oUsuario.PedidoLibre)
            chkFSEP(12).Value = BooleanToSQLBinary(g_oUsuario.OtrasEmp)
            chkFSEP(11).Value = BooleanToSQLBinary(g_oUsuario.ModificarPrecios)
            chkFSEP(18).Value = BooleanToSQLBinary(g_oUsuario.AnularRecepciones)
            chkFSEP(14).Value = BooleanToSQLBinary(g_oUsuario.VerPedidosCCImputables)
            chkFSEP(17).Value = BooleanToSQLBinary(g_oUsuario.RecepcionarPedidosCCImputables)
            chkFSEP(16).Value = BooleanToSQLBinary(g_oUsuario.PermisoReabrirPedidos)
            chkFSEP(15).Value = BooleanToSQLBinary(g_oUsuario.PermisoCerrarPedidos)
            chkFSEP(19) = BooleanToSQLBinary(g_oUsuario.BloqueoAlbaranesFacturacion)
            chkFSEP(20) = BooleanToSQLBinary(g_oUsuario.RestrSelCentroAprovUsu)
            chkFSEP(13) = BooleanToSQLBinary(g_oUsuario.RestringirPedidiosArtUONUsuario)
            chkFSEP(1) = BooleanToSQLBinary(g_oUsuario.EmitirPedAbierto)
            chkFSEP(2) = BooleanToSQLBinary(g_oUsuario.RestrEmiContraPedAbiertoEmpUsu)
            chkFSEP(3) = BooleanToSQLBinary(g_oUsuario.PermitSelPres1UOEncimaUsu)
            chkFSEP(5) = BooleanToSQLBinary(g_oUsuario.PermitSelPres2UOEncimaUsu)
            chkFSEP(6) = BooleanToSQLBinary(g_oUsuario.PermitSelPres3UOEncimaUsu)
            chkFSEP(7) = BooleanToSQLBinary(g_oUsuario.PermitSelPres4UOEncimaUsu)
            chkFSEP(4) = BooleanToSQLBinary(g_oUsuario.RestrSelPres1UOUsu)
            chkFSEP(8) = BooleanToSQLBinary(g_oUsuario.RestrSelPres2UOUsu)
            chkFSEP(9) = BooleanToSQLBinary(g_oUsuario.RestrSelPres3UOUsu)
            chkFSEP(10) = BooleanToSQLBinary(g_oUsuario.RestrSelPres4UOUsu)
            chkFSEP(21) = BooleanToSQLBinary(g_oUsuario.EPPermitirAnularPedidos)
            chkFSEP(22) = BooleanToSQLBinary(g_oUsuario.EPPermitirVerPedidosBorradosSeguimiento)
            chkFSEP(23) = BooleanToSQLBinary(g_oUsuario.EPPermitirBorrarLineasPedido)
            chkFSEP(24) = BooleanToSQLBinary(g_oUsuario.EPPermitirDeshacerBorradoLineasPedido)
            chkFSEP(25) = BooleanToSQLBinary(g_oUsuario.EPPermitirBorrarPedidos)
            chkFSEP(26) = BooleanToSQLBinary(g_oUsuario.EPPermitirDeshacerBorradoPedidos)
            chkFSEP(27) = BooleanToSQLBinary(g_oUsuario.EPPermitirModificarPorcentajeDesvio)
        Case 4  'FSQA
            ssTabQA.Tab = 0
            CargarFSQA
            fraAcciones.Visible = False
            picEdit.Visible = False
            
        Case 5  'FSSM
            Screen.MousePointer = vbHourglass
            sstabSM.Tab = 0
            sstabEstrorg.Tab = 0
            'CARGAMOS TODOS LOS NODOS EN SM
            CargarFSSM_ArbolImputacion
            CargarFSSM_ControlImportes
            fraAcciones.Visible = False
            picEdit.Visible = False
            'incidencia 31800.4 2008 14
            FrameFSSMTab0.Enabled = True
            FrameFSSMTab1.Enabled = True
            fraArboles.Enabled = True
            CargarFSSM_PermisosActivos
            Screen.MousePointer = vbNormal
            
        Case 6  'CN
            SSTabCN.Tab = 0
            CargarFSCN
            fraAcciones.Visible = False
            picEdit.Visible = False
            
            
        Case 7  'FSGS Destinos
            PicDestPorDefecto.Enabled = False
            CargarFSDirectos
            
        Case 8 'Perfil
            If Not g_oUsuario.Perfil Is Nothing Then MostrarPerfil g_oUsuario.Perfil.Id
        Case 9 'Integracion
            cargarFSIS
        Case 10 'Facturacion
            CargarFSIM
        Case 11 'Contratos
            CargarFSCM
        Case 12 'Seguridad de solicitudes
            CargarFSSOL
    End Select
End Sub



''' <summary>Muestra los datos del perfil accediendo a la URL de su mantenimiento</summary>
''' <remarks>Llamadas desde: sstabUsu_Click</remarks>
''' <revision>LTG 28/12/2012</revision>

Private Sub MostrarPerfil(ByVal lIdPerfil As Long)
    Dim sRuta As String
    Dim sSessionId As String
    
    sSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
    
    sRuta = gParametrosGenerales.gsRutasFullstepWeb
    If Right(sRuta, 1) <> "/" Then sRuta = sRuta & "/"
    sRuta = sRuta & "App_Pages/SG/DetallePerfil.aspx?sessionId=" & sSessionId & "&idPerfil=" & CStr(lIdPerfil) & "&desdeGS=1&usu=1"
    
    wPerfil.Navigate2 sRuta, 4 'que no coja de la cache
End Sub

Private Sub tvwsegur_Collapse(ByVal node As MSComctlLib.node)
   If g_oUsuario.Tipo = 5 And node.Tag = "Pedidos" Then
        Exit Sub
    End If
End Sub

Private Sub tvwsegur_Expand(ByVal node As MSComctlLib.node)
    If g_oUsuario.Tipo = 5 And node.Tag = "Pedidos" Then
        node.Expanded = False
        Exit Sub
    End If

End Sub

Private Sub tvwSegur_NodeClick(ByVal node As MSComctlLib.node)
If Not cmdGS(5).Enabled Then cmdGS(5).Visible = False
Dim iAccion As AccionesDeSeguridad
Dim iIndice As Integer
Dim iInd2 As Integer
Dim sCadena As String
                
lstvwSegur_LostFocus

Select Case Accion

Case AccionesSummit.accusucon
    sCadena = node.Tag
    If Mid(sCadena, 1, 3) = "ACC" Then
        sCadena = Mid(node.Tag, 4, 7)
        iAccion = Int(val(sCadena))
        CargarAcciones iAccion
        
        cmdGS(5).Visible = False
    Else
        cmdGS(5).Visible = False
        lstSegurIdAcciones.clear
        lstvwSegurReadOnly.ListItems.clear
    End If

Case AccionesSummit.ACCUsuModSeg
    sCadena = node.Tag
    If Mid(sCadena, 1, 3) = "ACC" Then
        sCadena = Mid(node.Tag, 4, 7)
        iAccion = Int(val(sCadena))
        
        LlenarListaConPlantillaDeAcciones iAccion, NUM_ACCIONES
        MarcarAccionesDelUsuario
        iInd2 = 0
        For iIndice = 0 To NUM_ACCIONES - 1
        If Not m_oPlantillaDeAccionesCargadas.Item(CStr(iAccion + iIndice)) Is Nothing Then
            ' Marco las acciones a anyadir y las quito, ya que se cargaran de nuevo en el lost focus de lstSegur
            If Not m_oAccionesAAnyadir.Item(CStr(iAccion + iIndice)) Is Nothing Then
                lstvwSegur.ListItems((m_oPlantillaDeAccionesCargadas.Item(CStr(iAccion + iIndice)).Orden - iAccion) - (iIndice - iInd2) + 1).Checked = True
                m_oAccionesAAnyadir.Remove CStr(iIndice + iAccion)
            Else
                ' Borro las acciones a eliminar y las quito de la lista
                If Not m_oAccionesAEliminar.Item(CStr(iAccion + iIndice)) Is Nothing Then
                    lstvwSegur.ListItems((m_oPlantillaDeAccionesCargadas.Item(CStr(iAccion + iIndice)).Orden - iAccion) - (iIndice - iInd2) + 1).Checked = False
                    m_oAccionesAEliminar.Remove (CStr(iAccion + iIndice))
                End If
            
            End If
            If iInd2 < lstvwSegur.ListItems.Count - 1 Then
                iInd2 = iInd2 + 1
            End If
        End If
        Next
                
        cmdGS(5).Visible = False
    Else
        cmdGS(5).Visible = False
        lstSegurIdAcciones.clear
        lstvwSegurReadOnly.ListItems.clear
        lstvwSegur.ListItems.clear
    End If
 
 End Select

End Sub


Private Sub tvwUsu_Collapse(ByVal node As MSComctlLib.node)
    sstabUsu.TabVisible(1) = False
    sstabUsu.TabVisible(2) = False
    sstabUsu.TabVisible(3) = False
    sstabUsu.TabVisible(4) = False
    sstabUsu.TabVisible(5) = False
    sstabUsu.TabVisible(6) = False
    sstabUsu.TabVisible(7) = False
    sstabUsu.TabVisible(8) = False
    sstabUsu.TabVisible(9) = False
    sstabUsu.TabVisible(10) = False
    sstabUsu.TabVisible(12) = False
End Sub

Private Sub tvwUsu_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nodx As node
Set nodx = tvwUsu.selectedItem

If nodx Is Nothing Then Exit Sub

If Button = 2 Then
    
    Select Case nodx.Tag
    
    Case "Raiz"

        PopupMenu MDI.mnuPopUpUsuarios
            
    Case "Comprador", "Persona"
        PopupMenu MDI.mnuPopUpUsuario
                        
    End Select
    
End If

End Sub

''' Revisado por: blp. Fecha: 24/10/2012
''' Carga las pesta�as en funci�n del usuario sobre el que se ha hecho click
''' node: nodo (usuario) sobre el que se ha hecho click
''' Llamada desde evento click. M�x. 0,3 seg.

Public Sub tvwUsu_NodeClick(ByVal node As MSComctlLib.node)
        
    Select Case node.Tag
        Case "Persona", "Comprador"
            cmdUsu(0).Enabled = False
            cmdUsu(1).Enabled = True
            cmdUsu(2).Enabled = True
            cmdUsu(3).Enabled = True
    
            Set g_oUsuario = Nothing
            Set g_oUsuario = oFSGSRaiz.generar_cusuario
            g_oUsuario.Cod = DevolverCod(node)
        
            Select Case node.Tag
                Case "Persona"
                    g_oUsuario.Tipo = TIpoDeUsuario.Persona
                Case "Comprador"
                    g_oUsuario.Tipo = TIpoDeUsuario.comprador
            End Select
            Me.caption = m_sIdiUsuario & " " & DevolverCod(node)
    
            If InStr(node.Text, m_sIdiPersonalizado) <> 0 Then
                Set g_oUsuario.Perfil = Nothing
            Else
                Set g_oUsuario.Perfil = oFSGSRaiz.Generar_CPerfil
            End If
    
            g_oUsuario.ExpandirUsuario
        Case Else
            Me.caption = m_sCaption
            
            Set g_oUsuario = Nothing
            cmdUsu(2).Enabled = False
            cmdUsu(3).Enabled = False
            cmdUsu(1).Enabled = False
            cmdUsu(0).Enabled = True
    End Select
    
    VisibilidadTabs
    
    If Me.Visible Then tvwUsu.SetFocus
End Sub

''' <summary>Se encarga de la visibilidad de los tabs en funci�n del perfil del usuario � sus permisos de acceso</summary>
''' <remarks>Llamadas desde: tvwUsu_NodeClick</remarks>
''' <revision>LTG 28/12/2012</revision>

Private Sub VisibilidadTabs()
    Dim bFSWSActivoyVisible As Boolean
    Dim bContratosActivoyVisible As Boolean
    Dim Ador As Ador.Recordset
    Dim bAccesoFSGS As Boolean
    Dim bAccesoFSQA As Boolean
    Dim bAccesoFSPM As Boolean
    Dim bAccesoFSSM As Boolean
    Dim bAccesoFSEP As Boolean
    Dim bAccesoFSINT As Boolean
    Dim bAccesoFSIM As Boolean
    Dim bAccesoFSSOL As Boolean
    Dim bAccesoFSCM As Boolean
    
    With sstabUsu
        If Not g_oUsuario Is Nothing Then
            If g_oUsuario.Perfil Is Nothing Then
                bAccesoFSGS = g_oUsuario.AccesoFSGS
                bAccesoFSQA = g_oUsuario.AccesoFSQA
                bAccesoFSPM = g_oUsuario.AccesoFSWS
                bAccesoFSSM = g_oUsuario.AccesoFSSM
                bAccesoFSEP = g_oUsuario.AccesoFSEP
                bAccesoFSINT = g_oUsuario.AccesoFSINT
                bAccesoFSIM = g_oUsuario.AccesoFSIM
                bAccesoFSSOL = (g_oUsuario.AccesoFSIM Or g_oUsuario.AccesoFSQA Or g_oUsuario.AccesoFSWS Or g_oUsuario.AccesoFSContratos)
                bAccesoFSCM = g_oUsuario.AccesoFSContratos
            Else
                bAccesoFSGS = g_oUsuario.Perfil.FSGS
                bAccesoFSQA = g_oUsuario.Perfil.FSQA
                bAccesoFSPM = g_oUsuario.Perfil.FSPM
                bAccesoFSSM = g_oUsuario.Perfil.FSSM
                bAccesoFSEP = g_oUsuario.Perfil.FSEP
                bAccesoFSINT = g_oUsuario.Perfil.AccesoFSINT
                bAccesoFSIM = g_oUsuario.Perfil.FSIM
                bAccesoFSSOL = (g_oUsuario.Perfil.FSIM Or g_oUsuario.Perfil.FSQA Or g_oUsuario.Perfil.FSPM Or g_oUsuario.Perfil.FSCM)
                bAccesoFSCM = g_oUsuario.Perfil.FSCM
            End If
            
            'Acciones GS
            If g_oUsuario.Perfil Is Nothing Then
                sstabUsu.TabVisible(1) = bAccesoFSGS
            
                bFSWSActivoyVisible = (bAccesoFSPM And gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso)
                bContratosActivoyVisible = (g_oUsuario.AccesoFSContratos And gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.SinAcceso)
            Else
                .TabVisible(1) = False
                
                bFSWSActivoyVisible = (bAccesoFSPM And gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso)
                bContratosActivoyVisible = (g_oUsuario.Perfil.FSCM And gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.SinAcceso)
            End If
                     
            'PM
            If g_oUsuario.Perfil Is Nothing Then
                If bFSWSActivoyVisible Then
                    sstabUsu.TabVisible(2) = True
                                    
                    On Error Resume Next
                    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_USUARIOS, basPublic.gParametrosInstalacion.gIdioma, 36)
                    If Not Ador Is Nothing Then
                        sstabUsu.TabCaption(2) = Ador(0).Value  ' 36 Acceso a solicitudes FSPM
                        Ador.Close
                    End If
                    Set Ador = Nothing
                Else
                    sstabUsu.TabVisible(2) = False
                End If
            Else
                sstabUsu.TabVisible(2) = False
            End If
            
            'CM
            If g_oUsuario.Perfil Is Nothing Then
                If bContratosActivoyVisible Then
                    sstabUsu.TabVisible(11) = True
                                    
                    On Error Resume Next
                    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_USUARIOS, basPublic.gParametrosInstalacion.gIdioma, 166)
                    If Not Ador Is Nothing Then
                        sstabUsu.TabCaption(11) = Ador(0).Value  ' 166 Acceso a FSCM
                        Ador.Close
                    End If
                    Set Ador = Nothing
                Else
                    sstabUsu.TabVisible(11) = False
                End If
            Else
                sstabUsu.TabVisible(11) = False
            End If
            
            'EP
            If g_oUsuario.Perfil Is Nothing Then
                'si no est� activado el par�metro de aprovisionamiento no se muestra el tab.
                If Not gParametrosGenerales.gbPedidosAprov Then
                    sstabUsu.TabVisible(3) = False
                Else
                    sstabUsu.TabVisible(3) = g_oUsuario.AccesoFSEP
                End If
            Else
                .TabVisible(3) = False
            End If
            
            'QA
            If bAccesoFSQA Or bAccesoFSGS Then
                If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
                    sstabUsu.TabVisible(4) = True
                    If bAccesoFSQA = False Then
                        ssTabQA.TabVisible(1) = False
                        ssTabQA.TabVisible(2) = False
                        sdbgVariables.Columns("MOD").Visible = False
                        sdbgVariables.Columns("CONS").Width = 1450
                        sdbgVariables.Columns("MOD").Width = 1450
                        sdbgVariables.Columns("DEN").Width = sdbgVariables.Width - 2050
                    Else
                        ssTabQA.TabVisible(1) = (g_oUsuario.Perfil Is Nothing)
                        ssTabQA.TabVisible(2) = (g_oUsuario.Perfil Is Nothing)
                        sdbgVariables.Columns("MOD").Visible = True
                        sdbgVariables.Columns("CONS").Width = 1450
                        sdbgVariables.Columns("MOD").Width = 1450
                        sdbgVariables.Columns("DEN").Width = sdbgVariables.Width - 3500
                    End If
                Else
                    sstabUsu.TabVisible(4) = False
                End If
            Else
                sstabUsu.TabVisible(4) = False
            End If
            
            'SM
            If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.SinAcceso Then
                sstabUsu.TabVisible(5) = False
            Else
                If bAccesoFSSM Then
                    sstabUsu.TabVisible(5) = True
                    sstabSM.TabVisible(1) = (g_oUsuario.Perfil Is Nothing)
                    
                    sstabEstrorg.TabVisible(1) = True
                    sstabEstrorg.TabVisible(2) = True
                    
                    Set g_oUsuario.Presups5Niv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
                    g_oUsuario.Presups5Niv0.CargarPresupuestosConceptos5 , , , , , , , , g_oUsuario.Cod
                Else
                    If (gParametrosGenerales.gbAccesoFSGS = AccesoFSGS And bAccesoFSGS) Or gParametrosGenerales.gbPedidosAprov Or bFSWSActivoyVisible Then
                        sstabUsu.TabVisible(5) = True
                        sstabSM.TabVisible(1) = False
                        sstabEstrorg.TabVisible(1) = False
                        sstabEstrorg.TabVisible(2) = bFSWSActivoyVisible
                    Else
                        sstabUsu.TabVisible(5) = False
                    End If
                End If
            End If
            
            'CN
            If g_oUsuario.Perfil Is Nothing Then
                If gParametrosGenerales.gsAccesoFSCN <> TipoAccesoFSCN.SinAcceso Then
                    sstabUsu.TabVisible(6) = True
                    SSTabCN.TabVisible(0) = True
                    SSTabCN.TabVisible(1) = True
                Else
                    sstabUsu.TabVisible(6) = False
                End If
            Else
                .TabVisible(6) = False
            End If
        
            'Destinos para pedidos
            If (bAccesoFSGS And gParametrosGenerales.gbPedidosDirectos) Or bAccesoFSEP Then
                sstabUsu.TabVisible(7) = True
            Else
                sstabUsu.TabVisible(7) = False
            End If
            
            'Perfil
            If Not g_oUsuario.Perfil Is Nothing Then
                sstabUsu.TabVisible(8) = True
                sstabUsu.TabCaption(8) = g_oUsuario.Perfil.Den
            Else
                sstabUsu.TabVisible(8) = False
            End If
            
            'IS
            If g_oUsuario.Perfil Is Nothing And bAccesoFSINT Then
                sstabUsu.TabVisible(9) = True
            Else
                sstabUsu.TabVisible(9) = False
            End If
            
            'IM
            If g_oUsuario.Perfil Is Nothing Then
                If gParametrosGenerales.gsAccesoFSIM <> TipoAccesoFSIM.SinAcceso And bAccesoFSIM Then
                    .TabVisible(10) = True
                Else
                    .TabVisible(10) = False
                End If
            Else
                .TabVisible(10) = False
            End If
            
            'SOL
            If (bAccesoFSSOL And (gParametrosGenerales.gsAccesoFSIM <> TipoAccesoFSIM.SinAcceso Or gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Or bFSWSActivoyVisible Or bContratosActivoyVisible)) Then
                .TabVisible(12) = True
                
                If Not g_oUsuario.Perfil Is Nothing Then
                    fraFSSOLAcciones.Visible = False
                    fraFSSOLConcepPresup.Top = fraFSSOLAcciones.Top
                Else
                    fraFSSOLAcciones.Visible = True
                    fraFSSOLConcepPresup.Top = fraFSSOLAcciones.Top + fraFSSOLAcciones.Height
                End If
            Else
                .TabVisible(12) = False
            End If
        End If
    End With
End Sub

''' <summary>Evento que abre la ventana de detalle de usuario, al clicar en la opcion de detalle, cargando sus datos</summary>
''' <remarks>Llamadas desde: Autom�tico, al seleccionar la opci�n de detalle usuario</remarks>
''' <revision>LTG 18/12/2012</revision>

Public Sub mnuPopUpUsuDet()
    Dim nodx As node
    Dim teserror As TipoErrorSummit
    
    Set nodx = tvwUsu.selectedItem
    
    If nodx Is Nothing Then Exit Sub

    Accion = ACCusudet
    Set g_oUsuario = Nothing
    Set g_oUsuario = oFSGSRaiz.generar_cusuario
    g_oUsuario.Cod = DevolverCod(nodx)
    g_oUsuario.Tipo = TIpoDeUsuario.Persona
    
    
    ' Si el usuario ha sido borrado o se le ha modificado el codigo, se mostrara u mensaje indicandolo
    If g_oUsuario.ExpandirUsuario Then
        Set g_oIBaseDatos = g_oUsuario
        
        If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
            teserror = g_oUsuario.RetrieveUserProperties
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Accion = accusucon
                Set nodx = Nothing
                Exit Sub
            End If
        End If
                
        frmUSUPERSWinSec.caption = m_sIdiDatUsu
        frmUSUPERSWinSec.Show vbModal
    Else
        Accion = accusucon
    End If

    Set nodx = Nothing
End Sub

Private Sub CargarAcciones(ByVal iAccion As Integer)
    Dim iNumDeAcciones As Integer
    Dim oAccion As CAccion
    
    iNumDeAcciones = NUM_ACCIONES

    lstvwSegurReadOnly.ListItems.clear
    
    If Not m_oAccionesIniciales Is Nothing Then
        For Each oAccion In m_oAccionesIniciales
            If (oAccion.Id - iAccion) >= 0 And (oAccion.Id - iAccion) <= iNumDeAcciones - 1 Then
                If Not NoCargarAccion(oAccion.Id) Then
                    lstvwSegurReadOnly.ListItems.Add key:="A" & oAccion.Id, Text:=oAccion.Den
                End If
            End If
        Next
    End If


End Sub

''' <summary>
''' Hace editable el contenido de la pantalla para realizar modificaciones
''' </summary>
''' <param name="iTab1">Indica el tab dentro del primer nivel de tabs</param>
''' <param name="iTab1">Indica el tab dentro del segundo nivel de tabs</param>

Private Sub ModoEdicion(ByVal iTab As Integer, Optional ByVal iTab2 As Integer)
    With sstabUsu
        Select Case iTab
            Case 1   'Acceso FSGS
                Accion = ACCUsuModSeg
                
                If g_oUsuario.Perfil Is Nothing Then
                    lstvwSegurReadOnly.Visible = False
                    lstvwSegur.Visible = True
                End If
                
                picNavigateSegur.Visible = False
                picNavigateSegur.Enabled = False
                picEdit.Enabled = True
                picEdit.Visible = True
                .TabEnabled(0) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
                .TabEnabled(7) = False
                .TabEnabled(9) = False
                .TabEnabled(10) = False
                .TabEnabled(11) = False
                .TabEnabled(12) = False
                
            Case 2  'Acceso FSWS/Contratos
                picFSWS.Enabled = True
                picNavigateFSWS.Visible = False
                picEditFSWS.Visible = True
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
                .TabEnabled(7) = False
                .TabEnabled(9) = False
                .TabEnabled(10) = False
                .TabEnabled(11) = False
                .TabEnabled(12) = False
                
            Case 3  'Acceso FSEP
                picFSEP.Enabled = True
                picNavigateFSEP.Visible = False
                picEditFSEP.Visible = True
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
                .TabEnabled(7) = False
                .TabEnabled(9) = False
                .TabEnabled(10) = False
                .TabEnabled(11) = False
                .TabEnabled(12) = False
                
            Case 4   'Acceso FSQA
                Select Case iTab2
                    Case 1
                        picFSQA(1).Enabled = True
                        picNavigateFSQA(0).Visible = False
                        picEditFSQA(0).Visible = True
                        ssTabQA.TabEnabled(0) = False
                        ssTabQA.TabEnabled(2) = False
                        ssTabQA.TabEnabled(3) = False
                    Case 2
                        picFSQA(2).Enabled = True
                        picNavigateFSQA(1).Visible = False
                        picEditFSQA(1).Visible = True
                        ssTabQA.TabEnabled(0) = False
                        ssTabQA.TabEnabled(1) = False
                        ssTabQA.TabEnabled(3) = False
                    Case 3
                        m_sEstructuraUnidadesNegocio = "M"
                        
                        ssTabQA.TabEnabled(0) = False
                        ssTabQA.TabEnabled(1) = False
                        ssTabQA.TabEnabled(2) = False
    
                        cmdAceptarFSQA(2).Visible = True
                        cmdCancelarFSQA(2).Visible = True
                        cmdModificarFSQA(2).Visible = False
    
                        lstPermisosUndNegUsuRead.Visible = False
                        lstPermisosUndNegUsu.Visible = True
                        
                        If Not tvwUndNegUsuRead.selectedItem Is Nothing Then
                            CargarListaPermisosUnidadesNegocio tvwUndNegUsuRead.selectedItem.key
                        Else
                            Dim i As Integer
                            m_oRecargaPermisosUndNegUsuario = True
                            For i = 0 To lstPermisosUndNegUsu.ListCount - 1
                                lstPermisosUndNegUsu.Selected(i) = False
                            Next
                            lstPermisosUndNegUsu.Enabled = False
                            m_oRecargaPermisosUndNegUsuario = False
                        End If
                         
                        Set m_oPermisosUndNegUsuarioAgregar = oFSGSRaiz.Generar_CUnidadesNegQA
                        Set m_oPermisosUndNegUsuarioEliminar = oFSGSRaiz.Generar_CUnidadesNegQA
                                                                   
                End Select
                
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
                .TabEnabled(7) = False
                .TabEnabled(9) = False
                .TabEnabled(10) = False
                .TabEnabled(11) = False
                .TabEnabled(12) = False
            
            Case 5  'Acceso FSSM
                FrameFSSMTab0.Enabled = True
                FrameFSSMTab1.Enabled = True
                fraArboles.Enabled = True           '??????????
                picNavigateFSSM.Visible = False
                picEditFSSM.Visible = True
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(6) = False
                .TabEnabled(7) = False
                .TabEnabled(9) = False
                .TabEnabled(10) = False
                .TabEnabled(11) = False
                .TabEnabled(12) = False
                m_sModoArbolIPA = "M"
                m_sModoTabEstrOrg = "M"
                PonerArbolIPAVisible
                fraActivos.Enabled = True
                chkNotifDisponible.Enabled = True
            Case 6   'Acceso FSCN
                Select Case iTab2
                    Case 0
                        picFSCN(0).Enabled = True
                        picNavigateFSCN(0).Visible = False
                        picEditFSCN(0).Visible = True
                        SSTabCN.TabEnabled(1) = False
                    Case 1
                        picFSCN(1).Enabled = True
                        picNavigateFSCN(1).Visible = False
                        picEditFSCN(1).Visible = True
                        SSTabCN.TabEnabled(0) = False
                End Select
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(7) = False
                .TabEnabled(9) = False
                .TabEnabled(10) = False
                .TabEnabled(11) = False
                .TabEnabled(12) = False
                
            Case 7 'FSGS Destinos
                PicDestinos.Enabled = True
                picNavigateDest.Visible = False
                PicDestPorDefecto.Enabled = True
                picEditDest.Visible = True
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
                .TabEnabled(9) = False
                .TabEnabled(10) = False
                .TabEnabled(11) = False
                .TabEnabled(12) = False
                
            Case 9 'FSIS
                picEditFSIS.Visible = True
                picNavigateFSIS.Visible = False
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
                .TabEnabled(7) = False
                .TabEnabled(10) = False
                .TabEnabled(11) = False
                .TabEnabled(12) = False
                cmdAceptarFSIS.Visible = True
                cmdCancelarFSIS.Visible = True
                cmdModificarFSIS.Visible = False
                fraFSIS.Enabled = True
            Case 10 'FSIM
                fraFSIM.Enabled = True
                picNavigateFSIM.Visible = False
                picEditFSIM.Visible = True
                cmdAceptarFSIM.Visible = True
                cmdCancelarFSIM.Visible = True
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
                .TabEnabled(7) = False
                .TabEnabled(9) = False
                .TabEnabled(11) = False
                .TabEnabled(12) = False
            Case 11 'FSCM
                fraFSCM.Enabled = True
                picNavigateFSCM.Visible = False
                picEditFSCM.Visible = True
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
                .TabEnabled(7) = False
                .TabEnabled(9) = False
                .TabEnabled(10) = False
                .TabEnabled(12) = False
            Case 12 'FSSOL
                fraFSSOL.Enabled = True
                picNavigateFSSOL.Visible = False
                picEditFSSOL.Visible = True
                .TabEnabled(0) = False
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
                .TabEnabled(7) = False
                .TabEnabled(9) = False
                .TabEnabled(10) = False
                .TabEnabled(11) = False
        End Select
    End With
End Sub

''' <summary>
''' Hace que deje de ser editable el contenido de la pantalla para realizar modificaciones
''' </summary>
''' <param name="iTab1">Indica el tab dentro del primer nivel de tabs</param>
''' <param name="iTab1">Indica el tab dentro del segundo nivel de tabs</param>

Private Sub ModoConsulta(ByVal iTab As Integer, Optional ByVal iTab2 As Integer)
    With sstabUsu
        Select Case iTab
            Case 1  'Acceso FSGS
                Accion = accusucon
                lstvwSegurReadOnly.ListItems.clear
                
                lstvwSegur.ListItems.clear
                lstvwSegurReadOnly.Visible = True
                lstvwSegur.Visible = False
                .TabEnabled(0) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
                picNavigateSegur.Visible = True
                picNavigateSegur.Enabled = True
                picEdit.Visible = False
                picEdit.Enabled = False
            
            Case 2  'Acceso FSWS/Contratos
                picFSWS.Enabled = False
                picNavigateFSWS.Visible = True
                picEditFSWS.Visible = False
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
                
            Case 3  'Acceso FSEP
                picFSEP.Enabled = False
                picNavigateFSEP.Visible = True
                picEditFSEP.Visible = False
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
                
            Case 4  'Acceso FSQA
                Select Case iTab2
                    Case 1 'Restricciones
                        picFSQA(1).Enabled = False
                        picNavigateFSQA(0).Visible = True
                        picEditFSQA(0).Visible = False
                    Case 2  'Permisos
                        picFSQA(2).Enabled = False
                        picNavigateFSQA(1).Visible = True
                        picEditFSQA(1).Visible = False
                    Case 3
                        m_sEstructuraUnidadesNegocio = "C"
                        lstPermisosUndNegUsuRead.clear
                        lstPermisosUndNegUsuRead.Visible = True
                        lstPermisosUndNegUsu.Visible = False
                        
                        If Not tvwUndNegUsuRead.selectedItem Is Nothing Then
                            Dim NodeKeySeleccionado As Integer
                            
                            NodeKeySeleccionado = tvwUndNegUsuRead.selectedItem.Index
                            GenerarEstructuraUnidadesNegocio m_sEstructuraUnidadesNegocio
                            tvwUndNegUsuRead.Nodes(NodeKeySeleccionado).Selected = True
                            CargarListaPermisosUnidadesNegocio tvwUndNegUsuRead.selectedItem.key
                        End If
                        
                        cmdAceptarFSQA(2).Visible = False
                        cmdCancelarFSQA(2).Visible = False
                        cmdModificarFSQA(2).Visible = True
                End Select
                
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
                
                ssTabQA.TabEnabled(0) = True
                ssTabQA.TabEnabled(1) = True
                ssTabQA.TabEnabled(2) = True
                ssTabQA.TabEnabled(3) = True
                
            Case 5  'Acceso FSSM
                'Incidencia 31800.4 2008 12
                'fraArboles.Enabled = False
                fraArboles.Enabled = True
                picEditFSSM.Visible = False
                picNavigateFSSM.Visible = True
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
                m_sModoArbolIPA = "C"
                m_sModoTabEstrOrg = "C"
                PonerArbolIPAVisible
                fraActivos.Enabled = False
                chkNotifDisponible.Enabled = False
            Case 6  'Acceso FSCN
                Select Case iTab2
                    Case 0 'Permisos
                        picFSCN(0).Enabled = False
                        picNavigateFSCN(0).Visible = True
                        picEditFSCN(0).Visible = False
                    Case 1  'Restricciones
                        picFSCN(1).Enabled = False
                        picNavigateFSCN(1).Visible = True
                        picEditFSCN(1).Visible = False
                End Select
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
                SSTabCN.TabEnabled(0) = True
                SSTabCN.TabEnabled(1) = True
            Case 7 'FSGS Destinos
                picNavigateDest.Visible = True
                PicDestPorDefecto.Enabled = False
                picEditDest.Visible = False
                picEditFSEP.Visible = False
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
            Case 9 ' FSIS
                cmdAceptarFSIS.Visible = False
                cmdCancelarFSIS.Visible = False
                picEditFSIS.Visible = False
                cmdModificarFSIS.Visible = True
                picNavigateFSIS.Visible = True
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
                fraFSIS.Enabled = False
            Case 10 'FSIM
                fraFSIM.Enabled = False
                picNavigateFSIM.Visible = True
                picEditFSIM.Visible = False
                cmdAceptarFSIM.Visible = False
                cmdCancelarFSIM.Visible = False
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
            Case 11
                fraFSCM.Enabled = False
                picNavigateFSCM.Visible = True
                picEditFSCM.Visible = False
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
            Case 12 'FSSOL
                fraFSSOL.Enabled = False
                picNavigateFSSOL.Visible = True
                picEditFSSOL.Visible = False
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
                .TabEnabled(7) = True
                .TabEnabled(9) = True
                .TabEnabled(10) = True
                .TabEnabled(11) = True
                .TabEnabled(12) = True
        End Select
    End With
End Sub
Private Sub DesactivarTabulacionUsu()
    picNavigateUsu.Enabled = False
    tvwUsu.TabStop = False
End Sub
Private Sub ActivarTabulacionUsu()
    picNavigateUsu.Enabled = True
    tvwUsu.TabStop = True
End Sub

Public Function DevolverCod(ByVal nodx As node) As Variant

    DevolverCod = Mid(nodx.key, 4, Len(nodx.key))

End Function

''' Revisado por: sra. Fecha: 08/11/2011
''' <summary>
''' Carga las cadenas del modulo
''' </summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0,1</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_USUARIOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    caption = Ador(0).Value
    sstabUsu.TabCaption(0) = Ador(0).Value
    Ador.MoveNext
    sstabUsu.TabCaption(1) = Ador(0).Value
    Ador.MoveNext
    Ador.MoveNext
    Ador.MoveNext
    fraAcciones.caption = Ador(0).Value
    Ador.MoveNext
    Ador.MoveNext
    Ador.MoveNext
    cmdGS(3).caption = Ador(0).Value '8 &Aceptar
    cmdEP(1).caption = Ador(0).Value
    cmdWS(1).caption = Ador(0).Value
    cmdAceptarFSQA(0).caption = Ador(0).Value
    cmdAceptarFSQA(1).caption = Ador(0).Value
    cmdAceptarFSQA(2).caption = Ador(0).Value
    cmdAceptarFSSM.caption = Ador(0).Value
    cmdAceptarDest.caption = Ador(0).Value
    cmdAceptarCN(0).caption = Ador(0).Value
    cmdAceptarCN(1).caption = Ador(0).Value
    cmdAceptarFSIS.caption = Ador(0).Value
    cmdAceptarFSIM.caption = Ador(0).Value
    cmdAceptarFSSOL.caption = Ador(0).Value
    cmdCM(1).caption = Ador(0).Value
    Ador.MoveNext
    cmdUsu(0).caption = Ador(0).Value
    Ador.MoveNext
    cmdWS(2).caption = Ador(0).Value
    cmdEP(2).caption = Ador(0).Value
    cmdWS(2).caption = Ador(0).Value
    cmdCancelarFSQA(0).caption = Ador(0).Value
    cmdCancelarFSQA(1).caption = Ador(0).Value
    cmdCancelarFSQA(2).caption = Ador(0).Value
    cmdCancelarFSSM.caption = Ador(0).Value
    cmdCancelarDest.caption = Ador(0).Value
    cmdCancelarCN(0).caption = Ador(0).Value
    cmdCancelarCN(1).caption = Ador(0).Value
    cmdCancelarFSIS.caption = Ador(0).Value
    cmdCancelarFSIM.caption = Ador(0).Value
    cmdCancelarFSSOL.caption = Ador(0).Value
    cmdCM(2).caption = Ador(0).Value
    Ador.MoveNext
    cmdUsu(3).caption = Ador(0).Value
    Ador.MoveNext
    cmdUsu(2).caption = Ador(0).Value
    Ador.MoveNext
    cmdUsu(4).caption = Ador(0).Value
    Ador.MoveNext
    cmdGS(2).caption = Ador(0).Value
    cmdUsu(6).caption = Ador(0).Value
    Ador.MoveNext
    cmdGS(0).caption = Ador(0).Value
    cmdUsu(1).caption = Ador(0).Value
    cmdEP(0).caption = Ador(0).Value
    cmdWS(0).caption = Ador(0).Value
    cmdModificarFSQA(0).caption = Ador(0).Value
    cmdModificarFSQA(1).caption = Ador(0).Value
    cmdModificarFSQA(2).caption = Ador(0).Value
    cmdModificarFSSM.caption = Ador(0).Value
    cmdModifDest.caption = Ador(0).Value
    cmdModificarCN(0).caption = Ador(0).Value
    cmdModificarCN(1).caption = Ador(0).Value
    cmdModificarFSIS.caption = Ador(0).Value
    cmdModificarFSIM.caption = Ador(0).Value
    cmdModificarFSSOL.caption = Ador(0).Value
    cmdCM(0).caption = Ador(0).Value
    Ador.MoveNext
    cmdGS(1).caption = Ador(0).Value
    cmdUsu(5).caption = Ador(0).Value
    Ador.MoveNext
    m_sIdiPersonalizado = Ador(0).Value '17
    Ador.MoveNext
    m_sIdiA�aUsu = Ador(0).Value '23
    Ador.MoveNext
    m_sIdiUsuarioCod = Ador(0).Value
    Ador.MoveNext
    m_sIdiCodigo = Ador(0).Value
    Ador.MoveNext
    m_sIdiEliminar = Ador(0).Value
    Ador.MoveNext
    m_sIdiContinuar = Ador(0).Value
    Ador.MoveNext
    m_sIdiModifUsu = Ador(0).Value
    Ador.MoveNext
    m_sIdiDatUsu = Ador(0).Value
    Ador.MoveNext
    m_sIdiDetalle = Ador(0).Value
    Ador.MoveNext
    m_sIdiUsuarios = Ador(0).Value
    Ador.MoveNext
    m_sIdiUsuario = Ador(0).Value
    Ador.MoveNext
    Ador.MoveNext 'Saltamos el 36
    sstabUsu.TabCaption(3) = Ador(0).Value  '37 Acceso a aprovisionamiento FSEP
    Ador.MoveNext
    chkSOL(17).caption = Ador(0).Value  '38 Restringir los conceptos presupuestarios a la unidad organizativa del usuario
    Ador.MoveNext
    chkSOL(1).caption = Ador(0).Value  '39 Restringir a personas de la unidad organizativa del usuario
    Ador.MoveNext
    lblDefecto.caption = Ador(0).Value  ' 40 Valores por defecto para conceptos presupuestarios
    Ador.MoveNext
    chkSOL(18).caption = Ador(0).Value '41 Activar sustituci�n temporal
    Ador.MoveNext
    lblSustituir.caption = Ador(0).Value   '42 Sustituir por:
    Ador.MoveNext
    lblDestinoDef.caption = Ador(0).Value  '43 Destino por defecto para pedidos:
    Ador.MoveNext
    'chkSelecDest.caption = ador(0).Value   '44 Permitir seleccionar destinos de otras unidades organizativas
    Ador.MoveNext
    chkFSEP(0).caption = Ador(0).Value  '45  Permitir realizar pedidos libres
    Ador.MoveNext
    chkFSEP(12).caption = Ador(0).Value  '46  Restringir los pedidos a la empresa del usuario
    Ador.MoveNext
    m_sAcceso = Ador(0).Value '47 Acceso:
    
    Ador.MoveNext
    chkSOL(2).caption = Ador(0).Value  '48 Restringir a personas del departamento del usuario
    Ador.MoveNext
    chkSOL(3).caption = Ador(0).Value '49 Restringir los proveedores al material del usuario
    Ador.MoveNext
    chkSOL(4).caption = Ador(0).Value '50 Restringir el material asignado al usuario
    Ador.MoveNext
    chkSOL(5).caption = Ador(0).Value '51 Restringir los destinos a los de la unidad organizativa del usuario
    Ador.MoveNext
    chkFSEP(11).caption = Ador(0).Value '52 Permitir modificar los precios en la emisi�n de pedidos
    Ador.MoveNext
    sstabUsu.TabCaption(4) = Ador(0).Value '53 Acceso a calidad FSQA
    Ador.MoveNext
    ssTabQA.TabCaption(0) = Ador(0).Value  '54 Variables de calidad
    Ador.MoveNext
    ssTabQA.TabCaption(1) = Ador(0).Value  '55 Restricciones
    SSTabCN.TabCaption(1) = Ador(0).Value
    Ador.MoveNext
    fraQA(0).caption = Ador(0).Value  '56 Restringir a los proveedores
    Ador.MoveNext
    fraQA(1).caption = Ador(0).Value  '57 Restringir la consulta de los certificados
    Ador.MoveNext
    fraQA(2).caption = Ador(0).Value  '58 Restringir la consulta de no conformidades
    Ador.MoveNext
    chkQA(0).caption = Ador(0).Value   ' 59 A aquellos que tengan al menos un contacto del departamento de calidad
    Ador.MoveNext
    chkQA(1).caption = Ador(0).Value   ' 60 Al material del usuario
    Ador.MoveNext
    chkQA(2).caption = Ador(0).Value   ' 61 Al equipo del usuario
    Ador.MoveNext
    chkQA(3).caption = Ador(0).Value  '62 A los solicitados por el usuario
    Ador.MoveNext
    chkQA(4).caption = Ador(0).Value  '63 A los solicitados en su unidad organizativa
    Ador.MoveNext
    chkQA(5).caption = Ador(0).Value  '64 A los solicitados en su departamento
    Ador.MoveNext
    chkQA(6).caption = Ador(0).Value  '65 A las realizadas por el usuario
    Ador.MoveNext
    chkQA(7).caption = Ador(0).Value  '66 A las realizadas en su unidad organizativa
    Ador.MoveNext
    chkQA(8).caption = Ador(0).Value  '67 A las realizadas en su departamento
    Ador.MoveNext
    sdbgVariables.Columns("DEN").caption = Ador(0).Value '68 Variable
    Ador.MoveNext
    sdbgVariables.Columns("CONS").caption = Ador(0).Value '69 Consulta
    m_sConsulta = Ador(0).Value '69 Consulta
    Ador.MoveNext
    sdbgVariables.Columns("MOD").caption = Ador(0).Value '70 Modificaci�n
    m_sModificacion = Ador(0).Value '70 Modificaci�n
    Ador.MoveNext
    ssTabQA.TabCaption(2) = Ador(0).Value '71 Permisos
    SSTabCN.TabCaption(0) = Ador(0).Value
    Ador.MoveNext
    fraQA(6).caption = Ador(0).Value '72 Mantenimientos
    Ador.MoveNext
    chkQA(21).caption = Ador(0).Value '73 Permitir gestionar el Mantenimiento de materiales de QA
   
    Ador.MoveNext
    Me.fraQA(5) = Ador(0).Value '74 Par�metros generales
    Ador.MoveNext
    Me.fraQA(7) = Ador(0).Value '75 Certificados
    Ador.MoveNext
    
    Me.fraQA(8) = Ador(0).Value '76 No conformidades
    
    Ador.MoveNext
    Me.chkQA(13).caption = Ador(0).Value '77 Permitir modificar el modo de traspaso de proveedores al panel de calidad
    Ador.MoveNext
    Me.chkQA(15).caption = Ador(0).Value '78 Permitir modificar los par�metros generales de notificaciones por cambios en materiales de GS
    Ador.MoveNext
    chkQA(22).caption = Ador(0).Value  ' 79 Permitir gestionar el Mantenimiento de proveedores de QA
    Ador.MoveNext
    'Me.chkNoConfModifDatosOtrosUsu.Caption = Ador(0).Value '80 Permitir modificar los datos de las no conformidades de otros usuarios
    Ador.MoveNext
    Me.chkQA(28).caption = Ador(0).Value  '81 Permitir modificar los datos de los certificados de otros usuarios
    Ador.MoveNext
    Me.chkQA(26).caption = Ador(0).Value  '82  Permitir modificar el per�odo de antelaci�n para considerar los certificados pr�ximos a caducar
    Ador.MoveNext
    Me.chkQA(27).caption = Ador(0).Value   '83 Permitir publicar/despublicar certificados de otros usuarios
    Ador.MoveNext
    Me.fraQA(3).caption = Ador(0).Value '84 Restringir la seleccion de revisores de no conformidades
    Ador.MoveNext
    Me.chkQA(9).caption = Ador(0).Value  '85 A su unidad organizativa
    'Tambi�n copiamos el texto para las restricciones de selecci�n de notificados internos
    Me.chkQA(11).caption = Ador(0).Value
    Ador.MoveNext
    Me.chkQA(10).caption = Ador(0).Value   '86 A su unidad organizativa
    'Tambi�n copiamos el texto para las restricciones de selecci�n de notificados internos
    Me.chkQA(12).caption = Ador(0).Value
    
    Ador.MoveNext
    Me.chkQA(34).caption = Ador(0).Value '87 Permitir el cierre de no conformidades
   
    Ador.MoveNext
    Me.chkQA(32).caption = Ador(0).Value '88 Permitir cerrar noconformidades aunque las acciones no est�n finalizadas
    Ador.MoveNext
    Me.chkQA(33).caption = Ador(0).Value '89 Permitir reabrir noconformidades cerradas
    Ador.MoveNext
    Me.chkQA(14).caption = Ador(0).Value '90
    Ador.MoveNext
    Me.chkSOL(6).caption = Ador(0).Value '91 Restringir las solicitudes referenciadas a las abiertas en la unidad organizativa del usuario
    Ador.MoveNext
    Me.chkSOL(7).caption = Ador(0).Value '92 Permitir visualizar los precios de las �ltimas adjudicaciones para los art�culos

    'EP
    Ador.MoveNext
    sdbgDestinos.Columns("COD").caption = Ador(0).Value '93 C�digo
    Ador.MoveNext
    sdbgDestinos.Columns("DEN").caption = Ador(0).Value '94 Denominaci�n
    Ador.MoveNext
    sdbgDestinos.Columns("DIR").caption = Ador(0).Value '95 Direcci�n
    Ador.MoveNext
    sdbgDestinos.Columns("POB").caption = Ador(0).Value '96 Poblaci�n
    Ador.MoveNext
    sdbgDestinos.Columns("CP").caption = Ador(0).Value  '97 C�digo postal
    'SM
    Ador.MoveNext
    sstabUsu.TabCaption(5) = Ador(0).Value  '98 Acceso a control de gasto FSSM
    Ador.MoveNext
    sstabEstrorg.TabCaption(0) = Ador(0).Value  '99 Imputaci�n de pedidos de aprovisionamiento
    Ador.MoveNext
    sstabEstrorg.TabCaption(1) = Ador(0).Value  '100 Control de importes
    Ador.MoveNext
    'sstabSM.TabCaption(0) = Ador(0).Value       '101 Selecci�n de Centros de Coste para:
    Ador.MoveNext
    sstabUsu.TabCaption(7) = Ador(0).Value
    lblDestinoUp.caption = Ador(0).Value & ":"        'Destino para pedidos
    Ador.MoveNext
    Me.chkQA(29).caption = Ador(0).Value    '103 Permitir solicitar certificado
    Ador.MoveNext
    Me.chkQA(30).caption = Ador(0).Value    '104 Permitir Renovar certificado
    Ador.MoveNext
    Me.chkQA(31).caption = Ador(0).Value    '105 Permitir Enviar certificado
 
    Ador.MoveNext
    ssTabQA.TabCaption(3) = Ador(0).Value
    
    Ador.MoveNext
    chkQA(24).caption = Ador(0).Value
    
    Ador.MoveNext
    chkQA(23).caption = Ador(0).Value
    
    Ador.MoveNext
    fraPermisosUndNegUsuRead.caption = Ador(0).Value
    
    Dim i As Integer
    i = 0
    
    'Cargamos los tipos de permiso posibles. Desde 110 a 117.
    'M�S ADELANTE ESTA LA DE "MODIFICAR NO CONFORMIDADES EMITIDAS POR OTRO USUARIO"
    '=======1) Emitir no conformidades
    '=======2) Consultar no conformidades
    '=======3) Reabrir no conformidades
    '=======4) Mantener objetivos y suelos
    '=======5) Consultar objetivos y suelos
    '=======6) Asignar proveedores a la unidad de negocio
    '=======7) Modificar puntuaciones en el panel de calidad
    '=======8) Consultar puntuaciones en el panel de calidad
    For i = 0 To 7
        Ador.MoveNext
        With lstPermisosUndNegUsu
            .AddItem Ador(0).Value, i
        End With
    Next
    
    Ador.MoveNext
    sstabSM.TabCaption(1) = Ador(0).Value       '118 Activos
    Ador.MoveNext
    chkConsultaActivos.caption = Ador(0).Value  '119 Consulta activos
    Ador.MoveNext
    chkAltaActivos.caption = Ador(0).Value      '120 Alta de nuevos activos
    Ador.MoveNext
    chkModificarActivos.caption = Ador(0).Value '121 Modificacion de activos
    Ador.MoveNext
    chkEliminarActivos.caption = Ador(0).Value  '122 Eliminacion de activos
    Ador.MoveNext
    chkFSEP(18).caption = Ador(0).Value '123 Permitir anular recepciones
    
    'En una tarea posterior se a�ade un nuevo permiso.
    'Es el que se comenta anteriormente que falta para el listado
    Ador.MoveNext
    lstPermisosUndNegUsu.AddItem Ador(0).Value, 2
    
    'Se a�ade un nuevo permiso. Permitir modificar el periodo de validez de los certificados expirados
    Ador.MoveNext
    chkQA(16).caption = Ador(0).Value
    
    'A�adimos nuevo frame para las restricciones sobre notificados internos
    Ador.MoveNext
    fraQA(4).caption = Ador(0).Value
    Ador.MoveNext
    
    Me.chkQA(17).caption = Ador(0).Value 'DPermitir modificar el par�metro general de notificaci�n al proveedor por cambio de calificaci�n total
    Ador.MoveNext
    Me.chkQA(18).caption = Ador(0).Value 'DPermitir modificar los par�metros generales de notificaci�n de proximidad de fechas de fin de una no conformidad
    Ador.MoveNext
    Ador.MoveNext
    Ador.MoveNext
    Me.chkFSEP(14).caption = Ador(0).Value
    Ador.MoveNext
    
    m_sAlta = Ador(0).Value '132 Alta
    Ador.MoveNext
    m_sPresup = Ador(0).Value '133 Presupuestaci�n
    Ador.MoveNext
    chkQA(19).caption = Ador(0).Value 'DPermiso modificar el nivel o niveles de unidades de negocio a mostrar por defecto al proveedor en sus calificaciones
    
    Ador.MoveNext
    Me.chkSOL(0).caption = Ador(0).Value '135 Permitir realizar traslados
    
    Ador.MoveNext
    Me.sstabUsu.TabCaption(6) = Ador(0).Value '136 Acceso a la red de colaboraci�n FSRC
    
    Ador.MoveNext
    Me.fraCNPermis(0).caption = Ador(0).Value '137 Organizaci�n de la red de colaboraci�n
    
    Ador.MoveNext
    Me.chkCNPOrgRedCat.caption = Ador(0).Value '138 Permitir organizar los contenidos de la red. Gesti�n de categor�as
    
    Ador.MoveNext
    Me.chkCNPOrgRedGr.caption = Ador(0).Value '139 Permitir organizar los miembros de la red. Gesti�n de grupos corporativos
    
    Ador.MoveNext
    Me.fraCNPermis(1).caption = Ador(0).Value '140 Uso de la red de colaboraci�n
    
    Ador.MoveNext
    Me.chkCNPUsoRedMens.caption = Ador(0).Value '141 Permitir crear temas, noticias y eventos
    
    Ador.MoveNext
    Me.chkCNPUsoRedMensUrg.caption = Ador(0).Value '142 Permitir crear mensajes urgentes
    
    Ador.MoveNext
    Me.fraCNRestric(0).caption = Ador(0).Value '143 Env�o de mensajes. Estructura organizativa
    
    Ador.MoveNext
    Me.chkCNREstrOrgUON.caption = Ador(0).Value '144 Restringir el env�o a miembros de la red de colaboraci�n de la unidad organizativa del usuario
    
    Ador.MoveNext
    Me.chkCNREstrOrgDep.caption = Ador(0).Value '145 Restringir el env�o a miembros de la red de colaboraci�n del departamento del usuario
       
    Ador.MoveNext
    Me.fraCNRestric(1).caption = Ador(0).Value '146 Env�o de mensajes. Proveedores
    
    Ador.MoveNext
    Me.chkCNRProvMat.caption = Ador(0).Value '147 Restringir el env�o de mensajes a proveedores del material del usuario
    
    Ador.MoveNext
    Me.chkCNRProEqp.caption = Ador(0).Value '148 Restringir el env�o de mensajes a proveedores del equipo de compras del usuario
    
    Ador.MoveNext
    Me.chkCNRProvCon.caption = Ador(0).Value '149 Restringir el env�o de mensajes a proveedores con contacto de calidad
    
    Ador.MoveNext
    Me.fraCNRestric(2).caption = Ador(0).Value '150 Env�o de mensajes. Estructura de compras
    
    Ador.MoveNext
    Me.chkCNREqpUsu.caption = Ador(0).Value '151 Restringir el env�o a miembros del equipo de compras del usuario
    
    Ador.MoveNext
    Me.fraCNRestric(3).caption = Ador(0).Value '152 Organizaci�n de la red
    
    Ador.MoveNext
    Me.optOrgRed1.caption = Ador(0).Value '153 Restringir la gesti�n de categor�as y/o grupos de la red a las categor�as y/o grupos de la red dados de alta por el usuario
    
    Ador.MoveNext
    Me.optOrgRed2.caption = Ador(0).Value '154 Restringir la gesti�n de categor�as y/o grupos de la red a las categor�as y/o grupos dados de alta en el departamento del usuario
    
    Ador.MoveNext
    Me.optOrgRed3.caption = Ador(0).Value '155 Restringir la gesti�n de categor�as y/o grupos de la red a las categor�as y/o grupos dados de alta en la unidad organizativa del usuario
    
    Ador.MoveNext
    Me.optOrgRed4.caption = Ador(0).Value '156 Sin restricciones en la gesti�n de categor�as y/o grupos
    
    Ador.MoveNext
    Me.chkFSEP(15).caption = Ador(0).Value '157 Permitir cerrar pedidos
    
    Ador.MoveNext
    Me.chkFSEP(16).caption = Ador(0).Value '158 Permitir reabrir pedidos
    
    Ador.MoveNext
    Me.chkFSEP(17).caption = Ador(0).Value '159 Permitir recepcionar pedidos de los centros de coste del usuario
    
    Ador.MoveNext
    sstabSM.TabCaption(0) = Ador(0).Value '160 Control presupuestario
    
    Ador.MoveNext
    m_sAsignarPartidasGestor = Ador(0).Value '161 Asignar partidas al gestor
    Ador.MoveNext
    
    Me.chkFSEP(19).caption = Ador(0).Value '162 Permitir activar el bloqueo de albaranes para facturaci�n
    Ador.MoveNext
    
    sstabEstrorg.TabCaption(2) = Ador(0).Value  '163 Gesti�n de facturas
    
    Ador.MoveNext
    Ador.MoveNext 'saltamos el 164
    Ador.MoveNext 'saltamos el 165
    Ador.MoveNext 'saltamos el 166
    sdbgDestinos.Columns("EMISION").caption = Ador(0).Value '166 Emisi�n
    Ador.MoveNext
    sdbgDestinos.Columns("RECEPCION").caption = Ador(0).Value '167 Recepci�n
    Ador.MoveNext
    Me.chkRelanzar.caption = Ador(0).Value
    Ador.MoveNext
    sstabUsu.TabCaption(9) = Ador(0).Value

    Ador.MoveNext
    Me.chkFSEP(13).caption = Ador(0).Value '171 Restringir la emisi�n de pedidos a artículos de la unidad organizativa del usuario
    
    Ador.MoveNext
    Me.chkSOL(9).caption = Ador(0).Value '172 Permitir crear escenarios
    
    Ador.MoveNext
    Me.chkSOL(10).caption = Ador(0).Value '173 Permitir crear escenarios para otros usuarios

    Ador.MoveNext
    Me.chkSOL(11).caption = Ador(0).Value '174 Permitir editar escenarios compartidos por otros usuarios
    
    Ador.MoveNext
    Me.chkSOL(12).caption = Ador(0).Value '175 Ocultar escenario por defecto

    Ador.MoveNext
    Me.chkSOL(13).caption = Ador(0).Value '176 Restringir la creaci�n de escenarios para otros usuaruios a usuarios de la unidad organizativa del usuario
    Ador.MoveNext
    ''177 Restringir la creaci�n de escenarios para otros usuaruios a usuarios de las unidades organizativas del perfil (NO UTILIZADO)
    Ador.MoveNext
    Me.chkSOL(14).caption = Ador(0).Value '178 Restringir la creaci�n de escenarios para otros usuaruios a usuarios del departamento del usuario
    Ador.MoveNext
    Me.chkSOL(15).caption = Ador(0).Value '179 Restringir la edici�n de escenarios compartidos no creados por el usuario a escenarios de unidad organizativa del usuario
    Ador.MoveNext
    ''180 Restringir la edici�n de escenarios compartidos no creados por el usuario a escenarios de las unidades organizativas del perfil del usuario(NO UTILIZADO)
    Ador.MoveNext
    Me.chkSOL(16).caption = Ador(0).Value '181 Restringir la edici�n de escenarios compartidos no creados por el usuario a escenarios del departamento del usuario
    Ador.MoveNext
    Me.chkQA(20).caption = Ador(0).Value 'Permitir modificar el par�metro de selecci�n de proveedores por material en el panel de proveedores
    Ador.MoveNext
    Ador.MoveNext
    chkFSEP(1).caption = Ador(0).Value
    Ador.MoveNext
    chkFSEP(2).caption = Ador(0).Value
    Ador.MoveNext
    chkFSEP(3).caption = Ador(0).Value
    Ador.MoveNext
    chkFSEP(4).caption = Ador(0).Value
    Ador.MoveNext
    chkFSEP(5).caption = Ador(0).Value
    Ador.MoveNext
    chkFSEP(6).caption = Ador(0).Value
    Ador.MoveNext
    chkFSEP(7).caption = Ador(0).Value
    Ador.MoveNext
    chkFSEP(8).caption = Ador(0).Value
    Ador.MoveNext
    chkFSEP(9).caption = Ador(0).Value
    Ador.MoveNext
    chkFSEP(10).caption = Ador(0).Value
    Ador.MoveNext
    chkNotifDisponible.caption = Ador(0).Value
    For i = 9 To 20
        Ador.MoveNext
        m_sPermisosEscalacion(i - 9) = Ador(0).Value
    Next i
    Ador.MoveNext
    chkQA(25).caption = Ador(0).Value
    Ador.MoveNext
    lstPermisosUndNegUsu.AddItem Ador(0).Value
    Ador.MoveNext
    lstPermisosUndNegUsu.AddItem Ador(0).Value  'Recibir aviso de conversi�n autom�tica de proveedor en real en la unidad de negocio
    Ador.MoveNext
    chkFSIM(0).caption = Ador(0).Value      '210 Alta de facturas
    Ador.MoveNext
    chkFSIM(1).caption = Ador(0).Value      '211 Seguimiento de facturas
    Ador.MoveNext
    chkFSIM(2).caption = Ador(0).Value     '212 Visor de autofacturas
    Ador.MoveNext
    chkFSIM(3).caption = Ador(0).Value     '213 Permitir ver las notificaciones de IM
    Ador.MoveNext
    sstabUsu.TabCaption(10) = Ador(0).Value     '214 Acceso a FSIM
    Ador.MoveNext
    chkSOL(8).caption = Ador(0).Value     '215 Restringir empresa de la solicitud a la del usuario
    Ador.MoveNext
    Me.chkFSEP(20).caption = Ador(0).Value '216 Restringir el centro de aprovisionamiento al usuario
    Ador.MoveNext
    sstabUsu.TabCaption(12) = Ador(0).Value  '217 Seguridad de solicitudes
    Ador.MoveNext
    chkSOL(19).caption = Ador(0).Value   '218 Permitir crear ecenarios para el proveedor
    Ador.MoveNext
    chkPM.caption = Ador(0).Value   '219 Permitir ver las notificaciones de PM
    Ador.MoveNext
    chkCM.caption = Ador(0).Value   '220 Permitir ver las notificaciones de CM
    Ador.MoveNext
    Me.chkFSEP(21).caption = Ador(0).Value 'Permitir anular pedidos
    Ador.MoveNext
    Me.chkFSEP(22).caption = Ador(0).Value 'Permitir ver pedidos borrados en el seguimiento de pedidos
    Ador.MoveNext
    Me.chkFSEP(23).caption = Ador(0).Value 'Permitir borrar l�neas de pedido
    Ador.MoveNext
    Me.chkFSEP(24).caption = Ador(0).Value 'Permitir deshacer el borrado de l�neas de pedido
    Ador.MoveNext
    Me.chkFSEP(25).caption = Ador(0).Value 'Permitir borrar pedidos
    Ador.MoveNext
    Me.chkFSEP(26).caption = Ador(0).Value 'Permitir deshacer el borrado de pedidos
    Ador.MoveNext
    Me.chkQA(35).caption = Ador(0).Value
    Ador.MoveNext
    Me.chkFSEP(27).caption = Ador(0).Value 'Permitir modificar el porcentaje de desv�o de la recepci�n en la emisi�n de pedidos
    Ador.MoveNext
    Me.chkQA(36).caption = Ador(0).Value '229 A los de proveedores vinculados a unidades de negocio del usuario
    
    Ador.Close
    End If

    Set Ador = Nothing

    lblLitPres1.caption = gParametrosGenerales.gsSingPres1 & ":"
    lblLitPres2.caption = gParametrosGenerales.gsSingPres2 & ":"
    lblLitPres3.caption = gParametrosGenerales.gsSingPres3 & ":"
    lblLitPres4.caption = gParametrosGenerales.gsSingPres4 & ":"
End Sub

Private Sub sdbcDestCod_Change()
    If Not m_bRespetarComboDest Then
        m_bRespetarComboDest = True
        sdbcDestDen.Text = ""
        
        m_bRespetarComboDest = False
        
        m_bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcDestCod_Click()
    If Not sdbcDestCod.DroppedDown Then
        sdbcDestCod = ""
        sdbcDestDen = ""
    End If
End Sub

Private Sub sdbcDestCod_CloseUp()
    If sdbcDestCod.Value = "..." Then
        sdbcDestCod.Text = ""
        Exit Sub
    End If
    
    If sdbcDestCod.Value = "" Then Exit Sub
    
    m_bRespetarComboDest = True
    sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
    sdbcDestCod.Text = sdbcDestCod.Columns(0).Text
    m_bRespetarComboDest = False
    
    m_bCargarComboDesde = False
End Sub

Private Sub sdbcDestCod_DropDown()
    Dim oDest As CDestino
    Dim teserror As TipoErrorSummit
    
    Screen.MousePointer = vbHourglass
    
    sdbcDestCod.RemoveAll
        
    Set g_oIBaseDatos = g_oUsuario
    teserror = g_oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        Set g_oUsuario = Nothing
        Set g_oIBaseDatos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
          
    '31800.4 Ahora solo carga los que esten checked en el grid
    m_oDestinos.CargarDestinosUSUDEST g_oUsuario
     
    If m_oDestinos Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    For Each oDest In m_oDestinos
        sdbcDestCod.AddItem oDest.Cod & Chr(m_lSeparador) & oDest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oDest.dir & Chr(m_lSeparador) & oDest.POB & Chr(m_lSeparador) & oDest.cP
    Next
    
    'a estos destinos tenemos que a�adirles los que estan chequeados en el grid
    sdbgDestinos.Update
    If HayLineasChequeadas() Then
        Dim vbm As Variant
        Dim i As Integer
        Dim ador1 As Ador.Recordset
        
        sdbcDestCod.RemoveAll
        sdbgDestinos.MoveFirst
        For i = 0 To sdbgDestinos.Rows - 1
            vbm = sdbgDestinos.AddItemBookmark(i)
            If sdbgDestinos.Columns("EMISION").CellValue(vbm) = True Then
                'MsgBox CStr(sdbgDestinos.Columns("COD").CellValue(vbm))
                Set ador1 = g_oUsuario.DevuelveDatosDestino(CStr(sdbgDestinos.Columns("COD").CellValue(vbm)))
                If Not ador1 Is Nothing And Not ador1.EOF Then
                    sdbcDestCod.AddItem ador1.Fields("Cod").Value & Chr(m_lSeparador) & "" & ador1.Fields("DEN_CAMPO").Value & Chr(m_lSeparador) & "" & ador1.Fields("Dir").Value & Chr(m_lSeparador) & "" & ador1.Fields("POB").Value & Chr(m_lSeparador) & "" & ador1.Fields("cP").Value
                End If
            End If
        Next i
        
        ador1.Close
    End If
    
    sdbcDestCod.SelStart = 0
    sdbcDestCod.SelLength = Len(sdbcDestCod.Text)
    sdbcDestCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub


''' <summary>Indica si hay l�neas chequeadas para emisi�n</summary>
''' <remarks>Llamada desde: sdbcDestDen_DropDown</remarks>

Private Function HayLineasChequeadas() As Boolean
    Dim vbm As Variant
    Dim i As Integer
    
    HayLineasChequeadas = False
    
    With sdbgDestinos
        .MoveFirst
        For i = 0 To .Rows - 1
            vbm = .AddItemBookmark(i)
            If .Columns("EMISION").CellValue(vbm) Then
                HayLineasChequeadas = True
                Exit For
            End If
        Next i
    End With
End Function

Private Sub sdbcDestCod_InitColumnProps()
    sdbcDestCod.DataFieldList = "Column 0"
    sdbcDestCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcDestCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcDestCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcDestCod.Rows - 1
            bm = sdbcDestCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcDestCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcDestCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcDestCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
Dim ADORs As Ador.Recordset

    If sdbcDestCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el destino
    Set ADORs = m_oDestinos.DevolverTodosLosDestinos(Trim(sdbcDestCod.Columns(0).Text), , True)

    If ADORs Is Nothing Then
        bExiste = False
    Else
        bExiste = Not (ADORs.RecordCount = 0)
    End If
    
    If bExiste Then bExiste = (UCase(ADORs(0).Value) = UCase(Trim(sdbcDestCod.Text)))
    
    If Not bExiste Then
        sdbcDestCod.Text = ""
        
    Else
        m_bRespetarComboDest = True
        sdbcDestDen.Text = ADORs(1).Value
        
        sdbcDestCod.Columns(0).Value = sdbcDestCod.Text
        sdbcDestCod.Columns(1).Value = sdbcDestDen.Text
        
        m_bRespetarComboDest = False
        
    End If

    If Not ADORs Is Nothing Then
        ADORs.Close
        Set ADORs = Nothing
    End If
    
    m_bCargarComboDesde = False
End Sub


Private Sub sdbcDestDen_Change()
    If Not m_bRespetarComboDest Then
    
        m_bRespetarComboDest = True
        sdbcDestCod.Text = ""
        m_bRespetarComboDest = False
        
        m_bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcDestDen_Click()
    If Not sdbcDestDen.DroppedDown Then
        sdbcDestCod = ""
        sdbcDestDen = ""
    End If
End Sub


Private Sub sdbcDestDen_CloseUp()
    If sdbcDestDen.Value = "..." Then
        sdbcDestDen.Text = ""
        Exit Sub
    End If
    
    If sdbcDestDen.Value = "" Then Exit Sub
    
    m_bRespetarComboDest = True
    sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
    sdbcDestCod.Text = sdbcDestDen.Columns(1).Text
    m_bRespetarComboDest = False
    
    m_bCargarComboDesde = False
End Sub


Private Sub sdbcDestDen_DropDown()
    Dim oDest As CDestino
    Dim teserror As TipoErrorSummit
    
    Screen.MousePointer = vbHourglass
    
    sdbcDestDen.RemoveAll

    Set g_oIBaseDatos = g_oUsuario
    teserror = g_oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        Set g_oUsuario = Nothing
        Set g_oIBaseDatos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    m_oDestinos.CargarDestinosUSUDEST g_oUsuario
    
    If m_oDestinos Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    For Each oDest In m_oDestinos
        sdbcDestDen.AddItem oDest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oDest.Cod & Chr(m_lSeparador) & oDest.dir & Chr(m_lSeparador) & oDest.POB & Chr(m_lSeparador) & oDest.cP
    Next

    'a estos destinos tenemos que a�adirles los que estan chequeados en el grid
    sdbgDestinos.Update
    
    If HayLineasChequeadas() Then
        Dim vbm As Variant
        Dim i As Integer
        Dim ador1 As Ador.Recordset
        
        sdbcDestDen.RemoveAll
        sdbgDestinos.MoveFirst
           
        For i = 0 To sdbgDestinos.Rows - 1
            vbm = sdbgDestinos.AddItemBookmark(i)
            If sdbgDestinos.Columns("EMISION").CellValue(vbm) Then
                Set ador1 = g_oUsuario.DevuelveDatosDestino(CStr(sdbgDestinos.Columns("COD").CellValue(vbm)))
                If Not ador1 Is Nothing And Not ador1.EOF Then
                    sdbcDestDen.AddItem "" & ador1.Fields("DEN_CAMPO").Value & Chr(m_lSeparador) & ador1.Fields("Cod").Value & Chr(m_lSeparador) & "" & ador1.Fields("Dir").Value & Chr(m_lSeparador) & "" & ador1.Fields("POB").Value & Chr(m_lSeparador) & "" & ador1.Fields("cP").Value
                End If
            End If
        Next i
    End If
    sdbcDestDen.SelStart = 0
    sdbcDestDen.SelLength = Len(sdbcDestDen.Text)
    sdbcDestDen.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcDestDen_InitColumnProps()
    sdbcDestDen.DataFieldList = "Column 0"
    sdbcDestDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcDestDen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcDestDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcDestDen.Rows - 1
            bm = sdbcDestDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcDestDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcDestDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub CargarFSDirectos()
    Dim oIdi As CIdioma
    
    'Muestra las propiedades correspondientes al acceso al FSEP
    
    Set g_oUsuario.Destino = Nothing
    Set g_oUsuario.Destino = oFSGSRaiz.Generar_CDestino
    
    Set g_oUsuario.Destino.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdi In m_oIdiomas
        g_oUsuario.Destino.Denominaciones.Add oIdi.Cod, ""
    Next
    
    If g_oUsuario.ExpandirDestino Then
        If Not g_oUsuario.Destino Is Nothing Then
            m_bRespetarComboDest = True
            sdbcDestCod.Value = g_oUsuario.Destino.Cod
            sdbcDestCod.Columns(0).Value = g_oUsuario.Destino.Cod
            sdbcDestCod.Text = g_oUsuario.Destino.Cod
            sdbcDestDen.Text = g_oUsuario.Destino.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sdbcDestDen.ToolTipText = g_oUsuario.Destino.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            m_bRespetarComboDest = False
        Else
            m_bRespetarComboDest = True
            sdbcDestCod.Value = ""
            sdbcDestCod.Columns(0).Value = ""
            sdbcDestCod.Text = ""
            sdbcDestDen.Text = ""
            sdbcDestDen.ToolTipText = ""
            m_bRespetarComboDest = False
        End If
    Else
        m_bRespetarComboDest = True
        sdbcDestCod.Value = ""
        sdbcDestCod.Columns(0).Value = ""
        sdbcDestCod.Text = ""
        sdbcDestDen.Text = ""
        sdbcDestDen.ToolTipText = ""
        m_bRespetarComboDest = False
    End If
    
    CargarDestinos
End Sub

''' <summary>
''' Carga el �rbol de imputaci�n de partidas del SM
''' </summary>
''' <remarks>Llamada desde: cmdCancelarFSSM_Click; sstabEstrorg_Click; sstabUsu_Click</remarks>
Private Sub CargarFSSM_ArbolImputacion()
    PonerArbolIPAVisible
End Sub


'Revisado por: SRA (08/11/2011)
'Descripcion: carga los par�metros de PM
'Llamada desde:Form_Load
'Tiempo ejecucion:0,2seg.
Private Sub CargarFSWS()
    chkPM.Value = BooleanToSQLBinary(g_oUsuario.FSPMPermitirVisorNotificaciones)
End Sub

Private Sub CargarFSCM()
    chkCM.Value = BooleanToSQLBinary(g_oUsuario.FSCMPermitirVisorNotificaciones)
End Sub

Public Sub cargarFSIS()
    Me.chkRelanzar.Value = BooleanToSQLBinary(g_oUsuario.INTPermisoRelanzarIntegracion)
End Sub

Private Sub CargarFSIM()
    chkFSIM(0).Value = BooleanToSQLBinary(g_oUsuario.FSIMAltaFacturas)
    chkFSIM(1).Value = BooleanToSQLBinary(g_oUsuario.FSIMSeguimientoFacturas)
    chkFSIM(2).Value = BooleanToSQLBinary(g_oUsuario.FSIMVisorAutofacturas)
    chkFSIM(3).Value = BooleanToSQLBinary(g_oUsuario.FSIMNotificacionesIM)
    picEditFSIM.Visible = False
End Sub

Private Sub CargarFSSOL()
    Dim oPer As CPersona
    Dim iNivel As Integer
    Dim lIdPresup As Long
    Dim oPresupuestos1 As CPresProyectosNivel1
    Dim oPresupuestos2 As CPresContablesNivel1
    Dim oPresupuestos3 As CPresConceptos3Nivel1
    Dim oPresupuestos4 As CPresConceptos4Nivel1
    Dim sLblUO As String
    Dim Ador As ADODB.Recordset

    'muestra las propiedades correspondientes al acceso al FSWS:
    chkSOL(17).Value = BooleanToSQLBinary(g_oUsuario.FSWSPresUO)
    chkSOL(0).Value = BooleanToSQLBinary(g_oUsuario.FSPMTraslados)
    chkSOL(1).Value = BooleanToSQLBinary(g_oUsuario.FSWSTrasladosUO)
    chkSOL(2).Value = BooleanToSQLBinary(g_oUsuario.FSWSTrasladosDEP)
    chkSOL(3).Value = BooleanToSQLBinary(g_oUsuario.FSWSRestrProvMat)
    chkSOL(4).Value = BooleanToSQLBinary(g_oUsuario.FSWSRestrMatUsu)
    chkSOL(5).Value = BooleanToSQLBinary(g_oUsuario.FSWSRestrDestUO)
    chkSOL(6).Value = BooleanToSQLBinary(g_oUsuario.FSWSRestrRefAbiertasUO)
    chkSOL(7).Value = BooleanToSQLBinary(g_oUsuario.FSWSVisualizarUltADJ)
    chkSOL(8).Value = BooleanToSQLBinary(g_oUsuario.FSPMRestringirEmpresasUSU)
    chkSOL(9).Value = BooleanToSQLBinary(g_oUsuario.FSPMCrearEscenarios)
    chkSOL(10).Value = BooleanToSQLBinary(g_oUsuario.FSPMCrearEscenariosParaOtros)
    chkSOL(19).Value = BooleanToSQLBinary(g_oUsuario.FSPMPermitirCrearEscenariosProveedores)
    chkSOL(11).Value = BooleanToSQLBinary(g_oUsuario.FSPMEditarEscenariosCompartidos)
    chkSOL(12).Value = BooleanToSQLBinary(g_oUsuario.FSPMOcultarEscenarioDefecto)
    'RESTRICCIONES ESCENARIOS
    chkSOL(13).Value = BooleanToSQLBinary(g_oUsuario.FSPMRestringirCrearEscenariosUsuariosUONUsu)
    chkSOL(14).Value = BooleanToSQLBinary(g_oUsuario.FSPMRestringirCrearEscenariosUsuariosDEPUsu)
    chkSOL(15).Value = BooleanToSQLBinary(g_oUsuario.FSPMRestringirEdicionEscenariosUONUsu)
    chkSOL(16).Value = BooleanToSQLBinary(g_oUsuario.FSPMRestringirEdicionEscenariosDEPUsu)
    
    'Carga el sustituto temporal:
    If Not IsNull(g_oUsuario.FSWSSustitucion) Then
        chkSOL(18).Value = vbChecked
        
        Set oPer = oFSGSRaiz.Generar_CPersona
        oPer.Cod = g_oUsuario.FSWSSustitucion
        oPer.CargarTodosLosDatos True
        txtSustituto.Text = oPer.nombre & " " & oPer.Apellidos & " (" & oPer.DenDep & ")"
        txtSustituto.Tag = oPer.Cod
        Set oPer = Nothing
        
    Else
        chkSOL(18).Value = vbUnchecked
        txtSustituto.Text = ""
        txtSustituto.Tag = ""
    End If
    
    'Carga los presupuestos por defecto:
    If Not IsNull(g_oUsuario.FSWSPres1) Then
        iNivel = Left(CStr(g_oUsuario.FSWSPres1), 1)
        lIdPresup = Mid(CStr(g_oUsuario.FSWSPres1), 2)
                    
        Set oPresupuestos1 = oFSGSRaiz.generar_CPresProyectosNivel1
        Select Case iNivel
            Case 1
                Set Ador = oPresupuestos1.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
            Case 2
                Set Ador = oPresupuestos1.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
            Case 3
                Set Ador = oPresupuestos1.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
            Case 4
                Set Ador = oPresupuestos1.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
        End Select
        Set oPresupuestos1 = Nothing
        If Not Ador Is Nothing Then
            sLblUO = ""
            If Ador("UON1").Value <> "" Then
                sLblUO = "(" & Ador("UON1").Value
                If Ador("UON2").Value <> "" Then
                    sLblUO = sLblUO & " - " & Ador("UON2").Value
                    If Ador("UON3").Value <> "" Then
                        sLblUO = sLblUO & " - " & Ador("UON3").Value & ") "
                    Else
                        sLblUO = sLblUO & ") "
                    End If
                Else
                    sLblUO = sLblUO & ") "
                End If
            End If
            If Not IsNull(Ador("PRES4").Value) Then
                lblPres(0).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("PRES4").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES3").Value) Then
                lblPres(0).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES2").Value) Then
                lblPres(0).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES1").Value) Then
                lblPres(0).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("DEN").Value
            Else
                lblPres(0).caption = sLblUO
            End If
            lblPres(0).Tag = NullToStr(g_oUsuario.FSWSPres1)
            Ador.Close
            Set Ador = Nothing
        Else
            lblPres(0).caption = ""
            lblPres(0).Tag = ""
        End If
    Else
        lblPres(0).caption = ""
        lblPres(0).Tag = ""
    End If
    
    If Not IsNull(g_oUsuario.FSWSPres2) Then
        iNivel = Left(CStr(g_oUsuario.FSWSPres2), 1)
        lIdPresup = Mid(CStr(g_oUsuario.FSWSPres2), 2)
        
        Set oPresupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel1
        Select Case iNivel
            Case 1
                Set Ador = oPresupuestos2.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
            Case 2
                Set Ador = oPresupuestos2.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
            Case 3
                Set Ador = oPresupuestos2.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
            Case 4
                Set Ador = oPresupuestos2.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
        End Select
        Set oPresupuestos2 = Nothing
        If Not Ador Is Nothing Then
            sLblUO = ""
            If Ador("UON1").Value <> "" Then
                sLblUO = "(" & Ador("UON1").Value
                If Ador("UON2").Value <> "" Then
                    sLblUO = sLblUO & " - " & Ador("UON2").Value
                    If Ador("UON3").Value <> "" Then
                        sLblUO = sLblUO & " - " & Ador("UON3").Value & ") "
                    Else
                        sLblUO = sLblUO & ") "
                    End If
                Else
                    sLblUO = sLblUO & ") "
                End If
            End If
            If Not IsNull(Ador("PRES4").Value) Then
                lblPres(1).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("PRES4").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES3").Value) Then
                lblPres(1).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES2").Value) Then
                lblPres(1).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES1").Value) Then
                lblPres(1).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("DEN").Value
            Else
                lblPres(1).caption = sLblUO
            End If
            lblPres(1).Tag = g_oUsuario.FSWSPres2
            Ador.Close
            Set Ador = Nothing
        Else
            lblPres(1).caption = ""
            lblPres(1).Tag = ""
        End If
        
    Else
        lblPres(1).caption = ""
        lblPres(1).Tag = ""
    End If
    
    If Not IsNull(g_oUsuario.FSWSPres3) Then
        iNivel = Left(CStr(g_oUsuario.FSWSPres3), 1)
        lIdPresup = Mid(CStr(g_oUsuario.FSWSPres3), 2)
                                
        Set oPresupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel1
        Select Case iNivel
            Case 1
                Set Ador = oPresupuestos3.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
            Case 2
                Set Ador = oPresupuestos3.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
            Case 3
                Set Ador = oPresupuestos3.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
            Case 4
                Set Ador = oPresupuestos3.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
        End Select
        Set oPresupuestos3 = Nothing
        If Not Ador Is Nothing Then
            sLblUO = ""
            If Ador("UON1").Value <> "" Then
                sLblUO = "(" & Ador("UON1").Value
                If Ador("UON2").Value <> "" Then
                    sLblUO = sLblUO & " - " & Ador("UON2").Value
                    If Ador("UON3").Value <> "" Then
                        sLblUO = sLblUO & " - " & Ador("UON3").Value & ") "
                    Else
                        sLblUO = sLblUO & ") "
                    End If
                Else
                    sLblUO = sLblUO & ") "
                End If
            End If
            If Not IsNull(Ador("PRES4").Value) Then
                lblPres(2).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("PRES4").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES3").Value) Then
                lblPres(2).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES2").Value) Then
                lblPres(2).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES1").Value) Then
                lblPres(2).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("DEN").Value
            Else
                lblPres(2).caption = sLblUO
            End If
            lblPres(2).Tag = g_oUsuario.FSWSPres3
            Ador.Close
            Set Ador = Nothing
        Else
            lblPres(2).caption = ""
            lblPres(2).Tag = ""
        End If
        
    Else
        lblPres(2).caption = ""
        lblPres(2).Tag = ""
    End If
    
    If Not IsNull(g_oUsuario.FSWSPres4) Then
        iNivel = Left(CStr(g_oUsuario.FSWSPres4), 1)
        lIdPresup = Mid(CStr(g_oUsuario.FSWSPres4), 2)
                                
        Set oPresupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel1
        Select Case iNivel
            Case 1
                Set Ador = oPresupuestos4.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
            Case 2
                Set Ador = oPresupuestos4.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
            Case 3
                Set Ador = oPresupuestos4.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
            Case 4
                Set Ador = oPresupuestos4.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
        End Select
        Set oPresupuestos4 = Nothing
        
        If Not Ador Is Nothing Then
            sLblUO = ""
            If Ador("UON1").Value <> "" Then
                sLblUO = "(" & Ador("UON1").Value
                If Ador("UON2").Value <> "" Then
                    sLblUO = sLblUO & " - " & Ador("UON2").Value
                    If Ador("UON3").Value <> "" Then
                        sLblUO = sLblUO & " - " & Ador("UON3").Value & ") "
                    Else
                        sLblUO = sLblUO & ") "
                    End If
                Else
                    sLblUO = sLblUO & ") "
                End If
            End If
            If Not IsNull(Ador("PRES4").Value) Then
                lblPres(3).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("PRES4").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES3").Value) Then
                lblPres(3).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("PRES3").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES2").Value) Then
                lblPres(3).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("PRES2").Value & " - " & Ador("DEN").Value
            ElseIf Not IsNull(Ador("PRES1").Value) Then
                lblPres(3).caption = sLblUO & Ador("PRES1").Value & " - " & Ador("DEN").Value
            Else
                lblPres(3).caption = sLblUO
            End If
            lblPres(3).Tag = g_oUsuario.FSWSPres4
            Ador.Close
            Set Ador = Nothing
        Else
            lblPres(3).caption = ""
            lblPres(3).Tag = ""
        End If
    Else
        lblPres(3).caption = ""
        lblPres(3).Tag = ""
    End If
End Sub

Public Sub MostrarPresSeleccionado12(ByVal iTipo As Integer)
    Dim sLblUO As String
    Dim sPresup1 As String
    Dim sPresup2 As String
    Dim sPresup3 As String
    Dim sPresup4 As String
    Dim sUON1Pres As String
    Dim sUON2Pres As String
    Dim sUON3Pres As String
    Dim vId As Long
    
    sUON1Pres = frmSELPresAnuUON.g_sUON1
    sUON2Pres = frmSELPresAnuUON.g_sUON2
    sUON3Pres = frmSELPresAnuUON.g_sUON3

    sLblUO = ""
    If sUON1Pres <> "" Then
        sLblUO = "(" & sUON1Pres
        If sUON2Pres <> "" Then
            sLblUO = sLblUO & " - " & sUON2Pres
            If sUON3Pres <> "" Then
                sLblUO = sLblUO & " - " & sUON3Pres & ") "
            Else
                sLblUO = sLblUO & ") "
            End If
        Else
            sLblUO = sLblUO & ") "
        End If
    End If
    
    sPresup1 = frmSELPresAnuUON.g_sPRES1
    sPresup2 = frmSELPresAnuUON.g_sPRES2
    sPresup3 = frmSELPresAnuUON.g_sPRES3
    sPresup4 = frmSELPresAnuUON.g_sPRES4

    If sPresup4 <> "" Then
        vId = frmSELPresAnuUON.g_vIdPRES4
        lblPres(iTipo - 1).caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresAnuUON.g_sDenPres
        lblPres(iTipo - 1).Tag = 4 & vId
        
    ElseIf sPresup3 <> "" Then
        vId = frmSELPresAnuUON.g_vIdPRES3
        lblPres(iTipo - 1).caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresAnuUON.g_sDenPres
        lblPres(iTipo - 1).Tag = 3 & vId
        
    ElseIf sPresup2 <> "" Then
        vId = frmSELPresAnuUON.g_vIdPRES2
        lblPres(iTipo - 1).caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresAnuUON.g_sDenPres
        lblPres(iTipo - 1).Tag = 2 & vId
        
    ElseIf sPresup1 <> "" Then
        vId = frmSELPresAnuUON.g_vIdPRES1
        lblPres(iTipo - 1).caption = sLblUO & sPresup1 & " " & frmSELPresAnuUON.g_sDenPres
        lblPres(iTipo - 1).Tag = 1 & vId
    End If
    
End Sub


Public Sub MostrarPresSeleccionado34(ByVal iTipo As Integer)
    Dim sLblUO As String
    Dim sPresup1 As String
    Dim sPresup2 As String
    Dim sPresup3 As String
    Dim sPresup4 As String
    Dim sUON1Pres As String
    Dim sUON2Pres As String
    Dim sUON3Pres As String
    Dim vId As Variant
    
    sUON1Pres = ""
    sUON2Pres = ""
    sUON3Pres = ""
    sUON1Pres = frmSELPresUO.g_sUON1
    sUON2Pres = frmSELPresUO.g_sUON2
    sUON3Pres = frmSELPresUO.g_sUON3
    
    sLblUO = ""
    If sUON1Pres <> "" Then
        sLblUO = "(" & sUON1Pres
        If sUON2Pres <> "" Then
            sLblUO = sLblUO & " - " & sUON2Pres
            If sUON3Pres <> "" Then
                sLblUO = sLblUO & " - " & sUON3Pres & ") "
            Else
                sLblUO = sLblUO & ") "
            End If
        Else
            sLblUO = sLblUO & ") "
        End If
    End If
    
    sPresup1 = frmSELPresUO.g_sPRES1
    sPresup2 = frmSELPresUO.g_sPRES2
    sPresup3 = frmSELPresUO.g_sPRES3
    sPresup4 = frmSELPresUO.g_sPRES4

    If sPresup4 <> "" Then
        vId = frmSELPresUO.g_vIdPRES4
        lblPres(iTipo - 1).caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " - " & sPresup4 & " " & frmSELPresUO.g_sDenPres
        lblPres(iTipo - 1).Tag = 4 & vId
    ElseIf sPresup3 <> "" Then
        vId = frmSELPresUO.g_vIdPRES3
        lblPres(iTipo - 1).caption = sLblUO & sPresup1 & " - " & sPresup2 & " - " & sPresup3 & " " & frmSELPresUO.g_sDenPres
        lblPres(iTipo - 1).Tag = 3 & vId
    ElseIf sPresup2 <> "" Then
        vId = frmSELPresUO.g_vIdPRES2
        lblPres(iTipo - 1).caption = sLblUO & sPresup1 & " - " & sPresup2 & " " & frmSELPresUO.g_sDenPres
        lblPres(iTipo - 1).Tag = 2 & vId
    ElseIf sPresup1 <> "" Then
        vId = frmSELPresUO.g_vIdPRES1
        lblPres(iTipo - 1).caption = sLblUO & sPresup1 & " " & frmSELPresUO.g_sDenPres
        lblPres(iTipo - 1).Tag = 1 & vId
    Else
       lblPres(iTipo - 1).caption = sLblUO
       lblPres(iTipo - 1).Tag = ""
    End If

End Sub

''' <summary>
''' Muestra en pantalla los permisos correspondientes al QA
''' </summary>
''' <remarks>Llamada desde: cmdCancelarFSQA_Click   sstabUsu_Click; Tiempo m�ximo:0,1</remarks>
''' REVISADO POR ILG (15/11/2011)
Private Sub CargarFSQA()

    Screen.MousePointer = vbHourglass
    
    cmdAceptarFSQA(2).Visible = False
    cmdCancelarFSQA(2).Visible = False
    cmdModificarFSQA(2).Visible = True
   
    CargarVariablesCalidad
    
    'muestra las propiedades correspondientes al acceso al FSQA:
    chkQA.Item(0).Value = BooleanToSQLBinary(g_oUsuario.QARestProvDepCalidad)
    chkQA.Item(1).Value = BooleanToSQLBinary(g_oUsuario.QARestProvMaterial)
    chkQA.Item(2).Value = BooleanToSQLBinary(g_oUsuario.QARestProvEquipo)

    chkQA(3).Value = BooleanToSQLBinary(g_oUsuario.QARestCertifUsu)
    chkQA(4).Value = BooleanToSQLBinary(g_oUsuario.QARestCertifUO)
    chkQA(5).Value = BooleanToSQLBinary(g_oUsuario.QARestCertifDep)
    chkQA(36).Value = BooleanToSQLBinary(g_oUsuario.QARestCertifUnidNegocio)
   
    chkQA(6).Value = BooleanToSQLBinary(g_oUsuario.QARestNoConformUsu)
    chkQA(7).Value = BooleanToSQLBinary(g_oUsuario.QARestNoConformUO)
    chkQA(8).Value = BooleanToSQLBinary(g_oUsuario.QARestNoConformDep)

    chkQA(21).Value = BooleanToSQLBinary(g_oUsuario.QAMantenimientoMat)
    chkQA(25).Value = BooleanToSQLBinary(g_oUsuario.QAMantenimientoProveEsc)
    
    chkQA(22).Value = BooleanToSQLBinary(g_oUsuario.QAMantenimientoProv)
    chkQA(13).Value = BooleanToSQLBinary(g_oUsuario.QAModifProvCalidad)
    chkQA(14).Value = BooleanToSQLBinary(g_oUsuario.QAContactoCertAutom)
    chkQA(15).Value = BooleanToSQLBinary(g_oUsuario.QAModifNotifCambiosMatGS)
    chkQA(17).Value = BooleanToSQLBinary(g_oUsuario.QAModifNotifCambiosCalif)
    chkQA(26) = BooleanToSQLBinary(g_oUsuario.QACertifModifExpirar)
    chkQA(16) = BooleanToSQLBinary(g_oUsuario.QAPeriodoValidezCertificado)
    chkQA(27).Value = BooleanToSQLBinary(g_oUsuario.QACertifPubOtrosUsu)
    chkQA(28).Value = BooleanToSQLBinary(g_oUsuario.QACertifModifOtrosUsu)
    Me.chkQA(23).Value = BooleanToSQLBinary(g_oUsuario.QAMantUnidadesNegocio)
    Me.chkQA(34).Value = BooleanToSQLBinary(g_oUsuario.QARevCierreNoConfor)
    Me.chkQA(9).Value = BooleanToSQLBinary(g_oUsuario.QArestselecRevUO)
    Me.chkQA(10).Value = BooleanToSQLBinary(g_oUsuario.QARestselecRevDepart)
    chkQA(24).Value = BooleanToSQLBinary(g_oUsuario.QAMantenimientoObjetivosySuelos)
    Me.chkQA(18).Value = BooleanToSQLBinary(g_oUsuario.QAModifNotifProximidadNC)
    
    chkQA(11).Value = BooleanToSQLBinary(g_oUsuario.QARestrNotifUO)
    chkQA(12).Value = BooleanToSQLBinary(g_oUsuario.QARestrNotifDEP)
    
    chkQA(19).Value = BooleanToSQLBinary(g_oUsuario.QAModificacionNivelesDefectoUNQA)
    Me.chkQA(20).Value = BooleanToSQLBinary(g_oUsuario.QAPermitirModificarFiltroMaterial) '24008
    Me.fraQA(8).Visible = basParametros.gParametrosGenerales.gbAccesoQANoConformidad
    If basParametros.gParametrosGenerales.gbAccesoQANoConformidad Then
        Me.chkQA(32).Value = BooleanToSQLBinary(g_oUsuario.QACierreNoConf)
        Me.chkQA(33).Value = BooleanToSQLBinary(g_oUsuario.QAReabrirNoConf)
    End If
    
    chkQA(29).Value = BooleanToSQLBinary(g_oUsuario.QAPermitirSolicitarCertificados)
    chkQA(30).Value = BooleanToSQLBinary(g_oUsuario.QAPermitirRenovarCertificados)
    chkQA(31).Value = BooleanToSQLBinary(g_oUsuario.QAPermitirEnviarCertificados)
    chkQA(35).Value = BooleanToSQLBinary(g_oUsuario.QAPermitirVisorNotificaciones)
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Muestra en pantalla los permisos correspondientes al FSCN</summary>
''' <remarks>Llamada desde: cmdCancelarCN_Click   sstabUsu_Click; Tiempo m�ximo:0,1</remarks>
''' <revisado>JVS 26/01/2012</revisado>

Private Sub CargarFSCN()
    Screen.MousePointer = vbHourglass
    
    'muestra las propiedades correspondientes al acceso al FSCN:
    Me.chkCNPOrgRedCat.Value = BooleanToSQLBinary(g_oUsuario.CNPermitirGestionCategorias)
    Me.chkCNPOrgRedGr.Value = BooleanToSQLBinary(g_oUsuario.CNPermitirGestionGrupos)
    Me.chkCNPUsoRedMens.Value = BooleanToSQLBinary(g_oUsuario.CNPermitirCrearMens)
    Me.chkCNPUsoRedMensUrg.Value = BooleanToSQLBinary(g_oUsuario.CNPermitirCrearMensUrgentes)
    Me.chkCNREstrOrgUON.Value = BooleanToSQLBinary(g_oUsuario.CNRestrEnvioUON)
    Me.chkCNREstrOrgDep.Value = BooleanToSQLBinary(g_oUsuario.CNRestrEnvioDep)
    Me.chkCNRProvMat.Value = BooleanToSQLBinary(g_oUsuario.CNRestrEnvioProveedMat)
    Me.chkCNRProEqp.Value = BooleanToSQLBinary(g_oUsuario.CNRestrEnvioProveedEqp)
    Me.chkCNRProvCon.Value = BooleanToSQLBinary(g_oUsuario.CNRestrEnvioProveedCon)
    Me.chkCNREqpUsu.Value = BooleanToSQLBinary(g_oUsuario.CNRestrEnvioEqpUsu)
    Me.optOrgRed1.Value = g_oUsuario.CNRestrOrganizarRedUsu
    Me.optOrgRed2.Value = g_oUsuario.CNRestrOrganizarRedDep
    Me.optOrgRed3.Value = g_oUsuario.CNRestrOrganizarRedUO
    Me.optOrgRed4.Value = (Not g_oUsuario.CNRestrOrganizarRedUsu And Not g_oUsuario.CNRestrOrganizarRedDep And Not g_oUsuario.CNRestrOrganizarRedUO)
                        
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarVariablesCalidad()
'''MPG(28/04/2009)
''' <summary>
''' Carga las variables de calidad del usuario
''' </summary>
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde=Propio formulario; Tiempo m�ximo=0,1seg</remarks>
    Dim oVarsCalidad As CVariablesCalidad
    Dim oVarCal1 As CVariableCalidad
    Dim oVarCal2 As CVariableCalidad
    Dim oVarCal3 As CVariableCalidad
    Dim oVarCal4 As CVariableCalidad
    Dim oVarCal5 As CVariableCalidad

    sdbgVariables.RemoveAll
    
    'Cargo dos veces lo mismo porque en una voy a tener el original y en la otra las modificaciones
    'para poder comparar en todo momento con lo que hay en BD sin tener que leer de BD.
   Set oVarsCalidad = oFSGSRaiz.Generar_CVariablesCalidad
    If gParametrosGenerales.gbPymes Then
        oVarsCalidad.CargarVariables , g_oUsuario.Cod, lIdPyme:=g_oUsuario.Pyme
    Else
        oVarsCalidad.CargarVariables , g_oUsuario.Cod
    End If
    
    For Each oVarCal1 In oVarsCalidad
        sdbgVariables.AddItem oVarCal1.Id & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oVarCal1.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal1.UsuConsultar) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal1.UsuModificar) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        If Not oVarCal1.VariblesCal Is Nothing Then
            For Each oVarCal2 In oVarCal1.VariblesCal
                sdbgVariables.AddItem oVarCal2.Id & Chr(m_lSeparador) & "2" & Chr(m_lSeparador) & "    " & oVarCal2.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal2.UsuConsultar) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal2.UsuModificar) & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                If Not oVarCal2.VariblesCal Is Nothing Then
                    For Each oVarCal3 In oVarCal2.VariblesCal
                        sdbgVariables.AddItem oVarCal3.Id & Chr(m_lSeparador) & "3" & Chr(m_lSeparador) & "          " & oVarCal3.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal3.UsuConsultar) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal3.UsuModificar) & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & oVarCal2.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                        If Not oVarCal3.VariblesCal Is Nothing Then
                            For Each oVarCal4 In oVarCal3.VariblesCal
                                sdbgVariables.AddItem oVarCal4.Id & Chr(m_lSeparador) & "4" & Chr(m_lSeparador) & "               " & oVarCal4.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal4.UsuConsultar) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal4.UsuModificar) & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & oVarCal2.Id & Chr(m_lSeparador) & oVarCal3.Id & Chr(m_lSeparador) & ""
                                If Not oVarCal4.VariblesCal Is Nothing Then
                                    For Each oVarCal5 In oVarCal4.VariblesCal
                                        sdbgVariables.AddItem oVarCal5.Id & Chr(m_lSeparador) & "5" & Chr(m_lSeparador) & "                   " & oVarCal5.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal5.UsuConsultar) & Chr(m_lSeparador) & BooleanToSQLBinary(oVarCal5.UsuModificar) & Chr(m_lSeparador) & oVarCal1.Id & Chr(m_lSeparador) & oVarCal2.Id & Chr(m_lSeparador) & oVarCal3.Id & Chr(m_lSeparador) & oVarCal4.Id
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If
            Next
        End If
    Next
    
    Set oVarsCalidad = Nothing
End Sub

''' <summary>Carga el grid de destinos</summary>
''' <remarks>Llamada desde CargarFSDirectos</remarks>
''' <revision>LTG 22/11/2012</revision>

Private Sub CargarDestinos()
    Dim oDest As CDestino
    Dim oDestinos As CDestinos
      
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    oDestinos.CargarTodosLosDestinos , , , , , , , , , , True, , g_oUsuario.Cod

    sdbgDestinos.RemoveAll
    g_oUsuario.CargarDestinosUOUsuario True
    'En el prototipo salen estos  -----------------------
    'COD        C�digo
    'DEN        Denominaci�n
    'DIR        Direcci�n
    'POB        Poblaci�n
    'CP         CP
    'estos no --------------------------------------------
    'sdbgDestinos.AddItem BooleanToSQLBinary(oVarCal1.PublicarPortal) & chr(m_lSeparador) & oDest.Cod & chr(m_lSeparador) & oDest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & chr(m_lSeparador) & oDest.Dir & chr(m_lSeparador) & oDest.POB & chr(m_lSeparador) & oDest.cP & chr(m_lSeparador) & oDest.Pais & chr(m_lSeparador) & oDest.Provi
    'PROVI      Provincia
    'PAI        Pais
    
    For Each oDest In oDestinos
        sdbgDestinos.AddItem BooleanToSQLBinary(oDest.Emision) & Chr(m_lSeparador) & BooleanToSQLBinary(oDest.Recepcion) & Chr(m_lSeparador) & oDest.Cod & Chr(m_lSeparador) & oDest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oDest.dir & Chr(m_lSeparador) & oDest.POB & Chr(m_lSeparador) & oDest.cP & Chr(m_lSeparador) & oDest.Generico
    Next
    Set oDestinos = Nothing
End Sub

'***********************************************************************|
'*** NUEVA FUNCIONALIDAD PARA PERMISOS FSSM -382 ***********************|
'***********************************************************************|
''' <summary>
''' Genera la estructura de Unidades Organizativas en el treeview recibido
''' Tiene en cuenta el estado de cada nodo (Baja, Centro de Coste, imputaci�n de pedidos de aprovisionamoento) y si es modo consulta o modificaci�n
''' </summary>
''' <param name="tvwArbol">Treeview en el que crear la estructura de UONs</param>
''' <remarks>Llamada desde PonerArbolIPAVisible; Tiempo m�ximo < 1 seg.</remarks>
Private Sub GenerarEstructuraOrgCC(ByRef tvwArbol As TreeView)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUON1 As CUnidadOrgNivel1
Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1

Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUON2 As CUnidadOrgNivel2
Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2

Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
Dim oUON3 As CUnidadOrgNivel3
Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3

Dim oUnidadesorgN4 As CUnidadesOrgNivel4
Dim oUON4 As CUnidadOrgNivel4
Set oUnidadesorgN4 = oFSGSRaiz.Generar_CUnidadesOrgNivel4

' Otras
Dim nodx As node
Dim sKeyImage As String
                 
oUnidadesOrgN1.CargarTodosLosCCNivel1 False, , , g_oUsuario.Cod
oUnidadesOrgN2.CargarTodosLosCCNivel2 False, , , g_oUsuario.Cod
oUnidadesOrgN3.CargarTodosLosCCNivel3 False, , , g_oUsuario.Cod
oUnidadesorgN4.CargarTodosLosCCNivel4 False, , , g_oUsuario.Cod
        
        
   '**************************************************************
    'GENERAMOS LA ESTRUCTURA ARBOREA DE LAS UNIDADES ORGANIZATIVAS
    
    Set nodx = tvwArbol.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True

    For Each oUON1 In oUnidadesOrgN1
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        If Not IsNull(oUON1.idEmpresa) Then
            If oUON1.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            ElseIf oUON1.CC Then
                sKeyImage = "UON1CC"
            Else
                sKeyImage = "SELLO"
            End If
        Else
            If oUON1.BajaLog = 1 Then
                sKeyImage = "UON1BAJA"
            ElseIf oUON1.CC Then
                sKeyImage = "UON1CC"
            Else
                sKeyImage = "UON1"
            End If
        End If
        If oUON1.Nodo_Usuario Then
        End If
        
        Set nodx = tvwArbol.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, sKeyImage)
        
        '
        'Cuando el Nodo_Usuario es TRUE, significa que Puede imputar pedidos de aprovisionamiento.
        If oUON1.Nodo_Usuario Then
            Select Case m_sModoArbolIPA
                'En modo consulta, lo mostramos con una imagen diferente
                Case "C" 'CONSULTA ->Ponemos el OK al NODO
                    nodx.Image = "UON1CCRED"
                    'sKeyImage = "UON1CCRED"
                'En modo Modificacion, lo chequeamos. Para poder chequearlo, hemos tenido que esperar a a�adirlo al treeview
                Case "M" 'MODIFICACION ->CHEQUEAMOS EL NODO
                    nodx.Checked = True
            End Select
        End If
        nodx.Tag = "UON1" & oUON1.Cod & "#" & IIf(oUON1.CC = False, 0, 1)
    Next

    For Each oUON2 In oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        If Not IsNull(oUON2.idEmpresa) Then
            If oUON2.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            ElseIf oUON2.CC Then
                sKeyImage = "UON2CC"
            Else
                sKeyImage = "SELLO"
            End If
        Else
            If oUON2.BajaLog = 1 Then
                sKeyImage = "UON2BAJA"
            ElseIf oUON2.CC Then
                sKeyImage = "UON2CC"
            Else
                sKeyImage = "UON2"
            End If
        End If
        Set nodx = tvwArbol.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, sKeyImage)
        
        'Cuando el Nodo_Usuario es TRUE, significa que Puede imputar pedidos de aprovisionamiento.
        If oUON2.Nodo_Usuario Then
            Select Case m_sModoArbolIPA
                'En modo consulta, lo mostramos con una imagen diferente
                Case "C" 'CONSULTA ->Ponemos el OK al NODO
                    nodx.Image = "UON2CCRED"
                    'En modo Modificacion, lo chequeamos. Para poder chequearlo, hemos tenido que esperar a a�adirlo al treeview
                Case "M" 'MODIFICACION ->CHEQUEAMOS EL NODO
                    nodx.Checked = True
            End Select
        End If
        
        nodx.Tag = "UON2" & oUON2.Cod & "#" & IIf(oUON2.CC = False, 0, 1)
    Next

    For Each oUON3 In oUnidadesOrgN3
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        If Not IsNull(oUON3.idEmpresa) Then
            If oUON3.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            ElseIf oUON3.CC Then
                sKeyImage = "UON3CC"
            Else
                sKeyImage = "SELLO"
            End If

        Else
            If oUON3.BajaLog = 1 Then
                sKeyImage = "UON3BAJA"
            ElseIf oUON3.CC Then
                sKeyImage = "UON3CC"
            Else
                sKeyImage = "UON3"
            End If
        End If
        Set nodx = tvwArbol.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, sKeyImage)
        'Cuando el Nodo_Usuario es TRUE, significa que Puede imputar pedidos de aprovisionamiento.
        If oUON3.Nodo_Usuario Then
            Select Case m_sModoArbolIPA
                'En modo consulta, lo mostramos con una imagen diferente
                Case "C" 'CONSULTA ->Ponemos el OK al NODO
                    nodx.Image = "UON3CCRED"
                'En modo Modificacion, lo chequeamos. Para poder chequearlo, hemos tenido que esperar a a�adirlo al treeview
                Case "M" 'MODIFICACION ->CHEQUEAMOS EL NODO
                    nodx.Checked = True
            End Select
        End If
        nodx.Tag = "UON3" & oUON3.Cod & "#" & IIf(oUON3.CC = False, 0, 1)
    Next

    For Each oUON4 In oUnidadesorgN4
        scod1 = oUON4.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON4.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON4.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON4.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON4.CodUnidadOrgNivel3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON4.CodUnidadOrgNivel3))
        scod4 = scod3 & oUON4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(oUON4.Cod))
        If oUON4.BajaLog = 1 Then
             sKeyImage = "UON4"
        ElseIf oUON4.CC Then
            sKeyImage = "UON4CC"
        Else
            sKeyImage = "UON4"
        End If
        Set nodx = tvwArbol.Nodes.Add("UON3" & scod3, tvwChild, "UON4" & scod4, CStr(oUON4.Cod) & " - " & oUON4.Den, sKeyImage)
        'Cuando el Nodo_Usuario es TRUE, significa que Puede imputar pedidos de aprovisionamiento.
        If oUON4.Nodo_Usuario Then
            Select Case m_sModoArbolIPA
                'En modo consulta, lo mostramos con una imagen diferente
                Case "C" 'CONSULTA ->Ponemos el OK al NODO
                    nodx.Image = "UON4CCRED"
                   'En modo Modificacion, lo chequeamos. Para poder chequearlo, hemos tenido que esperar a a�adirlo al treeview
                Case "M" 'MODIFICACION ->CHEQUEAMOS EL NODO
                    nodx.Checked = True
            End Select
        End If
        nodx.Tag = "UON4" & oUON4.Cod & "#" & IIf(oUON4.CC = False, 0, 1)
    Next
    
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oUON4 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    Set oUnidadesorgN4 = Nothing
    
    
End Sub

Private Sub timer_Timer()
    Timer.Enabled = False
    mNode.Checked = Not mNode.Checked
    Set mNode = Nothing
End Sub


''' <summary>
''' Recorremos el treeview para borrar los nodos que no haya que mostrar
''' y expendir los marcados/chequeados
''' </summary>
''' <param name="tvwArbol">treeview en el que se efectuar�n las operaciones</param>
''' <remarks>Llamada desde PonerArbolIPAVisible; Tiempo m�ximo 1 seg.</remarks>
Private Sub RecorreryBorrarEstructuraOrgCC(ByRef tvwArbol As TreeView)
'UON1, UON2 UON3 O UON4 pueden ser CC
'pero solo habra un CC para cada familia
'Recorremos la familia hasta llegar al CC y ahi parar de recorrer esa familia

Dim nodo As node
Dim i As Integer
Dim bEncontrado As Boolean
Dim intcont As Integer

On Error Resume Next
    PicTapa.Visible = False
    ' recorre todos los nodos
    bEncontrado = False
    With tvwArbol
        For i = 1 To .Nodes.Count   'leo nodo a nodo
            If InStr(.Nodes(i).Tag, "#1") = 0 And .Nodes(i).Children = 0 Then
                Set nodo = .Nodes(i)
                .Nodes.Remove nodo.Index
                bEncontrado = True
                intcont = intcont + 1
            Else 'SI NO BORRAMOS ESE NODO, Expandimos los nodos seleccionados
                Select Case m_sModoArbolIPA
                    Case "C"
                        If Right(.Nodes(i).Image, 3) = "RED" Then
                            .Nodes(i).EnsureVisible
                            .Nodes(i).Expanded = False
                        End If
                    Case "M"
                        If .Nodes(i).Checked Then
                            .Nodes(i).EnsureVisible
                            .Nodes(i).Expanded = False
                        End If
                End Select
            End If
        Next
    End With
    tvwArbol.Refresh
    If bEncontrado Then RecorreryBorrarEstructuraOrgCC tvwArbol
End Sub





'*** ***
'aqui empieza la copia de frmESTrMAT

Private Sub ssImportes_BeforeRowsDeleted(ByVal Rows As UltraGrid.SSSelectedRows, ByVal DisplayPromptMsg As UltraGrid.SSReturnBoolean, ByVal Cancel As UltraGrid.SSReturnBoolean)
    'PARA QUE NO SALTE EL MENSAJE DE CONFIRMACI�N DE BORRADO DE LAS FILAS
    DisplayPromptMsg = False
End Sub

'Descripcion: Evento que salta al dar doble click en la grid
'             Expande la rama cuando pinchamos en el codigo
'Tiempo ejecucion:0seg.
Private Sub ssImportes_DblClick()
    If ssImportes.ActiveRow Is Nothing Then Exit Sub
    If ssImportes.ActiveCell Is Nothing Then Exit Sub
    If ssImportes.ActiveCell.Column.key = "COD" Then
        If ssImportes.ActiveRow.Expanded = True Then
            ssImportes.ActiveRow.CollapseAll
        Else
            ssImportes.ActiveRow.Expanded = True
        End If
    End If
End Sub

Private Sub ssImportes_Error(ByVal ErrorInfo As UltraGrid.SSError)
    If ErrorInfo.Code = 3617 Then
        ErrorInfo.DisplayErrorDialog = False
        oMensajes.FaltaValor
    End If
End Sub

Private Sub ssImportes_InitializeRow(ByVal Context As UltraGrid.Constants_Context, ByVal Row As UltraGrid.SSRow, ByVal ReInitialize As Boolean)
On Error GoTo err_ssImportes_InitializeRow

Dim sUON As String
Dim sUONCC As String

sUON = "UON" & Row.Band.Index
sUONCC = "UON" & Row.Band.Index & "CC"
    
'icono
    
If Row.Band.Index = 0 Then
        Row.Cells("COD").Appearance.Picture = ImageList2.ListImages("UON0").Picture
        Row.Cells("CC").Value = 0

Else


    If Not Row.Cells("C_CONSULTA").Column.Hidden Then
        If Row.Cells(sUONCC).Value = 0 Then
            Row.Cells("COD").Appearance.Picture = ImageList2.ListImages(sUON).Picture
            Row.Cells("CC").Value = 0
        ElseIf Not Row.Cells("TOCADO").Value Then
            Row.Cells("COD").Appearance.Picture = ImageList2.ListImages(sUONCC).Picture
            Row.Cells("C_CONSULTA").Value = GridCheckToBoolean(CStr(Row.Cells("CONSULTA").Value))
            Row.Cells("C_ALTA").Value = GridCheckToBoolean(CStr(Row.Cells("ALTA").Value))
            Row.Cells("C_MODIF").Value = GridCheckToBoolean(CStr(Row.Cells("MODIF").Value))
            Row.Cells("C_PRESUP").Value = GridCheckToBoolean(CStr(Row.Cells("PRESUP").Value))
            Row.Cells("CC").Value = 1
        End If
    End If
        
End If

   
    
    'Fin icono
    
    Exit Sub

err_ssImportes_InitializeRow:
    Select Case err.Number
        Case -2147217887
            Resume Next
    End Select
End Sub

''' <summary>
''' Configura la grid de seguridad del usuario con los centros de coste
''' Tiene la columna del codigo y las respectivas acciones de alta, modif...
''' la columna "CC" nos indica que es un centro de coste
''' </summary>
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde:ssImportes.InitialiteLayout; Tiempo m�ximo:0,2set</remarks>
Private Sub InicializarUltraGrid()
    If Not g_oUsuario Is Nothing Then
        ssImportes.caption = ""
        ssImportes.Font.Name = "Tahoma"
        ssImportes.Font.Size = 8
        
        '* * * UON0 * * *
        With ssImportes.Bands(0)
            .Columns.Add "C_CONSULTA", "C_CONSULTA"
            .Columns.Add "C_ALTA", "C_ALTA"
            .Columns.Add "C_MODIF", "C_MODIF"
            .Columns.Add "C_PRESUP", "C_PRESUP"
            .Columns.Add "TOCADO", "TOCADO"
            .Columns.Add "CC", "CC"
            
            .Columns("UON0COD").Hidden = True
            .Columns("CONSULTA").Hidden = True
            .Columns("ALTA").Hidden = True
            .Columns("MODIF").Hidden = True
            .Columns("PRESUP").Hidden = True
            .Columns("TOCADO").Hidden = True
            .Columns("CC").Hidden = True
            
            .Columns("COD").Header.caption = " "
                 
            With .Columns("C_CONSULTA").Header
                .caption = m_sConsulta
                .Appearance.TextAlign = ssAlignCenter: .Appearance.Backcolor = vbButtonFace
            End With
            
            With .Columns("C_ALTA").Header
                .caption = m_sAlta
                .Appearance.TextAlign = ssAlignCenter: .Appearance.Backcolor = vbButtonFace
            End With
            
            With .Columns("C_MODIF").Header
                .caption = m_sModificacion
                .Appearance.TextAlign = ssAlignCenter: .Appearance.Backcolor = vbButtonFace
            End With
            
            With .Columns("C_PRESUP").Header
                .caption = m_sPresup
                .Appearance.TextAlign = ssAlignCenter: .Appearance.Backcolor = vbButtonFace
            End With
            
            With .Columns("C_CONSULTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_ALTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_MODIF")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_PRESUP")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            .Columns("TOCADO").DataType = ssDataTypeBoolean
            .Columns("CC").DataType = ssDataTypeBoolean
        End With
        
        
        '* * * UON1 * * *
    
        With ssImportes.Bands(1)
            .Columns.Add "C_CONSULTA", "C_CONSULTA"
            .Columns.Add "C_ALTA", "C_ALTA"
            .Columns.Add "C_MODIF", "C_MODIF"
            .Columns.Add "C_PRESUP", "C_PRESUP"
            .Columns.Add "TOCADO", "TOCADO"
            .Columns.Add "CC", "CC"
            
            .ColHeadersVisible = False
    
            .Columns("UON1UON0").Hidden = True
            .Columns("UON1COD").Hidden = True
            .Columns("UON1CC").Hidden = True
            .Columns("CONSULTA").Hidden = True
            .Columns("ALTA").Hidden = True
            .Columns("MODIF").Hidden = True
            .Columns("PRESUP").Hidden = True
            .Columns("TOCADO").Hidden = True
            .Columns("CC").Hidden = True
            
            If Not g_oUsuario.TieneUON1CC() Then
                .Columns("C_CONSULTA").Hidden = True
                .Columns("C_ALTA").Hidden = True
                .Columns("C_MODIF").Hidden = True
                .Columns("C_PRESUP").Hidden = True
            End If
            
            With .Columns("C_CONSULTA").Header
                .caption = m_sConsulta
                .Appearance.TextAlign = ssAlignCenter: .Appearance.Backcolor = vbButtonFace
            End With
            
            With .Columns("C_ALTA").Header
                .caption = m_sAlta
                .Appearance.TextAlign = ssAlignCenter: .Appearance.Backcolor = vbButtonFace
            End With
            
            With .Columns("C_MODIF").Header
                .caption = m_sModificacion
                .Appearance.TextAlign = ssAlignCenter: .Appearance.Backcolor = vbButtonFace
            End With
            
            With .Columns("C_PRESUP").Header
                .caption = m_sPresup '"Presupuestaci�n"
                .Appearance.TextAlign = ssAlignCenter: .Appearance.Backcolor = vbButtonFace
            End With
            
            With .Columns("C_CONSULTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_ALTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_MODIF")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_PRESUP")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With

            .Columns("TOCADO").DataType = ssDataTypeBoolean
            .Columns("CC").DataType = ssDataTypeBoolean
        End With
    
        ' * * * UON2 * * *
    
        With ssImportes.Bands(2)
            .Columns.Add "C_CONSULTA", "C_CONSULTA"
            .Columns.Add "C_ALTA", "C_ALTA"
            .Columns.Add "C_MODIF", "C_MODIF"
            .Columns.Add "C_PRESUP", "C_PRESUP"
            .Columns.Add "TOCADO", "TOCADO"
            .Columns.Add "CC", "CC"
    
            .ColHeadersVisible = False
            .Columns("UON2UON1").Hidden = True
            .Columns("UON2COD").Hidden = True
            .Columns("UON2CC").Hidden = True
            .Columns("CONSULTA").Hidden = True
            .Columns("ALTA").Hidden = True
            .Columns("MODIF").Hidden = True
            .Columns("PRESUP").Hidden = True
            .Columns("TOCADO").Hidden = True
            .Columns("CC").Hidden = True
            
            If Not g_oUsuario.TieneUON2CC() Then
                .Columns("C_CONSULTA").Hidden = True
                .Columns("C_ALTA").Hidden = True
                .Columns("C_MODIF").Hidden = True
                .Columns("C_PRESUP").Hidden = True
            End If
            
            With .Columns("C_CONSULTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_ALTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_MODIF")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_PRESUP")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            .Columns("TOCADO").DataType = ssDataTypeBoolean
            .Columns("CC").DataType = ssDataTypeBoolean
    
        End With
    
        '* * * UON3 * * *
    
        With ssImportes.Bands(3)
            .Columns.Add "C_CONSULTA", "C_CONSULTA"
            .Columns.Add "C_ALTA", "C_ALTA"
            .Columns.Add "C_MODIF", "C_MODIF"
            .Columns.Add "C_PRESUP", "C_PRESUP"
            .Columns.Add "TOCADO", "TOCADO"
            .Columns.Add "CC", "CC"
    
            .ColHeadersVisible = False
            .Columns("UON3UON1").Hidden = True
            .Columns("UON3UON2").Hidden = True
            .Columns("UON3COD").Hidden = True
            .Columns("UON3CC").Hidden = True
            .Columns("CONSULTA").Hidden = True
            .Columns("ALTA").Hidden = True
            .Columns("MODIF").Hidden = True
            .Columns("PRESUP").Hidden = True
            .Columns("TOCADO").Hidden = True
            .Columns("CC").Hidden = True
            
            If Not g_oUsuario.TieneUON3CC() Then
                .Columns("C_CONSULTA").Hidden = True
                .Columns("C_ALTA").Hidden = True
                .Columns("C_MODIF").Hidden = True
                .Columns("C_PRESUP").Hidden = True
            End If
            
            With .Columns("C_CONSULTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_ALTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_MODIF")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_PRESUP")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            .Columns("TOCADO").DataType = ssDataTypeBoolean
            .Columns("CC").DataType = ssDataTypeBoolean
        End With
    
    
        '* * * UON4 * * *
        With ssImportes.Bands(4)
            .Columns.Add "C_CONSULTA", "C_CONSULTA"
            .Columns.Add "C_ALTA", "C_ALTA"
            .Columns.Add "C_MODIF", "C_MODIF"
            .Columns.Add "C_PRESUP", "C_PRESUP"
            .Columns.Add "TOCADO", "TOCADO"
            .Columns.Add "CC", "CC"
    
            .ColHeadersVisible = False
            .Columns("UON4UON1").Hidden = True
            .Columns("UON4UON2").Hidden = True
            .Columns("UON4UON3").Hidden = True
            .Columns("UON4COD").Hidden = True
            .Columns("UON4CC").Hidden = True
            .Columns("CONSULTA").Hidden = True
            .Columns("ALTA").Hidden = True
            .Columns("MODIF").Hidden = True
            .Columns("PRESUP").Hidden = True
            .Columns("TOCADO").Hidden = True
            .Columns("CC").Hidden = True
            
            If Not g_oUsuario.TieneUON4CC() Then
                .Columns("C_CONSULTA").Hidden = True
                .Columns("C_ALTA").Hidden = True
                .Columns("C_MODIF").Hidden = True
                .Columns("C_PRESUP").Hidden = True
            End If
            
            With .Columns("C_CONSULTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_ALTA")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_MODIF")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            With .Columns("C_PRESUP")
                .DataType = ssDataTypeBoolean
                .Style = UltraGrid.ssStyleCheckBox
            End With
            
            .Columns("TOCADO").DataType = ssDataTypeBoolean
            .Columns("CC").DataType = ssDataTypeBoolean
        End With
    
        'Ajusta el tama�o de las columnas de c�digo para que quede lo m�s parecido posible a un treview:
        ssImportes.Bands(0).Columns("COD").Width = 4000
        ssImportes.Bands(1).Columns("COD").Width = 4000
        ssImportes.Bands(2).Columns("COD").Width = 4000
        ssImportes.Bands(3).Columns("COD").Width = 4000
        ssImportes.Bands(4).Columns("COD").Width = 4000
        
        
    
    End If
End Sub

Private Sub cmdAceptarFSSM_Click()
    AceptarFSSM_Tab0
    AceptarFSSM_Tab1
    AceptarPermisosActivos
    ModoConsulta (sstabUsu.Tab)
End Sub

Private Sub AceptarFSSM_Tab0()
Dim i As Integer

' Caso1: antes estaba chequeado y ahora no
With tvwestrorg_Con
    For i = 1 To .Nodes.Count
        If Right(.Nodes(i).Image, 3) = "RED" Then
            EstaOkCaso1 .Nodes(i).key
        End If
    Next
End With

'Caso2: antes No estaba chequeado y ahora si
With tvwestrorg_Modif
    For i = 1 To .Nodes.Count
        If .Nodes(i).Checked Then
            EstaOkCaso2 .Nodes(i).key
        End If
    Next
End With

End Sub
'caso1 antes estaba chequeado y ahora no
Private Sub EstaOkCaso1(psNodeKey As String)
Dim i As Integer
With tvwestrorg_Modif
    For i = 1 To .Nodes.Count
        If .Nodes(i).key = psNodeKey Then
            If Not .Nodes(i).Checked Then
                GrabaCentroImputaci�n .Nodes(i)
            End If
        End If
    Next
End With
End Sub
'caso1 antes estaba chequeado y ahora no
Private Sub EstaOkCaso2(psNodeKey As String)
Dim i As Integer
With tvwestrorg_Con
    For i = 1 To .Nodes.Count
        If .Nodes(i).key = psNodeKey Then
            If Not Right(.Nodes(i).Image, 3) = "RED" Then
                GrabaCentroImputaci�n .Nodes(i)
            End If
        End If
    Next
End With
End Sub

Private Sub GrabaCentroImputaci�n(ByVal node As MSComctlLib.node)
Dim teserror As TipoErrorSummit
Dim oIBaseDatosCC As IBaseDatos
Dim sCadena As String, scadena_padre As String, scadena_padre_padre As String, scadena_padre_padre_padre As String
Dim oUnidadOrgCC As CUnidadOrgCC

    Set oUnidadOrgCC = oFSGSRaiz.Generar_CUnidadOrgCC
    With oUnidadOrgCC
        .USU = g_oUsuario.Cod
        sCadena = IIf(InStr(node.Tag, "#1") <> 0, Replace(node.Tag, "#1", ""), Replace(node.Tag, "#0", ""))
        Select Case Left(sCadena, 4)
            Case "UON1"
                .UON2 = "": .UON3 = "": .UON4 = ""
                'uon1 -> tag "UON1" & scod1
                .UON1 = Replace(sCadena, "UON1", "")
            Case "UON2"
                .UON3 = "": .UON4 = ""
                'uon2 -> tag "UON2" & scod2
                .UON2 = Replace(sCadena, "UON2", "")
                'uon1 -> tagpadre "UON1" & scod1
                 scadena_padre = IIf(InStr(node.Parent.Tag, "#1") <> 0, Replace(node.Parent.Tag, "#1", ""), Replace(node.Parent.Tag, "#0", ""))
                .UON1 = Replace(scadena_padre, "UON1", "")
            Case "UON3"
                .UON4 = ""
                'uon3 -> tag "UON3" & scod3
                .UON3 = Replace(sCadena, "UON3", "")
                'uon2 -> tagpadre "UON2" & scod2
                scadena_padre = IIf(InStr(node.Parent.Tag, "#1") <> 0, Replace(node.Parent.Tag, "#1", ""), Replace(node.Parent.Tag, "#0", ""))
                .UON2 = Replace(scadena_padre, "UON2", "")
                'uon1 -> tagpadrepadre "UON1" & scod1
                scadena_padre_padre = IIf(InStr(node.Parent.Parent.Tag, "#1") <> 0, Replace(node.Parent.Parent.Tag, "#1", ""), Replace(node.Parent.Parent.Tag, "#0", ""))
                .UON1 = Replace(scadena_padre_padre, "UON1", "")

            Case "UON4"
                'uon4 -> tag "UON4" & scod4
                .UON4 = Replace(sCadena, "UON4", "")
                'uon3 -> tagpadre "UON3" & scod3
                scadena_padre = IIf(InStr(node.Parent.Tag, "#1") <> 0, Replace(node.Parent.Tag, "#1", ""), Replace(node.Parent.Tag, "#0", ""))
                .UON3 = Replace(scadena_padre, "UON3", "")
                'uon2 -> tagpadrepadre "UON2" & scod2
                scadena_padre_padre = IIf(InStr(node.Parent.Parent.Tag, "#1") <> 0, Replace(node.Parent.Parent.Tag, "#1", ""), Replace(node.Parent.Parent.Tag, "#0", ""))
                .UON2 = Replace(scadena_padre_padre, "UON2", "")
                'uon1 -> tagpadrepadrepadre "UON1" & scod1
                scadena_padre_padre_padre = IIf(InStr(node.Parent.Parent.Parent.Tag, "#1") <> 0, Replace(node.Parent.Parent.Parent.Tag, "#1", ""), Replace(node.Parent.Parent.Parent.Tag, "#0", ""))
                .UON1 = Replace(scadena_padre_padre_padre, "UON1", "")
        End Select
    End With
    
    
    Set oIBaseDatosCC = oUnidadOrgCC
    '1� comprobamos si existe el registro
    If oIBaseDatosCC.ComprobarExistenciaEnBaseDatos() Then
        'existe --> lo borramos
        teserror = oIBaseDatosCC.EliminarDeBaseDatos
        node.Checked = False
    Else
        'no existe --> lo creamos
        teserror = oIBaseDatosCC.AnyadirABaseDatos
        node.Checked = True
    End If
    
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
    End If
  
    Set oUnidadOrgCC = Nothing
End Sub

''' <summary>
''' Lee los permisos para la gesti�n de activos seleccionados por el usuario y los actualiza en la base de datos
''' </summary>
''' <remarks>Llamada desde: cmdAceptarFSSM_Click</remarks>
Private Sub AceptarPermisosActivos()
    Dim teserror As TipoErrorSummit

    g_oUsuario.ActivoConsulta = chkConsultaActivos.Value
    g_oUsuario.ActivoAlta = chkAltaActivos.Value
    g_oUsuario.ActivoModif = chkModificarActivos.Value
    g_oUsuario.ActivoBaja = chkEliminarActivos.Value
    g_oUsuario.Notif_Disponible = chkNotifDisponible.Value
    teserror.NumError = TESnoerror
    teserror = g_oUsuario.ModificarPermisosFSSM()
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        Exit Sub
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub tvwestrorg_Modif_NodeCheck(ByVal node As MSComctlLib.node)
'Debido a que se puede navegar por el arbol en modo consulta comprobamos si estamos en modo edicion
'si picNavigateFSSM --> modificar esta visible
'si picEditFSSM --> Aceptar cancelar esta visible estamos en modo edicion

If picEditFSSM.Visible Then
    '1� hay que comprobar si se puede chequear
    If InStr(node.Tag, "#1") = 0 Then
        Set mNode = node
        Timer.Enabled = True
        Exit Sub
    End If
Else
    Set mNode = node
    Timer.Enabled = True
    Exit Sub
End If
End Sub

Private Sub AceptarFSSM_Tab1()
    RecorrerBandas
    CargarFSSM_ControlImportes
End Sub

Private Sub HayQueGrabar(ByVal iIndex As Integer, ByVal poRow As SSRow)
    If poRow.Cells("TOCADO").Value And poRow.Cells("CC").Value Then
        GrabarUON iIndex, poRow
    End If
End Sub
Private Sub GrabarUON(ByVal iIndex As Integer, ByVal poRow As SSRow)
Dim teserror As TipoErrorSummit
Dim oUnidadOrgCC As CUnidadOrgCC
Set oUnidadOrgCC = oFSGSRaiz.Generar_CUnidadOrgCC

    With oUnidadOrgCC
        .USU = g_oUsuario.Cod
        Select Case iIndex
            Case 1
                .UON1 = poRow.Cells("UON1COD").Value
                .UON2 = Null: .UON3 = Null: .UON4 = Null

            Case 2
                .UON1 = poRow.Cells("UON2UON1").Value
                .UON2 = poRow.Cells("UON2COD").Value
                .UON3 = Null: .UON4 = Null

            Case 3
                .UON1 = poRow.Cells("UON3UON1").Value
                .UON2 = poRow.Cells("UON3UON2").Value
                .UON3 = poRow.Cells("UON3COD").Value
                .UON4 = Null

            Case 4
                .UON1 = poRow.Cells("UON4UON1").Value
                .UON2 = poRow.Cells("UON4UON2").Value
                .UON3 = poRow.Cells("UON4UON3").Value
                .UON4 = poRow.Cells("UON4COD").Value
        End Select
    End With
    
    '1� comprobamos si existe el registro
    If oUnidadOrgCC.ComprobarExistenciaEnBaseDatosFSSM() Then
        'existe --> lo borramos
        teserror = oUnidadOrgCC.EliminarDeBaseDatosFSSM()
    End If
    
    Dim bResultado1 As Boolean
        
    bResultado1 = Not ((poRow.Cells("C_CONSULTA").Value) = False _
    And ((poRow.Cells("C_MODIF").Value) = False) _
    And ((poRow.Cells("C_ALTA").Value) = False) _
    And ((poRow.Cells("C_PRESUP").Value) = False))
    
    
    If bResultado1 Then
        'lo creamos
        teserror = oUnidadOrgCC.AnyadirABaseDatosFSSM(SSCheckToBoolean(poRow.Cells("C_CONSULTA").Value), SSCheckToBoolean(poRow.Cells("C_MODIF").Value), SSCheckToBoolean(poRow.Cells("C_ALTA").Value), SSCheckToBoolean(poRow.Cells("C_PRESUP").Value))
    End If
    
    
    
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
    End If
  
    Set oUnidadOrgCC = Nothing
End Sub

Private Function SSCheckToBoolean(ByVal Var As Boolean) As Variant
    If Var Then
        SSCheckToBoolean = 1
    Else
        SSCheckToBoolean = Null
    End If
End Function

Private Sub RecorrerBandas()
Dim oRow0 As SSRow
Dim oRow1 As SSRow
Dim oRow2 As SSRow
Dim oRow3 As SSRow
Dim oRow4 As SSRow

    Set oRow0 = ssImportes.GetRow(ssChildRowFirst)   'UON0
    If oRow0.HasChild = True Then
        Set oRow1 = oRow0.GetChild(ssChildRowFirst)  'UON1
        HayQueGrabar 1, oRow1
        
        If oRow1.HasChild = True Then
            Set oRow2 = oRow1.GetChild(ssChildRowFirst)  'UON2
            HayQueGrabar 2, oRow2
            
            If oRow2.HasChild = True Then
                Set oRow3 = oRow2.GetChild(ssChildRowFirst)  'UON3
                HayQueGrabar 3, oRow3
                
                If oRow3.HasChild = True Then
                    Set oRow4 = oRow3.GetChild(ssChildRowFirst)  'UON4
                    HayQueGrabar 4, oRow4
                    
                    Do Until Not oRow4.HasNextSibling
                         Set oRow4 = oRow4.GetSibling(ssSiblingRowNext)
                         HayQueGrabar 4, oRow4
                    Loop
                End If
                
                Do Until Not oRow3.HasNextSibling
                     Set oRow3 = oRow3.GetSibling(ssSiblingRowNext)
                     HayQueGrabar 3, oRow3
                     
                     If oRow3.HasChild = True Then
                        Set oRow4 = oRow3.GetChild(ssChildRowFirst)  'UON4
                        HayQueGrabar 4, oRow4
                        
                        Do Until Not oRow4.HasNextSibling
                             Set oRow4 = oRow4.GetSibling(ssSiblingRowNext)
                             HayQueGrabar 4, oRow4
                        Loop
                    End If
                Loop
            End If
                
            Do Until Not oRow2.HasNextSibling
                Set oRow2 = oRow2.GetSibling(ssSiblingRowNext)
                HayQueGrabar 2, oRow2
                 
                If oRow2.HasChild = True Then
                    Set oRow3 = oRow2.GetChild(ssChildRowFirst)  'UON3
                    HayQueGrabar 3, oRow3
                    
                    If oRow3.HasChild = True Then
                        Set oRow4 = oRow3.GetChild(ssChildRowFirst)  'UON4
                        HayQueGrabar 4, oRow4
                        
                        Do Until Not oRow4.HasNextSibling
                             Set oRow4 = oRow4.GetSibling(ssSiblingRowNext)
                             HayQueGrabar 4, oRow4
                        Loop
                    End If
                    
                    Do Until Not oRow3.HasNextSibling
                        Set oRow3 = oRow3.GetSibling(ssSiblingRowNext)
                        HayQueGrabar 3, oRow3
                                                 
                        If oRow3.HasChild = True Then
                            Set oRow4 = oRow3.GetChild(ssChildRowFirst)  'UON4
                            HayQueGrabar 4, oRow4
                            
                            Do Until Not oRow4.HasNextSibling
                                 Set oRow4 = oRow4.GetSibling(ssSiblingRowNext)
                                 HayQueGrabar 4, oRow4
                            Loop
                        End If
                         
                    Loop
                End If
            Loop
            
        End If
        
        Do Until Not oRow1.HasNextSibling
            Set oRow1 = oRow1.GetSibling(ssSiblingRowNext)
            HayQueGrabar 1, oRow1
             
            If oRow1.HasChild = True Then
                Set oRow2 = oRow1.GetChild(ssChildRowFirst)  'UON2
                HayQueGrabar 2, oRow2
                
                If oRow2.HasChild = True Then
                    Set oRow3 = oRow2.GetChild(ssChildRowFirst)  'UON3
                    HayQueGrabar 3, oRow3
                    
                    If oRow3.HasChild = True Then
                        Set oRow4 = oRow3.GetChild(ssChildRowFirst)  'UON4
                        HayQueGrabar 4, oRow4
                        
                        Do Until Not oRow4.HasNextSibling
                             Set oRow4 = oRow4.GetSibling(ssSiblingRowNext)
                             HayQueGrabar 4, oRow4
                        Loop
                    End If
                    
                    Do Until Not oRow3.HasNextSibling
                        Set oRow3 = oRow3.GetSibling(ssSiblingRowNext)
                        HayQueGrabar 3, oRow3
                        
                        If oRow3.HasChild = True Then
                            Set oRow4 = oRow3.GetChild(ssChildRowFirst)  'UON4
                            HayQueGrabar 4, oRow4
                            
                            Do Until Not oRow4.HasNextSibling
                                 Set oRow4 = oRow4.GetSibling(ssSiblingRowNext)
                                 HayQueGrabar 4, oRow4
                            Loop
                        End If
                    Loop
                End If
                    
                Do Until Not oRow2.HasNextSibling
                    Set oRow2 = oRow2.GetSibling(ssSiblingRowNext)
                    HayQueGrabar 2, oRow2
                    
                    If oRow2.HasChild = True Then
                        Set oRow3 = oRow2.GetChild(ssChildRowFirst)  'UON3
                        HayQueGrabar 3, oRow3
                        
                        If oRow3.HasChild = True Then
                            Set oRow4 = oRow3.GetChild(ssChildRowFirst)  'UON4
                            HayQueGrabar 4, oRow4
                            
                            Do Until Not oRow4.HasNextSibling
                                 Set oRow4 = oRow4.GetSibling(ssSiblingRowNext)
                                 HayQueGrabar 4, oRow4
                            Loop
                        End If
                        
                        Do Until Not oRow3.HasNextSibling
                            Set oRow3 = oRow3.GetSibling(ssSiblingRowNext)
                            HayQueGrabar 3, oRow3
                            
                            If oRow3.HasChild = True Then
                                Set oRow4 = oRow3.GetChild(ssChildRowFirst)  'UON4
                                HayQueGrabar 4, oRow4
                                
                                Do Until Not oRow4.HasNextSibling
                                     Set oRow4 = oRow4.GetSibling(ssSiblingRowNext)
                                     HayQueGrabar 4, oRow4
                                Loop
                            End If
                        Loop
                    End If
                Loop
            End If
        Loop
    End If

End Sub

Private Sub ssImportes_AfterCellUpdate(ByVal Cell As UltraGrid.SSCell)

        If ssImportes.ActiveRow Is Nothing Then Exit Sub
        If Cell.DataChanged = False Then Exit Sub
        Cell.Row.Cells("TOCADO").Value = True

End Sub
Private Sub ssImportes_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
    InicializarUltraGrid
End Sub

''' Revisado por: blp. Fecha: 14/06/2012
''' <summary>
''' Llenamos de contenido el treeview de UONs de la pesta�a "Imputaci�n de pedidos" (sstabEstrorg.Tab(0)) en modo Consulta o Modificaci�n seg�n corresponda
''' Asimismo cargamos los mismo datos en el treeview de "Gesti�n de facturas" (sstabEstrorg.Tab(3))
''' </summary>
''' <remarks>Llamada desde modoEdicion, modoConsulta, CargarFSSM_ArbolImputacion; Tiempo m�ximo < 2 seg.</remarks>
Private Sub PonerArbolIPAVisible()

    'Carga de datos en Imputacion de pedidos
    tvwestrorg_Modif.Nodes.clear
    tvwestrorg_Modif.Visible = False
    Select Case m_sModoArbolIPA
        Case "C"
            tvwestrorg_Con.Nodes.clear
            tvwestrorg_Con.Visible = False
            GenerarEstructuraOrgCC tvwestrorg_Con
            RecorreryBorrarEstructuraOrgCC tvwestrorg_Con
            tvwestrorg_Modif.Visible = False
            tvwestrorg_Con.Visible = True
        Case "M"
            GenerarEstructuraOrgCC tvwestrorg_Modif
            RecorreryBorrarEstructuraOrgCC tvwestrorg_Modif
            tvwestrorg_Con.Visible = False
            tvwestrorg_Modif.Visible = True
    End Select
    
    'Carga de datos en Gestion Facturas
    tvwestrorg_GesFac.Nodes.clear
    tvwestrorg_GesFac.Visible = False
    GenerarEstructuraOrgGestor tvwestrorg_GesFac
    RecorreryBorrarEstructuraOrgGestor tvwestrorg_GesFac
    tvwestrorg_GesFac.Visible = True
End Sub


''' <summary>
''' Carga la estructura de unidades de negocio
''' </summary>
''' <param name="Tipo">Indica si el arbol esta en edicion o no</param>
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde Al cargar el tab de unidades de negocio dentro del tab de QA; Tiempo m�ximo</remarks>

Private Sub GenerarEstructuraUnidadesNegocio(ByVal Tipo As String)

'Tipo:
'Si ES "C" El arbol es de lectura
'Si ES "M" El arbol es de lectura

Dim oNegocios As CUnidadesNegQA
Dim oNegocio As CUnidadNegQA
Dim oNegocio2 As CUnidadNegQA
Dim oNegocio3 As CUnidadNegQA
Dim oNegociosNiv0 As CUnidadesNegQA

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim nodx As MSComctlLib.node
Dim i As Integer

If Tipo = "C" Then


Screen.MousePointer = vbHourglass
    
   
    tvwUndNegUsuRead.Visible = True
        
    tvwUndNegUsuRead.Nodes.clear
   
    Set oNegocios = oFSGSRaiz.Generar_CUnidadesNegQA
    Set oNegociosNiv0 = oFSGSRaiz.Generar_CUnidadesNegQA
    
    If gParametrosGenerales.gbPymes Then
    
        oNegocios.GenerarEstructuraUnidadesNegQA g_oUsuario.Cod, basPublic.gParametrosInstalacion.gIdioma, gParametrosGenerales.gbPymes, IIf(gParametrosGenerales.gbPymes, g_oUsuario.Pyme, 0)
    Else
        
        oNegocios.GenerarEstructuraUnidadesNegQA g_oUsuario.Cod, basPublic.gParametrosInstalacion.gIdioma, False, 0
    
    End If
    
    If gParametrosGenerales.gbPymes Then
            Set oNegociosNiv0 = oFSGSRaiz.Generar_CUnidadesNegQA
            oNegociosNiv0.NodoRaiz g_oUsuario.Cod, basPublic.gParametrosInstalacion.gIdioma, gParametrosGenerales.gbPymes, IIf(gParametrosGenerales.gbPymes, g_oUsuario.Pyme, 0)
            Set nodx = tvwUndNegUsuRead.Nodes.Add(, , "UN0" & oNegociosNiv0.Item(1).Id, oNegociosNiv0.Item(1).Den)
            nodx.Expanded = True
            If oNegociosNiv0.Item(1).Seleccionada Then
                nodx.Checked = True
                nodx.Image = "UON0ASIG"
            Else
                nodx.Checked = False
                nodx.Image = "UON0"
            End If


    Else
            Set oNegociosNiv0 = oFSGSRaiz.Generar_CUnidadesNegQA
            oNegociosNiv0.NodoRaiz g_oUsuario.Cod, basPublic.gParametrosInstalacion.gIdioma, False, 0

            Set nodx = tvwUndNegUsuRead.Nodes.Add(, , "UN0" & oNegociosNiv0.Item(1).Id, oNegociosNiv0.Item(1).Den)
            'nodx.Tag = "Raiz"
            nodx.Tag = "UN0" & CStr(oNegociosNiv0.Item(1).Cod)
            nodx.Expanded = True
            If oNegociosNiv0.Item(1).Seleccionada Then
                nodx.Checked = True
                nodx.Image = "UON0ASIG"
            Else
                nodx.Checked = False
                nodx.Image = "UON0"
            End If

    End If


       
    For Each oNegocio In oNegocios.Unidades
            
            scod1 = CStr(oNegocio.Id)
            Set nodx = tvwUndNegUsuRead.Nodes.Add("UN0" & oNegociosNiv0.Item(1).Id, tvwChild, "UN1" & scod1, CStr(oNegocio.Cod) & " - " & oNegocio.Den)   ', "UN1")
            If oNegocio.Seleccionada Then
                nodx.Image = "UON1ASIG"
            Else
                nodx.Image = "UON1"
            End If
            nodx.Tag = "UN1" & CStr(oNegocio.Cod)
            
            For Each oNegocio2 In oNegocio.UnidadesNegQA.Unidades
                'scod2 = oNegocio2.Cod & Mid$("                         ", 1, 20 - Len(oNegocio2.Cod))
                scod2 = CStr(oNegocio2.Id)
                Set nodx = tvwUndNegUsuRead.Nodes.Add("UN1" & scod1, tvwChild, "UN2" & scod2, CStr(oNegocio2.Cod) & " - " & oNegocio2.Den) ', "UN2")
                nodx.Tag = "UN2" & CStr(oNegocio2.Cod)
                If oNegocio2.Seleccionada Then
                    nodx.Image = "UON2ASIG"
                Else
                    nodx.Image = "UON2"
                End If
                For Each oNegocio3 In oNegocio2.UnidadesNegQA.Unidades
                    'scod3 = oNegocio3.Cod & Mid$("                         ", 1, 20 - Len(oNegocio3.Cod))
                    scod3 = CStr(oNegocio3.Id)
                    
                    Set nodx = tvwUndNegUsuRead.Nodes.Add("UN2" & scod2, tvwChild, "UN3" & scod3, CStr(oNegocio3.Cod) & " - " & oNegocio3.Den) ',  "UN3")
                    nodx.Tag = "UN3" & CStr(oNegocio3.Cod)
                    If oNegocio3.Seleccionada Then
                        nodx.Image = "UON3ASIG"
                    Else
                        nodx.Image = "UON3"
                    End If
                Next
            Next
    Next
        

With tvwUndNegUsuRead
    For i = 2 To .Nodes.Count
        If Right(.Nodes(i).Image, 4) = "ASIG" Then
            .Nodes(i).EnsureVisible
            .Nodes(i).Expanded = False
        End If
    Next
End With
    
    Screen.MousePointer = vbNormal
    
End If


Set oNegocios = Nothing
Set oNegocio = Nothing
Set oNegocio2 = Nothing
Set oNegocio3 = Nothing
Set oNegociosNiv0 = Nothing


End Sub


''' <summary>
''' Valida los permisos para el seguimiento de pedidos.
''' </summary>
''' <param name></param>
''' <returns>Devuelve false si teniendo el permiso de consulta de pedidos en el seguimiento activo, no tiene ning�n permiso de seguimiento de pedidos chequeado. / True si todo ok </returns>
''' <remarks>Llamada desde: cmdGS(3); Tiempo m�ximo: 0,2</remarks>

Private Function ValidarSeguimiento() As Boolean
Dim i As Integer
Dim lbConsulta As Boolean
Dim lbPedSegDirec As Boolean
Dim lbPedSegAprov As Boolean
Dim lbPedSegErp As Boolean
    
       
    'Miramos que permisos tiene.
    For i = 1 To m_oAccionesIniciales.Count
        If m_oAccionesIniciales(i).Id = PEDSEGConsultar Then
             lbConsulta = True
        ElseIf m_oAccionesIniciales(i).Id = PEDSEGSoloDirectos Then
            lbPedSegDirec = True
        ElseIf m_oAccionesIniciales(i).Id = PEDSEGSoloAprov Then
            lbPedSegAprov = True
        ElseIf m_oAccionesIniciales(i).Id = PEDSEGErp Then
            lbPedSegErp = True
        End If
    Next
    
    ValidarSeguimiento = True

    'Comprobamos que si tiene la consulta chequeada tenga al menos un permiso chequeado.
    If lbConsulta Then
        'Existen los tres tipos de pedidos.
        If gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP Then
            If lbPedSegDirec = False And lbPedSegAprov = False And lbPedSegErp = False Then
                ValidarSeguimiento = False
            End If
        'Existen s�lo de aprovisionamiento y de erp.
        ElseIf Not gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP Then
            If lbPedSegAprov = False And lbPedSegErp = False Then
                ValidarSeguimiento = False
            Else
                'Como gParametrosGenerales.gbPedidosDirectos=False, deschequeamos el permiso que aunque no se carga se est� chequeando.
                If lbPedSegDirec Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDSEGSoloDirectos Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        'Existen s�lo directos y de erp.
        ElseIf gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP Then
            If lbPedSegDirec = False And lbPedSegErp = False Then
                ValidarSeguimiento = False
            Else
                'Como gParametrosGenerales.gbPedidosAprov=False, deschequeamos el permiso que aunque no se carga se est� chequeando.
                If lbPedSegAprov Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDSEGSoloAprov Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
                
            End If
        'Existen s�lo directos y de aprovisionamiento.
        ElseIf gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP Then
            If lbPedSegDirec = False And lbPedSegAprov = False Then
                ValidarSeguimiento = False
            Else
                'Como gParametrosGenerales.gbPedidosERP=False, deschequeamos el permiso que aunque no se carga se est� chequeando.
                If lbPedSegErp Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDSEGErp Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        'Existen s�lo directos.
        ElseIf gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP Then
            If lbPedSegDirec = False Then
                ValidarSeguimiento = False
            Else
                'Como gParametrosGenerales.gbPedidosERP=False y gParametrosGenerales.gbPedidosAprov=False, deschequeamos los permisos que aunque no se cargan se est�n chequeando.
                If lbPedSegErp Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDSEGErp Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
                If lbPedSegAprov Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDSEGSoloAprov Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        'Existen s�lo de aprovisionamiento.
        ElseIf Not gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP Then
            If lbPedSegAprov = False Then
                ValidarSeguimiento = False
            Else
                'Como gParametrosGenerales.gbPedidosERP=False y gParametrosGenerales.gbPedidosDirectos=False, deschequeamos los permisos que aunque no se cargan se est�n chequeando.
                If lbPedSegErp Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDSEGErp Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
                If lbPedSegDirec Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDSEGSoloDirectos Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        'Existen s�lo de erp.
        ElseIf Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP Then
            If lbPedSegErp = False Then
                ValidarSeguimiento = False
            Else
                'Como gParametrosGenerales.gbPedidosERP=False y gParametrosGenerales.gbPedidosDirectos=False, deschequeamos los permisos que aunque no se cargan se est�n chequeando.
                If lbPedSegAprov Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDSEGSoloAprov Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
                If lbPedSegDirec Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDSEGSoloDirectos Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
    End If
   

End Function


''' <summary>
''' Valida los permisos para la recepci�n de pedidos.
''' </summary>
''' <param name></param>
''' <returns>Devuelve false si teniendo el permiso de consulta de pedidos en la recepci�n activo, no tiene ning�n permiso de recepci�n de pedidos chequeado. / True si todo ok </returns>
''' <remarks>Llamada desde: cmdGS(3); Tiempo m�ximo: 0,2</remarks>

Private Function ValidarRecepcion() As Boolean
Dim i As Integer
Dim lbConsulta As Boolean
Dim lbPedRecDirec As Boolean
Dim lbPedRecAprov As Boolean
Dim lbPedRecErp As Boolean
    
    'Miramos que permisos tiene.
    For i = 1 To m_oAccionesIniciales.Count
        If m_oAccionesIniciales(i).Id = PEDRECConsultar Then
             lbConsulta = True
        ElseIf m_oAccionesIniciales(i).Id = PEDRECSoloDirectos Then
            lbPedRecDirec = True
        ElseIf m_oAccionesIniciales(i).Id = PEDRECSoloAprov Then
            lbPedRecAprov = True
        ElseIf m_oAccionesIniciales(i).Id = PEDRECErp Then
            lbPedRecErp = True
        End If
    Next
    
    ValidarRecepcion = True
    
    'Comprobamos que si tiene la consulta chequeada tenga al menos un permiso chequeado.
    If lbConsulta Then
        'Existen los tres tipos de pedidos.
        If gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP Then
            If lbPedRecDirec = False And lbPedRecAprov = False And lbPedRecErp = False Then
                ValidarRecepcion = False
            End If
        'Existen s�lo de aprovisionamiento y de erp.
        ElseIf Not gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP Then
            If lbPedRecAprov = False And lbPedRecErp = False Then
                ValidarRecepcion = False
            Else
                'Como gParametrosGenerales.gbPedidosDirectos=False, deschequeamos el permiso que aunque no se carga se est� chequeando.
                If lbPedRecDirec Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDRECSoloDirectos Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        'Existen s�lo directos y de erp.
        ElseIf gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP Then
            If lbPedRecDirec = False And lbPedRecErp = False Then
                ValidarRecepcion = False
            Else
                'Como gParametrosGenerales.gbPedidosAprov=False, deschequeamos el permiso que aunque no se carga se est� chequeando.
                If lbPedRecAprov Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDRECSoloAprov Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
                
            End If
        'Existen s�lo directos y de aprovisionamiento.
        ElseIf gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP Then
            If lbPedRecDirec = False And lbPedRecAprov = False Then
                ValidarRecepcion = False
            Else
                'Como gParametrosGenerales.gbPedidosERP=False, deschequeamos el permiso que aunque no se carga se est� chequeando.
                If lbPedRecErp Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDRECErp Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        'Existen s�lo directos.
        ElseIf gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP Then
            If lbPedRecDirec = False Then
                ValidarRecepcion = False
            Else
                'Como gParametrosGenerales.gbPedidosERP=False y gParametrosGenerales.gbPedidosAprov=False, deschequeamos los permisos que aunque no se cargan se est�n chequeando.
                If lbPedRecErp Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDRECErp Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
                If lbPedRecAprov Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDRECSoloAprov Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        'Existen s�lo de aprovisionamiento.
        ElseIf Not gParametrosGenerales.gbPedidosDirectos And gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosERP Then
            If lbPedRecAprov = False Then
                ValidarRecepcion = False
            Else
                'Como gParametrosGenerales.gbPedidosERP=False y gParametrosGenerales.gbPedidosDirectos=False, deschequeamos los permisos que aunque no se cargan se est�n chequeando.
                If lbPedRecErp Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDRECErp Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
                If lbPedRecDirec Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDRECSoloDirectos Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        'Existen s�lo de erp.
        ElseIf Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov And gParametrosGenerales.gbPedidosERP Then
            If lbPedRecErp = False Then
                ValidarRecepcion = False
            Else
                'Como gParametrosGenerales.gbPedidosERP=False y gParametrosGenerales.gbPedidosDirectos=False, deschequeamos los permisos que aunque no se cargan se est�n chequeando.
                If lbPedRecAprov Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDRECSoloAprov Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
                If lbPedRecDirec Then
                    For i = 1 To m_oAccionesIniciales.Count
                        If m_oAccionesIniciales(i).Id = PEDRECSoloDirectos Then
                            m_oAccionesIniciales.Remove (i)
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
        
    End If
   

End Function

'Parametros entrada:
'   oRow --> fila en la que hemos pulsado
'   sKey --> Nombre columna pulsada  ("C_ALTA")...
Private Sub RecorrerBandasySeleccionarCC(ByVal oRow As SSRow, ByVal sKey As String, ByVal bValor As Boolean)
Dim oRow1 As SSRow

Dim sNivel As String

    If oRow.HasChild Then
        Set oRow1 = oRow.GetChild(ssChildRowFirst)
    
        Do While oRow1.HasNextSibling
            
            sNivel = "UON" & oRow1.Band.Index & "CC"
            If oRow1.Cells(sNivel).Value = 0 Then
                RecorrerBandasySeleccionarCC oRow1, sKey, bValor
            End If
            IntroducirValor oRow1, sKey, bValor
            Set oRow1 = oRow1.GetSibling(ssSiblingRowNext)
        Loop

        If oRow1.HasChild Then
            RecorrerBandasySeleccionarCC oRow1, sKey, bValor
        End If
        'Tiene un unico hijo
        IntroducirValor oRow1, sKey, bValor
            
    End If
    

End Sub

Private Sub IntroducirValor(ByVal oRow As SSRow, ByVal sKey As String, ByVal bValor As Boolean)
Dim sCadena As String

sCadena = "Codigo= " & oRow.Cells("COD").Value

    If bValor = True Then
        Select Case sKey
        
        Case "C_ALTA"
                oRow.Cells("C_CONSULTA").Value = bValor
        Case "C_MODIF"
                oRow.Cells("C_CONSULTA").Value = bValor
        Case "C_PRESUP"
                oRow.Cells("C_CONSULTA").Value = bValor
                
        
        End Select
    
    Else
        If sKey = "C_CONSULTA" Then

            oRow.Cells("C_ALTA").Value = bValor
            oRow.Cells("C_MODIF").Value = bValor
            oRow.Cells("C_PRESUP").Value = bValor
        End If
    
    End If

    oRow.Cells(sKey).Value = bValor

End Sub


Private Function ActualizarPadre(ByVal oRow As SSRow, ByVal sKey As String) As Boolean

Dim bResultado As Boolean
Dim oRow1 As SSRow
Dim bSalir As Boolean

    bResultado = True

    Set oRow1 = oRow.GetChild(ssChildRowFirst)

    Do While oRow1.HasNextSibling And Not bSalir
        If Not oRow1.Cells(sKey).Value = True Then
            bSalir = True
        End If
        Set oRow1 = oRow1.GetSibling(ssSiblingRowNext)
    Loop

    If Not oRow1.Cells(sKey).Value = True Then
        bSalir = True
    End If

    If bSalir Then
        bResultado = False
    End If



ActualizarPadre = bResultado

End Function

Private Function ActualizarGrid(ByVal oRow As SSRow, ByVal sKey As String) As Boolean

Dim oRow1 As SSRow
Dim bResultado As Boolean
Dim bResultadoFinal As Boolean

bResultadoFinal = True

    If oRow.HasChild Then
        
        Set oRow1 = oRow.GetChild(ssChildRowFirst)
    
        Do While oRow1.HasNextSibling
            If oRow1.HasChild Then
                bResultado = ActualizarGrid(oRow1, sKey)
                oRow1.Cells(sKey).Value = bResultado
            Else
                 bResultado = oRow1.Cells(sKey).Value
            End If
            If Not bResultado Then
                bResultadoFinal = False
            End If
            Set oRow1 = oRow1.GetSibling(ssSiblingRowNext)
        Loop
        
        If oRow1.HasChild Then
            bResultado = ActualizarGrid(oRow1, sKey)
            oRow1.Cells(sKey).Value = bResultado
        Else
            bResultado = oRow1.Cells(sKey).Value
        End If
            
        If Not bResultado Then
            bResultadoFinal = False
        End If
            


        ActualizarGrid = bResultadoFinal

    Else

        bResultado = oRow.Cells(sKey).Value
        ActualizarGrid = bResultado
    End If
    
End Function

''' Revisado por: blp. Fecha: 02/05/2012
''' <summary>
''' Acci�n a ejecutar en funci�n de la opci�n escogida en el men� contextual
''' </summary>
''' <param name="ItemNumber">�ndice del elemento pulsado</param>
''' <remarks>
''' llamada desde el evento click del control cp. M�ximo 0,1 seg.
''' </remarks>
Private Sub cP_Click(ItemNumber As Long)
    Select Case cP.itemKey(ItemNumber)
        Case "mnuAsignarPartidasGestor"
            frmUSUARIOSSelGestores.Show 1
    End Select
End Sub
    
''' Revisado por: blp. Fecha: 19/06/2012
''' <summary>
''' Cargar el men� contextual
''' </summary>
''' <param name="X">Posici�n de ordenadas</param>
''' <param name="Y">Posici�n de abcisas</param>
''' <remarks>
''' llamada desde el evento tvwestrorg_GesFac_MouseUp. M�ximo 0,1 seg.
''' </remarks>
Private Sub cargarMenu(X As Single, Y As Single)
    cP.clear
    cP.AddItem m_sAsignarPartidasGestor, , 1, , , , , "mnuAsignarPartidasGestor"
        DoEvents
        cP.ShowPopupMenu X, Y
End Sub
    
''' Revisado por: blp. Fecha: 19/06/2012
''' <summary>
''' Genera la estructura de Unidades Organizativas en el treeview recibido
''' y marca cada centro de coste en el que se ha definido al usuario como gestor de alguna o todas
''' sus partidas presupuestarias
''' Tiene en cuenta el estado de cada nodo (Baja, Centro de Coste, imputaci�n de pedidos de aprovisionamoento)
''' </summary>
''' <param name="tvwArbol">Treeview en el que crear la estructura de UONs</param>
''' <remarks>Llamada desde PonerArbolIPAVisible; Tiempo m�ximo < 1 seg.</remarks>
Private Sub GenerarEstructuraOrgGestor(ByRef tvwArbol As TreeView)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUON1 As CUnidadOrgNivel1
Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1

Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUON2 As CUnidadOrgNivel2
Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2

Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
Dim oUON3 As CUnidadOrgNivel3
Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3

Dim oUnidadesorgN4 As CUnidadesOrgNivel4
Dim oUON4 As CUnidadOrgNivel4
Set oUnidadesorgN4 = oFSGSRaiz.Generar_CUnidadesOrgNivel4

' Otras
Dim nodx As node
Dim sKeyImage As String
                 
oUnidadesOrgN1.CargarTodosLosCCGestorNivel1 False, g_oUsuario.Cod
oUnidadesOrgN2.CargarTodosLosCCGestorNivel2 False, g_oUsuario.Cod
oUnidadesOrgN3.CargarTodosLosCCGestorNivel3 False, g_oUsuario.Cod
oUnidadesorgN4.CargarTodosLosCCGestorNivel4 False, g_oUsuario.Cod
        
        
   '**************************************************************
    'GENERAMOS LA ESTRUCTURA ARBOREA DE LAS UNIDADES ORGANIZATIVAS
    
    Set nodx = tvwArbol.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True

    For Each oUON1 In oUnidadesOrgN1
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        If Not IsNull(oUON1.idEmpresa) Then
            If oUON1.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            ElseIf oUON1.TieneGestor Then
                sKeyImage = "UON1CCRED"
            ElseIf oUON1.CC Then
                sKeyImage = "UON1CC"
            Else
                sKeyImage = "SELLO"
            End If
        Else
            If oUON1.BajaLog = 1 Then
                sKeyImage = "UON1BAJA"
            ElseIf oUON1.TieneGestor Then
                sKeyImage = "UON1CCRED"
            ElseIf oUON1.CC Then
                sKeyImage = "UON1CC"
            Else
                sKeyImage = "UON1"
            End If
        End If
        If oUON1.Nodo_Usuario Then
        End If
        
        Set nodx = tvwArbol.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, sKeyImage)
        
        nodx.Tag = "UON1" & oUON1.Cod & "#" & IIf(oUON1.CC = False, 0, 1)
    Next

    For Each oUON2 In oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        If Not IsNull(oUON2.idEmpresa) Then
            If oUON2.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            ElseIf oUON2.TieneGestor Then
                sKeyImage = "UON2CCRED"
            ElseIf oUON2.CC Then
                sKeyImage = "UON2CC"
            Else
                sKeyImage = "SELLO"
            End If
        Else
            If oUON2.BajaLog = 1 Then
                sKeyImage = "UON2BAJA"
            ElseIf oUON2.TieneGestor Then
                sKeyImage = "UON2CCRED"
            ElseIf oUON2.CC Then
                sKeyImage = "UON2CC"
            Else
                sKeyImage = "UON2"
            End If
        End If
        Set nodx = tvwArbol.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, sKeyImage)
        
        nodx.Tag = "UON2" & oUON2.Cod & "#" & IIf(oUON2.CC = False, 0, 1)
    Next

    For Each oUON3 In oUnidadesOrgN3
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        If Not IsNull(oUON3.idEmpresa) Then
            If oUON3.BajaLog = 1 Then
                sKeyImage = "SELLOBAJA"
            ElseIf oUON3.TieneGestor Then
                sKeyImage = "UON3CCRED"
            ElseIf oUON3.CC Then
                sKeyImage = "UON3CC"
            Else
                sKeyImage = "SELLO"
            End If

        Else
            If oUON3.BajaLog = 1 Then
                sKeyImage = "UON3BAJA"
            ElseIf oUON3.TieneGestor Then
                sKeyImage = "UON3CCRED"
            ElseIf oUON3.CC Then
                sKeyImage = "UON3CC"
            Else
                sKeyImage = "UON3"
            End If
        End If
        Set nodx = tvwArbol.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, sKeyImage)
        
        nodx.Tag = "UON3" & oUON3.Cod & "#" & IIf(oUON3.CC = False, 0, 1)
    Next

    For Each oUON4 In oUnidadesorgN4
        scod1 = oUON4.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON4.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON4.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON4.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON4.CodUnidadOrgNivel3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON4.CodUnidadOrgNivel3))
        scod4 = scod3 & oUON4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(oUON4.Cod))
        If oUON4.BajaLog = 1 Then
            sKeyImage = "UON4"
        ElseIf oUON4.TieneGestor Then
            sKeyImage = "UON4CCRED"
        ElseIf oUON4.CC Then
            sKeyImage = "UON4CC"
        Else
            sKeyImage = "UON4"
        End If
        Set nodx = tvwArbol.Nodes.Add("UON3" & scod3, tvwChild, "UON4" & scod4, CStr(oUON4.Cod) & " - " & oUON4.Den, sKeyImage)
        
        nodx.Tag = "UON4" & oUON4.Cod & "#" & IIf(oUON4.CC = False, 0, 1)
    Next
    
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oUON4 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    Set oUnidadesorgN4 = Nothing
    
    
End Sub

''' Revisado por: blp. Fecha: 19/06/2012
''' <summary>
''' Recorremos el treeview para borrar los nodos que no haya que mostrar
''' y expendir los marcados/chequeados
''' </summary>
''' <param name="tvwArbol">treeview en el que se efectuar�n las operaciones</param>
''' <remarks>Llamada desde PonerArbolIPAVisible; Tiempo m�ximo 1 seg.</remarks>
Private Sub RecorreryBorrarEstructuraOrgGestor(ByRef tvwArbol As TreeView)
'UON1, UON2 UON3 O UON4 pueden ser CC
'pero solo habra un CC para cada familia
'Recorremos la familia hasta llegar al CC y ahi parar de recorrer esa familia

Dim nodo As node
Dim i As Integer
Dim bEncontrado As Boolean
Dim intcont As Integer

On Error Resume Next
    PicTapa2.Visible = False
    ' recorre todos los nodos
    bEncontrado = False
    With tvwArbol
        For i = 1 To .Nodes.Count   'leo nodo a nodo
            If InStr(.Nodes(i).Tag, "#1") = 0 And .Nodes(i).Children = 0 Then
                Set nodo = .Nodes(i)
                .Nodes.Remove nodo.Index
                bEncontrado = True
                intcont = intcont + 1
            Else 'SI NO BORRAMOS ESE NODO, Expandimos los nodos seleccionados
                If Right(.Nodes(i).Image, 3) = "RED" Then
                    .Nodes(i).EnsureVisible
                    .Nodes(i).Expanded = False
                End If
            End If
        Next
    End With
    tvwArbol.Refresh
    If bEncontrado Then RecorreryBorrarEstructuraOrgGestor tvwArbol
End Sub






