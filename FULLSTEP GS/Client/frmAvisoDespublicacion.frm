VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmAvisoDespublicacion 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5745
   ClientLeft      =   2385
   ClientTop       =   5100
   ClientWidth     =   8610
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmAvisoDespublicacion.frx":0000
   ScaleHeight     =   5745
   ScaleWidth      =   8610
   Begin VB.CommandButton cmdCerrar 
      Height          =   225
      Left            =   8295
      Picture         =   "frmAvisoDespublicacion.frx":AED2
      Style           =   1  'Graphical
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   30
      Width           =   250
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgProcesos 
      Height          =   2235
      Left            =   0
      TabIndex        =   0
      Top             =   330
      Width           =   8565
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   12
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmAvisoDespublicacion.frx":B0CC
      stylesets(1).Name=   "Aviso"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmAvisoDespublicacion.frx":B0E8
      stylesets(1).AlignmentPicture=   1
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      CellNavigation  =   1
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      CaptionAlignment=   0
      Columns.Count   =   12
      Columns(0).Width=   1296
      Columns(0).Caption=   "DA�o"
      Columns(0).Name =   "ANYO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasHeadForeColor=   -1  'True
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadForeColor=   16777215
      Columns(0).HeadBackColor=   208
      Columns(0).BackColor=   14671839
      Columns(1).Width=   1191
      Columns(1).Caption=   "DGmn1"
      Columns(1).Name =   "GMN1"
      Columns(1).Alignment=   2
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasHeadForeColor=   -1  'True
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).HeadForeColor=   16777215
      Columns(1).HeadBackColor=   208
      Columns(1).BackColor=   14671839
      Columns(2).Width=   1614
      Columns(2).Caption=   "DProce"
      Columns(2).Name =   "PROCE"
      Columns(2).Alignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasHeadForeColor=   -1  'True
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).HeadForeColor=   16777215
      Columns(2).HeadBackColor=   208
      Columns(2).BackColor=   14671839
      Columns(3).Width=   4392
      Columns(3).Caption=   "DDescripcion"
      Columns(3).Name =   "DESCR"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasHeadForeColor=   -1  'True
      Columns(3).HasHeadBackColor=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).HeadForeColor=   16777215
      Columns(3).HeadBackColor=   208
      Columns(3).BackColor=   14671839
      Columns(4).Width=   3096
      Columns(4).Caption=   "DFecha Desp"
      Columns(4).Name =   "FECHA2"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasHeadForeColor=   -1  'True
      Columns(4).HasHeadBackColor=   -1  'True
      Columns(4).HeadForeColor=   16777215
      Columns(4).HeadBackColor=   208
      Columns(5).Width=   1879
      Columns(5).Caption=   "DFecha Adj"
      Columns(5).Name =   "FECHADJU"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).HasHeadForeColor=   -1  'True
      Columns(5).HasHeadBackColor=   -1  'True
      Columns(5).HeadForeColor=   16777215
      Columns(5).HeadBackColor=   208
      Columns(6).Width=   1191
      Columns(6).Caption=   "DResp"
      Columns(6).Name =   "RESP"
      Columns(6).Alignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).Style=   2
      Columns(6).HasHeadForeColor=   -1  'True
      Columns(6).HasHeadBackColor=   -1  'True
      Columns(6).HasBackColor=   -1  'True
      Columns(6).HeadForeColor=   16777215
      Columns(6).HeadBackColor=   208
      Columns(6).BackColor=   14671839
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Pub"
      Columns(7).Name =   "PUB"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "Adjdir"
      Columns(8).Name =   "ADJDIR"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ID"
      Columns(9).Name =   "ID"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ESTID"
      Columns(10).Name=   "ESTID"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "SUBASTA"
      Columns(11).Name=   "SUBASTA"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      _ExtentX        =   15108
      _ExtentY        =   3942
      _StockProps     =   79
      Caption         =   "DAviso de procesos proximos a la fecha de presenatci�n o adjudicaci�n y despublicaci�n"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgContratos 
      Height          =   2235
      Left            =   0
      TabIndex        =   3
      Top             =   2640
      Width           =   8565
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   8
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmAvisoDespublicacion.frx":B332
      stylesets(1).Name=   "Aviso"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmAvisoDespublicacion.frx":B34E
      stylesets(1).AlignmentPicture=   1
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      CellNavigation  =   1
      MaxSelectedRows =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      CaptionAlignment=   0
      Columns.Count   =   8
      Columns(0).Width=   1879
      Columns(0).Caption=   "DIdentificador"
      Columns(0).Name =   "IDENTIFICADOR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasHeadForeColor=   -1  'True
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadForeColor=   16777215
      Columns(0).HeadBackColor=   208
      Columns(0).BackColor=   14671839
      Columns(1).Width=   4233
      Columns(1).Caption=   "dDescripcion"
      Columns(1).Name =   "DESCRIPCION"
      Columns(1).CaptionAlignment=   0
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasHeadForeColor=   -1  'True
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).HeadForeColor=   16777215
      Columns(1).HeadBackColor=   208
      Columns(1).BackColor=   14671839
      Columns(2).Width=   1984
      Columns(2).Caption=   "DFechaInicio"
      Columns(2).Name =   "FEC_INI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasHeadForeColor=   -1  'True
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HeadForeColor=   16777215
      Columns(2).HeadBackColor=   208
      Columns(3).Width=   2408
      Columns(3).Caption=   "DFechaFin"
      Columns(3).Name =   "FEC_FIN"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasHeadForeColor=   -1  'True
      Columns(3).HasHeadBackColor=   -1  'True
      Columns(3).HeadForeColor=   16777215
      Columns(3).HeadBackColor=   208
      Columns(4).Width=   2725
      Columns(4).Caption=   "dProceso de Compra"
      Columns(4).Name =   "PROCE"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).HasHeadForeColor=   -1  'True
      Columns(4).HasHeadBackColor=   -1  'True
      Columns(4).HeadForeColor=   16777215
      Columns(4).HeadBackColor=   208
      Columns(5).Width=   1720
      Columns(5).Caption=   "DRenegociar"
      Columns(5).Name =   "RENEGOCIAR"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      Columns(5).ButtonsAlways=   -1  'True
      Columns(5).HasHeadForeColor=   -1  'True
      Columns(5).HasHeadBackColor=   -1  'True
      Columns(5).HeadForeColor=   16777215
      Columns(5).HeadBackColor=   208
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "CONTRATO"
      Columns(6).Name =   "CONTRATO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "OBSERVADOR"
      Columns(7).Name =   "OBSERVADOR"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   15108
      _ExtentY        =   3942
      _StockProps     =   79
      Caption         =   "DAviso de contratos pr�ximos a expirar"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblSaludo 
      BackColor       =   &H000000C0&
      Caption         =   "FULLSTEP GS"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8725
   End
End
Attribute VB_Name = "frmAvisoDespublicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_FechaDesAdj As Date
Public g_FechaHasAdj As Date
Public g_FechaDesDes As Date
Public g_FechaHasDes As Date
Public g_bAvisoNotificacion As Boolean
Public g_bAvisoContrato As Boolean


Private m_bHayErrores As Boolean

Private m_oProcesoEnEdicion As CProceso

'interface para manejar las publicaciones
Private m_oIPub As IPublicaciones

Private m_sPregunta As String
' Coleccion de procesos a cargar
Public g_oProcesos As CProcesos
Private m_bModomovimiento As Boolean
Private m_bModAdj As Boolean
Private m_bModPub As Boolean
Private m_bREqpPub As Boolean 'Restricci�n de equipo para la publicaci�n
Private m_bRComPub As Boolean 'Restricci�n de comprador asignado para la publicaci�n
Private m_bModSubasta As Boolean

Private m_bposx As Long
Private m_bposy As Long

Private m_sProceso As String
Private m_sGrupo As String
Private m_sItem As String


''' <summary>
''' Carga de los recursos de la pagina
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde;Load de la pagina Tiempo m�ximo 0</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FSgsidiomas.FRM_AVISO_DESPUBLICACION, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        Ador.MoveNext
        Ador.MoveNext
        If sdbgProcesos.AllowUpdate Then
            Ador.MoveNext
        Else
            Ador.MoveNext
        End If
        
        Ador.MoveNext
        sdbgProcesos.Columns("ANYO").caption = Ador(0).Value
        sdbgProcesos.Columns("GMN1").caption = basParametros.gParametrosGenerales.gsabr_GMN1
        Ador.MoveNext
        sdbgProcesos.Columns("PROCE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgProcesos.Columns("DESCR").caption = Ador(0).Value
        Ador.MoveNext
        sdbgProcesos.Columns("FECHA2").caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sPregunta = Ador(0).Value
        Ador.MoveNext
        sdbgProcesos.Columns("FECHADJU").caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sdbgProcesos.Columns("RESP").caption = Ador(0).Value
        Ador.MoveNext
        sdbgProcesos.caption = Ador(0).Value
        
        Ador.MoveNext
        m_sProceso = Ador(0).Value
        Ador.MoveNext
        m_sGrupo = Ador(0).Value
        Ador.MoveNext
        m_sItem = Ador(0).Value
        
        Ador.MoveNext
        Me.sdbgContratos.caption = Ador(0).Value 'Aviso de contratos pr�ximos a expirar
        Ador.MoveNext
        sdbgContratos.Columns("IDENTIFICADOR").caption = Ador(0).Value
        Ador.MoveNext
        sdbgContratos.Columns("DESCRIPCION").caption = Ador(0).Value
        Ador.MoveNext
        sdbgContratos.Columns("FEC_INI").caption = Ador(0).Value
        Ador.MoveNext
        sdbgContratos.Columns("FEC_FIN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgContratos.Columns("PROCE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgContratos.Columns("RENEGOCIAR").caption = Ador(0).Value

        Ador.Close
    End If
    
End Sub
Private Sub cmdCerrar_Click()
    
    m_bHayErrores = False
    sdbgProcesos.Update
    DoEvents
    
    If Not m_bHayErrores Then
'        frmEST.PicExclamacion.Visible = False
        Unload Me
    End If
    
    
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Private Sub CargarSeguridad()
    m_bModAdj = False
    m_bModPub = False
    m_bREqpPub = False
    m_bRComPub = False
    m_bModSubasta = False
    
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModifFecLimit)) Is Nothing) And (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModificarDatGen)) Is Nothing) Then
        m_bModPub = True
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModifSubasta)) Is Nothing Then
            m_bModSubasta = True
        End If
        If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestAsigEqp)) Is Nothing) And basOptimizacion.gTipoDeUsuario = comprador Then
            m_bREqpPub = True
        End If
        If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestAsigComp)) Is Nothing) And basOptimizacion.gTipoDeUsuario = comprador Then
            m_bRComPub = True
        End If
    Else
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEModifFecLimit)) Is Nothing) And (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEOfeyObj)) Is Nothing) Then
            m_bModPub = True
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModifSubasta)) Is Nothing Then
                m_bModSubasta = True
            End If
            If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestAsigEqp)) Is Nothing) And basOptimizacion.gTipoDeUsuario = comprador Then
                m_bREqpPub = True
            End If
            If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVERestAsigComp)) Is Nothing) And basOptimizacion.gTipoDeUsuario = comprador Then
                m_bRComPub = True
            End If
        End If
    End If
    If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModificarDatGen)) Is Nothing) Then
        m_bModAdj = True
    End If
End Sub
''' <summary>
''' Carga la informacion del formulario.
''' Carga los idiomas de los componentes
''' </summary>
''' <remarks>Tiempo m�ximo:0,8seg.</remarks>
Private Sub Form_Load()

    Me.Width = 8725
    Me.Height = 2565
    sdbgProcesos.Width = 8665
    sdbgContratos.Width = 8665
    cmdCerrar.Left = Me.Width - cmdCerrar.Width - 75
    
    CargarSeguridad
    CargarRecursos
    
    PonerFieldSeparator Me
    
    sdbgProcesos.Visible = False
    If g_bAvisoNotificacion Then
        sdbgProcesos.Visible = True
        If m_bModPub Then
            sdbgProcesos.Columns("FECHA2").Locked = False
        Else
            sdbgProcesos.Columns("FECHA2").Locked = True
        End If
        
        If m_bModAdj Then
            sdbgProcesos.Columns("FECHADJU").Locked = False
        Else
            sdbgProcesos.Columns("FECHADJU").Locked = True
        End If
    End If
    
    If g_bAvisoContrato Then
        If g_bAvisoNotificacion Then
            Me.Height = Me.Height + sdbgProcesos.Height + 200
        Else
            sdbgContratos.Top = sdbgProcesos.Top
             Me.Height = Me.Height + 200
        End If
        CargarContratos
    End If
    
End Sub
''' <summary>
''' Realiza la llamada web a detalle de contratos
''' </summary>
''' <remarks>Tiempo m�ximo:0,8seg.</remarks>
Private Sub sdbgContratos_DblClick()
    Dim sURL As String
    Dim sParametros As String
    Dim strSessionId As String
    Dim Ador As Ador.Recordset

    strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)

    sParametros = "?Observador=" & sdbgContratos.Columns("OBSERVADOR").Value
    sParametros = sParametros & "*Instancia=" & sdbgContratos.Columns("IDENTIFICADOR").Value
    sParametros = sParametros & "*NombreContrato=" & sdbgContratos.Columns("DESCRIPCION").Value
    sParametros = sParametros & "*desdeGS=1"

    sURL = gParametrosGenerales.gsURLPMDetalle & "sessionId=" & strSessionId & "&desdeGS=1&" & gParametrosGenerales.gcolRutas("DetalleContratos") & sParametros
    With frmInternet
        .g_sRuta = sURL
        Me.Hide
        .Show
    End With
End Sub

''' <summary>
''' Actualizar la fila en edicion
''' </summary>
''' <param name="RtnDispErrMsg">Buffer con los datos y bookmark de la fila</param>
''' <remarks>Llamada desde; Evento que se produce al hace un cambio en el grid Tiempo m�ximo</remarks>
Private Sub sdbgProcesos_AfterUpdate(RtnDispErrMsg As Integer)

Dim oTESError As TipoErrorSummit
Dim Ador As Ador.Recordset
Dim oProcesoSeleccionado As CProceso
Dim irespuesta As Integer
Dim sproveedor As String
Dim oProveedores As CProveedores
Dim oProve As CProveedor
Dim teserror As TipoErrorSummit
Dim v As Variant
Dim bPubli As Boolean
Dim oAtribsEspec As CAtributosEspecificacion
Dim oAtribEsp As CAtributoEspecificacion
Dim sCadena As String
Dim sCadenaTmp As String
    If m_oProcesoEnEdicion Is Nothing Then
        DoEvents
        sdbgProcesos_Change
        DoEvents
    End If
        
    ''' Modificamos en la base de datos
    
    m_bHayErrores = False
    
    If sdbgProcesos.Columns("FECHA2").Value <> "" And sdbgProcesos.Columns("FECHADJU").Value <> "" Then
        
        teserror = m_oProcesoEnEdicion.AplazarFechas(CDate(sdbgProcesos.Columns("FECHA2").Value), CDate(sdbgProcesos.Columns("FECHADJU").Value))
    Else
        If sdbgProcesos.Columns("FECHA2").Value = "" Then
            If sdbgProcesos.Columns("FECHADJU").Value = "" Then
                teserror = m_oProcesoEnEdicion.AplazarFechas
            Else
                teserror = m_oProcesoEnEdicion.AplazarFechas(, CDate(sdbgProcesos.Columns("FECHADJU").Value))
            End If
        Else
            If sdbgProcesos.Columns("FECHADJU").Value = "" Then
                teserror = m_oProcesoEnEdicion.AplazarFechas(CDate(sdbgProcesos.Columns("FECHA2").Value))
            Else
                teserror = m_oProcesoEnEdicion.AplazarFechas(CDate(sdbgProcesos.Columns("FECHA2").Value), CDate(sdbgProcesos.Columns("FECHADJU").Value))
            End If
        End If
    End If
    
    If teserror.NumError <> TESnoerror Then
        v = sdbgProcesos.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgProcesos.SetFocus
        sdbgProcesos.ActiveCell.Value = v
        sdbgProcesos.DataChanged = False
        
    Else
                
        Set m_oProcesoEnEdicion = Nothing
        bPubli = False
        
        If IsDate(g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaMinimoLimOfertas) Then
            ''Con fecha limite, luego, puede estar caducada --> controlar q solo hay q republicar en caso de estar caducada. Tarea 3110
            ''Ejemplo: Hoy 08/10. Fecha limite 01/12. Nueva fecha limite 15/12. Job Despublicacion no ha actuado. Pues no se le ve sentido a
            ''q los N proves q no se ha deseado publicar hasta el momento sean publicados ahora. O q los q fueron despublicados a mano vuelvan
            ''a ser publicados.
            If g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaMinimoLimOfertas < Now Then
                If IsDate(sdbgProcesos.Columns("FECHA2").Value) Then
                    If CDate(sdbgProcesos.Columns("FECHA2").Value) <> CDate(g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaMinimoLimOfertas) Then
                        bPubli = True
                    End If
                End If
            End If
        Else
            ''Sin fecha limite, luego, no caducada --> NO hay q republicar. Tarea 3110
            ''If IsDate(sdbgProcesos.Columns("FECHA2").Value) Then
            ''    bPubli = True
            ''End If
        End If
        
               
        If bPubli And gParametrosGenerales.giINSTWEB <> SinWeb Then
           Set oProcesoSeleccionado = Nothing
           Set oProcesoSeleccionado = oFSGSRaiz.Generar_CProceso
           oProcesoSeleccionado.Anyo = sdbgProcesos.Columns("ANYO").Value
           oProcesoSeleccionado.GMN1Cod = sdbgProcesos.Columns("GMN1").Value
           oProcesoSeleccionado.Cod = sdbgProcesos.Columns("PROCE").Value
           oProcesoSeleccionado.FechaMinimoLimOfertas = sdbgProcesos.Columns("FECHA2").Value
           Set oProveedores = Nothing
           If m_bRComPub Then
                Set oProveedores = oProcesoSeleccionado.DevolverProveedoresAPublicar(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
           ElseIf m_bREqpPub Then
                Set oProveedores = oProcesoSeleccionado.DevolverProveedoresAPublicar(basOptimizacion.gCodEqpUsuario)
           Else
                Set oProveedores = oProcesoSeleccionado.DevolverProveedoresAPublicar
           End If
           If oProveedores.Count > 0 Then
               For Each oProve In oProveedores
                   sproveedor = sproveedor & oProve.Cod & ": " & oProve.Den & vbLf
               Next
               sproveedor = sproveedor & m_sPregunta
               irespuesta = oMensajes.PreguntarPublicarProveedores(sproveedor)
               If irespuesta = vbYes Then
               
                    If Not oProcesoSeleccionado.AtributosEspValidados Then
                        'Comprobacion de atributo de epecificacion obligatorios con validacion en la publicacion
                        Set oAtribsEspec = oProcesoSeleccionado.ValidarAtributosEspObligatorios(TValidacionAtrib.Publicacion)
                        If Not oAtribsEspec.Count = 0 Then
                            sCadena = ""
                            sCadenaTmp = ""
                            For Each oAtribEsp In oAtribsEspec
                                sCadenaTmp = oAtribEsp.Den
                                Select Case oAtribEsp.ambito
                                    Case TipoAmbitoProceso.AmbProceso
                                        sCadenaTmp = sCadenaTmp & " " & m_sProceso & vbCrLf
                                    Case TipoAmbitoProceso.AmbGrupo
                                        sCadenaTmp = sCadenaTmp & " " & m_sGrupo & vbCrLf
                                    Case TipoAmbitoProceso.AmbItem
                                        sCadenaTmp = sCadenaTmp & " " & m_sItem & vbCrLf
                                End Select
                                If InStr(1, sCadena, sCadenaTmp) = 0 Then
                                    sCadena = sCadena & sCadenaTmp
                                End If
                            Next
                            oMensajes.AtributosEspObl sCadena, TValidacionAtrib.Publicacion
                        Else
                        
                            oProcesoSeleccionado.ActualizarValidacionAtribEsp (True)
                            oProcesoSeleccionado.AtributosEspValidados = True
    
                            Set m_oIPub = oProcesoSeleccionado
                            'Activa la publicaci�n
                            oTESError = m_oIPub.IActivarPublicacionProveedores(oProveedores)
                            If oTESError.NumError <> TESnoerror Then
                                basErrores.TratarError oTESError
                                m_bHayErrores = True
                            End If
                        
                        End If
                   
                    Else
                       Set m_oIPub = oProcesoSeleccionado
                       'Activa la publicaci�n
                       oTESError = m_oIPub.IActivarPublicacionProveedores(oProveedores)
                       If oTESError.NumError <> TESnoerror Then
                           basErrores.TratarError oTESError
                           m_bHayErrores = True
                       End If
                   End If
               End If
               
           End If
           Set oProveedores = Nothing

        End If
        g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaMinimoLimOfertas = sdbgProcesos.Columns("FECHA2").Value
        g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaPresentacion = sdbgProcesos.Columns("FECHADJU").Value

    End If
End Sub
Private Sub lblSaludo_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then
        m_bModomovimiento = True
        m_bposx = X
        m_bposy = Y
    End If

End Sub

Private Sub lblSaludo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If Me.WindowState = vbMaximized Then Exit Sub
    
    If m_bModomovimiento = True Then
        If m_bposx <> X Or m_bposy <> Y Then
            Me.Move Me.Left + (X - m_bposx), Me.Top + (Y - m_bposy)
        End If
    End If
End Sub

Private Sub lblSaludo_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    m_bModomovimiento = False
End Sub
Private Function FechaMinimaDeSobre() As Variant
Dim dFecha As Variant
Dim i As Integer

dFecha = Null
If Not g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).Sobres Is Nothing Then
    With g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).Sobres
        For i = 1 To .Count
            If IsNull(dFecha) Then
                If IsDate(.Item(i).FechaAperturaPrevista) Then
                    dFecha = .Item(i).FechaAperturaPrevista
                End If
            ElseIf IsDate(.Item(i).FechaAperturaPrevista) Then
                If CDate(.Item(i).FechaAperturaPrevista) < dFecha Then
                    dFecha = .Item(i).FechaAperturaPrevista
                End If
            End If
        Next
    End With
    FechaMinimaDeSobre = dFecha
End If
End Function

Private Sub sdbgProcesos_BeforeUpdate(Cancel As Integer)
    Dim irespuesta As Integer
Dim vFechaSobre  As Variant

    m_bHayErrores = False
    
    If sdbgProcesos.Columns("FECHA2").Value <> "" Then
        If Not IsDate(sdbgProcesos.Columns("FECHA2").Value) Then
            oMensajes.NoValido (35)
            Cancel = True
            sdbgProcesos.CancelUpdate
            sdbgProcesos.DataChanged = False
            If Me.Visible Then sdbgProcesos.SetFocus
            m_bHayErrores = True
            Exit Sub
        End If
        'Si proceso es de AminPublica no sea mayor que la minima fecha de sobre
        If g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).AdminPublica Then
            vFechaSobre = FechaMinimaDeSobre
            If Not IsNull(vFechaSobre) Then
                If CDate(sdbgProcesos.Columns("FECHA2").Value) > CDate(vFechaSobre) Then
                    oMensajes.FechaSobreMenorQueLimOfer CDate(vFechaSobre), 128
                    Cancel = True
                    sdbgProcesos.CancelUpdate
                    sdbgProcesos.DataChanged = False
                    If Me.Visible Then sdbgProcesos.SetFocus
                    m_bHayErrores = True
                    Exit Sub
                End If
            End If
        End If
        'Si es de subasta que no sea menor que la fecha de inicio de la subasta
        If g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).ModoSubasta Then
            If IsDate(g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaInicioSubasta) Then
                If CDate(sdbgProcesos.Columns("FECHA2").Value) >= CDate(g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaInicioSubasta) Then
                    oMensajes.FechaDesdeMayorFechaHasta 1
                    Cancel = True
                    sdbgProcesos.CancelUpdate
                    sdbgProcesos.DataChanged = False
                    If Me.Visible Then sdbgProcesos.SetFocus
                    m_bHayErrores = True
                    Exit Sub
                End If
            End If
        End If
    Else
        'Si es de admin pub no puede ser vacia
        If g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).AdminPublica Then
            oMensajes.NoValido (35)
            Cancel = True
            sdbgProcesos.CancelUpdate
            sdbgProcesos.DataChanged = False
            If Me.Visible Then sdbgProcesos.SetFocus
            m_bHayErrores = True
            Exit Sub
        End If
        'Si es de subasta no puede ser vacia
        If g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).ModoSubasta Then
            oMensajes.NoValido (35)
            Cancel = True
            sdbgProcesos.CancelUpdate
            sdbgProcesos.DataChanged = False
            If Me.Visible Then sdbgProcesos.SetFocus
            m_bHayErrores = True
            Exit Sub
        End If
        
    End If

    If sdbgProcesos.Columns("FECHADJU").Value <> "" Then
        If Not IsDate(sdbgProcesos.Columns("FECHADJU").Value) Then
            oMensajes.NoValido (119)
            Cancel = True
            sdbgProcesos.CancelUpdate
            sdbgProcesos.DataChanged = False
            If Me.Visible Then sdbgProcesos.SetFocus
            m_bHayErrores = True
            Exit Sub
        End If
        If CDate(sdbgProcesos.Columns("FECHADJU").Value) < CDate(g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaApertura) Then
            irespuesta = oMensajes.PreguntaFecPresentacionMenorApe
            If irespuesta = vbNo Then
                sdbgProcesos.CancelUpdate
                sdbgProcesos.DataChanged = False
                Cancel = True
                If Me.Visible Then sdbgProcesos.SetFocus
                m_bHayErrores = True
                Exit Sub
            End If
        End If
        If IsDate(g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaNecesidad) Then
            If CDate(sdbgProcesos.Columns("FECHADJU").Value) > CDate(g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaNecesidad) Then
                If g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).PermitirAdjDirecta Then
                    oMensajes.FechaPresentMayorQueNecesidad False
                Else
                    oMensajes.FechaPresentMayorQueNecesidad True
                End If
                sdbgProcesos.CancelUpdate
                sdbgProcesos.DataChanged = False
                Cancel = True
                If Me.Visible Then sdbgProcesos.SetFocus
                m_bHayErrores = True
                Exit Sub
            End If
        End If
    Else
        If Not g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).PermitirAdjDirecta Then
            If IsDate(g_oProcesos.Item(sdbgProcesos.Columns("ID").Value).FechaPresentacion) Then
                irespuesta = oMensajes.PreguntaNoHayFecPrimeraPres
                If irespuesta = vbNo Then
                    Cancel = True
                    sdbgProcesos.CancelUpdate
                    sdbgProcesos.DataChanged = False
                    If Me.Visible Then sdbgProcesos.SetFocus
                    m_bHayErrores = True
                    Exit Sub
                End If
            End If
        End If
    End If
    
    If IsDate(sdbgProcesos.Columns("FECHADJU").Value) And IsDate(sdbgProcesos.Columns("FECHA2").Value) Then
        If CDate(Format(sdbgProcesos.Columns("FECHADJU").Value, "short date")) < CDate(Format(sdbgProcesos.Columns("FECHA2").Value, "short date")) Then
            oMensajes.FechaMayorQuePresentacion False
            Cancel = True
            sdbgProcesos.CancelUpdate
            sdbgProcesos.DataChanged = False
            If Me.Visible Then sdbgProcesos.SetFocus
            m_bHayErrores = True
            Exit Sub
        End If
    End If
End Sub
Private Sub sdbgProcesos_DblClick()

    If sdbgProcesos.Rows = 0 Then Exit Sub
    
    m_bHayErrores = False
    sdbgProcesos.Update
    DoEvents
    
    If Not m_bHayErrores Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing) Then
            Screen.MousePointer = vbHourglass
            frmPROCE.sdbcAnyo = sdbgProcesos.Columns("ANYO").Value
            frmPROCE.sdbcGMN1_4Cod = sdbgProcesos.Columns("GMN1").Value
            frmPROCE.GMN1Seleccionado
            frmPROCE.sdbcProceCod = sdbgProcesos.Columns("PROCE").Value
            frmPROCE.ProceSelector1.Seleccion = 1
            frmPROCE.sdbcProceCod_Validate False
            Screen.MousePointer = vbNormal
'            frmEST.PicExclamacion.Visible = False
            Unload Me
        End If
    End If
End Sub

Private Sub sdbgProcesos_Change()
   Dim teserror As TipoErrorSummit

   DoEvents
        
   Set m_oProcesoEnEdicion = Nothing
   
   Set m_oProcesoEnEdicion = g_oProcesos.Item(sdbgProcesos.Columns("ID").Value)

   
End Sub
Private Sub sdbgProcesos_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        If sdbgProcesos.DataChanged = True Then
            sdbgProcesos.CancelUpdate
            sdbgProcesos.DataChanged = False
        End If
    End If
End Sub

Private Sub sdbgProcesos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If m_bModPub Then
        If Not m_bModSubasta Then
            If sdbgProcesos.Columns("SUBASTA").Value = "1" Then
                sdbgProcesos.Columns("FECHA2").Locked = True
            Else
                sdbgProcesos.Columns("FECHA2").Locked = False
            End If
        End If
    End If
End Sub

Private Sub sdbgProcesos_RowLoaded(ByVal Bookmark As Variant)

    If sdbgProcesos.Columns("FECHA2").Value <> "" And sdbgProcesos.Columns("PUB").Value = "1" Then
        If Date >= (CDate(sdbgProcesos.Columns("FECHA2").Value) - gParametrosInstalacion.giAvisoDespublica) And _
            Date >= CDate(sdbgProcesos.Columns("FECHA2").Value) Then
            sdbgProcesos.Columns("FECHA2").CellStyleSet "Aviso"
        End If
    End If
    If sdbgProcesos.Columns("FECHADJU").Value <> "" Then
        If Date >= (CDate(sdbgProcesos.Columns("FECHADJU").Value) - gParametrosInstalacion.giAvisoAdj) And _
            Date >= CDate(sdbgProcesos.Columns("FECHADJU").Value) Then
            sdbgProcesos.Columns("FECHADJU").CellStyleSet "Aviso"
        End If
    End If
End Sub

''' <summary>
''' Carga los contratos proximos a expirar en la grid
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo:=0,3seg.</remarks>
Private Sub CargarContratos()
Dim oContratos As CContratos
Dim rs As Recordset
Dim sPer As String

sPer = ""

If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
    sPer = oUsuarioSummit.Persona.Cod
End If
Set oContratos = oFSGSRaiz.Generar_CContratos

Set rs = oContratos.DevolverProximosExpirar(sPer)

If Not rs.EOF Then
    sdbgContratos.Visible = True
    While Not rs.EOF
        sdbgContratos.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("NOMBRE").Value & Chr(m_lSeparador) & rs("FEC_INI").Value & Chr(m_lSeparador) & rs("FEC_FIN").Value & Chr(m_lSeparador) & rs("PROCESO").Value & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & rs("CONTRATO").Value & Chr(m_lSeparador) & rs("OBSERVADOR").Value
        rs.MoveNext
    Wend
Else
    sdbgContratos.Visible = False
End If
rs.Close
Set rs = Nothing

Set oContratos = Nothing
End Sub

''' <summary>
''' Envia a la copia de procesos cuando pulsa el boton de renegociar.
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
Private Sub sdbgContratos_BtnClick()

If sdbgContratos.col < 0 Then Exit Sub

If sdbgContratos.Columns(sdbgContratos.col).Name = "RENEGOCIAR" Then
    If sdbgContratos.Columns("PROCE").Value <> "" Then
        frmPROCEClonar.g_sProceso = sdbgContratos.Columns("PROCE").Value
        frmPROCEClonar.g_lContratoOrigen = sdbgContratos.Columns("CONTRATO").Value
        Me.Hide
        frmPROCEClonar.Show
    End If
End If


End Sub

