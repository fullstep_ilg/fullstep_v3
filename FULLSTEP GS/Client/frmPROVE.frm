VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPROVE 
   Caption         =   "Proveedores"
   ClientHeight    =   9330
   ClientLeft      =   1275
   ClientTop       =   1185
   ClientWidth     =   9465
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROVE.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9330
   ScaleWidth      =   9465
   Begin TabDlg.SSTab stabGeneral 
      Height          =   9135
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   16113
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Filtro"
      TabPicture(0)   =   "frmPROVE.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraWeb"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame3"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Proveedores"
      TabPicture(1)   =   "frmPROVE.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picEdit"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "picNavigate"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "fraProve"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "stabPROVE"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      Begin TabDlg.SSTab stabPROVE 
         Height          =   7260
         Left            =   -74835
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   1200
         Width           =   9030
         _ExtentX        =   15928
         _ExtentY        =   12806
         _Version        =   393216
         Style           =   1
         Tabs            =   8
         TabsPerRow      =   8
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Datos generales"
         TabPicture(0)   =   "frmPROVE.frx":0182
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "FraMain"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraGeneralPortal"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraGeneral"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "Contactos"
         TabPicture(1)   =   "frmPROVE.frx":019E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "cmdEliminarContacto"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "cmdA�adirContacto"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "cmdDeshacerContacto"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "sdbgContactos"
         Tab(1).ControlCount=   4
         TabCaption(2)   =   "Homologaci�n"
         TabPicture(2)   =   "frmPROVE.frx":01BA
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "picCalif"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "Observaciones"
         TabPicture(3)   =   "frmPROVE.frx":01D6
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "txtObs"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "Archivos adjuntos"
         TabPicture(4)   =   "frmPROVE.frx":01F2
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "picEspProvePortal"
         Tab(4).Control(1)=   "picEsp"
         Tab(4).Control(2)=   "lstvwEspProvePortal"
         Tab(4).Control(3)=   "lstvwEsp"
         Tab(4).Control(4)=   "cmmdEsp"
         Tab(4).Control(5)=   "lblEspProvePortal"
         Tab(4).ControlCount=   6
         TabCaption(5)   =   "Acceso web"
         TabPicture(5)   =   "frmPROVE.frx":020E
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "cmdEliminarWeb"
         Tab(5).Control(0).Enabled=   0   'False
         Tab(5).Control(1)=   "cmdWebCod"
         Tab(5).Control(1).Enabled=   0   'False
         Tab(5).Control(2)=   "Frame4"
         Tab(5).ControlCount=   3
         TabCaption(6)   =   "Datos de integraci�n"
         TabPicture(6)   =   "frmPROVE.frx":022A
         Tab(6).ControlEnabled=   0   'False
         Tab(6).Control(0)=   "fraCodErp"
         Tab(6).ControlCount=   1
         TabCaption(7)   =   "DProveedores Relacionados"
         TabPicture(7)   =   "frmPROVE.frx":0246
         Tab(7).ControlEnabled=   0   'False
         Tab(7).Control(0)=   "ImageList2"
         Tab(7).Control(1)=   "sdbgProveRelac"
         Tab(7).Control(2)=   "cmdEliminarProveRelac"
         Tab(7).Control(2).Enabled=   0   'False
         Tab(7).Control(3)=   "cmdA�adirProveRelac"
         Tab(7).Control(3).Enabled=   0   'False
         Tab(7).Control(4)=   "cmdDeshacerProveRelac"
         Tab(7).Control(4).Enabled=   0   'False
         Tab(7).Control(5)=   "sdbddTipoRel"
         Tab(7).ControlCount=   6
         Begin VB.Frame fraGeneral 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2775
            Left            =   240
            TabIndex        =   54
            Top             =   600
            Width           =   8175
            Begin VB.TextBox txtNIF 
               Height          =   285
               Left            =   1950
               MaxLength       =   20
               TabIndex        =   58
               Top             =   240
               Width           =   2055
            End
            Begin VB.TextBox txtDIR 
               Height          =   285
               Left            =   1950
               MaxLength       =   255
               TabIndex        =   57
               Top             =   585
               Width           =   6210
            End
            Begin VB.TextBox txtCP 
               Height          =   285
               Left            =   1950
               MaxLength       =   20
               TabIndex        =   56
               Top             =   960
               Width           =   1020
            End
            Begin VB.TextBox txtPOB 
               Height          =   285
               Left            =   4035
               MaxLength       =   100
               TabIndex        =   55
               Top             =   960
               Width           =   4125
            End
            Begin VB.TextBox txtURL 
               Height          =   285
               Left            =   1950
               TabIndex        =   65
               Top             =   2400
               Width           =   6210
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
               Height          =   285
               Left            =   1950
               TabIndex        =   59
               Top             =   1320
               Width           =   1035
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
               Height          =   285
               Left            =   2970
               TabIndex        =   60
               Top             =   1320
               Width           =   5205
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   9181
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
               Height          =   285
               Left            =   1950
               TabIndex        =   61
               Top             =   1680
               Width           =   1035
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonDen 
               Height          =   285
               Left            =   2970
               TabIndex        =   62
               Top             =   1680
               Width           =   5205
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 1"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 0"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   9181
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
               Height          =   285
               Left            =   1950
               TabIndex        =   63
               Top             =   2040
               Width           =   1035
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1826
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
               Height          =   285
               Left            =   2970
               TabIndex        =   64
               Top             =   2040
               Width           =   5205
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   9181
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label Label21 
               Caption         =   "Moneda:"
               Height          =   225
               Left            =   300
               TabIndex        =   73
               Top             =   1740
               Width           =   1320
            End
            Begin VB.Label Label2 
               Caption         =   "Direcci�n:"
               Height          =   225
               Left            =   300
               TabIndex        =   72
               Top             =   660
               Width           =   1320
            End
            Begin VB.Label Label3 
               Caption         =   "C�digo postal:"
               Height          =   225
               Left            =   300
               TabIndex        =   71
               Top             =   1020
               Width           =   1320
            End
            Begin VB.Label Label4 
               Caption         =   "Poblaci�n:"
               Height          =   225
               Left            =   3120
               TabIndex        =   70
               Top             =   1020
               Width           =   870
            End
            Begin VB.Label Label6 
               Caption         =   "Provincia:"
               Height          =   225
               Left            =   300
               TabIndex        =   69
               Top             =   2100
               Width           =   1320
            End
            Begin VB.Label Label14 
               Caption         =   "URL:"
               Height          =   195
               Left            =   300
               TabIndex        =   68
               Top             =   2460
               Width           =   1290
            End
            Begin VB.Label Label5 
               Caption         =   "Pa�s:"
               Height          =   225
               Left            =   300
               TabIndex        =   67
               Top             =   1380
               Width           =   1320
            End
            Begin VB.Label Label7 
               Caption         =   "NIF:"
               Height          =   225
               Left            =   300
               TabIndex        =   66
               Top             =   300
               Width           =   1560
            End
         End
         Begin VB.Frame fraGeneralPortal 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2775
            Left            =   255
            TabIndex        =   74
            Top             =   600
            Width           =   8430
            Begin VB.PictureBox Picture1 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   390
               Left            =   6615
               ScaleHeight     =   390
               ScaleWidth      =   1080
               TabIndex        =   76
               Top             =   165
               Width           =   1080
               Begin VB.CheckBox chkPremium 
                  Caption         =   "Premium"
                  Height          =   225
                  Left            =   165
                  MaskColor       =   &H8000000F&
                  TabIndex        =   77
                  Top             =   105
                  Width           =   900
               End
            End
            Begin VB.TextBox lblCodPortal 
               BackColor       =   &H0099CCFF&
               Height          =   285
               Left            =   5160
               Locked          =   -1  'True
               TabIndex        =   75
               Top             =   240
               Width           =   1560
            End
            Begin VB.Label Label20 
               Caption         =   "Provincia:"
               Height          =   225
               Left            =   300
               TabIndex        =   97
               Top             =   2100
               Width           =   1215
            End
            Begin VB.Label Label19 
               Caption         =   "Pa�s:"
               Height          =   225
               Left            =   300
               TabIndex        =   96
               Top             =   1380
               Width           =   1215
            End
            Begin VB.Label Label18 
               Caption         =   "Poblaci�n:"
               Height          =   225
               Left            =   3120
               TabIndex        =   95
               Top             =   1020
               Width           =   870
            End
            Begin VB.Label Label17 
               Caption         =   "C�digo postal:"
               Height          =   225
               Left            =   300
               TabIndex        =   94
               Top             =   1020
               Width           =   1215
            End
            Begin VB.Label Label16 
               Caption         =   "Direcci�n:"
               Height          =   225
               Left            =   300
               TabIndex        =   93
               Top             =   660
               Width           =   1215
            End
            Begin VB.Label Label12 
               Caption         =   "Moneda:"
               Height          =   225
               Left            =   300
               TabIndex        =   92
               Top             =   1740
               Width           =   1215
            End
            Begin VB.Label lblCodigo 
               Caption         =   "C�digo Portal"
               Height          =   255
               Left            =   4065
               TabIndex        =   91
               Top             =   300
               Width           =   1230
            End
            Begin VB.Label lblNIF 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1950
               TabIndex        =   90
               Top             =   240
               UseMnemonic     =   0   'False
               Width           =   2055
            End
            Begin VB.Label lblDIR 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   315
               Left            =   1950
               TabIndex        =   89
               Top             =   585
               Width           =   6210
            End
            Begin VB.Label lblCP 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1950
               TabIndex        =   88
               Top             =   960
               Width           =   1020
            End
            Begin VB.Label lblCodProvi 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1950
               TabIndex        =   87
               Top             =   2040
               Width           =   1020
            End
            Begin VB.Label lblCodMon 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1950
               TabIndex        =   86
               Top             =   1680
               Width           =   1020
            End
            Begin VB.Label lblCodPai 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1950
               TabIndex        =   85
               Top             =   1320
               Width           =   1020
            End
            Begin VB.Label lblPOB 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   4035
               TabIndex        =   84
               Top             =   960
               Width           =   4125
            End
            Begin VB.Label lblDenProvi 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2970
               TabIndex        =   83
               Top             =   2040
               Width           =   5205
            End
            Begin VB.Label lblDenMon 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2970
               TabIndex        =   82
               Top             =   1680
               Width           =   5205
            End
            Begin VB.Label lblDenPai 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2970
               TabIndex        =   81
               Top             =   1320
               Width           =   5205
            End
            Begin VB.Label Label9 
               Caption         =   "NIF:"
               Height          =   225
               Left            =   300
               TabIndex        =   80
               Top             =   300
               Width           =   1065
            End
            Begin VB.Label Label15 
               Caption         =   "URL:"
               Height          =   195
               Left            =   300
               TabIndex        =   79
               Top             =   2460
               Width           =   1290
            End
            Begin VB.Label lblURL 
               BackColor       =   &H0099CCFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1950
               TabIndex        =   78
               Top             =   2400
               Width           =   6210
            End
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddTipoRel 
            Height          =   1575
            Left            =   -73080
            TabIndex        =   145
            Top             =   1800
            Width           =   5055
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            MaxDropDownItems=   10
            _Version        =   196617
            DataMode        =   2
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPROVE.frx":0262
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   4
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   3
            Columns(1).Width=   4604
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   50
            Columns(2).Width=   2117
            Columns(2).Caption=   "DAllow orders"
            Columns(2).Name =   "PED"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   2
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "ID"
            Columns(3).Name =   "ID"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            _ExtentX        =   8916
            _ExtentY        =   2778
            _StockProps     =   77
         End
         Begin VB.CommandButton cmdDeshacerProveRelac 
            Caption         =   "&Deshacer"
            Enabled         =   0   'False
            Height          =   300
            Left            =   -72840
            TabIndex        =   144
            TabStop         =   0   'False
            Top             =   3240
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.CommandButton cmdA�adirProveRelac 
            Caption         =   "A&�adir"
            Height          =   300
            Left            =   -74760
            TabIndex        =   143
            TabStop         =   0   'False
            Top             =   3240
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.CommandButton cmdEliminarProveRelac 
            Caption         =   "&Eliminar"
            Height          =   300
            Left            =   -73800
            TabIndex        =   142
            TabStop         =   0   'False
            Top             =   3240
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.PictureBox picCalif 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2535
            Left            =   -74475
            ScaleHeight     =   2535
            ScaleWidth      =   7170
            TabIndex        =   29
            Top             =   600
            Width           =   7170
            Begin VB.Frame fraCalif1 
               Caption         =   "Calificaci�n 1"
               Height          =   795
               Left            =   60
               TabIndex        =   44
               Top             =   0
               Width           =   7110
               Begin VB.TextBox txtVal1 
                  Height          =   285
                  Left            =   5550
                  TabIndex        =   45
                  Top             =   300
                  Width           =   1185
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCal1Den 
                  Height          =   285
                  Left            =   1680
                  TabIndex        =   46
                  Top             =   300
                  Visible         =   0   'False
                  Width           =   2895
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   4
                  Columns(0).Width=   4392
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   1693
                  Columns(1).Caption=   "Cod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   2249
                  Columns(2).Caption=   "Valor inferior"
                  Columns(2).Name =   "INF"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   2064
                  Columns(3).Caption=   "Valor superior"
                  Columns(3).Name =   "SUP"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  _ExtentX        =   5106
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCal1Cod 
                  Height          =   285
                  Left            =   780
                  TabIndex        =   47
                  Top             =   300
                  Visible         =   0   'False
                  Width           =   900
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   4
                  Columns(0).Width=   1693
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   4392
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   2249
                  Columns(2).Caption=   "Valor inferior"
                  Columns(2).Name =   "INF"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   2064
                  Columns(3).Caption=   "Valor superior"
                  Columns(3).Name =   "SUP"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  _ExtentX        =   1587
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label Label24 
                  Caption         =   "Calif.:"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   50
                  Top             =   375
                  Width           =   585
               End
               Begin VB.Label Label23 
                  Caption         =   "Valor:"
                  Height          =   165
                  Left            =   4905
                  TabIndex        =   49
                  Top             =   360
                  Width           =   570
               End
               Begin VB.Label lblDescRango1 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   780
                  TabIndex        =   48
                  Top             =   300
                  Width           =   3795
               End
            End
            Begin VB.Frame fraCalif2 
               Caption         =   "Calificaci�n 2"
               Height          =   795
               Left            =   60
               TabIndex        =   37
               Top             =   840
               Width           =   7110
               Begin VB.TextBox txtVal2 
                  Height          =   285
                  Left            =   5550
                  TabIndex        =   38
                  Top             =   300
                  Width           =   1185
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCal2Den 
                  Height          =   285
                  Left            =   1680
                  TabIndex        =   39
                  Top             =   300
                  Visible         =   0   'False
                  Width           =   2895
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   4
                  Columns(0).Width=   4392
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   1693
                  Columns(1).Caption=   "Cod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   2249
                  Columns(2).Caption=   "Valor inferior"
                  Columns(2).Name =   "INF"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   2064
                  Columns(3).Caption=   "Valor superior"
                  Columns(3).Name =   "SUP"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  _ExtentX        =   5106
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCal2Cod 
                  Height          =   285
                  Left            =   780
                  TabIndex        =   40
                  Top             =   300
                  Visible         =   0   'False
                  Width           =   900
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   4
                  Columns(0).Width=   1693
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   4392
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   2249
                  Columns(2).Caption=   "Valor inferior"
                  Columns(2).Name =   "INF"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   2064
                  Columns(3).Caption=   "Valor superior"
                  Columns(3).Name =   "SUP"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  _ExtentX        =   1587
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label Label22 
                  Caption         =   "Calif.:"
                  Height          =   195
                  Left            =   135
                  TabIndex        =   43
                  Top             =   360
                  Width           =   615
               End
               Begin VB.Label Label13 
                  Caption         =   "Valor:"
                  Height          =   165
                  Left            =   4905
                  TabIndex        =   42
                  Top             =   360
                  Width           =   570
               End
               Begin VB.Label lblDescRango2 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   780
                  TabIndex        =   41
                  Top             =   300
                  Width           =   3795
               End
            End
            Begin VB.Frame fraCalif3 
               Caption         =   "Calificaci�n 3"
               Height          =   795
               Left            =   60
               TabIndex        =   30
               Top             =   1680
               Width           =   7110
               Begin VB.TextBox txtVal3 
                  Height          =   285
                  Left            =   5550
                  TabIndex        =   31
                  Top             =   300
                  Width           =   1170
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCal3Den 
                  Height          =   285
                  Left            =   1680
                  TabIndex        =   32
                  Top             =   300
                  Visible         =   0   'False
                  Width           =   2895
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   4
                  Columns(0).Width=   4392
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   1693
                  Columns(1).Caption=   "Cod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   2249
                  Columns(2).Caption=   "Valor inferior"
                  Columns(2).Name =   "INF"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   2064
                  Columns(3).Caption=   "Valor superior"
                  Columns(3).Name =   "SUP"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  _ExtentX        =   5106
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcCal3Cod 
                  Height          =   285
                  Left            =   780
                  TabIndex        =   33
                  Top             =   300
                  Visible         =   0   'False
                  Width           =   900
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   4
                  Columns(0).Width=   1693
                  Columns(0).Caption=   "Cod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   4392
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   2249
                  Columns(2).Caption=   "Valor inferior"
                  Columns(2).Name =   "INF"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(3).Width=   2064
                  Columns(3).Caption=   "Valor superior"
                  Columns(3).Name =   "SUP"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  _ExtentX        =   1587
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label Label11 
                  Caption         =   "Calif.:"
                  Height          =   195
                  Left            =   135
                  TabIndex        =   36
                  Top             =   405
                  Width           =   585
               End
               Begin VB.Label Label10 
                  Caption         =   "Valor:"
                  Height          =   165
                  Left            =   4905
                  TabIndex        =   35
                  Top             =   360
                  Width           =   570
               End
               Begin VB.Label lblDescRango3 
                  BackColor       =   &H80000018&
                  BorderStyle     =   1  'Fixed Single
                  Height          =   285
                  Left            =   780
                  TabIndex        =   34
                  Top             =   300
                  Width           =   3795
               End
            End
         End
         Begin VB.CommandButton cmdEliminarContacto 
            Caption         =   "&Eliminar"
            Height          =   300
            Left            =   -73860
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   2700
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.CommandButton cmdA�adirContacto 
            Caption         =   "A&�adir"
            Height          =   300
            Left            =   -74820
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   2700
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.CommandButton cmdDeshacerContacto 
            Caption         =   "&Deshacer"
            Enabled         =   0   'False
            Height          =   300
            Left            =   -72900
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   2700
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Frame Frame4 
            Enabled         =   0   'False
            Height          =   2460
            Left            =   -74415
            TabIndex        =   18
            Top             =   675
            Width           =   7110
            Begin VB.CheckBox ChkCuentBloq 
               Caption         =   "Cuenta bloqueada"
               Height          =   195
               Left            =   1500
               TabIndex        =   22
               Top             =   1980
               Width           =   2400
            End
            Begin VB.TextBox txtCodAcceso 
               Height          =   285
               Left            =   2955
               TabIndex        =   21
               Top             =   360
               Width           =   2160
            End
            Begin VB.TextBox txtPwd 
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   2955
               MaxLength       =   512
               PasswordChar    =   "*"
               TabIndex        =   20
               Top             =   825
               Width           =   3480
            End
            Begin VB.TextBox txtPwdConf 
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   2970
               MaxLength       =   512
               PasswordChar    =   "*"
               TabIndex        =   19
               Top             =   1320
               Width           =   3480
            End
            Begin VB.Label Label26 
               Caption         =   "C�digo de acceso:"
               Height          =   210
               Left            =   1485
               TabIndex        =   25
               Top             =   390
               Width           =   1350
            End
            Begin VB.Label lblPwd 
               Caption         =   "Contrase�a:"
               Height          =   165
               Left            =   1485
               TabIndex        =   24
               Top             =   900
               Width           =   1350
            End
            Begin VB.Label lblPwdConf 
               Caption         =   "Confirmaci�n:"
               Height          =   165
               Left            =   1500
               TabIndex        =   23
               Top             =   1395
               Width           =   1350
            End
         End
         Begin VB.CommandButton cmdWebCod 
            Caption         =   "C�digo"
            Height          =   315
            Left            =   -74415
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   3390
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.CommandButton cmdEliminarWeb 
            Caption         =   "Eliminar"
            Height          =   315
            Left            =   -74430
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   3390
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.PictureBox picEspProvePortal 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   380
            Left            =   -68850
            ScaleHeight     =   375
            ScaleWidth      =   2850
            TabIndex        =   12
            Top             =   1840
            Width           =   2850
            Begin VB.CommandButton cmdCopiarEspPortal 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   1395
               Picture         =   "frmPROVE.frx":027E
               Style           =   1  'Graphical
               TabIndex        =   15
               TabStop         =   0   'False
               Top             =   60
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdSalvarEspPortal 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   1875
               Picture         =   "frmPROVE.frx":02D8
               Style           =   1  'Graphical
               TabIndex        =   14
               TabStop         =   0   'False
               Top             =   60
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdAbrirEspPortal 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   2355
               Picture         =   "frmPROVE.frx":0359
               Style           =   1  'Graphical
               TabIndex        =   13
               TabStop         =   0   'False
               Top             =   60
               UseMaskColor    =   -1  'True
               Width           =   420
            End
         End
         Begin VB.PictureBox picEsp 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   -68850
            ScaleHeight     =   300
            ScaleWidth      =   2850
            TabIndex        =   6
            Top             =   3610
            Width           =   2850
            Begin VB.CommandButton cmdAnyaEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   430
               Picture         =   "frmPROVE.frx":03D5
               Style           =   1  'Graphical
               TabIndex        =   11
               TabStop         =   0   'False
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdEliEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   910
               Picture         =   "frmPROVE.frx":0447
               Style           =   1  'Graphical
               TabIndex        =   10
               TabStop         =   0   'False
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdModifEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   1390
               Picture         =   "frmPROVE.frx":04CA
               Style           =   1  'Graphical
               TabIndex        =   9
               TabStop         =   0   'False
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdSalvarEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   1870
               Picture         =   "frmPROVE.frx":0614
               Style           =   1  'Graphical
               TabIndex        =   8
               TabStop         =   0   'False
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdAbrirEsp 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   2350
               Picture         =   "frmPROVE.frx":0695
               Style           =   1  'Graphical
               TabIndex        =   7
               TabStop         =   0   'False
               Top             =   0
               UseMaskColor    =   -1  'True
               Width           =   420
            End
         End
         Begin VB.TextBox txtObs 
            Height          =   3390
            Left            =   -74820
            Locked          =   -1  'True
            MaxLength       =   4000
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   5
            Top             =   510
            Width           =   8775
         End
         Begin VB.Frame fraCodErp 
            Caption         =   "Empresas con ERP"
            Height          =   3465
            Left            =   -74850
            TabIndex        =   2
            Top             =   435
            Width           =   8880
            Begin SSDataWidgets_B.SSDBGrid sdbgCodERP 
               Height          =   1815
               Left            =   135
               TabIndex        =   3
               Top             =   1545
               Width           =   8610
               _Version        =   196617
               DataMode        =   2
               Col.Count       =   10
               stylesets.count =   3
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777088
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmPROVE.frx":0711
               stylesets(1).Name=   "Normal"
               stylesets(1).BackColor=   -2147483633
               stylesets(1).HasFont=   -1  'True
               BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(1).Picture=   "frmPROVE.frx":072D
               stylesets(2).Name=   "Baja"
               stylesets(2).BackColor=   -2147483633
               stylesets(2).HasFont=   -1  'True
               BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(2).Picture=   "frmPROVE.frx":0749
               UseGroups       =   -1  'True
               AllowDelete     =   -1  'True
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   0
               SelectByCell    =   -1  'True
               HeadStyleSet    =   "Normal"
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               ExtraHeight     =   26
               Groups.Count    =   8
               Groups(0).Width =   4763
               Groups(0).Caption=   "C�digos de ERP"
               Groups(0).HasHeadForeColor=   -1  'True
               Groups(0).HasHeadBackColor=   -1  'True
               Groups(0).HeadForeColor=   16777215
               Groups(0).HeadBackColor=   32896
               Groups(0).Columns.Count=   2
               Groups(0).Columns(0).Width=   2011
               Groups(0).Columns(0).Caption=   "C�digo"
               Groups(0).Columns(0).Name=   "COD"
               Groups(0).Columns(0).DataField=   "Column 0"
               Groups(0).Columns(0).DataType=   8
               Groups(0).Columns(0).FieldLen=   256
               Groups(0).Columns(0).HasBackColor=   -1  'True
               Groups(0).Columns(0).HeadForeColor=   16777215
               Groups(0).Columns(0).HeadBackColor=   32896
               Groups(0).Columns(0).BackColor=   -2147483633
               Groups(0).Columns(1).Width=   2752
               Groups(0).Columns(1).Caption=   "Descripci�n"
               Groups(0).Columns(1).Name=   "DEN"
               Groups(0).Columns(1).DataField=   "Column 1"
               Groups(0).Columns(1).DataType=   8
               Groups(0).Columns(1).FieldLen=   256
               Groups(0).Columns(1).HasBackColor=   -1  'True
               Groups(0).Columns(1).HeadForeColor=   16777215
               Groups(0).Columns(1).HeadBackColor=   32896
               Groups(0).Columns(1).BackColor=   -2147483633
               Groups(1).Width =   3200
               Groups(1).Caption=   "CAMPO1"
               Groups(1).HasHeadForeColor=   -1  'True
               Groups(1).HasHeadBackColor=   -1  'True
               Groups(1).HeadForeColor=   16777215
               Groups(1).HeadBackColor=   32896
               Groups(1).Columns(0).Width=   3201
               Groups(1).Columns(0).Caption=   "Valor"
               Groups(1).Columns(0).Name=   "VALOR1"
               Groups(1).Columns(0).DataField=   "Column 2"
               Groups(1).Columns(0).DataType=   8
               Groups(1).Columns(0).FieldLen=   256
               Groups(1).Columns(0).HasBackColor=   -1  'True
               Groups(1).Columns(0).HeadForeColor=   16777215
               Groups(1).Columns(0).HeadBackColor=   32896
               Groups(1).Columns(0).BackColor=   -2147483633
               Groups(2).Width =   3200
               Groups(2).Caption=   "CAMPO2"
               Groups(2).HasHeadForeColor=   -1  'True
               Groups(2).HasHeadBackColor=   -1  'True
               Groups(2).HeadForeColor=   16777215
               Groups(2).HeadBackColor=   32896
               Groups(2).Columns(0).Width=   3201
               Groups(2).Columns(0).Caption=   "Valor"
               Groups(2).Columns(0).Name=   "VALOR2"
               Groups(2).Columns(0).DataField=   "Column 3"
               Groups(2).Columns(0).DataType=   8
               Groups(2).Columns(0).FieldLen=   256
               Groups(2).Columns(0).HasBackColor=   -1  'True
               Groups(2).Columns(0).HeadForeColor=   16777215
               Groups(2).Columns(0).HeadBackColor=   32896
               Groups(2).Columns(0).BackColor=   -2147483633
               Groups(3).Width =   3200
               Groups(3).Caption=   "CAMPO3"
               Groups(3).HasHeadForeColor=   -1  'True
               Groups(3).HasHeadBackColor=   -1  'True
               Groups(3).HeadForeColor=   16777215
               Groups(3).HeadBackColor=   32896
               Groups(3).Columns(0).Width=   3201
               Groups(3).Columns(0).Caption=   "Valor"
               Groups(3).Columns(0).Name=   "VALOR3"
               Groups(3).Columns(0).DataField=   "Column 4"
               Groups(3).Columns(0).DataType=   8
               Groups(3).Columns(0).FieldLen=   256
               Groups(3).Columns(0).HasBackColor=   -1  'True
               Groups(3).Columns(0).HeadForeColor=   16777215
               Groups(3).Columns(0).HeadBackColor=   32896
               Groups(3).Columns(0).BackColor=   -2147483633
               Groups(4).Width =   3200
               Groups(4).Caption=   "CAMPO4"
               Groups(4).HasHeadForeColor=   -1  'True
               Groups(4).HasHeadBackColor=   -1  'True
               Groups(4).HeadForeColor=   16777215
               Groups(4).HeadBackColor=   32896
               Groups(4).Columns.Count=   2
               Groups(4).Columns(0).Width=   3201
               Groups(4).Columns(0).Caption=   "Valor"
               Groups(4).Columns(0).Name=   "VALOR4"
               Groups(4).Columns(0).DataField=   "Column 5"
               Groups(4).Columns(0).DataType=   8
               Groups(4).Columns(0).FieldLen=   256
               Groups(4).Columns(0).HasBackColor=   -1  'True
               Groups(4).Columns(0).HeadForeColor=   16777215
               Groups(4).Columns(0).HeadBackColor=   32896
               Groups(4).Columns(0).BackColor=   -2147483633
               Groups(4).Columns(1).Width=   1614
               Groups(4).Columns(1).Visible=   0   'False
               Groups(4).Columns(1).Caption=   "BAJALOG"
               Groups(4).Columns(1).Name=   "BAJALOG"
               Groups(4).Columns(1).DataField=   "Column 6"
               Groups(4).Columns(1).DataType=   8
               Groups(4).Columns(1).FieldLen=   256
               Groups(5).Width =   3200
               Groups(5).Caption=   "Forma de pago"
               Groups(5).HasHeadForeColor=   -1  'True
               Groups(5).HasHeadBackColor=   -1  'True
               Groups(5).HeadForeColor=   16777215
               Groups(5).HeadBackColor=   32896
               Groups(5).Columns(0).Width=   3201
               Groups(5).Columns(0).Name=   "PAG"
               Groups(5).Columns(0).DataField=   "Column 7"
               Groups(5).Columns(0).DataType=   8
               Groups(5).Columns(0).FieldLen=   256
               Groups(6).Width =   3200
               Groups(6).Caption=   "V�a de pago"
               Groups(6).HasHeadForeColor=   -1  'True
               Groups(6).HasHeadBackColor=   -1  'True
               Groups(6).HeadForeColor=   16777215
               Groups(6).HeadBackColor=   32896
               Groups(6).Columns(0).Width=   3201
               Groups(6).Columns(0).Name=   "VIA_PAG"
               Groups(6).Columns(0).DataField=   "Column 8"
               Groups(6).Columns(0).DataType=   8
               Groups(6).Columns(0).FieldLen=   256
               Groups(7).Width =   5292
               Groups(7).Caption=   "DContacto"
               Groups(7).HasHeadForeColor=   -1  'True
               Groups(7).HasHeadBackColor=   -1  'True
               Groups(7).HeadForeColor=   16777215
               Groups(7).HeadBackColor=   32896
               Groups(7).Columns(0).Width=   5292
               Groups(7).Columns(0).Name=   "CONTACTO"
               Groups(7).Columns(0).DataField=   "Column 9"
               Groups(7).Columns(0).DataType=   8
               Groups(7).Columns(0).FieldLen=   256
               _ExtentX        =   15187
               _ExtentY        =   3201
               _StockProps     =   79
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin MSComctlLib.ListView lstvwEmpresas 
               Height          =   1245
               Left            =   135
               TabIndex        =   4
               Top             =   255
               Width           =   8610
               _ExtentX        =   15187
               _ExtentY        =   2196
               View            =   3
               Arrange         =   1
               LabelEdit       =   1
               LabelWrap       =   0   'False
               HideSelection   =   0   'False
               HideColumnHeaders=   -1  'True
               HotTracking     =   -1  'True
               _Version        =   393217
               Icons           =   "ImageList1"
               SmallIcons      =   "ImageList1"
               ForeColor       =   -2147483640
               BackColor       =   16777215
               BorderStyle     =   1
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               NumItems        =   1
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "CODERP"
                  Object.Width           =   14993
               EndProperty
            End
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgContactos 
            Height          =   2865
            Left            =   -74820
            TabIndex        =   51
            Top             =   480
            Width           =   23205
            _Version        =   196617
            DataMode        =   1
            GroupHeaders    =   0   'False
            stylesets.count =   5
            stylesets(0).Name=   "InOK"
            stylesets(0).ForeColor=   16777215
            stylesets(0).BackColor=   -2147483633
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPROVE.frx":0765
            stylesets(1).Name=   "Normal"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmPROVE.frx":0781
            stylesets(2).Name=   "IntHeader"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmPROVE.frx":079D
            stylesets(2).AlignmentText=   0
            stylesets(2).AlignmentPicture=   0
            stylesets(3).Name=   "Tan"
            stylesets(3).BackColor=   10079487
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmPROVE.frx":08FA
            stylesets(4).Name=   "IntKO"
            stylesets(4).ForeColor=   16777215
            stylesets(4).BackColor=   255
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmPROVE.frx":0916
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            BalloonHelp     =   0   'False
            MaxSelectedRows =   1
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterPos     =   2
            SplitterVisible =   -1  'True
            Columns.Count   =   17
            Columns(0).Width=   767
            Columns(0).Name =   "INT"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(0).HeadStyleSet=   "IntHeader"
            Columns(0).StyleSet=   "InOK"
            Columns(1).Width=   3200
            Columns(1).Caption=   "Apellidos"
            Columns(1).Name =   "APE"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   100
            Columns(2).Width=   1746
            Columns(2).Caption=   "Nombre"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   20
            Columns(3).Width=   2302
            Columns(3).Caption=   "Departamento"
            Columns(3).Name =   "DEP"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   20
            Columns(3).HasBackColor=   -1  'True
            Columns(3).BackColor=   16777215
            Columns(4).Width=   3200
            Columns(4).Caption=   "Cargo"
            Columns(4).Name =   "CAR"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   100
            Columns(5).Width=   2249
            Columns(5).Caption=   "Tel�fono"
            Columns(5).Name =   "TFNO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   20
            Columns(6).Width=   3200
            Columns(6).Caption=   "Tel�fono2"
            Columns(6).Name =   "TFNO2"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   20
            Columns(7).Width=   3201
            Columns(7).Caption=   "Tel�fono m�vil"
            Columns(7).Name =   "TFNO_MOVIL"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   20
            Columns(8).Width=   3200
            Columns(8).Caption=   "Fax"
            Columns(8).Name =   "FAX"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   20
            Columns(9).Width=   3200
            Columns(9).Caption=   "Mail"
            Columns(9).Name =   "MAIL"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   100
            Columns(10).Width=   3200
            Columns(10).Caption=   "NIF"
            Columns(10).Name=   "NIF"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   2540
            Columns(11).Caption=   "Recibe peticiones"
            Columns(11).Name=   "DEF"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).Style=   2
            Columns(12).Width=   3200
            Columns(12).Caption=   "Aprovisionamiento"
            Columns(12).Name=   "APROV"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(12).Style=   2
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "PORTAL"
            Columns(13).Name=   "PORTAL"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(14).Width=   3200
            Columns(14).Caption=   "Subasta"
            Columns(14).Name=   "SUB"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(14).Style=   2
            Columns(15).Width=   3200
            Columns(15).Caption=   "CALIDAD"
            Columns(15).Name=   "CALIDAD"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(15).Style=   2
            Columns(16).Width=   3200
            Columns(16).Caption=   "PRINCIPAL"
            Columns(16).Name=   "PRINCIPAL"
            Columns(16).DataField=   "Column 16"
            Columns(16).DataType=   8
            Columns(16).FieldLen=   256
            Columns(16).Locked=   -1  'True
            Columns(16).Style=   2
            _ExtentX        =   40931
            _ExtentY        =   5054
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComctlLib.ListView lstvwEspProvePortal 
            Height          =   1245
            Left            =   -74820
            TabIndex        =   52
            Top             =   600
            Width           =   8820
            _ExtentX        =   15558
            _ExtentY        =   2196
            View            =   3
            Arrange         =   1
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            HotTracking     =   -1  'True
            _Version        =   393217
            Icons           =   "ImageList1"
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   10079487
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Fichero"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Tamanyo"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Comentario"
               Object.Width           =   6588
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Fecha"
               Object.Width           =   2867
            EndProperty
         End
         Begin MSComctlLib.ListView lstvwEsp 
            Height          =   1260
            Left            =   -74820
            TabIndex        =   53
            Top             =   2295
            Width           =   8820
            _ExtentX        =   15558
            _ExtentY        =   2223
            View            =   3
            Arrange         =   1
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            HotTracking     =   -1  'True
            _Version        =   393217
            Icons           =   "ImageList1"
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Fichero"
               Object.Width           =   4410
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Tamanyo"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Comentario"
               Object.Width           =   6588
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Fecha"
               Object.Width           =   2867
            EndProperty
         End
         Begin MSComDlg.CommonDialog cmmdEsp 
            Left            =   -68200
            Top             =   200
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
            CancelError     =   -1  'True
            DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgProveRelac 
            Bindings        =   "frmPROVE.frx":0932
            Height          =   3315
            Left            =   -74880
            TabIndex        =   141
            Top             =   480
            Width           =   8850
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            Col.Count       =   7
            stylesets.count =   3
            stylesets(0).Name=   "Boton"
            stylesets(0).BackColor=   16777215
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPROVE.frx":0942
            stylesets(0).AlignmentPicture=   0
            stylesets(1).Name=   "Normal"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmPROVE.frx":0D1A
            stylesets(2).Name=   "Tan"
            stylesets(2).BackColor=   10079487
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmPROVE.frx":0D36
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            BalloonHelp     =   0   'False
            MaxSelectedRows =   0
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterPos     =   1
            Columns.Count   =   7
            Columns(0).Width=   3016
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(1).Width=   5398
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   3200
            Columns(2).Caption=   "Pais"
            Columns(2).Name =   "PAIS"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).StyleSet=   "Boton"
            Columns(3).Width=   2858
            Columns(3).Caption=   "Tipo de relaci�n"
            Columns(3).Name =   "TREL"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).StyleSet=   "Boton"
            Columns(4).Width=   503
            Columns(4).Name =   "INF"
            Columns(4).AllowSizing=   0   'False
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Style=   4
            Columns(4).ButtonsAlways=   -1  'True
            Columns(4).StyleSet=   "Boton"
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "ID"
            Columns(5).Name =   "IDTIPOREL"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "FSP_COD"
            Columns(6).Name =   "FSP_COD"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            _ExtentX        =   15610
            _ExtentY        =   5847
            _StockProps     =   79
            Caption         =   "DProveedores relacionados"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Frame FraMain 
            Height          =   6765
            Left            =   120
            TabIndex        =   146
            Top             =   360
            Width           =   8775
            Begin VB.Frame fraAcepRecepPedidos 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   285
               Left            =   420
               TabIndex        =   176
               Top             =   4440
               Width           =   8055
               Begin VB.CheckBox chkAcepRecepPedidos 
                  Caption         =   "DSolicitar aceptaci�n para la recepci�n de pedidos"
                  Height          =   255
                  Left            =   0
                  TabIndex        =   177
                  Top             =   0
                  Width           =   5835
               End
            End
            Begin VB.Frame fraImpuestos 
               BorderStyle     =   0  'None
               Height          =   285
               Left            =   420
               TabIndex        =   174
               Top             =   4090
               Width           =   8055
               Begin VB.CheckBox chkImpuestos 
                  Caption         =   "Dtomar tipo y valor impuestos"
                  Height          =   255
                  Left            =   0
                  TabIndex        =   175
                  Top             =   0
                  Width           =   7695
               End
            End
            Begin VB.Frame fraComunGeneral 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   1305
               Left            =   240
               TabIndex        =   164
               Top             =   2760
               Width           =   8295
               Begin VB.CheckBox chkAutoFactura 
                  Caption         =   "DPermite Autofactura"
                  Height          =   255
                  Left            =   5460
                  TabIndex        =   165
                  Top             =   975
                  Width           =   2415
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcFormato 
                  Height          =   285
                  Left            =   4020
                  TabIndex        =   166
                  Top             =   960
                  Width           =   1335
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).HasForeColor=   -1  'True
                  Columns(0).HasBackColor=   -1  'True
                  Columns(0).BackColor=   16777215
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "COD"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   2355
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcPagoCod 
                  Height          =   285
                  Left            =   4020
                  TabIndex        =   167
                  Top             =   240
                  Width           =   735
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).HasBackColor=   -1  'True
                  Columns(0).BackColor=   16777215
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "C�digo"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(1).HasForeColor=   -1  'True
                  Columns(1).HasBackColor=   -1  'True
                  Columns(1).BackColor=   16777215
                  _ExtentX        =   1296
                  _ExtentY        =   503
                  _StockProps     =   93
                  ForeColor       =   -2147483642
                  BackColor       =   -2147483643
                  DataFieldToDisplay=   "Column 0"
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcPagoDen 
                  Height          =   285
                  Left            =   4845
                  TabIndex        =   168
                  Top             =   240
                  Width           =   3135
                  _Version        =   196617
                  DataMode        =   2
                  Cols            =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns(0).Width=   3200
                  _ExtentX        =   5530
                  _ExtentY        =   503
                  _StockProps     =   93
                  ForeColor       =   -2147483640
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcViaPagoCod 
                  Height          =   285
                  Left            =   4020
                  TabIndex        =   169
                  Top             =   600
                  Width           =   735
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).HasBackColor=   -1  'True
                  Columns(0).BackColor=   16777215
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "C�digo"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(1).HasForeColor=   -1  'True
                  Columns(1).HasBackColor=   -1  'True
                  Columns(1).BackColor=   16777215
                  _ExtentX        =   1296
                  _ExtentY        =   503
                  _StockProps     =   93
                  ForeColor       =   -2147483642
                  BackColor       =   -2147483643
                  DataFieldToDisplay=   "Column 0"
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcViaPagoDen 
                  Height          =   285
                  Left            =   4860
                  TabIndex        =   170
                  Top             =   600
                  Width           =   3135
                  _Version        =   196617
                  DataMode        =   2
                  Cols            =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns(0).Width=   3200
                  _ExtentX        =   5530
                  _ExtentY        =   503
                  _StockProps     =   93
                  ForeColor       =   -2147483640
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblViaPago 
                  Caption         =   "V�a de pago para pedidos:"
                  Height          =   255
                  Left            =   180
                  TabIndex        =   173
                  Top             =   615
                  Width           =   3615
               End
               Begin VB.Label Label28 
                  Caption         =   "Formato de ficheros para pedidos:"
                  Height          =   255
                  Left            =   180
                  TabIndex        =   172
                  Top             =   975
                  Width           =   3615
               End
               Begin VB.Label Label27 
                  Caption         =   "Forma de pago para los pedidos de cat�logo:"
                  Height          =   255
                  Left            =   180
                  TabIndex        =   171
                  Top             =   240
                  Width           =   3615
               End
            End
            Begin VB.Frame fraProveRelacPri 
               BorderStyle     =   0  'None
               Height          =   1935
               Left            =   105
               TabIndex        =   161
               Top             =   4740
               Width           =   8175
               Begin SSDataWidgets_B.SSDBGrid sdbgProveedoresPri 
                  Bindings        =   "frmPROVE.frx":0D52
                  Height          =   1635
                  Left            =   300
                  TabIndex        =   162
                  Top             =   240
                  Width           =   7815
                  _Version        =   196617
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  GroupHeaders    =   0   'False
                  Col.Count       =   7
                  stylesets.count =   5
                  stylesets(0).Name=   "Boton"
                  stylesets(0).BackColor=   16777215
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmPROVE.frx":0D62
                  stylesets(0).AlignmentPicture=   2
                  stylesets(1).Name=   "FlechaDesenf"
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmPROVE.frx":113A
                  stylesets(1).AlignmentPicture=   2
                  stylesets(2).Name=   "Normal"
                  stylesets(2).HasFont=   -1  'True
                  BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(2).Picture=   "frmPROVE.frx":152B
                  stylesets(3).Name=   "Tan"
                  stylesets(3).BackColor=   10079487
                  stylesets(3).HasFont=   -1  'True
                  BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(3).Picture=   "frmPROVE.frx":1547
                  stylesets(4).Name=   "Flecha"
                  stylesets(4).HasFont=   -1  'True
                  BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(4).Picture=   "frmPROVE.frx":1563
                  stylesets(4).AlignmentPicture=   2
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   1
                  BalloonHelp     =   0   'False
                  MaxSelectedRows =   1
                  HeadStyleSet    =   "Normal"
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  SplitterPos     =   1
                  Columns.Count   =   7
                  Columns(0).Width=   3016
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).Locked=   -1  'True
                  Columns(1).Width=   5398
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(1).Locked=   -1  'True
                  Columns(2).Width=   2858
                  Columns(2).Caption=   "Tipo de relaci�n"
                  Columns(2).Name =   "TREL"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(2).Locked=   -1  'True
                  Columns(2).StyleSet=   "Boton"
                  Columns(3).Width=   741
                  Columns(3).Name =   "INF"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(3).Style=   4
                  Columns(3).ButtonsAlways=   -1  'True
                  Columns(3).StyleSet=   "Boton"
                  Columns(4).Width=   3200
                  Columns(4).Visible=   0   'False
                  Columns(4).Caption=   "ID"
                  Columns(4).Name =   "IDTIPOREL"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(5).Width=   3200
                  Columns(5).Visible=   0   'False
                  Columns(5).Caption=   "FSP_COD"
                  Columns(5).Name =   "FSP_COD"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(6).Width=   741
                  Columns(6).Name =   "CAR"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  Columns(6).Style=   4
                  Columns(6).ButtonsAlways=   -1  'True
                  _ExtentX        =   13785
                  _ExtentY        =   2884
                  _StockProps     =   79
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin VB.Label lblTitProvPri 
                  Caption         =   "DProveedores relacionados a �ste:"
                  Height          =   255
                  Left            =   300
                  TabIndex        =   163
                  Top             =   0
                  Width           =   3615
               End
            End
         End
         Begin MSComctlLib.ImageList ImageList2 
            Left            =   -75000
            Top             =   0
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   16
            ImageHeight     =   17
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   1
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVE.frx":1944
                  Key             =   "CHECK"
                  Object.Tag             =   "CHECK"
               EndProperty
            EndProperty
         End
         Begin VB.Label lblEspProvePortal 
            Caption         =   "Archivos disponibles en el portal:"
            Height          =   255
            Left            =   -74820
            TabIndex        =   98
            Top             =   360
            Width           =   4000
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Datos generales"
         Height          =   1815
         Left            =   360
         TabIndex        =   147
         Top             =   600
         Width           =   7755
         Begin VB.CommandButton cmdAyudaFiltro 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   7.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   6840
            Picture         =   "frmPROVE.frx":1CB9
            Style           =   1  'Graphical
            TabIndex        =   160
            Top             =   240
            Width           =   225
         End
         Begin VB.TextBox txtFiltroDen 
            Height          =   285
            Left            =   1950
            MaxLength       =   100
            TabIndex        =   150
            Top             =   600
            Width           =   4725
         End
         Begin VB.TextBox txtFiltroCod 
            Height          =   285
            Left            =   4920
            MaxLength       =   20
            TabIndex        =   149
            Top             =   240
            Width           =   1770
         End
         Begin VB.TextBox txtFiltroVAT 
            Height          =   285
            Left            =   1950
            MaxLength       =   20
            TabIndex        =   148
            Top             =   240
            Width           =   1770
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcFiltroPaisCod 
            Height          =   285
            Left            =   1950
            TabIndex        =   151
            Top             =   960
            Width           =   945
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1667
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcFiltroPaisDen 
            Height          =   285
            Left            =   2910
            TabIndex        =   152
            Top             =   960
            Width           =   3765
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcFiltroProviCod 
            Height          =   285
            Left            =   1950
            TabIndex        =   153
            Top             =   1320
            Width           =   945
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1667
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcFiltroProviDen 
            Height          =   285
            Left            =   2910
            TabIndex        =   154
            Top             =   1320
            Width           =   3765
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label Label34 
            Caption         =   "DDen:"
            Height          =   225
            Left            =   600
            TabIndex        =   159
            Top             =   600
            Width           =   1215
         End
         Begin VB.Label Label33 
            Caption         =   "DC�digo:"
            Height          =   225
            Left            =   4080
            TabIndex        =   158
            Top             =   300
            Width           =   720
         End
         Begin VB.Label Label32 
            Caption         =   "DProvincia:"
            Height          =   225
            Left            =   600
            TabIndex        =   157
            Top             =   1380
            Width           =   1320
         End
         Begin VB.Label Label31 
            Caption         =   "DPa�s:"
            Height          =   225
            Left            =   600
            TabIndex        =   156
            Top             =   1020
            Width           =   1320
         End
         Begin VB.Label Label25 
            Caption         =   "DNIF:"
            Height          =   225
            Left            =   600
            TabIndex        =   155
            Top             =   300
            Width           =   1320
         End
      End
      Begin VB.Frame fraProve 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   750
         Left            =   -74820
         TabIndex        =   120
         Top             =   360
         Width           =   9090
         Begin VB.TextBox txtDenProve 
            Height          =   285
            Left            =   2505
            MaxLength       =   100
            TabIndex        =   126
            Top             =   270
            Visible         =   0   'False
            Width           =   5250
         End
         Begin VB.TextBox txtCodProve 
            Height          =   285
            Left            =   900
            TabIndex        =   124
            Top             =   270
            Visible         =   0   'False
            Width           =   1560
         End
         Begin VB.CommandButton cmdBuscar 
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7785
            Picture         =   "frmPROVE.frx":1EEA
            Style           =   1  'Graphical
            TabIndex        =   122
            TabStop         =   0   'False
            Top             =   270
            Width           =   315
         End
         Begin VB.PictureBox PicIntegracion 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   8280
            Picture         =   "frmPROVE.frx":1F78
            ScaleHeight     =   255
            ScaleWidth      =   330
            TabIndex        =   121
            Top             =   270
            Width           =   325
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   900
            TabIndex        =   123
            Top             =   270
            Width           =   1575
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2778
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   2505
            TabIndex        =   125
            Top             =   270
            Width           =   5250
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   9260
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo:"
            Height          =   195
            Left            =   180
            TabIndex        =   129
            Top             =   330
            Width           =   735
         End
         Begin VB.Label lblDenProve 
            BackColor       =   &H0099CCFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   2505
            TabIndex        =   128
            Top             =   270
            Width           =   5250
         End
         Begin VB.Line LinIntegracion 
            BorderColor     =   &H8000000C&
            X1              =   8190
            X2              =   8190
            Y1              =   105
            Y2              =   715
         End
         Begin VB.Label lblEstInt 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   8640
            TabIndex        =   127
            Top             =   300
            Width           =   240
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Selecci�n por material"
         Height          =   2595
         Left            =   375
         TabIndex        =   106
         Top             =   2460
         Width           =   7680
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   6240
            Picture         =   "frmPROVE.frx":20C5
            Style           =   1  'Graphical
            TabIndex        =   107
            Top             =   240
            Width           =   345
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   1950
            TabIndex        =   108
            Top             =   720
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1164
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3889
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
            Height          =   285
            Left            =   1950
            TabIndex        =   109
            Top             =   1170
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
            Height          =   285
            Left            =   1950
            TabIndex        =   110
            Top             =   1620
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
            Height          =   285
            Left            =   1950
            TabIndex        =   111
            Top             =   2070
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
            Height          =   285
            Left            =   3060
            TabIndex        =   112
            Top             =   720
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
            Height          =   285
            Left            =   3060
            TabIndex        =   113
            Top             =   1170
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
            Height          =   285
            Left            =   3060
            TabIndex        =   114
            Top             =   1620
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
            Height          =   285
            Left            =   3060
            TabIndex        =   115
            Top             =   2070
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "Grupo:"
            Height          =   225
            Left            =   660
            TabIndex        =   119
            Top             =   2130
            Width           =   1185
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "Comodity:"
            Height          =   225
            Left            =   660
            TabIndex        =   118
            Top             =   735
            Width           =   1260
         End
         Begin VB.Label lblGMN2_4 
            Caption         =   "Familia:"
            Height          =   225
            Left            =   660
            TabIndex        =   117
            Top             =   1200
            Width           =   1185
         End
         Begin VB.Label lblGMN3_4 
            Caption         =   "Subfamilia:"
            Height          =   225
            Left            =   660
            TabIndex        =   116
            Top             =   1665
            Width           =   1185
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Selecci�n por equipo de compra"
         Height          =   915
         Left            =   375
         TabIndex        =   101
         Top             =   5100
         Width           =   7680
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
            Height          =   285
            Left            =   1950
            TabIndex        =   102
            Top             =   360
            Width           =   1065
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1455
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5345
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 3"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
            Height          =   285
            Left            =   3060
            TabIndex        =   103
            Top             =   360
            Width           =   3525
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6218
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label Label8 
            Caption         =   "Equipo:"
            Height          =   225
            Left            =   180
            TabIndex        =   105
            Top             =   405
            Width           =   1080
         End
         Begin VB.Label lblEqp 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label3"
            Height          =   285
            Left            =   1950
            TabIndex        =   104
            Top             =   360
            Visible         =   0   'False
            Width           =   4605
         End
      End
      Begin VB.Frame fraWeb 
         Caption         =   "Selecci�n por acceso web"
         Height          =   885
         Left            =   375
         TabIndex        =   99
         Top             =   6090
         Width           =   7695
         Begin VB.CheckBox ChkSoloProveWeb 
            Caption         =   "S�lo proveedores con acceso web"
            Height          =   195
            Left            =   1995
            TabIndex        =   100
            Top             =   405
            Width           =   3570
         End
      End
      Begin VB.PictureBox picNavigate 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   -74880
         ScaleHeight     =   495
         ScaleWidth      =   9135
         TabIndex        =   130
         TabStop         =   0   'False
         Top             =   8520
         Width           =   9135
         Begin VB.CommandButton cmdDatosPortal 
            Caption         =   "Datos &Port"
            Height          =   345
            Left            =   7965
            TabIndex        =   137
            TabStop         =   0   'False
            Top             =   90
            Width           =   1005
         End
         Begin VB.CommandButton cmdCodigo 
            Caption         =   "C�&digo"
            Enabled         =   0   'False
            Height          =   345
            Left            =   4275
            TabIndex        =   136
            TabStop         =   0   'False
            Top             =   90
            Width           =   1005
         End
         Begin VB.CommandButton cmdActualizar 
            Caption         =   "&Restaurar"
            Enabled         =   0   'False
            Height          =   345
            Left            =   5535
            TabIndex        =   135
            TabStop         =   0   'False
            Top             =   90
            Width           =   1005
         End
         Begin VB.CommandButton cmdListado 
            Caption         =   "&Listado"
            Height          =   345
            Left            =   6750
            TabIndex        =   134
            TabStop         =   0   'False
            Top             =   90
            Width           =   1005
         End
         Begin VB.CommandButton cmdEliminar 
            Caption         =   "&Eliminar"
            Enabled         =   0   'False
            Height          =   345
            Left            =   3015
            TabIndex        =   133
            TabStop         =   0   'False
            Top             =   90
            Width           =   1005
         End
         Begin VB.CommandButton cmdModificar 
            Caption         =   "&Modificar"
            Enabled         =   0   'False
            Height          =   345
            Left            =   1800
            TabIndex        =   132
            TabStop         =   0   'False
            Top             =   90
            Width           =   1005
         End
         Begin VB.CommandButton cmdA�adir 
            Caption         =   "&A�adir"
            Enabled         =   0   'False
            Height          =   345
            Left            =   0
            TabIndex        =   131
            TabStop         =   0   'False
            Top             =   90
            Width           =   1635
         End
      End
      Begin VB.PictureBox picEdit 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   -73200
         ScaleHeight     =   495
         ScaleWidth      =   4590
         TabIndex        =   138
         Top             =   8520
         Width           =   4590
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "&Cancelar"
            Height          =   345
            Left            =   2250
            TabIndex        =   140
            Top             =   75
            Width           =   1005
         End
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            Height          =   345
            Left            =   1110
            TabIndex        =   139
            Top             =   75
            Width           =   1005
         End
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVE.frx":2134
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVE.frx":228E
            Key             =   "ERP"
            Object.Tag             =   "ERP"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPROVE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmCONT
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 18/01/1999 (Alfredo Magallon)

Option Explicit

''' Contacto en edicion
Private m_oContactoEnEdicion As CContacto
Private m_oUsuario As CUsuario
Private m_oIBAseDatosEnEdicion As IBaseDatos
Private m_bCancelCheckContacto As Boolean

''' Proveedor relacionado en edici�n
Private m_oProveRelacEnEdicion As CProveedor
Private m_bRowColChangeProveRelac As Boolean
Private m_bBeforeRowColChangeProveRelac As Boolean

'Variables para contener las coleccciones de las combos
Private m_oFiltroPaisSeleccionado As CPais

''' Control de errores
Private m_bModError As Boolean
Private m_bAnyaError As Boolean
Private m_bValError As Boolean
Private m_bCalif As Boolean
Private m_TelfMailError As Boolean

''' Variables de control
Private m_bModoEdicion As Boolean
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean
Private m_bEliminarWeb As Boolean

' Variables de seguridad
Private m_bAnyadir As Boolean
Private m_bEliminar As Boolean
Private m_bModifDatGen As Boolean
Private m_bModifCalif As Boolean
Private m_bREqp As Boolean
Private m_bRMat As Boolean
Private m_bConsultaEspPortalProve As Boolean 'Permiso para ver y agregar archivos adjuntos del Portal
Private m_bModifEspProve As Boolean 'Permiso para modificar archivos adjuntos
Public g_bConsDatWeb As Boolean
Public g_bModifDatweb As Boolean

' Variables de idioma
Private m_sIdioma() As String
Private m_sIdiMail As String
Private m_sEliminarCodAcceso As String
Private m_skb As String
 
' Variables que indican la denominacion y la abrev. de cada nivel de la estructura de materiales
Public g_sDEN_GMN1 As String
Public g_sDEN_GMN2 As String
Public g_sDEN_GMN3 As String
Public g_sDEN_GMN4 As String

'Variables para materiales
Private m_oGruposMN1 As CGruposMatNivel1
Private m_oGruposMN2 As CGruposMatNivel2
Private m_oGruposMN3 As CGruposMatNivel3
Private m_oGruposMN4 As CGruposMatNivel4
Private m_oIMAsig As IMaterialAsignado

' variables para las selecciones en combos

Public g_oProveSeleccionado As CProveedor
Private m_oIBaseDatos As IBaseDatos
Private m_oPaisSeleccionado As CPais
Private m_oPagoSeleccionado As CPago
'_______________________________________________________________________
Private m_oViaPagoSeleccionada As CViaPago
'�����������������������������������������������������������������������
'variable para la coleccion de proveedores
Private m_oProves As CProveedores

' Variables para el control de las acciones
Private m_Accion As AccionesSummit
Private m_AccionCont As AccionesSummit
Private m_AccionProveRelac As AccionesSummit

' Variable necesaria para saber el bookmark de la grid en todo momento
Private m_lIndiceContactos As Long

'Variables para contener los Paises, provincias y monedas
Private m_oPaises As CPaises
Private m_oGMN1Seleccionado As CGrupoMatNivel1
Private m_oGMN2Seleccionado As CGrupoMatNivel2
Private m_oGMN3Seleccionado As CGrupoMatNivel3
Private m_oGMN4Seleccionado As CGrupoMatNivel4
'Variable para la colecci�n de pagos
Private m_oPagos As CPagos
'_______________________________________________________________________
Private m_oViaPagos As CViaPagos
'�����������������������������������������������������������������������
'Variables para las calificaciones de los proveedores
Private m_oCalificaciones1 As CCalificaciones
Private m_oCalificaciones2 As CCalificaciones
Private m_oCalificaciones3 As CCalificaciones
Private m_oCalif1Seleccionada As CCalificacion
Private m_oCalif2Seleccionada As CCalificacion
Private m_oCalif3Seleccionada As CCalificacion

Private m_oEqps As CEquipos
Private m_oEqpSeleccionado As CEquipo

Private m_oMonedas As CMonedas
''' Combos
Private m_bRespetarComboProve As Boolean
Private m_bPaiRespetarCombo As Boolean
Private m_bPaiCargarComboDesde As Boolean
Private m_bMonRespetarCombo As Boolean
Private m_bMonCargarComboDesde As Boolean
Private m_bProviRespetarCombo As Boolean
Private m_bProviCargarComboDesde As Boolean
Private m_bGMN1RespetarCombo As Boolean
Private m_bGMN1CargarComboDesde As Boolean
Private m_bGMN2RespetarCombo As Boolean
Private m_bGMN2CargarComboDesde As Boolean
Private m_bGMN3RespetarCombo As Boolean
Private m_bGMN3CargarComboDesde As Boolean
Private m_bGMN4RespetarCombo As Boolean
Private m_bGMN4CargarComboDesde As Boolean
Private m_bEqpRespetarCombo As Boolean
Private m_bEqpCargarComboDesde As Boolean
Private m_bCal1RespetarCombo As Boolean
Private m_bCal1CargarComboDesde As Boolean
Private m_bCal2RespetarCombo As Boolean
Private m_bCal2CargarComboDesde As Boolean
Private m_bCal3RespetarCombo As Boolean
Private m_bCal3CargarComboDesde As Boolean
Private m_bPagoRespetarCombo As Boolean
'_______________________________________________________________________
Private m_bViaPagoRespetarCombo As Boolean
'�����������������������������������������������������������������������
Private m_blnCargandoCalif As Boolean
Private m_sContrase�a As String
Private m_sFormato As String
Private m_bCambiandoModo As Boolean

'Seleccionar archivo adjunto de especificaci�n
Private m_sIdiSelecAdjunto As String
'Todos los archivos
Private m_sIdiTodosArchivos As String
'el Archivo:
Private m_sIdiElArchivo As String
'Guardar archivo de especificaci�n
Private m_sIdiGuardar As String
'Tipo original
Private m_sIdiTipoOrig As String
'Archivo
Private m_sIdiArchivo As String
' Estado integraci�n
Private m_sIdiSincronizado As String
Private m_sIdiNoSincronizado As String

Private m_sIdiValor As String

Public g_sComentario As String
Public g_bCancelarEsp As Boolean
'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private m_sArFileNames() As String

Private sIdiElProveRelac As String
Private sIdiEliminacionMultiple As String
Private m_oTiposRel As CRelacTipos

' Variables para el manejo de combos
Private m_bFiltroPaiRespetarCombo As Boolean
Private m_bFiltroPaiCargarComboDesde As Boolean
Private m_bFiltroProviRespetarCombo As Boolean
Private m_bFiltroProviCargarComboDesde As Boolean

Private Sub Arrange()
    Dim b As Boolean
    Dim dTop As Single
    
    On Error Resume Next
    
    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 9540 Then   'de tama�o de la ventana
            Me.Width = 9580       'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 9500 Then  'cuando no se maximiza ni
            Me.Height = 9550      'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width
        
    
    If Me.Width <= 1000 Then Exit Sub
    
    If Me.Height <= 2500 Then Exit Sub
    
    If Me.Width >= 205 Then sTabGeneral.Width = Me.Width - 205
    If Me.Height >= 525 Then sTabGeneral.Height = Me.Height - 560
    
    If sTabGeneral.Width >= 350 Then
        stabProve.Width = sTabGeneral.Width - 350
        fraGeneralPortal.Width = stabProve.Width - 400
        fraGeneral.Width = stabProve.Width - 400
        fraComunGeneral.Width = stabProve.Width - 400
        fraImpuestos.Width = stabProve.Width - 400
        fraProveRelacPri.Width = stabProve.Width - 400
        FraMain.Width = stabProve.Width - 200
    End If
    If sTabGeneral.Width >= 1750 Then stabProve.Height = sTabGeneral.Height - 1650
    If stabProve.Width >= 325 Then sdbgContactos.Width = stabProve.Width - 325
    
    If m_bModoEdicion = False Then
        If stabProve.Height >= 725 Then sdbgContactos.Height = stabProve.Height - 700
    Else
        If stabProve.Height >= 1025 Then sdbgContactos.Height = stabProve.Height - 1025
    End If
    
    FraMain.Height = stabProve.Height - 500
    sdbgContactos.Columns(1).Width = sdbgContactos.Width * 20 / 100
    sdbgContactos.Columns(2).Width = sdbgContactos.Width * 20 / 100
    sdbgContactos.Columns(3).Width = sdbgContactos.Width * 20 / 100
    sdbgContactos.Columns(4).Width = sdbgContactos.Width * 16 / 100
    sdbgContactos.Columns(5).Width = sdbgContactos.Width * 17 / 100
    sdbgContactos.Columns(6).Width = sdbgContactos.Width * 15 / 100
    
    If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove) Then
        sdbgContactos.Columns(0).Width = sdbgContactos.Width * 5 / 100
        sdbgContactos.Columns(7).Width = sdbgContactos.Width * 15 / 100
    Else
        sdbgContactos.Columns(0).Visible = False
        sdbgContactos.Columns(7).Width = sdbgContactos.Width * 20 / 100
    End If

    cmdA�adirContacto.Top = sdbgContactos.Top + sdbgContactos.Height + 100
    cmdEliminarContacto.Top = cmdA�adirContacto.Top
    cmdDeshacerContacto.Top = cmdEliminarContacto.Top
    If stabProve.Width >= 335 Then
        txtObs.Width = stabProve.Width - 335
    End If
    If stabProve.Height >= 725 Then
        stabProve.Height = stabProve.Height - 100
        txtObs.Height = stabProve.Height - 725
    End If
    
    fraGeneral.Top = 625
    fraGeneralPortal.Top = 625
    Me.fraComunGeneral.Top = Me.fraGeneral.Height + 50
    dTop = Me.fraComunGeneral.Top + Me.fraComunGeneral.Height + 20
    If Me.fraImpuestos.Visible Then
        Me.fraImpuestos.Top = dTop
        dTop = fraImpuestos.Top + fraImpuestos.Height + 50
    End If
    If fraAcepRecepPedidos.Visible Then
        fraAcepRecepPedidos.Top = dTop
        dTop = fraAcepRecepPedidos.Top + fraAcepRecepPedidos.Height + 50
    End If
    fraProveRelacPri.Top = dTop
    
    FraMain.Top = 325
    picNavigate.Top = stabProve.Top + stabProve.Height
    picEdit.Top = picNavigate.Top
    picEdit.Left = (stabProve.Width / 2) - (picEdit.Width / 2)
    fraProve.Width = stabProve.Width
    b = False
    If gParametrosGenerales.giINSTWEB <> ConPortal Or Not m_bConsultaEspPortalProve Then
        OcultarEspPortal
    Else
        If Not g_oProveSeleccionado Is Nothing Then
            If IsNull(g_oProveSeleccionado.CodPortal) Then
                OcultarEspPortal
                b = True
            End If
        End If
        If Not b Then
            If stabProve.Height > 5000 Then
                lstvwEspProvePortal.Height = stabProve.Height / 2.7
            Else
                lstvwEspProvePortal.Height = stabProve.Height / 3 - 100
            End If
            lstvwEspProvePortal.Width = stabProve.Width - 300
            picEspProvePortal.Top = lstvwEspProvePortal.Top + lstvwEspProvePortal.Height + 20
            If stabProve.Tab = 4 Then
                picEspProvePortal.Left = lstvwEspProvePortal.Width - (cmdAbrirEspPortal.Width * 6) - 100
            End If
            lstvwEsp.Top = picEspProvePortal.Top + picEspProvePortal.Height
            lstvwEsp.Height = stabProve.Height - lstvwEspProvePortal.Height - picEsp.Height - picEspProvePortal.Height - 835
            lstvwEsp.Width = lstvwEspProvePortal.Width
        End If
    End If
    
    
    fraCodERP.Width = stabProve.Width - 250
    
    lstvwEmpresas.Width = fraCodERP.Width - 250
    lstvwEmpresas.ColumnHeaders(1).Width = lstvwEmpresas.Width - 150
    sdbgCodERP.Width = fraCodERP.Width - 250
    
        Dim iNumCols  As Integer
        iNumCols = 2
        If Me.sdbgCodERP.Groups(1).Visible Then
            iNumCols = iNumCols + 1
        End If
        If Me.sdbgCodERP.Groups(2).Visible Then
            iNumCols = iNumCols + 1
        End If
        If Me.sdbgCodERP.Groups(3).Visible Then
            iNumCols = iNumCols + 1
        End If
        If Me.sdbgCodERP.Groups(4).Visible Then
            iNumCols = iNumCols + 1
        End If
        If Me.sdbgCodERP.Groups(7).Visible Then
            iNumCols = iNumCols + 1
        End If
        sdbgCodERP.Groups(0).Width = (sdbgCodERP.Width - 570) * (0.3 + (0.7 / 6) * (6 - iNumCols))
        sdbgCodERP.Columns("COD").Width = (sdbgCodERP.Width - 570) * 0.1
        sdbgCodERP.Columns("DEN").Width = (sdbgCodERP.Width - 570) * (0.2 + (0.7 / 6) * (6 - iNumCols))
        sdbgCodERP.Groups(1).Width = (sdbgCodERP.Width - 570) * (1 - (0.3 + (0.7 / 6) * (6 - iNumCols))) / iNumCols
        sdbgCodERP.Groups(2).Width = (sdbgCodERP.Width - 570) * (1 - (0.3 + (0.7 / 6) * (6 - iNumCols))) / iNumCols
        sdbgCodERP.Groups(3).Width = (sdbgCodERP.Width - 570) * (1 - (0.3 + (0.7 / 6) * (6 - iNumCols))) / iNumCols
        sdbgCodERP.Groups(4).Width = (sdbgCodERP.Width - 570) * (1 - (0.3 + (0.7 / 6) * (6 - iNumCols))) / iNumCols
        sdbgCodERP.Groups(5).Width = (sdbgCodERP.Width - 570) * (1 - (0.3 + (0.7 / 6) * (6 - iNumCols))) / iNumCols
        sdbgCodERP.Groups(6).Width = (sdbgCodERP.Width - 570) * (1 - (0.3 + (0.7 / 6) * (6 - iNumCols))) / iNumCols
        sdbgCodERP.Groups(7).Width = (sdbgCodERP.Width - 570) * (1 - (0.3 + (0.7 / 6) * (6 - iNumCols))) / iNumCols
        sdbgCodERP.Columns("VALOR1").Width = sdbgCodERP.Groups(1).Width
        sdbgCodERP.Columns("VALOR2").Width = sdbgCodERP.Groups(2).Width
        sdbgCodERP.Columns("VALOR3").Width = sdbgCodERP.Groups(3).Width
        sdbgCodERP.Columns("VALOR4").Width = sdbgCodERP.Groups(4).Width
        sdbgCodERP.Columns("PAG").Width = sdbgCodERP.Groups(5).Width
        sdbgCodERP.Columns("VIA_PAG").Width = sdbgCodERP.Groups(6).Width
        sdbgCodERP.Columns("CONTACTO").Width = sdbgCodERP.Groups(7).Width
    Me.fraCodERP.Height = stabProve.Height - 550
    
    Me.lstvwEmpresas.Height = (Me.fraCodERP.Height - 540) / 2
    Me.sdbgCodERP.Top = Me.lstvwEmpresas.Top + Me.lstvwEmpresas.Height + 100
    Me.sdbgCodERP.Height = (Me.fraCodERP.Height - 540) / 2

    
    If stabProve.Tab = 4 Then
        picEsp.Left = lstvwEsp.Width - (cmdAbrirEsp.Width * 6) - 90
    End If
    
    lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.2
    lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.1
    lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.5
    lstvwEsp.ColumnHeaders(4).Width = lstvwEsp.Width * 0.19
    
    lstvwEspProvePortal.ColumnHeaders(1).Width = lstvwEspProvePortal.Width * 0.2
    lstvwEspProvePortal.ColumnHeaders(2).Width = lstvwEspProvePortal.Width * 0.1
    lstvwEspProvePortal.ColumnHeaders(3).Width = lstvwEspProvePortal.Width * 0.5
    lstvwEspProvePortal.ColumnHeaders(4).Width = lstvwEspProvePortal.Width * 0.19

    picEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 60
    
    
    
    'Redimensionamiento de la grid de relaciones
    If stabProve.Width >= 325 Then sdbgProveRelac.Width = stabProve.Width - 225
    
    If m_bModoEdicion = False Then
        If stabProve.Height >= 725 Then sdbgProveRelac.Height = stabProve.Height - 600
    Else
        If stabProve.Height >= 1025 Then sdbgProveRelac.Height = stabProve.Height - 925
    End If
    
    cmdA�adirProveRelac.Top = sdbgProveRelac.Top + sdbgProveRelac.Height + 100
    cmdEliminarProveRelac.Top = cmdA�adirProveRelac.Top
    cmdDeshacerProveRelac.Top = cmdEliminarProveRelac.Top
    
    sdbgProveRelac.Columns(0).Width = sdbgProveRelac.Width * 10 / 100
    sdbgProveRelac.Columns(1).Width = sdbgProveRelac.Width * 50 / 100
    sdbgProveRelac.Columns(2).Width = sdbgProveRelac.Width * 25 / 100
    sdbgProveRelac.Columns(3).Width = sdbgProveRelac.Width * 8 / 100
    sdbgProveRelac.Columns(4).Width = sdbgProveRelac.Width * 2.5 / 100
    
    cmdWebCod.Top = stabProve.Height - 500
    cmdEliminarWeb.Top = cmdWebCod.Top
    
End Sub
Private Sub OcultarEspPortal()
        lblEspProvePortal.Visible = False
        lstvwEspProvePortal.Visible = False
        picEspProvePortal.Visible = False
        lstvwEsp.Height = stabProve.Height - 850
        lstvwEsp.Top = lblEspProvePortal.Top
        lstvwEsp.Width = stabProve.Width - 300
        picEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 60
        
End Sub

''' <summary>
''' El check "Tomar Impuestos de facturas" solo puede estar activo y chequeado si el check "Autofactura" esta activo.
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub chkAutoFactura_Click()
    Me.fraImpuestos.Enabled = (Me.chkAutoFactura.Value = vbChecked) And (Me.fraComunGeneral.Enabled)
    If Me.chkAutoFactura.Value = vbUnchecked Then
        Me.chkImpuestos.Value = Unchecked
        Me.fraImpuestos.Enabled = False
    Else
        Me.fraImpuestos.Enabled = True
    End If
End Sub

Private Sub cmdAbrirEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
    
On Error GoTo Cancelar:
    
    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    Set m_oIBAseDatosEnEdicion = g_oProveSeleccionado
        
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion(False)
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set m_oIBAseDatosEnEdicion = Nothing
        Exit Sub
    End If
    
    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
                
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing

        ' Cargamos el contenido en la esp.
        Set oEsp = g_oProveSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspProveedor)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
        
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspProveedor, oEsp.DataSize, sFileName
                
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
    
    Set m_oIBAseDatosEnEdicion = Nothing
    
Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    Set oEsp = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing
End Sub

Private Sub cmdAbrirEspPortal_Click()
Dim Item As MSComctlLib.listItem
Dim oEspPortal As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
    
On Error GoTo Cancelar:
    
    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    Set m_oIBAseDatosEnEdicion = g_oProveSeleccionado
        
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion(False)
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set m_oIBAseDatosEnEdicion = Nothing
        Exit Sub
    End If
    
    Set Item = lstvwEspProvePortal.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
                
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing

        ' Cargamos el contenido en la esp.
        Set oEspPortal = g_oProveSeleccionado.EspecificacionesProvePortal.Item(CStr(lstvwEspProvePortal.selectedItem.Tag))
        teserror = oEspPortal.ComenzarLecturaData(TipoEspecificacion.EspProveedor, True)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEspPortal = Nothing
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
        
        oEspPortal.LeerAdjuntoCia oEspPortal.Id, oEspPortal.Proveedor.IDPortal, oEspPortal.DataSize, sFileName
                
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
    
    Set m_oIBAseDatosEnEdicion = Nothing
    Set oEspPortal = Nothing
    
Cancelar:
On Error Resume Next
    If err.Number = 70 Then
        Resume Next
    End If
    
    Unload frmProgreso
    
    Set oEspPortal = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing

End Sub

Private Sub cmdAceptar_Click()
    Dim oIMatAsig As IMaterialAsignado
    Dim teserror As TipoErrorSummit
    Dim oIEqpAsig As IEquiposAsigAProveedor
    Dim iCont As Integer
    Dim inum As Integer
    Dim oProvesVinculados As CProveedores
    Dim oProveERPs As CProveERPs
    Dim sProvesVinc As String
    Dim oProve  As CProveedor

    ' Si se ha producido algun error anyadiendo contactos salimos
    If m_bValError Then Exit Sub
        
    Select Case m_Accion
        Case AccionesSummit.ACCProveAnya
            sdbgContactos.Update
            DoEvents
            
            iCont = 0
            inum = 1
            
            If m_bModError Or m_bAnyaError Or m_bValError Or m_TelfMailError Then
                m_TelfMailError = False
                Exit Sub
            End If
            
            If Trim(txtCodProve.Text = "") Then
                oMensajes.NoValido m_sIdioma(1)
                If Me.Visible Then txtCodProve.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDenProve.Text = "") Then
                oMensajes.NoValido m_sIdioma(2)
                If Me.Visible Then txtDenProve.SetFocus
                Exit Sub
            End If
            
            If Trim(txtVal1) <> "" Then
                If Not IsNumeric(txtVal1) Then
                    oMensajes.NoValido m_sIdioma(3)
                    Exit Sub
                End If
            End If
            
            If Not m_oCalif1Seleccionada Is Nothing Then
                If Trim(txtVal1) = "" Then
                    oMensajes.NoValido m_sIdioma(4)
                    Exit Sub
                Else
                    If m_oCalif1Seleccionada.ValorInf <= m_oCalif1Seleccionada.ValorSup Then
                        If txtVal1 < m_oCalif1Seleccionada.ValorInf Or txtVal1 > m_oCalif1Seleccionada.ValorSup Then
                            oMensajes.NoValido m_sIdioma(4)
                            Exit Sub
                        End If
                    Else
                        If txtVal1 < m_oCalif1Seleccionada.ValorSup Or txtVal1 > m_oCalif1Seleccionada.ValorInf Then
                            oMensajes.NoValido m_sIdioma(4)
                            Exit Sub
                        End If
                    End If
                End If
            End If
            
            If Trim(txtVal2) <> "" Then
                If Not IsNumeric(txtVal2) Then
                    oMensajes.NoValido m_sIdioma(5)
                    Exit Sub
                End If
            End If
            
            If Not m_oCalif2Seleccionada Is Nothing Then
                If Trim(txtVal2) = "" Then
                    oMensajes.NoValido m_sIdioma(6)
                    Exit Sub
                Else
                    If m_oCalif2Seleccionada.ValorInf <= m_oCalif2Seleccionada.ValorSup Then
                        If txtVal2 < m_oCalif2Seleccionada.ValorInf Or txtVal2 > m_oCalif2Seleccionada.ValorSup Then
                            oMensajes.NoValido m_sIdioma(6)
                            Exit Sub
                        End If
                    Else
                        If txtVal2 < m_oCalif2Seleccionada.ValorSup Or txtVal2 > m_oCalif2Seleccionada.ValorInf Then
                            oMensajes.NoValido m_sIdioma(6)
                            Exit Sub
                        End If
                    End If
                End If
            End If
            
            If Trim(txtVal3) <> "" Then
                If Not IsNumeric(txtVal3) Then
                    oMensajes.NoValido m_sIdioma(7)
                    Exit Sub
                End If
            End If
            
            If Not m_oCalif3Seleccionada Is Nothing Then
                If Trim(txtVal3) = "" Then
                    oMensajes.NoValido m_sIdioma(8)
                    Exit Sub
                Else
                    If m_oCalif3Seleccionada.ValorInf <= m_oCalif3Seleccionada.ValorSup Then
                        If txtVal3 < m_oCalif3Seleccionada.ValorInf Or txtVal3 > m_oCalif3Seleccionada.ValorSup Then
                            oMensajes.NoValido m_sIdioma(8)
                            Exit Sub
                        End If
                    Else
                        If txtVal3 < m_oCalif3Seleccionada.ValorSup Or txtVal3 > m_oCalif3Seleccionada.ValorInf Then
                            oMensajes.NoValido m_sIdioma(8)
                            Exit Sub
                        End If
                    End If
                End If
            End If
            
            If gParametrosGenerales.gbActivarCodProveErp Then
                If Trim(txtNIF) <> "" Then
                    Set oProveERPs = oFSGSRaiz.Generar_CProveERPs
                    Set oProvesVinculados = oProveERPs.ProveedoresVinculados(txtNIF)
                    If oProvesVinculados.Count > 0 Then
                        sProvesVinc = ""
                        For Each oProve In oProvesVinculados
                            sProvesVinc = m_sIdioma(9) & ": " & oProve.Cod & " - " & oProve.Den & vbCrLf
                        Next
                        oMensajes.ExistenProveedoresMismoNIF (sProvesVinc)
                                                
                        Set oProveERPs = Nothing
                        Set oProvesVinculados = Nothing
                        Exit Sub
                    End If
                    Set oProveERPs = Nothing
                    Set oProvesVinculados = Nothing
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            
            g_oProveSeleccionado.Cod = Trim(txtCodProve)
            g_oProveSeleccionado.Den = Trim(txtDenProve)
            If Trim(txtDIR) = "" Then
                g_oProveSeleccionado.Direccion = Null
            Else
                g_oProveSeleccionado.Direccion = txtDIR
            End If
            If Trim(txtCP) = "" Then
                g_oProveSeleccionado.cP = Null
            Else
                g_oProveSeleccionado.cP = txtCP
            End If
            If Trim(txtPOB) = "" Then
                g_oProveSeleccionado.Poblacion = Null
            Else
                g_oProveSeleccionado.Poblacion = txtPOB
            End If
            If Trim(sdbcPaiCod) = "" Then
                g_oProveSeleccionado.CodPais = Null
            Else
                g_oProveSeleccionado.CodPais = sdbcPaiCod
            End If
            If Trim(sdbcMonCod) = "" Then
                g_oProveSeleccionado.CodMon = Null
            Else
                g_oProveSeleccionado.CodMon = sdbcMonCod
            End If
            If Trim(sdbcProviCod) = "" Then
                g_oProveSeleccionado.CodProvi = Null
            Else
                g_oProveSeleccionado.CodProvi = sdbcProviCod
            End If
            If Trim(txtNIF) = "" Then
                g_oProveSeleccionado.NIF = Null
            Else
                g_oProveSeleccionado.NIF = txtNIF
            End If
            g_oProveSeleccionado.Val1 = StrToDblOrNull(txtVal1)
            If m_oCalif1Seleccionada Is Nothing Then Set m_oCalif1Seleccionada = BuscarCalificacion(StrToDblOrNull(txtVal1), m_oCalificaciones1)
            g_oProveSeleccionado.Val2 = StrToDblOrNull(txtVal2)
            If m_oCalif2Seleccionada Is Nothing Then Set m_oCalif2Seleccionada = BuscarCalificacion(StrToDblOrNull(txtVal2), m_oCalificaciones2)
            g_oProveSeleccionado.Val3 = StrToDblOrNull(txtVal3)
            If m_oCalif3Seleccionada Is Nothing Then Set m_oCalif3Seleccionada = BuscarCalificacion(StrToDblOrNull(txtVal3), m_oCalificaciones3)
            If Not m_oCalif1Seleccionada Is Nothing Then g_oProveSeleccionado.Calif1 = m_oCalif1Seleccionada.Cod
            If Not m_oCalif2Seleccionada Is Nothing Then g_oProveSeleccionado.Calif2 = m_oCalif2Seleccionada.Cod
            If Not m_oCalif3Seleccionada Is Nothing Then g_oProveSeleccionado.Calif3 = m_oCalif3Seleccionada.Cod
            If Trim(txtObs) = "" Then
                g_oProveSeleccionado.obs = Null
            Else
                g_oProveSeleccionado.obs = txtObs.Text
            End If
            If Trim(txtURL) = "" Then
                g_oProveSeleccionado.URLPROVE = Null
            Else
                g_oProveSeleccionado.URLPROVE = txtURL
            End If
            If Trim(sdbcPagoCod) = "" Then
                g_oProveSeleccionado.FormaPagoCod = Null
            Else
                g_oProveSeleccionado.FormaPagoCod = sdbcPagoCod
            End If
            If Trim(sdbcViaPagoCod) = "" Then
                g_oProveSeleccionado.ViaPagoCod = Null
            Else
                g_oProveSeleccionado.ViaPagoCod = sdbcViaPagoCod
            End If
            If Trim(sdbcFormato.Text) = "" Then
                g_oProveSeleccionado.FormatoPedido = 0
            Else
                g_oProveSeleccionado.FormatoPedido = sdbcFormato.Columns(1).Value
            End If
            g_oProveSeleccionado.AUTOFACTURA = (Me.chkAutoFactura.Value = vbChecked)
            g_oProveSeleccionado.TomarImpuestosDeFacturas = (Me.chkImpuestos.Value = vbChecked)
            ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
            g_oProveSeleccionado.Usuario = basOptimizacion.gvarCodUsuario
            g_oProveSeleccionado.SolicitarAceptacionRecepcion = (chkAcepRecepPedidos.Value = vbChecked)
            
            teserror = m_oIBaseDatos.AnyadirABaseDatos
                                    
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
            Else
                'No ha habido error.
                ' Ahora miramos a ver si hay que asignarles algun grupo de material
                Set m_oIBaseDatos = Nothing
                RegistrarAccion AccionesSummit.ACCProveAnya, "Cod:" & Trim(txtCodProve)
                m_oProves.Add Trim(txtCodProve), "", "", "", "", "", "", "", "", 0, 0, 0, 0, 0, "", "", ""
                    
                If Not (m_oGMN4Seleccionado Is Nothing And m_oGMN3Seleccionado Is Nothing And m_oGMN2Seleccionado Is Nothing And m_oGMN1Seleccionado Is Nothing) Then
                    
                    If Not m_oGMN4Seleccionado Is Nothing Then
                        Set oIMatAsig = g_oProveSeleccionado
                        Set oIMatAsig.GruposMN4 = oFSGSRaiz.Generar_CGruposMatNivel4
                        oIMatAsig.GruposMN4.Add m_oGMN1Seleccionado.Cod, m_oGMN2Seleccionado.Cod, m_oGMN3Seleccionado.Cod, "", "", "", m_oGMN4Seleccionado.Cod, ""
                    Else
                        If Not m_oGMN3Seleccionado Is Nothing Then
                            Set oIMatAsig = g_oProveSeleccionado
                            Set oIMatAsig.GruposMN3 = oFSGSRaiz.Generar_CGruposMatNivel3
                            oIMatAsig.GruposMN3.Add m_oGMN1Seleccionado.Cod, m_oGMN2Seleccionado.Cod, m_oGMN3Seleccionado.Cod, "", "", ""
                        Else
                            If Not m_oGMN2Seleccionado Is Nothing Then
                                Set oIMatAsig = g_oProveSeleccionado
                                Set oIMatAsig.GruposMN2 = oFSGSRaiz.Generar_CGruposMatNivel2
                                oIMatAsig.GruposMN2.Add m_oGMN1Seleccionado.Cod, "", m_oGMN2Seleccionado.Cod, ""
                            Else
                                If Not m_oGMN1Seleccionado Is Nothing Then
                                    Set oIMatAsig = g_oProveSeleccionado
                                    Set oIMatAsig.GruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
                                    oIMatAsig.GruposMN1.Add m_oGMN1Seleccionado.Cod, ""
                                End If
                            End If
                        End If
                    End If
                    
                    teserror = oIMatAsig.AsignarMaterial
                    Set oIMatAsig.GruposMN1 = Nothing
                    Set oIMatAsig.GruposMN2 = Nothing
                    Set oIMatAsig.GruposMN3 = Nothing
                    Set oIMatAsig.GruposMN4 = Nothing
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    Else
                        RegistrarAccion AccionesSummit.ACCMatPorProveMod, "Cod:" & Trim(txtCodProve)
                    End If
                End If
                  
                If Not m_oEqpSeleccionado Is Nothing Then
                    Set oIEqpAsig = g_oProveSeleccionado
                    Set oIEqpAsig.Equipos = oFSGSRaiz.Generar_CEquipos
                    oIEqpAsig.Equipos.Add m_oEqpSeleccionado.Cod, ""
                        teserror = oIEqpAsig.AsignarEquipos
                    Set oIEqpAsig.Equipos = Nothing
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Exit Sub
                    Else
                        RegistrarAccion AccionesSummit.ACCEqpPorProveMod, "Cod: " & Trim(txtCodProve)
                    End If
                
                End If
                
                FinEdicion
                
                'Ahora hay que cargar el combo con el proveedor reci�n a�adido
                sdbcProveCod = txtCodProve
                sdbcProveCod_Validate False
                
                'Proveedores relacionados
                sdbgProveRelac.Update
                DoEvents
            End If
                    
        Case AccionesSummit.ACCProveMod
            If sdbcPagoCod = "" Then
                If g_oProveSeleccionado.ExisteProveEnCatalogo Then
                    oMensajes.FormaPago
                    Exit Sub
                End If
            End If
                
            '_______________________________________________________________________
            'Para Via de Pago no hay que hacer esta comprobaci�n
            '�����������������������������������������������������������������������
            sdbgContactos.Update
            If m_bValError Or m_TelfMailError Then
                m_TelfMailError = False
                m_bValError = False
                Exit Sub
            End If
            DoEvents
               
            iCont = 0
            inum = 1
            sdbgContactos.MoveFirst
            Do While inum <= sdbgContactos.Rows
                If sdbgContactos.Columns("APROV").Value = True Then
                    iCont = iCont + 1
                End If
                If inum < sdbgContactos.Rows Then sdbgContactos.MoveNext
                inum = inum + 1
            Loop
            
            If iCont = 0 Then
                If g_oProveSeleccionado.ExisteProveEnCatalogo Then
                    oMensajes.ContactoAprov
                    Exit Sub
                End If
            End If
            
            If m_bModError Or m_bAnyaError Or m_bValError Then Exit Sub
            
            If gParametrosGenerales.giINSTWEB <> ConPortal Or IsNull(g_oProveSeleccionado.CodPortal) Then
                If Trim(txtDenProve.Text = "") Then
                    Screen.MousePointer = vbNormal
                    oMensajes.NoValido m_sIdioma(2)
                    If Me.Visible Then txtDenProve.SetFocus
                    Exit Sub
                End If
            End If
            If Trim(txtVal1) <> "" Then
                If Not IsNumeric(txtVal1) Then
                    oMensajes.NoValido m_sIdioma(3)
                    Exit Sub
                End If
            End If
            If Trim(txtVal2) <> "" Then
                If Not IsNumeric(txtVal2) Then
                    oMensajes.NoValido m_sIdioma(5)
                    Exit Sub
                End If
            End If
            If Trim(txtVal3) <> "" Then
                If Not IsNumeric(txtVal3) Then
                    oMensajes.NoValido m_sIdioma(7)
                    Exit Sub
                End If
            End If
            
            If Not m_oCalif1Seleccionada Is Nothing Then
                If m_oCalif1Seleccionada.ValorInf <= m_oCalif1Seleccionada.ValorSup Then
                    If StrToDblOrNull(txtVal1) < m_oCalif1Seleccionada.ValorInf Or StrToDblOrNull(txtVal1) > m_oCalif1Seleccionada.ValorSup Then
                        oMensajes.NoValido m_sIdioma(3)
                        Exit Sub
                    End If
                Else
                    If StrToDblOrNull(txtVal1) < m_oCalif1Seleccionada.ValorSup Or StrToDblOrNull(txtVal1) > m_oCalif1Seleccionada.ValorInf Then
                        oMensajes.NoValido m_sIdioma(3)
                        Exit Sub
                    End If
                End If
            End If
            
            If Not m_oCalif2Seleccionada Is Nothing Then
                If m_oCalif2Seleccionada.ValorInf <= m_oCalif2Seleccionada.ValorSup Then
                    If StrToDblOrNull(txtVal2) < m_oCalif2Seleccionada.ValorInf Or StrToDblOrNull(txtVal2) > m_oCalif2Seleccionada.ValorSup Then
                        oMensajes.NoValido m_sIdioma(5)
                        Exit Sub
                    End If
                Else
                    If StrToDblOrNull(txtVal2) < m_oCalif2Seleccionada.ValorSup Or StrToDblOrNull(txtVal2) > m_oCalif2Seleccionada.ValorInf Then
                        oMensajes.NoValido m_sIdioma(5)
                        Exit Sub
                    End If
                End If
            End If
            
            If Not m_oCalif3Seleccionada Is Nothing Then
                If m_oCalif3Seleccionada.ValorInf <= m_oCalif3Seleccionada.ValorSup Then
                    If StrToDblOrNull(txtVal3) < m_oCalif3Seleccionada.ValorInf Or StrToDblOrNull(txtVal3) > m_oCalif3Seleccionada.ValorSup Then
                        oMensajes.NoValido m_sIdioma(7)
                        Exit Sub
                    End If
                Else
                    If StrToDblOrNull(txtVal3) < m_oCalif3Seleccionada.ValorSup Or StrToDblOrNull(txtVal3) > m_oCalif3Seleccionada.ValorInf Then
                        oMensajes.NoValido m_sIdioma(7)
                        Exit Sub
                    End If
                End If
            End If
            
            If gParametrosGenerales.gbActivarCodProveErp Then
                If Trim(txtNIF) <> "" And g_oProveSeleccionado.NIF <> txtNIF Then
                    Set oProveERPs = oFSGSRaiz.Generar_CProveERPs
                    Set oProvesVinculados = oProveERPs.ProveedoresVinculados(txtNIF)
                    If oProvesVinculados.Count > 0 Then
                        sProvesVinc = ""
                        For Each oProve In oProvesVinculados
                            sProvesVinc = sProvesVinc & m_sIdioma(9) & ": " & oProve.Cod & " - " & oProve.Den & vbCrLf
                        Next
                        oMensajes.ExistenProveedoresMismoNIF (sProvesVinc)
                        
                        Set oProveERPs = Nothing
                        Set oProvesVinculados = Nothing
                        Exit Sub
                    End If
                    Set oProveERPs = Nothing
                    Set oProvesVinculados = Nothing
                End If
            End If
            
            '21693 Actualizar el grid de proveedores relacionados
            sdbgProveRelac.Update
            
            If m_bValError Then
                m_bValError = False
                Exit Sub
            End If
            
            Screen.MousePointer = vbHourglass
            
            If gParametrosGenerales.giINSTWEB <> ConPortal Or IsNull(g_oProveSeleccionado.CodPortal) Then
                If Trim(txtDIR) = "" Then
                    g_oProveSeleccionado.Direccion = Null
                Else
                    g_oProveSeleccionado.Direccion = txtDIR
                End If
                
                If Trim(txtCP) = "" Then
                    g_oProveSeleccionado.cP = Null
                Else
                    g_oProveSeleccionado.cP = txtCP
                End If
                
                If Trim(txtPOB) = "" Then
                    g_oProveSeleccionado.Poblacion = Null
                Else
                    g_oProveSeleccionado.Poblacion = txtPOB
                End If
                
                If Trim(sdbcPaiCod) = "" Then
                    g_oProveSeleccionado.CodPais = Null
                Else
                    g_oProveSeleccionado.CodPais = sdbcPaiCod
                End If
                
                If Trim(sdbcMonCod) = "" Then
                    g_oProveSeleccionado.CodMon = Null
                Else
                    g_oProveSeleccionado.CodMon = sdbcMonCod
                End If
                
                If Trim(sdbcProviCod) = "" Then
                    g_oProveSeleccionado.CodProvi = Null
                Else
                    g_oProveSeleccionado.CodProvi = sdbcProviCod
                End If
                
                If Trim(txtNIF) = "" Then
                    g_oProveSeleccionado.NIF = Null
                Else
                    g_oProveSeleccionado.NIF = txtNIF
                End If
                g_oProveSeleccionado.Den = Trim(txtDenProve.Text)
            End If
            g_oProveSeleccionado.Val1 = StrToDblOrNull(txtVal1)
            If m_oCalif1Seleccionada Is Nothing Then Set m_oCalif1Seleccionada = BuscarCalificacion(StrToDblOrNull(txtVal1), m_oCalificaciones1)
            g_oProveSeleccionado.Val2 = StrToDblOrNull(txtVal2)
            If m_oCalif2Seleccionada Is Nothing Then Set m_oCalif2Seleccionada = BuscarCalificacion(StrToDblOrNull(txtVal2), m_oCalificaciones2)
            g_oProveSeleccionado.Val3 = StrToDblOrNull(txtVal3)
            If m_oCalif3Seleccionada Is Nothing Then Set m_oCalif3Seleccionada = BuscarCalificacion(StrToDblOrNull(txtVal3), m_oCalificaciones3)
            If Not m_oCalif1Seleccionada Is Nothing Then
                g_oProveSeleccionado.Calif1 = m_oCalif1Seleccionada.Cod
            Else
                g_oProveSeleccionado.Calif1 = ""
            End If
            If Not m_oCalif2Seleccionada Is Nothing Then
                g_oProveSeleccionado.Calif2 = m_oCalif2Seleccionada.Cod
            Else
                g_oProveSeleccionado.Calif2 = ""
            End If
            If Not m_oCalif3Seleccionada Is Nothing Then
                g_oProveSeleccionado.Calif3 = m_oCalif3Seleccionada.Cod
            Else
                g_oProveSeleccionado.Calif3 = ""
            End If
            If Trim(txtObs) = "" Then
                g_oProveSeleccionado.obs = Null
            Else
                g_oProveSeleccionado.obs = txtObs.Text
            End If
            If Trim(txtURL) = "" Then
                g_oProveSeleccionado.URLPROVE = Null
            Else
                g_oProveSeleccionado.URLPROVE = txtURL
            End If
            If Trim(sdbcPagoCod) = "" Then
                g_oProveSeleccionado.FormaPagoCod = Null
            Else
                g_oProveSeleccionado.FormaPagoCod = sdbcPagoCod
            End If
            If Trim(sdbcViaPagoCod) = "" Then
                g_oProveSeleccionado.ViaPagoCod = Null
            Else
                g_oProveSeleccionado.ViaPagoCod = sdbcViaPagoCod
            End If
            If Trim(sdbcFormato) = "" Then
                g_oProveSeleccionado.FormatoPedido = 0
            Else
                g_oProveSeleccionado.FormatoPedido = sdbcFormato.Columns(1).Value
            End If
            g_oProveSeleccionado.AUTOFACTURA = (Me.chkAutoFactura.Value = vbChecked)
            g_oProveSeleccionado.TomarImpuestosDeFacturas = (Me.chkImpuestos.Value = vbChecked)
            g_oProveSeleccionado.SolicitarAceptacionRecepcion = (Me.chkAcepRecepPedidos.Value = vbChecked)

            teserror = m_oIBaseDatos.FinalizarEdicionModificando
                                    
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Exit Sub
            Else
                Set m_oIBaseDatos = Nothing
                RegistrarAccion AccionesSummit.ACCProveMod, "Cod:" & sdbcProveCod.Text
            End If
            ' se ha modificado luego ponemos
            If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove) Then
                lblEstInt.ToolTipText = m_sIdiNoSincronizado
                lblEstInt.Backcolor = RGB(255, 0, 0)
            End If
            
            g_oProveSeleccionado.CargarEmpresas
            MostrarEmpresas
            
            Dim oEmpresa As CEmpresa
            For Each oEmpresa In g_oProveSeleccionado.Empresas
    
                Exit For
            Next
            If Not oEmpresa Is Nothing Then
                oEmpresa.CargarERPs g_oProveSeleccionado.Cod
                lstvwEmpresas_ItemClick lstvwEmpresas.ListItems(1)
            End If
            FinEdicion
            m_bRespetarComboProve = False
            sdbcProveCod.Text = txtCodProve.Text
            sdbcProveCod_Validate False
            sTabGeneral.TabEnabled(0) = True
            BloquearDatosProve False
            
            'Proveedores relacionados
            sdbgProveRelac.Update
            DoEvents
    End Select
    
    sdbgProveedoresPri.Columns("CAR").Locked = False
    sdbgProveedoresPri.Columns("CAR").CellStyleSet "Flecha"
    
    m_Accion = ACCProveCon
    m_AccionCont = ACCContCon
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdActualizar_Click()
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdAnyaEsp_Click()
Dim DataFile As Integer
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim arrFileNames As Variant
Dim iFile As Integer
Dim oFos As Scripting.FileSystemObject
On Error GoTo Cancelar:
    
    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    Set m_oIBAseDatosEnEdicion = g_oProveSeleccionado
        
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion(False)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        TratarError teserror
        Set m_oIBAseDatosEnEdicion = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    cmmdEsp.DialogTitle = m_sIdiSelecAdjunto 'Seleccionar archivo adjunto de especificaci�n

    cmmdEsp.Filter = m_sIdiTodosArchivos & "|*.*" 'Todos los archivos|*.*
    cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer

    cmmdEsp.filename = ""
    cmmdEsp.ShowOpen
    
    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileName = "" Then
        Set m_oIBAseDatosEnEdicion = Nothing
        Exit Sub
    End If
    
    arrFileNames = ExtraerFicheros(sFileName)
    
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.chkProcFich.Visible = False
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2300
    End If
    frmPROCEComFich.sOrigen = "frmPROVE"
    g_bCancelarEsp = False
    frmPROCEComFich.Show 1
    
    If Not g_bCancelarEsp Then
        Set oFos = New Scripting.FileSystemObject
        For iFile = 1 To UBound(arrFileNames)
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
        
            Set oEsp = oFSGSRaiz.generar_CEspecificacion
            Set oEsp.Proveedor = g_oProveSeleccionado
            oEsp.nombre = sFileTitle
            oEsp.Comentario = g_sComentario
            
            teserror = oEsp.ComenzarEscrituraData(TipoEspecificacion.EspProveedor)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Set m_oIBAseDatosEnEdicion = Nothing
                Exit Sub
            End If
            
            Dim sAdjunto As String
            Dim ArrayAdjunto() As String
            Dim oFile As File
            Dim bites As Long
            
            Set oFile = oFos.GetFile(sFileName)
            bites = oFile.Size
            oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
            
            sAdjunto = oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspProveedor)
            
            'Creamos un array, cada "substring" se asignar�
            'a un elemento del array
            ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
            oEsp.Id = ArrayAdjunto(0)
            oEsp.DataSize = bites
            oEsp.Fecha = Date & " " & Time
            
            basSeguridad.RegistrarAccion AccionesSummit.ACCProveEspAnya, "Prove:" & sdbcProveCod.Text & " - " & sdbcProveDen.Text & "Archivo:" & sFileTitle
            
            g_oProveSeleccionado.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , , oEsp.Comentario, , , g_oProveSeleccionado
            lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
    
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
        Next
    End If
    lstvwEsp.Refresh
    Set oEsp = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing
    Set oFos = Nothing

Exit Sub
    
Cancelar:
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Close DataFile
        Set oEsp = Nothing
        Set m_oIBAseDatosEnEdicion = Nothing
    End If

End Sub

Private Sub cmdA�adir_Click()

    m_Accion = ACCProveAnya
    
    Set g_oProveSeleccionado = Nothing
    Set g_oProveSeleccionado = oFSGSRaiz.generar_CProveedor
    Set g_oProveSeleccionado.Contactos = oFSGSRaiz.generar_CContactos
    If Not g_bModifDatweb Then
        Set g_oProveSeleccionado.UsuarioWeb = oFSGSRaiz.generar_cusuario
    End If
    Set g_oProveSeleccionado.especificaciones = oFSGSRaiz.Generar_CEspecificaciones
    Set m_oIBaseDatos = g_oProveSeleccionado
    
    PrepararAnyadir

    cmdAbrirEsp.Enabled = False
    cmdAnyaEsp.Enabled = False
    cmdEliEsp.Enabled = False
    cmdSalvarEsp.Enabled = False
    cmdModifEsp.Enabled = False
    
    sdbgProveedoresPri.RemoveAll
    stabProve.Tab = 0

End Sub

Private Sub cmdA�adirContacto_Click()
    
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgContactos.Scroll 0, sdbgContactos.Rows - sdbgContactos.Row
    
    If sdbgContactos.VisibleRows > 0 Then
        
        If sdbgContactos.VisibleRows >= sdbgContactos.Rows Then
            sdbgContactos.Row = sdbgContactos.Rows
        Else
            sdbgContactos.Row = sdbgContactos.Rows - (sdbgContactos.Rows - sdbgContactos.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgContactos.SetFocus

End Sub

Private Sub cmdA�adirProveRelac_Click()
    ''' * Objetivo: Situarnos en la fila de adicion
    
    '21368 Control para evitar que se puedan a�adir m�s proveedores mientras haya una l�nea en edici�n
    If sdbgProveRelac.RowChanged Then
        Exit Sub
    End If
    sdbgProveRelac.Scroll 0, sdbgProveRelac.Rows - sdbgProveRelac.Row
    
    If sdbgProveRelac.VisibleRows > 0 Then
        
        If sdbgProveRelac.VisibleRows >= sdbgProveRelac.Rows Then
            sdbgProveRelac.Row = sdbgProveRelac.Rows
        Else
            sdbgProveRelac.Row = sdbgProveRelac.Rows - (sdbgProveRelac.Rows - sdbgProveRelac.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgProveRelac.SetFocus
    
    frmPROVEBuscar.sOrigen = "ProveRelac"
    frmPROVEBuscar.Show vbModal

End Sub

Private Sub cmdAyudaFiltro_Click()
    MostrarFormPROCEAyuda oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Private Sub cmdBuscar_Click()
With frmPROVEBuscar
    .sOrigen = "Prove"
    .CodGMN1 = Trim(sdbcGMN1_4Cod)
    .CodGMN2 = Trim(sdbcGMN2_4Cod)
    .CodGMN3 = Trim(sdbcGMN3_4Cod)
    .codGMN4 = Trim(sdbcGMN4_4Cod)
    .bREqp = m_bREqp
    .bRMat = m_bRMat
    .codEqp = sdbcEqpCod.Value
    
    .Hide
    .Show 1
End With
End Sub

Private Sub cmdCancelar_Click()
    m_bCancelCheckContacto = True
    
    sdbgProveRelac.DataChanged = False
    sdbgProveRelac.CancelUpdate
    DoEvents
    
    Select Case m_Accion
        Case AccionesSummit.ACCProveAnya
            sdbcProveCod.Text = ""
            sdbcProveDen.Text = ""
            txtCodProve = ""
            txtDenProve = ""
            BorrarTodo
            sTabGeneral.TabEnabled(0) = True
            
            Set g_oProveSeleccionado = Nothing
        
        Case AccionesSummit.ACCProveMod
            BloquearDatosProve False
            NoBorrarTodo
            txtDenProve = sdbcProveDen.Text
            cmdActualizar_Click
            sTabGeneral.TabEnabled(0) = True
            
        Case AccionesSummit.ACCProveEli
        
    End Select

    m_Accion = ACCProveCon
    m_AccionCont = ACCContCon
    
    FinEdicion
    
    m_bCancelCheckContacto = False
End Sub

Private Sub cmdCodigo_Click()
    Dim iResp As Integer
    Dim teserror As TipoErrorSummit
    
    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    
    Set m_oIBaseDatos = Nothing
    Set m_oIBaseDatos = g_oProveSeleccionado
    
    teserror = m_oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
        
    m_oIBaseDatos.CancelarEdicion
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    
    frmMODCOD.caption = m_sIdioma(9) & " (" & m_sIdioma(1) & ")"
    frmMODCOD.Left = frmPROVE.Left + 500
    frmMODCOD.Top = frmPROVE.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodPROVE
    frmMODCOD.txtCodAct.Text = g_oProveSeleccionado.Cod
    Set frmMODCOD.fOrigen = frmPROVE
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    DoEvents
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
        oMensajes.NoValido m_sIdioma(1)
        Set m_oIBaseDatos = Nothing
        Exit Sub
    End If
        
    If UCase(g_sCodigoNuevo) = UCase(g_oProveSeleccionado.Cod) Then
        oMensajes.NoValido m_sIdioma(1)
        Set m_oIBaseDatos = Nothing
        Exit Sub
    End If
            
    iResp = oMensajes.PreguntarCambiarCodigoProveedor
    If iResp = vbNo Then Exit Sub

    'Pasamos el usuario que realiza la modificaci�n (LOG e Integraci�n)
    g_oProveSeleccionado.Usuario = basOptimizacion.gvarCodUsuario
    Screen.MousePointer = vbHourglass
    teserror = m_oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If
    Screen.MousePointer = vbDefault
        
    Set m_oIBaseDatos = Nothing

End Sub

Private Sub cmdCopiarEspPortal_Click()
Dim oEspProvePortal As CEspecificacion
Dim oTESError As TipoErrorSummit
Dim Item As MSComctlLib.listItem
Dim sNombre As String
Dim vComentario As Variant
Dim dtFecha As Date
Dim bEspModif As Boolean 'Variable para controlar si ha habido cambios en el archivo

    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    Set Item = lstvwEspProvePortal.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
    
        Screen.MousePointer = vbHourglass
        bEspModif = False
        Set oEspProvePortal = g_oProveSeleccionado.EspecificacionesProvePortal.Item(CStr(lstvwEspProvePortal.selectedItem.Tag))
        'Guardamos los datos originales por si cambian en el portal
        sNombre = oEspProvePortal.nombre
        vComentario = oEspProvePortal.Comentario
        dtFecha = oEspProvePortal.Fecha
        
        oTESError = oEspProvePortal.DevolverEspProvePortal
        If oTESError.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            If oTESError.NumError = TESDatoEliminado Then
                basErrores.TratarError oTESError
                g_oProveSeleccionado.EspecificacionesProvePortal.Remove (CStr(lstvwEspProvePortal.selectedItem.Tag))
                lstvwEspProvePortal.ListItems.Remove (CStr(lstvwEspProvePortal.selectedItem.key))
                oEspProvePortal.EliminarProveEspPortal
                oMensajes.DatosProveEspPortalActualizados
            Else
                basErrores.TratarError oTESError
            End If
            Set oEspProvePortal = Nothing
            Exit Sub
        End If
        
        oTESError = oEspProvePortal.CrearEspProvePortalAProve()
        If oTESError.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            oEspProvePortal.DeshacerTransferEspProvePortalAProve
            Set oEspProvePortal = Nothing
            basErrores.TratarError oTESError
            Exit Sub
        End If
                
        g_oProveSeleccionado.especificaciones.Add oEspProvePortal.nombre, oEspProvePortal.Fecha, oEspProvePortal.Id_ProveEsp, , , oEspProvePortal.Comentario, , , g_oProveSeleccionado
        'Comprobamos si los datos del archivo que acabamos de leer
        'son los mismos que los que tenemos en la lista del portal
        If oEspProvePortal.nombre <> sNombre Then
            lstvwEspProvePortal.selectedItem.Text = oEspProvePortal.nombre
            bEspModif = True
        End If
        If NullToStr(oEspProvePortal.Comentario) <> NullToStr(vComentario) Then
            lstvwEspProvePortal.selectedItem.ListSubItems.Item(1).Text = NullToStr(oEspProvePortal.Comentario)
            bEspModif = True
        End If
        If DateDiff("s", oEspProvePortal.Fecha, dtFecha) <> 0 Then
            lstvwEspProvePortal.selectedItem.ListSubItems.Item(2).Text = oEspProvePortal.Fecha
            bEspModif = True
        End If
        lstvwEsp.ListItems.Add , "ESP" & CStr(oEspProvePortal.Id_ProveEsp), oEspProvePortal.nombre, , "ESP"
        lstvwEsp.ListItems.Item("ESP" & CStr(oEspProvePortal.Id_ProveEsp)).ToolTipText = NullToStr(oEspProvePortal.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEspProvePortal.Id_ProveEsp)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEspProvePortal.DataSize / 1024) & " " & m_skb
        lstvwEsp.ListItems.Item("ESP" & CStr(oEspProvePortal.Id_ProveEsp)).ListSubItems.Add , "Com", NullToStr(oEspProvePortal.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEspProvePortal.Id_ProveEsp)).ListSubItems.Add , "Fec", oEspProvePortal.Fecha
        lstvwEsp.ListItems.Item("ESP" & CStr(oEspProvePortal.Id_ProveEsp)).Tag = oEspProvePortal.Id_ProveEsp
        
        If bEspModif Then
            oEspProvePortal.ActualizarProveEspPortal
            oMensajes.DatosProveEspPortalActualizados
        End If
        
        Screen.MousePointer = vbNormal
        Set oEspProvePortal = Nothing
    
    End If

End Sub

Private Sub cmdDatosPortal_Click()
Dim teserror As TipoErrorSummit

    If gParametrosGenerales.giINSTWEB <> ConPortal Then Exit Sub

    Screen.MousePointer = vbHourglass
    frmEsperaPortal.Show
    DoEvents
    'integracion
    g_oProveSeleccionado.Usuario = basOptimizacion.gvarCodUsuario
    teserror = g_oProveSeleccionado.CopiarDatosProveedorPortal
    
    Unload frmEsperaPortal
    Screen.MousePointer = vbNormal

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    ProveedorSeleccionado

End Sub

Private Sub cmdDeshacerContacto_Click()
    
    ''' * Objetivo: Deshacer la edicion en la Contacto actual
    
    m_bCancelCheckContacto = True
    sdbgContactos.CancelUpdate
    sdbgContactos.DataChanged = False
    m_bValError = False
    
    If Not m_oContactoEnEdicion Is Nothing Then
        Set m_oContactoEnEdicion = Nothing
    End If
    
    cmdA�adirContacto.Enabled = True
    cmdEliminarContacto.Enabled = True
    cmdDeshacerContacto.Enabled = False
    
    If Me.Visible Then sdbgContactos.SetFocus
    m_AccionCont = ACCContCon
    
    m_bCancelCheckContacto = False

End Sub

''' <summary>Deshacer �ltima modificaci�n sobre le proveedor relacionado</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub cmdDeshacerProveRelac_Click()
   ''' * Objetivo: Deshacer la edicion en la Contacto actual
    
    sdbgProveRelac.DataChanged = False
    sdbgProveRelac.CancelUpdate
    DoEvents
    
    m_bValError = False
    
    If Not m_oProveRelacEnEdicion Is Nothing Then
        Set m_oProveRelacEnEdicion = Nothing
    End If
    
    cmdA�adirProveRelac.Enabled = True
    cmdEliminarProveRelac.Enabled = True
    cmdDeshacerProveRelac.Enabled = False
    
    sdbgProveRelac.MoveFirst
    If Me.Visible Then sdbgProveRelac.SetFocus
    m_AccionProveRelac = ACCProveRelacCon
    
End Sub

Private Sub cmdEliEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
        
On Error GoTo Cancelar:

    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        irespuesta = oMensajes.PreguntaEliminar(" " & m_sIdiElArchivo & " " & lstvwEsp.selectedItem.Text)

        If irespuesta = vbNo Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        Set oEsp = g_oProveSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        Set m_oIBAseDatosEnEdicion = oEsp
        teserror = m_oIBAseDatosEnEdicion.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            g_oProveSeleccionado.especificaciones.Remove (CStr(lstvwEsp.selectedItem.Tag))
            basSeguridad.RegistrarAccion AccionesSummit.ACCProveEspEli, "Prove:" & sdbcProveCod.Text & " - " & sdbcProveDen.Text & "Archivo:" & lstvwEsp.selectedItem.Text
            lstvwEsp.ListItems.Remove (CStr(lstvwEsp.selectedItem.key))
        End If
        
        Set oEsp = Nothing
        Set m_oIBAseDatosEnEdicion = Nothing
    End If
    
    Screen.MousePointer = vbNormal
    
Cancelar:
    
    Set oEsp = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing

End Sub

Private Sub cmdEliminar_Click()
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer

    teserror.NumError = TESnoerror
    
    irespuesta = oMensajes.PreguntaEliminar(m_sIdioma(9))
    
    If irespuesta = vbYes Then
    
        m_Accion = ACCProveEli
    
        Set m_oIBaseDatos = g_oProveSeleccionado
        If gParametrosGenerales.giINSTWEB = ConPortal Then
            frmEsperaPortal.Show
            DoEvents
        End If
        teserror = m_oIBaseDatos.EliminarDeBaseDatos
        If gParametrosGenerales.giINSTWEB = ConPortal Then
            Unload frmEsperaPortal
        End If
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        Else
            Set m_oProves = Nothing
            Set m_oProves = oFSGSRaiz.generar_CProveedores
            BorrarTodo
            ModoConsulta
            sdbcProveCod = ""
            m_Accion = ACCProveCon
        End If
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove) Then
            LinIntegracion.Visible = False
            PicIntegracion.Visible = False
            lblEstInt.Visible = False
        End If
    End If

End Sub

Private Sub cmdEliminarContacto_Click()

    ''' * Objetivo: Eliminar la Destino actual
    
    If sdbgContactos.Rows = 0 Then Exit Sub
    sdbgContactos.SelBookmarks.Add sdbgContactos.Bookmark
    sdbgContactos.DeleteSelected
    sdbgContactos.SelBookmarks.RemoveAll

End Sub

Private Sub cmdEliminarProveRelac_Click()

Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim i As Integer
Dim irespuesta As Integer
Dim sCod As String

    If sdbgProveRelac.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass

    Select Case sdbgProveRelac.SelBookmarks.Count
        Case 0
            Screen.MousePointer = vbNormal
            Exit Sub
        Case 1
            irespuesta = oMensajes.PreguntaEliminar(sIdiElProveRelac & " " & sdbgProveRelac.Columns(0).Value & " (" & sdbgProveRelac.Columns(1).Value & ")")
        Case Is > 1
            irespuesta = oMensajes.PreguntaEliminarOfeEst(sIdiEliminacionMultiple)
    End Select
    
    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    ReDim aIdentificadores(sdbgProveRelac.SelBookmarks.Count)
    ReDim aBookmarks(sdbgProveRelac.SelBookmarks.Count)
    
    i = 0
    While i < sdbgProveRelac.SelBookmarks.Count
        sCod = sdbgProveRelac.Columns("COD").CellValue(sdbgProveRelac.SelBookmarks(i))
        aIdentificadores(i + 1) = sCod
        aBookmarks(i + 1) = sdbgProveRelac.SelBookmarks(i)
        sdbgProveRelac.Bookmark = sdbgProveRelac.SelBookmarks(i)
        g_oProveSeleccionado.ProveRel.Remove (sCod)
        i = i + 1
    Wend
    If sdbgProveRelac.SelBookmarks.Count > 0 Then
        sdbgProveRelac.DeleteSelected
        DoEvents
    End If
    sdbgProveRelac.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdEliminarWeb_Click()
Dim irespuesta As Integer

    irespuesta = oMensajes.PreguntaEliminar(m_sEliminarCodAcceso)
    If irespuesta = vbYes Then
        txtCodAcceso.Text = ""
        txtCodAcceso.Enabled = True
        txtPwd.Text = ""
        txtPwdConf.Text = ""
        m_bEliminarWeb = True
        cmdEliminarWeb.Visible = False
    End If

End Sub

Private Sub cmdlistado_Click()

    ''' * Objetivo: Obtener un listado de compradores por material
With frmLstPROVE
    If Not m_oEqpSeleccionado Is Nothing Then
        Set .g_oEqpSeleccionado = m_oEqpSeleccionado
        .sdbcEqpCod = m_oEqpSeleccionado.Cod
        .sdbcEqpCod_Validate False
    Else
        If sdbcEqpCod.Text <> "" Then
            .sdbcEqpCod = sdbcEqpCod.Text
            .EquipoSeleccionado
            .sdbcEqpCod = .g_oEqpSeleccionado.Cod
            .sdbcEqpCod_Validate False
        End If
    End If
    If Not m_oGMN1Seleccionado Is Nothing Then
        Set .g_oGMN1Seleccionado = m_oGMN1Seleccionado
        .sdbcGMN1_4Cod = m_oGMN1Seleccionado.Cod
        .sdbcGMN1_4Cod_Validate False
    End If
    If Not m_oGMN2Seleccionado Is Nothing Then
        Set .g_oGMN2Seleccionado = m_oGMN2Seleccionado
        .sdbcGMN2_4Cod = m_oGMN2Seleccionado.Cod
        .sdbcGMN2_4Cod_Validate False
    End If
    If Not m_oGMN3Seleccionado Is Nothing Then
        Set .g_oGMN3Seleccionado = m_oGMN3Seleccionado
        .sdbcGMN3_4Cod = m_oGMN3Seleccionado.Cod
        .sdbcGMN3_4Cod_Validate False
    End If
    If Not m_oGMN4Seleccionado Is Nothing Then
        Set .g_oGMN4Seleccionado = m_oGMN4Seleccionado
        .sdbcGMN4_4Cod = m_oGMN4Seleccionado.Cod
        .sdbcGMN4_4Cod_Validate False
    End If
    .WindowState = vbNormal
    .ComprobarMaterialSeleccionado
    .g_bProveRespetarCombo = True
    .sdbcProveCod.Text = sdbcProveCod.Text
    .sdbcProveDen.Text = sdbcProveDen.Text
    .g_bProveRespetarCombo = False
    If g_oProveSeleccionado Is Nothing Then
        .stabProve.Tab = 0
    Else
        .stabProve.Tab = 1
    End If
    .Show vbModal
End With
End Sub
Private Sub cmdModifEsp_Click()
    Dim teserror As TipoErrorSummit
    
    If g_oProveSeleccionado Is Nothing Then Exit Sub
    
    If lstvwEsp.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        Set m_oIBAseDatosEnEdicion = g_oProveSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
        
        frmPROCEEspMod.g_sOrigen = "frmPROVE"
        Set frmPROCEEspMod.g_oIBaseDatos = m_oIBAseDatosEnEdicion
        frmPROCEEspMod.Show 1
    End If
End Sub

Private Sub cmdModificar_Click()
Dim teserror As TipoErrorSummit

    teserror.NumError = TESnoerror
    
    m_Accion = ACCProveMod
    Set m_oIBaseDatos = g_oProveSeleccionado
    
    Screen.MousePointer = vbHourglass
    teserror = m_oIBaseDatos.IniciarEdicion
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        m_Accion = ACCProveCon
        Exit Sub
    Else
        PrepararModificar
    End If
End Sub

Private Sub cmdSalvarEsp_Click()
    Dim Item As MSComctlLib.listItem
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
            
    On Error GoTo Cancelar:
    
    If g_oProveSeleccionado Is Nothing Then Exit Sub
        
    Set m_oIBAseDatosEnEdicion = g_oProveSeleccionado
        
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion(False)
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set m_oIBAseDatosEnEdicion = Nothing
        Exit Sub
    End If
    
    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        cmmdEsp.DialogTitle = m_sIdiGuardar

        cmmdEsp.Filter = m_sIdiTipoOrig & "|*.*"
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdiArchivo
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
        
        ' Cargamos el contenido en la esp.
        Set oEsp = g_oProveSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspProveedor)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
                
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspProveedor, oEsp.DataSize, sFileName
    End If
    
Cancelar:
    
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set m_oIBAseDatosEnEdicion = Nothing
        Set oEsp = Nothing
    End If

End Sub

Private Sub cmdSalvarEspPortal_Click()
Dim Item As MSComctlLib.listItem
Dim oEspProvePortal As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit

On Error GoTo Cancelar:
    
    If g_oProveSeleccionado Is Nothing Then Exit Sub
        
    Set m_oIBAseDatosEnEdicion = g_oProveSeleccionado
        
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion(False)
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set m_oIBAseDatosEnEdicion = Nothing
        Exit Sub
    End If
    
    Set Item = lstvwEspProvePortal.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        cmmdEsp.DialogTitle = m_sIdiGuardar
        cmmdEsp.Filter = m_sIdiTipoOrig & "|*.*"
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdiArchivo
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
        
        ' Cargamos el contenido en la esp.
        Set oEspProvePortal = g_oProveSeleccionado.EspecificacionesProvePortal.Item(CStr(lstvwEspProvePortal.selectedItem.Tag))
        teserror = oEspProvePortal.ComenzarLecturaData(TipoEspecificacion.EspProveedor, True)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oEspProvePortal = Nothing
            Set m_oIBAseDatosEnEdicion = Nothing
            Exit Sub
        End If
        
        oEspProvePortal.LeerAdjuntoCia oEspProvePortal.Id, oEspProvePortal.Proveedor.IDPortal, oEspProvePortal.DataSize, sFileName
    End If
    
Cancelar:
    
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set m_oIBAseDatosEnEdicion = Nothing
        Set oEspProvePortal = Nothing
    End If
End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = "frmPROVE"
    frmSELMAT.bRComprador = m_bRMat
    frmSELMAT.Hide
    frmSELMAT.Show 1
End Sub

Private Sub cmdWebCod_Click()
    Dim teserror As TipoErrorSummit
    Dim sAux As String
    Dim i As Integer
    Dim oIBaseDatos As IBaseDatos
   
'   ''' * Objetivo: Cambiar de codigo el proveedor
    If Not g_bModifDatweb Then Exit Sub
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo

    frmMODCOD.caption = m_sIdioma(9) & " (" & m_sIdioma(1) & ")"
    frmMODCOD.Left = frmPROVE.Left + 500
    frmMODCOD.Top = frmPROVE.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodUSU
    frmMODCOD.txtCodAct.Text = g_oProveSeleccionado.UsuarioWeb.Cod
    Set frmMODCOD.fOrigen = frmPROVE
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    If g_sCodigoNuevo = "" Then
        oMensajes.NoValido m_sIdioma(1)
        Exit Sub
    End If
        
    If UCase(g_sCodigoNuevo) = UCase(g_oProveSeleccionado.UsuarioWeb.Cod) Then
        oMensajes.NoValido m_sIdioma(1)
        Exit Sub
    End If
    
    sAux = g_sCodigoNuevo & Format(g_oProveSeleccionado.UsuarioWeb.fecpwd, "DD\/MM\/YYYY HH\:NN\:SS")
    For i = 1 To Len(sAux)
        If InStr(1, FSGSLibrary.sAuxMatrix, Mid(sAux, i, 1)) = 0 Then
            oMensajes.CodUsuarioNoValido (Mid(sAux, i, 1))
            Exit Sub
        End If
    Next
    
    Screen.MousePointer = vbHourglass
    
    'Cambiar codigo
    
    Set m_oUsuario = oFSGSRaiz.generar_cusuario
    m_oUsuario.Cod = g_oProveSeleccionado.UsuarioWeb.Cod
    Set oIBaseDatos = m_oUsuario
    oIBaseDatos.IniciarEdicion
    teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
    If teserror.NumError <> 0 Then
        Screen.MousePointer = vbNormal
        TratarError teserror
        Set oIBaseDatos = Nothing
        Exit Sub
    End If
    
    'Hago esto para recuperar los valores de la pwd y fecpwd
    oIBaseDatos.IniciarEdicion
    g_oProveSeleccionado.UsuarioWeb.Cod = m_oUsuario.Cod
    g_oProveSeleccionado.UsuarioWeb.Pwd = m_oUsuario.Pwd
    g_oProveSeleccionado.UsuarioWeb.fecpwd = m_oUsuario.fecpwd
    txtCodAcceso.Text = m_oUsuario.Cod
    Set oIBaseDatos = Nothing
    Set m_oUsuario = Nothing
    
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub Form_Activate()
    Unload frmPROVEBuscar
End Sub


''' <summary>
''' Carga los textos del m�dulo
''' </summary>
''' <remarks>Llamada desde Form_Load; Tiempo m�ximo</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub CargarRecursos()
    Dim Adores As Ador.Recordset
    Dim i As Integer

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Adores Is Nothing Then
        ReDim m_sIdioma(1 To 13)
        For i = 1 To 13
            m_sIdioma(i) = Adores(0).Value
            Adores.MoveNext
        Next
        fraCalif1.caption = m_sIdioma(3)
        fraCalif2.caption = m_sIdioma(5)
        fraCalif3.caption = m_sIdioma(7)
        Label1.caption = m_sIdioma(1) & ":"
        sdbcGMN1_4Cod.Columns(0).caption = m_sIdioma(1)
        sdbcGMN2_4Cod.Columns(0).caption = m_sIdioma(1)
        sdbcGMN3_4Cod.Columns(0).caption = m_sIdioma(1)
        sdbcGMN4_4Cod.Columns(0).caption = m_sIdioma(1)
        sdbcGMN1_4Den.Columns(1).caption = m_sIdioma(1)
        sdbcGMN2_4Den.Columns(1).caption = m_sIdioma(1)
        sdbcGMN3_4Den.Columns(1).caption = m_sIdioma(1)
        sdbcGMN4_4Den.Columns(1).caption = m_sIdioma(1)
        sdbcEqpCod.Columns(0).caption = m_sIdioma(1)
        sdbcEqpDen.Columns(1).caption = m_sIdioma(1)
        sdbcProveCod.Columns(0).caption = m_sIdioma(1)
        sdbcProveDen.Columns(1).caption = m_sIdioma(1)
        sdbcPaiCod.Columns(0).caption = m_sIdioma(1)
        sdbcPaiDen.Columns(1).caption = m_sIdioma(1)
        sdbcMonCod.Columns(0).caption = m_sIdioma(1)
        sdbcMonDen.Columns(1).caption = m_sIdioma(1)
        sdbcProviCod.Columns(0).caption = m_sIdioma(1)
        sdbcProviDen.Columns(1).caption = m_sIdioma(1)
        sdbcCal1Cod.Columns(0).caption = m_sIdioma(1)
        sdbcCal1Den.Columns(1).caption = m_sIdioma(1)
        sdbcCal2Cod.Columns(0).caption = m_sIdioma(1)
        sdbcCal2Den.Columns(1).caption = m_sIdioma(1)
        sdbcCal3Cod.Columns(0).caption = m_sIdioma(1)
        sdbcCal3Den.Columns(1).caption = m_sIdioma(1)
        sdbcGMN1_4Cod.Columns(1).caption = m_sIdioma(2)
        sdbcGMN2_4Cod.Columns(1).caption = m_sIdioma(2)
        sdbcGMN3_4Cod.Columns(1).caption = m_sIdioma(2)
        sdbcGMN4_4Cod.Columns(1).caption = m_sIdioma(2)
        sdbcGMN1_4Den.Columns(0).caption = m_sIdioma(2)
        sdbcGMN2_4Den.Columns(0).caption = m_sIdioma(2)
        sdbcGMN3_4Den.Columns(0).caption = m_sIdioma(2)
        sdbcGMN4_4Den.Columns(0).caption = m_sIdioma(2)
        sdbcEqpCod.Columns(1).caption = m_sIdioma(2)
        sdbcEqpDen.Columns(0).caption = m_sIdioma(2)
        sdbcProveCod.Columns(1).caption = m_sIdioma(2)
        sdbcProveDen.Columns(0).caption = m_sIdioma(2)
        sdbcPaiCod.Columns(1).caption = m_sIdioma(2)
        sdbcPaiDen.Columns(0).caption = m_sIdioma(2)
        sdbcMonCod.Columns(1).caption = m_sIdioma(2)
        sdbcMonDen.Columns(0).caption = m_sIdioma(2)
        sdbcProviCod.Columns(1).caption = m_sIdioma(2)
        sdbcProviDen.Columns(0).caption = m_sIdioma(2)
        sdbcCal1Cod.Columns(1).caption = m_sIdioma(2)
        sdbcCal1Den.Columns(0).caption = m_sIdioma(2)
        sdbcCal2Cod.Columns(1).caption = m_sIdioma(2)
        sdbcCal2Den.Columns(0).caption = m_sIdioma(2)
        sdbcCal3Cod.Columns(1).caption = m_sIdioma(2)
        sdbcCal3Den.Columns(0).caption = m_sIdioma(2)
        cmdAceptar.caption = Adores(0).Value '14
        Adores.MoveNext
        cmdActualizar.caption = Adores(0).Value '15
        Adores.MoveNext
        cmdA�adir.caption = Adores(0).Value
        Adores.MoveNext
        cmdA�adirContacto.caption = Adores(0).Value
        cmdA�adirProveRelac.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCodigo.caption = Adores(0).Value
        cmdWebCod.caption = Adores(0).Value
        Adores.MoveNext
        cmdDeshacerContacto.caption = Adores(0).Value '20
        cmdDeshacerProveRelac.caption = Adores(0).Value '20
        Adores.MoveNext
        cmdEliminar.caption = Adores(0).Value
        cmdEliminarContacto.caption = Adores(0).Value
        cmdEliminarProveRelac.caption = Adores(0).Value
        cmdEliminarWeb.caption = Adores(0).Value
        Adores.MoveNext
        cmdListado.caption = Adores(0).Value
        Adores.MoveNext
        cmdModificar.caption = Adores(0).Value
        Adores.MoveNext
        Frame1.caption = Adores(0).Value
        Adores.MoveNext
        Frame2.caption = Adores(0).Value '25
        Adores.MoveNext
        frmPROVE.caption = Adores(0).Value
        sTabGeneral.TabCaption(1) = Adores(0).Value
        Adores.MoveNext
        label10.caption = Adores(0).Value & ":" '27 Valor
        Label13.caption = Adores(0).Value & ":"
        Label23.caption = Adores(0).Value & ":"
        m_sIdiValor = Adores(0).Value
        
        Adores.MoveNext
        Label11.caption = Adores(0).Value '28 Calif.:
        Label22.caption = Adores(0).Value
        Label24.caption = Adores(0).Value
        Adores.MoveNext
        label2.caption = Adores(0).Value
        Label16.caption = Adores(0).Value
        Adores.MoveNext
        Label21.caption = Adores(0).Value '30
        Label12.caption = Adores(0).Value
        Adores.MoveNext
        Label3.caption = Adores(0).Value
        Label17.caption = Adores(0).Value
        Adores.MoveNext
        Label4.caption = Adores(0).Value
        Label18.caption = Adores(0).Value
        Adores.MoveNext
        Label5.caption = Adores(0).Value
        Label19.caption = Adores(0).Value
        Label31.caption = Adores(0).Value '33 Pa�s:
        Adores.MoveNext
        Label6.caption = Adores(0).Value
        Label20.caption = Adores(0).Value
        Label32.caption = Adores(0).Value '34 Provincia:
        Adores.MoveNext
        Label7.caption = Adores(0).Value
        Label9.caption = Adores(0).Value
        Label25.caption = Adores(0).Value '35 NIF:
        Adores.MoveNext
        Label8.caption = Adores(0).Value
        Adores.MoveNext
        lblCodigo.caption = Adores(0).Value
        Adores.MoveNext
        sdbcCal1Cod.Columns(2).caption = Adores(0).Value '38
        sdbcCal1Den.Columns(2).caption = Adores(0).Value
        sdbcCal2Cod.Columns(2).caption = Adores(0).Value
        sdbcCal2Den.Columns(2).caption = Adores(0).Value
        sdbcCal3Cod.Columns(2).caption = Adores(0).Value
        sdbcCal3Den.Columns(2).caption = Adores(0).Value
        Adores.MoveNext
        sdbcCal1Cod.Columns(3).caption = Adores(0).Value
        sdbcCal1Den.Columns(3).caption = Adores(0).Value
        sdbcCal2Cod.Columns(3).caption = Adores(0).Value
        sdbcCal2Den.Columns(3).caption = Adores(0).Value
        sdbcCal3Cod.Columns(3).caption = Adores(0).Value
        sdbcCal3Den.Columns(3).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(1).caption = Adores(0).Value '40
        Adores.MoveNext
        sdbgContactos.Columns(2).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(3).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(4).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(5).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(6).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(8).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(9).caption = Adores(0).Value
        m_sIdiMail = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(11).caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(7).caption = Adores(0).Value
        Adores.MoveNext
        sTabGeneral.TabCaption(0) = Adores(0).Value
        Adores.MoveNext
        stabProve.TabCaption(0) = Adores(0).Value '50
        Frame3.caption = Adores(0).Value
        Adores.MoveNext
        stabProve.TabCaption(1) = Adores(0).Value
        Adores.MoveNext
        stabProve.TabCaption(2) = Adores(0).Value
        Adores.MoveNext
        stabProve.TabCaption(4) = Adores(0).Value
        Adores.MoveNext
        cmdDatosPortal.caption = Adores(0).Value
        Adores.MoveNext
        Label14.caption = Adores(0).Value
        Label15.caption = Adores(0).Value
        Adores.MoveNext
        fraWeb.caption = Adores(0).Value
        Adores.MoveNext
        ChkSoloProveWeb.caption = Adores(0).Value
        Adores.MoveNext
        stabProve.TabCaption(5) = Adores(0).Value
        Adores.MoveNext
        Label26.caption = Adores(0).Value
        m_sEliminarCodAcceso = Adores(0).Value
        Adores.MoveNext
        lblPwd.caption = Adores(0).Value
        m_sContrase�a = Adores(0).Value
        Adores.MoveNext
        ChkCuentBloq.caption = Adores(0).Value
        Adores.MoveNext
        Label27.caption = Adores(0).Value
        Adores.MoveNext
        Label28.caption = Adores(0).Value
        Adores.MoveNext
        m_sFormato = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns(12).caption = Adores(0).Value
        Adores.MoveNext
        sdbcPagoCod.Columns(1).caption = Adores(0).Value
        sdbcPagoDen.Columns(0).caption = Adores(0).Value
        sdbcFormato.Columns(0).caption = Adores(0).Value
        Adores.MoveNext 'cod
        sdbcPagoCod.Columns(0).caption = Adores(0).Value
        sdbcPagoDen.Columns(1).caption = Adores(0).Value
        sdbgCodERP.Columns(0).caption = Adores(0).Value
        '_______________________________________________________________________
        sdbcViaPagoCod.Columns(0).caption = Adores(0).Value
        sdbcViaPagoDen.Columns(1).caption = Adores(0).Value
        '�����������������������������������������������������������������������
        Adores.MoveNext
        m_sIdiSelecAdjunto = Adores(0).Value
        Adores.MoveNext
        m_sIdiTodosArchivos = Adores(0).Value
        Adores.MoveNext
        m_sIdiElArchivo = Adores(0).Value
        Adores.MoveNext
        m_sIdiGuardar = Adores(0).Value
        Adores.MoveNext
        m_sIdiTipoOrig = Adores(0).Value
        Adores.MoveNext
        m_sIdiArchivo = Adores(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(1).Text = Adores(0).Value
        lstvwEspProvePortal.ColumnHeaders(1).Text = Adores(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(3).Text = Adores(0).Value
        lstvwEspProvePortal.ColumnHeaders(3).Text = Adores(0).Value
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(4).Text = Adores(0).Value
        lstvwEspProvePortal.ColumnHeaders(4).Text = Adores(0).Value
        Adores.MoveNext
        stabProve.TabCaption(3) = Adores(0).Value
        Adores.MoveNext
        lblEspProvePortal.caption = Adores(0).Value
        Adores.MoveNext
        lblPwdConf.caption = Adores(0).Value
        Adores.MoveNext
        m_sIdiSincronizado = Adores(0).Value
        Adores.MoveNext
        m_sIdiNoSincronizado = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns("SUB").caption = Adores(0).Value
        
        Adores.MoveNext
        lstvwEsp.ColumnHeaders(2).Text = Adores(0).Value
        lstvwEspProvePortal.ColumnHeaders(2).Text = Adores(0).Value
        
        Adores.MoveNext
        m_skb = Adores(0).Value
        Adores.MoveNext
        
        Me.fraCodERP.caption = Adores(0).Value
        Adores.MoveNext
        
        Me.sdbgCodERP.Groups(0).caption = Adores(0).Value
        Adores.MoveNext
        Me.sdbgCodERP.Columns(1).caption = Adores(0).Value
        sdbgProveRelac.Columns("DESC").caption = Adores(0).Value '88 Descripci�n
        sdbgProveedoresPri.Columns("DESC").caption = Adores(0).Value
        
        Adores.MoveNext
        stabProve.TabCaption(6) = Adores(0).Value
        
        Adores.MoveNext
        sdbgContactos.Columns("CALIDAD").caption = Adores(0).Value   '90 Calidad

        Adores.MoveNext
        Me.sdbgCodERP.Groups(5).caption = Adores(0).Value '91 Forma de pago
        
        Adores.MoveNext
        Me.sdbgCodERP.Groups(6).caption = Adores(0).Value '92 V�a de pago

        Adores.MoveNext
        sdbcViaPagoCod.Columns(1).caption = Adores(0).Value     '93      Denominaci�n
        sdbcViaPagoDen.Columns(0).caption = Adores(0).Value
        Adores.MoveNext
        lblViaPago.caption = Adores(0).Value                 '94 V�a de pago para pedidos:
        '�����������������������������������������������������������������������
        
        Adores.MoveNext
        sIdiElProveRelac = Adores(0).Value '95 El proveedor relacionado
        Adores.MoveNext
        sIdiEliminacionMultiple = Adores(0).Value '96 m�ltiples proveedores relacionados.
        Adores.MoveNext
        sdbgProveRelac.caption = Adores(0).Value '97 Proveedores relacionados
        Adores.MoveNext
        sdbgProveRelac.Columns("TREL").caption = Adores(0).Value '98 Tipo de relaci�n
        sdbgProveedoresPri.Columns("TREL").caption = Adores(0).Value
        Adores.MoveNext
        sdbddTipoRel.Columns("PED").caption = Adores(0).Value '99 Pedidos
        Adores.MoveNext
        sdbgProveRelac.Columns("PAIS").caption = Adores(0).Value '100 Pa�s
        Adores.MoveNext
        lblTitProvPri.caption = Adores(0).Value '101 Proveedores relacionados a �ste:
        Adores.MoveNext
        Label33.caption = Adores(0).Value '102 C�digo:
        Adores.MoveNext
        Label34.caption = Adores(0).Value '103 Descripci�n:
        
        Adores.MoveNext
        Me.chkAutoFactura.caption = Adores(0).Value '104 permite autofacturaci�n
        
        sdbgProveRelac.Columns("COD").caption = m_sIdioma(1) '1 C�digo
        sdbgProveedoresPri.Columns("COD").caption = m_sIdioma(1)
        sdbddTipoRel.Columns("COD").caption = m_sIdioma(1)
        sdbddTipoRel.Columns("DEN").caption = m_sIdioma(2) '2 Denominaci�n
        stabProve.TabCaption(7) = gParametrosGenerales.gsPlurNombreRel
        Adores.MoveNext
        Me.chkImpuestos.caption = Adores(0).Value
        
        sdbgContactos.Columns("NIF").caption = "Nif"
        Adores.MoveNext
        sdbgCodERP.Groups(7).caption = Adores(0).Value '105 Contacto
        sdbgContactos.Columns("CONTACTO").caption = ""
        Adores.MoveNext
        chkAcepRecepPedidos.caption = Adores(0).Value
        Adores.MoveNext
        sdbgContactos.Columns("PRINCIPAL").caption = Adores(0).Value
        
        Adores.Close
    End If
    
    Set Adores = Nothing
End Sub

Private Sub Form_Load()
    CargarRecursos
    
    PonerFieldSeparator Me
    
    m_Accion = ACCProveCon
    m_AccionCont = ACCContCon
    
    stabProve.Enabled = False
    Me.Height = 7740
    Me.Width = 9580
    
    sdbcFormato.AllowInput = False
    
    If FSEPConf Then
        Frame2.Visible = False
        fraWeb.Top = Frame2.Top
        sdbgContactos.Columns("DEF").Visible = False
        sdbgContactos.Columns("SUB").Visible = False
    Else
        If Not gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosDirectos Then
            sdbgContactos.Columns("APROV").Visible = False
            sdbcPagoCod.Visible = False
            sdbcPagoDen.Visible = False
            '____________________________
            sdbcViaPagoCod.Visible = False
            sdbcViaPagoDen.Visible = False
            '����������������������������
            Label27.Visible = False
            sdbcFormato.Visible = False
            Label28.Visible = False
            Me.Height = Me.Height - 500
            sTabGeneral.Height = sTabGeneral.Height - 500
            stabProve.Height = stabProve.Height - 450
            picNavigate.Top = picNavigate.Top - 600
        ElseIf gParametrosGenerales.gbPedidosAprov = True Then
            sdbgContactos.Columns("APROV").Visible = True
        End If
        
        If gParametrosGenerales.gbPedidosAprov = False Then sdbgContactos.Columns("APROV").Visible = False
    End If
    
    Arrange
    
    picEdit.Visible = False
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    ConfigurarNombres
    
    ConfigurarSeguridad
    
    If Not gParametrosGenerales.gbPremium Then
        chkPremium.Visible = False
    End If
    
    fraWeb.Visible = False
    stabProve.TabVisible(5) = False
    
    If m_bREqp Then
        
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        lblEqp.Visible = True
        lblEqp.caption = basOptimizacion.gCodEqpUsuario & " " & oUsuarioSummit.comprador.DenEqp
        sdbcEqpCod.Text = basOptimizacion.gCodEqpUsuario
        Set m_oEqpSeleccionado = Nothing
        Set m_oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
        m_oEqpSeleccionado.Cod = basOptimizacion.gCodEqpUsuario
    Else
        Set m_oEqps = oFSGSRaiz.Generar_CEquipos
      
    End If
    
    cmdDatosPortal.Visible = False
    cmdDatosPortal.Enabled = False
    
    'Mostramos la columna de subasta si la instalacion es con web
    If Not FSEPConf Then
        If gParametrosGenerales.giINSTWEB = SinWeb Or gParametrosGenerales.gbSubasta = False Or ADMIN_OBLIGATORIO Then
            sdbgContactos.Columns(14).Visible = False
        Else
            sdbgContactos.Columns(14).Visible = True
        End If
    End If
    'Configurar tamanyo campos
    txtCodProve.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPROVE
     
    Set m_oProves = oFSGSRaiz.generar_CProveedores
    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    Set m_oTiposRel = oFSGSRaiz.Generar_CRelacTipos
    
    Set m_oCalificaciones1 = oFSGSRaiz.generar_CCalificaciones
    m_oCalificaciones1.CargarTodasLasCalificaciones 1, , , , , , , , basPublic.gParametrosInstalacion.gIdioma
    Set m_oCalificaciones2 = oFSGSRaiz.generar_CCalificaciones
    m_oCalificaciones2.CargarTodasLasCalificaciones 2, , , , , , , , basPublic.gParametrosInstalacion.gIdioma
    Set m_oCalificaciones3 = oFSGSRaiz.generar_CCalificaciones
    m_oCalificaciones3.CargarTodasLasCalificaciones 3, , , , , , , , basPublic.gParametrosInstalacion.gIdioma

    'Inicializar el array de ficheros
    ReDim m_sArFileNames(0)

    'Habilitamos los botones de Abrir y Guardar Especificaciones
    cmdAbrirEsp.Enabled = True
    cmdSalvarEsp.Enabled = True

    m_bCambiandoModo = False
    'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And m_bRMat Then
        CargarGMN1Automaticamente
    End If
        
    chkAutoFactura.Visible = (gParametrosGenerales.gsAccesoFSIM = AccesoFSIM)
    fraImpuestos.Visible = (gParametrosGenerales.gsAccesoFSIM = AccesoFSIM)
    fraAcepRecepPedidos.Visible = (Not gParametrosGenerales.gbMarcarAcepProve)
End Sub

''' <summary>Configura la seguridad, a lo q se tiene permiso, del formulario</summary>
''' <remarks>Llamada desde=form_load; Tiempo m�ximo=0seg.</remarks>
''' <revision>LTG 03/05/2013</revision>

Private Sub ConfigurarSeguridad()

    LinIntegracion.Visible = False
    PicIntegracion.Visible = False
    lblEstInt.Visible = False

    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEModificarCodigo)) Is Nothing Then
        cmdCodigo.Visible = False
        cmdListado.Left = cmdActualizar.Left
        cmdActualizar.Left = cmdCodigo.Left
    End If
    
    m_bModifDatGen = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEModificarDatGen)) Is Nothing)
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEModifcarCalif)) Is Nothing) Then
        m_bModifCalif = True
    Else
        picCalif.Enabled = False
    End If
    
    m_bAnyadir = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEAnyadir)) Is Nothing)
    m_bEliminar = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEEliminar)) Is Nothing)
    m_bREqp = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVERestEquipo)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And basParametros.gParametrosGenerales.gbOblProveEqp
    m_bRMat = Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVERestMatComp)) Is Nothing And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador
    m_bModifEspProve = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEEspProveModificar)) Is Nothing)

    If gParametrosGenerales.giINSTWEB = ConPortal Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEEspPortalProveConsultar)) Is Nothing) Then
            m_bConsultaEspPortalProve = True
        End If
    End If
    
    If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Prove) Then
        ' Restricciones si sentido integraci�n es solo de entrada,tambien para el Administrador
        m_bAnyadir = False
        m_bEliminar = False
        m_bModifDatGen = False
        cmdCodigo.Visible = False
        If basOptimizacion.gTipoDeUsuario = Administrador Then
            cmdListado.Left = cmdActualizar.Left
            cmdActualizar.Left = cmdCodigo.Left
        End If
    End If
    
    If Not m_bAnyadir Then
        cmdA�adir.Visible = False
        cmdDatosPortal.Left = cmdListado.Left
        cmdListado.Left = cmdActualizar.Left
        cmdActualizar.Left = cmdEliminar.Left
        cmdEliminar.Left = cmdModificar.Left
        cmdModificar.Left = cmdA�adir.Left
    Else
        cmdA�adir.Enabled = True
    End If
    
    If Not m_bModifDatGen And Not m_bModifCalif Then
        cmdModificar.Visible = False
        cmdDatosPortal.Visible = False
        cmdDatosPortal.Left = cmdListado.Left
        cmdListado.Left = cmdActualizar.Left
        cmdActualizar.Left = cmdEliminar.Left
        cmdEliminar.Left = cmdModificar.Left
    End If
    
    If Not m_bEliminar Then
        cmdEliminar.Visible = False
        cmdDatosPortal.Left = cmdListado.Left
        cmdListado.Left = cmdActualizar.Left
        cmdActualizar.Left = cmdEliminar.Left
    End If
    
    If Not basParametros.gParametrosGenerales.gbOblProveEqp Then
        Frame1.Top = Frame1.Top + 300
        Frame2.Visible = False
    End If

    If Not m_bModifDatGen Then
        txtObs.Locked = True
    End If
    
    If Not m_bConsultaEspPortalProve Then
        OcultarEspPortal
        cmdCopiarEspPortal.Enabled = False
    Else
        If m_bModifDatGen Then 'Si se pueden modificar los datos generales, los adjuntos se copiar�n en modo Edici�n
            cmdCopiarEspPortal.Enabled = False
        Else
            cmdCopiarEspPortal.Enabled = True
        End If
    End If

    If Not m_bModifEspProve Then
        cmdAnyaEsp.Enabled = False
        cmdEliEsp.Enabled = False
        cmdModifEsp.Enabled = False
    Else
        If m_bModifDatGen Then 'Si se pueden modificar los datos generales, los adjuntos se modificar�n en modo Edici�n
            cmdAnyaEsp.Enabled = False
            cmdEliEsp.Enabled = False
            cmdModifEsp.Enabled = False
        Else ' Si no aparece el bot�n de modificar (no se pueden modificar datos generales) se pueden modificar directamente los adjuntos
            cmdAnyaEsp.Enabled = True
            cmdEliEsp.Enabled = True
            cmdModifEsp.Enabled = True
        End If
    End If

    Me.stabProve.TabVisible(6) = basParametros.gParametrosGenerales.gbActivarCodProveErp
End Sub

Private Sub Form_Resize()
    Arrange
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim i As Integer
Dim oFOSFile As Scripting.FileSystemObject
Dim bBorrando As Boolean

Set m_oProves = Nothing
Set g_oProveSeleccionado = Nothing
Set m_oPaises = Nothing
Set m_oEqps = Nothing
Set m_oMonedas = Nothing
Set m_oGMN1Seleccionado = Nothing
Set m_oGMN2Seleccionado = Nothing
Set m_oGMN3Seleccionado = Nothing
Set m_oGMN4Seleccionado = Nothing
Set m_oGruposMN1 = Nothing
Set m_oGruposMN2 = Nothing
Set m_oGruposMN3 = Nothing
Set m_oGruposMN4 = Nothing

m_Accion = ACCProveCon
m_AccionCont = ACCContCon

On Error GoTo Error
    Set oFOSFile = New Scripting.FileSystemObject
    i = 0
    While i < UBound(m_sArFileNames)
        bBorrando = False
        If oFOSFile.FileExists(m_sArFileNames(i)) Then
            oFOSFile.DeleteFile m_sArFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set oFOSFile = Nothing
    Me.Visible = False
    
    Exit Sub

Error:
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = m_sArFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
End Sub

Private Sub lstvwEmpresas_ItemClick(ByVal Item As MSComctlLib.listItem)
        Dim oEmpresa As CEmpresa
        Dim oProveERP As CProveERP
        
        
        Dim sCod As String
        
        sCod = Mid(Item.key, 4, Len(Item.key))

        Set oEmpresa = g_oProveSeleccionado.Empresas.Item(sCod)
        
        oEmpresa.CargarERPs g_oProveSeleccionado.Cod, True
        sdbgCodERP.RemoveAll
        For Each oProveERP In oEmpresa.ProveERPs
            sdbgCodERP.AddItem oProveERP.Cod & Chr(m_lSeparador) & oProveERP.Den & Chr(m_lSeparador) & NullToStr(oProveERP.Campo1) & Chr(m_lSeparador) & NullToStr(oProveERP.Campo2) & Chr(m_lSeparador) & NullToStr(oProveERP.Campo3) & Chr(m_lSeparador) & NullToStr(oProveERP.Campo4) & Chr(m_lSeparador) & IIf(oProveERP.BajaLogica, "1", "0") & Chr(m_lSeparador) & NullToStr(oProveERP.Pag) & Chr(m_lSeparador) & NullToStr(oProveERP.ViaPag) & Chr(m_lSeparador) & NullToStr(oProveERP.Contacto)
        Next
End Sub

Private Sub sdbcCal1Cod_Change()
    
    If Not m_bCal1RespetarCombo Then
    
        m_bCal1RespetarCombo = True
        sdbcCal1Den.Text = ""
        If sdbcCal1Cod.Text = "" Then
            txtVal1 = ""
        Else
            m_bCal1CargarComboDesde = True
        End If
        m_bCal1RespetarCombo = False
        Set m_oCalif1Seleccionada = Nothing
        
    End If

End Sub

Private Sub sdbcCal1Cod_PositionList(ByVal Text As String)
PositionList sdbcCal1Cod, Text
End Sub

Private Sub sdbcCal1Cod_Validate(Cancel As Boolean)
Dim oCalif1 As CCalificaciones
Dim bExiste As Boolean
    
With sdbcCal1Cod
    If .Text = "" Then
        .Columns(0).Text = ""
        .Columns(1).Text = ""
        txtVal1.Text = ""
        sdbcCal1Den.Text = ""
        Exit Sub
    End If
    
    If UCase(.Text) = UCase(.Columns(0).Text) Then
        m_bCal1RespetarCombo = True
        sdbcCal1Den.Text = .Columns(1).Text
        m_bCal1RespetarCombo = False
        
        If m_oCalif1Seleccionada Is Nothing Then
            '''''''''''''
            Set oCalif1 = oFSGSRaiz.generar_CCalificaciones
            oCalif1.CargarTodasLasCalificaciones 1, .Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
            .Columns(0).Value = .Text
            .Columns(1).Value = sdbcCal1Den.Text
            .Columns(2).Value = oCalif1.Item(1).ValorInf
            .Columns(3).Value = oCalif1.Item(1).ValorSup
            Set oCalif1 = Nothing
            '''''''''''''''
            m_blnCargandoCalif = True
            Calif1Seleccionada
            m_blnCargandoCalif = False
        End If
        
        Exit Sub
    End If
    
    
    ''' Solo continuamos si existe el grupo
    Set oCalif1 = oFSGSRaiz.generar_CCalificaciones
    Screen.MousePointer = vbHourglass
    oCalif1.CargarTodasLasCalificaciones 1, .Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCalif1.Count = 0)
    
    If Not bExiste Then
        Set m_oCalif1Seleccionada = Nothing
        .Text = ""
        sdbcCal1Den.Text = ""
        txtVal1.Text = ""
    Else
        m_bCal1RespetarCombo = True
        sdbcCal1Den.Text = oCalif1.Item(1).Den
        
        .Columns(0).Value = .Text
        .Columns(1).Value = sdbcCal1Den.Text
        
        .Columns(2).Value = oCalif1.Item(1).ValorInf
        .Columns(3).Value = oCalif1.Item(1).ValorSup
        
        Set m_oCalif1Seleccionada = oCalif1.Item(1)
        
        m_bCal1RespetarCombo = False
        m_bCal1CargarComboDesde = False
        
        Calif1Seleccionada
        
    End If
    
    Set oCalif1 = Nothing
End With
End Sub

Private Sub sdbcCal1Den_Change()
    If Not m_bCal1RespetarCombo Then
        m_bCal1RespetarCombo = True
        sdbcCal1Cod.Text = ""
        If sdbcCal1Den.Text = "" Then
            txtVal1 = ""
        Else
            m_bCal1CargarComboDesde = True
        End If
        m_bCal1RespetarCombo = False
        Set m_oCalif1Seleccionada = Nothing
    End If
End Sub

Private Sub sdbcCal1Den_PositionList(ByVal Text As String)
PositionList sdbcCal1Den, Text
End Sub

Private Sub sdbcCal2Cod_Change()
    If Not m_bCal2RespetarCombo Then
    
        m_bCal2RespetarCombo = True
        sdbcCal2Den.Text = ""
        If sdbcCal2Cod.Text = "" Then
            txtVal2 = ""
        Else
            m_bCal2CargarComboDesde = True
        End If
        m_bCal2RespetarCombo = False
        Set m_oCalif2Seleccionada = Nothing
        
    End If
End Sub

Private Sub sdbcCal2Cod_PositionList(ByVal Text As String)
PositionList sdbcCal2Cod, Text
End Sub

Private Sub sdbcCal2Cod_Validate(Cancel As Boolean)
Dim oCalif2 As CCalificaciones
Dim bExiste As Boolean
    
With sdbcCal2Cod
    If .Text = "" Then
        .Columns(0).Text = ""
        .Columns(1).Text = ""
        txtVal2.Text = ""
        sdbcCal2Den.Text = ""
        Exit Sub
    End If
    
    If UCase(.Text) = UCase(.Columns(0).Text) Then
        
        m_bCal2RespetarCombo = True
        sdbcCal2Den.Text = .Columns(1).Text
        m_bCal2RespetarCombo = False
        
        If m_oCalif2Seleccionada Is Nothing Then
            '''''''''''''
            Set oCalif2 = oFSGSRaiz.generar_CCalificaciones
            oCalif2.CargarTodasLasCalificaciones 2, .Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
            .Columns(0).Value = .Text
            .Columns(1).Value = sdbcCal2Den.Text
            .Columns(2).Value = oCalif2.Item(1).ValorInf
            .Columns(3).Value = oCalif2.Item(1).ValorSup
            Set oCalif2 = Nothing
            ''''''''''
            m_blnCargandoCalif = True
            Calif2Seleccionada
            m_blnCargandoCalif = False
        End If
        
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    Set oCalif2 = oFSGSRaiz.generar_CCalificaciones
    Screen.MousePointer = vbHourglass
    oCalif2.CargarTodasLasCalificaciones 2, .Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCalif2.Count = 0)
    Screen.MousePointer = vbNormal
    
    If Not bExiste Then
        Set m_oCalif2Seleccionada = Nothing
        .Text = ""
        sdbcCal2Den.Text = ""
        txtVal2.Text = ""
    Else
        m_bCal2RespetarCombo = True
        sdbcCal2Den.Text = oCalif2.Item(1).Den
        
        .Columns(0).Value = .Text
        .Columns(1).Value = sdbcCal2Den.Text
        
        Set m_oCalif2Seleccionada = oCalif2.Item(1)
        
        .Columns(2).Value = oCalif2.Item(1).ValorInf
        .Columns(3).Value = oCalif2.Item(1).ValorSup
        
        m_bCal2RespetarCombo = False
        m_bCal2CargarComboDesde = False
        
        Calif2Seleccionada
    End If
    
    Set oCalif2 = Nothing
End With
End Sub

Private Sub sdbcCal2Den_Change()
    If Not m_bCal2RespetarCombo Then
    
        m_bCal2RespetarCombo = True
        sdbcCal2Cod.Text = ""
        If sdbcCal2Den.Text = "" Then
            txtVal2 = ""
        Else
            m_bCal2CargarComboDesde = True
        End If
        m_bCal2RespetarCombo = False
        Set m_oCalif2Seleccionada = Nothing
        
    End If
End Sub

Private Sub sdbcCal2Den_PositionList(ByVal Text As String)
PositionList sdbcCal2Den, Text
End Sub

Private Sub sdbcCal3Cod_Change()
    If Not m_bCal3RespetarCombo Then
        m_bCal3RespetarCombo = True
        sdbcCal3Den.Text = ""
        If sdbcCal3Cod.Text = "" Then
            txtVal3 = ""
        Else
            m_bCal3CargarComboDesde = True
        End If
        m_bCal3RespetarCombo = False
        Set m_oCalif3Seleccionada = Nothing
    End If
End Sub

Private Sub sdbcCal3Cod_PositionList(ByVal Text As String)
PositionList sdbcCal3Cod, Text
End Sub

Private Sub sdbcCal3Cod_Validate(Cancel As Boolean)
Dim oCalif3 As CCalificaciones
Dim bExiste As Boolean

With sdbcCal3Cod
    If .Text = "" Then
        m_bCal3RespetarCombo = True
        .Columns(0).Text = ""
        .Columns(1).Text = ""
        sdbcCal3Den.Text = ""
        txtVal3.Text = ""
        m_bCal3RespetarCombo = False
        Exit Sub
    End If
    
    If UCase(.Text) = UCase(.Columns(0).Text) Then
        m_bCal3RespetarCombo = True
        sdbcCal3Den.Text = .Columns(1).Text
        m_bCal3RespetarCombo = False
        
        If m_oCalif3Seleccionada Is Nothing Then
            '''''''''''''
            Set oCalif3 = oFSGSRaiz.generar_CCalificaciones
            oCalif3.CargarTodasLasCalificaciones 3, .Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
            .Columns(0).Value = .Text
            .Columns(1).Value = sdbcCal3Den.Text
            .Columns(2).Value = oCalif3.Item(1).ValorInf
            .Columns(3).Value = oCalif3.Item(1).ValorSup
            Set oCalif3 = Nothing
            '''''''
            m_blnCargandoCalif = True
            Calif3Seleccionada
            m_blnCargandoCalif = False
        End If
        
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    Set oCalif3 = oFSGSRaiz.generar_CCalificaciones
    Screen.MousePointer = vbHourglass
    oCalif3.CargarTodasLasCalificaciones 3, .Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCalif3.Count = 0)
    
    If Not bExiste Then
        Set m_oCalif3Seleccionada = Nothing
        .Text = ""
        sdbcCal3Den.Text = ""
        txtVal3.Text = ""
   Else
        m_bCal3RespetarCombo = True
        sdbcCal3Den.Text = oCalif3.Item(1).Den
        
        .Columns(0).Value = .Text
        .Columns(1).Value = sdbcCal3Den.Text
        
        Set m_oCalif3Seleccionada = oCalif3.Item(1)
        
        .Columns(2).Value = oCalif3.Item(1).ValorInf
        .Columns(3).Value = oCalif3.Item(1).ValorSup
        
        m_bCal3RespetarCombo = False
        m_bCal3CargarComboDesde = False
        
        Calif3Seleccionada
    End If
    
    Set oCalif3 = Nothing
End With
End Sub

Private Sub sdbcCal3Den_Change()
    If Not m_bCal3RespetarCombo Then
    
        m_bCal3RespetarCombo = True
        sdbcCal3Cod.Text = ""
        If sdbcCal3Den.Text = "" Then
            txtVal3 = ""
        Else
            m_bCal3CargarComboDesde = True
        End If
        m_bCal3RespetarCombo = False
        Set m_oCalif3Seleccionada = Nothing
    End If
End Sub

Private Sub sdbcCal3Den_PositionList(ByVal Text As String)
PositionList sdbcCal3Den, Text
End Sub

Private Sub sdbcCal3Cod_CloseUp()
    If sdbcCal3Cod.Value = "..." Then
        sdbcCal3Cod.Text = ""
        Exit Sub
    End If
    
    m_bCal3RespetarCombo = True
    sdbcCal3Den.Text = sdbcCal3Cod.Columns(1).Text
    sdbcCal3Cod.Text = sdbcCal3Cod.Columns(0).Text
    m_bCal3RespetarCombo = False
    
    If m_oCalif3Seleccionada Is Nothing Then
        Calif3Seleccionada
    Else
        'Carga la calificaci�n si se selecciona una diferente a la actual
        If UCase(sdbcCal3Cod.Columns(0).Value) <> UCase(m_oCalif3Seleccionada.Cod) Then
            Calif3Seleccionada
        End If
    End If
    
    m_bCal3CargarComboDesde = False
End Sub

Private Sub sdbcCal3Cod_DropDown()
Dim oCal As CCalificacion
Dim oCalif As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalif = oFSGSRaiz.generar_CCalificaciones
    
    sdbcCal3Cod.RemoveAll

    If m_bCal3CargarComboDesde Then
        oCalif.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 3, Trim(sdbcCal3Cod.Text), , , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalif.CargarTodasLasCalificaciones 3, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalif
        sdbcCal3Cod.AddItem oCal.Cod & Chr(m_lSeparador) & oCal.Den & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
    Next

    If m_bCal3CargarComboDesde And Not oCalif.EOF Then
        sdbcCal3Cod.AddItem "..."
    End If

    Set oCalif = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal3Cod_InitColumnProps()
    sdbcCal3Cod.DataFieldList = "Column 0"
    sdbcCal3Cod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCal3Den_CloseUp()
    If sdbcCal3Den.Value = "..." Then
        sdbcCal3Den.Text = ""
        Exit Sub
    End If
    
    m_bCal3RespetarCombo = True
    sdbcCal3Cod.Text = sdbcCal3Den.Columns(1).Text
    sdbcCal3Den.Text = sdbcCal3Den.Columns(0).Text
    m_bCal3RespetarCombo = False
    
    If m_oCalif3Seleccionada Is Nothing Then
        Calif3Seleccionada
    Else
        'Carga la calificaci�n si se selecciona una diferente a la actual
        If UCase(sdbcCal3Den.Columns(1).Value) <> UCase(m_oCalif3Seleccionada.Cod) Then
            Calif3Seleccionada
        End If
    End If
    m_bCal3CargarComboDesde = False
  
End Sub

Private Sub sdbcCal3Den_DropDown()
Dim oCal As CCalificacion
Dim oCalif3 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalif3 = oFSGSRaiz.generar_CCalificaciones
    
    sdbcCal3Den.RemoveAll

    If m_bCal3CargarComboDesde Then
        oCalif3.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 3, , Trim(sdbcCal3Den.Text), , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalif3.CargarTodasLasCalificaciones 3, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalif3
        sdbcCal3Den.AddItem oCal.Den & Chr(m_lSeparador) & oCal.Cod & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
    Next

    If m_bCal3CargarComboDesde And Not oCalif3.EOF Then
        sdbcCal3Den.AddItem "..."
    End If

    Set oCalif3 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal3Den_InitColumnProps()
    sdbcCal3Den.DataFieldList = "Column 0"
    sdbcCal3Den.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcCal2Cod_CloseUp()

    If sdbcCal2Cod.Value = "..." Then
        sdbcCal2Cod.Text = ""
        Exit Sub
    End If
    
    m_bCal2RespetarCombo = True
    sdbcCal2Den.Text = sdbcCal2Cod.Columns(1).Text
    sdbcCal2Cod.Text = sdbcCal2Cod.Columns(0).Text
    m_bCal2RespetarCombo = False
    
    If m_oCalif2Seleccionada Is Nothing Then
        Calif2Seleccionada
    Else
        'Carga la calificaci�n si se selecciona una diferente a la actual
        If UCase(sdbcCal2Cod.Columns(0).Value) <> UCase(m_oCalif2Seleccionada.Cod) Then
            Calif2Seleccionada
        End If
    End If
    
    m_bCal2CargarComboDesde = False
    
End Sub

Private Sub sdbcCal2Cod_DropDown()
Dim oCal As CCalificacion
Dim oCalif2 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalif2 = oFSGSRaiz.generar_CCalificaciones
    
    sdbcCal2Cod.RemoveAll

    If m_bCal2CargarComboDesde Then
        oCalif2.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 2, Trim(sdbcCal2Cod.Text), , , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalif2.CargarTodasLasCalificaciones 2, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalif2
        sdbcCal2Cod.AddItem oCal.Cod & Chr(m_lSeparador) & oCal.Den & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
    Next

    If m_bCal2CargarComboDesde And Not oCalif2.EOF Then
        sdbcCal2Cod.AddItem "..."
    End If

    Set oCalif2 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal2Cod_InitColumnProps()
    sdbcCal2Cod.DataFieldList = "Column 0"
    sdbcCal2Cod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcCal2Den_CloseUp()

    If sdbcCal2Den.Value = "..." Then
        sdbcCal2Den.Text = ""
        Exit Sub
    End If
    
    m_bCal2RespetarCombo = True
    sdbcCal2Cod.Text = sdbcCal2Den.Columns(1).Text
    sdbcCal2Den.Text = sdbcCal2Den.Columns(0).Text
    m_bCal2RespetarCombo = False
    
    If m_oCalif2Seleccionada Is Nothing Then
        Calif2Seleccionada
    Else
        'Carga la calificaci�n si se selecciona una diferente a la actual
        If UCase(sdbcCal2Den.Columns(1).Value) <> UCase(m_oCalif2Seleccionada.Cod) Then
            Calif2Seleccionada
        End If
    End If
    
    m_bCal2CargarComboDesde = False
        
End Sub

Private Sub sdbcCal2Den_DropDown()
Dim oCal As CCalificacion
Dim oCalif2 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalif2 = oFSGSRaiz.generar_CCalificaciones
   
    sdbcCal2Den.RemoveAll

    If m_bCal2CargarComboDesde Then
        oCalif2.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 2, , Trim(sdbcCal2Den.Text), , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalif2.CargarTodasLasCalificaciones 2, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalif2
        
        sdbcCal2Den.AddItem oCal.Den & Chr(m_lSeparador) & oCal.Cod & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
        
    Next

    If m_bCal2CargarComboDesde And Not oCalif2.EOF Then
        sdbcCal2Den.AddItem "..."
    End If

    Set oCalif2 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal2Den_InitColumnProps()
    sdbcCal2Den.DataFieldList = "Column 0"
    sdbcCal2Den.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcEqpCod_Click()
    If Not sdbcEqpCod.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpCod_PositionList(ByVal Text As String)
PositionList sdbcEqpCod, Text
End Sub
Private Sub sdbcEqpCod_Validate(Cancel As Boolean)
Dim oEquipos As CEquipos
Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        Set m_oEqpSeleccionado = Nothing
        sdbcEqpCod.Text = ""
        sdbcEqpCod.Text = ""
    Else
        m_bEqpRespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        
        sdbcEqpCod.Columns(0).Value = sdbcEqpCod.Text
        sdbcEqpCod.Columns(1).Value = sdbcEqpDen.Text
        
        m_bEqpRespetarCombo = False
        m_bEqpCargarComboDesde = False
    End If
    
    Set oEquipos = Nothing
End Sub

Private Sub sdbcEqpDen_Click()
    
    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
    
End Sub

Private Sub sdbcEqpDen_PositionList(ByVal Text As String)
PositionList sdbcEqpDen, Text
End Sub

''' <summary>Evento de cambio de c�digo de pa�s en el filtro</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub sdbcFiltroPaisCod_Change()
    
    If Not m_bFiltroPaiRespetarCombo Then
    
        m_bFiltroPaiRespetarCombo = True
        sdbcFiltroPaisDen.Text = ""
        m_bFiltroPaiRespetarCombo = False
        
        m_bFiltroPaiCargarComboDesde = True
        Set m_oFiltroPaisSeleccionado = Nothing
        
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
    
End Sub

''' <summary>Evento de selecci�n de c�digo de pa�s en el filtro</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub sdbcFiltroPaisCod_CloseUp()
    
    If sdbcFiltroPaisCod.Value = "..." Then
        sdbcFiltroPaisCod.Text = ""
        Exit Sub
    End If
    
    m_bFiltroPaiRespetarCombo = True
    sdbcFiltroPaisDen.Text = sdbcFiltroPaisCod.Columns(1).Text
    sdbcFiltroPaisCod.Text = sdbcFiltroPaisCod.Columns(0).Text
    m_bFiltroPaiRespetarCombo = False
    
    Set m_oFiltroPaisSeleccionado = m_oPaises.Item(sdbcFiltroPaisCod.Columns(0).Text)
        
    m_bFiltroPaiCargarComboDesde = False
        
End Sub

''' <summary>Evento de dropdown de c�digo de pa�s en el filtro</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub sdbcFiltroPaisCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    
    sdbcFiltroPaisCod.RemoveAll
    
    If m_bFiltroPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcFiltroPaisCod.Text), , , False
    Else
        m_oPaises.CargarTodosLosPaises , , , , , False
    End If
    
    Codigos = m_oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcFiltroPaisCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If m_bFiltroPaiCargarComboDesde And Not m_oPaises.EOF Then
        sdbcFiltroPaisCod.AddItem "..."
    End If

    sdbcFiltroPaisCod.SelStart = 0
    sdbcFiltroPaisCod.SelLength = Len(sdbcFiltroPaisCod.Text)
    sdbcFiltroPaisCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFiltroPaisCod_InitColumnProps()
    sdbcFiltroPaisCod.DataFieldList = "Column 0"
    sdbcFiltroPaisCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcFiltroPaisCod_PositionList(ByVal Text As String)
PositionList sdbcFiltroPaisCod, Text
End Sub

Private Sub sdbcFiltroPaisCod_Validate(Cancel As Boolean)

    Dim oPaises As CPaises
    Dim bExiste As Boolean
    
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    If sdbcFiltroPaisCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    oPaises.CargarTodosLosPaises sdbcFiltroPaisCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcFiltroPaisCod.Text = ""
    Else
        m_bFiltroPaiRespetarCombo = True
        sdbcFiltroPaisDen.Text = oPaises.Item(1).Den
        
        sdbcFiltroPaisCod.Columns(0).Value = sdbcFiltroPaisCod.Text
        sdbcFiltroPaisCod.Columns(1).Value = sdbcFiltroPaisDen.Text
        
        m_bFiltroPaiRespetarCombo = False
        Set m_oFiltroPaisSeleccionado = oPaises.Item(1)
        m_bFiltroPaiCargarComboDesde = False

    End If
    
    Set oPaises = Nothing
End Sub

Private Sub sdbcFiltroPaisDen_Change()
    If Not m_bFiltroPaiRespetarCombo Then
        m_bFiltroPaiRespetarCombo = True
        sdbcFiltroPaisCod.Text = ""
        m_bFiltroPaiRespetarCombo = False
        m_bFiltroPaiCargarComboDesde = True
        Set m_oFiltroPaisSeleccionado = Nothing
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
End Sub

Private Sub sdbcFiltroPaisDen_CloseUp()
    If sdbcFiltroPaisDen.Value = "..." Then
        sdbcFiltroPaisDen.Text = ""
        Exit Sub
    End If
    
    m_bFiltroPaiRespetarCombo = True
    sdbcFiltroPaisCod.Text = sdbcFiltroPaisDen.Columns(1).Text
    sdbcFiltroPaisDen.Text = sdbcFiltroPaisDen.Columns(0).Text
    m_bFiltroPaiRespetarCombo = False
    
    Set m_oFiltroPaisSeleccionado = m_oPaises.Item(sdbcFiltroPaisDen.Columns(1).Text)
    
    m_bFiltroPaiCargarComboDesde = False
End Sub

''' <summary>Evento de dropdown de denominaci�n de pa�s en el filtro</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub sdbcFiltroPaisDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
   
    sdbcFiltroPaisDen.RemoveAll
    
    If m_bFiltroPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcFiltroPaisDen.Text), True, False
    Else
        m_oPaises.CargarTodosLosPaises , , , True, , False
    End If
    
    Codigos = m_oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcFiltroPaisDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If m_bFiltroPaiCargarComboDesde And Not m_oPaises.EOF Then
        sdbcFiltroPaisDen.AddItem "..."
    End If

    sdbcFiltroPaisDen.SelStart = 0
    sdbcFiltroPaisDen.SelLength = Len(sdbcFiltroPaisDen.Text)
    sdbcFiltroPaisCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFiltroPaisDen_InitColumnProps()

    sdbcFiltroPaisDen.DataFieldList = "Column 0"
    sdbcFiltroPaisDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcFiltroPaisDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcFiltroPaisCod.DroppedDown = False
        sdbcFiltroPaisCod.Text = ""
        sdbcFiltroPaisCod.RemoveAll
        sdbcFiltroPaisDen.DroppedDown = False
        sdbcFiltroPaisDen.Text = ""
        sdbcFiltroPaisDen.RemoveAll
        Set m_oFiltroPaisSeleccionado = Nothing
    End If
End Sub

Private Sub sdbcFiltroPaisDen_PositionList(ByVal Text As String)
PositionList sdbcFiltroPaisDen, Text
End Sub

Private Sub sdbcFiltroPaisDen_Validate(Cancel As Boolean)

    Dim oPaises As CPaises
    Dim bExiste As Boolean
    
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    If sdbcFiltroPaisDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    oPaises.CargarTodosLosPaises , sdbcFiltroPaisDen.Text, True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcFiltroPaisDen.Text = ""
    Else
        m_bFiltroPaiRespetarCombo = True
        sdbcFiltroPaisCod.Text = oPaises.Item(1).Cod
        
        sdbcFiltroPaisDen.Columns(0).Value = sdbcFiltroPaisDen.Text
        sdbcFiltroPaisDen.Columns(1).Value = sdbcFiltroPaisCod.Text
        
        m_bFiltroPaiRespetarCombo = False
        Set m_oFiltroPaisSeleccionado = oPaises.Item(1)
        m_bFiltroPaiCargarComboDesde = False
        
    End If
    
    Set oPaises = Nothing

End Sub

Private Sub sdbcFiltroProviCod_Change()
    If Not m_bFiltroProviRespetarCombo Then
        m_bFiltroProviRespetarCombo = True
        sdbcFiltroProviDen.Text = ""
        m_bFiltroProviRespetarCombo = False
        
        m_bFiltroProviCargarComboDesde = True
    End If
End Sub

Private Sub sdbcFiltroProviCod_CloseUp()
    
    If sdbcFiltroProviCod.Value = "..." Then
        sdbcFiltroProviCod.Text = ""
        Exit Sub
    End If
    
    m_bFiltroProviRespetarCombo = True
    sdbcFiltroProviDen.Text = sdbcFiltroProviCod.Columns(1).Text
    sdbcFiltroProviCod.Text = sdbcFiltroProviCod.Columns(0).Text
    m_bFiltroProviRespetarCombo = False
    
    m_bFiltroProviCargarComboDesde = False
        
End Sub

Private Sub sdbcFiltroProviCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcFiltroProviCod.RemoveAll
        
    If Not m_oFiltroPaisSeleccionado Is Nothing Then
        If m_bFiltroProviCargarComboDesde Then
            m_oFiltroPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcFiltroProviCod.Text), , , False
        Else
            m_oFiltroPaisSeleccionado.CargarTodasLasProvincias , , , , False
        End If
        
        Codigos = m_oFiltroPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcFiltroProviCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
        
        If m_bFiltroProviCargarComboDesde And Not m_oFiltroPaisSeleccionado.Provincias.EOF Then
            sdbcFiltroProviCod.AddItem "..."
        End If
    End If
    
    sdbcFiltroProviCod.SelStart = 0
    sdbcFiltroProviCod.SelLength = Len(sdbcFiltroProviCod.Text)
    sdbcFiltroProviCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFiltroProviCod_InitColumnProps()
    sdbcFiltroProviCod.DataFieldList = "Column 0"
    sdbcFiltroProviCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcFiltroProviCod_PositionList(ByVal Text As String)
PositionList sdbcFiltroProviCod, Text
End Sub

Private Sub sdbcFiltroProviCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcFiltroProviCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    If Not m_oFiltroPaisSeleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        m_oFiltroPaisSeleccionado.CargarTodasLasProvincias sdbcFiltroProviCod.Text, , True, , False
        Screen.MousePointer = vbNormal
        
        bExiste = Not (m_oFiltroPaisSeleccionado.Provincias.Count = 0)
    Else
        bExiste = False
    End If
            
    If Not bExiste Then
        sdbcFiltroProviCod.Text = ""
    Else
        m_bFiltroProviRespetarCombo = True
        sdbcFiltroProviDen.Text = m_oFiltroPaisSeleccionado.Provincias.Item(1).Den
        
        sdbcFiltroProviCod.Columns(0).Value = sdbcFiltroProviCod.Text
        sdbcFiltroProviCod.Columns(1).Value = sdbcFiltroProviDen.Text
        
        m_bFiltroProviRespetarCombo = False
        m_bFiltroProviCargarComboDesde = False
    End If
End Sub

Private Sub sdbcFiltroProviDen_Change()
    If Not m_bFiltroProviRespetarCombo Then
        m_bFiltroProviRespetarCombo = True
        sdbcFiltroProviCod.Text = ""
        m_bFiltroProviRespetarCombo = False
        m_bFiltroProviCargarComboDesde = True
    End If
End Sub

Private Sub sdbcFiltroProviDen_CloseUp()
    
    If sdbcFiltroProviDen.Value = "..." Then
        sdbcFiltroProviDen.Text = ""
        Exit Sub
    End If
    
    m_bFiltroProviRespetarCombo = True
    sdbcFiltroProviCod.Text = sdbcFiltroProviDen.Columns(1).Text
    sdbcFiltroProviDen.Text = sdbcFiltroProviDen.Columns(0).Text
    m_bFiltroProviRespetarCombo = False
    
    m_bFiltroProviCargarComboDesde = False
        
End Sub

Private Sub sdbcFiltroProviDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcFiltroProviDen.RemoveAll
    
    If Not m_oFiltroPaisSeleccionado Is Nothing Then
    
        
        If m_bFiltroProviCargarComboDesde Then
            m_oFiltroPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcFiltroProviDen.Text), True, False
        Else
            m_oFiltroPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If
        
        Codigos = m_oFiltroPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcFiltroProviDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
        
        If m_bFiltroProviCargarComboDesde And Not m_oFiltroPaisSeleccionado.Provincias.EOF Then
            sdbcFiltroProviDen.AddItem "..."
        End If

    End If
    
    sdbcFiltroProviDen.SelStart = 0
    sdbcFiltroProviDen.SelLength = Len(sdbcFiltroProviDen.Text)
    sdbcFiltroProviCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFiltroProviDen_InitColumnProps()
    sdbcFiltroProviDen.DataFieldList = "Column 0"
    sdbcFiltroProviDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcFiltroProviDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcFiltroProviCod.DroppedDown = False
        sdbcFiltroProviCod.Text = ""
        sdbcFiltroProviCod.RemoveAll
        sdbcFiltroProviDen.DroppedDown = False
        sdbcFiltroProviDen.Text = ""
        sdbcFiltroProviDen.RemoveAll
    End If
End Sub

Private Sub sdbcFiltroProviDen_PositionList(ByVal Text As String)
PositionList sdbcFiltroProviDen, Text
End Sub

Private Sub sdbcFiltroProviDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcFiltroProviDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    If Not m_oFiltroPaisSeleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        m_oFiltroPaisSeleccionado.CargarTodasLasProvincias , sdbcFiltroProviDen.Text, True, , False
        Screen.MousePointer = vbNormal
        
        bExiste = Not (m_oFiltroPaisSeleccionado.Provincias.Count = 0)
        
    Else
        bExiste = False
    End If
  
    If Not bExiste Then
        sdbcFiltroProviDen.Text = ""
    Else
        m_bFiltroProviRespetarCombo = True
        Screen.MousePointer = vbHourglass
        sdbcFiltroProviCod.Text = m_oFiltroPaisSeleccionado.Provincias.Item(1).Cod
        sdbcFiltroProviDen.Columns(0).Value = sdbcFiltroProviDen.Text
        sdbcFiltroProviDen.Columns(1).Value = sdbcFiltroProviCod.Text
        
        Screen.MousePointer = vbNormal
        m_bFiltroProviRespetarCombo = False
        m_bFiltroProviCargarComboDesde = False
    End If
End Sub

Private Sub sdbcFormato_DropDown()
    
    Screen.MousePointer = vbHourglass
         
    sdbcFormato.RemoveAll
        
    sdbcFormato.AddItem m_sFormato & Chr(m_lSeparador) & "0"
    sdbcFormato.AddItem "Word" & Chr(m_lSeparador) & "1"
    sdbcFormato.AddItem "Excel" & Chr(m_lSeparador) & "2"
           
    sdbcFormato.SelStart = 0
    sdbcFormato.SelLength = Len(sdbcFormato.Text)
    sdbcFormato.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcGMN1_4Cod_Click()
    
    If Not sdbcGMN1_4Cod.DroppedDown Then
        sdbcGMN1_4Cod = ""
        sdbcGMN1_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    m_bGMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    m_bGMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    m_bGMN1CargarComboDesde = False
    
End Sub
Private Sub sdbcGMN1_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim m_oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    If m_bRMat Then
        
        Set m_oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN1 = Nothing
        If m_bGMN1CargarComboDesde Then
            Set m_oGruposMN1 = m_oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            Set m_oGruposMN1 = m_oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        
        If m_bGMN1CargarComboDesde Then
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        Else
            m_oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    Codigos = m_oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If m_bGMN1CargarComboDesde And Not m_oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim m_oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If m_bRMat Then
        Screen.MousePointer = vbHourglass
        Set m_oIMAsig = oUsuarioSummit.comprador
        Set oGMN1s = m_oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False)
        Screen.MousePointer = vbNormal
    Else
        Screen.MousePointer = vbHourglass
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
        Screen.MousePointer = vbNormal
    End If
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set m_oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
    Else
        m_bGMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
        m_bGMN1RespetarCombo = False
        GMN1Seleccionado
        m_bGMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing

End Sub

Private Sub sdbcGMN1_4Den_Click()
    
    If Not sdbcGMN1_4Den.DroppedDown Then
        sdbcGMN1_4Cod = ""
        sdbcGMN1_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN2_4Cod_Click()
    
    If Not sdbcGMN2_4Cod.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub
''' <summary>
''' Comprueba q el texto/valor dado es correcto para el combo de GMN2
''' </summary>
''' <param name="Cancel">Cancelar la edici�n</param>
''' <remarks>Llamada desde: Sistema, PonerMatSeleccionadoEnCombos ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim m_oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    
    If sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text Then
        m_bGMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
                
        Set m_oGMN2Seleccionado = Nothing
        Set m_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
        m_oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
        m_oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
                
        m_bGMN2RespetarCombo = False
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text Then
        m_bGMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
                
        Set m_oGMN2Seleccionado = Nothing
        Set m_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
        m_oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
        m_oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
                
        m_bGMN2RespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If m_bRMat Then
            Set m_oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = m_oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False)
        Else
             m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = m_oGMN1Seleccionado.GruposMatNivel2
        End If
        
        Screen.MousePointer = vbNormal
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        m_bGMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        
        m_bGMN2RespetarCombo = False
        GMN2Seleccionado
        m_bGMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    
End Sub

Private Sub sdbcGMN2_4Den_Click()
    
    If Not sdbcGMN2_4Den.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN3_4Cod_Click()
    
    If Not sdbcGMN3_4Cod.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub
''' <summary>
''' Comprueba q el texto/valor dado es correcto para el combo de GMN3
''' </summary>
''' <param name="Cancel">Cancelar la edici�n</param>
''' <remarks>Llamada desde: Sistema, PonerMatSeleccionadoEnCombos ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim m_oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text Then
        m_bGMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
                        
        Set m_oGMN3Seleccionado = Nothing
        Set m_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
        m_oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
        m_oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
        m_oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
  
        m_bGMN3RespetarCombo = False
        Exit Sub
    End If
    
    If sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text Then
        m_bGMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
        
        Set m_oGMN3Seleccionado = Nothing
        Set m_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
        m_oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
        m_oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
        m_oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
  
        m_bGMN3RespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If m_bRMat Then
            Set m_oIMAsig = oUsuarioSummit.comprador
             Set oGMN3s = m_oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False)
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = m_oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
    Else
        m_bGMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        
        m_bGMN3RespetarCombo = False
        GMN3Seleccionado
        m_bGMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not m_bGMN3RespetarCombo Then

        m_bGMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcEqpCod.Text = ""
        m_bGMN3RespetarCombo = False
        Set m_oGMN3Seleccionado = Nothing
        m_bGMN3CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN3_4Den_Click()
    
    If Not sdbcGMN3_4Den.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Den.Value = "" Then Exit Sub
    
    m_bGMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    m_bGMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    m_bGMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim m_oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Den.RemoveAll
    
    If m_oGMN2Seleccionado Is Nothing Then Exit Sub
            
    Screen.MousePointer = vbHourglass
    
    If m_bRMat Then
                
        Set m_oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN3 = Nothing
        If m_bGMN3CargarComboDesde Then
            Set m_oGruposMN3 = m_oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set m_oGruposMN3 = m_oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False)
        End If
        
    
    Else
        
        If m_bGMN3CargarComboDesde Then
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN3 = m_oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = m_oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If m_bGMN3CargarComboDesde And Not m_oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN4_4Cod_Click()
    If Not sdbcGMN4_4Cod.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

''' <summary>
''' Comprueba q el texto/valor dado es correcto para el combo de GMN4
''' </summary>
''' <param name="Cancel">Cancelar la edici�n</param>
''' <remarks>Llamada desde: Sistema, PonerMatSeleccionadoEnCombos ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim m_oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    If sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text Then
        m_bGMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
        m_bGMN4RespetarCombo = False
        If m_oGMN4Seleccionado Is Nothing Then
            GMN4Seleccionado
        End If
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text Then
        m_bGMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
        m_bGMN4RespetarCombo = False
        If m_oGMN4Seleccionado Is Nothing Then
            GMN4Seleccionado
        End If
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN3Seleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        If m_bRMat Then
            Set m_oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = m_oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False)
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = m_oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        m_bGMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        
        m_bGMN4RespetarCombo = False
        GMN4Seleccionado
        m_bGMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub

Private Sub sdbcGMN4_4Den_Click()
    
    If Not sdbcGMN4_4Den.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub

Private Sub sdbcMonCod_Change()
    
    If Not m_bMonRespetarCombo Then
    
        m_bMonRespetarCombo = True
        sdbcMonDen.Text = ""
        m_bMonRespetarCombo = False
        
        m_bMonCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcMonCod_CloseUp()
    
    If sdbcMonCod.Value = "..." Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    m_bMonRespetarCombo = True
    sdbcMonDen.Text = sdbcMonCod.Columns(1).Text
    sdbcMonCod.Text = sdbcMonCod.Columns(0).Text
    m_bMonRespetarCombo = False
    
    m_bMonCargarComboDesde = False
        
End Sub

Private Sub sdbcMonCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    If gParametrosGenerales.gbBloqModifProve Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set m_oMonedas = Nothing
    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
   
    sdbcMonCod.RemoveAll
    
    If m_bMonCargarComboDesde Then
        m_oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcMonCod.Text)
    Else
        m_oMonedas.CargarTodasLasMonedas
    End If
    
    Codigos = m_oMonedas.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcMonCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If m_bMonCargarComboDesde And Not m_oMonedas.EOF Then
        sdbcMonCod.AddItem "..."
    End If

    sdbcMonCod.SelStart = 0
    sdbcMonCod.SelLength = Len(sdbcMonCod.Text)
    sdbcMonCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMonCod_InitColumnProps()

    sdbcMonCod.DataFieldList = "Column 0"
    sdbcMonCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMonCod_PositionList(ByVal Text As String)
PositionList sdbcMonCod, Text
End Sub
Private Sub sdbcMonCod_Validate(Cancel As Boolean)

    Dim m_oMonedas As CMonedas
    Dim bExiste As Boolean
    
    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
    
    If sdbcMonCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    Screen.MousePointer = vbHourglass
    m_oMonedas.CargarTodasLasMonedas sdbcMonCod.Text, , True, , False, , True
    Screen.MousePointer = vbNormal
    
    bExiste = Not (m_oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMonCod.Text = ""
    Else
        m_bMonRespetarCombo = True
        sdbcMonDen.Text = m_oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        
        sdbcMonCod.Columns(0).Value = sdbcMonCod.Text
        sdbcMonCod.Columns(1).Value = sdbcMonDen.Text
        
        m_bMonRespetarCombo = False
        m_bMonCargarComboDesde = False
    End If
    
    Set m_oMonedas = Nothing
End Sub

Private Sub sdbcMonden_Change()
    If Not m_bMonRespetarCombo Then
        m_bMonRespetarCombo = True
        sdbcMonCod.Text = ""
        m_bMonRespetarCombo = False
        m_bMonCargarComboDesde = True
    
    End If
End Sub
Private Sub sdbcMonDen_CloseUp()
    
    If sdbcMonDen.Value = "..." Then
        sdbcMonDen.Text = ""
        Exit Sub
    End If
    
    m_bMonRespetarCombo = True
    sdbcMonCod.Text = sdbcMonDen.Columns(1).Text
    sdbcMonDen.Text = sdbcMonDen.Columns(0).Text
    m_bMonRespetarCombo = False
    
    m_bMonCargarComboDesde = False

End Sub

Private Sub sdbcMonDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set m_oMonedas = Nothing
    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
     
    sdbcMonDen.RemoveAll
    
    If m_bMonCargarComboDesde Then
        m_oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcMonDen.Text), True
    Else
        m_oMonedas.CargarTodasLasMonedas , , , True
    End If
    
    Codigos = m_oMonedas.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcMonDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If m_bMonCargarComboDesde And Not m_oMonedas.EOF Then
        sdbcMonDen.AddItem "..."
    End If

    sdbcMonDen.SelStart = 0
    sdbcMonDen.SelLength = Len(sdbcMonDen.Text)
    sdbcMonCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcMonDen_InitColumnProps()

    sdbcMonDen.DataFieldList = "Column 0"
    sdbcMonDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcMonDen_PositionList(ByVal Text As String)
PositionList sdbcMonDen, Text
End Sub

Private Sub sdbcPagoCod_Change()
 
 If Not m_bPagoRespetarCombo Then
    
    m_bPagoRespetarCombo = True
    sdbcPagoDen.Text = ""
    m_bPagoRespetarCombo = False
    
    Set m_oPagoSeleccionado = Nothing
        
End If
End Sub

Private Sub sdbcPagoCod_Click()
    
    If Not sdbcPagoCod.DroppedDown Then
        sdbcPagoCod = ""
        sdbcPagoDen = ""
    End If
    
End Sub

Private Sub sdbcPagoCod_CloseUp()
    
    If sdbcPagoCod.Value = "..." Then
        sdbcPagoCod.Text = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoDen.Text = sdbcPagoCod.Columns(1).Text
    sdbcPagoCod.Text = sdbcPagoCod.Columns(0).Text
    m_bPagoRespetarCombo = False
    
    Set m_oPagoSeleccionado = m_oPagos.Item(sdbcPagoCod.Columns(0).Text)
    
End Sub

Private Sub sdbcPagoCod_DropDown()

Dim Codigos As TipoDatosCombo
Dim i As Integer
        
Set m_oPagos = Nothing
Set m_oPagos = oFSGSRaiz.generar_CPagos
    
    Screen.MousePointer = vbHourglass
    
    sdbcPagoCod.RemoveAll
    
    m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoCod.AddItem "..."
    End If

    sdbcPagoCod.SelStart = 0
    sdbcPagoCod.SelLength = Len(sdbcPagoCod.Text)
    sdbcPagoCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPagoCod_InitColumnProps()
    sdbcPagoCod.DataFieldList = "Column 0"
    sdbcPagoCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcPagoCod_PositionList(ByVal Text As String)
PositionList sdbcPagoCod, Text
End Sub

Private Sub sdbcPagoCod_Validate(Cancel As Boolean)
    
    Dim m_oPagos As CPagos
    Dim bExiste As Boolean
    
    Set m_oPagos = oFSGSRaiz.generar_CPagos
    
    If sdbcPagoCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    m_oPagos.CargarTodosLosPagos sdbcPagoCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (m_oPagos.Count = 0)
    
    If Not bExiste Then
        sdbcPagoCod.Text = ""
    Else
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Text = m_oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcPagoCod.Columns(0).Value = sdbcPagoCod.Text
        sdbcPagoCod.Columns(1).Value = sdbcPagoDen.Text
        m_bPagoRespetarCombo = False
        
        Set m_oPagoSeleccionado = m_oPagos.Item(1)
        
    End If
    
    Set m_oPagos = Nothing
End Sub

Private Sub sdbcPagoDen_Change()
If Not m_bPagoRespetarCombo Then
    
        m_bPagoRespetarCombo = True
        sdbcPagoCod.Text = ""
        m_bPagoRespetarCombo = False
        Set m_oPagoSeleccionado = Nothing

    End If
    
End Sub

Private Sub sdbcPagoDen_CloseUp()
  
    If sdbcPagoDen.Value = "..." Then
        sdbcPagoDen.Text = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoCod.Text = sdbcPagoDen.Columns(1).Text
    sdbcPagoDen.Text = sdbcPagoDen.Columns(0).Text
    m_bPagoRespetarCombo = False
    
    Set m_oPagoSeleccionado = m_oPagos.Item(sdbcPagoDen.Columns(1).Text)
 
End Sub

Private Sub sdbcPagoDen_DropDown()

Dim Codigos As TipoDatosCombo
Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set m_oPagos = Nothing
    Set m_oPagos = oFSGSRaiz.generar_CPagos
     
    sdbcPagoDen.RemoveAll
    
    m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoDen.AddItem "..."
    End If

    sdbcPagoDen.SelStart = 0
    sdbcPagoDen.SelLength = Len(sdbcPagoDen.Text)

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPagoDen_InitColumnProps()

    sdbcPagoDen.DataFieldList = "Column 0"
    sdbcPagoDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcPagoDen_PositionList(ByVal Text As String)
PositionList sdbcPagoDen, Text
End Sub

Private Sub sdbcPaiCod_Change()
    
    If Not m_bPaiRespetarCombo Then
    
        m_bPaiRespetarCombo = True
        sdbcPaiDen.Text = ""
        m_bPaiRespetarCombo = False
        
        m_bPaiCargarComboDesde = True
        Set m_oPaisSeleccionado = Nothing
        
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
    
End Sub

Private Sub sdbcPaiCod_CloseUp()
    
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    m_bPaiRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    m_bPaiRespetarCombo = False
    
    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiCod.Columns(0).Text)
    
    If Not m_oPaisSeleccionado Is Nothing Then
        If Not m_oPaisSeleccionado.Moneda Is Nothing Then
            sdbcMonCod.Text = m_oPaisSeleccionado.Moneda.Cod
            sdbcMonCod_Validate False
        End If
    End If

    m_bPaiCargarComboDesde = False
   
End Sub

Private Sub sdbcPaiCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    
    Screen.MousePointer = vbHourglass
    
    sdbcPaiCod.RemoveAll
    
    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcPaiCod.Text), , , False
    Else
        m_oPaises.CargarTodosLosPaises , , , , , False
    End If
    
    Codigos = m_oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If m_bPaiCargarComboDesde And Not m_oPaises.EOF Then
        sdbcPaiCod.AddItem "..."
    End If

    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPaiCod_InitColumnProps()
    sdbcPaiCod.DataFieldList = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcPaiCod_PositionList(ByVal Text As String)
PositionList sdbcPaiCod, Text
End Sub
Private Sub sdbcPaiCod_Validate(Cancel As Boolean)

    Dim m_oPaises As CPaises
    Dim bExiste As Boolean
    
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    
    If sdbcPaiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    m_oPaises.CargarTodosLosPaises sdbcPaiCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (m_oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiCod.Text = ""
    Else
        m_bPaiRespetarCombo = True
        sdbcPaiDen.Text = m_oPaises.Item(1).Den
        sdbcPaiCod.Columns(0).Value = sdbcPaiCod.Text
        sdbcPaiCod.Columns(1).Value = sdbcPaiDen.Text
        m_bPaiRespetarCombo = False
        
        Set m_oPaisSeleccionado = m_oPaises.Item(1)
        m_bPaiCargarComboDesde = False
        If Not m_oPaisSeleccionado.Moneda Is Nothing Then
            sdbcMonCod.Text = m_oPaisSeleccionado.Moneda.Cod
            sdbcMonCod_Validate False
        End If

    End If
    
    Set m_oPaises = Nothing
End Sub
Private Sub sdbcPaiDen_Change()
    
    If Not m_bPaiRespetarCombo Then
    
        m_bPaiRespetarCombo = True
        sdbcPaiCod.Text = ""
        m_bPaiRespetarCombo = False
        m_bPaiCargarComboDesde = True
        Set m_oPaisSeleccionado = Nothing

    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
        
End Sub
Private Sub sdbcPaiDen_CloseUp()
    
    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    m_bPaiRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    m_bPaiRespetarCombo = False
    
    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiDen.Columns(1).Text)
    If Not m_oPaisSeleccionado Is Nothing Then
        If Not m_oPaisSeleccionado.Moneda Is Nothing Then
            sdbcMonCod.Text = m_oPaisSeleccionado.Moneda.Cod
            sdbcMonCod_Validate False
        End If
    End If
    
    m_bPaiCargarComboDesde = False
    
End Sub

Private Sub sdbcPaiDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
     
    sdbcPaiDen.RemoveAll
    
    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPaiDen.Text), True, False
    Else
        m_oPaises.CargarTodosLosPaises , , , True, , False
    End If
    
    Codigos = m_oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If m_bPaiCargarComboDesde And Not m_oPaises.EOF Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiCod.Refresh

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcPaiDen_InitColumnProps()

    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
    
End Sub


Private Sub sdbcPaiDen_PositionList(ByVal Text As String)
PositionList sdbcPaiDen, Text
End Sub

Private Sub sdbcProveCod_Change()
    
    If Not m_bRespetarComboProve Then
    
        m_bRespetarComboProve = True
        sdbcProveDen.Text = ""
        BorrarTodo
        ModoConsulta
        m_bRespetarComboProve = False
                
        Set g_oProveSeleccionado = Nothing
        
    End If
  
End Sub

Private Sub sdbcProveCod_Click()
    
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
    
End Sub

Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
      
    If sdbcProveCod.Value = "" Then Exit Sub
    
    m_bRespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    m_bRespetarComboProve = False
    
    DoEvents
    
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll
    
    m_oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod), Trim(sdbcProveCod.Text), , Trim(sdbcFiltroPaisCod), , Trim(sdbcFiltroProviCod), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False, , , , , ChkSoloProveWeb, , , , , , , , txtFiltroVAT.Text
    
    Codigos = m_oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next
    
    If Not m_oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    If sdbcProveCod.Text = sdbcProveCod.Columns(0).Text Then
        m_bRespetarComboProve = True
        sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
        m_bRespetarComboProve = False
        If g_oProveSeleccionado Is Nothing Then
            Screen.MousePointer = vbHourglass
            ProveedorSeleccionado
            Screen.MousePointer = vbNormal
        End If
        Exit Sub
    End If
    
    If sdbcProveCod.Text = sdbcProveDen.Columns(1).Text Then
        m_bRespetarComboProve = True
        sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
        m_bRespetarComboProve = False
        If g_oProveSeleccionado Is Nothing Then
            Screen.MousePointer = vbHourglass
            ProveedorSeleccionado
            Screen.MousePointer = vbNormal
        End If
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proveedor
    Screen.MousePointer = vbHourglass
    m_oProves.BuscarTodosLosProveedoresDesde 1, Trim(sdbcEqpCod), Trim(sdbcProveCod.Text), , Trim(sdbcFiltroPaisCod), , Trim(sdbcFiltroProviCod), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False, , , , , ChkSoloProveWeb, , , , , , , , txtFiltroVAT.Text
    Screen.MousePointer = vbNormal
    
    bExiste = Not (m_oProves.Count = 0)
    If bExiste Then bExiste = (UCase(m_oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        m_bRespetarComboProve = True
        sdbcProveDen.Text = m_oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Text = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Text = sdbcProveDen.Text
            
        m_bRespetarComboProve = False
        
        Screen.MousePointer = vbHourglass
        ProveedorSeleccionado
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub sdbcProveDen_Change()
    
    If Not m_bRespetarComboProve Then
    
        m_bRespetarComboProve = True
        sdbcProveCod.Text = ""
        BorrarTodo
        ModoConsulta
        m_bRespetarComboProve = False
        
        Set g_oProveSeleccionado = Nothing
    End If
    
End Sub

Private Sub sdbcProveDen_Click()
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveDen = ""
        sdbcProveCod = ""
    End If
End Sub

Private Sub sdbcProveDen_CloseUp()
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    m_bRespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    m_bRespetarComboProve = False
    
    DoEvents
    
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll
    
    m_oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod), , Trim(sdbcProveDen), Trim(sdbcFiltroPaisCod), , Trim(sdbcFiltroProviCod), Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , True, , , , False, , , , , ChkSoloProveWeb, , , , , , , , txtFiltroVAT.Text
    
    Codigos = m_oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    If Not m_oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Private Sub sdbcProviCod_Change()
    If Not m_bProviRespetarCombo Then
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = ""
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = True
    End If
End Sub

Private Sub sdbcProviCod_Click()
    If Not sdbcProviCod.DroppedDown Then
        sdbcProviCod = ""
        sdbcProviDen = ""
    End If
End Sub

Private Sub sdbcProviCod_CloseUp()
    
    If sdbcProviCod.Value = "..." Then
        sdbcProviCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProviCod.Value = "" Then Exit Sub
    
    m_bProviRespetarCombo = True
    sdbcProviDen.Text = sdbcProviCod.Columns(1).Text
    sdbcProviCod.Text = sdbcProviCod.Columns(0).Text
    m_bProviRespetarCombo = False
    
    m_bProviCargarComboDesde = False
        
End Sub

Private Sub sdbcProviCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProviCod.RemoveAll
        
    If Not m_oPaisSeleccionado Is Nothing Then
        
        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProviCod.Text), , , False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , , False
        End If
        
        Codigos = m_oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
        
        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.EOF Then
            sdbcProviCod.AddItem "..."
        End If

    End If
    
    sdbcProviCod.SelStart = 0
    sdbcProviCod.SelLength = Len(sdbcProviCod.Text)
    sdbcProviCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProviCod_InitColumnProps()

    sdbcProviCod.DataFieldList = "Column 0"
    sdbcProviCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProviCod_PositionList(ByVal Text As String)
PositionList sdbcProviCod, Text
End Sub
Private Sub sdbcProviCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProviCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe la provincia
    
    If Not m_oPaisSeleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        m_oPaisSeleccionado.CargarTodasLasProvincias sdbcProviCod.Text, , True, , False
    
        bExiste = Not (m_oPaisSeleccionado.Provincias.Count = 0)
        Screen.MousePointer = vbNormal
    Else
    
        bExiste = False
        
    End If
    
        
    If Not bExiste Then
        sdbcProviCod.Text = ""
    Else
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = m_oPaisSeleccionado.Provincias.Item(1).Den
        
        sdbcProviCod.Columns(0).Value = sdbcProviCod.Text
        sdbcProviCod.Columns(1).Value = sdbcProviDen.Text
        
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = False
    End If
    
End Sub
Private Sub sdbcProviDen_Change()
    
    If Not m_bProviRespetarCombo Then
    
        m_bProviRespetarCombo = True
        sdbcProviCod.Text = ""
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcProviDen_Click()
    
    If Not sdbcProviDen.DroppedDown Then
        sdbcProviCod = ""
        sdbcProviDen = ""
    End If
    
End Sub

Private Sub sdbcProviDen_CloseUp()
    
    If sdbcProviDen.Value = "..." Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProviDen.Value = "" Then Exit Sub
    
    m_bProviRespetarCombo = True
    sdbcProviCod.Text = sdbcProviDen.Columns(1).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(0).Text
    m_bProviRespetarCombo = False
    
    m_bProviCargarComboDesde = False
        
End Sub
Private Sub sdbcProviDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProviDen.RemoveAll
    
    If Not m_oPaisSeleccionado Is Nothing Then
         
        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProviDen.Text), True, False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If
        
        Codigos = m_oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
        
        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.EOF Then
            sdbcProviDen.AddItem "..."
        End If

    End If
    
    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
    sdbcProviCod.Refresh

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcProviDen_InitColumnProps()

    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub ProveedorSeleccionado()
    m_oProves.CargarDatosProveedor sdbcProveCod, False, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    
    Set g_oProveSeleccionado = Nothing
    Set g_oProveSeleccionado = m_oProves.Item(sdbcProveCod.Text)
       
    If Not g_oProveSeleccionado Is Nothing Then
        With g_oProveSeleccionado
            ''' Pasamos el usuario que realiza la modificaci�n (LOG e Integraci�n)
            .Usuario = basOptimizacion.gvarCodUsuario

            m_bCal1CargarComboDesde = False
            sdbcCal1Cod_DropDown
            m_bCal2CargarComboDesde = False
            sdbcCal2Cod_DropDown
            m_bCal3CargarComboDesde = False
            sdbcCal3Cod_DropDown
            stabProve.Enabled = True
            MostrarDatosProveedor
             
            If Trim(.Calif1) <> "" And Not m_oCalificaciones1.Item(.Calif1) Is Nothing Then
                lblDescRango1 = .Calif1 & " " & m_oCalificaciones1.Item(.Calif1).Den
            Else
                lblDescRango1 = ""
            End If
            
            If Trim(.Calif2) <> "" And Not m_oCalificaciones2.Item(.Calif2) Is Nothing Then
                lblDescRango2 = .Calif2 & " " & m_oCalificaciones2.Item(.Calif2).Den
            Else
                lblDescRango2 = ""
            End If
            If Trim(.Calif3) <> "" And Not m_oCalificaciones3.Item(.Calif3) Is Nothing Then
                lblDescRango3 = .Calif3 & " " & m_oCalificaciones3.Item(.Calif3).Den
            Else
                lblDescRango3 = ""
            End If
            
            txtVal1 = ""
            txtVal2 = ""
            txtVal3 = ""
            txtVal1 = NullToStr(.Val1)
            txtVal2 = NullToStr(.Val2)
            txtVal3 = NullToStr(.Val3)
            'Rellena los combos de calificaciones
            If Not IsNull(.Calif1) And .Calif1 <> "" Then
                sdbcCal1Cod.Columns(0).Value = .Calif1
                sdbcCal1Cod.Columns(1).Value = m_oCalificaciones1.Item(.Calif1).Den
                sdbcCal1Den.Columns(1).Value = .Calif1
                sdbcCal1Den.Columns(0).Value = m_oCalificaciones1.Item(.Calif1).Den
            Else
                sdbcCal1Cod.Columns(0).Value = ""
                sdbcCal1Cod.Columns(1).Value = ""
                sdbcCal1Den.Columns(1).Value = ""
                sdbcCal1Den.Columns(0).Value = ""
            End If
            
            If Not IsNull(.Calif2) And .Calif2 <> "" Then
                sdbcCal2Cod.Columns(0).Value = .Calif2
                sdbcCal2Cod.Columns(1).Value = m_oCalificaciones2.Item(.Calif2).Den
                sdbcCal2Den.Columns(1).Value = .Calif2
                sdbcCal2Den.Columns(0).Value = m_oCalificaciones2.Item(.Calif2).Den
            Else
                sdbcCal2Cod.Columns(0).Value = ""
                sdbcCal2Cod.Columns(1).Value = ""
                sdbcCal2Den.Columns(1).Value = ""
                sdbcCal2Den.Columns(0).Value = ""
            End If
            
            If Not IsNull(.Calif3) And .Calif3 <> "" Then
                sdbcCal3Cod.Columns(0).Value = .Calif3
                sdbcCal3Cod.Columns(1).Value = m_oCalificaciones3.Item(.Calif3).Den
                sdbcCal3Den.Columns(1).Value = .Calif3
                sdbcCal3Den.Columns(0).Value = m_oCalificaciones3.Item(.Calif3).Den
            Else
                sdbcCal3Cod.Columns(0).Value = ""
                sdbcCal3Cod.Columns(1).Value = ""
                sdbcCal3Den.Columns(1).Value = ""
                sdbcCal3Den.Columns(0).Value = ""
            End If
            
            'Cargamos las especificaciones
            txtObs = NullToStr(.obs)
            .CargarTodasLasEspecificaciones True, False
            AnyadirEspsALista
            If m_bConsultaEspPortalProve Then
                If Not IsNull(.CodPortal) Then
                    AnyadirEspsProvePortalALista
                    'Volvemos a mostrar la lista de especificaciones del portal por si antes
                    'hemos seleccionado un prove no enlazado y se ha ocultado
                    lblEspProvePortal.Visible = True
                    lstvwEspProvePortal.Visible = True
                    picEspProvePortal.Visible = True
                    lstvwEspProvePortal.Height = stabProve.Height / 3 - 100
                    lstvwEspProvePortal.Width = stabProve.Width - 300
                    lstvwEspProvePortal.ColumnHeaders(1).Width = lstvwEspProvePortal.Width * 0.2
                    lstvwEspProvePortal.ColumnHeaders(2).Width = lstvwEspProvePortal.Width * 0.1
                    lstvwEspProvePortal.ColumnHeaders(3).Width = lstvwEspProvePortal.Width * 0.5
                    lstvwEspProvePortal.ColumnHeaders(4).Width = lstvwEspProvePortal.Width * 0.19
                    picEspProvePortal.Top = lstvwEspProvePortal.Top + lstvwEspProvePortal.Height + 20
                    If stabProve.Tab = 4 Then
                        picEspProvePortal.Left = lstvwEspProvePortal.Width - (cmdAbrirEspPortal.Width * 6) - 100
                    End If
                    lstvwEsp.Top = picEspProvePortal.Top + picEspProvePortal.Height
                    If stabProve.Height > 5000 Then
                        lstvwEsp.Height = lstvwEspProvePortal.Height + (lstvwEspProvePortal.Height * 0.3)
                    Else
                        lstvwEsp.Height = lstvwEspProvePortal.Height + (lstvwEspProvePortal.Height * 0.125)
                    End If
                    picEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 60
                    lstvwEsp.Width = lstvwEspProvePortal.Width
                Else
                    'Si el prove no est� enlazado ocultamos la lista de especificaciones del portal
                    OcultarEspPortal
                End If
            End If
            
            lblCodPortal.Text = NullToStr(.CodPortal)
            
            .CargarTodosLosContactos , , , , True, , , , , , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
            
            sdbgContactos.ReBind
            
            
            If m_bModifDatGen Or m_bModifCalif Then cmdModificar.Enabled = True
            If m_bEliminar Then cmdEliminar.Enabled = True
                        
            If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador And Not basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Prove) Then
                cmdCodigo.Enabled = True
            End If
                              
            .CargarEmpresas
            MostrarEmpresas
            
            Dim oEmpresa As CEmpresa
            For Each oEmpresa In .Empresas

                Exit For
            Next
            If Not oEmpresa Is Nothing Then
                oEmpresa.CargarERPs .Cod
                lstvwEmpresas_ItemClick lstvwEmpresas.ListItems(1)
            End If
            
            'Cargar los proveedores relacionados
            CargarProvesRelacionados 1
        End With
        
        cmdActualizar.Enabled = True
    Else
        cmdEliminar.Enabled = False
        cmdModificar.Enabled = False
        cmdActualizar.Enabled = False
        cmdCodigo.Enabled = False
        cmdDatosPortal.Visible = False
        cmdDatosPortal.Enabled = False
    End If
End Sub

Private Sub AnyadirEspsProvePortalALista()
Dim oEspProvePortal As CEspecificacion
With lstvwEspProvePortal.ListItems
    .clear
    
    For Each oEspProvePortal In g_oProveSeleccionado.EspecificacionesProvePortal
        .Add , "ESP" & CStr(oEspProvePortal.Id), oEspProvePortal.nombre, , "ESP"
        .Item("ESP" & CStr(oEspProvePortal.Id)).ToolTipText = NullToStr(oEspProvePortal.Comentario)
        .Item("ESP" & CStr(oEspProvePortal.Id)).ListSubItems.Add , "Tamanyo", IIf(oEspProvePortal.DataSize = 0, "", TamanyoAdjuntos(oEspProvePortal.DataSize / 1024) & " " & m_skb)
        .Item("ESP" & CStr(oEspProvePortal.Id)).ListSubItems.Add , "Com", NullToStr(oEspProvePortal.Comentario)
        .Item("ESP" & CStr(oEspProvePortal.Id)).ListSubItems.Add , "Fec", oEspProvePortal.Fecha
        .Item("ESP" & CStr(oEspProvePortal.Id)).Tag = oEspProvePortal.Id
    Next
End With
Set oEspProvePortal = Nothing

End Sub
Private Sub AnyadirEspsALista()
Dim oEsp As CEspecificacion
    With lstvwEsp.ListItems
    .clear
    
    For Each oEsp In g_oProveSeleccionado.especificaciones
        .Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        .Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        .Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", IIf(oEsp.DataSize = 0, "", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb)
        .Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        .Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        .Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next
    
    End With
    Set oEsp = Nothing
End Sub

''' <summary>Pasos previos a a�adir un proveedor</summary>
''' <remarks>Llamada desde: frmPROVE.cmdA�adir_Click; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub PrepararAnyadir()

    BorrarTodo
    
    sTabGeneral.TabEnabled(0) = False
    stabProve.Enabled = True
    fraGeneral.Enabled = True
    fraComunGeneral.Enabled = True

    sdbcProveCod.Visible = False
    sdbcProveDen.Visible = False
    txtCodProve.Enabled = True
    txtDenProve.Enabled = True
    txtCodProve.Visible = True
    txtDenProve.Visible = True
    txtCodProve = ""
    txtDenProve = ""
    
    If gParametrosInstalacion.gsPais = "" Then
        If gParametrosGenerales.gsPAIDEF <> "" Then
            gParametrosInstalacion.gsPais = gParametrosGenerales.gsPAIDEF
            g_GuardarParametrosIns = True
        End If
    End If
    sdbcPaiCod.Text = gParametrosInstalacion.gsPais
    sdbcPaiCod_Validate False
    
    If gParametrosInstalacion.gsProvincia = "" Then
        If gParametrosGenerales.gsPROVIDEF <> "" Then
            gParametrosInstalacion.gsProvincia = gParametrosGenerales.gsPROVIDEF
            g_GuardarParametrosIns = True
        End If
    End If
    sdbcProviCod.Text = gParametrosInstalacion.gsProvincia
    sdbcProviCod_Validate False
    
    If gParametrosInstalacion.gsMoneda = "" Then
        If gParametrosGenerales.gsMONCEN <> "" Then
            gParametrosInstalacion.gsMoneda = gParametrosGenerales.gsMONCEN
            g_GuardarParametrosIns = True
        End If
    End If
    sdbcMonCod.Text = gParametrosInstalacion.gsMoneda
    sdbcMonCod_Validate False
    
    sdbcCal1Cod = ""
    sdbcCal2Cod = ""
    sdbcCal3Cod = ""
    txtVal1 = ""
    txtVal2 = ""
    txtVal3 = ""
    If m_bModifCalif Then
        picCalif.Enabled = True
        sdbcCal1Cod.Visible = True
        sdbcCal2Cod.Visible = True
        sdbcCal3Cod.Visible = True
        sdbcCal1Den.Visible = True
        sdbcCal2Den.Visible = True
        sdbcCal3Den.Visible = True
        lblDescRango1.Visible = False
        lblDescRango2.Visible = False
        lblDescRango3.Visible = False
    End If
    
    
    ' Preparar para a�adir en la grid de relaciones
    CargarProvesRelacionados 1
    sdbgProveRelac.AllowAddNew = True
    sdbgProveRelac.AllowUpdate = True
    sdbgProveRelac.AllowDelete = True
    sdbgProveRelac.TabNavigation = ssMoveToNextCell
    sdbgProveRelac.ActiveRowStyleSet = "(No StyleSet)"
    sdbgProveRelac.SelectTypeRow = ssSelectionTypeSingleSelect
    sdbgProveRelac.col = 1
    
    cmdA�adirProveRelac.Visible = True
    cmdEliminarProveRelac.Visible = True
    cmdDeshacerProveRelac.Visible = True
    cmdA�adirProveRelac.Enabled = True
    cmdEliminarProveRelac.Enabled = False
    cmdDeshacerProveRelac.Enabled = False
    
    sdbgContactos.ReBind
    sdbgContactos.AllowAddNew = True
    sdbgContactos.AllowUpdate = True
    sdbgContactos.AllowDelete = True
    sdbgContactos.TabNavigation = ssMoveToNextCell
    sdbgContactos.ActiveRowStyleSet = "(No StyleSet)"
    sdbgContactos.SelectTypeRow = ssSelectionTypeSingleSelect
    sdbgContactos.col = 1
    
    cmdA�adirContacto.Visible = True
    cmdEliminarContacto.Visible = True
    cmdDeshacerContacto.Visible = True
    cmdA�adirContacto.Enabled = True
    cmdEliminarContacto.Enabled = False
    cmdDeshacerContacto.Enabled = False
    
    txtObs = ""
    txtObs.Locked = False
    
    picNavigate.Visible = False
    picEdit.Visible = True
    
    lstvwEsp.ListItems.clear
    lstvwEspProvePortal.ListItems.clear
    OcultarEspPortal
    ModoEdicion
    If Me.Visible Then txtCodProve.SetFocus
    If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove) Then
        LinIntegracion.Visible = False
        PicIntegracion.Visible = False
        lblEstInt.Visible = False
    End If
    
End Sub

''' <summary>Pasos previos a modificar un proveedor</summary>
''' <remarks>Llamada desde: frmPROVE.cmdModificar_Click; Tiempo m�ximo:0,1</remarks>
Private Sub PrepararModificar()
    m_bCambiandoModo = True
    
    sdbgProveRelac.col = 3

    sTabGeneral.TabEnabled(0) = False
    stabProve.Enabled = True
    
    sdbcProveCod.Visible = False
    sdbcProveDen.Visible = False
    
    If m_bModifEspProve Then
        cmdAnyaEsp.Enabled = True
        cmdEliEsp.Enabled = True
        cmdModifEsp.Enabled = True
    End If
    
    If m_bConsultaEspPortalProve Then
        cmdCopiarEspPortal.Enabled = True
    End If
    
    If gParametrosGenerales.giINSTWEB = ConPortal And Not IsNull(g_oProveSeleccionado.CodPortal) Then
        txtCodProve.Visible = True
        txtCodProve.Enabled = False
        lblDenProve.Visible = True
        txtCodProve = sdbcProveCod.Text
        lblDenProve = sdbcProveDen.Text
        txtDenProve = sdbcProveDen.Text
    Else
        txtCodProve.Visible = True
        txtCodProve.Enabled = False
        txtDenProve.Visible = True
        txtCodProve = sdbcProveCod.Text
        txtDenProve = sdbcProveDen.Text
        
        BloquearDatosProve gParametrosGenerales.gbBloqModifProve
    End If
    
    If m_bModifCalif Then
            
        picCalif.Enabled = True
        sdbcCal1Cod.Visible = True
        sdbcCal2Cod.Visible = True
        sdbcCal3Cod.Visible = True
        
        sdbcCal1Den.Visible = True
        sdbcCal2Den.Visible = True
        sdbcCal3Den.Visible = True
        
        m_bCalif = True
    
        If Trim(g_oProveSeleccionado.Calif1) <> "" Then
            sdbcCal1Cod.Text = g_oProveSeleccionado.Calif1
            sdbcCal1Cod_Validate False
        Else
            sdbcCal1Cod.Text = ""
        End If
        
        If Trim(g_oProveSeleccionado.Calif2) <> "" Then
            sdbcCal2Cod.Text = g_oProveSeleccionado.Calif2
            sdbcCal2Cod_Validate False
        Else
            sdbcCal2Cod.Text = ""
        End If
        
        If Trim(g_oProveSeleccionado.Calif3) <> "" Then
            sdbcCal3Cod.Text = g_oProveSeleccionado.Calif3
            sdbcCal3Cod_Validate False
        Else
            sdbcCal3Cod.Text = ""
        End If
        
        lblDescRango1.Visible = False
        lblDescRango2.Visible = False
        lblDescRango3.Visible = False
        If Not m_bModifDatGen Then
            txtDenProve.Enabled = False
        End If
    End If
        
    picNavigate.Visible = False
    picEdit.Visible = True
    
    
    ModoEdicion
    
   m_bCalif = False
    If gParametrosGenerales.giINSTWEB <> ConPortal Or IsNull(g_oProveSeleccionado.CodPortal) Then
        If m_bModifDatGen Then
            If txtDenProve.Enabled Then
                If Me.Visible Then txtDenProve.SetFocus
            Else
                If Me.Visible Then txtDIR.SetFocus
            End If
        End If
    End If
m_bCambiandoModo = False
End Sub

'**********************************************************************************************
'*** Descripci�n: Bloque la edici�n de la raz�n Social, NIF y Moneda del Proveedor.       *****
'*** Par�metros: bBloquear: Indica si hay que bloquear/desbloquear los datos indicados.   *****
'*** Valor que devuelve:                                                                  *****
'**********************************************************************************************

Private Sub BloquearDatosProve(ByVal bBloquear As Boolean)
    txtDenProve.Locked = bBloquear
    txtNIF.Locked = bBloquear
    sdbcMonCod.Enabled = Not bBloquear
    sdbcMonDen.Enabled = Not bBloquear
    
    
    If bBloquear Then
        txtDenProve.Backcolor = &HE1FFFF
        txtNIF.Backcolor = &HE1FFFF
        sdbcMonCod.Backcolor = &HE1FFFF
        sdbcMonDen.Backcolor = &HE1FFFF
    Else
        txtDenProve.Backcolor = &H80000005
        txtNIF.Backcolor = &H80000005
        sdbcMonCod.Backcolor = &H80000005
        sdbcMonDen.Backcolor = &H80000005
    End If
End Sub

Private Sub ModoEdicion()
    m_bModoEdicion = True
    Arrange
    
    cmdBuscar.Visible = False
    
    If m_bModifDatGen Then
        If gParametrosGenerales.giINSTWEB <> ConPortal Or IsNull(g_oProveSeleccionado.CodPortal) Then
            fraGeneral.Enabled = True
        Else
            fraGeneralPortal.Enabled = True
        End If
        fraComunGeneral.Enabled = True
        fraImpuestos.Enabled = (Me.chkAutoFactura.Value = vbChecked)
        fraAcepRecepPedidos.Enabled = True
        cmdA�adirContacto.Visible = True
        cmdEliminarContacto.Visible = True
        cmdDeshacerContacto.Visible = True
        cmdA�adirContacto.Enabled = True
        cmdEliminarContacto.Enabled = False
        cmdDeshacerContacto.Enabled = False
        sdbgContactos.AllowUpdate = True
        sdbgContactos.AllowAddNew = True
        sdbgContactos.AllowDelete = True
        txtObs.Locked = False
        
        'Modo edici�n proveedores relacionados
        cmdA�adirProveRelac.Visible = True
        cmdEliminarProveRelac.Visible = True
        cmdDeshacerProveRelac.Visible = True
        cmdA�adirProveRelac.Enabled = True
        cmdEliminarProveRelac.Enabled = False
        cmdDeshacerProveRelac.Enabled = False
        sdbgProveRelac.AllowUpdate = True
        sdbgProveRelac.AllowAddNew = True
        sdbgProveRelac.AllowDelete = False
        sdbgProveedoresPri.Columns("CAR").CellStyleSet "FlechaDesenf"
    Else
        cmdEliminarContacto.Visible = False
        cmdA�adirContacto.Visible = False
        sdbgContactos.AllowUpdate = False
                
       'Modo edici�n proveedores relacionados
        cmdEliminarProveRelac.Visible = False
        cmdA�adirProveRelac.Visible = False
        sdbgProveRelac.AllowUpdate = False

        fraGeneral.Enabled = False
        fraGeneralPortal.Enabled = False
        fraComunGeneral.Enabled = False
        fraImpuestos.Enabled = False
        fraAcepRecepPedidos.Enabled = False
        txtObs.Locked = True
        sdbgContactos.AllowUpdate = False
        sdbgContactos.AllowAddNew = False
        sdbgContactos.AllowDelete = False
        'Modo edici�n proveedores relacionados
        sdbgProveRelac.AllowUpdate = False
        sdbgProveRelac.AllowAddNew = False
        sdbgProveRelac.AllowDelete = False
        sdbgProveedoresPri.Columns("CAR").CellStyleSet "FlechaDesenf"

        txtObs.Locked = True
    End If
    
    If g_bModifDatweb Then Frame4.Enabled = False
        
    sdbgContactos.TabNavigation = ssMoveToNextCell
    sdbgContactos.ActiveRowStyleSet = "(No StyleSet)"
    sdbgContactos.SelectTypeRow = ssSelectionTypeSingleSelect
    
    'Modo edici�n proveedores relacionados
    sdbgProveRelac.TabNavigation = ssMoveToNextCell
    sdbgProveRelac.ActiveRowStyleSet = "(No StyleSet)"
     
    If g_oProveSeleccionado.Contactos.Count = 0 Then
        sdbgContactos.col = 0
    Else
        sdbgContactos.col = 1
    End If
    
    'Modo edici�n proveedores relacionados
    If g_oProveSeleccionado.ProveRel.Count = 0 Then
        sdbgProveRelac.col = 0
    Else
        sdbgProveRelac.col = 1
    End If
        
    If m_bModifCalif Then
        picCalif.Enabled = True
    Else
        picCalif.Enabled = False
    End If
    
    sdbddTipoRel.Enabled = True
    DoEvents
End Sub

Private Sub ModoConsulta()
        
    m_bModoEdicion = False
    m_AccionCont = ACCContCon
    Arrange
    

    cmdBuscar.Visible = True
    cmdA�adirContacto.Visible = False
    cmdEliminarContacto.Visible = False
    cmdDeshacerContacto.Visible = False
    
    sdbgContactos.TabNavigation = ssMoveToNextControl
    sdbgContactos.ActiveRowStyleSet = "ActiveRow"
    sdbgContactos.AllowUpdate = False
    sdbgContactos.AllowAddNew = False
    sdbgContactos.AllowDelete = False
    
    'Modo consulta proveedores relacionados
    cmdA�adirProveRelac.Visible = False
    cmdEliminarProveRelac.Visible = False
    cmdDeshacerProveRelac.Visible = False
    
    sdbgProveRelac.TabNavigation = ssMoveToNextControl
    sdbgProveRelac.ActiveRowStyleSet = "ActiveRow"
    sdbgProveRelac.AllowUpdate = False
    sdbgProveRelac.AllowAddNew = False
    sdbgProveRelac.AllowDelete = False
        
    picCalif.Enabled = False
    txtObs.Locked = True
    
    cmdAbrirEsp.Enabled = True
    cmdSalvarEsp.Enabled = True
    picNavigate.Visible = True
    picEdit.Visible = False
    
    If m_bModifDatGen Then ' solo si se pueden modificar los datos generales, hay que deshabilitar los botones de adjuntos
        cmdAnyaEsp.Enabled = False
        cmdCopiarEspPortal.Enabled = False
        cmdModifEsp.Enabled = False
        cmdEliEsp.Enabled = False
    End If
    
    sdbddTipoRel.Enabled = False
End Sub

Private Sub sdbcProviDen_PositionList(ByVal Text As String)
PositionList sdbcProviDen, Text
End Sub

Private Sub sdbgCodERP_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
        
    For i = 0 To sdbgCodERP.Columns.Count - 1
        sdbgCodERP.Columns(i).CellStyleSet IIf(sdbgCodERP.Columns("BAJALOG").Value = "1", "Baja", "Activo")
    Next
End Sub

''' <summary>Comprobaciones de telf. y email antes de actualizar cada columna.</summary>
''' <param name="Cancel">Cancela el cambio de columna o fila</param>

Private Sub sdbgContactos_BeforeRowColChange(Cancel As Integer)
    Dim bOk As Boolean
    Dim iMensaje As Integer
    Dim sPaiDen As String
    
    On Error GoTo Error
    
    If Not m_bCancelCheckContacto Then
        bOk = True
        
        With sdbgContactos
            If .col <> -1 Then
                If Not .Columns(.col).Locked Then
                    If Trim(.Columns(.col).Value) <> "" Then
                        If sdbcPaiDen.Visible Then
                            sPaiDen = sdbcPaiDen.Text
                        Else
                            sPaiDen = lblDenPai.caption
                        End If
        
                        Select Case .Columns(.col).Name
                            Case "TFNO"
                                If gParametrosGenerales.gbCompruebaTfno Then
                                    bOk = ComprobarTelefono(.Columns(.col).Value, sPaiDen)
                                    iMensaje = 1131
                                End If
                            Case "TFNO2"
                                If gParametrosGenerales.gbCompruebaTfno Then
                                    bOk = ComprobarTelefono(.Columns(.col).Value, sPaiDen)
                                    iMensaje = 1131
                                End If
                            Case "TFNO_MOVIL"
                                If gParametrosGenerales.gbCompruebaTfno Then
                                    bOk = ComprobarTelefono(.Columns(.col).Value, sPaiDen)
                                    iMensaje = 1131
                                End If
                            Case "FAX"
                                If gParametrosGenerales.gbCompruebaTfno Then
                                    bOk = ComprobarTelefono(.Columns(.col).Value, sPaiDen)
                                    iMensaje = 1131
                                End If
                            Case "MAIL"
                                bOk = ComprobarEmail(.Columns(.col).Value)
                                iMensaje = 1132
                        End Select
                        
                        If Not bOk Then
                            oMensajes.MensajeOKOnly iMensaje
                            If stabProve.Tab <> 1 Then stabProve.Tab = 1
                            If Me.Visible Then .SetFocus
                            '.col = .col 'Refrescar la posici�n del cursor cuando el grid no estaba activo
                            Cancel = True
                        End If
                        
                        m_TelfMailError = Cancel
                    End If
                End If
            End If
        End With
    End If
    
    Exit Sub
Error:
    On Error Resume Next
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
End Sub

Private Sub sdbgContactos_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    
    With sdbgContactos
    If .Columns("PORTAL").Value = "1" And gParametrosGenerales.giINSTWEB = ConPortal Then
        ' FALTA
        For i = 1 To .Cols - 1
            .Columns(i).CellStyleSet "Tan"
        Next i
    End If

    ''' INTEGRACION
    If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove) Then
        If .Columns("INT").Visible Then
        If .Columns("INT").Value = "" Or .Columns("INT").Value = CStr(EstadoIntegracion.recibidocorrecto) Then
            .Columns("INT").Value = "    " & m_sIdiSincronizado
            .Columns("INT").CellStyleSet "InOK", Bookmark
        Else
            .Columns("INT").Value = "     " & m_sIdiNoSincronizado
            .Columns("INT").CellStyleSet "IntKO", Bookmark
        End If
    End If
    End If
    End With

End Sub

Private Sub sdbgContactos_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
    
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    m_bModError = False
    
    ''' Modificamos en la base de datos
    
    'PENDIENTE DE CORREGIR
    If m_oContactoEnEdicion Is Nothing Then Exit Sub
    
    With m_oContactoEnEdicion
        .Apellidos = sdbgContactos.Columns("APE").Value
        .nombre = sdbgContactos.Columns("DEN").Value
        .Departamento = sdbgContactos.Columns("DEP").Value
        .Cargo = sdbgContactos.Columns("CAR").Value
        .Tfno = sdbgContactos.Columns("TFNO").Value
        .Tfno2 = sdbgContactos.Columns("TFNO2").Value
        .Fax = sdbgContactos.Columns("FAX").Value
        .tfnomovil = sdbgContactos.Columns("TFNO_MOVIL").Value
        .mail = sdbgContactos.Columns("MAIL").Value
        .NIF = sdbgContactos.Columns("NIF").Value
        .Def = val(sdbgContactos.Columns("DEF").Value)
        .Aprovisionador = val(sdbgContactos.Columns("APROV").Value)
        .NotifSubasta = val(sdbgContactos.Columns("SUB").Value)
        .Calidad = val(sdbgContactos.Columns("CALIDAD").Value)
        .Principal = val(sdbgContactos.Columns("PRINCIPAL").Value)
    End With
    
    m_AccionCont = ACCContCon
        
    Set m_oContactoEnEdicion = Nothing
   
        
End Sub

Private Sub sdbgProveedoresPri_BtnClick()
    Select Case sdbgProveedoresPri.Columns(sdbgProveedoresPri.col).Name
        Case "INF"
            'Muestra el detalle del proveedor:
            MostrarDetalleProveedorPri (sdbgProveedoresPri.Columns("COD").Value)
        Case "CAR"
            'Carga el proveedor principal:
            If m_bModoEdicion = False Then
                CargaProveedorPri (sdbgProveedoresPri.Columns("COD").Value)
            End If
    End Select
End Sub

Private Sub sdbgProveedoresPri_HeadClick(ByVal ColIndex As Integer)
    Select Case sdbgProveedoresPri.Columns(ColIndex).Name
    Case "COD"
        CargarProvesRelacionadosPri 1
    Case "DEN"
        CargarProvesRelacionadosPri 2
    Case "TRE"
        CargarProvesRelacionadosPri 3
    End Select
End Sub

Private Sub sdbgProveedoresPri_RowLoaded(ByVal Bookmark As Variant)
    sdbgProveedoresPri.Columns("INF").CellStyleSet "Boton"
    sdbgProveedoresPri.Columns("INF").AllowSizing = False
    sdbgProveedoresPri.Columns("CAR").CellStyleSet "Flecha"
    sdbgProveedoresPri.Columns("CAR").AllowSizing = False
End Sub

Private Sub sdbgProveRelac_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgProveRelac_AfterInsert(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If m_bAnyaError = False Then
        cmdA�adirProveRelac.Enabled = True
        cmdEliminarProveRelac.Enabled = True
        cmdDeshacerProveRelac.Enabled = True
    End If
End Sub

Private Sub sdbgProveRelac_AfterUpdate(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If m_bAnyaError = False And m_bModError = False Then
        cmdA�adirProveRelac.Enabled = True
        cmdEliminarProveRelac.Enabled = True
        cmdDeshacerProveRelac.Enabled = False
    End If
End Sub

Private Sub sdbgProveRelac_BeforeUpdate(Cancel As Integer)
    ''' * Objetivo: Validar los datos
    'variable para una coleccion de proveedores
    Dim oProves As CProveedores
    Dim oProve As CProveedor
    Dim oProveRel As CProveedor
    Dim scodProve As String

    Dim lIndice As Long
    
    m_bValError = False
    Cancel = False

    If sdbgProveRelac.IsAddRow Then
        scodProve = Trim(sdbgProveRelac.Columns("COD").Value)
        If scodProve = "" Then
            oMensajes.NoValido m_sIdioma(1)
            Cancel = True
            m_bBeforeRowColChangeProveRelac = True
            sdbgProveRelac.col = 0
            GoTo Salir
        End If

        Set oProves = oFSGSRaiz.generar_CProveedores

        oProves.CargarTodosLosProveedoresDesde3 1, scodProve, , True

        If oProves.Count = 0 Then
            oMensajes.NoValido m_sIdioma(1)
            Cancel = True
            m_bBeforeRowColChangeProveRelac = True
            sdbgProveRelac.col = 0
            GoTo Salir
        End If

        Set oProve = oProves.Item(1)
        'Cargamos los datos del proveedor
        oProves.CargarDatosProveedor scodProve

        If oProve.Cod = g_oProveSeleccionado.Cod Then
            oMensajes.NoValido m_sIdioma(1)
            Cancel = True
            m_bBeforeRowColChangeProveRelac = True
            sdbgProveRelac.col = 0
            GoTo Salir
        End If

        If m_bRowColChangeProveRelac = False Then
            For Each oProveRel In g_oProveSeleccionado.ProveRel
                If oProve.Cod = oProveRel.Cod Then
                    oMensajes.NoValido m_sIdioma(1)
                    Cancel = True
                    m_bBeforeRowColChangeProveRelac = True
                    sdbgProveRelac.col = 0
                    GoTo Salir
                End If
            Next
        
            Screen.MousePointer = vbHourglass
            lIndice = g_oProveSeleccionado.ProveRel.Count
            sdbgProveRelac.Columns.Item(1).Value = oProve.Den
            sdbgProveRelac.Columns.Item(2).Value = NullToStr(oProve.CodPais) & " " & NullToStr(oProve.DenPais)
            sdbgProveRelac.Columns.Item(3).Value = NullToStr(oProve.TipoRelacionDen)
            sdbgProveRelac.Columns.Item(4).Value = ""
            sdbgProveRelac.Columns.Item(5).Value = NullToStr(oProve.TipoRelacion)
            sdbgProveRelac.Columns.Item(6).Value = NullToStr(oProve.CodPortal)
            
            g_oProveSeleccionado.ProveRel.Add sCod:=oProve.Cod, sDen:=oProve.Den, vCodPais:=oProve.CodPais, vDenPais:=oProve.DenPais, vTipoRel:=NullToStr(oProve.TipoRelacion), vTipoRelDen:=NullToStr(oProve.TipoRelacionDen), vCodPortal:=oProve.CodPortal
            Screen.MousePointer = vbNormal
        Else
            Screen.MousePointer = vbHourglass
        
            g_oProveSeleccionado.ProveRel.Add sCod:=oProve.Cod, sDen:=oProve.Den, vCodPais:=oProve.CodPais, vDenPais:=oProve.DenPais _
            , vTipoRel:=sdbgProveRelac.Columns.Item(5).Value, vTipoRelDen:=sdbgProveRelac.Columns.Item(3).Value, vCodPortal:=oProve.CodPortal
        
            m_bRowColChangeProveRelac = False
            
            Screen.MousePointer = vbNormal
        End If
    End If
Salir:

    m_bValError = Cancel
    If Me.Visible Then sdbgProveRelac.SetFocus
End Sub

Private Sub sdbgProveRelac_BtnClick()
    Dim oProves As CProveedores
    Dim oProve As CProveedor
    Dim oCon As CContacto
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, sdbgProveRelac.Columns("COD").Value, , True
    
    If oProves.Count = 0 Then
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor sdbgProveRelac.Columns("COD").Value
    
    With frmProveDetalle
    Set .g_oProveSeleccionado = oProve
    
    .caption = oProve.Cod & "  " & oProve.Den
    .lblCodPortProve = NullToStr(oProve.CodPortal)
    .g_bPremium = oProve.EsPremium
    .g_bActivo = oProve.EsPremiumActivo
    .lblDir = NullToStr(oProve.Direccion)
    .lblCp = NullToStr(oProve.cP)
    .lblPob = NullToStr(oProve.Poblacion)
    .lblPaiCod = NullToStr(oProve.CodPais)
    .lblPaiDen = NullToStr(oProve.DenPais)
    .lblMonCod = NullToStr(oProve.CodMon)
    .lblMonDen = NullToStr(oProve.DenMon)
    .lblProviCod = NullToStr(oProve.CodProvi)
    .lblProviDen = NullToStr(oProve.DenProvi)
    .lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    .lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    .lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If Trim(oProve.Calif1) <> "" Then
        .lblCal1Den = oProve.Calif1
    End If
    
    If Trim(oProve.Calif2) <> "" Then
        .lblcal2den = oProve.Calif2
    End If
    
    If Trim(oProve.Calif3) <> "" Then
        .lblcal3den = oProve.Calif3
    End If
    
    .lblCal1Val = DblToStr(oProve.Val1)
    .lblCal2Val = DblToStr(oProve.Val2)
    .lblCal3Val = DblToStr(oProve.Val3)

    .lblURL = NullToStr(oProve.URLPROVE)

    oProve.CargarTodosLosContactos , , , , True, , , , , , , True
    
    For Each oCon In oProve.Contactos
        
        .sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
        & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
    
    Next
        
    .Show vbModal
    End With
End Sub

Private Sub sdbgProveRelac_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
   
    If cmdDeshacerProveRelac.Enabled = False Then
        cmdDeshacerProveRelac.Enabled = True
        cmdA�adirProveRelac.Enabled = False
        cmdEliminarProveRelac.Enabled = False
    End If
    
    If m_AccionProveRelac = ACCProveRelacCon And Not sdbgProveRelac.IsAddRow Then
        Set m_oProveRelacEnEdicion = Nothing
        Set m_oProveRelacEnEdicion = g_oProveSeleccionado.ProveRel.Item(CStr(sdbgProveRelac.Columns("COD").Value))
        m_AccionProveRelac = ACCProveRelacMod
    End If
    m_bValError = False
End Sub

Private Sub sdbgProveRelac_HeadClick(ByVal ColIndex As Integer)

If m_bModoEdicion Then Exit Sub

Select Case sdbgProveRelac.Columns(ColIndex).Name
Case "COD"
    CargarProvesRelacionados 1
Case "DEN"
    CargarProvesRelacionados 2
Case "PAI"
    CargarProvesRelacionados 3
Case "TRE"
    CargarProvesRelacionados 4
End Select
End Sub

Private Sub sdbgProveRelac_KeyPress(KeyAscii As Integer)
    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes

    If KeyAscii = vbKeyEscape Then

        If sdbgProveRelac.DataChanged = False Then

            sdbgProveRelac.CancelUpdate
            sdbgProveRelac.DataChanged = False

            If Not m_oProveRelacEnEdicion Is Nothing Then
                Set m_oProveRelacEnEdicion = Nothing
            End If

            cmdA�adirProveRelac.Enabled = True
            cmdEliminarProveRelac.Enabled = True
            cmdDeshacerProveRelac.Enabled = False

            m_AccionProveRelac = ACCProveRelacCon

        Else
            If sdbgProveRelac.IsAddRow Then
                cmdA�adirProveRelac.Enabled = True
                cmdEliminarProveRelac.Enabled = True
                cmdDeshacerProveRelac.Enabled = False
                m_AccionProveRelac = ACCProveRelacCon
            End If
        End If
    End If
End Sub

Private Sub sdbgProveRelac_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    Dim scodProve As String
    Dim oProves As CProveedores
    Dim oProveRel As CProveedor
    Dim oProve As CProveedor
    
    Dim lIndice As Long
        
    If m_bCambiandoModo Then Exit Sub
    If m_bBeforeRowColChangeProveRelac Then
        If sdbgProveRelac.col > 0 Then
            sdbgProveRelac.col = 0
        Else
            m_bBeforeRowColChangeProveRelac = False
        End If
        Exit Sub
    End If
        
    If Not sdbgProveRelac.IsAddRow Then
        sdbgProveRelac.Columns("COD").Locked = True
        If sdbgProveRelac.DataChanged = False Then
            cmdA�adirProveRelac.Enabled = True
            cmdEliminarProveRelac.Enabled = True
            cmdDeshacerProveRelac.Enabled = False
        End If
    Else
        If sdbgProveRelac.DataChanged = True Then cmdDeshacerProveRelac.Enabled = True
        sdbgProveRelac.Columns("COD").Locked = False
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgProveRelac.Row) Then
                sdbgProveRelac.col = 0
                cmdDeshacerProveRelac.Enabled = False
            End If
        End If
        cmdA�adirProveRelac.Enabled = True
        cmdEliminarProveRelac.Enabled = False
    End If
    
    If sdbgProveRelac.DataChanged = True Then
        cmdA�adirProveRelac.Enabled = False
        scodProve = Trim(sdbgProveRelac.Columns("COD").Value)
        If scodProve = "" Then
            oMensajes.NoValido m_sIdioma(1)
            m_bBeforeRowColChangeProveRelac = True
            sdbgProveRelac.col = 0
            Exit Sub
        End If

        Set oProves = oFSGSRaiz.generar_CProveedores
        oProves.CargarTodosLosProveedoresDesde3 1, scodProve, , True

        If oProves.Count = 0 Then
            oMensajes.NoValido m_sIdioma(1)
            m_bBeforeRowColChangeProveRelac = True
            sdbgProveRelac.col = 0
            Exit Sub
        End If

        Set oProve = oProves.Item(1)
        'Cargamos los datos del proveedor
        oProves.CargarDatosProveedor scodProve

        If oProve.Cod = g_oProveSeleccionado.Cod Then
            oMensajes.NoValido m_sIdioma(1)
            m_bBeforeRowColChangeProveRelac = True
            sdbgProveRelac.col = 0
            Exit Sub
        End If

        If sdbgProveRelac.IsAddRow Then
            For Each oProveRel In g_oProveSeleccionado.ProveRel
                If oProve.Cod = oProveRel.Cod Then
                    oMensajes.NoValido m_sIdioma(1)
                    m_bBeforeRowColChangeProveRelac = True
                    sdbgProveRelac.col = 0
                    Exit Sub
                End If
            Next
        End If

        Screen.MousePointer = vbHourglass
        lIndice = g_oProveSeleccionado.ProveRel.Count
        
        sdbgProveRelac.Columns.Item(1).Value = oProve.Den
        sdbgProveRelac.Columns.Item(2).Value = NullToStr(oProve.CodPais) & " " & NullToStr(oProve.DenPais)
        sdbgProveRelac.Columns.Item(3).Value = NullToStr(oProve.TipoRelacionDen)
        sdbgProveRelac.Columns.Item(4).Value = ""
        sdbgProveRelac.Columns.Item(5).Value = NullToStr(oProve.TipoRelacion)
        sdbgProveRelac.Columns.Item(6).Value = NullToStr(oProve.CodPortal)
        
        Screen.MousePointer = vbNormal
        
        m_bRowColChangeProveRelac = True
    End If
End Sub

Private Sub sdbgProveRelac_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
If sdbgProveRelac.Columns("FSP_COD").Value <> "" Then
    For i = 0 To sdbgProveRelac.Cols - 1
        sdbgProveRelac.Columns(i).CellStyleSet "Tan"
    Next
End If
sdbgProveRelac.Columns("INF").CellStyleSet "Boton"
End Sub

Private Sub stabGeneral_Click(PreviousTab As Integer)
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer

If sTabGeneral.Tab = 1 Then
        
    If cmdModificar.Enabled Then Exit Sub
    
    If m_bRMat Then
    
        If m_oGMN4Seleccionado Is Nothing And m_oGMN3Seleccionado Is Nothing And m_oGMN2Seleccionado Is Nothing And m_oGMN1Seleccionado Is Nothing Then
            sTabGeneral.Tab = 0
            If Me.Visible Then sdbcGMN1_4Cod.SetFocus
            Exit Sub
        End If
            
        If Not m_oGMN4Seleccionado Is Nothing Then
            If Me.Visible Then sdbcProveCod.SetFocus
            Exit Sub
        End If
            
        If Not m_oGMN3Seleccionado Is Nothing Then
            Screen.MousePointer = vbHourglass
            Set oICompAsignado = m_oGMN3Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            Screen.MousePointer = vbNormal
            If iNivelAsig = 0 Or iNivelAsig > 3 Then
                sTabGeneral.Tab = 0
                If Me.Visible Then sdbcGMN1_4Cod.SetFocus
                Exit Sub
            End If
            If Me.Visible Then sdbcProveCod.SetFocus
            Exit Sub
        End If
                
        If Not m_oGMN2Seleccionado Is Nothing Then
            Screen.MousePointer = vbHourglass
            Set oICompAsignado = m_oGMN2Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            Screen.MousePointer = vbNormal
            If iNivelAsig = 0 Or iNivelAsig > 2 Then
                sTabGeneral.Tab = 0
                If Me.Visible Then sdbcGMN1_4Cod.SetFocus
                Exit Sub
            End If
            If Me.Visible Then sdbcProveCod.SetFocus
            Exit Sub
        End If
            
        If Not m_oGMN1Seleccionado Is Nothing Then
            Screen.MousePointer = vbHourglass
            Set oICompAsignado = m_oGMN1Seleccionado
            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
            Screen.MousePointer = vbNormal
            If iNivelAsig < 1 Or iNivelAsig > 2 Then
                sTabGeneral.Tab = 0
                If Me.Visible Then sdbcGMN1_4Cod.SetFocus
                Exit Sub
            End If
            If Me.Visible Then sdbcProveCod.SetFocus
            Exit Sub
        End If
            
    End If
    fraProveRelacPri.Visible = False
    sdbgProveedoresPri.RemoveAll
    If Me.Visible Then sdbcProveCod.SetFocus
Else
            
        If m_Accion = ACCProveMod Or m_Accion = AccionesSummit.ACCProveAnya Then
            sTabGeneral.Tab = 1
            If gParametrosGenerales.giINSTWEB <> ConPortal Or IsNull(g_oProveSeleccionado.CodPortal) Then
                If Me.Visible Then txtDenProve.SetFocus
            End If
        Else
            sdbcProveCod.RemoveAll
            sdbcProveDen.RemoveAll
            BorrarTodo
            ModoConsulta
            sdbcProveCod = ""
            sdbcProveDen = ""
        End If
                
End If
End Sub

Private Sub stabPROVE_Click(PreviousTab As Integer)
    Select Case stabProve.Tab
    
    Case 0
        sdbcProveCod_DropDown
        
    Case 1
    
    Case 4
         
        picEsp.Left = lstvwEsp.Width - (cmdAbrirEsp.Width * 6) - 90
        If gParametrosGenerales.giINSTWEB = ConPortal Then
            picEspProvePortal.Left = lstvwEspProvePortal.Width - (cmdAbrirEspPortal.Width * 6) - 100
        End If
            
    End Select

End Sub

Private Sub FinEdicion()
    sTabGeneral.TabEnabled(0) = True
    fraGeneral.Enabled = False
    fraGeneralPortal.Enabled = False
    fraComunGeneral.Enabled = False
    fraImpuestos.Enabled = False
    fraAcepRecepPedidos.Enabled = False
    Frame4.Enabled = False
    txtCodAcceso.Enabled = True
    sdbcProveCod.Visible = True
    sdbcProveDen.Visible = True
    sdbcProveCod.Text = txtCodProve
    sdbcProveDen.Text = txtDenProve
    txtCodProve.Visible = False
    txtDenProve.Visible = False
    lblDenProve.Visible = False
    lblDescRango1.Visible = True
    lblDescRango2.Visible = True
    lblDescRango3.Visible = True
    
    If Not m_oCalif1Seleccionada Is Nothing Then
        lblDescRango1 = m_oCalif1Seleccionada.Cod & " " & m_oCalif1Seleccionada.Den
    Else
        lblDescRango1 = ""
    End If
    
    If Not m_oCalif2Seleccionada Is Nothing Then
        lblDescRango2 = m_oCalif2Seleccionada.Cod & " " & m_oCalif2Seleccionada.Den
    Else
        lblDescRango2 = ""
    End If
    
    If Not m_oCalif3Seleccionada Is Nothing Then
        lblDescRango3 = m_oCalif3Seleccionada.Cod & " " & m_oCalif3Seleccionada.Den
    Else
        lblDescRango3 = ""
    End If
    
    picCalif.Enabled = False
    txtObs.Locked = True
    
    cmdEliminarWeb.Visible = False
    
    picNavigate.Visible = True
    picEdit.Visible = False
    
    Set m_oCalif1Seleccionada = Nothing
    Set m_oCalif2Seleccionada = Nothing
    Set m_oCalif3Seleccionada = Nothing
    
    sdbcCal1Cod.Visible = False
    sdbcCal2Cod.Visible = False
    sdbcCal3Cod.Visible = False
        
    sdbcCal1Den.Visible = False
    sdbcCal2Den.Visible = False
    sdbcCal3Den.Visible = False
    
    ModoConsulta
    
End Sub

Private Sub BorrarTodo()

    lblCodPortal.Text = ""
    stabProve.Tab = 0
    stabProve.Enabled = False
    fraGeneral.Enabled = False
    fraGeneralPortal.Enabled = False
    fraComunGeneral.Enabled = False
    txtCodAcceso.Enabled = True
    Frame4.Enabled = False
    
    fraGeneral.Visible = True
    fraGeneralPortal.Visible = False
    fraComunGeneral.Visible = True
    sdbcFormato.Columns(1).Visible = True 'Antes sdbcFormato2 desde el form_load lo ocultaba
    
    sdbcProveCod.Visible = True
    sdbcProveDen.Visible = True
    
    sdbcMonCod = ""
    sdbcProviCod = ""
    sdbcMonDen = ""
    sdbcProviDen = ""
    sdbcPaiCod = ""
    sdbcPaiDen = ""
    txtCodProve.Visible = False
    txtDenProve.Visible = False
    sdbcCal1Cod.Visible = False
    sdbcCal2Cod.Visible = False
    sdbcCal3Cod.Visible = False
    sdbcCal1Den.Visible = False
    sdbcCal2Den.Visible = False
    sdbcCal3Den.Visible = False
    lblDescRango1.Visible = True
    lblDescRango2.Visible = True
    lblDescRango3.Visible = True
    lblDescRango1 = ""
    lblDescRango2 = ""
    lblDescRango3 = ""
    txtDIR = ""
    txtCP = ""
    txtPOB = ""
    txtNIF = ""
    txtURL.Text = ""
    txtPwd = ""
    txtPwdConf = ""
    txtCodAcceso = ""
    sdbcPagoCod = ""
    sdbcPagoDen = ""
    sdbcViaPagoCod = ""
    sdbcViaPagoDen = ""
    sdbcFormato = ""
    ChkCuentBloq.Value = vbUnchecked
    
    chkAutoFactura.Value = vbUnchecked
    Me.chkImpuestos.Value = vbUnchecked
    fraImpuestos.Enabled = False
    chkAcepRecepPedidos.Value = vbUnchecked
    fraAcepRecepPedidos.Enabled = False
    
    BorrarDatosPortal
    Set m_oCalif1Seleccionada = Nothing
    Set m_oCalif2Seleccionada = Nothing
    Set m_oCalif3Seleccionada = Nothing
    If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove) Then
        lblEstInt.Backcolor = RGB(212, 208, 200)
        lblEstInt.ToolTipText = m_sIdiSincronizado
    End If
    cmdModificar.Enabled = False
    cmdEliminar.Enabled = False
    cmdCodigo.Enabled = False
    cmdDatosPortal.Enabled = False
    Me.sdbgCodERP.RemoveAll
    Me.fraProveRelacPri.Visible = False
    Me.sdbgProveedoresPri.RemoveAll

    cmdEliminarWeb.Visible = False

End Sub
Private Sub BorrarDatosPortal()

    lblCodMon = ""
    lblCodProvi = ""
    lblDenMon = ""
    lblDenProvi = ""
    lblCodPai = ""
    lblDenPai = ""
    lblDenProve = ""
    lblDenProve.Visible = False
    lblDir = ""
    lblCp = ""
    lblPob = ""
    lblNIF = ""
    lblCodPortal.Text = ""
    chkPremium.Value = vbUnchecked

End Sub
Private Sub sdbcGMN1_4Cod_Change()

    If Not m_bGMN1RespetarCombo Then
    
        m_bGMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        sdbcEqpCod.Text = ""
        m_bGMN1RespetarCombo = False
        Set m_oGMN1Seleccionado = Nothing
        m_bGMN1CargarComboDesde = True
        
    End If

End Sub
Private Sub sdbcGMN1_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim m_oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Den.RemoveAll

    If m_bRMat Then
                
        Set m_oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN1 = Nothing
        If m_bGMN1CargarComboDesde Then
            Set m_oGruposMN1 = m_oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False)
        Else
            Set m_oGruposMN1 = m_oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        If m_bGMN1CargarComboDesde Then
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            m_oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    Codigos = m_oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If m_bGMN1CargarComboDesde And Not m_oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not m_bGMN2RespetarCombo Then
    
        m_bGMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcEqpCod.Text = ""
        m_bGMN2RespetarCombo = False
        Set m_oGMN2Seleccionado = Nothing
        
        m_bGMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    
    m_bGMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    m_bGMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    m_bGMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim m_oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Cod.RemoveAll

    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If m_bRMat Then
                
        Set m_oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN2 = Nothing
        If m_bGMN2CargarComboDesde Then
            Set m_oGruposMN2 = m_oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False)
        Else
            Set m_oGruposMN2 = m_oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False)
        End If
    Else
        
        If m_bGMN2CargarComboDesde Then
            m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            m_oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set m_oGruposMN2 = m_oGMN1Seleccionado.GruposMatNivel2
        
    End If
    
    Codigos = m_oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next

    If m_bGMN2CargarComboDesde And Not m_oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not m_bGMN2RespetarCombo Then

        m_bGMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcEqpCod.Text = ""
        m_bGMN2RespetarCombo = False
        Set m_oGMN2Seleccionado = Nothing
        m_bGMN2CargarComboDesde = True
        
    End If
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Den.Value = "" Then Exit Sub
    
    m_bGMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    m_bGMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    m_bGMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim m_oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Den.RemoveAll
    
    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If m_bRMat Then
                
        Set m_oIMAsig = oUsuarioSummit.comprador
         Set m_oGruposMN2 = Nothing
        If m_bGMN2CargarComboDesde Then
            Set m_oGruposMN2 = m_oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            Set m_oGruposMN2 = m_oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4Cod), , , , , False)
        End If
    
    Else
        
        If m_bGMN2CargarComboDesde Then
            m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            m_oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set m_oGruposMN2 = m_oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = m_oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If m_bGMN2CargarComboDesde And Not m_oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Cod_Change()
    
    If Not m_bGMN3RespetarCombo Then
    
        m_bGMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        sdbcEqpCod.Text = ""
        m_bGMN3RespetarCombo = False
        Set m_oGMN3Seleccionado = Nothing
        m_bGMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    m_bGMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    m_bGMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    m_bGMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim m_oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Cod.RemoveAll

    If m_oGMN2Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If m_bRMat Then
                
        Set m_oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN3 = Nothing
        If m_bGMN3CargarComboDesde Then
            Set m_oGruposMN3 = m_oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            Set m_oGruposMN3 = m_oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False)
        End If
    Else
        
         If m_bGMN3CargarComboDesde Then
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN3 = m_oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = m_oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If m_bGMN3CargarComboDesde And Not m_oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not m_bGMN1RespetarCombo Then

        m_bGMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        sdbcEqpCod.Text = ""
        m_bGMN1RespetarCombo = False
        Set m_oGMN1Seleccionado = Nothing
        m_bGMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Den.Value = "" Then Exit Sub
    
    m_bGMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    m_bGMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    m_bGMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
    If Not m_bGMN4RespetarCombo Then
    
        m_bGMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        sdbcEqpCod.Text = ""
        m_bGMN4RespetarCombo = False
        Set m_oGMN4Seleccionado = Nothing
        m_bGMN4CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Value = "" Then Exit Sub
    
    m_bGMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    m_bGMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    m_bGMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim m_oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Cod.RemoveAll

    If m_oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If m_bRMat Then
                
        Set m_oIMAsig = oUsuarioSummit.comprador
         Set m_oGruposMN4 = Nothing
        If m_bGMN4CargarComboDesde Then
            Set m_oGruposMN4 = m_oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set m_oGruposMN4 = m_oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False)
        End If
    Else
        
        If m_bGMN4CargarComboDesde Then
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN4 = m_oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = m_oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If m_bGMN4CargarComboDesde And Not m_oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEqpCod_Change()

    If Not m_bEqpRespetarCombo Then
    
        m_bEqpRespetarCombo = True
        sdbcEqpDen.Text = ""
        m_bEqpRespetarCombo = False
        Set m_oEqpSeleccionado = Nothing
        m_bEqpCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN4_4Den_Change()
    
    If Not m_bGMN4RespetarCombo Then

        m_bGMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        sdbcEqpCod.Text = ""
        m_bGMN4RespetarCombo = False
        Set m_oGMN4Seleccionado = Nothing
        m_bGMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Den.Value = "" Then Exit Sub
    
    m_bGMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    m_bGMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    m_bGMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim m_oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Den.RemoveAll
    
    If m_oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If m_bRMat Then
                
        Set m_oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN4 = Nothing
        If m_bGMN4CargarComboDesde Then
            Set m_oGruposMN4 = m_oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set m_oGruposMN4 = m_oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False)
        End If
    Else
        
        If m_bGMN4CargarComboDesde Then
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN4 = m_oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = m_oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If m_bGMN4CargarComboDesde And Not m_oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcEqpCod_CloseUp()

    If sdbcEqpCod.Value = "..." Then
        sdbcEqpCod.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpCod.Value = "" Then Exit Sub
    
    m_bEqpRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    m_bEqpRespetarCombo = False
    
    m_bEqpCargarComboDesde = False
    
End Sub

Private Sub sdbcEqpCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcEqpCod.RemoveAll

    If m_bEqpCargarComboDesde Then
        m_oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , , False
    Else
        m_oEqps.CargarTodosLosEquipos , , , , False
    End If
    
    Codigos = m_oEqps.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If m_bEqpCargarComboDesde And Not m_oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEqpCod_InitColumnProps()
    
    sdbcEqpCod.DataField = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcEqpDen_Change()

    If Not m_bEqpRespetarCombo Then

        m_bEqpRespetarCombo = True
        sdbcEqpCod.Text = ""
        m_bEqpRespetarCombo = False
        Set m_oEqpSeleccionado = Nothing
        m_bEqpCargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEqpDen_CloseUp()
   
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpDen.Value = "" Then Exit Sub
    
    m_bEqpRespetarCombo = True
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    m_bEqpRespetarCombo = False
    
    m_bEqpCargarComboDesde = False
    
End Sub

Private Sub sdbcEqpDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcEqpDen.RemoveAll

    If m_bEqpCargarComboDesde Then
        m_oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), , False
    Else
        m_oEqps.CargarTodosLosEquipos , , , , False
    End If
    
    Codigos = m_oEqps.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If m_bEqpCargarComboDesde And Not m_oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub GMN1Seleccionado()
    
    sdbcGMN2_4Cod.Text = ""
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcGMN2_4Den.Text = ""
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    Set m_oGMN1Seleccionado = Nothing
    Set m_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    Set m_oGMN2Seleccionado = Nothing
    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN4Seleccionado = Nothing
    
    m_oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    stabProve.Enabled = True
    
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN4Seleccionado = Nothing
    
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    Set m_oGMN2Seleccionado = Nothing
    Set m_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    m_oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
    m_oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    
    stabProve.Enabled = True
    
End Sub
Private Sub GMN3Seleccionado()

    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    Set m_oGMN4Seleccionado = Nothing
    
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    m_oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    m_oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    m_oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    
    stabProve.Enabled = True
    
End Sub
Private Sub GMN4Seleccionado()
    
    If sdbcGMN4_4Cod.Value = "" Then Exit Sub
    
    Set m_oGMN4Seleccionado = Nothing
    Set m_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    m_oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    m_oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    m_oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    m_oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
    stabProve.Enabled = True
    
End Sub
Private Sub ConfigurarNombres()

    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"
    
    fraCalif1.caption = m_sIdioma(10) & " " & gParametrosGenerales.gsDEN_CAL1
    fraCalif2.caption = m_sIdioma(10) & " " & gParametrosGenerales.gsDEN_CAL2
    fraCalif3.caption = m_sIdioma(10) & " " & gParametrosGenerales.gsDEN_CAL3

    If gParametrosGenerales.gbCampo1ERPAct Then
        Me.sdbgCodERP.Groups("CAMPO1").caption = NullToStr(gParametrosGenerales.gsCampo1ERP)
        Me.sdbgCodERP.Columns("VALOR1").caption = m_sIdiValor
    Else
        Me.sdbgCodERP.Groups("CAMPO1").Visible = False
    End If
    If gParametrosGenerales.gbCampo2ERPAct Then
        Me.sdbgCodERP.Groups("CAMPO2").caption = NullToStr(gParametrosGenerales.gsCampo2ERP)
        Me.sdbgCodERP.Columns("VALOR2").caption = m_sIdiValor
    Else
        Me.sdbgCodERP.Groups("CAMPO2").Visible = False
    End If
    If gParametrosGenerales.gbCampo3ERPAct Then
        Me.sdbgCodERP.Groups("CAMPO3").caption = NullToStr(gParametrosGenerales.gsCampo3ERP)
        Me.sdbgCodERP.Columns("VALOR3").caption = m_sIdiValor
    Else
        Me.sdbgCodERP.Groups("CAMPO3").Visible = False
    End If
    If gParametrosGenerales.gbCampo4ERPAct Then
        Me.sdbgCodERP.Groups("CAMPO4").caption = NullToStr(gParametrosGenerales.gsCampo4ERP)
        Me.sdbgCodERP.Columns("VALOR4").caption = m_sIdiValor
    Else
        Me.sdbgCodERP.Groups("CAMPO4").Visible = False
    End If
    
End Sub
Private Sub EquipoSeleccionado()
    
    Set m_oEqpSeleccionado = Nothing
    Set m_oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
    m_oEqpSeleccionado.Cod = Trim(sdbcEqpCod)
    
End Sub

Public Sub CargarProveedorConBusqueda()
    Set m_oProves = Nothing
    Set m_oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbcProveCod = m_oProves.Item(1).Cod
    sdbcProveCod_Validate False
    
    Screen.MousePointer = vbHourglass
    ProveedorSeleccionado
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal1Cod_CloseUp()

    If sdbcCal1Cod.Value = "..." Then
        sdbcCal1Cod.Text = ""
        Exit Sub
    End If
    
    m_bCal1RespetarCombo = True
    sdbcCal1Den.Text = sdbcCal1Cod.Columns(1).Text
    sdbcCal1Cod.Text = sdbcCal1Cod.Columns(0).Text
    m_bCal1RespetarCombo = False
    
    If m_oCalif1Seleccionada Is Nothing Then
        Calif1Seleccionada
    Else
        'Carga la calificaci�n si se selecciona una diferente a la actual
        If UCase(sdbcCal1Cod.Columns(0).Value) <> UCase(m_oCalif1Seleccionada.Cod) Then
            Calif1Seleccionada
        End If
    End If
    
    m_bCal1CargarComboDesde = False
    
End Sub

Private Sub sdbcCal1Cod_DropDown()

    Dim oCal As CCalificacion
    Dim oCalif1 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalif1 = oFSGSRaiz.generar_CCalificaciones
    
    sdbcCal1Cod.RemoveAll

    If m_bCal1CargarComboDesde Then
        oCalif1.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 1, Trim(sdbcCal1Cod.Text), , , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalif1.CargarTodasLasCalificaciones 1, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalif1
        
        sdbcCal1Cod.AddItem oCal.Cod & Chr(m_lSeparador) & oCal.Den & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
        
    Next

    If m_bCal1CargarComboDesde And Not oCalif1.EOF Then
        sdbcCal1Cod.AddItem "..."
    End If

    Set oCalif1 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal1Cod_InitColumnProps()
    sdbcCal1Cod.DataFieldList = "Column 0"
    sdbcCal1Cod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcCal1Den_CloseUp()

    If sdbcCal1Den.Value = "..." Then
        sdbcCal1Den.Text = ""
        Exit Sub
    End If
    
    m_bCal1RespetarCombo = True
    sdbcCal1Cod.Text = sdbcCal1Den.Columns(1).Text
    sdbcCal1Den.Text = sdbcCal1Den.Columns(0).Text
    m_bCal1RespetarCombo = False
    
    If m_oCalif1Seleccionada Is Nothing Then
        Calif1Seleccionada
    Else
        'Carga la calificaci�n si se selecciona una diferente a la actual
        If UCase(sdbcCal1Den.Columns(1).Value) <> UCase(m_oCalif1Seleccionada.Cod) Then
            Calif1Seleccionada
        End If
    End If
    
    m_bCal1CargarComboDesde = False
        
End Sub

Private Sub sdbcCal1Den_DropDown()
Dim oCal As CCalificacion
Dim oCalif1 As CCalificaciones

    Screen.MousePointer = vbHourglass
    
    Set oCalif1 = oFSGSRaiz.generar_CCalificaciones
    
    sdbcCal1Den.RemoveAll

    If m_bCal1CargarComboDesde Then
        oCalif1.CargarTodasLasCalificacionesDesde gParametrosInstalacion.giCargaMaximaCombos, 1, , Trim(sdbcCal1Den.Text), , , , False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oCalif1.CargarTodasLasCalificaciones 1, , , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If

    For Each oCal In oCalif1
        
        sdbcCal1Den.AddItem oCal.Den & Chr(m_lSeparador) & oCal.Cod & Chr(m_lSeparador) & oCal.ValorInf & Chr(m_lSeparador) & oCal.ValorSup
        
    Next

    If m_bCal1CargarComboDesde And Not oCalif1.EOF Then
        sdbcCal1Den.AddItem "..."
    End If

    Set oCalif1 = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCal1Den_InitColumnProps()
    sdbcCal1Den.DataFieldList = "Column 0"
    sdbcCal1Den.DataFieldToDisplay = "Column 0"
End Sub
Private Sub Calif1Seleccionada()
    
    If sdbcCal1Cod.Text = "" Then
        Set m_oCalif1Seleccionada = Nothing
        Exit Sub
    End If
    
    Set m_oCalif1Seleccionada = oFSGSRaiz.generar_CCalificacion
    
    m_oCalif1Seleccionada.Cod = sdbcCal1Cod.Text
    m_oCalif1Seleccionada.Den = sdbcCal1Den.Text
        
    On Error Resume Next
    
    
    If UCase(sdbcCal1Cod.Columns(0).Value) <> UCase(sdbcCal1Cod.Text) Then
        m_oCalif1Seleccionada.ValorInf = sdbcCal1Den.Columns(2).Value
        m_oCalif1Seleccionada.ValorSup = sdbcCal1Den.Columns(3).Value
    Else
        m_oCalif1Seleccionada.ValorInf = sdbcCal1Cod.Columns(2).Value
        m_oCalif1Seleccionada.ValorSup = sdbcCal1Cod.Columns(3).Value
    End If
    
    If m_blnCargandoCalif = True Then Exit Sub
    
    'Carga el valor por defecto para la calificaci�n seg�n el orden sea
    'ascendente o descendente
    
    m_bCalif = True
    If gParametrosGenerales.gbSENT_ORD_CAL1 = True Then
        'Descendente
        txtVal1.Text = m_oCalif1Seleccionada.ValorSup
    Else
        'Ascendente
        
        txtVal1.Text = m_oCalif1Seleccionada.ValorInf
        
    End If
    m_bCalif = False
    
    On Error GoTo 0

End Sub

Private Sub Calif2Seleccionada()
    
    If sdbcCal2Cod.Text = "" Then
        Set m_oCalif2Seleccionada = Nothing
        Exit Sub
    End If
    
    Set m_oCalif2Seleccionada = oFSGSRaiz.generar_CCalificacion
    
    m_oCalif2Seleccionada.Cod = sdbcCal2Cod.Text
    m_oCalif2Seleccionada.Den = sdbcCal2Den.Text
    
    On Error Resume Next
    
    If UCase(sdbcCal2Cod.Columns(0).Value) <> UCase(sdbcCal2Cod.Text) Then
        m_oCalif2Seleccionada.ValorInf = sdbcCal2Den.Columns(2).Value
        m_oCalif2Seleccionada.ValorSup = sdbcCal2Den.Columns(3).Value
    Else
        m_oCalif2Seleccionada.ValorInf = sdbcCal2Cod.Columns(2).Value
        m_oCalif2Seleccionada.ValorSup = sdbcCal2Cod.Columns(3).Value
    
    End If
    
    If m_blnCargandoCalif = True Then Exit Sub
    
    'Carga el valor por defecto para la calificaci�n seleccionada
    'seg�n el orden sea ascendente o descendente
    
    m_bCalif = True
    If gParametrosGenerales.gbSENT_ORD_CAL2 = True Then
        'Descendente
        txtVal2.Text = m_oCalif2Seleccionada.ValorSup
    Else
        'Ascendente
        txtVal2.Text = m_oCalif2Seleccionada.ValorInf
    End If
    m_bCalif = False
    On Error GoTo 0

End Sub
Private Sub Calif3Seleccionada()
    
    If sdbcCal3Cod.Text = "" Then
        Set m_oCalif3Seleccionada = Nothing
        Exit Sub
    End If
    
    Set m_oCalif3Seleccionada = oFSGSRaiz.generar_CCalificacion
    
    m_oCalif3Seleccionada.Cod = sdbcCal3Cod.Text
    m_oCalif3Seleccionada.Den = sdbcCal3Den.Text
    
    On Error Resume Next
    
    If UCase(sdbcCal3Cod.Columns(0).Value) <> UCase(sdbcCal3Cod.Text) Then
        m_oCalif3Seleccionada.ValorInf = sdbcCal3Den.Columns(2).Value
        m_oCalif3Seleccionada.ValorSup = sdbcCal3Den.Columns(3).Value
    Else
        m_oCalif3Seleccionada.ValorInf = sdbcCal3Cod.Columns(2).Value
        m_oCalif3Seleccionada.ValorSup = sdbcCal3Cod.Columns(3).Value
    End If
    
    If m_blnCargandoCalif = True Then Exit Sub
    
    'Carga el valor por defecto para la calificaci�n seg�n el orden sea
    'ascendente o descendente
    m_bCalif = True
    If gParametrosGenerales.gbSENT_ORD_CAL3 = True Then
        'Descendente
        txtVal3.Text = m_oCalif3Seleccionada.ValorSup
    Else
        'Ascendente
        txtVal3.Text = m_oCalif3Seleccionada.ValorInf
    End If
    m_bCalif = False
    
    On Error GoTo 0
    
End Sub

Public Sub PonerMatSeleccionadoEnCombos()
    
    Set m_oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not m_oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = m_oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    End If
    
    Set m_oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not m_oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = m_oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
        
    End If
    
    Set m_oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not m_oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = m_oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    End If
    
    Set m_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
        
    If Not m_oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4Cod.Text = m_oGMN4Seleccionado.Cod
        sdbcGMN4_4Cod_Validate False
    End If
        
End Sub

Private Sub sdbgContactos_AfterDelete(RtnDispErrMsg As Integer)

    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    If Me.Visible Then sdbgContactos.SetFocus
    sdbgContactos.Bookmark = sdbgContactos.RowBookmark(sdbgContactos.Row)
    
End Sub
Private Sub sdbgContactos_AfterInsert(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If m_bAnyaError = False Then
        cmdA�adirContacto.Enabled = True
        cmdEliminarContacto.Enabled = True
        cmdDeshacerContacto.Enabled = True
    End If
    
End Sub
Private Sub sdbgContactos_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    If m_bAnyaError = False And m_bModError = False Then
        cmdA�adirContacto.Enabled = True
        cmdEliminarContacto.Enabled = True
        cmdDeshacerContacto.Enabled = False
    End If
        
End Sub
''' <summary>
''' Objetivo: Confirmacion antes de eliminar
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgContactos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

    ''' * Objetivo: Confirmacion antes de eliminar
    
    Dim irespuesta As Integer
    
    DispPromptMsg = 0
    'Con Portal, si es contacto del portal no se puede eliminar
    If gParametrosGenerales.giINSTWEB = ConPortal And Not IsNull(g_oProveSeleccionado.CodPortal) And sdbgContactos.Columns("PORTAL").Value = "1" Then
        oMensajes.ContactoDelPortal
        Cancel = True
        Exit Sub
    End If
    
    irespuesta = oMensajes.PreguntaEliminar(m_sIdioma(11) & ": " & sdbgContactos.Columns(1).Value & " (" & sdbgContactos.Columns(2).Value & ")")
    
    If irespuesta = vbNo Then Cancel = True
    
End Sub

''' <summary>
''' Objetivo: Validar los datos
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgContactos_BeforeUpdate(Cancel As Integer)

    ''' * Objetivo: Validar los datos
    m_bValError = False
    Cancel = False
    
    If Trim(sdbgContactos.Columns(1).Value) = "" Then
        oMensajes.NoValido m_sIdioma(12)
        Cancel = True
        GoTo Salir
    End If

    If GridCheckToBoolean(sdbgContactos.Columns(12).Value) Or GridCheckToBoolean(sdbgContactos.Columns(14).Value) Or GridCheckToBoolean(sdbgContactos.Columns("CALIDAD").Value) Then
        If sdbgContactos.Columns("MAIL").Value = "" Then
            oMensajes.FaltaEMail
            Cancel = True
            GoTo Salir
        End If
    End If
    
Salir:
        
    m_bValError = Cancel
    If Cancel = True Then
        If stabProve.Tab <> 1 Then stabProve.Tab = 1
    End If
    If Me.Visible Then sdbgContactos.SetFocus
        
End Sub
Private Sub sdbgContactos_Change()
    Dim oContacto As CContacto

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    If cmdDeshacerContacto.Enabled = False Then
    
        cmdA�adirContacto.Enabled = False
        cmdEliminarContacto.Enabled = False
        cmdDeshacerContacto.Enabled = True
     
    End If
    
    If m_AccionCont = ACCContCon And Not sdbgContactos.IsAddRow Then
    
        Set m_oContactoEnEdicion = Nothing
        If IsNull(sdbgContactos.Bookmark) Then
            If sdbgContactos.Rows = 1 Then
                sdbgContactos.Bookmark = 0
            End If
        End If
        
        Set m_oContactoEnEdicion = g_oProveSeleccionado.Contactos.Item(CStr(sdbgContactos.Bookmark))
        
        m_AccionCont = ACCContMod
      
    End If
        

    If GridCheckToBoolean(sdbgContactos.Columns("PORTAL").Value) = True Then
        If sdbgContactos.col = 13 Then
            For Each oContacto In g_oProveSeleccionado.Contactos
                oContacto.NotifSubasta = False
            Next
            
            m_oContactoEnEdicion.NotifSubasta = True
            
            sdbgContactos.Refresh
        End If
        If sdbgContactos.col = 11 Then
            For Each oContacto In g_oProveSeleccionado.Contactos
                oContacto.Aprovisionador = False
            Next
            
            m_oContactoEnEdicion.Aprovisionador = True
            
            sdbgContactos.Refresh
        End If
        
        If sdbgContactos.col = 14 Then
            For Each oContacto In g_oProveSeleccionado.Contactos
                oContacto.Calidad = False
            Next
            
            m_oContactoEnEdicion.Calidad = True
            
            sdbgContactos.Refresh
        End If
        
    Else
        If sdbgContactos.col = 13 And g_oProveSeleccionado.Contactos.Count > 0 Then
            For Each oContacto In g_oProveSeleccionado.Contactos
                oContacto.NotifSubasta = False
            Next
            sdbgContactos.Refresh
        End If
        If sdbgContactos.col = 11 And g_oProveSeleccionado.Contactos.Count > 0 Then
            For Each oContacto In g_oProveSeleccionado.Contactos
                oContacto.Aprovisionador = False
            Next
            sdbgContactos.Refresh
        End If
        If sdbgContactos.col = 14 And g_oProveSeleccionado.Contactos.Count > 0 Then
            For Each oContacto In g_oProveSeleccionado.Contactos
                oContacto.Calidad = False
            Next
            sdbgContactos.Refresh
        End If
    End If
    
    m_bValError = False
    
End Sub
Private Sub sdbgContactos_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    
    Dim sHeadCaption As String
    
    If m_bModoEdicion Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgContactos.Columns(ColIndex).caption
    sdbgContactos.Columns(ColIndex).caption = m_sIdioma(13) & "...."
    
    ''' Volvemos a cargar las Contactos, ordenadas segun la columna
    ''' en cuyo encabezado se ha hecho 'clic'.
    
    ''' Datos no filtrados
        
    Select Case ColIndex
    Case 0 ' Estado integracion
        g_oProveSeleccionado.CargarTodosLosContactos , , , , , , , , , , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove), True
    Case 1 ' Apellidos
        g_oProveSeleccionado.CargarTodosLosContactos , , , , True, , , , , , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    Case 2 ' Nombre
        g_oProveSeleccionado.CargarTodosLosContactos , , , , True, , , , , , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    Case 3  ' Depart
        g_oProveSeleccionado.CargarTodosLosContactos , , , , , True, , , , , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    Case 4  ' Cargo
        g_oProveSeleccionado.CargarTodosLosContactos , , , , , , True, , , , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    Case 5  ' Tfno
        g_oProveSeleccionado.CargarTodosLosContactos , , , , , , , True, , , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    Case 6  ' Tfno2
        g_oProveSeleccionado.CargarTodosLosContactos , , , , , , , , True, , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    Case 8  ' Fax
        g_oProveSeleccionado.CargarTodosLosContactos , , , , , , , , , True, , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    Case 7  ' Tfnomovil
        g_oProveSeleccionado.CargarTodosLosContactos , , , , , , , , , , True, , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    Case 9  ' Mail
        g_oProveSeleccionado.CargarTodosLosContactos , , , , , , , , , , , , True, , True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    Case 16  ' Principal
        g_oProveSeleccionado.CargarTodosLosContactos , , , , , , , , , , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove), , , , True
    End Select


    sdbgContactos.ReBind
    sdbgContactos.Columns(ColIndex).caption = sHeadCaption

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgContactos_InitColumnProps()
    
    sdbgContactos.Columns(1).FieldLen = 100
    
End Sub

Private Sub sdbgContactos_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgContactos.DataChanged = False Then
                        
            sdbgContactos.CancelUpdate
            sdbgContactos.DataChanged = False
            
            If Not m_oContactoEnEdicion Is Nothing Then
                Set m_oContactoEnEdicion = Nothing
            End If
           
            cmdA�adirContacto.Enabled = True
            cmdEliminarContacto.Enabled = True
            cmdDeshacerContacto.Enabled = False
            
            m_AccionCont = ACCContCon
            
        Else
        
            If sdbgContactos.IsAddRow Then
           
                cmdA�adirContacto.Enabled = True
                cmdEliminarContacto.Enabled = True
                cmdDeshacerContacto.Enabled = False
                
                m_AccionCont = ACCContCon
            
            End If
            
        End If
        
    End If
    
End Sub

Private Sub sdbgContactos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim i As Integer
    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion

    If m_bCambiandoModo Then Exit Sub
    
    If Not sdbgContactos.IsAddRow Then
        If sdbgContactos.AllowUpdate = True Then
            If gParametrosGenerales.giINSTWEB = ConPortal And Not IsNull(g_oProveSeleccionado.CodPortal) And Not IsEmpty(g_oProveSeleccionado.CodPortal) Then
            
                If Not IsNull(LastRow) Then
                    If val(LastRow) <> val(sdbgContactos.Row) Or sdbgContactos.Row = 0 Then
                        If GridCheckToBoolean(sdbgContactos.Columns("PORTAL").Value) = True Then
                            For i = 1 To 10
                                sdbgContactos.Columns(i).Locked = True
                            Next
                        Else
                            For i = 1 To 10
                                sdbgContactos.Columns(i).Locked = False
                            Next
                        End If
                    End If
                End If
            End If
        End If
    
        If sdbgContactos.DataChanged = False Then
            cmdA�adirContacto.Enabled = True
            cmdEliminarContacto.Enabled = True
            cmdDeshacerContacto.Enabled = False
        End If
    Else
        'For I = 0 To 8
        For i = 1 To 10
            sdbgContactos.Columns(i).Locked = False
        Next
        If sdbgContactos.DataChanged = True Then cmdDeshacerContacto.Enabled = True
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgContactos.Row) Then
                'sdbgContactos.col = 0
                sdbgContactos.col = 1
                cmdDeshacerContacto.Enabled = False
            End If
        End If
        
        If sdbgContactos.Rows > 0 Then
            cmdA�adirContacto.Enabled = False
            cmdEliminarContacto.Enabled = False
        End If
    End If
        
End Sub
Private Sub sdbgContactos_UnboundAddData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, NewRowBookmark As Variant)

    ''' * Objetivo: Hemos anyadido una fila al grid,
    ''' * Objetivo: ahora hay que anyadirla a la
    ''' * Objetivo: coleccion
    ''' * Recibe: Buffer con los datos a anyadir y bookmark de la fila
        
    Dim i As Long
    Dim oCon As CContacto
    
    m_bAnyaError = False
    
    ''Busco el ID mas grande para poner el ID
    i = 0
    For Each oCon In g_oProveSeleccionado.Contactos
        If i < oCon.Id Then i = oCon.Id
    Next
    i = i + 1
    ''' Anyadir a la coleccion
    g_oProveSeleccionado.Contactos.Add i, sdbgContactos.Columns("DEN").Value, sdbgContactos.Columns("APE").Value, sdbgContactos.Columns("NIF").Value, sdbgContactos.Columns("DEP").Value, sdbgContactos.Columns("CAR").Value, sdbgContactos.Columns("TFNO").Value, sdbgContactos.Columns("FAX").Value, sdbgContactos.Columns("MAIL").Value, val(sdbgContactos.Columns("DEF").Value), g_oProveSeleccionado.Contactos.Count, sdbgContactos.Columns("TFNO2").Value, , sdbgContactos.Columns("TFNO_MOVIL").Value, val(sdbgContactos.Columns("APROV").Value), val(sdbgContactos.Columns("SUB").Value), , , , val(sdbgContactos.Columns("CALIDAD").Value), val(sdbgContactos.Columns("PRINCIPAL").Value)
        
    m_AccionCont = ACCContCon
    
        
End Sub
Private Sub sdbgContactos_UnboundDeleteRow(Bookmark As Variant)

    ''' * Objetivo: Hemos eliminado una fila al grid,
    ''' * Objetivo: ahora hay que eliminarla de la
    ''' * Objetivo: coleccion
    ''' * Recibe: Bookmark de la fila
    
    Dim IndCONT As Long
    Dim numCont As Integer
    
    numCont = g_oProveSeleccionado.Contactos.Count - 2
    ''' Eliminamos de la base de datos
    
    Set m_oContactoEnEdicion = g_oProveSeleccionado.Contactos.Item(CStr(Bookmark))
            
    ''' Eliminar de la coleccion
        
    IndCONT = val(Bookmark)
        
    g_oProveSeleccionado.Contactos.BorrarEnModoIndice IndCONT
       
    m_AccionCont = ACCContCon
        
    Set m_oContactoEnEdicion = Nothing
    
End Sub
Private Sub sdbgContactos_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Integer
    Dim i As Integer
    Dim j As Integer
    
    Dim oCont As CContacto
    
    Dim iNumContactos As Integer
    
    If g_oProveSeleccionado Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If

    If g_oProveSeleccionado.Contactos Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If

    iNumContactos = g_oProveSeleccionado.Contactos.Count
    
    If IsNull(StartLocation) Then       'If the grid is empty then
        If ReadPriorRows Then               'If moving backwards through grid then
            m_lIndiceContactos = iNumContactos - 1                             'pointer = # of last grid row
        Else                                        'else
            m_lIndiceContactos = 0                                       'pointer = # of first grid row
        End If
    Else                                        'If the grid already has data in it then
        m_lIndiceContactos = StartLocation                       'pointer = location just before or after the row where data will be added
    
        If ReadPriorRows Then               'If moving backwards through grid then
                m_lIndiceContactos = m_lIndiceContactos - 1                               'move pointer back one row
        Else                                        'else
                m_lIndiceContactos = m_lIndiceContactos + 1                               'move pointer ahead one row
        End If
    End If
    
    'The pointer (m_lIndiceContactos) now points to the row of the grid where you will start adding data.
    
    For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
        
        If m_lIndiceContactos < 0 Or m_lIndiceContactos > iNumContactos - 1 Then Exit For           'If the pointer is outside the grid then stop this
    
        Set oCont = Nothing
        Set oCont = g_oProveSeleccionado.Contactos.Item(CStr(m_lIndiceContactos))
    
        For j = 0 To 16
      
            Select Case j
                    
                    Case 1:
                            RowBuf.Value(i, 1) = oCont.Apellidos
                    Case 2:
                            RowBuf.Value(i, 2) = oCont.nombre
                    Case 3:
                            RowBuf.Value(i, 3) = oCont.Departamento
                    Case 4:
                            RowBuf.Value(i, 4) = oCont.Cargo
                    Case 5:
                            RowBuf.Value(i, 5) = oCont.Tfno
                    Case 6:
                            RowBuf.Value(i, 6) = oCont.Tfno2
                    Case 7:
                            RowBuf.Value(i, 7) = oCont.tfnomovil
                    Case 8:
                            RowBuf.Value(i, 8) = oCont.Fax
                    Case 9:
                            RowBuf.Value(i, 9) = oCont.mail
                    Case 10:
                            RowBuf.Value(i, 10) = oCont.NIF
                    Case 11:
                           RowBuf.Value(i, 11) = BooleanToSQLBinary(oCont.Def)
                    Case 12:
                            RowBuf.Value(i, 12) = BooleanToSQLBinary(oCont.Aprovisionador)
                    Case 13:
                            RowBuf.Value(i, 13) = BooleanToSQLBinary(oCont.Port)
                    Case 14:
                            RowBuf.Value(i, 14) = BooleanToSQLBinary(oCont.NotifSubasta)
                    Case 15:
                            RowBuf.Value(i, 15) = BooleanToSQLBinary(oCont.Calidad)
                    Case 16:
                            RowBuf.Value(i, 16) = BooleanToSQLBinary(oCont.Principal)
                    Case 0:
                            If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove) Then
                                RowBuf.Value(i, 0) = oCont.EstadoIntegracion
                            End If
                            
            End Select
                        'Set the value of each column in the row buffer to the corresponding value in the arrray
        Next j
    
        RowBuf.Bookmark(i) = m_lIndiceContactos                              'set the value of the bookmark for the current row in the rowbuffer
    
        If ReadPriorRows Then                               'move the pointer forward or backward, depending
            m_lIndiceContactos = m_lIndiceContactos - 1                                           'on which way it's supposed to move
        Else
            m_lIndiceContactos = m_lIndiceContactos + 1
        End If
            r = r + 1                                               'increment the number of rows read
        Next i
    
    RowBuf.RowCount = r                                         'set the size of the row buffer to the number of rows read

    Set oCont = Nothing
    
End Sub

Private Sub txtCodAcceso_Change()
If gParametrosGenerales.giLOGPREBLOQ > 0 Then
    If txtPwd <> "" Then
        ChkCuentBloq.Enabled = True
    Else
        ChkCuentBloq.Enabled = False
    End If
End If
End Sub

Private Sub txtCodProve_Validate(Cancel As Boolean)
    txtCodProve.Text = FSGSLibrary.EliminarEspacioYTab(txtCodProve.Text)
End Sub

Private Sub txtDenProve_LostFocus()
    
    If txtNIF.Enabled And Not m_bModifCalif Then
        If Me.Visible Then txtNIF.SetFocus
    End If
    
End Sub

Private Sub txtFiltroCod_Change()
    sdbcProveCod.Text = txtFiltroCod.Text
End Sub

Private Sub txtFiltroDen_Change()
    sdbcProveDen.Text = txtFiltroDen.Text
End Sub

Private Sub txtObs_GotFocus()
'    stabPROVE.Tab = 3
End Sub

Private Sub txtPwd_Change()
If gParametrosGenerales.giLOGPREBLOQ > 0 Then
    If txtCodAcceso <> "" Then
        ChkCuentBloq.Enabled = True
    Else
        ChkCuentBloq.Enabled = False
    End If
End If
End Sub

Private Sub txtVal1_Change()

If m_bCalif Then Exit Sub
    If txtVal1 = "" Then
        Exit Sub
    End If
    
    If Not IsNumeric(txtVal1) Then
        oMensajes.NoValido m_sIdioma(3)
        txtVal1 = ""
        If Me.Visible Then txtVal1.SetFocus
        Exit Sub
    End If
    
    Set m_oCalif1Seleccionada = BuscarCalificacion(txtVal1, m_oCalificaciones1)
    If m_oCalif1Seleccionada Is Nothing Then 'Si se han cambiado los rangos de las calificaciones, y el valor no se encuentra dentro de los rangos, al cargar los datos NO enviamos mensaje error
        If m_bModoEdicion Then
            m_bCal1RespetarCombo = True
            oMensajes.NoValido m_sIdioma(3)
            m_bCal1RespetarCombo = False
        End If
    Else
        m_bCal1RespetarCombo = True
        sdbcCal1Cod.Text = m_oCalif1Seleccionada.Cod
        sdbcCal1Den.Text = m_oCalif1Seleccionada.Den
        sdbcCal1Cod.Columns(1).Value = m_oCalif1Seleccionada.Den
        sdbcCal1Cod.Columns(0).Value = sdbcCal1Cod.Text
        sdbcCal1Cod.Columns(1).Value = sdbcCal1Den.Text
        m_bCal1RespetarCombo = False
        sdbcCal1Cod_Validate False
 
    End If
    
End Sub

Private Function BuscarCalificacion(ByVal valor As Variant, ByVal oCalificaciones As CCalificaciones) As CCalificacion
Dim iIndice As Integer
Dim iLimSup As Integer

    iIndice = 1
    
    If IsNull(valor) Then
        Set BuscarCalificacion = Nothing
    End If
    
    iLimSup = oCalificaciones.Count
       
       
    While iIndice <= iLimSup
        If oCalificaciones.Item(iIndice).ValorInf <= oCalificaciones.Item(iIndice).ValorSup Then
            If valor >= oCalificaciones.Item(iIndice).ValorInf And valor <= oCalificaciones.Item(iIndice).ValorSup Then
                Set BuscarCalificacion = oCalificaciones.Item(iIndice)
                Exit Function
            End If
        Else
            If valor >= oCalificaciones.Item(iIndice).ValorSup And valor <= oCalificaciones.Item(iIndice).ValorInf Then
                Set BuscarCalificacion = oCalificaciones.Item(iIndice)
                Exit Function
            End If
        End If
        iIndice = iIndice + 1
    Wend
    
    Set BuscarCalificacion = Nothing

End Function



Private Sub txtVal1_KeyPress(KeyAscii As Integer)
m_bCalif = False
End Sub

Private Sub txtVal1_Validate(Cancel As Boolean)
If m_oCalif1Seleccionada Is Nothing Then
    txtVal1 = ""
End If
End Sub

Private Sub txtVal2_Change()

    If m_bCalif Then Exit Sub
    
    If txtVal2 = "" Then
        Exit Sub
    End If
    
    If Not IsNumeric(txtVal2) Then
        oMensajes.NoValido m_sIdioma(5)
        txtVal2 = ""
        If Me.Visible Then txtVal2.SetFocus
        Exit Sub
    End If
    
    Set m_oCalif2Seleccionada = BuscarCalificacion(txtVal2, m_oCalificaciones2)
    
            Set m_oCalif1Seleccionada = BuscarCalificacion(txtVal1, m_oCalificaciones1)
    
    If m_oCalif2Seleccionada Is Nothing Then
        If m_bModoEdicion Then 'Si se han cambiado los rangos de las calificaciones, y el valor no se encuentra dentro de los rangos, al cargar los datos NO enviamos mensaje error
            m_bCal2RespetarCombo = True
            oMensajes.NoValido m_sIdioma(3)
            m_bCal2RespetarCombo = False
        End If
    Else
        m_bCal2RespetarCombo = True
        sdbcCal2Cod.Text = m_oCalif2Seleccionada.Cod
        sdbcCal2Den.Text = m_oCalif2Seleccionada.Den
        sdbcCal2Cod.Columns(1).Value = m_oCalif2Seleccionada.Den
        sdbcCal2Cod.Columns(0).Value = sdbcCal2Cod.Text
        sdbcCal2Cod.Columns(1).Value = sdbcCal2Den.Text
        m_bCal2RespetarCombo = False
        sdbcCal2Cod_Validate False
    
    End If
    
End Sub

Private Sub txtVal2_KeyPress(KeyAscii As Integer)
m_bCalif = False
End Sub

Private Sub txtVal2_Validate(Cancel As Boolean)
If m_oCalif2Seleccionada Is Nothing Then
    txtVal2 = ""
End If

End Sub

Private Sub txtVal3_Change()
   
    If m_bCalif Then Exit Sub
       
    If txtVal3 = "" Then
        Exit Sub
    End If
    
    If Not IsNumeric(txtVal3) Then
        oMensajes.NoValido m_sIdioma(7)
        txtVal3 = ""
        If Me.Visible Then txtVal3.SetFocus
        Exit Sub
    End If
    
    Set m_oCalif3Seleccionada = BuscarCalificacion(txtVal3, m_oCalificaciones3)
        
        If m_oCalif3Seleccionada Is Nothing Then
            If m_bModoEdicion Then 'Si se han cambiado los rangos de la calificacion, y el valor no se encuentra dentro del rango, en modo consulta no se envia mensaje de error
                m_bCal3RespetarCombo = True
                oMensajes.NoValido m_sIdioma(3)
                m_bCal3RespetarCombo = False
            End If
        Else
            m_bCal3RespetarCombo = True
            sdbcCal3Cod.Text = m_oCalif3Seleccionada.Cod
            sdbcCal3Den.Text = m_oCalif3Seleccionada.Den
            sdbcCal3Cod.Columns(1).Value = m_oCalif3Seleccionada.Den
            sdbcCal3Cod.Columns(0).Value = sdbcCal3Cod.Text
            sdbcCal3Cod.Columns(1).Value = sdbcCal3Den.Text
            m_bCal3RespetarCombo = False
            sdbcCal3Cod_Validate False
        
        End If
End Sub

Private Sub txtVal3_KeyPress(KeyAscii As Integer)
m_bCalif = False
End Sub

Private Sub txtVal3_Validate(Cancel As Boolean)
If m_oCalif3Seleccionada Is Nothing Then
    txtVal3 = ""
End If

End Sub

Private Sub MostrarDatosProveedor()
    With g_oProveSeleccionado
        m_bRespetarComboProve = True
        sdbcProveDen.Text = NullToStr(.Den)
        m_bRespetarComboProve = False
                
        fraComunGeneral.Visible = True
        
        'Todos estos datos se usan al grabar, visibles o no, deben estar rellenos correctamente.
        txtDIR.Text = NullToStr(.Direccion)
        txtCP.Text = NullToStr(.cP)
        txtPOB.Text = NullToStr(.Poblacion)
        sdbcPaiCod.Text = NullToStr(.CodPais)
        sdbcPaiCod_Validate False
        sdbcMonCod.Text = NullToStr(.CodMon)
        sdbcMonCod_Validate False
        sdbcMonDen.Text = NullToStr(.DenMon)
        sdbcProviCod.Text = NullToStr(.CodProvi)
        sdbcProviCod_Validate False
        txtNIF.Text = NullToStr(.NIF)
        If .URLPROVE = "NULL" Then
           txtURL = ""
        Else
           txtURL.Text = NullToStr(.URLPROVE)
        End If
        sdbcPagoDen = NullToStr(.FormaPagoDen)
        sdbcPagoCod = NullToStr(.FormaPagoCod)
        sdbcPagoCod_Validate False
        '_______________________________________________________________________
        sdbcViaPagoDen = NullToStr(.ViaPagoDen)
        sdbcViaPagoCod = NullToStr(.ViaPagoCod)
        sdbcViaPagoCod_Validate False
        '�����������������������������������������������������������������������
        Select Case .FormatoPedido
            Case 0:
                sdbcFormato.Text = m_sFormato
                sdbcFormato.Columns(0).Text = m_sFormato
            Case 1:
                sdbcFormato.Text = "Word"
                sdbcFormato.Columns(0).Text = "Word"
            Case 2:
                sdbcFormato.Text = "Excel"
                sdbcFormato.Columns(0).Text = "Excel"
        End Select
        
        sdbcFormato.Columns(1).Value = .FormatoPedido
        chkPremium.Value = BooleanToSQLBinary(.EsPremium)
        
        ''' indicador estado integraci�n
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove) Then
            LinIntegracion.Visible = True
            PicIntegracion.Visible = True
            lblEstInt.Visible = True
            If IsNull(.EstadoIntegracion) Or (.EstadoIntegracion = EstadoIntegracion.recibidocorrecto) Then
                lblEstInt.ToolTipText = m_sIdiSincronizado
                lblEstInt.Backcolor = RGB(212, 208, 200)
            Else
                lblEstInt.ToolTipText = m_sIdiNoSincronizado
                lblEstInt.Backcolor = RGB(255, 0, 0)
            End If
        End If
        
        If gParametrosGenerales.giINSTWEB = ConPortal And Not IsNull(.CodPortal) Then
            fraGeneral.Visible = False
            fraGeneralPortal.Visible = True
            sdbcFormato.Columns(1).Visible = False 'Antes sdbcFormato2 desde el form_load lo ocultaba
                        
            lblDir = NullToStr(.Direccion)
            lblCp = NullToStr(.cP)
            lblPob = NullToStr(.Poblacion)
            lblCodPai = NullToStr(.CodPais)
            lblDenPai = NullToStr(.DenPais)
            lblCodMon = NullToStr(.CodMon)
            lblDenMon = NullToStr(.DenMon)
            lblCodProvi = NullToStr(.CodProvi)
            lblDenProvi = NullToStr(.DenProvi)
            lblNIF = NullToStr(.NIF)
            lblURL = NullToStr(.URLPROVE)
                         
            If m_bModifDatGen Then
                cmdDatosPortal.Enabled = True
                cmdDatosPortal.Visible = True
            Else
                cmdDatosPortal.Enabled = False
                cmdDatosPortal.Visible = False
            End If
        Else
            fraGeneral.Visible = True
            fraGeneralPortal.Visible = False
            sdbcFormato.Columns(1).Visible = True 'Antes sdbcFormato2 desde el form_load lo ocultaba
                                                    
            If .UsuarioWeb Is Nothing Then
                cmdWebCod.Visible = False
                txtCodAcceso.Text = ""
                txtPwd.Text = ""
                txtPwdConf.Text = ""
                ChkCuentBloq.Value = 0
            End If
    
            cmdDatosPortal.Enabled = False
            cmdDatosPortal.Visible = False
        End If
                
        Me.chkAutoFactura.Value = IIf(.AUTOFACTURA, vbChecked, vbUnchecked)
        If .AUTOFACTURA = False Then Me.fraImpuestos.Enabled = False
        Me.chkImpuestos.Value = IIf(.TomarImpuestosDeFacturas, vbChecked, vbUnchecked)
        chkAcepRecepPedidos.Value = IIf(.SolicitarAceptacionRecepcion, vbChecked, vbUnchecked)
        
        'Cargar los proveedores relacionados a los que el proveedor est� subordinado
        CargarProvesRelacionadosPri 1
    End With
End Sub

Private Sub NoBorrarTodo()
        
    picNavigate.Visible = True
    sdbcProveCod.Visible = True
    sdbcProveDen.Visible = True
    cmdA�adir.Visible = m_bAnyadir
    cmdEliminar.Visible = m_bEliminar
    If Not basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Prove) Then
        If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
            cmdCodigo.Visible = True
        End If
    End If
    
    cmdActualizar.Visible = True
    cmdListado.Visible = True
    
    cmdA�adirContacto.Visible = False
    cmdEliminarContacto.Visible = False
    cmdDeshacerContacto.Visible = False
    
    'Proveedores relacionados
    cmdA�adirProveRelac.Visible = False
    cmdEliminarProveRelac.Visible = False
    cmdDeshacerProveRelac.Visible = False

    sdbcCal1Cod.Visible = False
    sdbcCal2Cod.Visible = False
    sdbcCal3Cod.Visible = False
    sdbcCal1Den.Visible = False
    sdbcCal2Den.Visible = False
    sdbcCal3Den.Visible = False
    lblDescRango1.Visible = True
    lblDescRango2.Visible = True
    lblDescRango3.Visible = True
    lblDescRango1 = ""
    lblDescRango2 = ""
    lblDescRango3 = ""
    Set m_oCalif1Seleccionada = Nothing
    Set m_oCalif2Seleccionada = Nothing
    Set m_oCalif3Seleccionada = Nothing
    sdbcCal1Cod.Visible = False
    sdbcCal2Cod.Visible = False
    sdbcCal3Cod.Visible = False
    sdbcCal1Den.Visible = False
    sdbcCal2Den.Visible = False
    sdbcCal3Den.Visible = False
    
    If Not g_oProveSeleccionado Is Nothing Then
        If gParametrosGenerales.giINSTWEB = ConPortal And Not IsNull(g_oProveSeleccionado.CodPortal) Then
            cmdDatosPortal.Visible = True
            cmdDatosPortal.Enabled = True
        Else
            cmdDatosPortal.Visible = False
            cmdDatosPortal.Enabled = False
        End If
    Else
        cmdDatosPortal.Visible = False
        cmdDatosPortal.Enabled = False
    End If
    
    cmdModificar.Enabled = True
    cmdEliminar.Enabled = True
    cmdCodigo.Enabled = True
    cmdA�adir.Enabled = True
    cmdActualizar.Enabled = True
            
    ModoConsulta

End Sub


Private Sub MostrarEmpresas()

Dim oEmpresa As CEmpresa


    Me.sdbgCodERP.RemoveAll
    lstvwEmpresas.ListItems.clear
    
    For Each oEmpresa In g_oProveSeleccionado.Empresas
        Dim sUons As String
        Dim oUON As IUon
        sUons = ""
        For Each oUON In oEmpresa.uons
            sUons = sUons & oUON.titulo(bIncluirDen:=False) & ", "
        Next
        If sUons <> "" Then
            sUons = Left(sUons, Len(sUons) - 2)
        End If
        lstvwEmpresas.ListItems.Add , "EMP" & CStr(oEmpresa.Id), oEmpresa.NIF & " - " & oEmpresa.Den & IIf(sUons <> "", " (" & sUons & ")", ""), , "ERP"
    Next
End Sub

'_______________________________________________________________________
'*** sdbcViaPagoCod ***
'�����������������������������������������������������������������������
Private Sub sdbcViaPagoCod_Change()
 
 If Not m_bViaPagoRespetarCombo Then
    
    m_bViaPagoRespetarCombo = True
    sdbcViaPagoDen.Text = ""
    m_bViaPagoRespetarCombo = False
    
    Set m_oViaPagoSeleccionada = Nothing
        
End If
    
End Sub

Private Sub sdbcViaPagoCod_Click()
    
    If Not sdbcViaPagoCod.DroppedDown Then
        sdbcViaPagoCod = ""
        sdbcViaPagoDen = ""
    End If
    
End Sub

Private Sub sdbcViaPagoCod_CloseUp()
    
    If sdbcViaPagoCod.Value = "..." Then
        sdbcViaPagoCod.Text = ""
        Exit Sub
    End If
    
    m_bViaPagoRespetarCombo = True
    sdbcViaPagoDen.Text = sdbcViaPagoCod.Columns(1).Text
    sdbcViaPagoCod.Text = sdbcViaPagoCod.Columns(0).Text
    m_bViaPagoRespetarCombo = False
    
    Set m_oViaPagoSeleccionada = m_oViaPagos.Item(sdbcViaPagoCod.Columns(0).Text)
    
End Sub

Private Sub sdbcViaPagoCod_DropDown()

Dim Codigos As TipoDatosCombo
Dim i As Integer
        
Set m_oViaPagos = Nothing
Set m_oViaPagos = oFSGSRaiz.Generar_CViaPagos
    
    Screen.MousePointer = vbHourglass
    
    sdbcViaPagoCod.RemoveAll
    
    m_oViaPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
    
    Codigos = m_oViaPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcViaPagoCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oViaPagos.EOF Then
        sdbcViaPagoCod.AddItem "..."
    End If

    sdbcViaPagoCod.SelStart = 0
    sdbcViaPagoCod.SelLength = Len(sdbcViaPagoCod.Text)
    sdbcViaPagoCod.Refresh
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcViaPagoCod_InitColumnProps()
    sdbcViaPagoCod.DataFieldList = "Column 0"
    sdbcViaPagoCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcViaPagoCod_PositionList(ByVal Text As String)
PositionList sdbcViaPagoCod, Text
End Sub

Private Sub sdbcViaPagoCod_Validate(Cancel As Boolean)
    
    Dim m_oViaPagos As CViaPagos
    Dim bExiste As Boolean
    
    Set m_oViaPagos = oFSGSRaiz.Generar_CViaPagos
    
    If sdbcViaPagoCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    m_oViaPagos.CargarTodosLosPagos sdbcViaPagoCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (m_oViaPagos.Count = 0)
    
    If Not bExiste Then
        sdbcViaPagoCod.Text = ""
    Else
        m_bViaPagoRespetarCombo = True
        sdbcViaPagoDen.Text = m_oViaPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcViaPagoCod.Columns(0).Value = sdbcViaPagoCod.Text
        sdbcViaPagoCod.Columns(1).Value = sdbcViaPagoDen.Text
        m_bViaPagoRespetarCombo = False
        
        Set m_oViaPagoSeleccionada = m_oViaPagos.Item(1)
        
    End If
    
    Set m_oViaPagos = Nothing
End Sub

'_______________________________________________________________________
'* * * sdbcViaPagoDen * * *
'�����������������������������������������������������������������������
Private Sub sdbcViaPagoDen_Change()
If Not m_bViaPagoRespetarCombo Then

        m_bViaPagoRespetarCombo = True
        sdbcViaPagoCod.Text = ""
        m_bViaPagoRespetarCombo = False
        Set m_oViaPagoSeleccionada = Nothing
End If
    
End Sub

Private Sub sdbcViaPagoDen_CloseUp()
  
    If sdbcViaPagoDen.Value = "..." Then
        sdbcViaPagoDen.Text = ""
        Exit Sub
    End If
    
    m_bViaPagoRespetarCombo = True
    sdbcViaPagoCod.Text = sdbcViaPagoDen.Columns(1).Text
    sdbcViaPagoDen.Text = sdbcViaPagoDen.Columns(0).Text
    m_bViaPagoRespetarCombo = False
    
    Set m_oViaPagoSeleccionada = m_oViaPagos.Item(sdbcViaPagoDen.Columns(1).Text)
    
    
End Sub

Private Sub sdbcViaPagoDen_DropDown()

Dim Codigos As TipoDatosCombo
Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set m_oViaPagos = Nothing
    Set m_oViaPagos = oFSGSRaiz.Generar_CViaPagos
     
    sdbcViaPagoDen.RemoveAll
    
    m_oViaPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
    
    Codigos = m_oViaPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcViaPagoDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not m_oViaPagos.EOF Then
        sdbcViaPagoDen.AddItem "..."
    End If

    sdbcViaPagoDen.SelStart = 0
    sdbcViaPagoDen.SelLength = Len(sdbcViaPagoDen.Text)
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcViaPagoDen_InitColumnProps()

    sdbcViaPagoDen.DataFieldList = "Column 0"
    sdbcViaPagoDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcViaPagoDen_PositionList(ByVal Text As String)
PositionList sdbcViaPagoDen, Text
End Sub




'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
Private Sub CargarGMN1Automaticamente()
    Dim oGMN1s As CGruposMatNivel1
    Dim m_oIMAsig As IMaterialAsignado
    Set m_oIMAsig = oUsuarioSummit.comprador
    Set oGMN1s = m_oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
    If Not oGMN1s.Count = 0 Then
        m_bGMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        m_bGMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    Set oGMN1s = Nothing
End Sub

Private Sub CargarProvesRelacionados(ByVal iOrden As Integer)
Dim oProve As CProveedor

With g_oProveSeleccionado
    Select Case iOrden
    Case 1
        .CargarTodosLosProveedoresRelacionados True
    Case 2
        .CargarTodosLosProveedoresRelacionados , True
    Case 3
        .CargarTodosLosProveedoresRelacionados , , True
    Case 4
        .CargarTodosLosProveedoresRelacionados , , , True
    End Select
    
    sdbgProveRelac.RemoveAll
    
    For Each oProve In .ProveRel
        sdbgProveRelac.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den & Chr(m_lSeparador) & NullToStr(oProve.CodPais) & " " & NullToStr(oProve.DenPais) & Chr(m_lSeparador) & NullToStr(oProve.TipoRelacionDen) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oProve.TipoRelacion) & Chr(m_lSeparador) & NullToStr(oProve.CodPortal)
    Next
    
    If .ProveRel.Count > 0 Then
        stabProve.Tab = 7
        stabProve.Picture = ImageList2.ListImages("CHECK").Picture
        stabProve.Tab = 0
    Else
        stabProve.Tab = 7
        stabProve.Picture = LoadPicture("")
        stabProve.Tab = 0
    End If
    
End With

sdbddTipoRel.Enabled = False
sdbddTipoRel.AddItem ""
sdbgProveRelac.Columns("TREL").DropDownHwnd = sdbddTipoRel.hWnd
End Sub

''' <summary>Cargar proveedores relacionados principales (a los que el proveedor est� subordinado)</summary>
''' <param name="iOrden">Orden: 1-Cod, 2-den,3-TipoRel</param>
''' <remarks>Llamada desde:frmProve.ProveedorRelacionado, frmProve.PrepararAnyadir, frmProve.HeadClick
''' Tiempo m�ximo: 1 sec </remarks>
''' Revisada JVS 09/04/2011
Private Sub CargarProvesRelacionadosPri(ByVal iOrden As Integer)
Dim oProve As CProveedor

With g_oProveSeleccionado
    Select Case iOrden
    Case 1
        .CargarTodosLosProveedoresRelacionadosPri True
    Case 2
        .CargarTodosLosProveedoresRelacionadosPri , True
    Case 3
        .CargarTodosLosProveedoresRelacionadosPri , , True
    End Select
    
    sdbgProveedoresPri.RemoveAll
    
    For Each oProve In .ProveRelPri
        sdbgProveedoresPri.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den & Chr(m_lSeparador) & NullToStr(oProve.TipoRelacionDen) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oProve.TipoRelacion) & Chr(m_lSeparador) & NullToStr(oProve.CodPortal)
    Next
    If sdbgProveedoresPri.Rows > 0 Then
        fraProveRelacPri.Visible = True
    Else
        fraProveRelacPri.Visible = False
    End If
End With

End Sub

''' <summary>
''' Evento de drop-down del grid de tipos relacionados
''' </summary>
''' <remarks>Llamada desde: ; Tiempo m�ximo: 1 sec</remarks>
''' <revision>JVS 24/08/2011</revision>
Private Sub sdbddTipoRel_DropDown()


    If sdbgProveRelac.Columns("TREL").Locked Then
        sdbddTipoRel.DroppedDown = False
        Exit Sub
    End If
    
    m_oTiposRel.CargarTodosLosRelacTipos

    CargarGridConTipos

    sdbgProveRelac.ActiveCell.SelStart = 0
    sdbgProveRelac.ActiveCell.SelLength = Len(sdbgProveRelac.ActiveCell.Value)
End Sub

Private Sub sdbddTipoRel_InitColumnProps()

    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid

    sdbddTipoRel.DataFieldList = "Column 1"
    sdbddTipoRel.DataFieldToDisplay = "Column 1"

End Sub

Private Sub sdbddTipoRel_PositionList(ByVal Text As String)
PositionList sdbddTipoRel, Text, 1
End Sub

Private Sub sdbddTipoRel_CloseUp()
    sdbgProveRelac.Columns.Item(5).Value = sdbddTipoRel.Columns("ID").Value
    
    If Not g_oProveSeleccionado.ProveRel.Item(sdbgProveRelac.Columns.Item("COD").Value) Is Nothing Then
        g_oProveSeleccionado.ProveRel.Item(sdbgProveRelac.Columns.Item("COD").Value).TipoRelacion = sdbddTipoRel.Columns("ID").Value
    End If
End Sub

Private Sub sdbddTipoRel_ValidateList(Text As String, RtnPassed As Integer)

    If Text = "" Then
        RtnPassed = True
    Else

        ''' Comprobar la existencia en la lista
        If sdbddTipoRel.Columns(1).Value = Text Then
            RtnPassed = True
            Exit Sub
        End If

        m_oTiposRel.CargarTodosLosRelacTipos False, , Text, gParametrosInstalacion.gIdioma

        If Not m_oTiposRel.Item(1) Is Nothing Then
            sdbgProveRelac.Columns.Item(5).Value = m_oTiposRel.Item(1).Id
            If Not g_oProveSeleccionado.ProveRel.Item(sdbgProveRelac.Columns.Item("COD").Value) Is Nothing Then
                g_oProveSeleccionado.ProveRel.Item(sdbgProveRelac.Columns.Item("COD").Value).TipoRelacion = m_oTiposRel.Item(1).Id
            End If
            RtnPassed = True
        Else
            sdbgProveRelac.Columns("TREL").Value = ""
        End If

    End If

End Sub

Private Sub CargarGridConTipos()

    ''' * Objetivo: Cargar combo con la coleccion de formas de pago

    Dim oTipo As CRelacTipo

    sdbddTipoRel.RemoveAll

    For Each oTipo In m_oTiposRel
        sdbddTipoRel.AddItem oTipo.Cod & Chr(m_lSeparador) & oTipo.Descripciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & BooleanToSQLBinary(oTipo.PermitePedidos) & Chr(m_lSeparador) & oTipo.Id
    Next

    If sdbddTipoRel.Rows = 0 Then
        sdbddTipoRel.AddItem ""
    End If

End Sub


Public Sub AnyadirProveRelacConBusqueda()
    Dim oProve As CProveedor
    Dim oProveEncontrado As CProveedor
    Dim oProves As CProveedores
    Dim oProvesEncontrados As CProveedores
    Dim sCodProveEncontrado As String
    Dim lIndice As Long
    

    Set oProvesEncontrados = Nothing
    Set oProvesEncontrados = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    
    Screen.MousePointer = vbHourglass
    lIndice = g_oProveSeleccionado.ProveRel.Count
    For Each oProveEncontrado In oProvesEncontrados
        Set oProves = oFSGSRaiz.generar_CProveedores
        sCodProveEncontrado = oProveEncontrado.Cod
        oProves.CargarTodosLosProveedoresDesde3 1, sCodProveEncontrado, , True
        Set oProve = oProves.Item(1)
        oProves.CargarDatosProveedor sCodProveEncontrado
        sdbgProveRelac.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den & Chr(m_lSeparador) & NullToStr(oProve.CodPais) & " " & NullToStr(oProve.DenPais) & Chr(m_lSeparador) & NullToStr(oProve.TipoRelacionDen) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oProve.TipoRelacion) & Chr(m_lSeparador) & NullToStr(oProve.CodPortal)
        g_oProveSeleccionado.ProveRel.Add sCod:=oProve.Cod, sDen:=oProve.Den, vCodPais:=oProve.CodPais, vDenPais:=oProve.DenPais, vTipoRel:=oProve.TipoRelacion, vTipoRelDen:=oProve.TipoRelacionDen, vCodPortal:=oProve.CodPortal
        lIndice = lIndice + 1
    Next
    Screen.MousePointer = vbNormal
    
    Unload frmPROVEBuscar
    
    sdbgProveRelac.MoveLast
    If Me.Visible Then sdbgProveRelac.SetFocus

End Sub

Private Sub MostrarDetalleProveedorPri(ByVal CodProve As String)
    Dim oProves As CProveedores
    Dim oProve As CProveedor
    Dim oCon As CContacto
    
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    
    With frmProveDetalle
    Set .g_oProveSeleccionado = oProve
    
    .caption = oProve.Cod & "  " & oProve.Den
    .lblCodPortProve = NullToStr(oProve.CodPortal)
    .g_bPremium = oProve.EsPremium
    .g_bActivo = oProve.EsPremiumActivo
    .lblDir = NullToStr(oProve.Direccion)
    .lblCp = NullToStr(oProve.cP)
    .lblPob = NullToStr(oProve.Poblacion)
    .lblPaiCod = NullToStr(oProve.CodPais)
    .lblPaiDen = NullToStr(oProve.DenPais)
    .lblMonCod = NullToStr(oProve.CodMon)
    .lblMonDen = NullToStr(oProve.DenMon)
    .lblProviCod = NullToStr(oProve.CodProvi)
    .lblProviDen = NullToStr(oProve.DenProvi)
    .lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    .lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    .lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If Trim(oProve.Calif1) <> "" Then
        .lblCal1Den = oProve.Calif1
    End If
    
    If Trim(oProve.Calif2) <> "" Then
        .lblcal2den = oProve.Calif2
    End If
    
    If Trim(oProve.Calif3) <> "" Then
        .lblcal3den = oProve.Calif3
    End If
    
    .lblCal1Val = DblToStr(oProve.Val1)
    .lblCal2Val = DblToStr(oProve.Val2)
    .lblCal3Val = DblToStr(oProve.Val3)

    .lblURL = NullToStr(oProve.URLPROVE)

    oProve.CargarTodosLosContactos , , , , True, , , , , , , True
    
    For Each oCon In oProve.Contactos
        
        .sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
        & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
    
    Next
        
    .Show vbModal
    End With
End Sub

Private Sub CargaProveedorPri(ByVal CodProve As String)
    sdbcProveCod.Text = CodProve
    sdbcProveCod_DropDown

    m_oProves.CargarDatosProveedor CodProve, False, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
    
    Set g_oProveSeleccionado = Nothing
    Set g_oProveSeleccionado = m_oProves.Item(CodProve)
    
   
    If Not g_oProveSeleccionado Is Nothing Then
                    
        With g_oProveSeleccionado
            ''' Pasamos el usuario que realiza la modificaci�n (LOG e Integraci�n)
            .Usuario = basOptimizacion.gvarCodUsuario

            m_bCal1CargarComboDesde = False
            sdbcCal1Cod_DropDown
            m_bCal2CargarComboDesde = False
            sdbcCal2Cod_DropDown
            m_bCal3CargarComboDesde = False
            sdbcCal3Cod_DropDown
        
            stabProve.Enabled = True
            MostrarDatosProveedor
            
             
            If Trim(.Calif1) <> "" And Not m_oCalificaciones1.Item(.Calif1) Is Nothing Then
                lblDescRango1 = .Calif1 & " " & m_oCalificaciones1.Item(.Calif1).Den
            Else
                lblDescRango1 = ""
            End If
            
            If Trim(.Calif2) <> "" And Not m_oCalificaciones2.Item(.Calif2) Is Nothing Then
                lblDescRango2 = .Calif2 & " " & m_oCalificaciones2.Item(.Calif2).Den
            Else
                lblDescRango2 = ""
            End If
            If Trim(.Calif3) <> "" And Not m_oCalificaciones3.Item(.Calif3) Is Nothing Then
                lblDescRango3 = .Calif3 & " " & m_oCalificaciones3.Item(.Calif3).Den
            Else
                lblDescRango3 = ""
            End If
            
            txtVal1 = ""
            txtVal2 = ""
            txtVal3 = ""
            txtVal1 = NullToStr(.Val1)
            txtVal2 = NullToStr(.Val2)
            txtVal3 = NullToStr(.Val3)
            'Rellena los combos de calificaciones
            If Not IsNull(.Calif1) And .Calif1 <> "" Then
                sdbcCal1Cod.Columns(0).Value = .Calif1
                sdbcCal1Cod.Columns(1).Value = m_oCalificaciones1.Item(.Calif1).Den
                sdbcCal1Den.Columns(1).Value = .Calif1
                sdbcCal1Den.Columns(0).Value = m_oCalificaciones1.Item(.Calif1).Den
            Else
                sdbcCal1Cod.Columns(0).Value = ""
                sdbcCal1Cod.Columns(1).Value = ""
                sdbcCal1Den.Columns(1).Value = ""
                sdbcCal1Den.Columns(0).Value = ""
            End If
            
            If Not IsNull(.Calif2) And .Calif2 <> "" Then
                sdbcCal2Cod.Columns(0).Value = .Calif2
                sdbcCal2Cod.Columns(1).Value = m_oCalificaciones2.Item(.Calif2).Den
                sdbcCal2Den.Columns(1).Value = .Calif2
                sdbcCal2Den.Columns(0).Value = m_oCalificaciones2.Item(.Calif2).Den
            Else
                sdbcCal2Cod.Columns(0).Value = ""
                sdbcCal2Cod.Columns(1).Value = ""
                sdbcCal2Den.Columns(1).Value = ""
                sdbcCal2Den.Columns(0).Value = ""
            End If
            
            If Not IsNull(.Calif3) And .Calif3 <> "" Then
                sdbcCal3Cod.Columns(0).Value = .Calif3
                sdbcCal3Cod.Columns(1).Value = m_oCalificaciones3.Item(.Calif3).Den
                sdbcCal3Den.Columns(1).Value = .Calif3
                sdbcCal3Den.Columns(0).Value = m_oCalificaciones3.Item(.Calif3).Den
            Else
                sdbcCal3Cod.Columns(0).Value = ""
                sdbcCal3Cod.Columns(1).Value = ""
                sdbcCal3Den.Columns(1).Value = ""
                sdbcCal3Den.Columns(0).Value = ""
            End If
            
            'Cargamos las especificaciones
            txtObs = NullToStr(.obs)
            .CargarTodasLasEspecificaciones True, False
            AnyadirEspsALista
            If m_bConsultaEspPortalProve Then
                If Not IsNull(.CodPortal) Then
                    AnyadirEspsProvePortalALista
                    'Volvemos a mostrar la lista de especificaciones del portal por si antes
                    'hemos seleccionado un prove no enlazado y se ha ocultado
                    lblEspProvePortal.Visible = True
                    lstvwEspProvePortal.Visible = True
                    picEspProvePortal.Visible = True
                    lstvwEspProvePortal.Height = stabProve.Height / 3 - 100
                    lstvwEspProvePortal.Width = stabProve.Width - 300
                    lstvwEspProvePortal.ColumnHeaders(1).Width = lstvwEspProvePortal.Width * 0.2
                    lstvwEspProvePortal.ColumnHeaders(2).Width = lstvwEspProvePortal.Width * 0.1
                    lstvwEspProvePortal.ColumnHeaders(3).Width = lstvwEspProvePortal.Width * 0.5
                    lstvwEspProvePortal.ColumnHeaders(4).Width = lstvwEspProvePortal.Width * 0.19
                    picEspProvePortal.Top = lstvwEspProvePortal.Top + lstvwEspProvePortal.Height + 20
                    If stabProve.Tab = 4 Then
                        picEspProvePortal.Left = lstvwEspProvePortal.Width - (cmdAbrirEspPortal.Width * 6) - 100
                    End If
                    lstvwEsp.Top = picEspProvePortal.Top + picEspProvePortal.Height
                    If stabProve.Height > 5000 Then
                        lstvwEsp.Height = lstvwEspProvePortal.Height + (lstvwEspProvePortal.Height * 0.3)
                    Else
                        lstvwEsp.Height = lstvwEspProvePortal.Height + (lstvwEspProvePortal.Height * 0.125)
                    End If
                    picEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 60
                    lstvwEsp.Width = lstvwEspProvePortal.Width
                Else
                    'Si el prove no est� enlazado ocultamos la lista de especificaciones del portal
                    OcultarEspPortal
                End If
            End If
            
            lblCodPortal.Text = NullToStr(.CodPortal)
            
            .CargarTodosLosContactos , , , , True, , , , , , , , True, , , basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Prove)
            
            sdbgContactos.ReBind
            
            
            If m_bModifDatGen Or m_bModifCalif Then
                cmdModificar.Enabled = True
            End If
            If m_bEliminar Then
                cmdEliminar.Enabled = True
            End If
            
            If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador And Not basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Prove) Then
                cmdCodigo.Enabled = True
            End If
                               
       
            .CargarEmpresas
            MostrarEmpresas
            
            Dim oEmpresa As CEmpresa
            For Each oEmpresa In .Empresas

                Exit For
            Next
            If Not oEmpresa Is Nothing Then
                oEmpresa.CargarERPs .Cod
                lstvwEmpresas_ItemClick lstvwEmpresas.ListItems(1)
            End If
            
            'Cargar los proveedores relacionados
            CargarProvesRelacionados 1
            
            'Cargar los proveedores relacionados a los que el proveedor est� subordinado
            CargarProvesRelacionadosPri 1
            
        End With
        
        cmdActualizar.Enabled = True
    Else
        cmdEliminar.Enabled = False
        cmdModificar.Enabled = False
        cmdActualizar.Enabled = False
        cmdCodigo.Enabled = False
        cmdDatosPortal.Visible = False
        cmdDatosPortal.Enabled = False
    End If
End Sub

