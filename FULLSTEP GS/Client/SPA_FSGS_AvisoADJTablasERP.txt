
La siguiente adjudicaci�n @PROCESO
no ha podido ser trasladada al ERP.
  

A continuaci�n, le indicamos los c�digos FULLSTEP GS para los que no existe 
su correspondiente c�digo en el ERP.
Por favor, actualice los c�digos ERP a trav�s de la aplicaci�n Access
de mantenimiento de tablas intermedias para la/s empresa/s indicada/s.

<EMPRESA>
============================================================	
Empresa : @NIF - @DEN
c�digo ERP Proveedor: @PROVE
	
Unidades
========
@UNI
	
Monedas
=======
@MON

Formas de pago
===============
@PAG
============================================================
</EMPRESA>

Un saludo,




