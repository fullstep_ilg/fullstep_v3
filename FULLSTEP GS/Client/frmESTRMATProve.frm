VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRMATProve 
   BackColor       =   &H00808000&
   Caption         =   "Ultimas adjudicaciones (Consulta)"
   ClientHeight    =   3195
   ClientLeft      =   570
   ClientTop       =   3150
   ClientWidth     =   9255
   Icon            =   "frmESTRMATProve.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3195
   ScaleWidth      =   9255
   Begin SSDataWidgets_B.SSDBGrid sdbgAdjudicaciones 
      Height          =   2535
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   9135
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   2
      stylesets(0).Name=   "Gris"
      stylesets(0).BackColor=   14671839
      stylesets(0).Picture=   "frmESTRMATProve.frx":014A
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmESTRMATProve.frx":0166
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   14
      Columns(0).Width=   1005
      Columns(0).Caption=   "Dest."
      Columns(0).Name =   "DEST"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1138
      Columns(1).Caption=   "Unidad"
      Columns(1).Name =   "UNI"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   2487
      Columns(2).Caption=   "dProceso"
      Columns(2).Name =   "PROCE"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1693
      Columns(3).Caption=   "Proveedor"
      Columns(3).Name =   "COD"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   2990
      Columns(4).Caption=   "Nombre Proveedor"
      Columns(4).Name =   "DEN"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   1852
      Columns(5).Caption=   "Precio"
      Columns(5).Name =   "PREC"
      Columns(5).Alignment=   1
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "#,##0.00####################"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1402
      Columns(6).Caption=   "dMoneda"
      Columns(6).Name =   "MON"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   2461
      Columns(7).Caption=   "Cantidad"
      Columns(7).Name =   "CANT"
      Columns(7).Alignment=   1
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).NumberFormat=   "Standard"
      Columns(7).FieldLen=   256
      Columns(8).Width=   1323
      Columns(8).Caption=   "% Adj"
      Columns(8).Name =   "PORC"
      Columns(8).Alignment=   1
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).NumberFormat=   "0.0#\%"
      Columns(8).FieldLen=   256
      Columns(9).Width=   2143
      Columns(9).Caption=   "F. Inicio"
      Columns(9).Name =   "FECINI"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Style=   1
      Columns(10).Width=   2143
      Columns(10).Caption=   "F. Fin"
      Columns(10).Name=   "FECFIN"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).Style=   1
      Columns(11).Width=   3200
      Columns(11).Caption=   "dFecha"
      Columns(11).Name=   "FECHA"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   2328
      Columns(12).Caption=   "DUsuario"
      Columns(12).Name=   "USU"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "ULTIMO"
      Columns(13).Name=   "ULTIMO"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      _ExtentX        =   16113
      _ExtentY        =   4471
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   9255
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2640
      Width           =   9255
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5460
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Left            =   1200
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveedoresDen 
      Height          =   1575
      Left            =   1185
      TabIndex        =   11
      Top             =   975
      Width           =   4770
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5239
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   50
      Columns(1).Width=   3149
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   3
      _ExtentX        =   8414
      _ExtentY        =   2778
      _StockProps     =   77
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProveedores 
      Height          =   1575
      Left            =   1980
      TabIndex        =   8
      Top             =   690
      Width           =   4275
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2514
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   4868
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      _ExtentX        =   7541
      _ExtentY        =   2778
      _StockProps     =   77
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddUnidades 
      Height          =   1575
      Left            =   1275
      TabIndex        =   9
      Top             =   360
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2461
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   5530
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddDestinos 
      Height          =   1260
      Left            =   300
      TabIndex        =   10
      Top             =   120
      Width           =   7350
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelColorFace  =   12632256
      ForeColorEven   =   0
      BackColorEven   =   -2147483633
      BackColorOdd    =   -2147483633
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   1429
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadBackColor=   -2147483633
      Columns(0).BackColor=   16777152
      Columns(1).Width=   2487
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HeadBackColor=   -2147483633
      Columns(2).Width=   3200
      Columns(2).Caption=   "Direcci�n"
      Columns(2).Name =   "DIR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HeadBackColor=   -2147483633
      Columns(3).Width=   2090
      Columns(3).Caption=   "Poblaci�n"
      Columns(3).Name =   "POB"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasHeadBackColor=   -1  'True
      Columns(3).HeadBackColor=   -2147483633
      Columns(4).Width=   1138
      Columns(4).Caption=   "CP"
      Columns(4).Name =   "CP"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasHeadBackColor=   -1  'True
      Columns(4).HeadBackColor=   -2147483633
      Columns(5).Width=   926
      Columns(5).Caption=   "Pa�s"
      Columns(5).Name =   "PAI"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).HasHeadBackColor=   -1  'True
      Columns(5).HeadBackColor=   -2147483633
      Columns(6).Width=   1402
      Columns(6).Caption=   "Provincia"
      Columns(6).Name =   "PROVI"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).HasHeadBackColor=   -1  'True
      Columns(6).HeadBackColor=   -2147483633
      _ExtentX        =   12965
      _ExtentY        =   2222
      _StockProps     =   77
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmESTRMATProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

''' *** Formulario: frmESTRMATProve
''' *** Creacion: 24/2/1999 (Javier Arana)
Option Explicit

'Variable para indicar en que modo de trabajo estamos
' - 1 = Con adjudicaciones anteriores al summit
' - 0 = Con adjudicaciones del summit (Solo lectura)
Public bModoAdjAnteriores As Boolean

'Articulo sobre el que trabajamos
Public oArticulo As CArticulo

''' Coleccion de Adjudicaciones, y adjudicaciones de articulos
Public oAdjudicaciones As CAdjsDeArt

''' Adjudicacion en edicion
Private oAdjudicacionEnEdicion As CAdjDeArt
Private oIBAseDatosEnEdicion As IBaseDatos

''' Propiedades de seguridad
Private bModif  As Boolean

''' Control de errores
Private bModError As Boolean
Private bAnyaError As Boolean
Private bValError As Boolean

''' Variables de control
Public Accion As AccionesSummit
Public bModoEdicion As Boolean

''' Posicion para UnboundReadData
Private p As Long
''' Coleccion de proveedores
Private oProveedores As CProveedores
''' Coleccion de destinos
Private oDestinos As CDestinos
''' Coleccion de unidades
Private oUnidades As CUnidades

Private bCargarComboDesdeDD As Boolean

'HBA
Private sIdiTitulos(2) As String
Private sIdiModos(2) As String
Private sIdiOperacion As String
Private sIdiMoneda As String
Private sIdiAdj As String
Private sIdiDest As String
Private sIdiUnid As String
Private sIdiProv As String
Private sIdiPrec As String
Private sIdiPorc As String
Private sIdiFIni As String
Private sIdiFFin As String
Private sIdiCant As String

Private m_bmostrarAdjudicacionesLineaSolicitud As Boolean
Private m_bmostrarMonPrecio As Boolean
Public g_sOrigen As String
Public g_bPreAdj As Boolean
''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiAdjMod)) Is Nothing) Then
        bModif = True
    Else
        bModif = False
    End If
        
    cmdModoEdicion.Visible = bModif
End Sub

Private Sub Arrange()
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    If bModoAdjAnteriores Then
        If Height >= 900 Then sdbgAdjudicaciones.Height = Height - 900
    Else
        If Height >= 550 Then sdbgAdjudicaciones.Height = Height - 550
    End If
    If Width >= 250 Then sdbgAdjudicaciones.Width = Width - 250
    sdbgAdjudicaciones.Columns("DEST").Width = sdbgAdjudicaciones.Width * 6 / 100
    sdbgAdjudicaciones.Columns("UNI").Width = sdbgAdjudicaciones.Width * 6 / 100
    sdbgAdjudicaciones.Columns("COD").Width = sdbgAdjudicaciones.Width * 12 / 100
    sdbgAdjudicaciones.Columns("DEN").Width = sdbgAdjudicaciones.Width * 20 / 100
    sdbgAdjudicaciones.Columns("PREC").Width = sdbgAdjudicaciones.Width * 8 / 100
    sdbgAdjudicaciones.Columns("CANT").Width = sdbgAdjudicaciones.Width * 5 / 100
    sdbgAdjudicaciones.Columns("USU").Width = sdbgAdjudicaciones.Width * 10 / 100
    cmdModoEdicion.Left = sdbgAdjudicaciones.Left + sdbgAdjudicaciones.Width - cmdModoEdicion.Width
    
End Sub
Private Sub cmdA�adir_Click()
    
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgAdjudicaciones.Scroll 0, sdbgAdjudicaciones.Rows - sdbgAdjudicaciones.Row
    
    If sdbgAdjudicaciones.VisibleRows > 0 Then
        
        If sdbgAdjudicaciones.VisibleRows >= sdbgAdjudicaciones.Rows Then
            sdbgAdjudicaciones.Row = sdbgAdjudicaciones.Rows
        Else
            sdbgAdjudicaciones.Row = sdbgAdjudicaciones.Rows - (sdbgAdjudicaciones.Rows - sdbgAdjudicaciones.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgAdjudicaciones.SetFocus
    
End Sub
Private Sub cmdDeshacer_Click()

    ''' * Objetivo: Deshacer la edicion en la Adjudicacion actual
    
    sdbgAdjudicaciones.CancelUpdate
    sdbgAdjudicaciones.DataChanged = False
    
    If Not oAdjudicacionEnEdicion Is Nothing Then
        Screen.MousePointer = vbHourglass
        oIBAseDatosEnEdicion.CancelarEdicion
        Set oIBAseDatosEnEdicion = Nothing
        Set oAdjudicacionEnEdicion = Nothing
        Screen.MousePointer = vbNormal
    End If
    
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    
    Accion = ACCArtAdjCon
        
End Sub
Private Sub cmdEliminar_Click()

    ''' * Objetivo: Eliminar la Adjudicacion actual
    
    If sdbgAdjudicaciones.Rows = 0 Then Exit Sub
    sdbgAdjudicaciones.SelBookmarks.Add sdbgAdjudicaciones.Bookmark
    sdbgAdjudicaciones.DeleteSelected
    sdbgAdjudicaciones.SelBookmarks.RemoveAll
    
End Sub
Private Sub cmdlistado_Click()

    ''' * Objetivo: Obtener un listado de Adjudicaciones

    MsgBox "Opcion no disponible en esta versi�n.", vbInformation, "FullStep"
    
End Sub
Private Sub cmdModoEdicion_Click()
    
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
                
        'cmdRestaurar_Click
        
        sdbgAdjudicaciones.AllowAddNew = True
        sdbgAdjudicaciones.AllowUpdate = True
        sdbgAdjudicaciones.AllowDelete = True
        
'        frmESTRMATProve.Caption = "Ultimas adjudicaciones (Edici�n)"
         frmESTRMATProve.caption = sIdiTitulos(2)
         
        
'        cmdModoEdicion.Caption = "&Consulta"
        cmdModoEdicion.caption = sIdiModos(2)
        
        cmdRestaurar.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
            
        bModoEdicion = True
        
        Accion = ACCArtAdjCon
    
        'inicializar las dropdown
        sdbddUnidades.AddItem ""
        sdbddDestinos.AddItem ""
        sdbddProveedores.AddItem ""
        sdbddProveedoresDen.AddItem ""
        'inicializar las colecciones
        Set oDestinos = Nothing
        
        Screen.MousePointer = vbHourglass
        Set oDestinos = oFSGSRaiz.Generar_CDestinos
        Set oUnidades = Nothing
        Set oUnidades = oFSGSRaiz.Generar_CUnidades
        Set oProveedores = Nothing
        Set oProveedores = oFSGSRaiz.generar_CProveedores
        Screen.MousePointer = vbNormal
        
    Else
                
        If sdbgAdjudicaciones.DataChanged = True Then
        
            v = sdbgAdjudicaciones.ActiveCell.Value
            If Me.Visible Then sdbgAdjudicaciones.SetFocus
            If (v <> "") Then
                sdbgAdjudicaciones.ActiveCell.Value = v
            End If
            
            bValError = False
            bAnyaError = False
            bModError = False
            
            sdbgAdjudicaciones.Update
            
            If bValError Or bAnyaError Or bModError Then
                Exit Sub
            End If
            
        End If
        
        sdbgAdjudicaciones.AllowAddNew = False
        sdbgAdjudicaciones.AllowUpdate = False
        sdbgAdjudicaciones.AllowDelete = False
                
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdRestaurar.Visible = True
        'cmdListado.Visible = True
        
        
'        frmESTRMATProve.Caption = "Ultimas adjudicaciones (Consulta)"
        frmESTRMATProve.caption = sIdiTitulos(1)
        
'        cmdModoEdicion.Caption = "&Edicion"
        cmdModoEdicion.caption = sIdiModos(1)
        
        cmdRestaurar_Click
        
        bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgAdjudicaciones.SetFocus
    
End Sub
Private Sub cmdRestaurar_Click()

    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
    
'    Me.Caption = "Ultimas adjudicaciones (Consulta)"
    Me.caption = sIdiTitulos(1)
    
    Screen.MousePointer = vbHourglass
    
    'If bModoAdjAnteriores Then
        
    '    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, oArticulo, True, True
    'Else
        oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, oArticulo, False, True
    'End If
    
    Screen.MousePointer = vbNormal
    
    sdbgAdjudicaciones.ReBind
    
    If Me.Visible Then sdbgAdjudicaciones.SetFocus
    
End Sub

Private Sub Form_Initialize()
    m_bmostrarMonPrecio = True
End Sub

Private Sub Form_Load()
   
    Me.Width = 13000
    Me.Height = 4350
    
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2


    ''' * Objetivo: Cargar las Adjudicaciones e iniciar
    ''' * Objetivo: el formulario
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    ConfigurarSeguridad
    
    ConfigurarVisibilidadColumnas
       
    'Ya tenemos cargadas las adjudicaciones en oAdjudicaciones
    
    If bModoAdjAnteriores Then
        'Enganchar las dropdown
        sdbgAdjudicaciones.Columns("DEST").DropDownHwnd = sdbddDestinos.hWnd
        sdbgAdjudicaciones.Columns("UNI").DropDownHwnd = sdbddUnidades.hWnd
        sdbgAdjudicaciones.Columns("COD").DropDownHwnd = sdbddProveedores.hWnd
        sdbgAdjudicaciones.Columns("DEN").DropDownHwnd = sdbddProveedoresDen.hWnd
        If bModoEdicion Then
           
            sdbgAdjudicaciones.AllowAddNew = True
            sdbgAdjudicaciones.AllowUpdate = True
            sdbgAdjudicaciones.AllowDelete = True
    
            frmESTRMATProve.caption = sIdiTitulos(2)
             
            cmdModoEdicion.caption = sIdiModos(2)
            
            cmdRestaurar.Visible = False
            cmdA�adir.Visible = True
            cmdEliminar.Visible = True
            cmdDeshacer.Visible = True
                
            bModoEdicion = True
            
            Accion = ACCArtAdjCon
        
            'inicializar las dropdown
            sdbddUnidades.AddItem ""
            sdbddDestinos.AddItem ""
            sdbddProveedores.AddItem ""
            sdbddProveedoresDen.AddItem ""
            'inicializar las colecciones
            Set oDestinos = Nothing
            
            Screen.MousePointer = vbHourglass
            Set oDestinos = oFSGSRaiz.Generar_CDestinos
            Set oUnidades = Nothing
            Set oUnidades = oFSGSRaiz.Generar_CUnidades
            Set oProveedores = Nothing
            Set oProveedores = oFSGSRaiz.generar_CProveedores
            Screen.MousePointer = vbNormal
        End If
    Else
        'Pasamos al modo lectura
        picNavigate.Visible = False
        If Me.Height > 300 Then Me.Height = Me.Height - 300
        sdbgAdjudicaciones.AllowAddNew = False
        sdbgAdjudicaciones.AllowDelete = False
        sdbgAdjudicaciones.AllowUpdate = False
        bModoEdicion = False
    End If
    
    
    
    Accion = ACCArtAdjCon
    
    
End Sub
Private Sub Form_Resize()
    
    ''' * Objetivo: Adecuar los controles
    
    Arrange
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    ''' * Objetivo: Descargar el formulario si no
    ''' * Objetivo: hay cambios pendientes
    
    Dim v As Variant
        
    If bModoAdjAnteriores Then
    
        If sdbgAdjudicaciones.DataChanged = True Then
    
            v = sdbgAdjudicaciones.ActiveCell.Value
            If Me.Visible Then sdbgAdjudicaciones.SetFocus
            sdbgAdjudicaciones.ActiveCell.Value = v
        
            If (actualizarYSalir() = True) Then
                Cancel = True
                Exit Sub
            End If
        
        End If
    
        Set oAdjudicaciones = Nothing
        Set oAdjudicacionEnEdicion = Nothing
        Set oIBAseDatosEnEdicion = Nothing
        
        Set oUnidades = Nothing
        Set oDestinos = Nothing
        Set oProveedores = Nothing
    
    Else
        
        Set oAdjudicaciones = Nothing
        
    End If
    If sdbgAdjudicaciones.Rows > 0 Then
       oArticulo.ConAdjsAnteriores = True
    Else
       oArticulo.ConAdjsAnteriores = False
    End If

 Set oArticulo = Nothing
 bModoAdjAnteriores = False
 g_bPreAdj = False
End Sub
Private Sub sdbgAdjudicaciones_AfterDelete(RtnDispErrMsg As Integer)

    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    RtnDispErrMsg = 0
    If Me.Visible Then sdbgAdjudicaciones.SetFocus
    sdbgAdjudicaciones.Bookmark = sdbgAdjudicaciones.RowBookmark(sdbgAdjudicaciones.Row)
    
End Sub
Private Sub sdbgAdjudicaciones_AfterInsert(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    RtnDispErrMsg = 0
    If bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If
    
End Sub
Private Sub sdbgAdjudicaciones_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    RtnDispErrMsg = 0
    If bAnyaError = False And bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If
    
End Sub
Private Sub sdbgAdjudicaciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

    ''' * Objetivo: Confirmacion antes de eliminar
    
    Dim irespuesta As Integer
    
    DispPromptMsg = 0
    
    If sdbgAdjudicaciones.IsAddRow Or Accion = ACCArtadjMod Then
        Cancel = True
        Exit Sub
    End If
    
    irespuesta = oMensajes.PreguntaEliminar(sIdiAdj)
    
    If irespuesta = vbNo Then Cancel = True
    
End Sub
Private Sub sdbgAdjudicaciones_BeforeUpdate(Cancel As Integer)

    ''' * Objetivo: Validar los datos
    
    bValError = False
    Cancel = False
    
    If Trim(sdbgAdjudicaciones.Columns("DEST").Value) = "" Then
        oMensajes.NoValido sIdiDest
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgAdjudicaciones.Columns("UNI").Value) = "" Then
        oMensajes.NoValido sIdiUnid
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgAdjudicaciones.Columns("COD").Value) = "" Then
        oMensajes.NoValido sIdiProv
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgAdjudicaciones.Columns("PREC").Value) = "" Then
        oMensajes.NoValido sIdiPrec
        Cancel = True
        GoTo Salir
    Else
        If Not IsNumeric(sdbgAdjudicaciones.Columns("PREC").Value) Then
            oMensajes.NoValido sIdiPrec
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If Trim(sdbgAdjudicaciones.Columns("CANT").Value) = "" Then
        oMensajes.NoValido sIdiCant
        Cancel = True
        GoTo Salir
    Else
        If Not IsNumeric(sdbgAdjudicaciones.Columns("CANT").Value) Then
            oMensajes.NoValido sIdiCant
            Cancel = True
            GoTo Salir
        End If
    End If
   
    If Trim(sdbgAdjudicaciones.Columns("PORC").Value) <> "" Then
        If Not IsNumeric(sdbgAdjudicaciones.Columns("PORC").Value) Then
            oMensajes.NoValido sIdiPorc
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If Trim(sdbgAdjudicaciones.Columns("FECINI").Value) <> "" Then
        If Not IsDate(sdbgAdjudicaciones.Columns("FECINI").Value) Then
            oMensajes.NoValido sIdiFIni
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If Trim(sdbgAdjudicaciones.Columns("FECFIN").Value) <> "" Then
        If Not IsDate(sdbgAdjudicaciones.Columns("FECFIN").Value) Then
            oMensajes.NoValido sIdiFFin
            Cancel = True
            GoTo Salir
        End If
    End If

Salir:
        
    bValError = Cancel
    If Me.Visible Then sdbgAdjudicaciones.SetFocus
        
End Sub
''' <summary>
''' Evento de grid cuando se pulsa algun boton en el grid
''' </summary>
''' <remarks>Llamada desde: Evento de grid; Tiempo m�ximo: instantes</remarks>
Private Sub sdbgAdjudicaciones_BtnClick()
    If sdbgAdjudicaciones.col = -1 Then Exit Sub
    If sdbgAdjudicaciones.Columns(sdbgAdjudicaciones.col).Name = "FECINI" Then
    
        If bModoEdicion Then
        
            Set frmCalendar.frmDestination = frmESTRMATProve
            frmCalendar.sOrigen = "INI"
                
            frmCalendar.addtotop = 900 + 360
            frmCalendar.addtoleft = 180
        
            If sdbgAdjudicaciones.Columns("FECINI").Value <> "" Then
                frmCalendar.Calendar.Value = sdbgAdjudicaciones.Columns("FECINI").Value
            Else
                frmCalendar.Calendar.Value = Date
            End If
        
            frmCalendar.Show 1
            If sdbgAdjudicaciones.DataChanged Then
                sdbgAdjudicaciones_Change
            End If
            If Me.Visible Then sdbgAdjudicaciones.SetFocus
            sdbgAdjudicaciones.col = 7
        End If
    Else
        If sdbgAdjudicaciones.Columns(sdbgAdjudicaciones.col).Name = "FECFIN" Then
    
            If bModoEdicion Then
        
                Set frmCalendar.frmDestination = frmESTRMATProve
                frmCalendar.sOrigen = "FIN"
                
                frmCalendar.addtotop = 900 + 360
                frmCalendar.addtoleft = 180
        
                If sdbgAdjudicaciones.Columns("FECFIN").Value <> "" Then
                    frmCalendar.Calendar.Value = sdbgAdjudicaciones.Columns("FECFIN").Value
                Else
                    frmCalendar.Calendar.Value = Date
                End If
        
                frmCalendar.Show 1
                If sdbgAdjudicaciones.DataChanged Then
                    sdbgAdjudicaciones_Change
                End If
                If Me.Visible Then sdbgAdjudicaciones.SetFocus
                sdbgAdjudicaciones.col = 8
            End If
        End If
    End If
    
End Sub

Private Sub sdbgAdjudicaciones_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
        
    End If
    
    If Accion = ACCArtAdjCon And Not sdbgAdjudicaciones.IsAddRow Then
        If IsNull(sdbgAdjudicaciones.Bookmark) Then Exit Sub
        Set oAdjudicacionEnEdicion = Nothing
        Set oAdjudicacionEnEdicion = oAdjudicaciones.Item(CStr(sdbgAdjudicaciones.Bookmark))
        Set oIBAseDatosEnEdicion = oAdjudicacionEnEdicion
        
        Screen.MousePointer = vbHourglass
        teserror = oIBAseDatosEnEdicion.IniciarEdicion
        Screen.MousePointer = vbNormal
        
        If teserror.NumError = TESInfModificada Then
            
            TratarError teserror
            sdbgAdjudicaciones.DataChanged = False
            teserror.NumError = TESnoerror
            oIBAseDatosEnEdicion.CancelarEdicion
        End If
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            ' Puede que otros usuarios hayan hecho una adjudicacion de summit
            ' por lo que deberemos descargar el formulario
            If teserror.NumError = TESAdjAntDenegada Then
                Accion = ACCArtAdjCon
                Unload Me
                Exit Sub
            End If
        
            If Me.Visible Then sdbgAdjudicaciones.SetFocus
        Else
            Accion = ACCArtadjMod
        End If
            
    End If
    
    If sdbgAdjudicaciones.IsAddRow Then
        If sdbgAdjudicaciones.Columns("DEST").Text = "" Then
            If gParametrosInstalacion.gsDestino <> "" Then
                sdbgAdjudicaciones.Columns("DEST").Value = gParametrosInstalacion.gsDestino
            Else
                If gParametrosGenerales.gsDESTDEF <> "" Then
                    'Registrar como parametro de instalacion
                    gParametrosInstalacion.gsDestino = gParametrosGenerales.gsDESTDEF
                    g_GuardarParametrosIns = True
                    sdbgAdjudicaciones.Columns("DEST").Value = gParametrosGenerales.gsDESTDEF
                End If
            End If
        End If
        
        If sdbgAdjudicaciones.Columns("UNI").Text = "" Then
            sdbgAdjudicaciones.Columns("UNI").Value = gParametrosGenerales.gsUNIDEF
        End If
    End If
    
End Sub

Private Sub sdbgAdjudicaciones_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    If g_sOrigen <> "frmSolicitudDesglose" Then
        Dim sHeadCaption As String
        
        If bModoEdicion Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        sHeadCaption = sdbgAdjudicaciones.Columns(ColIndex).caption
    
        sdbgAdjudicaciones.Columns(ColIndex).caption = sIdiOperacion
        
        ''' Volvemos a cargar las Adjudicaciones, ordenadas segun la columna
        ''' en cuyo encabezado se ha hecho 'clic'.
        
        'Depende en que modo estemos seran las antiguas o las nuevas
        
            Select Case ColIndex
                
                Case 0
                    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorDest, oArticulo, bModoAdjAnteriores, True
                Case 1
                    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorUni, oArticulo, bModoAdjAnteriores, True
                Case 2
                    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorCodProve, oArticulo, bModoAdjAnteriores, True
                Case 3
                    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorDescr, oArticulo, bModoAdjAnteriores, True
                Case 4
                    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorPrec, oArticulo, bModoAdjAnteriores, True
                Case 5
                    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorCant, oArticulo, bModoAdjAnteriores, True
                Case 6
                    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorPorc, oArticulo, bModoAdjAnteriores, True
                Case 7
                    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecIni, oArticulo, bModoAdjAnteriores, True
                Case 8
                    oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, oArticulo, bModoAdjAnteriores, True
    
            End Select
            
        sdbgAdjudicaciones.ReBind
        sdbgAdjudicaciones.Columns(ColIndex).caption = sHeadCaption
    
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub sdbgAdjudicaciones_InitColumnProps()

    sdbgAdjudicaciones.Columns("PREC").caption = sIdiMoneda & " " & IIf(m_bmostrarMonPrecio, gParametrosGenerales.gsMONCEN, "")
    
End Sub


Private Sub sdbgAdjudicaciones_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgAdjudicaciones.DataChanged = False Then
            
            sdbgAdjudicaciones.CancelUpdate
            sdbgAdjudicaciones.DataChanged = False
            
            If Not oAdjudicacionEnEdicion Is Nothing Then
                oIBAseDatosEnEdicion.CancelarEdicion
                Set oIBAseDatosEnEdicion = Nothing
                Set oAdjudicacionEnEdicion = Nothing
            End If
           
            If Not sdbgAdjudicaciones.IsAddRow Then
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
            Else
                cmdA�adir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
            End If
            
            Accion = ACCArtAdjCon
            
        Else
        
            If sdbgAdjudicaciones.IsAddRow Then
           
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
           
                Accion = ACCArtAdjCon
            
            End If
            
        End If
        
    End If
    
    If KeyAscii = vbKeyReturn Or KeyAscii = vbKeySpace Then
        sdbgAdjudicaciones_BtnClick
    End If

End Sub

Private Sub sdbgAdjudicaciones_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgAdjudicaciones.IsAddRow Then
        sdbgAdjudicaciones.Columns("DEST").Locked = True
        If sdbgAdjudicaciones.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
        End If
    Else
    
        sdbgAdjudicaciones.Columns("DEST").Locked = False
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgAdjudicaciones.Row) Then
                sdbgAdjudicaciones.col = 0
            End If
        End If
        
    End If
     
End Sub

Private Sub sdbgAdjudicaciones_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
If NullToDbl0(sdbgAdjudicaciones.Columns("ULTIMO").Value) = 0 Then
    For i = 0 To sdbgAdjudicaciones.Columns.Count - 1
        sdbgAdjudicaciones.Columns(i).CellStyleSet "Gris"
    Next i
End If
End Sub

Private Sub sdbgAdjudicaciones_UnboundAddData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, NewRowBookmark As Variant)

    ''' * Objetivo: Hemos anyadido una fila al grid,
    ''' * Objetivo: ahora hay que anyadirla a la
    ''' * Objetivo: coleccion
    ''' * Recibe: Buffer con los datos a anyadir y bookmark de la fila
        
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    
    bAnyaError = False
    
    ''' Anyadir a la coleccion
    
    If sdbgAdjudicaciones.Columns("COD").Value <> "" Then
        oAdjudicaciones.Add oArticulo, 0, sdbgAdjudicaciones.Columns("DEST").Value, sdbgAdjudicaciones.Columns("UNI").Value, NullToDbl0(StrToDblOrNull(sdbgAdjudicaciones.Columns("PREC").Value)), sdbgAdjudicaciones.Columns("COD").Value, StrToDblOrNull(sdbgAdjudicaciones.Columns("PORC").Value), StrToNull(sdbgAdjudicaciones.Columns("FECINI").Value), StrToNull(sdbgAdjudicaciones.Columns("FECFIN").Value), sdbgAdjudicaciones.Columns("DEN").Value, oAdjudicaciones.Count, sdbgAdjudicaciones.Columns("CANT").Value
    Else
        oAdjudicaciones.Add oArticulo, 0, sdbgAdjudicaciones.Columns("DEST").Value, sdbgAdjudicaciones.Columns("UNI").Value, NullToDbl0(StrToDblOrNull(sdbgAdjudicaciones.Columns("PREC").Value)), , StrToDblOrNull(sdbgAdjudicaciones.Columns("PORC").Value), StrToNull(sdbgAdjudicaciones.Columns("FECINI").Value), StrToNull(sdbgAdjudicaciones.Columns("FECFIN").Value), sdbgAdjudicaciones.Columns("DEN").Value, oAdjudicaciones.Count, sdbgAdjudicaciones.Columns("CANT").Value
    End If
    
    ''' Anyadir a la base de datos
    
    Set oAdjudicacionEnEdicion = oAdjudicaciones.Item(CStr(oAdjudicaciones.Count - 1))
   
    Set oIBAseDatosEnEdicion = oAdjudicacionEnEdicion
    
    Screen.MousePointer = vbHourglass
    teserror = oIBAseDatosEnEdicion.AnyadirABaseDatos
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
            
        v = sdbgAdjudicaciones.ActiveCell.Value
        oAdjudicaciones.Remove (CStr(oAdjudicaciones.Count - 1))
        TratarError teserror
        ' Puede que otros usuarios hayan hecho una adjudicacion de summit
        ' por lo que deberemos descargar el formulario
        If teserror.NumError = TESAdjAntDenegada Then
            Accion = ACCArtAdjCon
            Unload Me
            Exit Sub
        End If
        If Me.Visible Then sdbgAdjudicaciones.SetFocus
        bAnyaError = True
        RowBuf.RowCount = 0
        sdbgAdjudicaciones.ActiveCell.Value = v
        
    Else
        ''' Registro de acciones
        basSeguridad.RegistrarAccion AccionesSummit.ACCArtAdjAnya, "Id:" & oAdjudicacionEnEdicion.Id
        
        Accion = ACCArtAdjCon
        
    End If
    
    Set oIBAseDatosEnEdicion = Nothing
    Set oAdjudicacionEnEdicion = Nothing
        
End Sub
Private Sub sdbgAdjudicaciones_UnboundDeleteRow(Bookmark As Variant)

    ''' * Objetivo: Hemos eliminado una fila al grid,
    ''' * Objetivo: ahora hay que eliminarla de la
    ''' * Objetivo: coleccion
    ''' * Recibe: Bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim IndAdjArt As Long
        
    ''' Eliminamos de la base de datos
    
    Set oAdjudicacionEnEdicion = oAdjudicaciones.Item(CStr(Bookmark))
    
    Set oIBAseDatosEnEdicion = oAdjudicacionEnEdicion
    
    Screen.MousePointer = vbHourglass
    teserror = oIBAseDatosEnEdicion.EliminarDeBaseDatos
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
    
        TratarError teserror
        ' Puede que otros usuarios hayan hecho una adjudicacion de summit
        ' por lo que deberemos descargar el formulario
        If teserror.NumError = TESAdjAntDenegada Then
            Accion = ACCArtAdjCon
            Unload Me
            Exit Sub
        End If
        
        sdbgAdjudicaciones.Refresh
        If Me.Visible Then sdbgAdjudicaciones.SetFocus
        
    Else
    
        ''' Registro de acciones
        
            basSeguridad.RegistrarAccion AccionesSummit.ACCArtAdjEli, "Id:" & oAdjudicacionEnEdicion.Id
            
        ''' Eliminar de la coleccion
        
        IndAdjArt = val(Bookmark)
        
        oAdjudicaciones.BorrarEnModoIndice IndAdjArt
       
        Accion = ACCArtAdjCon
        
    End If
    
    Set oIBAseDatosEnEdicion = Nothing
    Set oAdjudicacionEnEdicion = Nothing
    
End Sub
Private Sub sdbgAdjudicaciones_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Integer
    Dim i As Integer
    Dim j As Integer
    
    Dim oAdjArt As CAdjDeArt
    
    Dim iNumAdjudicaciones As Integer
    
            If oAdjudicaciones Is Nothing Then
                RowBuf.RowCount = 0
                Exit Sub
            End If
        
            iNumAdjudicaciones = oAdjudicaciones.Count
            
            If IsNull(StartLocation) Then       'If the grid is empty then
                If ReadPriorRows Then               'If moving backwards through grid then
                    p = iNumAdjudicaciones - 1                             'pointer = # of last grid row
                Else                                        'else
                    p = 0                                       'pointer = # of first grid row
                End If
            Else                                        'If the grid already has data in it then
                p = StartLocation                       'pointer = location just before or after the row where data will be added
            
                If ReadPriorRows Then               'If moving backwards through grid then
                        p = p - 1                               'move pointer back one row
                Else                                        'else
                        p = p + 1                               'move pointer ahead one row
                End If
            End If
            
            'The pointer (p) now points to the row of the grid where you will start adding data.
            
            For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
                
                If p < 0 Or p > iNumAdjudicaciones - 1 Then Exit For           'If the pointer is outside the grid then stop this
            
                Set oAdjArt = Nothing
                Set oAdjArt = oAdjudicaciones.Item(CStr(p))
            
                For j = 0 To sdbgAdjudicaciones.Columns.Count - 1
              
                    Select Case sdbgAdjudicaciones.Columns(j).Name
                            
                            Case "DEST":
                                    RowBuf.Value(i, j) = oAdjArt.DestCod
                            Case "UNI":
                                    RowBuf.Value(i, j) = oAdjArt.UniCod
                            Case "PROCE":
                                If Not IsMissing(oAdjArt.proceAnyo) Then
                                    RowBuf.Value(i, j) = oAdjArt.proceAnyo & "/" & oAdjArt.procegmn & "/" & oAdjArt.ProceCod
                                End If
                            Case "COD":
                                    RowBuf.Value(i, j) = oAdjArt.ProveCod
                            Case "DEN":
                                    RowBuf.Value(i, j) = oAdjArt.Descr
                            Case "PREC":
                                    RowBuf.Value(i, j) = oAdjArt.Precio
                            Case "MON":
                                    RowBuf.Value(i, j) = oAdjArt.Moneda
                            Case "CANT":
                                    RowBuf.Value(i, j) = oAdjArt.Cantidad
                            Case "PORC":
                                    RowBuf.Value(i, j) = oAdjArt.Porcentaje
                            Case "FECINI":
                                    RowBuf.Value(i, j) = oAdjArt.FechaInicioSuministro
                            Case "FECFIN":
                                    RowBuf.Value(i, j) = oAdjArt.FechaFinSuministro
                            Case "USU"
                                    RowBuf.Value(i, j) = oAdjArt.Usuario
                            Case "FECHA"
                                    RowBuf.Value(i, j) = oAdjArt.Fecha_PreAdj
                            Case "ULTIMO"
                                    RowBuf.Value(i, j) = oAdjArt.Ultimo_PreAdj
                    
                    End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
                Next j
            
                RowBuf.Bookmark(i) = p                              'set the value of the bookmark for the current row in the rowbuffer
            
                If ReadPriorRows Then                               'move the pointer forward or backward, depending
                    p = p - 1                                           'on which way it's supposed to move
                Else
                    p = p + 1
                End If
                    r = r + 1                                               'increment the number of rows read
                Next i
            
            RowBuf.RowCount = r                                         'set the size of the row buffer to the number of rows read
    
            Set oAdjArt = Nothing
    
End Sub
Private Sub sdbgAdjudicaciones_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
    
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim v As Variant
    
    bModError = False
    
    ''' Modificamos en la base de datos
    
    oAdjudicacionEnEdicion.DestCod = sdbgAdjudicaciones.Columns("DEST").Value
    oAdjudicacionEnEdicion.UniCod = sdbgAdjudicaciones.Columns("UNI").Value
    oAdjudicacionEnEdicion.ProveCod = StrToNull(sdbgAdjudicaciones.Columns("COD").Value)
    oAdjudicacionEnEdicion.Descr = StrToNull(sdbgAdjudicaciones.Columns("DEN").Value)
    oAdjudicacionEnEdicion.Precio = sdbgAdjudicaciones.Columns("PREC").Value
    oAdjudicacionEnEdicion.Cantidad = sdbgAdjudicaciones.Columns("CANT").Value
    If Trim(sdbgAdjudicaciones.Columns("PORC").Value) = "" Then
        oAdjudicacionEnEdicion.Porcentaje = 100
    Else
        oAdjudicacionEnEdicion.Porcentaje = StrToNull(sdbgAdjudicaciones.Columns("PORC").Value)
    End If
    
    oAdjudicacionEnEdicion.FechaInicioSuministro = StrToNull(sdbgAdjudicaciones.Columns("FECINI").Value)
    oAdjudicacionEnEdicion.FechaFinSuministro = StrToNull(sdbgAdjudicaciones.Columns("FECFIN").Value)
    
    Screen.MousePointer = vbHourglass
    teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        
        v = sdbgAdjudicaciones.ActiveCell.Value
        TratarError teserror
        ' Puede que otros usuarios hayan hecho una adjudicacion de summit
        ' por lo que deberemos descargar el formulario
        If teserror.NumError = TESAdjAntDenegada Then
            Accion = ACCArtAdjCon
            Unload Me
            Exit Sub
        End If
        
        If Me.Visible Then sdbgAdjudicaciones.SetFocus
        bModError = True
        RowBuf.RowCount = 0
        sdbgAdjudicaciones.ActiveCell.Value = v
        
    Else
        
        ''' Registro de acciones
        
        basSeguridad.RegistrarAccion AccionesSummit.ACCArtAdjAnya, "Id:" & oAdjudicacionEnEdicion.Id
        
        Accion = ACCArtAdjCon
        
        Set oIBAseDatosEnEdicion = Nothing
        Set oAdjudicacionEnEdicion = Nothing
        
    End If
        
End Sub

Private Sub sdbddDestinos_DropDown()

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    
    Screen.MousePointer = vbHourglass
    If bCargarComboDesdeDD Then
        oDestinos.CargarTodosLosDestinos CStr(sdbgAdjudicaciones.ActiveCell.Value), , , , , , , , , , True
    Else
        oDestinos.CargarTodosLosDestinos , , , , , , , , , , True
    End If
    
        
    CargarGridConDestinos
    
    sdbgAdjudicaciones.ActiveCell.SelStart = 0
    sdbgAdjudicaciones.ActiveCell.SelLength = Len(sdbgAdjudicaciones.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbddDestinos_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddDestinos.DataFieldList = "Column 0"
    sdbddDestinos.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddDestinos_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddDestinos.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddDestinos.Rows - 1
            bm = sdbddDestinos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddDestinos.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddDestinos.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddDestinos_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddDestinos.Columns(0).Value = Text Then
            RtnPassed = True
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        oDestinos.CargarTodosLosDestinos Text, , True, , , , , , , , True
        
        If Not oDestinos.Item(1) Is Nothing Then
            RtnPassed = True
        Else
            sdbgAdjudicaciones.Columns("DEST").Value = ""
        End If
        
        Screen.MousePointer = vbNormal
    
    End If
    
End Sub

Private Sub CargarGridConDestinos()

        ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oDest As CDestino
    
    sdbddDestinos.RemoveAll
    
    For Each oDest In oDestinos
        sdbddDestinos.AddItem oDest.Cod & Chr(m_lSeparador) & oDest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oDest.dir & Chr(m_lSeparador) & oDest.POB & Chr(m_lSeparador) & oDest.cP & Chr(m_lSeparador) & oDest.Pais & Chr(m_lSeparador) & oDest.Provi
    Next
    
    If sdbddDestinos.Rows = 0 Then
        sdbddDestinos.AddItem ""
    End If

End Sub

Private Sub sdbddUnidades_DropDown()

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    
    Screen.MousePointer = vbHourglass
    If bCargarComboDesdeDD Then
        oUnidades.CargarTodasLasUnidades CStr(sdbgAdjudicaciones.ActiveCell.Value)
    Else
        oUnidades.CargarTodasLasUnidades
    End If
        
    CargarGridConUnidades
    
    sdbgAdjudicaciones.ActiveCell.SelStart = 0
    sdbgAdjudicaciones.ActiveCell.SelLength = Len(sdbgAdjudicaciones.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbddUnidades_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddUnidades.DataFieldList = "Column 0"
    sdbddUnidades.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddUnidades_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddUnidades.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddUnidades.Rows - 1
            bm = sdbddUnidades.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddUnidades.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddUnidades.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddUnidades_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddUnidades.Columns(0).Value = Text Then
            RtnPassed = True
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        
        oUnidades.CargarTodasLasUnidades Text, , True
        
        If Not oUnidades.Item(1) Is Nothing Then
            RtnPassed = True
        Else
            sdbgAdjudicaciones.Columns("UNI").Value = ""
        End If
        
        Screen.MousePointer = vbNormal
    
    End If
    
End Sub

Private Sub CargarGridConUnidades()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oUni As CUnidad
    
    sdbddUnidades.RemoveAll
    
    For Each oUni In oUnidades
        sdbddUnidades.AddItem oUni.Cod & Chr(m_lSeparador) & oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    Next
    
    If sdbddUnidades.Rows = 0 Then
        sdbddUnidades.AddItem ""
    End If
    
End Sub


Private Sub sdbddProveedores_CloseUp()
    
    If sdbgAdjudicaciones.Columns("COD").Text = "" Then Exit Sub
    
    If sdbddProveedores.Columns(0).Value <> "" Then
        sdbgAdjudicaciones_Change
        sdbgAdjudicaciones.Columns("DEN").Value = sdbddProveedores.Columns(1).Value
        
    End If
    
End Sub

Private Sub sdbddProveedores_DropDown()

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    
    Screen.MousePointer = vbHourglass
    
    oProveedores.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, sdbgAdjudicaciones.ActiveCell.Value, , , False
    CargarGridConProveedores
  
    sdbgAdjudicaciones.ActiveCell.SelStart = 0
    sdbgAdjudicaciones.ActiveCell.SelLength = Len(sdbgAdjudicaciones.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbddProveedores_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveedores.DataFieldList = "Column 0"
    sdbddProveedores.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddProveedores_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveedores.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveedores.Rows - 1
            bm = sdbddProveedores.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveedores.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveedores.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddProveedores_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        sdbgAdjudicaciones.Columns("DEN").Text = ""
        RtnPassed = True
        
    Else
        
        ''' Comprobar la existencia en la lista
        If sdbddProveedores.Columns(0).Value = Text Then
            RtnPassed = True
            sdbgAdjudicaciones.Columns("DEN").Value = sdbddProveedores.Columns(1).Value
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        oProveedores.CargarTodosLosProveedoresDesde3 1, Text
        
        If Not oProveedores.Item(1) Is Nothing Then
            If UCase(oProveedores.Item(1).Cod) = UCase(Text) Then
                RtnPassed = True
                sdbgAdjudicaciones.Columns("DEN").Value = oProveedores.Item(1).Den
            Else
                sdbgAdjudicaciones.Columns("COD").Value = ""
                sdbgAdjudicaciones.Columns("DEN").Text = ""
            End If
        Else
            sdbgAdjudicaciones.Columns("COD").Value = ""
            sdbgAdjudicaciones.Columns("DEN").Text = ""
        End If
        
        Screen.MousePointer = vbNormal
    
    End If
    
End Sub

Private Sub CargarGridConProveedores()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oProve As CProveedor
    
    sdbddProveedores.RemoveAll
    
    For Each oProve In oProveedores
        sdbddProveedores.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den
    Next
    
    If sdbddProveedores.Rows = 0 Then
        sdbddProveedores.AddItem ""
    End If
    
    'sdbddProveedores.Columns(0).Value
End Sub


Private Sub sdbddProveedoresDen_CloseUp()
    If sdbgAdjudicaciones.Columns("DEN").Text = "" Then Exit Sub
    
    If sdbddProveedoresDen.Columns(0).Value <> "" Then
        sdbgAdjudicaciones_Change
        DoEvents
        sdbgAdjudicaciones.Columns("COD").Value = sdbddProveedoresDen.Columns(1).Value
    End If
    
End Sub

Private Sub sdbddProveedoresDen_DropDown()

    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    
    Screen.MousePointer = vbHourglass
    oProveedores.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , sdbgAdjudicaciones.ActiveCell.Value, False, True
   
    CargarGridConProveedoresDen
    
    sdbgAdjudicaciones.ActiveCell.SelStart = 0
    sdbgAdjudicaciones.ActiveCell.SelLength = Len(sdbgAdjudicaciones.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbddProveedoresDen_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddProveedoresDen.DataFieldList = "Column 0"
    sdbddProveedoresDen.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddProveedoresDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProveedoresDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProveedoresDen.Rows - 1
            bm = sdbddProveedoresDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProveedoresDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProveedoresDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddProveedoresDen_ValidateList(Text As String, RtnPassed As Integer)
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        sdbgAdjudicaciones.Columns("COD").Text = ""
        RtnPassed = True

    Else
        
        ''' Comprobar la existencia en la lista
        If sdbddProveedoresDen.Columns(0).Value = Text Then
            RtnPassed = True
            sdbgAdjudicaciones.Columns("COD").Value = sdbddProveedoresDen.Columns(1).Value
            Exit Sub
        End If
                
        Screen.MousePointer = vbHourglass
        oProveedores.CargarTodosLosProveedoresDesde3 1, , Text, True
        If Not oProveedores.Item(1) Is Nothing Then
            If UCase(Text) = UCase(oProveedores.Item(1).Den) Then
                RtnPassed = True
                sdbgAdjudicaciones.Columns("COD").Value = oProveedores.Item(1).Cod
            Else
                sdbgAdjudicaciones.Columns("COD").Value = ""
                sdbgAdjudicaciones.Columns("DEN").Value = ""
            End If
        Else
            sdbgAdjudicaciones.Columns("COD").Value = ""
            sdbgAdjudicaciones.Columns("DEN").Value = ""
        End If
        
        Screen.MousePointer = vbNormal
        
    End If
    
End Sub

Private Sub CargarGridConProveedoresDen()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oProve As CProveedor
    
    sdbddProveedoresDen.RemoveAll
    
    For Each oProve In oProveedores
        sdbddProveedoresDen.AddItem oProve.Den & Chr(m_lSeparador) & oProve.Cod
    Next
    
    If sdbddProveedoresDen.Rows = 0 Then
        sdbddProveedoresDen.AddItem ""
    End If
    
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRMAT_PROVE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    sIdiTitulos(1) = Ador(0).Value
    Ador.MoveNext
    sIdiTitulos(2) = Ador(0).Value
    caption = sIdiTitulos(1)
    Ador.MoveNext
    
    sdbgAdjudicaciones.Columns("DEST").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("UNI").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("COD").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("DEN").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("PREC").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("PORC").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("FECINI").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("FECFIN").caption = Ador(0).Value
    Ador.MoveNext
    sdbddDestinos.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbddDestinos.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    sdbddDestinos.Columns(2).caption = Ador(0).Value
    Ador.MoveNext
    sdbddDestinos.Columns(3).caption = Ador(0).Value
    Ador.MoveNext
    sdbddDestinos.Columns(4).caption = Ador(0).Value
    Ador.MoveNext
    sdbddDestinos.Columns(5).caption = Ador(0).Value
    Ador.MoveNext
    sdbddDestinos.Columns(6).caption = Ador(0).Value
    Ador.MoveNext
    sdbddUnidades.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbddUnidades.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    sdbddProveedores.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbddProveedores.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    sdbddProveedoresDen.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbddProveedoresDen.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    cmdA�adir.caption = Ador(0).Value
    Ador.MoveNext
    cmdDeshacer.caption = Ador(0).Value
    Ador.MoveNext
    cmdEliminar.caption = Ador(0).Value
    Ador.MoveNext
    cmdListado.caption = Ador(0).Value
    Ador.MoveNext
    sIdiModos(1) = Ador(0).Value
    Ador.MoveNext
    sIdiModos(2) = Ador(0).Value
    Ador.MoveNext
    cmdModoEdicion.caption = sIdiModos(1)
    
    cmdRestaurar.caption = Ador(0).Value
    Ador.MoveNext
    sIdiOperacion = Ador(0).Value
    Ador.MoveNext
    sIdiMoneda = Ador(0).Value
    Ador.MoveNext
    sIdiAdj = Ador(0).Value
    Ador.MoveNext
    sIdiDest = Ador(0).Value
    Ador.MoveNext
    sIdiUnid = Ador(0).Value
    Ador.MoveNext
    sIdiProv = Ador(0).Value
    Ador.MoveNext
    sIdiPrec = Ador(0).Value
    Ador.MoveNext
    sIdiPorc = Ador(0).Value
    Ador.MoveNext
    sIdiFIni = Ador(0).Value
    Ador.MoveNext
    sIdiFFin = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("CANT").caption = Ador(0).Value
    sIdiCant = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("MON").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("PROCE").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("FECHA").caption = Ador(0).Value
    Ador.MoveNext
    sdbgAdjudicaciones.Columns("USU").caption = Ador(0).Value
    Ador.Close
    
    End If

    Set Ador = Nothing

End Sub

Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    If sdbgAdjudicaciones.DataChanged = True Then
        bValError = False
        bAnyaError = False
        bModError = False
        If sdbgAdjudicaciones.Row = 0 Then
            sdbgAdjudicaciones.MoveNext
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgAdjudicaciones.MovePrevious
            End If
        Else
            sdbgAdjudicaciones.MovePrevious
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgAdjudicaciones.MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
End Function



Public Property Get mostrarAdjudicacionesLineaSolicitud() As Boolean

    mostrarAdjudicacionesLineaSolicitud = m_bmostrarAdjudicacionesLineaSolicitud

End Property

Public Property Let mostrarAdjudicacionesLineaSolicitud(ByVal bmostrarAdjudicacionesLineaSolicitud As Boolean)

    m_bmostrarAdjudicacionesLineaSolicitud = bmostrarAdjudicacionesLineaSolicitud

End Property

Private Sub ConfigurarVisibilidadColumnas()
    If Me.mostrarAdjudicacionesLineaSolicitud Then
        sdbgAdjudicaciones.Columns("DEST").Visible = False
        sdbgAdjudicaciones.Columns("UNI").Visible = False
        sdbgAdjudicaciones.Columns("PROCE").Visible = True
        sdbgAdjudicaciones.Columns("MON").Visible = True
        sdbgAdjudicaciones.Columns("FECHA").Visible = g_bPreAdj
        sdbgAdjudicaciones.Columns("USU").Visible = g_bPreAdj
        If g_bPreAdj Then
            sdbgAdjudicaciones.Columns("FECHA").Position = 0
            sdbgAdjudicaciones.Columns("USU").Position = 1
        End If
    Else
        sdbgAdjudicaciones.Columns("DEST").Visible = True
        sdbgAdjudicaciones.Columns("UNI").Visible = True
        sdbgAdjudicaciones.Columns("PROCE").Visible = False
        sdbgAdjudicaciones.Columns("MON").Visible = False
        sdbgAdjudicaciones.Columns("FECHA").Visible = False
        sdbgAdjudicaciones.Columns("USU").Visible = False
    End If
End Sub

Public Property Get mostrarMonPrecio() As Boolean

    mostrarMonPrecio = m_bmostrarMonPrecio

End Property

Public Property Let mostrarMonPrecio(ByVal bmostrarMonPrecio As Boolean)

    m_bmostrarMonPrecio = bmostrarMonPrecio

End Property
