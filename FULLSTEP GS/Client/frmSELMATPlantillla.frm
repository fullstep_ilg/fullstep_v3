VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELMATPlantilla 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   5940
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5415
   Icon            =   "frmSELMATPlantillla.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5940
   ScaleWidth      =   5415
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2625
      TabIndex        =   1
      Top             =   5475
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   0
      Top             =   5475
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwEstrMat 
      Height          =   4335
      Left            =   120
      TabIndex        =   2
      Top             =   1035
      Width           =   4995
      _ExtentX        =   8811
      _ExtentY        =   7646
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwestrmatplantilla 
      Height          =   4335
      Left            =   330
      TabIndex        =   6
      Top             =   1440
      Visible         =   0   'False
      Width           =   4995
      _ExtentX        =   8811
      _ExtentY        =   7646
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":0CB2
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":1106
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":1218
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":132A
            Key             =   "Raiz2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":177C
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":182C
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":18EE
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":199E
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":1A60
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":1B10
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":1BD2
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMATPlantillla.frx":1C9A
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label3 
      BackColor       =   &H00808000&
      Caption         =   "Seleccione el material en el que desea ubicar el art�culo:"
      ForeColor       =   &H80000014&
      Height          =   300
      Left            =   90
      TabIndex        =   5
      Top             =   660
      Width           =   4965
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Se dispone a dar alta del art�culo en la estructura de materiales."
      ForeColor       =   &H80000014&
      Height          =   225
      Left            =   90
      TabIndex        =   4
      Top             =   300
      Width           =   4995
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Ha introducido un c�digo de articulo inexistente."
      ForeColor       =   &H80000014&
      Height          =   210
      Left            =   90
      TabIndex        =   3
      Top             =   90
      Width           =   4950
   End
End
Attribute VB_Name = "frmSELMATPlantilla"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public oActsN1 As CActividadesNivel1
Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4

Public oProcesoSeleccionado As CProceso
Public bPermProcMultiMaterial As Boolean
Public oFrm As Form ' para asignar formulario llamador

Private sIdioma() As String
' Variables de restricciones
Public bRComprador As Boolean

' Variables para interactuar con otros forms
Public sOrigen As String

Public bRCompResponsable As Boolean
Public bRUsuAper As Boolean

' Variable de control de flujo
Public Accion As accionessummit

Private oPlantillas As CPlantillas
Public oPlantillaSeleccionada As CPlantilla
Public IDPlantilla As Integer
Private restriccion As Boolean
Private oCEstructura As CAEstructuraMat
'permisos sobre asignacion de materiales
Public bRestringirSoloMaterialesComprador As Boolean

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub cmdAceptar_Click()
Dim node As MSComctlLib.node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set node = tvwEstrMat.selectedItem
    
    If node Is Nothing Then Exit Sub
    
        If node.Tag = "Raiz" Then
    
        Screen.MousePointer = vbHourglass

        Screen.MousePointer = vbNormal
    
        Exit Sub
        
    End If

    
    Screen.MousePointer = vbHourglass
    Select Case Left(node.Tag, 4)
                
            Case "GMN1"
                        If sOrigen = "frmProce" Or sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulos" Or sOrigen = "frmPROCEModifArtNoCodif" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmSOLAbrirProc" Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneMatNivel4
                            Exit Sub
                        End If
                        
                        Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                        oGMN1Seleccionado.Cod = DevolverCod(node)
                        oGMN1Seleccionado.Den = Mid$(node.Text, Len(DevolverCod(node)) + 4, Len(node.Text) - Len(DevolverCod(node)))
                            
            Case "GMN2"
                        If sOrigen = "frmProce" Or sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulos" Or sOrigen = "frmPROCEModifArtNoCodif" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmSOLAbrirProc" Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneMatNivel4
                            Exit Sub
                        End If
                            
                        Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                        oGMN1Seleccionado.Cod = DevolverCod(node.Parent)
                        oGMN1Seleccionado.Den = Mid$(node.Parent.Text, Len(CStr(DevolverCod(node.Parent))) + 4, Len(node.Parent.Text) - Len(CStr(DevolverCod(node.Parent))))
                        
                        Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                        oGMN2Seleccionado.GMN1Cod = DevolverCod(node.Parent)
                        oGMN2Seleccionado.Cod = DevolverCod(node)
                        oGMN2Seleccionado.Den = Mid$(node.Text, Len(CStr(DevolverCod(node))) + 4, Len(node.Text) - Len(CStr(DevolverCod(node))))
                        oGMN2Seleccionado.GMN1Cod = DevolverCod(node.Parent)
                        
                        
            Case "GMN3"
                        If sOrigen = "frmProce" Or sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulos" Or sOrigen = "frmPROCEModifArtNoCodif" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmSOLAbrirProc" Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneMatNivel4
                            Exit Sub
                        End If
                        
                        Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                        oGMN1Seleccionado.Cod = DevolverCod(node.Parent.Parent)
                        oGMN1Seleccionado.Den = Mid$(node.Parent.Parent.Text, Len(CStr(DevolverCod(node.Parent.Parent))) + 4, Len(node.Parent.Parent.Text) - Len(CStr(DevolverCod(node.Parent.Parent))))
                        
                        Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                        oGMN2Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent)
                        oGMN2Seleccionado.Cod = DevolverCod(node.Parent)
                        oGMN2Seleccionado.Den = Mid$(node.Parent.Text, Len(CStr(DevolverCod(node.Parent))) + 4, Len(node.Parent.Text) - Len(CStr(DevolverCod(node.Parent))))
                        
                        Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
                        oGMN3Seleccionado.Cod = DevolverCod(node)
                        oGMN3Seleccionado.Den = Mid$(node.Text, Len(CStr(DevolverCod(node))) + 4, Len(node.Text) - Len(CStr(DevolverCod(node))))
                        oGMN3Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent)
                        oGMN3Seleccionado.GMN2Cod = DevolverCod(node.Parent)
                        
            Case "GMN4"
                            
                        Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                        oGMN1Seleccionado.Cod = DevolverCod(node.Parent.Parent.Parent)
                        oGMN1Seleccionado.Den = Mid$(node.Parent.Parent.Parent.Text, Len(CStr(DevolverCod(node.Parent.Parent.Parent))) + 4, Len(node.Parent.Parent.Parent.Text) - Len(CStr(DevolverCod(node.Parent.Parent.Parent))))
                        
                        Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                        oGMN2Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent.Parent)
                        oGMN2Seleccionado.Cod = DevolverCod(node.Parent.Parent)
                        oGMN2Seleccionado.Den = Mid$(node.Parent.Parent.Text, Len(CStr(DevolverCod(node.Parent.Parent))) + 4, Len(node.Parent.Parent.Text) - Len(CStr(DevolverCod(node.Parent.Parent))))
                        
                        Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
                        oGMN3Seleccionado.Cod = DevolverCod(node.Parent)
                        oGMN3Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent.Parent)
                        oGMN3Seleccionado.GMN2Cod = DevolverCod(node.Parent.Parent)
                        oGMN3Seleccionado.Den = Mid$(node.Parent.Text, Len(CStr(DevolverCod(node.Parent))) + 4, Len(node.Parent.Text) - Len(CStr(DevolverCod(node.Parent))))
                            
                        Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
                        
                        oGMN4Seleccionado.Cod = DevolverCod(node)
                        oGMN4Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent.Parent)
                        oGMN4Seleccionado.GMN2Cod = DevolverCod(node.Parent.Parent)
                        oGMN4Seleccionado.GMN3Cod = DevolverCod(node.Parent)
                        oGMN4Seleccionado.Den = Mid$(node.Text, Len(CStr(DevolverCod(node))) + 4, Len(node.Text) - Len(CStr(DevolverCod(node))))
                        
                                                
    End Select

    Screen.MousePointer = vbNormal

    
    Select Case sOrigen

        Case "frmProce"

            frmPROCE.PonerMatSeleccionadoEnCombosNew oGMN1Seleccionado, oGMN2Seleccionado, oGMN3Seleccionado, oGMN4Seleccionado
        
        Case "frmPROCEAltaArticulosNoCodif"
            frmPROCE.ActualizarMaterialArticuloNoCodif oGMN4Seleccionado.GMN1Cod, oGMN4Seleccionado.GMN2Cod, oGMN4Seleccionado.GMN3Cod, oGMN4Seleccionado.Cod, oGMN4Seleccionado.Den
            
        Case "frmPROCEAltaArticulos"
            frmPROCE.ActualizarMaterialArticuloNoCodif oGMN4Seleccionado.GMN1Cod, oGMN4Seleccionado.GMN2Cod, oGMN4Seleccionado.GMN3Cod, oGMN4Seleccionado.Cod
             
        Case "frmPROCEModifArtNoCodif"
            frmPROCE.ActualizarMaterialArticuloNoCodif oGMN4Seleccionado.GMN1Cod, oGMN4Seleccionado.GMN2Cod, oGMN4Seleccionado.GMN3Cod, oGMN4Seleccionado.Cod, oGMN4Seleccionado.Den
        
        Case "frmPROCEBuscarArticulo"
            frmPROCEBuscarArticulo.PonerMatSeleccionadoNew oGMN4Seleccionado
        
        Case "frmSOLAbrirProc"
            frmSOLAbrirProc.PonerMatSeleccionadoNew oGMN4Seleccionado
            
        Case Else
            oFrm.PonerMatSeleccionadoEnCombos oGMN1Seleccionado, oGMN2Seleccionado, oGMN3Seleccionado, oGMN4Seleccionado
    
    End Select
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   Set frmPROCE.g_oGMN4Seleccionado = Nothing
   Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELMAT, basPublic.gParametrosInstalacion.gIdioma)
If Not Ador Is Nothing Then
    ReDim sIdioma(1 To 17)
    For i = 1 To 17
        sIdioma(i) = Ador(0).Value
        Ador.MoveNext
    Next
    Me.caption = sIdioma(3)
    cmdAceptar.caption = sIdioma(8)
    cmdCancelar.caption = sIdioma(9)
End If
Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub


Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "Form_Activate", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()

    Dim sCaption As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    CargarRecursos

    
    Me.Height = 5385
    Me.Width = 5355
    
    Label1.Visible = False
    Label2.Visible = False
    Label3.Visible = False
    cmdAceptar.Top = 315
    cmdCancelar.Top = 315
    cmdCancelar.Visible = True
    cmdAceptar.Visible = True
    tvwEstrMat.Top = 120
    
    If sOrigen = "frmPROCEMostrarMateriales" Then
        Me.caption = sIdioma(15)
        cmdAceptar.Visible = False
        cmdCancelar.Left = (Me.Width / 2) - (cmdCancelar.Width / 2)
    Else
        Me.caption = sIdioma(3)
    End If
    
    Me.caption = sIdioma(3)
    sCaption = Me.caption
    
    Me.caption = sIdioma(4)
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    DoEvents
    
    tvwEstrMat.Scroll = False
    
    If sOrigen = "frmPROCEMostrarMateriales" Then
        MostrarMateriales
    ElseIf sOrigen = "frmProce" Then
        GenerarEstructuraMatProceAper
    ElseIf sOrigen = "frmPROCEModifArtNoCodif" Then
        cmdCancelar.Visible = False
        GenerarEstructuraMatModifNoCodif False
    ElseIf sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Then
        GenerarEstructuraMatModifNoCodif False
    Else
        CargarPlantilla
        GenerarEstructuraMaterialesPlantilla
    End If

    Screen.MousePointer = vbNormal
    
    tvwEstrMat.Scroll = True
    
    Me.caption = sCaption
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub Form_Resize()
        
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 1200 Or Me.Height > 12500 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    If sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmPROCEModifArtNoCodif" Then
        Me.Height = 6000
        tvwEstrMat.Height = Me.Height - cmdAceptar.Height - 1030
    Else
        If sOrigen <> "frmPROCEAltaArticulos" Then
            tvwEstrMat.Height = Me.Height - 1030
        Else
            tvwEstrMat.Height = Me.Height - 2000
        End If
    End If
    tvwEstrMat.Width = Me.Width - 360
    
    cmdAceptar.Top = Me.Height - 825
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = (tvwEstrMat.Width / 2) - (cmdAceptar.Width / 2) - 300
    If sOrigen = "frmPROCEMostrarMateriales" Then
        cmdCancelar.Left = (Me.Width / 2) - (cmdCancelar.Width / 2)
    Else
        cmdCancelar.Left = (tvwEstrMat.Width / 2) + 300
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Set oGMN4Seleccionado = Nothing
If m_bDescargarFrm = False Then
    If sOrigen = "frmPROCEModifArtNoCodif" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Then
        cmdAceptar_Click
        If oGMN4Seleccionado Is Nothing Then
            Cancel = True
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
End If
      m_bDescargarFrm = False
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Set oActsN1 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If node Is Nothing Then Exit Function

DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If node Is Nothing Then Exit Function

DevolverId = val(Right(node.Tag, Len(node.Tag) - 4))
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "DevolverId", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub CargarPlantilla()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oPlantillaSeleccionada Is Nothing Then
        ' la plantilla ya se instanci�
        Exit Sub
    End If
    
    ' la plantilla est� pendiente de instanciar
    If Not IsNull(IDPlantilla) Then
        Set oPlantillas = oFSGSRaiz.Generar_CPlantillas
        oPlantillas.CargarTodasLasPlantillas IDPlantilla, False
        Set oPlantillaSeleccionada = oPlantillas.Item(CStr(IDPlantilla))
        If oPlantillaSeleccionada.ExisteRestriccionMaterial Then
            restriccion = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "CargarPlantilla", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub GenerarEstructuraMaterialesPlantilla()


    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oCEstructura Is Nothing Then
        Set oCEstructura = New CAEstructuraMat
        oCEstructura.sTituloArbol = sIdioma(5)  ' "Material"
    End If

    If sOrigen = "frmProce" Or sOrigen = "frmPROCEBuscarArticulo" Or Left(sOrigen, 9) = "PLANTILLA" Then
        ' generar todos los niveles
        Select Case bRestringirSoloMaterialesComprador
        Case True
            If oPlantillaSeleccionada.ExisteRestriccionMaterial Then
                oCEstructura.GenerarEstructuraDeComprador oUsuarioSummit.comprador, tvwestrmatplantilla
                oCEstructura.GenerarEstructuraDePlantilla oPlantillaSeleccionada, tvwEstrMat
                oCEstructura.Desmarcar tvwEstrMat
                oCEstructura.TrasladarMarcas tvwestrmatplantilla, tvwEstrMat, True
                oCEstructura.EliminarNodos tvwEstrMat, False ' quitar nodos sin marcar
                oCEstructura.Desmarcar tvwEstrMat
            Else
                oCEstructura.GenerarEstructuraDeComprador oUsuarioSummit.comprador, tvwEstrMat
            End If
        Case False
            oCEstructura.GenerarEstructuraDePlantilla oPlantillaSeleccionada, tvwEstrMat
        End Select
    Else
        ' generar solo nivel 4
    
        oCEstructura.GenerarEstructuraDeUltimoNivel oPlantillaSeleccionada, tvwEstrMat
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "GenerarEstructuraMaterialesPlantilla", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub MostrarMateriales()
    Dim nodx As MSComctlLib.node
    Dim oGrupsMN1 As CGruposMatNivel1
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    
    'Recorrer el arbol y marcar cada rama de material seleccionada en el proceso y dejarla expandida
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm
    
    oProcesoSeleccionado.MaterialProce.DevolverEstructuraDeMateriales
    
    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(5), "Raiz")
    nodx.Selected = True
    
    Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    
    oGrupsMN1.GenerarEstructuraMateriales False
      Dim j As Integer
    For Each oGMN1 In oGrupsMN1
        For j = 1 To oProcesoSeleccionado.MaterialProce.Count
            If oProcesoSeleccionado.MaterialProce.Item(j).GMN1Cod = oGMN1.Cod Then
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                For Each oGMN2 In oGMN1.GruposMatNivel2
                    If oProcesoSeleccionado.MaterialProce.Item(j).GMN2Cod = oGMN2.Cod Then
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            If oProcesoSeleccionado.MaterialProce.Item(j).GMN3Cod = oGMN3.Cod Then
                                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                                For Each oGMN4 In oGMN3.GruposMatNivel4
                                    If oProcesoSeleccionado.MaterialProce.Item(j).Cod = oGMN4.Cod Then
                                        scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                        
                                        Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                                        nodx.Tag = "GMN1" & oGMN1.Cod
                                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                        nodx.Tag = "GMN2" & oGMN2.Cod
                                        Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                                        nodx.Tag = "GMN3" & oGMN3.Cod
                                        Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                                        nodx.Tag = "GMN4" & oGMN4.Cod
                                        nodx.Image = "GMN4A"
                                        nodx.Expanded = True
                                        nodx.Parent.Expanded = True
                                        nodx.Parent.Parent.Expanded = True
                                        nodx.Parent.Parent.Parent.Expanded = True
                                        nodx.Parent.Parent.Parent.Parent.Expanded = True
                                        
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next
    Next
        
ERROR_Frm:
    Set nodx = Nothing
    Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "MostrarMateriales", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If

End Sub

Private Sub GenerarEstructuraMatProceAper()

Dim oGrupsMN1 As CGruposMatNivel1

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim oIMaterialComprador As IMaterialAsignado

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
tvwEstrMat.Nodes.clear

Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(5), "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True
nodx.Selected = True

    
    If bRComprador Then
        
        Set oIMaterialComprador = oUsuarioSummit.comprador
        Set oGrupsMN1 = oIMaterialComprador.DevolverEstructuraMat(oPlantillaSeleccionada.Id)
    Else
        Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        oGrupsMN1.GenerarEstructuraMateriales False, oPlantillaSeleccionada.Id
    End If
    
        
    Select Case gParametrosGenerales.giNEM
        
        Case 1 'Solo un nivel de material
                                
                    scod1 = frmPROCE.sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(frmPROCE.sdbcGMN1_4Cod.Text))
                    Set oGMN1 = oGrupsMN1.Item(scod1)
                    If oGMN1 Is Nothing Then
                        oMensajes.DatoEliminado sIdioma(6)
                        Set oGrupsMN1 = Nothing
'                        Unload Me
                        Exit Sub
                    End If
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                                    
        Case 2 'Dos niveles de materiales
            
                scod1 = frmPROCE.sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(frmPROCE.sdbcGMN1_4Cod.Text))
                Set oGMN1 = oGrupsMN1.Item(scod1)
                
                If oGMN1 Is Nothing Then
                    oMensajes.DatoEliminado sIdioma(6)
                    Set oGrupsMN1 = Nothing
                    Exit Sub
                Else
                
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                    
                    For Each oGMN2 In oGMN1.GruposMatNivel2
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN2.Cod
                        
                    Next
                End If
        
        Case 3 'Hay tres niveles de materiales
                        
                scod1 = frmPROCE.sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(frmPROCE.sdbcGMN1_4Cod.Text))
                Set oGMN1 = oGrupsMN1.Item(scod1)
                
                If oGMN1 Is Nothing Then
                    oMensajes.DatoEliminado sIdioma(6)
                    Set oGrupsMN1 = Nothing
                    Exit Sub
                Else
                
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                    
                    For Each oGMN2 In oGMN1.GruposMatNivel2
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN2.Cod
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                            nodx.Tag = "GMN3" & oGMN3.Cod
                        Next
                    Next
                End If
        
        Case 4 'Cuatro niveles de materiales
                
                scod1 = frmPROCE.sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(frmPROCE.sdbcGMN1_4Cod.Text))
                Set oGMN1 = oGrupsMN1.Item(scod1)
                
                If oGMN1 Is Nothing Then
                    oMensajes.DatoEliminado sIdioma(6)
                    Set oGrupsMN1 = Nothing
'                    Unload Me
                    Exit Sub
                Else
                
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                    
                    For Each oGMN2 In oGMN1.GruposMatNivel2
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN2.Cod
                        
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                            nodx.Tag = "GMN3" & oGMN3.Cod
                            
                            For Each oGMN4 In oGMN3.GruposMatNivel4
                                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                                nodx.Tag = "GMN4" & oGMN4.Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                                If Not frmPROCE.g_oGMN4Seleccionado Is Nothing Then
                                    'Material seleccionado en la apertura
                                    If frmPROCE.lblMat.caption <> "" Then
                                        If oGMN4.Cod = frmPROCE.g_oGMN4Seleccionado.Cod And oGMN4.GMN3Cod = frmPROCE.g_oGMN4Seleccionado.GMN3Cod And oGMN4.GMN2Cod = frmPROCE.g_oGMN4Seleccionado.GMN2Cod And oGMN4.GMN1Cod = frmPROCE.g_oGMN4Seleccionado.GMN1Cod Then
                                            nodx.Parent.Parent.Parent.Expanded = True
                                            nodx.Parent.Parent.Expanded = True
                                            nodx.Parent.Expanded = True
                                            nodx.Selected = True
                                        End If
                                    End If
                                End If
                            Next
                        Next
                    Next
                     'Si solo tiene un material  lo mostramos expandido
                    If tvwEstrMat.Nodes.Count = 5 Then
                        tvwEstrMat.Nodes(1).Expanded = True
                        tvwEstrMat.Nodes(1).Child.Expanded = True
                        tvwEstrMat.Nodes(1).Child.Child.Expanded = True
                        tvwEstrMat.Nodes(1).Child.Child.Child.Expanded = True
                        Set tvwEstrMat.selectedItem = tvwEstrMat.Nodes(1).Child.Child.Child.Child
                    End If
              End If
                        
    End Select
    
    Screen.MousePointer = vbNormal
    
    Set oGrupsMN1 = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "GenerarEstructuraMatProceAper", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Genera la estructura de materiales</summary>
''' <param name="bOrdenadoPorDen">Ordenado por denominaci�n<param>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub GenerarEstructuraMatModifNoCodif(ByVal bOrdenadoPorDen As Boolean)

Dim oGrupsMN1 As CGruposMatNivel1
Dim oGrupsMN4 As CGruposMatNivel4

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim oIMaterialComprador As IMaterialAsignado

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Dim nodx As MSComctlLib.node
Dim sImage As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then
   Exit Sub
End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
tvwEstrMat.Nodes.clear

Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(5), "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True
nodx.Selected = True

If bRComprador Then
    
    Set oIMaterialComprador = oUsuarioSummit.comprador
 
    With oIMaterialComprador

        '************** GruposMN4 ***********

        Set oGrupsMN4 = oIMaterialComprador.DevolverGruposMN4Asignados(, , , bOrdenadoPorDen, False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, oPlantillaSeleccionada.Id)

        'Solo control de errores en bbdd, los nodos SIEMPRE cascan si coinciden el 1ro y el 2do en el GMN1/2/3. Pq el 1ro ya ha metido el nodo GMN1/2/3 y el 2do intenta meterlo de nuevo.
        'Ejemplo:GMN1   GMN2    GMN3    COD DEN_ENG
        '        CGE    100     100     100 HARDWARE/STORAGE CABINS HARDWARE
        '        CGE    100     100     110 HARDWARE/NETWORK ELECTRONIC EQUIPMENT

        If Not oGrupsMN4 Is Nothing Then
            If sOrigen = "frmPROCEModifArtNoCodif" Or sOrigen = "frmPROCEBuscarArticulo" Then
                sImage = "GMN4"
            Else
                sImage = "GMN4A"
            End If

            For Each oGMN4 In oGrupsMN4

                scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                
                If Not ExisteNodo("GMN1" & scod1) Then
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN4.GMN1Cod & " - " & oGMN4.GMN1Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN4.GMN1Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                End If
                
                If Not ExisteNodo("GMN2" & scod1 & scod2) Then
                    Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN4.GMN2Cod & " - " & oGMN4.GMN2Den, "GMN2")
                    nodx.Tag = "GMN2" & oGMN4.GMN2Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                End If
                
                If Not ExisteNodo("GMN3" & scod1 & scod2 & scod3) Then
                    Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN4.GMN3Cod & " - " & oGMN4.GMN3Den, "GMN3")
                    nodx.Tag = "GMN3" & oGMN4.GMN3Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                End If
                
                If sOrigen <> "frmPRESPorMat" Then
                    Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, sImage)
                    nodx.Tag = "GMN4" & oGMN4.Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    If sOrigen = "frmSOLAbrirProc" Or sOrigen = "frmPROCEAltaArticulos" Or sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Then
                        nodx.Parent.Parent.Parent.Expanded = False
                        nodx.Parent.Parent.Expanded = False
                        nodx.Parent.Expanded = False
                    Else
                        If oGMN4.GMN1Cod = oGMN4Seleccionado.GMN1Cod And oGMN4.GMN2Cod = oGMN4Seleccionado.GMN2Cod And oGMN4.GMN3Cod = oGMN4Seleccionado.GMN3Cod And oGMN4.Cod = oGMN4Seleccionado.Cod Then
                            nodx.Parent.Parent.Parent.Expanded = True
                            nodx.Parent.Parent.Expanded = True
                            nodx.Parent.Expanded = True
                            nodx.Selected = True
                        End If
                    End If
                End If
            Next
             'Si solo tiene un material  lo mostramos expandido
            If tvwEstrMat.Nodes.Count = 5 Then
                tvwEstrMat.Nodes(1).Expanded = True
                tvwEstrMat.Nodes(1).Child.Expanded = True
                tvwEstrMat.Nodes(1).Child.Child.Expanded = True
                tvwEstrMat.Nodes(1).Child.Child.Child.Expanded = True
                Set tvwEstrMat.selectedItem = tvwEstrMat.Nodes(1).Child.Child.Child.Child
            End If
        End If
        
     End With
    
Else 'No hay restriccion de comprador

    Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    
    oGrupsMN1.GenerarEstructuraMateriales bOrdenadoPorDen, oPlantillaSeleccionada.Id
    
    'Solo control de errores en bbdd, los nodos SIEMPRE cascan si coinciden el 1ro y el 2do en el GMN1/2/3. Pq el 1ro ya ha metido el nodo GMN1/2/3 y el 2do intenta meterlo de nuevo.
    'Ejemplo:GMN1   GMN2    GMN3    COD DEN_ENG
    '        CGE    100     100     100 HARDWARE/STORAGE CABINS HARDWARE
    '        CGE    100     100     110 HARDWARE/NETWORK ELECTRONIC EQUIPMENT
    
    On Error Resume Next
    
    Select Case gParametrosGenerales.giNEM
        
        Case 1 'Solo un nivel de material
            
            For Each oGMN1 In oGrupsMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                nodx.Tag = "GMN1" & oGMN1.Cod
            Next
                
        Case 2 'Dos niveles de materiales
            
                For Each oGMN1 In oGrupsMN1
                    scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                    
                    For Each oGMN2 In oGMN1.GruposMatNivel2
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN2.Cod
                    Next
                Next
               
        Case 3 'Hay tres niveles de materiales

                For Each oGMN1 In oGrupsMN1
                    scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                    
                    For Each oGMN2 In oGMN1.GruposMatNivel2
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN2.Cod
                            
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                            nodx.Tag = "GMN3" & oGMN3.Cod
                        Next
                    Next
                Next
        
        Case 4 'Cuatro niveles de materiales
                
                For Each oGMN1 In oGrupsMN1
                    scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                    
                    For Each oGMN2 In oGMN1.GruposMatNivel2
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN2.Cod
                        
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                            nodx.Tag = "GMN3" & oGMN3.Cod
                            
                            For Each oGMN4 In oGMN3.GruposMatNivel4
                                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                                nodx.Tag = "GMN4" & oGMN4.Cod
                                If Not oGMN4Seleccionado Is Nothing Then
                                    If oGMN1.Cod = oGMN4Seleccionado.GMN1Cod And oGMN2.Cod = oGMN4Seleccionado.GMN2Cod And oGMN3.Cod = oGMN4Seleccionado.GMN3Cod And oGMN4.Cod = oGMN4Seleccionado.Cod Then
                                        nodx.Parent.Parent.Parent.Expanded = True
                                        nodx.Parent.Parent.Expanded = True
                                        nodx.Parent.Expanded = True
                                        nodx.Expanded = True
                                        nodx.Selected = True
                                    End If
                                End If
                            Next
                        Next
                    Next
                Next
       
    End Select
    
End If

Set oGrupsMN1 = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMATPlantilla", "GenerarEstructuraMatModifNoCodif", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Comprueba si existe un nodo en el �rbol. La colecci�n de nodos del �rbol genera un error si se intenta acceder a un nodo que no existe</summary>
''' <param name="sKey">Key del nodo<param>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Function ExisteNodo(ByVal sKey As String) As Boolean
    Dim nodx As MSComctlLib.node
    
    ''********* ������ NO MODIFICAR EL CONTROL DE ERRORES !!!!!! ***************
    On Error GoTo NoSeEncuentra:

    Set nodx = tvwEstrMat.Nodes.Item(sKey)
    ExisteNodo = True
    
    Exit Function
NoSeEncuentra:
    ExisteNodo = False
    Set nodx = Nothing
End Function
