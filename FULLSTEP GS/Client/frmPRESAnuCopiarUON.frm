VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPRESAnuCopiarUON 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DPresupuesto a copiar"
   ClientHeight    =   4200
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8160
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPRESAnuCopiarUON.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   8160
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkCopiarImpObjs 
      BackColor       =   &H00808000&
      Caption         =   "DCopiar importes y objetivos"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   3000
      TabIndex        =   3
      Top             =   45
      Value           =   1  'Checked
      Width           =   2550
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   4110
      TabIndex        =   2
      Top             =   3795
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   3015
      TabIndex        =   1
      Top             =   3795
      Width           =   1005
   End
   Begin VB.CheckBox chkCopiarRama 
      BackColor       =   &H00808000&
      Caption         =   "DCopiar toda la rama"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   45
      Value           =   1  'Checked
      Width           =   2550
   End
   Begin MSComctlLib.TreeView tvwEstrOrg 
      Height          =   3015
      Left            =   90
      TabIndex        =   4
      Top             =   705
      Width           =   7965
      _ExtentX        =   14049
      _ExtentY        =   5318
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList2"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   6975
      Top             =   45
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPRESAnuCopiarUON.frx":014A
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPRESAnuCopiarUON.frx":0512
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPRESAnuCopiarUON.frx":0866
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPRESAnuCopiarUON.frx":0BBA
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPRESAnuCopiarUON.frx":0F0E
            Key             =   "Persona"
            Object.Tag             =   "Persona"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPRESAnuCopiarUON.frx":101A
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPRESAnuCopiarUON.frx":13AE
            Key             =   "PerCesta"
            Object.Tag             =   "PerCesta"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblUON 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "DUnidad organizativa"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   90
      TabIndex        =   5
      Top             =   345
      Width           =   7965
   End
End
Attribute VB_Name = "frmPRESAnuCopiarUON"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public sInfoPRES As String
Public sOrigen As String
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String
Public bRUO As Boolean
Public m_bCheckRama As Boolean

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim bCopiaTotal As Boolean
    Dim bImpObjs As Boolean
    Dim bRama As Boolean
    Dim s1 As String
    Dim s2 As String
    Dim s3 As String

    If UOOrigenIgualUODestino Then
        oMensajes.SeleccioneOtraUO
        Exit Sub
    End If
    If UODestinoNoValida Then
        oMensajes.AccesoDenegadoAUO
        Exit Sub
    End If
    UOSeleccionada m_sUON1, m_sUON2, m_sUON3
    irespuesta = vbNo
    
    'Ahora se determina el tipo de copia de los
    'presupuestos: total o parcial
    If sOrigen = "frmPresupuestos1" Then
        If Left(frmPresupuestos1.tvwEstrPres.selectedItem.Tag, 4) = "Raiz" Then
            bCopiaTotal = True
        Else
            bCopiaTotal = False
        End If
    ElseIf sOrigen = "frmPresupuestos2" Then
        If Left(frmPresupuestos2.tvwEstrPres.selectedItem.Tag, 4) = "Raiz" Then
            bCopiaTotal = True
        Else
            bCopiaTotal = False
        End If
    ElseIf sOrigen = "frmPresupuestos3" Then
        If Left(frmPresupuestos3.tvwEstrPres.selectedItem.Tag, 4) = "Raiz" Then
            bCopiaTotal = True
        Else
            bCopiaTotal = False
        End If
    ElseIf sOrigen = "frmPresupuestos4" Then
        If Left(frmPresupuestos4.tvwEstrPres.selectedItem.Tag, 4) = "Raiz" Then
            bCopiaTotal = True
        Else
            bCopiaTotal = False
        End If
    End If
        
    If bCopiaTotal Then
        irespuesta = oMensajes.PreguntaCopiarPresupuestos
        If irespuesta = vbYes Then
            Select Case sOrigen
                Case "frmPresupuestos1"
                    Screen.MousePointer = vbHourglass
                    If frmPresupuestos1.oPresupuestos Is Nothing Then Exit Sub
                    bImpObjs = False
                    If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                    ObtenerOrigenCopiaUO s1, s2, s3
                    teserror = frmPresupuestos1.oPresupuestos.CopiarTodosLosProyectosEnUON(s1, s2, s3, frmPresupuestos1.sdbcAnyo.Text, m_sUON1, m_sUON2, m_sUON3, bImpObjs)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                Case "frmPresupuestos2"
                    Screen.MousePointer = vbHourglass
                    If frmPresupuestos2.oPresupuestos Is Nothing Then Exit Sub
                    bImpObjs = False
                    If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                    ObtenerOrigenCopiaUO s1, s2, s3
                    teserror = frmPresupuestos2.oPresupuestos.CopiarTodosLasPartidasEnUON(s1, s2, s3, frmPresupuestos2.sdbcAnyo.Text, m_sUON1, m_sUON2, m_sUON3, bImpObjs)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                Case "frmPresupuestos3"
                    Screen.MousePointer = vbHourglass
                    If frmPresupuestos3.oPresupuestos Is Nothing Then Exit Sub
                    bImpObjs = False
                    If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                    ObtenerOrigenCopiaUO s1, s2, s3
                    teserror = frmPresupuestos3.oPresupuestos.CopiarTodosLosPresupuestosConceptos3EnUON(s1, s2, s3, m_sUON1, m_sUON2, m_sUON3, bImpObjs)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                Case "frmPresupuestos4"
                    Screen.MousePointer = vbHourglass
                    If frmPresupuestos4.oPresupuestos Is Nothing Then Exit Sub
                    bImpObjs = False
                    If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                    ObtenerOrigenCopiaUO s1, s2, s3
                    teserror = frmPresupuestos4.oPresupuestos.CopiarTodosLosPresupuestosConceptos4EnUON(s1, s2, s3, m_sUON1, m_sUON2, m_sUON3, bImpObjs)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End Select
        End If
    Else
        Select Case sOrigen
            Case "frmPresupuestos1"
                If Not frmPresupuestos1.tvwEstrPres.selectedItem Is Nothing Then
                    Screen.MousePointer = vbHourglass
                
                    Select Case Left(frmPresupuestos1.tvwEstrPres.selectedItem.Tag, 5)
                        Case "Raiz "
                
                        Case "PRES1"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos1.oPres1Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES2"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos1.oPres2Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES3"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos1.oPres3Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES4"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            teserror = frmPresupuestos1.oPres4Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                    End Select
                End If
                
            Case "frmPresupuestos2"
                If Not frmPresupuestos2.tvwEstrPres.selectedItem Is Nothing Then
                    Screen.MousePointer = vbHourglass
                
                    Select Case Left(frmPresupuestos2.tvwEstrPres.selectedItem.Tag, 5)
                        Case "Raiz "
                
                        Case "PRES1"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos2.oPres1Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES2"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos2.oPres2Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES3"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos2.oPres3Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES4"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            teserror = frmPresupuestos2.oPres4Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                    End Select
                End If
                
            Case "frmPresupuestos3"
                If Not frmPresupuestos3.tvwEstrPres.selectedItem Is Nothing Then
                    Screen.MousePointer = vbHourglass
                
                    Select Case Left(frmPresupuestos3.tvwEstrPres.selectedItem.Tag, 5)
                        Case "Raiz "
                
                        Case "PRES1"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos3.oPres1Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES2"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos3.oPres2Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES3"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos3.oPres3Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES4"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            teserror = frmPresupuestos3.oPres4Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                    End Select
                End If
                
            Case "frmPresupuestos4"
                If Not frmPresupuestos4.tvwEstrPres.selectedItem Is Nothing Then
                    Screen.MousePointer = vbHourglass
                
                    Select Case Left(frmPresupuestos4.tvwEstrPres.selectedItem.Tag, 5)
                        Case "Raiz "
                
                        Case "PRES1"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos4.oPres1Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES2"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos4.oPres2Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES3"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            bRama = False
                            If chkCopiarRama.Value = vbChecked Then bRama = True
                            teserror = frmPresupuestos4.oPres3Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, Not bRama, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                        Case "PRES4"
                            bImpObjs = False
                            If chkCopiarImpObjs.Value = vbChecked Then bImpObjs = True
                            teserror = frmPresupuestos4.oPres4Seleccionado.CopiarEnUON(m_sUON1, m_sUON2, m_sUON3, bImpObjs)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            End If
                    End Select
                End If
        End Select
    End If
    
    Screen.MousePointer = vbNormal
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    CargarRecursos
    If Not m_bCheckRama Then
        chkCopiarRama.Visible = False
        chkCopiarImpObjs.Left = tvwestrorg.Left
    End If
    CargarEstructuraOrg False
End Sub

Private Sub CargarRecursos()
    Dim adoresAdor As Ador.Recordset

    On Error Resume Next
    
    Set adoresAdor = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESANUCOPIARUON, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not adoresAdor Is Nothing Then
        frmPRESAnuCopiarUON.caption = adoresAdor.Fields.Item(0).Value & " " & sInfoPRES '1
        adoresAdor.MoveNext
        chkCopiarRama.caption = adoresAdor.Fields.Item(0).Value '2
        adoresAdor.MoveNext
        chkCopiarImpObjs.caption = adoresAdor.Fields.Item(0).Value '3
        adoresAdor.MoveNext
        lblUON.caption = adoresAdor.Fields.Item(0).Value '4
        adoresAdor.MoveNext
        cmdAceptar.caption = adoresAdor.Fields.Item(0).Value '5
        adoresAdor.MoveNext
        cmdCancelar.caption = adoresAdor.Fields.Item(0).Value '6
        adoresAdor.Close
    End If
    Set adoresAdor = Nothing
    cmdAceptar.Enabled = False
End Sub

Private Sub CargarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
    Dim scod1 As String
    Dim sCod2 As String
    Dim sCod3 As String
    Dim sCod4 As String

    ' Unidades organizativas
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3

    ' Otras
    Dim nodx As Node
    Dim bOrdCod As Boolean
    Dim bOrdDen As Boolean

    
    Select Case sOrigen
        Case "frmPresupuestos1"
            frmPresupuestos1.sOrdenListadoDen = bOrdenadoPorDen
        
            Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
            Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
            Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
             
                 
            If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And frmPresupuestos1.bRUO Then
                
                'Cargamos toda la estrucutura organizativa
                Select Case gParametrosGenerales.giNEO
                    Case 1
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos1.bRUO, , , False, , bOrdenadoPorDen, False
                    Case 2
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos1.bRUO, , , , , bOrdenadoPorDen
                        oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos1.bRUO, , , , , bOrdenadoPorDen
                    Case 3
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos1.bRUO, , , , , bOrdenadoPorDen, False
                        oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos1.bRUO, , , , , bOrdenadoPorDen, False
                        oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos1.bRUO, , , , , bOrdenadoPorDen, False
                End Select
                       
            Else
                
                'Cargamos toda la estrucutura organizativa
                Select Case gParametrosGenerales.giNEO
                    Case 1
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                    Case 2
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                            oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                    Case 3
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                            oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                            oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                End Select
            End If
        
        Case "frmPresupuestos2"
            frmPresupuestos2.sOrdenListadoDen = bOrdenadoPorDen
        
            Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
            Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
            Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
             
                 
            If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And frmPresupuestos2.bRUO Then
                
                'Cargamos toda la estrucutura organizativa
                Select Case gParametrosGenerales.giNEO
                    Case 1
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos2.bRUO, , , False, , bOrdenadoPorDen, False
                    Case 2
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos2.bRUO, , , , , bOrdenadoPorDen
                        oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos2.bRUO, , , , , bOrdenadoPorDen
                    Case 3
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos2.bRUO, , , , , bOrdenadoPorDen, False
                        oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos2.bRUO, , , , , bOrdenadoPorDen, False
                        oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos2.bRUO, , , , , bOrdenadoPorDen, False
                End Select
                       
            Else
                
                'Cargamos toda la estrucutura organizativa
                Select Case gParametrosGenerales.giNEO
                    Case 1
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                    Case 2
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                            oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                    Case 3
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                            oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                            oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                End Select
            End If
            
        Case "frmPresupuestos3"
            frmPresupuestos3.sOrdenListadoDen = bOrdenadoPorDen
        
            Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
            Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
            Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
             
                 
            If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And frmPresupuestos3.bRUO Then
                
                'Cargamos toda la estrucutura organizativa
                Select Case gParametrosGenerales.giNEO
                    Case 1
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos3.bRUO, , , False, , bOrdenadoPorDen, False
                    Case 2
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos3.bRUO, , , , , bOrdenadoPorDen
                        oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos3.bRUO, , , , , bOrdenadoPorDen
                    Case 3
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos3.bRUO, , , , , bOrdenadoPorDen, False
                        oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos3.bRUO, , , , , bOrdenadoPorDen, False
                        oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos3.bRUO, , , , , bOrdenadoPorDen, False
                End Select
                       
            Else
                
                'Cargamos toda la estrucutura organizativa
                Select Case gParametrosGenerales.giNEO
                    Case 1
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                    Case 2
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                            oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                    Case 3
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                            oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                            oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                End Select
            End If
            
        Case "frmPresupuestos4"
            frmPresupuestos4.sOrdenListadoDen = bOrdenadoPorDen
        
            Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
            Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
            Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
             
                 
            If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And frmPresupuestos4.bRUO Then
                
                'Cargamos toda la estrucutura organizativa
                Select Case gParametrosGenerales.giNEO
                    Case 1
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos4.bRUO, , , False, , bOrdenadoPorDen, False
                    Case 2
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos4.bRUO, , , , , bOrdenadoPorDen
                        oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos4.bRUO, , , , , bOrdenadoPorDen
                    Case 3
                        oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos4.bRUO, , , , , bOrdenadoPorDen, False
                        oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos4.bRUO, , , , , bOrdenadoPorDen, False
                        oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPresupuestos4.bRUO, , , , , bOrdenadoPorDen, False
                End Select
                       
            Else
                
                'Cargamos toda la estrucutura organizativa
                Select Case gParametrosGenerales.giNEO
                    Case 1
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                    Case 2
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                            oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                    Case 3
                            oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                            oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                            oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                End Select
            End If
    End Select
    
    
        
        
    '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
    
    For Each oUON1 In oUnidadesOrgN1
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        sCod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & sCod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        sCod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        sCod3 = sCod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & sCod2, tvwChild, "UON3" & sCod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
    Next
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
        
End Sub

Private Sub tvwestrorg_NodeClick(ByVal Node As MSComctlLib.Node)
    lblUON.caption = Node.Text
    If bRUO Then
        Select Case Left(Node.Tag, 4)
            Case "UON1", "UON2", "UON3"
                cmdAceptar.Enabled = True
            Case Else
                cmdAceptar.Enabled = False
        End Select
    Else
        cmdAceptar.Enabled = True
    End If
End Sub

Private Function UOOrigenIgualUODestino() As Boolean
'********************************************************************
'*** Descripci�n: Determinar cuando la Unidad Organizativa del    ***
'***              presupuesto a copiar es igual a la Unidad       ***
'***              Organizativa seleccionada para la copia.        ***
'***                                                              ***
'*** Par�metros:  ---------                                       ***
'***                                                              ***
'*** Valor que devuelve: Un valor booleano que ser� TRUE cuando   ***
'***                     las UO de origen y destino seleccionadas ***
'***                     para la copia de presupuestos sean la    ***
'***                     misma.                                   ***
'********************************************************************
    Dim nodx As MSComctlLib.Node
    Dim sComp1 As String
    Dim sComp2 As String
    Dim s1 As String
    Dim s2 As String
    Dim s3 As String
    
    'Obteniendo UO origen
    ObtenerOrigenCopiaUO s1, s2, s3
    sComp1 = s1 & s2 & s3
    'Obteniendo UO destino
    Set nodx = tvwestrorg.selectedItem
    sComp2 = ""
    Select Case Left(nodx.Tag, 4)
        Case "UON1"
            sComp2 = Right(nodx.Tag, Len(nodx.Tag) - 4)
        Case "UON2"
            sComp2 = Right(nodx.Parent.Tag, Len(nodx.Parent.Tag) - 4) & Right(nodx.Tag, Len(nodx.Tag) - 4)
        Case "UON3"
            sComp2 = Right(nodx.Parent.Parent.Tag, Len(nodx.Parent.Parent.Tag) - 4) & Right(nodx.Parent.Tag, Len(nodx.Parent.Tag) - 4) & Right(nodx.Tag, Len(nodx.Tag) - 4)
    End Select
    If sComp1 = sComp2 Then
        UOOrigenIgualUODestino = True
    Else
        UOOrigenIgualUODestino = False
    End If
End Function

Private Sub ObtenerOrigenCopiaUO(ByRef s1 As String, ByRef s2 As String, ByRef s3 As String)
'*********************************************************************
'*** Descripci�n: Determinar las unidades organizativas que el     ***
'***              usuario ha seleccionado como origen de la copia, ***
'***              en la pantalla de presupuestos, pinchando un     ***
'***              nodo del control TreeView.                       ***
'***                                                               ***
'*** Par�metros:  s1, s2, s3: estos tres par�metros de             ***
'***              tipo String ser�n la salida del procedimiento y  ***
'***              tras la ejecuci�n del mismo contendr�n los       ***
'***              c�digos de la unidad organizativa                ***
'***                                                               ***
'*** Valor que devuelve: s1, s2, s3; ya explicados en la           ***
'***                     secci�n anterior de par�metros.           ***
'***                                                               ***
'*********************************************************************

    Dim nodx As MSComctlLib.Node
    
    s1 = ""
    s2 = ""
    s3 = ""
    
    If sOrigen = "frmPresupuestos1" Then
        Set nodx = frmPresupuestos1.tvwestrorg.selectedItem
    ElseIf sOrigen = "frmPresupuestos2" Then
        Set nodx = frmPresupuestos2.tvwestrorg.selectedItem
    ElseIf sOrigen = "frmPresupuestos3" Then
        Set nodx = frmPresupuestos3.tvwestrorg.selectedItem
    ElseIf sOrigen = "frmPresupuestos4" Then
        Set nodx = frmPresupuestos4.tvwestrorg.selectedItem
    End If
    Select Case Left(nodx.Tag, 4)
        Case "UON1"
            s1 = Right(nodx.Tag, Len(nodx.Tag) - 4)
        Case "UON2"
            s1 = Right(nodx.Parent.Tag, Len(nodx.Parent.Tag) - 4)
            s2 = Right(nodx.Tag, Len(nodx.Tag) - 4)
        Case "UON3"
            s1 = Right(nodx.Parent.Parent.Tag, Len(nodx.Parent.Parent.Tag) - 4)
            s2 = Right(nodx.Parent.Tag, Len(nodx.Parent.Tag) - 4)
            s3 = Right(nodx.Tag, Len(nodx.Tag) - 4)
    End Select
End Sub

Private Sub UOSeleccionada(ByRef sUON1 As String, ByRef sUON2 As String, ByRef sUON3 As String)
'*********************************************************************
'*** Descripci�n: Determinar las unidades organizativas que el     ***
'***              usuario ha seleccionado en la pantalla pinchando ***
'***              un nodo del control TreeView.                    ***
'***                                                               ***
'*** Par�metros:  sUON1, sUON2, sUON3: estos tres par�metros de    ***
'***              tipo String ser�n la salida del procedimiento y  ***
'***              tras la ejecuci�n del mismo contendr�n los       ***
'***              c�digos de la unidad organizativa                ***
'***                                                               ***
'*** Valor que devuelve: sUON1, sUON2, sUON3; ya explicados en la  ***
'***                     secci�n anterior de par�metros.           ***
'***                                                               ***
'*********************************************************************

    Dim nodx As MSComctlLib.Node
    
    
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    Set nodx = tvwestrorg.selectedItem
    Select Case Left(nodx.Tag, 4)
        Case "UON1"
            sUON1 = Right(nodx.Tag, Len(nodx.Tag) - 4)
        Case "UON2"
            sUON1 = Right(nodx.Parent.Tag, Len(nodx.Parent.Tag) - 4)
            sUON2 = Right(nodx.Tag, Len(nodx.Tag) - 4)
        Case "UON3"
            sUON1 = Right(nodx.Parent.Parent.Tag, Len(nodx.Parent.Parent.Tag) - 4)
            sUON2 = Right(nodx.Parent.Tag, Len(nodx.Parent.Tag) - 4)
            sUON3 = Right(nodx.Tag, Len(nodx.Tag) - 4)
    End Select
End Sub

Private Function UODestinoNoValida() As Boolean
'********************************************************************
'*** Descripci�n: Determinar si se est� intentando copiar los     ***
'***              presupuestos en alguna UO predecesora para un   ***
'***              usuario con restricci�n de UO.                  ***
'***                                                              ***
'*** Par�metros:  ---------                                       ***
'***                                                              ***
'*** Valor que devuelve: Un valor booleano que ser� TRUE cuando   ***
'***                     la UO de destino seleccionada es padre   ***
'***                     o predecesor dela UO a la que se ve      ***
'***                     restringido un usuario con esta          ***
'***                     restricci�n.                             ***
'********************************************************************

    Dim iUOBase As Integer
    Dim nodx As MSComctlLib.Node
    
    
    iUOBase = 0
    If bRUO Then
        If (CStr(basOptimizacion.gUON3Usuario) <> "") Then
            iUOBase = 3
        ElseIf (CStr(basOptimizacion.gUON2Usuario) <> "") Then
            iUOBase = 2
        ElseIf (CStr(basOptimizacion.gUON1Usuario) <> "") Then
            iUOBase = 1
        End If
    End If
    Set nodx = tvwestrorg.selectedItem
    If val(Right(Left(nodx.Tag, 4), 1)) < iUOBase Then
        UODestinoNoValida = True
    Else
        UODestinoNoValida = False
    End If
End Function
