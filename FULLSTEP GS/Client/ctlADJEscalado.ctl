VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.UserControl ctlADJEscalado 
   ClientHeight    =   6720
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   12750
   ScaleHeight     =   6720
   ScaleWidth      =   12750
   Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
      Height          =   915
      Left            =   9480
      TabIndex        =   6
      Top             =   2280
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlADJEscalado.ctx":0000
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      Columns(0).Width=   3201
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picOcultarItemsEsc 
      BackColor       =   &H00A56E3A&
      Height          =   4335
      Left            =   3600
      ScaleHeight     =   4275
      ScaleWidth      =   5430
      TabIndex        =   1
      Top             =   2280
      Visible         =   0   'False
      WhatsThisHelpID =   6435
      Width           =   5490
      Begin VB.CommandButton cmdCerrar 
         Height          =   195
         Left            =   5040
         Picture         =   "ctlADJEscalado.ctx":001C
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   120
         Width           =   205
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "&Ok"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1800
         TabIndex        =   2
         Top             =   3840
         Width           =   2055
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgOcultarItems 
         Height          =   3255
         Left            =   180
         TabIndex        =   3
         Top             =   480
         Width           =   4980
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   3
         stylesets.count =   4
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16367808
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "ctlADJEscalado.ctx":02D7
         stylesets(0).AlignmentPicture=   0
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "ctlADJEscalado.ctx":0629
         stylesets(1).AlignmentText=   0
         stylesets(2).Name=   "Interno"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "ctlADJEscalado.ctx":0645
         stylesets(2).AlignmentPicture=   1
         stylesets(3).Name=   "Seleccionada"
         stylesets(3).ForeColor=   16777215
         stylesets(3).BackColor=   8388608
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "ctlADJEscalado.ctx":0997
         DividerType     =   0
         BevelColorFrame =   -2147483646
         RowSelectionStyle=   1
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         PictureButton   =   "ctlADJEscalado.ctx":09B3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorEven   =   15777984
         BackColorOdd    =   15777984
         RowHeight       =   370
         ExtraHeight     =   132
         Columns.Count   =   3
         Columns(0).Width=   397
         Columns(0).Name =   "INC"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HeadBackColor=   12615680
         Columns(1).Width=   7858
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadForeColor=   16777215
         Columns(1).HeadBackColor=   12615680
         Columns(1).HeadStyleSet=   "Normal"
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8784
         _ExtentY        =   5741
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   16367808
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblCheck 
         BackColor       =   &H00A56E3A&
         Caption         =   "DChequear/deschequear items para mostrarlos/ocultarlos"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   180
         Width           =   4395
      End
      Begin VB.Shape Shape4 
         BorderColor     =   &H00FFFFFF&
         Height          =   4095
         Left            =   60
         Top             =   60
         Width           =   5280
      End
      Begin VB.Shape Shape9 
         BorderColor     =   &H00FFFFFF&
         Height          =   4215
         Left            =   0
         Top             =   0
         Width           =   5400
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgEscalado 
      Height          =   4860
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   11940
      _Version        =   196617
      DataMode        =   2
      GroupHeadLines  =   2
      HeadLines       =   3
      Col.Count       =   23
      stylesets.count =   26
      stylesets(0).Name=   "NoHomologado"
      stylesets(0).ForeColor=   192
      stylesets(0).BackColor=   13170165
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlADJEscalado.ctx":0D05
      stylesets(0).AlignmentText=   1
      stylesets(1).Name=   "ProvActual"
      stylesets(1).ForeColor=   0
      stylesets(1).BackColor=   9693431
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "ctlADJEscalado.ctx":0D21
      stylesets(2).Name=   "Item"
      stylesets(2).BackColor=   9876917
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "ctlADJEscalado.ctx":0D3D
      stylesets(3).Name=   "ItemCerradoClip"
      stylesets(3).ForeColor=   16777215
      stylesets(3).BackColor=   12632256
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "ctlADJEscalado.ctx":0D59
      stylesets(3).AlignmentPicture=   0
      stylesets(4).Name=   "ProveNoAdj"
      stylesets(4).ForeColor=   16777215
      stylesets(4).BackColor=   -2147483633
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "ctlADJEscalado.ctx":0D75
      stylesets(5).Name=   "Yellow"
      stylesets(5).ForeColor=   0
      stylesets(5).BackColor=   13170165
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "ctlADJEscalado.ctx":0D91
      stylesets(5).AlignmentText=   1
      stylesets(6).Name=   "NoHomologadoClip"
      stylesets(6).ForeColor=   192
      stylesets(6).BackColor=   13170165
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "ctlADJEscalado.ctx":0DAD
      stylesets(6).AlignmentPicture=   0
      stylesets(7).Name=   "YellowClip"
      stylesets(7).ForeColor=   0
      stylesets(7).BackColor=   13169908
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "ctlADJEscalado.ctx":0DC9
      stylesets(7).AlignmentPicture=   0
      stylesets(8).Name=   "Normal"
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "ctlADJEscalado.ctx":0DE5
      stylesets(9).Name=   "Prove"
      stylesets(9).BackColor=   13170165
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "ctlADJEscalado.ctx":0E01
      stylesets(10).Name=   "YellowHead"
      stylesets(10).ForeColor=   0
      stylesets(10).BackColor=   13170165
      stylesets(10).HasFont=   -1  'True
      BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(10).Picture=   "ctlADJEscalado.ctx":0E1D
      stylesets(11).Name=   "YellowERPError"
      stylesets(11).BackColor=   13170165
      stylesets(11).HasFont=   -1  'True
      BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(11).Picture=   "ctlADJEscalado.ctx":0E39
      stylesets(12).Name=   "BlueHead"
      stylesets(12).ForeColor=   12582912
      stylesets(12).BackColor=   16777215
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "ctlADJEscalado.ctx":11DA
      stylesets(13).Name=   "ProveedorConAdjuntos"
      stylesets(13).HasFont=   -1  'True
      BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(13).Picture=   "ctlADJEscalado.ctx":11F6
      stylesets(13).AlignmentText=   2
      stylesets(13).AlignmentPicture=   2
      stylesets(14).Name=   "Adjudicado"
      stylesets(14).BackColor=   10079487
      stylesets(14).HasFont=   -1  'True
      BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(14).Picture=   "ctlADJEscalado.ctx":1212
      stylesets(15).Name=   "Blue"
      stylesets(15).ForeColor=   12582912
      stylesets(15).BackColor=   16777215
      stylesets(15).HasFont=   -1  'True
      BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(15).Picture=   "ctlADJEscalado.ctx":122E
      stylesets(15).AlignmentText=   1
      stylesets(16).Name=   "Proveedor"
      stylesets(16).BackColor=   -2147483633
      stylesets(16).HasFont=   -1  'True
      BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(16).Picture=   "ctlADJEscalado.ctx":124A
      stylesets(16).AlignmentText=   0
      stylesets(16).AlignmentPicture=   1
      stylesets(17).Name=   "Red"
      stylesets(17).BackColor=   4744445
      stylesets(17).HasFont=   -1  'True
      BeginProperty stylesets(17).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(17).Picture=   "ctlADJEscalado.ctx":1266
      stylesets(17).AlignmentText=   1
      stylesets(18).Name=   "AdjudicadoClip"
      stylesets(18).BackColor=   10079487
      stylesets(18).HasFont=   -1  'True
      BeginProperty stylesets(18).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(18).Picture=   "ctlADJEscalado.ctx":1282
      stylesets(18).AlignmentPicture=   0
      stylesets(19).Name=   "HeadBold"
      stylesets(19).HasFont=   -1  'True
      BeginProperty stylesets(19).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(19).Picture=   "ctlADJEscalado.ctx":129E
      stylesets(20).Name=   "Green"
      stylesets(20).BackColor=   10409634
      stylesets(20).HasFont=   -1  'True
      BeginProperty stylesets(20).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(20).Picture=   "ctlADJEscalado.ctx":12BA
      stylesets(20).AlignmentText=   1
      stylesets(21).Name=   "YellowLeft"
      stylesets(21).ForeColor=   0
      stylesets(21).BackColor=   13170165
      stylesets(21).HasFont=   -1  'True
      BeginProperty stylesets(21).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(21).Picture=   "ctlADJEscalado.ctx":12D6
      stylesets(21).AlignmentText=   0
      stylesets(22).Name=   "ItemCerrado"
      stylesets(22).ForeColor=   16777215
      stylesets(22).BackColor=   12632256
      stylesets(22).HasFont=   -1  'True
      BeginProperty stylesets(22).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(22).Picture=   "ctlADJEscalado.ctx":12F2
      stylesets(23).Name=   "Grey"
      stylesets(23).BackColor=   12632256
      stylesets(23).HasFont=   -1  'True
      BeginProperty stylesets(23).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   675
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(23).Picture=   "ctlADJEscalado.ctx":130E
      stylesets(23).AlignmentText=   1
      stylesets(23).AlignmentPicture=   2
      stylesets(24).Name=   "GrayHead"
      stylesets(24).ForeColor=   0
      stylesets(24).BackColor=   -2147483633
      stylesets(24).HasFont=   -1  'True
      BeginProperty stylesets(24).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(24).Picture=   "ctlADJEscalado.ctx":132A
      stylesets(25).Name=   "DobleFondo"
      stylesets(25).HasFont=   -1  'True
      BeginProperty stylesets(25).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(25).Picture=   "ctlADJEscalado.ctx":1346
      stylesets(25).AlignmentPicture=   5
      UseGroups       =   -1  'True
      DividerStyle    =   0
      MultiLine       =   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      CellNavigation  =   1
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   265
      CaptionAlignment=   0
      Groups.Count    =   2
      Groups(0).Width =   17568
      Groups(0).Caption=   "(Pulse aqu� para ocultar o visualizar la segunda linea)"
      Groups(0).HeadStyleSet=   "GrayHead"
      Groups(0).Columns.Count=   23
      Groups(0).Columns(0).Width=   529
      Groups(0).Columns(0).Visible=   0   'False
      Groups(0).Columns(0).Caption=   "ID"
      Groups(0).Columns(0).Name=   "ID"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   582
      Groups(0).Columns(1).Visible=   0   'False
      Groups(0).Columns(1).Caption=   "ART"
      Groups(0).Columns(1).Name=   "ART"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(2).Width=   635
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "DEST"
      Groups(0).Columns(2).Name=   "DEST"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(3).Width=   714
      Groups(0).Columns(3).Visible=   0   'False
      Groups(0).Columns(3).Caption=   "CERRADO"
      Groups(0).Columns(3).Name=   "CERRADO"
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   11
      Groups(0).Columns(3).FieldLen=   256
      Groups(0).Columns(4).Width=   767
      Groups(0).Columns(4).Visible=   0   'False
      Groups(0).Columns(4).Caption=   "UNI"
      Groups(0).Columns(4).Name=   "UNI"
      Groups(0).Columns(4).DataField=   "Column 4"
      Groups(0).Columns(4).DataType=   8
      Groups(0).Columns(4).FieldLen=   256
      Groups(0).Columns(5).Width=   1138
      Groups(0).Columns(5).Visible=   0   'False
      Groups(0).Columns(5).Caption=   "PAG"
      Groups(0).Columns(5).Name=   "PAG"
      Groups(0).Columns(5).DataField=   "Column 5"
      Groups(0).Columns(5).DataType=   8
      Groups(0).Columns(5).FieldLen=   256
      Groups(0).Columns(6).Width=   1931
      Groups(0).Columns(6).Visible=   0   'False
      Groups(0).Columns(6).Caption=   "NUMOFE"
      Groups(0).Columns(6).Name=   "NUMOFE"
      Groups(0).Columns(6).DataField=   "Column 6"
      Groups(0).Columns(6).DataType=   8
      Groups(0).Columns(6).FieldLen=   256
      Groups(0).Columns(7).Width=   2778
      Groups(0).Columns(7).Visible=   0   'False
      Groups(0).Columns(7).Caption=   "Proveedor"
      Groups(0).Columns(7).Name=   "PROV"
      Groups(0).Columns(7).DataField=   "Column 7"
      Groups(0).Columns(7).DataType=   8
      Groups(0).Columns(7).FieldLen=   256
      Groups(0).Columns(7).Locked=   -1  'True
      Groups(0).Columns(7).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(7).StyleSet=   "YellowLeft"
      Groups(0).Columns(8).Width=   2249
      Groups(0).Columns(8).Caption=   "Description (Year/Dest)"
      Groups(0).Columns(8).Name=   "DESCR"
      Groups(0).Columns(8).CaptionAlignment=   0
      Groups(0).Columns(8).DataField=   "Column 8"
      Groups(0).Columns(8).DataType=   8
      Groups(0).Columns(8).FieldLen=   256
      Groups(0).Columns(8).Locked=   -1  'True
      Groups(0).Columns(8).Style=   1
      Groups(0).Columns(8).HasHeadBackColor=   -1  'True
      Groups(0).Columns(8).HasBackColor=   -1  'True
      Groups(0).Columns(8).HeadBackColor=   -2147483633
      Groups(0).Columns(8).BackColor=   16776960
      Groups(0).Columns(8).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(8).StyleSet=   "YellowLeft"
      Groups(0).Columns(9).Width=   1244
      Groups(0).Columns(9).Caption=   "Quantity"
      Groups(0).Columns(9).Name=   "CANT"
      Groups(0).Columns(9).Alignment=   1
      Groups(0).Columns(9).CaptionAlignment=   2
      Groups(0).Columns(9).DataField=   "Column 9"
      Groups(0).Columns(9).DataType=   8
      Groups(0).Columns(9).NumberFormat=   "Standard"
      Groups(0).Columns(9).FieldLen=   256
      Groups(0).Columns(9).HasHeadForeColor=   -1  'True
      Groups(0).Columns(9).HasHeadBackColor=   -1  'True
      Groups(0).Columns(9).HasForeColor=   -1  'True
      Groups(0).Columns(9).HeadForeColor=   12582912
      Groups(0).Columns(9).HeadBackColor=   16777215
      Groups(0).Columns(9).ForeColor=   12582912
      Groups(0).Columns(9).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(9).StyleSet=   "Blue"
      Groups(0).Columns(10).Width=   1482
      Groups(0).Columns(10).Caption=   "Used amount"
      Groups(0).Columns(10).Name=   "PRECAPE"
      Groups(0).Columns(10).Alignment=   1
      Groups(0).Columns(10).CaptionAlignment=   2
      Groups(0).Columns(10).DataField=   "Column 10"
      Groups(0).Columns(10).DataType=   8
      Groups(0).Columns(10).NumberFormat=   "Standard"
      Groups(0).Columns(10).FieldLen=   256
      Groups(0).Columns(10).Locked=   -1  'True
      Groups(0).Columns(10).HasHeadForeColor=   -1  'True
      Groups(0).Columns(10).HasHeadBackColor=   -1  'True
      Groups(0).Columns(10).HasForeColor=   -1  'True
      Groups(0).Columns(10).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(10).StyleSet=   "Yellow"
      Groups(0).Columns(11).Width=   1482
      Groups(0).Columns(11).Caption=   "Current Supplier"
      Groups(0).Columns(11).Name=   "CODPROVEACTUAL"
      Groups(0).Columns(11).Alignment=   1
      Groups(0).Columns(11).CaptionAlignment=   2
      Groups(0).Columns(11).DataField=   "Column 11"
      Groups(0).Columns(11).DataType=   8
      Groups(0).Columns(11).NumberFormat=   "Standard"
      Groups(0).Columns(11).FieldLen=   256
      Groups(0).Columns(11).Locked=   -1  'True
      Groups(0).Columns(11).HasHeadForeColor=   -1  'True
      Groups(0).Columns(11).HasHeadBackColor=   -1  'True
      Groups(0).Columns(11).HasForeColor=   -1  'True
      Groups(0).Columns(11).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(11).StyleSet=   "Yellow"
      Groups(0).Columns(12).Width=   1429
      Groups(0).Columns(12).Caption=   "Dec. Amount"
      Groups(0).Columns(12).Name=   "IMP"
      Groups(0).Columns(12).Alignment=   1
      Groups(0).Columns(12).CaptionAlignment=   2
      Groups(0).Columns(12).DataField=   "Column 12"
      Groups(0).Columns(12).DataType=   8
      Groups(0).Columns(12).NumberFormat=   "Standard"
      Groups(0).Columns(12).FieldLen=   256
      Groups(0).Columns(12).Locked=   -1  'True
      Groups(0).Columns(12).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(12).StyleSet=   "Yellow"
      Groups(0).Columns(13).Width=   1323
      Groups(0).Columns(13).Caption=   "Saving"
      Groups(0).Columns(13).Name=   "AHORROIMP"
      Groups(0).Columns(13).Alignment=   1
      Groups(0).Columns(13).CaptionAlignment=   2
      Groups(0).Columns(13).DataField=   "Column 13"
      Groups(0).Columns(13).DataType=   8
      Groups(0).Columns(13).NumberFormat=   "Standard"
      Groups(0).Columns(13).FieldLen=   256
      Groups(0).Columns(13).Locked=   -1  'True
      Groups(0).Columns(13).HasHeadBackColor=   -1  'True
      Groups(0).Columns(13).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(14).Width=   1244
      Groups(0).Columns(14).Caption=   "Saving %"
      Groups(0).Columns(14).Name=   "AHORROPORCEN"
      Groups(0).Columns(14).Alignment=   2
      Groups(0).Columns(14).CaptionAlignment=   2
      Groups(0).Columns(14).DataField=   "Column 14"
      Groups(0).Columns(14).DataType=   8
      Groups(0).Columns(14).NumberFormat=   "0.0#\%"
      Groups(0).Columns(14).FieldLen=   256
      Groups(0).Columns(14).Locked=   -1  'True
      Groups(0).Columns(14).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(15).Width=   7011
      Groups(0).Columns(15).Visible=   0   'False
      Groups(0).Columns(15).Caption=   "CANTMAX"
      Groups(0).Columns(15).Name=   "CANTMAX"
      Groups(0).Columns(15).DataField=   "Column 15"
      Groups(0).Columns(15).DataType=   8
      Groups(0).Columns(15).FieldLen=   256
      Groups(0).Columns(16).Width=   7011
      Groups(0).Columns(16).Visible=   0   'False
      Groups(0).Columns(16).Caption=   "HOM"
      Groups(0).Columns(16).Name=   "HOM"
      Groups(0).Columns(16).DataField=   "Column 16"
      Groups(0).Columns(16).DataType=   8
      Groups(0).Columns(16).FieldLen=   256
      Groups(0).Columns(17).Width=   7038
      Groups(0).Columns(17).Visible=   0   'False
      Groups(0).Columns(17).Caption=   "GRUPOCOD"
      Groups(0).Columns(17).Name=   "GRUPOCOD"
      Groups(0).Columns(17).DataField=   "Column 17"
      Groups(0).Columns(17).DataType=   8
      Groups(0).Columns(17).FieldLen=   256
      Groups(0).Columns(18).Width=   1244
      Groups(0).Columns(18).Caption=   "Solicitud"
      Groups(0).Columns(18).Name=   "VINCULADO_ITEM"
      Groups(0).Columns(18).Alignment=   1
      Groups(0).Columns(18).DataField=   "Column 18"
      Groups(0).Columns(18).DataType=   8
      Groups(0).Columns(18).FieldLen=   256
      Groups(0).Columns(18).Locked=   -1  'True
      Groups(0).Columns(18).Style=   1
      Groups(0).Columns(18).ButtonsAlways=   -1  'True
      Groups(0).Columns(18).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(19).Width=   1852
      Groups(0).Columns(19).Caption=   "DFec. ini. suministro"
      Groups(0).Columns(19).Name=   "INI"
      Groups(0).Columns(19).CaptionAlignment=   2
      Groups(0).Columns(19).DataField=   "Column 19"
      Groups(0).Columns(19).DataType=   8
      Groups(0).Columns(19).FieldLen=   256
      Groups(0).Columns(19).Locked=   -1  'True
      Groups(0).Columns(19).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(20).Width=   1799
      Groups(0).Columns(20).Caption=   "DFec. fin suministro"
      Groups(0).Columns(20).Name=   "FIN"
      Groups(0).Columns(20).CaptionAlignment=   2
      Groups(0).Columns(20).DataField=   "Column 20"
      Groups(0).Columns(20).DataType=   8
      Groups(0).Columns(20).FieldLen=   256
      Groups(0).Columns(20).Locked=   -1  'True
      Groups(0).Columns(20).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(21).Width=   2223
      Groups(0).Columns(21).Caption=   "DEstr. material"
      Groups(0).Columns(21).Name=   "GMN"
      Groups(0).Columns(21).CaptionAlignment=   2
      Groups(0).Columns(21).DataField=   "Column 21"
      Groups(0).Columns(21).DataType=   8
      Groups(0).Columns(21).FieldLen=   256
      Groups(0).Columns(21).Locked=   -1  'True
      Groups(0).Columns(21).HeadStyleSet=   "DobleFondo"
      Groups(0).Columns(22).Width=   8467
      Groups(0).Columns(22).Visible=   0   'False
      Groups(0).Columns(22).Caption=   "ERRORINT"
      Groups(0).Columns(22).Name=   "ERRORINT"
      Groups(0).Columns(22).DataField=   "Column 22"
      Groups(0).Columns(22).DataType=   8
      Groups(0).Columns(22).FieldLen=   256
      Groups(1).Width =   3201
      Groups(1).Visible=   0   'False
      Groups(1).Caption=   "ATRIB_CARAC"
      Groups(1).Columns.Count=   0
      UseDefaults     =   0   'False
      _ExtentX        =   21061
      _ExtentY        =   8572
      _StockProps     =   79
      Caption         =   "sdbgEscalado"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuAdjudicacion 
      Caption         =   ""
      Begin VB.Menu mnuAdjudicar 
         Caption         =   "DAdjudicar"
      End
      Begin VB.Menu mnuDesadjudicar 
         Caption         =   "DDesadjudicar"
      End
   End
End
Attribute VB_Name = "ctlADJEscalado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Option Explicit

Private Const ANCHO_FILA_GR = 480.189

Public frmOrigen As Form
Public Textos As Variant
Public ProcesoSeleccionado As CProceso
Public VistaSeleccionada As CConfVistaProce
Public VistaSeleccionadaGr As CConfVistaGrupo
Public VistaSeleccionadaAll As CConfVistaAll
Public GrupoSeleccionado As CGrupo
Public Asigs As CAsignaciones
Public ProvesAsig As CProveedores
Public AtribsFormulas As CAtributos
Public ProvesUmbrales As CProveedores
Public EstOfes As COfeEstados
Public PermitirModifVistas As Boolean
Public AdjsProve As CAdjudicaciones 'Colecci�n para las adjudicaciones totales por proveedor (datos reales)

Public Event BtnGrupoClick(ByVal Grid As SSDBGrid, ByVal scodProve As String)
Public Event BtnAllClick(ByVal Grid As SSDBGrid, ByVal sCodGrupo As String, ByVal scodProve As String)
Public Event ItemDblClick(ByVal Grid As SSDBGrid, ByVal oItem As CItem)
Public Event ProveMouseUp(ByVal scodProve As String)
Public Event GridMouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

Private m_bGrupo As Boolean
Private m_arrOrden(10) As Variant
Private m_arrOrdenColsGr() As Variant
Private ColorWhite As ColorConstants
Private ColorButton As ColorConstants
Private ColorGrey As ColorConstants
Private ColorBlue As ColorConstants
Private m_bComparativaQA As Boolean
Private m_dcItemsOcultos As Dictionary
Private m_colOcultarItemsBookmarks As Collection
Private m_oAdjsProve As CAdjudicaciones   'Colecci�n para las adjudicaciones totales por proveedor (c�lculos internos)
Private m_lIdItem As Long
Private m_bBorrarAdj As Boolean
Private m_bRecalcular As Boolean
Private m_bRecalcularItem As Boolean
Private m_bCambioCantAdjEsc As Boolean
Private m_iFilaItem As Integer
Private m_iGrupo As Integer
Private m_iRow As Integer
Private m_dblCantidadAnterior As Double
Private m_bRecalculando As Boolean
Private m_bMouseUpDblClick As Boolean
Private m_bComprobandoActualizarGrid As Boolean
Private m_bTabPressed As Boolean
Private m_bHayProvesSinOfesOcultos As Boolean

'Tarea 3381
Private m_bModificacionObjetivos As Boolean


Public m_sNoReabrirGrupos As String
Public m_sNoReabrirItems As String
Public m_sNingunoAdjudicado As String
Public m_sGruposReabiertos As String
Public m_sItemsReabiertos As String

Public Property Get GridEscalado() As SSDBGrid
    Set GridEscalado = sdbgEscalado
End Property

Public Property Get PicOcultarItems() As PictureBox
    Set PicOcultarItems = picOcultarItemsEsc
End Property

Public Property Get HayProvesSinOfesOcultos() As Boolean
    HayProvesSinOfesOcultos = m_bHayProvesSinOfesOcultos
End Property

Private Sub cmdCerrar_Click()
    picOcultarItemsEsc.Visible = False
End Sub

Private Sub cmdOK_Click()
    Dim i As Integer
    Dim vBookmark As Variant
    
    'Rellenar la colecci�n de items ocultos
    Set m_dcItemsOcultos = New Dictionary
    With sdbgOcultarItems
        .Update
        
        For i = 0 To .Rows - 1
            vBookmark = .AddItemBookmark(i)
            If .Columns("INC").CellValue(vBookmark) = "0" Then
                m_dcItemsOcultos.Add CStr(.Columns("ID").CellValue(vBookmark)), .Columns("ID").CellValue(vBookmark)
            End If
        Next
    End With
        
    If m_bGrupo Then
        RellenarGridGrupo sdbgEscalado.Groups(1).Columns.Count
    Else
        RellenarGridAll sdbgEscalado.Groups(1).Columns.Count
    End If
    
    picOcultarItemsEsc.Visible = False
End Sub
''' <summary>
''' Adjudicar
''' </summary>
''' <returns>Llamada desde: sistema ; Tiempo m�ximo: 0,2</returns>
Private Sub mnuAdjudicar_Click()
    With sdbgEscalado
        If .Grp > -1 Then
            AdjudicarDesadjudicarEscalado ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value), .Columns("ID").Value, .Columns("ART").Value, _
                .Columns("ESC" & .Grp).Value, True
        End If
    End With
End Sub
''' <summary>
''' Desadjudicar
''' </summary>
''' <returns>Llamada desde: sistema ; Tiempo m�ximo: 0,2</returns>
Private Sub mnuDesadjudicar_Click()
    With sdbgEscalado
        If .Grp > -1 Then
            AdjudicarDesadjudicarEscalado ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value), .Columns("ID").Value, .Columns("ART").Value, _
                .Columns("ESC" & .Grp).Value, False
        End If
    End With
End Sub

Private Sub sdbddValor_DropDown()
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim lIdAtribProce As Long
    Dim sProve As String
    
    sdbddValor.RemoveAll
                   
    If sdbgEscalado.Columns(sdbgEscalado.col).Locked Then
        sdbddValor.Enabled = False
        Exit Sub
    End If
    
    sProve = sdbgEscalado.Columns("ID").Value
    lIdAtribProce = sdbgEscalado.Columns(sdbgEscalado.col).Name
    
    If Not ProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        If Not ProcesoSeleccionado.ATRIBUTOS.Item(CStr(lIdAtribProce)) Is Nothing Then
            Set oatrib = ProcesoSeleccionado.ATRIBUTOS.Item(CStr(lIdAtribProce))
        End If
    End If
    If Not ProcesoSeleccionado.AtributosGrupo Is Nothing Then
        If Not ProcesoSeleccionado.AtributosGrupo.Item(CStr(lIdAtribProce)) Is Nothing Then
            Set oatrib = ProcesoSeleccionado.AtributosGrupo.Item(CStr(lIdAtribProce))
        End If
    End If
    If Not ProcesoSeleccionado.AtributosItem Is Nothing Then
        If Not ProcesoSeleccionado.AtributosItem.Item(CStr(lIdAtribProce)) Is Nothing Then
            Set oatrib = ProcesoSeleccionado.AtributosItem.Item(CStr(lIdAtribProce))
        End If
    End If
                                     
    If oatrib.TipoIntroduccion = Introselec Then
        Set oLista = oatrib.ListaPonderacion
        For Each oElem In oLista
            sdbddValor.AddItem oElem.ValorLista
        Next
        Set oLista = Nothing
    Else
        If oatrib.Tipo = TipoBoolean Then
            sdbddValor.AddItem Textos(16)
            sdbddValor.AddItem Textos(15)
        End If
    End If
    
    Set oatrib = Nothing
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgEscalado.Columns.Item(sdbgEscalado.col).Value = sdbddValor.Columns(0).CellText(bm)
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

''' <summary>
''' Despues de updatar (en grid) un dato se hacen en cascada las modificaciones asociadas para la bbdd (grabar cambios escalados) y diferentes colecciones
''' </summary>
''' <param name="ColIndex">Columna modificada</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgEscalado_AfterColUpdate(ByVal ColIndex As Integer)
    Dim sIdAtrib As String
    Dim vValor As Variant
    Dim oItem As CItem
    Dim teserror As TipoErrorSummit
    Dim vPrecio As Variant
    Dim oEscalado As CEscalado
    Dim dblCantidad As Double
    Dim sCod As String
    Dim oAdj As CAdjudicacion
    Dim oOferta As COferta
    Dim oPrecio As CPrecioItem
    Dim bEspera As Boolean
    Dim vAtrib As Variant
    Dim dPrecioAplicFormula As Double
    Dim irespuesta As Integer
    Dim vPrecioOfer As Variant
    Dim iNumAtribCarac As Integer
    Dim oGrupo As CGrupo
    Dim bNuevoPresup As Boolean
    Dim dblCantAdj As Double
    Dim bHayEscAdj As Boolean
    Dim oEsc As CEscalado
    Dim bNuevoPrecio As Boolean
    
    If m_bRecalculando Then Exit Sub
    
    With sdbgEscalado
        Select Case .Grp
            Case 0
                Select Case .Columns(ColIndex).Name
                    Case "CANT"
                        Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                        
                        If .Columns("PROV").Value = "0" Then
                            Set oItem = oGrupo.Items.Item(CStr(.Columns("ID").Value))
                                                        
                            'Modificaci�n de cantidad de item
                            If Trim(.ActiveCell.Value) = "" Then
                                oItem.Cantidad = Null
                                oItem.Presupuesto = Null
                            Else
                                If Not IsEmpty(oItem.Cantidad) And Not IsNull(oItem.Cantidad) Then m_dblCantidadAnterior = oItem.Cantidad
                                oItem.Cantidad = CDbl(.ActiveCell.Value)
                                                                
                                If Not IsNull(oItem.Precio) And Not IsEmpty(oItem.Precio) Then
                                    oItem.Presupuesto = oItem.Cantidad * oItem.Precio
                                End If
                            End If
                            
                            teserror = ProcesoSeleccionado.ModificarCantidadItemEsc(oGrupo.Codigo, oItem.Id, m_bBorrarAdj)
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                .CancelUpdate
                            End If
                        Else
                            dblCantidad = IIf(Trim(.ActiveCell.Value) <> "", .ActiveCell.Value, 0)
                            
                            'Comprobar si hay escalados adjudicados
                            bHayEscAdj = False
                            For Each oEsc In oGrupo.Escalados
                                sCod = KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, oEsc.Id)
                                Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
                                If Not oAdj Is Nothing Then
                                    bHayEscAdj = True
                                    
                                    RecalcularPorEscalado oGrupo, True, .Columns("ART").Value, .Columns("ID").Value, oAdj.Escalado, , dblCantidad
                                    AdjudicacionModificada
                                End If
                                Set oAdj = Nothing
                            Next
                            Set oEsc = Nothing
                            
                            If Not bHayEscAdj Then
                                sCod = .Columns("ID").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(.Columns("ID").Value))
                                
'                                If (dblCantidad > 0 And oGrupo.Adjudicaciones.Item(.Columns("ART").Value & sCod) Is Nothing) Or _
'                                        (dblCantidad = 0 And Not oGrupo.Adjudicaciones.Item(.Columns("ART").Value & sCod) Is Nothing) Then
                                    RecalcularPorProveedor oGrupo, (dblCantidad > 0), .Columns("ART").Value, .Columns("ID").Value, dblCantidad
                                    AdjudicacionModificada
'                                End If
                            End If
                        End If
                End Select
            
            Case 1
                'Modificaci�n de atributos de caracter�sticas. S�lo es necesario guardar el nuevo valor
                sIdAtrib = .Columns(.col).Name
                Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                If Not oGrupo.AtributosItem.Item(sIdAtrib) Is Nothing Then
                    vValor = frmOrigen.ModificarValorAtributo(sdbgEscalado, .Columns("ID").Value, sIdAtrib, ColIndex)
                    If IsNull(vValor) Then
                        .CancelUpdate
                        Exit Sub
                    End If
                End If
                
            Case Else
                Select Case Left(.Columns(ColIndex).Name, 4)
                    Case "PRES"
                        If .Columns("PROV").Value = "0" Then
                            'Modificaci�n del presupuesto del item por escalado
                            If Trim(.ActiveCell.Value) <> "" Then
                                If Not IsNumeric(.ActiveCell.Value) Then
                                    oMensajes.NoValido Textos(7)
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                            
                            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                            Set oItem = oGrupo.Items.Item(CStr(.Columns("ID").Value))
                                                    
                            bNuevoPresup = False
                            Set oEscalado = oItem.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value))
                            If oEscalado Is Nothing Then
                                Set oEscalado = oFSGSRaiz.Generar_CEscalado
                                oEscalado.Id = .Columns("ESC" & .Grp).Value
                                oEscalado.Inicial = .Columns("INI" & .Grp).Value
                                If .Columns("FIN" & .Grp).Value <> "" Then oEscalado.final = .Columns("FIN" & .Grp).Value
                                bNuevoPresup = True
                            End If
                            
                            If ProcesoSeleccionado.Estado < conadjudicaciones And ProcesoSeleccionado.Bloqueado Then
                                frmOrigen.cmdGuardar.Enabled = True
                                ProcesoSeleccionado.GuardarProceso = True
                            ElseIf ProcesoSeleccionado.Estado > ParcialmenteCerrado Then
                                ProcesoSeleccionado.ModificadoHojaAdj = True
                            End If
                            
                            vPrecio = oItem.Precio 'Guardo el anterior para hacer las cuentas
                            If .ActiveCell.Value = "" Then
                                oEscalado.Presupuesto = Null
                            Else
                                oEscalado.Presupuesto = .ActiveCell.Value
                            End If
                            
                            oItem.TipoEscalados = oGrupo.TipoEscalados
                            teserror = oItem.ModificarPresupuestoEscalado(oEscalado.Id, oEscalado.Presupuesto, oEscalado.Inicial, oEscalado.final, bNuevoPresup)
                            If teserror.NumError <> TESnoerror Then
                                If teserror.NumError = TESVolumenAdjDirSuperado Then
                                    If NullToStr(ProcesoSeleccionado.IgnorarRestricAdjDirecta) = "" Then
                                        basErrores.TratarError teserror
                                        If Not ProcesoSeleccionado.PermSaltarseVolMaxAdjDir Then
                                            .CancelUpdate
                                            Exit Sub
                                        Else
                                            basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompModPres, "Anyo:" & CStr(ProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(ProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(ProcesoSeleccionado.Cod) & " Item:" & .Columns("ID").Value
                                        End If
                                    Else
                                        basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompModPres, "Anyo:" & CStr(ProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(ProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(ProcesoSeleccionado.Cod) & " Item:" & .Columns("ID").Value
                                    End If
                                Else
                                    basErrores.TratarError teserror
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            Else
                                If bNuevoPresup Then oItem.Escalados.Add oEscalado.Id, oEscalado.Inicial, oEscalado.final, oEscalado.Presupuesto
                                basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompModPres, "Anyo:" & CStr(ProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(ProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(ProcesoSeleccionado.Cod) & " Item:" & .Columns("ID").Value
                                m_bRecalcular = True
                            End If
                        Else
                            'Modificaci�n de los precios de la oferta
                            
                            If Trim(.ActiveCell.Value) <> "" Then
                                If Not IsNumeric(.ActiveCell.Value) Then
                                    oMensajes.NoValido Textos(11)
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                                                      
                            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                            Set oItem = oGrupo.Items.Item(CStr(.Columns("ART").Value))
                            
                            If ProcesoSeleccionado.Estado < conadjudicaciones And ProcesoSeleccionado.Bloqueado Then
                                frmOrigen.cmdGuardar.Enabled = True
                                ProcesoSeleccionado.GuardarProceso = True
                            ElseIf ProcesoSeleccionado.Estado > ParcialmenteCerrado Then
                                ProcesoSeleccionado.ModificadoHojaAdj = True
                            End If
                            vAtrib = Null
                            
                            Set oOferta = oGrupo.UltimasOfertas.Item(Trim(.Columns("ID").Value))
                            'Comprueba que no haya atributos aplicados al precio
                            If Not oOferta Is Nothing Then
                                If Not oOferta.Lineas.Item(CStr(oItem.Id)) Is Nothing Then
                                    If Not oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(sdbgEscalado.Groups(sdbgEscalado.Grp).TagVariant)) Is Nothing Then
                                        With oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(sdbgEscalado.Groups(sdbgEscalado.Grp).TagVariant))
                                            'Si el precio de la oferta es distinto al precio sin atributos es pq hay atribs aplicados
                                            dPrecioAplicFormula = NullToDbl0(.Precio)
                                              
                                            'Si hay atributos aplicados de precio se tienen que desaplicar para cambiar el precio
                                            If CDec(NullToDbl0(dPrecioAplicFormula)) <> CDec(NullToDbl0(.PrecioOferta)) Then
                                                vAtrib = frmOrigen.DevolverAtributosAplicadosPrecUnitario(oGrupo.Codigo, oItem.Id, sdbgEscalado.Columns("ID").Value)
                                                If Not IsNull(vAtrib) Then
                                                    irespuesta = oMensajes.PreguntaModificarPrecio(vAtrib)
                                                    If irespuesta = vbNo Then
                                                        sdbgEscalado.CancelUpdate
                                                        Exit Sub
                                                    End If
                                                End If
                                            End If
                                        End With
                                    End If
                                End If
                            End If
                            
                            If Not oOferta Is Nothing Then
                                bNuevoPrecio = False
                                
                                Set oPrecio = oOferta.Lineas.Item(CStr(oItem.Id))
                                If Not oPrecio.Escalados.Item(CStr(.Groups(.Grp).TagVariant)) Is Nothing Then
                                    vPrecio = oPrecio.Escalados.Item(CStr(.Groups(.Grp).TagVariant)).PrecioOferta
                                Else
                                    bNuevoPrecio = True
                                    oPrecio.Escalados.Add .Groups(.Grp).TagVariant, oGrupo.Escalados.Item(CStr(.Groups(.Grp).TagVariant)).Inicial, oGrupo.Escalados.Item(CStr(.Groups(.Grp).TagVariant)).final
                                End If
                                If .Columns(ColIndex).Value <> vbNullString Then
                                    oPrecio.Escalados.Item(CStr(.Groups(.Grp).TagVariant)).PrecioOferta = CDec(StrToDbl0(.Columns(ColIndex).Value) * NullToDbl0(oItem.CambioComparativa(Trim(.Columns("ID").Value), True, m_oAdjsProve)))
                                Else
                                    oPrecio.Escalados.Item(CStr(.Groups(.Grp).TagVariant)).PrecioOferta = Null
                                End If
                                vPrecioOfer = oPrecio.Escalados.Item(CStr(.Groups(.Grp).TagVariant)).PrecioOferta
                                oPrecio.Escalados.Item(CStr(.Groups(.Grp).TagVariant)).Precio = oPrecio.Escalados.Item(CStr(.Groups(.Grp).TagVariant)).PrecioOferta
                                        
                                'Si se ha modificado el precio, y hay cantidad en el item, y esta se corresponde con la del escalado del que se ha
                                'modificado el precio hay que recalcular la oferta.
                                bEspera = False
                                If NullToDbl0(.Columns(ColIndex).Value) <> NullToDbl0(vPrecio) And Not (IsNull(oItem.Cantidad) Or IsEmpty(oItem.Cantidad)) Then
                                    Set oEscalado = oGrupo.Escalados.Item(CStr(.Groups(.Grp).TagVariant))
                                    If (oGrupo.TipoEscalados = ModoDirecto And oEscalado.Inicial = oItem.Cantidad) Or _
                                        (oGrupo.TipoEscalados = ModoRangos And oEscalado.Inicial <= oItem.Cantidad And oEscalado.final >= oItem.Cantidad) Then
                                        bEspera = True
                                        oOferta.Recalcular = True
                                        Set oOferta.proceso = ProcesoSeleccionado
                                    End If
                                    Set oEscalado = Nothing
                                End If
                                        
                                teserror = oPrecio.ModificarPrecioEscalado(oGrupo.TipoEscalados, .Groups(.Grp).TagVariant, bNuevoPrecio)
                                                                
                                If teserror.NumError <> TESnoerror Then
                                    basErrores.TratarError teserror
                                    .CancelUpdate
                                    Exit Sub
                                Else
                                    basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompModPrec, "Anyo:" & CStr(ProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(ProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(ProcesoSeleccionado.Cod) & " Item:" & CStr(.Columns("ART").Value)
                                    'Registramos llamada a recalcular
                                    If bEspera Then
                                        basSeguridad.RegistrarAccion AccionesSummit.ACCCondOfeRecalculo, "Llamada desde:Comparativa Anyo:" & CStr(ProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(ProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(ProcesoSeleccionado.Cod)
                                    End If
                                End If
                                
                                'si tiene atributos de precio unitario aplicados los quita:
                                If Not IsNull(vAtrib) And Not IsNull(oPrecio.PrecioOferta) Then
                                    'si hab�a atributos de precio unitario aplicados habr� que desaplicarlos
                                    frmOrigen.DeshabilitarAtribsUnitarioItem oGrupo.Codigo, vAtrib
                                    'Aqu� de momento lo calculo todo
                                    frmOrigen.CalcularPrecioConFormulas
                                    
                                    If m_bGrupo Then
                                        RellenarGridGrupo iNumAtribCarac
                                    Else
                                        RellenarGridAll iNumAtribCarac
                                    End If
                                        
                                    frmOrigen.RecalcularTotalesGrupo
                                Else
                                    sCod = KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, .Groups(.Grp).TagVariant)
                                    If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                        RecalcularPorEscalado oGrupo, True, .Columns("ART").Value, .Columns("ID").Value, .Groups(.Grp).TagVariant, oGrupo.Adjudicaciones.Item(sCod).Adjudicado
                                    Else
                                        CalcularAdjudicacionesGrupo
                                    End If
                                End If
                                If ProcesoSeleccionado.Estado >= ConObjetivosSinNotificarYPreadjudicado And ProcesoSeleccionado.Estado < Cerrado Then
                                    sCod = KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, .Groups(.Grp).TagVariant)
                                    If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                        teserror = ProcesoSeleccionado.ActualizarTotalesVisor(frmOrigen.m_dblAdjudicadoProcReal, frmOrigen.m_dblConsumidoProcReal)
                                    End If
                                End If
                            End If
                            Set oOferta = Nothing
                        End If
                        
                    Case "AHOR"
                        If .Columns("PROV").Value = "0" Then
                            If Trim(.ActiveCell.Value) <> "" Then
                                If Not IsNumeric(.ActiveCell.Value) Then
                                    oMensajes.NoValido Textos(1)
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                            
                            'Modificaci�n del objetivo del item por escalado
                            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                            Set oItem = oGrupo.Items.Item(CStr(.Columns("ID").Value))
                            
                            bNuevoPresup = False
                            Set oEscalado = oItem.Escalados.Item(CStr(.Columns("ESC" & .Grp).Value))
                            If oEscalado Is Nothing Then
                                Set oEscalado = oFSGSRaiz.Generar_CEscalado
                                oEscalado.Id = .Columns("ESC" & .Grp).Value
                                oEscalado.Inicial = .Columns("INI" & .Grp).Value
                                If .Columns("FIN" & .Grp).Value <> "" Then oEscalado.final = .Columns("FIN" & .Grp).Value
                                bNuevoPresup = True
                            End If

                            If ProcesoSeleccionado.Estado < conadjudicaciones And ProcesoSeleccionado.Bloqueado Then
                                frmOrigen.cmdGuardar.Enabled = True
                                ProcesoSeleccionado.GuardarProceso = True
                            ElseIf ProcesoSeleccionado.Estado > ParcialmenteCerrado Then
                                ProcesoSeleccionado.ModificadoHojaAdj = True
                            End If
                            
                            If .ActiveCell.Value = "" Then
                                If oEscalado.Objetivo <> Null Then
                                    Me.ModificacionObjetivos = True
                                End If
                                oEscalado.Objetivo = Null
                            Else
                                If oEscalado.Objetivo <> CDbl(.ActiveCell.Value) Then
                                    Me.ModificacionObjetivos = True
                                End If
                                oEscalado.Objetivo = CDbl(.ActiveCell.Value)
                            End If

                            teserror = oItem.ModificarObjetivoEscalado(oEscalado.Id, oEscalado.Objetivo, Date, bNuevoPresup, Me.ModificacionObjetivos)
                            If teserror.NumError <> TESnoerror And teserror.NumError <> TESVolumenAdjDirSuperado Then
                                basErrores.TratarError teserror
                                .CancelUpdate
                                Exit Sub
                            Else
                                basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompFijObjetivos, "Anyo:" & CStr(ProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(ProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(ProcesoSeleccionado.Cod) & " Item:" & .Columns("ID").Value
                                If bNuevoPresup Then oItem.Escalados.Add oEscalado.Id, oEscalado.Inicial, oEscalado.final, Null, , , , , , oEscalado.Objetivo
                            End If
                        End If
                    Case "ADJC"
                        If .Columns("PROV").Value = "1" Then
                            'Modificaci�n de la cantidad adjudicada a un escalado
                            
                            If Trim(.ActiveCell.Value) <> "" Then
                                If Not IsNumeric(.ActiveCell.Value) Then
                                    oMensajes.NoValido Textos(10)
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                            
                            m_bCambioCantAdjEsc = True
                            m_lIdItem = .Columns("ART").Value
                            m_iRow = .Row
                            
                            If .ActiveCell.Value = "" Then
                                dblCantAdj = 0
                            Else
                                dblCantAdj = CDbl(.ActiveCell.Value)
                            End If
                            
                            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                            sCod = KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, .Groups(.Grp).TagVariant)
                            If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                                oGrupo.Adjudicaciones.Item(sCod).Adjudicado = dblCantAdj
                                RecalcularPorEscalado oGrupo, True, m_lIdItem, .Columns("ID").Value, .Groups(.Grp).TagVariant, dblCantAdj
                            Else
                                AdjudicarEscalado oGrupo, .Columns("ART").Value, .Columns("ID").Value, .Groups(.Grp).TagVariant, True, dblCantAdj
                            End If
                            AdjudicacionModificada
                        End If
                    Case Else
                        If .Columns("PROV").Value = "1" Then
                            'Modificaci�n de atributos de precio � precio/importe por escalado � objetivo por escalado
                            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                            If Not oGrupo.AtributosItem Is Nothing Then
                                Set oItem = oGrupo.Items.Item(CStr(.Columns("ART").Value))
                                'Set oEscalado = oItem.Escalados.Item(CStr(.Groups(.Grp).TagVariant))
                                sIdAtrib = Left(.Columns(.col).Name, Len(.Columns(.col).Name) - Len(CStr(.Grp)))
                                If Not oGrupo.AtributosItem.Item(sIdAtrib) Is Nothing Then
                                    vPrecio = frmOrigen.ModificarValorAtributo(sdbgEscalado, .Columns("ID").Value, sIdAtrib, ColIndex, CLng(.Groups(.Grp).TagVariant))
                                    If IsNull(vPrecio) Then
                                        .CancelUpdate
                                        Exit Sub
                                    Else
                                        If .Columns("ADJCANT" & .Grp).Value = "" Then
                                            dblCantAdj = 0
                                        Else
                                            dblCantAdj = CDbl(.Columns("ADJCANT" & .Grp).Value)
                                        End If
                                        RecalcularPorEscalado oGrupo, True, oItem.Id, .Columns("ID").Value, CLng(.Groups(.Grp).TagVariant), dblCantAdj
                                    End If
                                End If
                            End If
                        End If
                End Select
        End Select
        
        'Cuando hay cambio de la cant. adjudicada a un escalado no se llama al Update porque hay que dar la oportunidad
        'al usaurio de rellenar las cantidades adjudicadas a cada escalado antes de hacer el Update de la l�nea
        If Not m_bCambioCantAdjEsc Then .Update
    End With
    
    Set oGrupo = Nothing
    Set oItem = Nothing
    Set oEscalado = Nothing
End Sub
''' <summary>
''' Despues de updatar (en grid) un dato se hacen en cascada las modificaciones asociadas para la bbdd (grabar cambios escalados) y diferentes colecciones
''' </summary>
''' <param name="RtnDispErrMsg">Buffer con los datos y bookmark de la fila</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgEscalado_AfterUpdate(RtnDispErrMsg As Integer)
    Dim oEscalado As CEscalado
    Dim oAdj As CAdjudicacion
    Dim iRow As Integer
    Dim i As Integer
    Dim sCod As String
    Dim oProve As CProveedor
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim oAsig As COferta
    Dim sAhorro As String
    Dim dblPresup As Double
    Dim bRecalc As Boolean
    Dim scodProve As String
    
    If m_bRecalculando Then Exit Sub
    
    If m_bBorrarAdj Then
        LockWindowUpdate UserControl.hWnd
        
        Set oGrupo = ProcesoSeleccionado.Grupos.Item(sdbgEscalado.Columns("GRUPOCOD").Value)
        For Each oAdj In oGrupo.Adjudicaciones
            If oAdj.Id = m_lIdItem Then
                DesadjudicarEscalado oGrupo, oAdj.Id, oAdj.ProveCod, oAdj.Escalado, bRecalc
                frmOrigen.cmdGuardar.Enabled = True
            End If
        Next
        Set oAdj = Nothing
        
        m_bBorrarAdj = False
        
        LockWindowUpdate 0&
    ElseIf m_bRecalcularItem Then
        LockWindowUpdate frmOrigen.hWnd
        LockWindowUpdate UserControl.hWnd
                        
        Set oGrupo = ProcesoSeleccionado.Grupos.Item(sdbgEscalado.Columns("GRUPOCOD").Value)
        For Each oAdj In m_oAdjsProve
            'S�lo es necesario recalcular las adjudicaciones de los proveedores que no tienen cantidad adjudicada para mostrar
            'los datos correctos con respecto a la nueva cantidad de item
            If oAdj.Id = m_lIdItem Then
                For Each oEscalado In oGrupo.Escalados
                    sCod = KeyEscalado(oAdj.ProveCod, m_lIdItem, oEscalado.Id)
                    If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        RecalcularPorEscalado oGrupo, True, m_lIdItem, oAdj.ProveCod, oEscalado.Id, oGrupo.Adjudicaciones.Item(sCod).Adjudicado
                    End If
                Next
            End If
        Next
        Set oAdj = Nothing
                
        m_bRecalcularItem = False
        
        LockWindowUpdate 0&
    ElseIf m_bCambioCantAdjEsc Then
        LockWindowUpdate frmOrigen.hWnd
        LockWindowUpdate UserControl.hWnd
                        
        With sdbgEscalado
            iRow = .Row
            .Row = m_iRow
            scodProve = .Columns("ID").Value
            .Row = iRow
            
            Set oGrupo = ProcesoSeleccionado.Grupos.Item(sdbgEscalado.Columns("GRUPOCOD").Value)
            For Each oAdj In oGrupo.Adjudicaciones
                If oAdj.Id = m_lIdItem And (oAdj.ProveCod = scodProve Or oAdj.Adjudicado = 0) Then
                    RecalcularPorEscalado oGrupo, True, m_lIdItem, oAdj.ProveCod, oAdj.Escalado, oAdj.Adjudicado
                End If
            Next
            Set oAdj = Nothing
            Set oGrupo = Nothing
        End With
        
        m_bCambioCantAdjEsc = False
        
        LockWindowUpdate 0&
    ElseIf m_bRecalcular Then
        m_bRecalcular = False
        
        LockWindowUpdate frmOrigen.hWnd
        LockWindowUpdate UserControl.hWnd
        
        With sdbgEscalado
            iRow = .Row
            .Row = m_iFilaItem
            
            If .Columns("PRES" & m_iGrupo).Value <> "" Then
                dblPresup = .Columns("PRES" & m_iGrupo).Value
            End If
                
            'Si hay adjudicaciones hay que recalcularlas
            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
            For Each oProve In ProvesAsig
                sCod = KeyEscalado(oProve.Cod, .Columns("ID").Value, .Groups(m_iGrupo).TagVariant)
                If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                    RecalcularPorEscalado oGrupo, True, .Columns("ID").Value, oProve.Cod, .Groups(m_iGrupo).TagVariant, oGrupo.Adjudicaciones.Item(sCod).Adjudicado
                    AdjudicacionModificada
                End If
            Next
            
            'Recalcular objetivos
            i = m_iFilaItem + 1
            .Row = i
            Set oItem = oGrupo.Items.Item(CStr(.Columns("ART").Value))
            While .Columns("PROV").Value = "1" And i < .Rows
                sAhorro = vbNullString
                
                If dblPresup > 0 Then
                    Set oAsig = oGrupo.UltimasOfertas.Item(Trim(.Columns("ID").Value))
                    If Not oAsig Is Nothing Then
                        If Not oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(.Groups(m_iGrupo).TagVariant)) Is Nothing Then
                            If Not IsNull(oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(.Groups(m_iGrupo).TagVariant)).PrecioOferta) Then
                                sAhorro = CStr(dblPresup - (oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(.Groups(m_iGrupo).TagVariant)).PrecioOferta / oItem.CambioComparativa(oProve.Cod, True, m_oAdjsProve)))
                            End If
                        End If
                    End If
                End If
                
                .Columns("AHORRO" & m_iGrupo).Value = sAhorro
                
                i = i + 1
                .Row = i
            Wend
            
            Set oAsig = Nothing
            Set oItem = Nothing
            Set oGrupo = Nothing
            
            .Row = iRow
        End With
        
        LockWindowUpdate 0&
    End If
End Sub

Private Sub sdbgEscalado_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim oAdj As CAdjudicacion
    Dim bHayAdj As Boolean
    Dim vCantidad As Variant
    Dim bComprobarAdj As Boolean
    Dim dblSumaCantAdj As Double
    Dim oGrupo As CGrupo
    Dim dblCantAdj As Double
    
    If m_bRecalculando Then Exit Sub
    
    With sdbgEscalado
        Select Case .Grp
            Case 0
                If .Columns(ColIndex).Name = "CANT" Then
                    If .Columns("PROV").Value = "0" Then
                        'Comprobar si el usuario tiene permisos para modificar el item en la apertura
                        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
                            If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEModificarArti)) Is Nothing Then
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                        
                        Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                        
                        'Si se est� modificando la cantidad del item
                        If .ActiveCell.Value <> vbNullString Then
                            If Not IsNumeric(.ActiveCell.Value) Or .ActiveCell.Value = "0" Then
                                oMensajes.NoValido Textos(2)
                                .CancelUpdate
                                Exit Sub
                            End If
                        End If
                        
                        vCantidad = oGrupo.Items.Item(.Columns("ID").Value).Cantidad
                            
                        If Not IsNull(vCantidad) And Not IsEmpty(vCantidad) Then
                            If Trim(.Columns("CANT").Value) <> vbNullString Then
                                If CDbl(OldValue) > CDbl(.Columns("CANT").Value) Then
                                    bComprobarAdj = True
                                Else
                                    'Si hay proveedores con adjudicaciones sin cantidad
                                    For Each oAdj In AdjsProve
                                        If oAdj.Adjudicado = 0 Then
                                            bComprobarAdj = True
                                            Exit For
                                        End If
                                    Next
                                End If
                            Else
                                bComprobarAdj = True
                            End If
                        Else
                            If .Columns("CANT").Value <> vbNullString Then
                                bComprobarAdj = True
                            End If
                        End If
                                                
                        If bComprobarAdj Then
                            bHayAdj = False
                            m_bBorrarAdj = False
                            
                            For Each oAdj In oGrupo.Adjudicaciones
                                If oAdj.Id = .Columns("ID").Value Then
                                    If Not oGrupo.Items.Item(CStr(oAdj.Id)) Is Nothing Then
                                        bHayAdj = True
                                        dblCantAdj = dblCantAdj + NullToDbl0(oAdj.Adjudicado)
                                    End If
                                End If
                            Next
                            Set oAdj = Nothing
                            
                            If bHayAdj Then
                                m_lIdItem = .Columns("ID").Value
                                
                                If Not IsNull(vCantidad) And Not IsEmpty(vCantidad) Then
                                    If dblCantAdj > .Columns("CANT").Value Then
                                        If oMensajes.AdjudicacionesNoCorrespondientes = vbNo Then
                                            Cancel = True
                                        Else
                                            m_bBorrarAdj = True
                                        End If
                                    Else
                                        m_bRecalcularItem = True
                                    End If
                                Else
                                    If dblCantAdj > .Columns("CANT").Value Then
                                        If oMensajes.AdjudicacionesNoCorrespondientes = vbNo Then
                                            Cancel = True
                                        Else
                                            m_bBorrarAdj = True
                                        End If
                                    Else
                                        m_bRecalcularItem = True
                                    End If
                                End If
                            End If
                        End If
                    Else
                        'Si se est� modificando la cantidad del proveedor
                        
                        If Trim(.Columns("CANT").Value) <> "" Then
                            If .ActiveCell.Value <> vbNullString Then
                                If Not IsNumeric(.ActiveCell.Value) Or .ActiveCell.Value = "0" Then
                                    oMensajes.NoValido Textos(10)
                                    .CancelUpdate
                                    Exit Sub
                                End If
                            End If
                        
                            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                                                    
                            'Comprobar que la suma de las cantidades adjudicadas no supera la del item
                            bHayAdj = False
                            For Each oAdj In m_oAdjsProve
                                If oAdj.Grupo = oGrupo.Codigo And oAdj.Id = .Columns("ART").Value Then
                                    If oAdj.ProveCod <> .Columns("ID").Value Then
                                        dblSumaCantAdj = dblSumaCantAdj + oAdj.Adjudicado
                                    Else
                                        bHayAdj = True
                                        dblSumaCantAdj = dblSumaCantAdj + .Columns("CANT").Value
                                    End If
                                End If
                            Next
                            Set oAdj = Nothing
                            
                            If Not bHayAdj Then dblSumaCantAdj = dblSumaCantAdj + .Columns("CANT").Value
                            
                            vCantidad = oGrupo.Items.Item(.Columns("ART").Value).Cantidad
                            If dblSumaCantAdj > NullToDbl0(vCantidad) Then
                                oMensajes.CantidadItemSuperada
                                Cancel = True
                            End If
                        End If
                    End If
                    
                    Set oGrupo = Nothing
                End If
            Case 1
                'Comprobar el valor del atributo
                Cancel = Not ComprobarValorAtributo(ProcesoSeleccionado, .Columns(.col).Name, .Columns(ColIndex).Value, Textos(16), Textos(15), Textos(28), _
                                Textos(29), Textos(30), AmbItem)
            Case Else
                Select Case Left(.Columns(ColIndex).Name, Len(.Columns(ColIndex).Name) - Len(CStr(.Grp)))
                    Case "PRES"
                        If .Columns("PROV").Value = "0" Then
                            m_iFilaItem = .Row
                            m_iGrupo = .Grp
                        End If
                End Select
        End Select
    End With
End Sub

''' <summary>Comprobaciones antes de actualizar una fila</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="lIdItem">Id. item</param>
''' <returns>Booleano indicando si se puede actualizar la fila</returns>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 21/03/2012</revision>
Private Function ComprobarActualizarFilaGrid(ByVal oGrupo As CGrupo, ByVal lIdItem As Long) As Boolean
    Dim bAct As Boolean
    
    ComprobarActualizarFilaGrid = True
    
    If m_bRecalculando Then Exit Function
    
    m_bComprobandoActualizarGrid = True
    
    bAct = True
                    
    'Comprobaci�n suma cantidad adjudicada
    If Not ComprobarSumaCantAdj(oGrupo, lIdItem) Then
        oMensajes.ExcesoCantAdjEscaladosItem
        bAct = False
    End If

    If bAct Then AdjudicacionModificada
    
    m_bComprobandoActualizarGrid = False
    
    ComprobarActualizarFilaGrid = bAct
End Function
''' <summary>
''' Antes de actualizar comprobar
''' </summary>
''' <param name="Cancel">si se cancela el update</param>
''' <remarks>Llamada desde: Sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgEscalado_BeforeUpdate(Cancel As Integer)
    Dim oGrupo As CGrupo
    
    If m_bRecalculando Then Exit Sub
    
    If m_bCambioCantAdjEsc Then
        With sdbgEscalado
        
            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
            If .Columns("PROV").Value = "1" Then
                If Not ComprobarActualizarFilaGrid(oGrupo, .Columns("ART").Value) Then Cancel = True
            Else
                If Not ComprobarActualizarFilaGrid(oGrupo, .Columns("ID").Value) Then Cancel = True
            End If
            Set oGrupo = Nothing
        End With
    End If
End Sub

Private Sub sdbgEscalado_BtnClick()
    Dim sProve As String
    Dim sCodGrupo As String
    
    sCodGrupo = sdbgEscalado.Columns("GRUPOCOD").Value
    If sdbgEscalado.Columns("PROV").Value = "1" Then
        sProve = sdbgEscalado.Columns("ID").Value
    End If
        
    If m_bGrupo Then
        RaiseEvent BtnGrupoClick(sdbgEscalado, sProve)
    Else
        RaiseEvent BtnAllClick(sdbgEscalado, sCodGrupo, sProve)
    End If
End Sub

''' <summary>
''' Tras mover una columna indicar a las vistas q hay cambios a grabar.
''' </summary>
''' <param name="ColIndex">Columna</param>
''' <param name="NewPos">Posicion a la q se mueve</param>
''' <param name="Cancel">Cancelar el movimiento</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgEscalado_ColMove(ByVal ColIndex As Integer, ByVal NewPos As Integer, Cancel As Integer)
    If m_bGrupo Then
        If VistaSeleccionadaGr Is Nothing Then
            Cancel = True
            Exit Sub
        End If
    Else
        If VistaSeleccionadaAll Is Nothing Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    'Si el scroll no est� en la posici�n 0 no permite mover las columnas
    If sdbgEscalado.GrpPosition(0) <> 0 Then
        Cancel = True
        Exit Sub
    End If
    
    If ColIndex = 0 Then Cancel = True
    If NewPos = 0 Then Cancel = True
    
    If m_bGrupo Then
        VistaSeleccionadaGr.HayCambios = True
    Else
        VistaSeleccionadaAll.HayCambios = True
    End If
    
    
End Sub

''' <summary>
''' Tras redimensionar una columna indicar a las vistas q hay cambios a grabar.
''' </summary>
''' <param name="ColIndex">Columna</param>
''' <param name="Cancel">Cancelar el resize</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgEscalado_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
    Dim i As Integer
    Dim sColum As String
    Dim bOcultar As Boolean
    
    
    
    'Indica que ha habido cambios en la vista
    If m_bGrupo Then
        If VistaSeleccionadaGr Is Nothing Then Exit Sub
        VistaSeleccionadaGr.HayCambios = True
    Else
        If VistaSeleccionadaAll Is Nothing Then Exit Sub
        VistaSeleccionadaAll.HayCambios = True
    End If

    With sdbgEscalado
        If .Columns(ColIndex).Group <> .Groups(0).Position And .Columns(ColIndex).Group <> .Groups(1).Position Then
            'Si se modifica una columna de uno de los grupos de escalados
            'se hace el resize tambi�n de la columna en el resto de grupos de la grid
            sColum = Mid(.Columns(ColIndex).Name, 1, Len(.Columns(ColIndex).Name) - Len(CStr(.Columns(ColIndex).Group)))
            For i = 2 To .Groups.Count - 1
                bOcultar = False
                If i <> .Grp Then
                    If .Groups(i).Visible = False Then
                        bOcultar = True
                        .Groups(i).Visible = True
                    End If
                    .Columns(sColum & i).Width = .ResizeWidth
                    If bOcultar = True Then
                        .Groups(i).Visible = False
                    End If
                     
                End If
            Next i
        End If
    End With
End Sub

''' <summary>
''' Dependiendo de donde pulses: adjudicar/DesAdjudicar el presupuesto � mostrar el detalle del item
''' </summary>
''' <returns>Llamada desde: sistema ; Tiempo m�ximo: 0,2</returns>
Private Sub sdbgEscalado_DblClick()
    Dim oGrupo As CGrupo
    Dim dblCantEsc As Double
    Dim dblCantProve As Double
    
    If m_bMouseUpDblClick Then Exit Sub
        
    With sdbgEscalado
        'If .Columns("CERRADO").Value Then Exit Sub
        Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
        
        If oGrupo.Cerrado = 1 Then Exit Sub

        'Si se ha hecho doble click  sobre un item mostrar el detalle del item
        Select Case .Grp
            Case 0
                If .Columns("PROV").Value = "0" Then
                    RaiseEvent ItemDblClick(sdbgEscalado, oGrupo.Items.Item(CStr(.Columns("ID").Value)))
                Else
                    Screen.MousePointer = vbHourglass
                    LockWindowUpdate frmOrigen.hWnd
                    LockWindowUpdate frmOrigen.hWnd
                    
                    'Si hay cantidad en el item ser� esa la cantidad a adjudicar
                    dblCantProve = NullToDbl0(oGrupo.Items.Item(.Columns("ART").Value).Cantidad)
                        
                    AdjudicarEscaladosProve oGrupo, .Columns("ID").Value, .Columns("ART").Value, dblCantProve
                    
                    LockWindowUpdate 0&
                    Screen.MousePointer = vbDefault
                End If
            Case 1
            Case Else
                If .Grp > -1 Then
                    If Left(.Columns(.col).Name, 4) = "PRES" And .Columns("PROV").Value = "1" Then
                        LockWindowUpdate frmOrigen.hWnd
                        LockWindowUpdate UserControl.hWnd
                                                                                                                
                        If Trim(.Columns("ADJCANT" & .Grp).Value) <> "" Then
                            dblCantEsc = .Columns("ADJCANT" & .Grp).Value
                        Else
                            dblCantEsc = 0
                        End If
                        
                        If Trim(.Columns("CANT").Value) <> "" Then
                            dblCantProve = .Columns("CANT").Value
                        Else
                            dblCantProve = 0
                        End If
                        
                        If dblCantProve > 0 Then
                            AdjudicarDesadjudicarEscalado oGrupo, .Columns("ID").Value, .Columns("ART").Value, .Columns("ESC" & .Grp).Value, _
                                (.Columns("ADJ" & .Grp).Value = "0"), dblCantEsc, dblCantProve
                        Else
                            AdjudicarDesadjudicarEscalado oGrupo, .Columns("ID").Value, .Columns("ART").Value, .Columns("ESC" & .Grp).Value, _
                                (.Columns("ADJ" & .Grp).Value = "0"), dblCantEsc
                        End If
                        
                        LockWindowUpdate 0&
                    End If
                End If
        End Select
    End With
End Sub

''' <summary>Se adjudican todos los escalados de un proveedor</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="sProve">Cod. proveedor</param>
''' <param name="lItem">Id. del item</param>
''' <param name="dblCantidad">Cantidad a adjudicar</param>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>21/03/2012</revision>

Private Sub AdjudicarEscaladosProve(ByVal oGrupo As CGrupo, ByVal sProve As String, ByVal lItem As Long, ByVal dblCantidad As Double)
    Dim oProve As CProveedor
    Dim oEsc As CEscalado
    Dim bHayOfertas As Boolean
    
    bHayOfertas = False
    
    'Comprobar que hay ofertas
    If Not oGrupo.UltimasOfertas.Item(Trim(sProve)) Is Nothing Then
        If Not oGrupo.UltimasOfertas.Item(Trim(sProve)).Lineas.Item(CStr(lItem)) Is Nothing Then
            For Each oEsc In oGrupo.Escalados
                If Not oGrupo.UltimasOfertas.Item(Trim(sProve)).Lineas.Item(CStr(lItem)).Escalados.Item(CStr(oEsc.Id)) Is Nothing Then
                    If Not IsNull(oGrupo.UltimasOfertas.Item(Trim(sProve)).Lineas.Item(CStr(lItem)).Escalados.Item(CStr(oEsc.Id)).PrecioOferta) Then
                        bHayOfertas = True
                        Exit For
                    End If
                End If
            Next
        End If
    End If
    
    If bHayOfertas Then
        'Eliminar las adjudicaciones existentes
        For Each oProve In ProvesAsig
            For Each oEsc In oGrupo.Escalados
                If Not oGrupo.Adjudicaciones.Item(KeyEscalado(oProve.Cod, lItem, oEsc.Id)) Is Nothing Then
                    AdjudicarDesadjudicarEscalado oGrupo, oProve.Cod, lItem, oEsc.Id, False
                End If
            Next
        Next
        Set oProve = Nothing
                
        'Se adjudicar�n todos los escalados del proveedor seleccionado.
        For Each oEsc In oGrupo.Escalados
            If Not IsNull(PrecioOfertaEscalado(oGrupo, sProve, lItem, oEsc.Id)) Then
                If dblCantidad > 0 Then
                    AdjudicarDesadjudicarEscalado oGrupo, sProve, lItem, oEsc.Id, True, , dblCantidad
                Else
                    AdjudicarDesadjudicarEscalado oGrupo, sProve, lItem, oEsc.Id, True
                End If
            End If
        Next
    End If
    
    Set oEsc = Nothing
End Sub

''' <summary>Devuelve la fila correspondiente al item para el proveedor actual</summary>
''' <param name="IdItem">Id. del item</param>
''' <returns>La fila del item</returns>
''' <remarks>Llamada desde: sdbgEscalado_DblClick, CantidadEscalado, CantidadProveedor</remarks>
''' <revision>21/03/2012</revision>

Private Function FilaItem(ByVal IdItem As Long) As Integer
    Dim i As Integer
    Dim vbm As Variant
        
    With sdbgEscalado
        For i = 0 To .Rows - 1
            vbm = .AddItemBookmark(i)
            If .Columns("PROV").CellValue(vbm) = "0" Then
                If .Columns("ID").CellValue(vbm) = IdItem Then
                    Exit For
                End If
            End If
        Next i
    End With
    
    FilaItem = i
End Function

''' <summary>Gestiona la adjudicaci�n/desadjudicaci�n de un escalado</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="sCodProve">Cod. proveedor</param>
''' <param name="lIdItem">Id del item</param>
''' <param name="lIdEscalado">Id del escalado</param>
''' <param name="bAdjudicar">Indica si es adjudicaci�n o desadjudicaci�n</param>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 20/02/2012</revision>

Private Sub AdjudicarDesadjudicarEscalado(ByRef oGrupo As CGrupo, ByVal scodProve As String, ByVal lIdItem As Long, ByVal lIdEscalado As Long, _
        ByVal bAdjudicar As Boolean, Optional ByVal dblCantEsc As Variant, Optional ByVal dblCantProve As Variant)
    Dim bRecalcAdjsItem As Boolean
    Dim bOk As Boolean
    Dim oAdj As CAdjudicacion
                            
    With sdbgEscalado
        If bAdjudicar Then
            bOk = AdjudicarEscalado(oGrupo, lIdItem, scodProve, lIdEscalado, bRecalcAdjsItem, dblCantEsc, dblCantProve)
        Else
            bOk = DesadjudicarEscalado(oGrupo, lIdItem, scodProve, lIdEscalado, bRecalcAdjsItem, dblCantEsc, dblCantProve)
        End If
        
        If bOk Then AdjudicacionModificada
    End With
    
    If bRecalcAdjsItem Then
        For Each oAdj In oGrupo.Adjudicaciones
            If oAdj.Id = lIdItem And (oAdj.ProveCod <> scodProve Or (oAdj.ProveCod = scodProve And oAdj.Escalado <> lIdEscalado)) Then
                If Not IsNull(oAdj.Escalado) Then
                    AdjudicarEscalado oGrupo, lIdItem, oAdj.ProveCod, oAdj.Escalado, bRecalcAdjsItem, oAdj.Adjudicado, dblCantProve
'                Else
'                    RecalcularPorProveedor oGrupo, True, lIdItem, oAdj.ProveCod, oAdj.Adjudicado
                End If
            End If
        Next
        Set oAdj = Nothing
    End If
End Sub

''' <summary>Realiza las acciones necesarias despu�s de que se ha modificado una adjudicaci�n</summary>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 21/03/2012</revision>

Private Sub AdjudicacionModificada()
    If ProcesoSeleccionado.Estado < conadjudicaciones And ProcesoSeleccionado.Bloqueado Then
        frmOrigen.cmdGuardar.Enabled = True
        ProcesoSeleccionado.GuardarProceso = True
    ElseIf ProcesoSeleccionado.Estado > ParcialmenteCerrado Then
        ProcesoSeleccionado.ModificadoHojaAdj = True
    End If
End Sub

''' <summary>Realiza la adjudicaci�n de un escalado de un proveedor</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="lIdItem">Id. �tem</param>
''' <param name="sCodProve">Cod. proveedor</param>
''' <param name="IdEscalado">Id del escalado a adjudicar</param>
''' <param name="bRecalcAdjsItem">Indica si hay que recalcular el resto de adjudicaciones del item</param>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <returns>Booleano indicando si se ha producido la adjudicaci�n</returns>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 17/02/2012</revision>

Private Function AdjudicarEscalado(ByRef oGrupo As CGrupo, ByVal lIdItem As Long, ByVal scodProve As String, ByVal IdEscalado As Long, _
        ByRef bRecalcAdjsItem As Boolean, Optional ByVal dblCantEsc As Variant, Optional ByVal dblCantProve As Variant) As Boolean
    
    AdjudicarEscalado = False
    
    With sdbgEscalado
        If .Columns("CERRADO").Value Then Exit Function
         
        'Si no hay precio no se puede adjudicar
        If IsNull(PrecioOfertaEscalado(oGrupo, scodProve, lIdItem, IdEscalado)) Then Exit Function
                        
        'Actualizar las colecciones de frmADJ
        RecalcularPorEscalado oGrupo, True, lIdItem, scodProve, IdEscalado, dblCantEsc, dblCantProve
        
        bRecalcAdjsItem = ComprobarRecalcular(oGrupo, lIdItem, dblCantEsc)
        
        .Update
    End With
    
    AdjudicarEscalado = True
End Function

''' <summary>Deshace la la adjudicaci�n de un escalado de un proveedor</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="lIdItem">Id. �tem</param>
''' <param name="sCodProve">Cod. proveedor</param>
''' <param name="IdEscalado">Id del escalado a adjudicar</param>
''' <param name="bRecalcAdjsItem">Indica si hay que recalcular el resto de adjudicaciones del item</param>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: sdbgEscalado_DblClick</remarks>
''' <revision>LTG 17/02/2012</revision>

Private Function DesadjudicarEscalado(ByRef oGrupo As CGrupo, ByVal lIdItem As Long, ByVal scodProve As String, ByVal IdEscalado As Long, _
        ByRef bRecalcAdjsItem As Boolean, Optional ByVal dblCantEsc As Variant, Optional ByVal dblCantProve As Variant) As Boolean
    DesadjudicarEscalado = False
    
    
    With sdbgEscalado
        If .Columns("CERRADO").Value Then Exit Function
                        
        'Actualizar las colecciones de frmADJ
        RecalcularPorEscalado oGrupo, False, lIdItem, scodProve, IdEscalado
                
        bRecalcAdjsItem = ComprobarRecalcular(oGrupo, lIdItem, dblCantEsc)
        
        .Update
    End With
    
    DesadjudicarEscalado = True
End Function

''' <summary>Comprueba si hay que hacer un rec�lculo de las adjudicaciones</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="lIdItem">Id. �tem</param>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <returns>Booleano indicando si hay que recalcular</returns>
''' <remarks>Llamada desde: AdjudiocarEscalado, Desadjudicarescalado</remarks>
''' <revision>LTG 16/04/2012</revision>

Private Function ComprobarRecalcular(ByRef oGrupo As CGrupo, ByVal lIdItem As Long, Optional ByVal dblCantEsc As Variant) As Boolean
    Dim oAdj As CAdjudicacion
    
    ComprobarRecalcular = False
    
    'Si no hay cantidades adjudicadas hay que recalcular
    If IsMissing(dblCantEsc) Then
        ComprobarRecalcular = True
    ElseIf dblCantEsc = 0 Then
        ComprobarRecalcular = True
    Else
        For Each oAdj In oGrupo.Adjudicaciones
            If oAdj.Id = lIdItem Then
                If NullToDbl0(oAdj.Adjudicado) = 0 Then
                    ComprobarRecalcular = True
                    Exit For
                End If
            End If
        Next
        Set oAdj = Nothing
    End If
End Function

''' <summary>Actualiza las colecciones de frmADJ para que los cambios efectuados en el control tengan reflejo en el formulario</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="bAdjudicar">Indica si se trata de una adjudicaci�n o desadjudicaci�n</param>
''' <param name="lItem">Id del item que se est� modificando</param>
''' <param name="sProv">C�digo del proveedor</param>
''' <param name="lIdEscalado">Id del escalado</param>
''' <param name="dblCantEsc">Cantidad adjudicada al escalado</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: AdjudicarEscalado, DesadjudicarEscalado</remarks>
''' <revision>21/03/2012</revision>

Private Sub RecalcularPorEscalado(ByRef oGrupo As CGrupo, ByVal bAdjudicar As Boolean, ByVal lItem As Long, ByVal sProv As String, ByVal lIdEscalado As Long, _
        Optional ByVal dblCantEsc As Variant, Optional ByVal dblCantProve As Variant)
    Dim dblCantidadEscalado As Double
    Dim dblImporteEscalado As Double
    Dim vCantidadItem As Variant
    Dim dblConsumidoProve As Double
    Dim oAdj As CAdjudicacion
    Dim oAdjProve As CAdjudicacion
    Dim oAdjProveReal As CAdjudicacion
    Dim oOferta As COferta
    Dim oItem As CItem
    Dim oEscalado As CEscalado
    Dim oEsc As CEscalado
    Dim oProve As CProveedor
    Dim sCod As String
    Dim scod1 As String
    Dim scodProve As String
    Dim bDistintos As Boolean
    Dim bAdjudicado As Boolean
    Dim i As Integer
    Dim dblCantidadProveedor As Double
    Dim dblImporteAdjudicadoProveedor As Double
    Dim dblImporteAdjudicadoProveedorTot As Double
    Dim dblImporteAhorroProve As Double
    Dim dblPorcenAhorroProve As Double
    Dim vbm As Variant
    Dim vBmFilaItem As Variant
    Dim iFilaItem As Variant
    Dim dblCantidadItem As Double
    Dim dblImporteAdjudicadoItem As Double
    Dim dblImporteAhorroItem As Double
    Dim dblPorcenAhorroItem As Double
    Dim dblConsumidoItem As Double
    Dim dblPorcenAnt As Double
    Dim bHayOtrosEscAdj As Boolean
    Dim dblPorcen As Double
    Dim vPrecio As Variant
    Dim vObjetivo As Variant
    Dim dblImporteAux As Double
    Dim vBookmark As Variant
    Dim iNumAdjProve As Integer
    Dim iNumAdjEsc As Integer
    Dim dblCambio As Double
    
    m_bRecalculando = True
    
    'Obtenci�n de objetos
    Set oItem = oGrupo.Items.Item(CStr(lItem))
    scodProve = sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
    sCod = KeyEscalado(sProv, lItem, lIdEscalado)
    Set oEscalado = oItem.Escalados.Item(CStr(lIdEscalado))
    'Si existe una adjudicaci�n a nivel de proveedor se elimina
    If Not oGrupo.Adjudicaciones.Item(CStr(lItem) & scodProve) Is Nothing Then
        oGrupo.Adjudicaciones.Remove (CStr(lItem) & scodProve)
    End If
    Set oAdj = oGrupo.Adjudicaciones.Item(sCod)
    Set oOferta = oGrupo.UltimasOfertas.Item(Trim(sProv))
    Set oAdjProve = m_oAdjsProve.Item(CStr(lItem) & scodProve)
    Set oAdjProveReal = AdjsProve.Item(CStr(lItem) & scodProve)
    
    scod1 = CStr(oGrupo.Codigo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
    
    With oItem
        dblPorcenAnt = .PorcenCons
        dblCambio = oItem.CambioComparativa(sProv, True, m_oAdjsProve)
        
        'Modifico las colecciones y luego las grids
        frmOrigen.m_dblConsumidoProc = frmOrigen.m_dblConsumidoProc - .Consumido
        frmOrigen.m_dblConsumidoProcReal = frmOrigen.m_dblConsumidoProcReal - .Consumido
        frmOrigen.m_dblAdjudicadoSinProc = frmOrigen.m_dblAdjudicadoSinProc - oGrupo.AdjudicadoTotal
        frmOrigen.m_dblAdjudicadoSinProcReal = frmOrigen.m_dblAdjudicadoSinProcReal - oGrupo.AdjudicadoTotalReal
        oGrupo.Adjudicado = oGrupo.Adjudicado - .ImporteAdj
        oGrupo.AdjudicadoReal = oGrupo.AdjudicadoReal - .ImporteAdj
        oGrupo.Consumido = oGrupo.Consumido - .Consumido
        
        'Restamos lo adjudicado al grupo para ese prove
        oGrupo.AdjudicadoTotal = oGrupo.AdjudicadoTotal - frmOrigen.m_oAdjs.Item(scod1).AdjudicadoMonProce
        
        'Si hab�a adjudicaci�n resto los datos anteriores y sumo los nuevos
        If Not oAdjProve Is Nothing Then
            If Not frmOrigen.m_oAdjs.Item(scod1) Is Nothing Then
                vPrecio = .Precio
                vObjetivo = .Objetivo
                If IsNull(.Precio) Then
                    'Calcular el precio del item en funci�n de las adjudicaciones
                    PresupuestoYObjetivoItemConEscalados oGrupo, oItem, ProvesAsig, vPrecio, vObjetivo
                End If
                
                'Resto lo anterior, el consumido y el importe adjudicado con atribs de total item
                frmOrigen.m_oAdjs.Item(scod1).Consumido = frmOrigen.m_oAdjs.Item(scod1).Consumido - NullToDbl0(NullToDbl0(.Cantidad * dblPorcenAnt) / 100) * NullToDbl0(NullToDbl0(vPrecio) * dblCambio)
                frmOrigen.m_oAdjs.Item(scod1).ConsumidoMonProce = frmOrigen.m_oAdjs.Item(scod1).ConsumidoMonProce - (NullToDbl0(.Cantidad * dblPorcenAnt) / 100 * NullToDbl0(vPrecio))
                frmOrigen.m_oAdjs.Item(scod1).ConsumidoReal = frmOrigen.m_oAdjs.Item(scod1).ConsumidoReal - NullToDbl0(NullToDbl0(.Cantidad * dblPorcenAnt) / 100) * NullToDbl0(NullToDbl0(vPrecio) * dblCambio)
                frmOrigen.m_oAdjs.Item(scod1).ConsumidoRealMonProce = frmOrigen.m_oAdjs.Item(scod1).ConsumidoRealMonProce - (NullToDbl0(.Cantidad * dblPorcenAnt) / 100 * NullToDbl0(vPrecio))
                If Not oAdjProveReal Is Nothing Then
                    frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSin = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSin - oAdjProveReal.ImporteAdjTot
                    frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinMonProce = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinMonProce - (oAdjProveReal.ImporteAdjTot / dblCambio)
                    frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinReal = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinReal - oAdjProveReal.ImporteAdjTot
                    frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinRealMonProce = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinRealMonProce - (oAdjProveReal.ImporteAdjTot / dblCambio)
                    frmOrigen.m_oAdjs.Item(scod1).AdjudicadoItems = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoItems - oAdjProveReal.ImporteAdj
                    frmOrigen.m_oAdjs.Item(scod1).AdjudicadoItemsMonproce = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoItemsMonproce - (oAdjProveReal.ImporteAdj / dblCambio)
                End If
            End If
            'Datos de grupo
            'Importe del item sin atribs de total item
            If Not oAdjProveReal Is Nothing Then
                .ImporteAdj = .ImporteAdj - CDec(oAdjProveReal.ImporteAdjTot / dblCambio)
            End If
        End If
        
        'Elimino el total de adjudicaciones de grupo para esta oferta.
        Dim objAdj As CAdjGrupo
        Dim scod2 As String
        For Each objAdj In frmOrigen.m_oAdjs
            If objAdj.Prove = sProv Then
                scod2 = CStr(objAdj.Grupo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc - frmOrigen.m_oAdjs.Item(scod2).Adjudicado
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce - frmOrigen.m_oAdjs.Item(scod2).AdjudicadoMonProce
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal - frmOrigen.m_oAdjs.Item(scod2).AdjudicadoReal
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce - frmOrigen.m_oAdjs.Item(scod2).AdjudicadoRealMonProce
            End If
        Next
        
        If bAdjudicar Then
            If oAdj Is Nothing Then
                Set oAdj = oGrupo.Adjudicaciones.Add(sProv, oOferta.Num, lItem, oOferta.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(lIdEscalado)).PrecioOferta, _
                                         100, , oOferta.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(lIdEscalado)).PrecioOferta, _
                                         oOferta.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(lIdEscalado)).Objetivo, , oGrupo.Codigo, , , , lIdEscalado)
            End If
                   
            bHayOtrosEscAdj = False
            iNumAdjEsc = 0
            For Each oEsc In oGrupo.Escalados
                sCod = KeyEscalado(sProv, lItem, oEsc.Id)
                If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                    iNumAdjEsc = iNumAdjEsc + 1
                End If
            Next
            bHayOtrosEscAdj = (iNumAdjEsc > 0)
                   
            If Not IsMissing(dblCantEsc) Then
                If Not IsNull(dblCantEsc) Then
                    dblCantidadEscalado = dblCantEsc
                End If
            Else
                dblCantidadEscalado = 0
            End If
            oAdj.Adjudicado = dblCantidadEscalado
            If Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) And dblCantidadEscalado > 0 Then
                oAdj.Porcentaje = (dblCantidadEscalado / .Cantidad) * 100
            ElseIf Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) And Not IsMissing(dblCantProve) Then
                oAdj.Porcentaje = ((dblCantProve / iNumAdjEsc) / .Cantidad) * 100
            ElseIf Not IsMissing(dblCantProve) Then
                oAdj.Porcentaje = (dblCantProve / iNumAdjEsc) / dblCantProve * 100
            End If
            dblPorcen = 0
            If Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) Then
                If Not IsMissing(dblCantProve) Then
                    dblPorcen = (dblCantProve / .Cantidad) * 100
                Else
                    dblPorcen = (CantidadAdjItemProvePorEscalado(oGrupo, lItem, sProv) / .Cantidad) * 100
                End If
            End If
            If oAdjProve Is Nothing Then
                Set oAdjProve = m_oAdjsProve.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , , , oGrupo.Codigo)
            Else
                oAdjProve.Porcentaje = dblPorcen
            End If
            If oAdjProveReal Is Nothing Then
                Set oAdjProveReal = AdjsProve.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , , , oGrupo.Codigo)
            Else
                oAdjProveReal.Porcentaje = dblPorcen
            End If
            
            If dblCantidadEscalado > 0 Then
                If Not IsNull(oGrupo.Items.Item(CStr(oAdj.Id)).PresupuestoEscalado(oAdj.Escalado)) Then
                    dblImporteEscalado = dblCantidadEscalado * NullToDbl0(oOferta.Lineas.Item(CStr(oAdj.Id)).Escalados.Item(CStr(oAdj.Escalado)).PrecioOferta)
                Else
                    dblImporteEscalado = 0
                End If
                                
                'Datos de la adjudicaci�n
                oAdj.ImporteAdj = dblImporteEscalado
                oAdj.importe = CDec(dblImporteEscalado * dblCambio)
                If ComprobarAplicarAtribsItem(AtribsFormulas, CStr(lItem), oGrupo.Codigo) Then    'Si esta todo el item a este prove
                    oAdj.ImporteAdjTot = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, oAdj.ImporteAdj, TotalItem, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, lItem, lIdEscalado, dblCambio)
                Else
                    oAdj.ImporteAdjTot = oAdj.ImporteAdj
                End If
            End If
        Else
            oGrupo.Adjudicaciones.Remove sCod
        End If
                        
        If Not bHayOtrosEscAdj Then
            dblCantidadProveedor = 0
            dblImporteAdjudicadoProveedor = 0
            dblConsumidoProve = 0
            dblImporteAhorroProve = 0
            dblPorcenAhorroProve = 0
        Else
            'C�lculos proveedor
            If Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) Then
                If Not IsMissing(dblCantProve) Then
                    oAdjProve.Porcentaje = (dblCantProve / .Cantidad) * 100
                Else
                    oAdjProve.Porcentaje = (CantidadAdjItemProvePorEscalado(oGrupo, lItem, sProv) / .Cantidad) * 100
                End If
            End If
            If IsMissing(dblCantProve) Then
                dblCantidadProveedor = CantidadProveedor(oGrupo, ProvesAsig, m_oAdjsProve, sProv, lItem, oAdjProve.Porcentaje)
            Else
                dblCantidadProveedor = dblCantProve
            End If
            dblImporteAdjudicadoProveedor = ImporteAdjudicadoProveedor(ProcesoSeleccionado, oGrupo.Adjudicaciones, Nothing, oGrupo, oOferta, sProv, lItem, dblCantidadProveedor)
            dblImporteAdjudicadoProveedorTot = ImporteAdjudicadoProveedor(ProcesoSeleccionado, oGrupo.Adjudicaciones, AtribsFormulas, oGrupo, oOferta, sProv, lItem, dblCantidadProveedor)
            dblConsumidoProve = ConsumidoProveedor(oGrupo, sProv, lItem, dblCantidadProveedor)
            dblImporteAhorroProve = dblConsumidoProve - CDec(dblImporteAdjudicadoProveedor / dblCambio)
            If dblConsumidoProve > 0 Then
                dblPorcenAhorroProve = (dblImporteAhorroProve / dblConsumidoProve) * 100
            Else
                dblPorcenAhorroProve = 0
            End If
        End If
        
        iNumAdjProve = NumAdjudicacionesProve(oGrupo, scodProve, lItem)
        If iNumAdjProve = 0 Then
            m_oAdjsProve.Remove (CStr(lItem) & scodProve)
        End If
        
        If Not oAdjProve Is Nothing Then
            oAdjProve.Adjudicado = dblCantidadProveedor 'Se utiliza la prop. Adjudicado para guardar la cantidad
            If dblImporteAdjudicadoProveedor > 0 Then
                oAdjProve.PresUnitario = PresupuestoProveItemEscAdj(oGrupo, oItem, sProv, dblCantidadProveedor) * dblCambio
                oAdjProve.Precio = PrecioProveItemEscAdj(oGrupo, oOferta, oItem, sProv, dblCantidadProveedor)
                oAdjProve.ImporteAdj = dblImporteAdjudicadoProveedor
                oAdjProve.importe = dblImporteAhorroProve   'Se utiliza la prop. Importe para guardar el ahorro
                oAdjProve.ImporteAdjTot = dblConsumidoProve  'Se utiliza la prop. ImporteAdjTot para guardar el importe seg�n los datos del item
            Else
                oAdjProve.PresUnitario = Null
                oAdjProve.Precio = Null
                oAdjProve.ImporteAdj = 0
                oAdjProve.importe = 0
                oAdjProve.ImporteAdjTot = 0
            End If
        End If
                                                    
        'C�lculos item
        'Sumar las cantidades adjudicadas a los proveedores
        vCantidadItem = .Cantidad
        'Sumar las cantidades adjudicadas a los proveedores
        For Each oProve In ProvesAsig
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            sCod = CStr(lItem) & scodProve
            
            If Not m_oAdjsProve Is Nothing Then
                If Not m_oAdjsProve.Item(sCod) Is Nothing Then
                    dblCantidadItem = dblCantidadItem + NullToDbl0(m_oAdjsProve.Item(sCod).Adjudicado)
                    dblImporteAdjudicadoItem = dblImporteAdjudicadoItem + CDec(NullToDbl0(m_oAdjsProve.Item(sCod).ImporteAdj) / dblCambio)
                    dblImporteAhorroItem = dblImporteAhorroItem + NullToDbl0(m_oAdjsProve.Item(sCod).importe)
                    dblConsumidoItem = dblConsumidoItem + NullToDbl0(m_oAdjsProve.Item(sCod).ImporteAdjTot)
                End If
            End If
        Next
        If dblConsumidoItem > 0 Then dblPorcenAhorroItem = (dblImporteAhorroItem / dblConsumidoItem) * 100
               
        'Actualizar la colecci�n de adjudicaciones por proveedor (real)
        If Not oAdjProveReal Is Nothing Then
            If iNumAdjProve = 0 Then
                scodProve = oAdjProveReal.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdjProveReal.ProveCod))
                AdjsProve.Remove (CStr(lItem) & scodProve)
                
                oAdjProveReal.Adjudicado = 0
                oAdjProveReal.Porcentaje = 0
                oAdjProveReal.ImporteAdj = 0
                oAdjProveReal.importe = 0
                oAdjProveReal.ImporteAdjTot = 0
            Else
                If dblCantidadItem > 0 Then
                    dblPorcen = (dblCantidadProveedor / IIf(IsEmpty(vCantidadItem) Or IsNull(vCantidadItem), dblCantidadItem, vCantidadItem)) * 100
                End If
                
                oAdjProveReal.Adjudicado = dblCantidadProveedor
                oAdjProveReal.Porcentaje = dblPorcen
                oAdjProveReal.ImporteAdj = dblImporteAdjudicadoProveedor
                oAdjProveReal.importe = CDec(dblImporteAdjudicadoProveedor / dblCambio) 'En moneda proceso
                frmOrigen.m_dblImporteAux = oAdjProveReal.importe
                oAdjProveReal.ImporteAdjTot = dblImporteAdjudicadoProveedorTot
                oAdjProveReal.importe = frmOrigen.m_dblImporteAux
                
                .ImporteAdj = .ImporteAdj + CDec(oAdjProveReal.ImporteAdjTot / dblCambio) 'Adjudicado del item con atributos
            End If
            
            'Si el item no tiene cantidad puede haber m�s adjudicaciones para otros proveedores. Hay que recalcular los porcentajes
            If IsEmpty(vCantidadItem) Or IsNull(vCantidadItem) Then
                For Each oProve In ProvesAsig
                    If oProve.Cod <> sProv Then
                        scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                        If Not AdjsProve.Item(CStr(lItem) & scodProve) Is Nothing And dblCantidadItem > 0 Then
                            AdjsProve.Item(CStr(lItem) & scodProve).Porcentaje = (AdjsProve.Item(CStr(lItem) & scodProve).Adjudicado / dblCantidadItem) * 100
                        End If
                    End If
                Next
            End If
        End If
        
        'los totales objetivo: resto antes de cambiar la cantidad consumida del item
        oGrupo.ObjetivoImp = oGrupo.ObjetivoImp - CDec(.CantidadCons * NullToDbl0(vObjetivo))
        frmOrigen.m_dblTotalProveObjAll = frmOrigen.m_dblTotalProveObjAll - CDec(.CantidadCons * NullToDbl0(vObjetivo))
        If NullToDbl0(vObjetivo) = 0 Then
            'Si no has puesto objetivo. No te vale la cantidad a adjudicar pq NO hay precio sobre el que aplicar. NO precio NO importe
            oGrupo.ObjetivoAhorro = 0
            frmOrigen.m_dblTotalAhorroProveObjAll = 0
        Else
            oGrupo.ObjetivoAhorro = oGrupo.ObjetivoAhorro - (CDec(NullToDbl0(vPrecio) * .CantidadCons) - CDec(NullToDbl0(vObjetivo) * .CantidadCons))
            frmOrigen.m_dblTotalAhorroProveObjAll = frmOrigen.m_dblTotalAhorroProveObjAll - (CDec(NullToDbl0(vPrecio) * .CantidadCons) - CDec(NullToDbl0(vObjetivo) * .CantidadCons))
        End If
                  
        PresupuestoYObjetivoItemConEscalados oGrupo, oItem, ProvesAsig, vPrecio, vObjetivo
          
        'Datos del item: cantidad consumida y consumido
        If IsNull(.Cantidad) Or IsEmpty(.Cantidad) Then
            .CantidadCons = 0
        Else
            If .Cantidad = 0 Then
                .CantidadCons = 0
            Else
                .CantidadCons = .CantidadCons - CDec(.Cantidad * dblPorcenAnt / 100) + CDec(dblCantidadItem * dblPorcen / 100)
            End If
        End If
        .PorcenCons = .PorcenCons - dblPorcenAnt + dblPorcen
        .Consumido = dblConsumidoItem

        'Calculo los totales objetivo
        oGrupo.ObjetivoImp = oGrupo.ObjetivoImp + CDec(.CantidadCons * NullToDbl0(vObjetivo))
        frmOrigen.m_dblTotalProveObjAll = frmOrigen.m_dblTotalProveObjAll + CDec(.CantidadCons * NullToDbl0(vObjetivo))
        If NullToDbl0(.Objetivo) = 0 Then
            'Si no has puesto objetivo. No te vale la cantidad a adjudicar pq NO hay precio sobre el que aplicar. NO precio NO importe
            oGrupo.ObjetivoAhorro = 0
            frmOrigen.m_dblTotalAhorroProveObjAll = 0
        Else
            frmOrigen.m_dblTotalAhorroProveObjAll = frmOrigen.m_dblTotalAhorroProveObjAll + (dblConsumidoItem - (CDec(NullToDbl0(vObjetivo) * .CantidadCons)))
            oGrupo.ObjetivoAhorro = oGrupo.ObjetivoAhorro + (dblConsumidoItem - (CDec(NullToDbl0(vObjetivo) * .CantidadCons)))
        End If
        'Datos del grupo
        oGrupo.Adjudicado = oGrupo.Adjudicado + .ImporteAdj
        oGrupo.AdjudicadoReal = oGrupo.AdjudicadoReal + .ImporteAdj
        oGrupo.Consumido = oGrupo.Consumido + .Consumido
        'Datos de la adjudicacion prove-grupo
        If Not frmOrigen.m_oAdjs.Item(scod1) Is Nothing Then
            With frmOrigen.m_oAdjs.Item(scod1)
                'Le sumo lo nuevo
                If IsNull(vCantidadItem) Or IsEmpty(vCantidadItem) Then
                    .Consumido = .Consumido + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPrecio) * dblCambio))
                    .ConsumidoMonProce = .ConsumidoMonProce + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPrecio)))
                    .ConsumidoReal = .ConsumidoReal + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPrecio) * dblCambio))
                    .ConsumidoRealMonProce = .ConsumidoRealMonProce + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPrecio)))
                Else
                    .Consumido = .Consumido + CDec(CDec(oItem.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPrecio) * dblCambio))
                    .ConsumidoMonProce = .ConsumidoMonProce + CDec(CDec(oItem.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPrecio)))
                    .ConsumidoReal = .ConsumidoReal + CDec(CDec(oItem.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPrecio) * dblCambio))
                    .ConsumidoRealMonProce = .ConsumidoRealMonProce + CDec(CDec(oItem.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPrecio)))
                End If
                If dblPorcen > 0 Then
                    .AdjudicadoSin = .AdjudicadoSin + oAdjProveReal.ImporteAdjTot
                    .AdjudicadoSinMonProce = .AdjudicadoSinMonProce + (oAdjProveReal.ImporteAdjTot / dblCambio)
                    .AdjudicadoSinReal = .AdjudicadoSinReal + oAdjProveReal.ImporteAdjTot
                    .AdjudicadoSinRealMonProce = .AdjudicadoSinRealMonProce + (oAdjProveReal.ImporteAdjTot / dblCambio)
                    .AdjudicadoItems = .AdjudicadoItems + oAdjProveReal.ImporteAdj
                    .AdjudicadoItemsMonproce = .AdjudicadoItems + (oAdjProveReal.ImporteAdj / dblCambio)
                End If
                .importe = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoOfe, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                .ImporteMonProce = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoOfeMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                        oGrupo.Codigo, , , oGrupo.CambioComparativa(sProv))
                bDistintos = False
                bAdjudicado = True
                If Not AtribsFormulas Is Nothing Then bAdjudicado = AtribsFormulas.ComprobarAplicarAtribsGrupo(oGrupo.Codigo, sProv)
                If .AdjudicadoSin <> .AdjudicadoSinReal Then
                    bDistintos = True
                    If bAdjudicado Then
                        dblImporteAux = CDec(oGrupo.AdjudicadoReal * .Cambio)
                        'Voy a calcular el Adjudicado con atribs para el grupo pq si se pueden aplicar es pq est� adjudicado a este proveedor todo el grupo, sino es que no se pueden aplicar
                        .AdjudicadoReal = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoSinReal, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                        .AdjudicadoRealMonProce = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoSinRealMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                        oGrupo.Codigo, , , oGrupo.CambioComparativa(sProv))
                        
                        oGrupo.AdjudicadoTotalReal = CDec(.AdjudicadoRealMonProce)
                    Else
                        .AdjudicadoReal = .AdjudicadoSinReal
                        .AdjudicadoRealMonProce = .AdjudicadoSinRealMonProce
                        oGrupo.AdjudicadoTotalReal = oGrupo.AdjudicadoReal
                    End If
                End If
                If bAdjudicado Then
                    dblImporteAux = CDec(oGrupo.Adjudicado * .Cambio)
                    'Voy a calcular el Adjudicado con atribs para el grupo
                    .Adjudicado = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoSin, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                    .AdjudicadoMonProce = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoSinMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                oGrupo.Codigo, , , oGrupo.CambioComparativa(sProv))
                    
                    oGrupo.AdjudicadoTotal = oGrupo.AdjudicadoTotal + .AdjudicadoMonProce
                    
                    If Not bDistintos Then
                        .AdjudicadoReal = .Adjudicado
                        .AdjudicadoRealMonProce = .AdjudicadoMonProce
                        oGrupo.AdjudicadoTotalReal = oGrupo.AdjudicadoTotal
                    End If
                Else
                    If Not bDistintos Then
                        .AdjudicadoReal = .AdjudicadoSinReal
                        .AdjudicadoRealMonProce = .AdjudicadoSinRealMonProce
                        oGrupo.AdjudicadoTotalReal = oGrupo.AdjudicadoReal
                    End If
                    .Adjudicado = .AdjudicadoSin
                    .AdjudicadoMonProce = .AdjudicadoSinMonProce
                    oGrupo.AdjudicadoTotal = oGrupo.Adjudicado
                End If
                .Ahorrado = .Consumido - .Adjudicado
                .AhorradoMonProce = .ConsumidoMonProce - .AdjudicadoMonProce
                If .Consumido <> 0 Then
                    .AhorradoPorcen = CDec(.Ahorrado / Abs(.Consumido)) * 100
                Else
                    .AhorradoPorcen = 0
                End If
                
                i = 1
                frmOrigen.sdbgGruposProve.MoveFirst 'Me coloco en la fila correspondiente al grupo
                While frmOrigen.sdbgGruposProve.Columns("COD").Value <> oGrupo.Codigo And i <= frmOrigen.sdbgGruposProve.Rows
                    frmOrigen.sdbgGruposProve.MoveNext
                    i = i + 1
                Wend
                If frmOrigen.sdbgGruposProve.Columns("COD").Value = oGrupo.Codigo Then
                    frmOrigen.sdbgGruposProve.Columns("Consumido" & sProv).Value = NullToStr(CDec(.ConsumidoMonProce))
                    frmOrigen.sdbgGruposProve.Columns("Adjudicado" & sProv).Value = NullToStr(CDec(.AdjudicadoMonProce))
                    frmOrigen.sdbgGruposProve.Columns("Ahorrado" & sProv).Value = NullToStr(CDec(.AhorradoMonProce))
                    frmOrigen.sdbgGruposProve.Columns("Ahorro%" & sProv).Value = NullToStr(.AhorradoPorcen)
                    frmOrigen.sdbgGruposProve.Update
                End If
                frmOrigen.sdbgProveGrupos.MoveFirst
                i = 1
                While frmOrigen.sdbgProveGrupos.Columns("COD").Value <> sProv And i <= frmOrigen.sdbgProveGrupos.Rows
                    frmOrigen.sdbgProveGrupos.MoveNext
                    i = i + 1
                Wend
                If frmOrigen.sdbgProveGrupos.Columns("COD").Value = sProv Then
                    frmOrigen.sdbgProveGrupos.Columns("Consumido" & oGrupo.Codigo).Value = NullToStr(CDec(.ConsumidoMonProce))
                    frmOrigen.sdbgProveGrupos.Columns("Adjudicado" & oGrupo.Codigo).Value = NullToStr(CDec(.AdjudicadoMonProce))
                    frmOrigen.sdbgProveGrupos.Columns("Ahorrado" & oGrupo.Codigo).Value = NullToStr(CDec(.AhorradoMonProce))
                    frmOrigen.sdbgProveGrupos.Columns("Ahorro%" & oGrupo.Codigo).Value = NullToStr(.AhorradoPorcen)
                    frmOrigen.sdbgProveGrupos.Update
                End If
                frmOrigen.sdbgProveGrupos.MoveFirst
            End With
        End If
        
        'Guardo el nuevo total de adjudicaciones de grupo para esta oferta.
        For Each objAdj In frmOrigen.m_oAdjs
            If objAdj.Prove = sProv Then
                scod2 = CStr(objAdj.Grupo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
                
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc + frmOrigen.m_oAdjs.Item(scod2).Adjudicado
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce + frmOrigen.m_oAdjs.Item(scod2).AdjudicadoMonProce
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal + frmOrigen.m_oAdjs.Item(scod2).AdjudicadoReal
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce + frmOrigen.m_oAdjs.Item(scod2).AdjudicadoRealMonProce
            End If
        Next

        'Suma a los datos del proceso
        frmOrigen.m_dblConsumidoProc = frmOrigen.m_dblConsumidoProc + .Consumido
        frmOrigen.m_dblConsumidoProcReal = frmOrigen.m_dblConsumidoProcReal + .Consumido
        frmOrigen.m_dblAdjudicadoSinProc = frmOrigen.m_dblAdjudicadoSinProc + oGrupo.AdjudicadoTotal
        frmOrigen.m_dblAdjudicadoSinProcReal = frmOrigen.m_dblAdjudicadoSinProcReal + oGrupo.AdjudicadoTotalReal
        frmOrigen.m_dblAdjudicadoProc = frmOrigen.m_dblAdjudicadoSinProc
        frmOrigen.m_dblAdjudicadoProcReal = frmOrigen.m_dblAdjudicadoSinProcReal
        
        frmOrigen.AplicarAtributosDeProceso
        frmOrigen.CargarGridResultados
        
        '************ ACTUALIZACION GRID ESCALADOS *************
        'Buscar la fila del proveedor
        vbm = sdbgEscalado.Bookmark
        iFilaItem = 0
        If sdbgEscalado.Columns("PROV").Value = 1 Then iFilaItem = FilaItem(sdbgEscalado.Columns("ART").Value)
        vBmFilaItem = sdbgEscalado.AddItemBookmark(iFilaItem)
        For i = 1 To ProvesAsig.Count
            vBookmark = sdbgEscalado.AddItemBookmark(iFilaItem + i)
            If sdbgEscalado.Columns("ID").CellValue(vBookmark) = sProv Then
                sdbgEscalado.Bookmark = vBookmark
                Exit For
            End If
        Next
        'Buscar el grupo del escalado
        For i = 2 To sdbgEscalado.Groups.Count - 1
            If sdbgEscalado.Columns("ESC" & i).Value = lIdEscalado Then Exit For
        Next
        If bAdjudicar Then
            'Escalados
            sdbgEscalado.Columns("ADJ" & i).Value = "1"
            If dblCantidadEscalado > 0 Then sdbgEscalado.Columns("ADJCANT" & i).Value = dblCantidadEscalado
            sdbgEscalado.Columns("ADJIMP" & i).Value = dblImporteEscalado
            'Proveedor
            sdbgEscalado.Columns("CANT").Value = CDec(dblCantidadProveedor)
            sdbgEscalado.Columns("IMP").Value = IIf(dblImporteAdjudicadoProveedor = 0, "", CDec(dblImporteAdjudicadoProveedor / dblCambio))
            If dblImporteAdjudicadoProveedor = 0 And dblImporteAhorroProve = 0 Then
                sdbgEscalado.Columns("AHORROIMP").Value = ""
            Else
                sdbgEscalado.Columns("AHORROIMP").Value = dblImporteAhorroProve
            End If
            If dblImporteAdjudicadoProveedor = 0 And dblPorcenAhorroProve = 0 Then
                sdbgEscalado.Columns("AHORROPORCEN").Value = ""
            Else
                sdbgEscalado.Columns("AHORROPORCEN").Value = dblPorcenAhorroProve
            End If
            sdbgEscalado.Columns("PRECAPE").Value = oAdjProve.PresUnitario / dblCambio
            If dblImporteAdjudicadoProveedor = 0 And oAdjProve.Precio = 0 Then
                sdbgEscalado.Columns("CODPROVEACTUAL").Value = ""
            Else
                sdbgEscalado.Columns("CODPROVEACTUAL").Value = oAdjProve.Precio / dblCambio
            End If
        Else
            'escalados
            sdbgEscalado.Columns("ADJ" & i).Value = "0"
            sdbgEscalado.Columns("ADJCANT" & i).Value = ""
            sdbgEscalado.Columns("ADJIMP" & i).Value = ""
            'proveedor
            If dblCantidadProveedor = 0 Then
                sdbgEscalado.Columns("CANT").Value = ""
            Else
                sdbgEscalado.Columns("CANT").Value = dblCantidadProveedor
            End If
            sdbgEscalado.Columns("IMP").Value = IIf(dblImporteAdjudicadoProveedor = 0, "", dblImporteAdjudicadoProveedor)
            If dblImporteAdjudicadoProveedor > 0 Then
                sdbgEscalado.Columns("AHORROIMP").Value = dblImporteAhorroProve
                sdbgEscalado.Columns("AHORROPORCEN").Value = dblPorcenAhorroProve
                sdbgEscalado.Columns("PRECAPE").Value = oAdjProve.PresUnitario
                sdbgEscalado.Columns("CODPROVEACTUAL").Value = oAdjProve.Precio
            Else
                sdbgEscalado.Columns("AHORROIMP").Value = ""
                sdbgEscalado.Columns("AHORROPORCEN").Value = ""
                sdbgEscalado.Columns("PRECAPE").Value = ""
                sdbgEscalado.Columns("CODPROVEACTUAL").Value = ""
            End If
        End If
        sdbgEscalado.Update
        'Item
        sdbgEscalado.Bookmark = vBmFilaItem
        sdbgEscalado.Columns("IMP").Value = IIf(dblImporteAdjudicadoItem = 0, "", CDec(dblImporteAdjudicadoItem / dblCambio))
        If dblImporteAdjudicadoItem = 0 And dblImporteAhorroItem = 0 Then
            sdbgEscalado.Columns("AHORROIMP").Value = ""
        Else
            sdbgEscalado.Columns("AHORROIMP").Value = dblImporteAhorroItem
        End If
        If dblImporteAdjudicadoItem = 0 And dblPorcenAhorroItem = 0 Then
            sdbgEscalado.Columns("AHORROPORCEN").Value = ""
        Else
            sdbgEscalado.Columns("AHORROPORCEN").Value = dblPorcenAhorroItem
        End If
        sdbgEscalado.Columns("PRECAPE").Value = IIf(.Consumido = 0, "", .Consumido)
        sdbgEscalado.Columns("ADJCANT" & i).Value = NullToStr(CantidadAdjItemEscalado(oGrupo, lItem, lIdEscalado, ProvesAsig))
        sdbgEscalado.Bookmark = vbm
        
        frmOrigen.sdbgResultados.MoveFirst
        frmOrigen.sdbgResultados.MoveNext
        frmOrigen.sdbgResultados.Columns("Consumido").Value = oGrupo.Consumido
        frmOrigen.sdbgResultados.Columns("Abierto").Value = oGrupo.Abierto
        frmOrigen.sdbgResultados.Columns("Adjudicado").Value = oGrupo.AdjudicadoTotal
        frmOrigen.sdbgResultados.Columns("Ahorrado").Value = oGrupo.Consumido - oGrupo.AdjudicadoTotal
        If oGrupo.Consumido = 0 Then
            frmOrigen.sdbgResultados.Columns("%").Value = 0
        Else
            frmOrigen.sdbgResultados.Columns("%").Value = CDec((oGrupo.Consumido - oGrupo.AdjudicadoTotal) / Abs(oGrupo.Consumido)) * 100
        End If
        frmOrigen.sdbgResultados.MoveFirst
                
        'sdbgtotalesProve y sdbgtotales2
        frmOrigen.RestaurarTotalesGeneral
    End With

    m_bRecalculando = False

    Set oOferta = Nothing
    Set oItem = Nothing
    Set oAdj = Nothing
    Set oEscalado = Nothing
    Set oAdjProve = Nothing
    Set oAdjProveReal = Nothing
    Set oEsc = Nothing
    Set oProve = Nothing
End Sub

''' <summary>Actualiza las colecciones de frmADJ para que los cambios efectuados en el control tengan reflejo en el formulario</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="bAdjudicar">Indica si se trata de una adjudicaci�n o desadjudicaci�n</param>
''' <param name="lItem">Id del item que se est� modificando</param>
''' <param name="sProv">C�digo del proveedor</param>
''' <param name="dblCantProve">Cantidad adjudicada al proveedor</param>
''' <remarks>Llamada desde: AdjudicarEscalado, DesadjudicarEscalado</remarks>
''' <revision>21/03/2012</revision>

Private Sub RecalcularPorProveedor(ByRef oGrupo As CGrupo, ByVal bAdjudicar As Boolean, ByVal lItem As Long, ByVal sProv As String, ByVal dblCantProve As Variant)
    Dim vCantidadItem As Variant
    Dim dblConsumidoProve As Double
    Dim oAdj As CAdjudicacion
    Dim oAdjProve As CAdjudicacion
    Dim oAdjProveReal As CAdjudicacion
    Dim oOferta As COferta
    Dim oItem As CItem
    Dim oProve As CProveedor
    Dim sCod As String
    Dim scod1 As String
    Dim scodProve As String
    Dim bDistintos As Boolean
    Dim bAdjudicado As Boolean
    Dim i As Integer
    Dim dblImporteAdjudicadoProveedor As Double
    Dim dblImporteAdjudicadoProveedorTot As Double
    Dim dblImporteAhorroProve As Double
    Dim dblPorcenAhorroProve As Double
    Dim vbm As Variant
    Dim vBmFilaItem As Variant
    Dim iFilaItem As Variant
    Dim dblCantidadItem As Double
    Dim dblImporteAdjudicadoItem As Double
    Dim dblImporteAhorroItem As Double
    Dim dblPorcenAhorroItem As Double
    Dim dblConsumidoItem As Double
    Dim dblPorcenAnt As Double
    Dim dblPorcen As Double
    Dim vPres As Variant
    Dim vObjetivo As Variant
    Dim dblImporteAux As Double
    Dim dblCambio As Double
    
    m_bRecalculando = True
    
    'Obtenci�n de objetos
    Set oItem = oGrupo.Items.Item(CStr(lItem))
    scodProve = sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
    Set oAdj = oGrupo.Adjudicaciones.Item(CStr(lItem) & scodProve)
    Set oOferta = oGrupo.UltimasOfertas.Item(Trim(sProv))
    Set oAdjProve = m_oAdjsProve.Item(CStr(lItem) & scodProve)
    Set oAdjProveReal = AdjsProve.Item(CStr(lItem) & scodProve)
    
    scod1 = CStr(oGrupo.Codigo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
    
    With oItem
        dblPorcenAnt = .PorcenCons
        dblCambio = oItem.CambioComparativa(sProv, True, m_oAdjsProve)
        
        'Modifico las colecciones y luego las grids
        frmOrigen.m_dblConsumidoProc = frmOrigen.m_dblConsumidoProc - .Consumido
        frmOrigen.m_dblConsumidoProcReal = frmOrigen.m_dblConsumidoProcReal - .Consumido
        frmOrigen.m_dblAdjudicadoSinProc = frmOrigen.m_dblAdjudicadoSinProc - oGrupo.AdjudicadoTotal
        frmOrigen.m_dblAdjudicadoSinProcReal = frmOrigen.m_dblAdjudicadoSinProcReal - oGrupo.AdjudicadoTotalReal
        oGrupo.Adjudicado = oGrupo.Adjudicado - .ImporteAdj
        oGrupo.AdjudicadoReal = oGrupo.AdjudicadoReal - .ImporteAdj
        oGrupo.Consumido = oGrupo.Consumido - .Consumido
        
        'Restamos lo adjudicado al grupo para ese prove
        oGrupo.AdjudicadoTotal = oGrupo.AdjudicadoTotal - (frmOrigen.m_oAdjs.Item(scod1).AdjudicadoMonProce)
        
        'Si hab�a adjudicaci�n resto los datos anteriores y sumo los nuevos
        vPres = oItem.PresupuestoProveItemEsc(oGrupo.Escalados)
        vObjetivo = ObjetivoProveItemEsc(oGrupo, oItem)
        
        If Not oAdjProve Is Nothing Then
            If Not frmOrigen.m_oAdjs.Item(scod1) Is Nothing Then
                'Resto lo anterior, el consumido y el importe adjudicado con atribs de total item
                frmOrigen.m_oAdjs.Item(scod1).Consumido = frmOrigen.m_oAdjs.Item(scod1).Consumido - (NullToDbl0(.Cantidad * dblPorcenAnt) / 100 * NullToDbl0(vPres) * dblCambio)
                frmOrigen.m_oAdjs.Item(scod1).ConsumidoMonProce = frmOrigen.m_oAdjs.Item(scod1).ConsumidoMonProce - (NullToDbl0(.Cantidad * dblPorcenAnt) / 100 * NullToDbl0(vPres))
                frmOrigen.m_oAdjs.Item(scod1).ConsumidoReal = frmOrigen.m_oAdjs.Item(scod1).ConsumidoReal - (NullToDbl0(.Cantidad * dblPorcenAnt) / 100 * NullToDbl0(vPres) * dblCambio)
                frmOrigen.m_oAdjs.Item(scod1).ConsumidoRealMonProce = frmOrigen.m_oAdjs.Item(scod1).ConsumidoRealMonProce - (NullToDbl0(.Cantidad * dblPorcenAnt) / 100 * NullToDbl0(vPres))
                frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSin = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSin - oAdjProveReal.ImporteAdjTot
                frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinMonProce = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinMonProce - (oAdjProveReal.ImporteAdjTot / dblCambio)
                frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinReal = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinReal - oAdjProveReal.ImporteAdjTot
                frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinRealMonProce = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoSinRealMonProce - (oAdjProveReal.ImporteAdjTot / dblCambio)
                frmOrigen.m_oAdjs.Item(scod1).AdjudicadoItems = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoItems - oAdjProveReal.ImporteAdj
                frmOrigen.m_oAdjs.Item(scod1).AdjudicadoItemsMonproce = frmOrigen.m_oAdjs.Item(scod1).AdjudicadoItemsMonproce - (oAdjProveReal.ImporteAdj / dblCambio)
            End If
            'Datos de grupo
            'Importe del item sin atribs de total item
            .ImporteAdj = .ImporteAdj - CDec(oAdjProveReal.ImporteAdjTot / dblCambio)
        End If
        
        'Elimino el total de adjudicaciones de grupo para esta oferta.
        Dim objAdj As CAdjGrupo
        Dim scod2 As String
        For Each objAdj In frmOrigen.m_oAdjs
            If objAdj.Prove = sProv Then
                scod2 = CStr(objAdj.Grupo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc - frmOrigen.m_oAdjs.Item(scod2).Adjudicado
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce - frmOrigen.m_oAdjs.Item(scod2).AdjudicadoMonProce
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal - frmOrigen.m_oAdjs.Item(scod2).AdjudicadoReal�
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce - frmOrigen.m_oAdjs.Item(scod2).AdjudicadoRealMonProce
            End If
        Next
        
        If bAdjudicar Then
            dblPorcen = 0
            If Not IsNull(.Cantidad) And Not IsEmpty(.Cantidad) Then
                dblPorcen = (dblCantProve / .Cantidad) * 100
            End If
            
            If oAdj Is Nothing Then
                Set oAdj = oGrupo.Adjudicaciones.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , .Objetivo, , oGrupo.Codigo)
            Else
                oAdj.Porcentaje = dblPorcen
            End If
            If oAdjProve Is Nothing Then
                Set oAdjProve = m_oAdjsProve.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , , , oGrupo.Codigo)
                Set oAdjProveReal = AdjsProve.Add(sProv, oOferta.Num, lItem, , dblPorcen, , , , , oGrupo.Codigo)
            Else
                oAdjProve.Porcentaje = dblPorcen
                oAdjProveReal.Porcentaje = dblPorcen
            End If
        Else
            oGrupo.Adjudicaciones.Remove CStr(lItem) & scodProve
            m_oAdjsProve.Remove CStr(lItem) & scodProve
            AdjsProve.Remove CStr(lItem) & scodProve
        End If
                                        
        'C�lculos proveedor
        dblImporteAdjudicadoProveedor = ImporteAdjudicadoProveedor(ProcesoSeleccionado, oGrupo.Adjudicaciones, Nothing, oGrupo, oOferta, sProv, lItem, dblCantProve)
        dblImporteAdjudicadoProveedorTot = ImporteAdjudicadoProveedor(ProcesoSeleccionado, oGrupo.Adjudicaciones, AtribsFormulas, oGrupo, oOferta, sProv, lItem, dblCantProve)
        dblConsumidoProve = ConsumidoProveedor(oGrupo, sProv, lItem, dblCantProve)
        dblImporteAhorroProve = dblConsumidoProve - dblImporteAdjudicadoProveedor
        If dblConsumidoProve > 0 Then
            dblPorcenAhorroProve = (dblImporteAhorroProve / dblConsumidoProve) * 100
        Else
            dblPorcenAhorroProve = 0
        End If
        
        If Not oAdjProve Is Nothing Then
            oAdjProve.Adjudicado = dblCantProve 'Se utiliza la prop. Adjudicado para guardar la cantidad
            If dblImporteAdjudicadoProveedor > 0 Then
                oAdjProve.PresUnitario = oItem.PresupuestoProveItemEsc(oGrupo.Escalados)
                oAdjProve.Precio = PrecioProveItemEsc(oGrupo, oOferta, oItem)
                oAdjProve.ImporteAdj = dblImporteAdjudicadoProveedor
                oAdjProve.importe = dblImporteAhorroProve   'Se utiliza la prop. Importe para guardar el ahorro
                oAdjProve.ImporteAdjTot = dblConsumidoProve  'Se utiliza la prop. ImporteAdjTot para guardar el importe seg�n los datos del item
            Else
                oAdjProve.PresUnitario = Null
                oAdjProve.Precio = Null
                oAdjProve.ImporteAdj = Null
                oAdjProve.importe = Null
                oAdjProve.ImporteAdjTot = Null
            End If
        End If
                                                    
        'C�lculos item
        vCantidadItem = .Cantidad
        'Sumar las cantidades adjudicadas a los proveedores
        For Each oProve In ProvesAsig
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            sCod = CStr(lItem) & scodProve
            
            If Not m_oAdjsProve Is Nothing Then
                If Not m_oAdjsProve.Item(sCod) Is Nothing Then
                    dblCantidadItem = dblCantidadItem + NullToDbl0(m_oAdjsProve.Item(sCod).Adjudicado)
                    dblImporteAdjudicadoItem = dblImporteAdjudicadoItem + NullToDbl0(m_oAdjsProve.Item(sCod).ImporteAdj)
                    dblImporteAhorroItem = dblImporteAhorroItem + NullToDbl0(m_oAdjsProve.Item(sCod).importe)
                    dblConsumidoItem = dblConsumidoItem + NullToDbl0(m_oAdjsProve.Item(sCod).ImporteAdjTot)
                End If
            End If
        Next
        If dblConsumidoItem > 0 Then dblPorcenAhorroItem = (dblImporteAhorroItem / dblConsumidoItem) * 100
               
        'Actualizar la colecci�n de adjudicaciones por proveedor (real)
        If Not oAdjProveReal Is Nothing Then
            If bAdjudicar Then
                If dblCantidadItem > 0 Then
                    dblPorcen = (dblCantProve / IIf(IsEmpty(vCantidadItem) Or IsNull(vCantidadItem), dblCantidadItem, vCantidadItem)) * 100
                End If
                
                oAdjProveReal.Adjudicado = dblCantProve
                oAdjProveReal.Porcentaje = dblPorcen
                oAdjProveReal.ImporteAdj = dblImporteAdjudicadoProveedor
                oAdjProveReal.importe = CDec(dblImporteAdjudicadoProveedor * dblCambio)
                frmOrigen.m_dblImporteAux = oAdjProveReal.importe
                oAdjProveReal.ImporteAdjTot = dblImporteAdjudicadoProveedorTot
                If ComprobarAplicarAtribsItem(AtribsFormulas, CStr(lItem), oGrupo.Codigo) Then  'Si esta todo el item a este prove
                    oAdjProveReal.ImporteAdjTot = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, oAdjProveReal.ImporteAdj, TotalItem, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                        oGrupo.Codigo, lItem, , dblCambio)
                Else
                    oAdjProveReal.ImporteAdjTot = oAdjProveReal.ImporteAdj
                End If
                oAdjProveReal.importe = frmOrigen.m_dblImporteAux
                
                .ImporteAdj = .ImporteAdj + CDec(oAdjProveReal.ImporteAdjTot / dblCambio) 'Adjudicado del item con atributos
                
                oAdj.Adjudicado = oAdjProveReal.Adjudicado
                oAdj.Porcentaje = oAdjProveReal.Porcentaje
                oAdj.ImporteAdj = oAdjProveReal.ImporteAdj
                oAdj.importe = oAdjProveReal.importe
                oAdj.ImporteAdjTot = oAdjProveReal.ImporteAdjTot
            End If
            
            'Si el item no tiene cantidad puede haber m�s adjudicaciones para otros proveedores. Hay que recalcular los porcentajes
            If IsEmpty(vCantidadItem) Or IsNull(vCantidadItem) Then
                For Each oProve In ProvesAsig
                    If oProve.Cod <> sProv Then
                        scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                        If Not AdjsProve.Item(CStr(lItem) & scodProve) Is Nothing And dblCantidadItem > 0 Then
                            AdjsProve.Item(CStr(lItem) & scodProve).Porcentaje = (AdjsProve.Item(CStr(lItem) & scodProve).Adjudicado / dblCantidadItem) * 100
                        End If
                        
                        If Not oGrupo.Adjudicaciones.Item(CStr(lItem) & scodProve) Is Nothing And dblCantidadItem > 0 Then
                            oGrupo.Adjudicaciones.Item(CStr(lItem) & scodProve).Porcentaje = (AdjsProve.Item(CStr(lItem) & scodProve).Adjudicado / dblCantidadItem) * 100
                        End If
                    End If
                Next
            End If
        End If
        
        'los totales objetivo: resto antes de cambiar la cantidad consumida del item
        oGrupo.ObjetivoImp = oGrupo.ObjetivoImp - CDec(.CantidadCons * NullToDbl0(vObjetivo))
        frmOrigen.m_dblTotalProveObjAll = frmOrigen.m_dblTotalProveObjAll - CDec(.CantidadCons * NullToDbl0(vObjetivo))
        If NullToDbl0(vObjetivo) = 0 Then
            'Si no has puesto objetivo. No te vale la cantidad a adjudicar pq NO hay precio sobre el que aplicar. NO precio NO importe
            oGrupo.ObjetivoAhorro = 0
            frmOrigen.m_dblTotalAhorroProveObjAll = 0
        Else
            oGrupo.ObjetivoAhorro = oGrupo.ObjetivoAhorro - (CDec(NullToDbl0(vPres) * .CantidadCons) - CDec(NullToDbl0(vObjetivo) * .CantidadCons))
            frmOrigen.m_dblTotalAhorroProveObjAll = frmOrigen.m_dblTotalAhorroProveObjAll - (CDec(NullToDbl0(vPres) * .CantidadCons) - CDec(NullToDbl0(vObjetivo) * .CantidadCons))
        End If
          
        'Datos del item: cantidad consumida y consumido
        If IsNull(.Cantidad) Or IsEmpty(.Cantidad) Then
            .CantidadCons = 0
        Else
            If .Cantidad = 0 Then
                .CantidadCons = 0
            Else
                .CantidadCons = .CantidadCons - CDec(.Cantidad * dblPorcenAnt / 100) + CDec(dblCantidadItem * dblPorcen / 100)
            End If
        End If
        .PorcenCons = .PorcenCons - dblPorcenAnt + dblPorcen
        .Consumido = dblConsumidoItem

        'Calculo los totales objetivo
        oGrupo.ObjetivoImp = oGrupo.ObjetivoImp + CDec(.CantidadCons * NullToDbl0(vObjetivo))
        frmOrigen.m_dblTotalProveObjAll = frmOrigen.m_dblTotalProveObjAll + CDec(.CantidadCons * NullToDbl0(vObjetivo))
        If NullToDbl0(.Objetivo) = 0 Then
            'Si no has puesto objetivo. No te vale la cantidad a adjudicar pq NO hay precio sobre el que aplicar. NO precio NO importe
            oGrupo.ObjetivoAhorro = 0
            frmOrigen.m_dblTotalAhorroProveObjAll = 0
        Else
            frmOrigen.m_dblTotalAhorroProveObjAll = frmOrigen.m_dblTotalAhorroProveObjAll + (dblConsumidoItem - (CDec(NullToDbl0(vObjetivo) * .CantidadCons)))
            oGrupo.ObjetivoAhorro = oGrupo.ObjetivoAhorro + (dblConsumidoItem - (CDec(NullToDbl0(vObjetivo) * .CantidadCons)))
        End If
        'Datos del grupo
        oGrupo.Adjudicado = oGrupo.Adjudicado + .ImporteAdj
        oGrupo.AdjudicadoReal = oGrupo.AdjudicadoReal + .ImporteAdj
        oGrupo.Consumido = oGrupo.Consumido + .Consumido
        'Datos de la adjudicacion prove-grupo
        If Not frmOrigen.m_oAdjs.Item(scod1) Is Nothing Then
            With frmOrigen.m_oAdjs.Item(scod1)
                'Le sumo lo nuevo
                If IsNull(vCantidadItem) Or IsEmpty(vCantidadItem) Then
                    .Consumido = .Consumido + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPres) * dblCambio))
                    .ConsumidoMonProce = .ConsumidoMonProce + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPres)))
                    .ConsumidoReal = .ConsumidoReal + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPres) * dblCambio))
                    .ConsumidoRealMonProce = .ConsumidoRealMonProce + CDec(CDec(dblCantidadItem * dblPorcen / 100) * CDec(NullToDbl0(vPres)))
                Else
                    .Consumido = .Consumido + CDec(CDec(oItem.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPres) * dblCambio))
                    .ConsumidoMonProce = .ConsumidoMonProce + CDec(CDec(oItem.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPres)))
                    .ConsumidoReal = .ConsumidoReal + CDec(CDec(oItem.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPres) * dblCambio))
                    .ConsumidoRealMonProce = .ConsumidoRealMonProce + CDec(CDec(oItem.Cantidad * dblPorcen / 100) * CDec(NullToDbl0(vPres)))
                End If
                If dblPorcen > 0 Then
                    .AdjudicadoSin = .AdjudicadoSin + oAdjProveReal.ImporteAdjTot
                    .AdjudicadoSinMonProce = .AdjudicadoSinMonProce + (oAdjProveReal.ImporteAdjTot / dblCambio)
                    .AdjudicadoSinReal = .AdjudicadoSinReal + oAdjProveReal.ImporteAdjTot
                    .AdjudicadoSinRealMonProce = .AdjudicadoSinReal + (oAdjProveReal.ImporteAdjTot / dblCambio)
                    .AdjudicadoItems = .AdjudicadoItems + oAdjProveReal.ImporteAdj
                    .AdjudicadoItemsMonproce = .AdjudicadoItems + (oAdjProveReal.ImporteAdj / dblCambio)
                End If
                .importe = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoOfe, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                .ImporteMonProce = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoOfeMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                        oGrupo.Codigo, , , oGrupo.CambioComparativa(sProv))
                bDistintos = False
                bAdjudicado = True
                If Not AtribsFormulas Is Nothing Then bAdjudicado = AtribsFormulas.ComprobarAplicarAtribsGrupo(oGrupo.Codigo, sProv)
                If .AdjudicadoSin <> .AdjudicadoSinReal Then
                    bDistintos = True
                    If bAdjudicado Then
                        'Voy a calcular el Adjudicado con atribs para el grupo pq si se pueden aplicar es pq est� adjudicado a este proveedor todo el grupo, sino es que no se pueden aplicar
                        .AdjudicadoReal = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoSinReal, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                        .AdjudicadoRealMonProce = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoSinRealMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                        oGrupo.Codigo, , , oGrupo.CambioComparativa(sProv))
                                                        
                        oGrupo.AdjudicadoTotalReal = CDec(.AdjudicadoRealMonProce)
                    Else
                        .AdjudicadoReal = .AdjudicadoSinReal
                        .AdjudicadoRealMonProce = .AdjudicadoSinRealMonProce
                        oGrupo.AdjudicadoTotalReal = oGrupo.AdjudicadoReal
                    End If
                End If
                If bAdjudicado Then
                    'Voy a calcular el Adjudicado con atribs para el grupo
                    .Adjudicado = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoSin, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo)
                    .AdjudicadoMonProce = AplicarAtributos(ProcesoSeleccionado, AtribsFormulas, .AdjudicadoSinMonProce, TotalGrupo, sProv, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, _
                                                oGrupo.Codigo, , , oGrupo.CambioComparativa(sProv))
                    
                    oGrupo.AdjudicadoTotal = oGrupo.AdjudicadoTotal + (.AdjudicadoMonProce)
                    'oGrupo.AdjudicadoTotal = CDec(m_dblImporteAux / .Cambio)
                    If Not bDistintos Then
                        .AdjudicadoReal = .Adjudicado
                        .AdjudicadoRealMonProce = .AdjudicadoMonProce
                        oGrupo.AdjudicadoTotalReal = oGrupo.AdjudicadoTotal
                    End If
                Else
                    If Not bDistintos Then
                        .AdjudicadoReal = .AdjudicadoSinReal
                        .AdjudicadoRealMonProce = .AdjudicadoSinRealMonProce
                        oGrupo.AdjudicadoTotalReal = oGrupo.AdjudicadoReal
                    End If
                    .Adjudicado = .AdjudicadoSin
                    .AdjudicadoMonProce = .AdjudicadoSinMonProce
                    oGrupo.AdjudicadoTotal = oGrupo.Adjudicado
                End If
                .Ahorrado = .Consumido - .Adjudicado
                .AhorradoMonProce = .ConsumidoMonProce - .AdjudicadoMonProce
                If .Consumido <> 0 Then
                    .AhorradoPorcen = CDec(.Ahorrado / Abs(.Consumido)) * 100
                Else
                    .AhorradoPorcen = 0
                End If
                
                i = 1
                frmOrigen.sdbgGruposProve.MoveFirst 'Me coloco en la fila correspondiente al grupo
                While frmOrigen.sdbgGruposProve.Columns("COD").Value <> oGrupo.Codigo And i <= frmOrigen.sdbgGruposProve.Rows
                    frmOrigen.sdbgGruposProve.MoveNext
                    i = i + 1
                Wend
                If frmOrigen.sdbgGruposProve.Columns("COD").Value = oGrupo.Codigo Then
                    frmOrigen.sdbgGruposProve.Columns("Consumido" & sProv).Value = NullToStr(CDec(.ConsumidoMonProce))
                    frmOrigen.sdbgGruposProve.Columns("Adjudicado" & sProv).Value = NullToStr(CDec(.AdjudicadoMonProce))
                    frmOrigen.sdbgGruposProve.Columns("Ahorrado" & sProv).Value = NullToStr(CDec(.AhorradoMonProce))
                    frmOrigen.sdbgGruposProve.Columns("Ahorro%" & sProv).Value = NullToStr(.AhorradoPorcen)
                    frmOrigen.sdbgGruposProve.Update
                End If
                frmOrigen.sdbgProveGrupos.MoveFirst
                i = 1
                While frmOrigen.sdbgProveGrupos.Columns("COD").Value <> sProv And i <= frmOrigen.sdbgProveGrupos.Rows
                    frmOrigen.sdbgProveGrupos.MoveNext
                    i = i + 1
                Wend
                If frmOrigen.sdbgProveGrupos.Columns("COD").Value = sProv Then
                    frmOrigen.sdbgProveGrupos.Columns("Consumido" & oGrupo.Codigo).Value = NullToStr(CDec(.ConsumidoMonProce))
                    frmOrigen.sdbgProveGrupos.Columns("Adjudicado" & oGrupo.Codigo).Value = NullToStr(CDec(.AdjudicadoMonProce))
                    frmOrigen.sdbgProveGrupos.Columns("Ahorrado" & oGrupo.Codigo).Value = NullToStr(CDec(.AhorradoMonProce))
                    frmOrigen.sdbgProveGrupos.Columns("Ahorro%" & oGrupo.Codigo).Value = NullToStr(.AhorradoPorcen)
                    frmOrigen.sdbgProveGrupos.Update
                End If
                frmOrigen.sdbgProveGrupos.MoveFirst
            End With
        End If
        
        'Guardo el nuevo total de adjudicaciones de grupo para esta oferta.
        For Each objAdj In frmOrigen.m_oAdjs
            If objAdj.Prove = sProv Then
                scod2 = CStr(objAdj.Grupo) & sProv & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv))
                
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProc + frmOrigen.m_oAdjs.Item(scod2).Adjudicado
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcMonProce + frmOrigen.m_oAdjs.Item(scod2).AdjudicadoMonProce
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcReal + frmOrigen.m_oAdjs.Item(scod2).AdjudicadoReal
                ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce = ProcesoSeleccionado.Ofertas.Item(sProv).AdjudicadoSinProcRealMonProce + frmOrigen.m_oAdjs.Item(scod2).AdjudicadoRealMonProce
            End If
        Next

        'Suma a los datos del proceso
        frmOrigen.m_dblConsumidoProc = frmOrigen.m_dblConsumidoProc + .Consumido
        frmOrigen.m_dblConsumidoProcReal = frmOrigen.m_dblConsumidoProcReal + .Consumido
        frmOrigen.m_dblAdjudicadoSinProc = frmOrigen.m_dblAdjudicadoSinProc + oGrupo.AdjudicadoTotal
        frmOrigen.m_dblAdjudicadoSinProcReal = frmOrigen.m_dblAdjudicadoSinProcReal + oGrupo.AdjudicadoTotalReal
        frmOrigen.m_dblAdjudicadoProc = frmOrigen.m_dblAdjudicadoSinProc
        frmOrigen.m_dblAdjudicadoProcReal = frmOrigen.m_dblAdjudicadoSinProcReal
        
        frmOrigen.AplicarAtributosDeProceso
        frmOrigen.CargarGridResultados
        
        '************ ACTUALIZACION GRID ESCALADOS *************
        'Buscar la fila del proveedor
        vbm = sdbgEscalado.Bookmark
        iFilaItem = 0
        If sdbgEscalado.Columns("PROV").Value = 1 Then iFilaItem = FilaItem(sdbgEscalado.Columns("ART").Value)
        vBmFilaItem = sdbgEscalado.AddItemBookmark(iFilaItem)
        'Actualizar los datos del proveedor en el grid
        sdbgEscalado.Columns("IMP").Value = IIf(dblImporteAdjudicadoProveedor = 0, "", dblImporteAdjudicadoProveedor)
        If dblImporteAdjudicadoProveedor = 0 And dblImporteAhorroProve = 0 Then
            sdbgEscalado.Columns("AHORROIMP").Value = ""
        Else
            sdbgEscalado.Columns("AHORROIMP").Value = dblImporteAhorroProve
        End If
        If dblImporteAdjudicadoProveedor = 0 And dblPorcenAhorroProve = 0 Then
            sdbgEscalado.Columns("AHORROPORCEN").Value = ""
        Else
            sdbgEscalado.Columns("AHORROPORCEN").Value = dblPorcenAhorroProve
        End If
        sdbgEscalado.Columns("PRECAPE").Value = oAdjProve.PresUnitario
        If dblImporteAdjudicadoProveedor = 0 And oAdjProve.Precio = 0 Then
            sdbgEscalado.Columns("CODPROVEACTUAL").Value = ""
        Else
            sdbgEscalado.Columns("CODPROVEACTUAL").Value = oAdjProve.Precio
        End If
        sdbgEscalado.Update
    
        'Item
        sdbgEscalado.Row = vBmFilaItem
        sdbgEscalado.Columns("IMP").Value = IIf(dblImporteAdjudicadoItem = 0, "", dblImporteAdjudicadoItem)
        If dblImporteAdjudicadoItem = 0 And dblImporteAhorroItem = 0 Then
            sdbgEscalado.Columns("AHORROIMP").Value = ""
        Else
            sdbgEscalado.Columns("AHORROIMP").Value = dblImporteAhorroItem
        End If
        If dblImporteAdjudicadoItem = 0 And dblPorcenAhorroItem = 0 Then
            sdbgEscalado.Columns("AHORROPORCEN").Value = ""
        Else
            sdbgEscalado.Columns("AHORROPORCEN").Value = dblPorcenAhorroItem
        End If
        sdbgEscalado.Columns("PRECAPE").Value = IIf(.Consumido = 0, "", .Consumido)
        sdbgEscalado.Bookmark = vbm
        
        frmOrigen.sdbgResultados.MoveFirst
        frmOrigen.sdbgResultados.MoveNext
        frmOrigen.sdbgResultados.Columns("Consumido").Value = oGrupo.Consumido
        frmOrigen.sdbgResultados.Columns("Abierto").Value = oGrupo.Abierto
        frmOrigen.sdbgResultados.Columns("Adjudicado").Value = oGrupo.AdjudicadoTotal
        frmOrigen.sdbgResultados.Columns("Ahorrado").Value = oGrupo.Consumido - oGrupo.AdjudicadoTotal
        If oGrupo.Consumido = 0 Then
            frmOrigen.sdbgResultados.Columns("%").Value = 0
        Else
            frmOrigen.sdbgResultados.Columns("%").Value = CDec((oGrupo.Consumido - oGrupo.AdjudicadoTotal) / Abs(oGrupo.Consumido)) * 100
        End If
        frmOrigen.sdbgResultados.MoveFirst
                
        'sdbgtotalesProve y sdbgtotales2
        frmOrigen.RestaurarTotalesGeneral
    End With

    m_bRecalculando = False

    Set oOferta = Nothing
    Set oItem = Nothing
    Set oAdj = Nothing
    Set oAdjProve = Nothing
    Set oAdjProveReal = Nothing
    Set oProve = Nothing
End Sub

''' <summary>Devuelve la cantidad del item para el c�lculo de ahorros en funci�n del tipo de escalado</summary>
''' <remarks>Llamada desde: sdbgEscalado_GrpHeadClick</remarks>
''' <revision>21/03/2012</revision>

Private Sub sdbgEscalado_GrpHeadClick(ByVal GrpIndex As Integer)
    If GrpIndex = 0 Then
        CargarGridOcultarItems
        
        picOcultarItemsEsc.Left = (frmOrigen.Width / 2) - (picOcultarItemsEsc.Width / 2)
        picOcultarItemsEsc.Top = (UserControl.Height / 2) - (picOcultarItemsEsc.Height / 2)
        picOcultarItemsEsc.Visible = True
    End If
End Sub

''' <summary>Carga el grid para ocultar/mostrar items</summary>
''' <remarks>Llamada desde: sdbgEscalado_GrpHeadClick</remarks>
''' <revision>21/03/2012</revision>

Private Sub CargarGridOcultarItems()
    Dim oGrupo As CGrupo
        
    sdbgOcultarItems.RemoveAll
    
    If m_bGrupo Then
        CargarGridOcultarItemsGrupo GrupoSeleccionado
    Else
        For Each oGrupo In ProcesoSeleccionado.Grupos
            CargarGridOcultarItemsGrupo oGrupo
        Next
        Set oGrupo = Nothing
    End If
End Sub

''' <summary>Carga en el grid para ocultar/mostrar items los items de un grupo</summary>
''' <param name="oGrupo">Grupo</param>
''' <remarks>Llamada desde: CargarGridOcultarItems</remarks>
''' <revision>21/03/2012</revision>

Private Sub CargarGridOcultarItemsGrupo(ByVal oGrupo As CGrupo)
    Dim oItem As CItem
    Dim bCheckItem As Boolean
    
    If Not oGrupo.Items Is Nothing Then
        For Each oItem In oGrupo.Items
            bCheckItem = True
            
            If Not m_dcItemsOcultos Is Nothing Then
                If m_dcItemsOcultos.Count > 0 Then
                    If m_dcItemsOcultos.Exists(CStr(oItem.Id)) Then bCheckItem = False
                End If
            End If
            
            sdbgOcultarItems.AddItem BooleanToSQLBinary(bCheckItem) & Chr(m_lSeparador) & oItem.ArticuloCod & " " & oItem.Descr & Chr(m_lSeparador) & oItem.Id
        Next
        Set oItem = Nothing
    End If
End Sub

Private Sub sdbgEscalado_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
    Dim i As Integer
    
    If ProcesoSeleccionado Is Nothing Then Exit Sub
    
    If m_bGrupo Then
        VistaSeleccionadaGr.HayCambios = True
    Else
        VistaSeleccionadaAll.HayCambios = True
    End If
        
    With sdbgEscalado
        If GrpIndex > 1 Then
            'Si redimensiona un escalado
            For i = 2 To .Groups.Count - 1
                If i <> GrpIndex Then
                    .Groups(i).Width = .ResizeWidth
                End If
            Next i
        End If
    End With
End Sub

Private Sub sdbgEscalado_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 9 Then m_bTabPressed = True
End Sub
''' <summary>
''' Antes de salir comprobar
''' </summary>
''' <remarks>Llamada desde: Sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgEscalado_LostFocus()
    Dim oGrupo As CGrupo
    
    If m_bCambioCantAdjEsc And Not m_bComprobandoActualizarGrid Then
        With sdbgEscalado
            If .Columns("PROV").Value = "1" Then
                Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                    
                If Not ComprobarActualizarFilaGrid(oGrupo, .Columns("ART").Value) Then
                    If Me.frmOrigen.Visible Then .SetFocus
                End If
                
                Set oGrupo = Nothing
            End If
        End With
    End If
End Sub

Private Sub sdbgEscalado_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim ToolTip As String
Dim VisibleRow As Integer
Dim VisibleCol As Integer
Dim Bookmark As Variant
RaiseEvent GridMouseMove(Button, Shift, X, Y)
ToolTip = ""
If sdbgEscalado.WhereIs(X, Y) = ssWhereIsData Then
    'Get the Visible Row Number
    VisibleRow = sdbgEscalado.RowContaining(Y)
    VisibleCol = sdbgEscalado.ColContaining(X, Y)
    'Get the bookmark of the row
    Bookmark = sdbgEscalado.RowBookmark(VisibleRow)
    'Set the tooltip of the Grid to the first column of the row the mouse is currently over.
    If sdbgEscalado.Columns(VisibleCol).Name = "VINCULADO_ITEM" Then
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Adj) Then ToolTip = sdbgEscalado.Columns("ERRORINT").CellValue(Bookmark)
    End If
End If
sdbgEscalado.ToolTipText = ToolTip
End Sub

''' <summary>
''' Evento de rat�n sobre el grid sdbgEscalado. Boton derecho en datos, saca menu popup de comparativa con lo
''' q tenga acceso el proveedor.
''' </summary>
''' <param name="Button">Button izq/central/der pulsado</param>
''' <param name="Shift">Tecla Shift pulsada o no</param>
''' <param name="X">Coordenada X donde se cliqueo</param>
''' <param name="Y">Coordenada X donde se cliqueo</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgEscalado_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim iIndex As Integer
    Dim oAdj As CAdjudicacion
    Dim oGrupo As CGrupo
    
    If Button = 2 Then
        With sdbgEscalado
            iIndex = .RowContaining(Y)
            If iIndex > -1 Then
                .Row = iIndex

                iIndex = .ColContaining(X, Y)
                If iIndex > -1 Then
                    If .Columns("PROV").Value = "1" Then
                        'Se posiciona en la fila
                        mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
                        mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
                        'Un click izq seguido de un click der rapido mas este click izq por programa ES un dblclick izq??
                        m_bMouseUpDblClick = True
                        DoEvents
                        m_bMouseUpDblClick = False
                        
                        If .Columns(iIndex).Name = "DESCR" Then
                            'Si se pulsa el bot�n derecho y es la fila de un proveedor, mostrar el men� del proveedor
                            RaiseEvent ProveMouseUp(.Columns("ID").Value)
                        ElseIf Left(.Columns(iIndex).Name, 4) = "PRES" Then
                            If sdbgEscalado.Columns(iIndex).Group = -1 Then
                                Exit Sub
                            End If
                            
                            mnuDesadjudicar.Visible = True
                            mnuAdjudicar.Visible = True
                            
                            'Si es una columna de precio de un escalado mostrar el men� de adjudicar/desadjudicar
                            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                            Set oAdj = oGrupo.Adjudicaciones.Item(KeyEscalado(.Columns("ID").Value, .Columns("ART").Value, .Columns("ESC" & .Columns(iIndex).Group).Value))
                            mnuDesadjudicar.Visible = (Not oAdj Is Nothing)
                            mnuAdjudicar.Visible = (oAdj Is Nothing)
                                
                            PopupMenu mnuAdjudicacion
                            
                            Set oAdj = Nothing
                            Set oGrupo = Nothing
                        End If
                    End If
                End If
            End If
        End With
    End If
End Sub
''' <summary>
''' Antes de salir de la celda, comprobar y desactivar/activar columnas
''' </summary>
''' <param name="LastRow">row de la q sales</param>
''' <param name="LastCol">col de la q sales</param>
''' <remarks>Llamada desde: Sistema; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgEscalado_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim oatrib As CAtributo
    Dim oItem As CItem
    Dim oGrupo As CGrupo
    Dim vBmk As Variant
    
    With sdbgEscalado
        If .col > -1 Then
            Select Case .Columns(.col).Group
                Case 0
                    Select Case .Columns(.col).Name
                        Case "DESCR"
                            .Columns(.col).Locked = False
                            If .Columns("PROV").Value = "0" Then
                                .Columns("DESCR").Style = ssStyleEditButton
                            Else
                                .Columns("DESCR").Style = ssStyleEdit
                            End If
                        Case "CANT"
                            If Not ProcesoSeleccionado.Bloqueado Then
                                .Columns(.col).Locked = True
                            Else
                                If .Columns("PROV").Value = "0" Then
                                    .Columns(.col).Locked = False
                                Else
                                    Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                                    If ProveSinOfes(oGrupo.UltimasOfertas, Trim(.Columns("ID").Value), True, oGrupo.Escalados) Then
                                        'Si el proveedor no tiene ofertas o Tiene oferta pero sin precios se bloquea
                                        .Columns(.col).Locked = True
                                    Else
                                        If CantidadProveedorEditable(oGrupo, .Columns("ART").Value, .Columns("ID").Value) Then
                                            .Columns(.col).Locked = False
                                        Else
                                            .Columns(.col).Locked = True
                                        End If
                                    End If
                                End If
                            End If
                    End Select
                Case 1
                    If .Columns("PROV").Value = 0 Then
                        .Columns(.col).Locked = True
                        .Columns.Item(.col).DropDownHwnd = 0
                    Else
                        If Not ProcesoSeleccionado.Bloqueado Then
                            .Columns(.col).Locked = True
                            .Columns.Item(.col).DropDownHwnd = 0
                        Else
                            .Columns(.col).Locked = False
                            
                            'Si vamos a modificar un atributo comprobamos si es de tipo lista:
                            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                            Set oatrib = oGrupo.AtributosItem.Item(CStr(.Columns(.col).Name))
                            If Not oatrib Is Nothing Then
                                If oatrib.TipoIntroduccion = Introselec Then  'De tipo lista
                                    sdbddValor.RemoveAll
                                    sdbddValor.AddItem ""
                                    If oatrib.Tipo = TipoNumerico Then
                                        sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
                                    Else
                                        sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
                                    End If
                                    .Columns.Item(.col).DropDownHwnd = sdbddValor.hWnd
                                    sdbddValor.Enabled = True
                                Else  'Libre
                                    If oatrib.Tipo = TipoBoolean Then
                                        sdbddValor.RemoveAll
                                        sdbddValor.AddItem ""
                                        .Columns.Item(.col).DropDownHwnd = sdbddValor.hWnd
                                        sdbddValor.Enabled = True
                                    Else
                                        .Columns.Item(.col).DropDownHwnd = 0
                                        sdbddValor.Enabled = False
                                    End If
                                End If
                                Set oatrib = Nothing
                            End If
                        End If
                    End If
                Case Else
                    Select Case Left(.Columns(.col).Name, Len(.Columns(.col).Name) - Len(CStr(.Grp)))
                        Case "PRES"
                            If Not ProcesoSeleccionado.Bloqueado Then
                                .Columns(.col).Locked = True
                            Else
                                .Columns(.col).Locked = False
                            End If
                            If .Columns("PROV").Value = "1" Then
                                Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                                If oGrupo.UltimasOfertas.Item(Trim(.Columns("ID").Value)) Is Nothing Then
                                    'Si el proveedor no tiene ofertas se bloquea
                                    .Columns(.col).Locked = True
                                Else
                                    .Columns(.col).Style = ssStyleEditButton
                                End If
                            Else
                                .Columns(.col).Style = ssStyleEdit
                            End If
                        Case "AHORRO"
                            If .Columns("PROV").Value = "1" Then
                                'El ahorro es calculado
                                .Columns(.col).Locked = True
                            Else
                                If Not ProcesoSeleccionado.Bloqueado Then
                                    .Columns(.col).Locked = True
                                Else
                                    .Columns(.col).Locked = False
                                End If
                            End If
                        Case "ADJCANT"
                            If .Columns("PROV").Value = "0" Then
                                .Columns(.col).Locked = True
                            Else
                                Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
                                If ProveSinOfes(oGrupo.UltimasOfertas, Trim(.Columns("ID").Value), True, oGrupo.Escalados) Then
                                    'Si el proveedor no tiene ofertas o Tiene oferta pero sin precios se bloquea
                                    .Columns(.col).Locked = True
                                Else
                                    .Columns(.col).Locked = False
                                End If
                            End If
                    End Select
            End Select
                        
        End If
        
        If Not IsNull(LastRow) Then
            If val(LastRow) > -1 Then
                vBmk = .AddItemBookmark(.Row)
                
                'Cuando se cambia de fila presionando el tab el valor de LastRow es el mismo que el vBmk de .Row (como si no se hubiera cambiado de fila)
                If (val(vBmk) <> val(LastRow)) Or (m_bTabPressed And (val(vBmk) = val(LastRow)) And (.col < val(LastCol))) Then
                    'vBmk = .AddItemBookmark(val(LastRow))
                    If .Columns("PROV").CellValue(LastRow) = "1" Then
                        Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").CellValue(LastRow))
                        If Not ComprobarActualizarFilaGrid(oGrupo, .Columns("ART").CellValue(LastRow)) Then
                            '.Row = val(LastRow)
                            If m_bTabPressed Then
                                If .Row > 1 Then .Bookmark = .AddItemBookmark(.Row - 1)
                            Else
                                .Bookmark = LastRow
                            End If
                            .col = val(LastCol)
                        End If
                    End If
                End If
            End If
        End If
    End With
    
    If m_bTabPressed Then m_bTabPressed = False
    
    Set oGrupo = Nothing
    Set oItem = Nothing
    Set oatrib = Nothing
End Sub

Private Sub sdbgEscalado_RowLoaded(ByVal Bookmark As Variant)
If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Adj) And sdbgEscalado.Columns("ERRORINT").Value <> "" Then
    sdbgEscalado.Columns("VINCULADO_ITEM").CellStyleSet "YellowERPError"
Else
    sdbgEscalado.Columns("VINCULADO_ITEM").CellStyleSet "Yellow"
End If

If sdbgEscalado.Columns("PROV").CellValue(Bookmark) = "1" Then
    AplicarEstiloFila "Prove"
    sdbgEscalado.Columns("CANT").CellStyleSet "Blue"
    AsignarFormatoProveedor sdbgEscalado.Columns("ID").CellValue(Bookmark)
Else
    AplicarEstiloFila "Item"
    AsignarFormatoItem
End If
End Sub

''' <summary>Asigna el formato correspondiente a la celda del Proveedor en funci�n de su estado</summary>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <remarks>Llamada desde: sdbgEscalado_RowLoaded</remarks>
''' <revision>21/03/2012</revision>

Private Sub AsignarFormatoProveedor(ByVal scodProve As String)
    Dim oProve As CProveedor
    Dim sCadenaAviso  As String
    Dim sCadenaContrato As String
    Dim bComprobarAviso As Boolean
    Dim bComprobarContrato  As Boolean
    Dim oStyleSetSource As StyleSet
    Dim oStyleSet As StyleSet
    Dim sStyleName As String
    Dim i As Integer
    Dim oEscalado As CEscalado
    Dim bModifPorPubMatProve As Boolean
    Dim oGrupo As CGrupo
    Dim iGroupIndex As Integer
    
    Set oProve = ProvesAsig.Item(scodProve)
    If Not oProve Is Nothing Then
        'Si el item est� cerrado poner el proveedor con el mismo formato
        With sdbgEscalado
            If .Columns("CERRADO").Value Then
                For i = 0 To .Columns.Count - 1
                    .Columns(i).CellStyleSet "ItemCerrado"
                Next i
                Exit Sub
            End If
        End With
        
        'Marca los proveedores que son el Proveedor actual para el item en curso
        If Not ProcesoSeleccionado.AdminPublica Then
            If scodProve = sdbgEscalado.Columns("CODPROVEACTUAL").Value And _
                    Trim$(sdbgEscalado.Columns("CODPROVEACTUAL").Value) <> "" And sdbgEscalado.Columns("CANT").Value <> "" Then
                sdbgEscalado.Columns("CANT").CellStyleSet "ProvActual", sdbgEscalado.Row
                sdbgEscalado.Columns("DESCR").CellStyleSet "ProvActual", sdbgEscalado.Row
            End If
        End If
        
        bComprobarAviso = False
        If m_bComparativaQA Then
            bComprobarAviso = frmOrigen.ComprobarAvisoRestriccion(oProve.Cod)
        End If
        bComprobarContrato = frmOrigen.ComprobarContrato(oProve.Cod)
        
        sCadenaAviso = ""
        sCadenaContrato = ""
        If bComprobarAviso Then sCadenaAviso = "YAviso"
        If bComprobarContrato Then sCadenaContrato = "YContrato"
        
        With sdbgEscalado.Columns("DESCR")
            If oProve.NoOfe And Not oProve.HaOfertado Then
                sStyleName = "NOOFE" & sCadenaAviso & sCadenaContrato
            Else
                If Not oProve.HaOfertado Then
                    If oProve.Notificado Then
                        sStyleName = "NOTIF" & sCadenaAviso & sCadenaContrato
                    Else
                        If Not bComprobarAviso Then
                            If bComprobarContrato Then
                                sStyleName = "Contrato"
                            Else
                                sStyleName = "Proveedor"
                            End If
                        Else
                            sStyleName = "Aviso" & sCadenaContrato
                        End If
                    End If
                Else
                    'If frmOrigen.MostrarClip(oProve.Cod) Then
                    'En las filas de proveedor se guarda el id del item correspondiente en la columna ART
                    If frmOrigen.MostrarClipItem(sdbgEscalado.Columns("GRUPOCOD").Value, sdbgEscalado.Columns("ART").Value, oProve.Cod) Then
                        If Not ProcesoSeleccionado.Ofertas.Item(oProve.Cod) Is Nothing Then
                            If Not ProcesoSeleccionado.Ofertas.Item(oProve.Cod).Usuario Is Nothing Then
                                If ProcesoSeleccionado.Ofertas.Item(oProve.Cod).portal Then
                                    sStyleName = "ClipYMundo" & sCadenaAviso & sCadenaContrato
                                Else
                                    sStyleName = "ClipYPieza" & sCadenaAviso & sCadenaContrato
                                End If
                            Else
                                sStyleName = "ProveedorConAdjuntos" & sCadenaAviso & sCadenaContrato
                            End If
                        End If
                    Else
                        If Not ProcesoSeleccionado.Ofertas.Item(oProve.Cod) Is Nothing Then
                            If Not ProcesoSeleccionado.Ofertas.Item(oProve.Cod).Usuario Is Nothing Then
                                If ProcesoSeleccionado.Ofertas.Item(oProve.Cod).portal Then
                                    sStyleName = "IntroPORTAL" & sCadenaAviso & sCadenaContrato
                                Else
                                    sStyleName = "IntroGS" & sCadenaAviso & sCadenaContrato
                                End If
                            Else
                                If Not bComprobarAviso Then
                                    If bComprobarContrato Then
                                        sStyleName = "Contrato"
                                    Else
                                        sStyleName = "Proveedor"
                                    End If
                                Else
                                    sStyleName = "Aviso" & sCadenaContrato
                                End If
                            End If
                        Else
                            If Not bComprobarAviso Then
                                If bComprobarContrato Then
                                    sStyleName = "Contrato"
                                Else
                                    sStyleName = "Proveedor"
                                End If
                            Else
                                sStyleName = "Aviso" & sCadenaContrato
                            End If
                        End If
                    End If
                End If
            End If
            
            If sdbgEscalado.Columns("CERRADO").Value Then
                'Si el item est� cerrado el fondo tiene que ser de color gris
                Set oStyleSetSource = sdbgEscalado.StyleSets(sStyleName)
                sStyleName = sStyleName & "YCerrado"
                Set oStyleSet = sdbgEscalado.StyleSets(sStyleName)
                oStyleSet.AlignmentPicture = oStyleSetSource.AlignmentPicture
                Set oStyleSet.Font = oStyleSetSource.Font
                oStyleSet.Backcolor = ColorGrey
                oStyleSet.Forecolor = ColorWhite
                Set oStyleSet.Picture = oStyleSetSource.Picture
            Else
                'si la oferta es no adjudicable el nombre del proveedor saldr� en blanco
                'El cambio de la prop. ForeColor de la celda no funciona. Se coge el estilo, se crea uno
                'nuevo para el caso de no adjudicable y se asigna a la celda
                If Not OfertaAdjudicable(scodProve) Then
                    Set oStyleSetSource = sdbgEscalado.StyleSets(sStyleName)
                    sStyleName = sStyleName & "YNoAdj"
                    Set oStyleSet = sdbgEscalado.StyleSets(sStyleName)
                    oStyleSet.AlignmentPicture = oStyleSetSource.AlignmentPicture
                    Set oStyleSet.Font = oStyleSetSource.Font
                    oStyleSet.Forecolor = ColorWhite
                    oStyleSet.Backcolor = ColorButton
                    Set oStyleSet.Picture = oStyleSetSource.Picture
                End If
            End If
            
            .CellStyleSet sStyleName
        End With
        
        iGroupIndex = 2
        With sdbgEscalado
            Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").Value)
            For Each oEscalado In oGrupo.Escalados
                'Marca en gris los items que no se pueden modificar porque el proveedor tiene restricci�n por material.
                'Solo para multimaterial
                If gParametrosGenerales.gbMultiMaterial And ProcesoSeleccionado.PubMatProve Then
                    bModifPorPubMatProve = True
                    'Comprobemos si los proveedores tienen restringidos los items a los de su material
                    bModifPorPubMatProve = frmOrigen.m_oProcesos.ComprobarPubMatProve(ProcesoSeleccionado.Anyo, ProcesoSeleccionado.GMN1Cod, ProcesoSeleccionado.Cod, oGrupo.Id, .Columns("ART").Value, .Columns.Item("ART").Value)
                    If Not bModifPorPubMatProve Then
                        For i = 0 To .Columns.Count - 1
                            .Columns(i).CellStyleSet "Grey"
                        Next
                    End If
                End If
    
                If .Columns("ADJ" & iGroupIndex).Value <> "0" Then
                    If frmOrigen.MostrarClipItem(oGrupo.Codigo, .Columns("ART").Value, oProve.Cod) Then
                        .Columns("PRES" & iGroupIndex).CellStyleSet "AdjudicadoClip", .Row
                    Else
                        .Columns("PRES" & iGroupIndex).CellStyleSet "Adjudicado", .Row
                    End If
                Else
                    If frmOrigen.MostrarClipItem(oGrupo.Codigo, .Columns("ART").Value, oProve.Cod) Then
                        If .Columns("HOM").Value <> 1 And oGrupo.Items.Item(.Columns("ART").Value).ArticuloCod <> "" And Not basParametros.gParametrosGenerales.gbPermAdjSinHom Then
                            .Columns("PRES" & iGroupIndex).CellStyleSet "NoHomologadoClip", .Row
                        Else
                            .Columns("PRES" & iGroupIndex).CellStyleSet "YellowClip", .Row
                        End If
                    End If
                End If

                'Marca en rojo o en verde seg�n el ahorro sea negativo o positivo
                If Not .Columns("AHORRO" & iGroupIndex).Value = "" Then
                    If IsNumeric(.Columns("AHORRO" & iGroupIndex).Value) Then
                        If CDbl(.Columns("AHORRO" & iGroupIndex).Value) < 0 Then
                            .Columns("AHORRO" & iGroupIndex).CellStyleSet "Red"
                        ElseIf CDbl(.Columns("AHORRO" & iGroupIndex).Value) > 0 Then
                            .Columns("AHORRO" & iGroupIndex).CellStyleSet "Green"
                        Else
                            .Columns("AHORRO" & iGroupIndex).CellStyleSet "Yellow"
                        End If
                    End If
                End If
                
                iGroupIndex = iGroupIndex + 1
            Next
            
            Set oGrupo = Nothing
        End With
    End If
End Sub

''' <summary>Asigna el formato correspondiente a la fila del item</summary>
''' <remarks>Llamada desde: sdbgEscalado_RowLoaded</remarks>
''' <revision>21/03/2012</revision>

Private Sub AsignarFormatoItem()
    Dim i As Integer
    
    With sdbgEscalado
        'Marca los items que est�n cerrados
        If .Columns("CERRADO").Value Then
            For i = 0 To .Columns.Count - 1
                .Columns(i).CellStyleSet "ItemCerrado"
            Next i
            Exit Sub
        End If
    End With
End Sub

''' <summary>Indica si una oferta es adjudicable para un proveedor</summary>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <remarks>Llamada desde: sdbgEscalado_RowLoaded</remarks>
''' <revision>21/03/2012</revision>

Private Function OfertaAdjudicable(ByVal scodProve As String) As Boolean
    OfertaAdjudicable = True
    
    If Not ProcesoSeleccionado.Ofertas.Item(scodProve) Is Nothing Then
        OfertaAdjudicable = EstOfes.Item(CStr(ProcesoSeleccionado.Ofertas.Item(scodProve).CodEst)).adjudicable
    End If
End Function

''' <summary>Aplica un estilo a las columnas del grid</summary>
''' <param name="StyleSet">Nombre del estilo a aplicar</param>
''' <remarks>Llamada desde: sdbgEscalado_RowLoaded</remarks>
''' <revision>21/03/2012</revision>

Private Sub AplicarEstiloFila(ByVal StyleSet As String)
    Dim i As Integer
    
    For i = 0 To sdbgEscalado.Cols - 1
        sdbgEscalado.Columns(i).CellStyleSet StyleSet
    Next
End Sub

Private Sub sdbgEscalado_RowResize(Cancel As Integer)
    If m_bGrupo Then
        If VistaSeleccionadaGr Is Nothing Then Exit Sub
        
        If PermitirModifVistas Then
            VistaSeleccionadaGr.HayCambios = True
        Else
            VistaSeleccionadaGr.HayCambios = False
        End If
    Else
        If VistaSeleccionadaAll Is Nothing Then Exit Sub
        
        If PermitirModifVistas Then
            VistaSeleccionadaAll.HayCambios = True
        Else
            VistaSeleccionadaAll.HayCambios = False
        End If
    End If
End Sub

Private Sub sdbgOcultarItems_Change()
    Dim i As Integer
    Dim vbm As Variant
    Dim bInc As Boolean
    
    With sdbgOcultarItems
        bInc = .Columns("INC").Value
        
        If Not m_colOcultarItemsBookmarks Is Nothing Then
            i = 1
            While i <= m_colOcultarItemsBookmarks.Count
                vbm = m_colOcultarItemsBookmarks(i)
                .Bookmark = vbm
                .Columns("INC").Value = bInc
                                    
                i = i + 1
            Wend
            
            Set m_colOcultarItemsBookmarks = Nothing
        End If
    End With
End Sub

Private Sub sdbgOcultarItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim vbm As Variant
    
    With sdbgOcultarItems
        If .col = -1 Then
            Set m_colOcultarItemsBookmarks = New Collection
            For Each vbm In .SelBookmarks
                m_colOcultarItemsBookmarks.Add vbm
            Next
        End If
    End With
End Sub

Private Sub UserControl_Initialize()
    PonerFieldSeparator
    ColorWhite = RGB(255, 255, 255)
    ColorButton = RGB(212, 208, 200)
    ColorGrey = RGB(192, 192, 192)
    ColorBlue = RGB(0, 0, 192)
End Sub

''' <summary>Establece el separador de campos en controles dentro del UserControl</summary>
''' <remarks>Llamada desde:Initialize</remarks>
''' <revision>21/03/2012</revision>

Public Sub PonerFieldSeparator()
    Dim sdg As Control
    Dim Name4 As String
    Dim Name5 As String
        
    For Each sdg In UserControl.Controls
        
        Name4 = LCase(Left(sdg.Name, 4))
        Name5 = LCase(Left(sdg.Name, 5))
        
        If Name4 = "sdbg" Or Name4 = "sdbc" Or Name5 = "sdbdd" Or Name4 = "ssdb" Then
            If sdg.DataMode = ssDataModeAddItem Then
                sdg.FieldSeparator = Chr(m_lSeparador)
            End If
        End If
    Next
End Sub

Private Sub UserControl_Resize()
    sdbgEscalado.Left = 0
    sdbgEscalado.Top = 0
    sdbgEscalado.Width = UserControl.Width
    sdbgEscalado.Height = UserControl.Height
End Sub

''' <summary>Borra las adjudicaciones de proveedor almacenadas</summary>
''' <remarks>Llamada desde: frmADJ.RestaurarDatos   ; Tiempo m�ximo: 0,3</remarks>
''' <revision>21/03/2012</revision>

Public Sub BorrarAdjudicaciones()
    Set m_oAdjsProve = oFSGSRaiz.Generar_CAdjudicaciones
    Set AdjsProve = oFSGSRaiz.Generar_CAdjudicaciones
End Sub

''' <summary>Cargar las Adjudicaciones de un Grupo con escalado</summary>
''' <param name="bNoCargarGrupo">Si recargar la grid</param>
''' <remarks>Llamada desde: GrupoEscaladoSeleccionado   ; Tiempo m�ximo: 0,3</remarks>
''' <revision>21/03/2012</revision>

Public Sub CargarAdjudicacionesGrupo(Optional ByVal bNoCargarGrupo As Boolean)
    Dim iNumAtribCaract As Integer
    Dim oVarsQA As CVariablesCalidad
    
    Screen.MousePointer = vbHourglass
    
    m_bGrupo = True

    LockWindowUpdate UserControl.hWnd

    Set oVarsQA = oFSGSRaiz.Generar_CVariablesCalidad
    If Not (gParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso Or (Not oUsuarioSummit.ConsultarVariablesCalidad And basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador) Or Not oVarsQA.ExistenVariables) Then
        m_bComparativaQA = True
    Else
        m_bComparativaQA = False
    End If
    Set m_oAdjsProve = oFSGSRaiz.Generar_CAdjudicaciones
    
    CargarRecursos
    
    If Not bNoCargarGrupo Then
    
        'Prepara la grid poni�ndola en la configuraci�n normal para la carga
        PrepararGridGruposEscalados
        
        'comprueba que haya ofertas
        If GrupoSeleccionado.UltimasOfertas Is Nothing Then
            Exit Sub
        End If
                
        'Carga de los grupos de atributos y escalados en el grid
        iNumAtribCaract = CrearGruposDinamicos(GrupoSeleccionado, True)
        
        MostrarSplit
    Else
        iNumAtribCaract = sdbgEscalado.Groups(1).Columns.Count
    End If
    
    CalcularAdjudicacionesGrupo
     
    RellenarGridGrupo iNumAtribCaract
    
    RedimensionarGridGrupo
    frmOrigen.RellenarGridResumen

    LockWindowUpdate 0&

    Screen.MousePointer = vbDefault
End Sub

''' <summary>Cargar las Adjudicaciones de la pesta�a ALL</summary>
''' <param name="bNoCargar">Si recargar la grid</param>
''' <remarks>Llamada desde: GrupoEscaladoSeleccionado   ; Tiempo m�ximo: 0,3</remarks>
''' <revision>21/03/2012</revision>

Public Sub CargarAdjudicacionesAll(Optional ByVal bNoCargar As Boolean)
    Dim iNumAtribCaract As Integer
    Dim oVarsQA As CVariablesCalidad
    
    Screen.MousePointer = vbHourglass

    m_bGrupo = False

    LockWindowUpdate UserControl.hWnd

    Set oVarsQA = oFSGSRaiz.Generar_CVariablesCalidad
    If Not (gParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso Or (Not oUsuarioSummit.ConsultarVariablesCalidad And basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador) Or Not oVarsQA.ExistenVariables) Then
        m_bComparativaQA = True
    Else
        m_bComparativaQA = False
    End If
    Set m_oAdjsProve = oFSGSRaiz.Generar_CAdjudicaciones
'    Set AdjsProve = oFSGSRaiz.Generar_CAdjudicaciones
    
    CargarRecursos
    
    If Not bNoCargar Then
        'Prepara la grid poni�ndola en la configuraci�n normal para la carga
        PrepararGridGruposEscalados
                
        'Carga de los grupos de atributos y escalados en el grid
        CrearGruposDinamicos ProcesoSeleccionado.Grupos.Item(1), False
        
        MostrarSplit
    End If
    
    CalcularAdjudicacionesAll
    RellenarGridAll iNumAtribCaract
    RedimensionarGridAll

    LockWindowUpdate 0&

    Screen.MousePointer = vbDefault
End Sub

''' <summary>Calcula los datos de las adjudicaciones del grupo para mostrar en el grid</summary>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Private Sub CalcularAdjudicacionesAll()
    Dim oGrupo As CGrupo
    
    For Each oGrupo In ProcesoSeleccionado.Grupos
        Set m_oAdjsProve = CalcularAdjudicacionesInterno(ProcesoSeleccionado, oGrupo, ProvesAsig, AtribsFormulas)
    Next
    
    Set oGrupo = Nothing
End Sub

''' <summary>Calcula los datos de las adjudicaciones del grupo para mostrar en el grid</summary>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Private Sub CalcularAdjudicacionesGrupo()
    Set m_oAdjsProve = CalcularAdjudicacionesInterno(ProcesoSeleccionado, GrupoSeleccionado, ProvesAsig, AtribsFormulas)
End Sub

''' <summary>Rellena el grid con los datos de items y proveedores</summary>
''' <param name="iNumAtribCaract">n� de atributos de caracter�stica</param>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Function RellenarGridAll(ByVal iNumAtribCaract As Integer)
    Dim oGrupo As CGrupo
    Dim colProvesVisibles As Collection
    Dim oProve As CProveedor
    
    sdbgEscalado.RemoveAll
    sdbgEscalado.MoveFirst
    
    'Crear una colecci�n con la visibilidad de los proveedores
    If VistaSeleccionadaAll.Vista <> TipoDeVistaDefecto.vistainicial Then
        Set colProvesVisibles = New Collection
        For Each oProve In ProvesAsig
            colProvesVisibles.Add VistaSeleccionadaAll.ConfVistasAllProv.Item(oProve.Cod).Visible, oProve.Cod
        Next
        Set oProve = Nothing
    End If
    
    For Each oGrupo In ProcesoSeleccionado.Grupos
        If Not oGrupo Is Nothing Then
            RellenarGrid oGrupo, iNumAtribCaract, False, VistaSeleccionadaAll.Vista, VistaSeleccionadaAll.OcultarItCerrados, colProvesVisibles
        End If
    Next
    
    Set oGrupo = Nothing
    Set colProvesVisibles = Nothing
End Function

''' <summary>Rellena el grid con los datos de items y proveedores</summary>
''' <param name="oGrupo">Objeto grupo cuyos datos hay que rellenar</param>
''' <param name="iNumAtribCaract">n� de atributos de caracter�stica</param>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Function RellenarGridGrupo(ByVal iNumAtribCaract As Integer)
    Dim colProvesVisibles As Collection
    Dim oProve As CProveedor
     
    sdbgEscalado.RemoveAll
    sdbgEscalado.MoveFirst
    
    'Crear una colecci�n con la visibilidad de los proveedores
    If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
        Set colProvesVisibles = New Collection
        For Each oProve In ProvesAsig
            colProvesVisibles.Add VistaSeleccionadaGr.ConfVistasGrProv.Item(oProve.Cod).Visible, oProve.Cod
        Next
        Set oProve = Nothing
    End If
    
    If Not GrupoSeleccionado Is Nothing Then
        RellenarGrid GrupoSeleccionado, iNumAtribCaract, True, VistaSeleccionadaGr.Vista, VistaSeleccionadaGr.OcultarItCerrados, colProvesVisibles
    End If
    
    Set colProvesVisibles = Nothing
End Function

''' <summary>Rellena el grid con los datos de items y proveedores</summary>
''' <param name="oGrupo">Objeto grupo cuyos datos hay que rellenar</param>
''' <param name="iNumAtribCaract">n� de atributos de caracter�stica</param>
''' <param name="bCargarAtrib">Indica si hay que cargar los atributos</param>
''' <param name="TipoVista">Tipo de vista</param>
''' <param name="bOcultarItCerrados">Indica si hay que ocultar los items cerrados</param>
''' <param name="colProvesVisibles">colecci�n con la visibilidad de los proveedores</param>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Private Function RellenarGrid(ByVal oGrupo As CGrupo, ByVal iNumAtribCaract As Integer, ByVal bCargarAtrib As Boolean, ByVal TipoVista As TipoDeVistaDefecto, _
        ByVal bOcultarItCerrados As Boolean, ByVal colProvesVisibles As Collection)
    Dim oItem As CItem
    Dim bAddItem As Boolean
       
    For Each oItem In oGrupo.Items
        bAddItem = True
        
        If Not m_dcItemsOcultos Is Nothing Then
            If m_dcItemsOcultos.Count > 0 Then
                If m_dcItemsOcultos.Exists(CStr(oItem.Id)) Then bAddItem = False
            End If
        End If
        
        If bAddItem Then
            'A�adimos el item al grid
            If AddItem(oGrupo, oItem, iNumAtribCaract, bCargarAtrib, bOcultarItCerrados) Then
                'A�adimos los proveedores
                AddProveedores oGrupo, oItem, iNumAtribCaract, bCargarAtrib, TipoVista, bOcultarItCerrados, colProvesVisibles
            End If
        End If
    Next
    
    Set oItem = Nothing
End Function

''' <summary>Muestra el split del grid</summary>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Private Sub MostrarSplit()
    With sdbgEscalado
        If .Groups.Count > 1 Then
            .SplitterVisible = True
            .SplitterPos = 1
        Else
            .SplitterVisible = False
        End If
    End With
End Sub

''' <summary>Carga los textos en el idioma del usuario</summary>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>LTG 17/02/2012</revision>

Private Sub CargarRecursos()
    With sdbgEscalado
        .Groups(0).caption = Textos(17)
        
        .Groups(0).Columns("DESCR").caption = Textos(5) & " (" & Textos(6) & ")" & vbCrLf & vbCrLf & vbCrLf & Textos(4)
        .Groups(0).Columns("CANT").caption = Textos(27) & vbCrLf & vbCrLf & vbCrLf & Textos(10)
        .Groups(0).Columns("PRECAPE").caption = Textos(24) & vbCrLf & vbCrLf & vbCrLf & Textos(25)
        .Groups(0).Columns("CODPROVEACTUAL").caption = Textos(23) & vbCrLf & vbCrLf & vbCrLf & Textos(26)
        .Groups(0).Columns("IMP").caption = Textos(9) & vbCrLf & vbCrLf & vbCrLf & Textos(9)
        .Groups(0).Columns("AHORROIMP").caption = Textos(3) & vbCrLf & vbCrLf & vbCrLf & Textos(3)
        .Groups(0).Columns("AHORROPORCEN").caption = Textos(8) & vbCrLf & vbCrLf & vbCrLf & Textos(8)
        .Groups(0).Columns("VINCULADO_ITEM").caption = Textos(31) & vbCrLf & vbCrLf & vbCrLf & Textos(31)
        .Groups(0).Columns("INI").caption = Textos(32)
        .Groups(0).Columns("FIN").caption = Textos(33)
        .Groups(0).Columns("GMN").caption = Textos(34)
        lblCheck.caption = Textos(18)
        cmdOk.caption = Textos(19)
        mnuAdjudicar.caption = Textos(20)
        mnuDesadjudicar.caption = Textos(21)
    End With
End Sub

''' <summary>Muestra/Oculta las columnas fijas del grid</summary>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Private Sub PrepararGridGruposEscalados()
    Dim i As Integer
    Dim bComprobarGrupoSolicVinculada As Boolean
    Dim oGrupo As CGrupo
    Dim bComprobarItemSolicVinculada As Boolean
    Dim oItem As CItem

    With sdbgEscalado
        .RemoveAll
    
        'Si se ha hecho un scroll se deja la grid en la posici�n original
        If .GrpPosition(0) > 0 Then
            i = .Groups(0).Position
            .Scroll -i, 0
        End If
        
        For i = 0 To .Groups.Count - 1
            .Groups(i).Position = i
        Next i
        
        .AllowColumnMoving = ssRelocateWithinGroup
    
        'Hace visibles todos los grupos de escalados
        For i = 2 To .Groups.Count - 1
            .Groups(i).Visible = True
        Next i
        
        .Groups(1).Columns.RemoveAll
        
        i = .Groups.Count
        While i > 2
            .Groups.Remove (2)
            i = .Groups.Count
        Wend
        
        bComprobarItemSolicVinculada = False
        If (NullToDbl0(ProcesoSeleccionado.SolicitudId) <> 0) Then
            If m_bGrupo Then
                VistaSeleccionadaGr.SolicVinculadaVisible = False
            Else
                VistaSeleccionadaAll.SolicVinculadaVisible = False
            End If
        Else
            If m_bGrupo Then
                If (NullToDbl0(GrupoSeleccionado.SolicitudId) <> 0) Then
                    VistaSeleccionadaGr.SolicVinculadaVisible = False
                Else
                    For Each oItem In GrupoSeleccionado.Items
                        If NullToDbl0(oItem.SolicitudId) > 0 Then
                            bComprobarItemSolicVinculada = True
                            Exit For
                        End If
                    Next
                    If bComprobarItemSolicVinculada Then
                        bComprobarItemSolicVinculada = VistaSeleccionadaGr.SolicVinculadaVisible
                    Else
                        VistaSeleccionadaGr.SolicVinculadaVisible = False
                    End If
                End If
            Else
                bComprobarGrupoSolicVinculada = False
                
                For Each oGrupo In ProcesoSeleccionado.Grupos
                    If (NullToDbl0(oGrupo.SolicitudId) > 0) Then
                        bComprobarGrupoSolicVinculada = True
                        Exit For
                    End If
                    
                    For Each oItem In oGrupo.Items
                        If NullToDbl0(oItem.SolicitudId) > 0 Then
                            bComprobarItemSolicVinculada = True
                        End If
                    Next
                Next
                
                If bComprobarGrupoSolicVinculada Then
                    VistaSeleccionadaAll.SolicVinculadaVisible = False
                ElseIf bComprobarItemSolicVinculada Then
                    bComprobarItemSolicVinculada = VistaSeleccionadaAll.SolicVinculadaVisible
                Else
                    VistaSeleccionadaAll.SolicVinculadaVisible = False
                End If
            End If
        End If
        
        .Redraw = True
    End With
        
    DoEvents
End Sub

''' <summary>Crea en el grid los grupos que no son fijos: atributos y escalados</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="bCargarAtrib">Indica si hay que cargar los atributos</param>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo; Tiempo m�ximo:0</remarks>
''' <returns>Entero indicando el n� de columnas de atributos</returns>
''' <revision>21/03/2012</revision>

Private Function CrearGruposDinamicos(ByVal oGrupo As CGrupo, ByVal bCargarAtrib As Boolean) As Integer
    Dim iNumAtribCarac As Integer
    
    '����Si no, no se muestran datos en el grid?????
    sdbgEscalado.AddItem ""
    sdbgEscalado.col = 1
    sdbgEscalado.RemoveItem 0
    DoEvents
    
    'Creaci�n de grupos para los atributos de caracter�stica
    If bCargarAtrib Then iNumAtribCarac = CrearGrupoAtributosCaracteristicas
    
    'Creaci�n de grupos para los escalados
    CrearGruposEscalados oGrupo, bCargarAtrib
    
    CrearGruposDinamicos = iNumAtribCarac
End Function

''' <summary>A�ade los grupos de atributos de caracter�sticas al grid</summary>
''' <remarks>Llamada desde: CrearGruposDinamicos; Tiempo m�ximo:0</remarks>
''' <returns>Entero indicando el n� de columnas de atributos</returns>
''' <revision>21/03/2012</revision>

Private Function CrearGrupoAtributosCaracteristicas() As Integer
    Dim ogroup As SSDataWidgets_B.Group
    Dim oColumn As SSDataWidgets_B.Column
    Dim oatrib As CAtributo
    Dim i As Integer
    Dim bCrearGrupo As Boolean
        
    bCrearGrupo = False
    With sdbgEscalado
        'Columnas (una por cada atributo)
        If Not GrupoSeleccionado.AtributosItem Is Nothing Then
            'Primer bucle para crear el grupo y calcular su anchura (si se crea sin dar la anchura total,
            'al darle una anchura al final redimensiona s�lo la �ltima columna)
            i = 0
            For Each oatrib In GrupoSeleccionado.AtributosItem
                'Son atributos de caracter�stica si no tienen operaci�n asociada
                If IsNull(oatrib.PrecioFormula) Or IsEmpty(oatrib.PrecioFormula) Then
                    i = i + 1
                    bCrearGrupo = True
                End If
            Next
            
            '.Groups.Add 1
            Set ogroup = .Groups(1)
            ogroup.TagVariant = "ATRIB_CARAC"
            ogroup.caption = Textos(14)
            ogroup.CaptionAlignment = ssCaptionAlignmentCenter
            ogroup.AllowSizing = True
            ogroup.HeadStyleSet = "HeadBold"
            If bCrearGrupo Then
                ogroup.Visible = (VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial)
                
                i = 0
                For Each oatrib In GrupoSeleccionado.AtributosItem
                    'Son atributos de caracter�stica si no tienen operaci�n asociada
                    If IsNull(oatrib.PrecioFormula) Or IsEmpty(oatrib.PrecioFormula) Then
                        ogroup.Columns.Add i
                        Set oColumn = ogroup.Columns(i)
                        oColumn.Name = oatrib.idAtribProce
                        oColumn.caption = vbCrLf & vbCrLf & vbCrLf & oatrib.Cod
                        oColumn.Alignment = ssCaptionAlignmentRight
                        oColumn.CaptionAlignment = ssColCapAlignCenter
                        oColumn.HeadStyleSet = "DobleFondo"
                        DoEvents
                                                
                        i = i + 1
                    End If
                Next
                
                'Dar tama�o al grupo y las columnas
                If ogroup.Columns.Count > 1 Then
                    ogroup.Width = ogroup.Columns.Count * 800
                Else
                    ogroup.Width = 1600
                End If
                For Each oColumn In ogroup.Columns
                    oColumn.Width = 800
                Next
            Else
                ogroup.Visible = False
                ogroup.Columns.RemoveAll
            End If
            
            Set oatrib = Nothing
            Set ogroup = Nothing
        End If
    End With
    
    CrearGrupoAtributosCaracteristicas = i
End Function

''' <summary>A�ade los grupos de escalados al grid</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="bCargarAtrib">Indica si hay que cargar los atributos</param>
''' <remarks>Llamada desde: CrearGruposDinamicos; Tiempo m�ximo:0</remarks>
''' <revision>LTG 27/02/2012</revision>

Private Sub CrearGruposEscalados(ByVal oGrupo As CGrupo, ByVal bCargarAtrib As Boolean)
    Dim oEscalado As CEscalado
    Dim ogroup As SSDataWidgets_B.Group
    Dim oColumn As SSDataWidgets_B.Column
    Dim oatrib As CAtributo
    Dim sCaption As String
    Dim iGroupIndex
    Dim i As Integer
    
    If Not oGrupo.Escalados Is Nothing Then
        If oGrupo.Escalados.Count > 0 Then
            iGroupIndex = 2
            For Each oEscalado In oGrupo.Escalados
                With sdbgEscalado
                    .Groups.Add iGroupIndex
                    Set ogroup = .Groups(iGroupIndex)
                    
                    sCaption = Textos(12) & " (" & oGrupo.UnidadEscalado & ")" & vbCrLf & oEscalado.Inicial
                    If Not IsEmpty(oEscalado.final) And Not IsNull(oEscalado.final) Then
                        sCaption = sCaption & " - " & oEscalado.final
                    End If
        
                    ogroup.TagVariant = oEscalado.Id
                    ogroup.caption = sCaption
                    ogroup.AllowSizing = True
                    ogroup.CaptionAlignment = ssCaptionAlignmentCenter
                    ogroup.HeadStyleSet = "HeadBold"
        
                    'Columnas
                    ogroup.Columns.Add 0
                    Set oColumn = ogroup.Columns(0)
                    oColumn.caption = "ESC"
                    oColumn.Name = "ESC" & iGroupIndex  'oEscalado.Id
                    oColumn.Visible = False
        
                    ogroup.Columns.Add 1
                    Set oColumn = ogroup.Columns(1)
                    oColumn.caption = "INI"
                    oColumn.Name = "INI" & iGroupIndex  'oEscalado.Id
                    oColumn.Visible = False
        
                    ogroup.Columns.Add 2
                    Set oColumn = ogroup.Columns(2)
                    oColumn.caption = "FIN"
                    oColumn.Name = "FIN" & iGroupIndex  'oEscalado.Id
                    oColumn.Visible = False
        
                    ogroup.Columns.Add 3
                    Set oColumn = ogroup.Columns(3)
                    oColumn.caption = Textos(7) & vbCrLf & vbCrLf & vbCrLf & Textos(11)
                    oColumn.Name = "PRES" & iGroupIndex 'oEscalado.Id
                    oColumn.HeadStyleSet = "DobleFondo"
                    oColumn.Alignment = ssCaptionAlignmentCenter
                    oColumn.Style = ssStyleEditButton
                    
                    ogroup.Columns.Add 4
                    Set oColumn = ogroup.Columns(4)
                    oColumn.caption = Textos(1) & vbCrLf & vbCrLf & vbCrLf & Textos(13)
                    oColumn.Name = "AHORRO" & iGroupIndex   'oEscalado.Id
                    oColumn.HeadStyleSet = "DobleFondo"
                    oColumn.Alignment = ssCaptionAlignmentCenter
                    
                    ogroup.Columns.Add 5
                    Set oColumn = ogroup.Columns(5)
                    oColumn.caption = Textos(10) & vbCrLf & vbCrLf & vbCrLf & Textos(10)
                    oColumn.Name = "ADJCANT" & iGroupIndex  'oEscalado.Id
                    oColumn.HeadStyleSet = "DobleFondo"
                    oColumn.Alignment = ssCaptionAlignmentCenter
                    oColumn.HeadForeColor = ColorBlue
                    
                    ogroup.Columns.Add 6
                    Set oColumn = ogroup.Columns(6)
                    oColumn.caption = "ADJIMP"
                    oColumn.Name = "ADJIMP" & iGroupIndex   'oEscalado.Id
                    oColumn.Visible = False
                    
                    ogroup.Columns.Add 7
                    Set oColumn = ogroup.Columns(7)
                    oColumn.caption = "ADJ"
                    oColumn.Name = "ADJ" & iGroupIndex   'oEscalado.Id
                    oColumn.Visible = False
                                        
                    'A�adir columnas de atributos de costes/descuentos
                    i = 1
                    If bCargarAtrib Then
                        For Each oatrib In oGrupo.AtributosItem
                            'Son atributos de costes/descuentos si tienen operaci�n asociada
                            If Not IsNull(oatrib.PrecioFormula) And Not IsEmpty(oatrib.PrecioFormula) Then
                                ogroup.Columns.Add 7 + i
                                Set oColumn = ogroup.Columns(7 + i)
                                oColumn.caption = vbCrLf & vbCrLf & vbCrLf & oatrib.Cod
                                oColumn.Name = CStr(oatrib.idAtribProce) & iGroupIndex  'CStr(oEscalado.Id)
                                oColumn.HeadStyleSet = "DobleFondo"
                                oColumn.Visible = (VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial)
                        
                                i = i + 1
                            End If
                        Next
                    End If
                    
                    'Dar tama�o al grupo y las columnas
                    ogroup.Width = 2400 + ((i - 1) * 800)
                    i = 1
                    For Each oColumn In ogroup.Columns
                        If oColumn.Visible Then
                            'Las 2 primeras columnas m�s grandes
                            If i <= 2 Then
                                oColumn.Width = 1200
                            Else
                                oColumn.Width = 800
                            End If
                            
                            i = i + 1
                        End If
                    Next
        
                    Set oColumn = Nothing
                End With
        
                iGroupIndex = iGroupIndex + 1
            Next
            
            Set oEscalado = Nothing
            Set ogroup = Nothing
            Set oColumn = Nothing
        End If
    End If
End Sub

''' <summary>A�ade los proveedores al grid</summary>
''' <param name="oGrupo">Grupo del Item para el que se est�n a�adiendo los proveedores</param>
''' <param name="oItem">Item para el que se est�n a�adiendo los proveedores</param>
''' <param name="iNumAtribCarac">N� de atributos de caracter�stica</param>
''' <param name="bCargarAtrib">Indica si hay que cargar los atributos</param>
''' <param name="TipoVista">Tipo de vista</param>
''' <param name="bOcultarItCerrados">Indica si hay que ocultar los items cerrados</param>
''' <param name="colProvesVisibles">colecci�n con la visibilidad de los proveedores</param>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Private Sub AddProveedores(ByVal oGrupo As CGrupo, ByVal oItem As CItem, ByVal iNumAtribCarac As Integer, ByVal bCargarAtrib As Boolean, _
        ByVal TipoVista As TipoDeVistaDefecto, ByVal bOcultarItCerrados As Boolean, ByVal colProvesVisibles As Collection)
    Dim oProve As CProveedor
    Dim bCargar As Boolean
    Dim scodProve As String
    
    For Each oProve In ProvesAsig
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            If Asigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                bCargar = False
            End If
        End If
        
        'Comprobar si el proveedor est� visible en la vista
        If TipoVista <> TipoDeVistaDefecto.vistainicial Then
            bCargar = bCargar And colProvesVisibles.Item(oProve.Cod)
        End If
        
        If bCargar Then
            AddProveedor oGrupo, oItem, oProve, bCargarAtrib, TipoVista, bOcultarItCerrados
        End If
    Next
End Sub

''' <summary>A�ade un proveedor al grid</summary>
''' <param name="oGrupo">Grupo del item para el que se est�n a�adiendo los proveedores</param>
''' <param name="oItem">Item para el que se est�n a�adiendo los proveedores</param>
''' <param name="sCodProve">C�digo del proveedor a a�adir</param>
''' <param name="bCargarAtrib">Indica si hay que cargar los atributos</param>
''' <param name="TipoVista">Tipo de vista</param>
''' <param name="bOcultarItCerrados">Indica si hay que ocultar los items cerrados</param>
''' <param name="vIndex">Indice del grid en que se quiere a�adir el proveedor</param>
''' <remarks>Llamada desde: AddProveedores ; Tiempo m�ximo:0</remarks>
''' <revision>LTG 17/02/2012</revision>

Private Sub AddProveedor(ByVal oGrupo As CGrupo, ByVal oItem As CItem, ByVal oProve As CProveedor, ByVal bCargarAtrib As Boolean, ByVal TipoVista As TipoDeVistaDefecto, _
        ByVal bOcultarItCerrados As Boolean, Optional ByVal vIndex As Variant)
    Dim scodProve As String
    Dim oAsig As COferta
    Dim oatrib  As CAtributo
    Dim sFilaGrid As String
    Dim sCodAtrib As String
    Dim sValorAtrib As String
    Dim oEscalado As CEscalado
    Dim sCod As String
    Dim strCantidad As String
    Dim strPorcen As String
    Dim strImporteAdj As String
    Dim sAhorro As String
    Dim strCantAdjProve As String
    Dim strImpAdjProve As String
    Dim strAhorroImpProve As String
    Dim strAhorroPorcenProve As String
    Dim strPresUniProve As String
    Dim strPrecUniProve As String
    Dim dblImporteProve As Double
    Dim vPresEsc As Variant
    Dim strAdj As String
    Dim sFormatoNumerico As String
    Dim dCambioOfe As Double
    
    If (TipoVista <> TipoDeVistaDefecto.vistainicial) And (bOcultarItCerrados And oItem.Cerrado) Then Exit Sub
    
    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
    
    Set oAsig = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                            
    sFilaGrid = oProve.Cod & Chr(m_lSeparador) & oItem.Id & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & IIf(oItem.Cerrado, 1, 0) & Chr(m_lSeparador)
    sFilaGrid = sFilaGrid & "" & Chr(m_lSeparador)
    sFilaGrid = sFilaGrid & ""
    If Not oAsig Is Nothing Then
        sFilaGrid = sFilaGrid & "" & Chr(m_lSeparador) & oAsig.Num & Chr(m_lSeparador) & "1" & Chr(m_lSeparador)
        dCambioOfe = oItem.CambioComparativa(oProve.Cod, True, m_oAdjsProve)
    Else
        sFilaGrid = sFilaGrid & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador)
        dCambioOfe = 1
    End If
    sFilaGrid = sFilaGrid & oProve.Cod & " - " & oProve.Den
    
    'Datos de las adjudicaciones
    If Not m_oAdjsProve Is Nothing Then
        If Not m_oAdjsProve.Item(CStr(oItem.Id) & scodProve) Is Nothing Then
            strCantAdjProve = NullToStr(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).Adjudicado)
            If IsNull(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj) Or IsEmpty(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj) Or NullToDbl0(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj) = 0 Then
                strImpAdjProve = ""
            Else
                strImpAdjProve = CStr(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj)
            End If
            If NullToDbl0(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdj) > 0 Then
                strAhorroImpProve = IIf(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).importe = 0, "", m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).importe)
                strPresUniProve = NullToStr(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).PresUnitario)
                If NullToStr(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).Precio) = "" Then
                    strPrecUniProve = ""
                Else
                    strPrecUniProve = CDec(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).Precio / dCambioOfe)
                End If
                dblImporteProve = NullToDbl0(m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).ImporteAdjTot)
                If dblImporteProve <> 0 Then
                    If m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).importe = 0 Then
                        strAhorroPorcenProve = ""
                    Else
                        strAhorroPorcenProve = CDec((m_oAdjsProve.Item(CStr(oItem.Id) & scodProve).importe / dblImporteProve) * 100)
                    End If
                End If
            End If
        End If
    End If
    'Columna CANT
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strCantAdjProve
    'Columna PRECAPE: Presupuesto unitario medio
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strPresUniProve
    'Columna PROVEACT: Precio unitario medio
    If Not VistaSeleccionadaGr Is Nothing Then
        sFormatoNumerico = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
    ElseIf Not VistaSeleccionadaAll Is Nothing Then
        sFormatoNumerico = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
    Else
        sFormatoNumerico = ""
    End If
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & Format(strPrecUniProve, sFormatoNumerico)
    'Columna IMP
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strImpAdjProve
    'Columna AHORROIMP
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strAhorroImpProve
    'Columna AHORROPORCEN
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strAhorroPorcenProve
    
    'Rellena columna CantMax
    If Not oAsig Is Nothing Then
        If Not oAsig.Lineas.Item(CStr(oItem.Id)) Is Nothing Then
            If Not IsEmpty(oAsig.Lineas.Item(CStr(oItem.Id)).CantidadMaxima) Or IsNull(oAsig.Lineas.Item(CStr(oItem.Id)).CantidadMaxima) Then
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAsig.Lineas.Item(CStr(oItem.Id)).CantidadMaxima
            Else
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
            End If
            'Columna HOM
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oAsig.Lineas.Item(CStr(oItem.Id)).Hom
        Else
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        End If
    Else
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
    End If
    
    'Columna GRUPOCOD
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oItem.grupoCod
    
    'Solicitud Vinculada
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & NullToStr(oItem.SolicitudId)
    
    'Fechas de suministro y estr. material en el item
    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
             
    'Columnas de atributos de caracter�sticas
    If bCargarAtrib Then
        For Each oatrib In oGrupo.AtributosItem
            'Son atributos de caracter�stica si no tienen operaci�n asociada
            If IsNull(oatrib.PrecioFormula) Or IsEmpty(oatrib.PrecioFormula) Then
                sValorAtrib = ""
                sCodAtrib = CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)
                If Not oAsig Is Nothing Then
                    If Not oAsig.AtribItemOfertados Is Nothing Then
                        If Not oAsig.AtribItemOfertados.Item(sCodAtrib) Is Nothing Then
                            Select Case oatrib.Tipo
                                Case TiposDeAtributos.TipoBoolean
                                    If Not IsNull(oAsig.AtribItemOfertados.Item(sCodAtrib).valorBool) Then sValorAtrib = DevolverValorBoolean(oAsig.AtribItemOfertados.Item(sCodAtrib).valorBool)
                                Case TiposDeAtributos.TipoFecha
                                    If Not IsNull(oAsig.AtribItemOfertados.Item(sCodAtrib).valorFec) Then sValorAtrib = oAsig.AtribItemOfertados.Item(sCodAtrib).valorFec
                                Case TiposDeAtributos.TipoNumerico
                                    If Not IsNull(oAsig.AtribItemOfertados.Item(sCodAtrib).valorNum) Then
                                        If oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-" Then
                                            sValorAtrib = oAsig.AtribItemOfertados.Item(sCodAtrib).valorNum / dCambioOfe
                                        Else
                                            sValorAtrib = oAsig.AtribItemOfertados.Item(sCodAtrib).valorNum
                                        End If
                                    End If
                                Case TiposDeAtributos.TipoString
                                    sValorAtrib = EliminarTab(NullToStr(oAsig.AtribItemOfertados.Item(sCodAtrib).valorText))
                            End Select
                        End If
                    End If
                End If
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sValorAtrib
            End If
        Next
    End If
    
    'Asignaciones y Adjudicaciones de escalados
    If Not oGrupo.Escalados Is Nothing Then
        If oGrupo.Escalados.Count > 0 Then
            For Each oEscalado In oGrupo.Escalados
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oEscalado.Id & Chr(m_lSeparador) & oEscalado.Inicial & Chr(m_lSeparador) & oEscalado.final
                
                sCod = CStr(oItem.Id) & scodProve & CStr(oEscalado.Id)
                
                'Rellena columna PRES
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & NullToStr(PrecioOfertaEscalado(oGrupo, oProve.Cod, oItem.Id, oEscalado.Id))
                
                'Columna AHORRO
                sAhorro = vbNullString
                If Not oAsig Is Nothing Then
                    If Not oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                        If Not IsNull(oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)).PrecioOferta) Then
                            vPresEsc = oItem.PresupuestoEscalado(oEscalado.Id)
                            If Not IsNull(vPresEsc) Then
                                sAhorro = CStr(vPresEsc - (oAsig.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEscalado.Id)).PrecioOferta / dCambioOfe))
                            End If
                        End If
                    End If
                End If
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sAhorro
                
                'Columnas ADJCANT, ADJIMP, AHORRO
                strCantidad = vbNullString
                strPorcen = "100"
                strImporteAdj = vbNullString
                strAdj = "0"
                If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                    strAdj = "1"
                    If NullToDbl0(oGrupo.Adjudicaciones.Item(sCod).Adjudicado) > 0 Then
                        strCantidad = oGrupo.Adjudicaciones.Item(sCod).Adjudicado
                    End If
                    If Not IsNull(oGrupo.Adjudicaciones.Item(sCod).ImporteAdj) And Not IsEmpty(oGrupo.Adjudicaciones.Item(sCod).ImporteAdj) Then
                        strImporteAdj = oGrupo.Adjudicaciones.Item(sCod).ImporteAdj / dCambioOfe
                    End If
                End If
                                                   
                'Rellena columna AdjCantidad, AdjImp y Adj
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strCantidad
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strImporteAdj
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & strAdj
                
                'columnas de los atributos de costes/descuentos
                If bCargarAtrib Then
                    For Each oatrib In oGrupo.AtributosItem
                        If Not IsNull(oatrib.PrecioFormula) And Not IsEmpty(oatrib.PrecioFormula) Then
                            sValorAtrib = ""
                            sCodAtrib = CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)
                            If Not oAsig Is Nothing Then
                                If Not oAsig.AtribItemOfertados Is Nothing Then
                                If Not oAsig.AtribItemOfertados.Item(sCodAtrib) Is Nothing Then
                                    If Not oAsig.AtribItemOfertados.Item(sCodAtrib).Escalados Is Nothing Then
                                        If Not oAsig.AtribItemOfertados.Item(sCodAtrib).Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                                            Select Case oatrib.Tipo
                                                Case TiposDeAtributos.TipoNumerico
                                                    If Not IsNull(oAsig.AtribItemOfertados.Item(sCodAtrib).valorNum) Then
                                                        If oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-" Then
                                                            sValorAtrib = oAsig.AtribItemOfertados.Item(sCodAtrib).Escalados.Item(CStr(oEscalado.Id)).valorNum / dCambioOfe
                                                        Else
                                                            sValorAtrib = NullToStr(oAsig.AtribItemOfertados.Item(sCodAtrib).Escalados.Item(CStr(oEscalado.Id)).valorNum)
                                                        End If
                                                    End If
                                            End Select
                                        End If
                                    End If
                                End If
                                End If
                            End If
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sValorAtrib
                        End If
                    Next
                End If
            Next
            
            Set oEscalado = Nothing
        End If
    End If
    
    If Not IsMissing(vIndex) Then
        sdbgEscalado.AddItem sFilaGrid, vIndex
    Else
        sdbgEscalado.AddItem sFilaGrid
    End If
End Sub

''' <summary>Devuelve el precio de la oferta para un proveedor, item y escalado en la moneda de proceso</summary>
''' <param name="oGrupo">Grupo al que pertenece el item</param>
''' <param name="sCodProve">Cod. proveedor</param>
''' <param name="lItem">Id. del item</param>
''' <param name="lIdEscalado">Id del escalado</param>
''' <returns>precio, si no NULL</returns>
''' <remarks>Llamada desde: AddProveedor, AdjudicarEscalado; Tiempo m�ximo:0</remarks>
''' <revision>LTG 17/02/2012<revision>

Private Function PrecioOfertaEscalado(ByVal oGrupo As CGrupo, ByVal scodProve As String, ByVal lItem As Long, ByVal lIdEscalado As Long) As Variant
    Dim oAsig As COferta
    
    PrecioOfertaEscalado = Null
    
    Set oAsig = oGrupo.UltimasOfertas.Item(Trim(scodProve))
    If Not oAsig Is Nothing Then
        If Not oAsig.Lineas.Item(CStr(lItem)) Is Nothing Then
            If Not oAsig.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(lIdEscalado)) Is Nothing Then
                PrecioOfertaEscalado = oAsig.Lineas.Item(CStr(lItem)).Escalados.Item(CStr(lIdEscalado)).PrecioOferta / oGrupo.Items.Item(CStr(lItem)).CambioComparativa(scodProve, True, m_oAdjsProve)
            End If
        End If
    End If
    
    Set oAsig = Nothing
End Function

''' <summary>A�ade un item al grid</summary>
''' <param name="oGrupo">Grupo al que pertenece el item</param>
''' <param name="oItem">Item</param>
''' <param name="iNumAtribCarac">N� de atributos de caracter�sticas que se muestran en el grid</param>
''' <param name="bCargarAtrib">Indica si hay que cargar los atributos</param>
''' <param name="bOcultarItCerrados">Indica si hay que ocultar los items cerrados</param>
''' <returns>Booleano indicando si se ha a�adido el item</returns>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
''' <revision>LTG 17/02/2012</revision>

Private Function AddItem(ByVal oGrupo As CGrupo, ByVal oItem As CItem, ByVal iNumAtribCarac As Integer, ByVal bCargarAtrib As Boolean, _
        ByVal bOcultarItCerrados As Boolean) As Boolean
    Dim oAsig As COferta
    Dim sCod As String
    Dim bEsPC As Boolean
    Dim sObj As String
    Dim sAnyo As String
    Dim dPorcenItem As Double
    Dim dConsumidoItem As Double
    Dim dPreadjudicadoItem As Double
    Dim dCantidadAdj As Double
    Dim sFilaGrid As String
    Dim sFilaGrid1 As String
    Dim sFilaGrid2 As String
    Dim oProve As CProveedor
    Dim bCargar As Boolean
    Dim scodProve As String
    Dim oEscalado As CEscalado
    Dim oEscItem As CEscalado
    Dim oatrib As CAtributo
    Dim i As Integer
    Dim vImporteAhorroItem As Variant
    Dim vImporteItem As Variant
    Dim sPresup As String
    Dim sObjetivo As String
    Dim sCantAdj As String
    Dim sEstrMat As String
           
    AddItem = False
           
    'Rellena la grid:
    bEsPC = False
        
    If oItem.Confirmado And Not (bOcultarItCerrados And oItem.Cerrado) Then
        dPorcenItem = 0 'aqu� vamos acumulando el porcentaje del item
        dConsumidoItem = 0  'aqu� acumulamos el consumido del item
        dPreadjudicadoItem = 0  'aqu� acumulamos el preadjudicado del item
        
        sFilaGrid = ""
        sFilaGrid1 = ""
        sFilaGrid2 = ""
        
        If IsNull(oItem.Objetivo) Then
            sObj = ""
        Else
            sObj = oItem.Objetivo
        End If
        
        If oItem.Cerrado = True Then
            bEsPC = True
        End If
        
        sAnyo = Textos(6) & ": " & Right(Year(oItem.FechaInicioSuministro), 2) & "/" & oItem.DestCod
        
        With oItem
            sFilaGrid = .Id & Chr(m_lSeparador) & .ArticuloCod & Chr(m_lSeparador) & .DestCod & Chr(m_lSeparador) & IIf(.Cerrado, 1, 0) & Chr(m_lSeparador)
            sFilaGrid = sFilaGrid & .UniCod & Chr(m_lSeparador)
            sFilaGrid = sFilaGrid & .PagCod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador)
            sFilaGrid = sFilaGrid & .ArticuloCod & " " & .Descr & " (" & sAnyo & ")"
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & NullToStr(.Cantidad)
            'Importe consumido
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & IIf(.Consumido = 0, "", .Consumido)
            'Proveedor actual
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & .ProveAct
            'Importe adjudicado
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & IIf(IsNull(.ImporteAdj) Or IsEmpty(.ImporteAdj) Or NullToDbl0(.ImporteAdj) = 0, "", .ImporteAdj)
        End With
        
        vImporteAhorroItem = 0
        For Each oProve In ProvesAsig
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If Asigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                    bCargar = False
                End If
            End If
            If bCargar Then
                If Not oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                    Set oAsig = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                    
                    'Carga las adjudicaciones
                    sCod = oAsig.Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAsig.Prove))
                    sCod = CStr(oItem.Id) & sCod

                    If Not m_oAdjsProve.Item(sCod) Is Nothing Then
                        vImporteAhorroItem = vImporteAhorroItem + m_oAdjsProve.Item(sCod).importe   'Se utiliza la prop. Importe para guardar el ahorro
                        vImporteItem = vImporteItem + m_oAdjsProve.Item(sCod).ImporteAdjTot 'Se utiliza la prop. ImporteAdjTot para guardar el importe seg�n los datos del item
                    End If

                    'Va calculando el consumido y el preadjudicado del item con los valores de cada proveedor:
                    dConsumidoItem = dConsumidoItem + (NullToDbl0(oItem.Precio) * dCantidadAdj)
                    If Not oAsig.Lineas.Item(CStr(oItem.Id)) Is Nothing Then
                        dPreadjudicadoItem = dPreadjudicadoItem + ((NullToDbl0(oAsig.Lineas.Item(CStr(oItem.Id)).PrecioOferta) / oItem.CambioComparativa(oProve.Cod, True, m_oAdjsProve)) * dCantidadAdj)
                    End If
                  Else
''''                      'Si el proveedor no tiene ofertas rellena todos sus datos a vac�o.Lo hacemos as� para hacer el AddItem
''''                      'de la fila completa,porque rellenando por proveedor es mucho m�s lento.
''''                      sFilaGrid1 = sFilaGrid1 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oProve.Cod & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
''''                      For Each oAtribValor In m_oGrupoSeleccionado.AtributosItem
''''                          sFilaGrid1 = sFilaGrid1 & Chr(m_lSeparador) & ""
''''                      Next
                  End If
            End If
        Next
        Set oProve = Nothing

        'Columnas AHORROIMP, AHORROPORCEN
        If IsNull(vImporteAhorroItem) Or IsEmpty(vImporteAhorroItem) Then
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        Else
            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & IIf(vImporteAhorroItem = 0, "", vImporteAhorroItem)
            If vImporteItem > 0 Then
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & (vImporteAhorroItem / vImporteItem) * 100
            Else
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
            End If
        End If

        'columnas CantMax,HOM,GRUPOCOD
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oItem.grupoCod
                
        'Solicitud Vinculada
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & NullToStr(oItem.SolicitudId)
        
        'Fechas de suministro
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oItem.FechaInicioSuministro & Chr(m_lSeparador) & oItem.FechaFinSuministro
        
        'Estr. material
        sEstrMat = oItem.GMN1Cod & "-" & oItem.GMN2Cod & "-" & oItem.GMN3Cod & "-" & oItem.GMN4Cod & "-" & oItem.GMN4Den
        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sEstrMat & Chr(m_lSeparador) & oItem.ErrorINT

        'Columnas de atributos de caracter�sticas
        If bCargarAtrib Then
            For i = 1 To iNumAtribCarac
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
            Next
        End If
        
        'Datos de los escalados
        If Not oGrupo.Escalados Is Nothing Then
            If oGrupo.Escalados.Count > 0 Then
                For Each oEscalado In oGrupo.Escalados
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oEscalado.Id & Chr(m_lSeparador) & oEscalado.Inicial
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & NullToStr(oEscalado.final)

                    sPresup = vbNullString
                    sObjetivo = vbNullString
                    If Not oItem.Escalados Is Nothing Then
                        If Not oItem.Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                            Set oEscItem = oItem.Escalados.Item(CStr(oEscalado.Id))

                            sPresup = NullToStr(oItem.PresupuestoEscalado(oEscalado.Id))
                            sObjetivo = NullToStr(oEscItem.Objetivo)

                            Set oEscItem = Nothing
                        End If
                    End If
                    If sPresup = "" Then
                        sPresup = NullToStr(oItem.Precio)
                    End If

                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sPresup

                    'Columnas de las adjudicaciones
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sObjetivo
                    sCantAdj = NullToStr(CantidadAdjItemEscalado(oGrupo, oItem.Id, oEscalado.Id, ProvesAsig))
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & sCantAdj & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""

                    'columnas de los atributos de costes/descuentos
                    If bCargarAtrib Then
                        For Each oatrib In oGrupo.AtributosItem
                            If Not IsNull(oatrib.PrecioFormula) And Not IsEmpty(oatrib.PrecioFormula) Then
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                            End If
                        Next
                    End If
                Next

                Set oEscalado = Nothing
            End If
        End If
        
        'A�ade la fila a la grid
        sdbgEscalado.AddItem sFilaGrid
        
        AddItem = True
    End If
End Function

''' <summary>Devuelve el c�digo del item correspondiente a la fila actual</summary>
''' <returns>C�digo del item de la fila actual</returns>
''' <remarks>Llamada desde: frmOrigen.VisualizarFicherosAdjuntosItem ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Function GetCurrentItemCod() As String
    With sdbgEscalado
        If .Row > -1 Then
            If .Columns("PROV").Value = "1" Then
                GetCurrentItemCod = .Columns("ART").Value
            Else
                GetCurrentItemCod = .Columns("ID").Value
            End If
        End If
    End With
End Function

''' <summary>Devuelve el c�digo del art�culo del item correspondiente a la fila actual</summary>
''' <returns>C�digo del item de la fila actual</returns>
''' <remarks>Llamada desde: frmOrigen.VisualizarFicherosAdjuntosItem ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Function GetCurrentItemCodArt() As String
    With sdbgEscalado
        If .Row > -1 Then
            GetCurrentItemCodArt = GrupoSeleccionado.Items.Item(GetCurrentItemCod).ArticuloCod
        End If
    End With
End Function

''' <summary>Devuelve la descripci�n del item correspondiente a la fila actual</summary>
''' <returns>Desc. del item</returns>
''' <remarks>Llamada desde: frmOrigen.VisualizarFicherosAdjuntosItem ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Function GetCurrentItemDesc() As String
    With sdbgEscalado
        If .Row > -1 Then
            GetCurrentItemDesc = GrupoSeleccionado.Items.Item(GetCurrentItemCod).Descr
        End If
    End With
End Function

''' <summary>Devuelve el c�digo del proveedor de la fila actual (si es de proveedor)</summary>
''' <returns>C�digo del proveedor de la fila actual</returns>
''' <remarks>Llamada desde: frmOrigen.VisualizarDetalleProv ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Function GetCurrentProveCod() As String
    With sdbgEscalado
        If .Row > -1 Then
            If .Columns("PROV").Value = "1" Then
                GetCurrentProveCod = .Columns("ID").Value
            End If
        End If
    End With
End Function

''' <summary>Devuelve la descripci�n del proveedor de la fila actual (si es de proveedor)</summary>
''' <returns>desc. del proveedor</returns>
''' <remarks>Llamada desde: frmOrigen.IrARecepcionOfertas ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Function GetCurrentProveDesc() As String
    With sdbgEscalado
        If .Row > -1 Then
            If .Columns("PROV").Value = "1" Then
                GetCurrentProveDesc = .Columns("DESCR").Value
            End If
        End If
    End With
End Function

''' <summary>Devuelve la oferta del proveedor de la fila actual (si es de proveedor)</summary>
''' <returns>Id De la oferta</returns>
''' <remarks>Llamada desde: frmOrigen.IrARecepcionOfertas ; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Function GetCurrentProveNumOfe() As String
    With sdbgEscalado
        If .Row > -1 Then
            If .Columns("PROV").Value = "1" Then
                GetCurrentProveNumOfe = .Columns("NUMOFE").Value
            End If
        End If
    End With
End Function

''' <summary>Limpia el frid de datos</summary>
''' <remarks>Llamada desde frmOrigen.LimpiarCampos;  Tiempo m�ximo: 0</remarks>
''' <revision>21/03/2012</revision>

Public Function ClearGrid()
    Dim i As Integer
    
    With sdbgEscalado
        'Limpia a nivel del grupo
        .RemoveAll
                        
        i = .Groups.Count
        While i > 2
            .Groups(2).Visible = True
            .Groups.Remove (2)
            i = .Groups.Count
        Wend
        
        'Eliminar las columnas del grupo de caracter�sticas
        .Groups(1).Columns.RemoveAll
        .Groups(1).Visible = False
        
        Set m_oAdjsProve = Nothing
        Set AdjsProve = Nothing
    End With
End Function

''' <summary>Devuelve el string correspondiente a un valor boolean</summary>
''' <remarks>Llamada desde AddProveedores;  Tiempo m�ximo: 0</remarks>
''' <revision>21/03/2012</revision>

Private Function DevolverValorBoolean(vValor As Variant) As String
    Dim sResultado As String

    Select Case vValor
        Case True, 1
            sResultado = Textos(16)
        Case False, 0
            sResultado = Textos(15)
        Case Else
            sResultado = ""
    End Select

    DevolverValorBoolean = sResultado
End Function

''' <summary>Guarda la configuraci�n de la vista actual</summary>
''' <remarks>Llamada desde frmOrigen.GuardarVistaGr;  Tiempo m�ximo: 0</remarks>
''' <revision>LTG 17/02/2012</revision>

Public Sub GuardarVistaGr()
    Dim oAtribVista As CConfVistaGrupoAtrib
    Dim oAtributo As CAtributo
    Dim iEscVisible As Integer
    Dim iNumColsGrupo As Integer
    Dim iNumColsGR  As Integer
    Dim iNumColsGR1  As Integer
    Dim i As Integer

    'N� de columnas de los grupos de proveedores.Es necesario para obtener la posici�n de las celdas
    'por si se ha hecho scroll.Hay que hacerlo de esta forma,sino no funciona:
    With sdbgEscalado
        iNumColsGR = .Groups(0).Columns.Count + .Groups(1).Columns.Count - 1
        iNumColsGR1 = .Groups(0).Columns.Count - 1
        
        If .GrpPosition(0) > 0 Then
            i = .Groups(0).Position
            .Scroll -i, 0
            .Update
        End If
        For i = 0 To .Groups.Count - 1
            .Groups(i).Position = i
        Next i

        .AllowColumnMoving = ssRelocateWithinGroup

        'Obtiene cual es el 1� escalado que est� visible
        For iEscVisible = 2 To .Groups.Count - 1
            If .Groups(iEscVisible).Visible = True Then
                Exit For
            End If
        Next
        If iEscVisible = .Groups.Count Then Exit Sub

        VistaSeleccionadaGr.ExcluirCerradosResult = frmOrigen.chkExcluirItCerradosResul.Value
        VistaSeleccionadaGr.OcultarNoAdj = frmOrigen.chkOcultarNoAdjGr.Value
        VistaSeleccionadaGr.OcultarItCerrados = frmOrigen.chkOcultarItCerradosGr.Value
        VistaSeleccionadaGr.OcultarProvSinOfe = frmOrigen.chkOcultarProvSinOfeGr.Value

        VistaSeleccionadaGr.Grupo0Width = .Groups(0).Width  'Anchura grupo ppal
        VistaSeleccionadaGr.GruposWidth = .Groups(2).Width  'Anchura grupos escalados

        '--- GRUPO PPAL ---
        'almacena los widths de las columnas
        VistaSeleccionadaGr.DescrWidth = .Columns("DESCR").Width
        VistaSeleccionadaGr.CantidadWidth = .Columns("CANT").Width
        VistaSeleccionadaGr.PresUniWidth = .Columns("PRECAPE").Width
        VistaSeleccionadaGr.ProveedorWidth = .Columns("CODPROVEACTUAL").Width
        VistaSeleccionadaGr.ImporteWidth = .Columns("IMP").Width
        VistaSeleccionadaGr.AhorroWidth = .Columns("AHORROIMP").Width
        VistaSeleccionadaGr.AhorroPorcenWidth = .Columns("AHORROPORCEN").Width
        VistaSeleccionadaGr.SolicVinculadaWidth = .Columns("VINCULADO_ITEM").Width
        VistaSeleccionadaGr.FecIniSumWidth = .Columns("INI").Width
        VistaSeleccionadaGr.FecFinSumWidth = .Columns("FIN").Width
        VistaSeleccionadaGr.EstrMatWidth = .Columns("GMN").Width
        
        'almacena los position de las columnas
        VistaSeleccionadaGr.CantidadPos = .Columns("CANT").Position
        VistaSeleccionadaGr.PresUniPos = .Columns("PRECAPE").Position
        VistaSeleccionadaGr.ProveedorPos = .Columns("CODPROVEACTUAL").Position
        VistaSeleccionadaGr.ImportePos = .Columns("IMP").Position
        VistaSeleccionadaGr.AhorroPos = .Columns("AHORROIMP").Position
        VistaSeleccionadaGr.AhorroPorcenPos = .Columns("AHORROPORCEN").Position
        VistaSeleccionadaGr.SolicVinculadaPos = .Columns("VINCULADO_ITEM").Position
        VistaSeleccionadaGr.FecIniSumPos = .Columns("INI").Position
        VistaSeleccionadaGr.FecFinSumPos = .Columns("FIN").Position
        VistaSeleccionadaGr.EstrMatPos = .Columns("GMN").Position
        
        'almacena la visibilidad de las columnas
        VistaSeleccionadaGr.CantidadVisible = .Columns("CANT").Visible
        VistaSeleccionadaGr.PresUniVisible = .Columns("PRECAPE").Visible
        VistaSeleccionadaGr.ProveedorVisible = .Columns("CODPROVEACTUAL").Visible
        VistaSeleccionadaGr.ImporteVisible = .Columns("IMP").Visible
        VistaSeleccionadaGr.AhorroVisible = .Columns("AHORROIMP").Visible
        VistaSeleccionadaGr.AhorroPorcenVisible = .Columns("AHORROPORCEN").Visible
        VistaSeleccionadaGr.SolicVinculadaVisible = .Columns("VINCULADO_ITEM").Visible
        VistaSeleccionadaGr.FecIniSumVisible = .Columns("INI").Visible
        VistaSeleccionadaGr.FecFinSumVisible = .Columns("FIN").Visible
        VistaSeleccionadaGr.EstrMatVisible = .Columns("GMN").Visible
        
        VistaSeleccionadaGr.SolicVinculadaLevel = 0

        '--- GRUPO ATRIBUTOS DE CARACTERISTICA  ---
        'Almacena las propiedades del grupo de atributos de caracter�sticas
        If .Groups(1).TagVariant = "ATRIB_CARAC" Then
            If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
                If Not GrupoSeleccionado.AtributosItem Is Nothing Then
                    For Each oAtributo In GrupoSeleccionado.AtributosItem
                        If IsNull(GrupoSeleccionado.AtributosItem.Item(CStr(oAtributo.idAtribProce)).PrecioFormula) Or IsEmpty(GrupoSeleccionado.AtributosItem.Item(CStr(oAtributo.idAtribProce)).PrecioFormula) Then
                            VistaSeleccionadaGr.ConfVistasGrAtrib.Item(CStr(oAtributo.idAtribProce)).Posicion = .Columns(CStr(oAtributo.idAtribProce)).Position - iNumColsGR1
                            VistaSeleccionadaGr.ConfVistasGrAtrib.Item(CStr(oAtributo.idAtribProce)).Width = .Columns(CStr(oAtributo.idAtribProce)).Width
                            VistaSeleccionadaGr.ConfVistasGrAtrib.Item(CStr(oAtributo.idAtribProce)).Visible = .Columns(CStr(oAtributo.idAtribProce)).Visible
                        End If
                    Next
                End If
            End If
        End If
        
        '--- GRUPO ESCALADOS ---
        If .Groups.Count > 2 Then
            iNumColsGrupo = .Groups(iEscVisible).Columns.Count
            
            'Para las columnas de los escalados se usar�: PREC_PROV para PRES y ADJ_PROV para AHORRO
            'almacena los widths de las columnas
            VistaSeleccionadaGr.PrecioEscWidth = .Columns("PRES" & iEscVisible).Width
            VistaSeleccionadaGr.AhorroEscWidth = .Columns("AHORRO" & iEscVisible).Width
            VistaSeleccionadaGr.AdjEscWidth = .Columns("ADJCANT" & iEscVisible).Width
            'almacena los position de las columnas
            VistaSeleccionadaGr.PrecioEscPos = .Columns("PRES" & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
            VistaSeleccionadaGr.AhorroEscPos = .Columns("AHORRO" & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
            VistaSeleccionadaGr.AdjEscPos = .Columns("ADJCANT" & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
            'almacena la visibilidad de las columnas
            VistaSeleccionadaGr.PrecioEscVisible = .Columns("PRES" & iEscVisible).Visible
            VistaSeleccionadaGr.AhorroEscVisible = .Columns("AHORRO" & iEscVisible).Visible
            VistaSeleccionadaGr.AdjEscVisible = .Columns("ADJCANT" & iEscVisible).Visible
            
            'almacena los width de los atributos
            If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
                For Each oAtribVista In VistaSeleccionadaGr.ConfVistasGrAtrib
                    'Son atributos de coste/descuento si tienen operaci�n asociada
                    If Not IsNull(GrupoSeleccionado.AtributosItem.Item(CStr(oAtribVista.Atributo)).PrecioFormula) And Not IsEmpty(GrupoSeleccionado.AtributosItem.Item(CStr(oAtribVista.Atributo)).PrecioFormula) Then
                        oAtribVista.Posicion = .Columns(oAtribVista.Atributo & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
                        oAtribVista.Width = .Columns(oAtribVista.Atributo & iEscVisible).Width
                    End If
                Next
            End If
        End If

        'Almacena el ancho de la fila
        VistaSeleccionadaGr.AnchoFila = .RowHeight

        'Almacena el n�mero de decimales:
        VistaSeleccionadaGr.DecCant = frmOrigen.txtDecCantGr.Text
        VistaSeleccionadaGr.DecPorcen = frmOrigen.txtDecPorcenGr.Text
        VistaSeleccionadaGr.DecPrecios = frmOrigen.txtDecPrecGr.Text
        VistaSeleccionadaGr.DecResult = frmOrigen.txtDecResultGr.Text

'        If VistaSeleccionadaGr.TipoVista = vistainicial Then GuardarVistaInicialGrupo
    End With
End Sub

''' <summary>Guarda la configuraci�n de la vista actual</summary>
''' <remarks>Llamada desde frmOrigen.GuardarVistaAll;  Tiempo m�ximo: 0</remarks>
''' <revision>LTG 17/02/2012</revision>

Public Sub GuardarVistaAll()
    Dim iEscVisible As Integer
    Dim iNumColsGrupo As Integer
    Dim iNumColsGR  As Integer
    Dim iNumColsGR1  As Integer
    Dim i As Integer

    'N� de columnas de los grupos de proveedores.Es necesario para obtener la posici�n de las celdas
    'por si se ha hecho scroll.Hay que hacerlo de esta forma,sino no funciona:
    With sdbgEscalado
        iNumColsGR = .Groups(0).Columns.Count + .Groups(1).Columns.Count - 1
        iNumColsGR1 = .Groups(0).Columns.Count - 1
        
        If .GrpPosition(0) > 0 Then
            i = .Groups(0).Position
            .Scroll -i, 0
            .Update
        End If
        For i = 0 To .Groups.Count - 1
            .Groups(i).Position = i
        Next i

        .AllowColumnMoving = ssRelocateWithinGroup

        'Obtiene cual es el 1� escalado que est� visible
        For iEscVisible = 2 To .Groups.Count - 1
            If .Groups(iEscVisible).Visible = True Then
                Exit For
            End If
        Next
        If iEscVisible = .Groups.Count Then Exit Sub

        VistaSeleccionadaAll.ExcluirCerradosResult = frmOrigen.chkExcluirItCerradosResul.Value
        VistaSeleccionadaAll.OcultarNoAdj = frmOrigen.chkOcultarNoAdjGr.Value
        VistaSeleccionadaAll.OcultarItCerrados = frmOrigen.chkOcultarItCerradosGr.Value
        VistaSeleccionadaAll.OcultarProvSinOfe = frmOrigen.chkOcultarProvSinOfeGr.Value

        VistaSeleccionadaAll.Grupo0Width = .Groups(0).Width  'Anchura grupo ppal
        VistaSeleccionadaAll.GruposWidth = .Groups(2).Width  'Anchura grupos escalados

        '--- GRUPO PPAL ---
        'almacena los widths de las columnas
        VistaSeleccionadaAll.DescrWidth = .Columns("DESCR").Width
        VistaSeleccionadaAll.CantidadWidth = .Columns("CANT").Width
        VistaSeleccionadaAll.PresUniWidth = .Columns("PRECAPE").Width
        VistaSeleccionadaAll.ProveedorWidth = .Columns("CODPROVEACTUAL").Width
        VistaSeleccionadaAll.ImporteWidth = .Columns("IMP").Width
        VistaSeleccionadaAll.AhorroWidth = .Columns("AHORROIMP").Width
        VistaSeleccionadaAll.AhorroPorcenWidth = .Columns("AHORROPORCEN").Width
        VistaSeleccionadaAll.SolicVinculadaWidth = .Columns("VINCULADO_ITEM").Width
        VistaSeleccionadaAll.FecIniSumWidth = .Columns("INI").Width
        VistaSeleccionadaAll.FecFinSumWidth = .Columns("FIN").Width
        VistaSeleccionadaAll.EstrMatWidth = .Columns("GMN").Width
        
        'almacena los position de las columnas
        VistaSeleccionadaAll.CantidadPos = .Columns("CANT").Position
        VistaSeleccionadaAll.PresUniPos = .Columns("PRECAPE").Position
        VistaSeleccionadaAll.ProveedorPos = .Columns("CODPROVEACTUAL").Position
        VistaSeleccionadaAll.ImportePos = .Columns("IMP").Position
        VistaSeleccionadaAll.AhorroPos = .Columns("AHORROIMP").Position
        VistaSeleccionadaAll.AhorroPorcenPos = .Columns("AHORROPORCEN").Position
        VistaSeleccionadaAll.SolicVinculadaPos = .Columns("VINCULADO_ITEM").Position
        VistaSeleccionadaAll.FecIniSumPos = .Columns("INI").Position
        VistaSeleccionadaAll.FecFinSumPos = .Columns("FIN").Position
        VistaSeleccionadaAll.EstrMatPos = .Columns("GMN").Position
        
        'almacena la visibilidad de las columnas
        VistaSeleccionadaAll.CantidadVisible = .Columns("CANT").Visible
        VistaSeleccionadaAll.PresUniVisible = .Columns("PRECAPE").Visible
        VistaSeleccionadaAll.ProveedorVisible = .Columns("CODPROVEACTUAL").Visible
        VistaSeleccionadaAll.ImporteVisible = .Columns("IMP").Visible
        VistaSeleccionadaAll.AhorroVisible = .Columns("AHORROIMP").Visible
        VistaSeleccionadaAll.AhorroPorcenVisible = .Columns("AHORROPORCEN").Visible
        VistaSeleccionadaAll.SolicVinculadaVisible = .Columns("VINCULADO_ITEM").Visible
        VistaSeleccionadaAll.FecIniSumVisible = .Columns("INI").Visible
        VistaSeleccionadaAll.FecFinSumVisible = .Columns("FIN").Visible
        VistaSeleccionadaAll.EstrMatVisible = .Columns("GMN").Visible
        
        VistaSeleccionadaAll.SolicVinculadaLevel = 0
        
        '--- GRUPO ESCALADOS ---
        If .Groups.Count > 2 Then
            iNumColsGrupo = .Groups(iEscVisible).Columns.Count
            
            'Para las columnas de los escalados se usar�: PREC_PROV para PRES y ADJ_PROV para AHORRO
            'almacena los widths de las columnas
            VistaSeleccionadaAll.PrecioEscWidth = .Columns("PRES" & iEscVisible).Width
            VistaSeleccionadaAll.AhorroEscWidth = .Columns("AHORRO" & iEscVisible).Width
            VistaSeleccionadaAll.AdjEscWidth = .Columns("ADJCANT" & iEscVisible).Width
            'almacena los position de las columnas
            VistaSeleccionadaAll.PrecioEscPos = .Columns("PRES" & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
            VistaSeleccionadaAll.AhorroEscPos = .Columns("AHORRO" & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
            VistaSeleccionadaAll.AdjEscPos = .Columns("ADJCANT" & iEscVisible).Position - iNumColsGR - (iNumColsGrupo * (iEscVisible - 2))
            'almacena la visibilidad de las columnas
            VistaSeleccionadaAll.PrecioEscVisible = .Columns("PRES" & iEscVisible).Visible
            VistaSeleccionadaAll.AhorroEscVisible = .Columns("AHORRO" & iEscVisible).Visible
            VistaSeleccionadaAll.AdjEscVisible = .Columns("ADJCANT" & iEscVisible).Visible
        End If

        'Almacena el ancho de la fila
        VistaSeleccionadaAll.AnchoFila = .RowHeight

        'Almacena el n�mero de decimales:
        VistaSeleccionadaAll.DecCant = frmOrigen.txtDecCantAll.Text
        VistaSeleccionadaAll.DecPorcen = frmOrigen.txtDecPorcenAll.Text
        VistaSeleccionadaAll.DecPrecios = frmOrigen.txtDecPrecAll.Text
        VistaSeleccionadaAll.DecResult = frmOrigen.txtDecResultAll.Text
    End With
End Sub

''' <summary>Oculta los proveedores de la vista del grupo que tienen ofertas no adjudicables</summary>
''' <param name="bOcultar">Indica si hay que ocultar o mostrar los proveedores sin ofertas</param>
''' <remarks>Llamada desde: frmOrigen.chkOcultarNoAdjGr_Click; Tiempo m�ximo: 0</remarks>
''' <revision>21/03/2012</revision>

Public Sub OcultarNoAdjGr(ByVal bOcultar As Boolean)
    Dim oProve As CProveedor
    Dim oOferta As COferta
    Dim bOcultarProve As Boolean
    Dim bVisualizar As Boolean
    
    If bOcultar Then
        For Each oProve In ProvesAsig
            bOcultarProve = False
            
            If Not GrupoSeleccionado.UltimasOfertas Is Nothing Then
                Set oOferta = GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod))
                If Not oOferta Is Nothing Then
                    If Not EstOfes.Item(oOferta.CodEst).adjudicable Then
                        bOcultarProve = True
                    End If
                End If
                Set oOferta = Nothing
            End If
            
            If bOcultarProve Then
                OcultarProveedorGr oProve.Cod, False
                
                'Deschequea los proveedores sin ofertas que estaban visibles
                frmOrigen.ChequearProve frmOrigen.sdbgOcultarProvGr, oProve.Cod, False
            End If
            
            With VistaSeleccionadaGr
                If .Vista <> TipoDeVistaDefecto.vistainicial Then
                    If bOcultarProve Then
                        .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                    End If
                End If
            End With
        Next
    Else
        For Each oProve In ProvesAsig
            'compruebo si est� chequeada la opci�n de ocultar proveedores sin ofertas.si est� chequeada y el
            'proveedor no tiene ofertas no lo hago visible:
            If Not (VistaSeleccionadaGr.OcultarProvSinOfe And GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing) Then
                bVisualizar = True
        
                If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
                    If Not GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                        Set oOferta = GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod))
                        If Not oOferta Is Nothing Then
                            If EstOfes.Item(oOferta.CodEst).adjudicable Then
                                If VistaSeleccionadaGr.ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible Then
                                    bVisualizar = False
                                End If
                            End If
                        End If
                                                    
                    End If
                End If

                If bVisualizar Then
                    OcultarProveedorGr oProve.Cod, True
                
                    'chequea los proveedores sin ofertas para hacerlos visibles
                    frmOrigen.ChequearProve frmOrigen.sdbgOcultarProvGr, oProve.Cod, True
                End If
            End If
            
            With VistaSeleccionadaGr
                If .Vista <> TipoDeVistaDefecto.vistainicial Then
                    If Not (.OcultarProvSinOfe And GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing) Then
                        If bVisualizar Then
                            .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible
                        End If
                    End If
                End If
            End With
        Next
    End If
    
    Set oProve = Nothing
End Sub

''' <summary>Hace visible o invisible una fila de proveedor en funci�n de si tiene ofertas o no</summary>
''' <param name="bOcultar">Indica si hay que ocultar o mostrar los proveedores sin ofertas</param>
''' <remarks>Llamada desde: frmOrigen.chkOcultarProvSinOfeGr_Click; Tiempo m�ximo: 0</remarks>
''' <revision>21/03/2012</revision>

Public Sub OcultarProvesSinOfeGr(ByVal bOcultar As Boolean)
    Dim bOcultarProv As Boolean
    Dim oProve As CProveedor
    Dim oEsc As CEscalado
    Dim oOferta As COferta
    Dim bVisualizar2 As Boolean
    Dim i As Integer
    
    If bOcultar Then
        For Each oProve In ProvesAsig
            bOcultarProv = ProveSinOfes(GrupoSeleccionado.UltimasOfertas, Trim(oProve.Cod), True, GrupoSeleccionado.Escalados)
            If bOcultarProv Then
                m_bHayProvesSinOfesOcultos = True
                
                OcultarProveedorGr oProve.Cod, False
                
                'Deschequea los proveedores sin ofertas que estaban visibles
                frmOrigen.ChequearProve frmOrigen.sdbgOcultarProvGr, oProve.Cod, False
            End If
            With VistaSeleccionadaGr
                If .Vista <> TipoDeVistaDefecto.vistainicial Then
                    If bOcultarProv Then
                        .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                    End If
                End If
            End With
        Next
    Else
        m_bHayProvesSinOfesOcultos = False
        
        For Each oProve In ProvesAsig
            'Comprueba que no est� seleccionada la opci�n de ocultar ofertas no adjudicables.Si est�
            'seleccionada y se trata de una oferta no adjudicable no la hago visible
            bVisualizar2 = True
            With VistaSeleccionadaGr
                If .OcultarNoAdj Then
                    If Not GrupoSeleccionado.UltimasOfertas Is Nothing Then
                        Set oOferta = GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod))
                        If Not oOferta Is Nothing Then
                            If Not EstOfes.Item(oOferta.CodEst).adjudicable Then
                                bVisualizar2 = False
                                .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                            ElseIf .Vista <> TipoDeVistaDefecto.vistainicial Then
                                If .ConfVistasGrProv.Item(oProve.Cod).Visible <> TipoProvVisible.NoVisible Then
                                    .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible
                                Else
                                    bVisualizar2 = False
                                End If
                            End If
                        Else
                            bVisualizar2 = False
                        End If
                        Set oOferta = Nothing
                    Else
                        bVisualizar2 = False
                    End If
                ElseIf .Vista <> TipoDeVistaDefecto.vistainicial Then
                    'Si esta invisible
                    If Not GrupoSeleccionado.UltimasOfertas Is Nothing Then
                        Set oOferta = GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod))
                        If Not oOferta Is Nothing Then
                            If Not GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas Is Nothing Then
                                For i = 1 To GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Count
                                    For Each oEsc In GrupoSeleccionado.Escalados
                                        If Not GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(i).Escalados.Item(CStr(oEsc.Id)) Is Nothing Then
                                            If IsNull(GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(i).Escalados.Item(CStr(oEsc.Id)).PrecioOferta) Then
                                                bVisualizar2 = False
                                                .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                                                Exit For
                                            End If
                                        End If
                                    Next
                                    
                                    If Not bVisualizar2 Then Exit For
                                Next
                                
                                'Caso en que se ha marcado como no visible y tiene ofertas
                                If .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible And bVisualizar2 Then
                                    bVisualizar2 = False
                                    .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                                End If
                                
                            Else
                                .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible
                            End If
                        Else
                            .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible
                        End If
                        Set oOferta = Nothing
                    Else
                        .ConfVistasGrProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible
                    End If
                End If
            End With
                
            If bVisualizar2 Then
                OcultarProveedorGr oProve.Cod, True
                
                'chequea los proveedores sin ofertas para hacerlos visibles
                frmOrigen.ChequearProve frmOrigen.sdbgOcultarProvGr, oProve.Cod, True
            End If
        Next
    End If
    
    Set oProve = Nothing
    Set oEsc = Nothing
    Set oOferta = Nothing
End Sub

''' <summary>Hace visible o invisible una fila de proveedor en funci�n de si tiene ofertas o no</summary>
''' <param name="bOcultar">Indica si hay que ocultar o mostrar los proveedores sin ofertas</param>
''' <remarks>Llamada desde: frmOrigen.chkOcultarProvSinOfeAll_Click</remarks>
''' <revision>21/03/2012</revision>

Public Sub OcultarProvesSinOfeAll(ByVal bOcultar As Boolean)
    Dim bOcultarProv As Boolean
    Dim oProve As CProveedor
    Dim oEsc As CEscalado
    Dim oOferta As COferta
    Dim bVisualizar2 As Boolean
    Dim i As Integer
    Dim oGrupo As CGrupo
    
    m_bHayProvesSinOfesOcultos = False
    
    If bOcultar Then
        For Each oProve In ProvesAsig
            For Each oGrupo In ProcesoSeleccionado.Grupos
                bOcultarProv = bOcultarProv Or ProveSinOfes(oGrupo.UltimasOfertas, Trim(oProve.Cod), True, oGrupo.Escalados)
                If Not bOcultarProv Then Exit For
            Next
            Set oGrupo = Nothing
            
            If bOcultarProv Then
                m_bHayProvesSinOfesOcultos = True
                
                OcultarProveedorAll oProve.Cod, False
                
                'Deschequea los proveedores sin ofertas que estaban visibles
                frmOrigen.ChequearProve frmOrigen.sdbgOcultarProvAll, oProve.Cod, False
            End If
            With VistaSeleccionadaAll
                If .Vista <> TipoDeVistaDefecto.vistainicial Then
                    If bOcultarProv Then
                        .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                    End If
                End If
            End With
        Next
    Else
        For Each oProve In ProvesAsig
            'Comprueba que no est� seleccionada la opci�n de ocultar ofertas no adjudicables.Si est�
            'seleccionada y se trata de una oferta no adjudicable no la hago visible
            bVisualizar2 = True
            With VistaSeleccionadaAll
                If .OcultarNoAdj Then
                    For Each oGrupo In ProcesoSeleccionado.Grupos
                        If Not oGrupo.UltimasOfertas Is Nothing Then
                            Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                            If Not oOferta Is Nothing Then
                                If Not EstOfes.Item(oOferta.CodEst).adjudicable Then
                                    bVisualizar2 = False
                                    .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                                ElseIf .Vista <> TipoDeVistaDefecto.vistainicial Then
                                    If .ConfVistasAllProv.Item(oProve.Cod).Visible <> TipoProvVisible.NoVisible Then
                                        .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible
                                    Else
                                        bVisualizar2 = False
                                    End If
                                End If
                            Else
                                bVisualizar2 = False
                            End If
                            Set oOferta = Nothing
                        Else
                            bVisualizar2 = False
                        End If
                        
                        If Not bVisualizar2 Then
                            m_bHayProvesSinOfesOcultos = True
                            Exit For
                        End If
                    Next
                    Set oGrupo = Nothing
                ElseIf .Vista <> TipoDeVistaDefecto.vistainicial Then
                    'Si esta invisible
                    For Each oGrupo In ProcesoSeleccionado.Grupos
                        If Not oGrupo.UltimasOfertas Is Nothing Then
                            Set oOferta = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                            If Not oOferta Is Nothing Then
                                If Not oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas Is Nothing Then
                                    For i = 1 To oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Count
                                        For Each oEsc In oGrupo.Escalados
                                            If Not oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(i).Escalados.Item(CStr(oEsc.Id)) Is Nothing Then
                                                If IsNull(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(i).Escalados.Item(CStr(oEsc.Id)).PrecioOferta) Then
                                                    bVisualizar2 = False
                                                    .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                                                    Exit For
                                                End If
                                            Else
                                                bVisualizar2 = False
                                                .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                                                Exit For
                                            End If
                                        Next
                                        
                                        If Not bVisualizar2 Then Exit For
                                    Next
                                    
                                    'Caso en que se ha marcado como no visible y tiene ofertas
                                    If .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible And bVisualizar2 Then
                                        bVisualizar2 = False
                                        .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.NoVisible
                                    End If
                                    
                                Else
                                    .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible
                                End If
                            Else
                                .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible
                            End If
                            Set oOferta = Nothing
                        Else
                            .ConfVistasAllProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible
                        End If
                        
                        If Not bVisualizar2 Then
                            m_bHayProvesSinOfesOcultos = True
                            Exit For
                        End If
                    Next
                    Set oGrupo = Nothing
                End If
            End With
                
            If bVisualizar2 Then
                OcultarProveedorAll oProve.Cod, True
                
                'chequea los proveedores sin ofertas para hacerlos visibles
                frmOrigen.ChequearProve frmOrigen.sdbgOcultarProvAll, oProve.Cod, True
            End If
        Next
    End If
    
    Set oProve = Nothing
    Set oEsc = Nothing
    Set oOferta = Nothing
End Sub

''' <summary>Hace visible o invisible una fila de proveedor</summary>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <param name="bVisible">Booleano que indica si se va a hacer visible o invisible</param>
''' <remarks>Llamada desde: frmOrigen.OcultarProveedorGr; Tiempo m�ximo: 0</remarks>
''' <revision>21/03/2012</revision>

Public Sub OcultarProveedorGr(ByVal scodProve As String, ByVal bVisible As Boolean)
    Dim vBookmark As Variant
    Dim i As Integer
    Dim iItemID As Integer
    Dim sCodigo As String
    Dim bCargar As Boolean
    Dim oProve As CProveedor
    Dim bBuscarProve As Boolean
    Dim dcProves As Dictionary
    
    LockWindowUpdate UserControl.hWnd
    
    With sdbgEscalado
        If Not bVisible Then
            'Buscar las filas correspondientes al c�digo del proveedor y eliminarlas
            For i = 0 To .Rows - 1
                vBookmark = .AddItemBookmark(i)
                If .Columns("PROV").CellValue(vBookmark) = "1" And .Columns("ID").CellValue(vBookmark) = scodProve Then
                    .RemoveItem .AddItemRowIndex(vBookmark)
                End If
            Next
        Else
            'Comprobar si hay que cargar el proveedor
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                sCodigo = scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
                If Asigs.Item(sCodigo).Grupos.Item(GrupoSeleccionado.Codigo) Is Nothing Then
                    bCargar = False
                End If
            End If
            If bCargar Then
                'Crear un diccionario de los proveedores con el orden que ocupar�an
                Set dcProves = New Dictionary
                i = 0
                For Each oProve In ProvesAsig
                    dcProves.Add oProve.Cod, i
                    i = i + 1
                Next
                
                'A�adir las filas correspondientes al proveedor
                'Si ha habido cambio de item y no se ha encontrado la fila del proveedor a�adirla
                i = 0
                bBuscarProve = False
                Do While i < .Rows
                    vBookmark = .AddItemBookmark(i)
                    If .Columns("PROV").CellValue(vBookmark) = "0" And iItemID <> .Columns("ID").CellValue(vBookmark) Then
                        If bBuscarProve Then
                            'Ha terminado las filas del item anterior sin encontrarlo
                            AddProveedor GrupoSeleccionado, GrupoSeleccionado.Items.Item(CStr(iItemID)), ProvesAsig.Item(scodProve), True, VistaSeleccionadaGr.Vista, VistaSeleccionadaGr.OcultarItCerrados, i
                            bBuscarProve = False
                        End If
                        
                        iItemID = .Columns("ID").CellValue(vBookmark)
                        bBuscarProve = True
                    Else
                        If bBuscarProve Then
                            vBookmark = .AddItemBookmark(i)
                            If .Columns("ID").CellValue(vBookmark) <> scodProve Then
                                If dcProves(scodProve) < dcProves(.Columns("ID").CellValue(vBookmark)) Then
                                    AddProveedor GrupoSeleccionado, GrupoSeleccionado.Items.Item(CStr(iItemID)), ProvesAsig.Item(scodProve), True, VistaSeleccionadaGr.Vista, VistaSeleccionadaGr.OcultarItCerrados, i
                                    bBuscarProve = False
                                End If
                            Else
                                bBuscarProve = False
                            End If
                        End If
                    End If
                    
                    i = i + 1
                Loop
                
                If bBuscarProve Then
                    'Ha terminado las filas sin encontrarlo
                    AddProveedor GrupoSeleccionado, GrupoSeleccionado.Items.Item(CStr(iItemID)), ProvesAsig.Item(scodProve), True, VistaSeleccionadaGr.Vista, VistaSeleccionadaGr.OcultarItCerrados, i
                End If
                
                Set dcProves = Nothing
            End If
        End If
    
        If Me.frmOrigen.Visible Then .SetFocus
    End With
    
    LockWindowUpdate 0&
End Sub

''' <summary>Hace visible o invisible una fila de proveedor</summary>
''' <param name="sCodProve">C�digo del proveedor</param>
''' <param name="bVisible">Booleano que indica si se va a hacer visible o invisible</param>
''' <remarks>Llamada desde: frmOrigen.OcultarProveedorAll; Tiempo m�ximo: 0</remarks>
''' <revision>21/03/2012</revision>

Public Sub OcultarProveedorAll(ByVal scodProve As String, ByVal bVisible As Boolean)
    Dim vBookmark As Variant
    Dim i As Integer
    Dim j As Integer
    Dim iItemID As Integer
    Dim sCodigo As String
    Dim bCargar As Boolean
    Dim oProve As CProveedor
    Dim bBuscarProve As Boolean
    Dim dcProves As Dictionary
    Dim sGrupoCod As String
    Dim oGrupo As CGrupo
    
    LockWindowUpdate UserControl.hWnd
    
    With sdbgEscalado
        If Not bVisible Then
            'Buscar las filas correspondientes al c�digo del proveedor y eliminarlas
            For i = 0 To .Rows - 1
                vBookmark = .AddItemBookmark(i)
                If .Columns("PROV").CellValue(vBookmark) = "1" And .Columns("ID").CellValue(vBookmark) = scodProve Then
                    .RemoveItem .AddItemRowIndex(vBookmark)
                End If
            Next
        Else
            i = 0
            bBuscarProve = False
            sGrupoCod = vbNullString
            Do While i < .Rows
                vBookmark = .AddItemBookmark(i)
                
                If .Columns("GRUPOCOD").CellValue(vBookmark) <> sGrupoCod Then
                    'Si ha habido cambio de grupo sin encontrar el proveedor
                    If bBuscarProve Then
                        AddProveedor oGrupo, oGrupo.Items.Item(CStr(iItemID)), ProvesAsig.Item(scodProve), True, VistaSeleccionadaAll.Vista, VistaSeleccionadaAll.OcultarItCerrados, i
                        bBuscarProve = False
                    End If
                    
                    sGrupoCod = .Columns("GRUPOCOD").CellValue(vBookmark)
                    Set oGrupo = ProcesoSeleccionado.Grupos.Item(.Columns("GRUPOCOD").CellValue(vBookmark))
                    
                    'Comprobar si hay que cargar el proveedor
                    bCargar = True
                    If gParametrosGenerales.gbProveGrupos Then
                        sCodigo = scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
                        If Asigs.Item(sCodigo).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                            bCargar = False
                        End If
                    End If
                    
                    If bCargar Then
                        'Crear un diccionario de los proveedores con el orden que ocupar�an
                        Set dcProves = New Dictionary
                        j = 0
                        For Each oProve In ProvesAsig
                            dcProves.Add oProve.Cod, j
                            j = j + 1
                        Next
                    End If
                End If
            
                If bCargar Then
                    'A�adir las filas correspondientes al proveedor
                    'Si ha habido cambio de item y no se ha encontrado la fila del proveedor a�adirla
                                        
                    If .Columns("PROV").CellValue(vBookmark) = "0" And iItemID <> .Columns("ID").CellValue(vBookmark) Then
                        If bBuscarProve Then
                            'Ha terminado las filas del item anterior sin encontrarlo
                            AddProveedor oGrupo, oGrupo.Items.Item(CStr(iItemID)), ProvesAsig.Item(scodProve), True, VistaSeleccionadaAll.Vista, VistaSeleccionadaAll.OcultarItCerrados, i
                            bBuscarProve = False
                        End If
                        
                        iItemID = .Columns("ID").CellValue(vBookmark)
                        bBuscarProve = True
                    Else
                        If bBuscarProve Then
                            vBookmark = .AddItemBookmark(i)
                            If .Columns("ID").CellValue(vBookmark) <> scodProve Then
                                If dcProves(scodProve) < dcProves(.Columns("ID").CellValue(vBookmark)) Then
                                    AddProveedor oGrupo, oGrupo.Items.Item(CStr(iItemID)), ProvesAsig.Item(scodProve), True, VistaSeleccionadaAll.Vista, VistaSeleccionadaAll.OcultarItCerrados, i
                                    bBuscarProve = False
                                End If
                            Else
                                bBuscarProve = False
                            End If
                        End If
                    End If
                End If
                
                i = i + 1
            Loop
            
            If bBuscarProve Then
                'Ha terminado las filas sin encontrarlo
                AddProveedor oGrupo, oGrupo.Items.Item(CStr(iItemID)), ProvesAsig.Item(scodProve), True, VistaSeleccionadaAll.Vista, VistaSeleccionadaAll.OcultarItCerrados, i
            End If
            
            Set dcProves = Nothing
        End If
    
        If Me.frmOrigen.Visible Then .SetFocus
    End With
    
    LockWindowUpdate 0&
End Sub

''' <summary>Muestra/Oculta y redimensiona las columnas y grupos del grid, columnas fijas y las de los escalados</summary>
''' <remarks>Llamada desde: frmOrigen.RedimensionarGridGrupo; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Sub RedimensionarGridGrupo()
    Dim i As Integer
    Dim sGrupo As String
    Dim oatrib As CConfVistaGrupoAtrib
    Dim vbm As Variant
    Dim iOrden As Integer
    Dim sColumna As String
    Dim j As Integer
    Dim oAtributo As CAtributo
    Dim bVisible As Boolean
    Dim bEncontrado As Boolean
    Dim iIndex As Integer
    Dim inumColumns As Integer
    Dim h As Integer
    Dim dblAnchoFila As Double
    Dim oProve As CProveedor
    Dim iNumColsGR As Integer
    Dim iNumColsGR1 As Integer
    Dim dblGrupoCarWidth As Double
    Dim bGrupoAtribVisible As Boolean
    Dim oColumn As SSDataWidgets_B.Column
    
    With sdbgEscalado
        iNumColsGR1 = .Groups(0).Columns.Count - 1
        iNumColsGR = .Groups(0).Columns.Count + .Groups(1).Columns.Count - 1
        
        'Si se ha hecho un scroll se vuelve a hacerlo para dejar el grid en la posici�n
        'original porque sino no funciona bien
        If .GrpPosition(0) > 0 Then
            DoEvents
            i = .Groups(0).Position
            .Scroll -i, 0
            .Update
        End If
        For j = 0 To .Groups.Count - 1
            .Groups(j).Position = j
        Next j
        
        .AllowColumnMoving = ssRelocateWithinGroup
    
        'Redimensiona el grupo 0:
        .Groups(0).Width = VistaSeleccionadaGr.Grupo0Width
        
        'Posici�n de los campos del grupo 0 (les pone las posiciones en orden,porque sino a veces no funciona bien)
        OrdenarColsGrupo0
        .Groups(0).Columns("DESCR").Position = 0
        For i = 0 To 9
            Select Case m_arrOrden(i)
                Case "CANT"
                    .Columns("CANT").Position = VistaSeleccionadaGr.CantidadPos
                Case "PRECAPE"
                    .Columns("PRECAPE").Position = VistaSeleccionadaGr.PresUniPos
                Case "CODPROVEACTUAL"
                    .Columns("CODPROVEACTUAL").Position = VistaSeleccionadaGr.ProveedorPos
                Case "IMP"
                    .Columns("IMP").Position = VistaSeleccionadaGr.ImportePos
                Case "AHORROIMP"
                    .Columns("AHORROIMP").Position = VistaSeleccionadaGr.AhorroPos
                Case "AHORROPORCEN"
                    .Columns("AHORROPORCEN").Position = VistaSeleccionadaGr.AhorroPorcenPos
                Case "VINCULADO_ITEM"
                    .Columns("VINCULADO_ITEM").Position = VistaSeleccionadaGr.SolicVinculadaPos
                Case "INI"
                    .Columns("INI").Position = VistaSeleccionadaGr.FecIniSumPos
                Case "FIN"
                    .Columns("FIN").Position = VistaSeleccionadaGr.FecFinSumPos
                Case "GMN"
                    .Columns("GMN").Position = VistaSeleccionadaGr.EstrMatPos
            End Select
        Next i
        
        'Visibilidad de los campos del grupo 0
        .Columns("DESCR").Visible = True
        .Columns("CANT").Visible = VistaSeleccionadaGr.CantidadVisible
        .Columns("PRECAPE").Visible = VistaSeleccionadaGr.PresUniVisible
        .Columns("CODPROVEACTUAL").Visible = VistaSeleccionadaGr.ProveedorVisible
        .Columns("IMP").Visible = VistaSeleccionadaGr.ImporteVisible
        .Columns("AHORROIMP").Visible = VistaSeleccionadaGr.AhorroVisible
        .Columns("AHORROPORCEN").Visible = VistaSeleccionadaGr.AhorroPorcenVisible
        If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
            .Columns("VINCULADO_ITEM").Visible = VistaSeleccionadaGr.SolicVinculadaVisible
        Else
            .Columns("VINCULADO_ITEM").Visible = False
        End If
        .Columns("INI").Visible = VistaSeleccionadaGr.FecIniSumVisible
        .Columns("FIN").Visible = VistaSeleccionadaGr.FecFinSumVisible
        .Columns("GMN").Visible = VistaSeleccionadaGr.EstrMatVisible
        
        'Tama�o de los campos del grupo 0
        'Se pone el width en el mismo orden en el que est�n los positions,porque sino algunas veces no funciona
        For j = 0 To .Groups(0).Columns.Count - 1
            iIndex = .Groups(0).ColPosition(j)
            Select Case .Groups(0).Columns(iIndex).Name
                Case "DESCR"
                    .Columns("DESCR").Width = VistaSeleccionadaGr.DescrWidth
                Case "CANT"
                    .Columns("CANT").Width = VistaSeleccionadaGr.CantidadWidth
                Case "PRECAPE"
                    .Columns("PRECAPE").Width = VistaSeleccionadaGr.PresUniWidth
                Case "CODPROVEACTUAL"
                    .Columns("CODPROVEACTUAL").Width = VistaSeleccionadaGr.ProveedorWidth
                Case "IMP"
                    .Columns("IMP").Width = VistaSeleccionadaGr.ImporteWidth
                Case "AHORROIMP"
                    .Columns("AHORROIMP").Width = VistaSeleccionadaGr.AhorroWidth
                Case "AHORROPORCEN"
                    .Columns("AHORROPORCEN").Width = VistaSeleccionadaGr.AhorroPorcenWidth
                Case "VINCULADO_ITEM"
                    .Columns("VINCULADO_ITEM").Width = VistaSeleccionadaGr.SolicVinculadaWidth
                Case "INI"
                    .Columns("INI").Width = VistaSeleccionadaGr.FecIniSumWidth
                Case "FIN"
                    .Columns("FIN").Width = VistaSeleccionadaGr.FecFinSumWidth
                Case "GMN"
                    .Columns("GMN").Width = VistaSeleccionadaGr.EstrMatWidth
            End Select
        Next j
                
        'Ordena las posiciones de los grupos de escalados:
        OrdenarColsGrupoEsc
        
        'Formateo num�rico de las columnas del grupo 0 de la grid
        .Columns("AHORROIMP").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecResult)
        .Columns("AHORROPORCEN").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPorcen, True)
        .Columns("IMP").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecResult)
        .Columns("CANT").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecCant)
        .Columns("PRECAPE").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
        'No se le indica el formato aqu� porque si notambi�n intentar�a aplicar el formato num�rico al c�digo
        'del proveedor actual (a nivel de item)
        .Columns("CODPROVEACTUAL").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                        
        'Redimensionar el grupo de atributos de caracter�stica
        If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
            If Not GrupoSeleccionado.AtributosItem Is Nothing Then
                bGrupoAtribVisible = False
                
                'Visibilidad
                For Each oatrib In VistaSeleccionadaGr.ConfVistasGrAtrib
                    If IsNull(GrupoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Or IsEmpty(GrupoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Then
                        .Columns(CStr(oatrib.Atributo)).Visible = oatrib.Visible
                        If .Columns(CStr(oatrib.Atributo)).Visible Then
                            bGrupoAtribVisible = True
                            dblGrupoCarWidth = dblGrupoCarWidth + oatrib.Width
                        End If
                    End If
                Next
                .Groups(1).Width = dblGrupoCarWidth
                .Groups(1).Visible = bGrupoAtribVisible
                
                'Tama�o y posici�n
                For Each oatrib In VistaSeleccionadaGr.ConfVistasGrAtrib 'GrupoSeleccionado.AtributosItem
                    If IsNull(GrupoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Or IsEmpty(GrupoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Then
                        If oatrib.Posicion = 0 Then oatrib.Posicion = 1
                        .Columns(CStr(oatrib.Atributo)).Position = iNumColsGR1 + oatrib.Posicion
                        If oatrib.Width = 0 Then
                            .Columns(CStr(oatrib.Atributo)).Width = 500
                        Else
                            .Columns(CStr(oatrib.Atributo)).Width = oatrib.Width
                        End If
                    End If
                Next
            End If
        Else
            'Visibilidad
            For Each oColumn In .Groups(1).Columns
                oColumn.Visible = False
            Next
            .Groups(1).Visible = False
        End If
            
        'Redimensiona el resto de grupos (para cada escalado)
        For i = 2 To .Groups.Count - 1
            inumColumns = 3     'N� de columnas en cada grupo (3 + n� de atributos aplicables a precio)
            sGrupo = .Groups(i).TagVariant
            
            If Not .Groups(i).Visible Then
                .Groups(i).Visible = True
            End If
            
            .Groups(i).Width = VistaSeleccionadaGr.GruposWidth   'tama�o del grupo
                                    
            'Posici�n de los campos:
            iOrden = iNumColsGR + (.Groups(i).Columns.Count * (i - 2))
            
            'Formateo num�rico de las columnas:
            .Columns("PRES" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
            .Columns("AHORRO" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
            .Columns("ADJCANT" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
            
            'Atributos
            If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
                'Formateo num�rico de los atributos aplicables al precio
                If Not GrupoSeleccionado.AtributosItem Is Nothing Then
                    For Each oAtributo In GrupoSeleccionado.AtributosItem
                        If Not IsNull(oAtributo.PrecioFormula) And Not IsEmpty(oAtributo.PrecioFormula) Then
                            inumColumns = inumColumns + 1
                            
                            If Not IsNull(oAtributo.PrecioAplicarA) Then
                                .Columns(oAtributo.idAtribProce & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                            ElseIf oAtributo.Tipo = TipoNumerico Then
                                If oAtributo.NumDecAtrib > VistaSeleccionadaGr.DecPrecios Then
                                    .Columns(oAtributo.idAtribProce & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                                Else
                                    .Columns(oAtributo.idAtribProce & i).NumberFormat = FormateoNumericoComp(oAtributo.NumDecAtrib)
                                End If
                            End If
                        End If
                    Next
                End If
            End If
            
            '-------- Posici�n de las columnas -------------
            For j = 0 To inumColumns - 1
                Select Case m_arrOrdenColsGr(j)
                    Case "ESC", "INI", "FIN", "CANTMAX", "ADJ"
                        'columnas ocultas
                    Case "ADJCANT"
                        .Columns("ADJCANT" & i).Position = VistaSeleccionadaGr.AdjEscPos + iOrden
                    Case "PRES"
                        .Columns("PRES" & i).Position = VistaSeleccionadaGr.PrecioEscPos + iOrden
                    Case "AHORRO"
                        .Columns("AHORRO" & i).Position = VistaSeleccionadaGr.AhorroEscPos + iOrden
                    Case Else
                        'Es un atributo
                        .Columns(m_arrOrdenColsGr(j) & i).Position = VistaSeleccionadaGr.ConfVistasGrAtrib.Item(CStr(m_arrOrdenColsGr(j))).Posicion + iOrden
                End Select
            Next j
            
            '-------- Visibilidad de los campos -----------
            .Columns("PRES" & i).Visible = VistaSeleccionadaGr.PrecioEscVisible
            .Columns("AHORRO" & i).Visible = VistaSeleccionadaGr.AhorroEscVisible
            .Columns("ADJCANT" & i).Visible = VistaSeleccionadaGr.AdjEscVisible
            'Atributos
            If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
                For Each oatrib In VistaSeleccionadaGr.ConfVistasGrAtrib
                    If Not IsNull(GrupoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) And Not IsEmpty(GrupoSeleccionado.AtributosItem.Item(CStr(oatrib.Atributo)).PrecioFormula) Then
                        .Columns(oatrib.Atributo & i).Visible = oatrib.Visible
                    End If
                Next
            Else
                'Es la vista inicial
                For j = 0 To .Groups(i).Columns.Count - 1
                    sColumna = Left(.Groups(i).Columns(j).Name, Len(.Groups(i).Columns(j).Name) - Len(CStr(i)))
                    If sColumna <> "PRES" And sColumna <> "AHORRO" And sColumna <> "ADJCANT" Then
                        .Groups(i).Columns(j).Visible = False
                    End If
                Next j
            End If
            
            '-------- Tama�o de los campos -----------
            For j = 0 To .Groups(i).Columns.Count - 1
                sColumna = ""
                iIndex = .Groups(i).ColPosition(j)
                sColumna = Left(.Groups(i).Columns(iIndex).Name, Len(.Groups(i).Columns(iIndex).Name) - Len(CStr(i)))
                Select Case sColumna
                    Case "ESC", "INI", "FIN", "CANTMAX", "ADJIMP", "ADJ"
                        'campos ocultos
                    Case "ADJCANT"
                        If VistaSeleccionadaGr.AdjEscWidth = 0 Then
                            .Columns("ADJCANT" & i).Width = 1200
                        Else
                            .Columns("ADJCANT" & i).Width = VistaSeleccionadaGr.AdjEscWidth
                        End If
                    Case "PRES"
                        If VistaSeleccionadaGr.PrecioEscWidth = 0 Then
                            .Columns("PRES" & i).Width = 1200
                        Else
                            .Columns("PRES" & i).Width = VistaSeleccionadaGr.PrecioEscWidth
                        End If
                    Case "AHORRO"
                        If VistaSeleccionadaGr.AhorroEscWidth = 0 Then
                            .Columns("AHORRO" & i).Width = 1200
                        Else
                            .Columns("AHORRO" & i).Width = VistaSeleccionadaGr.AhorroEscWidth
                        End If
                    Case Else
                        'Comprueba si la columna es un atributo
                        If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
                            If Not ProcesoSeleccionado.AtributosItem Is Nothing Then
                                If Not IsNull(GrupoSeleccionado.AtributosItem.Item(sColumna).PrecioFormula) And Not IsEmpty(GrupoSeleccionado.AtributosItem.Item(sColumna).PrecioFormula) Then
                                    If Not VistaSeleccionadaGr.ConfVistasGrAtrib.Item(CStr(sColumna)) Is Nothing Then
                                        If VistaSeleccionadaGr.ConfVistasGrAtrib.Item(CStr(sColumna)).Width = 0 Then
                                            .Columns(sColumna & i).Width = 500
                                        Else
                                            .Columns(sColumna & i).Width = VistaSeleccionadaGr.ConfVistasGrAtrib.Item(CStr(sColumna)).Width
                                        End If
                                    End If
                                End If
                            End If
                        End If
                End Select
            Next j
        Next i
        
        'Ocultar proveedores no seleccionados
        For Each oProve In ProvesAsig
            If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
                'Vistas 1,2,3 o 4
                Select Case VistaSeleccionadaGr.ConfVistasGrProv.Item(oProve.Cod).Visible
                    Case TipoProvVisible.Visible
                        OcultarProveedorGr oProve.Cod, True
                        
                    Case TipoProvVisible.NoVisible
                        OcultarProveedorGr oProve.Cod, False
                        
                    Case TipoProvVisible.DepenVista
                        bVisible = True
                        
                        'Si no tiene ofertas
                        If VistaSeleccionadaGr.OcultarProvSinOfe Then
                            If Not GrupoSeleccionado.UltimasOfertas Is Nothing Then
                                If GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                                    bVisible = False
                                Else
                                    bVisible = False
                                    If Not GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas Is Nothing Then
                                        For h = 1 To GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Count
                                            If (Not IsNull(GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(h).Precio) Or Not IsNull(GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(h).Precio2) Or Not IsNull(GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(h).Precio3)) Then
                                                bVisible = True
                                                Exit For
                                            End If
                                        Next
                                    End If
                                    'Puede que esten todas a 0
                                
                                End If
                                
                            Else
                                bVisible = False
                            End If
                        End If
                        
                        'Si no es una oferta adjudicable
                        If VistaSeleccionadaGr.OcultarNoAdj Then
                            If Not OfertaAdjudicable(oProve.Cod) Then
                                bVisible = False
                            End If
                        End If
                        
                        OcultarProveedorGr oProve.Cod, bVisible
                End Select
                
            Else   'Es la vista inicial
                'Oculta ofertas no adjudicables
                If Not OfertaAdjudicable(oProve.Cod) Then
                    OcultarProveedorGr oProve.Cod, Not VistaSeleccionadaGr.OcultarNoAdj
                End If
        
                'Oculta proveedores sin ofertas
                If Not ProveSinOfes(GrupoSeleccionado.UltimasOfertas, Trim(oProve.Cod), True, GrupoSeleccionado.Escalados) Then
                    OcultarProveedorGr oProve.Cod, True
                Else
                    OcultarProveedorGr oProve.Cod, Not VistaSeleccionadaGr.OcultarProvSinOfe
                End If
            End If
        Next
        
        'Oculta los items cerrados
        If VistaSeleccionadaGr.OcultarItCerrados Then OcultarItemsCerrados
    
        'Pone el ancho de las filas
        If VistaSeleccionadaGr.AnchoFila = 0 Or IsNull(VistaSeleccionadaGr.AnchoFila) Then
            dblAnchoFila = ANCHO_FILA_GR
        Else
             dblAnchoFila = VistaSeleccionadaGr.AnchoFila
        End If
        If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
            .RowHeight = dblAnchoFila
        Else
            .RowHeight = dblAnchoFila / 3
        End If
        
        'Si anteriormente hab�amos cargado el grupo y nos hab�amos posicionado en alg�n art�culo
        'nos volvemos a posicionar en el.
        If VistaSeleccionadaGr.itemSeleccionado = 0 Then
            .MoveFirst
        Else
            bEncontrado = False
            For i = 0 To .Rows - 1
                vbm = .AddItemBookmark(i)
                If .Columns("ID").CellValue(vbm) = VistaSeleccionadaGr.itemSeleccionado Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
            If bEncontrado = True Then
                .MoveRecords i
            Else
                .MoveFirst
            End If
        End If
    End With
End Sub

''' <summary>Muestra/Oculta y redimensiona las columnas y grupos del grid, columnas fijas y las de los escalados</summary>
''' <remarks>Llamada desde: frmOrigen.RedimensionarGridGrupo; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Sub RedimensionarGridAll()
    Dim i As Integer
    Dim sGrupo As String
    Dim vbm As Variant
    Dim iOrden As Integer
    Dim sColumna As String
    Dim j As Integer
    Dim bVisible As Boolean
    Dim bEncontrado As Boolean
    Dim iIndex As Integer
    Dim inumColumns As Integer
    Dim oProve As CProveedor
    Dim iNumColsGR As Integer
    Dim iNumColsGR1 As Integer

    With sdbgEscalado
        iNumColsGR1 = .Groups(0).Columns.Count - 1
        iNumColsGR = .Groups(0).Columns.Count + .Groups(1).Columns.Count - 1
        
        'Si se ha hecho un scroll se vuelve a hacerlo para dejar el grid en la posici�n
        'original porque sino no funciona bien
        If .GrpPosition(0) > 0 Then
            DoEvents
            i = .Groups(0).Position
            .Scroll -i, 0
            .Update
        End If
        For j = 0 To .Groups.Count - 1
            .Groups(j).Position = j
        Next j
        
        .AllowColumnMoving = ssRelocateWithinGroup
    
        'Redimensiona el grupo 0:
        .Groups(0).Width = VistaSeleccionadaAll.Grupo0Width
        
        'Posici�n de los campos del grupo 0 (les pone las posiciones en orden,porque sino a veces no funciona bien)
        OrdenarColsGrupo0All
        .Groups(0).Columns("DESCR").Position = 0
        For i = 0 To 9
            Select Case m_arrOrden(i)
                Case "CANT"
                    .Columns("CANT").Position = VistaSeleccionadaAll.CantidadPos
                Case "PRECAPE"
                    .Columns("PRECAPE").Position = VistaSeleccionadaAll.PresUniPos
                Case "CODPROVEACTUAL"
                    .Columns("CODPROVEACTUAL").Position = VistaSeleccionadaAll.ProveedorPos
                Case "IMP"
                    .Columns("IMP").Position = VistaSeleccionadaAll.ImportePos
                Case "AHORROIMP"
                    .Columns("AHORROIMP").Position = VistaSeleccionadaAll.AhorroPos
                Case "AHORROPORCEN"
                    .Columns("AHORROPORCEN").Position = VistaSeleccionadaAll.AhorroPorcenPos
                Case "VINCULADO_ITEM"
                    .Columns("VINCULADO_ITEM").Position = VistaSeleccionadaAll.SolicVinculadaPos
                Case "INI"
                    .Columns("INI").Position = VistaSeleccionadaAll.FecIniSumPos
                Case "FIN"
                    .Columns("FIN").Position = VistaSeleccionadaAll.FecFinSumPos
                Case "GMN"
                    .Columns("GMN").Position = VistaSeleccionadaAll.EstrMatPos
            End Select
        Next i
        
        'Visibilidad de los campos del grupo 0
        .Columns("DESCR").Visible = True
        .Columns("CANT").Visible = VistaSeleccionadaAll.CantidadVisible
        .Columns("PRECAPE").Visible = VistaSeleccionadaAll.PresUniVisible
        .Columns("CODPROVEACTUAL").Visible = VistaSeleccionadaAll.ProveedorVisible
        .Columns("IMP").Visible = VistaSeleccionadaAll.ImporteVisible
        .Columns("AHORROIMP").Visible = VistaSeleccionadaAll.AhorroVisible
        .Columns("AHORROPORCEN").Visible = VistaSeleccionadaAll.AhorroPorcenVisible
        If VistaSeleccionadaAll.Vista <> TipoDeVistaDefecto.vistainicial Then
            .Columns("VINCULADO_ITEM").Visible = VistaSeleccionadaAll.SolicVinculadaVisible
        Else
            .Columns("VINCULADO_ITEM").Visible = False
        End If
        .Columns("INI").Visible = VistaSeleccionadaAll.FecIniSumVisible
        .Columns("FIN").Visible = VistaSeleccionadaAll.FecFinSumVisible
        .Columns("GMN").Visible = VistaSeleccionadaAll.EstrMatVisible
        
        'Tama�o de los campos del grupo 0
        'Se pone el width en el mismo orden en el que est�n los positions,porque sino algunas veces no funciona
        For j = 0 To .Groups(0).Columns.Count - 1
            iIndex = .Groups(0).ColPosition(j)
            Select Case .Groups(0).Columns(iIndex).Name
                Case "DESCR"
                    .Columns("DESCR").Width = VistaSeleccionadaAll.DescrWidth
                Case "CANT"
                    .Columns("CANT").Width = VistaSeleccionadaAll.CantidadWidth
                Case "PRECAPE"
                    .Columns("PRECAPE").Width = VistaSeleccionadaAll.PresUniWidth
                Case "CODPROVEACTUAL"
                    .Columns("CODPROVEACTUAL").Width = VistaSeleccionadaAll.ProveedorWidth
                Case "IMP"
                    .Columns("IMP").Width = VistaSeleccionadaAll.ImporteWidth
                Case "AHORROIMP"
                    .Columns("AHORROIMP").Width = VistaSeleccionadaAll.AhorroWidth
                Case "AHORROPORCEN"
                    .Columns("AHORROPORCEN").Width = VistaSeleccionadaAll.AhorroPorcenWidth
                Case "VINCULADO_ITEM"
                    .Columns("VINCULADO_ITEM").Width = VistaSeleccionadaAll.SolicVinculadaWidth
                Case "INI"
                    .Columns("INI").Width = VistaSeleccionadaAll.FecIniSumWidth
                Case "FIN"
                    .Columns("FIN").Width = VistaSeleccionadaAll.FecFinSumWidth
                Case "GMN"
                    .Columns("GMN").Width = VistaSeleccionadaAll.EstrMatWidth
            End Select
        Next j
                
        'Ordena las posiciones de los grupos de escalados:
        OrdenarColsAllEsc
        
        'Formateo num�rico de las columnas del grupo 0 de la grid
        .Columns("AHORROIMP").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecResult)
        .Columns("AHORROPORCEN").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPorcen, True)
        .Columns("IMP").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecResult)
        .Columns("CANT").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecCant)
        .Columns("PRECAPE").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
        'No se le indica el formato aqu� porque si notambi�n intentar�a aplicar el formato num�rico al c�digo
        'del proveedor actual (a nivel de item)
        .Columns("CODPROVEACTUAL").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
            
        'Redimensiona el resto de grupos (para cada escalado)
        For i = 2 To .Groups.Count - 1
            inumColumns = 3     'N� de columnas en cada grupo (3 + n� de atributos aplicables a precio)
            sGrupo = .Groups(i).TagVariant
            
            If Not .Groups(i).Visible Then
                .Groups(i).Visible = True
            End If
            
            .Groups(i).Width = VistaSeleccionadaAll.GruposWidth   'tama�o del grupo
                                    
            'Posici�n de los campos:
            iOrden = iNumColsGR + (.Groups(i).Columns.Count * (i - 2))
            
            'Formateo num�rico de las columnas:
            .Columns("PRES" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
            .Columns("AHORRO" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
            .Columns("ADJCANT" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
            
            '-------- Posici�n de las columnas -------------
            For j = 0 To inumColumns - 1
                Select Case m_arrOrdenColsGr(j)
                    Case "ESC", "INI", "FIN", "CANTMAX", "ADJ"
                        'columnas ocultas
                    Case "ADJCANT"
                        .Columns("ADJCANT" & i).Position = VistaSeleccionadaAll.AdjEscPos + iOrden
                    Case "PRES"
                        .Columns("PRES" & i).Position = VistaSeleccionadaAll.PrecioEscPos + iOrden
                    Case "AHORRO"
                        .Columns("AHORRO" & i).Position = VistaSeleccionadaAll.AhorroEscPos + iOrden
                End Select
            Next j
            
            '-------- Visibilidad de los campos -----------
            .Columns("PRES" & i).Visible = VistaSeleccionadaAll.PrecioEscVisible
            .Columns("AHORRO" & i).Visible = VistaSeleccionadaAll.AhorroEscVisible
            .Columns("ADJCANT" & i).Visible = VistaSeleccionadaAll.AdjEscVisible
            
            '-------- Tama�o de los campos -----------
            For j = 0 To .Groups(i).Columns.Count - 1
                sColumna = ""
                iIndex = .Groups(i).ColPosition(j)
                sColumna = Left(.Groups(i).Columns(iIndex).Name, Len(.Groups(i).Columns(iIndex).Name) - Len(CStr(i)))
                Select Case sColumna
                    Case "ESC", "INI", "FIN", "CANTMAX", "ADJIMP", "ADJ"
                        'campos ocultos
                    Case "ADJCANT"
                        If VistaSeleccionadaAll.AdjEscWidth = 0 Then
                            .Columns("ADJCANT" & i).Width = 1200
                        Else
                            .Columns("ADJCANT" & i).Width = VistaSeleccionadaAll.AdjEscWidth
                        End If
                    Case "PRES"
                        If VistaSeleccionadaAll.PrecioEscWidth = 0 Then
                            .Columns("PRES" & i).Width = 1200
                        Else
                            .Columns("PRES" & i).Width = VistaSeleccionadaAll.PrecioEscWidth
                        End If
                    Case "AHORRO"
                        If VistaSeleccionadaAll.AhorroEscWidth = 0 Then
                            .Columns("AHORRO" & i).Width = 1200
                        Else
                            .Columns("AHORRO" & i).Width = VistaSeleccionadaAll.AhorroEscWidth
                        End If
                End Select
            Next j
        Next i
        
        'Ocultar proveedores no seleccionados
        For Each oProve In ProvesAsig
            If VistaSeleccionadaAll.Vista <> TipoDeVistaDefecto.vistainicial Then
                'Vistas 1,2,3 o 4
                Select Case VistaSeleccionadaAll.ConfVistasAllProv.Item(oProve.Cod).Visible
                    Case TipoProvVisible.Visible
                        OcultarProveedorAll oProve.Cod, True
                        
                    Case TipoProvVisible.NoVisible
                        OcultarProveedorAll oProve.Cod, False
                        
                    Case TipoProvVisible.DepenVista
                        bVisible = True
                        
                        'Si no tiene ofertas
                        If VistaSeleccionadaAll.OcultarProvSinOfe Then
                            If Not ProcesoSeleccionado.Ofertas Is Nothing Then
                                If ProcesoSeleccionado.Ofertas.Item(oProve.Cod) Is Nothing Then
                                    bVisible = False
                                Else
                                    bVisible = False
                                End If
                                
                            Else
                                bVisible = False
                            End If
                        End If
                        
                        'Si no es una oferta adjudicable
                        If VistaSeleccionadaAll.OcultarNoAdj Then
                            If Not OfertaAdjudicable(oProve.Cod) Then
                                bVisible = False
                            End If
                        End If
                        
                        OcultarProveedorAll oProve.Cod, bVisible
                End Select
                
            Else   'Es la vista inicial
                'Oculta ofertas no adjudicables
                If Not OfertaAdjudicable(oProve.Cod) Then
                    OcultarProveedorAll oProve.Cod, Not VistaSeleccionadaAll.OcultarNoAdj
                End If
        
                'Oculta proveedores sin ofertas
                If ProcesoSeleccionado.Ofertas.Item(oProve.Cod) Is Nothing Then
                    OcultarProveedorAll oProve.Cod, True
                Else
                    OcultarProveedorAll oProve.Cod, Not VistaSeleccionadaAll.OcultarProvSinOfe
                End If
            End If
        Next
        
        'Oculta los items cerrados
        If VistaSeleccionadaAll.OcultarItCerrados Then OcultarItemsCerrados
    
        'Pone el ancho de las filas
        If VistaSeleccionadaAll.AnchoFila = 0 Or IsNull(VistaSeleccionadaAll.AnchoFila) Then
            .RowHeight = ANCHO_FILA_GR
        Else
             .RowHeight = VistaSeleccionadaAll.AnchoFila
        End If
        
        'Si anteriormente hab�amos cargado el grupo y nos hab�amos posicionado en alg�n art�culo
        'nos volvemos a posicionar en el.
        If VistaSeleccionadaAll.itemSeleccionado = 0 Then
            .MoveFirst
        Else
            bEncontrado = False
            For i = 0 To .Rows - 1
                vbm = .AddItemBookmark(i)
                If .Columns("ID").CellValue(vbm) = VistaSeleccionadaAll.itemSeleccionado Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
            If bEncontrado = True Then
                .MoveRecords i
            Else
                .MoveFirst
            End If
        End If
    End With
End Sub

''' <summary>Oculta los items cerrados en el grid</summary>
''' <remarks>Llamada desde: RedimensionarGridGrupo; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Sub OcultarItemsCerrados()
    Dim i As Integer
    Dim vbm As Variant
    
    With sdbgEscalado
        i = 0
        While i <= .Rows - 1
            vbm = .AddItemBookmark(i)
            If .Columns("PROV").CellValue(vbm) = "0" Then
                If .Columns("CERRADO").CellValue(vbm) Then
                    .RemoveItem (.AddItemRowIndex(vbm))
                    
                    While .Columns("PROV").CellValue(vbm) = "1"
                        'Borrar las filas de proveedores para ese item
                        .RemoveItem (.AddItemRowIndex(vbm))
                    Wend
                End If
            End If
            
            i = i + 1
        Wend
    End With
End Sub

''' <summary>Genera un array con las columnas de los grupos de escalados ordenadas</summary>
''' <remarks>Llamada desde: RedimensionarGridGrupo; Tiempo m�ximo:0</remarks>
''' <revision>LTG 27/02/2012</revision>

Private Sub OrdenarColsGrupoEsc()
    Dim iOrden As Integer
    Dim iOrdenados As Integer
    Dim iOrdenAnt As Integer
    Dim sCol As String
    Dim bEncontrado As Boolean
    Dim i As Integer
    Dim oAtribGr As CConfVistaGrupoAtrib
    Dim iNumAtrib As Integer
    
    'Posici�n de los campos del grupo 0 (les pone las posiciones en orden,porque sino a veces no funciona bien)
    iOrdenAnt = 0
    iOrdenados = 0
        
    'Pesta�a de grupo
    If IsNull(VistaSeleccionadaGr.PrecioEscPos) Or VistaSeleccionadaGr.PrecioEscPos = 0 Then VistaSeleccionadaGr.PrecioEscPos = 1
    If IsNull(VistaSeleccionadaGr.AhorroEscPos) Or VistaSeleccionadaGr.AhorroEscPos = 0 Then VistaSeleccionadaGr.AhorroEscPos = 2
    If IsNull(VistaSeleccionadaGr.AdjEscPos) Or VistaSeleccionadaGr.AdjEscPos = 0 Then VistaSeleccionadaGr.AdjEscPos = 3
    If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
        If Not VistaSeleccionadaGr.ConfVistasGrAtrib Is Nothing Then
            iNumAtrib = 0
            For Each oAtribGr In VistaSeleccionadaGr.ConfVistasGrAtrib
                If Not IsNull(GrupoSeleccionado.AtributosItem.Item(CStr(oAtribGr.Atributo)).PrecioFormula) And Not IsEmpty(GrupoSeleccionado.AtributosItem.Item(CStr(oAtribGr.Atributo)).PrecioFormula) Then
                    iNumAtrib = iNumAtrib + 1
                End If
                If IsNull(oAtribGr.Posicion) Or oAtribGr.Posicion = 0 Then
                    oAtribGr.Posicion = 1
                End If
            Next
        End If
    End If
        
    ReDim m_arrOrdenColsGr(2 + iNumAtrib)

    While iOrdenados <= UBound(m_arrOrdenColsGr)
        iOrden = 0
        sCol = ""
        If iOrdenAnt <= VistaSeleccionadaGr.PrecioEscPos Then
            bEncontrado = False
            For i = 0 To iOrdenados
                If m_arrOrdenColsGr(i) = "PRES" Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
            If Not bEncontrado Then
                iOrden = VistaSeleccionadaGr.PrecioEscPos
                sCol = "PRES"
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.AhorroEscPos) Then
            If VistaSeleccionadaGr.AhorroEscPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrdenColsGr(i) = "AHORRO" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If Not bEncontrado Then
                    iOrden = VistaSeleccionadaGr.AhorroEscPos
                    sCol = "AHORRO"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.AdjEscPos) Then
            If VistaSeleccionadaGr.AdjEscPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrdenColsGr(i) = "ADJCANT" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If Not bEncontrado Then
                    iOrden = VistaSeleccionadaGr.AdjEscPos
                    sCol = "ADJCANT"
                End If
            End If
        End If
        
        If Not VistaSeleccionadaGr.ConfVistasGrAtrib Is Nothing Then
            For Each oAtribGr In VistaSeleccionadaGr.ConfVistasGrAtrib
                If Not IsNull(GrupoSeleccionado.AtributosItem.Item(CStr(oAtribGr.Atributo)).PrecioFormula) And Not IsEmpty(GrupoSeleccionado.AtributosItem.Item(CStr(oAtribGr.Atributo)).PrecioFormula) Then
                    If oAtribGr.Posicion < iOrden Or (iOrden = 0 And sCol = "") Then
                        bEncontrado = False
                        For i = 0 To iOrdenados
                            If m_arrOrdenColsGr(i) = oAtribGr.Atributo Then
                                bEncontrado = True
                                Exit For
                            End If
                        Next i
                        If bEncontrado = False Then
                            iOrden = oAtribGr.Posicion
                            sCol = oAtribGr.Atributo
                        End If
                    End If
                End If
            Next
        End If
        
        If sCol <> "" Then
            m_arrOrdenColsGr(iOrdenados) = sCol
            iOrdenados = iOrdenados + 1
            iOrdenAnt = iOrden
        Else
            iOrdenAnt = iOrdenAnt + 1
        End If
    Wend
End Sub

''' <summary>Genera un array con las columnas de los grupos de escalados ordenadas</summary>
''' <remarks>Llamada desde: RedimensionarGridAll; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Private Sub OrdenarColsAllEsc()
    Dim iOrden As Integer
    Dim iOrdenados As Integer
    Dim iOrdenAnt As Integer
    Dim sCol As String
    Dim bEncontrado As Boolean
    Dim i As Integer
    Dim iNumAtrib As Integer
    
    'Posici�n de los campos del grupo 0 (les pone las posiciones en orden,porque sino a veces no funciona bien)
    iOrdenAnt = 0
    iOrdenados = 0
        
    'Pesta�a de grupo
    If IsNull(VistaSeleccionadaAll.PrecioEscPos) Or VistaSeleccionadaAll.PrecioEscPos = 0 Then VistaSeleccionadaAll.PrecioEscPos = 1
    If IsNull(VistaSeleccionadaAll.AhorroEscPos) Or VistaSeleccionadaAll.AhorroEscPos = 0 Then VistaSeleccionadaAll.AhorroEscPos = 2
    If IsNull(VistaSeleccionadaAll.AdjEscPos) Or VistaSeleccionadaAll.AdjEscPos = 0 Then VistaSeleccionadaAll.AdjEscPos = 3
        
    ReDim m_arrOrdenColsGr(2 + iNumAtrib)

    While iOrdenados <= UBound(m_arrOrdenColsGr)
        iOrden = 0
        sCol = ""
        If iOrdenAnt <= VistaSeleccionadaAll.PrecioEscPos Then
            bEncontrado = False
            For i = 0 To iOrdenados
                If m_arrOrdenColsGr(i) = "PRES" Then
                    bEncontrado = True
                    Exit For
                End If
            Next i
            If Not bEncontrado Then
                iOrden = VistaSeleccionadaAll.PrecioEscPos
                sCol = "PRES"
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.AhorroEscPos) Then
            If VistaSeleccionadaAll.AhorroEscPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrdenColsGr(i) = "AHORRO" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If Not bEncontrado Then
                    iOrden = VistaSeleccionadaAll.AhorroEscPos
                    sCol = "AHORRO"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.AdjEscPos) Then
            If VistaSeleccionadaAll.AdjEscPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrdenColsGr(i) = "ADJCANT" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If Not bEncontrado Then
                    iOrden = VistaSeleccionadaAll.AdjEscPos
                    sCol = "ADJCANT"
                End If
            End If
        End If
                
        If sCol <> "" Then
            m_arrOrdenColsGr(iOrdenados) = sCol
            iOrdenados = iOrdenados + 1
            iOrdenAnt = iOrden
        Else
            iOrdenAnt = iOrdenAnt + 1
        End If
    Wend
End Sub

''' <summary>Posici�n de los campos del grupo 0</summary>
''' <remarks>Llamada desde: RedimensionarGridGrupo; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Private Sub OrdenarColsGrupo0()
    Dim iOrden As Integer
    Dim iOrdenados As Integer
    Dim iOrdenAnt As Integer
    Dim sCol As String
    Dim bEncontrado As Boolean
    Dim i As Integer
    
    'Posici�n de los campos del grupo 0 (les pone las posiciones en orden,porque sino a veces no funciona bien)
    iOrdenAnt = 0
    iOrdenados = 0
    Erase m_arrOrden
        
    While iOrdenados < 10
        iOrden = 0
        sCol = ""
        
        If (iOrdenAnt <= VistaSeleccionadaGr.CantidadPos) Then
           If VistaSeleccionadaGr.CantidadPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "CANT" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.CantidadPos
                    sCol = "CANT"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.PresUniPos) Then
           If VistaSeleccionadaGr.PresUniPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "PRECAPE" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.PresUniPos
                    sCol = "PRECAPE"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.ProveedorPos) Then
           If VistaSeleccionadaGr.ProveedorPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "CODPROVEACTUAL" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.ProveedorPos
                    sCol = "CODPROVEACTUAL"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.ImportePos) Then
            If VistaSeleccionadaGr.ImportePos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "IMP" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.ImportePos
                    sCol = "IMP"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.AhorroPos) Then
            If VistaSeleccionadaGr.AhorroPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "AHORROIMP" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.AhorroPos
                    sCol = "AHORROIMP"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.AhorroPorcenPos) Then
            If VistaSeleccionadaGr.AhorroPorcenPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "AHORROPORCEN" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.AhorroPorcenPos
                    sCol = "AHORROPORCEN"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.SolicVinculadaPos) Then
            If VistaSeleccionadaGr.SolicVinculadaPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "VINCULADO_ITEM" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.SolicVinculadaPos
                    sCol = "VINCULADO_ITEM"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.FecIniSumPos) Then
            If VistaSeleccionadaGr.FecIniSumPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "INI" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.FecIniSumPos
                    sCol = "INI"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.FecFinSumPos) Then
            If VistaSeleccionadaGr.FecFinSumPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FIN" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.FecFinSumPos
                    sCol = "FIN"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaGr.EstrMatPos) Then
            If VistaSeleccionadaGr.EstrMatPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "GMN" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaGr.EstrMatPos
                    sCol = "GMN"
                End If
            End If
        End If
        
        If sCol <> "" Then
            m_arrOrden(iOrdenados) = sCol
            iOrdenados = iOrdenados + 1
            iOrdenAnt = iOrden
        Else
            iOrdenAnt = iOrdenAnt + 1
        End If
    Wend
End Sub

''' <summary>Posici�n de los campos del grupo 0</summary>
''' <remarks>Llamada desde: RedimensionarGridGrupo; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Private Sub OrdenarColsGrupo0All()
    Dim iOrden As Integer
    Dim iOrdenados As Integer
    Dim iOrdenAnt As Integer
    Dim sCol As String
    Dim bEncontrado As Boolean
    Dim i As Integer
    
    'Posici�n de los campos del grupo 0 (les pone las posiciones en orden,porque sino a veces no funciona bien)
    iOrdenAnt = 0
    iOrdenados = 0
    Erase m_arrOrden
        
    While iOrdenados < 10
        iOrden = 0
        sCol = ""
        
        If (iOrdenAnt <= VistaSeleccionadaAll.CantidadPos) Then
           If VistaSeleccionadaAll.CantidadPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "CANT" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.CantidadPos
                    sCol = "CANT"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.PresUniPos) Then
           If VistaSeleccionadaAll.PresUniPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "PRECAPE" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.PresUniPos
                    sCol = "PRECAPE"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.ProveedorPos) Then
           If VistaSeleccionadaAll.ProveedorPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "CODPROVEACTUAL" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.ProveedorPos
                    sCol = "CODPROVEACTUAL"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.ImportePos) Then
            If VistaSeleccionadaAll.ImportePos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "IMP" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.ImportePos
                    sCol = "IMP"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.AhorroPos) Then
            If VistaSeleccionadaAll.AhorroPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "AHORROIMP" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.AhorroPos
                    sCol = "AHORROIMP"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.AhorroPorcenPos) Then
            If VistaSeleccionadaAll.AhorroPorcenPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "AHORROPORCEN" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.AhorroPorcenPos
                    sCol = "AHORROPORCEN"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.SolicVinculadaPos) Then
            If VistaSeleccionadaAll.SolicVinculadaPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "VINCULADO_ITEM" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.SolicVinculadaPos
                    sCol = "VINCULADO_ITEM"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.FecIniSumPos) Then
            If VistaSeleccionadaAll.FecIniSumPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "INI" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.FecIniSumPos
                    sCol = "INI"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.FecFinSumPos) Then
            If VistaSeleccionadaAll.FecFinSumPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "FIN" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.FecFinSumPos
                    sCol = "FIN"
                End If
            End If
        End If
        
        If (iOrdenAnt <= VistaSeleccionadaAll.EstrMatPos) Then
            If VistaSeleccionadaAll.EstrMatPos < iOrden Or (iOrden = 0 And sCol = "") Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "GMN" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = VistaSeleccionadaAll.EstrMatPos
                    sCol = "GMN"
                End If
            End If
        End If
        
        If sCol <> "" Then
            m_arrOrden(iOrdenados) = sCol
            iOrdenados = iOrdenados + 1
            iOrdenAnt = iOrden
        Else
            iOrdenAnt = iOrdenAnt + 1
        End If
    Wend
End Sub

''' <summary>Oculta o hace visible un campo del grid</summary>
''' <param name="Campo">Campo a ocultar o hacer visible</param>
''' <param name="bVisible">visibilidad del campo</param>
''' <remarks>Llamada desde: frmOrigen.OcultarCampoGr; Tiempo m�ximo:0</remarks>
''' <remarks>Revisado por: LTG  Fecha: 13/09/2011</remarks>

Public Sub OcultarCampoGr(ByVal Campo As String, ByVal bVisible As Boolean)
    Dim i As Integer
    Dim oColumn As Column
    Dim bGrupoVisible As Boolean
    
    VistaSeleccionadaGr.HayCambios = True
        
    With sdbgEscalado
        Select Case Campo
             Case "CANT"
                VistaSeleccionadaGr.CantidadVisible = bVisible
                .Columns("CANT").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("CANT").Position = VistaSeleccionadaGr.CantidadPos
                End If
            
            Case "PRECAPE"
                VistaSeleccionadaGr.PresUniVisible = bVisible
                .Columns("PRECAPE").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("PRECAPE").Position = VistaSeleccionadaGr.PresUniPos
                End If
                
            Case "CODPROVEACTUAL"
                VistaSeleccionadaGr.ProveedorVisible = bVisible
                .Columns("CODPROVEACTUAL").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("CODPROVEACTUAL").Position = VistaSeleccionadaGr.ProveedorPos
                End If
                
            Case "IMP"
                VistaSeleccionadaGr.ImporteVisible = bVisible
                .Columns("IMP").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("IMP").Position = VistaSeleccionadaGr.ImportePos
                End If
                
            Case "AHORROIMP"
                VistaSeleccionadaGr.AhorroVisible = bVisible
                .Columns("AHORROIMP").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("AHORROIMP").Position = VistaSeleccionadaGr.AhorroPos
                End If
                
            Case "AHORROPORCEN"
                VistaSeleccionadaGr.AhorroPorcenVisible = bVisible
                .Columns("AHORROPORCEN").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("AHORROPORCEN").Position = VistaSeleccionadaGr.AhorroPorcenPos
                End If
                
            Case "PREC_ESC"   'PRES
                VistaSeleccionadaGr.PrecioEscVisible = bVisible
                'Lo oculta o hace visible en las grids
                For i = 2 To .Groups.Count - 1
                    .Columns("PRES" & i).Visible = bVisible
                    If VistaSeleccionadaGr.PrecioEscWidth = 0 Then
                        .Columns("PRES" & i).Width = 1200
                    End If
                Next i
                
            Case "AHOR_ESC"    'AHORRO
                VistaSeleccionadaGr.AhorroEscVisible = bVisible
                'Lo oculta o hace visible en las grids
                For i = 2 To .Groups.Count - 1
                    .Columns("AHORRO" & i).Visible = bVisible
                    If VistaSeleccionadaGr.AhorroEscWidth = 0 Then
                        .Columns("AHORRO" & i).Width = 1200
                    End If
                Next i
                                            
            Case "CANTADJ_ESC"  'Adjudicado escalado
                VistaSeleccionadaGr.AdjEscVisible = bVisible
                'Lo oculta o hace visible en las grids
                For i = 2 To .Groups.Count - 1
                    .Columns("ADJCANT" & i).Visible = bVisible
                    If VistaSeleccionadaGr.AdjEscWidth = 0 Then
                        .Columns("ADJCANT" & i).Width = 1200
                    End If
                Next i
                
            Case "VINCULADO_ITEM"
                VistaSeleccionadaGr.SolicVinculadaVisible = bVisible
                .Columns("VINCULADO_ITEM").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("VINCULADO_ITEM").Position = VistaSeleccionadaGr.SolicVinculadaPos
                End If
                
            Case "INI"
                VistaSeleccionadaGr.FecIniSumVisible = bVisible
                .Columns("INI").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("INI").Position = VistaSeleccionadaGr.FecIniSumPos
                End If
            
            Case "FIN"
                VistaSeleccionadaGr.FecFinSumVisible = bVisible
                .Columns("FIN").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("FIN").Position = VistaSeleccionadaGr.FecFinSumPos
                End If
                                          
            Case "GMN"
                VistaSeleccionadaGr.EstrMatVisible = bVisible
                .Columns("GMN").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("GMN").Position = VistaSeleccionadaGr.EstrMatPos
                End If
                                          
            Case Else  'Es un atributo
                VistaSeleccionadaGr.ConfVistasGrAtrib.Item(Campo).Visible = bVisible

                'Son atributos de coste/descuento si tienen operaci�n asociada
                If Not IsNull(GrupoSeleccionado.AtributosItem.Item(Campo).PrecioFormula) And Not IsEmpty(GrupoSeleccionado.AtributosItem.Item(Campo).PrecioFormula) Then
                    'Lo oculta o hace visible en las grids
                    For i = 2 To .Groups.Count - 1
                        .Columns(Campo & i).Visible = bVisible
                        If VistaSeleccionadaGr.ConfVistasGrAtrib.Item(Campo).Width = 0 Then
                            .Columns(Campo & i).Width = 500
                        End If
                    Next i
                Else
                    .Columns(Campo).Visible = bVisible
                    If VistaSeleccionadaGr.ConfVistasGrAtrib.Item(Campo).Width = 0 Then
                        .Columns(Campo).Width = 500
                    End If
                    If bVisible Then
                        If Not .Groups(1).Visible Then
                            .Groups(1).Visible = True
                            .Groups(1).Width = 800
                        End If
                        If .Groups(1).Width < .Columns(Campo).Width Then .Groups(1).Width = .Columns(Campo).Width
                    Else
                        'Ocultar el grupo si no hay columnas visibles
                        bGrupoVisible = False
                        For Each oColumn In .Groups(1).Columns
                            If oColumn.Visible Then
                                bGrupoVisible = True
                                Exit For
                            End If
                        Next
                        Set oColumn = Nothing
                        .Groups(1).Visible = bGrupoVisible
                    End If
                End If
        End Select
    
        If UserControl.Enabled And Me.frmOrigen.Visible Then .SetFocus
        
        .MoveFirst
    End With
End Sub

''' <summary>Oculta o hace visible un campo del grid</summary>
''' <param name="Campo">Campo a ocultar o hacer visible</param>
''' <param name="bVisible">visibilidad del campo</param>
''' <remarks>Llamada desde: frmOrigen.OcultarCampoGr; Tiempo m�ximo:0</remarks>
''' <revision>21/03/2012</revision>

Public Sub OcultarCampoAll(ByVal Campo As String, ByVal bVisible As Boolean)
    Dim i As Integer
    
    VistaSeleccionadaAll.HayCambios = True
        
    With sdbgEscalado
        Select Case Campo
             Case "CANT"
                VistaSeleccionadaAll.CantidadVisible = bVisible
                .Columns("CANT").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("CANT").Position = VistaSeleccionadaAll.CantidadPos
                End If
                
            Case "PRECAPE"
                VistaSeleccionadaAll.PresUniVisible = bVisible
                .Columns("PRECAPE").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("PRECAPE").Position = VistaSeleccionadaAll.PresUniPos
                End If
                
            Case "CODPROVEACTUAL"
                VistaSeleccionadaAll.ProveedorVisible = bVisible
                .Columns("CODPROVEACTUAL").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("CODPROVEACTUAL").Position = VistaSeleccionadaAll.ProveedorPos
                End If
                
            Case "IMP"
                VistaSeleccionadaAll.ImporteVisible = bVisible
                .Columns("IMP").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("IMP").Position = VistaSeleccionadaAll.ImportePos
                End If
                
            Case "AHORROIMP"
                VistaSeleccionadaAll.AhorroVisible = bVisible
                .Columns("AHORROIMP").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("AHORROIMP").Position = VistaSeleccionadaAll.AhorroPos
                End If
                
            Case "AHORROPORCEN"
                VistaSeleccionadaAll.AhorroPorcenVisible = bVisible
                .Columns("AHORROPORCEN").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("AHORROPORCEN").Position = VistaSeleccionadaAll.AhorroPorcenPos
                End If
                
            Case "PREC_ESC"   'PRES
                VistaSeleccionadaAll.PrecioEscVisible = bVisible
                'Lo oculta o hace visible en las grids
                For i = 2 To .Groups.Count - 1
                    .Columns("PRES" & i).Visible = bVisible
                    If VistaSeleccionadaAll.PrecioEscWidth = 0 Then
                        .Columns("PRES" & i).Width = 1200
                    End If
                Next i
                
            Case "AHOR_ESC"    'AHORRO
                VistaSeleccionadaAll.AhorroEscVisible = bVisible
                'Lo oculta o hace visible en las grids
                For i = 2 To .Groups.Count - 1
                    .Columns("AHORRO" & i).Visible = bVisible
                    If VistaSeleccionadaAll.AhorroEscWidth = 0 Then
                        .Columns("AHORRO" & i).Width = 1200
                    End If
                Next i
            
            Case "CANTADJ_ESC"    'AHORRO
                VistaSeleccionadaAll.AdjEscVisible = bVisible
                'Lo oculta o hace visible en las grids
                For i = 2 To .Groups.Count - 1
                    .Columns("ADJCANT" & i).Visible = bVisible
                    If VistaSeleccionadaAll.AdjEscWidth = 0 Then
                        .Columns("ADJCANT" & i).Width = 1200
                    End If
                Next i
                
            Case "VINCULADO_ITEM"
                VistaSeleccionadaAll.SolicVinculadaVisible = bVisible
                .Columns("VINCULADO_ITEM").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("VINCULADO_ITEM").Position = VistaSeleccionadaAll.SolicVinculadaPos
                End If
                
            Case "INI"
                VistaSeleccionadaAll.FecIniSumVisible = bVisible
                .Columns("INI").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("INI").Position = VistaSeleccionadaAll.FecIniSumPos
                End If
                
            Case "FIN"
                VistaSeleccionadaAll.FecFinSumVisible = bVisible
                .Columns("FIN").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("FIN").Position = VistaSeleccionadaAll.FecFinSumPos
                End If
                
            Case "GMN"
                VistaSeleccionadaAll.EstrMatVisible = bVisible
                .Columns("GMN").Visible = bVisible
                If .GrpPosition(0) = 0 Then
                    .Columns("GMN").Position = VistaSeleccionadaAll.EstrMatPos
                End If
                                
        End Select
    
        If UserControl.Enabled And Me.frmOrigen.Visible Then .SetFocus
        
        .MoveFirst
    End With
End Sub

''' <summary>Realiza un cierre parcial</summary>
''' <returns>Booleano indicando si se produce un cierre parcial</returns>
''' <remarks>Llamada desde: frmADJ.Adjudicar; Tiempo m�ximo:0</remarks>
''' <revision>LTG 23/02/2012</revision>

Public Function CierreParcial() As Boolean
    Dim bItemsSelec As Boolean
    Dim vbm As Variant
    Dim i As Integer
    Dim j As Integer
    Dim dblCantidad As Double
    Dim dblPrecio As Double
    Dim bAdj As Boolean
    Dim oProve As CProveedor
    Dim sCod As String
    Dim bCierreTotal As Boolean
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim oAdjParcs As CAdjudicaciones
    Dim oAdjParcsGrupo As CAdjsGrupo
    Dim iResp As Integer
    Dim dYaCerrado  As Double
    Dim dblCambio As Double
    
    CierreParcial = False
    
    With sdbgEscalado
        If .SelBookmarks.Count > 0 Then
            'Comprobar que se han seleccionado filas de item
            bItemsSelec = True
            For Each vbm In .SelBookmarks
                If .Columns("PROV").CellValue(vbm) = "1" Then
                    bItemsSelec = False
                    Exit For
                End If
            Next
            
            If Not bItemsSelec Then
                oMensajes.SeleccionItemsCierreParcial
            Else
                If Not frmADJCierreParc.m_oProcesoSeleccionado Is Nothing Then Exit Function
                
                Screen.MousePointer = vbHourglass
                
                frmOrigen.g_dblAdjudicadoProcCierre = frmOrigen.m_dblAdjudicadoProcReal
                frmOrigen.g_dblConsumidoProcCierre = frmOrigen.m_dblConsumidoProcReal
                Set frmADJCierreParc.m_oProcesoSeleccionado = ProcesoSeleccionado
                Set frmADJCierreParc.m_oGrupoSeleccionado = GrupoSeleccionado
                Set frmADJCierreParc.frmOrigen = frmOrigen
            
                'Carga todos los items seleccionados y que no est�n ya parcialmente cerrados
                For i = 0 To .SelBookmarks.Count - 1
                    If Not .Columns("CERRADO").CellValue(.SelBookmarks.Item(i)) Then
                        For Each oProve In ProvesAsig
                            sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            sCod = .Columns("ID").CellValue(.SelBookmarks.Item(i)) & sCod
                            If Not AdjsProve.Item(sCod) Is Nothing Then
                                dblCambio = GrupoSeleccionado.Items.Item(CStr(.Columns("ID").CellValue(.SelBookmarks.Item(i)))).CambioComparativa(oProve.Cod, True, m_oAdjsProve)
                                dblCantidad = AdjsProve.Item(sCod).Adjudicado
                                If dblCantidad > 0 Then dblPrecio = CDec(AdjsProve.Item(sCod).ImporteAdj / dblCambio) / AdjsProve.Item(sCod).Adjudicado
                                                                
                                bAdj = OfertaAdjudicable(oProve.Cod)
                
                                'If dblCantidad <> 0 Then
                                    frmADJCierreParc.sdbgAdj.AddItem .Columns("DESCR").CellValue(.SelBookmarks.Item(i)) & Chr(m_lSeparador) & oProve.Cod & " " & _
                                        oProve.Den & Chr(m_lSeparador) & dblCantidad & Chr(m_lSeparador) & dblPrecio & Chr(m_lSeparador) & _
                                        .Columns("ID").CellValue(.SelBookmarks.Item(i)) & Chr(m_lSeparador) & oProve.Cod & Chr(m_lSeparador) & _
                                        AdjsProve.Item(sCod).Porcentaje & Chr(m_lSeparador) & AdjsProve.Item(sCod).NumOfe & Chr(m_lSeparador) & IIf(bAdj, -1, 0)
                                'End If
                            End If
                        Next
                        Set oProve = Nothing
                    End If
                Next i
            
                'comprueba si se va a realizar un cierre parcial o si va a ser total
                bCierreTotal = True
                For i = 0 To .Rows - 1
                    vbm = .AddItemBookmark(i)
                    If .Columns("PROV").CellValue(vbm) = "0" Then
                        If Not .Columns("CERRADO").CellValue(vbm) Then
                            For j = 0 To .SelBookmarks.Count - 1
                                If .Columns("ID").CellValue(vbm) = .Columns("ID").CellValue(.SelBookmarks.Item(j)) Then
                                    bCierreTotal = True
                                    Exit For
                                Else
                                    bCierreTotal = False
                                End If
                            Next
                        End If
                    End If
                    
                    If Not bCierreTotal Then Exit For
                Next
                
                If bCierreTotal Then  'Si se va a cerrar el grupo comprueba si es o no un cierre total del proceso
                    For Each oGrupo In ProcesoSeleccionado.Grupos
                        If oGrupo.Codigo <> GrupoSeleccionado.Codigo Then
                            For Each oItem In oGrupo.Items
                                If oItem.Confirmado Then
                                    If Not oItem.Cerrado Then
                                        bCierreTotal = False
                                        frmOrigen.m_bCierreGrupo = True
                                        Exit For
                                    End If
                                End If
                            Next
            
                        End If
                    Next
                End If
                '3147
                dYaCerrado = 0
                For Each oGrupo In ProcesoSeleccionado.Grupos
                    For Each oItem In oGrupo.Items
                        If oItem.Confirmado = True Then
                            If oItem.Cerrado = True Then
                                dYaCerrado = dYaCerrado + oItem.ImporteAdj
                            End If
                        End If
                    Next
                Next
                frmADJCierreParc.m_dYaCerrado = dYaCerrado
                                
                'Carga la colecci�n con los items a cerrar parcialmente y la pasa al form. de cierre parcial
                If PreadjudicarParcialGrupo(oAdjParcs, oAdjParcsGrupo) Then
                    iResp = vbYes
                    
                    If ExistenAdjSinCantPresup(False, ProcesoSeleccionado, oAdjParcs) Then
                        iResp = oMensajes.PreguntaContinuarAdjudicacionSinCantPresup
                        If iResp = vbNo Then
                            Set frmADJCierreParc.m_oProcesoSeleccionado = Nothing
                            Set frmADJCierreParc.m_oGrupoSeleccionado = Nothing
                        End If
                    End If
                
                    If iResp = vbYes Then
                        Set frmADJCierreParc.oGRCierre = oFSGSRaiz.Generar_CGrupos
                        If frmOrigen.m_bCierreGrupo Then
                            frmADJCierreParc.oGRCierre.Add ProcesoSeleccionado, GrupoSeleccionado.Codigo, GrupoSeleccionado.Den
                        End If
                        frmADJCierreParc.bCierreTotal = bCierreTotal
                    
                        Set frmADJCierreParc.g_oAdjsGrupo = frmOrigen.m_oAdjs
                        Set frmADJCierreParc.oAdjs = oAdjParcs
                        Set frmOrigen.m_oAdjParcs = oAdjParcs
                        Set frmOrigen.m_oAdjParcsGrupo = oAdjParcsGrupo
                        Screen.MousePointer = vbNormal
                        frmADJCierreParc.Show vbModal
                    End If
                Else
                    Set frmADJCierreParc.m_oProcesoSeleccionado = Nothing
                    Set frmADJCierreParc.m_oGrupoSeleccionado = Nothing
                End If
            End If
            
            Screen.MousePointer = vbNormal
            CierreParcial = True
        End If
    End With
    
    Set oAdjParcs = Nothing
End Function

''' <summary>Realiza una reapertura parcial</summary>
''' <returns>Booleano indicando si se produce un cierre parcial</returns>
''' <remarks>Llamada desde: frmADJ.Adjudicar; Tiempo m�ximo:0</remarks>
''' <revision>LTG 23/02/2012</revision>

Public Function ReaperturaParcial(bItemsDeGrupo As Boolean) As Boolean
    Dim bItemsSelec As Boolean
    Dim vbm As Variant
    Dim i As Integer
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim oItems As CItems
    Dim oAdjParcs As CAdjudicaciones
    Dim teserror As TipoErrorSummit
    Set oItems = oFSGSRaiz.Generar_CItems
    
    ReaperturaParcial = False
    
    With sdbgEscalado
        If .SelBookmarks.Count > 0 Then
            'Comprobar que se han seleccionado filas de item
            bItemsSelec = True
            For Each vbm In .SelBookmarks
                If .Columns("PROV").CellValue(vbm) = "1" Then
                    bItemsSelec = False
                    Exit For
                End If
            Next
            If bItemsSelec Then
                'Carga todos los items seleccionados y que no est�n ya parcialmente cerrados
                For i = 0 To .SelBookmarks.Count - 1
                    Set oGrupo = ProcesoSeleccionado.Grupos.Item(sdbgEscalado.Columns("GRUPOCOD").CellValue(.SelBookmarks.Item(i)))
                    Set oItem = oGrupo.Items.Item(.Columns("ID").CellValue(.SelBookmarks.Item(i)))
                    If oItem.isCerrado Then
                        oItem.Cerrado = True
                        oItems.AddItem oItem
                    End If
                Next i
            
                If oItems.Count > 0 Then
                    If ProcesoSeleccionado.estanAbiertosTodosLosGrupos Then
                        teserror = ProcesoSeleccionado.Reabrir(oUsuarioSummit.Cod)
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                        Else
                            oMensajes.ProcesoCerrado
                        End If
                    Else
                        teserror = ProcesoSeleccionado.reabrirItems(oUsuarioSummit.Cod, oItems)
                        If teserror.NumError <> TESnoerror Then
                            For Each oItem In oItems
                                oItem.Cerrado = False
                            Next
                        Else
                            ReaperturaParcial = True
                        End If
                        
                    End If
                Else
                    If bItemsDeGrupo Then
                        oMensajes.mensajeGenericoOkOnly (m_sNoReabrirGrupos & "." & vbCrLf & m_sNingunoAdjudicado)
                    Else
                        oMensajes.mensajeGenericoOkOnly (m_sNoReabrirItems & "." & vbCrLf & m_sNingunoAdjudicado)
                    End If
                    ReaperturaParcial = False
                End If
            End If
        Else
            Parent.reabrirProceso
        End If
        
        Screen.MousePointer = vbNormal
        
        
    End With
    
    Set oAdjParcs = Nothing
End Function

''' <summary>Ver si el grupo seleccionado es adjudicable parcialmente y cargar la colecci�n de adjudicaciones parciales</summary>
''' <param name="oAdjParcs">Colecci�n de adjudicaciones parciales</param>
''' <returns>Adjudicable parcialmente si o no</returns>
''' <remarks>Llamada desde: frmAdj/Adjudicar ; Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 21/03/2012</revision>

Private Function PreadjudicarParcialGrupo(ByRef oAdjParcs As CAdjudicaciones, ByRef oAdjParcsGrupo As CAdjsGrupo) As Boolean
    Dim j As Integer
    Dim i As Integer
    Dim dPrecio As Double
    Dim iIndice As Integer
    Dim dAdjudicadoTot As Double
    Dim sCod As String
    Dim bAdjudicadas As Boolean
    Dim dAdjudicado As Double
    Dim dConsumido As Double
    Dim sId As String
    Dim oProve As CProveedor
    Dim oProves As CProveedores
    Dim sCodGMN4 As String
    Dim sProveBloqueo() As String
    Dim sProveAviso() As String
    Dim oProveUmbral As CProveedor
    Dim iAvisoBloqueo As Integer
    Dim bMensajeAviso As Boolean
    Dim bMensajeBloqueo As Boolean
    Dim iResp As Integer
    Dim bAnadir As Boolean
    Dim vPrecio As Variant
    Dim vObjetivo As Variant
    Dim oItem As CItem
    Dim oEsc As CEscalado
    Dim bEscAdjSinCant As Boolean
    Dim oEscalado As CEscalado
    Dim oAdjEsc As CAdjudicacion
    Dim sCodEsc As String
    Dim scodProve As String
    Dim dPorcenAdj As Double
    Dim iNumAdjProve As Integer
    
    On Error GoTo Error
    
    'Rellenamos la colecci�n de adjudicaciones
    Set oAdjParcs = oFSGSRaiz.Generar_CAdjudicaciones
    Set oAdjParcsGrupo = oFSGSRaiz.Generar_CAdjsGrupo
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    bAdjudicadas = False
    
    iIndice = 0

    'Se recorre cada item seleccionado
    With sdbgEscalado
        For j = 0 To .SelBookmarks.Count - 1
            Set oItem = GrupoSeleccionado.Items.Item(.Columns("ID").CellValue(.SelBookmarks.Item(j)))
            
            If Not .Columns("CERRADO").CellValue(.SelBookmarks.Item(j)) Then
                'Primero almaceno las adjudicaciones para cada proveedor
                For Each oProve In ProvesAsig
                    If Not GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then  'Si hay oferta
                        'Comprobar si se trata de escalados adjudicados sin cantidad pero con cantidad de proveedor
                        bEscAdjSinCant = True
                        iNumAdjProve = 0
                        For Each oEscalado In GrupoSeleccionado.Escalados
                            sCodEsc = KeyEscalado(oProve.Cod, oItem.Id, oEscalado.Id)
                            Set oAdjEsc = GrupoSeleccionado.Adjudicaciones.Item(sCodEsc)
                            If Not oAdjEsc Is Nothing Then
                                If NullToDbl0(oAdjEsc.Adjudicado) > 0 Then bEscAdjSinCant = False
                                iNumAdjProve = iNumAdjProve + 1
                            End If
                        Next
                        
                        For Each oEsc In GrupoSeleccionado.Escalados
                            sCod = KeyEscalado(oProve.Cod, .Columns("ID").CellValue(.SelBookmarks.Item(j)), oEsc.Id)
                            If Not GrupoSeleccionado.Adjudicaciones.Item(sCod) Is Nothing Then
                                dAdjudicadoTot = GrupoSeleccionado.Adjudicaciones.Item(sCod).ImporteAdjTot
                                
                                bAdjudicadas = True
                                                                                            
                                dPrecio = GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(.Columns("ID").CellValue(.SelBookmarks.Item(i))).Escalados.Item(CStr(oEsc.Id)).Precio
                                                            
                                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                If NullToDbl0(oItem.Cantidad) > 0 Then
                                    If bEscAdjSinCant Then
                                        'Repartir el porcentaje entre todos los escalados
                                        dPorcenAdj = AdjsProve.Item(CStr(oItem.Id) & scodProve).Porcentaje / iNumAdjProve
                                    Else
                                        dPorcenAdj = (NullToDbl0(GrupoSeleccionado.Adjudicaciones.Item(sCod).Adjudicado) / oItem.Cantidad) * 100
                                    End If
                                Else
                                    If bEscAdjSinCant Then
                                        dPorcenAdj = 100 / iNumAdjProve
                                    Else
                                        dPorcenAdj = (NullToDbl0(GrupoSeleccionado.Adjudicaciones.Item(sCod).Adjudicado) / AdjsProve.Item(CStr(oItem.Id) & scodProve).Adjudicado) * 100
                                    End If
                                End If
                                
                                oAdjParcs.Add oProve.Cod, GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Num, val(.Columns("ID").CellValue(.SelBookmarks.Item(j))), _
                                    dPrecio, dPorcenAdj, iIndice, Null, Null, , GrupoSeleccionado.Codigo, , , GrupoSeleccionado.Adjudicaciones.Item(sCod).Adjudicado, oEsc.Id
                                oAdjParcs.Item(CStr(iIndice)).ImporteAdjTot = dPrecio * (NullToDbl0(oItem.Cantidad) * (dPorcenAdj / 100))
                                iIndice = iIndice + 1
                                
                                'Carga la coleccion de proveedores y materiales, para comprobar los umbrales
                                If oProves.Item(oProve.Cod) Is Nothing Then
                                    oProves.Add oProve.Cod, " "
                                    Set oProves.Item(oProve.Cod).IMaterialAsignado_GruposMN4 = Nothing
                                    Set oProves.Item(oProve.Cod).IMaterialAsignado_GruposMN4 = oFSGSRaiz.Generar_CGruposMatNivel4
                                    
                                    oProves.Item(oProve.Cod).Den = oProves.Item(oProve.Cod).DevolverDenominacionProve
                                End If
                
                                With GrupoSeleccionado.Items.Item(.Columns("ID").CellValue(.SelBookmarks.Item(i)))
                                    sCodGMN4 = .GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(.GMN1Cod))
                                    sCodGMN4 = sCodGMN4 & .GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(.GMN2Cod))
                                    sCodGMN4 = sCodGMN4 & .GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(.GMN3Cod))
                                    sCodGMN4 = sCodGMN4 & .GMN4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(.GMN4Cod))
                
                                    If oProves.Item(oProve.Cod).IMaterialAsignado_GruposMN4.Item(sCodGMN4) Is Nothing Then
                                        oProves.Item(oProve.Cod).IMaterialAsignado_GruposMN4.Add .GMN1Cod, .GMN2Cod, .GMN3Cod, _
                                            " ", " ", " ", .GMN4Cod, " "
                                    End If
                                End With
                            End If
                        Next
                        
                        Set oEsc = Nothing
                    End If
                Next
        
                iIndice = iIndice + 1
            End If
        Next j
    End With
    
    'Guarda la clase de adjudicaciones para el grupo a cerrar
    If Not frmOrigen.m_bCierreGrupo Then
        For Each oProve In ProvesAsig
            dAdjudicado = 0
            dConsumido = 0
            
            With sdbgEscalado
                For j = 0 To .SelBookmarks.Count - 1
                    If .Columns("CERRADO").CellValue(.SelBookmarks.Item(j)) = False Then
                        sId = CStr(.Columns("ID").CellValue(.SelBookmarks.Item(j)))
                        
                        If Not GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                            If Not GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(sId) Is Nothing Then
                                sCod = sId & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                
                                If Not AdjsProve.Item(sCod) Is Nothing Then
                                    Set oItem = GrupoSeleccionado.Items.Item(sId)
                                    
                                    dAdjudicado = dAdjudicado + AdjsProve.Item(sCod).importe
                                                            
                                    If IsNull(oItem.Precio) Then
                                        'Volver a calcular el precio del item en funci�n de las nuevas adjudicaciones
                                        PresupuestoYObjetivoItemConEscalados GrupoSeleccionado, oItem, ProvesAsig, vPrecio, vObjetivo
                                    End If
                                                                      
                                    dConsumido = dConsumido + CDec(NullToDbl0(vPrecio) * AdjsProve.Item(sCod).Adjudicado)
    
                                    Set oItem = Nothing
                                End If
                            End If
                        End If
                    End If
                Next j
            End With
            
            'Si hay oferta para el proveedor
            If Not GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                oAdjParcsGrupo.Add ProcesoSeleccionado, GrupoSeleccionado.Codigo, oProve.Cod, GrupoSeleccionado.UltimasOfertas.Item(Trim(oProve.Cod)).Num, _
                    dAdjudicado, 0, 0, dConsumido, , , , , , , ProcesoSeleccionado.Ofertas.Item(oProve.Cod).Cambio
            End If
        Next
    Else
        For Each oProve In ProvesAsig
            sCod = GrupoSeleccionado.Codigo & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            If Not frmOrigen.m_oAdjs.Item(sCod) Is Nothing Then
                oAdjParcsGrupo.Add ProcesoSeleccionado, GrupoSeleccionado.Codigo, oProve.Cod, frmOrigen.m_oAdjs.Item(sCod).NumOfe, _
                    frmOrigen.m_oAdjs.Item(sCod).Adjudicado, 0, 0, frmOrigen.m_oAdjs.Item(sCod).Consumido, frmOrigen.m_oAdjs.Item(sCod).importe, , , , , , _
                    frmOrigen.m_oAdjs.Item(sCod).Cambio
            End If
        Next
    End If
    
    If frmOrigen.m_bComparativaQA Then
        bMensajeAviso = False
        bMensajeBloqueo = False
        
        ReDim sProveBloqueo(0)
        ReDim sProveAviso(0)
        
        For Each oProveUmbral In oProves  'para cada proveedor
            If Not (ProvesUmbrales.Item(oProveUmbral.Cod & "#2") Is Nothing) Then
                bMensajeBloqueo = True
                
                bAnadir = True
                For i = 1 To UBound(sProveBloqueo)
                    If sProveBloqueo(i) = oProveUmbral.Cod & " - " & oProveUmbral.Den Then
                        bAnadir = False
                        Exit For
                    End If
                Next
                
                If bAnadir Then
                    ReDim Preserve sProveBloqueo(UBound(sProveBloqueo) + 1)
                    sProveBloqueo(UBound(sProveBloqueo)) = oProveUmbral.Cod & " - " & oProveUmbral.Den
                End If
                
            End If
            
            If Not (ProvesUmbrales.Item(oProveUmbral.Cod & "#1") Is Nothing) Then
                bMensajeAviso = True
                
                bAnadir = True
                For i = 1 To UBound(sProveAviso)
                    If sProveAviso(i) = oProveUmbral.Cod & " - " & oProveUmbral.Den Then
                        bAnadir = False
                        Exit For
                    End If
                Next
                
                If bAnadir Then
                    ReDim Preserve sProveAviso(UBound(sProveAviso) + 1)
                    sProveAviso(UBound(sProveAviso)) = oProveUmbral.Cod & " - " & oProveUmbral.Den
                End If
            End If
        Next
        Set oProveUmbral = Nothing
        
        If bMensajeBloqueo Then
            If UBound(sProveBloqueo) > 0 Then
                oMensajes.BloqueoProveedoresUmbrales (sProveBloqueo)
                PreadjudicarParcialGrupo = False
                Exit Function
            End If
        End If
        If bMensajeAviso Then
            If UBound(sProveAviso) > 0 Then
                iResp = oMensajes.AvisoProveedoresUmbrales(sProveAviso)
                If iResp = vbNo Then
                    PreadjudicarParcialGrupo = False
                    Exit Function
                End If
            End If
        End If
    End If
    
    PreadjudicarParcialGrupo = True
    
    Exit Function
    
Error:
    Select Case err.Number
        Case 9
            Select Case iAvisoBloqueo
                Case 1
                    ReDim Preserve sProveAviso(1)
                Case 2
                    ReDim Preserve sProveBloqueo(1)
            End Select
            Resume Next
    End Select
End Function

Private Sub UserControl_Terminate()
    Set m_dcItemsOcultos = Nothing
    Set m_colOcultarItemsBookmarks = Nothing
    Set m_oAdjsProve = Nothing
    Set AdjsProve = Nothing
End Sub

''' <summary>Actualiza la apariencia de las grids despues de un cierre</summary>
''' <remarks>Llamada desde: frmADJ.ActualizarGrid; Tiempo m�ximo: 0</remarks>
''' <revision>21/03/2012</revision>

Public Sub ActualizarGrid()
    Dim i As Integer
    Dim k As Integer
    
    With sdbgEscalado
        For i = 0 To .SelBookmarks.Count - 1


            
            'Refrescar el estado del item
            .Bookmark = .SelBookmarks.Item(i)
            If .Columns("ID").Value = .Columns("ID").CellValue(.SelBookmarks.Item(i)) Then
                'Pone cerrado a true y la fila cerrada en gris
                .Columns("CERRADO").Value = True
                Item(.Columns("GRUPOCOD").Value, .Columns("ID").Value).Cerrado = True
            End If
            
            'Refrescar el estado de cada proveedor
            k = .Row + 1
            If k < .Rows Then
                .Row = k
                While .Columns("PROV").Value = "1" And k < .Rows
                    .Columns("CERRADO").Value = True
                    k = k + 1
                    .Row = k
                Wend
            End If
        Next
         
        .Refresh
        .MoveFirst
    End With
End Sub

''' <summary>Asigna el formato a las columnas con datos num�ricos de los grids</summary>
''' <remarks>Llamada desde: frmADJ.FormateoDecimales, frmRESREU.FormateoDecimales</remarks>
''' <revision>LTG 19/09/2012</revision>

Public Sub FormateoDecimales(ByVal sAmbito As String)
    Dim i As Integer
    Dim oAtributo As CAtributo

    With sdbgEscalado
        Select Case sAmbito
            Case "GR"
                .Columns("AHORROIMP").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecResult)
                .Columns("AHORROPORCEN").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPorcen, True)
                .Columns("IMP").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecResult)
                .Columns("CANT").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecCant)
                .Columns("PRECAPE").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                'No se le indica el formato aqu� porque si notambi�n intentar�a aplicar el formato num�rico al c�digo
                'del proveedor actual (a nivel de item)
                .Columns("CODPROVEACTUAL").NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                
                For i = 2 To .Groups.Count - 1
                    .Columns("PRES" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                    .Columns("AHORRO" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                    .Columns("ADJCANT" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                    
                    'Atributos
                    If VistaSeleccionadaGr.Vista <> TipoDeVistaDefecto.vistainicial Then
                        'Formateo num�rico de los atributos aplicables al precio
                        If Not GrupoSeleccionado.AtributosItem Is Nothing Then
                            For Each oAtributo In GrupoSeleccionado.AtributosItem
                                If Not IsNull(oAtributo.PrecioFormula) And Not IsEmpty(oAtributo.PrecioFormula) Then
                                    If Not IsNull(oAtributo.PrecioAplicarA) Then
                                        .Columns(oAtributo.idAtribProce & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                                    ElseIf oAtributo.Tipo = TipoNumerico Then
                                        If oAtributo.NumDecAtrib > VistaSeleccionadaGr.DecPrecios Then
                                            .Columns(oAtributo.idAtribProce & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaGr.DecPrecios)
                                        Else
                                            .Columns(oAtributo.idAtribProce & i).NumberFormat = FormateoNumericoComp(oAtributo.NumDecAtrib)
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next

            Case "ALL"
                .Columns("AHORROIMP").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecResult)
                .Columns("AHORROPORCEN").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPorcen, True)
                .Columns("IMP").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecResult)
                .Columns("CANT").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecCant)
                .Columns("PRECAPE").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
                'No se le indica el formato aqu� porque si notambi�n intentar�a aplicar el formato num�rico al c�digo
                'del proveedor actual (a nivel de item)
                .Columns("CODPROVEACTUAL").NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
                
                For i = 2 To .Groups.Count - 1
                    .Columns("PRES" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
                    .Columns("AHORRO" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
                    .Columns("ADJCANT" & i).NumberFormat = FormateoNumericoComp(VistaSeleccionadaAll.DecPrecios)
                Next
        End Select
    End With
End Sub

Public Property Get ModificacionObjetivos() As Boolean

    ModificacionObjetivos = m_bModificacionObjetivos

End Property

Public Property Let ModificacionObjetivos(ByVal bModificacionObjetivos As Boolean)

    m_bModificacionObjetivos = bModificacionObjetivos

    Call UserControl.PropertyChanged("ModificacionObjetivos")

End Property

Public Property Get Item(ByVal codgrupo As String, ByVal IdItem As Integer) As CItem
    Set Item = ProcesoSeleccionado.Grupos.Item(codgrupo).Items.Item(CStr(IdItem))
End Property


