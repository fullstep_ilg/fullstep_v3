VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFormularios 
   Caption         =   "DFormularios"
   ClientHeight    =   7395
   ClientLeft      =   60
   ClientTop       =   510
   ClientWidth     =   10365
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormularios.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   7395
   ScaleWidth      =   10365
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   5145
      Left            =   315
      TabIndex        =   10
      Top             =   1605
      Width           =   10095
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   17
      stylesets.count =   61
      stylesets(0).Name=   "TituloAzulGER"
      stylesets(0).BackColor=   16777160
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormularios.frx":014A
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "ECSPA"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFormularios.frx":0166
      stylesets(1).AlignmentPicture=   1
      stylesets(2).Name=   "PiezasDefectuosasGER"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmFormularios.frx":0638
      stylesets(2).AlignmentPicture=   1
      stylesets(3).Name=   "TituloAzulSPA"
      stylesets(3).BackColor=   16777160
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmFormularios.frx":0654
      stylesets(3).AlignmentPicture=   1
      stylesets(4).Name=   "TituloAmarilloENG"
      stylesets(4).BackColor=   12648447
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmFormularios.frx":0670
      stylesets(4).AlignmentPicture=   1
      stylesets(5).Name=   "Calculado"
      stylesets(5).BackColor=   16766421
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmFormularios.frx":068C
      stylesets(5).AlignmentPicture=   1
      stylesets(6).Name=   "PDSumatorioGER"
      stylesets(6).BackColor=   16766421
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmFormularios.frx":06A8
      stylesets(6).AlignmentPicture=   1
      stylesets(7).Name=   "PiezasDefectuosasSPA"
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmFormularios.frx":15CE
      stylesets(7).AlignmentPicture=   1
      stylesets(8).Name=   "TituloAzulPeque�o"
      stylesets(8).BackColor=   16777130
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmFormularios.frx":15EA
      stylesets(8).AlignmentPicture=   1
      stylesets(9).Name=   "PDSumatorioSPA"
      stylesets(9).BackColor=   16766421
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "frmFormularios.frx":1606
      stylesets(9).AlignmentPicture=   1
      stylesets(10).Name=   "TituloDenProcesoENG"
      stylesets(10).HasFont=   -1  'True
      BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(10).Picture=   "frmFormularios.frx":1622
      stylesets(10).AlignmentPicture=   1
      stylesets(11).Name=   "TituloENG"
      stylesets(11).HasFont=   -1  'True
      BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(11).Picture=   "frmFormularios.frx":23F4
      stylesets(11).AlignmentPicture=   1
      stylesets(12).Name=   "PiezasDefectuosasSumatorioENG"
      stylesets(12).BackColor=   16766421
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "frmFormularios.frx":28C6
      stylesets(12).AlignmentPicture=   1
      stylesets(13).Name=   "Gris"
      stylesets(13).BackColor=   -2147483633
      stylesets(13).HasFont=   -1  'True
      BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(13).Picture=   "frmFormularios.frx":28E2
      stylesets(14).Name=   "PDENG"
      stylesets(14).HasFont=   -1  'True
      BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(14).Picture=   "frmFormularios.frx":28FE
      stylesets(14).AlignmentPicture=   1
      stylesets(15).Name=   "ECSumatorioGER"
      stylesets(15).BackColor=   16766421
      stylesets(15).HasFont=   -1  'True
      BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(15).Picture=   "frmFormularios.frx":291A
      stylesets(15).AlignmentPicture=   1
      stylesets(16).Name=   "CalculadoPesoGER"
      stylesets(16).BackColor=   16766421
      stylesets(16).Picture=   "frmFormularios.frx":3A7C
      stylesets(16).AlignmentPicture=   1
      stylesets(17).Name=   "ECSumatorioSPA"
      stylesets(17).BackColor=   16766421
      stylesets(17).HasFont=   -1  'True
      BeginProperty stylesets(17).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(17).Picture=   "frmFormularios.frx":406E
      stylesets(17).AlignmentPicture=   1
      stylesets(18).Name=   "CalculadoPesoSPA"
      stylesets(18).BackColor=   16766421
      stylesets(18).HasFont=   -1  'True
      BeginProperty stylesets(18).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(18).Picture=   "frmFormularios.frx":45A0
      stylesets(18).AlignmentPicture=   1
      stylesets(19).Name=   "TituloAmarilloGER"
      stylesets(19).BackColor=   12648447
      stylesets(19).HasFont=   -1  'True
      BeginProperty stylesets(19).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(19).Picture=   "frmFormularios.frx":4A72
      stylesets(19).AlignmentPicture=   1
      stylesets(20).Name=   "PresBajaLog"
      stylesets(20).HasFont=   -1  'True
      BeginProperty stylesets(20).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(20).Picture=   "frmFormularios.frx":4A8E
      stylesets(21).Name=   "Normal"
      stylesets(21).HasFont=   -1  'True
      BeginProperty stylesets(21).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(21).Picture=   "frmFormularios.frx":4AAA
      stylesets(22).Name=   "TituloAmarilloSPA"
      stylesets(22).BackColor=   12648447
      stylesets(22).HasFont=   -1  'True
      BeginProperty stylesets(22).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(22).Picture=   "frmFormularios.frx":4F7C
      stylesets(22).AlignmentPicture=   1
      stylesets(23).Name=   "EnviosCorrectosSumatorioENG"
      stylesets(23).BackColor=   16766421
      stylesets(23).HasFont=   -1  'True
      BeginProperty stylesets(23).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(23).Picture=   "frmFormularios.frx":4F98
      stylesets(23).AlignmentPicture=   1
      stylesets(24).Name=   "TituloDenProcesoGER"
      stylesets(24).HasFont=   -1  'True
      BeginProperty stylesets(24).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(24).Picture=   "frmFormularios.frx":4FB4
      stylesets(24).AlignmentPicture=   1
      stylesets(25).Name=   "PesoENG"
      stylesets(25).BackColor=   16777215
      stylesets(25).HasFont=   -1  'True
      BeginProperty stylesets(25).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(25).Picture=   "frmFormularios.frx":5486
      stylesets(25).AlignmentPicture=   1
      stylesets(26).Name=   "CalculadoPesoFRA"
      stylesets(26).BackColor=   16766421
      stylesets(26).Picture=   "frmFormularios.frx":5B18
      stylesets(26).AlignmentPicture=   1
      stylesets(27).Name=   "TituloGER"
      stylesets(27).HasFont=   -1  'True
      BeginProperty stylesets(27).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(27).Picture=   "frmFormularios.frx":5FEA
      stylesets(27).AlignmentPicture=   1
      stylesets(28).Name=   "Amarillo"
      stylesets(28).BackColor=   12648447
      stylesets(28).HasFont=   -1  'True
      BeginProperty stylesets(28).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(28).Picture=   "frmFormularios.frx":64BC
      stylesets(29).Name=   "TituloDenProcesoPeque�o"
      stylesets(29).HasFont=   -1  'True
      BeginProperty stylesets(29).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(29).Picture=   "frmFormularios.frx":64D8
      stylesets(29).AlignmentPicture=   1
      stylesets(30).Name=   "TituloDenProcesoSPA"
      stylesets(30).HasFont=   -1  'True
      BeginProperty stylesets(30).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(30).Picture=   "frmFormularios.frx":64F4
      stylesets(30).AlignmentPicture=   1
      stylesets(31).Name=   "PiezasDefectuosasSumatorioGER"
      stylesets(31).BackColor=   16766421
      stylesets(31).HasFont=   -1  'True
      BeginProperty stylesets(31).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(31).Picture=   "frmFormularios.frx":69C6
      stylesets(31).AlignmentPicture=   1
      stylesets(32).Name=   "TituloSPA"
      stylesets(32).HasFont=   -1  'True
      BeginProperty stylesets(32).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(32).Picture=   "frmFormularios.frx":69E2
      stylesets(32).AlignmentPicture=   1
      stylesets(33).Name=   "Titulo"
      stylesets(33).HasFont=   -1  'True
      BeginProperty stylesets(33).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(33).Picture=   "frmFormularios.frx":6EB4
      stylesets(33).AlignmentPicture=   1
      stylesets(34).Name=   "PDGER"
      stylesets(34).HasFont=   -1  'True
      BeginProperty stylesets(34).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(34).Picture=   "frmFormularios.frx":6ED0
      stylesets(34).AlignmentPicture=   1
      stylesets(35).Name=   "DenProcesoENG"
      stylesets(35).HasFont=   -1  'True
      BeginProperty stylesets(35).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(35).Picture=   "frmFormularios.frx":6EEC
      stylesets(35).AlignmentPicture=   1
      stylesets(36).Name=   "PiezasDefectuosasSumatorioSPA"
      stylesets(36).BackColor=   16766421
      stylesets(36).HasFont=   -1  'True
      BeginProperty stylesets(36).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(36).Picture=   "frmFormularios.frx":804E
      stylesets(36).AlignmentPicture=   1
      stylesets(37).Name=   "EnviosCorrectosENG"
      stylesets(37).HasFont=   -1  'True
      BeginProperty stylesets(37).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(37).Picture=   "frmFormularios.frx":806A
      stylesets(37).AlignmentPicture=   1
      stylesets(38).Name=   "PDSPA"
      stylesets(38).HasFont=   -1  'True
      BeginProperty stylesets(38).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(38).Picture=   "frmFormularios.frx":8086
      stylesets(38).AlignmentPicture=   1
      stylesets(39).Name=   "TituloPeque�o"
      stylesets(39).BackColor=   16777215
      stylesets(39).HasFont=   -1  'True
      BeginProperty stylesets(39).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(39).Picture=   "frmFormularios.frx":9098
      stylesets(39).AlignmentPicture=   1
      stylesets(40).Name=   "ECENG"
      stylesets(40).HasFont=   -1  'True
      BeginProperty stylesets(40).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(40).Picture=   "frmFormularios.frx":923A
      stylesets(40).AlignmentPicture=   1
      stylesets(41).Name=   "TituloAzulENG"
      stylesets(41).BackColor=   16777130
      stylesets(41).HasFont=   -1  'True
      BeginProperty stylesets(41).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(41).Picture=   "frmFormularios.frx":93DC
      stylesets(41).AlignmentPicture=   1
      stylesets(42).Name=   "CalculadoValor"
      stylesets(42).BackColor=   16766421
      stylesets(42).HasFont=   -1  'True
      BeginProperty stylesets(42).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(42).Picture=   "frmFormularios.frx":93F8
      stylesets(42).AlignmentPicture=   1
      stylesets(43).Name=   "EnviosCorrectosSumatorioGER"
      stylesets(43).BackColor=   16766421
      stylesets(43).HasFont=   -1  'True
      BeginProperty stylesets(43).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(43).Picture=   "frmFormularios.frx":9414
      stylesets(43).AlignmentPicture=   1
      stylesets(44).Name=   "PesoGER"
      stylesets(44).BackColor=   16777215
      stylesets(44).HasFont=   -1  'True
      BeginProperty stylesets(44).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(44).Picture=   "frmFormularios.frx":98E6
      stylesets(44).AlignmentPicture=   1
      stylesets(45).Name=   "PDSumatorioENG"
      stylesets(45).BackColor=   16766421
      stylesets(45).HasFont=   -1  'True
      BeginProperty stylesets(45).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(45).Picture=   "frmFormularios.frx":9ED8
      stylesets(45).AlignmentPicture=   1
      stylesets(46).Name=   "EnviosCorrectosSumatorioSPA"
      stylesets(46).BackColor=   16766421
      stylesets(46).HasFont=   -1  'True
      BeginProperty stylesets(46).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(46).Picture=   "frmFormularios.frx":9EF4
      stylesets(46).AlignmentPicture=   1
      stylesets(47).Name=   "PiezasDefectuosasENG"
      stylesets(47).HasFont=   -1  'True
      BeginProperty stylesets(47).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(47).Picture=   "frmFormularios.frx":9F10
      stylesets(47).AlignmentPicture=   1
      stylesets(48).Name=   "PesoSPA"
      stylesets(48).BackColor=   16777215
      stylesets(48).HasFont=   -1  'True
      BeginProperty stylesets(48).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(48).Picture=   "frmFormularios.frx":A442
      stylesets(48).AlignmentPicture=   1
      stylesets(49).Name=   "TituloAmarillo"
      stylesets(49).BackColor=   12648447
      stylesets(49).HasFont=   -1  'True
      BeginProperty stylesets(49).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(49).Picture=   "frmFormularios.frx":A914
      stylesets(49).AlignmentPicture=   1
      stylesets(50).Name=   "DenProcesoGER"
      stylesets(50).HasFont=   -1  'True
      BeginProperty stylesets(50).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(50).Picture=   "frmFormularios.frx":A930
      stylesets(50).AlignmentPicture=   1
      stylesets(51).Name=   "EnviosCorrectosGER"
      stylesets(51).HasFont=   -1  'True
      BeginProperty stylesets(51).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(51).Picture=   "frmFormularios.frx":AE02
      stylesets(51).AlignmentText=   0
      stylesets(51).AlignmentPicture=   1
      stylesets(52).Name=   "DenProcesoSPA"
      stylesets(52).HasFont=   -1  'True
      BeginProperty stylesets(52).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(52).Picture=   "frmFormularios.frx":AE1E
      stylesets(52).AlignmentPicture=   1
      stylesets(53).Name=   "PesoFRA"
      stylesets(53).BackColor=   16777215
      stylesets(53).Picture=   "frmFormularios.frx":C01C
      stylesets(53).AlignmentPicture=   1
      stylesets(54).Name=   "Azul"
      stylesets(54).BackColor=   16777160
      stylesets(54).HasFont=   -1  'True
      BeginProperty stylesets(54).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(54).Picture=   "frmFormularios.frx":C4EE
      stylesets(55).Name=   "TituloAmarilloPeque�o"
      stylesets(55).BackColor=   12648447
      stylesets(55).HasFont=   -1  'True
      BeginProperty stylesets(55).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(55).Picture=   "frmFormularios.frx":C50A
      stylesets(55).AlignmentPicture=   1
      stylesets(56).Name=   "EnviosCorrectosSPA"
      stylesets(56).HasFont=   -1  'True
      BeginProperty stylesets(56).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(56).Picture=   "frmFormularios.frx":C526
      stylesets(56).AlignmentPicture=   1
      stylesets(57).Name=   "ECGER"
      stylesets(57).HasFont=   -1  'True
      BeginProperty stylesets(57).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(57).Picture=   "frmFormularios.frx":C542
      stylesets(57).AlignmentPicture=   1
      stylesets(58).Name=   "ECSumatorioENG"
      stylesets(58).BackColor=   16766421
      stylesets(58).HasFont=   -1  'True
      BeginProperty stylesets(58).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(58).Picture=   "frmFormularios.frx":C55E
      stylesets(58).AlignmentPicture=   1
      stylesets(59).Name=   "DenProcesoPeque�o"
      stylesets(59).HasFont=   -1  'True
      BeginProperty stylesets(59).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(59).Picture=   "frmFormularios.frx":C57A
      stylesets(59).AlignmentPicture=   1
      stylesets(60).Name=   "CalculadoPesoENG"
      stylesets(60).BackColor=   16766421
      stylesets(60).Picture=   "frmFormularios.frx":C596
      stylesets(60).AlignmentPicture=   1
      DividerType     =   0
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   17
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "ATRIBUTO"
      Columns(1).Name =   "ATRIBUTO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   5001
      Columns(2).Caption=   "VALOR"
      Columns(2).Name =   "VALOR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   1
      Columns(3).Width=   1402
      Columns(3).Caption=   "APROCESO"
      Columns(3).Name =   "APROCESO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(4).Width=   1402
      Columns(4).Caption=   "APEDIDO"
      Columns(4).Name =   "APEDIDO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   1588
      Columns(5).Caption=   "AYUDA"
      Columns(5).Name =   "AYUDA"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      Columns(5).ButtonsAlways=   -1  'True
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "TIPO"
      Columns(6).Name =   "TIPO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "SUBTIPO"
      Columns(7).Name =   "SUBTIPO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "INTRO"
      Columns(8).Name =   "INTRO"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "CAMPO_GS"
      Columns(9).Name =   "CAMPO_GS"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "COD_VALOR"
      Columns(10).Name=   "COD_VALOR"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "BAJALOG"
      Columns(11).Name=   "BAJALOG"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   11
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "Titulo"
      Columns(12).Name=   "Titulo"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "PESO"
      Columns(13).Name=   "PESO"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "GENERICO"
      Columns(14).Name=   "GENERICO"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   11
      Columns(14).FieldLen=   10
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "IDATRIB"
      Columns(15).Name=   "IDATRIB"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "DENPROCESO"
      Columns(16).Name=   "DENPROCESO"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      _ExtentX        =   17806
      _ExtentY        =   9075
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picItems 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   320
      ScaleHeight     =   375
      ScaleWidth      =   2175
      TabIndex        =   22
      Top             =   1200
      Width           =   2175
      Begin VB.CommandButton cmdBajar 
         Enabled         =   0   'False
         Height          =   312
         Left            =   540
         Picture         =   "frmFormularios.frx":CC28
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   432
      End
      Begin VB.CommandButton cmdSubir 
         Enabled         =   0   'False
         Height          =   312
         Left            =   0
         Picture         =   "frmFormularios.frx":CF6A
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   432
      End
      Begin VB.CommandButton cmdAnyaItem 
         Enabled         =   0   'False
         Height          =   312
         Left            =   1080
         Picture         =   "frmFormularios.frx":D2AC
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   0
         Width           =   432
      End
      Begin VB.CommandButton cmdElimItem 
         Enabled         =   0   'False
         Height          =   312
         Left            =   1620
         Picture         =   "frmFormularios.frx":D32E
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   0
         Width           =   432
      End
   End
   Begin MSComctlLib.TabStrip ssTabGrupos 
      Height          =   6135
      Left            =   120
      TabIndex        =   21
      Top             =   720
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   10821
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      Height          =   400
      Left            =   120
      ScaleHeight     =   405
      ScaleWidth      =   10095
      TabIndex        =   20
      Top             =   6940
      Width           =   10095
      Begin VB.CommandButton cmdMoverGrupo 
         Caption         =   "DMover grupo"
         Enabled         =   0   'False
         Height          =   345
         Left            =   4275
         TabIndex        =   14
         Top             =   0
         Width           =   1350
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "DDeshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   8550
         TabIndex        =   17
         Top             =   0
         Width           =   1350
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "DListado"
         Enabled         =   0   'False
         Height          =   345
         Left            =   7125
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   0
         Width           =   1350
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "DRestaurar"
         Enabled         =   0   'False
         Height          =   345
         Left            =   5700
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   0
         Width           =   1350
      End
      Begin VB.CommandButton cmdEliminarGr 
         Caption         =   "DEliminar grupo"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2850
         TabIndex        =   13
         Top             =   0
         Width           =   1350
      End
      Begin VB.CommandButton cmdModifGr 
         Caption         =   "DModificar grupo"
         Enabled         =   0   'False
         Height          =   345
         Left            =   1425
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   0
         Width           =   1350
      End
      Begin VB.CommandButton cmdAnyaGr 
         Caption         =   "DA�adir grupo"
         Enabled         =   0   'False
         Height          =   345
         Left            =   0
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   0
         Width           =   1350
      End
   End
   Begin VB.PictureBox picCabecera 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      ScaleHeight     =   495
      ScaleWidth      =   10320
      TabIndex        =   18
      Top             =   120
      Width           =   10315
      Begin VB.CommandButton cmdAnyaCalculo 
         Enabled         =   0   'False
         Height          =   285
         Left            =   6225
         Picture         =   "frmFormularios.frx":D3C0
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   60
         Width           =   315
      End
      Begin VB.PictureBox picMoneda 
         BorderStyle     =   0  'None
         Height          =   400
         Left            =   8150
         ScaleHeight     =   405
         ScaleWidth      =   2070
         TabIndex        =   28
         Top             =   60
         Width           =   2070
      End
      Begin VB.PictureBox picMultiidioma 
         BorderStyle     =   0  'None
         Height          =   250
         Left            =   6700
         ScaleHeight     =   255
         ScaleWidth      =   1395
         TabIndex        =   23
         Top             =   120
         Width           =   1400
         Begin VB.CheckBox chkMultiidioma 
            Caption         =   "Multiidioma"
            Enabled         =   0   'False
            Height          =   192
            Left            =   0
            TabIndex        =   5
            Top             =   0
            Width           =   1300
         End
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcFormulario 
         Height          =   285
         Left            =   1540
         TabIndex        =   0
         Top             =   60
         Width           =   3440
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8229
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6068
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.CommandButton cmdModifForm 
         Enabled         =   0   'False
         Height          =   285
         Left            =   5830
         Picture         =   "frmFormularios.frx":D417
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   60
         Width           =   315
      End
      Begin VB.CommandButton cmdElimForm 
         Enabled         =   0   'False
         Height          =   285
         Left            =   5435
         Picture         =   "frmFormularios.frx":D4FE
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   60
         Width           =   315
      End
      Begin VB.CommandButton cmdAnyaForm 
         Height          =   285
         Left            =   5040
         Picture         =   "frmFormularios.frx":D590
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   60
         Width           =   315
      End
      Begin VB.Label lblFormulario 
         Caption         =   "Formulario"
         Height          =   192
         Left            =   0
         TabIndex        =   19
         Top             =   120
         Width           =   1392
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
      Height          =   915
      Left            =   0
      TabIndex        =   24
      Top             =   0
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormularios.frx":D612
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5318
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddDestinos 
      Height          =   1260
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   7350
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormularios.frx":D62E
      stylesets(1).Name=   "Espec"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFormularios.frx":D64A
      BevelColorFace  =   12632256
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   1958
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadBackColor=   -2147483633
      Columns(0).BackColor=   16777215
      Columns(1).Width=   5106
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN1"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HeadBackColor=   -2147483633
      Columns(2).Width=   5106
      Columns(2).Caption=   "DEN2"
      Columns(2).Name =   "DEN2"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HeadBackColor=   -2147483633
      Columns(3).Width=   5106
      Columns(3).Caption=   "DEN3"
      Columns(3).Name =   "DEN3"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasHeadBackColor=   -1  'True
      Columns(3).HeadBackColor=   -2147483633
      Columns(4).Width=   3200
      Columns(4).Caption=   "Direcci�n"
      Columns(4).Name =   "DIR"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasHeadBackColor=   -1  'True
      Columns(4).HeadBackColor=   -2147483633
      Columns(5).Width=   2090
      Columns(5).Caption=   "Poblaci�n"
      Columns(5).Name =   "POB"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).HasHeadBackColor=   -1  'True
      Columns(5).HeadBackColor=   -2147483633
      Columns(6).Width=   1138
      Columns(6).Caption=   "CP"
      Columns(6).Name =   "CP"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).HasHeadBackColor=   -1  'True
      Columns(6).HeadBackColor=   -2147483633
      Columns(7).Width=   926
      Columns(7).Caption=   "Pa�s"
      Columns(7).Name =   "PAI"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).HasHeadBackColor=   -1  'True
      Columns(7).HeadBackColor=   -2147483633
      Columns(8).Width=   1402
      Columns(8).Caption=   "Provincia"
      Columns(8).Name =   "PROVI"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).HasHeadBackColor=   -1  'True
      Columns(8).HeadBackColor=   -2147483633
      _ExtentX        =   12965
      _ExtentY        =   2222
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddPagos 
      Height          =   1575
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   2895
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   1085
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4868
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN1"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   4868
      Columns(2).Caption=   "DEN2"
      Columns(2).Name =   "DEN2"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   4868
      Columns(3).Caption=   "DEN3"
      Columns(3).Name =   "DEN3"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown ssdbValorMultiIdi 
      Height          =   915
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormularios.frx":D6C7
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddOtrosCombos 
      Height          =   1575
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   2461
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   5530
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      Columns(2).Width=   5530
      Columns(2).Caption=   "DEN2"
      Columns(2).Name =   "DEN2"
      Columns(2).DataField=   "Column 3"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   5530
      Columns(3).Caption=   "DEN3"
      Columns(3).Name =   "DEN3"
      Columns(3).DataField=   "Column 4"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "GENERICO"
      Columns(4).Name =   "GENERICO"
      Columns(4).DataField=   "Column 2"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   10
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddFecha 
      Height          =   500
      Left            =   0
      TabIndex        =   34
      Top             =   0
      Width           =   3000
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5636
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5292
      _ExtentY        =   882
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddAlmacenes 
      Height          =   1575
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2461
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   3
      Columns(2).Width=   5530
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   50
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddArticulos 
      Height          =   1575
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   2461
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   5530
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "GENERICO"
      Columns(2).Name =   "GENERICO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   11
      Columns(2).FieldLen=   10
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "PREC_ULT_ADJ"
      Columns(3).Name =   "PREC_ULT_ADJ"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "CODPROV_ULT_ADJ"
      Columns(4).Name =   "CODPROV_ULT_ADJ"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "DENPROV_ULT_ADJ"
      Columns(5).Name =   "DENPROV_ULT_ADJ"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "UNI"
      Columns(6).Name =   "UNI"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTiposPedido 
      Height          =   1260
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   7350
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormularios.frx":D6E3
      stylesets(1).Name=   "Espec"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFormularios.frx":D6FF
      BevelColorFace  =   12632256
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   1958
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadBackColor=   -2147483633
      Columns(0).BackColor=   16777215
      Columns(1).Width=   5106
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HeadBackColor=   -2147483633
      Columns(2).Width=   3200
      Columns(2).Caption=   "Concepto"
      Columns(2).Name =   "CONCEPTO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HeadBackColor=   -2147483633
      Columns(3).Width=   3200
      Columns(3).Caption=   "Almac�n"
      Columns(3).Name =   "ALMACEN"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasHeadBackColor=   -1  'True
      Columns(3).HeadBackColor=   -2147483633
      Columns(4).Width=   3200
      Columns(4).Caption=   "Recepci�n"
      Columns(4).Name =   "RECEPCION"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasHeadBackColor=   -1  'True
      Columns(4).HeadBackColor=   -2147483633
      _ExtentX        =   12965
      _ExtentY        =   2222
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddCompradores 
      Height          =   1575
      Left            =   0
      TabIndex        =   32
      Top             =   0
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   5530
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddEmpresas 
      Height          =   1575
      Left            =   0
      TabIndex        =   35
      Top             =   0
      Width           =   4755
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2461
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   3
      Columns(2).Width=   5530
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   50
      _ExtentX        =   8387
      _ExtentY        =   2778
      _StockProps     =   77
   End
End
Attribute VB_Name = "frmFormularios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas:
Public g_oFormularios As CFormularios
Public g_oFormSeleccionado As CFormulario
Public g_oGrupoSeleccionado As CFormGrupo
Public g_Accion As AccionesSummit
Public g_bUpdate As Boolean
Public g_ofrmDesglose As frmDesglose
Public g_ofrmATRIB As frmAtrib
Public g_strPK As String

'Variables privadas:
Private m_blnQuitarValorTablaExterna As Boolean
Private m_strPK_old As String
Private m_oIBaseDatos As IBaseDatos
Private m_bRespetarCombo As Boolean
Private m_bCargarComboDesde As Boolean
Public m_oIdiomas As CIdiomas
Private m_oPagos As CPagos
Private m_oDestinos As CDestinos
Private m_oPaises As CPaises
Private m_oUnidades As CUnidades
Private m_oCampoEnEdicion As CFormItem
Private m_oMonedas As CMonedas
Private m_oMonSeleccionada As CMoneda
Private m_oTiposPedido As CTiposPedido
Private m_bCargando As Boolean
Private m_bHayPres1BajaLog As Boolean
Private m_bHayPres2BajaLog As Boolean
Private m_bHayPres3BajaLog As Boolean
Private m_bHayPres4BajaLog As Boolean
Private m_dblPres1Asig As Double
Private m_dblPres2Asig As Double
Private m_dblPres3Asig As Double
Private m_dblPres4Asig As Double
Private m_bCancel As Boolean

'Variables para idiomas
Private m_sForm As String
Private m_sDatosGen As String
Private m_sGrupo As String
Private m_sDato As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sIdiDesglose As String
Private m_sIdiKB As String
Private m_sIdiTipoFecha(17) As String
Private m_sMensaje(1 To 9) As String
Private m_sIdiErrorEvaluacion(2) As String
Private m_sImposibleAnyadircampo As String
Private m_sNoPuedenExistirDosCampoMoneda As String

Private m_sCamposExterno(1) As String
Private m_intTabla As Integer                           'paso de vbles entre Click y BeforeUpdate
Private m_blnExisteArticulo As Boolean
Private m_blnExisteTablaExternaParaArticulo As Boolean
Private m_oTablaExterna As CTablaExterna            'clase que almacena funciones

Private m_sCamposGS(35) As String

Private m_sCamposSC(10) As String
Private m_sDen As String
Private m_sDenApli As String

'Variables de seguridad
Private m_bModif As Boolean
Private m_bRestrDest As Boolean
Private m_bRestrMat As Boolean
Private m_bRestrProve As Boolean
Private m_bRestrPresUO As Boolean
Private m_bRPerfUON As Boolean

'''' Control de errores
Private m_bModError As Boolean

'''' Carga de variable FECHA_VALOR_CAMPO
Public g_lCampoId As Long
Private m_bConEnviosCorrectos As Boolean
Private m_bConPiezasDefectuosas As Boolean
Private m_bConDesgloseActividad As Boolean
Private m_bConDesgloseFactura As Boolean
Private m_bConDesgloseDePedido As Boolean
Private m_bConCampos As Boolean
Private m_bArt As Boolean

'Para el combo de tipos de pedidos
Dim m_arrConcep(2) As String
Dim m_arrRecep(2) As String
Dim m_arrAlmac(2) As String

Private Sub chkMultiidioma_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim oGrupo As CFormGrupo
    Dim sDen As String
    Dim oMultiidi As CMultiidioma
    Dim i As Integer
    Dim vbm As Variant
    Dim iRow As Integer
    Dim oIdioma As CIdioma

    If m_bCargando = True Then Exit Sub
    
    If chkMultiidioma.Value = vbChecked Then
        g_oFormSeleccionado.Multiidioma = True
    Else
        g_oFormSeleccionado.Multiidioma = False
    End If
    
    udtTeserror = g_oFormSeleccionado.ModificarMultiidioma
    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
        Exit Sub
    Else
        RegistrarAccion ACCFormularioModificar, "Id:" & g_oFormSeleccionado.Id
        
        If g_oFormSeleccionado Is Nothing Then g_oFormSeleccionado.CargarTodosLosGrupos False
                
        ssTabGrupos.Tabs.clear
        i = 1
        For Each oGrupo In g_oFormSeleccionado.Grupos
            sDen = ""
            sDen = sDen & NullToStr(oGrupo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den) & "/"
            For Each oMultiidi In oGrupo.Denominaciones
                If oMultiidi.Cod <> gParametrosInstalacion.gIdioma And Not IsNull(oMultiidi.Den) Then
                    sDen = sDen & oMultiidi.Den & "/"
                End If
            Next
            sDen = Mid(sDen, 1, Len(sDen) - 1)
            
            If g_oFormSeleccionado.Multiidioma = True Then
                ssTabGrupos.Tabs.Add i, "A" & oGrupo.Id, sDen
            Else
                ssTabGrupos.Tabs.Add i, "A" & oGrupo.Id, NullToStr(oGrupo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)  'si no es multiidioma ser� la misma para todos
            End If
            ssTabGrupos.Tabs(i).Tag = CStr(oGrupo.Id)
            i = i + 1
        Next
        'Redimensiona la grid:
        RedimensionarColumnasMultiIdi
        
        'Recorremos las filas del grid para poner el campo TITULO (si lo hay) con el style que le corresponda
        iRow = sdbgCampos.Row
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.AddItemBookmark(i)
            If sdbgCampos.Columns("TITULO").CellValue(vbm) = 1 Then
                sdbgCampos.Row = i
                If sdbgCampos.Columns("TIPO").CellValue(vbm) = TipoCampoPredefinido.CampoGS Then
                    If g_oFormSeleccionado.Multiidioma Then
                        For Each oIdioma In m_oIdiomas
                            sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "TituloAmarilloPeque�o"
                        Next
                    Else
                        For Each oIdioma In m_oIdiomas
                            sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "TituloAmarillo" & oIdioma.Cod
                        Next
                    End If
                Else
                    If g_oFormSeleccionado.Multiidioma Then
                        For Each oIdioma In m_oIdiomas
                            sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "TituloPeque�o"
                        Next
                    Else
                        For Each oIdioma In m_oIdiomas
                            sdbgCampos.Columns(oIdioma.Cod).CellStyleSet "TituloAmarillo" & oIdioma.Cod
                        Next
                    End If
                End If
                sdbgCampos.Row = iRow
                sdbgCampos.Refresh
            End If
        Next
    End If
End Sub

Private Sub cmdAnyaCalculo_Click()
    'Muestra la pantalla de los campos calculados:
    frmFormularioCampoCalculado.g_bModif = m_bModif
    frmFormularioCampoCalculado.g_sOrigen = "frmFormularios"
    frmFormularioCampoCalculado.Show vbModal
End Sub

Private Sub cmdAnyaForm_Click()
    'Comprueba 1� si se ha seleccionado un formulario previamente
    If IsNull(Me.sdbcFormulario.Value) Or (Me.sdbcFormulario.Value = "") Then
        AnyaNuevoFormulario
    Else
        MDI.mnuPOPUPNewCopyForm.Item(0).Visible = True
        MDI.mnuPOPUPNewCopyForm.Item(1).Visible = True
        MDI.mnuPOPUPNewCopyForm.Item(2).Visible = False
        MDI.mnuPOPUPNewCopyForm.Item(3).Visible = False
        MDI.mnuPOPUPNewCopyForm.Item(6).Visible = False
        MDI.mnuPOPUPNewCopyForm.Item(7).Visible = False
        
        MDI.PopupMenu MDI.mnuPOPUPCopiarForm
    End If
End Sub

Private Sub cmdAnyaGr_Click()
    'si hay cambios los guarda
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
        If m_bModError = True Then Exit Sub
    End If
    
    g_Accion = ACCFormGrupoAnyadir
               
    Set frmFormGrupoAnya.g_oIdiomas = m_oIdiomas
    frmFormGrupoAnya.WindowState = vbNormal
    frmFormGrupoAnya.Show vbModal
End Sub

''' Procedimiento que oculta/visualiza las opciones de men� para a�adir campos.
Private Sub cmdAnyaItem_Click()
    Dim ADORs As Ador.Recordset
    Dim oTiposPredef As CTiposSolicit
    Dim lTipos As Integer
    Dim i As Integer
    Dim oArbolPres As cPresConceptos5Nivel0

    'si hay cambios los guarda
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
        If m_bModError = True Then Exit Sub
    End If
    
    MDI.mnuAnyaCampoGS(7).Visible = True 'Muestra el campo de GS de desglose
    MDI.mnuAnyaCampoGS.Item(11).Visible = basParametros.gParametrosGenerales.gbUsarPres1
    MDI.mnuAnyaCampoGS.Item(12).Visible = basParametros.gParametrosGenerales.gbUsarPres2
    MDI.mnuAnyaCampoGS.Item(13).Visible = basParametros.gParametrosGenerales.gbUsarPres3
    MDI.mnuAnyaCampoGS.Item(14).Visible = basParametros.gParametrosGenerales.gbUsarPres4
    MDI.mnuAnyaCampoGS.Item(16).Visible = (gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.SolicitudPM) Or gParametrosIntegracion.gaExportar(EntidadIntegracion.SolicitudPM))
    MDI.mnuAnyaCampoGS.Item(19).Visible = gParametrosGenerales.gbUsarOrgCompras 'Opcion de Menu Organizacion de Compras
    MDI.mnuAnyaCampoGS.Item(20).Visible = gParametrosGenerales.gbUsarOrgCompras 'Opcion de Menu Departamento
    MDI.mnuAnyaCampoGS.Item(22).Visible = True 'Opcion de Menu Importe solicitudes vinculadas
    MDI.mnuAnyaCampoGS.Item(23).Visible = True 'Opcion de Menu Referencia a solicitud
    MDI.mnuAnyaCampoGS.Item(24).Visible = True 'Opcion de Tipo de Pedido
    MDI.mnuAnyaCampoGS.Item(32).Visible = True 'Opcion de Retencion en garantia
    MDI.mnuAnyaCampoGS.Item(33).Visible = True 'Opcion de Unidad de pedido
    MDI.mnuAnyaCampoGS.Item(35).Visible = True 'Opcion de comprador

    MDI.mnuAnyaCampoGS(28).Visible = (gParametrosGenerales.gsAccesoFSIM = AccesoFSIM) ' muestra el campo de GS de Desglose de factura
    MDI.mnuAnyaCampoGS(30).Visible = (gParametrosGenerales.gsAccesoFSIM = AccesoFSIM) ' muestra el campo de GS de Inicio de abono
    MDI.mnuAnyaCampoGS(31).Visible = (gParametrosGenerales.gsAccesoFSIM = AccesoFSIM) ' muestra el campo de GS de Fin de abono
        
    If Not g_oParametrosSM Is Nothing Then
        'Habr� un campo de sistema nuevo por cada �rbol presupuestario que hayamos definido en la secci�n presupuestaria del SM.
        Set oArbolPres = oFSGSRaiz.Generar_CPresConceptos5Nivel0
        Set ADORs = oArbolPres.DevolverArbolesPresupuestarios
           
        'Por si hay arboles presupuestarios, los descarga todos
        Dim cont As Integer
        cont = MDI.mnuAnyaCampoGS.Count
        While cont >= 39
            Unload MDI.mnuAnyaCampoGS(cont)
            cont = cont - 1
        Wend
        If Not ADORs Is Nothing Then
            While Not ADORs.EOF
                Load MDI.mnuAnyaCampoGS(MDI.mnuAnyaCampoGS.Count + 1)
                MDI.mnuAnyaCampoGS(MDI.mnuAnyaCampoGS.Count).caption = NullToStr(ADORs("DEN").Value)
                MDI.mnuAnyaCampoGS(MDI.mnuAnyaCampoGS.Count).Tag = TipoCampoGS.PartidaPresupuestaria & "#" & ADORs("COD").Value
                ADORs.MoveNext
            Wend
            ADORs.Close
            Set ADORs = Nothing
        End If
    End If

    'Dependiendo del acceso se podr� seleccionar el a�adir campos de distintos tipos.Configura el men�:
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
        MDI.mnuAnyaCampo(2).Visible = True
        MDI.mnuAnyaCampo(3).Visible = False
        MDI.mnuAnyaCampo(5).Visible = False
        MDI.PopupMenu MDI.mnuPOPUPAnyaCampo
    End If
    
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSCompleto Or gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.AccesoContrato _
        Or gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM Or gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
        MDI.mnuAnyaCampo(2).Visible = False
        MDI.mnuAnyaCampo(5).Visible = True
        
        'Si s�lo est� activado Facturaci�n, s�lo se muestran mnuAnyaCampo(5), o sea, los campos de sistema
        If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.AccesoFSWSCompleto _
        And gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.AccesoContrato _
        And gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM Then
            MDI.mnuAnyaCampo(3).Visible = False
        Else
            MDI.mnuAnyaCampo(3).Visible = True
            'A�ade los tipos de solicitudes din�micamente al men� (menos la de solicitudes de compras):
            MDI.mnuAnyaCampoPredef(1).Tag = TipoSolicitud.SolicitudCompras
        
            Set oTiposPredef = oFSGSRaiz.Generar_CTiposSolicit
            Set ADORs = oTiposPredef.DevolverTiposSolicitudes(True)
        
            If Not ADORs Is Nothing Then
                lTipos = ADORs.RecordCount
                While Not ADORs.EOF
                    If Not (((gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.SinAcceso Or Not oUsuarioSummit.AccesoFSContratos) And ADORs("TIPO").Value = 5) _
                    Or ((gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso Or Not oUsuarioSummit.AccesoFSIM) And ADORs("TIPO").Value = 7)) Then
                        Load MDI.mnuAnyaCampoPredef(MDI.mnuAnyaCampoPredef.Count + 1)
                        MDI.mnuAnyaCampoPredef(MDI.mnuAnyaCampoPredef.Count).caption = NullToStr(ADORs("DESCR_" & gParametrosInstalacion.gIdioma).Value)
                        MDI.mnuAnyaCampoPredef(MDI.mnuAnyaCampoPredef.Count).Tag = ADORs("ID").Value
                    End If
                    ADORs.MoveNext
                Wend
                ADORs.Close
                Set ADORs = Nothing
            End If
        End If
        
        Dim ador1 As Ador.Recordset

        MDI.mnuAnyaCampo(6).Visible = True 'Campos Externos
        MDI.mnuAnyaCampoExterno(1).Visible = True 'Carga bajo demanda
        Dim oServicio As CServicio
        Set oServicio = oFSGSRaiz.Generar_CServicio
        If oServicio.TieneResultados() Then
            Set ador1 = oServicio.DenominacionesIdioma(basParametros.gParametrosGenerales.gIdioma)
            If Not ador1 Is Nothing Then
                ador1.MoveFirst
                'Por si hay Submenu cargados, los descarga a todos menos la opci�n Nuevo
                For i = 1 To MDI.mnuAnyaCampoExternoDemanda.Count - 1
                    Unload MDI.mnuAnyaCampoExternoDemanda(MDI.mnuAnyaCampoExternoDemanda.Count)
                Next
                Do While Not ador1.EOF
                    Load MDI.mnuAnyaCampoExternoDemanda(MDI.mnuAnyaCampoExternoDemanda.Count + 1)
                    MDI.mnuAnyaCampoExternoDemanda(MDI.mnuAnyaCampoExternoDemanda.Count).caption = NullToStr(ador1.Fields(1).Value)  'denominacion
                    MDI.mnuAnyaCampoExternoDemanda(MDI.mnuAnyaCampoExternoDemanda.Count).Tag = ador1.Fields(0).Value               'id
                    ador1.MoveNext
                Loop
                ador1.Close
                Set ador1 = Nothing
            End If
        End If
        If m_oTablaExterna.TieneResultados() Then
            MDI.mnuAnyaCampoExterno(2).Visible = True 'Carga peri�dica
            Set ador1 = m_oTablaExterna.Denominaciones()
            If Not ador1 Is Nothing Then
                ador1.MoveFirst
                'Por si hay Submenu cargados, los descarga a todos
                For i = 1 To MDI.mnuAnyaCampoExternoPeriodico.Count - 1
                    Unload MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count)
                Next
                Do While Not ador1.EOF
                    MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count).caption = NullToStr(ador1.Fields(1).Value)   'denominacion
                    MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count).Tag = ador1.Fields(0).Value                  'id
                    Load MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count + 1)
                    ador1.MoveNext
                Loop
                Unload MDI.mnuAnyaCampoExternoPeriodico(MDI.mnuAnyaCampoExternoPeriodico.Count)
                ador1.Close
                Set ador1 = Nothing
            End If
        Else
            MDI.mnuAnyaCampoExterno(2).Visible = False
        End If
        
        'Opcion menu Desglose de actividad
        MDI.mnuAnyaCampoGS(27).Visible = (gParametrosGenerales.gbAccesoFSGA)
        MDI.PopupMenu MDI.mnuPOPUPAnyaCampo
        
        'borra los tipos de solicitudes del men� (menos la de solicitudes de compras):
        For i = 1 To lTipos
            If Not MDI.mnuAnyaCampoPredef(MDI.mnuAnyaCampoPredef.Count).Tag = TipoSolicitud.SolicitudCompras Then
                Unload MDI.mnuAnyaCampoPredef(MDI.mnuAnyaCampoPredef.Count)
            End If
        Next i
    End If
End Sub

Private Sub cmdBajar_Click()
    Dim i As Integer
    Dim arrValoresMat() As Variant
    Dim arrValoresArt() As Variant
    Dim arrValoresDen() As Variant
    Dim arrValoresAux() As Variant
    Dim bOrdenoMat As Boolean
    Dim bOrdenoPais As Boolean
    Dim bOrdenoArt As Boolean
    Dim bOrdenoProvi As Boolean
    Dim bOrdenoCampoMat As Boolean
    Dim bOrdenoCampoPais As Boolean
    Dim bOrdenoDen As Boolean
    Dim bOrdeno3 As Boolean
    Dim bOrdeno4 As Boolean
    Dim bOrdeno5 As Boolean
    Dim arrValoresPrecAdj() As Variant
    Dim arrValoresProvAdj() As Variant
    Dim arrValoresCantAdj() As Variant
    Dim bOrdeno2Art As Boolean
    Dim bOrdeno3Art As Boolean
    Dim bOrdeno4Art As Boolean
    Dim bOrdeno1PrecAdj As Boolean
    Dim bOrdeno2PrecAdj As Boolean
    Dim bOrdeno3PrecAdj As Boolean
    Dim bOrdeno1ProvAdj As Boolean
    Dim bOrdeno2ProvAdj As Boolean
    Dim bOrdeno1ProvPrecAdj As Boolean
    Dim bOrdeno2ProvPrecAdj As Boolean
    Dim bOrdeno1CantAdj As Boolean
    Dim bOrdeno1CantProvAdj As Boolean
    Dim bOrdeno1CantPrecAdj As Boolean
    Dim bOrdeno1CantProvPrecAdj As Boolean
    Dim bOrdenoCampoPrecProvCantAdj As Boolean
    Dim bOrdenoCampoPrecProvAdj As Boolean
    Dim bOrdenoCampoPrecCantAdj As Boolean
    Dim bOrdenoCampoPrecAdj As Boolean
    Dim bOrdenoCampoProvCantAdj As Boolean
    Dim bOrdenoCampoProvAdj As Boolean
    Dim bOrdenoCampoCantAdj As Boolean
    Dim vbm As Variant
    
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
        If m_bModError = True Then Exit Sub
    End If
    
    If sdbgCampos.SelBookmarks.Count = 0 Then Exit Sub
    If sdbgCampos.AddItemRowIndex(sdbgCampos.SelBookmarks.Item(0)) = sdbgCampos.Rows - 1 Then Exit Sub
    
    g_bUpdate = False
    
    ReDim arrValoresMat(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresArt(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresDen(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresAux(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresPrecAdj(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresProvAdj(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresCantAdj(sdbgCampos.Columns.Count - 1)
    
    'Ordenacion del Material // Provincia
    If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.Provincia) Or _
        (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And ((sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.CodArticulo) Or (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.NuevoCodArticulo))) Then
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.DenArticulo) Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                bOrdenoMat = True
            ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoSC.PrecioUnitarioAdj) Then
                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoSC.ProveedorAdj Then
                    If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(4)) = TipoCampoSC.CantidadAdj Then
                        If val(sdbgCampos.Row) = sdbgCampos.Rows - 5 Then Exit Sub
                        bOrdeno5 = True
                    Else
                        If val(sdbgCampos.Row) = sdbgCampos.Rows - 4 Then Exit Sub
                        bOrdeno4 = True
                    End If
                ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoSC.CantidadAdj Then
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 4 Then Exit Sub
                    bOrdeno4 = True
                Else
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                    bOrdeno3 = True
                End If
            ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoSC.ProveedorAdj) Then
                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoSC.CantidadAdj Then
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 4 Then Exit Sub
                    bOrdeno4 = True
                Else
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                    bOrdeno3 = True
                End If
            ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoSC.CantidadAdj) Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                bOrdeno3 = True
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdenoPais = True
            End If
                
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).Value
            Next i
    'Ordenacion del articulo // Provincia
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Provincia Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
        vbm = sdbgCampos.GetBookmark(-1)
        If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Provincia And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais) Or _
            (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material) Or _
           (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material) Then
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Provincia Or sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) <> TipoCampoGS.DenArticulo Then
                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoSC.PrecioUnitarioAdj Then
                    If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoSC.ProveedorAdj Then
                        If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoSC.CantidadAdj Then
                            If val(sdbgCampos.Row) = sdbgCampos.Rows - 4 Then Exit Sub
                            bOrdeno4Art = True
                        Else
                            If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                            bOrdeno3Art = True
                        End If
                    ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoSC.CantidadAdj Then
                        If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                        bOrdeno3Art = True
                    Else
                        If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                        bOrdeno2Art = True
                    End If
                ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoSC.ProveedorAdj Then
                    If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoSC.CantidadAdj Then
                        If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                        bOrdeno3Art = True
                    Else
                        If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                        bOrdeno2Art = True
                    End If
                ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoSC.CantidadAdj Then
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                    bOrdeno2Art = True
                Else
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
                    bOrdenoProvi = True
                End If
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdenoArt = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    'Ordenacion de la denominacion
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
        If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.NuevoCodArticulo Then
            If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
            bOrdenoDen = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresDen(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    'Ordenacion de Precio Unitario Adjudicado
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.PrecioUnitarioAdj) Then
        If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.CodArticulo Then
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoSC.ProveedorAdj Then
                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoSC.CantidadAdj Then
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                    bOrdeno3PrecAdj = True
                Else
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                    bOrdeno2PrecAdj = True
                End If
            ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoSC.CantidadAdj Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdeno2PrecAdj = True
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
                bOrdeno1PrecAdj = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresPrecAdj(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    'Ordenacion de Proveedor Adjudicado
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.ProveedorAdj) Then
        If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.CodArticulo Then
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoSC.CantidadAdj Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdeno2ProvAdj = True
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
                bOrdeno1ProvAdj = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresProvAdj(i) = sdbgCampos.Columns(i).Value
            Next i
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoSC.PrecioUnitarioAdj Then
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.CodArticulo Then
                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoSC.CantidadAdj Then
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                    bOrdeno2ProvPrecAdj = True
                Else
                    If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
                    bOrdeno1ProvPrecAdj = True
                End If
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-3))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresPrecAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresProvAdj(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    'Ordenacion de Cantidad Adjudicada
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.CantidadAdj) Then
        If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.CodArticulo Then
            If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
            bOrdeno1CantAdj = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresCantAdj(i) = sdbgCampos.Columns(i).Value
            Next i
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoSC.ProveedorAdj And _
            sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.CodArticulo Then
            If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
            bOrdeno1CantProvAdj = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-3))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresProvAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresCantAdj(i) = sdbgCampos.Columns(i).Value
            Next i
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoSC.PrecioUnitarioAdj And _
            sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.CodArticulo Then
            If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
            bOrdeno1CantPrecAdj = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-3))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresPrecAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresCantAdj(i) = sdbgCampos.Columns(i).Value
            Next i
        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoSC.ProveedorAdj And _
            sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoSC.PrecioUnitarioAdj And _
            sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-3)) = TipoCampoGS.CodArticulo Then
            If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
            bOrdeno1CantProvPrecAdj = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-4))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-3))
                arrValoresPrecAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresProvAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresCantAdj(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    Else
        vbm = sdbgCampos.GetBookmark(1)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.CodArticulo Then
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoSC.PrecioUnitarioAdj Then
                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(4)) = TipoCampoSC.ProveedorAdj Then
                    If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(5)) = TipoCampoSC.CantidadAdj Then
                        bOrdenoCampoPrecProvCantAdj = True
                        For i = 0 To sdbgCampos.Columns.Count - 1
                            arrValoresAux(i) = sdbgCampos.Columns(i).Value
                            arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                            arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                            arrValoresPrecAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
                            arrValoresProvAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(4))
                            arrValoresCantAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(5))
                        Next i
                    Else
                        bOrdenoCampoPrecProvAdj = True
                        For i = 0 To sdbgCampos.Columns.Count - 1
                            arrValoresAux(i) = sdbgCampos.Columns(i).Value
                            arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                            arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                            arrValoresPrecAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
                            arrValoresProvAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(4))
                        Next i
                    End If
                ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(4)) = TipoCampoSC.CantidadAdj Then
                    bOrdenoCampoPrecCantAdj = True
                    For i = 0 To sdbgCampos.Columns.Count - 1
                        arrValoresAux(i) = sdbgCampos.Columns(i).Value
                        arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                        arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                        arrValoresPrecAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
                        arrValoresCantAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(4))
                    Next i
                Else
                    bOrdenoCampoPrecAdj = True
                    For i = 0 To sdbgCampos.Columns.Count - 1
                        arrValoresAux(i) = sdbgCampos.Columns(i).Value
                        arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                        arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                        arrValoresPrecAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
                    Next i
                End If
            ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoSC.ProveedorAdj Then
                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(4)) = TipoCampoSC.CantidadAdj Then
                    bOrdenoCampoProvCantAdj = True
                    For i = 0 To sdbgCampos.Columns.Count - 1
                        arrValoresAux(i) = sdbgCampos.Columns(i).Value
                        arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                        arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                        arrValoresProvAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
                        arrValoresCantAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(4))
                    Next i
                Else
                    bOrdenoCampoProvAdj = True
                    For i = 0 To sdbgCampos.Columns.Count - 1
                        arrValoresAux(i) = sdbgCampos.Columns(i).Value
                        arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                        arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                        arrValoresProvAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
                    Next i
                End If
            ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoSC.CantidadAdj Then
                bOrdenoCampoCantAdj = True
                For i = 0 To sdbgCampos.Columns.Count - 1
                    arrValoresAux(i) = sdbgCampos.Columns(i).Value
                    arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                    arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                    arrValoresCantAdj(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
                Next i
            End If
        ElseIf (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.Provincia) Or _
            (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.NuevoCodArticulo) Or _
           (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.CodArticulo) Then
            'Por si debajo del campo a bajar, esta el Material // Provincia
            If (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoGS.DenArticulo) Then
                bOrdenoCampoMat = True
            Else
                bOrdenoCampoPais = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
            Next i
        End If
    End If
    
'Ordenaci�n de material
    If bOrdenoMat Or bOrdenoPais Then
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)  'pongo el Material//pa�s en la fila siguiente
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)  'pongo la Articulo//provincia en la fila siguiente
        Next i
        
        If bOrdenoMat Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'pongo la provincia en la fila siguiente
            Next i
        sdbgCampos.MovePrevious
        End If
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If bOrdenoMat Then
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresAux(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
        End If
        
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext

    ElseIf bOrdeno3 Then
        'Ordeno conjuntamente los campos de material, art�culo y (prec. adj., prov. adj., � cant. adj. (la que est� despu�s de art�culo))
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)  'pongo el Material
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresPrecAdj(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)  'pongo el Articulo
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)  'pongo el prec. adj., prov. adj., � cant. adj.)
        Next i
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)  'pongo la 4� fila donde antes estaba el material.
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
    ElseIf bOrdeno4 Then
        'Ordeno conjuntamente los campos de material, art�culo y (prec. adj. y prov. adj., prec. adj. y  cant. adj., � prov. adj. y cant. adj. (los 2 que est� despu�s de art�culo))
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)  'pongo el Material
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresPrecAdj(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)  'pongo el Articulo
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresProvAdj(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)  'pongo el prec. adj. � prov. adj.
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)  'pongo el prov. adj. � cant. adj.
        Next i
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)  'pongo la 5� fila donde antes estaba el material.
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
    ElseIf bOrdeno5 Then
        'Ordeno conjuntamente los campos de material, art�culo, prec. adj., prov. adj., cant. adj.
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)  'pongo el Material
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresPrecAdj(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)  'pongo el Articulo
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresProvAdj(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)  'pongo el prec. adj.
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresCantAdj(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)  'pongo el prov. adj.
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresCantAdj(i)  'pongo el cant. adj.
        Next i
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)  'pongo la 5� fila donde antes estaba el material.
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        
'Ordenaci�n de art�culo
    ElseIf bOrdenoArt Or bOrdenoProvi Or bOrdeno2Art Or bOrdeno3Art Or bOrdeno4Art Then
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)  'donde est� la provincia pongo el pa�s
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdeno2Art Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            sdbgCampos.MovePrevious
        End If
        If bOrdeno3Art Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresPrecAdj(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)
            Next i
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        If bOrdeno4Art Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresPrecAdj(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresProvAdj(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)
            Next i
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
            Next i
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        If bOrdenoArt Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            sdbgCampos.MovePrevious
        End If
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If bOrdenoArt Or bOrdeno2Art Or bOrdeno3Art Or bOrdeno4Art Then
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresAux(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
        End If
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
    
'Ordenaci�n de denominaci�n
    ElseIf bOrdenoDen = True Then
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresDen(i)
        Next i
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext

'Ordenaci�n de precio unitario adjudicado
    ElseIf bOrdeno1PrecAdj Or bOrdeno2PrecAdj Or bOrdeno3PrecAdj Then
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdeno1PrecAdj Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)
            Next i
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        If bOrdeno2PrecAdj Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresProvAdj(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)
            Next i
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
            Next i
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        If bOrdeno3PrecAdj Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresProvAdj(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)
            Next i
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresCantAdj(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
            Next i
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresCantAdj(i)
            Next i
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext

'Ordenaci�n de proveedor adjudicado
    ElseIf bOrdeno1ProvAdj Or bOrdeno2ProvAdj Then
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdeno1ProvAdj Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
            Next i
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        If bOrdeno2ProvAdj Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresCantAdj(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
            Next i
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresCantAdj(i)
            Next i
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        
    ElseIf bOrdeno1ProvPrecAdj Or bOrdeno2ProvPrecAdj Then
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)
        Next i
        If bOrdeno1ProvPrecAdj Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
            Next i
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        If bOrdeno2ProvPrecAdj Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresCantAdj(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
            Next i
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresCantAdj(i)
            Next i
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        
'Ordenaci�n de cantidad adjudicada
    ElseIf bOrdeno1CantAdj Then
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresCantAdj(i)
        Next i
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext

    ElseIf bOrdeno1CantProvAdj Or bOrdeno1CantPrecAdj Then
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            If bOrdeno1CantProvAdj Then
                sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
            Else
                sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)
            End If
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresCantAdj(i)
        Next i
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        
    ElseIf bOrdeno1CantProvPrecAdj Then
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresCantAdj(i)
        Next i
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext

'Ordenaci�n de un campo cualquiera
    ElseIf bOrdenoCampoMat Or bOrdenoCampoPais Or bOrdenoCampoPrecProvCantAdj Or bOrdenoCampoPrecProvAdj Or bOrdenoCampoPrecCantAdj Or bOrdenoCampoPrecAdj Or bOrdenoCampoProvCantAdj Or bOrdenoCampoProvAdj Or bOrdenoCampoCantAdj Then
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        If bOrdenoCampoMat Or bOrdenoCampoPrecProvCantAdj Or bOrdenoCampoPrecProvAdj Or bOrdenoCampoPrecCantAdj Or bOrdenoCampoPrecAdj Or bOrdenoCampoProvCantAdj Or bOrdenoCampoProvAdj Or bOrdenoCampoCantAdj Then
            sdbgCampos.MoveNext
        End If
        If bOrdenoCampoPrecProvCantAdj Or bOrdenoCampoPrecProvAdj Or bOrdenoCampoPrecCantAdj Or bOrdenoCampoProvCantAdj Then
            sdbgCampos.MoveNext
        End If
        If bOrdenoCampoPrecProvCantAdj Then
            sdbgCampos.MoveNext
        End If
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i

        If bOrdenoCampoCantAdj Or bOrdenoCampoPrecProvCantAdj Or bOrdenoCampoPrecProvCantAdj Or bOrdenoCampoPrecCantAdj Or bOrdenoCampoProvCantAdj Then
            sdbgCampos.MovePrevious
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresCantAdj(i)
            Next i
        End If
        If bOrdenoCampoProvAdj Or bOrdenoCampoPrecProvCantAdj Or bOrdenoCampoPrecProvAdj Or bOrdenoCampoProvCantAdj Then
            sdbgCampos.MovePrevious
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresProvAdj(i)
            Next i
        End If
        If bOrdenoCampoPrecProvCantAdj Or bOrdenoCampoPrecProvAdj Or bOrdenoCampoPrecCantAdj Or bOrdenoCampoPrecAdj Then
            sdbgCampos.MovePrevious
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresPrecAdj(i)
            Next i
        End If
        If bOrdenoCampoMat Then
            sdbgCampos.MovePrevious
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
        End If
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        If bOrdenoCampoMat Or bOrdenoCampoPrecProvCantAdj Or bOrdenoCampoPrecProvAdj Or bOrdenoCampoPrecCantAdj Or bOrdenoCampoPrecAdj Or bOrdenoCampoProvCantAdj Or bOrdenoCampoProvAdj Or bOrdenoCampoCantAdj Then
            sdbgCampos.MoveNext
        End If
        If bOrdenoCampoPrecProvCantAdj Or bOrdenoCampoPrecProvAdj Or bOrdenoCampoPrecCantAdj Or bOrdenoCampoProvCantAdj Then
            sdbgCampos.MoveNext
        End If
        If bOrdenoCampoPrecProvCantAdj Then
            sdbgCampos.MoveNext
        End If
        sdbgCampos.SelBookmarks.RemoveAll
    Else  'Ordeno normal,de 1 en 1
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
        Next i
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MoveNext
    End If
    
    sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark
    g_bUpdate = True
    
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos
    
     Dim sCodOrgCompras As String
    Dim sCodCentro As String
    Dim lIdAlmacen As Long
    Dim oAlmacenes As CAlmacenes
    Dim oCentros As CCentros
    
    'Comprobacion de los valores ORGANIZACION DE COMPRAS, CENTROS Y ALMACEN
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.Centro And sdbgCampos.Columns("VALOR").CellValue(sdbgCampos.GetBookmark(1)) <> "" _
    Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Centro And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.OrganizacionCompras Then
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.Centro And sdbgCampos.Columns("VALOR").CellValue(sdbgCampos.GetBookmark(1)) <> "" Then
            sCodOrgCompras = sdbgCampos.Columns("COD_VALOR").Value
            sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(1))
        Else
            sCodOrgCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
            sCodCentro = sdbgCampos.Columns("COD_VALOR").Value
        End If
        If sCodCentro = "" Then
            sCodCentro = "-1"
        End If
        Set oCentros = oFSGSRaiz.Generar_CCentros
        oCentros.CargarTodosLosCentros sCodCentro, sCodOrgCompras
        If oCentros.Count = 0 Then
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.Centro And sdbgCampos.Columns("VALOR").CellValue(sdbgCampos.GetBookmark(1)) <> "" Then
                sdbgCampos.MoveNext
            End If
            sdbgCampos.Columns("COD_VALOR").Value = ""
            sdbgCampos.Columns("VALOR").Value = ""
            g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = ""
            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.Almacen And sdbgCampos.Columns("VALOR").CellValue(sdbgCampos.GetBookmark(1)) <> "" Then
                sdbgCampos.MoveNext
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
                g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = Null
                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                sdbgCampos.MovePrevious
            End If
            sdbgCampos.Update
        Else
            'Eliminar ALMACEN
            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.Almacen And sdbgCampos.Columns("VALOR").CellValue(sdbgCampos.GetBookmark(1)) <> "" Then
                lIdAlmacen = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(1))
                Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
                oAlmacenes.CargarTodosLosAlmacenes , sCodCentro, , , lIdAlmacen
                If oAlmacenes.Count = 0 Then
                    sdbgCampos.MoveNext
                    sdbgCampos.Columns("COD_VALOR").Value = ""
                    sdbgCampos.Columns("VALOR").Value = ""
                    g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = Null
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                    sdbgCampos.Update
                    sdbgCampos.MovePrevious
                End If

            End If
        End If
    
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Almacen And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.Centro And sdbgCampos.Columns("VALOR").Value <> "" _
    Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Centro And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.Almacen Then
        Dim sw As Boolean
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Almacen And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.Centro Then
            lIdAlmacen = sdbgCampos.Columns("COD_VALOR").Value
            sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
        Else
            lIdAlmacen = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(1))
            sCodCentro = sdbgCampos.Columns("COD_VALOR").Value
        End If
        Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
        oAlmacenes.CargarTodosLosAlmacenes , sCodCentro, , , lIdAlmacen
        If oAlmacenes.Count = 0 Then
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Centro And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.Almacen Then
                sdbgCampos.MoveNext
                sw = True
            End If
            sdbgCampos.Columns("COD_VALOR").Value = ""
            sdbgCampos.Columns("VALOR").Value = ""
            g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = Null
            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
            sdbgCampos.Update
            If sw Then
                sdbgCampos.MovePrevious
            End If
        End If
    
    Else
        'Si movemos un campo culquiera y ponemos juntos el Centro y Almacen
        If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.Centro And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.Almacen And sdbgCampos.Columns("VALOR").CellValue(sdbgCampos.GetBookmark(-1)) <> "" Then
        '''''''''''''''''''''''''''''
            sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-2))
            lIdAlmacen = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
            Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
            oAlmacenes.CargarTodosLosAlmacenes , sCodCentro, , , lIdAlmacen
            If oAlmacenes.Count = 0 Then
                sdbgCampos.MovePrevious
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
                g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = Null
                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                sdbgCampos.MoveNext
                
            End If
            sdbgCampos.Update

        ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.Centro And sdbgCampos.Columns("VALOR").CellValue(sdbgCampos.GetBookmark(-1)) <> "" Then
            sCodOrgCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-2))
            sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
            Set oCentros = oFSGSRaiz.Generar_CCentros
            oCentros.CargarTodosLosCentros sCodCentro, sCodOrgCompras
        
            If oCentros.Count = 0 Then
                'Eliminar CENTRO
                sdbgCampos.MovePrevious
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
                g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = ""
                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                sdbgCampos.MoveNext
                sdbgCampos.Update
            Else
                'Eliminar ALMACEN
                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoGS.Almacen And sdbgCampos.Columns("VALOR").CellValue(sdbgCampos.GetBookmark(3)) <> "" Then
                    lIdAlmacen = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(3))
                    Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
                    oAlmacenes.CargarTodosLosAlmacenes , sCodCentro, , , lIdAlmacen
                    If oAlmacenes.Count = 0 Then
                        sdbgCampos.MoveNext
                        sdbgCampos.Columns("COD_VALOR").Value = ""
                        sdbgCampos.Columns("VALOR").Value = ""
                        g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorNum = Null
                        Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                        sdbgCampos.Update
                        sdbgCampos.MovePrevious
                    End If
                End If
            End If
        End If
    End If
    
    Set oAlmacenes = Nothing
    Set oCentros = Nothing
End Sub

Private Sub cmdDeshacer_Click()
    sdbgCampos.CancelUpdate
    sdbgCampos.DataChanged = False

    If Not m_oCampoEnEdicion Is Nothing Then Set m_oCampoEnEdicion = Nothing
    cmdDeshacer.Enabled = False
    m_bModError = False
    g_Accion = ACCFormularioCons
    If Me.Enabled Then sdbgCampos.SetFocus
End Sub

Private Sub cmdElimForm_Click()
Dim udtTeserror As TipoErrorSummit
Dim i As Integer
Dim Ador As ADODB.Recordset
Dim sMensaje As String

On Error GoTo Cancelar:
    
    g_Accion = ACCFormularioEliminar
    
    i = oMensajes.PreguntaEliminar(m_sForm & " " & g_oFormSeleccionado.Den)
    If i = vbNo Then
        g_Accion = ACCFormularioCons
        Exit Sub
    End If
    
    'Comprueba si el formulario est� asignado a alg�n tipo de solicitud. Si es as� no se podr� borrar:
    sMensaje = ""
    Set Ador = g_oFormSeleccionado.DevolverSolicitudesAsociadas
    If Not Ador.EOF Then
        While Not Ador.EOF
            sMensaje = sMensaje & vbCrLf & Ador.Fields("COD").Value & "-" & Ador.Fields("DEN_" & gParametrosInstalacion.gIdioma).Value
            Ador.MoveNext
        Wend
        Ador.Close
    End If
    Set Ador = Nothing
    
    If sMensaje <> "" Then
        oMensajes.ImposibleEliminarForm sMensaje
        Exit Sub
    End If
    
    'Elimina el formulario de BD:
    Set m_oIBaseDatos = g_oFormSeleccionado
    
    udtTeserror = m_oIBaseDatos.EliminarDeBaseDatos
    If udtTeserror.NumError <> TESnoerror Then
        basErrores.TratarError udtTeserror
    Else
        basSeguridad.RegistrarAccion AccionesSummit.ACCFormularioEliminar, g_oFormSeleccionado.Id & "-" & g_oFormSeleccionado.Den
        g_oFormularios.Remove (CStr(g_oFormSeleccionado.Id))
        sdbcFormulario.Text = ""
        sdbcFormulario.Value = ""
    End If
    
    Set m_oIBaseDatos = Nothing
    g_Accion = ACCFormularioCons
    Exit Sub
    
Cancelar:
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub cmdEliminarGr_Click()
Dim udtTeserror As TipoErrorSummit
Dim i As Integer
Dim oGrupo As CFormGrupo

On Error GoTo Cancelar:
    
    'si hay cambios los guarda
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
        If m_bModError = True Then Exit Sub
    End If
    
    g_Accion = ACCFormGrupoEliminar
    
    i = oMensajes.PreguntaEliminar(m_sGrupo & " " & ssTabGrupos.selectedItem.caption)
    If i = vbNo Then
        g_Accion = ACCFormularioCons
        Exit Sub
    End If
    
    Set m_oIBaseDatos = g_oGrupoSeleccionado
    
    udtTeserror = m_oIBaseDatos.EliminarDeBaseDatos
    If udtTeserror.NumError <> TESnoerror Then
        basErrores.TratarError udtTeserror
    Else
        basSeguridad.RegistrarAccion AccionesSummit.ACCFormGrupoEliminar, g_oGrupoSeleccionado.Id & "-" & ssTabGrupos.selectedItem.caption
        
        'modifica los �rdenes de los grupos
        For Each oGrupo In g_oFormSeleccionado.Grupos
            If oGrupo.Id <> g_oGrupoSeleccionado.Id Then
                If oGrupo.Orden > g_oGrupoSeleccionado.Orden Then
                    oGrupo.Orden = oGrupo.Orden - 1
                End If
            End If
        Next
        
        g_oFormSeleccionado.Grupos.Remove (CStr(g_oGrupoSeleccionado.Id))
        ssTabGrupos.Tabs.Remove (ssTabGrupos.selectedItem.Index)
        ssTabGrupos.Tabs(1).Selected = True
    End If
    
    Set m_oIBaseDatos = Nothing
    g_Accion = ACCFormularioCons
    Exit Sub
    
Cancelar:
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub cmdElimItem_Click()
Dim udtTeserror As TipoErrorSummit
Dim iResp As Integer
Dim vbm As Variant
Dim bPaisMat As Boolean
Dim bArtDen As Boolean
Dim bPaisProvi As Boolean
Dim bQaImpPie As Boolean
Dim bQaUno As Boolean
Dim bHayFechaImputacionDefecto As Boolean
Dim iTipo As Integer
Dim bm As Variant
Dim b3Adj As Boolean
Dim b4Adj As Boolean
Dim i As Integer
Dim j As Integer
Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim vbm2 As Variant
Dim vbm3 As Variant
Dim vbm4 As Variant
Dim bConEnviosCorrectos As Boolean
Dim bExiste As Boolean
Dim sLista As String

On Error GoTo Cancelar:

With sdbgCampos
    'si hay cambios los guarda
    If .DataChanged = True Then
        .Update
        If m_bModError = True Then Exit Sub
    End If
    
    If .Rows = 0 Then Exit Sub
    If .SelBookmarks.Count = 0 Then .SelBookmarks.Add .Bookmark
    
    g_Accion = ACCFormItemEliminar
    
    'Hasta ahora esto de todo en lineas seguidas se usaba para codigo de articulo viejo. Ya no se este codigo. Se deja
    vbm = .GetBookmark(1)
    vbm2 = .GetBookmark(2)
    vbm3 = .GetBookmark(3)
    vbm4 = .GetBookmark(4)

    'Para los campos Qa relacionados con Codigo de art�culo nuevo, hay q recorrer el grid.
    If .Columns("CAMPO_GS").Value = TipoCampoGS.Pais And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Provincia Then
        bArtDen = True
        bPaisProvi = True
        iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.Pais)
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.material And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo Then
        If .Columns("CAMPO_GS").CellValue(vbm2) = TipoCampoSC.PrecioUnitarioAdj Then
            If .Columns("CAMPO_GS").CellValue(vbm3) = TipoCampoSC.ProveedorAdj Then
                If .Columns("CAMPO_GS").CellValue(vbm4) = TipoCampoSC.CantidadAdj Then
                    b4Adj = True
                Else
                    b3Adj = True
                End If
            ElseIf .Columns("CAMPO_GS").CellValue(vbm3) = TipoCampoSC.CantidadAdj Then
                b3Adj = True
            Else
                bPaisMat = True
            End If
        ElseIf .Columns("CAMPO_GS").CellValue(vbm2) = TipoCampoSC.ProveedorAdj Then
            If .Columns("CAMPO_GS").CellValue(vbm3) = TipoCampoSC.CantidadAdj Then
                b3Adj = True
            Else
                bPaisMat = True
            End If
        ElseIf .Columns("CAMPO_GS").CellValue(vbm2) = TipoCampoSC.CantidadAdj Then
            bPaisMat = True
        Else
            bArtDen = True
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.material)
        End If
        bConEnviosCorrectos = m_bConEnviosCorrectos
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.material And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo Then
        bQaImpPie = False
        bQaUno = False
        bConEnviosCorrectos = m_bConEnviosCorrectos
        bPaisMat = True
        bHayFechaImputacionDefecto = False
        
        If HayCamposQARelacionados(bQaImpPie, bQaUno, iTipo, bHayFechaImputacionDefecto) Then
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.material, iTipo)
        Else
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.material)
        End If
        
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.DenArticulo Then
        bQaImpPie = False
        bQaUno = False
        bConEnviosCorrectos = m_bConEnviosCorrectos
        bArtDen = True
        bHayFechaImputacionDefecto = False
        
        If HayCamposQARelacionados(bQaImpPie, bQaUno, iTipo, bHayFechaImputacionDefecto) Then
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.NuevoCodArticulo, iTipo)
        Else
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.CodArticulo)
        End If
    
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.PrecioUnitarioAdj Then
        If .Columns("CAMPO_GS").CellValue(vbm2) = TipoCampoSC.ProveedorAdj Then
            If .Columns("CAMPO_GS").CellValue(vbm3) = TipoCampoSC.CantidadAdj Then
                b3Adj = True
            Else
                bPaisMat = True
            End If
        ElseIf .Columns("CAMPO_GS").CellValue(vbm2) = TipoCampoSC.CantidadAdj Then
            bPaisMat = True
        Else
            bArtDen = True
        End If

    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.ProveedorAdj Then
        If .Columns("CAMPO_GS").CellValue(vbm2) = TipoCampoSC.CantidadAdj Then
            bPaisMat = True
        Else
            bArtDen = True
        End If
    
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.CantidadAdj Then
        bArtDen = True
    
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
        bQaImpPie = False
        bQaUno = False
        bConEnviosCorrectos = m_bConEnviosCorrectos
        bArtDen = True
        bHayFechaImputacionDefecto = False
        vbm = .GetBookmark(-1)
        
        If HayCamposQARelacionados(bQaImpPie, bQaUno, iTipo, bHayFechaImputacionDefecto) Then
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.DenArticulo, iTipo)
        Else
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.DenArticulo)
        End If
    
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoNoConformidad.EnviosCorrectos Then
        iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
        If iResp = vbYes Then m_bConEnviosCorrectos = False
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas Then
        iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
        If iResp = vbYes Then m_bConPiezasDefectuosas = False
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.DesgloseActividad Then
        iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
        If iResp = vbYes Then m_bConDesgloseActividad = False
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.DesgloseFactura Then
        iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
        If iResp = vbYes Then
            m_bConDesgloseFactura = False
            cmdAnyaGr.Enabled = True
            cmdMoverGrupo.Enabled = True
        End If
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.DesgloseDePedido Then
        iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
        If iResp = vbYes Then
            m_bConDesgloseDePedido = False
            cmdAnyaGr.Enabled = True
            cmdMoverGrupo.Enabled = True
        End If
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.CentroCoste Then
            
        Dim oCentroCoste As CFormItem
        Set oCentroCoste = ComprobarCentroDeCoste(.Columns("ID").Value)
        If Not oCentroCoste Is Nothing Then
            oMensajes.ImposibleEliminarCentroDeCoste oCentroCoste, gParametrosGenerales.gIdioma
            iResp = vbNo
        Else
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
        End If
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.Proveedor Then
        'Si se quiere eliminar el proveedor, se comprobara antes que no exista el campoGS Proveedor ERP, si existe se deberia eliminar este campo primero
        bExiste = AccionSobreCampoGsProveedorERP(AccionSobreCampoProveedorERP.ExisteCampo, TipoCampoGS.Proveedor)
        If bExiste Then
            oMensajes.ImposibleEliminarProveedor (TipoCampoGS.Proveedor)
            iResp = vbNo
        Else
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
        End If
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras Then
        'Si se quiere eliminar el campo Organizacion de compras y existe el campo Proveedor ERP, se debera eliminar este campo 1�
        bExiste = AccionSobreCampoGsProveedorERP(AccionSobreCampoProveedorERP.ExisteCampo)
        If bExiste Then
            oMensajes.ImposibleEliminarProveedor (TipoCampoGS.OrganizacionCompras)
            iResp = vbNo
        Else
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
        End If
    ElseIf .Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
        'si se quiere borrar un desglose, previamente habra q ver si hay calculados a nivel de Campo q tiren del desglose. De existir, se debera eliminar este calculado 1�
        sLista = HayCamposCalculadoRelacionados(.Columns("ID").Value)
        If sLista <> "" Then
            oMensajes.ImposibleEliminarDesglosePorCalculados (sLista)
            iResp = vbNo
        Else
            iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
        End If
    Else
        iResp = oMensajes.PreguntaEliminarCampo(.Columns(gParametrosInstalacion.gIdioma).Value)
    End If
    
    If iResp = vbNo Then
        g_Accion = ACCFormularioCons
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    'Elimina de base de datos:
    Dim iCont As Byte
    If b4Adj Then
        iCont = 4
    ElseIf b3Adj Then
        iCont = 3
    ElseIf bPaisMat = True Then
        If bQaImpPie = True Then
            iCont = 4
        ElseIf bQaUno = True Then
            iCont = 3
        Else
            iCont = 2
        End If
        m_bConPiezasDefectuosas = False
        m_bArt = False
    ElseIf bArtDen Then
        If bQaImpPie Then
            iCont = 3
        ElseIf bQaUno Then
            iCont = 2
        Else
            iCont = 1
        End If
        m_bConPiezasDefectuosas = False
        If Not bPaisProvi Then m_bArt = False
    Else
        iCont = 0
    End If
    If bConEnviosCorrectos Then
        iCont = iCont + 1
        m_bConEnviosCorrectos = False
    End If
    If bHayFechaImputacionDefecto Then
        iCont = iCont + 1
    End If
    
    ReDim aIdentificadores(.SelBookmarks.Count + iCont)
    ReDim aBookmarks(.SelBookmarks.Count + iCont)
    i = 0
    While i < .SelBookmarks.Count
        .Bookmark = .SelBookmarks(i)
        aIdentificadores(i + 1) = .Columns("ID").Value
        aBookmarks(i + 1) = .SelBookmarks(i)
        i = i + 1
    Wend
    
    If bPaisMat = True Or bArtDen Or b3Adj Or b4Adj Then
        aIdentificadores(i + 1) = .Columns("ID").CellValue(vbm)
        aBookmarks(i + 1) = vbm
        If bPaisMat Or b3Adj Or b4Adj Then
            'Almacenamiento del codigo de la denominacion
            aIdentificadores(i + 2) = .Columns("ID").CellValue(.GetBookmark(2))
            aBookmarks(i + 2) = .GetBookmark(2)
            If b3Adj Or b4Adj Then
                aIdentificadores(i + 3) = .Columns("ID").CellValue(.GetBookmark(3))
                aBookmarks(i + 3) = .GetBookmark(3)
                If b4Adj Then
                    aIdentificadores(i + 4) = .Columns("ID").CellValue(.GetBookmark(4))
                    aBookmarks(i + 4) = .GetBookmark(4)
                End If
            End If
        End If
    End If
    
    If bQaImpPie Or bQaUno Or bConEnviosCorrectos Or bHayFechaImputacionDefecto Then
        If bArtDen Then
            i = i + 1
        Else
            i = i + 2
        End If

        For j = 0 To .Rows - 1
            bm = .RowBookmark(j)
            If .Columns("CAMPO_GS").CellValue(bm) = TipoCampoNoConformidad.PiezasDefectuosas _
            Or .Columns("CAMPO_GS").CellValue(bm) = TipoCampoNoConformidad.ImporteRepercutido _
            Or .Columns("CAMPO_GS").CellValue(bm) = TipoCampoNoConformidad.EnviosCorrectos _
            Or .Columns("CAMPO_GS").CellValue(bm) = TipoCampoNoConformidad.FechaImputacionDefecto Then
                aIdentificadores(i + 1) = .Columns("ID").CellValue(bm)
                aBookmarks(i + 1) = bm
                
                i = i + 1
            End If
        Next
    End If
    '''
    'LISTAS ENLAZADAS: NO BORRAR SI TIENE LISTAS ENLAZADAS HIJO
    Dim sCampoRelacionado As String
    Dim g_oCampo As CFormItem
    Set g_oCampo = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
    For j = 1 To UBound(aIdentificadores)
        sCampoRelacionado = g_oCampo.DevolverNombreCampoHijo(0)
        
        If sCampoRelacionado <> "" Then
            oMensajes.NoModificarCampoPadre g_oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den, sCampoRelacionado
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    Next
    
    udtTeserror = g_oGrupoSeleccionado.Campos.EliminarCamposDeBaseDatos(aIdentificadores)
    
    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError udtTeserror
    Else
        'Elimina los campos de la colecci�n:
        For i = 1 To UBound(aIdentificadores)
             g_oGrupoSeleccionado.Campos.Remove (CStr(aIdentificadores(i)))
             basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemEliminar, aIdentificadores(i)
        Next i
        'Elimina los campos de la grid:
        For i = 1 To UBound(aBookmarks)
            .RemoveItem (.AddItemRowIndex(aBookmarks(i)))
        Next i
        'Se posiciona en la fila correspondiente:
        If .Rows > 0 Then
            If IsEmpty(.RowBookmark(.Row)) Then
                .Bookmark = .RowBookmark(.Row - 1)
            Else
                .Bookmark = .RowBookmark(.Row)
            End If
        Else
            'Indico que no tiene campos
            m_bConCampos = False
        End If
    End If
    
    .SelBookmarks.RemoveAll
    If Me.Visible Then .SetFocus
    
    g_Accion = ACCFormularioCons

    'si antes habia articulo y despues se ha elegido un registro de una tabla externa con articulos
    'ahora hay que borrar ese registro elegido.Por lo tanto comprobar
    'Antes habia articulo y ahora lo no hay
    'entonces buscar aquellos que son tipo externo de tipo articulo
    If m_blnExisteTablaExternaParaArticulo Then
        If Not ExisteAtributoArticulo() Then
            'AXIOMA 1: solo puede existir un articulo por grupo (en el formulario fuera del desglose)
            'AXIOMA 2: solo puede existir un articulo por LINEA (en un desglose)
            'AXIOMA 3: pueden existir n tablas externas tirando del mismo articulo
            
            'ha desaparecido el articulo --> quitamos el valor para la columna de la tabla externa
            QuitarValorTablaExterna
            m_blnExisteTablaExternaParaArticulo = False
            m_blnExisteArticulo = False
        End If
    End If
        
    Screen.MousePointer = vbNormal
    End With
    Exit Sub
    
Cancelar:
    Screen.MousePointer = vbNormal
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub cmdlistado_Click()
    'si hay cambios los guarda
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
        If m_bModError = True Then Exit Sub
    End If
    
    frmLstFormularios.g_sOrigen = "frmFormularios"
    frmLstFormularios.Show vbModal
End Sub

Private Sub cmdModifForm_Click()
    Dim udtTeserror As TipoErrorSummit
    
    Screen.MousePointer = vbHourglass
    
    udtTeserror.NumError = TESnoerror
    g_Accion = ACCFormularioModificar
    
    Set m_oIBaseDatos = g_oFormSeleccionado
    udtTeserror = m_oIBaseDatos.IniciarEdicion
    
    If udtTeserror.NumError = TESnoerror Then
        Screen.MousePointer = vbNormal
        
        udtTeserror = MostrarFormFormularioAnya(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, g_Accion, g_oFormSeleccionado, oGestorParametros.DevolverIdiomas(, , True), _
                   basPublic.gParametrosInstalacion.gIdioma)
        If udtTeserror.Arg2 Then
            If udtTeserror.NumError = TESnoerror Then
                'Cambia el combo de formulario
                AnyaModifFormulario
                
                RegistrarAccion ACCFormularioModificar, "Id:" & frmFormularios.g_oFormSeleccionado.Id
            Else
                TratarError udtTeserror
            End If
        End If
        g_Accion = ACCFormularioCons
    Else
        TratarError udtTeserror
    End If

    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdModifGr_Click()
    'si hay cambios los guarda
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
        If m_bModError = True Then Exit Sub
    End If
    
    g_Accion = ACCFormGrupoModif
               
    Set frmFormGrupoAnya.g_oIdiomas = m_oIdiomas
    frmFormGrupoAnya.WindowState = vbNormal
    frmFormGrupoAnya.Show vbModal
End Sub

Private Sub cmdMoverGrupo_Click()
    'Muestra la pantalla para cambiar el orden o copiar grupos en una posici�n en concreto
    CambiarOrdenGrupos
End Sub

Private Sub cmdRestaurar_Click()
    LimpiarCampos
    FormularioSeleccionado
End Sub

Private Sub cmdSubir_Click()
Dim i As Integer
Dim arrValoresMat() As Variant
Dim arrValoresArt() As Variant
Dim arrValoresDen() As Variant
Dim arrValoresAux() As Variant
Dim bOrdenoMat As Boolean
Dim bOrdenoPais As Boolean
Dim bOrdenoArt As Boolean
Dim bOrdenoProv As Boolean
Dim bOrdenoDen As Boolean
Dim bOrdenoCampoMat As Boolean
Dim bOrdenoCampoProv As Boolean
Dim vbm As Variant
Dim bOrdenoPrecProvCantAdj As Boolean
Dim bOrdenoPrecProvAdj As Boolean
Dim bOrdenoPrecCantAdj As Boolean
Dim bOrdenoPrecAdj As Boolean
Dim bOrdenoProvCantAdj As Boolean
Dim bOrdenoProvAdj As Boolean
Dim bOrdenoCantAdj As Boolean
Dim bOrdenoArtPrecProvCantAdj As Boolean
Dim bOrdenoArtPrecProvAdj As Boolean
Dim bOrdenoArtPrecCantAdj As Boolean
Dim bOrdenoArtPrecAdj As Boolean
Dim bOrdenoArtProvCantAdj As Boolean
Dim bOrdenoArtProvAdj As Boolean
Dim bOrdenoArtCantAdj As Boolean
Dim bOrdPrecProvCantAdj As Boolean
Dim bOrdPrecProvAdj As Boolean
Dim bOrdPrecCantAdj As Boolean
Dim bOrdPrecAdj As Boolean
Dim bOrdProvPrecCantAdj As Boolean
Dim bOrdProvPrecAdj As Boolean
Dim bOrdProvCantAdj As Boolean
Dim bOrdProvAdj As Boolean
Dim bOrdCantProvPrecAdj As Boolean
Dim bOrdCantProvAdj As Boolean
Dim bOrdCantPrecAdj As Boolean
Dim bOrdCantAdj As Boolean
Dim bOrdenoCampoCantProvPrecAdj As Boolean
Dim bOrdenoCampoCantProvAdj As Boolean
Dim bOrdenoCampoCantPrecAdj As Boolean
Dim bOrdenoCampoCantAdj As Boolean
Dim bOrdenoCampoProvPrecAdj As Boolean
Dim bOrdenoCampoProvAdj As Boolean
Dim bOrdenoCampoPrecAdj As Boolean

With sdbgCampos
    'si hay cambios los guarda
    If .DataChanged = True Then
        .Update
        If m_bModError = True Then Exit Sub
    End If
    
    If .SelBookmarks.Count = 0 Then Exit Sub
    If .AddItemRowIndex(.SelBookmarks.Item(0)) = 0 Then Exit Sub

    g_bUpdate = False
    
    ReDim arrValoresMat(.Columns.Count - 1)
    ReDim arrValoresArt(.Columns.Count - 1)
    ReDim arrValoresDen(.Columns.Count - 1)
    ReDim arrValoresAux(.Columns.Count - 1)
    ReDim arrValoresPrecAdj(.Columns.Count - 1)
    ReDim arrValoresProvAdj(.Columns.Count - 1)
    ReDim arrValoresCantAdj(.Columns.Count - 1)

    '''Ordenar  Material // Pais
    If (.Columns("CAMPO_GS").Value = TipoCampoGS.material) Or (.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
        vbm = .GetBookmark(1)
        If (.Columns("CAMPO_GS").Value = TipoCampoGS.Pais And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Provincia) Or _
           (.Columns("CAMPO_GS").Value = TipoCampoGS.material And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo) Or _
           (.Columns("CAMPO_GS").Value = TipoCampoGS.material And .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Then
           
            If (.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Provincia) Then
                bOrdenoPais = True
                For i = 0 To .Columns.Count - 1
                   arrValoresMat(i) = .Columns(i).Value
                   arrValoresArt(i) = .Columns(i).CellValue(vbm)
                Next i
            ElseIf (.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Then
                If (.Columns("CAMPO_GS").CellValue(.GetBookmark(2))) = TipoCampoSC.PrecioUnitarioAdj Then
                    If (.Columns("CAMPO_GS").CellValue(.GetBookmark(3))) = TipoCampoSC.ProveedorAdj Then
                        If (.Columns("CAMPO_GS").CellValue(.GetBookmark(4))) = TipoCampoSC.CantidadAdj Then
                            bOrdenoPrecProvCantAdj = True
                            For i = 0 To .Columns.Count - 1
                                arrValoresMat(i) = .Columns(i).Value
                                arrValoresArt(i) = .Columns(i).CellValue(vbm)
                                arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                                arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(3))
                                arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(4))
                            Next i
                        Else
                            bOrdenoPrecProvAdj = True
                            For i = 0 To .Columns.Count - 1
                                arrValoresMat(i) = .Columns(i).Value
                                arrValoresArt(i) = .Columns(i).CellValue(vbm)
                                arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                                arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(3))
                            Next i
                        End If
                    ElseIf (.Columns("CAMPO_GS").CellValue(.GetBookmark(3))) = TipoCampoSC.CantidadAdj Then
                        bOrdenoPrecCantAdj = True
                        For i = 0 To .Columns.Count - 1
                            arrValoresMat(i) = .Columns(i).Value
                            arrValoresArt(i) = .Columns(i).CellValue(vbm)
                            arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                            arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(3))
                        Next i
                    Else
                        bOrdenoPrecAdj = True
                        For i = 0 To .Columns.Count - 1
                            arrValoresMat(i) = .Columns(i).Value
                            arrValoresArt(i) = .Columns(i).CellValue(vbm)
                            arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                        Next i
                    End If
                ElseIf (.Columns("CAMPO_GS").CellValue(.GetBookmark(2))) = TipoCampoSC.ProveedorAdj Then
                    If (.Columns("CAMPO_GS").CellValue(.GetBookmark(3))) = TipoCampoSC.CantidadAdj Then
                        bOrdenoProvCantAdj = True
                        For i = 0 To .Columns.Count - 1
                            arrValoresMat(i) = .Columns(i).Value
                            arrValoresArt(i) = .Columns(i).CellValue(vbm)
                            arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                            arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(3))
                        Next i
                    Else
                        bOrdenoProvAdj = True
                        For i = 0 To .Columns.Count - 1
                            arrValoresMat(i) = .Columns(i).Value
                            arrValoresArt(i) = .Columns(i).CellValue(vbm)
                            arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                        Next i
                    End If
                ElseIf (.Columns("CAMPO_GS").CellValue(.GetBookmark(2))) = TipoCampoSC.CantidadAdj Then
                    bOrdenoCantAdj = True
                    For i = 0 To .Columns.Count - 1
                        arrValoresMat(i) = .Columns(i).Value
                        arrValoresArt(i) = .Columns(i).CellValue(vbm)
                        arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                    Next i
                Else
                    bOrdenoPais = True
                    For i = 0 To .Columns.Count - 1
                       arrValoresMat(i) = .Columns(i).Value
                       arrValoresArt(i) = .Columns(i).CellValue(vbm)
                    Next i
                End If
            
            ElseIf (.Columns("CAMPO_GS").CellValue(.GetBookmark(2))) = TipoCampoGS.DenArticulo Then
                bOrdenoMat = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).Value
                    arrValoresArt(i) = .Columns(i).CellValue(vbm)
                    arrValoresDen(i) = .Columns(i).CellValue(.GetBookmark(2))
                Next i
            End If
        Else
            For i = 0 To .Columns.Count - 1
                arrValoresAux(i) = .Columns(i).Value
            Next i
        End If
        
    'Ordenacion del c�digo del art�culo // Provincia
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or _
     .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or _
     .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Provincia Then
        If val(.Row) = 1 Then Exit Sub
        vbm = .GetBookmark(-1)
        If .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoSC.PrecioUnitarioAdj Then
                If .Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoSC.ProveedorAdj Then
                    If .Columns("CAMPO_GS").CellValue(.GetBookmark(3)) = TipoCampoSC.CantidadAdj Then
                        bOrdenoArtPrecProvCantAdj = True
                        For i = 0 To .Columns.Count - 1
                            arrValoresMat(i) = .Columns(i).CellValue(vbm)
                            arrValoresArt(i) = .Columns(i).Value
                            arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                            arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                            arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(3))
                        Next i
                    Else
                        bOrdenoArtPrecProvAdj = True
                        For i = 0 To .Columns.Count - 1
                            arrValoresMat(i) = .Columns(i).CellValue(vbm)
                            arrValoresArt(i) = .Columns(i).Value
                            arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                            arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                        Next i
                    End If
                ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoSC.CantidadAdj Then
                    bOrdenoArtPrecCantAdj = True
                    For i = 0 To .Columns.Count - 1
                        arrValoresMat(i) = .Columns(i).CellValue(vbm)
                        arrValoresArt(i) = .Columns(i).Value
                        arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                        arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                    Next i
                Else
                    bOrdenoArtPrecAdj = True
                    For i = 0 To .Columns.Count - 1
                        arrValoresMat(i) = .Columns(i).CellValue(vbm)
                        arrValoresArt(i) = .Columns(i).Value
                        arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                    Next i
                End If
            ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoSC.ProveedorAdj Then
                If .Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoSC.CantidadAdj Then
                    bOrdenoArtProvCantAdj = True
                    For i = 0 To .Columns.Count - 1
                        arrValoresMat(i) = .Columns(i).CellValue(vbm)
                        arrValoresArt(i) = .Columns(i).Value
                        arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                        arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                    Next i
                Else
                    bOrdenoArtProvAdj = True
                    For i = 0 To .Columns.Count - 1
                        arrValoresMat(i) = .Columns(i).CellValue(vbm)
                        arrValoresArt(i) = .Columns(i).Value
                        arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                    Next i
                End If
            ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoSC.CantidadAdj Then
                bOrdenoArtCantAdj = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(vbm)
                    arrValoresArt(i) = .Columns(i).Value
                    arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                Next i
            Else
                bOrdenoProv = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(vbm)
                    arrValoresArt(i) = .Columns(i).Value
                Next i
            End If
            
        ElseIf .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais Or .Columns("CAMPO_GS").Value = TipoCampoGS.Provincia Or .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) <> TipoCampoGS.DenArticulo Then
            bOrdenoProv = True
            For i = 0 To .Columns.Count - 1
                arrValoresMat(i) = .Columns(i).CellValue(vbm)
                arrValoresArt(i) = .Columns(i).Value
            Next i
        ElseIf .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material Then
            bOrdenoArt = True
            For i = 0 To .Columns.Count - 1
                arrValoresMat(i) = .Columns(i).CellValue(vbm)
                arrValoresArt(i) = .Columns(i).Value
                arrValoresDen(i) = .Columns(i).CellValue(.GetBookmark(1))
            Next i
        End If

    'Ordenacion de la denominacion
    ElseIf (.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
        If val(.Row) = 2 Then Exit Sub
        If .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoGS.NuevoCodArticulo Then
            bOrdenoDen = True
            For i = 0 To .Columns.Count - 1
                arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
                arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
                arrValoresDen(i) = .Columns(i).Value
            Next i
        End If
        
    'Ordenacion de precio unitario adjudicado
    ElseIf (.Columns("CAMPO_GS").Value = TipoCampoSC.PrecioUnitarioAdj) Then
        If val(.Row) = 2 Then Exit Sub
        If .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoGS.CodArticulo Then
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoSC.ProveedorAdj Then
                If .Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoSC.CantidadAdj Then
                    bOrdPrecProvCantAdj = True
                    For i = 0 To .Columns.Count - 1
                        arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
                        arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
                        arrValoresPrecAdj(i) = .Columns(i).Value
                        arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                        arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(2))
                    Next i
                Else
                    bOrdPrecProvAdj = True
                    For i = 0 To .Columns.Count - 1
                        arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
                        arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
                        arrValoresPrecAdj(i) = .Columns(i).Value
                        arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                    Next i
                End If
            ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoSC.CantidadAdj Then
                bOrdPrecCantAdj = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
                    arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
                    arrValoresPrecAdj(i) = .Columns(i).Value
                    arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                Next i
            Else
                bOrdPrecAdj = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
                    arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
                    arrValoresPrecAdj(i) = .Columns(i).Value
                Next i
            End If
        End If
        
    'Ordenacion de proveedor adjudicado
    ElseIf (.Columns("CAMPO_GS").Value = TipoCampoSC.ProveedorAdj) Then
        If .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoSC.PrecioUnitarioAdj Then
            If val(.Row) = 3 Then Exit Sub
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoSC.CantidadAdj Then
                bOrdProvPrecCantAdj = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-3))
                    arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-2))
                    arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(-1))
                    arrValoresProvAdj(i) = .Columns(i).Value
                    arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                Next i
            Else
                bOrdProvPrecAdj = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-3))
                    arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-2))
                    arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(-1))
                    arrValoresProvAdj(i) = .Columns(i).Value
                Next i
            End If
        ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoGS.CodArticulo Then
            If val(.Row) = 2 Then Exit Sub
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoSC.CantidadAdj Then
                bOrdProvCantAdj = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
                    arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
                    arrValoresProvAdj(i) = .Columns(i).Value
                    arrValoresCantAdj(i) = .Columns(i).CellValue(.GetBookmark(1))
                Next i
            Else
                bOrdProvAdj = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
                    arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
                    arrValoresProvAdj(i) = .Columns(i).Value
                Next i
            End If
        End If
        
    'Ordenacion de cantidad adjudicada
    ElseIf (.Columns("CAMPO_GS").Value = TipoCampoSC.CantidadAdj) Then
        If .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoSC.ProveedorAdj Then
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoSC.PrecioUnitarioAdj Then
                If val(.Row) = 4 Then Exit Sub
                bOrdCantProvPrecAdj = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-4))
                    arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-3))
                    arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(-2))
                    arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(-1))
                    arrValoresCantAdj(i) = .Columns(i).Value
                Next i
            ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoGS.CodArticulo Then
                If val(.Row) = 3 Then Exit Sub
                bOrdCantProvAdj = True
                For i = 0 To .Columns.Count - 1
                    arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-3))
                    arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-2))
                    arrValoresProvAdj(i) = .Columns(i).CellValue(.GetBookmark(-1))
                    arrValoresCantAdj(i) = .Columns(i).Value
                Next i
            End If
        ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoSC.PrecioUnitarioAdj Then
            If val(.Row) = 3 Then Exit Sub
            bOrdCantPrecAdj = True
            For i = 0 To .Columns.Count - 1
                arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-3))
                arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-2))
                arrValoresPrecAdj(i) = .Columns(i).CellValue(.GetBookmark(-1))
                arrValoresCantAdj(i) = .Columns(i).Value
            Next i
        ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoGS.CodArticulo Then
            If val(.Row) = 2 Then Exit Sub
            bOrdCantAdj = True
            For i = 0 To .Columns.Count - 1
                arrValoresMat(i) = .Columns(i).CellValue(.GetBookmark(-2))
                arrValoresArt(i) = .Columns(i).CellValue(.GetBookmark(-1))
                arrValoresCantAdj(i) = .Columns(i).Value
            Next i
        End If
        
    Else
        vbm = .GetBookmark(-1)
        If .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoSC.CantidadAdj Then
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoSC.ProveedorAdj Then
                If .Columns("CAMPO_GS").CellValue(.GetBookmark(-3)) = TipoCampoSC.PrecioUnitarioAdj Then
                    If .Columns("CAMPO_GS").CellValue(.GetBookmark(-4)) = TipoCampoGS.CodArticulo And _
                        .Columns("CAMPO_GS").CellValue(.GetBookmark(-5)) = TipoCampoGS.material Then
                        bOrdenoCampoCantProvPrecAdj = True
                    End If
                ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-3)) = TipoCampoGS.CodArticulo And _
                    .Columns("CAMPO_GS").CellValue(.GetBookmark(-4)) = TipoCampoGS.material Then
                    bOrdenoCampoCantProvAdj = True
                End If
            ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoSC.PrecioUnitarioAdj Then
                If .Columns("CAMPO_GS").CellValue(.GetBookmark(-3)) = TipoCampoGS.CodArticulo And _
                    .Columns("CAMPO_GS").CellValue(.GetBookmark(-4)) = TipoCampoGS.material Then
                    bOrdenoCampoCantPrecAdj = True
                End If
            ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoGS.CodArticulo And _
                .Columns("CAMPO_GS").CellValue(.GetBookmark(-3)) = TipoCampoGS.material Then
                bOrdenoCampoCantAdj = True
            End If
        ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoSC.ProveedorAdj Then
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoSC.PrecioUnitarioAdj Then
                If .Columns("CAMPO_GS").CellValue(.GetBookmark(-3)) = TipoCampoGS.CodArticulo And _
                    .Columns("CAMPO_GS").CellValue(.GetBookmark(-4)) = TipoCampoGS.material Then
                    bOrdenoCampoProvPrecAdj = True
                End If
            ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoGS.CodArticulo And _
                .Columns("CAMPO_GS").CellValue(.GetBookmark(-3)) = TipoCampoGS.material Then
                bOrdenoCampoProvAdj = True
            End If
        ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoSC.PrecioUnitarioAdj Then
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoGS.CodArticulo And _
                .Columns("CAMPO_GS").CellValue(.GetBookmark(-3)) = TipoCampoGS.material Then
                bOrdenoCampoPrecAdj = True
            End If
        ElseIf .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.DenArticulo Or _
         .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Provincia Or _
         .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo Or _
         .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo Then
           If .Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.DenArticulo Then
               bOrdenoCampoMat = True
           Else
               bOrdenoCampoProv = True
           End If
        End If
        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
        Next i
    End If
    
    If bOrdenoMat Or bOrdenoArt Or bOrdenoDen Or bOrdenoPais Or bOrdenoProv Then
        'Ordeno conjuntamente los campos de material y art�culo
        .MovePrevious
        If bOrdenoArt Or bOrdenoDen Or bOrdenoProv Then
            .MovePrevious
        End If
        
        If bOrdenoDen Then
            .MovePrevious
        End If
        
        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresArt(i)
        Next i
        If Not bOrdenoPais And Not bOrdenoProv Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresDen(i)
            Next i
        End If
        
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresAux(i)
        Next i
        
        .SelBookmarks.RemoveAll
                
        If Not bOrdenoArt And Not bOrdenoDen And Not bOrdenoPais And Not bOrdenoProv Then .MovePrevious
        If Not bOrdenoDen And Not bOrdenoProv Then .MovePrevious
        .MovePrevious
    
    ElseIf bOrdenoPrecProvCantAdj Or bOrdenoPrecProvAdj Or bOrdenoPrecCantAdj Or bOrdenoPrecAdj Or _
        bOrdenoProvCantAdj Or bOrdenoProvAdj Or bOrdenoCantAdj Then
        .MovePrevious
        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdenoPrecProvCantAdj Or bOrdenoPrecProvAdj Or bOrdenoPrecCantAdj Or bOrdenoPrecAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresPrecAdj(i)
            Next i
        End If
        If bOrdenoPrecProvCantAdj Or bOrdenoPrecProvAdj Or bOrdenoProvCantAdj Or bOrdenoProvAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresProvAdj(i)
            Next i
        End If
        If bOrdenoPrecProvCantAdj Or bOrdenoPrecCantAdj Or bOrdenoProvCantAdj Or bOrdenoCantAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresCantAdj(i)
            Next i
        End If
        
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresAux(i)
        Next i
        
        .SelBookmarks.RemoveAll
        
        .MovePrevious
        .MovePrevious
        .MovePrevious
        If bOrdenoPrecProvCantAdj Or bOrdenoPrecProvAdj Or bOrdenoPrecCantAdj Or bOrdenoProvCantAdj Then .MovePrevious
        If bOrdenoPrecProvCantAdj Then .MovePrevious
        
'Ordenacion Art�culo
    ElseIf bOrdenoArtPrecProvCantAdj Or bOrdenoArtPrecProvAdj Or bOrdenoArtPrecCantAdj Or bOrdenoArtPrecAdj Or _
        bOrdenoArtProvCantAdj Or bOrdenoArtProvAdj Or bOrdenoArtCantAdj Then
        .MovePrevious
        .MovePrevious
        
        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdenoArtPrecProvCantAdj Or bOrdenoArtPrecProvAdj Or bOrdenoArtPrecCantAdj Or bOrdenoArtPrecAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresPrecAdj(i)
            Next i
        End If
        If bOrdenoArtPrecProvCantAdj Or bOrdenoArtPrecProvAdj Or bOrdenoArtProvCantAdj Or bOrdenoArtProvAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresProvAdj(i)
            Next i
        End If
        If bOrdenoArtPrecProvCantAdj Or bOrdenoArtPrecCantAdj Or bOrdenoArtProvCantAdj Or bOrdenoArtCantAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresCantAdj(i)
            Next i
        End If
        
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresAux(i)
        Next i
        
        .SelBookmarks.RemoveAll
        
        .MovePrevious
        .MovePrevious
        If bOrdenoArtPrecProvCantAdj Or bOrdenoArtPrecProvAdj Or bOrdenoArtPrecCantAdj Or bOrdenoArtProvCantAdj Then .MovePrevious
        If bOrdenoArtPrecProvCantAdj Then .MovePrevious
        
'Ordenaci�n Precio Unitario Adjudicado
    ElseIf bOrdPrecProvCantAdj Or bOrdPrecProvAdj Or bOrdPrecCantAdj Or bOrdPrecAdj Then
        .MovePrevious
        .MovePrevious
        .MovePrevious

        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresArt(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresPrecAdj(i)
        Next i
        If bOrdPrecProvAdj Or bOrdPrecProvCantAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresProvAdj(i)
            Next i
        End If
        If bOrdPrecProvCantAdj Or bOrdPrecCantAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresCantAdj(i)
            Next i
        End If

        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresAux(i)
        Next i
        
        .SelBookmarks.RemoveAll
        
        .MovePrevious
        If bOrdPrecProvCantAdj Or bOrdPrecProvAdj Or bOrdPrecCantAdj Then .MovePrevious
        If bOrdPrecProvCantAdj Then .MovePrevious
        
'Ordenaci�n Proveedor Adjudicado
    ElseIf bOrdProvPrecCantAdj Or bOrdProvPrecAdj Or bOrdProvCantAdj Or bOrdProvAdj Then
        .MovePrevious
        .MovePrevious
        .MovePrevious
        If bOrdProvPrecCantAdj Or bOrdProvPrecAdj Then .MovePrevious
        
        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdProvPrecCantAdj Or bOrdProvPrecAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresPrecAdj(i)
            Next i
        End If
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresProvAdj(i)
        Next i
        If bOrdProvPrecCantAdj Or bOrdProvCantAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresCantAdj(i)
            Next i
        End If
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresAux(i)
        Next i
        .SelBookmarks.RemoveAll
        .MovePrevious
        If bOrdProvPrecCantAdj Or bOrdProvCantAdj Then .MovePrevious
        
'Ordenaci�n Cantidad Adjudicada
    ElseIf bOrdCantProvPrecAdj Or bOrdCantProvAdj Or bOrdCantPrecAdj Or bOrdCantAdj Then
        .MovePrevious
        .MovePrevious
        .MovePrevious
        If bOrdCantProvPrecAdj Or bOrdCantProvAdj Or bOrdCantPrecAdj Then .MovePrevious
        If bOrdCantProvPrecAdj Then .MovePrevious
        
        For i = 0 To .Columns.Count - 1
            arrValoresAux(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdCantProvPrecAdj Or bOrdCantPrecAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresPrecAdj(i)
            Next i
        End If
        If bOrdCantProvPrecAdj Or bOrdCantProvAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresProvAdj(i)
            Next i
        End If
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresCantAdj(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresAux(i)
        Next i
        .SelBookmarks.RemoveAll
        .MovePrevious
        
'Ordenaci�n de un campo cualquiera
    ElseIf bOrdenoCampoMat Or bOrdenoCampoProv Or bOrdenoCampoCantProvPrecAdj Or bOrdenoCampoCantProvAdj Or bOrdenoCampoCantPrecAdj Or _
        bOrdenoCampoCantAdj Or bOrdenoCampoProvPrecAdj Or bOrdenoCampoProvAdj Or bOrdenoCampoPrecAdj Then
        .MovePrevious
        .MovePrevious
        If bOrdenoCampoMat Then .MovePrevious
        If bOrdenoCampoCantProvPrecAdj Or bOrdenoCampoCantProvAdj Or bOrdenoCampoCantPrecAdj Or _
            bOrdenoCampoCantAdj Or bOrdenoCampoProvPrecAdj Or bOrdenoCampoProvAdj Or bOrdenoCampoPrecAdj Then
            .MovePrevious
        End If
        If bOrdenoCampoCantProvPrecAdj Or bOrdenoCampoCantProvAdj Or bOrdenoCampoCantPrecAdj Or bOrdenoCampoProvPrecAdj Then .MovePrevious
        If bOrdenoCampoCantProvPrecAdj Then .MovePrevious

        For i = 0 To .Columns.Count - 1
            arrValoresMat(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresAux(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            arrValoresArt(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresMat(i)
        Next i
        .MoveNext
        For i = 0 To .Columns.Count - 1
            arrValoresDen(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdenoCampoMat Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresDen(i)
            Next i
        End If
        
        If bOrdenoCampoCantProvPrecAdj Or bOrdenoCampoCantProvAdj Or bOrdenoCampoCantPrecAdj Or _
            bOrdenoCampoCantAdj Or bOrdenoCampoProvPrecAdj Or bOrdenoCampoProvAdj Or bOrdenoCampoPrecAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                arrValoresProvAdj(i) = .Columns(i).Value
                .Columns(i).Value = arrValoresDen(i)
            Next i
        End If
        
        If bOrdenoCampoCantProvPrecAdj Or bOrdenoCampoCantProvAdj Or bOrdenoCampoCantPrecAdj Or bOrdenoCampoProvPrecAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                arrValoresCantAdj(i) = .Columns(i).Value
                .Columns(i).Value = arrValoresProvAdj(i)
            Next i
        End If
        
        If bOrdenoCampoCantProvPrecAdj Then
            .MoveNext
            For i = 0 To .Columns.Count - 1
                .Columns(i).Value = arrValoresCantAdj(i)
            Next i
        End If

        .SelBookmarks.RemoveAll
        .MovePrevious
        .MovePrevious
        If bOrdenoCampoMat Then .MovePrevious
        If bOrdenoCampoCantProvPrecAdj Or bOrdenoCampoCantProvAdj Or bOrdenoCampoCantPrecAdj Or _
            bOrdenoCampoCantAdj Or bOrdenoCampoProvPrecAdj Or bOrdenoCampoProvAdj Or bOrdenoCampoPrecAdj Then
            .MovePrevious
        End If
        If bOrdenoCampoCantProvPrecAdj Or bOrdenoCampoCantProvAdj Or bOrdenoCampoCantPrecAdj Or bOrdenoCampoProvPrecAdj Then .MovePrevious
        If bOrdenoCampoCantProvPrecAdj Then .MovePrevious
        
    Else  'Ordeno normal,de 1 en 1

        .MovePrevious

        For i = 0 To .Columns.Count - 1
            arrValoresMat(i) = .Columns(i).Value
            .Columns(i).Value = arrValoresAux(i)
        Next i

        .MoveNext

        For i = 0 To .Columns.Count - 1
            .Columns(i).Value = arrValoresMat(i)
        Next i

        .SelBookmarks.RemoveAll
        .MovePrevious
        
        
    End If
    .SelBookmarks.Add .Bookmark
    
    g_bUpdate = True
    
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos
    
    Dim sCodOrgCompras As String
    Dim sCodCentro As String
    Dim lIdAlmacen As Long
    Dim oAlmacenes As CAlmacenes
    Dim oCentros As CCentros
    
    'Comprobacion de los valores ORGANIZACION DE COMPRAS, CENTROS Y ALMACEN
    If .Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras And .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.Centro _
    Or .Columns("CAMPO_GS").Value = TipoCampoGS.Centro And .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoGS.OrganizacionCompras And .Columns("VALOR").Value <> "" Then
        If .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.Centro And .Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras Then
            sCodCentro = .Columns("COD_VALOR").CellValue(.GetBookmark(1))
            sCodOrgCompras = .Columns("COD_VALOR").Value
        Else
            sCodCentro = .Columns("COD_VALOR").Value
            sCodOrgCompras = .Columns("COD_VALOR").CellValue(.GetBookmark(-1))
        End If
        If sCodCentro = "" Then
            sCodCentro = "-1"
        End If
        Set oCentros = oFSGSRaiz.Generar_CCentros
        oCentros.CargarTodosLosCentros sCodCentro, sCodOrgCompras
        If oCentros.Count = 0 Then
            If .Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras And .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.Centro Then
                .MoveNext
            End If
            .Columns("COD_VALOR").Value = ""
            .Columns("VALOR").Value = ""
            g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorText = ""
            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.Almacen And .Columns("VALOR").CellValue(.GetBookmark(1)) <> "" Then
                .MoveNext
                .Columns("COD_VALOR").Value = ""
                .Columns("VALOR").Value = ""
                g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = Null
                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                .MovePrevious
            End If
            .Update
        Else
            'Eliminar ALMACEN
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.Almacen Then
                lIdAlmacen = .Columns("COD_VALOR").CellValue(.GetBookmark(1))
                Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
                oAlmacenes.CargarTodosLosAlmacenes , sCodCentro, , , lIdAlmacen
                If oAlmacenes.Count = 0 Then
                    .MoveNext
                    .Columns("COD_VALOR").Value = ""
                    .Columns("VALOR").Value = ""
                    g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = Null
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                    .Update
                    .MovePrevious
                End If

            End If
        End If
    
    ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.Almacen And .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoGS.Centro And .Columns("VALOR").Value <> "" Then
        lIdAlmacen = .Columns("COD_VALOR").Value
        sCodCentro = .Columns("COD_VALOR").CellValue(.GetBookmark(-1))
        Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
        oAlmacenes.CargarTodosLosAlmacenes , sCodCentro, , , lIdAlmacen
        If oAlmacenes.Count = 0 Then
            .Columns("COD_VALOR").Value = ""
            .Columns("VALOR").Value = ""
            g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = Null
            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
            .Update
        End If
    
    Else
        'Si movemos un campo culquiera y ponemos juntos el Centro y Almacen
        If .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.Centro And .Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoGS.Almacen Then
            If .Columns("COD_VALOR").CellValue(.GetBookmark(2)) <> "" Then
                sCodCentro = .Columns("COD_VALOR").CellValue(.GetBookmark(1))
                lIdAlmacen = .Columns("COD_VALOR").CellValue(.GetBookmark(2))
                Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
                oAlmacenes.CargarTodosLosAlmacenes , sCodCentro, , , lIdAlmacen
                If oAlmacenes.Count = 0 Then
                    .MoveNext
                    .MoveNext
                    .Columns("COD_VALOR").Value = ""
                    .Columns("VALOR").Value = ""
                    g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = Null
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                    .MovePrevious
                    .MovePrevious
                End If
                .Update
            End If
        ElseIf .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.OrganizacionCompras And .Columns("CAMPO_GS").CellValue(.GetBookmark(2)) = TipoCampoGS.Centro And .Columns("VALOR").CellValue(.GetBookmark(2)) <> "" Then
            sCodOrgCompras = .Columns("COD_VALOR").CellValue(.GetBookmark(1))
            sCodCentro = .Columns("COD_VALOR").CellValue(.GetBookmark(2))
            Set oCentros = oFSGSRaiz.Generar_CCentros
            oCentros.CargarTodosLosCentros sCodCentro, sCodOrgCompras
        
            If oCentros.Count = 0 Then
                'Eliminar CENTRO
                .MoveNext
                .MoveNext
                .Columns("COD_VALOR").Value = ""
                .Columns("VALOR").Value = ""
                g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorText = ""
                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                If .Columns("CAMPO_GS").CellValue(.GetBookmark(1)) = TipoCampoGS.Almacen And .Columns("VALOR").CellValue(.GetBookmark(1)) <> "" Then
                    'Eliminar ALMACEN
                    .MoveNext
                    .Columns("COD_VALOR").Value = ""
                    .Columns("VALOR").Value = ""
                    g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = Null
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                    .MovePrevious
                End If
                .MovePrevious
                .MovePrevious
                .Update
            Else
                'Eliminar ALMACEN
                If .Columns("CAMPO_GS").CellValue(.GetBookmark(3)) = TipoCampoGS.Almacen And .Columns("VALOR").CellValue(.GetBookmark(3)) <> "" Then
                    lIdAlmacen = .Columns("COD_VALOR").CellValue(.GetBookmark(3))
                    Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
                    oAlmacenes.CargarTodosLosAlmacenes , sCodCentro, , , lIdAlmacen
                    If oAlmacenes.Count = 0 Then
                        .MoveNext
                        .Columns("COD_VALOR").Value = ""
                        .Columns("VALOR").Value = ""
                        g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = Null
                        Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                        .Update
                        .MovePrevious
                    End If
                End If
            End If
        End If
    End If
    
    Set oAlmacenes = Nothing
    Set oCentros = Nothing
    
    End With
End Sub

''' <summary>
''' Carga la pagina
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub Form_Load()
    Dim oIdioma As CIdioma
    Dim i As Integer
    Dim iPosition As Integer
    Dim iValor As Integer
       
    Set m_oTablaExterna = oFSGSRaiz.Generar_CTablaExterna
    
    Me.Height = 7905
    Me.Width = 10485
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n):
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    PonerFieldSeparator Me
    
    With sdbgCampos
        i = .Columns.Count '- 1
        iPosition = 2
        
        .Columns.Add i
        .Columns(i).Name = gParametrosInstalacion.gIdioma
        .Columns(i).caption = m_sDato
        .Columns(i).Style = ssStyleEditButton
        .Columns(i).ButtonsAlways = True
        .Columns(i).Position = iPosition
        .Columns(i).FieldLen = 300
        If Not m_bModif Then
            .Columns(i).Locked = True
            .Columns("VALOR").Locked = True
        End If
                
        iValor = ssdbValorMultiIdi.Columns.Count
        ssdbValorMultiIdi.Columns.Add iValor
        ssdbValorMultiIdi.Columns(iValor).Name = gParametrosInstalacion.gIdioma
        ssdbValorMultiIdi.Columns(iValor).caption = m_oIdiomas.Item(gParametrosInstalacion.gIdioma).Den
        
        For Each oIdioma In m_oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                i = i + 1
                iPosition = iPosition + 1
                .Columns.Add i
                .Columns(i).Name = oIdioma.Cod
                .Columns(i).caption = oIdioma.Den
                .Columns(i).Style = ssStyleEditButton
                .Columns(i).ButtonsAlways = True
                .Columns(i).Position = iPosition
                .Columns(i).Visible = False
                .Columns(i).FieldLen = 300
                If Not m_bModif Then .Columns(i).Locked = True
                
                iValor = iValor + 1
                ssdbValorMultiIdi.Columns.Add iValor
                ssdbValorMultiIdi.Columns(iValor).Name = oIdioma.Cod
                ssdbValorMultiIdi.Columns(iValor).caption = oIdioma.Den
            End If
        Next
        
        i = i + 1
        iPosition = iPosition + 1
        'A�ado la columna de id de tabla externa
        .Columns.Add i
        .Columns(i).Name = "IDTABLAEXTERNA"
        .Columns(i).Position = iPosition
        .Columns(i).Visible = False
    End With
    
    sdbddDestinos.AddItem ""
    sdbddPagos.AddItem ""
    sdbddValor.AddItem ""
    sdbddOtrosCombos.AddItem ""
    sdbddAlmacenes.AddItem ""
    sdbddArticulos.AddItem ""
    sdbddTiposPedido.AddItem ""
    sdbddCompradores.AddItem ""
    sdbddEmpresas.AddItem ""
    
    Set m_oPagos = Nothing
    Set m_oPagos = oFSGSRaiz.Generar_CPagos
    
    Set m_oDestinos = Nothing
    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
    
    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises
    
    Set m_oUnidades = Nothing
    Set m_oUnidades = oFSGSRaiz.Generar_CUnidades
    
    Set m_oMonedas = Nothing
    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
    m_oMonedas.CargarTodasLasMonedas , , , , , False, True
    
    Set m_oTiposPedido = Nothing
    Set m_oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    'Cargamos todos los tipos de pedidos.
    m_oTiposPedido.CargarTodosLosTiposPedidos gParametrosInstalacion.gIdioma
    
    Set g_oFormularios = Nothing
    Set g_oFormularios = oFSGSRaiz.Generar_CFormularios
    
    'Cargo el combo de las fechas de inicio y fin de suministro y de necesidad:
    For i = 1 To 17
        sdbddFecha.AddItem i & Chr(m_lSeparador) & m_sIdiTipoFecha(i)
    Next i
    
    'Si tiene acceso b�sico al FSWS (solo solicitudes de compras) no ver� el bot�n para crear campos calculados:
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
        cmdAnyaCalculo.Visible = False
    End If
    
    If m_bConDesgloseFactura Or m_bConDesgloseDePedido Then
        Me.cmdAnyaGr.Enabled = False
        Me.cmdMoverGrupo.Enabled = False
    End If
    
    g_Accion = ACCFormularioCons
    
    'Forzar el BeforeUpdate
    g_bUpdate = True
End Sub

''' <summary>
''' Carga los recursos
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo:0</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Dim oIdi As CIdioma

    On Error Resume Next
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FORMULARIOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value   '1 Formularios
        Ador.MoveNext
        lblFormulario.caption = Ador(0).Value & ":"  '2 Formulario
        m_sForm = Ador(0).Value
        Ador.MoveNext
        chkMultiidioma.caption = Ador(0).Value '3 Multiidioma
        Ador.MoveNext
        'Eliminado lblMoneda.caption = Ador(0).Value  '4 Moneda
        Ador.MoveNext
        cmdAnyaGr.caption = Ador(0).Value  '5 A�adir grupo
        Ador.MoveNext
        cmdModifGr.caption = Ador(0).Value  '6 Modificar grupo
        Ador.MoveNext
        cmdEliminarGr.caption = Ador(0).Value  '7 Eliminar grupo
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value  '8 Restaurar
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value  '9 Listado
        Ador.MoveNext
        cmdAnyaForm.ToolTipText = Ador(0).Value  '10 A�adir formulario
        Ador.MoveNext
        cmdElimForm.ToolTipText = Ador(0).Value  '11 Eliminar formulario
        Ador.MoveNext
        cmdModifForm.ToolTipText = Ador(0).Value  '12 Modificar formulario
        Ador.MoveNext
        m_sDatosGen = Ador(0).Value '13 Datos generales
        ssTabGrupos.Tabs(1).caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyaItem.ToolTipText = Ador(0).Value ' 14 A�adir campo
        Ador.MoveNext
        cmdElimItem.ToolTipText = Ador(0).Value ' 15 Eliminar campo
        Ador.MoveNext
        m_sGrupo = Ador(0).Value  '16 Grupo
        Ador.MoveNext
        m_sDato = Ador(0).Value '17 Dato
        Ador.MoveNext
        sdbgCampos.Columns("VALOR").caption = Ador(0).Value '18 Valor
        Ador.MoveNext
        sdbgCampos.Columns("AYUDA").caption = Ador(0).Value '19 Ayuda
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value  '20 Deshacer
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value '21 Si
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value '22 No
        Ador.MoveNext
        m_sIdiDesglose = Ador(0).Value '23 (Pulse sobre el bot�n para acceder al desglose)
        Ador.MoveNext
        sdbddPagos.Columns("COD").caption = Ador(0).Value  '24 C�digo
        sdbddDestinos.Columns("COD").caption = Ador(0).Value  '24 C�digo
        sdbddOtrosCombos.Columns("COD").caption = Ador(0).Value
        sdbddAlmacenes.Columns("COD").caption = Ador(0).Value
        sdbddArticulos.Columns("COD").caption = Ador(0).Value
        sdbddTiposPedido.Columns("COD").caption = Ador(0).Value
        sdbddCompradores.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbddPagos.Columns("DEN1").caption = Ador(0).Value   '25 Denominaci�n
        sdbddPagos.Columns("DEN2").caption = Ador(0).Value
        sdbddPagos.Columns("DEN3").caption = Ador(0).Value
        sdbddDestinos.Columns("DEN1").caption = Ador(0).Value
        sdbddDestinos.Columns("DEN2").caption = Ador(0).Value
        sdbddDestinos.Columns("DEN3").caption = Ador(0).Value
        sdbddOtrosCombos.Columns("DEN").caption = Ador(0).Value
        sdbddOtrosCombos.Columns("DEN2").caption = Ador(0).Value
        sdbddOtrosCombos.Columns("DEN3").caption = Ador(0).Value
        sdbddAlmacenes.Columns("DEN").caption = Ador(0).Value
        sdbddArticulos.Columns("DEN").caption = Ador(0).Value
        sdbddTiposPedido.Columns("DEN").caption = Ador(0).Value
        sdbddCompradores.Columns("DEN").caption = Ador(0).Value
        sdbddEmpresas.Columns("COD").caption = Ador(0).Value
        m_sDen = Ador(0).Value
        Ador.MoveNext
        sdbddDestinos.Columns("DIR").caption = Ador(0).Value  ' 26 Direcci�n
        Ador.MoveNext
        sdbddDestinos.Columns("POB").caption = Ador(0).Value  ' 27 Poblaci�n
        Ador.MoveNext
        sdbddDestinos.Columns("CP").caption = Ador(0).Value  ' 28 C�digo postal
        Ador.MoveNext
        sdbddDestinos.Columns("PAI").caption = Ador(0).Value  ' 29 Pais
        Ador.MoveNext
        sdbddDestinos.Columns("PROVI").caption = Ador(0).Value  ' 30 Provincia
        Ador.MoveNext
        m_sIdiKB = Ador(0).Value '31 KB
        
        For i = 1 To 17
            Ador.MoveNext
            m_sIdiTipoFecha(i) = Ador(0).Value
        Next i
        
        For i = 1 To 7
            Ador.MoveNext
            m_sMensaje(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        Ador.MoveNext
        cmdMoverGrupo.caption = Ador(0).Value  '58 Mover &grupo
        Ador.MoveNext
        cmdAnyaCalculo.ToolTipText = Ador(0).Value  '59 Campos calculados
        Ador.MoveNext
        m_sIdiErrorEvaluacion(1) = Ador(0).Value  '60 Error al realizar el c�lculo:F�rmula inv�lida.
        Ador.MoveNext
        m_sIdiErrorEvaluacion(2) = Ador(0).Value  '61 Error al realizar el c�lculo:Valores incorrectos.
        
        For i = 1 To 10
            Ador.MoveNext
            m_sCamposGS(i) = Ador(0).Value
        Next i
        
        For i = 1 To 9
            Ador.MoveNext
            m_sCamposSC(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sCamposGS(11) = Ador(0).Value
        Ador.MoveNext
        m_sCamposGS(12) = Ador(0).Value  '82 Denominacion de articulo
        Ador.MoveNext
        m_sCamposGS(13) = Ador(0).Value  'Numero Solicitud ERP
        Ador.MoveNext
        m_sCamposGS(14) = Ador(0).Value  '84 Unidad organizativa
        Ador.MoveNext
        m_sCamposGS(15) = Ador(0).Value  '85 Departamento
        Ador.MoveNext
        m_sCamposGS(16) = Ador(0).Value  '86 Organizaci�n de compras
        Ador.MoveNext
        m_sCamposGS(17) = Ador(0).Value  '87 Centro
        Ador.MoveNext
        m_sCamposGS(18) = Ador(0).Value  '88 Almac�n
        Ador.MoveNext
        m_sCamposGS(19) = Ador(0).Value  '89 Importe solicitudes vinculadas
        Ador.MoveNext
        m_sCamposGS(20) = Ador(0).Value  '90 Referencia a solicitudes
        Ador.MoveNext
        sdbgCampos.Columns("ATRIBUTO").caption = Ador(0).Value '91 Atributo
        Ador.MoveNext
        sdbgCampos.Columns("APROCESO").caption = Ador(0).Value '92 Proceso
        Ador.MoveNext
        sdbgCampos.Columns("APEDIDO").caption = Ador(0).Value '93 Pedido
        Ador.MoveNext
        m_sCamposGS(21) = Ador(0).Value  '94 Centro de coste
        Ador.MoveNext
        m_sCamposGS(22) = Ador(0).Value  '95 Activo
        Ador.MoveNext
        m_sCamposGS(23) = Ador(0).Value  '96 Tipo de pedido
        'Para los tipos de pedidos
        Ador.MoveNext
        m_arrConcep(0) = Ador(0).Value '97 Gasto
        Ador.MoveNext
        m_arrConcep(1) = Ador(0).Value  '98 Inversi�n
        Ador.MoveNext
        m_arrConcep(2) = Ador(0).Value  '99 Gas./Inv.
        Ador.MoveNext
        m_arrAlmac(1) = Ador(0).Value   '100 Obligatorio
        m_arrRecep(1) = Ador(0).Value
        Ador.MoveNext
        m_arrAlmac(0) = Ador(0).Value   '101 No almacenable
        Ador.MoveNext
        m_arrAlmac(2) = Ador(0).Value   '102 Opcional
        m_arrRecep(2) = Ador(0).Value
        Ador.MoveNext
        m_arrRecep(0) = Ador(0).Value   '103 No recepci�n
        Ador.MoveNext
        sdbddTiposPedido.Columns("CONCEPTO").caption = Ador(0).Value  '104
        Ador.MoveNext
        sdbddTiposPedido.Columns("ALMACEN").caption = Ador(0).Value  '105
        Ador.MoveNext
        sdbddTiposPedido.Columns("RECEPCION").caption = Ador(0).Value  '106
        Ador.MoveNext
        m_sCamposGS(24) = Ador(0).Value  '107  "Desglose de actividad"
        Ador.MoveNext
        m_sCamposGS(25) = Ador(0).Value  '108  "Desglose de factura"
        Ador.MoveNext
        m_sCamposGS(26) = Ador(0).Value  '109  "Factura"
        Ador.MoveNext
        m_sCamposGS(27) = Ador(0).Value  '110  "Inicio abono"
        Ador.MoveNext
        m_sCamposGS(28) = Ador(0).Value  '111  "Fin abono"
        Ador.MoveNext
        m_sCamposGS(29) = Ador(0).Value  '112  "Retencion en Garantia"
        Ador.MoveNext
        m_sCamposGS(30) = Ador(0).Value  '113  "Unidad de pedido"
        Ador.MoveNext
        m_sCamposGS(31) = Ador(0).Value  '114  "Proveedor ERP"
        Ador.MoveNext
        m_sCamposGS(32) = Ador(0).Value  '115  "Comprador"
        Ador.MoveNext
        m_sCamposGS(33) = Ador(0).Value  '116  "Desglose de pedido"
        Ador.MoveNext
        m_sImposibleAnyadircampo = Ador(0).Value
        Ador.MoveNext
        m_sNoPuedenExistirDosCampoMoneda = Ador(0).Value
        m_sMensaje(8) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(9) = Ador(0).Value   'El siguiente dato no es v�lido: Campo con formato debe tener el formato correcto.
        Ador.MoveNext
        m_sCamposGS(34) = Ador(0).Value 'Empresa
        Ador.MoveNext
        sdbddEmpresas.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        m_sCamposGS(35) = Ador(0).Value 'A�o de Imputaci�n
        Ador.Close
        
        'Se cargan los t�tulos de las columnas en los distintos idioma para DESTINOS, PAGOS
        i = 1
        For Each oIdi In m_oIdiomas
            If oIdi.Cod = gParametrosInstalacion.gIdioma Then
                sdbddDestinos.Columns(i).caption = sdbddDestinos.Columns(i).caption & " (" & oIdi.Den & ")"
                sdbddDestinos.Columns(i).Name = "DEN_" & gParametrosInstalacion.gIdioma
                sdbddPagos.Columns(i).caption = sdbddPagos.Columns(i).caption & " (" & oIdi.Den & ")"
                sdbddPagos.Columns(i).Name = "DEN_" & gParametrosInstalacion.gIdioma
                sdbddOtrosCombos.Columns(i).caption = sdbddOtrosCombos.Columns(i).caption & " (" & oIdi.Den & ")"
                m_sDenApli = sdbddOtrosCombos.Columns(i).caption
                i = i + 1
                Exit For
            End If
        Next
        
        For Each oIdi In m_oIdiomas
            If oIdi.Cod <> gParametrosInstalacion.gIdioma Then
                sdbddDestinos.Columns(i).caption = sdbddDestinos.Columns(i).caption & " (" & oIdi.Den & ")"
                sdbddDestinos.Columns(i).Name = "DEN_" & oIdi.Cod
                sdbddPagos.Columns(i).caption = sdbddPagos.Columns(i).caption & " (" & oIdi.Den & ")"
                sdbddPagos.Columns(i).Name = "DEN_" & oIdi.Cod
                sdbddOtrosCombos.Columns(i).caption = sdbddOtrosCombos.Columns(i).caption & " (" & oIdi.Den & ")"
                i = i + 1
            End If
        Next
    End If
    Set Ador = Nothing
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Arrange()
Dim oIdioma As CIdioma
Dim dblWGrid As Double

With sdbgCampos
    If Me.Height < 3000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    ssTabGrupos.Height = Me.Height - 1770
    ssTabGrupos.Width = Me.Width - 330
    
    If m_bModif = False Then
        .Height = ssTabGrupos.Height - 985 + picItems.Height
    Else
        .Height = ssTabGrupos.Height - 985
    End If
    
    If .Rows > 0 Then
        Dim vbm As Variant
        vbm = .Bookmark
        .Bookmark = 0
    End If
    
    .Width = ssTabGrupos.Width - 305
    
    .Columns("AYUDA").Width = .Width / 12
    .Columns("VALOR").Width = .Width / 5
    sdbddValor.Columns(0).Width = .Columns("VALOR").Width
    
    dblWGrid = .Width - .Columns("ATRIBUTO").Width - .Columns("VALOR").Width - .Columns("APROCESO").Width - .Columns("APEDIDO").Width - .Columns("AYUDA").Width - 600
    
    If Not m_oIdiomas Is Nothing Then
        If Not g_oFormSeleccionado Is Nothing Then
            If g_oFormSeleccionado.Multiidioma = True Then
                For Each oIdioma In m_oIdiomas
                    .Columns(oIdioma.Cod).Width = dblWGrid / m_oIdiomas.Count
                Next
            Else
                .Columns(gParametrosInstalacion.gIdioma).Width = dblWGrid
            End If
        Else
            .Columns(gParametrosInstalacion.gIdioma).Width = dblWGrid
        End If
    End If
    
    If .Rows > 0 Then
        'Recup�ro el bookmark y si no se ve la fila la pongo la priemra visible
        .Bookmark = vbm
        If .AddItemRowIndex(.Bookmark) < .AddItemRowIndex(.FirstRow) Or .AddItemRowIndex(.Bookmark) > .AddItemRowIndex(.FirstRow) + .VisibleRows - 1 Then
            .FirstRow = .Bookmark
        End If
    End If
    picNavigate.Top = ssTabGrupos.Top + ssTabGrupos.Height + 85
End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'Si hay cambios sin guardar
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
    End If

    g_strPK = ""
    
    Set g_oFormularios = Nothing
    Set g_oFormSeleccionado = Nothing
    Set g_oGrupoSeleccionado = Nothing
    Set m_oCampoEnEdicion = Nothing
    Set m_oIBaseDatos = Nothing
    Set m_oPagos = Nothing
    Set m_oDestinos = Nothing
    Set m_oMonedas = Nothing
    Set m_oMonSeleccionada = Nothing
    Set m_oPaises = Nothing
    Set m_oUnidades = Nothing
    Set m_oTiposPedido = Nothing
    Set m_oTablaExterna = Nothing
    
    If Not g_ofrmDesglose Is Nothing Then
        Unload g_ofrmDesglose
        Set g_ofrmDesglose = Nothing
    End If
            
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If
    
    Me.Visible = False
    Set m_oIdiomas = Nothing
End Sub

Private Sub sdbcFormulario_Change()
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        LimpiarCampos
        Set g_oFormSeleccionado = Nothing
        m_bRespetarCombo = False
        
        If sdbcFormulario <> "" Then m_bCargarComboDesde = True
                
        cmdElimForm.Enabled = False
        cmdModifForm.Enabled = False
        cmdAnyaCalculo.Enabled = False
        cmdAnyaGr.Enabled = False
        cmdEliminarGr.Enabled = False
        cmdModifGr.Enabled = False
        cmdMoverGrupo.Enabled = False
        cmdRestaurar.Enabled = False
        cmdListado.Enabled = False
    End If
End Sub

Private Sub sdbcFormulario_CloseUp()
    If sdbcFormulario.Value = "" Then Exit Sub
    Set g_oFormSeleccionado = g_oFormularios.Item(sdbcFormulario.Columns(0).Text)
    FormularioSeleccionado
    m_bCargarComboDesde = False
End Sub


Private Sub sdbcFormulario_DropDown()
    Dim oForm As CFormulario
    Screen.MousePointer = vbHourglass
    
    Set g_oFormularios = Nothing
    Set g_oFormularios = oFSGSRaiz.Generar_CFormularios
    
    sdbcFormulario.RemoveAll
    
    If m_bCargarComboDesde Then
        g_oFormularios.CargarTodosFormularios Trim(sdbcFormulario.Text)
    Else
        g_oFormularios.CargarTodosFormularios
    End If
    
    For Each oForm In g_oFormularios
        sdbcFormulario.AddItem oForm.Id & Chr(m_lSeparador) & oForm.Den
    Next
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFormulario_InitColumnProps()
    sdbcFormulario.DataFieldList = "Column 1"
    sdbcFormulario.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcFormulario_PositionList(ByVal Text As String)
PositionList sdbcFormulario, Text
End Sub

Private Sub FormularioSeleccionado(Optional ByVal iTab As Integer)
    If g_oFormSeleccionado Is Nothing Then Exit Sub
    
    'A�ade un tab para cada grupo:
    LockWindowUpdate Me.hWnd
    LimpiarCampos
    m_bCargando = True
    
    'Multiidioma
    chkMultiidioma.Enabled = True
    If g_oFormSeleccionado.Multiidioma = True Then
        chkMultiidioma.Value = vbChecked
    Else
        chkMultiidioma.Value = vbUnchecked
    End If

    RedimensionarColumnasMultiIdi
    CargarGruposEnTab
    
    If iTab = 0 Then ssTabGrupos.Tabs(1).Selected = True
    
    cmdElimForm.Enabled = True
    cmdModifForm.Enabled = True
    cmdAnyaCalculo.Enabled = True
    
    If g_oFormSeleccionado.Grupos.Count > 0 Then
        cmdModifGr.Enabled = True
        cmdMoverGrupo.Enabled = True
        If g_oFormSeleccionado.Grupos.Count > 1 Then
            cmdEliminarGr.Enabled = True
        End If
    Else
        cmdEliminarGr.Enabled = False
        cmdModifGr.Enabled = False
        cmdMoverGrupo.Enabled = False
    End If
    
    If m_bConDesgloseFactura Or m_bConDesgloseDePedido Then
        cmdAnyaGr.Enabled = False
        cmdMoverGrupo.Enabled = False
    Else
        cmdAnyaGr.Enabled = True
        cmdMoverGrupo.Enabled = True
    End If
    
    cmdRestaurar.Enabled = True
    cmdListado.Enabled = True
    cmdAnyaItem.Enabled = True
    cmdElimItem.Enabled = True
    cmdSubir.Enabled = True
    cmdBajar.Enabled = True
    
    m_bCargando = False
    LockWindowUpdate 0&
End Sub

Public Sub AnyaModifFormulario(Optional ByVal Id As Long)
    If Id <> 0 Then
        sdbcFormulario.Columns(0).Value = Id
        sdbcFormulario.Columns(1).Value = g_oFormularios.Item(CStr(Id)).Den
        sdbcFormulario.Text = g_oFormularios.Item(CStr(Id)).Den
        Set g_oFormSeleccionado = g_oFormularios.Item(CStr(Id))
        FormularioSeleccionado
    Else
        m_bRespetarCombo = True
        sdbcFormulario.Text = g_oFormSeleccionado.Den
        m_bRespetarCombo = False
    End If
End Sub

Private Sub sdbddAlmacenes_CloseUp()
    sdbgCampos.Columns("COD_VALOR").Value = sdbddAlmacenes.Columns("ID").Value
      
    If sdbddAlmacenes.Columns("COD").Value <> "" Or sdbddAlmacenes.Columns("DEN").Value <> "" Then
        sdbgCampos.Columns("VALOR").Value = sdbddAlmacenes.Columns("COD").Value & " - " & sdbddAlmacenes.Columns("DEN").Value
    End If
End Sub

Private Sub sdbddAlmacenes_DropDown()
    Dim vbm As Variant
    Dim oAlmacen As CAlmacen
    
    If Not m_bModif Then
        sdbddAlmacenes.DroppedDown = False
        Exit Sub
    End If
    
    sdbddAlmacenes.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
        Dim oAlmacenes As CAlmacenes
        Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
        Dim sCodCentro As String

        vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Centro Then
            sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(vbm)
            oAlmacenes.CargarTodosLosAlmacenes , sCodCentro
        Else
            oAlmacenes.CargarTodosLosAlmacenes
        End If

        For Each oAlmacen In oAlmacenes
            sdbddAlmacenes.AddItem oAlmacen.Id & Chr(m_lSeparador) & oAlmacen.Cod & Chr(m_lSeparador) & oAlmacen.Den
        Next
        Set oAlmacenes = Nothing
    End If
    
    If sdbddAlmacenes.Rows = 0 Then sdbddAlmacenes.AddItem ""

    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbddAlmacenes_InitColumnProps()
    sdbddAlmacenes.DataFieldList = "Column 0"
    sdbddAlmacenes.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbddAlmacenes_PositionList(ByVal Text As String)
    PositionList sdbddAlmacenes, Text
End Sub
Private Sub sdbddEmpresas_CloseUp()
    sdbgCampos.Columns("COD_VALOR").Value = sdbddEmpresas.Columns("ID").Value
    If sdbddEmpresas.Columns("COD").Value <> "" Or sdbddEmpresas.Columns("DEN").Value <> "" Then
        sdbgCampos.Columns("VALOR").Value = sdbddEmpresas.Columns("COD").Value & " - " & sdbddEmpresas.Columns("DEN").Value
    End If
End Sub
Private Sub sdbddEmpresas_DropDown()
    Dim vbm As Variant
    Dim oEmpresa As CEmpresa
    
    If Not m_bModif Then
        sdbddEmpresas.DroppedDown = False
        Exit Sub
    End If
    
    sdbddEmpresas.RemoveAll
    Screen.MousePointer = vbHourglass
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Empresa Then
        Dim oEmpresas As CEmpresas
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        Dim sCodCentro As String

        vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
        'MIRAR RESTRICCIONES=>MIRAR EMISION????
        Dim rs As Recordset
        Set rs = oEmpresas.DevolverTodasEmpresas(0)

        While Not rs.EOF
            sdbddEmpresas.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("NIF").Value & Chr(m_lSeparador) & rs("DEN").Value
            rs.MoveNext
        Wend
        Set oEmpresas = Nothing
    End If
    
    If sdbddEmpresas.Rows = 0 Then
        sdbddEmpresas.AddItem ""
    End If
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddEmpresas_InitColumnProps()
    sdbddEmpresas.DataFieldList = "Column 0"
    sdbddEmpresas.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddEmpresas_PositionList(ByVal Text As String)
    PositionList sdbddEmpresas, Text
End Sub

Private Sub sdbddCompradores_CloseUp()
    Dim Den As String
    Dim i As Integer
    sdbgCampos.Columns("COD_VALOR").Value = sdbddCompradores.Columns("COD").Value
      
    If sdbddCompradores.Columns("COD").Value <> "" Or sdbddCompradores.Columns("DEN").Value <> "" Then
        Den = sdbddCompradores.Columns("DEN").Value
        i = InStr(Den, " - ")
        sdbgCampos.Columns("VALOR").Value = Right$(Den, Len(Den) - (i + 2))
    End If
End Sub

Private Sub sdbddCompradores_DropDown()
    Dim oCompradores As CCompradores
    Set oCompradores = oFSGSRaiz.Generar_CCompradores
    Dim lIdPerfil As Long
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    sdbddCompradores.RemoveAll
    
    If oUsuarioSummit.Pyme <> 0 Then
        oCompradores.CargarTodosLosCompradoresPorUON , , , , , , , True, "NOM", False, oUsuarioSummit.Pyme, m_bRPerfUON, lIdPerfil
    Else
        oCompradores.CargarTodosLosCompradores , , , True, True, False, False, False, False, True
    End If
    
    Dim oComprador As CComprador
    For Each oComprador In oCompradores
        sdbddCompradores.AddItem oComprador.Cod & Chr(m_lSeparador) & oComprador.Cod & " - " & oComprador.nombre & " " & oComprador.Apel
    Next

    Set oCompradores = Nothing
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
End Sub

Private Sub sdbddCompradores_InitColumnProps()
    sdbddCompradores.DataFieldList = "Column 0"
    sdbddCompradores.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddCompradores_PositionList(ByVal Text As String)
PositionList sdbddCompradores, Text
End Sub

Private Sub sdbddDestinos_CloseUp()
    sdbgCampos.Columns("COD_VALOR").Value = sdbddDestinos.Columns("COD").Value
    sdbgCampos.Columns("VALOR").Value = sdbddDestinos.Columns("COD").Value & " - " & sdbddDestinos.Columns("DEN_" & gParametrosInstalacion.gIdioma).Value
End Sub

Private Sub sdbddDestinos_DropDown()
    Dim ADORs As Ador.Recordset
    Dim oIdioma As CIdioma
    Dim sDen As String
    
    ''' * Objetivo: Abrir el combo de destinos de la forma adecuada
    If Not m_bModif Then
        sdbddDestinos.DroppedDown = False
        Exit Sub
    End If
    
    sdbddDestinos.RemoveAll
    
    If Not oUsuarioSummit.Persona Is Nothing Then
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
    Else
        Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest)
    End If
    
    If ADORs Is Nothing Then
        sdbddDestinos.RemoveAll
        Screen.MousePointer = vbNormal
        sdbddDestinos.AddItem ""
        sdbgCampos.ActiveCell.SelStart = 0
        sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sDen = ""
        sDen = sDen & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador)
        For Each oIdioma In m_oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                sDen = sDen & ADORs("DEN_" & oIdioma.Cod).Value & Chr(m_lSeparador)
            End If
        Next
        sdbddDestinos.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & sDen & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If chkMultiidioma.Value = 0 Then
        sdbddDestinos.Columns(1).caption = m_sDen
        sdbddDestinos.Columns(2).Visible = False
        sdbddDestinos.Columns(3).Visible = False
    End If
    
    If sdbddDestinos.Rows = 0 Then
        sdbddDestinos.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)

End Sub

Private Sub sdbddDestinos_InitColumnProps()
    sdbddDestinos.DataFieldList = "Column 0"
    sdbddDestinos.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddDestinos_PositionList(ByVal Text As String)
PositionList sdbddDestinos, Text
End Sub
Private Sub sdbddFecha_CloseUp()
    sdbgCampos.Columns("COD_VALOR").Value = sdbddFecha.Columns("ID").Value
End Sub
Private Sub sdbddFecha_InitColumnProps()
    sdbddFecha.DataFieldList = "Column 1"
    sdbddFecha.DataFieldToDisplay = "Column 1"
End Sub
Private Sub sdbddFecha_PositionList(ByVal Text As String)
PositionList sdbddFecha, Text, 1
End Sub

Private Sub sdbddOtrosCombos_CloseUp()
    Dim bCambiado As Boolean
    Dim lRowInit As Long
    Dim i As Integer

With sdbgCampos
    
    bCambiado = Not (.Columns("COD_VALOR").Value = sdbddOtrosCombos.Columns("COD").Value)

    .Columns("COD_VALOR").Value = sdbddOtrosCombos.Columns("COD").Value

    If sdbddOtrosCombos.Columns("COD").Value <> "" Or sdbddOtrosCombos.Columns("DEN").Value <> "" Then
        .Columns("VALOR").Value = sdbddOtrosCombos.Columns("COD").Value & " - " & sdbddOtrosCombos.Columns("DEN").Value
    End If

    
    If .Columns("CAMPO_GS").Value = TipoCampoGS.Pais Then
        .MoveNext
        If .Columns("CAMPO_GS").Value = TipoCampoGS.Provincia Then
            .Columns("COD_VALOR").Value = ""
            .Columns("VALOR").Value = ""
        End If
        .MovePrevious
    End If
    
    If .Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras Then
        If bCambiado Then
            .MoveNext
            If .Columns("CAMPO_GS").Value = TipoCampoGS.Centro Then
                .Columns("COD_VALOR").Value = ""
                .Columns("VALOR").Value = ""
                g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorText = ""
                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                .Update
                .MoveNext
                If .Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
                    .Columns("COD_VALOR").Value = ""
                    .Columns("VALOR").Value = ""
                    g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = ""
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                    .Update
                ElseIf (.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose) Or (.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose) Then
                    g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).EliminarArticulosEnDesglose
                End If
                .MovePrevious
            ElseIf (.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose) Or (.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose) Then
                g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).EliminarArticulosEnDesglose
            End If
            'Borrar el articulo
            lRowInit = .Row
            For i = lRowInit To .Rows - 1
                .Row = i
                If .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                    .Columns("VALOR").Value = ""
                    .Columns("COD_VALOR").Value = ""
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                    .Row = i + 1
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                        .Columns("VALOR").Value = ""
                        .Columns("COD_VALOR").Value = ""
                    End If
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                    Exit For
                End If
            Next i
            .Row = lRowInit
            
            .MovePrevious
            'Quitamos el valor del campoGS ProveedorERP si estuviese en el grupo
            AccionSobreCampoGsProveedorERP AccionSobreCampoProveedorERP.EliminarValor, TipoCampoGS.OrganizacionCompras
        End If
     ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.Centro Then
        If bCambiado Then
             g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorText = ""
             Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
             .Update
             .MoveNext
             If .Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
                 .Columns("COD_VALOR").Value = ""
                 .Columns("VALOR").Value = ""
                 g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = ""
                 Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                 .Update
             End If
            'Borrar el articulo
            If .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras Then
                If .Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
                    g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).EliminarArticulosEnDesglose
                Else
                    lRowInit = .Row
                    For i = lRowInit To .Rows - 1
                        .Row = i
                        If .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                            .Columns("VALOR").Value = ""
                            .Columns("COD_VALOR").Value = ""
                            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                            .Row = i + 1
                            If .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                                .Columns("VALOR").Value = ""
                                .Columns("COD_VALOR").Value = ""
                            End If
                            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                            Exit For
                        End If
                    Next i
                    .Row = lRowInit
                End If

            End If
            .MovePrevious
            If .Columns("CAMPO_GS").Value = TipoCampoGS.Departamento Then
                .Columns("BAJALOG").Value = False
                .Columns("VALOR").CellStyleSet ""
                .Bookmark = .GetBookmark(0) 'Esto es necesario para refrescar el estilo
            End If
        End If
    End If
End With

End Sub

Private Sub sdbddOtrosCombos_DropDown()
Dim oPais As CPais
Dim oUnidad As CUnidad
Dim oProvi As CProvincia
Dim oIdioma As CIdioma
Dim sDen As String
Dim vbm As Variant
Dim oMonedas As CMonedas
Dim oMoneda As CMoneda
Dim oDepartamentos As CDepartamentos
Dim oDepartamento As CDepartamento
Dim oOrganizacionCompras As COrganizacionCompras
Dim oCentro As CCentro
Dim oUON As IUon

    If Not m_bModif Then
        sdbddOtrosCombos.DroppedDown = False
        Exit Sub
    End If
    
    sdbddOtrosCombos.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.Unidad And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.Moneda Then
        sdbddOtrosCombos.Columns(1).caption = m_sDen
        sdbddOtrosCombos.Columns(2).Visible = False
        sdbddOtrosCombos.Columns(3).Visible = False
    ElseIf Not sdbddOtrosCombos.Columns(2).Visible Then
        sdbddOtrosCombos.Columns(1).caption = m_sDenApli
        sdbddOtrosCombos.Columns(2).Visible = True
        sdbddOtrosCombos.Columns(3).Visible = True
    End If
    
    Select Case sdbgCampos.Columns("CAMPO_GS").Value
        Case TipoCampoGS.Pais
            m_oPaises.CargarTodosLosPaises
            
            For Each oPais In m_oPaises
                sdbddOtrosCombos.AddItem oPais.Cod & Chr(m_lSeparador) & oPais.Den
            Next
            
        Case TipoCampoGS.Unidad, TipoCampoGS.UnidadPedido
            m_oUnidades.CargarTodasLasUnidades
                
            For Each oUnidad In m_oUnidades
            sDen = ""
                sDen = sDen & Chr(m_lSeparador) & oUnidad.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                For Each oIdioma In m_oIdiomas
                    If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                        sDen = sDen & Chr(m_lSeparador) & oUnidad.Denominaciones.Item(oIdioma.Cod).Den
                    End If
                Next
                sdbddOtrosCombos.AddItem oUnidad.Cod & sDen
            Next
        
            If chkMultiidioma.Value = 0 Then
                sdbddOtrosCombos.Columns(1).caption = m_sDen
                sdbddOtrosCombos.Columns(2).Visible = False
                sdbddOtrosCombos.Columns(3).Visible = False
            End If
            
        Case TipoCampoGS.Provincia
            'Obtiene el pa�s seleccionado en la fila anterior.si no ha seleccionado pa�s no muestra las provincias.
            vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
            If sdbgCampos.Columns("COD_VALOR").CellValue(vbm) <> "" Then
                Set oPais = oFSGSRaiz.Generar_CPais
                oPais.Cod = sdbgCampos.Columns("COD_VALOR").CellValue(vbm)
                oPais.CargarTodasLasProvincias
                For Each oProvi In oPais.Provincias
                    sdbddOtrosCombos.AddItem oProvi.Cod & Chr(m_lSeparador) & oProvi.Den
                Next
                Set oPais = Nothing
            End If
            
        Case TipoCampoGS.Moneda
            Set oMonedas = oFSGSRaiz.Generar_CMonedas
            oMonedas.CargarTodasLasMonedas
            
            For Each oMoneda In m_oMonedas
            sDen = ""
                sDen = sDen & Chr(m_lSeparador) & oMoneda.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                For Each oIdioma In m_oIdiomas
                    If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                        sDen = sDen & Chr(m_lSeparador) & oMoneda.Denominaciones.Item(oIdioma.Cod).Den
                    End If
                Next
                sdbddOtrosCombos.AddItem oMoneda.Cod & sDen
            Next
        
            If chkMultiidioma.Value = 0 Then
                sdbddOtrosCombos.Columns(1).caption = m_sDen
                sdbddOtrosCombos.Columns(2).Visible = False
                sdbddOtrosCombos.Columns(3).Visible = False
            End If

            Set oMonedas = Nothing
            
        Case TipoCampoGS.Departamento
            vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
            If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) <> TipoCampoGS.UnidadOrganizativa Then
                'El campo Departamento es independiente a las unidades Organizativas
                Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
                oDepartamentos.CargarTodosLosDepartamentos
                For Each oDepartamento In oDepartamentos
                    sdbddOtrosCombos.AddItem oDepartamento.Cod & Chr(m_lSeparador) & oDepartamento.Den
                Next
                
                Set oDepartamentos = Nothing
            Else
                'El campo Departamento est� relacionado a las unidades Organizativas
                Dim vUnidadOrganizativa As Variant
                vUnidadOrganizativa = Split(sdbgCampos.Columns("valor").CellValue(vbm), " - ")
                ReDim Preserve vUnidadOrganizativa(UBound(vUnidadOrganizativa) - 1)
                ReDim Preserve vUnidadOrganizativa(3)
                Set oUON = createUon(vUnidadOrganizativa(0), vUnidadOrganizativa(1), vUnidadOrganizativa(2))
                oUON.CargarTodosLosDepartamentos
                For Each oDepartamento In oUON.Departamentos
                    sdbddOtrosCombos.AddItem oDepartamento.Cod & Chr(m_lSeparador) & oDepartamento.Den
                Next
                Set oUON = Nothing
            End If
            
            Case TipoCampoGS.OrganizacionCompras
                Dim oOrganizacionesCompras As COrganizacionesCompras
                Set oOrganizacionesCompras = oFSGSRaiz.Generar_COrganizacionesCompras
                
                oOrganizacionesCompras.CargarOrganizacionesCompra
                For Each oOrganizacionCompras In oOrganizacionesCompras.OrganizacionesCompras
                    sdbddOtrosCombos.AddItem oOrganizacionCompras.Cod & Chr(m_lSeparador) & oOrganizacionCompras.Den
                Next
                Set oOrganizacionesCompras = Nothing
                
            Case TipoCampoGS.Centro
                Dim oCentros As CCentros
                Set oCentros = oFSGSRaiz.Generar_CCentros
                Dim sCodOrganizacion As String
                vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
                If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.OrganizacionCompras Then
                    sCodOrganizacion = sdbgCampos.Columns("COD_VALOR").CellValue(vbm)
                    oCentros.CargarTodosLosCentros , sCodOrganizacion
                Else
                    oCentros.CargarTodosLosCentros
                End If
               
                For Each oCentro In oCentros.Centros
                    sdbddOtrosCombos.AddItem oCentro.Cod & Chr(m_lSeparador) & oCentro.Den
                Next
                Set oCentros = Nothing
           Case TipoCampoGS.ProveedorERP
                Dim oCampo As CFormItem
                Dim sOrgCompras As String
                Dim sCodProveedor As String
                'Miramos el valor del campo proveedor en el grupo
                For Each oCampo In g_oGrupoSeleccionado.Campos
                    Select Case oCampo.CampoGS
                        Case TipoCampoGS.Proveedor
                            sCodProveedor = NullToStr(oCampo.valorText)
                            Exit For
                    End Select
                Next
                'Miramos el valor de organizacion de compras a nivel del formulario
                sOrgCompras = ComprobarOrgCompras(True)
                If sCodProveedor <> "" And sOrgCompras <> "" Then
                    Dim oProveERPs As CProveERPs
                    Dim oProveERP As CProveERP
                    Set oProveERPs = oFSGSRaiz.Generar_CProveERPs
                    oProveERPs.CargarProveedoresERP , sCodProveedor, , , sOrgCompras, True
                    For Each oProveERP In oProveERPs
                        sdbddOtrosCombos.AddItem oProveERP.Cod & Chr(m_lSeparador) & oProveERP.Den
                    Next
                End If
    End Select
        
    If sdbddOtrosCombos.Rows = 0 Then sdbddOtrosCombos.AddItem ""
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbddOtrosCombos_InitColumnProps()
    sdbddOtrosCombos.DataFieldList = "Column 0"
    sdbddOtrosCombos.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbddOtrosCombos_PositionList(ByVal Text As String)
PositionList sdbddOtrosCombos, Text
End Sub
Private Sub sdbddPagos_CloseUp()
    sdbgCampos.Columns("COD_VALOR").Value = sdbddPagos.Columns("COD").Value
    sdbgCampos.Columns("VALOR").Value = sdbddPagos.Columns("COD").Value & " - " & sdbddPagos.Columns("DEN_" & gParametrosInstalacion.gIdioma).Value
End Sub
Private Sub sdbddPagos_DropDown()
Dim oPag As CPago
Dim oIdioma As CIdioma
Dim sDen As String

    If Not m_bModif Then
        sdbddPagos.DroppedDown = False
        Exit Sub
    End If

    m_oPagos.CargarTodosLosPagos
    sdbddPagos.RemoveAll

    For Each oPag In m_oPagos
        sDen = ""
        sDen = sDen & Chr(m_lSeparador) & oPag.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        For Each oIdioma In m_oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                sDen = sDen & Chr(m_lSeparador) & oPag.Denominaciones.Item(oIdioma.Cod).Den
            End If
        Next
        sdbddPagos.AddItem oPag.Cod & sDen
    Next
    
    If chkMultiidioma.Value = 0 Then
        sdbddPagos.Columns(1).caption = m_sDen
        sdbddPagos.Columns(2).Visible = False
        sdbddPagos.Columns(3).Visible = False
    End If

    If sdbddPagos.Rows = 0 Then sdbddPagos.AddItem ""
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Value)
End Sub
Private Sub sdbddPagos_InitColumnProps()
    sdbddPagos.DataFieldList = "Column 0"
    sdbddPagos.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbddPagos_PositionList(ByVal Text As String)
PositionList sdbddPagos, Text
End Sub
Private Sub sdbddTiposPedido_CloseUp()
    sdbgCampos.Columns("COD_VALOR").Value = sdbddTiposPedido.Columns("COD").Value
    sdbgCampos.Columns("VALOR").Value = sdbddTiposPedido.Columns("COD").Value & " - " & sdbddTiposPedido.Columns("DEN").Value
End Sub
Private Sub sdbddTiposPedido_DropDown()
    Dim oTipoPedido As CTipoPedido

    If Not m_bModif Then
        sdbddTiposPedido.DroppedDown = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    sdbddTiposPedido.RemoveAll
       
    For Each oTipoPedido In m_oTiposPedido
        sdbddTiposPedido.AddItem oTipoPedido.Cod & Chr(m_lSeparador) & oTipoPedido.Den & _
        Chr(m_lSeparador) & m_arrConcep(oTipoPedido.CodConcep) & Chr(m_lSeparador) & m_arrAlmac(oTipoPedido.CodAlmac) & Chr(m_lSeparador) & m_arrRecep(oTipoPedido.CodRecep)
    Next
      
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbddTiposPedido_InitColumnProps()
    sdbddTiposPedido.DataFieldList = "Column 0"
    sdbddTiposPedido.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbddTiposPedido_PositionList(ByVal Text As String)
PositionList sdbddTiposPedido, Text
End Sub
Private Sub sdbddValor_CloseUp()
    'Si es un campo de tipo lista y de tipo texto
    If sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec And sdbgCampos.Columns("COD_VALOR").Value <> sdbddValor.Columns("ID").Value Then
        If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or _
            sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Then
            sdbgCampos.Columns("COD_VALOR").Value = sdbddValor.Columns("ID").Value
                
            LimpiarValorListaHija sdbgCampos.Columns("ID").Value
        End If
    End If
End Sub
Private Sub sdbddValor_DropDown()
    Dim oLista As CCampoValorListas
    Dim oElem As CCampoValorLista
    Dim iOrdenPadre As Integer
    Dim lConCampoPadre As Long
    Dim rs As Recordset
    Dim sGMN As String
    Dim arGMN As Variant
    Dim oCampo As CFormItem
    
    If Not m_bModif Then
        sdbddValor.Enabled = False
        Exit Sub
    End If

    sdbddValor.RemoveAll
    lConCampoPadre = 0
    If sdbgCampos.Columns("INTRO").Value = "1" Then
        'Mirar si la lista tiene campoPadre seleccionado
        If g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).IDListaCampoPadre > 0 Then
            lConCampoPadre = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).IDListaCampoPadre
            If Not g_oGrupoSeleccionado.Campos.Item(CStr(lConCampoPadre)) Is Nothing Then
                If g_oGrupoSeleccionado.Campos.Item(CStr(lConCampoPadre)).CampoGS = TipoCampoGS.material Then
                    If Trim(g_oGrupoSeleccionado.Campos.Item(CStr(lConCampoPadre)).valorText) <> "" Then
                        arGMN = DevolverMaterial(Trim(g_oGrupoSeleccionado.Campos.Item(CStr(lConCampoPadre)).valorText))
                        sGMN = arGMN(1) & IIf(arGMN(2) = "", "", "|" & arGMN(2) & IIf(arGMN(3) = "", "", "|" & arGMN(3) & IIf(arGMN(4) = "", "", "|" & arGMN(4))))
                    End If
                Else
                    If IsNumeric(g_oGrupoSeleccionado.Campos.Item(CStr(lConCampoPadre)).valorNum) Then
                        iOrdenPadre = g_oGrupoSeleccionado.Campos.Item(CStr(lConCampoPadre)).valorNum
                    End If
                End If
            Else
                'Se trata de una lista con padre en otro grupo. S�lo un campo material puede ser padre de una lista de otro grupo
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                oCampo.Id = lConCampoPadre
                sGMN = NullToStr(oCampo.DevolverValorCampo)
                If sGMN <> "" Then
                    arGMN = DevolverMaterial(sGMN)
                    sGMN = arGMN(1) & IIf(arGMN(2) = "", "", "|" & arGMN(2) & IIf(arGMN(3) = "", "", "|" & arGMN(3) & IIf(arGMN(4) = "", "", "|" & arGMN(4))))
                End If
                Set oCampo = Nothing
            End If
        End If
        
        If (lConCampoPadre = 0) Or (lConCampoPadre > 0 And iOrdenPadre > 0) Or sGMN <> "" Then
            'Selecci�n mediante lista de valores:
            If sGMN <> "" Then
                Set oCampo = oFSGSRaiz.Generar_CFormCampo
                Set rs = oCampo.DevolverValoresListasCampoPadre(sdbgCampos.Columns("ID").Value, 0, g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo, sGMN)
                Set oCampo = Nothing
                
                While Not rs.EOF
                    sdbddValor.AddItem rs("TEXTO").Value & Chr(m_lSeparador) & rs("ORDEN").Value
                    rs.MoveNext
                Wend
            Else
                g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).CargarValoresLista , iOrdenPadre
                
                Set oLista = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).ValoresLista
                
                For Each oElem In oLista
                    Select Case sdbgCampos.Columns("SUBTIPO").Value
                        Case TiposDeAtributos.TipoFecha
                            sdbddValor.AddItem oElem.valorFec
                        Case TiposDeAtributos.TipoNumerico
                            sdbddValor.AddItem oElem.valorNum
                        Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                            sdbddValor.AddItem oElem.valorText.Item(CStr(gParametrosInstalacion.gIdioma)).Den & Chr(m_lSeparador) & oElem.Orden
                    End Select
                Next
                Set oLista = Nothing
            End If
        End If
    Else
        'Campo de tipo boolean:
        If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoBoolean Then
            sdbddValor.AddItem m_sIdiTrue
            sdbddValor.AddItem m_sIdiFalse
        End If
    End If
End Sub
Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgCampos.Columns("VALOR").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgCampos_AfterUpdate(RtnDispErrMsg As Integer)
Dim RowPrec As Integer
Dim RowProv As Integer
Dim rowInit As Integer
Dim i As Integer
Dim bCargarUltADJ As Boolean
Dim bm As Variant
Dim sCodArt As String

RowPrec = -1
RowProv = -1

    If Not g_bUpdate Then Exit Sub

    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
        rowInit = sdbgCampos.Row - 1
        'Buscar si hay Precio y Proveedor y mirar si tiene el checkBox de cargar el ultimo Precio/proveedor adjucicado
        For i = 0 To sdbgCampos.Rows - 1
            bm = sdbgCampos.GetBookmark(i)
            If sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoSC.PrecioUnitario Then
                If g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(bm))).CargarUltADJ = True Then
                    RowPrec = i + 1
                End If
            End If
            If sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoGS.Proveedor Then
                If g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(bm))).CargarUltADJ = True Then
                    RowProv = i + 1
                End If
            End If
        Next i
        
        If RowPrec <> -1 Or RowProv <> -1 Then bCargarUltADJ = True
            
        ''Si el Codigo articulo introducido es un articulo codificado introducir la denominacion
        Dim oArticulos As CArticulos
        Set oArticulos = oFSGSRaiz.Generar_CArticulos

        If sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1)) <> "" Then
            Dim arrMat As Variant
            sCodArt = sdbgCampos.Columns("COD_VALOR").Value
            
            arrMat = DevolverMaterial(sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1)))
            oArticulos.CargarTodosLosArticulos sCodArt, , True, , , , arrMat(1), arrMat(2), arrMat(3), arrMat(4)
            
            If oArticulos.Count = 1 Then
                g_bUpdate = True
                
                sdbgCampos.Row = sdbgCampos.Row + 1
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                    sdbgCampos_Change
                    sdbgCampos.Columns("COD_VALOR").Value = oArticulos.Item(1).Den
                    sdbgCampos.Columns("VALOR").Value = oArticulos.Item(1).Den
                End If
                If bCargarUltADJ Then
                    If RowPrec <> -1 Then
                        Dim sMoneda As String
                        Dim dPrecio As Double
                        dPrecio = oArticulos.cargarUltPrecioAdj(sCodArt, sMoneda)
                        
                        sdbgCampos.Row = RowPrec
                        sdbgCampos.Columns("COD_VALOR").Value = dPrecio
                        sdbgCampos.Columns("VALOR").Value = dPrecio
                        
                        Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                    End If
                    If RowProv <> -1 Then
                        Dim vProveedor As Variant
                        vProveedor = oArticulos.cargarUltProveedorAdj(sCodArt)
                        sdbgCampos.Row = RowProv
                        If Not IsNull(vProveedor) Then
                            sdbgCampos.Columns("COD_VALOR").Value = vProveedor(1)
                            sdbgCampos.Columns("VALOR").Value = vProveedor(1) & " - " & vProveedor(2)
                        Else
                            sdbgCampos.Columns("COD_VALOR").Value = Null
                            sdbgCampos.Columns("VALOR").Value = ""
                        End If
                        Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                    End If
                End If
                sdbgCampos.Row = rowInit
            End If
            Set oArticulos = Nothing
        End If
    End If

    If m_blnQuitarValorTablaExterna Then
        QuitarValorTablaExterna
        m_blnQuitarValorTablaExterna = False
    End If
End Sub
Private Sub sdbgCampos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
    
    If (sdbgCampos.Rows = 0) Then Exit Sub
    If IsEmpty(sdbgCampos.GetBookmark(0)) Then
        sdbgCampos.Bookmark = sdbgCampos.GetBookmark(-1)
    Else
        sdbgCampos.Bookmark = sdbgCampos.GetBookmark(0)
    End If
    If Me.Visible Then sdbgCampos.SetFocus
End Sub
Private Sub sdbgCampos_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim bFormula As Boolean
    Dim oCampo As CFormItem
    Dim i As Integer
    Dim oGrupo As CFormGrupo
    Dim vbm As Variant
        
    If m_oCampoEnEdicion Is Nothing Then Exit Sub
    If Not g_bUpdate Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    '******** REALIZA LAS COMPROBACIONES CORRESPONDIENTES ANTES DE GUARDAR ***************
    'La denominaci�n en el idioma de la aplicaci�n no puede ser nula:
    If Trim(sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).caption
        Cancel = True
        Exit Sub
    End If
    
    If sdbgCampos.Columns("VALOR").Value <> "" Then
        If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
            'no hay que validar nada
            
        ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then
            'Si son los campos de GS importe o cantidad:
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.importe Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.Cantidad Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.PrecioUnitario Then
                If Not IsNumeric(sdbgCampos.Columns("VALOR").Value) Then
                    Screen.MousePointer = vbNormal
                    m_bCancel = True
                    oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(1)
                    m_bCancel = False
                    Cancel = True
                    Exit Sub
                End If
            End If
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.RetencionEnGarantia Then
                'Numerico y con max/min
                If Not IsNumeric(sdbgCampos.Columns("VALOR").Value) Then
                    Screen.MousePointer = vbNormal
                    m_bCancel = True
                    oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(1)
                    m_bCancel = False
                    Cancel = True
                    Exit Sub
                End If
                'comprueba que el valor existente est� dentro de los valores de m�ximo y m�nimo
                If Not IsNull(m_oCampoEnEdicion.Maximo) And (m_oCampoEnEdicion.Maximo <> "") Then
                    If CDbl(sdbgCampos.Columns("VALOR").Value) > CDbl(m_oCampoEnEdicion.Maximo) Then
                        Screen.MousePointer = vbNormal
                        m_bCancel = True
                        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(7)
                        Cancel = True
                        m_bCancel = False
                        Exit Sub
                    End If
                End If
                If Not IsNull(m_oCampoEnEdicion.Minimo) And (m_oCampoEnEdicion.Minimo <> "") Then
                    If CDbl(sdbgCampos.Columns("VALOR").Value) < CDbl(m_oCampoEnEdicion.Minimo) Then
                        Screen.MousePointer = vbNormal
                        m_bCancel = True
                        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(7)
                        m_bCancel = False
                        Cancel = True
                        Exit Sub
                    End If
                End If
            End If
        
        ElseIf sdbgCampos.Columns("INTRO").Value <> TAtributoIntroduccion.Introselec Then  'Introducci�n libre
            m_oCampoEnEdicion.RecuperarFormatoPorId
            Select Case sdbgCampos.Columns("SUBTIPO").Value
                Case TiposDeAtributos.TipoFecha
                    If Not IsDate(sdbgCampos.Columns("VALOR").Value) Then
                        Screen.MousePointer = vbNormal
                        m_bCancel = True
                        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(2)
                        m_bCancel = False
                        Cancel = True
                        Exit Sub
                    End If
                    'La fecha no puede ser anterior a la fecha del sistema
                    If m_oCampoEnEdicion.NoFecAntSis Then
                        If IsDate(sdbgCampos.Columns("VALOR").Value) Then
                            If CDbl(CVDate(sdbgCampos.Columns("VALOR").Value)) < CDbl(CVDate(Date)) Then
                                Screen.MousePointer = vbNormal
                                m_bCancel = True
                                oMensajes.NoFecAntSis
                                m_bCancel = False
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                    End If
                    'comprueba que el valor existente est� dentro de los valores de m�ximo y m�nimo
                    If Not IsNull(m_oCampoEnEdicion.Maximo) Then
                        If m_oCampoEnEdicion.Maximo <> "" Then
                            If CDate(sdbgCampos.Columns("VALOR").Value) > CDate(m_oCampoEnEdicion.Maximo) Then
                                Screen.MousePointer = vbNormal
                                m_bCancel = True
                                oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(7)
                                m_bCancel = False
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                    End If
                    If Not IsNull(m_oCampoEnEdicion.Minimo) Then
                        If m_oCampoEnEdicion.Minimo <> "" Then
                            If CDate(sdbgCampos.Columns("VALOR").Value) < CDate(m_oCampoEnEdicion.Minimo) Then
                                Screen.MousePointer = vbNormal
                                m_bCancel = True
                                oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(7)
                                m_bCancel = False
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                    End If
                    
                Case TiposDeAtributos.TipoNumerico
                    If Not IsNumeric(sdbgCampos.Columns("VALOR").Value) Then
                        Screen.MousePointer = vbNormal
                        m_bCancel = True
                        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(1)
                        Cancel = True
                        m_bCancel = False
                        Exit Sub
                    End If
                    
                    'comprueba que el valor existente est� dentro de los valores de m�ximo y m�nimo
                    If Not IsNull(m_oCampoEnEdicion.Maximo) And (m_oCampoEnEdicion.Maximo <> "") Then
                        If CDbl(sdbgCampos.Columns("VALOR").Value) > CDbl(m_oCampoEnEdicion.Maximo) Then
                            Screen.MousePointer = vbNormal
                            m_bCancel = True
                            oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(7)
                            Cancel = True
                            m_bCancel = False
                            Exit Sub
                        End If
                    End If
                    If Not IsNull(m_oCampoEnEdicion.Minimo) And (m_oCampoEnEdicion.Minimo <> "") Then
                        If CDbl(sdbgCampos.Columns("VALOR").Value) < CDbl(m_oCampoEnEdicion.Minimo) Then
                            Screen.MousePointer = vbNormal
                            m_bCancel = True
                            oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(7)
                            m_bCancel = False
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                Case TiposDeAtributos.TipoBoolean
                    If UCase(sdbgCampos.Columns("VALOR").Value) <> UCase(m_sIdiTrue) And UCase(sdbgCampos.Columns("VALOR").Value) <> UCase(m_sIdiFalse) Then
                        Screen.MousePointer = vbNormal
                        m_bCancel = True
                        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & " " & m_sMensaje(3)
                        m_bCancel = False
                        Cancel = True
                        Exit Sub
                    End If
            End Select
            
            If (m_oCampoEnEdicion.Formato <> "") Then
                If Not (validarFormato(m_oCampoEnEdicion.Formato, sdbgCampos.Columns("VALOR").Value)) Then
                        Screen.MousePointer = vbNormal
                        m_bCancel = True
                        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value & ". " & m_sMensaje(9)
                        m_bCancel = False
                        Cancel = True
                        Exit Sub
                End If
            End If
        End If
    End If
    
    If Not sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
        'Si se ha modificado un campo num�rico y hay f�rmulas se calculan sus valores:
        bFormula = False
        If basParametros.gParametrosGenerales.gsAccesoFSWS = AccesoFSWSCompleto Then
            If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoNumerico And sdbgCampos.Columns("CAMPO_GS").Value = "" Then
                'si ha habido cambios
                If NullToStr(m_oCampoEnEdicion.valorNum) <> NullToStr(sdbgCampos.Columns("VALOR").Value) Then
                    m_oCampoEnEdicion.valorNum = StrToDbl0(sdbgCampos.Columns("VALOR").Value)
                    bFormula = RecalcularValoresFormulas
                End If
            End If
        End If
     End If
     
    '************************ GUARDA EN BASE DE DATOS ***************************
    m_bModError = False
    
    If bFormula Then
        m_oCampoEnEdicion.Modificado = True
        teserror = g_oFormSeleccionado.ModificarCamposCalculados
    Else
        '--------------------------------------------------------------------------------------------------------
        'Rellenamos la colecci�n con los nuevos valores dependiendo del tipo:
        Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
        If g_oFormSeleccionado.Multiidioma = True Then
            For Each oIdioma In m_oIdiomas
                oDenominaciones.Add oIdioma.Cod, Trim(sdbgCampos.Columns(CStr(oIdioma.Cod)).Value)
            Next
        Else
            For Each oIdioma In m_oIdiomas
                oDenominaciones.Add oIdioma.Cod, Trim(sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value)
            Next
        End If
        Set m_oCampoEnEdicion.Denominaciones = oDenominaciones
        '--------------------------------------------------------------------------------------------------------
        
        If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
            'ocampo.TipoPredef -->      FORM_CAMPO.TIPO
            'ocampo.Tipo        -->     FORM_CAMPO.SUBTIPO
            m_oCampoEnEdicion.TipoPredef = TipoCampoPredefinido.externo
            Dim intTabla As Integer
            Dim strNombreTabla As String, strPK As String
            Dim vTipoDeDatoPK As TiposDeAtributos
            intTabla = CInt(sdbgCampos.Columns("IDTABLAEXTERNA").Value)
            m_oCampoEnEdicion.TablaExterna = intTabla
            strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
            strPK = m_oTablaExterna.SacarPK(intTabla)
            vTipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
            If Trim(g_strPK) = "" Then
                m_oCampoEnEdicion.valorFec = Null: m_oCampoEnEdicion.valorBool = Null: m_oCampoEnEdicion.valorNum = Null: m_oCampoEnEdicion.valorText = Null
            Else
                Select Case vTipoDeDatoPK
                    Case TipoFecha
                        m_oCampoEnEdicion.Tipo = TipoFecha
                        m_oCampoEnEdicion.valorFec = CDate(g_strPK)
                        m_oCampoEnEdicion.valorBool = Null: m_oCampoEnEdicion.valorNum = Null: m_oCampoEnEdicion.valorText = Null
                    Case TipoNumerico
                        m_oCampoEnEdicion.Tipo = TipoNumerico
                        m_oCampoEnEdicion.valorNum = g_strPK
                        m_oCampoEnEdicion.valorBool = Null: m_oCampoEnEdicion.valorFec = Null: m_oCampoEnEdicion.valorText = Null
                    Case TipoTextoMedio
                        m_oCampoEnEdicion.Tipo = TipoTextoMedio
                        m_oCampoEnEdicion.valorText = g_strPK
                        m_oCampoEnEdicion.valorBool = Null: m_oCampoEnEdicion.valorFec = Null: m_oCampoEnEdicion.valorNum = Null
                End Select
            End If
        Else
            'ESTO HAY QUE REVISARLO FALTA FALTA
             m_oCampoEnEdicion.valorBool = Null: m_oCampoEnEdicion.valorFec = Null: m_oCampoEnEdicion.valorNum = Null: m_oCampoEnEdicion.valorText = Null
            '--------------------------------------------------------------------------------------------
            If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then
                Select Case sdbgCampos.Columns("CAMPO_GS").Value
                    Case TipoCampoSC.DescrBreve, TipoCampoSC.DescrDetallada, TipoCampoGS.Factura
                        m_oCampoEnEdicion.valorText = sdbgCampos.Columns("VALOR").Value
                        
                    Case TipoCampoGS.Dest, TipoCampoGS.FormaPago, TipoCampoGS.Proveedor, TipoCampoGS.Pais, TipoCampoGS.Provincia, TipoCampoGS.material, TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo, TipoCampoGS.DenArticulo, TipoCampoGS.Unidad, TipoCampoGS.Moneda, TipoCampoGS.CampoPersona, TipoCampoGS.TipoPedido, TipoCampoGS.UnidadPedido, TipoCampoGS.CodComprador
                        m_oCampoEnEdicion.valorText = StrToNull(sdbgCampos.Columns("COD_VALOR").Value)
                        
                    Case TipoCampoSC.importe, TipoCampoSC.Cantidad, TipoCampoSC.PrecioUnitario
                        m_oCampoEnEdicion.valorNum = sdbgCampos.Columns("VALOR").Value
                        
                    Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                    ''aqui hacer la comprobaci�n si es fecha_valor_defecto
                        Set oCampo = oFSGSRaiz.Generar_CFormCampo
                        oCampo.Id = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
                        oCampo.CargarFECHAVALORDEFECTO
    
                        If oCampo.FECHAVALORDEFECTO = True Then
                            m_oCampoEnEdicion.valorFec = sdbgCampos.Columns("VALOR").Value
                        Else
                            m_oCampoEnEdicion.valorNum = sdbgCampos.Columns("COD_VALOR").Value
                        End If
                        
                    Case TipoCampoGS.PRES1, TipoCampoGS.PRES2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                        m_oCampoEnEdicion.valorText = sdbgCampos.Columns("COD_VALOR").Value
                        
                    Case TipoCampoSC.ArchivoEspecific, TipoCampoGS.NumSolicitERP
                        m_oCampoEnEdicion.valorText = sdbgCampos.Columns("VALOR").Value
                        
                    Case TipoCampoGS.UnidadOrganizativa
                        m_oCampoEnEdicion.valorText = StrToNull(sdbgCampos.Columns("VALOR").Value)
                    Case TipoCampoGS.Departamento
                        m_oCampoEnEdicion.valorText = StrToNull(sdbgCampos.Columns("COD_VALOR").Value)
                        
                    Case TipoCampoGS.OrganizacionCompras, TipoCampoGS.Centro, TipoCampoGS.ProveedorERP, TipoCampoGS.CentroCoste
                        m_oCampoEnEdicion.valorText = sdbgCampos.Columns("COD_VALOR").Value
                    
                    Case TipoCampoGS.Almacen, TipoCampoGS.Empresa
                        m_oCampoEnEdicion.valorNum = StrToNull(sdbgCampos.Columns("COD_VALOR").Value)
                        
                    Case TipoCampoGS.InicioAbono, TipoCampoGS.FinAbono
                        m_oCampoEnEdicion.valorFec = sdbgCampos.Columns("VALOR").Value
                        
                    Case TipoCampoGS.RetencionEnGarantia
                        m_oCampoEnEdicion.valorNum = sdbgCampos.Columns("VALOR").Value
                    Case TipoCampoGS.RenovacionAutomatica
                        m_oCampoEnEdicion.valorBool = (sdbgCampos.Columns("VALOR").Value = m_sIdiTrue)
                    Case TipoCampoGS.PeriodoRenovacion
                        m_oCampoEnEdicion.valorText = sdbgCampos.Columns("VALOR").Value
                        If sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec Then  'Si es un campo de tipo lista guardamos el �ndice en valor num
                            m_oCampoEnEdicion.valorNum = sdbgCampos.Columns("COD_VALOR").Value
                        End If
                    Case TipoCampoGS.EstadoHomologacion
                        m_oCampoEnEdicion.valorText = sdbgCampos.Columns("VALOR").Value
                        m_oCampoEnEdicion.valorNum = sdbgCampos.Columns("COD_VALOR").Value
                End Select
                
            Else
                Select Case sdbgCampos.Columns("SUBTIPO").Value
                    Case TiposDeAtributos.TipoFecha
                        m_oCampoEnEdicion.valorFec = sdbgCampos.Columns("VALOR").Value
                        
                    Case TiposDeAtributos.TipoNumerico
                        m_oCampoEnEdicion.valorNum = sdbgCampos.Columns("VALOR").Value
                        
                    Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoEditor
                        m_oCampoEnEdicion.valorText = sdbgCampos.Columns("VALOR").Value
                        If sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec Then  'Si es un campo de tipo lista guardamos el �ndice en valor num
                            m_oCampoEnEdicion.valorNum = sdbgCampos.Columns("COD_VALOR").Value
                        End If
                    Case TiposDeAtributos.TipoBoolean
                        Select Case UCase(sdbgCampos.Columns("VALOR").Value)
                            Case UCase(m_sIdiTrue)
                                m_oCampoEnEdicion.valorBool = 1
                            Case UCase(m_sIdiFalse)
                                m_oCampoEnEdicion.valorBool = 0
                        End Select
                        
                    Case TiposDeAtributos.TipoArchivo
                        m_oCampoEnEdicion.valorText = sdbgCampos.Columns("VALOR").Value
                End Select
            End If
            '--------------------------------------------------------------------------------------------
        End If
        
        
        If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
            m_oCampoEnEdicion.AProceso = False              'FALTA correcto ??????
            m_oCampoEnEdicion.APedido = False               'FALTA correcto ??????
        Else
            If CamposSistemaAProceso(sdbgCampos.Columns("CAMPO_GS").Value) Or _
            (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.importe And ssTabGrupos.selectedItem.Index = 1) Then
                m_oCampoEnEdicion.AProceso = False
                m_oCampoEnEdicion.APedido = False
            Else
                m_oCampoEnEdicion.AProceso = GridCheckToBoolean(sdbgCampos.Columns("APROCESO").Value)
                m_oCampoEnEdicion.APedido = GridCheckToBoolean(sdbgCampos.Columns("APEDIDO").Value)
            End If
        End If
        
        Set oIBaseDatos = m_oCampoEnEdicion
        teserror = oIBaseDatos.FinalizarEdicionModificando
    End If
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        m_bModError = True
        basErrores.TratarError teserror
        sdbgCampos.CancelUpdate
        If Me.Visible Then sdbgCampos.SetFocus
        g_Accion = ACCFormularioCons
        sdbgCampos.DataChanged = False
    Else
        'Actualiza la grid en el caso de que el cambio haya afectado a campos calculados:
        If bFormula Then
            m_oCampoEnEdicion.Modificado = False
            For Each oGrupo In g_oFormSeleccionado.Grupos
                If Not oGrupo.Campos Is Nothing Then
                    For Each oCampo In oGrupo.Campos
                        If oCampo.Modificado And oCampo.Id <> m_oCampoEnEdicion.Id Then
                            If g_oGrupoSeleccionado.Id = oGrupo.Id Then
                                For i = 0 To sdbgCampos.Rows - 1
                                    vbm = sdbgCampos.AddItemBookmark(i)
                                    If CLng(sdbgCampos.Columns("ID").CellValue(vbm)) = oCampo.Id Then
                                        g_bUpdate = False
                                        sdbgCampos.Bookmark = vbm
                                        sdbgCampos.Columns("VALOR").Value = oCampo.valorNum
                                        sdbgCampos.Update
                                        g_bUpdate = True
                                        Exit For
                                    End If
                                Next i
                            End If
                            oCampo.Modificado = False
                        End If
                    Next
                End If
            Next
        End If
        
        ''' Registro de acciones
        basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemModif, "Id" & sdbgCampos.Columns("ID").Value
        g_Accion = ACCFormularioCons
        
        cmdDeshacer.Enabled = False
        Set oIBaseDatos = Nothing
        Set m_oCampoEnEdicion = Nothing
    End If
    
    Screen.MousePointer = vbNormal
End Sub
''' <summary>Evento que muestra el detalle de cada campo.</summary>
Private Sub sdbgCampos_BtnClick()
    Dim TipoDeDatoPK As TiposDeAtributos
    Dim intTabla As Integer
    Dim strNombreTabla As String, strPK As String, strDenoTabla As String, strPrimerCampoNoPK As String
    Dim valor As Variant
    Dim iResp As Integer
    Dim sObs As String
    
    On Error Resume Next
        
    If sdbgCampos.col < 0 Then Exit Sub
    
    If g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)) Is Nothing Then Exit Sub
    
    'si hay cambios los guarda
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
        If m_bModError Then Exit Sub
    End If
    
    Select Case sdbgCampos.Columns(sdbgCampos.col).Name
        Case "AYUDA"
            'Muestra el formulario con la ayuda:
            MostrarFormFormCampoAyuda oGestorIdiomas, gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, oGestorSeguridad, oUsuarioSummit.Cod, gParametrosGenerales, _
                FormCampoAyudaTipoAyuda.CampoFormulario, m_bModif, g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)), m_oIdiomas
        
        Case "VALOR"
            If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
                intTabla = CInt(sdbgCampos.Columns("IDTABLAEXTERNA").Value)
                m_intTabla = intTabla   'paso de vbles entre Click y BeforeUpdate
                m_blnExisteTablaExternaParaArticulo = IIf(m_oTablaExterna.SacarTablaTieneART(intTabla), True, False)
                strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
                strDenoTabla = m_oTablaExterna.SacarDenominacionTabla(intTabla)
                strPrimerCampoNoPK = m_oTablaExterna.SacarPrimerCampoDistinto(intTabla)
                strPK = m_oTablaExterna.SacarPK(intTabla)
                TipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
                '_______________________________________________________________________
                If sdbgCampos.Columns("VALOR").Value <> "" Then
                    valor = m_oTablaExterna.DevolverValor(sdbgCampos.Columns("ID").Value, TipoDeDatoPK)
                    Select Case TipoDeDatoPK
                        Case TipoFecha
                            valor = CDate(valor)
                        Case TipoNumerico
                            valor = valor
                    End Select
                End If
                Dim sIdART As String
                If ExisteAtributoArticulo() Then
                    sIdART = DevuelveIdArticulo()
                    m_blnExisteArticulo = True
                Else
                    sIdART = ""
                    m_blnExisteArticulo = False
                End If
                
                m_strPK_old = g_strPK
                
                FSGSForm.MostrarFormTablaExterna oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, g_strPK, valor, _
                                         strDenoTabla, strNombreTabla, strPK, sIdART, intTabla, "frmFormularios"
                                         
                If g_strPK = "vbFormControlMenu" Then
                    'lo dejo como estaba
                    g_strPK = m_strPK_old
                ElseIf g_strPK <> "" Then
                    sdbgCampos.Columns("VALOR").Value = g_strPK & " " & m_oTablaExterna.ValorCampo(g_strPK, strPK, strPrimerCampoNoPK, strNombreTabla, TipoDeDatoPK)
                Else    '?
                    sdbgCampos.Columns("VALOR").Value = ""
                End If
                '�����������������������������������������������������������������������
                sdbgCampos_Change       'la primera vez no lo hace desconozco porque
                sdbgCampos.Update       'esto es pa que acepte los cambios antes de cambiar de linea
            Else
                If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then
                'Dependiendo del tipo de campo de GS que sea mostrar� las pantallas para seleccionar los valores
                    'Si son campos de GS:
                    Select Case sdbgCampos.Columns("CAMPO_GS").Value
                        Case TipoCampoSC.FecNecesidad, TipoCampoSC.FinSuministro, TipoCampoSC.IniSuministro, TipoCampoGS.InicioAbono, TipoCampoGS.FinAbono
                        ''miramos si es FECHA_VALOR_DEFECTO O NO
                            Set frmCalendar.frmDestination = frmFormularios
                            Set frmCalendar.ctrDestination = Nothing
                            frmCalendar.addtotop = 900 + 360
                            frmCalendar.addtoleft = 180
                            If sdbgCampos.Columns("VALOR").Value <> "" Then
                                frmCalendar.Calendar.Value = sdbgCampos.Columns("VALOR").Value
                            Else
                                frmCalendar.Calendar.Value = Date
                            End If
                            frmCalendar.Show vbModal
                            
                        Case TipoCampoSC.DescrDetallada
                            sObs = NullToStr(g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText)
                            If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value, m_bModif, -1, sObs) Then
                                sdbgCampos.Columns("VALOR").Value = sObs
                                g_oGrupoSeleccionado.Campos.Item(CStr(frmFormularios.sdbgCampos.Columns("ID").Value)).valorText = sObs
                                sdbgCampos.Update
                            End If
                            
                        Case TipoCampoGS.Proveedor
                            frmPROVEBuscar.sOrigen = "frmFormularios"
                            If m_bRestrProve = True Then frmPROVEBuscar.codEqp = basOptimizacion.gCodEqpUsuario
                            frmPROVEBuscar.Show vbModal
                            
                        Case TipoCampoGS.PRES1, TipoCampoGS.PRES2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                            With frmPRESAsig
                              Select Case sdbgCampos.Columns("CAMPO_GS").Value
                              Case TipoCampoGS.PRES1
                                  .g_dblAsignado = m_dblPres1Asig
                                  .g_iTipoPres = 1
                                  .g_bHayPresBajaLog = m_bHayPres1BajaLog
                              Case TipoCampoGS.PRES2
                                  .g_dblAsignado = m_dblPres2Asig
                                  .g_iTipoPres = 2
                                  .g_bHayPresBajaLog = m_bHayPres2BajaLog
                              Case TipoCampoGS.Pres3
                                  .g_dblAsignado = m_dblPres3Asig
                                  .g_iTipoPres = 3
                                  .g_bHayPresBajaLog = m_bHayPres3BajaLog
                              Case TipoCampoGS.Pres4
                                  .g_dblAsignado = m_dblPres4Asig
                                  .g_iTipoPres = 4
                                  .g_bHayPresBajaLog = m_bHayPres4BajaLog
                              End Select
                            
                              .g_bHayPres = (.g_dblAsignado > 0)
                              .g_sOrigen = "frmFormularios"
                              .g_bModif = m_bModif
                              .g_bSinPermisos = False
                              .g_bRUO = m_bRestrPresUO
                              .g_sValorPresFormulario = sdbgCampos.Columns("COD_VALOR").Value
                              .Show vbModal
                            End With
                        Case TipoCampoSC.ArchivoEspecific
                            frmFormAdjuntos.g_sOrigen = "frmFormularios"
                            Set frmFormAdjuntos.g_oCampoSeleccionado = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                            frmFormAdjuntos.g_bModif = m_bModif
                            frmFormAdjuntos.Show vbModal
                            
                        Case TipoCampoGS.Desglose
                            'Valores del desglose:
                            frmDesgloseValores.g_sOrigen = "frmFormularios"
                            If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.OrganizacionCompras Then
                                frmDesgloseValores.g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
                                frmDesgloseValores.g_sCodCentro = "-1"
                            ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.Centro Then
                                frmDesgloseValores.g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-2))
                                frmDesgloseValores.g_sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
                            Else
                                frmDesgloseValores.g_sCodOrganizacionCompras = "-1"
                                frmDesgloseValores.g_sCodCentro = "-1"
                            End If
                            frmDesgloseValores.g_sMoneda = g_oFormSeleccionado.Moneda
                            Set frmDesgloseValores.g_oCampoDesglose = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                            frmDesgloseValores.Show vbModal
                            
                        Case TipoCampoGS.material
                            frmSELMAT.sOrigen = "frmFormularios"
                            frmSELMAT.bRComprador = m_bRestrMat
                            frmSELMAT.nivelSeleccion = CampoSeleccionado.nivelSeleccion
                            frmSELMAT.Hide
                            frmSELMAT.Show vbModal
                            
                        Case TipoCampoGS.CampoPersona
                            frmSOLSelPersona.g_sOrigen = "frmFormularios"
                            frmSOLSelPersona.bAllowSelUON = False
                            frmSOLSelPersona.bRDep = False 'No vamos a poner restricciones.
                            frmSOLSelPersona.bRUO = False 'No tiene sentido restringir al UO o Dep del que configura el Formulario.
                            frmSOLSelPersona.Show vbModal
                            
                        Case TipoCampoGS.UnidadOrganizativa
                            frmSELUO.sOrigen = "frmFormularios"
                            frmSELUO.Show vbModal
                    
                        Case TipoCampoGS.CentroCoste
                            frmSelCenCoste.g_sOrigen = "frmFormularios"
                            frmSelCenCoste.g_bCentrosSM = False
                            frmSelCenCoste.g_bSaltarComprobacionArbol = True
                            Set frmSelCenCoste.g_oOrigen = Me
                            frmSelCenCoste.Show vbModal
                    End Select
                    
                Else
                    'Si es un campo de tipo archivo
                    Select Case sdbgCampos.Columns("SUBTIPO").Value
                        Case TiposDeAtributos.TipoArchivo
                            With frmFormAdjuntos
                                .g_sOrigen = "frmFormularios"
                                Set .g_oCampoSeleccionado = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                                .g_bModif = m_bModif
                                .Show vbModal
                            End With
                        Case TiposDeAtributos.TipoFecha    'Muestra el calendario
                            With frmCalendar
                                Set .frmDestination = frmFormularios
                                Set .ctrDestination = Nothing
                                .addtotop = 900 + 360
                                .addtoleft = 180
                                If sdbgCampos.Columns("VALOR").Value <> "" Then
                                    .Calendar.Value = sdbgCampos.Columns("VALOR").Value
                                Else
                                    .Calendar.Value = Date
                                End If
                                .Show vbModal
                            End With
                        Case TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                            Dim lMaxLength As Long
                            If (IsNull(g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength)) Or (g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength = "") Then
                                If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Then
                                    lMaxLength = -1
                                Else
                                    lMaxLength = 800
                                End If
                            Else
                                lMaxLength = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength
                            End If
                            sObs = NullToStr(g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText)
                            If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value, m_bModif, lMaxLength, sObs) Then
                                sdbgCampos.Columns("VALOR").Value = sObs
                                g_oGrupoSeleccionado.Campos.Item(CStr(frmFormularios.sdbgCampos.Columns("ID").Value)).valorText = sObs
                                sdbgCampos.Update
                            End If
                            
                        Case TiposDeAtributos.TipoDesglose
                            'Valores del desglose:
                            With frmDesgloseValores
                                .g_sOrigen = "frmFormularios"
                                If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.OrganizacionCompras Then
                                    .g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
                                    .g_sCodCentro = "-1"
                                ElseIf sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.Centro Then
                                    .g_sCodOrganizacionCompras = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-2))
                                    .g_sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(sdbgCampos.GetBookmark(-1))
                                Else
                                    .g_sCodOrganizacionCompras = "-1"
                                    .g_sCodCentro = "-1"
                                End If
                                .g_sMoneda = g_oFormSeleccionado.Moneda
                                Set .g_oCampoDesglose = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                                .Show vbModal
                            End With
                        Case TiposDeAtributos.TipoEditor
                            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                            
                            Dim strSessionId As String
                            strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
                            
                            With frmEditor
                                .caption = sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value
                                .g_sOrigen = "frmFormularios"
                                .g_sRuta = gParametrosGenerales.gsURLCampoEditor & "?sessionId=" & strSessionId & "&desdeGS=1&form_campo=" & CStr(sdbgCampos.Columns("ID").Value)
                                .g_bReadOnly = False
                                .Show vbModal
                            End With
                    End Select
                End If
            End If
            
        Case Else
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DesgloseActividad Then
                'Tipo de campo predefinido del GS, o de certificados,no conformidades....
                With frmDetalleCampoPredef
                    .g_sDenCampo = ObtenerNombreCampo(g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoGS, g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef, g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo, g_oFormSeleccionado.Multiidioma, g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).PRES5)
                    .g_iTipo = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo
                    .g_iTipoCampo = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoGS
                    .g_iTipoSolicit = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef
                    .g_lCampoId = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
                    .g_sOrigen = "frmFormularios"
                    .Show vbModal
                End With
                Dim oCampo1 As CFormItem
                Set oCampo1 = oFSGSRaiz.Generar_CFormCampo
                oCampo1.Id = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
                oCampo1.CargarAnyadirArt
            ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
                'Desglose de material:
                If Not g_ofrmDesglose Is Nothing Then
                    Unload g_ofrmDesglose
                    Set g_ofrmDesglose = Nothing
                End If
                Set g_ofrmDesglose = New frmDesglose
                Set g_ofrmDesglose.g_oCampoDesglose = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                g_ofrmDesglose.g_sOrigen = "frmFormularios"
                g_ofrmDesglose.g_bModif = m_bModif
                Screen.MousePointer = vbHourglass
                MDI.MostrarFormulario g_ofrmDesglose
                Screen.MousePointer = vbNormal
            
            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
                'Muestra la pantalla frmDetalleCampoExterno:
                strDenoTabla = m_oTablaExterna.SacarDenominacionTabla(CInt(sdbgCampos.Columns("IDTABLAEXTERNA").Value))
                
                MostrarFormDetalleCampoExterno oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, strDenoTabla, sdbgCampos.Columns("SUBTIPO").Value
            
            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Servicio Then
                'Muestra la pantalla frmServicio:
                With frmServicio
                    .lIdFormulario = frmFormularios.g_oFormSeleccionado.Id
                    .lGrupoFormulario = frmFormularios.g_oGrupoSeleccionado.Id
                    .g_sOrigen = "frmFormularios"
                    Set .g_oIdiomas = frmFormularios.m_oIdiomas
                    .g_multiidioma = frmFormularios.g_oFormSeleccionado.Multiidioma
                    .iServAntiguo = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Servicio
                    .lCampoForm = CLng(sdbgCampos.Columns("ID").Value)
                    .g_Instancia = 0
                    .Show vbModal
                End With
            ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                'Muestra la pantalla de los campos calculados:
                With frmFormularioCampoCalculado
                    .g_bModif = m_bModif
                    .g_lCampoPosic = sdbgCampos.Columns("ID").Value
                    .g_sOrigen = "frmFormularios"
                    .Show vbModal
                End With
            ElseIf (sdbgCampos.Columns("CAMPO_GS").Value <> "") And (g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef <> Normal) _
            And (sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoNoConformidad.Subtipo) Then
                'Tipo de campo predefinido del GS, o de certificados,no conformidades....
                With frmDetalleCampoPredef
                    .g_sDenCampo = ObtenerNombreCampo(g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoGS, g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef, g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo, g_oFormSeleccionado.Multiidioma, g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).PRES5)
                    .g_iTipo = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo
                    .g_iTipoCampo = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoGS
                    .g_iTipoSolicit = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef
                    .g_lCampoId = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).Id
                    .g_sOrigen = "frmFormularios"
                    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material Then
                        .permitirCambiarNivelSeleccion = True
                        .nivelSeleccion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).nivelSeleccion
                    End If
                    .Show vbModal
                    
                    Dim oCampo As CFormItem
                    Set oCampo = oFSGSRaiz.Generar_CFormCampo
                    iResp = vbYes
                    If .haCambiadoNivelSeleccion And g_oFormSeleccionado.existeLineaDesglose And .nivelSeleccion <> 0 Then
                    iResp = oMensajes.MensajeYesNo(1450)
                    End If
                End With
                If iResp = vbYes Then
                    Set oCampo = CampoSeleccionado
                    oCampo.CargarAnyadirArt
                    If frmDetalleCampoPredef.haCambiadoNivelSeleccion And frmDetalleCampoPredef.nivelSeleccion <> 0 Then
                        oCampo.valorText = ""
                    End If
                    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And frmDetalleCampoPredef.haCambiadoNivelSeleccion Then
                        If frmDetalleCampoPredef.nivelSeleccion <> 0 Then
                            sdbgCampos.Columns("VALOR").Value = ""
                            g_oFormSeleccionado.EliminarArticulosEnTodosDesgloses
                        End If
                        
                        oCampo.nivelSeleccion = frmDetalleCampoPredef.nivelSeleccion
                        oCampo.ModificarNivelSeleccion
                        g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).nivelSeleccion = oCampo.nivelSeleccion
                        
                        frmDetalleCampoPredef.haCambiadoNivelSeleccion = False
                        
                    End If
                    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
                        g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).AnyadirArt = oCampo.AnyadirArt
                    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.PrecioUnitario Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Proveedor Then
                        oCampo.Cargar_SI_Ult_ADJ 0
                        g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).CargarUltADJ = oCampo.CargarUltADJ
                    End If
                    If Not oCampo.AnyadirArt Then
                        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
                            Dim oArticulo As CArticulo
                            Set oArticulo = oFSGSRaiz.Generar_CArticulo
                            oArticulo.Cod = sdbgCampos.Columns("COD_VALOR").Value
                            
                            If Not oArticulo.existeArticulo Then
                                'Quita el valor del art�culo
                                sdbgCampos.Columns("COD_VALOR").Value = ""
                                sdbgCampos.Columns("VALOR").Value = ""
                                sdbgCampos_Change
                                'Quita el valor de la denominaci�n del art�culo
                                sdbgCampos.Row = sdbgCampos.Row + 1
                                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                                    sdbgCampos.Columns("COD_VALOR").Value = ""
                                    sdbgCampos.Columns("VALOR").Value = ""
                                    sdbgCampos_Change
                                End If
                                sdbgCampos.Row = sdbgCampos.Row - 1
                            End If
    
                            Set oArticulo = Nothing
                        End If
                        sdbgCampos.Update
                    End If
                    Set oCampo = Nothing
                End If
            Else
                '----------------------------------------------------------------------------------------------------------------
                'Detalle del campo normal
                'si hay solicitudes asociadas, no se podr�n modificar los tipos de campos del formulario
                With frmDetalleCampos
                    .g_sOrigen = "frmFormularios"
                    .g_bModif = m_bModif
                    .g_bCampoEnUso = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).CampoEnUso
                    Set .g_oIdiomas = m_oIdiomas
                    Set .g_oCampo = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                    Dim FecActCampoHijo As Date
                    FecActCampoHijo = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).FecAct
                    .Show vbModal
                End With
                If Not g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).IDListaCampoPadre = 0 Then
                    If Not FecActCampoHijo = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).FecAct Then
                        g_oGrupoSeleccionado.Campos.Item(CStr(g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).IDListaCampoPadre)).FecAct = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).FecAct
                    End If
                End If
            End If
    End Select
End Sub

''' <summary>Comprueba si el campo se puede marcar para su env�o a proceso o a pedido</summary>
Public Sub sdbgCampos_Change()
    Dim vMaxLength As Variant
    Dim oGrupo As CFormGrupo
    
    If g_oGrupoSeleccionado Is Nothing Then Exit Sub
    If Not g_bUpdate Then Exit Sub
    If sdbgCampos.col < 0 Then Exit Sub
    
    DoEvents
        
    Set m_oCampoEnEdicion = Nothing

    If g_oGrupoSeleccionado.Campos Is Nothing Then Exit Sub
    If g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)) Is Nothing Then Exit Sub
    
    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
       
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo And sdbgCampos.col > 0 Then
        If sdbgCampos.Columns(sdbgCampos.col).Name = "VALOR" Then sdbgCampos.Columns("GENERICO").Value = True
    End If
    
    'Si hemos quitado el valor del proveedor y existe el campoGs Proveedor ERP eliminaremos su valor
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Proveedor Then
        AccionSobreCampoGsProveedorERP (AccionSobreCampoProveedorERP.EliminarValor)
        'Vuelvo a coger el campo en edicion ya que al borrar el proveedor ERP y desplazarme hasta su fila lo he perdido
        Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
    End If
    If m_oCampoEnEdicion.CampoGS = TipoCampoGS.DenArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
        sdbgCampos.Columns("COD_VALOR").Value = sdbgCampos.Columns("VALOR").Value
    End If
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
        If Len(sdbgCampos.Columns("VALOR").Value) + 1 > basParametros.gLongitudesDeCodigos.giLongCodDENART Then
            sdbgCampos.Columns("VALOR").Value = Left(sdbgCampos.Columns("VALOR").Value, basParametros.gLongitudesDeCodigos.giLongCodDENART)
        End If
    End If
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CentroCoste Then
        If sdbgCampos.Columns("VALOR").Value = "" Then sdbgCampos.Columns("COD_VALOR").Value = ""
    End If
    'Si es un campo de tipo lista y de tipo texto y elimino el valor elimino el valor de las listas hijas
    If sdbgCampos.Columns(sdbgCampos.col).Name = "VALOR" And sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec And sdbgCampos.Columns("VALOR").Value = "" Then
        sdbgCampos.Columns("COD_VALOR").Value = ""
        LimpiarValorListaHija sdbgCampos.Columns("ID").Value
    End If
    'Controlar los tipos TipoTextoCorto, TipoTextoMedio, TipoTextoLargo para que no superen el maximo permitido cuando se hace copy/paste
    If (sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.DenArticulo) And (sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo) Then
        vMaxLength = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength
        If IsNull(vMaxLength) Or vMaxLength = "" Then
            If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Then
               vMaxLength = 100
            ElseIf sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Then
                vMaxLength = 800
            Else
                vMaxLength = -1
            End If
        End If
        If CInt(vMaxLength) > 0 And Len(sdbgCampos.Columns("VALOR").Value) > CInt(vMaxLength) Then
            sdbgCampos.Columns("VALOR").Value = Left(sdbgCampos.Columns("VALOR").Value, CInt(vMaxLength))
        End If
    End If
        
    If sdbgCampos.col > 0 Then
        Select Case sdbgCampos.Columns(sdbgCampos.col).Name
            Case "APROCESO"
                If CamposSistemaAProceso(sdbgCampos.Columns("CAMPO_GS").Value) Or _
                (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.importe And ssTabGrupos.selectedItem.Index = 1) Then
                    sdbgCampos.Columns("APROCESO").Value = True
                    
                ElseIf sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoArchivo Or _
                    sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Then
                    
                ElseIf sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Atributo Or _
                    sdbgCampos.Columns("ATRIBUTO").Value <> "" Then
                    If GridCheckToBoolean(sdbgCampos.Columns("APROCESO").Value) = False Then
                        If GridCheckToBoolean(sdbgCampos.Columns("APEDIDO").Value) = True Then
                            sdbgCampos.Columns("APEDIDO").Value = False
                        End If
                    Else
                        'Comprobaremos que no haya en este �mbito el mismo atributo con marca de "a proceso"
                        If ExisteAtributoAProceso Then
                            sdbgCampos.Columns("APROCESO").Value = False
                            oMensajes.MensajeOKOnly 943, TipoIconoMensaje.Information
                        End If
                    End If
                Else
                    sdbgCampos.Columns("APROCESO").Value = False
                End If
                sdbgCampos.Update
                
            Case "APEDIDO"
                If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Atributo Or sdbgCampos.Columns("ATRIBUTO").Value <> "" Then
                
                    If GridCheckToBoolean(sdbgCampos.Columns("APEDIDO").Value) = True Then
                        If gParametrosGenerales.gbPedidosDirectos = False Then
                            sdbgCampos.Columns("APEDIDO").Value = False
                            Exit Sub
                        Else
                            If GridCheckToBoolean(sdbgCampos.Columns("APROCESO").Value) = False Then
                                'Comprobaremos que no haya en este �mbito el mismo atributo con marca de "a proceso"
                                If ExisteAtributoAProceso Then
                                    sdbgCampos.Columns("APROCESO").Value = False
                                    sdbgCampos.Columns("APEDIDO").Value = False
                                    oMensajes.MensajeOKOnly 943, TipoIconoMensaje.Information
                                Else
                                    If m_oCampoEnEdicion.ComprobarPermitirMarcarAtributoAPedido Then
                                        sdbgCampos.Columns("APROCESO").Value = True
                                    Else
                                        sdbgCampos.Columns("APEDIDO").Value = False
                                        oMensajes.MensajeOKOnly 950, TipoIconoMensaje.Information
                                    End If
                                End If
                            Else
                                If m_oCampoEnEdicion.ComprobarPermitirMarcarAtributoAPedido Then
                                Else
                                    sdbgCampos.Columns("APEDIDO").Value = False
                                    oMensajes.MensajeOKOnly 950, TipoIconoMensaje.Information
                                End If
                            End If
                        End If
                    End If
                
                Else
                    If Not (sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.CampoGS _
                        And (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Activo _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CentroCoste _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.PartidaPresupuestaria _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Almacen _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.TipoPedido _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Factura _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.InicioAbono _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.FinAbono _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.RetencionEnGarantia _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.UnidadPedido _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.ProveedorERP)) Then
                        
                        sdbgCampos.Columns("APEDIDO").Value = False
                    End If
                End If
                sdbgCampos.Update
        End Select
    End If
        
    g_Accion = ACCFormItemModif
    cmdDeshacer.Enabled = True
End Sub
''' <summary>Limpia los valores de las listas hijas del campo material pasado como par�metro</summary>
''' <param name="lIdCampo">Id del campo material padre</param>
''' <remarks>Llamada desde: sdbddValor_CloseUp, sdbgCampos_Change</remarks>
Private Sub LimpiarValorListaHijaMaterial(ByVal lIdCampo As Long)
    Dim lIdListaHija As Long
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim bEncontrado As Boolean
    Dim iRow As Integer
    Dim vbm As Variant
    Dim i As Integer
    
    'Si el campo est� como campo padre de alguna lista se quita el valor de esa lista
    lIdListaHija = g_oFormSeleccionado.DevolverIdCampoListaHija(lIdCampo)
    If lIdListaHija > 0 Then
        'Si est� cargado el la colecci�n se actualiza el valor del objeto
        For Each oGrupo In g_oFormSeleccionado.Grupos
            For Each oCampo In oGrupo.Campos
                If oCampo.Id = lIdListaHija Then
                    bEncontrado = True
                    oCampo.valorBool = Null
                    oCampo.valorFec = Null
                    oCampo.valorNum = Null
                    oCampo.valorText = Null
                    Exit For
                End If
            Next

            If bEncontrado Then Exit For
        Next
        Set oGrupo = Nothing
        Set oCampo = Nothing

        'Si est� en el mismo grupo se actualiza el valor en el grid (el valor en BD se actualizar� al hacer el Update del grid)
        'Si no est� en el mismo grupo se actualiza directamente en BD
        bEncontrado = False
        iRow = sdbgCampos.Row
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.GetBookmark(i)
            If sdbgCampos.Columns("ID").CellValue(vbm) = lIdListaHija Then
                bEncontrado = True
                sdbgCampos.Bookmark = vbm
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
                
                g_oGrupoSeleccionado.Campos.Item(CStr(lIdListaHija)).ResetearValores
                
                If g_oGrupoSeleccionado.Campos.Item(CStr(lIdListaHija)).TipoIntroduccion = Introselec Then LimpiarValorListaHija lIdListaHija
                
                Exit For
            End If
        Next
        sdbgCampos.Row = iRow
        If Not bEncontrado Then
            Set oCampo = oFSGSRaiz.Generar_CFormCampo
            oCampo.Id = lIdListaHija
            oCampo.ResetearValores
            
            oCampo.CargarDatosFormCampo
            If oCampo.TipoIntroduccion = Introselec Then LimpiarValorListaHija lIdListaHija
        End If
    End If
End Sub

''' <summary>Limpia los valores de las listas hijas del campo pasado como par�metro</summary>
''' <param name="lIdCampo">Id del campo de la lista padre</param>
''' <remarks>Llamada desde: sdbddValor_CloseUp, sdbgCampos_Change</remarks>
Private Sub LimpiarValorListaHija(ByVal lIdCampo As Long)
    Dim iRow As Integer
    Dim lIdListaHija As Long
    Dim i As Integer
    Dim vbm As Variant
    Dim iCol As Integer
    
    'Si el campo est� como campo padre de alguna lista se quita el valor de esa lista
    lIdListaHija = g_oFormSeleccionado.DevolverIdCampoListaHija(lIdCampo)
    If lIdListaHija > 0 Then
        iRow = sdbgCampos.Row
        iCol = sdbgCampos.col
        For i = 0 To sdbgCampos.Rows - 1
            vbm = sdbgCampos.GetBookmark(i)
            If sdbgCampos.Columns("ID").CellValue(vbm) = lIdListaHija Then
                sdbgCampos.Bookmark = vbm
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""

                g_oGrupoSeleccionado.Campos.Item(CStr(lIdListaHija)).ResetearValores
                
                If sdbgCampos.Columns("INTRO").CellValue(vbm) = TAtributoIntroduccion.Introselec Then LimpiarValorListaHija lIdListaHija
                
                Exit For
            End If
        Next
        sdbgCampos.Row = iRow
        sdbgCampos.col = iCol
    End If
End Sub

Private Sub sdbgCampos_KeyDown(KeyCode As Integer, Shift As Integer)
Dim vbm As Variant
Dim arrMat As Variant
Dim lRowInit As Long
Dim i As Integer

With sdbgCampos
    If .Columns("VALOR").Value = "" Then Exit Sub
    If .col < 0 Then Exit Sub
    If .Columns(.col).Name <> "VALOR" Then Exit Sub
    If .Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then Exit Sub
    If .Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Then Exit Sub
    If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoArchivo Then Exit Sub
    If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then Exit Sub
    If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoEditor Then Exit Sub
    If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then Exit Sub
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
        If .Columns("VALOR").Locked = True And m_bModif = True Then
            g_bUpdate = True
            '_______________________________________________________________________
            'Cuando se quita el valor de tabla externa que updatee
            If .Columns("TIPO").Value = TipoCampoPredefinido.externo Then
                .Columns("VALOR").Value = ""
                g_strPK = ""
                .Update
            End If
            '�����������������������������������������������������������������������
            'Si hemos quitado el valor del material quitamos tambi�n el del art�culo
            If .Columns("CAMPO_GS").Value = TipoCampoGS.material Then
                .MoveNext
                If .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                    .Columns("COD_VALOR").Value = ""
                    .Columns("VALOR").Value = ""
                    .Update
                ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
                    vbm = .GetBookmark(-1)
                    If .Columns("COD_VALOR").CellValue(vbm) <> "" Then
                        arrMat = DevolverMaterial(.Columns("COD_VALOR").CellValue(vbm))
                        Dim oArticulos As CArticulos
                        Set oArticulos = oFSGSRaiz.Generar_CArticulos
                        oArticulos.CargarTodosLosArticulos .Columns("COD_VALOR").Value, , True, , , , arrMat(1), arrMat(2), arrMat(3), arrMat(4)
            
                        If oArticulos.Count > 0 Then
                            .Columns("COD_VALOR").Value = ""
                            .Columns("VALOR").Value = ""
                            g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorText = ""
                            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                            
                            .MoveNext
                            If .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                                .Columns("COD_VALOR").Value = ""
                                .Columns("VALOR").Value = ""
                                g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorText = ""
                                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                                
                            End If
                            .MovePrevious
                        End If
                        Set oArticulos = Nothing
                    End If
                End If
                .MovePrevious
                
                'Si es un campo material con listas hijas y elimino el valor elimino el valor de las listas hijas
                LimpiarValorListaHijaMaterial .Columns("ID").Value
            End If
            .Columns("VALOR").Value = ""
            .Columns("COD_VALOR").Value = ""
            g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorText = ""
            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
            .Update
            '------------------------------------------------------------------------------------------------------------------
            'Si hemos quitado el valor del material quitamos tambi�n el del art�culo
            'y si habia una campo tabla con articulo relacionada tb quitamos ese valor
            'AXIOMA: solo puede existir un articulo por formulario
            'ha desaparecido el articulo --> quitamos el valor para la columna de la tabla externa
            QuitarValorTablaExterna
            m_blnExisteTablaExternaParaArticulo = False
            m_blnExisteArticulo = False
            '------------------------------------------------------------------------------------------------------------------
            'Si hemos quitado el valor del pa�s quitamos tambi�n el de la provincia
            If .Columns("CAMPO_GS").Value = TipoCampoGS.Pais Then
                .MoveNext
                If .Columns("CAMPO_GS").Value = TipoCampoGS.Provincia Then
                    .Columns("COD_VALOR").Value = ""
                    .Columns("VALOR").Value = ""
                    .Update
                End If
                .MovePrevious
            End If
            
            If .Columns("CAMPO_GS").Value = TipoCampoGS.UnidadOrganizativa Then
                .MoveNext
                If .Columns("CAMPO_GS").Value = TipoCampoGS.Departamento Then
                    .Columns("COD_VALOR").Value = ""
                    .Columns("VALOR").Value = ""
                    g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorText = ""
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                    .Update
                End If
                .MovePrevious
            ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.OrganizacionCompras Then
                .MoveNext
                If .Columns("CAMPO_GS").Value = TipoCampoGS.Centro Then
                    .Columns("COD_VALOR").Value = ""
                    .Columns("VALOR").Value = ""
                    g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorText = ""
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                    .Update
                    .MoveNext
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
                        .Columns("COD_VALOR").Value = ""
                        .Columns("VALOR").Value = ""
                        g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = ""
                        Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                        .Update
                    ElseIf (.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose) Or (.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose) Then
                        g_oGrupoSeleccionado.Campos.Item(.Columns("ID").Value).EliminarArticulosEnDesglose
                    End If
                    'Borrar el articulo
                    If .Columns("CAMPO_GS").CellValue(.GetBookmark(-1)) = TipoCampoGS.Centro Then
                        lRowInit = .Row
                        For i = lRowInit To .Rows - 1
                            .Row = i
                            If .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                                .Columns("VALOR").Value = ""
                                .Columns("COD_VALOR").Value = ""
                                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                                .Row = i + 1
                                If .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                                    .Columns("VALOR").Value = ""
                                    .Columns("COD_VALOR").Value = ""
                                End If
                                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                                Exit For
                            End If
                        Next i
                        .Row = lRowInit
                    End If
                    .MovePrevious
 
                ElseIf (.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose) Or (.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose) Then
                    g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).EliminarArticulosEnDesglose
                    
                End If
               
                .MovePrevious
                        ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.Centro Then
                    .MoveNext
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Then
                        .Columns("COD_VALOR").Value = ""
                        .Columns("VALOR").Value = ""
                        g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).valorNum = ""
                        Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                        .Update
                    End If
                    'Borrar el articulo
                    If .Columns("CAMPO_GS").CellValue(.GetBookmark(-2)) = TipoCampoGS.OrganizacionCompras Then
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
                        g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).EliminarArticulosEnDesglose
                    Else
                        lRowInit = .Row
                        For i = lRowInit To .Rows - 1
                            .Row = i
                            If .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or .Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
                                .Columns("VALOR").Value = ""
                                .Columns("COD_VALOR").Value = ""
                                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                                .Row = i + 1
                                If .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                                    .Columns("VALOR").Value = ""
                                    .Columns("COD_VALOR").Value = ""
                                End If
                                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                                Exit For
                            End If
                        Next i
                        .Row = lRowInit
                    End If
                End If
                .MovePrevious
            
            ElseIf .Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                    'Si hemos quitado el valor de la Denominacion quitar tambien el codigo
                        .MovePrevious
                        If .Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
                            .Columns("COD_VALOR").Value = ""
                            .Columns("VALOR").Value = ""
                            Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value))
                            .Update
                        End If
                        .MoveNext
            End If
            
        End If
    End If
End With
End Sub
''' Control para que los tipos TipoTextoCorto, TipoTextoMedio y TipoTextoLargo no superen el m�ximo permitido
Private Sub sdbgCampos_KeyPress(KeyAscii As Integer)
Dim vMaxLength As Variant
    If sdbgCampos.Columns("VALOR").Value = "" Then Exit Sub
    If sdbgCampos.col < 0 Then Exit Sub
    If sdbgCampos.Columns(sdbgCampos.col).Name <> "VALOR" Then Exit Sub
    If KeyAscii = vbKeyBack Then Exit Sub

    If (sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.DenArticulo) Then
        If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Then
            vMaxLength = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).MaxLength
            If IsNull(vMaxLength) Or vMaxLength = "" Then
                If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Then
                    vMaxLength = 100
                ElseIf sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Then
                    vMaxLength = 800
                Else
                    vMaxLength = -1
                End If
            End If
            If CInt(vMaxLength) > 0 And (Len(sdbgCampos.Columns("VALOR").Value) >= CInt(vMaxLength)) And sdbgCampos.ActiveCell.SelLength = 0 Then
                KeyAscii = 0
            End If
        End If
    End If

    If KeyAscii <> vbKeyBack Then
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
            If Len(sdbgCampos.Columns("VALOR").Value) + 1 > basParametros.gLongitudesDeCodigos.giLongCodDENART And sdbgCampos.ActiveCell.SelLength = 0 Then
                KeyAscii = 0
            End If
        End If
    End If

    If KeyAscii = vbKeyEscape Then

        If sdbgCampos.DataChanged = False Then

            sdbgCampos.CancelUpdate
            sdbgCampos.DataChanged = False
            
            Set m_oCampoEnEdicion = Nothing
            
            cmdDeshacer.Enabled = False
            g_Accion = ACCFormularioCons
        End If
    End If

End Sub

''' <summary>
''' Mostrar un menu PopUp donde ponga Marcar como Titulo, y a continuacion se a�ade en la fila seleccionada
''' un bmp al caption de la linea que ponga Titulo.
''' Como no me deja crear un menu nuevo porque se ha llegado al limite maximo de menus utilizo uno que ya esta
''' hecho, oculto las opciones que tenia y muestro la nueva
''' </summary>
''' <param name="button">que boton del raton hemos pulsado</param>
''' <param name="shift">accion del shift</param>
''' <param name="X">posicion X</param>
''' <param name="Y">posicion Y</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbgCampos_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim iIndex As Integer
    Dim NEnabled As Integer
    Dim bMostrarMenu As Boolean
    
    If Button = 2 Then
        iIndex = sdbgCampos.RowContaining(Y)
        If iIndex > -1 Then
            sdbgCampos.Row = iIndex
            
            If sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.Desglose And sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoDesglose _
               And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoSC.ArchivoEspecific And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoSC.importe _
               And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoSC.PrecioUnitario And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoSC.Cantidad _
               And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.PRES1 And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.PRES2 _
               And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.Pres3 And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.Pres4 _
               And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.FormaPago And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.Unidad _
               And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.UnidadOrganizativa And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.RetencionEnGarantia _
               And sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoArchivo And sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoBoolean Then
               
               If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoNumerico _
                And (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Almacen Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Empresa) Then
                    If sdbgCampos.Row >= 0 Then
                        MDI.mnuPOPUPNewCopyForm.Item(2).Visible = True
                        MDI.mnuPOPUPNewCopyForm.Item(3).Visible = True

                        If sdbgCampos.Columns("TITULO").Value = 1 Then
                            MDI.mnuPOPUPNewCopyForm.Item(2).Enabled = False
                            MDI.mnuPOPUPNewCopyForm.Item(3).Enabled = True
                        Else
                            MDI.mnuPOPUPNewCopyForm.Item(2).Enabled = True
                            MDI.mnuPOPUPNewCopyForm.Item(3).Enabled = False
                        End If
                        MDI.mnuPOPUPNewCopyForm.Item(0).Visible = False
                        MDI.mnuPOPUPNewCopyForm.Item(1).Visible = False
                        PopupMenu MDI.mnuPOPUPCopiarForm
                    End If
               Else
                    If sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoNumerico Then
                        If sdbgCampos.Row >= 0 Then
                            bMostrarMenu = True
                            
                            MDI.mnuPOPUPNewCopyForm.Item(2).Visible = True
                            MDI.mnuPOPUPNewCopyForm.Item(3).Visible = True
                            If sdbgCampos.Columns("TITULO").Value = 1 Then
                                MDI.mnuPOPUPNewCopyForm.Item(2).Enabled = False
                                MDI.mnuPOPUPNewCopyForm.Item(3).Enabled = True
                            Else
                                MDI.mnuPOPUPNewCopyForm.Item(2).Enabled = True
                                MDI.mnuPOPUPNewCopyForm.Item(3).Enabled = False
                            End If
                            MDI.mnuPOPUPNewCopyForm.Item(0).Visible = False
                            MDI.mnuPOPUPNewCopyForm.Item(1).Visible = False
                            'Solo mostraremos la opci�n de marcar el campo como "Denominacion de proceso" si es texto corto y no puede ser campo de sistema
                            If (sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto And sdbgCampos.Columns("CAMPO_GS").Value = "") Or (sdbgCampos.Columns("DENPROCESO").Value = 1) Then
                                MDI.mnuPOPUPNewCopyForm.Item(4).Visible = True
                                MDI.mnuPOPUPNewCopyForm.Item(5).Visible = True
                                If sdbgCampos.Columns("TITULO").Value = 1 And sdbgCampos.Columns("DENPROCESO").Value = 1 Then
                                    MDI.mnuPOPUPNewCopyForm.Item(4).Enabled = False
                                    MDI.mnuPOPUPNewCopyForm.Item(5).Enabled = True
                                ElseIf sdbgCampos.Columns("TITULO").Value = 1 And sdbgCampos.Columns("DENPROCESO").Value = 0 Then
                                    MDI.mnuPOPUPNewCopyForm.Item(4).Enabled = True
                                    MDI.mnuPOPUPNewCopyForm.Item(5).Enabled = False
                                ElseIf sdbgCampos.Columns("TITULO").Value = 0 And sdbgCampos.Columns("DENPROCESO").Value = 1 Then
                                    MDI.mnuPOPUPNewCopyForm.Item(4).Enabled = False
                                    MDI.mnuPOPUPNewCopyForm.Item(5).Enabled = True
                                Else
                                    MDI.mnuPOPUPNewCopyForm.Item(4).Enabled = True
                                    MDI.mnuPOPUPNewCopyForm.Item(5).Enabled = False
                                End If
                            Else
                                MDI.mnuPOPUPNewCopyForm.Item(4).Visible = False
                                MDI.mnuPOPUPNewCopyForm.Item(5).Visible = False
                            End If
                            
                            If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Normal And sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec Then
                                If g_oFormSeleccionado.ComprobarAsociadoNoConformidad Then
                                    'Si se trata de una lista se puede marcar como peso
                                    MDI.mnuPOPUPNewCopyForm.Item(6).Visible = True
                                    MDI.mnuPOPUPNewCopyForm.Item(7).Visible = True
                                    MDI.mnuPOPUPNewCopyForm.Item(6).Enabled = (sdbgCampos.Columns("PESO").Value = 0)
                                    MDI.mnuPOPUPNewCopyForm.Item(7).Enabled = (sdbgCampos.Columns("PESO").Value = 1)
                                    'El t�tulo no aplica a las NC
                                    MDI.mnuPOPUPNewCopyForm.Item(2).Visible = False
                                    MDI.mnuPOPUPNewCopyForm.Item(3).Visible = False
                                Else
                                    MDI.mnuPOPUPNewCopyForm.Item(6).Visible = False
                                    MDI.mnuPOPUPNewCopyForm.Item(7).Visible = False
                                End If
                            Else
                                MDI.mnuPOPUPNewCopyForm.Item(6).Visible = False
                                MDI.mnuPOPUPNewCopyForm.Item(7).Visible = False
                                
                                If g_oFormSeleccionado.ComprobarAsociadoNoConformidad Then
                                    'Marcar como t�tulo no se aplica a las no conformidades
                                    'Los submen�s correspondientes al t�tulo son los �nicos visibles en este caso, as� que no se muestra el men�
                                    bMostrarMenu = False
                                End If
                            End If
                        
                            If bMostrarMenu Then PopupMenu MDI.mnuPOPUPCopiarForm
                        End If
                    Else
                        'Un campo num�rico se puede marcar como peso
                        MDI.mnuPopUpFSQAVarCal.Item(5).Visible = False
                        MDI.mnuPopUpFSQAVarCal.Item(6).Visible = False
                        If Not sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.externo Then
                            If g_oFormSeleccionado.ComprobarAsociadoNoConformidad Then
                                MDI.mnuPopUpFSQAVarCal.Item(5).Visible = True
                                MDI.mnuPopUpFSQAVarCal.Item(6).Visible = True
                                MDI.mnuPopUpFSQAVarCal.Item(5).Enabled = (sdbgCampos.Columns("PESO").Value = 0)
                                MDI.mnuPopUpFSQAVarCal.Item(6).Enabled = (sdbgCampos.Columns("PESO").Value = 1)
                                NEnabled = 2
                            End If
                        End If
                        
                        If sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoNoConformidad.EnviosCorrectos And sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoNoConformidad.PiezasDefectuosas And sdbgCampos.Columns("CAMPO_GS").Value = "" Then
                            MDI.mnuPopUpFSQAVarCal.Item(1).Enabled = True
                            MDI.mnuPopUpFSQAVarCal.Item(2).Enabled = True
                            MDI.mnuPopUpFSQAVarCal.Item(3).Enabled = False
                            MDI.mnuPopUpFSQAVarCal.Item(4).Enabled = False
                            NEnabled = NEnabled + 2
                            If m_bArt Then
                                If m_bConPiezasDefectuosas Then
                                    MDI.mnuPopUpFSQAVarCal.Item(1).Enabled = False
                                    NEnabled = NEnabled - 1
                                End If
                                If m_bConEnviosCorrectos Then
                                    MDI.mnuPopUpFSQAVarCal.Item(2).Enabled = False
                                    NEnabled = NEnabled - 1
                                End If
                            Else
                                MDI.mnuPopUpFSQAVarCal.Item(1).Enabled = False
                                MDI.mnuPopUpFSQAVarCal.Item(2).Enabled = False
                                NEnabled = NEnabled - 2
                            End If

                            If NEnabled > 0 Then PopupMenu MDI.mnuPopUpFSQAVarCalCab
                        Else
                            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.EnviosCorrectos Then
                                MDI.mnuPopUpFSQAVarCal.Item(1).Enabled = False
                                MDI.mnuPopUpFSQAVarCal.Item(2).Enabled = False
                                MDI.mnuPopUpFSQAVarCal.Item(3).Enabled = False
                                MDI.mnuPopUpFSQAVarCal.Item(4).Enabled = True
    
                                PopupMenu MDI.mnuPopUpFSQAVarCalCab
                            ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas Then
                                MDI.mnuPopUpFSQAVarCal.Item(1).Enabled = False
                                MDI.mnuPopUpFSQAVarCal.Item(2).Enabled = False
                                MDI.mnuPopUpFSQAVarCal.Item(3).Enabled = True
                                MDI.mnuPopUpFSQAVarCal.Item(4).Enabled = False
                                
                                PopupMenu MDI.mnuPopUpFSQAVarCalCab
                            End If
                        End If
                    End If
               End If
            Else
                'Si se trata de un campo de tipo num�rico, lista o claculado se puede asignar como peso
                If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoNumerico Or (sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Normal And sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec) Or _
                    sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                    MDI.mnuPOPUPNewCopyForm.Item(0).Visible = False
                    MDI.mnuPOPUPNewCopyForm.Item(1).Visible = False
                    MDI.mnuPOPUPNewCopyForm.Item(2).Visible = False
                    MDI.mnuPOPUPNewCopyForm.Item(3).Visible = False
                    MDI.mnuPOPUPNewCopyForm.Item(4).Visible = False
                    MDI.mnuPOPUPNewCopyForm.Item(5).Visible = False
                    MDI.mnuPOPUPNewCopyForm.Item(6).Visible = True
                    MDI.mnuPOPUPNewCopyForm.Item(7).Visible = True
                End If
            End If
        End If
    End If
End Sub
Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim oIdioma As CIdioma
Dim oCampo As CFormItem

With sdbgCampos
    If .col < 0 Then Exit Sub

    .Columns("VALOR").DropDownHwnd = 0
    
    If m_bModif = True Then
        If g_oFormSeleccionado.Multiidioma Then
            For Each oIdioma In m_oIdiomas
                .Columns(CStr(oIdioma.Cod)).Locked = False
            Next
        Else
            .Columns(gParametrosInstalacion.gIdioma).Locked = False
        End If
        
        If .Columns("TIPO").Value = TipoCampoPredefinido.externo Then
            .Columns("VALOR").Style = ssStyleEditButton
            .Columns(15).Style = ssStyleEditButton
        ElseIf .Columns("TIPO").Value = TipoCampoPredefinido.Servicio Then
            'Si es servicio externo no se puede modificar valor
            .Columns("VALOR").Locked = True
        ElseIf .Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then      'Es un campo de GS
            Select Case .Columns("CAMPO_GS").Value
                Case TipoCampoGS.ProveedorERP
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddOtrosCombos.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoSC.ArchivoEspecific
                    .Columns("VALOR").Style = ssStyleEditButton
                    .Columns("VALOR").Locked = True
                Case TipoCampoSC.DescrBreve, TipoCampoSC.Cantidad, TipoCampoSC.importe, TipoCampoSC.PrecioUnitario, TipoCampoGS.NumSolicitERP
                    .Columns("VALOR").Style = ssStyleEdit
                    .Columns("VALOR").Locked = False
                Case TipoCampoSC.DescrDetallada, TipoCampoGS.Proveedor, TipoCampoGS.Desglose
                    .Columns("VALOR").Style = ssStyleEditButton
                    .Columns("VALOR").Locked = True
                    
                    'Si se tiene solamente acceso exclusivo al m�dulo de Solicitudes de compras no dejar� modificar la denominaci�n del campo desglose
                    If .Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Then
                        If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.AccesoFSWSSolicCompra Then
                            For Each oIdioma In m_oIdiomas
                                .Columns(CStr(oIdioma.Cod)).Locked = True
                            Next
                        End If
                    End If
                Case TipoCampoGS.Dest
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddDestinos.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.FormaPago
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddPagos.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                    ' Comprobar si ha de ser FECHA_VALOR_DEFECTO
                    Set oCampo = oFSGSRaiz.Generar_CFormCampo
                    oCampo.Id = g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).Id
                    oCampo.CargarFECHAVALORDEFECTO
                    
                    If oCampo.FECHAVALORDEFECTO = True Then
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                    Else
                        .Columns("VALOR").Style = ssStyleComboBox
                        .Columns("VALOR").DropDownHwnd = sdbddFecha.hWnd
                        .Columns("VALOR").Locked = True
                    End If
                Case TipoCampoGS.PRES1, TipoCampoGS.PRES2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                    .Columns("VALOR").Style = ssStyleEditButton
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.Unidad, TipoCampoGS.Pais, TipoCampoGS.Provincia, TipoCampoGS.Moneda, TipoCampoGS.UnidadPedido
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddOtrosCombos.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.material
                    .Columns("VALOR").Style = ssStyleEditButton
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.CodArticulo
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddArticulos.hWnd
                    .Columns("VALOR").Locked = False  'deja modificar sin seleccionar del combo para introducir art�culos no codificados
                    m_blnQuitarValorTablaExterna = True
                Case TipoCampoGS.NuevoCodArticulo
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddArticulos.hWnd
                    .Columns("VALOR").Locked = Not (g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").Value)).AnyadirArt) 'deja modificar sin seleccionar del combo para introducir art�culos no codificados
                    m_blnQuitarValorTablaExterna = True
                Case TipoCampoGS.DenArticulo
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddArticulos.hWnd
                    .Columns("VALOR").Locked = Not (esGenerico(.Columns("COD_VALOR").CellValue(.GetBookmark(-1)), g_oGrupoSeleccionado.Campos.Item(CStr(.Columns("ID").CellValue(.GetBookmark(-1)))).AnyadirArt))
                Case TipoCampoGS.CampoPersona
                    .Columns("VALOR").Style = ssStyleEditButton
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.UnidadOrganizativa
                    .Columns("VALOR").Style = ssStyleEditButton
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.Departamento
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddOtrosCombos.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.OrganizacionCompras, TipoCampoGS.Centro
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddOtrosCombos.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.Almacen
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddAlmacenes.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.Empresa
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddEmpresas.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoSC.PrecioUnitarioAdj, TipoCampoSC.ProveedorAdj, TipoCampoSC.CantidadAdj
                    .Columns("VALOR").Style = ssStyleEdit
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.ImporteSolicitudesVinculadas, TipoCampoGS.RefSolicitud
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.CentroCoste
                    .Columns("VALOR").Style = ssStyleEditButton
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.Activo, TipoCampoGS.PartidaPresupuestaria, TipoCampoGS.AnyoImputacion
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.TipoPedido
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddTiposPedido.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.DesgloseActividad, TipoCampoGS.DesgloseFactura, TipoCampoGS.DesgloseDePedido
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.InicioAbono, TipoCampoGS.FinAbono
                    .Columns("VALOR").Style = ssStyleEditButton
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.RetencionEnGarantia, TipoCampoGS.Factura
                    .Columns("VALOR").Style = ssStyleEdit
                    .Columns("VALOR").Locked = False
                Case TipoCampoGS.CodComprador
                    'Cargar el combo
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddCompradores.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.RenovacionAutomatica
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.PeriodoRenovacion
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                    .Columns("VALOR").Locked = True
                Case TipoCampoGS.EstadoHomologacion
                    .Columns("VALOR").Style = ssStyleComboBox
                    .Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                    .Columns("VALOR").Locked = True
                Case Else
                    .Columns("VALOR").Locked = False
            End Select
    
        ElseIf .Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
            'Si es un campo calculado no se podr� modificar su valor:
            .Columns("VALOR").Locked = True
            'Si inicialmente era un campo atributo, no se deja cambiar la denominaci�n
            If .Columns("ATRIBUTO").Value <> "" Then
                If g_oFormSeleccionado.Multiidioma Then
                    For Each oIdioma In m_oIdiomas
                        .Columns(CStr(oIdioma.Cod)).Locked = True
                    Next
                Else
                    .Columns(gParametrosInstalacion.gIdioma).Locked = True
                End If
            End If
        ElseIf .Columns("CAMPO_GS").Value = TipoCampoNoConformidad.FechaImputacionDefecto Then
            .Columns("VALOR").Locked = True
        Else   'Es un atributo o campo normal:
                        
            If .Columns("TIPO").Value = TipoCampoPredefinido.Atributo Then
                If g_oFormSeleccionado.Multiidioma Then
                    For Each oIdioma In m_oIdiomas
                        .Columns(CStr(oIdioma.Cod)).Locked = True
                    Next
                Else
                    .Columns(gParametrosInstalacion.gIdioma).Locked = True
                End If
            End If
            If .Columns("INTRO").Value = TAtributoIntroduccion.IntroLibre Then
                Select Case .Columns("SUBTIPO").Value
                    Case TiposDeAtributos.TipoFecha
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = False
                    Case TiposDeAtributos.TipoBoolean
                        .Columns("VALOR").Style = ssStyleComboBox
                        sdbddValor.RemoveAll
                        sdbddValor.AddItem ""
                        sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
                        .Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                        .Columns("VALOR").Locked = False
                    Case TiposDeAtributos.TipoArchivo, TiposDeAtributos.TipoEditor
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                    Case TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = False
                    Case TiposDeAtributos.TipoDesglose
                        .Columns("VALOR").Style = ssStyleEditButton
                        .Columns("VALOR").Locked = True
                    Case TiposDeAtributos.TipoEnlace
                        .Columns("VALOR").Style = ssStyleEdit
                        .Columns("VALOR").Locked = True
                    Case Else
                        .Columns("VALOR").Style = ssStyleEdit
                        .Columns("VALOR").Locked = False
                End Select
            ElseIf .Columns("INTRO").Value = TAtributoIntroduccion.Introselec Then
                'Selecci�n mediante una lista
                .Columns("VALOR").Style = ssStyleComboBox
                sdbddValor.RemoveAll
                sdbddValor.AddItem ""
                
                If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoNumerico Then
                    sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
                    .Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                Else
                    sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
                    
                    If g_oFormSeleccionado.Multiidioma = True And (.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo) Then
                        ssdbValorMultiIdi.RemoveAll
                        ssdbValorMultiIdi.AddItem ""
                        .Columns("VALOR").DropDownHwnd = ssdbValorMultiIdi.hWnd
                    Else
                        .Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                    End If
                End If
                .Columns("VALOR").Locked = True
            Else
                .Columns("VALOR").Style = ssStyleEdit
                .Columns("VALOR").Locked = True
            End If
        End If
    
    Else
        'si no hay permisos de modificaci�n solo podr� ver los botones de archivos y descripci�n detallada:
        If .Columns("TIPO").Value = TipoCampoPredefinido.externo Then
            .Columns("VALOR").Style = ssStyleEditButton
            .Columns(15).Style = ssStyleEditButton
        ElseIf .Columns("TIPO").Value = TipoCampoPredefinido.CampoGS Then
            If .Columns("CAMPO_GS").Value = TipoCampoSC.ArchivoEspecific Or .Columns("CAMPO_GS").Value = TipoCampoSC.DescrDetallada Or .Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Then
                .Columns("VALOR").Style = ssStyleEditButton
            Else
                .Columns("VALOR").Style = ssStyleEdit
            End If
        Else
            If .Columns("SUBTIPO").Value = TiposDeAtributos.TipoArchivo Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or .Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
                .Columns("VALOR").Style = ssStyleEditButton
            Else
                .Columns("VALOR").Style = ssStyleEdit
            End If
        End If
    End If
End With
End Sub
Private Sub sdbgCampos_RowLoaded(ByVal Bookmark As Variant)
    Dim oIdioma As CIdioma
    
    With sdbgCampos
        If .Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.externo Then
            .Columns("APROCESO").CellStyleSet "Gris"
            .Columns("APEDIDO").CellStyleSet "Gris"
            .Columns("VALOR").Style = ssStyleEditButton
            .Columns(15).Style = ssStyleEditButton
            If .Columns("TITULO").Value = 1 Then
                If g_oFormSeleccionado.Multiidioma Then
                    For Each oIdioma In m_oIdiomas
                        .Columns(oIdioma.Cod).CellStyleSet "TituloPeque�o"
                    Next
                Else
                    For Each oIdioma In m_oIdiomas
                        .Columns(oIdioma.Cod).CellStyleSet "Titulo" & oIdioma.Cod
                    Next
                End If
            End If
        Else
            'Los campos de tipo desglose se muestran con fondo gris en la columna de valor:
            If .Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoGS.Desglose Or (.Columns("SUBTIPO").CellValue(Bookmark) = TiposDeAtributos.TipoDesglose And .Columns("CAMPO_GS").CellValue(Bookmark) <> TipoCampoGS.DesgloseActividad) Then
                .Columns("VALOR").CellStyleSet "Gris"
            Else
                .Columns("VALOR").CellStyleSet ""
            End If
            
            If .Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
                If .Columns("TITULO").Value = 1 Then
                    If g_oFormSeleccionado.Multiidioma Then
                        For Each oIdioma In m_oIdiomas
                            .Columns(oIdioma.Cod).CellStyleSet "TituloAmarilloPeque�o"
                        Next
                    Else
                        For Each oIdioma In m_oIdiomas
                            .Columns(oIdioma.Cod).CellStyleSet "TituloAmarillo" & oIdioma.Cod
                        Next
                    End If
                Else
                    For Each oIdioma In m_oIdiomas  'Los campos predefinidos se ven en amarillo:
                        .Columns(oIdioma.Cod).CellStyleSet "Amarillo"
                    Next
                End If
             
            ElseIf .Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
                For Each oIdioma In m_oIdiomas   'Los campos calculados en azul:
                    If .Columns("PESO").Value = 1 Then
                        .Columns(oIdioma.Cod).CellStyleSet "CalculadoPeso" & oIdioma.Cod
                    Else
                        .Columns(oIdioma.Cod).CellStyleSet "Calculado"
                    End If
                Next
                .Columns("VALOR").CellStyleSet "CalculadoValor"
            Else
                If .Columns("TITULO").Value = 1 And .Columns("DENPROCESO").Value = 0 Then
                    If g_oFormSeleccionado.Multiidioma Then
                        For Each oIdioma In m_oIdiomas
                            .Columns(oIdioma.Cod).CellStyleSet "TituloPeque�o"
                        Next
                    Else
                        For Each oIdioma In m_oIdiomas
                            .Columns(oIdioma.Cod).CellStyleSet "Titulo" & oIdioma.Cod
                        Next
                    End If
                ElseIf .Columns("TITULO").Value = 1 And .Columns("DENPROCESO").Value = 1 Then
                    If g_oFormSeleccionado.Multiidioma Then
                        For Each oIdioma In m_oIdiomas
                            .Columns(oIdioma.Cod).CellStyleSet "TituloDenProcesoPeque�o"
                        Next
                    Else
                        For Each oIdioma In m_oIdiomas
                            .Columns(oIdioma.Cod).CellStyleSet "TituloDenProceso" & oIdioma.Cod
                        Next
                    End If
                ElseIf .Columns("TITULO").Value = 0 And .Columns("DENPROCESO").Value = 1 Then
                    If g_oFormSeleccionado.Multiidioma Then
                        For Each oIdioma In m_oIdiomas
                            .Columns(oIdioma.Cod).CellStyleSet "DenProcesoPeque�o"
                        Next
                    Else
                        For Each oIdioma In m_oIdiomas
                            .Columns(oIdioma.Cod).CellStyleSet "DenProceso" & oIdioma.Cod
                        Next
                    End If
                ElseIf .Columns("PESO").Value = 1 Then
                    For Each oIdioma In m_oIdiomas
                        .Columns(oIdioma.Cod).CellStyleSet "Peso" & oIdioma.Cod
                    Next
                End If
            End If
            
            If (.Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoGS.PRES1 Or _
            .Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoGS.PRES2 Or _
            .Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoGS.Pres3 Or _
            .Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoGS.Pres4) And _
            .Columns("BAJALOG").Value Then
                .Columns("VALOR").CellStyleSet "PresBajaLog"
            End If
            
            If (.Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoGS.UnidadOrganizativa And _
               .Columns("BAJALOG").Value) Then
                    .Columns("VALOR").CellStyleSet "PresBajaLog"
            End If
            
            If (.Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoGS.Departamento And _
            .Columns("BAJALOG").Value) Then
                    .Columns("VALOR").CellStyleSet "PresBajaLog"
            End If
            
            If .Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Atributo Then
                If .Columns("TITULO").Value = 1 Then
                    If g_oFormSeleccionado.Multiidioma Then
                        For Each oIdioma In m_oIdiomas
                            .Columns("ATRIBUTO").CellStyleSet "Azul"
                            .Columns("APROCESO").CellStyleSet ""
                            .Columns("APEDIDO").CellStyleSet ""
                            .Columns(oIdioma.Cod).CellStyleSet "TituloAzulPeque�o"
                        Next
                    Else
                        For Each oIdioma In m_oIdiomas
                            .Columns("ATRIBUTO").CellStyleSet "Azul"
                            .Columns("APROCESO").CellStyleSet ""
                            .Columns("APEDIDO").CellStyleSet ""
                            .Columns(oIdioma.Cod).CellStyleSet "TituloAzul" & oIdioma.Cod
                        Next
                    End If
                Else
                    .Columns("ATRIBUTO").CellStyleSet "Azul"
                    .Columns("APROCESO").CellStyleSet ""
                    .Columns("APEDIDO").CellStyleSet ""
                            
                    For Each oIdioma In m_oIdiomas
                        .Columns(oIdioma.Cod).CellStyleSet "Azul"
                    Next
                End If
            ElseIf .Columns("ATRIBUTO").Value <> "" Then 'Campo atributo que pasa a ser calculado
                .Columns("APROCESO").CellStyleSet ""
                .Columns("APEDIDO").CellStyleSet ""
            ElseIf CamposSistemaAProceso(.Columns("CAMPO_GS").CellValue(Bookmark)) Or _
                (.Columns("CAMPO_GS").CellValue(Bookmark) = TipoCampoSC.importe And ssTabGrupos.selectedItem.Index = 1) Or _
                .Columns("SUBTIPO").CellValue(Bookmark) = TiposDeAtributos.TipoTextoLargo Or _
                .Columns("SUBTIPO").CellValue(Bookmark) = TiposDeAtributos.TipoArchivo Then
                .Columns("APROCESO").CellStyleSet ""
                .Columns("APEDIDO").CellStyleSet "Gris"
            Else
                .Columns("APROCESO").CellStyleSet "Gris"
                
                Select Case .Columns("CAMPO_GS").CellValue(Bookmark)
                    Case TipoCampoGS.Activo, TipoCampoGS.CentroCoste, TipoCampoGS.PartidaPresupuestaria, TipoCampoGS.Almacen, _
                                TipoCampoGS.TipoPedido, TipoCampoGS.Factura, TipoCampoGS.InicioAbono, TipoCampoGS.FinAbono, _
                                TipoCampoGS.RetencionEnGarantia, TipoCampoGS.UnidadPedido, TipoCampoGS.ProveedorERP
                        .Columns("APEDIDO").CellStyleSet ""
                    Case Else
                        .Columns("APEDIDO").CellStyleSet "Gris"
                End Select
            End If
            
            If .Columns("CAMPO_GS").Value = TipoCampoNoConformidad.EnviosCorrectos Then
                If .Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                    If Not g_oFormSeleccionado.Multiidioma Then
                        .Columns(gParametrosInstalacion.gIdioma).CellStyleSet "EnviosCorrectosSumatorio" & gParametrosInstalacion.gIdioma
                    Else
                        For Each oIdioma In m_oIdiomas
                           .Columns(oIdioma.Cod).CellStyleSet "ECSumatorio" & oIdioma.Cod
                        Next
                        
                    End If
                Else
                    If Not g_oFormSeleccionado.Multiidioma Then
                        .Columns(gParametrosInstalacion.gIdioma).CellStyleSet "EnviosCorrectos" & gParametrosInstalacion.gIdioma
                    Else
                        For Each oIdioma In m_oIdiomas
                           .Columns(oIdioma.Cod).CellStyleSet "EC" & oIdioma.Cod
                        Next
                        
                    End If
                End If
            ElseIf .Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas Then
                If .Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                    If Not g_oFormSeleccionado.Multiidioma Then
                        .Columns(gParametrosInstalacion.gIdioma).CellStyleSet "PiezasDefectuosasSumatorio" & gParametrosInstalacion.gIdioma
                    Else
                        For Each oIdioma In m_oIdiomas
                           .Columns(oIdioma.Cod).CellStyleSet "PDSumatorio" & oIdioma.Cod
                        Next
                        
                    End If
                Else
                    If Not g_oFormSeleccionado.Multiidioma Then
                        .Columns(gParametrosInstalacion.gIdioma).CellStyleSet "PiezasDefectuosas" & gParametrosInstalacion.gIdioma
                    Else
                        For Each oIdioma In m_oIdiomas
                           .Columns(oIdioma.Cod).CellStyleSet "PD" & oIdioma.Cod
                        Next
                    End If
                End If
            End If
        End If
    End With
End Sub

Private Sub ssdbValorMultiIdi_CloseUp()
    sdbgCampos.Columns("COD_VALOR").Value = ssdbValorMultiIdi.Columns("ID").Value
    
    If sdbgCampos.Columns("INTRO").Value = TAtributoIntroduccion.Introselec And sdbgCampos.Columns("COD_VALOR").Value <> sdbddValor.Columns("ID").Value Then
        If sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoCorto Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoMedio Or _
            sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoTextoLargo Then
            LimpiarValorListaHija sdbgCampos.Columns("ID").Value
        End If
    End If
End Sub

Private Sub ssdbValorMultiIdi_DropDown()
Dim oLista As CCampoValorListas
Dim oElem As CCampoValorLista
Dim sCadena As String
Dim oIdioma As CIdioma
Dim iOrdenPadre As Integer
Dim lConCampoPadre As Long

    If Not m_bModif Then
        ssdbValorMultiIdi.Enabled = False
        Exit Sub
    End If

    ssdbValorMultiIdi.RemoveAll
    lConCampoPadre = 0
    
    'Mirar si la lista tiene campoPadre seleccionado
    If g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).IDListaCampoPadre > 0 Then
        lConCampoPadre = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).IDListaCampoPadre
        If IsNumeric(g_oGrupoSeleccionado.Campos.Item(CStr(lConCampoPadre)).valorNum) Then
            iOrdenPadre = g_oGrupoSeleccionado.Campos.Item(CStr(g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).IDListaCampoPadre)).valorNum
        End If
    End If
        
    If (lConCampoPadre = 0) Or (lConCampoPadre > 0 And iOrdenPadre > 0) Then
        'Selecci�n mediante lista de valores de tipo texto:
        g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).CargarValoresLista iOrdenCampoPadre:=iOrdenPadre
        
        Set oLista = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).ValoresLista
        
        For Each oElem In oLista
            sCadena = oElem.Orden & Chr(m_lSeparador) & oElem.valorText.Item(CStr(gParametrosInstalacion.gIdioma)).Den
            
            For Each oIdioma In m_oIdiomas
                If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                    sCadena = sCadena & Chr(m_lSeparador) & oElem.valorText.Item(CStr(oIdioma.Cod)).Den
                End If
            Next
                    
            ssdbValorMultiIdi.AddItem sCadena
            
        Next
        Set oLista = Nothing
    End If
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
End Sub
Private Sub ssdbValorMultiIdi_InitColumnProps()
    ssdbValorMultiIdi.DataFieldList = "Column 1"
    ssdbValorMultiIdi.DataFieldToDisplay = "Column 1"
End Sub
Private Sub ssdbValorMultiIdi_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    ssdbValorMultiIdi.MoveFirst
    
    If Text <> "" Then
        For i = 0 To ssdbValorMultiIdi.Rows - 1
            bm = ssdbValorMultiIdi.GetBookmark(i)
            If UCase(Text) = UCase(Mid(ssdbValorMultiIdi.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbgCampos.Columns("VALOR").Value = Mid(ssdbValorMultiIdi.Columns(1).CellText(bm), 1, Len(Text))
                ssdbValorMultiIdi.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
Private Sub SSTabGrupos_Click()
    If g_oFormSeleccionado Is Nothing Then Exit Sub
    If m_bCancel Then Exit Sub
    
    'si hay cambios los guarda
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
        If m_bModError = True Then Exit Sub
    End If
    
    GrupoSeleccionado
    
    cmdModifGr.Enabled = True
    
    If Not m_bConDesgloseFactura And Not m_bConDesgloseDePedido Then
        cmdMoverGrupo.Enabled = True
    End If
    
    If g_oFormSeleccionado.Grupos.Count > 1 Then
        cmdEliminarGr.Enabled = True
    Else
        cmdEliminarGr.Enabled = False
    End If
End Sub

Private Sub LimpiarCampos()
    Dim i As Integer
    
    'Borra los tabs de los grupos
    i = ssTabGrupos.Tabs.Count
    While i > 1
        ssTabGrupos.Tabs.Remove i
        DoEvents
        i = ssTabGrupos.Tabs.Count
    Wend
    
    ssTabGrupos.Tabs(1).Tag = ""
    ssTabGrupos.Tabs(1).caption = m_sDatosGen
    
    sdbgCampos.RemoveAll
    
    m_bCargando = True
    chkMultiidioma.Value = vbUnchecked
    m_bCargando = False
    
    cmdElimForm.Enabled = False
    cmdModifForm.Enabled = False
    cmdAnyaCalculo.Enabled = False
    cmdAnyaGr.Enabled = False
    cmdEliminarGr.Enabled = False
    cmdModifGr.Enabled = False
    cmdMoverGrupo.Enabled = False
    cmdRestaurar.Enabled = False
    cmdListado.Enabled = False
    cmdAnyaItem.Enabled = False
    cmdElimItem.Enabled = False
    cmdSubir.Enabled = False
    cmdBajar.Enabled = False
    
    chkMultiidioma.Enabled = False
End Sub

Public Sub AnyaModifGrupo(Optional ByVal Id As Long)
    Dim sDen As String
    Dim i As Integer
    Dim oMultiidi As CMultiidioma
    
    If Id <> 0 Then
        Set g_oGrupoSeleccionado = g_oFormSeleccionado.Grupos.Item(CStr(Id))
    End If
    
    If g_oFormSeleccionado.Multiidioma = True Then
        sDen = NullToStr(g_oGrupoSeleccionado.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den) & "/"
        For Each oMultiidi In g_oGrupoSeleccionado.Denominaciones
            If oMultiidi.Cod <> gParametrosInstalacion.gIdioma Then
                If Not IsNull(oMultiidi.Den) And oMultiidi.Den <> "" Then
                    sDen = sDen & oMultiidi.Den & "/"
                End If
            End If
        Next
        sDen = Mid(sDen, 1, Len(sDen) - 1)
    Else
        sDen = NullToStr(g_oGrupoSeleccionado.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
    End If
        
    If Id <> 0 Then
        'Se ha a�adido un grupo nuevo.Lo a�adimos al tab:
        i = ssTabGrupos.Tabs.Count + 1
        ssTabGrupos.Tabs.Add i, "A" & g_oGrupoSeleccionado.Id, sDen
        ssTabGrupos.Tabs(i).Tag = CStr(g_oGrupoSeleccionado.Id)
        ssTabGrupos.Tabs(i).Selected = True
        
        cmdModifGr.Enabled = True
        cmdMoverGrupo.Enabled = True
        If g_oFormSeleccionado.Grupos.Count > 1 Then
            cmdEliminarGr.Enabled = True
        Else
            cmdEliminarGr.Enabled = False
        End If
    
    Else
        'Ha modificado el c�digo del grupo seleccionado.Modificamos el tab:
        ssTabGrupos.selectedItem.caption = sDen
    End If
    
End Sub
''' <summary>
''' Carga el grupo seleccionado del formulario en la clase y en el tab
''' </summary>
''' <remarks>Llamada desde=frmFormularios-->; Tiempo m�ximo=0,5seg.</remarks>
Private Sub GrupoSeleccionado()
    If ssTabGrupos.selectedItem.Tag <> "" Then Set g_oGrupoSeleccionado = g_oFormSeleccionado.Grupos.Item(CStr(ssTabGrupos.selectedItem.Tag))
    
    If g_oGrupoSeleccionado Is Nothing Then Exit Sub
    Set g_oGrupoSeleccionado.Campos = Nothing
    g_oGrupoSeleccionado.CargarTodosLosCampos
    
    sdbgCampos.RemoveAll
    m_bConEnviosCorrectos = False
    m_bConPiezasDefectuosas = False
    m_bConDesgloseActividad = False
    m_bConDesgloseDePedido = False
    m_bConDesgloseFactura = False
    m_bArt = False
    AnyadirCampos g_oGrupoSeleccionado.Campos, True
    sdbgCampos.MoveFirst
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    m_bModif = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIOModificar)) Is Nothing)
    m_bRestrDest = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIORestDest)) Is Nothing)
    m_bRestrMat = (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIORestMat)) Is Nothing))
    m_bRestrProve = (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIORestProve)) Is Nothing))
    m_bRPerfUON = (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestPerfUON)) Is Nothing))
    
    'Si no es el administrador comprueba los datos del usuario
    Dim oUsuario As CUsuario
    Set oUsuario = oFSGSRaiz.Generar_CUsuario
    oUsuario.Cod = basOptimizacion.gvarCodUsuario
    oUsuario.ExpandirUsuario
    If oUsuario.FSWSPresUO = True Then m_bRestrPresUO = True
    Set oUsuario = Nothing
    
    If m_bModif = False Then
        cmdAnyaForm.Visible = False
        cmdElimForm.Visible = False
        cmdModifForm.Visible = False
        cmdAnyaCalculo.Left = cmdAnyaForm.Left
        cmdDeshacer.Visible = False
        cmdAnyaGr.Visible = False
        cmdEliminarGr.Visible = False
        cmdModifGr.Visible = False
        cmdMoverGrupo.Visible = False
        cmdRestaurar.Left = cmdAnyaGr.Left
        cmdListado.Left = cmdModifGr.Left
        picItems.Visible = False
        sdbgCampos.Top = picItems.Top
        picMoneda.Enabled = False
        picMultiidioma.Enabled = False
    End If
End Sub
''' A�ade los campos de la coleccion a la grid
''' <remarks>Llamada desde=AnyadirCampoExterno,AnyadirCampoGS,GrupoSeleccionado; Tiempo m�ximo=0,3seg.</remarks>
Public Sub AnyadirCampos(ByVal oCampos As CFormItems, Optional ByVal bCargarCol As Boolean = False)
    Dim oCampo As CFormItem
    Dim oCampo2 As CFormItem
    Dim sCadena As String
    Dim oIdioma As CIdioma
    Dim sValor As String
    Dim sCodValor As String
    Dim bBajaLog As Boolean
    Dim oProveedores As CProveedores
    Dim oProveERPs As CProveERPs
    Dim oDestinos As CDestinos
    Dim oPagos As CPagos
    Dim oPais As CPais
    Dim oPaises As CPaises
    Dim sPaisAnterior As String
    Dim oArticulos As CArticulos
    Dim oGmn As Variant
    Dim sMatAnterior As String
    Dim arrMat As Variant
    Dim oUnidades As CUnidades
    Dim oMonedas As CMonedas
    Dim oPersona As CPersona
    Dim bGenerico As Boolean
    Dim iNumCampo As Integer
    Dim vAProceso As Long
    Dim strPrimerCampoNoPK As String
    Dim intTabla As Integer
    Dim strNombreTabla As String, strPK As String
            
    iNumCampo = 0
    m_bConCampos = False

    If oCampos Is Nothing Then Exit Sub
    For Each oCampo In oCampos
        If Not bCargarCol Then
            'Lo a�ade a la colecci�n:
            Set oCampo2 = g_oGrupoSeleccionado.Campos.Add(oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo, , , , , , , , , , , , , oCampo.MostrarPOPUP, , , , , oCampo.MaxLength, oCampo.CodAtrib, oCampo.DenAtrib, oCampo.AProceso, oCampo.APedido, oCampo.TablaExterna, oCampo.PRES5, , , , oCampo.RecordarValores, oCampo.IDListaCampoPadre, , , oCampo.Servicio, , , , oCampo.Formato)
            oCampo2.NoFecAntSis = oCampo.NoFecAntSis
        End If
                
        bBajaLog = False
        
        If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
            '----------------------------------------------------------------------------------------------------
            sCodValor = Chr(m_lSeparador) & ""
            intTabla = CInt(NullToStr(oCampo.TablaExterna))
            strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
            strPrimerCampoNoPK = m_oTablaExterna.SacarPrimerCampoDistinto(intTabla)
            strPK = m_oTablaExterna.SacarPK(intTabla)
            Select Case oCampo.Tipo
                Case TipoFecha
                    If IsNull(oCampo.valorFec) Then
                        sValor = Chr(m_lSeparador) & oCampo.valorFec
                    Else
                        sValor = Chr(m_lSeparador) & Format(oCampo.valorFec, "Short date")
                        sValor = sValor & " " & m_oTablaExterna.ValorCampo(oCampo.valorFec, strPK, strPrimerCampoNoPK, strNombreTabla, oCampo.Tipo)
                    End If
                    
                Case TipoNumerico
                    If IsNull(oCampo.valorNum) Then
                        sValor = Chr(m_lSeparador) & oCampo.valorNum
                    Else
                        sValor = Chr(m_lSeparador) & CStr(oCampo.valorNum)
                        sValor = sValor & " " & m_oTablaExterna.ValorCampo(oCampo.valorNum, strPK, strPrimerCampoNoPK, strNombreTabla, oCampo.Tipo)
                    End If
                    
                Case TipoTextoMedio
                    If IsNull(oCampo.valorText) Then
                        sValor = Chr(m_lSeparador) & oCampo.valorText
                    Else
                        sValor = Chr(m_lSeparador) & oCampo.valorText
                        sValor = sValor & " " & m_oTablaExterna.ValorCampo(oCampo.valorText, strPK, strPrimerCampoNoPK, strNombreTabla, oCampo.Tipo)
                    End If
            End Select
            '----------------------------------------------------------------------------------------------------
        Else
            If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                'Campos predefinidos de GS:
                sCodValor = Chr(m_lSeparador) & ""
                sValor = Chr(m_lSeparador) & ""
                Select Case oCampo.CampoGS
                    Case TipoCampoSC.ArchivoEspecific
                        oCampo.CargarAdjuntos
                        sValor = Chr(m_lSeparador) & LiteralAdjuntos(oCampo)
                    Case TipoCampoSC.Cantidad, TipoCampoSC.importe, TipoCampoSC.PrecioUnitario
                        If IsNull(oCampo.valorNum) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorNum
                        Else
                            sValor = Chr(m_lSeparador) & Format(oCampo.valorNum, "Standard")
                        End If
                    Case TipoCampoSC.DescrBreve, TipoCampoSC.DescrDetallada
                        sValor = Chr(m_lSeparador) & EliminarTab(NullToStr(oCampo.valorText))
                    Case TipoCampoGS.Desglose
                        sValor = Chr(m_lSeparador) & m_sIdiDesglose
                    Case TipoCampoGS.Dest
                        If Not IsNull(oCampo.valorText) Then
                            Set oDestinos = oFSGSRaiz.Generar_CDestinos
                            oDestinos.CargarTodosLosDestinosDesde 1, oCampo.valorText, , True
                            If Not oDestinos.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oDestinos.Item(1).Cod & " - " & oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oDestinos = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.FormaPago
                        If Not IsNull(oCampo.valorText) Then
                            Set oPagos = oFSGSRaiz.Generar_CPagos
                            oPagos.CargarTodosLosPagos oCampo.valorText, , True
                            If Not oPagos.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oPagos.Item(1).Cod & " - " & oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oPagos = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.Proveedor
                        If Not IsNull(oCampo.valorText) And oCampo.valorText <> "" Then
                                Set oProveedores = oFSGSRaiz.Generar_CProveedores
                                oProveedores.CargarTodosLosProveedoresDesde3 1, oCampo.valorText, , True
                                If Not oProveedores.Item(1) Is Nothing Then
                                    sValor = Chr(m_lSeparador) & oProveedores.Item(1).Cod & " - " & oProveedores.Item(1).Den
                                    sCodValor = Chr(m_lSeparador) & oCampo.valorText
                                Else
                                    sValor = Chr(m_lSeparador) & ""
                                    sCodValor = Chr(m_lSeparador) & ""
                                End If
                                Set oProveedores = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.ProveedorERP
                        Set oProveERPs = oFSGSRaiz.Generar_CProveERPs
                        If Not IsNull(oCampo.valorText) Then
                            oProveERPs.CargarProveedoresERP , , oCampo.valorText, , , True
                            If Not oProveERPs.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oProveERPs.Item(1).Cod & " - " & oProveERPs.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        
                        
                        Set oProveERPs = Nothing
                    Case TipoCampoSC.FecNecesidad, TipoCampoSC.FinSuministro, TipoCampoSC.IniSuministro
                        oCampo.CargarFECHAVALORDEFECTO
                        If oCampo.FECHAVALORDEFECTO = True Then
                            sValor = Chr(m_lSeparador) & oCampo.valorFec
                        Else
                            If IsNull(oCampo.valorNum) Or (oCampo.valorNum = "") Then
                                sValor = Chr(m_lSeparador) & ""
                            Else
                                sValor = Chr(m_lSeparador) & m_sIdiTipoFecha(oCampo.valorNum)
                            End If
                        End If
                    Case TipoCampoGS.Unidad, TipoCampoGS.UnidadPedido
                        If Not IsNull(oCampo.valorText) Then
                            Set oUnidades = oFSGSRaiz.Generar_CUnidades
                            oUnidades.CargarTodasLasUnidades oCampo.valorText, , True
                            If Not oUnidades.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oUnidades.Item(1).Cod & " - " & oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oUnidades = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.Pais
                        If Not IsNull(oCampo.valorText) Then
                            Set oPaises = oFSGSRaiz.Generar_CPaises
                            oPaises.CargarTodosLosPaises oCampo.valorText, , True
                            If Not oPaises.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oPaises.Item(1).Cod & " - " & oPaises.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oPaises = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        sPaisAnterior = NullToStr(oCampo.valorText)
                    Case TipoCampoGS.Provincia
                        If sPaisAnterior <> "" Then
                            If Not IsNull(oCampo.valorText) Then
                                Set oPais = oFSGSRaiz.Generar_CPais
                                oPais.Cod = sPaisAnterior
                                oPais.CargarTodasLasProvincias oCampo.valorText, , True
                                If Not oPais.Provincias.Item(1) Is Nothing Then
                                    sValor = Chr(m_lSeparador) & oPais.Provincias.Item(1).Cod & " - " & oPais.Provincias.Item(1).Den
                                    sCodValor = Chr(m_lSeparador) & oCampo.valorText
                                Else
                                    sValor = Chr(m_lSeparador) & ""
                                    sCodValor = Chr(m_lSeparador) & ""
                                End If
                                Set oPais = Nothing
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.Moneda
                        If Not IsNull(oCampo.valorText) And Not IsEmpty(oCampo.valorText) Then
                            Set oMonedas = oFSGSRaiz.Generar_CMonedas
                            oMonedas.CargarTodasLasMonedas oCampo.valorText, , True
                            If Not oMonedas.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oMonedas.Item(1).Cod & " - " & oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                            Set oMonedas = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.material
                        If NullToStr(oCampo.valorText) <> "" Then
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            
                            arrMat = DevolverMaterial(oCampo.valorText)
                            Set oGmn = createGMN(arrMat(1), arrMat(2), arrMat(3), arrMat(4))
                            oGmn.cargarDenominacion
                            sValor = Chr(m_lSeparador) & oGmn.titulo
                            If Not CampoSeleccionado Is Nothing Then
                                oCampo.GMN1Cod = oGmn.GMN1Cod
                                oCampo.GMN2Cod = oGmn.GMN2Cod
                                oCampo.GMN3Cod = oGmn.GMN3Cod
                                oCampo.GMN4Cod = oGmn.GMN4Cod
                            End If
                            Set oGmn = Nothing
                            
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        sMatAnterior = NullToStr(oCampo.valorText)
                    Case TipoCampoGS.CodArticulo
                        If sMatAnterior <> "" Then
                            If Not IsNull(oCampo.valorText) Then
                                arrMat = DevolverMaterial(sMatAnterior)
                
                                Set oGmn = oFSGSRaiz.Generar_CGrupoMatNivel4
                                oGmn.GMN1Cod = arrMat(1)
                                oGmn.GMN2Cod = arrMat(2)
                                oGmn.GMN3Cod = arrMat(3)
                                oGmn.Cod = arrMat(4)
                                oGmn.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, oCampo.valorText, , True
                                Set oArticulos = oGmn.ARTICULOS
                                Set oGmn = Nothing
                            
                                If Not oArticulos.Item(1) Is Nothing Then
                                    sValor = Chr(m_lSeparador) & oArticulos.Item(1).Cod & " - " & oArticulos.Item(1).Den
                                    sCodValor = Chr(m_lSeparador) & oCampo.valorText
                                Else
                                    sValor = Chr(m_lSeparador) & oCampo.valorText
                                    sCodValor = Chr(m_lSeparador) & oCampo.valorText
                                End If
                                Set oArticulos = Nothing
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        m_bArt = True
                    Case TipoCampoGS.NuevoCodArticulo
                        If sMatAnterior <> "" Then
                            If Not IsNull(oCampo.valorText) Then
                                arrMat = DevolverMaterial(sMatAnterior)
                                sValor = Chr(m_lSeparador) & oCampo.valorText
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                                bGenerico = esGenerico(oCampo.valorText)
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                         m_bArt = True
                    Case TipoCampoGS.DenArticulo
                        If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorText
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.PRES1, TipoCampoGS.PRES2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                        If Not IsNull(oCampo.valorText) Then
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Select Case oCampo.CampoGS
                                Case TipoCampoGS.PRES1
                                    sValor = Chr(m_lSeparador) & GenerarDenominacionPresupuesto(oCampo.valorText, 1, bBajaLog)
                                
                                Case TipoCampoGS.PRES2
                                    sValor = Chr(m_lSeparador) & GenerarDenominacionPresupuesto(oCampo.valorText, 2, bBajaLog)
                                
                                Case TipoCampoGS.Pres3
                                    sValor = Chr(m_lSeparador) & GenerarDenominacionPresupuesto(oCampo.valorText, 3, bBajaLog)
                                
                                Case TipoCampoGS.Pres4
                                    sValor = Chr(m_lSeparador) & GenerarDenominacionPresupuesto(oCampo.valorText, 4, bBajaLog)
                            End Select
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.CampoPersona
                        If Not IsNull(oCampo.valorText) Then
                            Set oPersona = oFSGSRaiz.Generar_CPersona
                            oPersona.Cod = oCampo.valorText
                            oPersona.CargarTodosLosDatos
                            sValor = Chr(m_lSeparador) & oPersona.nombre & " " & oPersona.Apellidos & " (" & oPersona.mail & ")"
                            sCodValor = Chr(m_lSeparador) & oPersona.Cod
                            Set oPersona = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.CodComprador
                        If Not IsNull(oCampo.valorText) Then
                            Set oPersona = oFSGSRaiz.Generar_CPersona
                            oPersona.Cod = oCampo.valorText
                            oPersona.CargarTodosLosDatos
                            sValor = Chr(m_lSeparador) & oPersona.nombre & " " & oPersona.Apellidos
                            sCodValor = Chr(m_lSeparador) & oPersona.Cod
                            Set oPersona = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.NumSolicitERP
                        If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorText
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.UnidadOrganizativa
                       If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorText
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            bBajaLog = PonerEstiloUON(oCampo.valorText)
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                            bBajaLog = False
                        End If
                    Case TipoCampoGS.Departamento
                        If Not IsNull(oCampo.valorText) Then
                            Dim vbm As Variant
                            vbm = sdbgCampos.GetBookmark(iNumCampo - 1)
    
                            Dim oDepartamentos As CDepartamentos
                            Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
                            
                            oDepartamentos.CargarTodosLosDepartamentos oCampo.valorText, , True
                                                    
                            If Not oDepartamentos.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oDepartamentos.Item(1).Cod & " - " & oDepartamentos.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & oCampo.valorText
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            End If
                            
                            If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.UnidadOrganizativa) Then
                                If sdbgCampos.Columns("BAJALOG").CellValue(vbm) Then
                                    bBajaLog = True
                                Else
                                    bBajaLog = False
                                End If
                            Else
                                'NO es un dep asociado pero puede estar de baja
                                bBajaLog = PonerEstiloDep(oCampo.valorText)
                            End If
                            Set oDepartamentos = Nothing
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.OrganizacionCompras
                        If Not IsNull(oCampo.valorText) Then
                            Dim oOrganizacionesCompras As COrganizacionesCompras
                            Set oOrganizacionesCompras = oFSGSRaiz.Generar_COrganizacionesCompras
                            
                            oOrganizacionesCompras.CargarOrganizacionesCompra oCampo.valorText
                            If Not oOrganizacionesCompras.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oOrganizacionesCompras.Item(1).Cod & " - " & oOrganizacionesCompras.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                         Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        Set oOrganizacionesCompras = Nothing
                    Case TipoCampoGS.Centro
                        If Not IsNull(oCampo.valorText) Then
                            Dim oCentros As CCentros
                            Set oCentros = oFSGSRaiz.Generar_CCentros
                            
                            oCentros.CargarTodosLosCentros oCampo.valorText
                            If Not oCentros.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oCentros.Item(1).Cod & " - " & oCentros.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorText
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                         Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        Set oCentros = Nothing
                    Case TipoCampoGS.CentroCoste
                        If NullToStr(oCampo.valorText) <> "" Then
                            Dim aCCoste() As String
                            aCCoste = Split(oCampo.valorText, "#")
                                                     
                            Dim oCCoste As CCentroCoste
                            Set oCCoste = oFSGSRaiz.Generar_CCentroCoste
                            Select Case UBound(aCCoste)
                                Case 0
                                    oCCoste.Cod = aCCoste(0)
                                    oCCoste.Den = oCCoste.DevolverDenominacion(aCCoste(0), "", "", "")
                                    oCCoste.UON1 = aCCoste(0)
                                Case 1
                                    oCCoste.Cod = aCCoste(1)
                                    oCCoste.Den = oCCoste.DevolverDenominacion(aCCoste(0), aCCoste(1), "", "")
                                    oCCoste.CodConcat = aCCoste(0)
                                    oCCoste.UON1 = aCCoste(0)
                                    oCCoste.UON2 = aCCoste(1)
                                Case 2
                                    oCCoste.Cod = aCCoste(2)
                                    oCCoste.Den = oCCoste.DevolverDenominacion(aCCoste(0), aCCoste(1), aCCoste(2), "")
                                    oCCoste.CodConcat = aCCoste(0) & "-" & aCCoste(1)
                                    oCCoste.UON1 = aCCoste(0)
                                    oCCoste.UON2 = aCCoste(1)
                                    oCCoste.UON3 = aCCoste(2)
                                Case 3
                                    oCCoste.Cod = aCCoste(3)
                                    oCCoste.Den = oCCoste.DevolverDenominacion(aCCoste(0), aCCoste(1), aCCoste(2), aCCoste(3))
                                    oCCoste.CodConcat = aCCoste(0) & "-" & aCCoste(1) & "-" & aCCoste(2)
                                    oCCoste.UON1 = aCCoste(0)
                                    oCCoste.UON2 = aCCoste(1)
                                    oCCoste.UON3 = aCCoste(2)
                                    oCCoste.UON4 = aCCoste(3)
                            End Select
                                                   
                            oCCoste.ValorCentroCoste sValor, sCodValor
                            Set oCCoste = Nothing
                            sValor = Chr(m_lSeparador) & sValor
                            sCodValor = Chr(m_lSeparador) & sCodValor
                        End If
                    Case TipoCampoGS.Almacen
                        If Not IsNull(oCampo.valorNum) Then
                            Dim oAlmacenes As CAlmacenes
                            Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
                            
                            oAlmacenes.CargarTodosLosAlmacenes , , , , CDbl(oCampo.valorNum)
                            If Not oAlmacenes.Item(1) Is Nothing Then
                                sValor = Chr(m_lSeparador) & oAlmacenes.Item(1).Cod & " - " & oAlmacenes.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorNum
                            Else
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            End If
                         Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        Set oAlmacenes = Nothing
                    Case TipoCampoGS.Empresa
                        If Not IsNull(oCampo.valorNum) Then
                            Dim oEmpresas As CEmpresas
                            Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
                            
                            oEmpresas.CargarTodasLasEmpresasDesde lID:=oCampo.valorNum
                            If oEmpresas.Count = 0 Then
                                sValor = Chr(m_lSeparador) & ""
                                sCodValor = Chr(m_lSeparador) & ""
                            Else
                                sValor = Chr(m_lSeparador) & oEmpresas.Item(1).NIF & " - " & oEmpresas.Item(1).Den
                                sCodValor = Chr(m_lSeparador) & oCampo.valorNum
                            End If
                         Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                        Set oEmpresas = Nothing
                    Case TipoCampoCertificado.Alcance, TipoCampoCertificado.NombreCertif, TipoCampoCertificado.Certificado, _
                        TipoCampoCertificado.EntidadCertific, TipoCampoCertificado.NumCertificado
                        If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorText
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoCertificado.FecObtencion
                        If Not IsNull(oCampo.valorFec) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorFec
                            sCodValor = Chr(m_lSeparador) & oCampo.valorFec
                        Else
                            sValor = Chr(m_lSeparador) & ""
                            sCodValor = Chr(m_lSeparador) & ""
                        End If
                    Case TipoCampoGS.TipoPedido
                        sValor = Chr(m_lSeparador) & ""
                        sCodValor = Chr(m_lSeparador) & ""
                            
                        If Not IsNull(oCampo.valorText) Then
                            Dim oTipoPedido As CTipoPedido
                            For Each oTipoPedido In m_oTiposPedido
                                If oCampo.valorText = oTipoPedido.Cod Then
                                    sValor = Chr(m_lSeparador) & oTipoPedido.Cod & " - " & oTipoPedido.Den
                                    sCodValor = Chr(m_lSeparador) & oCampo.valorText
                                    Exit For
                                End If
                            Next
                        End If
                    Case Else
                        If Not IsNull(oCampo.valorText) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorText
                            sCodValor = Chr(m_lSeparador) & oCampo.valorText
                        Else
                            If Not IsNull(oCampo.valorNum) Then
                                sValor = Chr(m_lSeparador) & Format(oCampo.valorNum, "Standard")
                                sCodValor = Chr(m_lSeparador) & oCampo.valorNum
                            Else
                                If Not IsNull(oCampo.valorFec) Then
                                    sValor = Chr(m_lSeparador) & oCampo.valorFec
                                    sCodValor = Chr(m_lSeparador) & oCampo.valorFec
                                Else
                                    If Not IsNull(oCampo.valorBool) Then
                                        Select Case oCampo.valorBool
                                            Case 0, False
                                                sValor = Chr(m_lSeparador) & m_sIdiFalse
                                                sCodValor = Chr(m_lSeparador) & m_sIdiFalse
                                            Case 1, True
                                                sValor = Chr(m_lSeparador) & m_sIdiTrue
                                                sCodValor = Chr(m_lSeparador) & m_sIdiTrue
                                            Case Else
                                                sValor = Chr(m_lSeparador) & ""
                                                sCodValor = Chr(m_lSeparador) & ""
                                        End Select
                                    Else
                                        sValor = Chr(m_lSeparador) & ""
                                        sCodValor = Chr(m_lSeparador) & ""
                                    End If
                                End If
                            End If
                        End If
                End Select
            ElseIf oCampo.TipoPredef = TipoCampoPredefinido.Calculado Then
                sCodValor = Chr(m_lSeparador) & ""
                If IsNull(oCampo.valorNum) Then
                    sValor = Chr(m_lSeparador) & oCampo.valorNum
                Else
                    sValor = Chr(m_lSeparador) & Format(oCampo.valorNum, "Standard")
                End If
            Else
                'Campos normales o de atributos
                sCodValor = Chr(m_lSeparador) & ""
                
                Select Case oCampo.Tipo
                    Case TiposDeAtributos.TipoArchivo
                        oCampo.CargarAdjuntos
                        sValor = Chr(m_lSeparador) & LiteralAdjuntos(oCampo)
                        
                    Case TiposDeAtributos.TipoBoolean
                        Select Case oCampo.valorBool
                            Case 0, False
                                sValor = Chr(m_lSeparador) & m_sIdiFalse
                            Case 1, True
                                sValor = Chr(m_lSeparador) & m_sIdiTrue
                            Case Else
                                If UCase(oCampo.valorBool) = UCase(m_sIdiTrue) Then 'Valor por defecto es "S�"
                                    sValor = Chr(m_lSeparador) & m_sIdiTrue
                                ElseIf UCase(oCampo.valorBool) = UCase(m_sIdiFalse) Then
                                    sValor = Chr(m_lSeparador) & m_sIdiFalse
                                Else
                                    sValor = Chr(m_lSeparador) & ""
                                End If
                        End Select
            
                    Case TiposDeAtributos.TipoFecha
                        If IsNull(oCampo.valorFec) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorFec
                        Else
                            sValor = Chr(m_lSeparador) & Format(oCampo.valorFec, "Short date")
                        End If
                        
                    Case TiposDeAtributos.TipoNumerico
                        If IsNull(oCampo.valorNum) Then
                            sValor = Chr(m_lSeparador) & oCampo.valorNum
                        Else
                            sValor = Chr(m_lSeparador) & Format(oCampo.valorNum, "Standard")
                        End If
                        
                    Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                        sValor = Chr(m_lSeparador) & EliminarTab(NullToStr(oCampo.valorText))
                        
                        If oCampo.TipoIntroduccion = Introselec Then sCodValor = Chr(m_lSeparador) & oCampo.valorNum
                        
                    Case TiposDeAtributos.TipoDesglose
                        sValor = Chr(m_lSeparador) & m_sIdiDesglose
                        
                    Case TiposDeAtributos.TipoEditor, TiposDeAtributos.TipoEnlace
                        sValor = Chr(m_lSeparador) & NullToStr(oCampo.valorText)
                End Select
            End If
        End If
        
        If CamposSistemaAProceso(oCampo.CampoGS) Or (oCampo.CampoGS = TipoCampoSC.importe And oCampo.Grupo.Orden = 1) Then
            vAProceso = 1
        Else
            vAProceso = oCampo.AProceso
        End If
        
        sCadena = oCampo.Id & Chr(m_lSeparador) & oCampo.CodAtrib & sValor & Chr(m_lSeparador) & BooleanToSQLBinary(vAProceso) & Chr(m_lSeparador) & BooleanToSQLBinary(oCampo.APedido) & _
            Chr(m_lSeparador) & " " & Chr(m_lSeparador) & oCampo.TipoPredef & Chr(m_lSeparador) & oCampo.Tipo & Chr(m_lSeparador) & oCampo.TipoIntroduccion & Chr(m_lSeparador) & oCampo.CampoGS & sCodValor & _
            Chr(m_lSeparador) & BooleanToSQLBinary(bBajaLog) & Chr(m_lSeparador) & BooleanToSQLBinary(oCampo.titulo) & Chr(m_lSeparador) & BooleanToSQLBinary(oCampo.Peso) & Chr(m_lSeparador) & bGenerico & _
            Chr(m_lSeparador) & oCampo.idAtrib & Chr(m_lSeparador) & BooleanToSQLBinary(oCampo.DenProceso)
        
        'Denominaci�n del campo en los diferentes idiomas:
        sCadena = sCadena & Chr(m_lSeparador) & NullToStr(oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
                
        If g_oFormSeleccionado.Multiidioma Then
            For Each oIdioma In m_oIdiomas
                If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                    sCadena = sCadena & Chr(m_lSeparador) & NullToStr(oCampo.Denominaciones.Item(CStr(oIdioma.Cod)).Den)
                End If
            Next
        Else
            sCadena = sCadena & Chr(m_lSeparador) & NullToStr(oCampo.Denominaciones.Item(oUsuarioSummit.idioma).Den)
        End If
       
        If oCampo.TipoPredef = TipoCampoPredefinido.externo Then sCadena = sCadena & Chr(m_lSeparador) & intTabla
        
        If oCampo.CampoGS = TipoCampoNoConformidad.EnviosCorrectos Then
            m_bConEnviosCorrectos = True
        ElseIf oCampo.CampoGS = TipoCampoNoConformidad.PiezasDefectuosas Then
            m_bConPiezasDefectuosas = True
        ElseIf oCampo.CampoGS = TipoCampoGS.DesgloseActividad Then
            m_bConDesgloseActividad = True
        ElseIf oCampo.CampoGS = TipoCampoGS.DesgloseFactura Then
            m_bConDesgloseFactura = True
            cmdAnyaGr.Enabled = False
            cmdMoverGrupo.Enabled = False
        ElseIf oCampo.CampoGS = TipoCampoGS.DesgloseDePedido Then
            m_bConDesgloseDePedido = True
            cmdAnyaGr.Enabled = False
            cmdMoverGrupo.Enabled = False
        End If
        
        bGenerico = False
       
        'a�ade la cadena a la grid:
        sdbgCampos.AddItem sCadena
        iNumCampo = iNumCampo + 1
    Next
    
    If iNumCampo > 0 And Not m_bConDesgloseFactura And Not m_bConDesgloseDePedido Then
        m_bConCampos = True
    End If
End Sub

''' <summary>
''' A�ade un campo externo al formulario
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Public Sub AnyadirCampoExterno(ByVal pIdTabla As Integer)
Dim oCampos As CFormItems
Dim oCampo As CFormItem
Dim udtTeserror As TipoErrorSummit
Dim strTipoDeDatoPK As TiposDeAtributos
Dim strPrimerCampoNoPK As String
Dim strNombreTabla As String, strPK As String, strDenoTabla As String

    If m_bConDesgloseFactura Or m_bConDesgloseDePedido Then
        oMensajes.ImposibleAnyadirCampoGrupo (2)
        Exit Sub
    End If

    '**********************************************************************
    m_blnExisteTablaExternaParaArticulo = IIf(m_oTablaExterna.SacarTablaTieneART(pIdTabla), True, False)
    strNombreTabla = m_oTablaExterna.SacarNombreTabla(pIdTabla)
    strDenoTabla = m_oTablaExterna.SacarDenominacionTabla(pIdTabla)
    strPrimerCampoNoPK = m_oTablaExterna.SacarPrimerCampoDistinto(pIdTabla)
    strPK = m_oTablaExterna.SacarPK(pIdTabla)
    strTipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)  'aqui se podria poner TipoTextoMedio
    '************************************************************************************
    
    g_Accion = ACCFormItemAnyadir
    Set oCampos = oFSGSRaiz.Generar_CFormCampos
    Set oCampo = GenerarEstructuraCampoExterno(pIdTabla, strTipoDeDatoPK, False)
    oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , , , oCampo.Orden, oCampo.idAtrib, , oCampo.FecAct, , , oCampo.EsSubCampo, , , , , , , , , , , , , , , , , , , , , , , oCampo.TablaExterna
    
    'A�ade el campo a BD:
    udtTeserror = oCampos.AnyadirCamposExternosoAtributos(False, True)
    If udtTeserror.NumError = TESnoerror Then
        AnyadirCampos oCampos
        RegistrarAccion ACCFormItemAnyadir, "Id:" & oCampo.Id
    Else
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
        Exit Sub
    End If

    Set oCampos = Nothing
    Set oCampo = Nothing

    g_Accion = ACCFormularioCons
End Sub
Public Sub CargarProveedorConBusqueda()
    Dim oProves As CProveedores
    
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing

    sdbgCampos.Columns("VALOR").Value = oProves.Item(1).Cod & " - " & oProves.Item(1).Den
    sdbgCampos.Columns("COD_VALOR").Value = oProves.Item(1).Cod
    sdbgCampos.Update
    
    Set oProves = Nothing
End Sub
Private Function GenerarDenominacionPresupuesto(ByVal sValor As String, ByVal iTipo As Integer, Optional ByRef bBajaLog As Boolean) As String

Dim arrPresupuestos() As String
Dim Ador As Ador.Recordset

Dim iTipoGS As TipoCampoGS

Select Case iTipo
    Case 1
        iTipoGS = PRES1
        m_dblPres1Asig = 0
    Case 2
        iTipoGS = PRES2
        m_dblPres2Asig = 0
    Case 3
        iTipoGS = Pres3
        m_dblPres3Asig = 0
    Case 4
        iTipoGS = Pres4
        m_dblPres4Asig = 0
End Select

arrPresupuestos = Split(sValor, "#")

Dim oPresup As Variant
Dim arrPresup As Variant
Dim iNivel As Integer
Dim lIdPresup As Long
Dim iContadorPres As Integer
iContadorPres = 0

Dim sTexto As String
sTexto = ""
Dim dPorcent As Double
For Each oPresup In arrPresupuestos
    iContadorPres = iContadorPres + 1
    arrPresup = Split(oPresup, "_")

    iNivel = arrPresup(0)
    dPorcent = Numero(arrPresup(2), ".")
    
    lIdPresup = arrPresup(1)
    Select Case iTipoGS
        Case TipoCampoGS.PRES1
        Dim oPresupuestos1 As CPresProyectosNivel1
            Set oPresupuestos1 = oFSGSRaiz.Generar_CPresProyectosNivel1
            Select Case iNivel
                Case 1
                    Set Ador = oPresupuestos1.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
                Case 2
                    Set Ador = oPresupuestos1.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
                Case 3
                    Set Ador = oPresupuestos1.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
                Case 4
                    Set Ador = oPresupuestos1.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
            End Select
            Set oPresupuestos1 = Nothing
        Case TipoCampoGS.PRES2
            Dim oPresupuestos2 As CPresContablesNivel1
            Set oPresupuestos2 = oFSGSRaiz.Generar_CPresContablesNivel1
            Select Case iNivel
                Case 1
                    Set Ador = oPresupuestos2.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
                Case 2
                    Set Ador = oPresupuestos2.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
                Case 3
                    Set Ador = oPresupuestos2.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
                Case 4
                    Set Ador = oPresupuestos2.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
            End Select
            Set oPresupuestos2 = Nothing
        Case TipoCampoGS.Pres3
            Dim oPresupuestos3  As CPresConceptos3Nivel1
            Set oPresupuestos3 = oFSGSRaiz.Generar_CPresConceptos3Nivel1
            Select Case iNivel
                Case 1
                    Set Ador = oPresupuestos3.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
                Case 2
                    Set Ador = oPresupuestos3.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
                Case 3
                    Set Ador = oPresupuestos3.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
                Case 4
                    Set Ador = oPresupuestos3.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
            End Select
            Set oPresupuestos3 = Nothing
        Case TipoCampoGS.Pres4
            Dim oPresupuestos4 As CPresConceptos4Nivel1
            Set oPresupuestos4 = oFSGSRaiz.Generar_CPresConceptos4Nivel1
            Select Case iNivel
                Case 1
                    Set Ador = oPresupuestos4.DevolverPresupuestosDesde(lIdPresup, Null, Null, Null)
                Case 2
                    Set Ador = oPresupuestos4.DevolverPresupuestosDesde(Null, lIdPresup, Null, Null)
                Case 3
                    Set Ador = oPresupuestos4.DevolverPresupuestosDesde(Null, Null, lIdPresup, Null)
                Case 4
                    Set Ador = oPresupuestos4.DevolverPresupuestosDesde(Null, Null, Null, lIdPresup)
            End Select
            Set oPresupuestos4 = Nothing
    End Select
                        
    If iTipoGS = TipoCampoGS.PRES1 Or iTipoGS = TipoCampoGS.PRES2 Then sTexto = sTexto & Ador.Fields("ANYO").Value & " - "

    If Not Ador Is Nothing Then
        If Not IsNull(Ador.Fields("PRES4").Value) Then
            sTexto = sTexto & Ador.Fields("PRES4").Value & " (" & Format(dPorcent, "0.00%") & "); "
        ElseIf Not IsNull(Ador.Fields("PRES3").Value) Then
            sTexto = sTexto & Ador.Fields("PRES3").Value & " (" & Format(dPorcent, "0.00%") & "); "
        ElseIf Not IsNull(Ador.Fields("PRES2").Value) Then
            sTexto = sTexto & Ador.Fields("PRES2").Value & " (" & Format(dPorcent, "0.00%") & "); "
        ElseIf Not IsNull(Ador.Fields("PRES1").Value) Then
            sTexto = sTexto & Ador.Fields("PRES1").Value & " (" & Format(dPorcent, "0.00%") & "); "
        End If
        
        bBajaLog = Ador.Fields("BAJALOG").Value
        
        If bBajaLog Then
            Select Case iTipoGS
                Case TipoCampoGS.PRES1
                    m_bHayPres1BajaLog = bBajaLog
                Case TipoCampoGS.PRES2
                    m_bHayPres2BajaLog = bBajaLog
                Case TipoCampoGS.Pres3
                    m_bHayPres3BajaLog = bBajaLog
                Case TipoCampoGS.Pres4
                    m_bHayPres4BajaLog = bBajaLog
            End Select
        Else
            Select Case iTipoGS
                Case TipoCampoGS.PRES1
                    m_dblPres1Asig = m_dblPres1Asig + (dPorcent)
                Case TipoCampoGS.PRES2
                    m_dblPres2Asig = m_dblPres2Asig + (dPorcent)
                Case TipoCampoGS.Pres3
                    m_dblPres3Asig = m_dblPres3Asig + (dPorcent)
                Case TipoCampoGS.Pres4
                    m_dblPres4Asig = m_dblPres4Asig + (dPorcent)
            End Select
        End If
        
        If iContadorPres <> 1 Then bBajaLog = False
        Ador.Close
    End If
Next

GenerarDenominacionPresupuesto = sTexto
End Function
Public Sub MostrarPresSeleccionado(ByVal sValor As String, ByVal iTipo As Integer, Optional ByVal bBajaLog As Boolean = False)
    '''Escribimos el nuevo presupuesto en la grid
    sdbgCampos.Columns("VALOR").Value = GenerarDenominacionPresupuesto(sValor, iTipo)
    sdbgCampos.Columns("COD_VALOR").Value = sValor
    sdbgCampos.Columns("BAJALOG").Value = bBajaLog
    
    '''Aqu� cambiamos el estilo de la celda
    '''para que el presupuesto no se muestre tachado
    If Not sdbgCampos.Columns("BAJALOG").Value Then
        sdbgCampos.Columns("VALOR").CellStyleSet ""
        sdbgCampos.Bookmark = sdbgCampos.GetBookmark(0) 'Esto es necesario para refrescar el estilo
    End If
End Sub
Public Sub MostrarCentroCosteSeleccionado(ByRef oCCoste As CCentroCoste)
    Dim sValor As String
    Dim sCodValor As String
    
    oCCoste.ValorCentroCoste sValor, sCodValor
    sdbgCampos.Columns("VALOR").Value = sValor
    sdbgCampos.Columns("COD_VALOR").Value = sCodValor
End Sub
Private Function LiteralAdjuntos(ByVal oCampo As CFormItem) As String
Dim sAdjuntos As String
Dim oAdjunto As CCampoAdjunto

    sAdjuntos = ""
    For Each oAdjunto In oCampo.Adjuntos
        sAdjuntos = sAdjuntos & oAdjunto.nombre & " (" & TamanyoAdjuntos(oAdjunto.DataSize / 1024) & m_sIdiKB & ");"
    Next
    If sAdjuntos <> "" Then sAdjuntos = Mid(sAdjuntos, 1, Len(sAdjuntos) - 1)
    LiteralAdjuntos = sAdjuntos
End Function
Public Function ModificarAdjuntos(ByVal oCampo As CFormItem)
    If m_bModif = False Then Exit Function
    sdbgCampos.Columns("VALOR").Value = LiteralAdjuntos(oCampo)
    sdbgCampos.Update
End Function
Private Sub GuardarOrdenCampos()
    Dim oCampos As CFormItems
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim vbm As Variant
    Dim oCampo As CFormItem
    
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    Set oCampos = oFSGSRaiz.Generar_CFormCampos
    
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        'Guarda en BD solo el orden de los atributos que han cambiado
        If NullToDbl0(g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Orden) <> i + 1 Then
            g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Orden = i + 1
            oCampos.Add g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Id, g_oGrupoSeleccionado, g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Denominaciones, , , , , , , , , , , g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).Orden
        End If
    Next i
    
    teserror = oCampos.GuardarOrdenCampos
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        For Each oCampo In oCampos
            g_oGrupoSeleccionado.Campos.Item(CStr(oCampo.Id)).FecAct = oCampo.FecAct
        Next
    End If
            
    Set oCampos = Nothing
End Sub
Private Sub RedimensionarColumnasMultiIdi()
Dim oIdioma As CIdioma
Dim dblWGrid As Double
    
    For Each oIdioma In m_oIdiomas
        If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
            sdbgCampos.Columns(oIdioma.Cod).Visible = g_oFormSeleccionado.Multiidioma
        Else
            sdbgCampos.Columns(oIdioma.Cod).Visible = True
            If g_oFormSeleccionado.Multiidioma = True Then
                sdbgCampos.Columns(oIdioma.Cod).caption = m_oIdiomas.Item(CStr(gParametrosInstalacion.gIdioma)).Den
            Else
                sdbgCampos.Columns(oIdioma.Cod).caption = m_sDato
            End If
        End If
    Next
    
    dblWGrid = sdbgCampos.Width - sdbgCampos.Columns("ATRIBUTO").Width - sdbgCampos.Columns("VALOR").Width - sdbgCampos.Columns("APROCESO").Width - sdbgCampos.Columns("APEDIDO").Width - sdbgCampos.Columns("AYUDA").Width - 600
    
    If g_oFormSeleccionado.Multiidioma = True Then
        For Each oIdioma In m_oIdiomas
            sdbgCampos.Columns(oIdioma.Cod).Width = dblWGrid / m_oIdiomas.Count
        Next
    Else
        sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Width = dblWGrid
    End If
    
    If g_oFormSeleccionado.Multiidioma = True Then
        sdbddDestinos.Columns(1).caption = m_sDen & " (" & m_oIdiomas.Item(gParametrosInstalacion.gIdioma).Den & ")"
        sdbddDestinos.Columns(2).Visible = True
        sdbddDestinos.Columns(3).Visible = True
        
        sdbddPagos.Columns(1).caption = m_sDen & " (" & m_oIdiomas.Item(gParametrosInstalacion.gIdioma).Den & ")"
        sdbddPagos.Columns(2).Visible = True
        sdbddPagos.Columns(3).Visible = True
        
        sdbddOtrosCombos.Columns(1).caption = m_sDen & " (" & m_oIdiomas.Item(gParametrosInstalacion.gIdioma).Den & ")"
        sdbddOtrosCombos.Columns(2).Visible = True
        sdbddOtrosCombos.Columns(3).Visible = True
    End If
End Sub
Private Sub CargarGruposEnTab()
Dim i As Integer
Dim oGrupo As CFormGrupo
Dim sDen As String
Dim oMultiidi As CMultiidioma

    g_oFormSeleccionado.CargarTodosLosGrupos False
    Set g_oGrupoSeleccionado = g_oFormSeleccionado.Grupos.Item(1)
    ssTabGrupos.Tabs.clear
    i = 1
    For Each oGrupo In g_oFormSeleccionado.Grupos
        sDen = ""
        sDen = sDen & NullToStr(oGrupo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den) & "/"
        For Each oMultiidi In oGrupo.Denominaciones
            If oMultiidi.Cod <> gParametrosInstalacion.gIdioma And Not IsNull(oMultiidi.Den) Then
                sDen = sDen & oMultiidi.Den & "/"
            End If
        Next
        sDen = Mid(sDen, 1, Len(sDen) - 1)
        
        If g_oFormSeleccionado.Multiidioma = True Then
            ssTabGrupos.Tabs.Add i, "A" & oGrupo.Id, sDen
        Else
            ssTabGrupos.Tabs.Add i, "A" & oGrupo.Id, NullToStr(oGrupo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)  'si no es multiidioma ser� la misma para todos
        End If
        ssTabGrupos.Tabs(i).Tag = CStr(oGrupo.Id)
        i = i + 1
    Next
End Sub
Public Sub ModificarOrdenGrupos()
    Dim i As Integer
    LockWindowUpdate Me.hWnd
    CargarGruposEnTab
    For i = 1 To Me.ssTabGrupos.Tabs.Count
        If ssTabGrupos.Tabs(i).Tag = CStr(g_oGrupoSeleccionado.Id) Then
            ssTabGrupos.Tabs(i).Selected = True
            Exit For
        End If
    Next i
    LockWindowUpdate 0&
End Sub
Private Sub ssTabGrupos_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If m_bModif = False Or m_bConDesgloseFactura Or m_bConDesgloseDePedido Then Exit Sub
    If Button = 2 Then PopupMenu MDI.mnuPOPUPMoverGrupo
End Sub
Public Sub CambiarOrdenGrupos()
    If sdbgCampos.DataChanged = True Then
        sdbgCampos.Update
        If m_bModError = True Then Exit Sub
    End If
    frmFormMoverGrupo.Show vbModal
End Sub
Public Sub PonerMatSeleccionado()
    Dim oGmn As Variant
    Dim sNuevoCod1 As String
    Dim sNuevoCod2 As String
    Dim sNuevoCod3 As String
    Dim sNuevoCod4 As String
    Dim sMat As String
    Dim iResp As Integer
    Dim lIdCampoMat As Long
    Dim iRow As Integer
    
    lIdCampoMat = CampoSeleccionado.Id
    
    'Si el nivel de selecci�n es obligatorio, Comprobamos que haya habido cambios de familia de material
    If CampoSeleccionado.nivelSeleccion <> 0 Then
        If (NullToStr(CampoSeleccionado.GMN1Cod) <> frmSELMAT.GMNSeleccionado.GMN1Cod Or NullToStr(CampoSeleccionado.GMN2Cod) <> frmSELMAT.GMNSeleccionado.GMN2Cod _
            Or NullToStr(CampoSeleccionado.GMN3Cod) <> frmSELMAT.GMNSeleccionado.GMN3Cod Or NullToStr(CampoSeleccionado.GMN4Cod) <> frmSELMAT.GMNSeleccionado.GMN4Cod) _
            And CampoSeleccionado.GMN1Cod <> "" And g_oFormSeleccionado.existeLineaDesglose Then
                iResp = oMensajes.MensajeYesNo(1450)
                If iResp = vbNo Then
                    Exit Sub
                Else
                    g_oFormSeleccionado.EliminarArticulosEnTodosDesgloses
                End If
        End If
    End If
    
    Set oGmn = frmSELMAT.oGMNSeleccionado
    If Not oGmn Is Nothing Then
        sNuevoCod1 = oGmn.GMN1Cod
        sNuevoCod2 = oGmn.GMN2Cod
        sNuevoCod3 = oGmn.GMN3Cod
        sNuevoCod4 = oGmn.GMN4Cod
    End If
            
    If Not oGmn Is Nothing Then
        sdbgCampos.Columns("VALOR").Value = oGmn.titulo
        sMat = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sNuevoCod1)) & sNuevoCod1
        sMat = sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(sNuevoCod2)) & sNuevoCod2
        sMat = sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(sNuevoCod3)) & sNuevoCod3
        sMat = sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(sNuevoCod4)) & sNuevoCod4
    End If
    
    sdbgCampos.Columns("COD_VALOR").Value = sMat
    Set oGmn = Nothing
            
    'Quita el valor del art�culo
    iRow = sdbgCampos.Row
    sdbgCampos.Row = sdbgCampos.Row + 1
    If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo) And sdbgCampos.Columns("COD_VALOR").Value <> "" Then
        Dim oArticulos As CArticulos
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        oArticulos.CargarTodosLosArticulos sdbgCampos.Columns("COD_VALOR").Value, , True
       
        If oArticulos.Count > 0 Then
            sdbgCampos.Columns("COD_VALOR").Value = ""
            sdbgCampos.Columns("VALOR").Value = ""
            sdbgCampos.Update
            'Quita el valor de la denominaci�n del art�culo
            sdbgCampos.Row = sdbgCampos.Row + 1
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
                sdbgCampos.Columns("COD_VALOR").Value = ""
                sdbgCampos.Columns("VALOR").Value = ""
                sdbgCampos.Update
            End If
            sdbgCampos.Row = sdbgCampos.Row - 1
        End If
    
        Set oArticulos = Nothing
    End If
    sdbgCampos.Row = iRow
    
    'Si el campo est� como campo padre de alguna lista se quita el valor de esa lista
    LimpiarValorListaHijaMaterial lIdCampoMat
    
    CampoSeleccionado.GMN1Cod = frmSELMAT.GMNSeleccionado.GMN1Cod
    CampoSeleccionado.GMN2Cod = frmSELMAT.GMNSeleccionado.GMN2Cod
    CampoSeleccionado.GMN3Cod = frmSELMAT.GMNSeleccionado.GMN3Cod
    CampoSeleccionado.GMN4Cod = frmSELMAT.GMNSeleccionado.GMN4Cod
    Dim oCampoMaterial As CFormItem
    If Me.g_oFormSeleccionado.ExisteNivelSeleccionMatObligatorio Then
        Set oCampoMaterial = Me.g_oFormSeleccionado.getCampoTipoNivelSeleccionObligatoria()
        oCampoMaterial.GMN1Cod = CampoSeleccionado.GMN1Cod
        oCampoMaterial.GMN2Cod = CampoSeleccionado.GMN2Cod
        oCampoMaterial.GMN3Cod = CampoSeleccionado.GMN3Cod
        oCampoMaterial.GMN4Cod = CampoSeleccionado.GMN4Cod
        oCampoMaterial.valorText = CampoSeleccionado.valorText
        Set oCampoMaterial = Nothing
    End If
        
    sdbgCampos.Update
End Sub
Public Function RecalcularValoresFormulas() As Boolean
Dim lErrCode As Integer
Dim lIndex As Integer
Dim iEq As USPExpression
Dim sVariables() As String
Dim dValues() As Double
Dim i As Integer
Dim Adores As Ador.Recordset
Dim bHayModif As Boolean

    Set Adores = g_oFormSeleccionado.CargarCamposCalculados(False)
    If Adores Is Nothing Then
        RecalcularValoresFormulas = False
        Exit Function
    End If

    bHayModif = False
    Set iEq = New USPExpression
    i = 1
    
    'Almacena las variables en un array:
    ReDim sVariables(Adores.RecordCount)
    ReDim dValues(Adores.RecordCount)

    While Not Adores.EOF
        sVariables(i) = Adores.Fields("ID_CALCULO").Value
        If g_oFormSeleccionado.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).Campos Is Nothing Then
            g_oFormSeleccionado.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).CargarTodosLosCampos
        End If
        If NullToDbl0(g_oFormSeleccionado.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).Campos.Item(CStr(Adores.Fields("ID").Value)).valorNum) = "" Then
            dValues(i) = 0
        Else
            dValues(i) = NullToDbl0(g_oFormSeleccionado.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).Campos.Item(CStr(Adores.Fields("ID").Value)).valorNum)
        End If
        i = i + 1
        Adores.MoveNext
    Wend

    'Ahora va recalculando todas las f�rmulas:
    Adores.MoveFirst
    i = 1
    While Not Adores.EOF
        If Adores.Fields("TIPO").Value = TipoCampoPredefinido.Calculado And Not IsNull(Adores.Fields("FORMULA").Value) Then
            If IsNull(Adores.Fields("ORIGEN_CALC_DESGLOSE").Value) Then
                lIndex = iEq.Parse(Adores.Fields("FORMULA").Value, sVariables, lErrCode)
                If lErrCode = USPEX_NO_ERROR Then
                    On Error GoTo EvaluationError
                    dValues(i) = iEq.Evaluate(dValues)
                    'almacena en la colecci�n:
                    If g_oFormSeleccionado.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).Campos.Item(CStr(Adores.Fields("ID").Value)).valorNum <> dValues(i) Then
                        g_oFormSeleccionado.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).Campos.Item(CStr(Adores.Fields("ID").Value)).Modificado = True
                        g_oFormSeleccionado.Grupos.Item(CStr(Adores.Fields("GRUPO").Value)).Campos.Item(CStr(Adores.Fields("ID").Value)).valorNum = dValues(i)
                        bHayModif = True
                    End If

                Else  'Ha habido error:
                    oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                    RecalcularValoresFormulas = False
                    Exit Function
                End If
            End If
        End If

        i = i + 1
        Adores.MoveNext
    Wend

    Adores.Close
    Set Adores = Nothing
    Set iEq = Nothing
    RecalcularValoresFormulas = bHayModif
    Exit Function

EvaluationError:
    oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(2))
    Set Adores = Nothing
    RecalcularValoresFormulas = False
End Function
Public Sub AnyadirCampoPredefinido(ByVal lTipo As Long)
    If m_bConDesgloseFactura Or m_bConDesgloseDePedido Then
        oMensajes.ImposibleAnyadirCampoGrupo (2)
        Exit Sub
    End If

    'Llama a la pantalla para a�adir campos predefinidos de un tipo de solicitud en concreto
    g_Accion = ACCFormItemAnyadir
    
    frmCamposSolic.g_lTipoCampoPredef = lTipo
    frmCamposSolic.g_sOrigen = "frmFormularios"
    frmCamposSolic.g_bConCampos = m_bConCampos
    frmCamposSolic.Show vbModal
End Sub
Public Sub AnyadirCampoAtributoGS()
    If m_bConDesgloseFactura Or m_bConDesgloseDePedido Then
        oMensajes.ImposibleAnyadirCampoGrupo (2)
        Exit Sub
    End If

    g_Accion = ACCFormItemAnyadir
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If
    Set g_ofrmATRIB = New frmAtrib
    g_ofrmATRIB.g_sOrigen = "frmFormularios"

    Screen.MousePointer = vbHourglass
    MDI.MostrarFormulario g_ofrmATRIB
    Screen.MousePointer = vbNormal
End Sub
Public Sub AnyadirCampoNuevo()
    If m_bConDesgloseFactura Or m_bConDesgloseDePedido Then
        oMensajes.ImposibleAnyadirCampoGrupo (2)
        Exit Sub
    End If

    'LLama a la pantalla para crear un campo nuevo:
    g_Accion = ACCFormItemAnyadir
    
    frmFormAnyaCampos.g_sOrigen = "frmFormularios"
    frmFormAnyaCampos.g_lIdGrupo = g_oGrupoSeleccionado.Id
    
    Set frmFormAnyaCampos.g_oIdiomas = m_oIdiomas
    frmFormAnyaCampos.Show vbModal
End Sub
Public Sub AnyadirCampoGS(ByVal iTipoCampoGS As Integer, Optional ByVal sPRES5 As String)
Dim oCampos As CFormItems
Dim oCampo As CFormItem
Dim oGrupo As CFormGrupo
Dim udtTeserror As TipoErrorSummit
Dim i As Integer

    If m_bConDesgloseFactura Then
        If iTipoCampoGS = TipoCampoGS.DesgloseFactura Then
            oMensajes.ImposibleAnyadirCampoGrupo (1)
        Else
            oMensajes.ImposibleAnyadirCampoGrupo (2)
        End If
        Exit Sub
    End If
    
    If m_bConDesgloseDePedido Then
        If iTipoCampoGS = TipoCampoGS.DesgloseDePedido Then
            oMensajes.ImposibleAnyadirCampoGrupo (4)
        Else
            oMensajes.ImposibleAnyadirCampoGrupo (2)
        End If
        Exit Sub
    End If
    
    'A�ade un campo del sistema al grupo seleccionado:
    g_Accion = ACCFormItemAnyadir
    Set oCampos = oFSGSRaiz.Generar_CFormCampos
    
    If iTipoCampoGS = TipoCampoGS.Provincia Then
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.Pais, False)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
    ElseIf iTipoCampoGS = TipoCampoGS.NuevoCodArticulo Then
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.material, False)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.DenArticulo, False)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
    ElseIf iTipoCampoGS = TipoCampoGS.Desglose Then
        Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo, , , , , , , , , , , , , oCampo.MostrarPOPUP
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.material, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.NuevoCodArticulo, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.DenArticulo, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoSC.PrecioUnitario, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoSC.Cantidad, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.Unidad, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoSC.IniSuministro, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoSC.FinSuministro, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.Dest, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.FormaPago, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.Proveedor, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        If gParametrosGenerales.gbUsarPres1 = True Then
            Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.PRES1, True)
            oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        End If
        If gParametrosGenerales.gbUsarPres2 = True Then
            Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.PRES2, True)
            oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        End If
        If gParametrosGenerales.gbUsarPres3 = True Then
            Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.Pres3, True)
            oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        End If
        If gParametrosGenerales.gbUsarPres4 = True Then
            Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.Pres4, True)
            oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        End If
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoSC.ArchivoEspecific, True)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
     ElseIf iTipoCampoGS = TipoCampoGS.PartidaPresupuestaria Then
        Set oCampo = GenerarEstructuraArbolPresupuestario(iTipoCampoGS, sPRES5, False)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo, , , , , , , , , , , , , , , , , , , , , , , , oCampo.PRES5
    ElseIf iTipoCampoGS = TipoCampoGS.DesgloseActividad Then
        If Not m_bConDesgloseActividad Then
            Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
            oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
            m_bConDesgloseActividad = True
        Else
            oMensajes.MensajeOKOnly 1209
            Exit Sub
        End If
    ElseIf iTipoCampoGS = TipoCampoGS.DesgloseFactura Then
        If g_oFormSeleccionado.Grupos.Count > 1 Then 'si hay m�s de un grupo
            oMensajes.ImposibleAnyadirCampoGrupo (3)
            Exit Sub
        End If
        If Not m_bConCampos Then
            Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
            oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
            m_bConDesgloseFactura = True
            cmdAnyaGr.Enabled = False
            cmdMoverGrupo.Enabled = False
        Else
            oMensajes.ImposibleAnyadirCampoGrupo (2)
            Exit Sub
        End If
    ElseIf iTipoCampoGS = TipoCampoGS.ProveedorERP Then
        'En ese grupo deberia existir el campo de sistema Proveedor
        Dim bCampoGSProveedorEncontrado As Boolean
        For Each oCampo In g_oGrupoSeleccionado.Campos
            If oCampo.CampoGS = TipoCampoGS.Proveedor Then
                bCampoGSProveedorEncontrado = True
                Exit For
            End If
        Next
        'Si en el grupo no esta el campo de sistema Proveedor,sacamos un mensaje
        If bCampoGSProveedorEncontrado = False Then
            oMensajes.NoExisteCampoGsProveedor
            Exit Sub
        Else
            'Comprobaremos que tambien exista el campo Org.compras a nivel de todo el formulario
            If ComprobarOrgCompras Then
                Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
                oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
            Else
                oMensajes.NoExisteCampoGsOrgCompras
                Exit Sub
            End If
        End If
    ElseIf iTipoCampoGS = TipoCampoGS.CodComprador Then
        'Compruebo que no exista ya un campo de tipo comprador en el formulario
        Dim bCampoGSCompradorEncontrado As Boolean
        For Each oGrupo In g_oFormSeleccionado.Grupos
            If Not oGrupo.Campos Is Nothing Then
                For Each oCampo In oGrupo.Campos
                    If oCampo.CampoGS = TipoCampoGS.CodComprador Then
                        bCampoGSCompradorEncontrado = True
                        Exit For
                    End If
                Next
            End If
        Next
        If bCampoGSCompradorEncontrado Then
            oMensajes.ExisteComprador
            Exit Sub
        Else
            Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
            oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
        End If
     ElseIf iTipoCampoGS = TipoCampoGS.DesgloseDePedido Then
        If g_oFormSeleccionado.Grupos.Count > 1 Then 'si hay m�s de un grupo
            oMensajes.ImposibleAnyadirCampoGrupo (5)
            Exit Sub
        End If
        If Not m_bConCampos Then
            Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
            oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
            m_bConDesgloseDePedido = True
            cmdAnyaGr.Enabled = False
            cmdMoverGrupo.Enabled = False
        Else
            oMensajes.ImposibleAnyadirCampoGrupo (2)
            Exit Sub
        End If
    ElseIf iTipoCampoGS = TipoCampoGS.Moneda Then
        If g_oFormSeleccionado.ExisteCampoGSFormulario(TipoCampoGS.Moneda) Then
            oMensajes.mensajeGenericoOkOnly m_sImposibleAnyadircampo & vbCrLf & m_sNoPuedenExistirDosCampoMoneda, Exclamation
            Exit Sub
        Else
            Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
            oCampos.addCampo oCampo, g_oGrupoSeleccionado
        End If
    ElseIf iTipoCampoGS = TipoCampoGS.AnyoImputacion Then
        Set oCampo = GenerarEstructuraCampoSistema(TipoCampoGS.AnyoImputacion, False)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
    Else
        Set oCampo = GenerarEstructuraCampoSistema(iTipoCampoGS, False)
        oCampos.Add oCampo.Id, g_oGrupoSeleccionado, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, , , , , oCampo.Minimo, oCampo.Maximo, oCampo.Orden, oCampo.idAtrib, oCampo.CampoGS, oCampo.FecAct, , , oCampo.EsSubCampo
    End If
    
    'A�ade el campo a BD:
    udtTeserror = oCampos.AnyadirCamposDeGSoAtributos(False, True)
    If udtTeserror.NumError = TESnoerror Then
        'Lo a�ade a la colecci�n y grid de formularios
        If iTipoCampoGS = TipoCampoGS.Desglose Then
            'si era un campo de tipo desglose solo a�ado el campo,los de desglose no:
            For i = oCampos.Count To 2 Step -1   'Elimina de la colecci�n los subcampos del campo desglose
                oCampos.Remove (i)
            Next i
        End If
        
        AnyadirCampos oCampos
        RegistrarAccion ACCFormItemAnyadir, "Id:" & oCampo.Id
    Else
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
        Exit Sub
    End If
    
    Set oCampos = Nothing
    Set oCampo = Nothing
    
    g_Accion = ACCFormularioCons
End Sub

Private Function GenerarEstructuraCampoSistema(ByVal iTipoCampoGS, Optional ByVal bSubcampo As Boolean = False) As CFormItem
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim oParametros As CLiterales
    Dim Ador As Ador.Recordset
    Dim oCampo As CFormItem

    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    Set oCampo.Grupo = g_oGrupoSeleccionado
    oCampo.Id = iTipoCampoGS
    oCampo.EsSubCampo = bSubcampo
    
    Select Case iTipoCampoGS
        Case TipoCampoSC.importe, TipoCampoSC.Cantidad, TipoCampoSC.PrecioUnitario, TipoCampoGS.ImporteSolicitudesVinculadas, _
                TipoCampoGS.Almacen, TipoCampoGS.RefSolicitud, TipoCampoGS.Empresa, TipoCampoGS.AnyoImputacion
            oCampo.Tipo = TiposDeAtributos.TipoNumerico
        Case TipoCampoGS.Desglose
            oCampo.Tipo = TiposDeAtributos.TipoDesglose
            oCampo.MostrarPOPUP = True
        Case TipoCampoSC.ArchivoEspecific
            oCampo.Tipo = TiposDeAtributos.TipoArchivo
        Case TipoCampoSC.DescrDetallada
            oCampo.Tipo = TiposDeAtributos.TipoTextoLargo
         Case TipoCampoGS.DesgloseActividad
            oCampo.Tipo = TiposDeAtributos.TipoDesglose
        Case TipoCampoSC.FecNecesidad, TipoCampoSC.FinSuministro, TipoCampoSC.IniSuministro, TipoCampoGS.InicioAbono, TipoCampoGS.FinAbono
            oCampo.Tipo = TiposDeAtributos.TipoFecha
        Case TipoCampoGS.RetencionEnGarantia
            oCampo.Tipo = TiposDeAtributos.TipoNumerico
            oCampo.Minimo = 0
            oCampo.Maximo = 100
        Case Else
            oCampo.Tipo = TiposDeAtributos.TipoTextoMedio
    End Select
     
    oCampo.CampoGS = iTipoCampoGS
    oCampo.TipoPredef = TipoCampoPredefinido.CampoGS
    
    'Carga las denominaciones del grupo de datos generales en todos los idiomas
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In m_oIdiomas
        If oIdioma.Cod = gParametrosInstalacion.gIdioma Then
            Select Case iTipoCampoGS
                Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(5)
                Case TipoCampoGS.Desglose
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(7)
                Case TipoCampoGS.Dest
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(10)
                Case TipoCampoGS.FormaPago
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(2)
                Case TipoCampoGS.material
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(4)
                Case TipoCampoGS.Moneda
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(3)
                Case TipoCampoGS.Pais
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(8)
                Case TipoCampoGS.Proveedor
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(1)
                Case TipoCampoGS.Provincia
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(9)
                Case TipoCampoGS.Unidad
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(6)
                Case TipoCampoGS.PRES1
                    oDenominaciones.Add oIdioma.Cod, gParametrosGenerales.gsSingPres1
                Case TipoCampoGS.PRES2
                    oDenominaciones.Add oIdioma.Cod, gParametrosGenerales.gsSingPres2
                Case TipoCampoGS.Pres3
                    oDenominaciones.Add oIdioma.Cod, gParametrosGenerales.gsSingPres3
                Case TipoCampoGS.Pres4
                    oDenominaciones.Add oIdioma.Cod, gParametrosGenerales.gsSingPres4
                Case TipoCampoGS.CampoPersona
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(11)
                Case TipoCampoGS.DenArticulo
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(12)
                Case TipoCampoSC.DescrBreve
                    oDenominaciones.Add oIdioma.Cod, m_sCamposSC(1)
                Case TipoCampoSC.DescrDetallada
                    oDenominaciones.Add oIdioma.Cod, m_sCamposSC(2)
                Case TipoCampoSC.importe
                    oDenominaciones.Add oIdioma.Cod, m_sCamposSC(3)
                Case TipoCampoSC.Cantidad
                    oDenominaciones.Add oIdioma.Cod, m_sCamposSC(4)
                Case TipoCampoSC.FecNecesidad
                    oDenominaciones.Add oIdioma.Cod, m_sCamposSC(5)
                Case TipoCampoSC.IniSuministro
                    oDenominaciones.Add oIdioma.Cod, m_sCamposSC(6)
                Case TipoCampoSC.FinSuministro
                    oDenominaciones.Add oIdioma.Cod, m_sCamposSC(7)
                Case TipoCampoSC.ArchivoEspecific
                    oDenominaciones.Add oIdioma.Cod, m_sCamposSC(8)
                Case TipoCampoSC.PrecioUnitario
                    oDenominaciones.Add oIdioma.Cod, m_sCamposSC(9)
                Case TipoCampoGS.NumSolicitERP
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(13)
                Case TipoCampoGS.UnidadOrganizativa
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(14)
                Case TipoCampoGS.Departamento
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(15)
                Case TipoCampoGS.OrganizacionCompras
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(16)
                Case TipoCampoGS.Centro
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(17)
                Case TipoCampoGS.Almacen
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(18)
                Case TipoCampoGS.ImporteSolicitudesVinculadas
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(19)
                Case TipoCampoGS.RefSolicitud
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(20)
                Case TipoCampoGS.CentroCoste
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(21)
                Case TipoCampoGS.Activo
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(22)
                Case TipoCampoGS.TipoPedido
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(23)
                Case TipoCampoGS.DesgloseActividad
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(24)
                Case TipoCampoGS.DesgloseFactura
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(25)
                Case TipoCampoGS.Factura
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(26)
                Case TipoCampoGS.InicioAbono
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(27)
                Case TipoCampoGS.FinAbono
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(28)
                Case TipoCampoGS.RetencionEnGarantia
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(29)
                Case TipoCampoGS.UnidadPedido
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(30)
                Case TipoCampoGS.ProveedorERP
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(31)
                Case TipoCampoGS.CodComprador
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(32)
                Case TipoCampoGS.DesgloseDePedido
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(33)
                Case TipoCampoGS.Empresa
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(34)
                Case TipoCampoGS.AnyoImputacion
                    oDenominaciones.Add oIdioma.Cod, m_sCamposGS(35)
            End Select
        Else
            Select Case iTipoCampoGS
                Case TipoCampoGS.PRES1
                    Set oParametros = oGestorParametros.DevolverLiterales(20, 20, oIdioma.Cod)
                    oDenominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case TipoCampoGS.PRES2
                    Set oParametros = oGestorParametros.DevolverLiterales(21, 21, oIdioma.Cod)
                    oDenominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case TipoCampoGS.Pres3
                    Set oParametros = oGestorParametros.DevolverLiterales(27, 27, oIdioma.Cod)
                    oDenominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case TipoCampoGS.Pres4
                    Set oParametros = oGestorParametros.DevolverLiterales(28, 28, oIdioma.Cod)
                    oDenominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case Else
                    Select Case iTipoCampoGS
                        Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 5)
                        Case TipoCampoGS.DenArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 36)
                        Case TipoCampoGS.Desglose
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 7)
                        Case TipoCampoGS.Dest
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 10)
                        Case TipoCampoGS.FormaPago
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 2)
                        Case TipoCampoGS.material
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 4)
                        Case TipoCampoGS.Moneda
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 3)
                        Case TipoCampoGS.Pais
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 8)
                        Case TipoCampoGS.Proveedor
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 1)
                        Case TipoCampoGS.Provincia
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 9)
                        Case TipoCampoGS.Unidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 6)
                        Case TipoCampoGS.CampoPersona
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 35)
                        Case TipoCampoSC.DescrBreve
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 26)
                        Case TipoCampoSC.DescrDetallada
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 27)
                        Case TipoCampoSC.importe
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 28)
                        Case TipoCampoSC.Cantidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 29)
                        Case TipoCampoSC.FecNecesidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 30)
                        Case TipoCampoSC.IniSuministro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 31)
                        Case TipoCampoSC.FinSuministro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 32)
                        Case TipoCampoSC.ArchivoEspecific
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 33)
                        Case TipoCampoSC.PrecioUnitario
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 34)
                        Case TipoCampoGS.NumSolicitERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 37)
                        Case TipoCampoGS.UnidadOrganizativa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 38)
                        Case TipoCampoGS.Departamento
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 39)
                        Case TipoCampoGS.OrganizacionCompras
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 40)
                        Case TipoCampoGS.Centro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 41)
                        Case TipoCampoGS.Almacen
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 42)
                        Case TipoCampoGS.ImporteSolicitudesVinculadas
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 43)
                        Case TipoCampoGS.RefSolicitud
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 44)
                        Case TipoCampoGS.CentroCoste
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 48)
                        Case TipoCampoGS.Activo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 49)
                        Case TipoCampoGS.TipoPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 50)
                        Case TipoCampoGS.DesgloseActividad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 51)
                        Case TipoCampoGS.DesgloseFactura
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 52)
                        Case TipoCampoGS.Factura
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 53)
                        Case TipoCampoGS.InicioAbono
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 54)
                        Case TipoCampoGS.FinAbono
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 55)
                        Case TipoCampoGS.RetencionEnGarantia
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 56)
                        Case TipoCampoGS.UnidadPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 57)
                        Case TipoCampoGS.ProveedorERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 58)
                        Case TipoCampoGS.CodComprador
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 59)
                        Case TipoCampoGS.DesgloseDePedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 60)
                        Case TipoCampoGS.Empresa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 65)
                        Case TipoCampoGS.AnyoImputacion
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 67)
                    End Select
                    oDenominaciones.Add oIdioma.Cod, Ador(0).Value
                    Ador.Close
            End Select
            Set oParametros = Nothing
        End If
    Next
    Set oCampo.Denominaciones = oDenominaciones
    Set Ador = Nothing
    
    Set GenerarEstructuraCampoSistema = oCampo
End Function

Private Function GenerarEstructuraCampoExterno(ByVal pIdTabla As Integer, pTipoDeDatoPK As TiposDeAtributos, Optional ByVal bSubcampo As Boolean = False) As CFormItem
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim oParametros As CLiterales
    Dim oCampo As CFormItem

    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    Set oCampo.Grupo = g_oGrupoSeleccionado
    oCampo.Id = TipoCampoPredefinido.externo
    oCampo.EsSubCampo = bSubcampo
    oCampo.Tipo = pTipoDeDatoPK
    oCampo.TipoPredef = TipoCampoPredefinido.externo
    oCampo.TablaExterna = pIdTabla

    m_sCamposExterno(1) = m_oTablaExterna.SacarDenominacionTabla(pIdTabla)
    
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In m_oIdiomas
        oDenominaciones.Add oIdioma.Cod, m_sCamposExterno(1)
    Next
    
    Set oParametros = Nothing
    Set oCampo.Denominaciones = oDenominaciones
    Set GenerarEstructuraCampoExterno = oCampo
End Function

Private Function GenerarEstructuraArbolPresupuestario(ByVal iTipoCampoGS As Integer, ByVal sPRES5 As String, Optional ByVal bSubcampo As Boolean = False) As CFormItem
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim oCampo As CFormItem
    Dim oPres5Niv0 As cPresConceptos5Nivel0
    Dim Adores As ADODB.Recordset
    
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    Set oCampo.Grupo = g_oGrupoSeleccionado
    oCampo.Id = iTipoCampoGS
    oCampo.EsSubCampo = bSubcampo
    oCampo.Tipo = TiposDeAtributos.TipoTextoMedio
    oCampo.CampoGS = iTipoCampoGS
    oCampo.TipoPredef = TipoCampoPredefinido.CampoGS
    oCampo.PRES5 = sPRES5
    
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    Set oPres5Niv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    For Each oIdioma In m_oIdiomas
        Set Adores = oPres5Niv0.DevolverDenominacion(oIdioma.Cod, sPRES5)
        oDenominaciones.Add oIdioma.Cod, Adores(0).Value
    Next
    
    Set oCampo.Denominaciones = oDenominaciones
    Set GenerarEstructuraArbolPresupuestario = oCampo
End Function

Public Sub CopiarFormularioSeleccionado()
    'Crea una copia del formulario seleccionado
Dim i As Integer
Dim bm As Variant
Dim oForm As CFormulario
Dim teserror As TipoErrorSummit

    Screen.MousePointer = vbHourglass
    teserror = Me.g_oFormSeleccionado.CopiarFormulario()
    If teserror.NumError = TESnoerror Then
        'Ha copiado el formulario, ahora nos posicionamos en ese
        'formulario, para ello tenemos en g_oFormSeleccionado.Id, el ID del nuevo form
        'Vaciamos el combo y lo volvemos a cargar
        sdbcFormulario.RemoveAll
        g_oFormularios.CargarTodosFormularios
        For Each oForm In g_oFormularios
            sdbcFormulario.AddItem oForm.Id & Chr(m_lSeparador) & oForm.Den
        Next
        'seleccionamos nuestro formulario
        sdbcFormulario.MoveFirst
         For i = 0 To sdbcFormulario.Rows - 1
            bm = sdbcFormulario.GetBookmark(i)
            If Me.g_oFormSeleccionado.Id = sdbcFormulario.Columns(0).CellText(bm) Then
                sdbcFormulario.Bookmark = bm
                sdbcFormulario.Value = sdbcFormulario.Columns(1).CellText(bm)
                Exit For
            End If
        Next i
        'Cargamos los datos
        Set g_oFormSeleccionado = g_oFormularios.Item(sdbcFormulario.Columns(0).Text)
        FormularioSeleccionado
    Else
        basErrores.TratarError teserror
    End If
    Screen.MousePointer = vbNormal
End Sub
Public Sub AnyaNuevoFormulario()
    Dim udtTeserror As TipoErrorSummit
    
    'Hace la llamada al formulario frmFormularioAnya para a�adir un formulario
    g_Accion = ACCFormularioAnyadir
    
    udtTeserror = MostrarFormFormularioAnya(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, g_Accion, g_oFormSeleccionado, oGestorParametros.DevolverIdiomas(, , True), _
                   basPublic.gParametrosInstalacion.gIdioma)
    If udtTeserror.Arg2 Then
        If udtTeserror.NumError = TESnoerror Then
            g_oFormularios.Add udtTeserror.Arg1.Id, udtTeserror.Arg1.Den, udtTeserror.Arg1.Multiidioma, , udtTeserror.Arg1.FecAct
            AnyaModifFormulario udtTeserror.Arg1.Id
            
            RegistrarAccion ACCFormularioAnyadir, "Id:" & udtTeserror.Arg1.Id
        Else
            TratarError udtTeserror
        End If
    End If
    g_Accion = ACCFormularioCons
End Sub
Public Sub MostrarUOSeleccionada()
    If frmSELUO.sUON3 <> "" Then
        sdbgCampos.Columns("VALOR").Value = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sUON3 & " - " & frmSELUO.sDen
    ElseIf frmSELUO.sUON2 <> "" Then
        sdbgCampos.Columns("VALOR").Value = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sDen
    ElseIf frmSELUO.sUON1 <> "" Then
        sdbgCampos.Columns("VALOR").Value = frmSELUO.sUON1 & " - " & frmSELUO.sDen
    ElseIf frmSELUO.sDen <> "" Then
        sdbgCampos.Columns("VALOR").Value = frmSELUO.sDen
    End If
    sdbgCampos.Columns("BAJALOG").Value = False
    sdbgCampos.Columns("VALOR").CellStyleSet ""
    sdbgCampos.Bookmark = sdbgCampos.GetBookmark(0) 'Esto es necesario para refrescar el estilo

    sdbgCampos.MoveNext
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Departamento Then
        sdbgCampos.Columns("COD_VALOR").Value = ""
        sdbgCampos.Columns("VALOR").Value = ""
        g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).valorText = ""
        Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
        sdbgCampos.Columns("BAJALOG").Value = False
        sdbgCampos.Columns("VALOR").CellStyleSet ""
        sdbgCampos.Bookmark = sdbgCampos.GetBookmark(0) 'Esto es necesario para refrescar el estilo
    End If
    sdbgCampos.MovePrevious
End Sub
''' Funcion que devuelve el Nombre del CampoGS, certificado o No Conformidad
''' y si es o no MultiIdioma carga adem�s los nombres en los distintos idiomas
Private Function ObtenerNombreCampo(ByVal lCampoGS As Long, ByVal iTipo As Integer, ByVal iTipoPredef As Integer, ByVal bMultiIdioma As Boolean, Optional ByVal sPRES5 As String) As String
    Dim sNombre As String
    Dim sNombreIdiomas As String
    Dim sNombreAux As String
    
    Dim oIdioma As CIdioma
    Dim oParametros As CLiterales
    Dim Ador As Ador.Recordset
    Dim oPres5Niv0 As cPresConceptos5Nivel0

    Select Case iTipo
        Case TipoCampoPredefinido.CampoGS
            For Each oIdioma In m_oIdiomas
                Select Case lCampoGS
                    Case TipoCampoGS.PRES1
                        Set oParametros = oGestorParametros.DevolverLiterales(20, 20, oIdioma.Cod)
                        sNombreAux = oParametros.Item(1).Den
                    Case TipoCampoGS.PRES2
                        Set oParametros = oGestorParametros.DevolverLiterales(21, 21, oIdioma.Cod)
                        sNombreAux = oParametros.Item(1).Den
                    Case TipoCampoGS.Pres3
                        Set oParametros = oGestorParametros.DevolverLiterales(27, 27, oIdioma.Cod)
                        sNombreAux = oParametros.Item(1).Den
                    Case TipoCampoGS.Pres4
                        Set oParametros = oGestorParametros.DevolverLiterales(28, 28, oIdioma.Cod)
                        sNombreAux = oParametros.Item(1).Den
                    
                    Case Else
                        Select Case lCampoGS
                            Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 5)
                            Case TipoCampoGS.DenArticulo
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 36)
                            Case TipoCampoGS.Desglose
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 7)
                            Case TipoCampoGS.Dest
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 10)
                            Case TipoCampoGS.FormaPago
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 2)
                            Case TipoCampoGS.material
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 4)
                            Case TipoCampoGS.Moneda
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 3)
                            Case TipoCampoGS.Pais
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 8)
                            Case TipoCampoGS.Proveedor
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 1)
                            Case TipoCampoGS.Provincia
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 9)
                            Case TipoCampoGS.Unidad
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 6)
                            Case TipoCampoGS.CampoPersona
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 35)
                            Case TipoCampoSC.DescrBreve
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 26)
                            Case TipoCampoSC.DescrDetallada
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 27)
                            Case TipoCampoSC.importe
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 28)
                            Case TipoCampoSC.Cantidad
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 29)
                            Case TipoCampoSC.FecNecesidad
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 30)
                            Case TipoCampoSC.IniSuministro
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 31)
                            Case TipoCampoSC.FinSuministro
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 32)
                            Case TipoCampoSC.ArchivoEspecific
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 33)
                            Case TipoCampoSC.PrecioUnitario
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 34)
                            Case TipoCampoGS.NumSolicitERP
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 37)
                            Case TipoCampoGS.UnidadOrganizativa
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 38)
                            Case TipoCampoGS.Departamento
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 39)
                            Case TipoCampoGS.OrganizacionCompras
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 40)
                            Case TipoCampoGS.Centro
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 41)
                            Case TipoCampoGS.Almacen
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 42)
                            Case TipoCampoGS.ImporteSolicitudesVinculadas
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 43)
                            Case TipoCampoGS.RefSolicitud
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 44)
                            Case TipoCampoSC.PrecioUnitarioAdj
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 45)
                            Case TipoCampoSC.ProveedorAdj
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 46)
                            Case TipoCampoSC.CantidadAdj
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 47)
                            Case TipoCampoGS.CentroCoste
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 48)
                            Case TipoCampoGS.Activo
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 49)
                            Case TipoCampoGS.TipoPedido
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 50)
                            Case TipoCampoGS.DesgloseActividad
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 51)
                            Case TipoCampoGS.DesgloseFactura
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 52)
                            Case TipoCampoGS.Factura
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 53)
                            Case TipoCampoGS.InicioAbono
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 54)
                            Case TipoCampoGS.FinAbono
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 55)
                            Case TipoCampoGS.RetencionEnGarantia
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 56)
                            Case TipoCampoGS.UnidadPedido
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 57)
                            Case TipoCampoGS.ProveedorERP
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 58)
                            Case TipoCampoGS.CodComprador
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 59)
                             Case TipoCampoGS.DesgloseDePedido
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 60)
                            Case TipoCampoGS.PartidaPresupuestaria
                                Set oPres5Niv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
                                Set Ador = oPres5Niv0.DevolverDenominacion(oIdioma.Cod, sPRES5)
                                Set oPres5Niv0 = Nothing
                            Case TipoCampoGS.RenovacionAutomatica
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 63)
                            Case TipoCampoGS.PeriodoRenovacion
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 64)
                            Case TipoCampoGS.Empresa
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 65)
                            Case TipoCampoGS.EstadoHomologacion
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 66)
                            Case TipoCampoGS.AnyoImputacion
                                Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICITUDES, oIdioma.Cod, 67)
                        End Select
                        sNombreAux = Ador(0).Value
                        Ador.Close
                    End Select
                    
                    If (oIdioma.Cod = gParametrosInstalacion.gIdioma) Then
                        sNombre = sNombreAux
                    Else
                        sNombreIdiomas = sNombreIdiomas & " / " & sNombreAux
                    End If
                    Set oParametros = Nothing
                Next
                
                If bMultiIdioma Then
                    sNombre = sNombre & sNombreIdiomas
                End If
    
    Case TipoCampoPredefinido.Certificado, TipoCampoPredefinido.NoConformidad
        Dim oTipoSolic As CTipoSolicit
        
        Set oTipoSolic = oFSGSRaiz.Generar_CTipoSolicit
        oTipoSolic.CargarTipoSolicitud lCampoGS, iTipo, iTipoPredef
        
        While oTipoSolic.Campos.Item(1).Denominaciones.Count > 0
            If oTipoSolic.Campos.Item(1).Denominaciones.Item(1).Cod = gParametrosInstalacion.gIdioma Then
                sNombre = oTipoSolic.Campos.Item(1).Denominaciones.Item(1).Den
            Else
                sNombreIdiomas = sNombreIdiomas & " / " & oTipoSolic.Campos.Item(1).Denominaciones.Item(1).Den
            End If
            oTipoSolic.Campos.Item(1).Denominaciones.Remove (oTipoSolic.Campos.Item(1).Denominaciones.Item(1).Cod)
        Wend
        
        If bMultiIdioma Then
            sNombre = sNombre & sNombreIdiomas
        End If
        Set oTipoSolic = Nothing
    End Select

ObtenerNombreCampo = sNombre

        
End Function

Public Sub MarcarTitulo()
    'Se actualiza la tabla FORM_CAMPO poniendo TITULO a 1. Este campo se mostrara luego en el PM en la columna
    'de denominacion del visor de seguimiento y tareas.
    Dim oGrupo As CFormGrupo
    Dim i As Integer
    i = ssTabGrupos.selectedItem.Index
    'Solo se marcaran como titulo los campos que no sean de tipo desglose
    If sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.Desglose And sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoDesglose Then
         'Comprobar si en los otros grupos del formulario hay ya un titulo marcado para quitarlo y marcar el nuevo
        For Each oGrupo In g_oFormSeleccionado.Grupos
            g_oFormSeleccionado.ActualizarCampoTitulo 0, oGrupo.Id, 0, 0
        Next
        g_oFormSeleccionado.ActualizarCampoTitulo sdbgCampos.Columns("ID").Value, g_oGrupoSeleccionado.Id, sdbgCampos.Columns("TIPO").Value, sdbgCampos.Columns("CAMPO_GS").Value
        'Actualizar el grid
        FormularioSeleccionado (i)
        ssTabGrupos.Tabs(i).Selected = True
    End If
End Sub

Public Sub DesmarcarTitulo()
    'Se actualiza la tabla FORM_CAMPO poniendo TITULO a 0.
    Dim i As Integer
    i = ssTabGrupos.selectedItem.Index
     g_oFormSeleccionado.DesmarcarTitulo sdbgCampos.Columns("ID").Value, g_oGrupoSeleccionado.Id, sdbgCampos.Columns("TIPO").Value, NullToDbl0(sdbgCampos.Columns("CAMPO_GS").Value)
    'Actualizar el grid
     FormularioSeleccionado (i)
     ssTabGrupos.Tabs(i).Selected = True
End Sub

Public Sub DesmarcarDenominacionProceso()
    'Se actualiza la tabla FORM_CAMPO poniendo DEN_PROCESO a 0.
    Dim i As Integer
    i = ssTabGrupos.selectedItem.Index
     g_oFormSeleccionado.DesmarcarDenominacionProceso sdbgCampos.Columns("ID").Value, g_oGrupoSeleccionado.Id, sdbgCampos.Columns("TIPO").Value, NullToDbl0(sdbgCampos.Columns("CAMPO_GS").Value)
    'Actualizar el grid
     FormularioSeleccionado (i)
     ssTabGrupos.Tabs(i).Selected = True
End Sub

Public Sub MarcarDenominacionProceso()
    'Se actualiza la tabla FORM_CAMPO poniendo DEN_PROCESO a 1
    Dim oGrupo As CFormGrupo
    Dim i As Integer
    i = ssTabGrupos.selectedItem.Index
    'Solo se marcaran como Denominacion de proceso los campos que no sean de tipo desglose
    If sdbgCampos.Columns("CAMPO_GS").Value <> TipoCampoGS.Desglose And sdbgCampos.Columns("SUBTIPO").Value <> TiposDeAtributos.TipoDesglose Then
         'Comprobar si en los otros grupos del formulario hay ya un campo denominacion de proceso marcado para quitarlo y marcar el nuevo
        For Each oGrupo In g_oFormSeleccionado.Grupos
            g_oFormSeleccionado.ActualizarCampoDenominacionProceso 0, oGrupo.Id, 0, 0
        Next
        g_oFormSeleccionado.ActualizarCampoDenominacionProceso sdbgCampos.Columns("ID").Value, g_oGrupoSeleccionado.Id, sdbgCampos.Columns("TIPO").Value, sdbgCampos.Columns("CAMPO_GS").Value
        'Actualizar el grid
        FormularioSeleccionado (i)
        ssTabGrupos.Tabs(i).Selected = True
    End If
End Sub
''' <summary>Marca un campo como peso y desmarca los que pudiera haber</summary>
''' <remarks>Llamada desde: MDI.mnuPOPUPNewCopyForm_Click</remarks>
Public Sub MarcarComoPeso()
    Dim i As Integer
    i = ssTabGrupos.selectedItem.Index
    g_oFormSeleccionado.MarcarPeso (sdbgCampos.Columns("ID").Value)
    FormularioSeleccionado i
    ssTabGrupos.Tabs(i).Selected = True
End Sub
''' <summary>Desmarca un campo como peso</summary>
''' <remarks>Llamada desde: MDI.mnuPOPUPNewCopyForm_Click</remarks>
Public Sub DesmarcarComoPeso()
    Dim i As Integer
    i = ssTabGrupos.selectedItem.Index
    g_oFormSeleccionado.DesmarcarPeso (sdbgCampos.Columns("ID").Value)
    FormularioSeleccionado i
    ssTabGrupos.Tabs(i).Selected = True
End Sub
Private Sub sdbgCampos_LostFocus()
    If m_bCancel Then Exit Sub
End Sub

Private Function esGenerico(sCodArt As String, Optional bAnyadirArt As Boolean = False) As Boolean

    Dim oArticulos As CArticulos
    Set oArticulos = oFSGSRaiz.Generar_CArticulos

    If sCodArt <> "" Then
        oArticulos.CargarTodosLosArticulos sCodArt, , True
        
        If oArticulos.Count > 0 Then
            esGenerico = CBool(oArticulos.Item(1).Generico)
        Else
            If bAnyadirArt Then
                esGenerico = True
            Else
                esGenerico = False
            End If
        End If
    Else
        esGenerico = True
    End If
    
    Set oArticulos = Nothing
End Function

Private Sub sdbddArticulos_CloseUp()
    Dim i As Long
    Dim bm As Variant
    Dim oUnidades As CUnidades
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
        g_bUpdate = False
    End If
   
    sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("COD").Value
    
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
        g_bUpdate = True
        sdbgCampos.Columns("GENERICO").Value = sdbddArticulos.Columns("GENERICO").Value
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Then
            sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("COD").Value & " - " & sdbddArticulos.Columns("DEN").Value
        Else
            sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("COD").Value
        End If
        
        sdbgCampos.MoveNext
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
            sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("DEN").Value
            sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("DEN").Value
            sdbgCampos.MovePrevious
        End If
        
        
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo Then
        sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("DEN").Value
        sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("DEN").Value
        sdbgCampos.MovePrevious
        sdbgCampos.Columns("GENERICO").Value = sdbddArticulos.Columns("GENERICO").Value
        sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("COD").Value
        sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("COD").Value
        
        sdbgCampos.MoveNext
        
    Else
        If sdbddArticulos.Columns("COD").Value <> "" Or sdbddArticulos.Columns("DEN").Value <> "" Then
            sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("COD").Value & " - " & sdbddArticulos.Columns("DEN").Value
        End If
    End If
    If sdbgCampos.Columns("COD_VALOR").Value <> "" Then
        'Buscar si hay Precio y Proveedor y mirar si tiene el checkBox de cargar el ultimo Precio/proveedor adjucicado
        Dim lRowInit As Long
        lRowInit = sdbgCampos.Row
        sdbgCampos.Row = 0
        For i = 0 To sdbgCampos.Rows - 1
            bm = sdbgCampos.GetBookmark(i)
            If sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoSC.PrecioUnitario Then
                If g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(bm))).CargarUltADJ = True Then
                    sdbgCampos.Row = i
                    sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("PREC_ULT_ADJ").Value
                    sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("PREC_ULT_ADJ").Value
                    
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                    sdbgCampos.Row = 0
                End If
            End If
            If sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoGS.Proveedor And sdbddArticulos.Columns("CODPROV_ULT_ADJ").Value <> "" Then
                If g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").CellValue(bm))).CargarUltADJ = True Then
                    sdbgCampos.Row = i
                    sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("CODPROV_ULT_ADJ").Value
                    sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("CODPROV_ULT_ADJ").Value & " - " & sdbddArticulos.Columns("DENPROV_ULT_ADJ").Value
                    
                    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                    sdbgCampos.Row = 0
                End If
            End If
            
            If sdbgCampos.Columns("CAMPO_GS").CellValue(bm) = TipoCampoGS.Unidad Then
                sdbgCampos.Row = i
                If Not IsNull(sdbddArticulos.Columns("UNI").Value) Then
                    Set oUnidades = oFSGSRaiz.Generar_CUnidades
                    oUnidades.CargarTodasLasUnidades sdbddArticulos.Columns("UNI").Value, , True
                    
                    If Not oUnidades.Item(1) Is Nothing Then
                        sdbgCampos.Columns("VALOR").Value = oUnidades.Item(1).Cod & " - " & oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        sdbgCampos.Columns("COD_VALOR").Value = oUnidades.Item(1).Cod
                    Else
                        sdbgCampos.Columns("VALOR").Value = sdbddArticulos.Columns("UNI").Value
                        sdbgCampos.Columns("COD_VALOR").Value = sdbddArticulos.Columns("UNI").Value
                    End If
                    Set oUnidades = Nothing
                End If
                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                sdbgCampos.Row = 0
            End If
        Next i
        sdbgCampos.Row = lRowInit
    End If
  
End Sub

Private Sub sdbddArticulos_DropDown()

Dim oArt As CArticulo
Dim vbm As Variant
Dim arrMat As Variant
Dim oGMN4 As CGrupoMatNivel4
Dim sCodOrgCompras, sCodCentro As String
Dim bUsarOrgCompras As Boolean
Dim i As Integer

Dim oArticulos As CArticulos
    ''' * Objetivo: Abrir el combo de destinos de la forma adecuada
    If Not m_bModif Then
        sdbddArticulos.DroppedDown = False
        Exit Sub
    End If
    
    sdbddArticulos.RemoveAll
    Screen.MousePointer = vbHourglass
    
    Select Case sdbgCampos.Columns("CAMPO_GS").Value

        Case TipoCampoGS.CodArticulo, TipoCampoGS.DenArticulo, TipoCampoGS.NuevoCodArticulo
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
                vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 1)
            Else
                vbm = sdbgCampos.AddItemBookmark(sdbgCampos.Row - 2)
            End If
            
            If gParametrosGenerales.gbUsarOrgCompras = True Then
                Dim vbmOrgCompras, vbmCentro As Variant
                
                'Buscar la Organizacion de Compras y Centros
                i = 0
                While i < sdbgCampos.Row 'And contTerminar <> 1
                     vbmOrgCompras = sdbgCampos.AddItemBookmark(i)
                     If sdbgCampos.Columns("CAMPO_GS").CellValue(vbmOrgCompras) = TipoCampoGS.OrganizacionCompras Then
                         vbmCentro = sdbgCampos.AddItemBookmark(i + 1)
                         sCodOrgCompras = sdbgCampos.Columns("COD_VALOR").CellValue(vbmOrgCompras)
                         If sdbgCampos.Columns("CAMPO_GS").CellValue(vbmCentro) = TipoCampoGS.Centro Then
                             sCodCentro = sdbgCampos.Columns("COD_VALOR").CellValue(vbmCentro)
                             bUsarOrgCompras = True
                         End If
                     End If
                     i = i + 1
                 Wend
                    
            End If

            If sdbgCampos.Columns("COD_VALOR").CellValue(vbm) <> "" Then
                arrMat = DevolverMaterial(sdbgCampos.Columns("COD_VALOR").CellValue(vbm))
        
                Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
                oGMN4.GMN1Cod = arrMat(1)
                oGMN4.GMN2Cod = arrMat(2)
                oGMN4.GMN3Cod = arrMat(3)
                oGMN4.Cod = arrMat(4)
                
                Dim sMoneda As String
                sMoneda = g_oFormSeleccionado.Moneda
                
                If bUsarOrgCompras Then
                    oGMN4.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , False, , , , , True, True, sMoneda, sCodOrgCompras, sCodCentro
                Else
                    oGMN4.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , False, , , , , True, True, sMoneda
                End If
                           
                Set oArticulos = oGMN4.ARTICULOS
                Set oGMN4 = Nothing
                
                For Each oArt In oArticulos
                    sdbddArticulos.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den & Chr(m_lSeparador) & oArt.Generico & Chr(m_lSeparador) & oArt.PREC_UltADJ & Chr(m_lSeparador) & oArt.CODPROV_UltADJ & Chr(m_lSeparador) & oArt.DENPROV_UltADJ & Chr(m_lSeparador) & oArt.CodigoUnidad
                Next
                Set oArticulos = Nothing
            End If

    End Select
    
    
    If sdbddArticulos.Rows = 0 Then
        sdbddArticulos.AddItem ""
    End If
    
    sdbgCampos.ActiveCell.SelStart = 0
    sdbgCampos.ActiveCell.SelLength = Len(sdbgCampos.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal
End Sub




Private Sub sdbddArticulos_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddArticulos.DataFieldList = "Column 0"
    sdbddArticulos.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddArticulos_PositionList(ByVal Text As String)
PositionList sdbddArticulos, Text
End Sub

Private Function PonerEstiloUON(ByVal sValor As String) As Boolean

Dim arrUon() As String

arrUon = Split(sValor, " - ")

Dim ilong As Integer
ilong = UBound(arrUon)

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3
'
    Set oUON1 = oFSGSRaiz.Generar_CUnidadOrgNivel1
    Set oUON2 = oFSGSRaiz.Generar_CUnidadOrgNivel2
    Set oUON3 = oFSGSRaiz.Generar_CUnidadOrgNivel3

Dim Estado As Boolean
'
Select Case ilong
Case 1
        Estado = oUON1.EstadeBajaUON1(arrUon(0))
Case 2
        Estado = oUON2.EstadeBajaUON2(arrUon(0), arrUon(1))
Case 3
        Estado = oUON3.EstadeBajaUON3(arrUon(0), arrUon(1), arrUon(2))

End Select

    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing


PonerEstiloUON = Estado


End Function
 


Private Function PonerEstiloDep(ByVal sValor As String) As Boolean

Dim arrDep() As String

arrDep = Split(sValor, " - ")

Dim ilong As Integer
ilong = UBound(arrDep)

Dim oDep As CDepartamentos
    
    Set oDep = oFSGSRaiz.Generar_CDepartamentos

PonerEstiloDep = oDep.EstadeBajaDep(arrDep(0))


Set oDep = Nothing


End Function


Private Function ExisteAtributoAProceso() As Boolean
    Dim i As Integer
    Dim vbm As Variant
    Dim bExiste As Boolean
    
    bExiste = False
    
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        If sdbgCampos.Columns("IDATRIB").CellValue(vbm) = sdbgCampos.Columns("IDATRIB").Value _
        And sdbgCampos.Columns("APROCESO").CellValue(vbm) = True _
        And sdbgCampos.Row <> i Then
            bExiste = True
            Exit For
        End If
    Next
    
    ExisteAtributoAProceso = bExiste
    
End Function

Public Function CamposSistemaAProceso(Tipo As Variant) As Boolean
  
    Select Case Tipo
    Case TipoCampoSC.DescrBreve, TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro, TipoCampoGS.Dest, TipoCampoGS.FormaPago, TipoCampoGS.Proveedor, TipoCampoGS.PRES1, TipoCampoGS.PRES2, TipoCampoGS.Pres3, TipoCampoGS.Pres4, TipoCampoGS.material, TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo, TipoCampoGS.DenArticulo, TipoCampoSC.Cantidad, TipoCampoSC.PrecioUnitario, TipoCampoGS.Unidad, TipoCampoPredefinido.externo
        CamposSistemaAProceso = True
    Case Else
        CamposSistemaAProceso = False
    End Select

End Function


Private Function ExisteAtributoArticulo() As Boolean
    Dim i As Integer
    Dim vbm As Variant
    Dim bExiste As Boolean
    
    bExiste = False
    
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
    
        If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo _
        Or sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo) Then
            If sdbgCampos.Columns("VALOR").CellValue(vbm) <> "" And sdbgCampos.Row <> i Then
                bExiste = True
                '12010101 - PRUEBAS FUSION  'TipoCampoGS.DenArticulo
                Exit For
            End If
        End If
    Next
    
    ExisteAtributoArticulo = bExiste
    
End Function

Private Function DevuelveIdArticulo() As String
Dim i As Integer
Dim vbm As Variant
Dim tArray() As String
    
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo Then
            If sdbgCampos.Columns("VALOR").CellValue(vbm) <> "" And sdbgCampos.Row <> i Then
                '12010101 - PRUEBAS FUSION  'COD - DEN
                tArray = Split(sdbgCampos.Columns("VALOR").CellValue(vbm), "-")
                DevuelveIdArticulo = Trim(tArray(0))
                Exit For
            End If
        End If
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo Then
            If sdbgCampos.Columns("VALOR").CellValue(vbm) <> "" And sdbgCampos.Row <> i Then
                DevuelveIdArticulo = Trim(sdbgCampos.Columns("VALOR").CellValue(vbm))
                Exit For
            End If
        End If
    Next
End Function


'Funcion que dependiendo de la accion hara diferentes acciones sobre el campoGs Proveedor ERP
'Accion: 0: Elimina el valor del campo;1: Devuelve true si el campo existe en el formulario y false si no
'CampoGsOrigen: Indica si la accion es 0, que campo GS ha provocado que se tenga que borrar el proveedor ERP, si la accion es 1 indica que campo GS vinculado con el proveedor ERP se quiere eliminar
Private Function AccionSobreCampoGsProveedorERP(ByVal Accion As Byte, Optional ByVal CampoGsOrigen As TipoCampoGS) As Boolean
Dim vbm As Variant
Dim vbm_Old As Variant
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim i As Integer
Dim oCampo As CFormItem
Dim oSubCampo As CFormItem
Dim oLineaDesglose As CLineaDesglose
Dim bExisteOrgComprasEnDesglose As Boolean
Dim bExisteProveedorErpEnDesglose As Boolean
Dim rs As ADODB.Recordset
    
    vbm_Old = sdbgCampos.Bookmark
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.ProveedorERP Then
            
            sdbgCampos.Bookmark = vbm
            Select Case Accion
            Case 0 'Eliminamos el valor del campo
                sdbgCampos.Columns("VALOR").Value = ""
                sdbgCampos.Columns("COD_VALOR").Value = ""
                'Guardamos el campo
                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                m_oCampoEnEdicion.valorText = Null
                Set oIBaseDatos = m_oCampoEnEdicion
                teserror = oIBaseDatos.FinalizarEdicionModificando
                
                'Manejamos el posible error
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    m_bModError = True
                    basErrores.TratarError teserror
                    sdbgCampos.CancelUpdate
                    If Me.Visible Then sdbgCampos.SetFocus
                    g_Accion = ACCFormularioCons
                    sdbgCampos.DataChanged = False
                End If
                Set oIBaseDatos = Nothing
                Set m_oCampoEnEdicion = Nothing
                sdbgCampos.Bookmark = vbm_Old
                sdbgCampos.Update
            Case 1 'Devolvemos que el campo existe
                sdbgCampos.Bookmark = vbm_Old
                AccionSobreCampoGsProveedorERP = True
                Exit Function
            End Select
        End If
    Next
    Select Case Accion
        Case 0
            'Si se pretende eliminar el valor del campo proveedor Erp por haber modificado otro campo
            'habra que recorrer tambien los desgloses por si habria campos Proveedor ERP en ellos
            
            Select Case CampoGsOrigen
                Case TipoCampoGS.OrganizacionCompras
                    'Miramos los campos de ese formulario buscando campos de desglose
                    For Each oCampo In g_oGrupoSeleccionado.Campos
                        If oCampo.Tipo = TipoDesglose Then
                            'Recorremos una primera vez los campos del desglose porque solo se eliminara el valor
                            'del proveedor Erp cuando la organizacion de compras no este tambien en el desglose
                            oCampo.CargarDesglose
                            Set rs = oCampo.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse)
                            For Each oSubCampo In oCampo.Desglose
                                If oSubCampo.CampoGS = TipoCampoGS.OrganizacionCompras Then
                                    bExisteOrgComprasEnDesglose = True
                                ElseIf oSubCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                                    bExisteProveedorErpEnDesglose = True
                                End If
                            Next
                            If bExisteOrgComprasEnDesglose = False And bExisteProveedorErpEnDesglose = True Then
                                'Si la organizacion de compras no esta en el desglose borraremos el valor del campoGs Proveedor ERP en todas las lineas de desglose
                                For Each oLineaDesglose In oCampo.LineasDesglose
                                    If oLineaDesglose.CampoHijo.CampoGS = TipoCampoGS.ProveedorERP Then
                                        'Borramos el valor del campo Proveedor ERP
                                        oLineaDesglose.valorText = Null
                                        Set oIBaseDatos = oLineaDesglose
        
                                        teserror = oIBaseDatos.IniciarEdicion
                                        If teserror.NumError <> TESnoerror Then
                                            Screen.MousePointer = vbNormal
                                            TratarError teserror
                                            Set oIBaseDatos = Nothing
                                            Exit Function
                                        Else
                                            teserror = oIBaseDatos.FinalizarEdicionModificando
                                            If teserror.NumError = TESnoerror Then
                                                RegistrarAccion ACCLineaDesgloseModif, "Linea:" & oLineaDesglose.Linea & ",Campo=" & oLineaDesglose.CampoHijo.Id
                                            Else
                                                Screen.MousePointer = vbNormal
                                                TratarError teserror
                                                Set oIBaseDatos = Nothing
                                                Exit Function
                                            End If
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Next
            End Select
            sdbgCampos.Bookmark = vbm_Old
        Case 1
            'Se busca el campo Proveedor ERP en los desgloses
            If CampoGsOrigen = TipoCampoGS.Proveedor Then
                'Si se quiere eliminar un campo Proveedor(CampoGsOrigen = Proveedor), se tendra que mirar que no haya un campo Proveedor ERP,
                'En este CASE estariamos buscando el campo Proveedor ERP en los desgloses y como el campo Proveedor ERP tiene que estar en el mismo nivel que el campo proveedor ya sabriamos
                'que no esta al mismo nivel, asi que devolvemos False
                sdbgCampos.Bookmark = vbm_Old
                AccionSobreCampoGsProveedorERP = False
                Exit Function
            Else
                'Miramos los campos de ese formulario buscando campos de desglose
                For Each oCampo In g_oGrupoSeleccionado.Campos
                    If oCampo.Tipo = TipoDesglose Then
                        'Recorremos una primera vez los campos del desglose para ver si el campo Proveedor ERP esta
                        'en el desglose y esta solo, no esta con el campo Organizacion de compras
                        oCampo.CargarDesglose
                        Set rs = oCampo.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse)
                        For Each oSubCampo In oCampo.Desglose
                            If oSubCampo.CampoGS = TipoCampoGS.OrganizacionCompras Then
                                bExisteOrgComprasEnDesglose = True
                            ElseIf oSubCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                                bExisteProveedorErpEnDesglose = True
                            End If
                        Next
                        If bExisteOrgComprasEnDesglose = False And bExisteProveedorErpEnDesglose = True Then
                            'Si la organizacion de compras no esta en el desglose y si esta el proveedor ERP, devolveremos True
                            sdbgCampos.Bookmark = vbm_Old
                            AccionSobreCampoGsProveedorERP = True
                            Exit Function
                        End If
                    End If
                Next
                
                Dim g As CFormGrupo
                Dim c As CFormItem
                'Si el campo proveedor ERP no esta ni en el propio grupo a nivel de formulario, ni en los campos desglose de ese grupo,
                'se buscaria en los campos a nivel de formulario del resto de grupos y los campos de desglose
                If Not g_oGrupoSeleccionado.Formulario.Grupos Is Nothing Then
                    For Each g In g_oGrupoSeleccionado.Formulario.Grupos
                        If g.Campos Is Nothing Then
                            g.CargarTodosLosCampos
                        End If
                        
                        If Not g.Campos Is Nothing And Not g.Id = g_oGrupoSeleccionado.Id Then
                            For Each c In g.Campos
                                bExisteOrgComprasEnDesglose = False
                                bExisteProveedorErpEnDesglose = False
                                If c.Tipo = TipoDesglose Then
                                    'Recorremos una primera vez los campos del desglose para ver si el campo Proveedor ERP esta
                                    'en el desglose y esta solo, no esta con el campo Organizacion de compras
                                    c.CargarDesglose
                                    Set rs = c.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse)
                                    For Each oSubCampo In c.Desglose
                                        If oSubCampo.CampoGS = TipoCampoGS.OrganizacionCompras Then
                                            bExisteOrgComprasEnDesglose = True
                                        ElseIf oSubCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                                            bExisteProveedorErpEnDesglose = True
                                        End If
                                    Next
                                    If bExisteOrgComprasEnDesglose = False And bExisteProveedorErpEnDesglose = True Then
                                        'Si la organizacion de compras no esta en el desglose y si esta el proveedor ERP, devolveremos True
                                        sdbgCampos.Bookmark = vbm_Old
                                        AccionSobreCampoGsProveedorERP = True
                                        Exit Function
                                    End If
                                ElseIf c.CampoGS = TipoCampoGS.ProveedorERP Then
                                    'El campo Proveedor ERP esta a nivel de formulario
                                    sdbgCampos.Bookmark = vbm_Old
                                    AccionSobreCampoGsProveedorERP = True
                                    Exit Function
                                End If
                            Next
                        End If
                    Next
                End If
                
                
            End If
            sdbgCampos.Bookmark = vbm_Old
            AccionSobreCampoGsProveedorERP = False
            Exit Function
    End Select
    
End Function

Private Sub QuitarValorTablaExterna()
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim i As Integer
Dim vbm As Variant
Dim intTabla As Integer
    
    For i = 0 To sdbgCampos.Rows - 1
        vbm = sdbgCampos.AddItemBookmark(i)
        If sdbgCampos.Columns("TIPO").CellValue(vbm) = TipoCampoPredefinido.externo Then
            sdbgCampos.Bookmark = vbm
            intTabla = CInt(sdbgCampos.Columns("IDTABLAEXTERNA").Value)
            If m_oTablaExterna.SacarTablaTieneART(intTabla) Then
                sdbgCampos.Columns("VALOR").Value = ""
                '---------------------------------------------------------------------------------------------------------------------
                'hay que actualizar el campo
                Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
                m_oCampoEnEdicion.valorText = Null
                m_oCampoEnEdicion.valorBool = Null
                m_oCampoEnEdicion.valorFec = Null
                m_oCampoEnEdicion.valorNum = Null
                
                Set oIBaseDatos = m_oCampoEnEdicion
                teserror = oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    m_bModError = True
                    basErrores.TratarError teserror
                    sdbgCampos.CancelUpdate
                    If Me.Visible Then sdbgCampos.SetFocus
                    g_Accion = ACCFormularioCons
                    sdbgCampos.DataChanged = False
                End If
                Set oIBaseDatos = Nothing
                Set m_oCampoEnEdicion = Nothing
                sdbgCampos.Update
                'Exit For   'lo comento pq puede haber mas de una TABLA EXTERNA
                '---------------------------------------------------------------------------------------------------------------------
            End If
        End If
    Next
End Sub

''' <summary>
''' Cuando se va a borrar un campo art�culo se debe comprobar que los campos de qa relacionados con el art�culo
''' se borren. Esta funci�n dice si hay campos qa relacionados y cual/cuales es/son.
''' </summary>
''' <param name="bQaDos">Hay tanto Piezas Defectuosas como Importe Repercutido</param>
''' <param name="bQaUno">Hay Piezas Defectuosas � Importe Repercutido</param>
''' <param name="iTipo">3 Hay tanto Piezas Defectuosas como Importe Repercutido
'''     1 Hay Piezas Defectuosas    2 Hay Importe Repercutido</param>
'''     4 Hay envios correctos      5 Envios correctos y piezas defectuosas
'''     6 Importe repercutido y envios correctos
'''     7 Hay los 3 tipos (piezas, importe y envio)
''' <returns>S�/No hay campos qa relacionados. Los parametros bQaDos, bQaUno e iTipo son de salida</returns>
''' <remarks>Llamada desde:cmdElimItem_Click; Tiempo m�ximo:0</remarks>
Private Function HayCamposQARelacionados(ByRef bQaDos As Boolean, ByRef bQaUno As Boolean, ByRef iTipo As Integer, ByRef bHayFechaImputacionDefecto As Boolean) As Boolean
    Dim j As Integer
    Dim pos As Integer
    
    LockWindowUpdate Me.hWnd
    
    pos = Me.sdbgCampos.Row
    
    Me.sdbgCampos.MoveFirst
    For j = 0 To sdbgCampos.Rows - 1
        If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas Then
            If bQaUno Then
                bQaDos = True
                iTipo = 3
            Else
                bQaUno = True
                iTipo = 1
            End If
        ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.ImporteRepercutido Then
            If bQaUno Then
                bQaDos = True
                iTipo = 3
            Else
                bQaUno = True
                iTipo = 2
            End If
        ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.FechaImputacionDefecto Then
            bHayFechaImputacionDefecto = True
        End If
        
        Me.sdbgCampos.MoveNext
    Next
    
    Me.sdbgCampos.Row = pos
    LockWindowUpdate 0&

    If m_bConEnviosCorrectos Then
        If iTipo = 1 Then
            iTipo = 5
        ElseIf iTipo = 2 Then
            iTipo = 6
        ElseIf iTipo = 3 Then
            iTipo = 7
        Else
            iTipo = 4
        End If
    End If
    
    If bHayFechaImputacionDefecto Then
        Select Case iTipo
            Case 1
                iTipo = 8
            Case 2
                iTipo = 9
            Case 3
                iTipo = 10
            Case 4
                iTipo = 11
            Case 5
                iTipo = 12
            Case 6
                iTipo = 13
            Case 7
                iTipo = 14
            Case Else
                iTipo = 15
        End Select
    End If
    HayCamposQARelacionados = bQaDos Or bQaUno Or m_bConEnviosCorrectos Or bHayFechaImputacionDefecto
End Function
Public Sub MarcarEnviosCorrectos(ByVal bMarcar As Boolean)
    Dim iTipo As Integer
    iTipo = -1
    If bMarcar Then
        sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.EnviosCorrectos
        If sdbgCampos.Columns("TIPO").Value <> TipoCampoPredefinido.Calculado Then
            iTipo = NoConformidad
        End If
    Else
        sdbgCampos.Columns("CAMPO_GS").Value = Null
        If sdbgCampos.Columns("TIPO").Value <> TipoCampoPredefinido.Calculado Then
            iTipo = 0
        End If
    End If
    sdbgCampos.Update
    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
    
    m_oCampoEnEdicion.CampoGS = TipoCampoNoConformidad.EnviosCorrectos
    m_oCampoEnEdicion.ModificarTipoGS_CampoFSQA bMarcar, iTipo
    If iTipo > -1 Then
        g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = iTipo
    End If
    
    Set m_oCampoEnEdicion = Nothing
    m_bConEnviosCorrectos = bMarcar
    sdbgCampos.Refresh
End Sub
Public Sub marcarPiezasDefectuosas(ByVal bMarcar As Boolean)
Dim iTipo As Integer
    iTipo = -1
    
    If bMarcar Then
        sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.PiezasDefectuosas
        If sdbgCampos.Columns("TIPO").Value <> TipoCampoPredefinido.Calculado Then
            iTipo = NoConformidad
        End If
    Else
        sdbgCampos.Columns("CAMPO_GS").Value = Null
        If sdbgCampos.Columns("TIPO").Value <> TipoCampoPredefinido.Calculado Then
            iTipo = 0
        End If
    End If
    sdbgCampos.Update
    Set m_oCampoEnEdicion = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
    
    m_oCampoEnEdicion.CampoGS = TipoCampoNoConformidad.PiezasDefectuosas
        
    m_oCampoEnEdicion.ModificarTipoGS_CampoFSQA bMarcar, iTipo
    If iTipo > -1 Then
        g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = iTipo
    End If
    Set m_oCampoEnEdicion = Nothing
    m_bConPiezasDefectuosas = bMarcar
    sdbgCampos.Refresh
End Sub
Private Function ComprobarCentroDeCoste(IDCentroCoste As Long) As CFormItem
    Dim g As CFormGrupo
    Dim c As CFormItem
    Dim r As CFormItem
    
    If Not g_oFormSeleccionado Is Nothing Then
        For Each g In g_oFormSeleccionado.Grupos
            g.CargarTodosLosCampos
                        
            If Not g.Campos Is Nothing Then
                For Each c In g.Campos
                    If c.CampoGS = TipoCampoGS.Activo Or c.CampoGS = TipoCampoGS.PartidaPresupuestaria Then
                        If c.CentroCoste = IDCentroCoste Then
                            Set r = c
                            Exit For
                        End If
                    End If
                Next
                If Not r Is Nothing Then
                    Exit For
                End If
            End If
        Next
    End If
    Set ComprobarCentroDeCoste = r
End Function
Private Function ComprobarOrgCompras(Optional ByVal DevolverValor As Boolean) As Variant
    Dim g As CFormGrupo
    Dim c As CFormItem
    
    If Not g_oFormSeleccionado Is Nothing Then
        For Each g In g_oFormSeleccionado.Grupos
            g.CargarTodosLosCampos
            If Not g.Campos Is Nothing Then
                For Each c In g.Campos
                    If c.CampoGS = TipoCampoGS.OrganizacionCompras Then
                        If DevolverValor Then
                            ComprobarOrgCompras = c.valorText
                        Else
                            ComprobarOrgCompras = True
                        End If
                        Exit Function
                    End If
                Next
            End If
        Next
    End If
    ComprobarOrgCompras = False
End Function
Private Function HayCamposCalculadoRelacionados(ByVal lIdDesglose As Long) As String
    Dim oGrupo As CFormGrupo
    Dim oItem As CFormItem
    Dim oIdioma As CIdioma
    Dim res As String
    res = ""

    For Each oGrupo In g_oFormSeleccionado.Grupos
        If oGrupo.Campos Is Nothing Then oGrupo.CargarTodosLosCampos
        
        For Each oItem In oGrupo.Campos
            If oItem.TipoPredef = Calculado Then
                If oItem.OrigenCalcDesglose = lIdDesglose Then
                    If g_oFormSeleccionado.Multiidioma Then
                        For Each oIdioma In m_oIdiomas
                            res = res & IIf(res = "", "", ", ") & oGrupo.Denominaciones.Item(oIdioma.Cod).Den & "/"
                            res = res & oItem.Denominaciones.Item(oIdioma.Cod).Den
                        Next
                    Else
                        res = res & IIf(res = "", "", ", ") & oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & "/"
                        res = res & oItem.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                    End If
                End If
            End If
        Next
    Next

    HayCamposCalculadoRelacionados = res

End Function
Private Function validarFormato(ByVal tFormato As String, ByVal tValor As String) As Boolean
Dim rex As RegExp
Set rex = New RegExp

If (tFormato <> "") Then
    rex.Pattern = tFormato
    ' Set Case Insensitivity.
    rex.IgnoreCase = True
    'Set global applicability.
    rex.Global = True
    'Test whether the String can be compared.
    If (rex.Test(tValor) = True) Then
        validarFormato = True
    Else
        validarFormato = False
    End If
Else
    validarFormato = False
End If
End Function
Private Function CampoSeleccionado() As CFormItem
    Set CampoSeleccionado = g_oGrupoSeleccionado.Campos.Item(CStr(sdbgCampos.Columns("ID").Value))
End Function
