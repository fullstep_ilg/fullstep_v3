VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstMON 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de monedas (Opciones)"
   ClientHeight    =   2040
   ClientLeft      =   765
   ClientTop       =   1305
   ClientWidth     =   4815
   Icon            =   "frmLstMON.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2040
   ScaleWidth      =   4815
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   4815
      TabIndex        =   6
      Top             =   1665
      Width           =   4815
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   4
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1635
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   2884
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      Tab             =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Opciones"
      TabPicture(0)   =   "frmLstMON.frx":0CB2
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame3"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstMON.frx":0CCE
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame3 
         Height          =   1110
         Left            =   -74880
         TabIndex        =   8
         Top             =   360
         Width           =   4575
         Begin VB.CheckBox chkIncluirEqNoAct 
            Caption         =   "DIncluir monedas con equivalencia fija (zona Euro)"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   180
            TabIndex        =   0
            Top             =   465
            Width           =   4095
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1110
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   4575
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   1
            Top             =   300
            Value           =   -1  'True
            Width           =   1515
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   2
            Top             =   690
            Width           =   1515
         End
         Begin VB.CheckBox chkOrdenarEqNoAct 
            Caption         =   "DListar primero la zona Euro"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1980
            TabIndex        =   3
            Top             =   450
            Width           =   2415
         End
      End
   End
End
Attribute VB_Name = "frmLstMON"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables de idioma
Private sIdiSel1 As String
Private sIdiSel2 As String
Private txtTitulo As String
Private txtPag As String
Private txtDe As String
Private txtSeleccion As String
Private txtCod As String
Private txtDen As String
Private txtEqui As String
Private txtAct As String

Private m_oIdiomas As CIdiomas


Private Sub chkIncluirEqNoAct_Click()
If chkIncluirEqNoAct.Value = vbUnchecked Then
    chkOrdenarEqNoAct.Enabled = False
Else
    chkOrdenarEqNoAct.Enabled = True
End If
End Sub

Private Sub chkOrdenarEqNoAct_Click()
    'If chkOrdEqNoAct.Value = True Then
    '    chkIncluirEqNoAct.Value = True
    'End If
End Sub

''' <summary>
''' Listado de monedas
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Boton cmdObtener Tiempo m�ximo: 0,1</remarks>
Private Sub cmdObtener_Click()
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim SelectionText As String
    Dim oCRMonedas As CRMonedas
    Dim oIdi As CIdioma
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
   
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Set oCRMonedas = GenerarCRMonedas
    
   If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptMON.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
   
    If chkIncluirEqNoAct = 1 Then
        SelectionText = sIdiSel1
    Else
        SelectionText = sIdiSel2
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & txtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & txtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCod")).Text = """" & txtCod & """"
    For Each oIdi In m_oIdiomas
            If oIdi.Cod = gParametrosInstalacion.gIdioma Then
                oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDen")).Text = """" & txtDen & " (" & oIdi.Den & ")" & """"
                Exit For
            End If
    Next
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEqui")).Text = """" & txtEqui & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAct")).Text = """" & txtAct & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "IDI")).Text = """" & gParametrosInstalacion.gIdioma & """"

    'oCRMonedas.Listado oReport, chkIncluirEqNoAct, chkOrdenarEqNoAct, opOrdDen
    'If oReport Is Nothing Then
    '    Screen.MousePointer = vbNormal
    '    Exit Sub
    'End If
    
    Dim Ador As ADODB.Recordset
    Set Ador = oGestorInformes.ListadoMonedas(opOrdDen)
    If Ador Is Nothing Then
        Screen.MousePointer = vbNormal
        oMensajes.NoHayDatos
        Set oReport = Nothing
        Exit Sub
    Else
        oReport.Database.SetDataSource Ador
    End If
    
    
    

    Set pv = New Preview
    Me.Hide
    pv.Hide
    pv.caption = txtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    pv.Show
    Unload Me
    Screen.MousePointer = vbNormal

    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTMON, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        SSTab1.TabCaption(0) = Ador(0).Value      '1
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value     '2
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        chkIncluirEqNoAct.caption = Ador(0).Value
        Ador.MoveNext
        chkOrdenarEqNoAct.caption = Ador(0).Value '5
        Ador.MoveNext
        frmLstMON.caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        sIdiSel1 = Ador(0).Value
        Ador.MoveNext
        sIdiSel2 = Ador(0).Value '10
        'Idiomas del RPT
        Ador.MoveNext
        txtTitulo = Ador(0).Value '200
        Ador.MoveNext
        txtPag = Ador(0).Value
        Ador.MoveNext
        txtDe = Ador(0).Value
        Ador.MoveNext
        txtSeleccion = Ador(0).Value
        Ador.MoveNext
        txtCod = Ador(0).Value
        Ador.MoveNext
        txtDen = Ador(0).Value
        Ador.MoveNext
        txtEqui = Ador(0).Value
        Ador.MoveNext
        txtAct = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Load()

    Me.Width = 4905
    Me.Height = 2415
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    CargarRecursos
    
    If chkIncluirEqNoAct.Value = vbUnchecked Then
        chkOrdenarEqNoAct.Enabled = False
    End If
    
    chkOrdenarEqNoAct.Visible = False
    SSTab1.TabVisible(0) = False
    
End Sub

