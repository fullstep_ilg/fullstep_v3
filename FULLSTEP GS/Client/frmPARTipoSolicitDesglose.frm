VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPARTipoSolicitDesglose 
   Caption         =   "Form1"
   ClientHeight    =   5670
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9990
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPARTipoSolicitDesglose.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5670
   ScaleWidth      =   9990
   Begin VB.PictureBox picEdit 
      BorderStyle     =   0  'None
      Height          =   450
      Left            =   60
      ScaleHeight     =   450
      ScaleWidth      =   9000
      TabIndex        =   5
      Top             =   5250
      Width           =   9000
      Begin VB.CommandButton cmdCrearCampo 
         Caption         =   "DCrear campo nuevo"
         Height          =   345
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   1700
      End
      Begin VB.CommandButton cmdAnyaAtrib 
         Caption         =   "DA�adir atributo de GS"
         Height          =   345
         Left            =   1820
         TabIndex        =   2
         Top             =   0
         Width           =   2200
      End
      Begin VB.CommandButton cmdAnyadirCampoGS 
         Caption         =   "DA�adir campo del sistema"
         Height          =   345
         Left            =   4140
         TabIndex        =   3
         Top             =   0
         Width           =   2200
      End
      Begin VB.CommandButton cmdEliminarCampo 
         Caption         =   "DEliminar campo"
         Height          =   345
         Left            =   6460
         TabIndex        =   4
         Top             =   0
         Width           =   1700
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   5150
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9850
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   2
      stylesets.count =   3
      stylesets(0).Name=   "Calculado"
      stylesets(0).BackColor=   16766421
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPARTipoSolicitDesglose.frx":0CB2
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "Gris"
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPARTipoSolicitDesglose.frx":0D19
      stylesets(2).Name=   "Amarillo"
      stylesets(2).BackColor=   12648447
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmPARTipoSolicitDesglose.frx":0D35
      DividerType     =   0
      MultiLine       =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1588
      Columns(1).Caption=   "AYUDA"
      Columns(1).Name =   "AYUDA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   4
      Columns(1).ButtonsAlways=   -1  'True
      _ExtentX        =   17374
      _ExtentY        =   9084
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPARTipoSolicitDesglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_Accion As AccionesSummit
Public g_ofrmATRIB As frmAtrib
Public g_oCampoDesglose As CCampoPredef

Private m_oCampos As CCamposPredef
Private m_oIBaseDatos As IBaseDatos
Private m_oCampoEnEdicion As CCampoPredef

'Variables de idiomas:
Private m_sDato As String
Private m_sCamposGS(25) As String

Private Sub cmdAnyaAtrib_Click()
    'A�ade un campo de tipo atributo de GS
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If

    Set g_ofrmATRIB = New frmAtrib
    g_ofrmATRIB.g_sOrigen = "frmPARTipoSolicitDesglose"

    Screen.MousePointer = vbHourglass
    MDI.MostrarFormulario g_ofrmATRIB
    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' A�ade campos predefinidos del sistema
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAnyadirCampoGS_Click()
    Dim oArbolPres As cPresConceptos5Nivel0
    Dim ADORs As Ador.Recordset

    If Not basParametros.gParametrosGenerales.gbUsarPres1 Then
        MDI.mnuCamposGS.Item(11).Visible = False
    Else
        MDI.mnuCamposGS.Item(11).Visible = True
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres2 Then
        MDI.mnuCamposGS.Item(12).Visible = False
    Else
        MDI.mnuCamposGS.Item(12).Visible = True
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres3 Then
        MDI.mnuCamposGS.Item(13).Visible = False
    Else
        MDI.mnuCamposGS.Item(13).Visible = True
    End If
    
    If Not basParametros.gParametrosGenerales.gbUsarPres4 Then
        MDI.mnuCamposGS.Item(14).Visible = False
    Else
        MDI.mnuCamposGS.Item(14).Visible = True
    End If
    
    If gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.SolicitudPM) Or gParametrosIntegracion.gaExportar(EntidadIntegracion.SolicitudPM) Then
        MDI.mnuCamposGS.Item(16).Visible = True
    Else
        MDI.mnuCamposGS.Item(16).Visible = False
    End If
    
    If gParametrosGenerales.gbUsarOrgCompras Then
        MDI.mnuCamposGS.Item(19).Visible = True
        MDI.mnuCamposGS.Item(20).Visible = True
        
    Else
        MDI.mnuCamposGS.Item(19).Visible = False
        MDI.mnuCamposGS.Item(20).Visible = False
     End If
    
    MDI.mnuCamposGS(7).Visible = False
    
    MDI.mnuCamposGS(22).Visible = False 'Opcion de Menu Importe solicitudes vinculadas
    MDI.mnuCamposGS(23).Visible = False 'Opcion de Menu Referencia a solicitud
    MDI.mnuCamposGS(24).Visible = False 'Tipo de pedido
    
    MDI.mnuCamposGS(28).Visible = False 'no muestra el campo de GS de Desglose de factura
    MDI.mnuCamposGS(30).Visible = False 'no muestra el campo de GS de Inicio de abono
    MDI.mnuCamposGS(31).Visible = False 'no muestra el campo de GS de Fin de abono
    MDI.mnuCamposGS(32).Visible = False 'no muestra el campo de GS de Retenci�n en Garant�a
    MDI.mnuCamposGS(33).Visible = True 'no muestra el campo de GS de Unidad de pedido
    MDI.mnuCamposGS(35).Visible = False 'no muestra el campo de GS de Comprador
    
    If Not g_oParametrosSM Is Nothing Then
        'Habr� un campo de sistema nuevo por cada �rbol presupuestario que hayamos definido en la secci�n presupuestaria del SM.
        Set oArbolPres = oFSGSRaiz.Generar_CPresConceptos5Nivel0
        Set ADORs = oArbolPres.DevolverArbolesPresupuestarios
              
        Dim cont As Integer
        cont = MDI.mnuCamposGS.Count
        While cont >= 37
            Unload MDI.mnuCamposGS(cont)
            cont = cont - 1
        Wend
        
        If Not ADORs Is Nothing Then
            While Not ADORs.EOF
                Load MDI.mnuCamposGS(MDI.mnuCamposGS.Count + 1)
                MDI.mnuCamposGS(MDI.mnuCamposGS.Count).caption = NullToStr(ADORs("DEN").Value)
                MDI.mnuCamposGS(MDI.mnuCamposGS.Count).Tag = TipoCampoGS.PartidaPresupuestaria & "#" & ADORs("COD").Value
                ADORs.MoveNext
            Wend
            ADORs.Close
            Set ADORs = Nothing
        End If
    End If
    
    MDI.PopupMenu MDI.mnuPOPUPCamposGS
End Sub

Private Sub cmdCrearCampo_Click()
    'Crea un campo predefinido nuevo:

    g_Accion = ACCTipoSolicitudItemAnyadir

    frmFormAnyaCampos.g_sOrigen = "frmPARTipoSolicitDesglose"
    Set frmFormAnyaCampos.g_oIdiomas = frmPARTipoSolicit.m_oIdiomas
    frmFormAnyaCampos.Show vbModal

End Sub

Private Sub cmdEliminarCampo_Click()
Dim udtTeserror As TipoErrorSummit
Dim irespuesta As Integer
Dim vbm As Variant
Dim bEliminar2Campos As Boolean
Dim bEliminar3Campos As Boolean
Dim i As Integer
Dim aIdentificadores As Variant
Dim aBookmarks As Variant

'Elimina el campo predefinido

On Error GoTo Cancelar:

    If sdbgCampos.Rows = 0 Then Exit Sub
    If sdbgCampos.SelBookmarks.Count = 0 Then sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark

    g_Accion = ACCTipoSolicitudItemEliminar

    vbm = sdbgCampos.GetBookmark(1)
    If m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.Pais And m_oCampos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.provincia Then
        bEliminar2Campos = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.Pais)
    ElseIf m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.material And m_oCampos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.CodArticulo Then
        bEliminar2Campos = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.material)
    ElseIf m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.material And m_oCampos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.NuevoCodArticulo Then
        bEliminar3Campos = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.material)
    ElseIf m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.NuevoCodArticulo And m_oCampos.Item(CStr(sdbgCampos.Columns("ID").CellValue(vbm))).TipoCampoGS = TipoCampoGS.DenArticulo Then
        bEliminar2Campos = True
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.material)
    ElseIf m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoGS.DenArticulo Then
        bEliminar2Campos = True
        vbm = sdbgCampos.GetBookmark(-1)
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value, TipoCampoGS.DenArticulo)
    Else
        irespuesta = oMensajes.PreguntaEliminarCampo(sdbgCampos.Columns(gParametrosInstalacion.gIdioma).Value)
    End If

    If irespuesta = vbNo Then
        g_Accion = ACCTipoSolicitudCons
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    If bEliminar3Campos = True Then
        ReDim aIdentificadores(sdbgCampos.SelBookmarks.Count + 2)
        ReDim aBookmarks(sdbgCampos.SelBookmarks.Count + 2)
    ElseIf bEliminar2Campos Then
        ReDim aIdentificadores(sdbgCampos.SelBookmarks.Count + 1)
        ReDim aBookmarks(sdbgCampos.SelBookmarks.Count + 1)
    Else
        ReDim aIdentificadores(sdbgCampos.SelBookmarks.Count)
        ReDim aBookmarks(sdbgCampos.SelBookmarks.Count)
    End If
    
    
    i = 0
    While i < sdbgCampos.SelBookmarks.Count
        sdbgCampos.Bookmark = sdbgCampos.SelBookmarks(i)
        aIdentificadores(i + 1) = sdbgCampos.Columns("ID").Value
        aBookmarks(i + 1) = sdbgCampos.SelBookmarks(i)
        i = i + 1
    Wend
    
    If bEliminar2Campos = True Or bEliminar3Campos Then
        aIdentificadores(i + 1) = sdbgCampos.Columns("ID").CellValue(vbm)
        aBookmarks(i + 1) = vbm
        If bEliminar3Campos Then
            'Almacenamiento del codigo de la denominacion
            aIdentificadores(i + 2) = sdbgCampos.Columns("ID").CellValue(sdbgCampos.GetBookmark(2))
            aBookmarks(i + 2) = sdbgCampos.GetBookmark(2)
        End If
    End If

    udtTeserror = m_oCampos.EliminarCamposPredefDeBaseDatos(aIdentificadores)

    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError udtTeserror
    Else
        'Elimina los campos de la colecci�n:
        For i = 1 To UBound(aIdentificadores)
             m_oCampos.Remove (CStr(aIdentificadores(i)))
             basSeguridad.RegistrarAccion AccionesSummit.ACCTipoSolicitudItemEliminar, aIdentificadores(i)
        Next i

        'Elimina los campos de la grid:
        For i = 1 To UBound(aBookmarks)
            sdbgCampos.RemoveItem (sdbgCampos.AddItemRowIndex(aBookmarks(i)))
        Next i

        'Se posiciona en la fila correspondiente:
        If sdbgCampos.Rows > 0 Then
            If IsEmpty(sdbgCampos.RowBookmark(sdbgCampos.Row)) Then
                sdbgCampos.Bookmark = sdbgCampos.RowBookmark(sdbgCampos.Row - 1)
            Else
                sdbgCampos.Bookmark = sdbgCampos.RowBookmark(sdbgCampos.Row)
            End If
        End If

        sdbgCampos.SelBookmarks.RemoveAll
        If Me.Visible Then sdbgCampos.SetFocus

    End If

    g_Accion = ACCTipoSolicitudCons

    Screen.MousePointer = vbNormal

    Exit Sub

Cancelar:
    Screen.MousePointer = vbNormal
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub Form_Load()
    Dim oIdioma As CIdioma
    Dim i As Integer
    Dim iPosition As Integer

    Me.Height = 6180
    Me.Width = 10110

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
    
    PonerFieldSeparator Me

    'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n):
    i = sdbgCampos.Columns.Count
    iPosition = 1

    sdbgCampos.Columns.Add i
    sdbgCampos.Columns(i).Name = gParametrosInstalacion.gIdioma
    sdbgCampos.Columns(i).caption = m_sDato
    sdbgCampos.Columns(i).Style = ssStyleEditButton
    sdbgCampos.Columns(i).ButtonsAlways = True
    sdbgCampos.Columns(i).Position = iPosition
    sdbgCampos.Columns(i).Locked = False

    For Each oIdioma In frmPARTipoSolicit.m_oIdiomas
        If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
            i = i + 1
            iPosition = iPosition + 1
            sdbgCampos.Columns.Add i
            sdbgCampos.Columns(i).Name = oIdioma.Cod
            sdbgCampos.Columns(i).caption = oIdioma.Den
            sdbgCampos.Columns(i).Style = ssStyleEditButton
            sdbgCampos.Columns(i).ButtonsAlways = True
            sdbgCampos.Columns(i).Position = iPosition
            sdbgCampos.Columns(i).Visible = True
            sdbgCampos.Columns(i).Locked = False
        End If
    Next
    
    CargarCamposDesglose

    g_Accion = ACCTipoSolicitudCons
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        For i = 1 To 9
            m_sCamposGS(i) = Ador(0).Value
            Ador.MoveNext
        Next i

        Me.caption = Ador(0).Value & g_oCampoDesglose.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den  '10 Campo
        
        Ador.MoveNext
        m_sDato = Ador(0).Value '11 Dato
        Ador.MoveNext
        sdbgCampos.Columns("AYUDA").caption = Ador(0).Value '12 Ayuda
        Ador.MoveNext
        'sdbgCampos.Caption = Ador(0).Value '13 Campos
        Ador.MoveNext
        cmdCrearCampo.caption = Ador(0).Value  ' 14 Crear campo nuevo
        Ador.MoveNext
        cmdAnyaAtrib.caption = Ador(0).Value  '15 A�adir atributo de GS
        Ador.MoveNext
        cmdAnyadirCampoGS.caption = Ador(0).Value '16 A�adir campo del sistema
        Ador.MoveNext
        cmdEliminarCampo.caption = Ador(0).Value  '17 Eliminar campo

        Ador.MoveNext
        m_sCamposGS(14) = Ador(0).Value   '18 Persona
        Ador.MoveNext
        m_sCamposGS(15) = Ador(0).Value   '19 Num solicitud ERP
        Ador.MoveNext
        m_sCamposGS(16) = Ador(0).Value   '20 Denominaci�n de articulo
        Ador.MoveNext
        m_sCamposGS(17) = Ador(0).Value   '21 Unidad Organizativa
        Ador.MoveNext
        m_sCamposGS(18) = Ador(0).Value   '22 Departamento
        Ador.MoveNext
        m_sCamposGS(19) = Ador(0).Value   '23 Organizaci�n de compras
        Ador.MoveNext
        m_sCamposGS(20) = Ador(0).Value   '24 Centro
        Ador.MoveNext
        m_sCamposGS(21) = Ador(0).Value   '25 Almac�n
        Ador.MoveNext
        m_sCamposGS(22) = Ador(0).Value   '26 Centro
        Ador.MoveNext
        m_sCamposGS(23) = Ador(0).Value   '27 Almac�n
        Ador.MoveNext
        m_sCamposGS(24) = Ador(0).Value   '28 Factura
        Ador.MoveNext
        m_sCamposGS(25) = Ador(0).Value   '29 Unidad de pedido
        Ador.Close
    End If

    Set Ador = Nothing

    m_sCamposGS(10) = gParametrosGenerales.gsSingPres1
    m_sCamposGS(11) = gParametrosGenerales.gsSingPres2
    m_sCamposGS(12) = gParametrosGenerales.gsSingPres3
    m_sCamposGS(13) = gParametrosGenerales.gsSingPres4


End Sub

Private Sub Form_Resize()
Dim dblWGrid As Double
Dim oIdioma As CIdioma

    'Redimensiona el formulario

    If Me.Height < 2000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub

    sdbgCampos.Height = Me.Height - 1030
    sdbgCampos.Width = Me.Width - 315

    sdbgCampos.Columns("AYUDA").Width = sdbgCampos.Width / 12
    dblWGrid = sdbgCampos.Width - sdbgCampos.Columns("AYUDA").Width - 600

    If Not frmPARTipoSolicit.m_oIdiomas Is Nothing Then
        If sdbgCampos.Columns.Count > 2 Then
            For Each oIdioma In frmPARTipoSolicit.m_oIdiomas
                sdbgCampos.Columns(oIdioma.Cod).Width = dblWGrid / frmPARTipoSolicit.m_oIdiomas.Count
            Next
        End If
    End If

    picEdit.Top = sdbgCampos.Top + sdbgCampos.Height + 80
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Me.sdbgCampos.DataChanged = True Then Me.sdbgCampos.Update
    
    If Not g_ofrmATRIB Is Nothing Then
        Unload g_ofrmATRIB
        Set g_ofrmATRIB = Nothing
    End If

    Set m_oIBaseDatos = Nothing
    Set m_oCampos = Nothing
    Set m_oCampoEnEdicion = Nothing
    
    Me.Visible = False
End Sub

Public Sub AnyadirCampos(ByVal oCampos As CCamposPredef, Optional ByVal bCargarCol As Boolean = False)
    Dim oCampo As CCampoPredef
    Dim sCadena As String
    Dim oIdioma As CIdioma
    Dim oParametros As CLiterales

    If oCampos Is Nothing Then Exit Sub

    For Each oCampo In oCampos
      If oCampo.EsSubCampo = True Then
        If Not bCargarCol Then
            'Lo a�ade a la colecci�n:
            m_oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre, , oCampo.PRES5
        End If

        sCadena = oCampo.Id & Chr(m_lSeparador) & " "
        'Denominaci�n del campo en los diferentes idiomas:
        sCadena = sCadena & Chr(m_lSeparador) & oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den

        For Each oIdioma In frmPARTipoSolicit.m_oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                sCadena = sCadena & Chr(m_lSeparador) & oCampo.Denominaciones.Item(CStr(oIdioma.Cod)).Den
                Set oParametros = Nothing
            End If
        Next

        'a�ade la cadena a la grid:
        sdbgCampos.AddItem sCadena
     End If
    Next

End Sub


Private Sub sdbgCampos_BeforeUpdate(Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim oDenominaciones As CMultiidiomas
Dim oIdioma As CIdioma

    If m_oCampoEnEdicion Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    '******** REALIZA LAS COMPROBACIONES CORRESPONDIENTES ANTES DE GUARDAR ***************
    'La denominaci�n no puede ser nula (por lo menos la del idioma de la aplicaci�n)
    If Trim(sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).Value) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sdbgCampos.Columns(CStr(gParametrosInstalacion.gIdioma)).caption
        Cancel = True
        Exit Sub
    End If
    
    
    '************************ GUARDA EN BASE DE DATOS ***************************
    'Rellenamos la colecci�n con los nuevos valores dependiendo del tipo:
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In frmPARTipoSolicit.m_oIdiomas
        oDenominaciones.Add oIdioma.Cod, Trim(sdbgCampos.Columns(CStr(oIdioma.Cod)).Value)
    Next
    Set m_oCampoEnEdicion.Denominaciones = oDenominaciones
    
    Set oIBaseDatos = m_oCampoEnEdicion
    teserror = oIBaseDatos.FinalizarEdicionModificando
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        sdbgCampos.CancelUpdate
        If Me.Visible Then sdbgCampos.SetFocus
        sdbgCampos.DataChanged = False
    Else
        ''' Registro de acciones
        basSeguridad.RegistrarAccion AccionesSummit.ACCTipoSolicitudItemModif, "Id" & sdbgCampos.Columns("ID").Value
        Set oIBaseDatos = Nothing
        Set m_oCampoEnEdicion = Nothing
    End If
    
    g_Accion = ACCTipoSolicitudCons
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgCampos_BtnClick()
    If sdbgCampos.Columns(sdbgCampos.Col).Name = "AYUDA" Then
        'Muestra el formulario con la ayuda:
        MostrarFormFormCampoAyuda oGestorIdiomas, gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, oGestorSeguridad, oUsuarioSummit.Cod, gParametrosGenerales, _
                FormCampoAyudaTipoAyuda.CampoPredefinido, True, , frmPARTipoSolicit.m_oIdiomas, m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value))

    Else
        'Detalle de campo:
        If m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.CampoGS Or _
           m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.Certificado Or _
           m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef = TipoCampoPredefinido.NoConformidad Then
            'Es un campo predefinido de GS:
            frmDetalleCampoPredef.g_iTipo = m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).Tipo
            frmDetalleCampoPredef.g_sDenCampo = m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            frmDetalleCampoPredef.g_iTipoCampo = m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS
            frmDetalleCampoPredef.g_iTipoSolicit = m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoPredef
            frmDetalleCampoPredef.Show vbModal

        Else
            'Es un campo normal o un atributo de GS:
            frmDetalleCampos.g_sOrigen = "frmCamposSolic"
            frmDetalleCampos.g_bModif = False
            Set frmDetalleCampos.g_oIdiomas = frmPARTipoSolicit.m_oIdiomas
            Set frmDetalleCampos.g_oCampoPredef = m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value))
            frmDetalleCampos.Show vbModal
        End If
    End If
End Sub


Public Sub AnyadirCampoDeGS(ByVal iTipoCampo As Integer, Optional ByVal sPRES5 As String)
Dim oCampo As CCampoPredef
Dim teserror As TipoErrorSummit
Dim oCampos As CCamposPredef
Dim bVariosArt As Boolean

    bVariosArt = True

    Set oCampos = oFSGSRaiz.Generar_CCamposPredef

    If iTipoCampo = TipoCampoGS.provincia Then  'Si insertamos la provincia inserta tambi�n el pa�s:
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.Pais)
        oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre

        Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
        oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre

    ElseIf iTipoCampo = TipoCampoGS.CodArticulo Then 'Si insertamos el art�culo inserta tambi�n el material:
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.material)
        oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre

        Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
        oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre
      
    ElseIf iTipoCampo = TipoCampoGS.NuevoCodArticulo Then 'Si insertamos el art�culo inserta tambi�n el material:
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.material)
        oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre

        Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
        oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre
        
        Set oCampo = GenerarEstructuraNuevoCampo(TipoCampoGS.DenArticulo)
        oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre
    ElseIf iTipoCampo = TipoCampoGS.PartidaPresupuestaria Then
       
        Set oCampo = GenerarEstructuraArbolPresupuestario(iTipoCampo, sPRES5)
        oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre, , oCampo.PRES5
    
    Else
        bVariosArt = False
        Set oCampo = GenerarEstructuraNuevoCampo(iTipoCampo)
    End If

    'Inserta en BD:
    If bVariosArt = True Then
        teserror = oCampos.AnyadirCamposPredefABaseDatos
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
        Else
            'a�ade el nuevo campo a la colecci�n y a la grid:
            AnyadirCampos oCampos
            For Each oCampo In oCampos
                RegistrarAccion ACCTipoSolicitudItemAnyadir, "Id:" & oCampo.Id
            Next
        End If

    Else
        'Inserta el campo en BD:
        Set m_oIBaseDatos = oCampo

        teserror = m_oIBaseDatos.AnyadirABaseDatos

        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
        Else
            'a�ade el nuevo campo a la colecci�n y a la grid:
            oCampos.Add oCampo.Id, g_oCampoDesglose.TipoSolicitud, oCampo.Denominaciones, oCampo.Ayudas, oCampo.TipoPredef, oCampo.Tipo, oCampo.TipoIntroduccion, oCampo.MinNum, oCampo.MaxNum, oCampo.MinFec, oCampo.MaxFec, oCampo.TipoCampoGS, oCampo.idAtrib, oCampo.Orden, oCampo.EsSubCampo, oCampo.FECACT, oCampo.CampoPadre
            AnyadirCampos oCampos
            RegistrarAccion ACCTipoSolicitudItemAnyadir, "Id:" & oCampo.Id
        End If

        Set m_oIBaseDatos = Nothing
    End If

    Set oCampos = Nothing
    Set oCampo = Nothing

    g_Accion = ACCTipoSolicitudCons
End Sub

Private Function GenerarEstructuraNuevoCampo(ByVal iTipoCampo) As CCampoPredef
Dim oCampo As CCampoPredef
Dim oIdioma As CIdioma
Dim Ador As Ador.Recordset
Dim oParametros As CLiterales

    Set oCampo = oFSGSRaiz.Generar_CCampoPredef

    oCampo.Id = iTipoCampo
    oCampo.EsSubCampo = True
    oCampo.idAtrib = Null
    oCampo.MaxFec = Null
    oCampo.MinFec = Null
    oCampo.MaxNum = Null
    oCampo.MinNum = Null

    oCampo.TipoIntroduccion = TAtributoIntroduccion.IntroLibre
    oCampo.TipoPredef = TipoCampoPredefinido.CampoGS
    oCampo.TipoSolicitud = g_oCampoDesglose.TipoSolicitud

    oCampo.TipoCampoGS = iTipoCampo

    Set oCampo.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas

    For Each oIdioma In frmPARTipoSolicit.m_oIdiomas
        If oIdioma.Cod = gParametrosInstalacion.gIdioma Then
            Select Case iTipoCampo
                Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(5)
                Case TipoCampoGS.Dest
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(9)
                Case TipoCampoGS.FormaPago
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(2)
                Case TipoCampoGS.material
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(4)
                Case TipoCampoGS.Moneda
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(3)
                Case TipoCampoGS.Pais
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(7)
                Case TipoCampoGS.Proveedor
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(1)
                Case TipoCampoGS.provincia
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(8)
                Case TipoCampoGS.Unidad
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(6)
                Case TipoCampoGS.PRES1
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(10)
                Case TipoCampoGS.PRES2
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(11)
                Case TipoCampoGS.Pres3
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(12)
                Case TipoCampoGS.Pres4
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(13)
                Case TipoCampoGS.CampoPersona
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(14)
                Case TipoCampoGS.NumSolicitERP
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(15)
                Case TipoCampoGS.DenArticulo
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(16)
                Case TipoCampoGS.UnidadOrganizativa
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(17)
                Case TipoCampoGS.Departamento
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(18)
                Case TipoCampoGS.OrganizacionCompras
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(19)
                Case TipoCampoGS.Centro
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(20)
                Case TipoCampoGS.Almacen
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(21)
                Case TipoCampoGS.CentroCoste
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(22)
                Case TipoCampoGS.Activo
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(23)
                Case TipoCampoGS.Factura
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(24)
                Case TipoCampoGS.UnidadPedido
                    oCampo.Denominaciones.Add oIdioma.Cod, m_sCamposGS(25)
            End Select
        
        Else
            Select Case iTipoCampo
                Case TipoCampoGS.PRES1
                    Set oParametros = oGestorParametros.DevolverLiterales(20, 20, oIdioma.Cod)
                    oCampo.Denominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case TipoCampoGS.PRES2
                    Set oParametros = oGestorParametros.DevolverLiterales(21, 21, oIdioma.Cod)
                    oCampo.Denominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case TipoCampoGS.Pres3
                    Set oParametros = oGestorParametros.DevolverLiterales(27, 27, oIdioma.Cod)
                    oCampo.Denominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case TipoCampoGS.Pres4
                    Set oParametros = oGestorParametros.DevolverLiterales(28, 28, oIdioma.Cod)
                    oCampo.Denominaciones.Add oIdioma.Cod, oParametros.Item(1).Den
                Case Else
                    Select Case iTipoCampo
                        Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 5)
                        Case TipoCampoGS.Dest
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 9)
                        Case TipoCampoGS.FormaPago
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 2)
                        Case TipoCampoGS.material
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 4)
                        Case TipoCampoGS.Moneda
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 3)
                        Case TipoCampoGS.Pais
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 7)
                        Case TipoCampoGS.Proveedor
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 1)
                        Case TipoCampoGS.provincia
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 8)
                        Case TipoCampoGS.Unidad
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 6)
                        Case TipoCampoGS.CampoPersona
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 18)
                        Case TipoCampoGS.NumSolicitERP
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 19)
                        Case TipoCampoGS.DenArticulo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 20)
                        Case TipoCampoGS.UnidadOrganizativa
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 21)
                        Case TipoCampoGS.Departamento
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 22)
                        Case TipoCampoGS.OrganizacionCompras
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 23)
                        Case TipoCampoGS.Centro
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 24)
                        Case TipoCampoGS.Almacen
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 25)
                        Case TipoCampoGS.CentroCoste
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 26)
                        Case TipoCampoGS.Activo
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 27)
                        Case TipoCampoGS.Factura
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 28)
                        Case TipoCampoGS.UnidadPedido
                            Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TIPO_SOLICIT_DESGLOSE, oIdioma.Cod, 29)
                    End Select
                    oCampo.Denominaciones.Add oIdioma.Cod, Ador(0).Value
                    Ador.Close
            End Select
            Set oParametros = Nothing
        End If
    Next
    Set Ador = Nothing

    Select Case iTipoCampo
        Case TipoCampoSC.importe, TipoCampoSC.Cantidad, TipoCampoSC.PrecioUnitario, TipoCampoGS.PRES1, TipoCampoGS.PRES2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
            oCampo.Tipo = TiposDeAtributos.TipoNumerico

        Case TipoCampoSC.FecNecesidad, TipoCampoSC.FinSuministro, TipoCampoSC.IniSuministro
            oCampo.Tipo = TiposDeAtributos.TipoFecha

        Case TipoCampoSC.DescrDetallada
            oCampo.Tipo = TiposDeAtributos.TipoTextoLargo

        Case TipoCampoSC.ArchivoEspecific
            oCampo.Tipo = TiposDeAtributos.TipoArchivo

        Case TipoCampoGS.Desglose
            oCampo.Tipo = TiposDeAtributos.TipoDesglose

        Case Else
            oCampo.Tipo = TiposDeAtributos.TipoTextoMedio

    End Select

    Set oCampo.CampoPadre = g_oCampoDesglose
    
    Set GenerarEstructuraNuevoCampo = oCampo
End Function

Private Function GenerarEstructuraArbolPresupuestario(ByVal iTipoCampo As Integer, ByVal sPRES5 As String) As CCampoPredef
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim oCampo As CCampoPredef
    Dim oPres5Niv0 As cPresConceptos5Nivel0
    Dim Adores As ADODB.Recordset
           
    Set oCampo = oFSGSRaiz.Generar_CCampoPredef
    
    oCampo.Id = iTipoCampo
    oCampo.EsSubCampo = True
    oCampo.idAtrib = Null
    oCampo.MaxFec = Null
    oCampo.MinFec = Null
    oCampo.MaxNum = Null
    oCampo.MinNum = Null

    oCampo.TipoIntroduccion = TAtributoIntroduccion.IntroLibre
    oCampo.TipoPredef = TipoCampoPredefinido.CampoGS
    oCampo.TipoSolicitud = g_oCampoDesglose.TipoSolicitud

    oCampo.TipoCampoGS = iTipoCampo
    oCampo.Tipo = TiposDeAtributos.TipoTextoMedio
    oCampo.PRES5 = sPRES5
        
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    Set oPres5Niv0 = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    For Each oIdioma In frmPARTipoSolicit.m_oIdiomas
        Set Adores = oPres5Niv0.DevolverDenominacion(oIdioma.Cod, sPRES5)
        oDenominaciones.Add oIdioma.Cod, Adores(0).Value
    Next
    
    Set oCampo.Denominaciones = oDenominaciones
    
    Set oCampo.CampoPadre = g_oCampoDesglose
    Set GenerarEstructuraArbolPresupuestario = oCampo
End Function


Private Sub CargarCamposDesglose()
    If g_oCampoDesglose Is Nothing Then Exit Sub

    'Carga los campos correspondientes al tipo seleccionado:
    Set m_oCampos = oFSGSRaiz.Generar_CCamposPredef
    m_oCampos.CargarTodosLosCamposDesglose (g_oCampoDesglose.Id)

    sdbgCampos.RemoveAll
    AnyadirCampos m_oCampos, True
    sdbgCampos.MoveFirst

End Sub

Private Sub sdbgCampos_Change()
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    If m_oCampos Is Nothing Then Exit Sub
    If m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)) Is Nothing Then Exit Sub
    
    DoEvents
    
    Set m_oCampoEnEdicion = Nothing

    Set m_oCampoEnEdicion = m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value))
        
    g_Accion = ACCTipoSolicitudItemModif
End Sub

Private Sub sdbgCampos_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then

        If sdbgCampos.DataChanged = False Then
            sdbgCampos.CancelUpdate
            sdbgCampos.DataChanged = False
            
            Set m_oCampoEnEdicion = Nothing
            
            g_Accion = ACCTipoSolicitudCons
        End If
    End If
End Sub

Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    Select Case frmPARTipoSolicit.g_oTipoSeleccionado.Tipo
        Case TipoSolicitud.SolicitudCompras
            If m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.DescrBreve Or m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.DescrDetallada Or _
               m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.importe Or m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.Cantidad Or _
               m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.FecNecesidad Or m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.IniSuministro Or _
               m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.FinSuministro Or m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.ArchivoEspecific Or _
               m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoSC.PrecioUnitario Then
                cmdEliminarCampo.Enabled = False
            Else
                cmdEliminarCampo.Enabled = True
            End If
        Case TipoSolicitud.NoConformidades
            If m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.Accion Or _
               m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.Fec_inicio Or m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.Fec_cierre Or _
               m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.responsable Or m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.Observaciones Or _
               m_oCampos.Item(CStr(sdbgCampos.Columns("ID").Value)).TipoCampoGS = TipoCampoNoConformidad.Documentaci�n Then
                cmdEliminarCampo.Enabled = False
            Else
                cmdEliminarCampo.Enabled = True
            End If
        
        Case Else
            cmdEliminarCampo.Enabled = True
    End Select
End Sub
