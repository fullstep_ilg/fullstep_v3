VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCATDatExtSolicitar 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DSolicitud de datos al proveedor"
   ClientHeight    =   4950
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8940
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATDatExtSolicitar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4950
   ScaleWidth      =   8940
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   4485
      TabIndex        =   8
      Top             =   4560
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   3360
      TabIndex        =   7
      Top             =   4560
      Width           =   1005
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00808000&
      Height          =   1215
      Left            =   60
      TabIndex        =   6
      Top             =   3220
      Width           =   8775
      Begin SSDataWidgets_B.SSDBCombo sdbcContacto 
         Height          =   285
         Left            =   2400
         TabIndex        =   3
         Top             =   360
         Width           =   4095
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         AutoRestore     =   0   'False
         ListWidth       =   7250
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   9
         Columns(0).Width=   7223
         Columns(0).Caption=   "Contacto"
         Columns(0).Name =   "CONTACTO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "APROV"
         Columns(1).Name =   "APROV"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "EMAIL"
         Columns(3).Name =   "EMAIL"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "TFNO"
         Columns(4).Name =   "TFNO"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "MOVIL"
         Columns(5).Name =   "MOVIL"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "FAX"
         Columns(6).Name =   "FAX"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "NOMBRE"
         Columns(7).Name =   "NOMBRE"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "APE"
         Columns(8).Name =   "APE"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   7223
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.TextBox txtEmail 
         Height          =   285
         Left            =   2400
         TabIndex        =   4
         Top             =   720
         Width           =   4095
      End
      Begin VB.Label lblEmail 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DEmail:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   780
         Width           =   525
      End
      Begin VB.Label lblContacto 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DContacto:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   400
         Width           =   825
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      Height          =   1215
      Left            =   60
      TabIndex        =   5
      Top             =   2000
      Width           =   8775
      Begin VB.CommandButton cmdExcel 
         Height          =   285
         Left            =   8160
         Picture         =   "frmCATDatExtSolicitar.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Examinar"
         Top             =   720
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.CommandButton cmdDOT 
         Height          =   285
         Left            =   8160
         Picture         =   "frmCATDatExtSolicitar.frx":0D71
         Style           =   1  'Graphical
         TabIndex        =   21
         TabStop         =   0   'False
         ToolTipText     =   "Examinar"
         Top             =   360
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.TextBox txtExcel 
         Height          =   285
         Left            =   2400
         TabIndex        =   2
         Top             =   720
         Width           =   5655
      End
      Begin VB.TextBox txtPlantilla 
         Height          =   285
         Left            =   2400
         TabIndex        =   1
         Top             =   360
         Width           =   5655
      End
      Begin VB.Label lblExcel 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "Recolecci�n de datos en Excel:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   780
         Width           =   2220
      End
      Begin VB.Label lblPlantilla 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DPlantilla:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   400
         Width           =   705
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   1935
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8775
      Begin VB.TextBox txtSolicitadoPor 
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   1440
         Width           =   5655
      End
      Begin VB.TextBox txtUltSol 
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   360
         Width           =   1935
      End
      Begin VB.TextBox txtSolicitadoA 
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   1080
         Width           =   5655
      End
      Begin VB.TextBox txtUltAct 
         BackColor       =   &H00C0FFFF&
         Height          =   285
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   720
         Width           =   1935
      End
      Begin VB.Label lblSolicitadoPor 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DSolicitado por:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   1520
         Width           =   1125
      End
      Begin VB.Label lblSolicitadoA 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "DSolicitado a :"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   1160
         Width           =   1020
      End
      Begin VB.Label lblUltAct 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "D�ltima actualizaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   780
         Width           =   1560
      End
      Begin VB.Label lblUltSolicitud 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "D�ltima solicitud:"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   400
         Width           =   1215
      End
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmCATDatExtSolicitar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const NUMCOLS = 4
Private Const GENERALES = 1
Private Const ARTICULOS = 2
Private Const ATRIBUTOS = 3
Private Const Seleccion = 4
Private Const TAM_ATRIBUTOS = 8

Public oArticulos As CArticulos
Public sProve As String
Public sProveDen As String
Public sGmn1 As String
Public sGmn2 As String
Public sGmn3 As String
Public sGmn4 As String

'Idiomas
Private sPlantillas As String
Private sExcel As String
Private sPlantillaDot As String
Private sFicha As String
Private sArticulo As String
Private sImagen As String
Private sGenerarExcel As String
Private sGenerarWord As String
Private sGenerarMail As String
Private sRellenarProve As String
Private sRellenarPer As String
Private sGenerarHoja As String
Private sImagenInsertar As String
Private sImagenEliminar As String
Private sImagenVer As String
Private sMensajeImage As String
Private sCodExt As String
Private sLeyenda As String
Private sBool As String
Private sString As String
Private sFecha As String
Private sNumerico As String
Private sTipo As String
Private sMensajeFormatoBoolean As String
Private sMensajeFormatoFecha As String
Private sMensajeFormatoNumerico As String
Private sMensajeMaximaLen100 As String
Private sproveedor As String
Private sFicheros As String
Private sFrase As String
Private sInsertar As String
Private sEliminar As String
Private sVer As String
Private sMaximo As String
Private sMinimo As String
Private sCelda As String
Private sTextoMayor100c As String
Private sTextoMayor200c As String
Private sValNum_FR As String
Private sValFec_FR As String
Private sListaSeleccion As String
Private sTrue As String
Private sFalse As String
Private oProveedor As CProveedor
Private bRespetarCombo As Boolean

Private oFos As Scripting.FileSystemObject

Private Sub cmdAceptar_Click()
    Dim sContacto As String
    Dim bMailGenerado As Boolean
    Dim sIdioma As String
    
    If txtEmail = "" Then
        oMensajes.FaltaEMail
        Exit Sub
    End If
    
    If txtPlantilla.Text = "" Then
        oMensajes.FaltaPlantilla
        Exit Sub
    Else
        If Right(txtPlantilla, 3) <> "dot" Then
            oMensajes.NoValido sPlantillaDot
            If Me.Visible Then cmdDOT.SetFocus
            Exit Sub
        End If
    End If
    If Not oFos.FileExists(txtPlantilla) Then
        oMensajes.PlantillaNoEncontrada txtPlantilla
        If Me.Visible Then cmdDOT.SetFocus
        Exit Sub
    End If
    
    If txtExcel.Text = "" Then
        oMensajes.FaltaExcel
        Exit Sub
    Else
        If Right(txtExcel, 3) <> "xls" Then
            oMensajes.NoValido sExcel
            If Me.Visible Then cmdExcel.SetFocus
            Exit Sub
        End If
    End If
    If Not oFos.FileExists(txtExcel) Then
        oMensajes.PlantillaNoEncontrada txtExcel
        If Me.Visible Then cmdExcel.SetFocus
        Exit Sub
    End If
    
    'Obtenemos el idioma del proveedor
    sIdioma = oProveedor.DevolverIdiomaProve
    'Si el idioma del proveedor es distinto al de la instalaci�n carga los idiomas para la
    'hoja excel
    If sIdioma <> basPublic.gParametrosInstalacion.gIdioma And sIdioma <> "" Then
        CargarIdiomaProve (sIdioma)
    End If
    
    'Comprobamos que las plantillas  de word son v�lidas
    If gParametrosGenerales.giMail = 0 Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    '1.Se genera un e-mail con la plantilla word   y
    '2.Se genera la hoja excel con la plantilla y se incluye como adjunto en el mail.
    bMailGenerado = GenerarMail

    '3.Realiza la solicitud de actualizaci�n de datos al proveedor
    If bMailGenerado = True Then
        sContacto = sdbcContacto.Columns("NOMBRE").Value & " " & sdbcContacto.Columns("APE").Value
        If oUsuarioSummit.Persona Is Nothing Then
            oProveedor.AnyadirSolicitudOActualizacion 0, , sContacto
        Else
            oProveedor.AnyadirSolicitudOActualizacion 0, oUsuarioSummit.Persona.Cod, sContacto
        End If
        
        basSeguridad.RegistrarAccion accionessummit.ACCArticDatExtSolicitar, "Proveedor:" & oProveedor.Cod
    End If
    
    Screen.MousePointer = vbNormal
    
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Set oArticulos = Nothing
    Set oProveedor = Nothing
    Unload Me
End Sub

Private Sub cmdDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sPlantillas & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtPlantilla.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub

Private Sub cmdExcel_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sPlantillas & " (*.xls)|*.xls"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtExcel.Text = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub

Private Sub Form_Load()
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    'Carga del hist�rico de solicitudes y actualizaciones de datos externos
    Set oProveedor = oFSGSRaiz.generar_CProveedor
    oProveedor.Cod = sProve
    oProveedor.Den = sProveDen
    
    Set oFos = New Scripting.FileSystemObject
    
    If gParametrosGenerales.giMail = 0 Then
        txtExcel.Enabled = False
        txtPlantilla.Enabled = False
        cmdDOT.Enabled = False
        cmdExcel.Enabled = False
    End If
    
    CargarDatos
    
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_SOLIC, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.caption = ador(0).Value
        ador.MoveNext
        Me.lblUltSolicitud.caption = ador(0).Value
        ador.MoveNext
        Me.lblUltAct.caption = ador(0).Value
        ador.MoveNext
        Me.lblSolicitadoA.caption = ador(0).Value
        ador.MoveNext
        Me.lblSolicitadoPor.caption = ador(0).Value
        ador.MoveNext
        Me.lblPlantilla.caption = ador(0).Value
        sPlantillaDot = ador(0).Value
        ador.MoveNext
        Me.lblExcel.caption = ador(0).Value
        sExcel = ador(0).Value
        ador.MoveNext
        Me.lblContacto.caption = ador(0).Value
        ador.MoveNext
        Me.lblEmail.caption = ador(0).Value
        ador.MoveNext
        cmdAceptar.caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.caption = ador(0).Value
        ador.MoveNext
        sPlantillas = ador(0).Value
        ador.MoveNext
        sdbcContacto.Columns(0).caption = ador(0).Value
        ador.MoveNext
        sArticulo = ador(0).Value
        ador.MoveNext
        sFicha = ador(0).Value
        ador.MoveNext
        sImagen = ador(0).Value
        ador.MoveNext
        sGenerarMail = ador(0).Value
        ador.MoveNext
        sGenerarWord = ador(0).Value
        ador.MoveNext
        sGenerarExcel = ador(0).Value
        ador.MoveNext
        sRellenarProve = ador(0).Value
        ador.MoveNext
        sRellenarPer = ador(0).Value
        ador.MoveNext
        sGenerarHoja = ador(0).Value
        ador.MoveNext
        sImagenInsertar = ador(0).Value
        ador.MoveNext
        sImagenEliminar = ador(0).Value
        ador.MoveNext
        sImagenVer = ador(0).Value
        ador.MoveNext
        sMensajeImage = ador(0).Value
        ador.MoveNext
        sCodExt = ador(0).Value
        ador.MoveNext
        sLeyenda = ador(0).Value
        ador.MoveNext
        sString = ador(0).Value
        ador.MoveNext
        sNumerico = ador(0).Value
        ador.MoveNext
        sFecha = ador(0).Value
        ador.MoveNext
        sBool = ador(0).Value
        ador.MoveNext
        sTipo = ador(0).Value
        ador.MoveNext
        sMensajeFormatoBoolean = ador(0).Value
        ador.MoveNext
        sMensajeFormatoFecha = ador(0).Value
        ador.MoveNext
        sMensajeFormatoNumerico = ador(0).Value
        ador.MoveNext
        sproveedor = ador(0).Value
        ador.MoveNext
        sFicheros = ador(0).Value
        ador.MoveNext
        sMensajeMaximaLen100 = ador(0).Value
        ador.MoveNext
        sFrase = ador(0).Value
        ador.MoveNext
        sInsertar = ador(0).Value
        ador.MoveNext
        sEliminar = ador(0).Value
        ador.MoveNext
        sVer = ador(0).Value
        ador.MoveNext
        sMaximo = ador(0).Value
        ador.MoveNext
        sMinimo = ador(0).Value
        ador.MoveNext
        sCelda = ador(0).Value
        ador.MoveNext
        sTextoMayor100c = ador(0).Value
        ador.MoveNext
        sTextoMayor200c = ador(0).Value
        ador.MoveNext
        sValNum_FR = ador(0).Value
        ador.MoveNext
        sValFec_FR = ador(0).Value
        ador.MoveNext
        sListaSeleccion = ador(0).Value
        ador.MoveNext
        sTrue = ador(0).Value
        ador.MoveNext
        sFalse = ador(0).Value
        ador.Close
    End If
    
    Set ador = Nothing
End Sub

Private Sub CargarDatos()
    Dim RS As ador.Recordset
    
    'Carga los datos del hist�rico de solicitudes y actualizaciones del proveedor
    Set RS = oProveedor.LeerUltimaSolicitudProveedor
    If Not RS Is Nothing Then
        txtUltSol.Text = RS("FECHA").Value
        txtSolicitadoA.Text = RS("NOM_CON").Value
        txtSolicitadoPor.Text = RS("PER").Value
        RS.Close
    End If
    Set RS = Nothing
    
    Set RS = oProveedor.LeerUltimaActualizacionProveedor
    If Not RS Is Nothing Then
        txtUltAct.Text = NullToStr(RS("FECHA").Value)
        RS.Close
    End If
    Set RS = Nothing
    
    
    'Carga las plantillas
    If gParametrosInstalacion.gsDatosExternosMail = "" Then
        txtPlantilla.Text = gParametrosGenerales.gsDATEXTMAILDOT
    Else
        txtPlantilla.Text = gParametrosInstalacion.gsDatosExternosMail
    End If
    
    If gParametrosInstalacion.gsDatosExternosExcel = "" Then
         txtExcel.Text = gParametrosGenerales.gsDATEXTXLS
    Else
         txtExcel.Text = gParametrosInstalacion.gsDatosExternosExcel
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oProveedor = Nothing
    Set oArticulos = Nothing
    
End Sub

Private Sub sdbcContacto_Click()
    If Not sdbcContacto.DroppedDown Then
        sdbcContacto = ""
        txtEmail.Text = ""
    End If
End Sub

Private Sub sdbcContacto_CloseUp()
    If sdbcContacto.Value = "" Then
        sdbcContacto.Text = ""
        Exit Sub
    End If
    
    If sdbcContacto.Text = "" Then
        txtEmail = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcContacto.Text = sdbcContacto.Columns(0).Text
    bRespetarCombo = False
    
    DoEvents
    
    txtEmail = sdbcContacto.Columns("EMAIL").Text
    
End Sub

Private Sub sdbcContacto_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim oCon As CContacto
        
    'Carga los contactos
    Screen.MousePointer = vbHourglass
    
    sdbcContacto.RemoveAll
    
    oProveedor.CargarTodosLosContactos , , , , True

    For Each oCon In oProveedor.Contactos
        sdbcContacto.AddItem oCon.Apellidos & " , " & oCon.nombre & Chr(m_lSeparador) & oCon.Aprovisionador & Chr(m_lSeparador) & oCon.Id & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Apellidos
    Next
    
    sdbcContacto.SelStart = 0
    sdbcContacto.SelLength = Len(sdbcContacto.Text)
    sdbcContacto.Refresh
    
    Screen.MousePointer = vbNormal
    
    
End Sub

Private Sub sdbcContacto_InitColumnProps()
    sdbcContacto.DataFieldList = "Column 0"
    sdbcContacto.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcContacto_PositionList(ByVal Text As String)
Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcContacto.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcContacto.Rows - 1
            bm = sdbcContacto.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcContacto.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcContacto.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub



Private Function GenerarPlantillaMail(blankword As Object) As Object
Dim oProves As CProveedores

On Error Resume Next
    
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    oProves.Add oProveedor.Cod, oProveedor.Den
    oProves.CargarDatosProveedor oProveedor.Cod
    
    With blankword
        
        'Datos del Proveedor
        If frmESPERA.ProgressBar1.Value >= 99 Then frmESPERA.ProgressBar1.Value = 1
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
        frmESPERA.lblDetalle = sRellenarProve
    
        If .Bookmarks.Exists("DEN_PROVE") Then
            DatoAWord blankword, "DEN_PROVE", oProves.Item(1).Den
        End If
        If .Bookmarks.Exists("COD_PROVE") Then
            DatoAWord blankword, "COD_PROVE", oProves.Item(1).Cod
        End If
        If .Bookmarks.Exists("DIR_PROVE") Then
                DatoAWord blankword, "DIR_PROVE", NullToStr(oProves.Item(1).Direccion)
        End If
        If .Bookmarks.Exists("POBL_PROVE") Then
                DatoAWord blankword, "POBL_PROVE", NullToStr(oProves.Item(1).Poblacion)
        End If
        If .Bookmarks.Exists("CP_PROVE") Then
                DatoAWord blankword, "CP_PROVE", NullToStr(oProves.Item(1).cP)
        End If
        If .Bookmarks.Exists("PROV_PROVE") Then
                DatoAWord blankword, "PROV_PROVE", NullToStr(oProves.Item(1).DenProvi)
        End If
        If .Bookmarks.Exists("PAIS_PROVE") Then
                DatoAWord blankword, "PAIS_PROVE", NullToStr(oProves.Item(1).DenPais)
        End If
        
        
        'Datos de la persona de contacto
        If frmESPERA.ProgressBar1.Value >= 99 Then frmESPERA.ProgressBar1.Value = 1
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
        frmESPERA.lblDetalle = sRellenarPer
        
        If .Bookmarks.Exists("NOM_CONTACTO") Then
                DatoAWord blankword, "NOM_CONTACTO", NullToStr(sdbcContacto.Columns("NOMBRE").Value)
        End If
        If .Bookmarks.Exists("APE_CONTACTO") Then
                DatoAWord blankword, "APE_CONTACTO", NullToStr(sdbcContacto.Columns("APE").Value)
        End If
        If .Bookmarks.Exists("TFNO_CONTACTO") Then
                DatoAWord blankword, "TFNO_CONTACTO", NullToStr(sdbcContacto.Columns("TFNO").Value)
        End If
        If .Bookmarks.Exists("TFNO_MOVIL_CONTACTO") Then
                DatoAWord blankword, "TFNO_MOVIL_CONTACTO", NullToStr(sdbcContacto.Columns("MOVIL").Value)
        End If
        If .Bookmarks.Exists("FAX_CONTACTO") Then
                DatoAWord blankword, "FAX_CONTACTO", NullToStr(sdbcContacto.Columns("FAX").Value)
        End If
        If .Bookmarks.Exists("MAIL_CONTACTO") Then
                DatoAWord blankword, "MAIL_CONTACTO", NullToStr(sdbcContacto.Columns("EMAIL").Value)
        End If
    
    
        'Datos de la persona que realiza la petici�n
        If frmESPERA.ProgressBar1.Value >= 99 Then frmESPERA.ProgressBar1.Value = 1
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
        frmESPERA.lblDetalle = sRellenarPer
        
        If oUsuarioSummit.Persona Is Nothing Then
            'Si es el administrador o no es persona se borran los marcadores
            .Bookmarks("NOM_PERSONA").Range.Delete
            .Bookmarks("APE_PERSONA").Range.Delete
            .Bookmarks("TFNO_PERSONA").Range.Delete
            .Bookmarks("FAX_PERSONA").Range.Delete
            .Bookmarks("MAIL_PERSONA").Range.Delete
        Else
            If .Bookmarks.Exists("NOM_PERSONA") Then
                DatoAWord blankword, "NOM_PERSONA", NullToStr(oUsuarioSummit.Persona.nombre)
            End If
            If .Bookmarks.Exists("APE_PERSONA") Then
                DatoAWord blankword, "APE_PERSONA", NullToStr(oUsuarioSummit.Persona.Apellidos)
            End If
            If .Bookmarks.Exists("TFNO_PERSONA") Then
                DatoAWord blankword, "TFNO_PERSONA", NullToStr(oUsuarioSummit.Persona.Tfno)
            End If
            If .Bookmarks.Exists("FAX_PERSONA") Then
                DatoAWord blankword, "FAX_PERSONA", NullToStr(oUsuarioSummit.Persona.Fax)
            End If
            If .Bookmarks.Exists("MAIL_PERSONA") Then
                DatoAWord blankword, "MAIL_PERSONA", NullToStr(oUsuarioSummit.Persona.mail)
            End If
        End If
    End With
    
    Set GenerarPlantillaMail = blankword

    Set oProves = Nothing
            
End Function

Private Function GenerarMail() As Boolean
    Dim appword  As Object
    Dim docword As Object
    Dim bSesionIniciada As Boolean   'Indica si se ha iniciado una sesion de correo
    Dim splantilla As String
    Dim sTemp As String
    Dim bSalvar As Boolean
    Dim ErrorMail As TipoErrorSummit
    Dim bDisplayAlerts As Boolean
    Dim fso As Scripting.FileSystemObject
    Dim sCuerpo As String
    Dim oFile
    Dim arAtach() As String
    
    On Error GoTo Error:
    
    
    
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sGenerarMail

    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show
    DoEvents
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.ProgressBar2.Value = 1

    'Empieza a generar el mail
    splantilla = txtPlantilla
   
    If Not bSesionIniciada Or oIdsMail Is Nothing Then
        DoEvents
        Set oIdsMail = IniciarSesionMail
        bSesionIniciada = True
    End If
    If frmESPERA.ProgressBar1.Value >= 99 Then frmESPERA.ProgressBar1.Value = 1
    frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    frmESPERA.lblDetalle = sGenerarWord
    
    If appword Is Nothing Then
        Set appword = CreateObject("Word.Application")
        bSalvar = appword.Options.SavePropertiesPrompt
        appword.Options.SavePropertiesPrompt = False
    End If
                        
        
    Set docword = appword.Documents.Add(splantilla)
    GenerarPlantillaMail docword
    Clipboard.clear
    docword.Range.Copy
    sCuerpo = Clipboard.GetText
    docword.Close False
    
    Clipboard.clear
             
    'Genera la hoja excel y la incluye en el mail como un adjunto
    If frmESPERA.ProgressBar1.Value >= 99 Then frmESPERA.ProgressBar1.Value = 1
    frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    frmESPERA.lblDetalle = sGenerarExcel

    sTemp = DevolverPathFichTemp & "DatosExt.xls"
    Set fso = New Scripting.FileSystemObject
    fso.CopyFile txtExcel.Text, sTemp
    Set oFile = fso.GetFile(sTemp)
    If oFile.Attributes And 1 Then
        oFile.Attributes = oFile.Attributes Xor 1
    End If
    Set oFile = Nothing
    Set fso = Nothing
    GenerarHojaExcel sTemp
    
    Unload frmESPERA
    
    Clipboard.clear
    appword.Options.SavePropertiesPrompt = bSalvar
    
    
     If appword.Visible = False Then
        ' cuando se realizan solo peticiones via web, queda una tarea
        ' de word abierta que no se destruye con appword = nothing
        appword.Quit
        'Si el usuario no tiene abierto ning�n documento m�s de excel cierra la aplicaci�n
'        If appexcel.UserControl = False Then
'            appexcel.Quit
'        End If
    End If
    
    Set docword = Nothing
    Set appword = Nothing
    
    'mensaje
    ReDim arAtach(0)
    arAtach(0) = "DatosExt.xls"
    ReDim Preserve arAtach(1)
    ErrorMail = ComponerMensaje(txtEmail, gParametrosInstalacion.gsDatosExtMailSubject, sCuerpo, arAtach, _
                                    entidadNotificacion:=PedDirecto, _
                                    tipoNotificacion:=PetDatosExtMail)
    If ErrorMail.NumError <> TESnoerror Then
        TratarError ErrorMail
        
        GenerarMail = False
    Else
        GenerarMail = True
    End If
          
    If bSesionIniciada Then
        FinalizarSesionMail
    End If
           
    Exit Function
                    
Error:

    Select Case err.Number
        
        Case 5151
            MsgBox err.Description, vbCritical, "Fullstep"
            Unload frmESPERA
            GenerarMail = False
            Screen.MousePointer = vbNormal
        Case Else
                Resume Next
    End Select
    
End Function

Private Function GenerarHojaExcel(Docexcel As String) As Boolean
    Dim splantillaExcel As String
    Dim oArt As CArticulo
    Dim oatrib As CAtributo
    Dim oProject As Object
    Dim oModule As Object
    Dim Fila As Integer
    Dim orange As Object
    Dim sComentario As String
    Dim iNumAtrib As Integer
    Dim iBoton As Integer
    Dim sCeldaImagen As String
    Dim sCeldaImagen2 As String
    Dim sCeldaImagen3 As String
    Dim sConnect As String
    Dim oExcelAdoConn As ADODB.Connection
    Dim oExcelAdoRS As ADODB.Recordset
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim SQL As String
    Dim xls() As String
    Dim i As Integer
    Dim j As Integer
    Dim iRegistroArts As Integer
    Dim iRegistroAtribs As Integer
    Dim iRegistroSeleccion As Integer
    Dim iLimArts As Integer
    Dim iNumTotAtribs As Integer
    Dim iNumLineas As Integer
    Dim aCodigosAtributosSeleccion() As String
    Dim iNumCodigos As Integer
    
    
    
    On Error GoTo Error:
    
    If frmESPERA.ProgressBar1.Value >= 99 Then frmESPERA.ProgressBar1.Value = 1
    frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    frmESPERA.lblDetalle = sGenerarHoja
    
    'Aqu� empieza la nueva forma
    iNumTotAtribs = 0
    iLimArts = oArticulos.Count
    For i = 1 To iLimArts
        iNumTotAtribs = iNumTotAtribs + oArticulos.Item(i).ATRIBUTOS.Count
    Next i
    iNumLineas = (iNumTotAtribs * TAM_ATRIBUTOS) + 1
    If iNumLineas < 43 Then iNumLineas = 43
    ReDim xls(1 To NUMCOLS, 1 To iNumLineas)
    'Llenar el array con los datos
    xls(GENERALES, 2) = NullToStr(sGmn1)
    xls(GENERALES, 3) = NullToStr(sGmn2)
    xls(GENERALES, 4) = NullToStr(sGmn3)
    xls(GENERALES, 5) = NullToStr(sGmn4)
    xls(GENERALES, 6) = NullToStr(oProveedor.Cod)
    xls(GENERALES, 7) = NullToStr(oProveedor.Den)
    xls(GENERALES, 8) = NullToStr(sArticulo)
    xls(GENERALES, 9) = NullToStr(sFicha)
    xls(GENERALES, 10) = NullToStr(sCodExt)
    xls(GENERALES, 11) = NullToStr(sLeyenda)
    xls(GENERALES, 12) = NullToStr(sFicheros)
    xls(GENERALES, 13) = NullToStr(sMensajeImage)
    xls(GENERALES, 14) = NullToStr(sImagen)
    xls(GENERALES, 15) = NullToStr(sArticulo)
    xls(GENERALES, 16) = NullToStr(sFrase)
    xls(GENERALES, 17) = NullToStr(sInsertar)
    xls(GENERALES, 18) = NullToStr(sEliminar)
    xls(GENERALES, 19) = NullToStr(sVer)
    xls(GENERALES, 20) = NullToStr(oArticulos.Count)
    xls(GENERALES, 21) = NullToStr(sproveedor)
    xls(GENERALES, 22) = NullToStr(sMensajeFormatoBoolean)
    xls(GENERALES, 23) = NullToStr(sMensajeFormatoFecha)
    xls(GENERALES, 24) = NullToStr(sMensajeFormatoNumerico)
    xls(GENERALES, 25) = NullToStr(sTipo)
    xls(GENERALES, 26) = NullToStr(sString)
    xls(GENERALES, 27) = NullToStr(sNumerico)
    xls(GENERALES, 28) = NullToStr(sFecha)
    xls(GENERALES, 29) = NullToStr(sBool)
    xls(GENERALES, 30) = NullToStr(sCelda)
    xls(GENERALES, 31) = NullToStr(sMaximo)
    xls(GENERALES, 32) = NullToStr(sMinimo)
    xls(GENERALES, 33) = NullToStr(sTextoMayor100c)
    xls(GENERALES, 34) = NullToStr(sTextoMayor200c)
    xls(GENERALES, 35) = NullToStr(sValNum_FR)
    xls(GENERALES, 36) = NullToStr(sValFec_FR)
    xls(GENERALES, 37) = NullToStr(sListaSeleccion)
    xls(GENERALES, 38) = NullToStr(sTrue)
    xls(GENERALES, 39) = NullToStr(sFalse)
    xls(GENERALES, 40) = NullToStr(sGmn1)
    xls(GENERALES, 41) = NullToStr(sGmn2)
    xls(GENERALES, 42) = NullToStr(sGmn3)
    xls(GENERALES, 43) = NullToStr(sGmn4)
    For i = 44 To iNumLineas
        xls(GENERALES, i) = "#"
    Next i
    
    j = 1
    Fila = 12
    iNumAtrib = 0
    iBoton = 1
    iNumCodigos = 0
    iRegistroArts = 2
    iRegistroAtribs = 2
    iRegistroSeleccion = 2
    For Each oArt In oArticulos
        If frmESPERA.ProgressBar2.Value >= 99 Then frmESPERA.ProgressBar2.Value = 1
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 1
        frmESPERA.lblContacto = oArt.Cod & " - " & oArt.Den
        xls(ARTICULOS, iRegistroArts) = NullToStr(oArt.Cod)
        iRegistroArts = iRegistroArts + 1
        xls(ARTICULOS, iRegistroArts) = NullToStr(oArt.Den)
        iRegistroArts = iRegistroArts + 1
        xls(ARTICULOS, iRegistroArts) = NullToStr(oArt.GMN1Cod)
        iRegistroArts = iRegistroArts + 1
        xls(ARTICULOS, iRegistroArts) = NullToStr(oArt.GMN2Cod)
        iRegistroArts = iRegistroArts + 1
        xls(ARTICULOS, iRegistroArts) = NullToStr(oArt.GMN3Cod)
        iRegistroArts = iRegistroArts + 1
        xls(ARTICULOS, iRegistroArts) = NullToStr(oArt.GMN4Cod)
        iRegistroArts = iRegistroArts + 1
        xls(ARTICULOS, iRegistroArts) = NullToStr(oArt.CodigoExterno)
        iRegistroArts = iRegistroArts + 1
        xls(ARTICULOS, iRegistroArts) = NullToStr(oArt.ATRIBUTOS.Count)
        iRegistroArts = iRegistroArts + 1
        For Each oatrib In oArt.ATRIBUTOS
            xls(ATRIBUTOS, iRegistroAtribs) = NullToStr(oatrib.Cod)
            iRegistroAtribs = iRegistroAtribs + 1
            xls(ATRIBUTOS, iRegistroAtribs) = NullToStr(oatrib.Den)
            iRegistroAtribs = iRegistroAtribs + 1
            xls(ATRIBUTOS, iRegistroAtribs) = NullToStr(oatrib.Id)
            iRegistroAtribs = iRegistroAtribs + 1
            xls(ATRIBUTOS, iRegistroAtribs) = NullToStr(oatrib.TipoIntroduccion)
            iRegistroAtribs = iRegistroAtribs + 1
            xls(ATRIBUTOS, iRegistroAtribs) = NullToStr(oatrib.Tipo)
            iRegistroAtribs = iRegistroAtribs + 1
            xls(ATRIBUTOS, iRegistroAtribs) = NullToStr(oatrib.Maximo)
            iRegistroAtribs = iRegistroAtribs + 1
            xls(ATRIBUTOS, iRegistroAtribs) = NullToStr(oatrib.Minimo)
            iRegistroAtribs = iRegistroAtribs + 1
            If oatrib.Tipo = TipoBoolean Then
                If oatrib.valor = 1 Then
                    xls(ATRIBUTOS, iRegistroAtribs) = sTrue
                Else
                    xls(ATRIBUTOS, iRegistroAtribs) = sFalse
                End If
            Else
                xls(ATRIBUTOS, iRegistroAtribs) = NullToStr(oatrib.valor)
            End If
            iRegistroAtribs = iRegistroAtribs + 1

            If oatrib.TipoIntroduccion = Introselec Then
                If Not ExisteCodigoEnSeleccion(aCodigosAtributosSeleccion, iNumCodigos, NullToStr(oatrib.Cod)) Then
                    ReDim Preserve aCodigosAtributosSeleccion(1 To iNumCodigos + 1)
                    iNumCodigos = iNumCodigos + 1
                    aCodigosAtributosSeleccion(iNumCodigos) = NullToStr(oatrib.Cod)
                    oatrib.CargarListaDeValores
                    xls(Seleccion, iRegistroSeleccion) = NullToStr(oatrib.Cod)
                    iRegistroSeleccion = iRegistroSeleccion + 1
                    xls(Seleccion, iRegistroSeleccion) = NullToStr(oatrib.ListaPonderacion.Count)
                    iRegistroSeleccion = iRegistroSeleccion + 1
                    For i = 1 To oatrib.ListaPonderacion.Count
                        xls(Seleccion, iRegistroSeleccion) = NullToStr(oatrib.ListaPonderacion.Item(i).ValorLista)
                        iRegistroSeleccion = iRegistroSeleccion + 1
                    Next i
                End If
            End If
        Next
        
        If iNumAtrib < i - 1 Then
            iNumAtrib = i - 1
        End If
        
        j = j + 1
        Fila = Fila + 5
        iBoton = iBoton + 3
    Next

        
    Set oExcelAdoConn = New ADODB.Connection
    sConnect = "Provider=MSDASQL.1;" _
             & "Extended Properties=""DBQ=" & Docexcel & ";" _
             & "Driver={Microsoft Excel Driver (*.xls)};" _
             & "FIL=excel 8.0;" _
             & "ReadOnly=0;" _
             & "UID=admin;"""
             
    oExcelAdoConn.Open sConnect
    SQL = "CREATE TABLE [Datos] (GENERALES memo, ARTICULOS memo, ATRIBUTOS memo, SELECCION memo, NUMERO memo)"
    oExcelAdoConn.Execute SQL
    
    Set adoComm = New ADODB.Command
    Set adoComm.ActiveConnection = oExcelAdoConn
    
    SQL = "INSERT INTO [Datos$] values ("
    For j = 1 To UBound(xls, 1)
        SQL = SQL & "?,"
        Set adoParam = adoComm.CreateParameter("", adLongVarChar, adParamInput, 2000, Null)
        adoComm.Parameters.Append adoParam
    Next
    SQL = SQL & "?)"
    Set adoParam = adoComm.CreateParameter("", adLongVarChar, adParamInput, 2000, Null)
    adoComm.Parameters.Append adoParam
    
    adoComm.CommandText = SQL
    adoComm.CommandType = adCmdText
    adoComm.Prepared = True
    
    For i = 2 To UBound(xls, 2)
        For j = 1 To UBound(xls, 1)
            adoComm.Parameters(j - 1).Size = IIf(Len(xls(j, i)) = 0, 0, Len(xls(j, i))) + 1
            adoComm.Parameters(j - 1).Value = StrToNull(xls(j, i))
        Next
        adoComm.Parameters(j - 1).Value = CStr(i)
        adoComm.Execute
    Next
    Set adoComm = Nothing
    oExcelAdoConn.Close
    Set oExcelAdoConn = Nothing
    GenerarHojaExcel = True
    Set oatrib = Nothing
    Set orange = Nothing
    Set oModule = Nothing
    Set oProject = Nothing
    'Set xlSheet = Nothing
    'Set xlBook = Nothing
        
    Exit Function

Error:

    Select Case err.Number
        Case 9:
            ReDim Preserve xls(1 To NUMCOLS, 1 To UBound(xls, 2) + 25)
            Resume 0
        
        Case 1004:
            Resume Next
        
        Case Else:
            MsgBox err.Description
            If err.Number = 7 Then
                Exit Function
            End If
    End Select
    
    
    Unload frmESPERA

    
End Function

Private Function ExisteCodigoEnSeleccion(aCodigos() As String, ByVal iNumCods As Integer, sCod As String) As Boolean
'*************************************************************************************
'*** Descripci�n: Compueba si un c�digo, string pasado como tercer argumento de    ***
'***              la funci�n, est� en el array de c�digos, array de strings        ***
'***              pasado como primer argumento de la funci�n.                      ***
'***                                                                               ***
'*** Par�metros : aCodigos() ::> Array de strings que contendr� varios c�digos o   ***
'***                             ninguno.                                          ***
'***              iNumCods   ::> Entero que lleva la cuenta del n�mero de c�digos  ***
'***                             en el array.                                      ***
'***              sCod       ::> String que contiene el c�digo cuya existencia se  ***
'***                             verificar� en el array.                           ***
'***                                                                               ***
'*** Valor que devuelve: un booleano, que ser� TRUE si existe el c�digo en el      ***
'***                     array, y FALSE en caso contrario.                         ***
'*************************************************************************************
    Dim sSalida As Boolean
    Dim i As Integer
    
    If iNumCods = 0 Then
        ExisteCodigoEnSeleccion = False
        Exit Function
    End If
    sSalida = False
    For i = 1 To iNumCods
        If aCodigos(i) = sCod Then
            sSalida = True
            Exit For
        End If
    Next i
    ExisteCodigoEnSeleccion = sSalida
End Function

Private Function CodigoChangeCelda() As String
Dim str As String
    
    str = "Private bRespetarCelda As Boolean" & Chr(13)
    str = str & "" & Chr(13)
    str = str & "Private Sub Worksheet_Change(ByVal Target As Range)" & Chr(13)
    str = str & "On Error Resume Next" & Chr(13)
    str = str & "Dim strFila As String" & Chr(13)
    str = str & "Dim strCol As String" & Chr(13)
    str = str & "Dim sTargetTipo As String" & Chr(13)
    str = str & "Dim strFilaAtrib As String" & Chr(13)
    str = str & "If Not bRespetarCelda Then" & Chr(13)
    str = str & "'Obtengo la fila" & Chr(13)
    str = str & "strFila = Mid(Target.Address, 4)" & Chr(13)
    str = str & "strCol = Left(Target.Address, 2)" & Chr(13)
    str = str & "strFilaAtrib = CStr(CInt(strFila) + 2)" & Chr(13)
    str = str & "If strCol = " & """$D""" & " Then" & Chr(13)
    str = str & "   If Len(Target) > 100 Then" & Chr(13)
    str = str & "      MsgBox """ & sMensajeMaximaLen100 & "100" & """" & Chr(13)
    str = str & "      bRespetarCelda = True" & Chr(13)
    str = str & "      Target = Left(Target, 100)" & Chr(13)
    str = str & "      bRespetarCelda = False" & Chr(13)
    str = str & "   End If" & Chr(13)
    str = str & "Else" & Chr(13)
    str = str & "   sTargetTipo = left(Target.Address,3) & strFilaAtrib" & Chr(13)
    str = str & "   Select Case Range(sTargetTipo).Value" & Chr(13)
    str = str & "       Case 2:" & Chr(13)
    str = str & "       'Entero" & Chr(13)
    str = str & "         If Not IsNumeric(Target) Then" & Chr(13)
    str = str & "            MsgBox """ & sMensajeFormatoNumerico & """" & Chr(13)
    str = str & "            bRespetarCelda = True" & Chr(13)
    str = str & "            Target = " & """""" & "" & Chr(13)
    str = str & "            bRespetarCelda = False" & Chr(13)
    str = str & "         End If" & Chr(13)
    str = str & "       Case 3:" & Chr(13)
    str = str & "       'Fecha" & Chr(13)
    str = str & "         If Not IsDate(Target) Then" & Chr(13)
    str = str & "            MsgBox """ & sMensajeFormatoFecha & """" & Chr(13)
    str = str & "            bRespetarCelda = True" & Chr(13)
    str = str & "            Target = " & """""" & "" & Chr(13)
    str = str & "            bRespetarCelda = False" & Chr(13)
    str = str & "         End If" & Chr(13)
    str = str & "       Case 4:" & Chr(13)
    str = str & "       'Boolean" & Chr(13)
    str = str & "         If UCase(Target) <>" & """YES""" & " And UCase(Target.Text) <>" & """NO""" & " Then" & Chr(13)
    str = str & "            MsgBox """ & sMensajeFormatoBoolean & """" & Chr(13)
    str = str & "            bRespetarCelda = True" & Chr(13)
    str = str & "            Target = " & """""" & "" & Chr(13)
    str = str & "            bRespetarCelda = False" & Chr(13)
    str = str & "         End If" & Chr(13)
    str = str & "       Case 1:" & Chr(13)
    str = str & "       'string" & Chr(13)
    str = str & "         If Len(Target) > 200 Then" & Chr(13)
    str = str & "            MsgBox """ & sMensajeMaximaLen100 & "200" & """" & Chr(13)
    str = str & "            bRespetarCelda = True" & Chr(13)
    str = str & "            Target = Left(Target, 200)" & Chr(13)
    str = str & "            bRespetarCelda = False" & Chr(13)
    str = str & "         End If" & Chr(13)
    str = str & "   End Select" & Chr(13)
    str = str & "End If" & Chr(13)
    str = str & "End If" & Chr(13)
    str = str & "End Sub"
    
    CodigoChangeCelda = str
End Function


Private Sub CargarIdiomaProve(idioma As String)
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_SOLIC, idioma)
    
    If Not ador Is Nothing Then
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        sArticulo = ador(0).Value
        ador.MoveNext
        sFicha = ador(0).Value
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        sImagenInsertar = ador(0).Value
        ador.MoveNext
        sImagenEliminar = ador(0).Value
        ador.MoveNext
        sImagenVer = ador(0).Value
        ador.MoveNext
        sMensajeImage = ador(0).Value
        ador.MoveNext
        sCodExt = ador(0).Value
        ador.MoveNext
        sLeyenda = ador(0).Value
        ador.MoveNext
        sString = ador(0).Value
        ador.MoveNext
        sNumerico = ador(0).Value
        ador.MoveNext
        sFecha = ador(0).Value
        ador.MoveNext
        sBool = ador(0).Value
        ador.MoveNext
        sTipo = ador(0).Value
        ador.MoveNext
        sMensajeFormatoBoolean = ador(0).Value
        ador.MoveNext
        sMensajeFormatoFecha = ador(0).Value
        ador.MoveNext
        sMensajeFormatoNumerico = ador(0).Value
        ador.MoveNext
        sproveedor = ador(0).Value
        ador.MoveNext
        sFicheros = ador(0).Value
        ador.MoveNext
        sMensajeMaximaLen100 = ador(0).Value
        ador.Close
            
    End If
    
    Set ador = Nothing
End Sub

Private Function CodigoActivate() As String
Dim str As String
    

str = "" & Chr(13)
str = str & "Private Sub Workbook_Open()" & Chr(13)
str = str & "" & Chr(13)
str = str & "If val(Application.Version) < 9 Then" & Chr(13)
str = str & "    Sheets(1).MostrarControles" & Chr(13)
str = str & "Else" & Chr(13)
str = str & "    Sheets(1).OcultarControles" & Chr(13)
str = str & "End If" & Chr(13)
str = str & "End Sub" & Chr(13)
CodigoActivate = str
End Function
Private Function CodigoChangeSelection() As String
Dim str As String
str = "" & Chr(13)
str = str & "Private Sub Worksheet_SelectionChange(ByVal Target As Excel.Range)" & Chr(13)
str = str & "Dim sAdress As String" & Chr(13)
str = str & "Dim sLetra As String" & Chr(13)
str = str & "Dim sNum As String" & Chr(13)
str = str & "Dim sAux As String" & Chr(13)
str = str & "Dim lNum As Long" & Chr(13)
str = str & "Dim FilaI As Long" & Chr(13)
str = str & "Dim Fila As Long" & Chr(13)
str = str & "Dim Indice As Long" & Chr(13)
str = str & "Dim bCargar As Boolean" & Chr(13)
str = str & "" & Chr(13)

str = str & "If bRespetarSelection Then Exit Sub" & Chr(13)
str = str & "FilaI = 12" & Chr(13)
str = str & "bCargar = False" & Chr(13)
str = str & "If Target.Cells.Count > 1 Then Exit Sub" & Chr(13)
str = str & "" & Chr(13)
str = str & "        sAdress = Target.Address" & Chr(13)
str = str & "        sAux = Right(sAdress, Len(sAdress) - 1)" & Chr(13)
str = str & "        sLetra = Left(sAux, InStr(1, sAux, " & """$""" & ") - 1)" & Chr(13)
str = str & "        sNum = Right(sAux, Len(sAux) - InStr(1, sAux, " & """$""" & "))" & Chr(13)
str = str & "        lNum = CLng(sNum) - 12" & Chr(13)
str = str & "        If lNum >= 0 Then" & Chr(13)
str = str & "            If lNum Mod 5 = 0 Or lNum Mod 5 = 1 Then" & Chr(13)
str = str & "                Fila = CLng(sNum)" & Chr(13)
str = str & "                Indice = ((Fila - FilaI) \ 5)" & Chr(13)
str = str & "                bCargar = True" & Chr(13)
str = str & "            End If" & Chr(13)
str = str & "        End If" & Chr(13)
str = str & "If bCargar = True And Indice < Range(" & """NUMART""" & ").Value Then" & Chr(13)
str = str & "    cmbArtic.ListIndex = Indice" & Chr(13)
str = str & "Else" & Chr(13)
str = str & "    cmbArtic.ListIndex = -1" & Chr(13)
str = str & "End If" & Chr(13)
str = str & "End Sub" & Chr(13)
str = str & "" & Chr(13)
CodigoChangeSelection = str
End Function
Private Function CodigoHyperLink() As String
    Dim str As String
    
    str = str & "" & Chr(13)
    str = str & "Private Sub Worksheet_FollowHyperlink(ByVal Target As Hyperlink)" & Chr(13)
    str = str & "On Error goto Error" & Chr(13)
    
    str = str & "Dim CodArt As String" & Chr(13)
    str = str & "Dim img As String" & Chr(13)
    str = str & "Dim bNoExiste As Boolean" & Chr(13)
    str = str & "Dim Fila As Long" & Chr(13)
    
    str = str & "Fila= clng(Mid(Target.Range.Address,4))" & Chr(13)
    str = str & "Fila= Fila +4" & Chr(13)
    str = str & "CodArt=Sheets(1).range (" & """F""" & "&Fila)" & Chr(13)
    
    str = str & "Select case Target.SubAddress" & Chr(13)
    str = str & "   case " & """IMAGEN""" & Chr(13)
    str = str & "       'C�digo para insertar la imagen del art�culo" & Chr(13)
    str = str & "       img = Application.GetOpenFilename(""" & sFicheros & """)" & Chr(13)
    str = str & "       If cbool(img) <> False Then" & Chr(13)
    str = str & "           Sheets(3).Shapes(" & """Picture""" & "&CodArt).OLEFormat.Object.Delete" & Chr(13)
    str = str & "           Sheets(3).Pictures.Insert (img)" & Chr(13)
    str = str & "           Sheets(3).Shapes(Sheets(3).Shapes.Count).Name = " & """Picture""" & "&CodArt" & Chr(13)
    str = str & "       End If" & Chr(13)
    
    
    str = str & "   case " & """IMAGEN2""" & Chr(13)
    str = str & "       'C�digo para eliminar la imagen del art�culo" & Chr(13)
    str = str & "       Sheets(3).Shapes(" & """Picture""" & "&CodArt).OLEFormat.Object.Delete" & Chr(13)
    
    
    str = str & "   case " & """IMAGEN3""" & Chr(13)
    str = str & "       'C�digo para ver la imagen del art�culo" & Chr(13)
    str = str & "       bNoExiste = False" & Chr(13)
    str = str & "       Sheets(3).Shapes(" & """Picture""" & "&CodArt).CopyPicture 1, 2" & Chr(13)
    str = str & "       If bNoExiste = True Then" & Chr(13)
    str = str & ""
    str = str & "           MsgBox """ & sMensajeImage & """" & Chr(13)
    str = str & "           Sheets(1).Range(" & """A""" & "&Fila).Select" & Chr(13)
    str = str & "           Exit Sub" & Chr(13)
    str = str & "       Else" & Chr(13)
    'Lo pega en la hoja
    str = str & "           Sheets(2).Paste Destination:=Sheets(2).Range(" & """A1""" & ")" & Chr(13)
    str = str & "           Sheets(2).cmdVolver.BringToFront" & Chr(13)
    str = str & "           Sheets(2).Fila = Fila" & Chr(13)
    str = str & "       End If" & Chr(13)
    'Hace la hoja invisible
    str = str & "       Sheets(2).Visible = xlSheetVisible" & Chr(13)
    str = str & "       Sheets(1).Visible = xlSheetHidden" & Chr(13)
    
    str = str & "End Select" & Chr(13)
    
    'Se posiciona en la fila en la que estaba
    str = str & "Sheets(1).Range(" & """A""" & "&Fila).Select" & Chr(13)
    str = str & "Exit Sub" & Chr(13)
    
    str = str & "Error:" & Chr(13)
    str = str & "bNoExiste = True" & Chr(13)
    str = str & "Resume Next" & Chr(13)
    
    str = str & "End Sub"
    
    CodigoHyperLink = str
End Function







