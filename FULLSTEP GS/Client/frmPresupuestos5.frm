VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPresupuestos5 
   Caption         =   "Form1"
   ClientHeight    =   5370
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9105
   Icon            =   "frmPresupuestos5.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5370
   ScaleWidth      =   9105
   Begin VB.PictureBox picNavigate1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   7560
      ScaleHeight     =   375
      ScaleWidth      =   1095
      TabIndex        =   15
      Top             =   120
      Width           =   1095
      Begin VB.CommandButton cmdModifArbol 
         Enabled         =   0   'False
         Height          =   285
         Left            =   795
         Picture         =   "frmPresupuestos5.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   0
         Width           =   315
      End
      Begin VB.CommandButton cmdElimArbol 
         Enabled         =   0   'False
         Height          =   285
         Left            =   390
         Picture         =   "frmPresupuestos5.frx":0231
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   0
         Width           =   315
      End
      Begin VB.CommandButton cmdAnyaArbol 
         Height          =   285
         Left            =   0
         Picture         =   "frmPresupuestos5.frx":02C3
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         Width           =   315
      End
   End
   Begin VB.CheckBox chkBajaLog 
      Caption         =   "Ver bajas l�gicas"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   465
      Width           =   2775
   End
   Begin VB.PictureBox picNavigate2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   100
      ScaleHeight     =   435
      ScaleWidth      =   8385
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   4900
      Width           =   8385
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "D&A�adir"
         Height          =   345
         Left            =   0
         TabIndex        =   7
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "D&Listado"
         Height          =   345
         Left            =   7200
         TabIndex        =   13
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "D&Buscar"
         Height          =   345
         Left            =   6120
         TabIndex        =   12
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "D&Restaurar"
         Height          =   345
         Left            =   5040
         TabIndex        =   11
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdModif 
         Caption         =   "D&Modificar"
         Height          =   345
         Left            =   1080
         TabIndex        =   8
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdEli 
         Caption         =   "D&Eliminar"
         Height          =   345
         Left            =   2160
         TabIndex        =   9
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdBajaLog 
         Caption         =   "Deshacer baja l�gica"
         Height          =   345
         Left            =   3240
         TabIndex        =   10
         Top             =   45
         Width           =   1705
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcPartidaPres 
      Height          =   285
      Left            =   1920
      TabIndex        =   1
      Top             =   120
      Width           =   5595
      DataFieldList   =   "Column 0"
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2672
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7064
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9869
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 1"
   End
   Begin MSComctlLib.TreeView tvwEstrPres 
      Height          =   4185
      Left            =   120
      TabIndex        =   6
      Top             =   720
      Width           =   8745
      _ExtentX        =   15425
      _ExtentY        =   7382
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8400
      Top             =   5160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   -2147483643
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos5.frx":0345
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos5.frx":0E0F
            Key             =   "PRES1"
            Object.Tag             =   "PRES1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos5.frx":1261
            Key             =   "PRES2"
            Object.Tag             =   "PRES2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos5.frx":16B3
            Key             =   "PRES3"
            Object.Tag             =   "PRES3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos5.frx":1B05
            Key             =   "PRES4"
            Object.Tag             =   "PRES4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos5.frx":1F57
            Key             =   "PRESBAJALOGICA"
            Object.Tag             =   "PRESBAJALOGICA"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblPartida 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Partida Presupuestaria:"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1665
   End
End
Attribute VB_Name = "frmPresupuestos5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables privadas
Private m_oPartidas As cPresConceptos5Nivel0
Private oPres0 As cPresConcep5Nivel0
Private m_bCargarComboDesde As Boolean
Private m_sPartida As String
Private oIBaseDatos As IBaseDatos
Private m_sCaptionDeshacerBaja As String
Private m_sCaptionBajaLogica As String
Private m_sUsu As String
Private m_sPerfilCod As String

Private m_bNodoConHijos As Boolean

Public m_bSaltar As Boolean
Public m_bActualizar As Boolean

'Variables publicas
Public m_oPresupuestos As CPresConceptos5Nivel1 'Contendra toda la estructura de presupuestos

Private Sub cmdlistado_Click()
Dim nodx As MSComctlLib.node


    Set nodx = tvwEstrPres.selectedItem
    If Not nodx Is Nothing Then
        Select Case Left(nodx.Tag, 5)

            Case "PRES1"
                frmLstPRESPorCon5.sParCon1 = DevolverCod(nodx)
                
            Case "PRES2"
                frmLstPRESPorCon5.sParCon1 = DevolverCod(nodx.Parent)
                frmLstPRESPorCon5.sParCon2 = DevolverCod(nodx)

            Case "PRES3"
                frmLstPRESPorCon5.sParCon1 = DevolverCod(nodx.Parent.Parent)
                frmLstPRESPorCon5.sParCon2 = DevolverCod(nodx.Parent)
                frmLstPRESPorCon5.sParCon3 = DevolverCod(nodx)
                
            Case "PRES4"
                frmLstPRESPorCon5.sParCon1 = DevolverCod(nodx.Parent.Parent.Parent)
                frmLstPRESPorCon5.sParCon2 = DevolverCod(nodx.Parent.Parent)
                frmLstPRESPorCon5.sParCon3 = DevolverCod(nodx.Parent)
                frmLstPRESPorCon5.sParCon4 = DevolverCod(nodx)

        End Select
        frmLstPRESPorCon5.sDen = DevolverDen(nodx)
    End If

If Not oPres0 Is Nothing Then
    If oPres0.Cod <> "" Then
        frmLstPRESPorCon5.m_sPres = oPres0.Cod
    End If
End If
frmLstPRESPorCon5.m_bajaLog = chkBajaLog.Value
frmLstPRESPorCon5.Show 1
End Sub

Private Sub Form_Load()
m_sUsu = ""
m_sPerfilCod = ""
CargarRecursos
CargarSeguridad
End Sub
Private Sub CargarRecursos()
Dim rs As New ADODB.Recordset

    Set rs = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESUPUESTOS5, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not rs Is Nothing Then
        Me.caption = rs(0).Value '1 Partidas de control de aprovisionamiento
        rs.MoveNext
        lblPartida.caption = rs(0).Value & ":" '2 Partida presupuestaria
        m_sPartida = rs(0).Value & ":"
        rs.MoveNext
        
        cmdAnyaArbol.ToolTipText = rs(0).Value '3 Crear �rbol presupuestario
        rs.MoveNext
        cmdElimArbol.ToolTipText = rs(0).Value '4 Eliminar �rbol presupuestario
        rs.MoveNext
        cmdModifArbol.ToolTipText = rs(0).Value '5 Modificar �rbol presupuestario
        rs.MoveNext
        chkBajaLog.caption = rs(0).Value '6 Ver bajas l�gicas
        rs.MoveNext
        cmdA�adir.caption = rs(0).Value '7 &A�adir
        rs.MoveNext
        cmdModif.caption = rs(0).Value '8 &Modificar
        rs.MoveNext
        cmdEli.caption = rs(0).Value '9 &Eliminar
        rs.MoveNext
        cmdBajaLog.caption = rs(0).Value '10 Baja l�gica
        m_sCaptionBajaLogica = rs(0).Value
        rs.MoveNext
        m_sCaptionDeshacerBaja = rs(0).Value '11 Deshacer baja l�gica
        rs.MoveNext
        cmdRestaurar.caption = rs(0).Value '12 &Actualizar
        rs.MoveNext
        cmdBuscar.caption = rs(0).Value '13 &Buscar
        rs.MoveNext
        cmdListado.caption = rs(0).Value '14 &Listado
        
        'rs.MoveNext
        
    
    End If
    
    rs.Close
End Sub

Private Sub CargarSeguridad()
    If oUsuarioSummit.Tipo <> TipoDeUsuario.Administrador Then
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESConcepto5CrearArboles)) Is Nothing) Then
            'No tiene permiso para crear arboles presupuestarios
            picNavigate1.Visible = False
            
            m_sUsu = oUsuarioSummit.Cod
        Else
            picNavigate1.Visible = True
            picNavigate2.Visible = True
        End If
        
        If Not oUsuarioSummit.Perfil Is Nothing Then m_sPerfilCod = oUsuarioSummit.Perfil.Cod
    End If
End Sub

Private Sub chkBajaLog_Click()
    If Not oPres0 Is Nothing Then
        GenerarArbolPresupuestos
        tvwEstrPres.Nodes.Item("Raiz ").Selected = True
    End If
    cmdA�adir.Enabled = True
    cmdModif.Enabled = True
    cmdEli.Enabled = True
    cmdBajaLog.Enabled = True
    cmdBuscar.Enabled = True
    cmdListado.Enabled = True
    cmdBajaLog.caption = m_sCaptionBajaLogica
End Sub

Private Sub cmdAnyaArbol_Click()
    frmPRESCon5Arbol.m_sAccion = "Anyadir"
    frmPRESCon5Arbol.WindowState = vbNormal
    frmPRESCon5Arbol.Show vbModal
    
    
    If picNavigate2.Visible Then

        If Not oPres0 Is Nothing Then If sdbcPartidaPres.Value = oPres0.Cod Then Exit Sub
        If sdbcPartidaPres.Value = "" Then Exit Sub
        PartidaSeleccionada sdbcPartidaPres.Value
        If Not oPres0 Is Nothing Then
            GenerarEstructuraPresupuestos sdbcPartidaPres.Value, False
        End If
        m_bSaltar = True
        sdbcPartidaPres = oPres0.Den
        m_bSaltar = False
        cmdElimArbol.Enabled = True
        cmdModifArbol.Enabled = True
        tvwEstrPres.Nodes.Item(1).Selected = True
    End If
End Sub

Public Sub cmdA�adir_Click()
Dim nodx As MSComctlLib.node


    Set nodx = tvwEstrPres.selectedItem
    Screen.MousePointer = vbHourglass
    If Not nodx Is Nothing Then
        Select Case Left(nodx.Tag, 5)
            Case "Raiz "
                frmPRESCon5Detalle.m_sAccion = "Anyadir"
                frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel1
                frmPRESCon5Detalle.m_iNivel = 1
                frmPRESCon5Detalle.m_sPres0 = oPres0.Cod
                
                Screen.MousePointer = vbNormal
                frmPRESCon5Detalle.Show 1

            Case "PRES1"
                frmPRESCon5Detalle.m_sAccion = "Anyadir"
                frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel2
                
                frmPRESCon5Detalle.m_iNivel = 2
                frmPRESCon5Detalle.m_sPres0 = oPres0.Cod
                frmPRESCon5Detalle.m_sPres1 = DevolverCod(nodx)
                frmPRESCon5Detalle.m_bNodoConHijos = m_bNodoConHijos
                Screen.MousePointer = vbNormal
                frmPRESCon5Detalle.Show 1

            Case "PRES2"
                frmPRESCon5Detalle.m_sAccion = "Anyadir"
                frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel3
                
                frmPRESCon5Detalle.m_iNivel = 3
                frmPRESCon5Detalle.m_sPres0 = oPres0.Cod
                frmPRESCon5Detalle.m_sPres1 = DevolverCod(nodx.Parent)
                frmPRESCon5Detalle.m_sPres2 = DevolverCod(nodx)
                frmPRESCon5Detalle.m_bNodoConHijos = m_bNodoConHijos
                Screen.MousePointer = vbNormal
                frmPRESCon5Detalle.Show 1


            Case "PRES3"
                frmPRESCon5Detalle.m_sAccion = "Anyadir"
                frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel4
                
                frmPRESCon5Detalle.m_iNivel = 4
                frmPRESCon5Detalle.m_sPres0 = oPres0.Cod
                frmPRESCon5Detalle.m_sPres1 = DevolverCod(nodx.Parent.Parent)
                frmPRESCon5Detalle.m_sPres2 = DevolverCod(nodx.Parent)
                frmPRESCon5Detalle.m_sPres3 = DevolverCod(nodx)
                frmPRESCon5Detalle.m_bNodoConHijos = m_bNodoConHijos
                Screen.MousePointer = vbNormal
                frmPRESCon5Detalle.Show 1
        End Select
    End If
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Ejecutar el evento click del bot�n Baja logica. Da de baja l�gica una partida presupuestaria
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Public Sub cmdBajaLog_Click()

    Dim nodx, nodSiguiente As MSComctlLib.node
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    
    
    Dim bBaja As Boolean
    Dim scod1, scod2, scod3, scod4 As String
    
    Set nodx = tvwEstrPres.selectedItem

    If nodx Is Nothing Then
        Exit Sub
    
    Else
        Screen.MousePointer = vbHourglass
        
        Select Case Left(nodx.Tag, 5)
            
            Case "PRES1"
                    
                'Seleccionamos el presupuesto
                Dim oPres1Seleccionado As CPresConcep5Nivel1
                Set oPres1Seleccionado = Nothing
                Set oPres1Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel1
                oPres1Seleccionado.Pres0 = sdbcPartidaPres.Value
                oPres1Seleccionado.Cod = DevolverCod(nodx)
                
                Set oIBaseDatos = oPres1Seleccionado
                teserror = oIBaseDatos.IniciarEdicion
                If teserror.NumError <> TESnoerror Then
                    'Ha habido un error al iniciar la edicion
                    basErrores.TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres1Seleccionado = Nothing
                    If Me.Visible Then tvwEstrPres.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                'Si el presupuesto est� dado de baja preguntamos "Deshacer baja???"
                'sino preguntamos "Dar de baja???"
                Screen.MousePointer = vbNormal
                bBaja = False
                If (nodx.Image = "PRESBAJALOGICA") Then
                    irespuesta = oMensajes.DeshacerBajaLogicaPres(oPres0.Nom_Nivel1 & ": " & CStr(oPres1Seleccionado.Cod) & " (" & oPres1Seleccionado.Den & ")")
                Else
                    bBaja = True
                    irespuesta = oMensajes.DarBajaLogicaPres(oPres0.Nom_Nivel1 & ": " & CStr(oPres1Seleccionado.Cod) & " (" & oPres1Seleccionado.Den & ")")
                End If
                Screen.MousePointer = vbHourglass
                If irespuesta = vbYes Then
                    teserror = oPres1Seleccionado.BajaAltaLogica(Not oPres1Seleccionado.BajaLog)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oPres1Seleccionado = Nothing
                        If Me.Visible Then tvwEstrPres.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        scod1 = DevolverCod(nodx)
                        scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
                        If (nodx.Image = "PRESBAJALOGICA") Then
                            m_oPresupuestos.Item(scod1).BajaLog = False
                            nodx.Image = "PRES1"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionBajaLogica
                        Else
                            m_oPresupuestos.Item(scod1).BajaLog = True
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionDeshacerBaja
                        End If
                    End If
                End If
                Set oIBaseDatos = Nothing
                Set oPres1Seleccionado = Nothing
        
        Case "PRES2"
                    
                'Seleccionamos el presupuesto
                Dim oPres2Seleccionado As CPresConcep5Nivel2
                Set oPres2Seleccionado = Nothing
                Set oPres2Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel2
                oPres2Seleccionado.Pres0 = sdbcPartidaPres.Value
                oPres2Seleccionado.Pres1 = DevolverCod(nodx.Parent)
                oPres2Seleccionado.Cod = DevolverCod(nodx)

                
                Set oIBaseDatos = oPres2Seleccionado
                teserror = oIBaseDatos.IniciarEdicion
                If teserror.NumError <> TESnoerror Then
                    'Ha habido un error al iniciar la edicion
                    basErrores.TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres2Seleccionado = Nothing
                    If Me.Visible Then tvwEstrPres.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                'Si el presupuesto est� dado de baja preguntamos "Deshacer baja???"
                'sino preguntamos "Dar de baja???"
                Screen.MousePointer = vbNormal
                bBaja = False
                If (nodx.Image = "PRESBAJALOGICA") Then
                    irespuesta = oMensajes.DeshacerBajaLogicaPres(oPres0.Nom_Nivel2 & ": " & CStr(oPres2Seleccionado.Cod) & " (" & oPres2Seleccionado.Den & ")")
                Else
                    bBaja = True
                    irespuesta = oMensajes.DarBajaLogicaPres(oPres0.Nom_Nivel2 & ": " & CStr(oPres2Seleccionado.Cod) & " (" & oPres2Seleccionado.Den & ")")
                End If
                Screen.MousePointer = vbHourglass
                If irespuesta = vbYes Then
                    teserror = oPres2Seleccionado.BajaAltaLogica(Not oPres2Seleccionado.BajaLog)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        If Me.Visible Then tvwEstrPres.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        scod1 = DevolverCod(nodx.Parent)
                        scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
                        scod2 = DevolverCod(nodx)
                        scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
                        If (nodx.Image = "PRESBAJALOGICA") Then
                            m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).BajaLog = False
                            nodx.Image = "PRES2"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionBajaLogica
                        Else
                            m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).BajaLog = True
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionDeshacerBaja
                        End If
                    End If
                End If
                Set oIBaseDatos = Nothing
                Set oPres2Seleccionado = Nothing
                
        Case "PRES3"
                    
                'Seleccionamos el presupuesto
                Dim oPres3Seleccionado As CPresConcep5Nivel3
                Set oPres3Seleccionado = Nothing
                Set oPres3Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel3
                oPres3Seleccionado.Pres0 = sdbcPartidaPres.Value
                oPres3Seleccionado.Pres1 = DevolverCod(nodx.Parent.Parent)
                oPres3Seleccionado.Pres2 = DevolverCod(nodx.Parent)
                oPres3Seleccionado.Cod = DevolverCod(nodx)
                
                Set oIBaseDatos = oPres3Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
                
                If teserror.NumError <> TESnoerror Then
                    'Ha habido un error al iniciar la edicion
                    basErrores.TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres3Seleccionado = Nothing
                    If Me.Visible Then tvwEstrPres.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                'Si el presupuesto est� dado de baja preguntamos "Deshacer baja???"
                'sino preguntamos "Dar de baja???"
                Screen.MousePointer = vbNormal
                bBaja = False
                If (nodx.Image = "PRESBAJALOGICA") Then
                    irespuesta = oMensajes.DeshacerBajaLogicaPres(oPres0.Nom_Nivel3 & ": " & CStr(oPres3Seleccionado.Cod) & " (" & oPres3Seleccionado.Den & ")")
                Else
                    bBaja = True
                    irespuesta = oMensajes.DarBajaLogicaPres(oPres0.Nom_Nivel3 & ": " & CStr(oPres3Seleccionado.Cod) & " (" & oPres3Seleccionado.Den & ")")
                End If
                Screen.MousePointer = vbHourglass
                If irespuesta = vbYes Then
                    teserror = oPres3Seleccionado.BajaAltaLogica(Not oPres3Seleccionado.BajaLog)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        If Me.Visible Then tvwEstrPres.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        scod1 = DevolverCod(nodx.Parent.Parent)
                        scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent)
                        scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
                        scod3 = DevolverCod(nodx)
                        scod3 = scod3 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(scod3))
                        If (nodx.Image = "PRESBAJALOGICA") Then
                            m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).BajaLog = False
                            nodx.Image = "PRES3"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionBajaLogica
                        Else
                            m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).BajaLog = True
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionDeshacerBaja
                        End If
                    End If
                End If
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
                Set oPres3Seleccionado = Nothing
        
        Case "PRES4"
                    
                'Seleccionamos el presupuesto
                Dim oPres4Seleccionado  As CPresConcep5Nivel4
                Set oPres4Seleccionado = Nothing
                Set oPres4Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel4

                oPres4Seleccionado.Pres0 = sdbcPartidaPres.Value
                oPres4Seleccionado.Pres1 = DevolverCod(nodx.Parent.Parent.Parent)
                oPres4Seleccionado.Pres2 = DevolverCod(nodx.Parent.Parent)
                oPres4Seleccionado.Pres3 = DevolverCod(nodx.Parent)
                oPres4Seleccionado.Cod = DevolverCod(nodx)
                Set oIBaseDatos = oPres4Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
                
                If teserror.NumError <> TESnoerror Then
                    'Ha habido un error al iniciar la edicion
                    basErrores.TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres4Seleccionado = Nothing
                    If Me.Visible Then tvwEstrPres.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                'Si el presupuesto est� dado de baja preguntamos "Deshacer baja???"
                'sino preguntamos "Dar de baja???"
                Screen.MousePointer = vbNormal
                bBaja = False
                If (nodx.Image = "PRESBAJALOGICA") Then
                    irespuesta = oMensajes.DeshacerBajaLogicaPres(oPres0.Nom_Nivel4 & ": " & CStr(oPres4Seleccionado.Cod) & " (" & oPres4Seleccionado.Den & ")")
                Else
                    bBaja = True
                    irespuesta = oMensajes.DarBajaLogicaPres(oPres0.Nom_Nivel4 & ": " & CStr(oPres4Seleccionado.Cod) & " (" & oPres4Seleccionado.Den & ")")
                End If
                Screen.MousePointer = vbHourglass
                If irespuesta = vbYes Then
                    teserror = oPres4Seleccionado.BajaAltaLogica(Not oPres4Seleccionado.BajaLog)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        If Me.Visible Then tvwEstrPres.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent)
                        scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent.Parent)
                        scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
                        scod3 = DevolverCod(nodx.Parent)
                        scod3 = scod3 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(scod3))
                        scod4 = DevolverCod(nodx)
                        scod4 = scod4 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(scod4))
                        If (nodx.Image = "PRESBAJALOGICA") Then
                            m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4.Item(scod1 & scod2 & scod3 & scod4).BajaLog = False
                            nodx.Image = "PRES4"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionBajaLogica
                        Else
                            m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4.Item(scod1 & scod2 & scod3 & scod4).BajaLog = True
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H80000009 'color gris
                            cmdBajaLog.caption = m_sCaptionDeshacerBaja
                        End If
                    End If
                End If
                Set oIBaseDatos = Nothing
                Set oPres4Seleccionado = Nothing
        
        End Select
    End If

    'Si el check VER BAJAS LOGICAS no esta checked
    'y hemos dado de baja el presupuesto
    'entonces lo eliminamos del arbol, es decir, lo borramos de la pantalla
    If Not chkBajaLog.Value = vbChecked And nodx.Image = "PRESBAJALOGICA" Then
        ' Me posiciono en el siguiente
        If Not nodx.Previous Is Nothing Then
            Set nodSiguiente = nodx.Previous
        Else
            If Not nodx.Next Is Nothing Then
                Set nodSiguiente = nodx.Next
                Else
                    Set nodSiguiente = nodx.Parent
            End If
        End If
    
        
        'Lo elimino del arbol
        tvwEstrPres.Nodes.Remove (frmPresupuestos5.tvwEstrPres.selectedItem.Index)
        tvwEstrPres_NodeClick nodSiguiente
    
    Else
        tvwEstrPres_NodeClick nodx
    End If
    
    Screen.MousePointer = vbNormal
    
    If Me.Visible Then tvwEstrPres.SetFocus
    
    Set oIBaseDatos = Nothing
    Set oPres1Seleccionado = Nothing
End Sub

Private Sub cmdBuscar_Click()
    m_bSaltar = True
    frmPRESCon5Buscar.m_sPres0 = Me.sdbcPartidaPres.Value
    m_bSaltar = False
    frmPRESCon5Buscar.Show 1
    If Me.Visible Then tvwEstrPres.SetFocus
End Sub

''' <summary>
''' Ejecutar el evento click del bot�n Eliminar. Elimina una partida presupuestaria
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

Public Sub cmdEli_Click()
Dim nodx As MSComctlLib.node
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Set nodx = tvwEstrPres.selectedItem


If Not nodx Is Nothing Then

    Screen.MousePointer = vbHourglass

    Select Case Left(nodx.Tag, 5)

    Case "PRES1"

            'Accion = ACCPresCon4Nivel1Eli
            Dim oPres1Seleccionado As CPresConcep5Nivel1
            Set oPres1Seleccionado = Nothing
            Set oPres1Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel1
            oPres1Seleccionado.Pres0 = Me.sdbcPartidaPres.Value
            oPres1Seleccionado.Cod = DevolverCod(nodx)
            Set oIBaseDatos = oPres1Seleccionado
            teserror = oIBaseDatos.IniciarEdicion

            If teserror.NumError = TESnoerror Then
                Screen.MousePointer = vbNormal
                irespuesta = oMensajes.PreguntaEliminar(oPres0.Nom_Nivel1 & ": " & CStr(oPres1Seleccionado.Cod) & " (" & oPres1Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oPres1Seleccionado = Nothing
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        Screen.MousePointer = vbHourglass
                        EliminarPRESDeEstructura
                    End If
                    
                    
                End If
            Else
                'Ha habido un error al iniciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oPres1Seleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set oPres1Seleccionado = Nothing
    Case "PRES2"

            'Accion = ACCPresCon4Nivel2Eli
            Dim oPres2Seleccionado As CPresConcep5Nivel2
            Set oPres2Seleccionado = Nothing
            Set oPres2Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel2
            oPres2Seleccionado.Pres0 = Me.sdbcPartidaPres.Value
            oPres2Seleccionado.Pres1 = DevolverCod(nodx.Parent)
            oPres2Seleccionado.Cod = DevolverCod(nodx)

            Set oIBaseDatos = oPres2Seleccionado

            teserror = oIBaseDatos.IniciarEdicion

            If teserror.NumError = TESnoerror Then

                Screen.MousePointer = vbNormal
                irespuesta = oMensajes.PreguntaEliminar(oPres0.Nom_Nivel2 & ": " & CStr(oPres2Seleccionado.Cod) & " (" & oPres2Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        Screen.MousePointer = vbHourglass
                        EliminarPRESDeEstructura
                    End If
                End If
            

            Else
                'Ha habido un error al iniciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oPres2Seleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            Set oPres2Seleccionado = Nothing
            
        Case "PRES3"

            'Accion = ACCPresCon4Nivel3Eli
            Dim oPres3Seleccionado As CPresConcep5Nivel3
            Set oPres3Seleccionado = Nothing
            Set oPres3Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel3
            oPres3Seleccionado.Pres0 = sdbcPartidaPres.Value
            oPres3Seleccionado.Pres1 = DevolverCod(nodx.Parent.Parent)
            oPres3Seleccionado.Pres2 = DevolverCod(nodx.Parent)
            oPres3Seleccionado.Cod = DevolverCod(nodx)

            Set oIBaseDatos = oPres3Seleccionado

            teserror = oIBaseDatos.IniciarEdicion

            If teserror.NumError = TESnoerror Then

                Screen.MousePointer = vbNormal
                irespuesta = oMensajes.PreguntaEliminar(oPres0.Nom_Nivel3 & ": " & CStr(oPres3Seleccionado.Cod) & " (" & oPres3Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        Screen.MousePointer = vbHourglass
                        'RegistrarAccion ACCPresCon4Nivel3Eli, "CodPRES1:" & CStr(oPres3Seleccionado.CodPRES1) & "CodPRES2:" & CStr(oPres3Seleccionado.CodPRES2) & "CodPRES3:" & CStr(oPres3Seleccionado.Cod)
                        EliminarPRESDeEstructura
                    End If
                End If

            Else
                'Ha habido un error al inciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oPres3Seleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            Set oPres3Seleccionado = Nothing

        Case "PRES4"

            'Accion = ACCPresCon4Nivel4Eli
            Dim oPres4Seleccionado As CPresConcep5Nivel4
            Set oPres4Seleccionado = Nothing
            Set oPres4Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel4
            oPres4Seleccionado.Pres0 = Me.sdbcPartidaPres.Value
            oPres4Seleccionado.Pres1 = DevolverCod(nodx.Parent.Parent.Parent)
            oPres4Seleccionado.Pres2 = DevolverCod(nodx.Parent.Parent)
            oPres4Seleccionado.Pres3 = DevolverCod(nodx.Parent)
            oPres4Seleccionado.Cod = DevolverCod(nodx)

            Set oIBaseDatos = oPres4Seleccionado

            teserror = oIBaseDatos.IniciarEdicion

            If teserror.NumError = TESnoerror Then

                Screen.MousePointer = vbNormal
                irespuesta = oMensajes.PreguntaEliminar(oPres0.Nom_Nivel4 & ": " & CStr(oPres4Seleccionado.Cod) & " (" & oPres4Seleccionado.Den & ")")

                If irespuesta = vbYes Then
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Exit Sub
                    Else
                        Screen.MousePointer = vbHourglass
                        'RegistrarAccion ACCPresCon4Nivel4Eli, "CodPRES1:" & CStr(oPres4Seleccionado.CodPRES1) & "CodPRES2:" & CStr(oPres4Seleccionado.CodPRES2) & "CodPRES3:" & CStr(oPres4Seleccionado.CodPRES3) & "Cod:" & CStr(oPres4Seleccionado.Cod)
                        EliminarPRESDeEstructura
                        Screen.MousePointer = vbNormal
                    End If
                End If

            Else
                ' Ha habido un error al inciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oPres4Seleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            Set oPres4Seleccionado = Nothing
    End Select
    
    Screen.MousePointer = vbNormal
    Set oIBaseDatos = Nothing
End If
End Sub

Private Sub cmdElimArbol_Click()
Dim oIBaseDatos As IBaseDatos
Dim opres5 As cPresConcep5Nivel0
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit

 irespuesta = oMensajes.PreguntaEliminar(m_sPartida & " " & sdbcPartidaPres.Text)
 If irespuesta = vbYes Then
    Set opres5 = oFSGSRaiz.Generar_CPresConcep5Nivel0
    opres5.Cod = oPres0.Cod
    Set oIBaseDatos = opres5
    
    teserror = oIBaseDatos.EliminarDeBaseDatos
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
    Else
        sdbcPartidaPres = ""
        sdbcPartidaPres.Value = ""
    End If
 End If
 
 Set opres5 = Nothing
 Set oIBaseDatos = Nothing
End Sub

''' <summary>
''' Ejecutar el evento click del bot�n Modificar. Modifica la denominaci�n de un presupuesto
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Public Sub cmdModif_Click()
    Dim nodx As MSComctlLib.node
    Dim teserror As TipoErrorSummit


    Set nodx = tvwEstrPres.selectedItem

    If Not nodx Is Nothing Then

        Screen.MousePointer = vbHourglass

        Select Case Left(nodx.Tag, 5)

        Case "PRES1"
                Dim oPres1Seleccionado As CPresConcep5Nivel1
                Set oPres1Seleccionado = Nothing
                Set oPres1Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel1
                
                m_bSaltar = True
                If oPres0 Is Nothing Then
                    PartidaSeleccionada Me.sdbcPartidaPres.Value
                    oPres1Seleccionado.Pres0 = oPres0.Cod
                Else
                    oPres1Seleccionado.Pres0 = oPres0.Cod
                End If
                m_bSaltar = False
                
                oPres1Seleccionado.Cod = DevolverCod(nodx)

                Set oIBaseDatos = oPres1Seleccionado

                teserror = oIBaseDatos.IniciarEdicion

                If teserror.NumError = TESnoerror Then

                    frmPRESCon5Detalle.m_sAccion = "Modificar"
                    frmPRESCon5Detalle.m_iNivel = 1
                    frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel1
                    frmPRESCon5Detalle.m_sPres0 = oPres1Seleccionado.Pres0
                    frmPRESCon5Detalle.m_sCod = oPres1Seleccionado.Cod
                    frmPRESCon5Detalle.m_sDen = oPres1Seleccionado.Den

                    Screen.MousePointer = vbNormal
                    frmPRESCon5Detalle.Show 1

                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres1Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                Set oPres1Seleccionado = Nothing

        Case "PRES2"
                Dim oPres2Seleccionado As CPresConcep5Nivel2
                Set oPres2Seleccionado = Nothing
                Set oPres2Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel2
                If oPres0 Is Nothing Then
                    PartidaSeleccionada Me.sdbcPartidaPres.Value
                    oPres2Seleccionado.Pres0 = oPres0.Cod
                Else
                    oPres2Seleccionado.Pres0 = oPres0.Cod
                End If
                
                oPres2Seleccionado.Pres1 = DevolverCod(nodx.Parent)
                oPres2Seleccionado.Cod = DevolverCod(nodx)


                Set oIBaseDatos = oPres2Seleccionado

                teserror = oIBaseDatos.IniciarEdicion

                If teserror.NumError = TESnoerror Then

                    frmPRESCon5Detalle.m_sAccion = "Modificar"
                    frmPRESCon5Detalle.m_iNivel = 2
                    frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel2
                    frmPRESCon5Detalle.m_sPres0 = oPres2Seleccionado.Pres0
                    frmPRESCon5Detalle.m_sPres1 = oPres2Seleccionado.Pres1
                    frmPRESCon5Detalle.m_sCod = oPres2Seleccionado.Cod
                    frmPRESCon5Detalle.m_sDen = oPres2Seleccionado.Den

                    Screen.MousePointer = vbNormal
                    frmPRESCon5Detalle.Show 1

                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres2Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oPres2Seleccionado = Nothing

            Case "PRES3"
                Dim oPres3Seleccionado As CPresConcep5Nivel3
                Set oPres3Seleccionado = Nothing
                Set oPres3Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel3
                If oPres0 Is Nothing Then
                    PartidaSeleccionada Me.sdbcPartidaPres.Value
                    oPres3Seleccionado.Pres0 = oPres0.Cod
                Else
                    oPres3Seleccionado.Pres0 = oPres0.Cod
                End If
                oPres3Seleccionado.Pres1 = DevolverCod(nodx.Parent.Parent)
                oPres3Seleccionado.Pres2 = DevolverCod(nodx.Parent)
                oPres3Seleccionado.Cod = DevolverCod(nodx)

                Set oIBaseDatos = oPres3Seleccionado

                teserror = oIBaseDatos.IniciarEdicion

                If teserror.NumError = TESnoerror Then

                    frmPRESCon5Detalle.m_sAccion = "Modificar"
                    frmPRESCon5Detalle.m_iNivel = 3
                    frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel3
                    frmPRESCon5Detalle.m_sPres0 = oPres3Seleccionado.Pres0
                    frmPRESCon5Detalle.m_sPres1 = oPres3Seleccionado.Pres1
                    frmPRESCon5Detalle.m_sPres2 = oPres3Seleccionado.Pres2
                    frmPRESCon5Detalle.m_sCod = oPres3Seleccionado.Cod
                    frmPRESCon5Detalle.m_sDen = oPres3Seleccionado.Den

                    Screen.MousePointer = vbNormal
                    frmPRESCon5Detalle.Show 1

                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres3Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oPres3Seleccionado = Nothing
            Case "PRES4"
                Dim oPres4Seleccionado As CPresConcep5Nivel4
                Set oPres4Seleccionado = Nothing
                Set oPres4Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel4
                If oPres0 Is Nothing Then
                    PartidaSeleccionada Me.sdbcPartidaPres.Value
                    oPres4Seleccionado.Pres0 = oPres0.Cod
                Else
                    oPres4Seleccionado.Pres0 = oPres0.Cod
                End If
                oPres4Seleccionado.Pres1 = DevolverCod(nodx.Parent.Parent.Parent)
                oPres4Seleccionado.Pres2 = DevolverCod(nodx.Parent.Parent)
                oPres4Seleccionado.Pres3 = DevolverCod(nodx.Parent)
                oPres4Seleccionado.Cod = DevolverCod(nodx)

                Set oIBaseDatos = oPres4Seleccionado

                teserror = oIBaseDatos.IniciarEdicion

                If teserror.NumError = TESnoerror Then

                    frmPRESCon5Detalle.m_sAccion = "Modificar"
                    frmPRESCon5Detalle.m_iNivel = 4
                    frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel4
                    frmPRESCon5Detalle.m_sPres0 = oPres4Seleccionado.Pres0
                    frmPRESCon5Detalle.m_sPres1 = oPres4Seleccionado.Pres1
                    frmPRESCon5Detalle.m_sPres2 = oPres4Seleccionado.Pres2
                    frmPRESCon5Detalle.m_sPres3 = oPres4Seleccionado.Pres3
                    frmPRESCon5Detalle.m_sCod = oPres4Seleccionado.Cod
                    frmPRESCon5Detalle.m_sDen = oPres4Seleccionado.Den

                    Screen.MousePointer = vbNormal
                    frmPRESCon5Detalle.Show 1

                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres4Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oPres4Seleccionado = Nothing
        End Select
        
        Set oIBaseDatos = Nothing
        Screen.MousePointer = vbNormal
    End If
End Sub

''' <summary>
''' Modificacion de arbol de nivel0
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Click del boton; Tiempo m�ximo:0</remarks>

Private Sub cmdModifArbol_Click()
    frmPRESCon5Arbol.m_sAccion = "Modificar"
    frmPRESCon5Arbol.m_sCod = oPres0.Cod
    frmPRESCon5Arbol.WindowState = vbNormal
    frmPRESCon5Arbol.Show vbModal
    
    
    If sdbcPartidaPres = "" Then Exit Sub
    'Permisos usuario
    cmdElimArbol.Enabled = True
    cmdModifArbol.Enabled = True
    
    If picNavigate2.Visible = False Then Exit Sub

    m_bSaltar = True
    If m_bActualizar Then
        'If Not oPres0 Is Nothing Then If sdbcPartidaPres.Value = oPres0.Cod Then Exit Sub
        PartidaSeleccionada sdbcPartidaPres.Value
        If Not oPres0 Is Nothing Then
            GenerarEstructuraPresupuestos sdbcPartidaPres.Value, False
        End If
        
        m_bActualizar = False
        
    End If
    m_bSaltar = False
    
    If Me.Visible Then tvwEstrPres.SetFocus

    
End Sub

Private Sub cmdRestaurar_Click()
    Screen.MousePointer = vbHourglass
    If Not oPres0 Is Nothing Then
        GenerarEstructuraPresupuestos oPres0.Cod, False
        If Me.Visible Then tvwEstrPres.SetFocus
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Resize()
' If Me.WindowState = 0 Then     'Limitamos la reducci�n
'        If Me.Width < 9315 Then   'de tama�o de la ventana
'            Me.Width = 9315         'para que no se superpongan
'            If Me.Height < 6435 Then  'cuando no se maximiza ni
'                Me.Height = 6435       'minimiza la ventana,
'                Exit Sub               'WindowState=0, pues cuando esto
'            End If
'            Exit Sub               'unos controles sobre
'        End If                     'otros. S�lo lo hacemos
'        If Me.Height < 6435 Then  'cuando no se maximiza ni
'            Me.Height = 6435       'minimiza la ventana,
'            Exit Sub               'WindowState=0, pues cuando esto
'        End If                     'ocurre no se pueden cambiar las
'    End If                         'propiedades Form.Height y Form.Width
If Me.Height < 1750 Then Exit Sub
    If Me.WindowState <> 1 Then
        tvwEstrPres.Height = Me.Height - Me.picNavigate2.Height - 1300
        tvwEstrPres.Width = Me.Width - 250
        tvwEstrPres.Left = 75
        picNavigate2.Top = tvwEstrPres.Top + tvwEstrPres.Height + 50
            
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
Set oPres0 = Nothing
End Sub

Private Sub sdbcPartidaPres_Change()
    If m_bSaltar Then Exit Sub
    m_bCargarComboDesde = True
    Set oPres0 = Nothing
    tvwEstrPres.Nodes.clear
    cmdA�adir.Enabled = False
    cmdModif.Enabled = False
    cmdEli.Enabled = False
    cmdBajaLog.Enabled = False
    cmdElimArbol.Enabled = False
    cmdModifArbol.Enabled = False
End Sub

Private Sub sdbcPartidaPres_InitColumnProps()
    sdbcPartidaPres.DataFieldList = "Column 0"
    sdbcPartidaPres.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbcPartidaPres_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyBack Then
        Set oPres0 = Nothing
        tvwEstrPres.Nodes.clear
        cmdA�adir.Enabled = False
        cmdModif.Enabled = False
        cmdEli.Enabled = False
        cmdBajaLog.Enabled = False
        cmdElimArbol.Enabled = False
        cmdModifArbol.Enabled = False
    End If
End Sub

Private Sub sdbcPartidaPres_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPartidaPres.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPartidaPres.Rows - 1
            bm = sdbcPartidaPres.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPartidaPres.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcPartidaPres.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcPartidaPres_DropDown()
    
    Dim oPartida As cPresConcep5Nivel0
    
    Screen.MousePointer = vbHourglass
    
    Set m_oPartidas = Nothing
    Set m_oPartidas = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    
    sdbcPartidaPres.RemoveAll
'
'    If m_bCargarComboDesde Then
'        m_oPartidas.CargarPresupuestosConceptos5 Trim(sdbcPartidaPres.Text), , , , , , , m_sUsu, m_sPerfilCod
'    Else
        m_oPartidas.CargarPresupuestosConceptos5 , , , , , , , , m_sUsu, m_sPerfilCod
'    End If
'
    For Each oPartida In m_oPartidas
        sdbcPartidaPres.AddItem oPartida.Cod & Chr(9) & oPartida.Den
    Next
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPartidaPres_CloseUp()
    If sdbcPartidaPres.Value = "" Then Exit Sub
    If sdbcPartidaPres = "" Then Exit Sub
    'Permisos usuario
    cmdElimArbol.Enabled = True
    cmdModifArbol.Enabled = True
    
    If picNavigate2.Visible = False Then Exit Sub

    If Not oPres0 Is Nothing Then If sdbcPartidaPres.Value = oPres0.Cod Then Exit Sub
    PartidaSeleccionada sdbcPartidaPres.Value
    If Not oPres0 Is Nothing Then
        GenerarEstructuraPresupuestos sdbcPartidaPres.Value, False
    End If
    
    If Me.Visible Then tvwEstrPres.SetFocus
    
End Sub

Private Sub PartidaSeleccionada(ByVal sPres0 As String)
    
    Set oPres0 = oFSGSRaiz.Generar_CPresConcep5Nivel0
    oPres0.Cod = sPres0
    Set oIBaseDatos = oPres0
    oIBaseDatos.IniciarEdicion
    Set oIBaseDatos = Nothing

End Sub

Private Sub GenerarEstructuraPresupuestos(ByVal sPres0 As String, ByVal bOrdenadoPorDen As Boolean)
    Dim nodo As MSComctlLib.node

    tvwEstrPres.Nodes.clear

    Set nodo = tvwEstrPres.Nodes.Add(, , "Raiz ", oPres0.Den, "Raiz")
    nodo.Tag = "Raiz "

    nodo.Expanded = True

    Set m_oPresupuestos = oFSGSRaiz.Generar_CPresConceptos5Nivel1
    
    m_oPresupuestos.GenerarEstructuraPresupuestos , sPres0
    
    'Generamos el arbol de presupuestos que se ve en el formulario
    GenerarArbolPresupuestos
    tvwEstrPres_NodeClick nodo
    Exit Sub

Error:
    Set nodo = Nothing
    Resume Next
End Sub


Private Sub GenerarArbolPresupuestos()
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim opres1 As CPresConcep5Nivel1
    Dim oPRES2 As CPresConcep5Nivel2
    Dim oPRES3 As CPresConcep5Nivel3
    Dim oPRES4 As CPresConcep5Nivel4
    Dim nodx As MSComctlLib.node
    
    tvwEstrPres.Nodes.clear

    Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", oPres0.Den, "Raiz")
    nodx.Tag = "Raiz "

    nodx.Expanded = True

    For Each opres1 In m_oPresupuestos
        If Not ((Not chkBajaLog.Value = vbChecked) And opres1.BajaLog) Then
            scod1 = opres1.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(opres1.Cod))
            Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, opres1.Cod & " - " & opres1.Den, "PRES1")
            nodx.Tag = "PRES1" & opres1.Cod
            If opres1.BajaLog Then
                nodx.Image = "PRESBAJALOGICA"
                nodx.BackColor = &HFFFFFF    'color blanco
                
            End If
        End If

        For Each oPRES2 In opres1.PresConceptos5Nivel2
            If Not ((Not chkBajaLog.Value = vbChecked) And oPRES2.BajaLog) Then
                scod2 = oPRES2.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(oPRES2.Cod))
                Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                nodx.Tag = "PRES2" & oPRES2.Cod
                If oPRES2.BajaLog Then
                    nodx.Image = "PRESBAJALOGICA"
                    nodx.BackColor = &HFFFFFF    'color blanco
                End If
            End If

                For Each oPRES3 In oPRES2.PresConceptos5Nivel3
                    If Not ((Not chkBajaLog.Value = vbChecked) And oPRES3.BajaLog) Then
                        scod3 = oPRES3.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(oPRES3.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                        nodx.Tag = "PRES3" & oPRES3.Cod
                        If oPRES3.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &HFFFFFF    'color blanco
                        End If
                    End If

                    For Each oPRES4 In oPRES3.PresConceptos5Nivel4
                        If Not ((Not chkBajaLog.Value = vbChecked) And oPRES4.BajaLog) Then
                            scod4 = oPRES4.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(oPRES4.Cod))
                            Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                            nodx.Tag = "PRES4" & oPRES4.Cod
                            If oPRES4.BajaLog Then
                                nodx.Image = "PRESBAJALOGICA"
                                nodx.BackColor = &HFFFFFF    'color blanco
                            End If
                        End If
                    Next
                    Set oPRES4 = Nothing
                Next
                Set oPRES3 = Nothing
        Next
        Set oPRES2 = Nothing
    Next

    Set opres1 = Nothing
        
    Exit Sub
    
Error:
    Set nodx = Nothing
    Resume Next
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

    If node Is Nothing Then Exit Function

    DevolverCod = Right(node.Tag, Len(node.Tag) - 5)

End Function

Private Function DevolverDen(ByVal node As MSComctlLib.node) As Variant

If node Is Nothing Then Exit Function

    DevolverDen = Right(node.Text, (Len(node.Text) - (Len(node.Tag) - 5)))

End Function

Private Sub tvwEstrPres_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
   Dim nodx As MSComctlLib.node


    If Button = 2 Then
        Set nodx = tvwEstrPres.selectedItem

        If Not nodx Is Nothing Then


            PopupMenu MDI.mnuEstrPres5Cab

        End If
    End If
End Sub

Private Sub tvwEstrPres_NodeClick(ByVal node As MSComctlLib.node)
    Dim nodoSiguiente As MSComctlLib.node
    
    If (node.Image = "PRESBAJALOGICA") Then 'Si es baja l�gica
    
        cmdA�adir.Enabled = False
        cmdModif.Enabled = False
        cmdEli.Enabled = True
        cmdBajaLog.Enabled = True
        cmdBajaLog.caption = m_sCaptionDeshacerBaja
        MDI.mnuEstrPres5.Item(1).Enabled = False    'A�adir
        'MDI.mnuEstrPres5.Item(2).Enabled = False   'Separador
        MDI.mnuEstrPres5.Item(3).Enabled = False     'Modificar
        MDI.mnuEstrPres5.Item(4).Enabled = True     'Eliminar
        MDI.mnuEstrPres5.Item(6).Enabled = True
        MDI.mnuEstrPres5.Item(8).Enabled = True     'Detalle
        
        MDI.mnuEstrPres5.Item(2).Visible = True
        MDI.mnuEstrPres5.Item(3).Visible = True
        MDI.mnuEstrPres5.Item(4).Visible = True
        MDI.mnuEstrPres5.Item(5).Visible = False    'Baja l�gica
        MDI.mnuEstrPres5.Item(6).Visible = True     'Deshacer baja
        MDI.mnuEstrPres5.Item(7).Visible = True
        MDI.mnuEstrPres5.Item(8).Visible = True
        
        

        Select Case Left(node.Tag, 5)
            Case "Raiz "
                cmdEli.Enabled = False
            Case "PRES1"
                    If (node.Children > 0) Or (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuEstrPres5.Item(4).Enabled = True 'Deshacer baja logica
                        MDI.mnuEstrPres5.Item(6).Enabled = True 'Deshacer baja logica
                        
                        cmdEli.Enabled = False
                    End If
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuEstrPres5.Item(5).Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
            
            Case "PRES2"
                    If (node.Children > 0) Or (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuEstrPres5.Item(4).Enabled = False
                        cmdEli.Enabled = False
                    End If
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuEstrPres5.Item(6).Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
        
            Case "PRES3"
                    If (node.Children > 0) Or (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuEstrPres5.Item(4).Enabled = False
                        cmdEli.Enabled = False
                    End If
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuEstrPres5.Item(6).Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
                
            Case "PRES4"
                    If (node.Children > 0) Or (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuEstrPres5.Item(4).Enabled = False
                        cmdEli.Enabled = False
                    End If
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuEstrPres5.Item(6).Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
        End Select
    
    Else
        'si no es baja l�gica
        cmdA�adir.Enabled = True
        cmdModif.Enabled = True
        cmdEli.Enabled = True
        cmdBajaLog.Enabled = True
        cmdBajaLog.caption = m_sCaptionBajaLogica
        MDI.mnuEstrPres5.Item(1).Enabled = True     'A�adir
        MDI.mnuEstrPres5.Item(3).Enabled = True     'Modificar
        MDI.mnuEstrPres5.Item(4).Enabled = True     'Eliminar
        MDI.mnuEstrPres5.Item(5).Enabled = True     'Baja l�gica
        MDI.mnuEstrPres5.Item(5).Visible = True
        MDI.mnuEstrPres5.Item(6).Visible = False    ''Deshacer Baja l�gica
        MDI.mnuEstrPres5.Item(8).Enabled = True     'Detalle
        
        MDI.mnuEstrPres5.Item(2).Visible = True
        MDI.mnuEstrPres5.Item(3).Visible = True
        MDI.mnuEstrPres5.Item(4).Visible = True
        MDI.mnuEstrPres5.Item(5).Visible = True
        MDI.mnuEstrPres5.Item(7).Visible = True
        MDI.mnuEstrPres5.Item(8).Visible = True
        
        m_bNodoConHijos = False

        
        Select Case Left(node.Tag, 5)
            Case "Raiz "
                cmdModif.Enabled = False
                cmdEli.Enabled = False
                cmdBajaLog.Enabled = False
                MDI.mnuEstrPres5.Item(2).Visible = False
                MDI.mnuEstrPres5.Item(3).Visible = False
                MDI.mnuEstrPres5.Item(4).Visible = False
                MDI.mnuEstrPres5.Item(5).Visible = False
                MDI.mnuEstrPres5.Item(7).Visible = False
                MDI.mnuEstrPres5.Item(8).Visible = False
            Case "PRES1"
                    If (node.Children > 0) Then
                        MDI.mnuEstrPres5.Item(4).Enabled = False
                        'MDI.mnuEstrPres5.Item(5).Enabled = False
                        cmdEli.Enabled = False
                        Set nodoSiguiente = node.Child
                        Do While Not nodoSiguiente Is Nothing
                            If nodoSiguiente.Image <> "PRESBAJALOGICA" Then
                                MDI.mnuEstrPres5.Item(5).Enabled = False
                                cmdBajaLog.Enabled = False
                                m_bNodoConHijos = True
                                Exit Do
                            End If
                            Set nodoSiguiente = nodoSiguiente.Next
                        Loop
                    End If
            
            Case "PRES2"
                    If (node.Children > 0) Then
                        MDI.mnuEstrPres5.Item(4).Enabled = False
                        cmdEli.Enabled = False
                        Set nodoSiguiente = node.Child
                        Do While Not nodoSiguiente Is Nothing
                            If nodoSiguiente.Image <> "PRESBAJALOGICA" Then
                                MDI.mnuEstrPres5.Item(5).Enabled = False
                                cmdBajaLog.Enabled = False
                                m_bNodoConHijos = True
                                Exit Do
                            End If
                            Set nodoSiguiente = nodoSiguiente.Next
                        Loop
                    End If

            
            Case "PRES3"
                    If (node.Children > 0) Then
                        'MDI.mnuEstrPresConcep4N1.Item(4).Enabled = False
                        cmdEli.Enabled = False
                        Set nodoSiguiente = node.Child
                        Do While Not nodoSiguiente Is Nothing
                            If nodoSiguiente.Image <> "PRESBAJALOGICA" Then
                                MDI.mnuEstrPres5.Item(6).Visible = False
                                cmdBajaLog.Enabled = False
                                m_bNodoConHijos = True
                                Exit Do
                            End If
                            Set nodoSiguiente = nodoSiguiente.Next
                        Loop
                    End If
                
            Case "PRES4"
                cmdA�adir.Enabled = False
                MDI.mnuEstrPres5.Item(1).Enabled = False
                If (node.Children > 0) Then
                    'MDI.mnuEstrPresConcep4N4.Item(2).Enabled = False
                    cmdEli.Enabled = False
                    Set nodoSiguiente = node.Child
                    Do While Not nodoSiguiente Is Nothing
                        If nodoSiguiente.Image <> "PRESBAJALOGICA" Then
                            MDI.mnuEstrPres5.Item(6).Visible = False
                            cmdBajaLog.Enabled = False
                            m_bNodoConHijos = True
                            Exit Do
                        End If
                        Set nodoSiguiente = nodoSiguiente.Next
                    Loop
                End If

        End Select
    End If
End Sub

Private Function EliminarPRESDeEstructura()
Dim nod As node
Dim nodSiguiente As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

    Set nod = tvwEstrPres.selectedItem
    Select Case Left(nod.Tag, 5)
    
        Case "PRES1"
    
                scod1 = DevolverCod(nod)
                scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
    
                'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
                m_oPresupuestos.Remove (scod1)
    
        Case "PRES2"
    
                scod1 = DevolverCod(nod.Parent)
                scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
                scod2 = DevolverCod(nod)
                scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
    
                'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
                m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Remove (scod1 & scod2)
    
        Case "PRES3"
    
                scod1 = DevolverCod(nod.Parent.Parent)
                scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
                scod2 = DevolverCod(nod.Parent)
                scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
                scod3 = DevolverCod(nod)
                scod3 = scod3 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(scod3))
    
                'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
                m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Remove (scod1 & scod2 & scod3)
    
        Case "PRES4"
    
                scod1 = DevolverCod(nod.Parent.Parent.Parent)
                scod1 = scod1 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(scod1))
                scod2 = DevolverCod(nod.Parent.Parent)
                scod2 = scod2 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(scod2))
                scod3 = DevolverCod(nod.Parent)
                scod3 = scod3 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(scod3))
                scod4 = DevolverCod(nod)
                scod4 = scod4 & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(scod4))
    
                'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
                m_oPresupuestos.Item(scod1).PresConceptos5Nivel2.Item(scod1 & scod2).PresConceptos5Nivel3.Item(scod1 & scod2 & scod3).PresConceptos5Nivel4.Remove (scod1 & scod2 & scod3 & scod4)
    
    End Select


    ' Me posiciono en el siguiente
    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
            Else
                Set nodSiguiente = nod.Parent
        End If
    End If


    'Lo elimino del arbol
    tvwEstrPres.Nodes.Remove (tvwEstrPres.selectedItem.Index)
    tvwEstrPres_NodeClick nodSiguiente

    nodSiguiente.Selected = True
    If Me.Visible Then tvwEstrPres.SetFocus

End Function

Public Sub CargarArbolConNodo(sPres0 As String, sCodNodo As String)
''LLamada Sub desde frmPRESCon5Buscar.frm

Dim nodx As MSComctlLib.node

On Error GoTo NoSeEncuentra
    
    If sdbcPartidaPres.Text = "" Then
        'Cargar el arbol del nodo
        PartidaSeleccionada sPres0
        If Not oPres0 Is Nothing Then
            m_bSaltar = True
            sdbcPartidaPres.Value = sPres0
            sdbcPartidaPres.Text = oPres0.Den
            m_bSaltar = False
            GenerarEstructuraPresupuestos sPres0, False
        End If
    End If
    
    Set nodx = tvwEstrPres.Nodes(sCodNodo)
    nodx.Selected = True
    nodx.EnsureVisible
    
    tvwEstrPres_NodeClick nodx
    
    Exit Sub
    
NoSeEncuentra:
    'oMensajes.Concepto4Nuevo
End Sub

''' <summary>
''' Muestra el detalle de un nodo
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: MDI-->mnuEstrPres5_click; Tiempo m�ximo:0</remarks>

Public Sub MostarDetalle()
 Dim nodx As MSComctlLib.node
    Dim teserror As TipoErrorSummit


    Set nodx = tvwEstrPres.selectedItem

    If Not nodx Is Nothing Then

        Screen.MousePointer = vbHourglass

        Select Case Left(nodx.Tag, 5)

        Case "PRES1"
'                Accion = ACCPresCon4Nivel1Mod

                Dim oPres1Seleccionado As CPresConcep5Nivel1
                Set oPres1Seleccionado = Nothing
                Set oPres1Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel1
                oPres1Seleccionado.Pres0 = Me.sdbcPartidaPres.Value
                oPres1Seleccionado.Cod = DevolverCod(nodx)

                Set oIBaseDatos = oPres1Seleccionado

                teserror = oIBaseDatos.IniciarEdicion

                If teserror.NumError = TESnoerror Then

                    frmPRESCon5Detalle.m_sAccion = "Detalle"
                    frmPRESCon5Detalle.m_iNivel = 1
                    frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel1
                    frmPRESCon5Detalle.m_sPres0 = oPres1Seleccionado.Pres0
                    frmPRESCon5Detalle.m_sCod = oPres1Seleccionado.Cod
                    frmPRESCon5Detalle.m_sDen = oPres1Seleccionado.Den

                    Screen.MousePointer = vbNormal
                    frmPRESCon5Detalle.Show 1

                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres1Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                Set oPres1Seleccionado = Nothing

        Case "PRES2"
                Dim oPres2Seleccionado As CPresConcep5Nivel2
                Set oPres2Seleccionado = Nothing
                Set oPres2Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel2
                oPres2Seleccionado.Pres0 = Me.sdbcPartidaPres.Value
                oPres2Seleccionado.Pres1 = DevolverCod(nodx.Parent)
                oPres2Seleccionado.Cod = DevolverCod(nodx)


                Set oIBaseDatos = oPres2Seleccionado

                teserror = oIBaseDatos.IniciarEdicion

                If teserror.NumError = TESnoerror Then

                    frmPRESCon5Detalle.m_sAccion = "Detalle"
                    frmPRESCon5Detalle.m_iNivel = 2
                    frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel2
                    frmPRESCon5Detalle.m_sPres0 = oPres2Seleccionado.Pres0
                    frmPRESCon5Detalle.m_sPres1 = oPres2Seleccionado.Pres1
                    frmPRESCon5Detalle.m_sCod = oPres2Seleccionado.Cod
                    frmPRESCon5Detalle.m_sDen = oPres2Seleccionado.Den

                    Screen.MousePointer = vbNormal
                    frmPRESCon5Detalle.Show 1

                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres2Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oPres2Seleccionado = Nothing

            Case "PRES3"
                Dim oPres3Seleccionado As CPresConcep5Nivel3
                Set oPres3Seleccionado = Nothing
                Set oPres3Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel3
                oPres3Seleccionado.Pres0 = Me.sdbcPartidaPres.Value
                oPres3Seleccionado.Pres1 = DevolverCod(nodx.Parent.Parent)
                oPres3Seleccionado.Pres2 = DevolverCod(nodx.Parent)
                oPres3Seleccionado.Cod = DevolverCod(nodx)

                Set oIBaseDatos = oPres3Seleccionado

                teserror = oIBaseDatos.IniciarEdicion

                If teserror.NumError = TESnoerror Then

                    frmPRESCon5Detalle.m_sAccion = "Detalle"
                    frmPRESCon5Detalle.m_iNivel = 3
                    frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel3
                    frmPRESCon5Detalle.m_sPres0 = oPres3Seleccionado.Pres0
                    frmPRESCon5Detalle.m_sPres1 = oPres3Seleccionado.Pres1
                    frmPRESCon5Detalle.m_sPres2 = oPres3Seleccionado.Pres2
                    frmPRESCon5Detalle.m_sCod = oPres3Seleccionado.Cod
                    frmPRESCon5Detalle.m_sDen = oPres3Seleccionado.Den

                    Screen.MousePointer = vbNormal
                    frmPRESCon5Detalle.Show 1

                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres3Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oPres3Seleccionado = Nothing
            Case "PRES4"
                Dim oPres4Seleccionado As CPresConcep5Nivel4
                Set oPres4Seleccionado = Nothing
                Set oPres4Seleccionado = oFSGSRaiz.Generar_CPresConcep5Nivel4
                oPres4Seleccionado.Pres0 = Me.sdbcPartidaPres.Value
                oPres4Seleccionado.Pres1 = DevolverCod(nodx.Parent.Parent.Parent)
                oPres4Seleccionado.Pres2 = DevolverCod(nodx.Parent.Parent)
                oPres4Seleccionado.Pres3 = DevolverCod(nodx.Parent)
                oPres4Seleccionado.Cod = DevolverCod(nodx)

                Set oIBaseDatos = oPres4Seleccionado

                teserror = oIBaseDatos.IniciarEdicion

                If teserror.NumError = TESnoerror Then

                    frmPRESCon5Detalle.m_sAccion = "Detalle"
                    frmPRESCon5Detalle.m_iNivel = 4
                    frmPRESCon5Detalle.m_sTitulo = oPres0.Nom_Nivel4
                    frmPRESCon5Detalle.m_sPres0 = oPres4Seleccionado.Pres0
                    frmPRESCon5Detalle.m_sPres1 = oPres4Seleccionado.Pres1
                    frmPRESCon5Detalle.m_sPres2 = oPres4Seleccionado.Pres2
                    frmPRESCon5Detalle.m_sPres3 = oPres4Seleccionado.Pres3
                    frmPRESCon5Detalle.m_sCod = oPres4Seleccionado.Cod
                    frmPRESCon5Detalle.m_sDen = oPres4Seleccionado.Den

                    Screen.MousePointer = vbNormal
                    frmPRESCon5Detalle.Show 1

                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres4Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oPres4Seleccionado = Nothing
        End Select
        
        Set oIBaseDatos = Nothing
        Screen.MousePointer = vbNormal
    End If
End Sub
