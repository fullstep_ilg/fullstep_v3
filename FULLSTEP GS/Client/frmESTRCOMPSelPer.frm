VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmESTRCOMPSelPer 
   BackColor       =   &H00808000&
   Caption         =   "A�adir comprador+"
   ClientHeight    =   4230
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5235
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRCOMPSelPer.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4230
   ScaleWidth      =   5235
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   435
      Left            =   1080
      ScaleHeight     =   435
      ScaleWidth      =   3435
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3840
      Width           =   3435
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1725
         TabIndex        =   4
         Top             =   0
         Width           =   1110
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   375
         TabIndex        =   3
         Top             =   0
         Width           =   1110
      End
   End
   Begin VB.TextBox txtPersona 
      Height          =   285
      Left            =   980
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   200
      Width           =   4105
   End
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   3105
      Left            =   120
      TabIndex        =   5
      Top             =   600
      Width           =   4980
      _ExtentX        =   8784
      _ExtentY        =   5477
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPSelPer.frx":0CB2
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPSelPer.frx":0D51
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPSelPer.frx":0E01
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPSelPer.frx":0EB1
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPSelPer.frx":0F61
            Key             =   "Persona"
            Object.Tag             =   "Persona"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPSelPer.frx":0FCC
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRCOMPSelPer.frx":135F
            Key             =   "PerCesta"
            Object.Tag             =   "PerCesta"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblPersona 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "DPersona : "
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   840
   End
End
Attribute VB_Name = "frmESTRCOMPSelPer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de restricciones
Private bModifEstr As Boolean
Private bRuo As Boolean
Private bRDep As Boolean

Private oPersN0 As CPersonas
Private oPersN1 As CPersonas
Private oPersN2 As CPersonas
Private oPersN3 As CPersonas
Private oDepAsoc As CDepAsociado

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRCOMP_SELPER, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblPersona.caption = Ador(0).Value

        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim oPersona As CPersona
    Dim Cod As String
    Dim oDepar As CDepartamento
    Dim CodDep As String



    'Obtiene los datos de la persona a a�adir o modificar
    If Left(tvwestrorg.selectedItem.Tag, 3) <> "PER" Then Exit Sub
    Cod = Mid(tvwestrorg.selectedItem.Tag, 5)
    
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And (bRuo Or bRDep) Then
        Set oPersona = oFSGSRaiz.Generar_CPersona
        oPersona.Cod = Cod
        teserror = oPersona.CargarTodosLosDatos
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Set oPersona = Nothing
            Exit Sub
        End If
    Else
        If Not oPersN0(Cod) Is Nothing Then
            Set oPersona = oPersN0(Cod)
        ElseIf Not oPersN1(Cod) Is Nothing Then
            Set oPersona = oPersN1(Cod)
        ElseIf Not oPersN2(Cod) Is Nothing Then
            Set oPersona = oPersN2(Cod)
        ElseIf Not oPersN3(Cod) Is Nothing Then
            Set oPersona = oPersN3(Cod)
        End If
    End If
    
    If oPersona Is Nothing Then Exit Sub
    
    'Dependiendo de la acci�n se har� una cosa u otra
    'Si A�adir comprador
    
    Select Case frmESTRCOMP.Accion
        
        Case ACCCompAnya

            frmESTRCOMP.oCompradorSeleccionado.Cod = Cod
            frmESTRCOMP.oCompradorSeleccionado.nombre = oPersona.nombre
            frmESTRCOMP.oCompradorSeleccionado.Apel = oPersona.Apellidos
            frmESTRCOMP.oCompradorSeleccionado.Fax = oPersona.Fax
            frmESTRCOMP.oCompradorSeleccionado.mail = oPersona.mail
            frmESTRCOMP.oCompradorSeleccionado.Tfno = oPersona.Tfno
            frmESTRCOMP.oCompradorSeleccionado.Tfno2 = oPersona.Tfno2
            frmESTRCOMP.oCompradorSeleccionado.UON1 = oPersona.UON1
            frmESTRCOMP.oCompradorSeleccionado.UON2 = oPersona.UON2
            frmESTRCOMP.oCompradorSeleccionado.UON3 = oPersona.UON3
            frmESTRCOMP.oCompradorSeleccionado.Dep = oPersona.CodDep
            frmESTRCOMP.oCompradorSeleccionado.Cargo = oPersona.Cargo

            Screen.MousePointer = vbHourglass
            teserror = frmESTRCOMP.oIBaseDatos.AnyadirABaseDatos
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESnoerror Then
                frmESTRCOMP.AnyadirCompradorAEstructura Cod, oPersona.nombre, oPersona.Apellidos
                '******* Registrar accion ***************
                    If gParametrosGenerales.gbActivLog Then
                        oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCCompAnya, "Cod:" & Cod
                    End If
                '*****************************************
            Else
                TratarError teserror
                Exit Sub
            End If

    Case ACCCompMod  'Modifica el comprador

            frmESTRCOMP.oCompradorSeleccionado.nombre = oPersona.nombre
            frmESTRCOMP.oCompradorSeleccionado.Apel = oPersona.Apellidos
            frmESTRCOMP.oCompradorSeleccionado.Fax = oPersona.Fax
            frmESTRCOMP.oCompradorSeleccionado.mail = oPersona.mail
            frmESTRCOMP.oCompradorSeleccionado.Tfno = oPersona.Tfno
            frmESTRCOMP.oCompradorSeleccionado.Tfno2 = oPersona.Tfno2
            frmESTRCOMP.oCompradorSeleccionado.UON1 = oPersona.UON1
            frmESTRCOMP.oCompradorSeleccionado.UON2 = oPersona.UON2
            frmESTRCOMP.oCompradorSeleccionado.UON3 = oPersona.UON3
            frmESTRCOMP.oCompradorSeleccionado.Dep = oPersona.CodDep
            frmESTRCOMP.oCompradorSeleccionado.Cargo = oPersona.Cargo
            
            Screen.MousePointer = vbHourglass
            teserror = frmESTRCOMP.oIBaseDatos.CambiarCodigo(Cod)
            Screen.MousePointer = vbNormal

            If teserror.NumError = TESnoerror Then

                frmESTRCOMP.ModificarCompradorEnEstructura

                 '******* Registrar accion ***************
                    If gParametrosGenerales.gbActivLog Then
                        oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCCompMod, "Cod:" & Cod
                    End If
                '*****************************************
            Else
                TratarError teserror
                Exit Sub
            End If

    End Select
    
    'Cierra los recordsets
    Set oPersona = Nothing
    
    Set frmESTRCOMP.oIBaseDatos = Nothing
    Set frmESTRCOMP.oCompradorSeleccionado = Nothing

    frmESTRCOMP.Accion = ACCCompCon

    Unload Me
    
End Sub


Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
'C�digos
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Departamentos asociados
Dim oDepsAsocN0 As CDepAsociados
Dim oDepsAsocN1 As CDepAsociados
Dim oDepsAsocN2 As CDepAsociados
Dim oDepsAsocN3 As CDepAsociados

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3

' Personas
Dim oPer As CPersona

' Otras
Dim nodx As Node


    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
     
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    
    Set oPersN0 = oFSGSRaiz.generar_CPersonas
    Set oPersN1 = oFSGSRaiz.generar_CPersonas
    Set oPersN2 = oFSGSRaiz.generar_CPersonas
    Set oPersN3 = oFSGSRaiz.generar_CPersonas
         
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And (bRuo Or bRDep) Then
        
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, 0, , , , bOrdenadoPorDen, False
        
        ' Cargamos las personas  de esos departamentos.
        
        For Each oDepAsoc In oDepsAsocN0
             oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, True
        Next
            
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, , False, , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, 1, , , , bOrdenadoPorDen, False
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, 1, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, 2, , , , bOrdenadoPorDen, False
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, 2, , , , bOrdenadoPorDen, False
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRuo, bRDep, 3, , , , bOrdenadoPorDen, False
                
                ' Cargamos las personas  de esos departamentos.
                'Personas
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, True
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                   oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, True
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, True
                    
                Next
        End Select
        
        
    Else
        
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
        oPersN0.CargarTodasLasPersonas , , , , 0, , True, , , False, True
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, True
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, True
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, True
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, True
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, True
                oPersN3.CargarTodasLasPersonas , , , , 3, , True, , , False, True
                
        End Select
        
        
    End If
    
        
        
   '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
            
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
            
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
            
    Next
    
    ' Departamentos
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And (bRuo Or bRDep) Then
    
        For Each oDepAsoc In oDepsAsocN0
            
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.codEqp) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
            Next
        
        Next
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.codEqp) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
            Next
        
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.codEqp) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
            Next
       
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.codEqp) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerCesta")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
        Next
    
    Else
    
        'Departamentos
            
            For Each oDepAsoc In oDepsAsocN0
            
                scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            
            Next
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            
        Next
                
        'Personas
        For Each oPer In oPersN0
            scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            nodx.Tag = "PER0" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN1
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            nodx.Tag = "PER1" & CStr(oPer.Cod)
            
        Next
        
        For Each oPer In oPersN2
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            nodx.Tag = "PER2" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN3
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
            scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            nodx.Tag = "PER3" & CStr(oPer.Cod)
        Next
    End If
    
    'cierra los recordsets
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    
    Set oPer = Nothing
    
End Sub

''' <summary>Configuraci�n de la seguridad de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGModificar)) Is Nothing) Then
        bModifEstr = True
    End If
    
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestUO)) Is Nothing) Then
            bRuo = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestDep)) Is Nothing) Then
            bRDep = True
        End If
    End If
End Sub


Private Sub Form_Load()
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    ' Cargar los captions dependiendo del idioma
    CargarRecursos

    ' Configurar la seguridad
    ConfigurarSeguridad
    
    ' Generar la estructura de la organizacion en el treeview
    GenerarEstructuraOrg (False)

    'si se va a modificar un comprador lo a�adimos al �rbol y nos posicionamos en �l
    If frmESTRCOMP.Accion = ACCCompMod Then
        SeleccionarCompMod
    End If
    
    
End Sub

Private Sub Form_Resize()
    Arrange
End Sub


Private Sub Form_Unload(Cancel As Integer)
    'Cierra los recordsets
    Set oDepAsoc = Nothing
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
End Sub

Private Sub tvwestrorg_NodeClick(ByVal Node As MSComctlLib.Node)
    'Carga en el text box el nombre de la persona seleccionada
    If Left(Node.Tag, 3) = "PER" Then
        txtPersona.Text = Node.Text
    Else
        txtPersona.Text = ""
    End If
End Sub


Private Sub Arrange()
    'Redimensiona la grid
    If Me.Height < 2000 Then Exit Sub
    If Me.Width < 2000 Then Exit Sub
    
    tvwestrorg.Width = Me.Width - 375
    tvwestrorg.Height = Me.Height - 1530
    
    txtPersona.Width = Me.Width - 1230
    
    picEdit.Left = (Me.Width / 2) - 1597
    picEdit.Top = tvwestrorg.Top + tvwestrorg.Height + 135
End Sub

Private Sub SeleccionarCompMod()
'Si es una modificaci�n a�ade el comprador y se posiciona en �l
Dim sInd As String
Dim scod As String
Dim nodx As Node

    If frmESTRCOMP.oCompradorSeleccionado Is Nothing Then Exit Sub
    
    'a�ade el comprador al �rbol
    sInd = 0
    If Not IsNull(frmESTRCOMP.oCompradorSeleccionado.UON1) And frmESTRCOMP.oCompradorSeleccionado.UON1 <> "" Then
        scod = frmESTRCOMP.oCompradorSeleccionado.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(frmESTRCOMP.oCompradorSeleccionado.UON1))
        sInd = 1
    End If
    If Not IsNull(frmESTRCOMP.oCompradorSeleccionado.UON2) And frmESTRCOMP.oCompradorSeleccionado.UON2 <> "" Then
        scod = scod & frmESTRCOMP.oCompradorSeleccionado.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(frmESTRCOMP.oCompradorSeleccionado.UON2))
        sInd = 2
    End If
    If Not IsNull(frmESTRCOMP.oCompradorSeleccionado.UON3) And frmESTRCOMP.oCompradorSeleccionado.UON3 <> "" Then
        scod = scod & frmESTRCOMP.oCompradorSeleccionado.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(frmESTRCOMP.oCompradorSeleccionado.UON3))
        sInd = 3
    End If
    scod = scod & frmESTRCOMP.oCompradorSeleccionado.Dep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(frmESTRCOMP.oCompradorSeleccionado.Dep))

    If Trim$(scod) <> "" Then
        Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod, tvwChild, "PERS" & CStr(frmESTRCOMP.oCompradorSeleccionado.Cod), CStr(frmESTRCOMP.oCompradorSeleccionado.Cod) & " - " & frmESTRCOMP.oCompradorSeleccionado.Apel & " " & frmESTRCOMP.oCompradorSeleccionado.nombre, "Persona")
        nodx.Tag = "PER" & sInd & CStr(frmESTRCOMP.oCompradorSeleccionado.Cod)
        nodx.Selected = True
    End If

End Sub
