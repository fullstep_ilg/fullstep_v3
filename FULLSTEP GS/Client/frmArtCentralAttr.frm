VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmArtCentralAttr 
   Caption         =   "dA�adir Art�culo central"
   ClientHeight    =   9810
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   10770
   Icon            =   "frmArtCentralAttr.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9810
   ScaleWidth      =   10770
   Begin VB.CommandButton cmdVolver 
      Caption         =   "<<             "
      Height          =   315
      Left            =   4800
      TabIndex        =   29
      Top             =   8880
      Width           =   1095
   End
   Begin VB.CommandButton cmdFinalizar 
      Caption         =   "d&Finalizar"
      Height          =   315
      Left            =   6000
      TabIndex        =   28
      Top             =   8880
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "d&Cancelar"
      Height          =   315
      Left            =   3600
      TabIndex        =   27
      Top             =   8880
      Width           =   1095
   End
   Begin VB.CommandButton cmdIncluirAtributo 
      Height          =   345
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   6360
      Width           =   495
   End
   Begin VB.CommandButton cmdExcluirAtributo 
      Height          =   345
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":019B
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   6765
      Width           =   495
   End
   Begin VB.CommandButton cmdIncluirTodosAtributos 
      Caption         =   "ALL"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":01ED
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   7245
      Width           =   495
   End
   Begin VB.CommandButton cmdExcluirTodosAtributos 
      Caption         =   "ALL"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":023E
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   7800
      Width           =   495
   End
   Begin VB.CommandButton cmdIncluirFichEspArt 
      Height          =   345
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":0290
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   4050
      Width           =   495
   End
   Begin VB.CommandButton cmdExcluirFichEspArt 
      Height          =   345
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":02E1
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   4455
      Width           =   495
   End
   Begin VB.CommandButton cmdIncluirEspArt 
      Height          =   345
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":0333
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   2760
      Width           =   495
   End
   Begin VB.CommandButton cmdExcluirEspArt 
      Height          =   345
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":0384
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   3240
      Width           =   495
   End
   Begin VB.CommandButton cmdIncluirTodosFichArt 
      Caption         =   "ALL"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":03D6
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   4935
      Width           =   495
   End
   Begin VB.CommandButton cmdExcluirTodosFichArt 
      Caption         =   "ALL"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   5160
      Picture         =   "frmArtCentralAttr.frx":0427
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   5520
      Width           =   495
   End
   Begin VB.Frame Frame2 
      Height          =   6495
      Left            =   5880
      TabIndex        =   5
      Top             =   2160
      Width           =   4695
      Begin VB.TextBox txtEspecificaciones 
         Height          =   1455
         Index           =   1
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   13
         Top             =   480
         Width           =   4215
      End
      Begin MSComctlLib.ListView lstvwArticuloEsp 
         Height          =   1365
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   2310
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   2408
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Path"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
         Height          =   1935
         Index           =   1
         Left            =   120
         TabIndex        =   10
         Top             =   4440
         Width           =   4245
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         Col.Count       =   3
         stylesets.count =   9
         stylesets(0).Name=   "Atributos"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmArtCentralAttr.frx":0479
         stylesets(1).Name=   "IntOK"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   -2147483633
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmArtCentralAttr.frx":0532
         stylesets(2).Name=   "Normal"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmArtCentralAttr.frx":054E
         stylesets(3).Name=   "ActiveRow"
         stylesets(3).ForeColor=   16777215
         stylesets(3).BackColor=   -2147483647
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmArtCentralAttr.frx":056A
         stylesets(3).AlignmentText=   0
         stylesets(4).Name=   "IntHeader"
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmArtCentralAttr.frx":0586
         stylesets(4).AlignmentText=   0
         stylesets(4).AlignmentPicture=   0
         stylesets(5).Name=   "Adjudica"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmArtCentralAttr.frx":06DE
         stylesets(6).Name=   "styEspAdjSi"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmArtCentralAttr.frx":0A53
         stylesets(7).Name=   "IntKO"
         stylesets(7).ForeColor=   16777215
         stylesets(7).BackColor=   255
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmArtCentralAttr.frx":0A6F
         stylesets(8).Name=   "HayImpuestos"
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmArtCentralAttr.frx":0AEC
         stylesets(8).AlignmentText=   2
         stylesets(8).AlignmentPicture=   2
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   2
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         MaxSelectedRows =   0
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "CODIGO"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7488
         _ExtentY        =   3413
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAtributos 
         Caption         =   "dAtributos"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   4080
         Width           =   2415
      End
      Begin VB.Label lblArticuloEsp 
         Caption         =   "dEspecificaciones del art�culo:"
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   3795
      End
      Begin VB.Label lblArticuloFich 
         Caption         =   "dArchivos adjuntos:"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   1
         Left            =   180
         TabIndex        =   7
         Top             =   2040
         Width           =   2580
      End
   End
   Begin VB.Frame Frame1 
      Height          =   6495
      Left            =   240
      TabIndex        =   0
      Top             =   2160
      Width           =   4695
      Begin VB.TextBox txtEspecificaciones 
         Height          =   1455
         Index           =   0
         Left            =   120
         TabIndex        =   12
         Top             =   480
         Width           =   4215
      End
      Begin MSComctlLib.ListView lstvwArticuloEsp 
         Height          =   1365
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   2310
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   2408
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "Fichero"
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "Tamanyo"
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "Path"
            Text            =   "Path"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Key             =   "Comentario"
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Key             =   "Fecha"
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Key             =   "ESP"
            Text            =   "ESP"
            Object.Width           =   2540
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
         Height          =   1935
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   4440
         Width           =   4245
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         Col.Count       =   3
         stylesets.count =   9
         stylesets(0).Name=   "Atributos"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmArtCentralAttr.frx":0E61
         stylesets(1).Name=   "IntOK"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   -2147483633
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmArtCentralAttr.frx":0F1A
         stylesets(2).Name=   "Normal"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmArtCentralAttr.frx":0F36
         stylesets(3).Name=   "ActiveRow"
         stylesets(3).ForeColor=   16777215
         stylesets(3).BackColor=   -2147483647
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmArtCentralAttr.frx":0F52
         stylesets(3).AlignmentText=   0
         stylesets(4).Name=   "IntHeader"
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmArtCentralAttr.frx":0F6E
         stylesets(4).AlignmentText=   0
         stylesets(4).AlignmentPicture=   0
         stylesets(5).Name=   "Adjudica"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmArtCentralAttr.frx":10C6
         stylesets(6).Name=   "styEspAdjSi"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmArtCentralAttr.frx":143B
         stylesets(7).Name=   "IntKO"
         stylesets(7).ForeColor=   16777215
         stylesets(7).BackColor=   255
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmArtCentralAttr.frx":1457
         stylesets(8).Name=   "HayImpuestos"
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmArtCentralAttr.frx":14D4
         stylesets(8).AlignmentText=   2
         stylesets(8).AlignmentPicture=   2
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   2
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         MaxSelectedRows =   0
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "CODIGO"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "DEN"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7488
         _ExtentY        =   3413
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAtributos 
         Caption         =   "dAtributos"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   4080
         Width           =   2415
      End
      Begin VB.Label lblArticuloFich 
         Caption         =   "dArchivos adjuntos:"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   180
         TabIndex        =   3
         Top             =   2040
         Width           =   2580
      End
      Begin VB.Label lblArticuloEsp 
         Caption         =   "dEspecificaciones del art�culo:"
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   1
         Top             =   240
         Width           =   3915
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcArticulos 
      Height          =   285
      Left            =   240
      TabIndex        =   11
      Top             =   1800
      Width           =   4530
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7938
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasForeColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      _ExtentX        =   7990
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6720
      Top             =   1320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmArtCentralAttr.frx":1849
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblArtDen 
      Caption         =   "XXXX-XXXXXXX"
      Height          =   375
      Left            =   240
      TabIndex        =   30
      Top             =   480
      Width           =   10215
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000000&
      X1              =   120
      X2              =   10440
      Y1              =   960
      Y2              =   960
   End
   Begin VB.Label lblEncabezado 
      Caption         =   "dSeleccione las especificaciones/atributos a trasladar al articulo : "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   16
      Top             =   120
      Width           =   10215
   End
   Begin VB.Label lblDescripcion 
      Caption         =   $"frmArtCentralAttr.frx":19A3
      Height          =   615
      Left            =   240
      TabIndex        =   9
      Top             =   1080
      Width           =   10215
   End
End
Attribute VB_Name = "frmArtCentralAttr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private m_oArticulo As CArticulo
Private m_oArticuloCentral As CArticulo
Public altaCentral As Boolean
'Separadores
Private Const lSeparador = 127

Public aceptar As Boolean

Public Property Let ArticuloCentral(ByRef oArtCentral As CArticulo)
    Set m_oArticuloCentral = oArtCentral
    
End Property

Public Property Get ArticuloCentral() As CArticulo
    Set ArticuloCentral = m_oArticuloCentral
End Property

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ARTCENTRALATTR, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        Me.lblEncabezado.caption = Ador(0).value 'seleccione las especificaciones...
        Ador.MoveNext
        Me.lblDescripcion.caption = Ador(0).value      'uno o mas articulos....
        Ador.MoveNext
        Me.lblArticuloEsp(1).caption = Ador(0).value 'Especificaciones del art�culo
        Me.lblArticuloEsp(0).caption = Ador(0).value 'Especificaciones del art�culo
        Ador.MoveNext
        Me.lblArticuloFich(1).caption = Ador(0).value 'Archivos adjuntos
        Me.lblArticuloFich(0).caption = Ador(0).value
        Ador.MoveNext
        Me.lblAtributos(1).caption = Ador(0).value 'Atributos
        Me.lblAtributos(0).caption = Ador(0).value
        Ador.MoveNext
        Me.cmdCancelar.caption = Ador(0).value
        Ador.MoveNext
        Me.cmdFinalizar.caption = Ador(0).value
        Ador.MoveNext
        Me.caption = Ador(0).value 'a�adir articulo
        Ador.MoveNext
        Me.lstvwArticuloEsp(0).ColumnHeaders(1).Text = Ador(0).value
        Me.lstvwArticuloEsp(1).ColumnHeaders(1).Text = Ador(0).value
        Ador.MoveNext
        Me.lstvwArticuloEsp(0).ColumnHeaders(3).Text = Ador(0).value
        Me.lstvwArticuloEsp(1).ColumnHeaders(3).Text = Ador(0).value
        Ador.MoveNext
        Me.lstvwArticuloEsp(0).ColumnHeaders(2).Text = Ador(0).value
        Me.lstvwArticuloEsp(1).ColumnHeaders(2).Text = Ador(0).value
        Ador.MoveNext
        Me.sdbgAtributos(0).Columns(1).caption = Ador(0).value
        Me.sdbgAtributos(1).Columns(1).caption = Ador(0).value
        Ador.MoveNext
        Me.sdbgAtributos(0).Columns(2).caption = Ador(0).value
        Me.sdbgAtributos(1).Columns(2).caption = Ador(0).value
        Ador.MoveNext
        Me.lstvwArticuloEsp(0).ColumnHeaders(4).Text = Ador(0).value
        Me.lstvwArticuloEsp(1).ColumnHeaders(4).Text = Ador(0).value
        Ador.MoveNext
        Me.lstvwArticuloEsp(0).ColumnHeaders(5).Text = Ador(0).value
        Me.lstvwArticuloEsp(1).ColumnHeaders(5).Text = Ador(0).value
        Ador.Close
    End If
    
    Set Ador = Nothing
    'pargen lit
    Dim oLiterales As CLiterales
    Set oLiterales = oGestorParametros.DevolverLiterales(48, 52, basPublic.gParametrosInstalacion.gIdioma)
    Me.caption = Me.caption & " " & oLiterales.Item(5).Den
    Me.lblArticuloEsp(2).caption = Me.lblArticuloEsp(1).caption & " " & oLiterales.Item(3).Den
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdFinalizar_Click()
    If actualizarcentrales(m_oArticuloCentral) Then
        Unload Me
    End If
End Sub

Private Sub cmdVolver_Click()
    Unload Me
    frmArtCentralUnifConfirm.Show
End Sub

Private Sub Form_Load()
    CargarRecursos
    PonerFieldSeparator Me
    cargarCombo
    CargarAtributos (1)
    CargarAdjuntos (1)
    Me.lblArtDen.caption = m_oArticuloCentral.CodDen
    Me.aceptar = False
End Sub


Private Function cargarCombo()
    Dim oArticulo As CArticulo
    For Each oArticulo In Me.ArticuloCentral.articulosAgregados
        Me.sdbcArticulos.AddItem oArticulo.Cod & Chr(lSeparador) & oArticulo.CodDen
    Next
End Function

Private Sub CargarAdjuntos(Index As Integer)
    Dim oEsp As CEspecificacion
    Dim oEsps As CEspecificaciones
    Screen.MousePointer = vbHourglass
    If Index = 0 Then
        Set oEsps = m_oArticulo.especificaciones
    Else
        Set oEsps = m_oArticuloCentral.especificaciones
    End If
    lstvwArticuloEsp(Index).ListItems.clear
    For Each oEsp In oEsps
        lstvwArticuloEsp(Index).ListItems.Add , "ESP" & CStr(oEsp.Id), , , "ESP"
        lstvwArticuloEsp(Index).ListItems.Item("ESP" & CStr(oEsp.Id)).Text = oEsp.nombre
        lstvwArticuloEsp(Index).ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.dataSize / 1024)
        lstvwArticuloEsp(Index).ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Path", ""
        lstvwArticuloEsp(Index).ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Comentario", oEsp.Comentario
        lstvwArticuloEsp(Index).ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
        lstvwArticuloEsp(Index).ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
        'lstvwArticuloEsp(Index).ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fichero", oEsp.nombre, , oEsp.nombre
    Next

    
    Screen.MousePointer = vbNormal

End Sub
Private Sub cmdExcluirAtributo_Click()
    Dim Index As Variant
    Dim oAtributo As CAtributo
    Index = Me.sdbgAtributos(1).Columns("ID").CellValue(Me.sdbgAtributos(1).SelBookmarks(0))
    Set oAtributo = m_oArticuloCentral.ATRIBUTOS.Item(Index)
    If Not oAtributo Is Nothing Then
        m_oArticuloCentral.ATRIBUTOS.Remove oAtributo.Id
        CargarAtributos (1)
    End If
End Sub

Private Sub cmdExcluirFichEspArt_click()
    Dim Index As Variant
    Dim oEspec As CEspecificacion
    If Not Me.lstvwArticuloEsp(1).selectedItem Is Nothing Then
        Index = Me.lstvwArticuloEsp(1).selectedItem.Tag
        Set oEspec = m_oArticuloCentral.especificaciones.Item(Index)
        m_oArticuloCentral.especificaciones.Remove Index
        CargarAdjuntos (1)
    End If
End Sub
Private Sub cmdExcluirEspArt_Click()
    Me.txtEspecificaciones(1).Text = ""
End Sub

Private Sub cmdExcluirTodosAtributos_Click()
    
    Set m_oArticuloCentral.ATRIBUTOS = oFSGSRaiz.Generar_CAtributos
    
    CargarAtributos (1)
End Sub

Private Sub cmdExcluirTodosFichArt_click()
    
     Set m_oArticuloCentral.especificaciones = oFSGSRaiz.Generar_CEspecificaciones
    
    CargarAdjuntos (1)
End Sub

Private Sub cmdIncluirAtributo_Click()
    Dim Index As Variant
    Dim oAtr As CAtributo
    
    Index = Me.sdbgAtributos(0).Columns("ID").CellValue(Me.sdbgAtributos(0).SelBookmarks(0))
    If Index <> "" Then
        Set oAtr = m_oArticulo.ATRIBUTOS.Item(Index)
        m_oArticuloCentral.ATRIBUTOS.addAtributo oAtr
        CargarAtributos 1
    End If
End Sub

Private Sub cmdIncluirEspArt_Click()
    If InStr(Me.txtEspecificaciones(1).Text, Me.txtEspecificaciones(0)) < 1 Then
        Me.txtEspecificaciones(1).Text = Me.txtEspecificaciones(1) & vbCrLf & Me.txtEspecificaciones(0)
    End If
End Sub

Private Sub cmdIncluirFichEspArt_click()
    Dim Index As Variant
    Dim oAdjunto As CEspecificacion
    If Not Me.lstvwArticuloEsp(0).selectedItem Is Nothing Then
        Index = Me.lstvwArticuloEsp(0).selectedItem.Index
        Set oAdjunto = m_oArticulo.especificaciones.Item(Index)
        m_oArticuloCentral.especificaciones.addEspecificacion oAdjunto
        CargarAdjuntos 1
    End If
End Sub

Private Sub cmdIncluirTodosAtributos_Click()
    Dim atr As CAtributo
    For Each atr In m_oArticulo.ATRIBUTOS
        
        m_oArticuloCentral.ATRIBUTOS.addAtributo atr
    Next
    CargarAtributos (1)
End Sub

Private Sub cmdIncluirTodosFichArt_click()
    Dim esp As CEspecificacion
    For Each esp In m_oArticulo.especificaciones
        m_oArticuloCentral.especificaciones.addEspecificacion esp
    Next
    CargarAdjuntos 1
End Sub

Private Sub Form_Initialize()
    Set m_oArticuloCentral = oFSGSRaiz.generar_CArticulo
    Set m_oArticuloCentral.ATRIBUTOS = oFSGSRaiz.Generar_CAtributos
    Set m_oArticuloCentral.especificaciones = oFSGSRaiz.Generar_CEspecificaciones
End Sub


'''<summary>carga las especificaciones de un art�culo seleccionado en el combo en la coleccion de articulos</summary>
'''<summary>en el listbox identificado por el indice</summary>
Private Sub CargarEspecificacionesArticulo(ByVal Index As Integer)
    Me.txtEspecificaciones(Index).Text = NullToStr(m_oArticulo.esp)
End Sub

Private Sub CargarAtributos(Index As Integer)
    Dim atrs As CAtributos
    Dim atr As CAtributo
    If Index = 0 Then
        Set atrs = m_oArticulo.ATRIBUTOS
    Else
        m_oArticuloCentral.DevolverTodosLosAtributosDelArticulo
        Set atrs = m_oArticuloCentral.ATRIBUTOS
    End If
    Me.sdbgAtributos(Index).RemoveAll
    For Each atr In atrs
        Dim sLinea As String
        sLinea = atr.Id & Chr(lSeparador) & atr.Cod & Chr(lSeparador)
        sLinea = sLinea & atr.Den & Chr(lSeparador)
        Me.sdbgAtributos(Index).AddItem sLinea
    Next
End Sub



Private Sub sdbcArticulos_CloseUp()
    'cambiamos el articulo origen por el del combo
    Set m_oArticulo = m_oArticuloCentral.articulosAgregados.Item(sdbcArticulos.Columns(0).value)
    
    CargarEspecificacionesArticulo (0)
    Set m_oArticulo.ATRIBUTOS = m_oArticulo.DevolverTodosLosAtributosDelArticulo
    CargarAtributos 0
    m_oArticulo.CargarTodasLasEspecificaciones
    CargarAdjuntos (0)
    sdbcArticulos.Text = m_oArticulo.CodDen
End Sub


