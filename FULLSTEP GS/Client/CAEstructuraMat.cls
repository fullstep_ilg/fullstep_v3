VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAEstructuraMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'edu
'PM 98
' nueva clase que realiza el tratamiento de la estructura de materiales a trav�s de un treeview
Option Explicit

Public oMensajes As CMensajes

Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

Private oGruposMN1Comprador As CGruposMatNivel1
Private oGruposMN2Comprador As CGruposMatNivel2
Private oGruposMN3Comprador As CGruposMatNivel3
Private oGruposMN4Comprador As CGruposMatNivel4

Private oGruposMN1Marcados As CGruposMatNivel1
Private oGruposMN2Marcados As CGruposMatNivel2
Private oGruposMN3Marcados As CGruposMatNivel3
Private oGruposMN4Marcados As CGruposMatNivel4

Private oGruposMN1Seleccionados As CGruposMatNivel1
Private oGruposMN2Seleccionados As CGruposMatNivel2
Private oGruposMN3Seleccionados As CGruposMatNivel3
Private oGruposMN4Seleccionados As CGruposMatNivel4
Public sTituloArbol As String

Private oGruposMN1DesSeleccionados As CGruposMatNivel1
Private oGruposMN2DesSeleccionados As CGruposMatNivel2
Private oGruposMN3DesSeleccionados As CGruposMatNivel3
Private oGruposMN4DesSeleccionados As CGruposMatNivel4

Private oGruposMN1SeleccionadosIniciales As CGruposMatNivel1
Private oGruposMN2SeleccionadosIniciales As CGruposMatNivel2
Private oGruposMN3SeleccionadosIniciales As CGruposMatNivel3


Public Sub GenerarEstructura(material As Object, tvw As TreeView, Optional SoloUltimoNivel As Boolean = False)

    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    
    Dim oGMN1 As CGrupoMatNivel1
    Dim nodx As MSComctlLib.node
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4

    
    'If oCompSeleccionado Is Nothing Then Exit Sub
    
    tvw.Scroll = False
    
    tvw.Nodes.clear
    
    On Error Resume Next
    Set nodx = tvw.Nodes.Add(, , "Raiz", sTituloArbol, "Raiz")
    If err = 35601 Then
        'No existe el nodo raiz. crearlo
        AddParentNode tvw, "Raiz", sTituloArbol
    End If
    
    On Error GoTo 0
    
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    On Error Resume Next
    
    Screen.MousePointer = vbHourglass
    
    With material
        '************** GruposMN1 ***********
        If (Not SoloUltimoNivel) Or (SoloUltimoNivel = True And gParametrosGenerales.giNEM = 1) Then
            Set oGruposMN1 = .DevolverGruposMN1Asignados(, , , , False, True)
            
            For Each oGMN1 In oGruposMN1
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                
                GenerarNodo tvw, "Raiz", "GMN1" & scod1, CStr(oGMN1.Cod), oGMN1.Den, IIf(oGMN1.GruposMatNivel2 Is Nothing, "GMN1A", "GMN1")
    
            Next
        End If
        
        If gParametrosGenerales.giNEM <= 1 Then
            tvw.Scroll = True
            Screen.MousePointer = vbNormal
            Exit Sub
        End If

        '************** GruposMN2 ***********
        If (Not SoloUltimoNivel) Or (SoloUltimoNivel = True And gParametrosGenerales.giNEM = 2) Then

            Set oGruposMN2 = .DevolverGruposMN2Asignados(, , , , False, True)
                
            For Each oGMN2 In oGruposMN2
                scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN2.GMN1Cod))
                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
    
                GenerarNodo tvw, "Raiz", "GMN1" & scod1, CStr(oGMN2.GMN1Cod), oGMN2.GMN1Den, "GMN1"
    
                GenerarNodo tvw, "GMN1" & scod1, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod), oGMN2.Den, IIf(oGMN2.GruposMatNivel3 Is Nothing, "GMN2A", "GMN2")
    
            Next
        End If
        
        If gParametrosGenerales.giNEM <= 2 Then
            tvw.Scroll = True
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        '************** GruposMN3 ***********
        If (Not SoloUltimoNivel) Or (SoloUltimoNivel = True And gParametrosGenerales.giNEM = 3) Then

            Set oGruposMN3 = .DevolverGruposMN3Asignados(, , , , False, True)
                
            If Not oGruposMN3 Is Nothing Then
                For Each oGMN3 In oGruposMN3
                    
                    scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN3.GMN1Cod))
                    scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN3.GMN2Cod))
                    scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                
                    GenerarNodo tvw, "Raiz", "GMN1" & scod1, CStr(oGMN3.GMN1Cod), oGMN3.GMN1Den, "GMN1"
    
                    GenerarNodo tvw, "GMN1" & scod1, "GMN2" & scod1 & scod2, CStr(oGMN3.GMN2Cod), oGMN3.GMN2Den, "GMN2"
    
                    GenerarNodo tvw, "GMN2" & scod1 & scod2, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod), oGMN3.Den, IIf(oGMN3.GruposMatNivel4 Is Nothing, "GMN3A", "GMN3")
     
                    
                Next
            
            End If
            
        End If
        
        If gParametrosGenerales.giNEM <= 3 Then
            tvw.Scroll = True
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        '************** GruposMN4 ***********

        Set oGruposMN4 = .DevolverGruposMN4Asignados(, , , , False, True)
        
        If Not oGruposMN4 Is Nothing Then
            
            For Each oGMN4 In oGruposMN4
                scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))

                GenerarNodo tvw, "Raiz", "GMN1" & scod1, CStr(oGMN4.GMN1Cod), oGMN4.GMN1Den, "GMN1"

                GenerarNodo tvw, "GMN1" & scod1, "GMN2" & scod1 & scod2, CStr(oGMN4.GMN2Cod), oGMN4.GMN2Den, "GMN2"

                GenerarNodo tvw, "GMN2" & scod1 & scod2, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN4.GMN3Cod), oGMN4.GMN3Den, "GMN3"

                GenerarNodo tvw, "GMN3" & scod1 & scod2 & scod3, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod), oGMN4.Den, "GMN4A"

            Next
        
        End If
     
     End With
     'Expandir tvw
     tvw.Scroll = True
     Screen.MousePointer = vbNormal
     
End Sub


Private Sub GenerarNodo(tvw As TreeView, padre As String, nodo As String, Codigo As String, Descripcion As String, imagen As String)

    Dim nodx As MSComctlLib.node

    On Error Resume Next
    Set nodx = tvw.Nodes(nodo)
    If err = 0 Then ' Ya existe
        Exit Sub
    End If
    On Error GoTo 0
    
    Set nodx = tvw.Nodes.Add(padre, tvwChild, nodo, Codigo & " - " & Descripcion, imagen)
    
    nodx.Tag = Left(nodo, 4) & Codigo
    
    ' hacer visibles marcas de primer nivel
    
    If Not Left(nodx.key, 4) = "GMN1" Then
        If Right(nodx.Image, 1) = "A" Then
            If Right(nodx.Parent.Image, 1) = "A" Then ' Si su padre tiene marca, no expandir
                nodx.Parent.Expanded = False
            Else
                nodx.EnsureVisible
            End If
            'nodx.Checked = True
        End If
    End If

End Sub


Public Sub SalvarEstructura()
    Set oGruposMN1Marcados = oGruposMN1
    Set oGruposMN2Marcados = oGruposMN2
    Set oGruposMN3Marcados = oGruposMN3
    Set oGruposMN4Marcados = oGruposMN4
End Sub

Public Sub RestaurarEstructura()
    Set oGruposMN1 = oGruposMN1Marcados
    Set oGruposMN2 = oGruposMN2Marcados
    Set oGruposMN3 = oGruposMN3Marcados
    Set oGruposMN4 = oGruposMN4Marcados
End Sub

Public Sub MemorizarEstructura(tvw As TreeView)
    Dim nodx As MSComctlLib.node
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node

    tvw.Nodes("Raiz").Root.Selected = True
    tvw.Scroll = True

    Set oGruposMN1DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
    
    Set oGruposMN1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4

    Set oGruposMN1SeleccionadosIniciales = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2SeleccionadosIniciales = oFSGSRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3SeleccionadosIniciales = oFSGSRaiz.Generar_CGruposMatNivel3
    
    Set nodx = tvw.Nodes(1)
    If nodx.Children > 0 Then
        Set nod1 = nodx.Child
        While Not nod1 Is Nothing
            If nod1.Checked Then
                oGruposMN1SeleccionadosIniciales.Add DevolverCod(nod1), ""
            End If
            
            Set nod2 = nod1.Child
            While Not nod2 Is Nothing
                If nod2.Checked Then
                    oGruposMN2SeleccionadosIniciales.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                End If
                
                Set nod3 = nod2.Child
                While Not nod3 Is Nothing
                    If nod3.Checked Then
                        oGruposMN3SeleccionadosIniciales.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                    End If
                    
                    Set nod3 = nod3.Next
                Wend
                
                Set nod2 = nod2.Next
            Wend
            
            Set nod1 = nod1.Next
        Wend
    End If
End Sub

Public Sub GenerarEstructuraMateriales(Optional ByVal bOrdenadoPorDen As Boolean, Optional tvw As TreeView)

    Dim ogsMN1 As CGruposMatNivel1
    Dim ogsMN2 As CGruposMatNivel2
    Dim ogsMN3 As CGruposMatNivel3
    Dim ogsMN4 As CGruposMatNivel4

    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    
    Dim nodx As MSComctlLib.node
            
    If IsMissing(bOrdenadoPorDen) Then
        bOrdenadoPorDen = False
    End If
    
    tvw.Scroll = False
    Screen.MousePointer = vbHourglass
    tvw.Nodes.clear
    
    On Error Resume Next
    Set nodx = tvw.Nodes.Add(, , "Raiz", sTituloArbol, "Raiz")
    If err = 35601 Then
        'No existe el nodo raiz. crearlo
        AddParentNode tvw, "Raiz", sTituloArbol
    End If
    
    On Error GoTo 0
    
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    nodx.Selected = True

    Set ogsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1

    ogsMN1.GenerarEstructuraMateriales bOrdenadoPorDen

    For Each oGMN1 In ogsMN1
        scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
        Set nodx = tvw.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
        nodx.Tag = "GMN1" & oGMN1.Cod
        Set ogsMN2 = oGMN1.GruposMatNivel2
        
        If gParametrosGenerales.giNEM <= 1 Then
            Exit For
        End If
        
        Set ogsMN2 = oGMN1.GruposMatNivel2
        
        For Each oGMN2 In ogsMN2 'oGMN1.GruposMatNivel2
            scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
            Set nodx = tvw.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
            nodx.Tag = "GMN2" & oGMN2.Cod
            
            If gParametrosGenerales.giNEM <= 2 Then
                Exit For
            End If
            
            Set ogsMN3 = oGMN2.GruposMatNivel3
            
            For Each oGMN3 In ogsMN3 ' oGMN2.GruposMatNivel3
                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                Set nodx = tvw.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                nodx.Tag = "GMN3" & oGMN3.Cod
                
                If gParametrosGenerales.giNEM <= 3 Then
                    Exit For
                End If
                
                Set ogsMN4 = oGMN3.GruposMatNivel4
                
                For Each oGMN4 In ogsMN4 'oGMN3.GruposMatNivel4
                    scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                    Set nodx = tvw.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                    nodx.Tag = "GMN4" & oGMN4.Cod
                Next
            Next
        Next
    Next

    Expandir tvw

    tvw.Scroll = True
    Screen.MousePointer = vbNormal
    Exit Sub

Error:
    Set nodx = Nothing
    tvw.Scroll = True
    Screen.MousePointer = vbNormal
    Resume Next
End Sub

Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.node)
    Dim nod As MSComctlLib.node
    Set nod = nodx.Child
    While Not nod Is Nothing
        nod.Checked = True
        MarcarTodosLosHijos nod
        Set nod = nod.Next
    Wend
End Sub
Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.node)
    Dim nod As MSComctlLib.node
    Set nod = nodx.Child
    While Not nod Is Nothing
        nod.Checked = False
        QuitarMarcaTodosLosHijos nod
        Set nod = nod.Next
    Wend
End Sub

Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.node)
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        nodx.Checked = False
        Set nodx = nodx.Parent
    Wend
End Sub

'edu pm98
'si modo es Alta ignorar la seleccion inicial y los no seleccionados.

Public Sub GuardarMateriales(tvr As TreeView, tvw As TreeView, Plantilla As Object, PlantillaNew As Object, modo As String)

    If modo = "ALTA" Then
        GuardarMaterialesAlta tvw, PlantillaNew
    Else
        GuardarMaterialesModificacion tvr, tvw, Plantilla, PlantillaNew
    End If
    
End Sub

Private Sub GuardarMaterialesAlta(tvw As TreeView, Plantilla As Object)
    Dim oIMaterial  As IMaterialAsignado 'Plantilla
    Dim teserror As TipoErrorSummit
    teserror.NumError = TESnoerror

    Set oIMaterial = AsignarMateriales(tvw, Plantilla)
    teserror = oIMaterial.AsignarMaterial

    If teserror.NumError <> TESnoerror Then
      Screen.MousePointer = vbNormal
      basErrores.TratarError teserror
    End If
    
End Sub

Public Function AsignarMateriales(tvw As TreeView, Plantilla As Object) As IMaterialAsignado
    Dim nodx As MSComctlLib.node
    
    Dim gmn1Seleccionados As CGruposMatNivel1
    Dim gmn2Seleccionados As CGruposMatNivel2
    Dim gmn3Seleccionados As CGruposMatNivel3
    Dim gmn4Seleccionados As CGruposMatNivel4

    Set gmn1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set gmn2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set gmn3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set gmn4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4

    If tvw.Nodes.Count = 0 Then
        'no hay nada en el arbol
        Exit Function
    End If
    
    Set nodx = tvw.Nodes(1)
    If nodx.Children = 0 Then Exit Function

    Screen.MousePointer = vbHourglass
    
    ' eliminar posibles marcas en nodos que no tienen marcados a todos sus hijos
    
    For Each nodx In tvw.Nodes
        If nodx.Checked = False Then
            QuitarMarcaTodosSusPadres nodx
        End If
    Next
    
    ' annadir nodos marcados a cualquier nivel
    
    Set nodx = tvw.Nodes(1)
    
    For Each nodx In tvw.Nodes
    
        If nodx.Checked = True Then
            Select Case Left(nodx.Tag, 4)
            
            Case "GMN1"
                    
                gmn1Seleccionados.Add DevolverCod(nodx), ""
                
            Case "GMN2"
                    
                gmn2Seleccionados.Add DevolverCod(nodx.Parent), "", DevolverCod(nodx), ""
        
            Case "GMN3"
                    
                gmn3Seleccionados.Add DevolverCod(nodx.Parent.Parent), DevolverCod(nodx.Parent), DevolverCod(nodx), "", "", ""
        
                
            Case "GMN4"
                    
                gmn4Seleccionados.Add DevolverCod(nodx.Parent.Parent.Parent), DevolverCod(nodx.Parent.Parent), DevolverCod(nodx.Parent), "", "", "", DevolverCod(nodx), ""
                
            End Select
        End If
    
    Next
    
    Set AsignarMateriales = Plantilla

    Set AsignarMateriales.GruposMN1 = gmn1Seleccionados
    Set AsignarMateriales.GruposMN2 = gmn2Seleccionados
    Set AsignarMateriales.GruposMN3 = gmn3Seleccionados
    Set AsignarMateriales.GruposMN4 = gmn4Seleccionados


    Screen.MousePointer = vbNormal

    Exit Function

End Function

Private Sub GuardarMaterialesModificacion(tvr As TreeView, tvw As TreeView, PlantillaOld As Object, PlantillaNew As Object)
    
    Dim nodx As MSComctlLib.node
    Dim nodr As MSComctlLib.node
    Dim estaba_checked As Boolean
    
    Dim oIMaterial  As IMaterialAsignado 'Plantilla
    Dim teserror As TipoErrorSummit

    Dim gmn1Seleccionados As CGruposMatNivel1
    Dim gmn2Seleccionados As CGruposMatNivel2
    Dim gmn3Seleccionados As CGruposMatNivel3
    Dim gmn4Seleccionados As CGruposMatNivel4

    Dim gmn1DesSeleccionados As CGruposMatNivel1
    Dim gmn2DesSeleccionados As CGruposMatNivel2
    Dim gmn3DesSeleccionados As CGruposMatNivel3
    Dim gmn4DesSeleccionados As CGruposMatNivel4

    Set gmn1DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set gmn2DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set gmn3DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set gmn4DesSeleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
    
    Set gmn1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
    Set gmn2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
    Set gmn3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
    Set gmn4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4

    tvw.Nodes("Raiz").Root.Selected = True

    teserror.NumError = TESnoerror
     
     ' paso 2. Guardar los seleccionados y los desseleccionados
     
     Set nodx = tvw.Nodes(1)
     If nodx.Children = 0 Then Exit Sub
      
     Screen.MousePointer = vbHourglass
     
    ' eliminar posibles marcas en nodos que no tienen marcados a todos sus hijos

    For Each nodx In tvw.Nodes
    
        If nodx.Checked = False Then
            QuitarMarcaTodosSusPadres nodx
        End If
        
    Next

    ' annadir nodos marcados a cualquier nivel
    
    Set nodx = tvw.Nodes(1)

    For Each nodx In tvw.Nodes
        
        ' averiguar su estado anterior
        'If nodx.key = "GMN407 02 02  02   " Then
        '    MsgBox nodx.key
        'End If
        On Error Resume Next
        Set nodr = tvr.Nodes(nodx.key)
        If err <> 0 Then
            'Nodo nuevo que no exist�a en el arbol original
            estaba_checked = False
        Else
            If Right(nodr.Image, 1) <> "A" Then
                estaba_checked = False
            Else
                estaba_checked = True
            End If
        End If
        
        On Error GoTo 0
        
        If nodx.Checked = True Then
            'If Right(nodr.Image, 1) <> "A" Then ' preguntar por el icono, ya que nunca estan checked si no tienen checkbox
            If estaba_checked = False Then
                Select Case Left(nodx.Tag, 4)
                
                Case "GMN1"
                    gmn1Seleccionados.Add DevolverCod(nodx), ""
                    
                Case "GMN2"
                    gmn2Seleccionados.Add DevolverCod(nodx.Parent), "", DevolverCod(nodx), ""
            
                Case "GMN3"
                    gmn3Seleccionados.Add DevolverCod(nodx.Parent.Parent), DevolverCod(nodx.Parent), DevolverCod(nodx), "", "", ""
            
                    
                Case "GMN4"
                    gmn4Seleccionados.Add DevolverCod(nodx.Parent.Parent.Parent), DevolverCod(nodx.Parent.Parent), DevolverCod(nodx.Parent), "", "", "", DevolverCod(nodx), ""
                    
                End Select
            End If
        Else
            'If Right(nodr.Image, 1) = "A" Then
            If estaba_checked = True Then
                Select Case Left(nodx.Tag, 4)
                
                Case "GMN1"
                    gmn1DesSeleccionados.Add DevolverCod(nodx), ""
                    
                Case "GMN2"
                    gmn2DesSeleccionados.Add DevolverCod(nodx.Parent), "", DevolverCod(nodx), ""
            
                Case "GMN3"
                    gmn3DesSeleccionados.Add DevolverCod(nodx.Parent.Parent), DevolverCod(nodx.Parent), DevolverCod(nodx), "", "", ""

                    
                Case "GMN4"
                    gmn4DesSeleccionados.Add DevolverCod(nodx.Parent.Parent.Parent), DevolverCod(nodx.Parent.Parent), DevolverCod(nodx.Parent), "", "", "", DevolverCod(nodx), ""
                    
                End Select
            End If
        End If
    Next

    Set oIMaterial = PlantillaNew
    
    Set oIMaterial.GruposMN1 = gmn1DesSeleccionados
    Set oIMaterial.GruposMN2 = gmn2DesSeleccionados
    Set oIMaterial.GruposMN3 = gmn3DesSeleccionados
    Set oIMaterial.GruposMN4 = gmn4DesSeleccionados
    
    teserror = oIMaterial.DesAsignarMaterial
      
    If teserror.NumError <> TESnoerror Then
      Screen.MousePointer = vbNormal
      basErrores.TratarError teserror
      Screen.MousePointer = vbNormal
      Exit Sub
    End If
    
    Set oIMaterial.GruposMN1 = gmn1Seleccionados
    Set oIMaterial.GruposMN2 = gmn2Seleccionados
    Set oIMaterial.GruposMN3 = gmn3Seleccionados
    Set oIMaterial.GruposMN4 = gmn4Seleccionados
    
    teserror = oIMaterial.AsignarMaterial
      
    If teserror.NumError <> TESnoerror Then
      Screen.MousePointer = vbNormal
      basErrores.TratarError teserror
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
    ' edu
    
    Select Case Left(node.Tag, 4)
    
    Case "GMN1"
            
            DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
        
    Case "GMN2"
            
            DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
        
    Case "GMN3"
            
            DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
        
    Case "GMN4"
            
            DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
        
    End Select

End Function
' edu 453

Private Function AddParentNode(tv As TreeView, key As String, Text As String) As node
On Error GoTo Error

Dim mNodeParent As node

Set mNodeParent = tv.Nodes.Add(, , key, Text)
  With mNodeParent
    'other properties
  End With
    
Set AddParentNode = mNodeParent

Exit Function
Error:
  'Application.Echo True
  'MsgBox Err.Number & ": " & Err.Description
End Function

' mover los checks del arbol de consulta al de edicion
' cuenta las marcas y las devuelve
Public Function TrasladarMarcas(tvr As TreeView, tvm As TreeView, Optional EliminarInexistentes As Boolean = True) As Integer
         ' Ahora de esta estructura debemos marcar los materiales que ya estan seleccionados
        Dim nodx As MSComctlLib.node
        Dim nodxm As MSComctlLib.node
        Dim falta_material As String
        Dim n As Integer
        n = 0
        
        falta_material = ""
        
        'TUDU
        'cambiar paleta de iconos respetando los nombres, pero simn marcas
        
        For Each nodx In tvr.Nodes
            If Right(nodx.Image, 1) = "A" Then
                On Error Resume Next
                Set nodxm = tvm.Nodes(nodx.key)
                ' MMM edu.
                ' Puede que se haya eliminado algun material de la plantilla del comprador
                ' despues de asignarselos a la plantilla.
                If err = 0 Then
                    'nodxm.EnsureVisible
                    nodxm.Checked = True
                    If Len(nodxm.Image) < 5 Then
                        nodxm.Image = nodxm.Image & "A"
                    End If
                    n = n + 1
                Else
                    If EliminarInexistentes Then
                        'tvm.Nodes.Remove nodxm.Index
                    End If
                End If
                On Error GoTo 0
            End If
        Next

        ExpandirChecks tvm
        DoEvents
        TrasladarMarcas = n

End Function

'Devuelve un numero con el numero de hojas de tvr que no estan en tvm

Public Function NoCoincidencias(ByVal tvr As TreeView, ByVal tvm As TreeView) As Integer
         ' Ahora de esta estructura debemos marcar los materiales que ya estan seleccionados
        Dim nodx As MSComctlLib.node
        Dim nodxm As MSComctlLib.node
        Dim falta_material As String
        Dim n As Integer
        n = 0
        
        falta_material = ""
        
        'TUDU
        'cambiar paleta de iconos respetando los nombres, pero simn marcas
        
        For Each nodx In tvr.Nodes
            On Error Resume Next
            Set nodxm = tvm.Nodes(nodx.key)
            If err <> 0 Then
                'no existe
                n = n + 1
            End If
            On Error GoTo 0
        Next

        NoCoincidencias = n

End Function

'edu pm98
'hacer visible la primera de las marcas de cada rama del arbol

Public Function Expandir(tvm As TreeView)

        Dim nodx As MSComctlLib.node

        For Each nodx In tvm.Nodes
            If Not Left(nodx.key, 4) = "GMN1" Then
                If Right(nodx.Image, 1) = "A" Then ' Or nodx.Checked = True Then
                    If Right(nodx.Parent.Image, 1) = "A" Then ' Or nodx.Parent.Checked = True Then  ' Si su padre tiene marca, expandir a su padre
                        nodx.Parent.Expanded = False ' mover los checks del arbol de consulta al de edicion
                    Else
                        nodx.EnsureVisible
                    End If
                    nodx.Checked = True
                End If
            End If
        Next
        
        DoEvents
End Function
Public Function ExpandirChecks(tvm As TreeView, Optional Todos As Boolean = False)

        Dim nodx As MSComctlLib.node

        For Each nodx In tvm.Nodes
            If Not nodx.Parent Is Nothing Then
                If nodx.Checked Then
                    If Todos Then
                        nodx.EnsureVisible
                    Else
                        If nodx.Parent.Checked Then
                            nodx.Expanded = False
                        Else
                            nodx.EnsureVisible
                        End If
                    End If
                Else
                    nodx.Expanded = False
                End If
            End If
      Next
      
      DoEvents
End Function
Public Sub MarcarNodo(SelNode As MSComctlLib.node, asignable As Boolean)

    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    
    If SelNode.Tag = "Raiz" Then
        SelNode.Checked = False
        DoEvents
        Exit Sub
    End If

    If asignable Then

        Select Case Left(SelNode.Tag, 4)
                    
        Case "GMN1"
                                                           
            scod1 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode)))
            If oGruposMN1Comprador.Item(scod1) Is Nothing Then
                SelNode.Checked = Not SelNode.Checked
                oMensajes.PermisoDenegadoDesAsignarMat
                Exit Sub
            End If
                            
        Case "GMN2"
                            
            scod1 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent)))
            scod2 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode)))
            If oGruposMN2Comprador.Item(scod1 & scod2) Is Nothing _
                And oGruposMN1Comprador.Item(scod1) Is Nothing Then
                SelNode.Checked = Not SelNode.Checked
                oMensajes.PermisoDenegadoDesAsignarMat
                DoEvents
                Exit Sub
            End If
                        
        Case "GMN3"
                            
            scod1 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent)))
            scod2 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent)))
            scod3 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode)))
            If oGruposMN3Comprador.Item(scod1 & scod2 & scod3) Is Nothing _
                And oGruposMN2Comprador.Item(scod1 & scod2) Is Nothing _
                And oGruposMN1Comprador.Item(scod1) Is Nothing Then
                SelNode.Checked = Not SelNode.Checked
                oMensajes.PermisoDenegadoDesAsignarMat
                DoEvents
                Exit Sub
            End If
                        
        Case "GMN4"
                                
            scod1 = DevolverCod(SelNode.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(SelNode.Parent.Parent.Parent)))
            scod2 = DevolverCod(SelNode.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(SelNode.Parent.Parent)))
            scod3 = DevolverCod(SelNode.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(SelNode.Parent)))
            scod4 = DevolverCod(SelNode) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(DevolverCod(SelNode)))
            If oGruposMN4Comprador.Item(scod1 & scod2 & scod3 & scod4) Is Nothing _
                And oGruposMN3Comprador.Item(scod1 & scod2 & scod3) Is Nothing _
                And oGruposMN2Comprador.Item(scod1 & scod2) Is Nothing _
                And oGruposMN1Comprador.Item(scod1) Is Nothing Then
                SelNode.Checked = Not SelNode.Checked
                oMensajes.PermisoDenegadoDesAsignarMat
                DoEvents
                Exit Sub
            End If

        End Select
                    
    End If
    
    If SelNode.Checked Then
    
        MarcarTodosLosHijos SelNode
        
'        ' Si sus hermanos estan marcados marcar tb a su padre
'
'        ' primero: marcar a sus padre
        
'        MarcarTodosSusPadres SelNode

    Else
        QuitarMarcaTodosSusPadres SelNode
        QuitarMarcaTodosLosHijos SelNode
    End If
End Sub


Sub MarcarTodosSusPadres(SelNode As MSComctlLib.node)
    Dim nodx As MSComctlLib.node
    Dim nod As MSComctlLib.node 'XXXXXXXXXXXXXX
    
    Set nodx = SelNode.Parent
    
    If nodx Is Nothing Then ' no hay padre
        Exit Sub
    End If
    
    Set nod = nodx.Child
    
    While Not nod Is Nothing
        If nod.Checked = False Then ' hermano sin marcar
            Exit Sub
        End If
        Set nod = nod.Next
    Wend
    
    nodx.Checked = True
    MarcarTodosSusPadres nodx
End Sub

Sub MarcarTodosSusPadresOld(SelNode As MSComctlLib.node)
    Dim nodx As MSComctlLib.node
    
    Set nodx = SelNode.Parent
    
    While Not nodx Is Nothing
        nodx.Checked = True
        Set nodx = nodx.Parent
    Wend
End Sub

Public Sub GenerarEstructuraDeComprador(oCompSeleccionado As CComprador, tvw As TreeView)
    Dim oIMaterialAsignado As IMaterialAsignado
    Set oIMaterialAsignado = oCompSeleccionado
    GenerarEstructura oIMaterialAsignado, tvw
End Sub

Public Sub GenerarEstructuraDePlantilla(oPlantSeleccionada As CPlantilla, tvw As TreeView)
    Dim oIMaterialPlantilla As IMaterialAsignado 'Plantilla
    Set oIMaterialPlantilla = oPlantSeleccionada
    GenerarEstructura oIMaterialPlantilla, tvw
End Sub

Public Sub GenerarEstructuraDeUltimoNivel(material As Object, tvw As TreeView)
    Dim oIMaterialPlantilla As IMaterialAsignado 'Plantilla
    Set oIMaterialPlantilla = material
    GenerarEstructura oIMaterialPlantilla, tvw, True
End Sub


Public Function ContarMarcas(tvr As TreeView, Optional bPorIcono As Boolean = False) As Integer
        Dim nodx As MSComctlLib.node
        Dim hay As Integer
        hay = 0

        If bPorIcono Then               ' contar los iconos de marca
            For Each nodx In tvr.Nodes
                If nodx.Children = 0 Then
                    If Right(nodx.Image, 1) = "A" Then
                        hay = hay + 1
                    End If
                End If
            Next
        Else                            ' contar los nodos checked
            For Each nodx In tvr.Nodes
                If nodx.Children = 0 Then
                    If nodx.Checked Then
                        hay = hay + 1
                    End If
                End If
            Next
        End If
                


        ContarMarcas = hay
End Function

Public Function ContarHijos(tvr As TreeView) As Integer
        Dim nodx As MSComctlLib.node
        Dim hay As Integer
        hay = 0

        For Each nodx In tvr.Nodes
            If nodx.Children = 0 Then
                hay = hay + 1
            End If
        Next
        ContarHijos = hay
End Function

' Eliminar los nodos no marcados, comenzando por el ultimo nivel
Public Function EliminarNodos(tvr As TreeView, Optional Marcadas As Boolean = False, Optional check As Boolean = False)
        Dim nodx As MSComctlLib.node
        Dim eliminado As Boolean
        eliminado = True
        
        While eliminado
            eliminado = False
            For Each nodx In tvr.Nodes
                'If nodx.Index <= 1 Then ' nodo raiz
                '    Exit For
                'End If
                If nodx.Child Is Nothing Then
                    If Not check Then
                        If Right(nodx.Image, 1) = "A" Or nodx.Checked = True Then ' marcada
                            If Marcadas = True Then
                                tvr.Nodes.Remove nodx.Index
                                eliminado = True
                            End If
                        Else
                            If Marcadas = False Then
                                tvr.Nodes.Remove nodx.Index
                                eliminado = True
                                Exit For
                            End If
                        End If
                    Else
                        If nodx.Checked = True Then ' marcada
                            If Marcadas = True Then
                                tvr.Nodes.Remove nodx.Index
                                eliminado = True
                            End If
                        Else
                            If Marcadas = False Then
                                tvr.Nodes.Remove nodx.Index
                                eliminado = True
                                Exit For
                            End If
                        End If
                    End If
                End If
            Next
        Wend
        ExpandirChecks tvr, True
End Function


' Eliminar los checks
Public Function Desmarcar(tvr As TreeView, Optional UsarIcono As Boolean = True)

        Dim nodx As MSComctlLib.node

        If Not UsarIcono Then
            For Each nodx In tvr.Nodes
                If nodx.Checked Then
                    nodx.Checked = False
                End If
            Next
        Else
            For Each nodx In tvr.Nodes
                If Right(nodx.Image, 1) = "A" Then ' marcada
                    nodx.Image = Left(nodx.Image, Len(nodx.Image) - 1)
                    nodx.Checked = False
                End If
            Next
        End If
        

End Function

Sub SeleccionarNodo(tvr As TreeView, sCod As String)

    Dim nodx As MSComctlLib.node
        
    For Each nodx In tvr.Nodes
        If Mid(nodx.key, 5) = sCod Then
            nodx.Selected = True
            nodx.Expanded = True
            Exit For
        End If
    Next
            
End Sub

Public Sub CrearArbolPosiblesGMN(oIMaterialAsignado As IMaterialAsignado)
    
    Screen.MousePointer = vbHourglass
    
    With oIMaterialAsignado

    Set oGruposMN1Comprador = .DevolverGruposMN1Asignados(, , , , False, False)
    If gParametrosGenerales.giNEM <= 1 Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oGruposMN2Comprador = .DevolverGruposMN2Asignados(, , , , False, False)
    If gParametrosGenerales.giNEM <= 2 Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Set oGruposMN3Comprador = oIMaterialAsignado.DevolverGruposMN3Asignados(, , , , False, False)
    If gParametrosGenerales.giNEM <= 3 Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oGruposMN4Comprador = oIMaterialAsignado.DevolverGruposMN4Asignados(, , , , False, False)
    
    End With
End Sub

