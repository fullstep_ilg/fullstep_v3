VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCatalogoCondSuministro 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DCondiciones Suministro"
   ClientHeight    =   7320
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12120
   Icon            =   "frmCatalogoCondSuministro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7320
   ScaleWidth      =   12120
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraRecepPorImporte 
      BackColor       =   &H00808000&
      Height          =   3615
      Left            =   120
      TabIndex        =   16
      Top             =   120
      Width           =   11775
      Begin VB.CommandButton cmdEdicionPorImporte 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   10680
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   3120
         Width           =   1005
      End
      Begin VB.TextBox txtImporteMinPedido 
         Alignment       =   1  'Right Justify
         Height          =   345
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   24
         Top             =   1320
         Width           =   2775
      End
      Begin VB.TextBox txtImporteMaxPedido 
         Alignment       =   1  'Right Justify
         Height          =   345
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   23
         Top             =   1800
         Width           =   2775
      End
      Begin VB.TextBox txtImporteMaxTotal 
         Alignment       =   1  'Right Justify
         Height          =   345
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   22
         Top             =   2280
         Width           =   2775
      End
      Begin VB.TextBox txtImporteConsumido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000000&
         Height          =   345
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   2760
         Width           =   2775
      End
      Begin VB.TextBox txtArticuloPorImporte 
         BackColor       =   &H00C0FFFF&
         Height          =   345
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   360
         Width           =   7695
      End
      Begin VB.TextBox txtProveedorPorImporte 
         BackColor       =   &H00C0FFFF&
         Height          =   345
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   840
         Width           =   7695
      End
      Begin VB.Label lblMonedaImpMax 
         BackColor       =   &H00808000&
         Caption         =   "DEUR"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   5280
         TabIndex        =   33
         Top             =   1920
         Width           =   615
      End
      Begin VB.Label lblMonedaImpTotal 
         BackColor       =   &H00808000&
         Caption         =   "DEUR"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   5280
         TabIndex        =   32
         Top             =   2400
         Width           =   615
      End
      Begin VB.Label lblMonedaImpConsumido 
         BackColor       =   &H00808000&
         Caption         =   "DEUR"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   5280
         TabIndex        =   31
         Top             =   2880
         Width           =   615
      End
      Begin VB.Label lblMonedaImpMin 
         BackColor       =   &H00808000&
         Caption         =   "DEUR"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   5280
         TabIndex        =   30
         Top             =   1440
         Width           =   615
      End
      Begin VB.Label lblImporteMinPedido 
         BackColor       =   &H00808000&
         Caption         =   "DImporte Min Pedido:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   1440
         Width           =   2055
      End
      Begin VB.Label lblImporteMaxPedido 
         BackColor       =   &H00808000&
         Caption         =   "DImporte Max Pedido:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   1920
         Width           =   2055
      End
      Begin VB.Label lblImporteMaxTotal 
         BackColor       =   &H00808000&
         Caption         =   "DImporte Max Total:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   2400
         Width           =   2055
      End
      Begin VB.Label lblImporteConsumido 
         BackColor       =   &H00808000&
         Caption         =   "DImporte Consumido:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   2880
         Width           =   2055
      End
      Begin VB.Label lblArticuloPorImporte 
         BackColor       =   &H00808000&
         Caption         =   "DArticulo:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   480
         Width           =   2055
      End
      Begin VB.Label lblProveedorPorImporte 
         BackColor       =   &H00808000&
         Caption         =   "DProveedor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   960
         Width           =   2055
      End
   End
   Begin VB.Frame fraRecepPorCantidad 
      BackColor       =   &H00808000&
      Height          =   6975
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11895
      Begin VB.TextBox txtArticulo 
         BackColor       =   &H00C0FFFF&
         Height          =   345
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   600
         Width           =   7695
      End
      Begin VB.TextBox txtProveedor 
         BackColor       =   &H00C0FFFF&
         Height          =   345
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   1080
         Width           =   7695
      End
      Begin VB.TextBox txtUnidadBase 
         BackColor       =   &H00C0FFFF&
         Height          =   345
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   1560
         Width           =   7695
      End
      Begin VB.TextBox txtCantMaxTotalUnidBase 
         Height          =   345
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   2040
         Width           =   2775
      End
      Begin VB.CommandButton cmdAnyadir 
         Caption         =   "&A�adir"
         Height          =   345
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   6480
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   10755
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   6480
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   1155
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   6480
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2190
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   6480
         Visible         =   0   'False
         Width           =   1005
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddUnidadesPedido 
         Height          =   1575
         Left            =   360
         TabIndex        =   1
         Top             =   3720
         Width           =   4890
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         MaxDropDownItems=   10
         _Version        =   196617
         DataMode        =   2
         stylesets.count =   1
         stylesets(0).Name=   "Actuales"
         stylesets(0).BackColor=   10944511
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogoCondSuministro.frx":0562
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1667
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   10
         Columns(1).Width=   4868
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   50
         Columns(2).Width=   1720
         Columns(2).Caption=   "FC"
         Columns(2).Name =   "FC"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   5
         Columns(2).NumberFormat=   "Standard"
         Columns(2).FieldLen=   256
         _ExtentX        =   8625
         _ExtentY        =   2778
         _StockProps     =   77
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCondicionesSuministro 
         Height          =   3810
         Left            =   120
         TabIndex        =   10
         Top             =   2520
         Width           =   11655
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   9
         stylesets.count =   3
         stylesets(0).Name=   "Gris"
         stylesets(0).BackColor=   -2147483633
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogoCondSuministro.frx":057E
         stylesets(1).Name=   "Normal"
         stylesets(1).ForeColor=   0
         stylesets(1).BackColor=   16777215
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCatalogoCondSuministro.frx":059A
         stylesets(2).Name=   "Selected"
         stylesets(2).ForeColor=   16777215
         stylesets(2).BackColor=   8388608
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmCatalogoCondSuministro.frx":05B6
         AllowDelete     =   -1  'True
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         StyleSet        =   "Normal"
         ForeColorEven   =   -2147483630
         ForeColorOdd    =   -2147483630
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         Columns.Count   =   9
         Columns(0).Width=   1826
         Columns(0).Caption=   "UNI_PED_DEFECTO"
         Columns(0).Name =   "UNI_PED_DEFECTO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).Style=   2
         Columns(1).Width=   2805
         Columns(1).Caption=   "UNI_PED_CODIGO"
         Columns(1).Name =   "UNI_PED_CODIGO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16776960
         Columns(2).Width=   3387
         Columns(2).Caption=   "UNI_PED_DEN"
         Columns(2).Name =   "UNI_PED_DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16776960
         Columns(3).Width=   1349
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   2355
         Columns(4).Caption=   "CANT_MIN_PED"
         Columns(4).Name =   "CANT_MIN_PED"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   2566
         Columns(5).Caption=   "CANT_MAX_PED"
         Columns(5).Name =   "CANT_MAX_PED"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   2461
         Columns(6).Caption=   "CANT_MAX_TOTAL"
         Columns(6).Name =   "CANT_MAX_TOTAL"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(7).Width=   3200
         Columns(7).Caption=   "CANT_CONSUMIDA"
         Columns(7).Name =   "CANT_CONSUMIDA"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ACTUALIZAR"
         Columns(8).Name =   "ACTUALIZAR"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   20558
         _ExtentY        =   6720
         _StockProps     =   79
         BackColor       =   -2147483643
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblArticulo 
         BackColor       =   &H00808000&
         Caption         =   "DArticulo:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   720
         Width           =   2055
      End
      Begin VB.Label lblProveedor 
         BackColor       =   &H00808000&
         Caption         =   "DProveedor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1200
         Width           =   2055
      End
      Begin VB.Label lblUnidadBase 
         BackColor       =   &H00808000&
         Caption         =   "DUnidad base:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   1680
         Width           =   2055
      End
      Begin VB.Label lblCantMaxTotal 
         BackColor       =   &H00808000&
         Caption         =   "DCant.Max Total(Unid Base):"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   2160
         Width           =   2295
      End
      Begin VB.Label lblUnidadCantMaxTotal 
         BackColor       =   &H00808000&
         Caption         =   "DUND"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   5400
         TabIndex        =   11
         Top             =   2160
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmCatalogoCondSuministro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_oLineaCatalogo As CLineaCatalogo
Public g_bEdicion As Boolean


Private bModoEdicion As Boolean
Private gCancelEventoChange As Boolean
Private gCancelEventoBeforeUpdate As Boolean
Private RowDefecto As Integer
Private bErrorModificacion As Boolean
Private tipoRecepcion As Integer '0 por cantidad, 1 por importe

Private bModoEdicionImporte As Boolean
Private m_sFormatoNumber As String
Private m_sFC As String
Private m_sCantMinPedido As String
Private m_sCantMaxPedido As String
Private m_sCantMaxTotal As String
Private m_sCantMaxTotalUnidadBase As String
Private m_sDeseaEliminar As String
Private m_sEliminarDefecto As String
Private m_sEdicion As String
Private m_sConsulta As String
'Si cambias de una Unidad a otra, las columnas "cantidad" cambian. Este es la Unidad original.
Private m_sOldUnidad As String

Private Sub Form_Load()
    PonerFieldSeparator Me
    
    CargarRecursos
    bModoEdicion = False
    bModoEdicionImporte = False
    ConfigurarPantalla
    
    'Inicializamos la variable que indicara cual es la fila con la unidad de pedido por defecto
    RowDefecto = -1
    'Cargamos las condiciones de suministro en el grid
    CargarCondicionesSuministro
End Sub

''' <summary>Carga Textos de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOG_COND_SUMINISTRO, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value  '1
        Ador.MoveNext
        Me.lblArticulo.caption = Ador(0).Value  '2
        Me.lblArticuloPorImporte.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblProveedor.caption = Ador(0).Value
        Me.lblProveedorPorImporte.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblUnidadBase.caption = Ador(0).Value
        Ador.MoveNext
        lblCantMaxTotal = Ador(0).Value
        m_sCantMaxTotalUnidadBase = Ador(0).Value
        Ador.MoveNext
        Me.cmdEdicionPorImporte.caption = Ador(0).Value
        Me.cmdModoEdicion.caption = Ador(0).Value
        m_sEdicion = Ador(0).Value
        Ador.MoveNext
        m_sConsulta = Ador(0).Value
        Ador.MoveNext
        Me.cmdAnyadir.caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCondicionesSuministro.Columns("UNI_PED_DEFECTO").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCondicionesSuministro.Columns("UNI_PED_DEN").caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCondicionesSuministro.Columns("FC").caption = Ador(0).Value
        m_sFC = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCondicionesSuministro.Columns("CANT_MIN_PED").caption = Ador(0).Value
        m_sCantMinPedido = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCondicionesSuministro.Columns("CANT_MAX_PED").caption = Ador(0).Value
        m_sCantMaxPedido = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCondicionesSuministro.Columns("CANT_MAX_TOTAL").caption = Ador(0).Value
        m_sCantMaxTotal = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCondicionesSuministro.Columns("CANT_CONSUMIDA").caption = Ador(0).Value
        Ador.MoveNext
        Me.lblImporteMinPedido.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblImporteMaxPedido.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblImporteMaxTotal.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblImporteConsumido.caption = Ador(0).Value
        Ador.MoveNext
        m_sEliminarDefecto = Ador(0).Value
        Ador.MoveNext
        m_sDeseaEliminar = Ador(0).Value
        Ador.Close
    End If
End Sub

Private Sub ConfigurarPantalla()
Dim I As Integer
    tipoRecepcion = nulltodbl0(frmCatalogo.oLineaCatalogoSeleccionada.tipoRecepcion)
    sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").DropDownHwnd = sdbddUnidadesPedido.hWnd
    'Obtiene el n� de decimales a mostrar en esta pantalla
    m_sFormatoNumber = "#,##0."
    For I = 1 To gParametrosInstalacion.giNumDecimalesComp
        m_sFormatoNumber = m_sFormatoNumber & "0"
    Next I
    If tipoRecepcion = 0 Then
        'Recepcion por cantidad
        fraRecepPorCantidad.Visible = True
        fraRecepPorImporte.Visible = False
        Me.Height = Me.fraRecepPorCantidad.Height + 700
        'Se comprueba si se permite editar el grid
        If g_bEdicion Then
            Me.cmdModoEdicion.Visible = True
        Else
            Me.cmdModoEdicion.Visible = False
        End If
        lblUnidadCantMaxTotal.caption = frmCatalogo.oLineaCatalogoSeleccionada.UnidadCompra
        txtProveedor.Text = nulltostr(frmCatalogo.oLineaCatalogoSeleccionada.ProveCod)
        txtUnidadBase.Text = nulltostr(frmCatalogo.oLineaCatalogoSeleccionada.UnidadCompra)
        If frmCatalogo.oLineaCatalogoSeleccionada.ArtCod_Interno = "" Then
            txtArticulo.Text = frmCatalogo.oLineaCatalogoSeleccionada.ArtDen
        Else
            txtArticulo.Text = frmCatalogo.oLineaCatalogoSeleccionada.ArtCod_Interno & " - " & frmCatalogo.oLineaCatalogoSeleccionada.ArtDen
        End If
    Else
        'Recepcion por importe
        fraRecepPorCantidad.Visible = False
        fraRecepPorImporte.Visible = True
        Me.Height = Me.fraRecepPorImporte.Height + 700
        If g_bEdicion Then
            cmdEdicionPorImporte.Visible = True
        Else
            cmdEdicionPorImporte.Visible = False
        End If
        txtProveedorPorImporte.Text = nulltostr(frmCatalogo.oLineaCatalogoSeleccionada.ProveCod)
        lblMonedaImpConsumido.caption = nulltostr(frmCatalogo.oLineaCatalogoSeleccionada.mon)
        lblMonedaImpMax.caption = nulltostr(frmCatalogo.oLineaCatalogoSeleccionada.mon)
        lblMonedaImpMin.caption = nulltostr(frmCatalogo.oLineaCatalogoSeleccionada.mon)
        lblMonedaImpTotal.caption = nulltostr(frmCatalogo.oLineaCatalogoSeleccionada.mon)
        If frmCatalogo.oLineaCatalogoSeleccionada.ArtCod_Interno = "" Then
            txtArticuloPorImporte.Text = frmCatalogo.oLineaCatalogoSeleccionada.ArtDen
        Else
            txtArticuloPorImporte.Text = frmCatalogo.oLineaCatalogoSeleccionada.ArtCod_Interno & " - " & frmCatalogo.oLineaCatalogoSeleccionada.ArtDen
        End If
    End If
End Sub

Private Sub cmdAnyadir_Click()
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgCondicionesSuministro.Scroll 0, sdbgCondicionesSuministro.Rows - sdbgCondicionesSuministro.Row

    If sdbgCondicionesSuministro.VisibleRows > 0 Then

        If sdbgCondicionesSuministro.VisibleRows >= sdbgCondicionesSuministro.Rows Then
            sdbgCondicionesSuministro.Row = sdbgCondicionesSuministro.Rows
         Else
            sdbgCondicionesSuministro.Row = sdbgCondicionesSuministro.Rows - (sdbgCondicionesSuministro.Rows - sdbgCondicionesSuministro.VisibleRows) - 1
         End If

    End If

    If Me.Visible Then sdbgCondicionesSuministro.SetFocus
End Sub
Private Sub cmdDeshacer_Click()
    ''' * Objetivo: Deshacer la edicion en la Linea de catalogo actual
    
    sdbgCondicionesSuministro.CancelUpdate
    sdbgCondicionesSuministro.DataChanged = False
    

    cmdAnyadir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
End Sub

Private Sub cmdEliminar_Click()
    ''' * Objetivo: Eliminar la Unidad de Pedido actual
    
    If sdbgCondicionesSuministro.Rows = 0 Then Exit Sub
    sdbgCondicionesSuministro.SelBookmarks.Add sdbgCondicionesSuministro.Bookmark
    If sdbgCondicionesSuministro.SelBookmarks.Count = 0 Then Exit Sub
    sdbgCondicionesSuministro.DeleteSelected
    sdbgCondicionesSuministro.SelBookmarks.RemoveAll

End Sub



Private Sub cmdModoEdicion_Click()
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant

    If Not bModoEdicion Then

        sdbgCondicionesSuministro.AllowAddNew = True
        sdbgCondicionesSuministro.AllowUpdate = True
        sdbgCondicionesSuministro.AllowDelete = True

'        cmdModoEdicion.Caption = "&Consulta"
        cmdModoEdicion.caption = m_sConsulta
        
        sdbgCondicionesSuministro.Columns("UNI_PED_DEFECTO").Locked = False
        sdbgCondicionesSuministro.Columns("FC").Locked = False
        sdbgCondicionesSuministro.Columns("CANT_MIN_PED").Locked = False
        sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Locked = False
        sdbgCondicionesSuministro.Columns("CANT_MAX_TOTAL").Locked = False
        txtCantMaxTotalUnidBase.Locked = False
        
        cmdAnyadir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True

        bModoEdicion = True

        'inicializar las dropdown
        sdbddUnidadesPedido.AddItem ""

    Else

        If sdbgCondicionesSuministro.DataChanged = True Then

            v = sdbgCondicionesSuministro.ActiveCell.Value
            If Me.Visible Then sdbgCondicionesSuministro.SetFocus
            If (v <> "") Then
                sdbgCondicionesSuministro.ActiveCell.Value = v
            End If
            
            sdbgCondicionesSuministro.Update
            If bErrorModificacion Then
                bErrorModificacion = False
                Exit Sub
            End If
        End If
        sdbgCondicionesSuministro.Columns("UNI_PED_DEFECTO").Locked = True
        sdbgCondicionesSuministro.Columns("FC").Locked = True
        sdbgCondicionesSuministro.Columns("CANT_MIN_PED").Locked = True
        sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Locked = True
        sdbgCondicionesSuministro.Columns("CANT_MAX_TOTAL").Locked = True
        txtCantMaxTotalUnidBase.Locked = True
        
        sdbgCondicionesSuministro.AllowAddNew = False
        sdbgCondicionesSuministro.AllowUpdate = False
        sdbgCondicionesSuministro.AllowDelete = False

        cmdAnyadir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False

        cmdModoEdicion.caption = m_sEdicion

        bModoEdicion = False

    End If

    If Me.Visible Then sdbgCondicionesSuministro.SetFocus

End Sub
Private Sub CargarCondicionesSuministro()
    Dim oCondicionSuministro As CUnidadPedido
    Dim I As Integer
    Dim strCondicionSuministro As String
    If frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro Is Nothing Then
        frmCatalogo.oLineaCatalogoSeleccionada.CargarCondicionesSuministro
    End If
    
    
    If tipoRecepcion = 0 Then
        'Si el tipo de recepcion es por cantidad
        If frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Count = 0 Then
            'Si es una linea nueva habra que cargar en las condiciones de suministro la Unidad de pedido por defecto, que en este caso seria la unidad de compra
            Dim oUnidadPedido As CUnidadPedido
            frmCatalogo.oLineaCatalogoSeleccionada.CargarUnidadesDePedido True
            For Each oUnidadPedido In frmCatalogo.oLineaCatalogoSeleccionada.UnidadesPedido
                If oUnidadPedido.UnidadPedido = frmCatalogo.oLineaCatalogoSeleccionada.UnidadPedido Then
                    frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Add frmCatalogo.oLineaCatalogoSeleccionada.Id, 0, , , , , , frmCatalogo.oLineaCatalogoSeleccionada.UnidadCompra, , frmCatalogo.oLineaCatalogoSeleccionada.UnidadPedido, oUnidadPedido.UPDen, frmCatalogo.oLineaCatalogoSeleccionada.FactorConversion, , , , , , , , , , , , True
                    Exit For
                End If
            Next
            
        End If
        'Cargamos la grid con las condiciones de suministro
        sdbgCondicionesSuministro.RemoveAll
        For Each oCondicionSuministro In frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro
            strCondicionSuministro = oCondicionSuministro.Defecto & Chr(m_lSeparador) & oCondicionSuministro.UnidadPedido & Chr(m_lSeparador) & oCondicionSuministro.UPDen & Chr(m_lSeparador) & oCondicionSuministro.FactorConversion & Chr(m_lSeparador)
            If Not IsNull(oCondicionSuministro.CantidadMinima) Then
                strCondicionSuministro = strCondicionSuministro & FormateoNumerico(oCondicionSuministro.CantidadMinima, m_sFormatoNumber) & " " & oCondicionSuministro.UnidadPedido & Chr(m_lSeparador)
            Else
                strCondicionSuministro = strCondicionSuministro & "" & Chr(m_lSeparador)
            End If
            If Not IsNull(oCondicionSuministro.CantidadMaximaPed) Then
                strCondicionSuministro = strCondicionSuministro & FormateoNumerico(oCondicionSuministro.CantidadMaximaPed, m_sFormatoNumber) & " " & oCondicionSuministro.UnidadPedido & Chr(m_lSeparador)
            Else
                strCondicionSuministro = strCondicionSuministro & "" & Chr(m_lSeparador)
            End If
            If Not IsNull(oCondicionSuministro.CantidadMaximaTotal) Then
                strCondicionSuministro = strCondicionSuministro & FormateoNumerico(oCondicionSuministro.CantidadMaximaTotal, m_sFormatoNumber) & " " & oCondicionSuministro.UnidadPedido & Chr(m_lSeparador)
            Else
                strCondicionSuministro = strCondicionSuministro & "" & Chr(m_lSeparador)
            End If
            If Not IsNull(oCondicionSuministro.CantidadConsumida) Then
                strCondicionSuministro = strCondicionSuministro & FormateoNumerico(oCondicionSuministro.CantidadConsumida, m_sFormatoNumber) & " " & oCondicionSuministro.UnidadPedido & Chr(m_lSeparador)
            Else
                strCondicionSuministro = strCondicionSuministro & "" & Chr(m_lSeparador)
            End If
            sdbgCondicionesSuministro.AddItem strCondicionSuministro
            If oCondicionSuministro.Defecto Then
                If Not IsNull(oCondicionSuministro.CantidadMaximaTotal_UC) And Not IsEmpty(oCondicionSuministro.CantidadMaximaTotal_UC) Then
                    Me.txtCantMaxTotalUnidBase.Text = FormateoNumerico(oCondicionSuministro.CantidadMaximaTotal_UC, m_sFormatoNumber)
                End If
                RowDefecto = I
            End If
            I = I + 1
        Next
    Else
        'Si el tipo de recepcion es por importe
        For Each oCondicionSuministro In frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro
            If Not IsNull(oCondicionSuministro.CantidadConsumida) And Not IsEmpty(oCondicionSuministro.CantidadConsumida) And oCondicionSuministro.CantidadConsumida <> "" Then
                txtImporteConsumido.Text = FormateoNumerico(oCondicionSuministro.CantidadConsumida, m_sFormatoNumber)
            End If
            If Not IsNull(oCondicionSuministro.CantidadMaximaPed) And Not IsEmpty(oCondicionSuministro.CantidadMaximaPed) And oCondicionSuministro.CantidadMaximaPed <> "" Then
                txtImporteMaxPedido.Text = FormateoNumerico(oCondicionSuministro.CantidadMaximaPed, m_sFormatoNumber)
            End If
            If Not IsNull(oCondicionSuministro.CantidadMaximaTotal) And Not IsEmpty(oCondicionSuministro.CantidadMaximaTotal) And oCondicionSuministro.CantidadMaximaTotal <> "" Then
                txtImporteMaxTotal.Text = FormateoNumerico(oCondicionSuministro.CantidadMaximaTotal, m_sFormatoNumber)
            End If
            If Not IsNull(oCondicionSuministro.CantidadMinima) And Not IsEmpty(oCondicionSuministro.CantidadMinima) And oCondicionSuministro.CantidadMinima <> "" Then
                txtImporteMinPedido.Text = FormateoNumerico(oCondicionSuministro.CantidadMinima, m_sFormatoNumber)
            End If
        Next
    End If
    
End Sub

Public Sub CargarComboUnidades()
Dim Adores As Ador.Recordset

    sdbddUnidadesPedido.RemoveAll
    
    Set Adores = frmCatalogo.oLineaCatalogoSeleccionada.DevolverUnidades
    If Not Adores Is Nothing Then
        While Not Adores.EOF
            sdbddUnidadesPedido.AddItem Adores("COD").Value & Chr(m_lSeparador) & Adores("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & Adores("FC").Value
            Adores.MoveNext
        Wend
        Adores.Close
        Set Adores = Nothing
    End If

    If sdbddUnidadesPedido.Rows = 0 Then
        sdbddUnidadesPedido.AddItem ""
    End If
    
End Sub

''' <summary>
''' Q las columnas "cantidad" tengan como formato Numero espacio Unidad
''' </summary>
''' <param name="Cancel">Cancelar el cambio de fila/columna</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgCondicionesSuministro_BeforeRowColChange(Cancel As Integer)

    If Me.sdbgCondicionesSuministro.col = -1 Then Exit Sub
    If Not bModoEdicion Then Exit Sub

    Select Case Me.sdbgCondicionesSuministro.Columns(Me.sdbgCondicionesSuministro.col).Name
    Case "CANT_MIN_PED", "CANT_MAX_PED", "CANT_MAX_TOTAL", "CANT_CONSUMIDA"
        If Me.sdbgCondicionesSuministro.Columns(Me.sdbgCondicionesSuministro.col).Text <> "" Then
            If IsNumeric(Me.sdbgCondicionesSuministro.Columns(Me.sdbgCondicionesSuministro.col).Text) Then
                Me.sdbgCondicionesSuministro.Columns(Me.sdbgCondicionesSuministro.col).Text = FormateoNumerico(Replace(Me.sdbgCondicionesSuministro.Columns(Me.sdbgCondicionesSuministro.col).Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""), m_sFormatoNumber) & " " & Me.sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text
            End If
        End If
    End Select
End Sub

Private Sub txtCantMaxTotalUnidBase_LostFocus()
    If Me.txtCantMaxTotalUnidBase.Text <> "" Then
        If IsNumeric(Me.txtCantMaxTotalUnidBase.Text) Then
           txtCantMaxTotalUnidBase.Text = FormateoNumerico(txtCantMaxTotalUnidBase.Text, m_sFormatoNumber)
        Else
            oMensajes.NoValido m_sCantMaxTotal
            If Me.Visible Then txtCantMaxTotalUnidBase.SetFocus
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    ''' * Objetivo: Descargar el formulario si no
    ''' * Objetivo: hay cambios pendientes
    Dim teserror As TipoErrorSummit
    Dim v As Variant
        
    If g_bEdicion Then
        If sdbgCondicionesSuministro.DataChanged = True Then
    
            v = sdbgCondicionesSuministro.ActiveCell.Value
            If Me.Visible Then sdbgCondicionesSuministro.SetFocus
            sdbgCondicionesSuministro.ActiveCell.Value = v
            
            sdbgCondicionesSuministro.Update
        
        End If
        If tipoRecepcion = 0 Then
            'Recepcion por cantidad
            'Si se ha introducido la cantidad maxima en unidad base se actualiza
            If Trim(Me.txtCantMaxTotalUnidBase.Text) <> "" Then
                If IsNumeric(Me.txtCantMaxTotalUnidBase.Text) Then
                    Dim oCondSuministro As CUnidadPedido
                    For Each oCondSuministro In frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro
                        If oCondSuministro.Defecto Then
                            oCondSuministro.CantidadMaximaTotal_UC = Me.txtCantMaxTotalUnidBase.Text
                            ''' Indicamos que se tiene que actualizar la linea de catalogo modificando la columna
                            Randomize
                            frmCatalogo.sdbgAdjudicaciones.Columns("COND_SUMINISTRO_HIDDEN").Value = CInt(Int((6 * Rnd()) + 1))
                        End If
                    Next
                End If
            End If
        Else
            'Recepcion por importe
            If Me.txtImporteMaxPedido.Text <> "" Then
                If Not IsNumeric(Me.txtImporteMaxPedido.Text) Then
                    oMensajes.NoValido m_sCantMaxPedido
                    Cancel = 1
                    Exit Sub
                End If
            End If
            If Me.txtImporteMinPedido.Text <> "" Then
                If Not IsNumeric(Me.txtImporteMinPedido.Text) Then
                    oMensajes.NoValido m_sCantMinPedido
                    Cancel = 1
                    Exit Sub
                End If
            End If
            If Me.txtImporteMaxTotal.Text <> "" Then
                If Not IsNumeric(Me.txtImporteMaxTotal.Text) Then
                    oMensajes.NoValido m_sCantMaxTotal
                    Cancel = 1
                    Exit Sub
                End If
            End If
            Set frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro = Nothing
            Set frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro = oFSGSRaiz.Generar_CUnidadesPedido
            frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Add frmCatalogo.oLineaCatalogoSeleccionada.Id, 0, , , , , , , , , , , Me.txtImporteMinPedido.Text, , , , , , Me.txtImporteMaxPedido.Text, Me.txtImporteMaxTotal.Text, , , Me.txtImporteConsumido.Text
'            ''' Indicamos que se tiene que actualizar la linea de catalogo modificando la columna
            Randomize
            frmCatalogo.sdbgAdjudicaciones.Columns("COND_SUMINISTRO_HIDDEN").Value = CInt(Int((6 * Rnd()) + 1))
        End If
    End If
End Sub

''' <summary>
''' Cambia el dato Unidad y los textos "cantidad" de la l�nea
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddUnidadesPedido_CloseUp()
    
    With Me.sdbgCondicionesSuministro
        If .Columns("CANT_MIN_PED").Text <> "" Then .Columns("CANT_MIN_PED").Text = FormateoNumerico(Replace(.Columns("CANT_MIN_PED").Text, m_sOldUnidad, ""), m_sFormatoNumber) & " " & sdbddUnidadesPedido.Columns("COD").Value
        If .Columns("CANT_MAX_PED").Text <> "" Then .Columns("CANT_MAX_PED").Text = FormateoNumerico(Replace(.Columns("CANT_MAX_PED").Text, m_sOldUnidad, ""), m_sFormatoNumber) & " " & sdbddUnidadesPedido.Columns("COD").Value
        If .Columns("CANT_MAX_TOTAL").Text <> "" Then .Columns("CANT_MAX_TOTAL").Text = FormateoNumerico(Replace(.Columns("CANT_MAX_TOTAL").Text, m_sOldUnidad, ""), m_sFormatoNumber) & " " & sdbddUnidadesPedido.Columns("COD").Value
        If .Columns("CANT_CONSUMIDA").Text <> "" Then .Columns("CANT_CONSUMIDA").Text = FormateoNumerico(Replace(.Columns("CANT_CONSUMIDA").Text, m_sOldUnidad, ""), m_sFormatoNumber) & " " & sdbddUnidadesPedido.Columns("COD").Value
    
        .Columns("UNI_PED_CODIGO").Value = sdbddUnidadesPedido.Columns("COD").Value
        .Columns("UNI_PED_DEN").Value = sdbddUnidadesPedido.Columns("DEN").Value
        If sdbddUnidadesPedido.Columns("FC").Value <> "" Then
            .Columns("FC").Value = sdbddUnidadesPedido.Columns("FC").Value
        End If
    End With
End Sub

''' <summary>
''' Cargar el combo de Unidades de la forma adecuada y tener cual era la unidad antes de cambiarla.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddUnidadesPedido_DropDown()
    
    m_sOldUnidad = nulltostr(sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Value)
        
    CargarComboUnidades
        
    sdbgCondicionesSuministro.ActiveCell.SelStart = 0
    sdbgCondicionesSuministro.ActiveCell.SelLength = Len(sdbgCondicionesSuministro.ActiveCell.Text)

End Sub

Private Sub sdbddUnidadesPedido_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddUnidadesPedido.DataFieldList = "Column 0"
    sdbddUnidadesPedido.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbddUnidadesPedido_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim I As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddUnidadesPedido.MoveFirst
    
    If Text <> "" Then
        For I = 0 To sdbddUnidadesPedido.Rows - 1
            bm = sdbddUnidadesPedido.GetBookmark(I)
            If UCase(Text) = UCase(Mid(sdbddUnidadesPedido.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddUnidadesPedido.Bookmark = bm
                Exit For
            End If
        Next I
    End If

End Sub


Private Sub sdbddUnidadesPedido_RowLoaded(ByVal Bookmark As Variant)
    If sdbddUnidadesPedido.Columns("FC").Value <> "" Then
        sdbddUnidadesPedido.Columns("COD").CellStyleSet "Actuales"
        sdbddUnidadesPedido.Columns("DEN").CellStyleSet "Actuales"
        sdbddUnidadesPedido.Columns("FC").CellStyleSet "Actuales"
    End If
End Sub


Private Sub sdbddUnidadesPedido_ValidateList(Text As String, RtnPassed As Integer)
Dim sCod As String

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddUnidadesPedido.Columns(0).Value = Text Then
            RtnPassed = True
            Exit Sub
        End If
        
'        oUnidades.CargarTodasLasUnidades Text, , True
'        If Not oUnidades.Item(Text) Is Nothing Then
'            RtnPassed = True
'        Else
'            SDBGCondicionesSuministro.Columns("COD").Value = ""
'        End If
    
    End If

End Sub

Private Sub sdbgCondicionesSuministro_AfterDelete(RtnDispErrMsg As Integer)
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    If Me.Visible Then sdbgCondicionesSuministro.SetFocus
    If IsEmpty(sdbgCondicionesSuministro.RowBookmark(sdbgCondicionesSuministro.Row)) Then
        sdbgCondicionesSuministro.Bookmark = sdbgCondicionesSuministro.RowBookmark(sdbgCondicionesSuministro.Row - 1)
    Else
        sdbgCondicionesSuministro.Bookmark = sdbgCondicionesSuministro.RowBookmark(sdbgCondicionesSuministro.Row)
    End If

End Sub



Private Sub sdbgCondicionesSuministro_AfterUpdate(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If Not bModoEdicion Then Exit Sub
    
    
    cmdAnyadir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    
       
End Sub

Private Sub sdbgCondicionesSuministro_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    ''' * Objetivo: Confirmacion antes de eliminar
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer

    DispPromptMsg = 0

    If sdbgCondicionesSuministro.IsAddRow Then
        Cancel = True
        Exit Sub
    End If
    
    If frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Item(sdbgCondicionesSuministro.Row + 1).Defecto = True Then
        MsgBox m_sEliminarDefecto, vbExclamation
        Cancel = True
        Exit Sub
    End If
    
    irespuesta = oMensajes.PreguntaEliminar(m_sDeseaEliminar & " " & sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Value & "?")
    If irespuesta = vbNo Then
        Cancel = True
        Exit Sub
    End If

    If sdbgCondicionesSuministro.Rows > 0 Then
        sdbgCondicionesSuministro.SelBookmarks.Add sdbgCondicionesSuministro.Bookmark
        '''Eliminar de Base de datos
        teserror = frmCatalogo.oLineaCatalogoSeleccionada.EliminarCondicionSuministro(frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Item(sdbgCondicionesSuministro.Row + 1))
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            sdbgCondicionesSuministro.Refresh
            If Me.Visible Then sdbgCondicionesSuministro.SetFocus
            Exit Sub
        End If
    End If

End Sub

''' <summary>
''' Primero q las condiciones sean correctas y luego cargar las condiciones de bloqueo en el Catalogo.
''' </summary>
''' <param name="Cancel">Cancelar el update</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SDBGCondicionesSuministro_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    Dim LongitudUnidad As String
    Dim CantMinPed As Variant
    Dim CantMaxPed As Variant
    Dim CantMaxTotal As Variant
        
    ''' * Objetivo: Validar los datos
    bErrorModificacion = False
    If gCancelEventoBeforeUpdate Then GoTo Salir
    
    Cancel = False
    
    If Trim(sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Value) = "" Then
        oMensajes.NoValido "" 'sIdiCod Aqu� no est� definida la variable por eso lo comento GFA
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgCondicionesSuministro.Columns("UNI_PED_DEN").Value) = "" Then
        oMensajes.NoValido "" 'sIdiDen Aqu� no est� definida la variable por eso lo comento GFA
        Cancel = True
        GoTo Salir
    End If
    
    If Not IsNumeric(sdbgCondicionesSuministro.Columns("FC").Value) Or IsEmpty(sdbgCondicionesSuministro.Columns("FC").Value) Then
        oMensajes.NoValido m_sFC
        Cancel = True
        GoTo Salir
    End If
    
    If sdbgCondicionesSuministro.Columns("CANT_MIN_PED").Text <> "" Then
        If Not IsNumeric(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MIN_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) Then
            oMensajes.NoValido m_sCantMinPedido
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Text <> "" Then
        If Not IsNumeric(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) Then
            oMensajes.NoValido m_sCantMaxPedido
            Cancel = True
            GoTo Salir
        Else
            If Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MIN_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, "")) <> "" Then
                If CDbl(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) < CDbl(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MIN_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) Then
                     oMensajes.NoValido m_sCantMaxPedido
                     Cancel = True
                     GoTo Salir
                 End If
            End If
        End If
    End If
    If sdbgCondicionesSuministro.Columns("CANT_MAX_TOTAL").Text <> "" Then
        If Not IsNumeric(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_TOTAL").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) Then
            oMensajes.NoValido m_sCantMaxTotal
            Cancel = True
            GoTo Salir
        Else
            If Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, "")) <> "" Then
                If CDbl(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_TOTAL").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) < CDbl(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) Then
                    oMensajes.NoValido m_sCantMaxTotal
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
    End If
    
    'Diferenciamos si estamos a�adiendo o modificando
        
    If frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Count < Me.sdbgCondicionesSuministro.Rows Then
        LongitudUnidad = Len(sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Value) + 1
    
        CantMinPed = nulltostr(sdbgCondicionesSuministro.Columns("CANT_MIN_PED").Value)
        If Not (CantMinPed = "") Then
            CantMinPed = Left(CantMinPed, Len(CantMinPed) - LongitudUnidad)
        Else
            CantMinPed = Null
        End If
        CantMaxPed = nulltostr(sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Value)
        If Not (CantMaxPed = "") Then
            CantMaxPed = Left(CantMaxPed, Len(CantMaxPed) - LongitudUnidad)
        Else
            CantMaxPed = Null
        End If
        CantMaxTotal = nulltostr(sdbgCondicionesSuministro.Columns("CANT_MAX_TOTAL").Value)
        If Not (CantMaxTotal = "") Then
            CantMaxTotal = Left(CantMaxTotal, Len(CantMaxTotal) - LongitudUnidad)
        Else
            CantMaxTotal = Null
        End If
    
        frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Add frmCatalogo.oLineaCatalogoSeleccionada.Id, frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Count, , , , , , , , sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Value, sdbgCondicionesSuministro.Columns("UNI_PED_DEN").Value, sdbgCondicionesSuministro.Columns("FC").Value _
        , CantMinPed, , , , , , CantMaxPed, CantMaxTotal, CantMaxTotal, , , sdbgCondicionesSuministro.Columns("UNI_PED_DEFECTO").Value
        ''' Indicamos que se tiene que actualizar la linea de catalogo modificando la columna
        Randomize
        frmCatalogo.sdbgAdjudicaciones.Columns("COND_SUMINISTRO_HIDDEN").Value = CInt(Int((6 * Rnd()) + 1))
        
        Exit Sub
    Else

        'Estamos modificando una l�nea
        Dim oCondSuministro As CUnidadPedido
        If Not frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Item(sdbgCondicionesSuministro.Row + 1) Is Nothing Then
            Set oCondSuministro = frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Item(sdbgCondicionesSuministro.Row + 1)
            oCondSuministro.Defecto = sdbgCondicionesSuministro.Columns("UNI_PED_DEFECTO").Value
            If IsNumeric(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MIN_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) Then
                oCondSuministro.CantidadMinima = Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MIN_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))
            Else
                oCondSuministro.CantidadMinima = Null
            End If
            If IsNumeric(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) Then
                oCondSuministro.CantidadMaximaPed = Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_PED").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))
            Else
                oCondSuministro.CantidadMaximaPed = Null
            End If
            If IsNumeric(Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_TOTAL").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))) Then
                oCondSuministro.CantidadMaximaTotal = Trim(Replace(sdbgCondicionesSuministro.Columns("CANT_MAX_TOTAL").Text, sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Text, ""))
            Else
                oCondSuministro.CantidadMaximaTotal = Null
            End If
            If oCondSuministro.Defecto = True Then
                If Me.txtCantMaxTotalUnidBase.Text <> "" And IsNumeric(Me.txtCantMaxTotalUnidBase.Text) Then
                    oCondSuministro.CantidadMaximaTotal_UC = Me.txtCantMaxTotalUnidBase.Text
                Else
                    oCondSuministro.CantidadMaximaTotal_UC = Null
                End If
            Else
                oCondSuministro.CantidadMaximaTotal_UC = Null
            End If
            oCondSuministro.FactorConversion = sdbgCondicionesSuministro.Columns("FC").Value
            ''' Indicamos que se tiene que actualizar la linea de catalogo modificando la columna
            Randomize
            frmCatalogo.sdbgAdjudicaciones.Columns("COND_SUMINISTRO_HIDDEN").Value = CInt(Int((6 * Rnd()) + 1))
            Exit Sub
        End If

End If

Salir:
        bErrorModificacion = True

End Sub


Private Sub SDBGCondicionesSuministro_Change()
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    If gCancelEventoChange Then
        gCancelEventoChange = False
        Exit Sub
    End If
    
    If cmdDeshacer.Enabled = False Then
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
    End If
    
    Dim RowActual As Integer
    If Me.sdbgCondicionesSuministro.col <> -1 Then
        If Me.sdbgCondicionesSuministro.Columns(Me.sdbgCondicionesSuministro.col).Name = "UNI_PED_DEFECTO" And Me.sdbgCondicionesSuministro.Columns(Me.sdbgCondicionesSuministro.col).Value = True Then
            'Si marca por defecto una unidad de pedido, quitamos la que estaba hasta ese momento marcada
            If RowDefecto > -1 Then
                RowActual = Me.sdbgCondicionesSuministro.Row
                gCancelEventoBeforeUpdate = True
                Me.sdbgCondicionesSuministro.Row = RowDefecto
                gCancelEventoChange = True
                Me.sdbgCondicionesSuministro.Columns("UNI_PED_DEFECTO").Value = False
                frmCatalogo.oLineaCatalogoSeleccionada.CondicionesSuministro.Item(RowDefecto + 1).Defecto = False
                Me.sdbgCondicionesSuministro.Row = RowActual
                RowDefecto = RowActual
                gCancelEventoBeforeUpdate = False
                'Hacemos un cambio en la fila para que se realice una actualizacion de las condiciones de suministro
                Randomize
                Me.sdbgCondicionesSuministro.Columns("ACTUALIZAR").Value = CInt(Int((6 * Rnd()) + 1))
            End If
        ElseIf Me.sdbgCondicionesSuministro.Columns(Me.sdbgCondicionesSuministro.col).Name = "UNI_PED_DEFECTO" And Me.sdbgCondicionesSuministro.Columns(Me.sdbgCondicionesSuministro.col).Value <> 1 Then
            'No se puede descheckear la unidad de pedido por defecto
            gCancelEventoChange = True
            sdbgCondicionesSuministro.Columns("UNI_PED_DEFECTO").Value = True
        End If
    End If
End Sub


Private Sub SDBGCondicionesSuministro_KeyPress(KeyAscii As Integer)
    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgCondicionesSuministro.DataChanged = False Then
            
            sdbgCondicionesSuministro.CancelUpdate
            sdbgCondicionesSuministro.DataChanged = False
            If Not sdbgCondicionesSuministro.IsAddRow Then
                cmdAnyadir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
            Else
                cmdAnyadir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
            End If
        Else
        
            If sdbgCondicionesSuministro.IsAddRow Then
                cmdAnyadir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
            End If
            
        End If
        
    End If

End Sub

''' <summary>
''' Habilitar/deshabilitar los botones de pantalla
''' </summary>
''' <param name="LastRow">Fila de la q vienes</param>
''' <param name="LastCol">Columna de la q vienes</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SDBGCondicionesSuministro_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    If Not sdbgCondicionesSuministro.IsAddRow Then
        If sdbgCondicionesSuministro.DataChanged = False Then
            cmdAnyadir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
        End If
    Else
        sdbgCondicionesSuministro.Columns("UNI_PED_CODIGO").Locked = False
        Me.sdbddUnidadesPedido.Enabled = True
        If sdbgCondicionesSuministro.DataChanged = True Then cmdDeshacer.Enabled = True
        If Not IsNull(LastRow) Then
            If CStr(LastRow) <> CStr(sdbgCondicionesSuministro.Bookmark) Then
                sdbgCondicionesSuministro.col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False
    End If
End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''RECEPCION POR IMPORTE''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub cmdEdicionPorImporte_Click()
    If Not bModoEdicionImporte Then
        Me.cmdEdicionPorImporte.caption = m_sConsulta
        Me.txtImporteMaxPedido.Locked = False
        Me.txtImporteMaxTotal.Locked = False
        Me.txtImporteMinPedido.Locked = False
        bModoEdicionImporte = True
    Else
        Me.cmdEdicionPorImporte.caption = m_sEdicion
        Me.txtImporteMaxPedido.Locked = True
        Me.txtImporteMaxTotal.Locked = True
        Me.txtImporteMinPedido.Locked = True
        bModoEdicionImporte = False
    End If
End Sub

Private Sub txtImporteMaxPedido_LostFocus()
    If Me.txtImporteMaxPedido.Text <> "" Then
        If IsNumeric(Me.txtImporteMaxPedido.Text) Then
           txtImporteMaxPedido.Text = FormateoNumerico(txtImporteMaxPedido.Text, m_sFormatoNumber)
        End If
    End If
End Sub

Private Sub txtImporteMaxTotal_LostFocus()
    If Me.txtImporteMaxTotal.Text <> "" Then
        If IsNumeric(Me.txtImporteMaxTotal.Text) Then
           txtImporteMaxTotal.Text = FormateoNumerico(txtImporteMaxTotal.Text, m_sFormatoNumber)
        End If
    End If
End Sub

Private Sub txtImporteMinPedido_LostFocus()
    If Me.txtImporteMinPedido.Text <> "" Then
        If IsNumeric(Me.txtImporteMinPedido.Text) Then
           txtImporteMinPedido.Text = FormateoNumerico(txtImporteMinPedido.Text, m_sFormatoNumber)
        End If
    End If
End Sub
