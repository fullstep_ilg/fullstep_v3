VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmProgreso 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Espere, por favor++++"
   ClientHeight    =   1095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6090
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1095
   ScaleWidth      =   6090
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   600
      Left            =   0
      ScaleHeight     =   600
      ScaleWidth      =   6135
      TabIndex        =   1
      Top             =   480
      Width           =   6135
      Begin MSComctlLib.ProgressBar ProgressBar 
         Height          =   315
         Left            =   200
         TabIndex        =   2
         Top             =   150
         Width           =   5750
         _ExtentX        =   10160
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   1
      End
   End
   Begin VB.Label lblTransferir 
      Alignment       =   2  'Center
      Caption         =   "Transfiriendo archivo adjunto...+++"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   6135
   End
End
Attribute VB_Name = "frmProgreso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROGRESO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).value
        Ador.MoveNext
        lblTransferir.caption = Ador(0).value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
            
End Sub

Private Sub Form_Deactivate()
    If Me.Visible Then Me.SetFocus
End Sub

Private Sub Form_Load()
    CargarRecursos
End Sub
