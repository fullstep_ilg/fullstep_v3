Attribute VB_Name = "basSeguridad"
Attribute VB_Description = "Este m�dulo contiene la definici�n de los identificadores correspondientes a cada propiedad de seguridad gestionable por la aplicaci�n.\r\nY un m�todo que carga la estructura base de las acciones de seguridad, en un treeview que recibe como par�metro."
Private sIdioma() As String

Private Sub CargarRecursosEstructuraAcc()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(Seguridad, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 76)
        For i = 1 To 76
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        
        Ador.Close
    End If
End Sub

''' <summary>
''' Genera el arbol para la configuracion de acciones
''' </summary>
''' <param name="tree">El treeview donde se muestra el arbol</param>
''' <returns></returns>
''' <remarks>Llamada desde:frmPerfil.Form_Load;frmUsuarios.Form_Load Tiempo m�ximo: 0,1</remarks>
Public Sub GenerarEstructuraAccionesFSEP(ByRef tree As TreeView)

    Dim nodx As node
    
    CargarRecursosEstructuraAcc
    
    tree.Nodes.clear
    Set nodx = tree.Nodes.Add(, , "Root", sIdioma(1), "Seguridad")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    Set nodx = tree.Nodes.Add("Root", tvwChild, "Mantenimiento", sIdioma(2), "Candado")
    nodx.Tag = "Mantenimiento"
    Set nodx = tree.Nodes.Add("Root", tvwChild, "Pedidos", sIdioma(57), "Candado")
    nodx.Tag = "Pedido"
    Set nodx = tree.Nodes.Add("Root", tvwChild, "Parametros", sIdioma(6), "Candado")
    nodx.Tag = "Parametros"
    
    'Compruebo si hay autenticaci�n de windows en GS y WEB para mostrar la opcion de seguridad
    If Not (gParametrosGenerales.giWinSecurity = Windows And gParametrosGenerales.giWinSecurityWeb = Windows) Then
        Set nodx = tree.Nodes.Add("Root", tvwChild, "Seguridad", sIdioma(7), "Candado")
        nodx.Tag = "Seguridad"
    End If
 
    '********** Mantenimientos ************
    Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Monedas", sIdioma(9), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.MONConsultar)
    Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Paises", sIdioma(10), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PAIConsultar)
    
    
    '******Estructura de la organizacion********
    Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "EstrOrg", sIdioma(12), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.ORGConsultar)
            
    ' ********Estructura de materiales*********
    Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "EstrMat", sIdioma(15), "Candado")
    nodx.Tag = "EstrMat"
    Set nodx = tree.Nodes.Add("EstrMat", tvwChild, "Materiales", sIdioma(16), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.MATConsultar)
    Set nodx = tree.Nodes.Add("EstrMat", tvwChild, "Unidades", sIdioma(17), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.UNIConsultar)
    Set nodx = tree.Nodes.Add("EstrMat", tvwChild, "Atributos", sIdioma(64), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.ATRIBConsultar)
    
    ' *********Proveedores **************
    Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Proveedores", sIdioma(22), "Candado")
    nodx.Tag = "Proveedores"
    Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "DatBas", sIdioma(23), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PROVEConsultar)
               
    If basParametros.gParametrosGenerales.giINSTWEB = ConPortal Then
        Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "PROVEPortal", sIdioma(24), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PROVEPortalConsultar)
    End If
                
    Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "GruPorPro", sIdioma(25), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.GRUPPorPROVEConsultar)
    Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "ProPorGru", sIdioma(26), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PROVEPorGRUPConsultar)
               
               
    ' *********** Formas de Pago **********
    Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Formas de pago", sIdioma(51), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PAGConsultar)

    ' *********** V�as de Pago **********
    Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "V�as de pago", sIdioma(75), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.VIAPAGConsultar)
    
    ' *********** Tipos de impuestos **********
    Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Tipos de impuestos", sIdioma(76), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.TipImpConsultar)
    
    
    ' *********** Pedidos *****************
    Set nodx = tree.Nodes.Add("Pedidos", tvwChild, "PedCat", sIdioma(58), "Candado")
    nodx.Tag = "PedCat"

    Set nodx = tree.Nodes.Add("PedCat", tvwChild, "PedCatConf", sIdioma(59), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.CATALOGConsultar)
    
    Set nodx = tree.Nodes.Add("PedCat", tvwChild, "PedCatDatExt", sIdioma(63), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.CATDatExtConsultar)

    Set nodx = tree.Nodes.Add("Pedidos", tvwChild, "PedSegui", sIdioma(60), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PEDSEGConsultar)

    Set nodx = tree.Nodes.Add("Pedidos", tvwChild, "PeddRec", sIdioma(62), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PEDRECConsultar)
    
    
    '**********Par�metros***********
    Set nodx = tree.Nodes.Add("Parametros", tvwChild, "ParGen", sIdioma(47), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PARGEN)
    
    ''' Integraci�n
    If basParametros.gParametrosGenerales.gbIntegracion Then
        Set nodx = tree.Nodes.Add("Parametros", tvwChild, "Integracion", sIdioma(66), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PARGENInt)
    End If
                
    Set nodx = tree.Nodes.Add("Parametros", tvwChild, "ConfInst", sIdioma(48), "Candado")
    nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.CONFINST)
    
    '**********Seguridad****************************
    'Compruebo si hay autenticaci�n de windows en GS y WEB para mostrar la opcion de seguridad
    If Not (gParametrosGenerales.giWinSecurity = Windows And gParametrosGenerales.giWinSecurityWeb = Windows) Then
        Set nodx = tree.Nodes.Add("Seguridad", tvwChild, "CamCon", sIdioma(49), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.CAMBContrasenya)
    End If
                     
End Sub

''' <summary>
''' Genera el arbol para la configuracion de acciones
''' </summary>
''' <param name="tree">El treeview donde se muestra el arbol</param>
''' <returns></returns>
''' <remarks>Llamada desde:frmPerfil.Form_Load;frmUsuarios.Form_Load Tiempo m�ximo: 0,1</remarks>
Public Sub GenerarEstructuraAcciones(ByRef tree As TreeView)

        Dim nodx As node
        Dim sProy As String
        Dim sEvolu As String
        
        CargarRecursosEstructuraAcc
        
        tree.Nodes.clear
        Set nodx = tree.Nodes.Add(, , "Root", sIdioma(1), "Seguridad")
        nodx.Tag = "Raiz"
        nodx.Expanded = True

        Set nodx = tree.Nodes.Add("Root", tvwChild, "Mantenimiento", sIdioma(2), "Candado")
        nodx.Tag = "Mantenimiento"
        If gParametrosGenerales.gbSolicitudesCompras Then
            Set nodx = tree.Nodes.Add("Root", tvwChild, "Solicitudes", sIdioma(70), "Candado")
            nodx.Tag = "Solicitudes"
        End If
        Set nodx = tree.Nodes.Add("Root", tvwChild, "Procesos", sIdioma(3), "Candado")
        nodx.Tag = "Procesos"
        Set nodx = tree.Nodes.Add("Root", tvwChild, "Reuniones", sIdioma(4), "Candado")
        nodx.Tag = "Reuniones"
        If gParametrosGenerales.gbPedidosAprov Or gParametrosGenerales.gbPedidosDirectos Then
            Set nodx = tree.Nodes.Add("Root", tvwChild, "Pedidos", sIdioma(57), "Candado")
            nodx.Tag = "Pedidos"
        End If
        
        Set nodx = tree.Nodes.Add("Root", tvwChild, "Informes", sIdioma(5), "Candado")
        nodx.Tag = "Informes"
        Set nodx = tree.Nodes.Add("Root", tvwChild, "Parametros", sIdioma(6), "Candado")
        nodx.Tag = "Opciones"
        'Compruebo si hay autenticaci�n de windows en GS y WEB para mostrar la opcion de seguridad
        'Adem�s compruebo si hay autenticaci�n de LDAP en GS y WEB para mostrar la opcion de seguridad
        If Not ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) _
        And (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP)) Then
            Set nodx = tree.Nodes.Add("Root", tvwChild, "Seguridad", sIdioma(7), "Candado")
            nodx.Tag = "Seguridad"
        End If
        Set nodx = tree.Nodes.Add("Root", tvwChild, "Ver", sIdioma(8), "Candado")
        nodx.Tag = "Ver"
        
        '********** Mantenimientos ************
        
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Monedas", sIdioma(9), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.MONConsultar)
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Paises", sIdioma(10), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PAIConsultar)
        
        '******Estructura de la organizacion********
        
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "EstrOrg", sIdioma(12), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.ORGConsultar)
        ' ********Estructura de materiales*********
        
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "EstrMat", sIdioma(15), "Candado")
        nodx.Tag = "EstrMat"
            Set nodx = tree.Nodes.Add("EstrMat", tvwChild, "Materiales", sIdioma(16), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.MATConsultar)
            Set nodx = tree.Nodes.Add("EstrMat", tvwChild, "Unidades", sIdioma(17), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.UNIConsultar)
            Set nodx = tree.Nodes.Add("EstrMat", tvwChild, "Atributos", sIdioma(64), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.ATRIBConsultar)
        
        ' *******Estructura de compras*********
        
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "EstrCom", sIdioma(18), "Candado")
        nodx.Tag = "EstrCom"
            Set nodx = tree.Nodes.Add("EstrCom", tvwChild, "Compradores", sIdioma(19), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.COMPConsultar)
            Set nodx = tree.Nodes.Add("EstrCom", tvwChild, "GruPorCom", sIdioma(20), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.GRUPPorCOMPConsultar)
            Set nodx = tree.Nodes.Add("EstrCom", tvwChild, "ComPorGru", sIdioma(21), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.COMPPorGRUPConsultar)
        
        ' *********Proveedores **************
        
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Proveedores", sIdioma(22), "Candado")
        nodx.Tag = "Proveedores"
            Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "DatBas", sIdioma(23), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PROVEConsultar)
            
            If basParametros.gParametrosGenerales.giINSTWEB = ConPortal Then
                Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "PROVEPortal", sIdioma(24), "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PROVEPortalConsultar)
            End If
            
            Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "GruPorPro", sIdioma(25), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.GRUPPorPROVEConsultar)
            Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "ProPorGru", sIdioma(26), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PROVEPorGRUPConsultar)
                        
            If basParametros.gParametrosGenerales.gbOblProveEqp Then
                Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "EqpPorPro", sIdioma(27), "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.EQPPorPROVEConsultar)
                Set nodx = tree.Nodes.Add("Proveedores", tvwChild, "ProPorEqp", sIdioma(28), "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PROVEPorEQPConsultar)
            End If
        
        
        ' ********* Presupuestos ********
        
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Presupuestos", sIdioma(29), "Candado")
        nodx.Tag = "Presupuestos"
                
            Set nodx = tree.Nodes.Add("Presupuestos", tvwChild, "PrePorMat", sIdioma(30), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PRESPorMATConsultar)
  
  
            If basParametros.gParametrosGenerales.gbUsarPres1 Or basParametros.gParametrosGenerales.gbUsarPres2 Then
               Set nodx = tree.Nodes.Add("Presupuestos", tvwChild, "Anuales", sIdioma(56), "Candado")
               nodx.Tag = "Anuales"
              
               If basParametros.gParametrosGenerales.gbUsarPres1 Then
                 Set nodx = tree.Nodes.Add("Anuales", tvwChild, "PrePorPro", basParametros.gParametrosGenerales.gsPlurPres1, "Candado")
                 nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PRESPorPROYConsultar)
               End If
               If basParametros.gParametrosGenerales.gbUsarPres2 Then
                 Set nodx = tree.Nodes.Add("Anuales", tvwChild, "PrePorCon", basParametros.gParametrosGenerales.gsPlurPres2, "Candado")
                 nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PRESPorParConConsultar)
               End If
               
            End If
            If basParametros.gParametrosGenerales.gbUsarPres3 Then
               Set nodx = tree.Nodes.Add("Presupuestos", tvwChild, "PrePorCon3", basParametros.gParametrosGenerales.gsPlurPres3, "Candado")
               nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PRESConcepto3Consultar)
            End If
            If basParametros.gParametrosGenerales.gbUsarPres4 Then
               Set nodx = tree.Nodes.Add("Presupuestos", tvwChild, "PrePorCon4", basParametros.gParametrosGenerales.gsplurpres4, "Candado")
               nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PRESConcepto4Consultar)
            End If
            If gParametrosGenerales.gsAccesoFSSM <> TipoAccesoFSSM.SinAcceso Then
               Set nodx = tree.Nodes.Add("Presupuestos", tvwChild, "Partidas382", sIdioma(74), "Candado")
               nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PRESConcepto5CrearArboles)
            End If
        ' *********** Formas de Pago **********
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Formas de pago", sIdioma(51), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PAGConsultar)

        ' *********** V�as de Pago **********
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "V�as de pago", sIdioma(75), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.VIAPAGConsultar)
        
        ' *********** Tipos de impuestos **********
        Set nodx = tree.Nodes.Add("Mantenimiento", tvwChild, "Tipos de impuestos", sIdioma(76), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.TipImpConsultar)
        
        
        '*******Solicitudes***********
        If gParametrosGenerales.gbSolicitudesCompras Then
             Set nodx = tree.Nodes.Add("Solicitudes", tvwChild, "FormSolicit", sIdioma(72), "Candado")  'Formularios
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.FORMULARIOConsultar)
            Set nodx = tree.Nodes.Add("Solicitudes", tvwChild, "ConfigSolicit", sIdioma(71), "Candado")  'Cumplimentaci�n
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.SOLCONFConsultar)
           
        End If
        
        ' *********** Procesos**********
        If basParametros.gParametrosGenerales.gbSolicitudesCompras = True Then
            Set nodx = tree.Nodes.Add("Procesos", tvwChild, "SeguimSolicitudes", sIdioma(67), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.SOLICConsultar)
        End If
        
        Set nodx = tree.Nodes.Add("Procesos", tvwChild, "Apertura", sIdioma(31), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.APEConsultar)
        Set nodx = tree.Nodes.Add("Procesos", tvwChild, "SelDePro", sIdioma(32), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.SELPROVEConsultar)
        Set nodx = tree.Nodes.Add("Procesos", tvwChild, "ComuniProve", sIdioma(33), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.COMUNIPROVEConsulta)
        ' *********** Procesos - Recepci�n de ofertas**********
        Set nodx = tree.Nodes.Add("Procesos", tvwChild, "RecOfe", sIdioma(34), "Candado")
        nodx.Tag = "RecOfe"
            Set nodx = tree.Nodes.Add("RecOfe", tvwChild, "OfeBuzon", sIdioma(52), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.BUZOFEConsultar)
            Set nodx = tree.Nodes.Add("RecOfe", tvwChild, "OfeProce", sIdioma(53), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.RECOFEConsultar)
        Set nodx = tree.Nodes.Add("Procesos", tvwChild, "Comparativa", sIdioma(35), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.OFECOMPAdjConsulta)
           
        
        
        
        ' *************Reuniones*********
        
        Set nodx = tree.Nodes.Add("Reuniones", tvwChild, "PlaDeReu", sIdioma(36), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PLANREUConsultar)
        Set nodx = tree.Nodes.Add("Reuniones", tvwChild, "ResDeReu", sIdioma(37), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.RESREUConsultar)
            
        
       
        ' *********** "Pedidos **************
        If gParametrosGenerales.gbPedidosAprov Or gParametrosGenerales.gbPedidosDirectos Then
            If gParametrosGenerales.gbPedidosAprov Then
                Set nodx = tree.Nodes.Add("Pedidos", tvwChild, "PedCat", sIdioma(58), "Candado")
                nodx.Tag = "PedCat"
                Set nodx = tree.Nodes.Add("PedCat", tvwChild, "PedCatConf", sIdioma(59), "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.CATALOGConsultar)
                Set nodx = tree.Nodes.Add("PedCat", tvwChild, "PedCatDatExt", sIdioma(63), "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.CATDatExtConsultar)
            End If
            
            If gParametrosGenerales.gbPedidosDirectos Then
                Set nodx = tree.Nodes.Add("Pedidos", tvwChild, "Peddir", sIdioma(61), "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PEDDIRConsultar)
            End If
            
            Set nodx = tree.Nodes.Add("Pedidos", tvwChild, "PedSegui", sIdioma(60), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PEDSEGConsultar)
            
            Set nodx = tree.Nodes.Add("Pedidos", tvwChild, "PeddRec", sIdioma(62), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PEDRECConsultar)
        End If
        
        
        ' **********Informes**********
        
        Set nodx = tree.Nodes.Add("Informes", tvwChild, "Negociados", sIdioma(38), "Candado")
        nodx.Tag = "Negociados"
            Set nodx = tree.Nodes.Add("Negociados", tvwChild, "NegGen", sIdioma(39), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfNegGenConsultar)
            Set nodx = tree.Nodes.Add("Negociados", tvwChild, "NegMat", sIdioma(40), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfNegmatConsultar)
            Set nodx = tree.Nodes.Add("Negociados", tvwChild, "NegEqpRes", sIdioma(41), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfNegEqpResConsultar)
            Set nodx = tree.Nodes.Add("Negociados", tvwChild, "NegEqp", sIdioma(42), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfNegEqpConsultar)
            If gParametrosGenerales.gbUsarPres3 = True Then
                Set nodx = tree.Nodes.Add("Negociados", tvwChild, "NegConcep3", gParametrosGenerales.gsPlurPres3, "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfNegCon3Consultar)
            End If
            If gParametrosGenerales.gbUsarPres4 = True Then
                Set nodx = tree.Nodes.Add("Negociados", tvwChild, "NegConcep4", gParametrosGenerales.gsplurpres4, "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfNegCon4Consultar)
            End If
            
        Set nodx = tree.Nodes.Add("Informes", tvwChild, "Aplicados", sIdioma(43), "Candado")
        nodx.Tag = "Aplicados"
            Set nodx = tree.Nodes.Add("Aplicados", tvwChild, "ApliGen", sIdioma(44), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfAplGenConsultar)
            Set nodx = tree.Nodes.Add("Aplicados", tvwChild, "ApliMat", sIdioma(45), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfAplMatConsultar)
            If gParametrosGenerales.gbUsarPres1 = True Then
                Set nodx = tree.Nodes.Add("Aplicados", tvwChild, "ApliProy", gParametrosGenerales.gsPlurPres1, "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfAplProyConsultar)
            End If
            If gParametrosGenerales.gbUsarPres2 = True Then
                Set nodx = tree.Nodes.Add("Aplicados", tvwChild, "ApliPar", gParametrosGenerales.gsPlurPres2, "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfAplPartConsultar)
            End If
            If gParametrosGenerales.gbUsarPres3 = True Then
                Set nodx = tree.Nodes.Add("Aplicados", tvwChild, "ApliConcep3", gParametrosGenerales.gsPlurPres3, "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfAplCon3Consultar)
            End If
            If gParametrosGenerales.gbUsarPres4 = True Then
                Set nodx = tree.Nodes.Add("Aplicados", tvwChild, "ApliConcep4", gParametrosGenerales.gsplurpres4, "Candado")
                nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfAplCon4Consultar)
            End If
            
            Set nodx = tree.Nodes.Add("Aplicados", tvwChild, "ApliUO", sIdioma(46), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfAplUOConsultar)
        
        sEvolu = sIdioma(54)
        sProy = gParametrosGenerales.gsPlurPres1
        sEvolu = Replace(sEvolu, "PROY1", sProy)
        If gParametrosGenerales.gbUsarPres1 = True Then
            Set nodx = tree.Nodes.Add("Informes", tvwChild, sIdioma(54), sEvolu, "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfEvolucionConcepto1Consultar)
        End If
        sEvolu = sIdioma(55)
        sProy = gParametrosGenerales.gsPlurPres2
        sEvolu = Replace(sEvolu, "PROY2", sProy)
        If gParametrosGenerales.gbUsarPres2 = True Then
            Set nodx = tree.Nodes.Add("Informes", tvwChild, sIdioma(55), sEvolu, "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.InfEvolucionConcepto2Consultar)
        End If
        
        Set nodx = tree.Nodes.Add("Informes", tvwChild, "CalculoAho", sIdioma(69), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.AhorrosActualizar)

        '**********Par�metros***********
    
        Set nodx = tree.Nodes.Add("Parametros", tvwChild, "ParGen", sIdioma(47), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PARGEN)
        
        ''' Integraci�n
        If basParametros.gParametrosGenerales.gbIntegracion Then
            Set nodx = tree.Nodes.Add("Parametros", tvwChild, "Integracion", sIdioma(66), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PARGENInt)
        End If
       
        Set nodx = tree.Nodes.Add("Parametros", tvwChild, "ConfInst", sIdioma(48), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.CONFINST)
        
        Set nodx = tree.Nodes.Add("Parametros", tvwChild, "PlantillasProce", sIdioma(65), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.PLANTILLASConsultar)
        
    '**********Seguridad****************************
        'Compruebo si hay autenticaci�n de windows en GS y WEB para mostrar la opcion de seguridad
        ' Adem�s compruebo si hay autenticaci�n de LDAP en GS y WEB para mostrar la opcion de seguridad
        If Not ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) _
        And (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP)) Then
            Set nodx = tree.Nodes.Add("Seguridad", tvwChild, "CamCon", sIdioma(49), "Candado")
            nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.CAMBContrasenya)
        End If
    
    '*********Estado de procesos********************
        
        Set nodx = tree.Nodes.Add("Ver", tvwChild, "EstProce", sIdioma(50), "Candado")
        nodx.Tag = "ACC" & CStr(AccionesDeSeguridad.ESTProceConsulta)
        
            
End Sub

Public Sub RegistrarAccion(ByVal idAccion As Integer, ByVal Cadena As String)
    FSGSLibrary.RegistrarAccion idAccion, Cadena, oUsuarioSummit.Cod, gParametrosGenerales.gbActivLog, basPublic.oGestorSeguridad
End Sub

''' <summary>Indica si debe cargarse una acci�n de seguridad</summary>
''' <param name="iAccion">Acci�n</param>
''' <returns>Booleano indicando si puede cargarse la acci�n</returns>
''' <remarks>Llamada desde: frmPerfil</remarks>
''' <revision>LTG 06/06/2012</revision>
Public Function NoCargarAccion(iAccion As Integer) As Boolean
    Dim bNoCargar As Boolean

    bNoCargar = True
    NoCargarAccion = bNoCargar
    
    If basParametros.gParametrosGenerales.gbOblProveEqp = False And (iAccion = PROVERestEquipo Or iAccion = GRUPPorPROVERestEquipo Or iAccion = ProvePorGRUPRestEquipo) Then
        'No cargar esta acci�n
        Exit Function
    ElseIf gParametrosGenerales.gbSubasta = False And (iAccion = APEConsultarSubasta Or iAccion = APEModifSubasta) Then
        'Si la instalaci�n no permite subasta no se cargan las 2 acciones correspondientes a la subasta
        Exit Function
    'El par�metro gParametrosGenerales.giINSTWEB siempre tendr� el valor  ConPortal = 2, el resto de valores no se consideran
    'ElseIf gParametrosGenerales.giINSTWEB <> conweb And (iAccion = PROVEWebConsultar Or iAccion = PROVEWebmodificar) Then
    ElseIf (iAccion = PROVEWebConsultar Or iAccion = PROVEWebmodificar) Then
        'sI INSTALACION NO BUYSITE no acciones de acceso web en prove
        Exit Function
    ElseIf (gParametrosGenerales.gbPedidosAprov = False) And ((iAccion = PEDSEGSoloAprov) Or (iAccion = PEDSEGRestUsuAprov) Or (iAccion = PEDSegModifPrecResto)) Then
        'sin pedidos de aprovisionamiento no se cargan acciones
            Exit Function
    ElseIf (gParametrosGenerales.gbPedidosAprov = False) And ((iAccion = PEDRECSoloAprov) Or (iAccion = PEDRECRestUsuAprov)) Then
        'sin pedidos directos no se cargan acciones
            Exit Function
    ElseIf (gParametrosGenerales.gbPedidosDirectos = False) And ((iAccion = PEDSEGSoloDirectos) Or (iAccion = PEDRECSoloDirectos) Or (iAccion = PEDSegModifPrecAdjDir) Or (iAccion = PEDSegModifPrecComite)) Then
        'No cargar esta acci�n
            Exit Function
    ElseIf (gParametrosGenerales.gbPedidosERP = False) And ((iAccion = PEDSEGErp) Or (iAccion = PEDRECErp)) Then
            Exit Function
    ElseIf (Not gParametrosGenerales.gbPedidosERP Or Not gParametrosGenerales.gbUsarPres1) And iAccion = PEDSEGPermitirImputaciones_GS1 Then
            Exit Function
    ElseIf (Not gParametrosGenerales.gbPedidosERP Or Not gParametrosGenerales.gbUsarPres2) And iAccion = PEDSEGPermitirImputaciones_GS2 Then
            Exit Function
    ElseIf (Not gParametrosGenerales.gbPedidosERP Or Not gParametrosGenerales.gbUsarPres3) And iAccion = PEDSEGPermitirImputaciones_GS3 Then
            Exit Function
    ElseIf (Not gParametrosGenerales.gbPedidosERP Or Not gParametrosGenerales.gbUsarPres4) And iAccion = PEDSEGPermitirImputaciones_GS4 Then
            Exit Function
    ElseIf (Not gParametrosGenerales.gbUsarPres1) And (iAccion = 20224 Or iAccion = 20225 Or iAccion = 20116 Or iAccion = 20117 Or iAccion = 11702 Or iAccion = 12026 Or iAccion = 14701 Or iAccion = 18102) Then
        'No cargar esta acci�n
        Exit Function
    ElseIf (Not gParametrosGenerales.gbUsarPres2) And (iAccion = 20226 Or iAccion = 20227 Or iAccion = 20118 Or iAccion = 20119 Or iAccion = 11902 Or iAccion = 12027 Or iAccion = 14801 Or iAccion = 19102) Then
        'No cargar esta acci�n
        Exit Function
    ElseIf (Not gParametrosGenerales.gbUsarPres3) And (iAccion = 20228 Or iAccion = 20229 Or iAccion = 20120 Or iAccion = 20121 Or iAccion = 19202 Or iAccion = 12028 Or iAccion = 19401 Or iAccion = 19601) Then
        'No cargar esta acci�n
        Exit Function
    ElseIf (Not gParametrosGenerales.gbUsarPres4) And (iAccion = 20230 Or iAccion = 20231 Or iAccion = 20122 Or iAccion = 20123 Or iAccion = 19302 Or iAccion = 12029 Or iAccion = 19501 Or iAccion = 19701) Then
        'No cargar esta acci�n
        Exit Function
    ElseIf (Not gParametrosGenerales.gbUsarPedPres1) And (iAccion = 20224 Or iAccion = 20225 Or iAccion = 20116 Or iAccion = 20117) Then
        'No cargar esta acci�n
        Exit Function
    ElseIf (Not gParametrosGenerales.gbUsarPedPres2) And (iAccion = 20226 Or iAccion = 20227 Or iAccion = 20118 Or iAccion = 20119) Then
        'No cargar esta acci�n
        Exit Function
    ElseIf (Not gParametrosGenerales.gbUsarPedPres3) And (iAccion = 20228 Or iAccion = 20229 Or iAccion = 20120 Or iAccion = 20121) Then
        'No cargar esta acci�n
        Exit Function
    ElseIf (Not gParametrosGenerales.gbUsarPedPres4) And (iAccion = 20230 Or iAccion = 20231 Or iAccion = 20122 Or iAccion = 20123) Then
        'No cargar esta acci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.mon) And (iAccion = MONModificar Or iAccion = MONActualizarCambio)) Then
        'Integraci�n MONEDAS Sentido entrada, no hay opci�n de modificaci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Pag) And iAccion = PAGModificar) Then
        'Integraci�n PAGOS Sentido entrada, no hay opci�n de modificaci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.ViaPag) And iAccion = viapagmodificar) Then
        'Integraci�n VIAS DE PAGOS Sentido entrada, no hay opci�n de modificaci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Pai) And iAccion = PAIModificar) Then
        'Integraci�n PAISES Sentido entrada, no hay opci�n de modificaci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Provi) And iAccion = PROVIModificar) Then
        'Integraci�n PROVINCIAS Sentido entrada, no hay opci�n de modificaci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.uni) And iAccion = UNIModificar) Then
        'Integraci�n UNIDADES Sentido entrada, no hay opci�n de modificaci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Dest) And iAccion = ORGModificarDest) Then
        'Integraci�n DESTINOS Sentido entrada, no hay opci�n de modificaci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.art4) And iAccion = MATArtiModificar) Then
        'Integraci�n ARTICULOS Sentido entrada, no hay opci�n de modificaci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Prove) And (iAccion = PROVEAnyadir Or iAccion = PROVEEliminar Or iAccion = PROVEModificarDatGen)) Then
        'Integraci�n PROVEEDORES Sentido entrada, no hay opci�n de modificaci�n
        Exit Function
    ElseIf (basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.PED_directo) And (iAccion = PEDDIREmitir Or iAccion = PEDDIRModifPrecioAdjDir Or iAccion = PEDDIRModifPrecioComite Or iAccion = PEDDIRSaltarCantAdj)) Then
        'Integraci�n PEDIDOS DIRECTOS Sentido entrada, no hay opci�n de emisi�n, modificaci�n precio, ni superar cantidad adjudicada
        Exit Function
    ElseIf basParametros.gParametrosGenerales.gbSolicitudesCompras = False And (iAccion = APESolicAsignar Or iAccion = APESolicRestAsig Or iAccion = APESolicRestEquipo _
        Or iAccion = APESolicRestUO Or iAccion = CATALOGSolicAsignar Or iAccion = CATALOGSolicRestAsig Or iAccion = CATALOGSolicRestEquipo _
        Or iAccion = CATALOGSolicRestUO Or iAccion = PEDSegSolicMostrar Or iAccion = PEDSegSolicRestAsig _
        Or iAccion = PEDSegSolicRestEquipo Or iAccion = PEDSegSolicRestUO Or iAccion = APEModifProcDesdeSolic) Then
        'No cargar las acciones de solicitudes de compras
        Exit Function
    ElseIf gParametrosGenerales.gbCodPersonalizDirecto = False And gParametrosGenerales.gbCodPersonalizPedido = False And (iAccion = PEDSegModifCodERP) Then
        Exit Function
    ElseIf Not (gParametrosIntegracion.gaExportar(EntidadIntegracion.Adj)) And (iAccion = OFECOMPAdjAdjaERP Or iAccion = RESREUAdjaErp) Then
        'Adjudicaciones a ERP
        Exit Function
    ElseIf (Not gParametrosGenerales.gbOblProveEqp) And (iAccion = BUZOFERestProvEquComp) Then
        'Si par�metro "No utilizar la selecci�n de proveedores por equipos de compra", no tiene sentido "Restringir el filtro de proveedores a los del equipo del comprador"
        Exit Function
    ElseIf (gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.SinAcceso) And (iAccion = AccionesDeSeguridad.PRESConcepto5CrearArboles) Then
        Exit Function
    ElseIf (gParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso) And (iAccion = OFECOMPPermitirVistasCompQA Or iAccion = RESREUPermitirVistasCompQA) Then
        Exit Function
    ElseIf (gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.SinAcceso) And (iAccion = ORGGestionarCC Or iAccion = ORGConsultarCC) Then
        Exit Function
    ElseIf gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso And iAccion = AccionesDeSeguridad.PEDSEGModifAutoFact Then
        Exit Function
    ElseIf gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso And iAccion = AccionesDeSeguridad.MATConsultaImpuesto Then
        Exit Function
    ElseIf gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso And iAccion = AccionesDeSeguridad.MATModificarImpuesto Then
        Exit Function
    ElseIf gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso And iAccion = AccionesDeSeguridad.PEDDIRModifAutoFact Then
        Exit Function
    ElseIf basParametros.gParametrosGenerales.gbProveGrupos = False And (iAccion = SELPROVEAsignarGrupos Or iAccion = SELPROVEBloquearGrupos) Then
        Exit Function
    ElseIf Not gParametrosGenerales.gbActPedAbierto And (iAccion = PEDSEGSoloAbiertos Or iAccion = APEPedidoAbierto Or iAccion = OFECOMPEmitirPedidoAbierto Or iAccion = RESREUEmitirPedidoAbierto Or _
        iAccion = PEDDIRPermitirPedidoAbierto) Then
        Exit Function
    ElseIf Not basParametros.gParametrosGenerales.gbACTIVAR_BORRADO_LOGICO_PEDIDOS And (iAccion = PEDSEGPermitirBorrarPedidos Or iAccion = PEDSEGPermitirBorrarLineasPedido) Then
        Exit Function
    Else
        bNoCargar = False
    End If
        
    NoCargarAccion = bNoCargar
End Function



