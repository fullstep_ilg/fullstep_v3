VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPROCEItemDist 
   Caption         =   "Distribuci�n de la compra en unidades organizativas"
   ClientHeight    =   4710
   ClientLeft      =   2010
   ClientTop       =   4035
   ClientWidth     =   8670
   Icon            =   "frmPROCEItemDist.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4710
   ScaleWidth      =   8670
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   3375
      Left            =   0
      TabIndex        =   6
      Top             =   360
      Width           =   8610
      _ExtentX        =   15187
      _ExtentY        =   5953
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8640
      Top             =   4200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":014A
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":01E8
            Key             =   "UON3A"
            Object.Tag             =   "UON3A"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":02A8
            Key             =   "UON2A"
            Object.Tag             =   "UON2A"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":036A
            Key             =   "UON1A"
            Object.Tag             =   "UON1A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":042A
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":04BC
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":056C
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":061C
            Key             =   "UON3B"
            Object.Tag             =   "UON3B"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":096E
            Key             =   "UON2B"
            Object.Tag             =   "UON2B"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemDist.frx":0CC0
            Key             =   "UON1B"
            Object.Tag             =   "UON1B"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picPorcen 
      Height          =   495
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   8550
      TabIndex        =   16
      Top             =   3780
      Width           =   8610
      Begin MSComctlLib.Slider Slider1 
         Height          =   315
         Left            =   6585
         TabIndex        =   9
         Top             =   60
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         Max             =   100
      End
      Begin VB.TextBox txtPorcentaje 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1170
         TabIndex        =   7
         Top             =   90
         Width           =   1605
      End
      Begin VB.TextBox txtCantidad 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3825
         TabIndex        =   8
         Top             =   90
         Width           =   2235
      End
      Begin VB.Label Label2 
         Caption         =   "Cantidad:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2940
         TabIndex        =   18
         Top             =   120
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Porcentaje:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   17
         Top             =   120
         Width           =   1035
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   390
      Left            =   0
      ScaleHeight     =   390
      ScaleWidth      =   8670
      TabIndex        =   15
      Top             =   4320
      Width           =   8670
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   1
         Left            =   30
         TabIndex        =   10
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdModif 
         Caption         =   "&Modificar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1100
         TabIndex        =   11
         Top             =   30
         Width           =   1005
      End
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   390
      Left            =   0
      ScaleHeight     =   390
      ScaleWidth      =   8670
      TabIndex        =   19
      Top             =   3930
      Visible         =   0   'False
      Width           =   8670
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3360
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2250
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   0
         Left            =   60
         TabIndex        =   12
         Top             =   30
         Width           =   1005
      End
   End
   Begin VB.Label lblSinDistCantidad 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5940
      TabIndex        =   5
      Top             =   30
      Width           =   2670
   End
   Begin VB.Label lblCantidad 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2070
      TabIndex        =   2
      Top             =   30
      Width           =   1440
   End
   Begin VB.Label Label5 
      Caption         =   "Distribu�do:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   0
      TabIndex        =   0
      Top             =   60
      Width           =   1065
   End
   Begin VB.Label lblPorcentaje 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1050
      TabIndex        =   1
      Top             =   30
      Width           =   960
   End
   Begin VB.Label lblSinDistPorcentaje 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4980
      TabIndex        =   4
      Top             =   30
      Width           =   960
   End
   Begin VB.Label Label3 
      Caption         =   "Sin distribuir:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3675
      TabIndex        =   3
      Top             =   60
      Width           =   1185
   End
End
Attribute VB_Name = "frmPROCEItemDist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oDistsNivel1 As CDistItemsNivel1
Private oDistsNivel2 As CDistItemsNivel2
Private oDistsNivel3 As CDistItemsNivel3
Private dDistPorcen As Double
Private dCantidad As Double
Private dSinCantidad As Double
Private dDistCantidad As Double
Private bSinCantidad As Boolean
Private m_bRDep As Boolean
Private m_bRuo As Boolean
Public g_oItem As CItem
Private Accion As AccionesSummit

Private m_stexto As String
Private m_bRespetarPorcen As Boolean
Public g_bModoEdicion As Boolean
Public g_iEstado As Integer
Public g_bCerrado As Boolean
Public g_bVisualizarTodos As Boolean
Public g_vIndic As Variant
Public g_bModoEdicionPROCE As Boolean
Public g_bModifItem As Boolean

Public g_bModif As Boolean

Private m_bCargandoForm As Boolean
Public g_oGMN4Seleccionado As CGrupoMatNivel4

'seguridad
Private m_bRestrDistrUsu As Boolean
Private m_bRestrDistrPerf As Boolean

Private m_sVariasUnidades As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String

Public g_bHaGrabadoArtPorBtDist As Boolean

''' <summary>
''' Salir grabando de la pantalla. Primero se valida, si no esta bien ni se graba ni se sale.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim dPorcen As Double
    Dim nod1 As MSComctlLib.node
    Dim nod2 As MSComctlLib.node
    Dim nod3 As MSComctlLib.node
    Dim nod4 As MSComctlLib.node
    Dim nodx As MSComctlLib.node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim sCad1 As Variant
    Dim sCad2 As Variant
    Dim sCad3 As Variant
    Dim oDistN1 As CDistItemNivel1
    Dim oDistN2 As CDistItemNivel2
    Dim oDistN3 As CDistItemNivel3
    Dim NoPasarNulos As Boolean
    Dim numUons As Integer
    'Si la distribuci�n es obligatoria controlar la selecci�n
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If dSinCantidad = dCantidad And Not bSinCantidad Then
        Screen.MousePointer = vbNormal
        oMensajes.ProceDistObligatoria
        Exit Sub
    End If
       
    Screen.MousePointer = vbHourglass
        
    Set nodx = tvwestrorg.Nodes(1)
    
    If nodx.Children = 0 Then
         Screen.MousePointer = vbNormal
         Exit Sub
    End If
    
    dPorcen = 0
    
    Set nod1 = nodx.Child
    While Not nod1 Is Nothing
            
    ' No distribu�do
        If nod1.Image = "UON1" Then
                
            Set nod2 = nod1.Child
               
                While Not nod2 Is Nothing
                    ' No distribu�do
                    If nod2.Image = "UON2" Then
                    
                        Set nod3 = nod2.Child
                        While Not nod3 Is Nothing
                            If nod3.Image = "UON3A" Or nod3.Image = "UON3B" Then
                                If DevolverPorcentaje(nod3) >= dPorcen Then
                                    sCad1 = Right(nod3.Parent.Parent.Tag, Len(nod3.Parent.Parent.Tag) - 4)
                                    sCad2 = Right(nod3.Parent.Tag, Len(nod3.Parent.Tag) - 4)
                                    sCad3 = Right(nod3.Tag, Len(nod3.Tag) - 4)
                                    dPorcen = DevolverPorcentaje(nod3)
                                    numUons = numUons + 1
                                End If
                            End If
                            Set nod3 = nod3.Next
                        Wend
                    Else
                        ' Si est� distribu�do UON2
                        If nod2.Image = "UON2A" Or nod2.Image = "UON3B" Then
                            If DevolverPorcentaje(nod2) >= dPorcen Then
                                sCad1 = Right(nod2.Parent.Tag, Len(nod2.Parent.Tag) - 4)
                                sCad2 = Right(nod2.Tag, Len(nod2.Tag) - 4)
                                sCad3 = Null
                                dPorcen = DevolverPorcentaje(nod2)
                                numUons = numUons + 1
                            End If
                        End If
                    End If
                    Set nod2 = nod2.Next
                Wend
        Else
        ' Si est� distribu�do UON1
            If nod1.Image = "UON1A" Or nod1.Image = "UON1B" Then
                If DevolverPorcentaje(nod1) >= dPorcen Then
                    sCad1 = Right(nod1.Tag, Len(nod1.Tag) - 4)
                    sCad2 = Null
                    sCad3 = Null
                    dPorcen = DevolverPorcentaje(nod1)
                    numUons = numUons + 1
                End If
            End If
        End If
        Set nod1 = nod1.Next
    Wend
        
'    frmPROCE.g_bHaPulsadoBoton = True
    If dDistPorcen > 1 Then
        oMensajes.NoValido Label1.caption & " " & CDec(dDistPorcen * 100)
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    
    'Controlar la asignacion de unidades organizativas de los articulos
        
    Dim adoRecordset As ADODB.Recordset
    Dim sUON As String
    Dim sUonMostrar As String
    Dim UON1tmp As String
    Dim UON2tmp As String
    Dim UON3tmp As String
    
    Dim iValidoN1 As Integer
    iValidoN1 = 0
    Dim iValidoN2 As Integer
    iValidoN2 = 0
    Dim iNoValidoN2 As Integer
    iNoValidoN2 = 0
    Dim iValidoN3 As Integer
    iValidoN3 = 0
        
    Dim iNumDist3 As Integer
    
    sUonMostrar = ""
    
    If frmPROCE.sdbgItems.Columns("COD").Value <> "" Then
        Set adoRecordset = g_oItem.DevolverUONsItem(frmPROCE.sdbgItems.Columns("COD").Value, NoPasarNulos)
        
        If g_bHaGrabadoArtPorBtDist And adoRecordset.RecordCount = 0 Then
            Dim oItems As CItems
            Set oItems = oFSGSRaiz.Generar_CItems
            
            g_oItem.ArticuloCod = frmPROCE.sdbgItems.Columns("COD").Value
            
            teserror = oItems.FsgsInsertarArt4Uon(g_oItem, oDistsNivel1, oDistsNivel2, oDistsNivel3)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
            Set oItems = Nothing
            
            Set adoRecordset = g_oItem.DevolverUONsItem(frmPROCE.sdbgItems.Columns("COD").Value, NoPasarNulos)
        End If
        
        If Not oDistsNivel1 Is Nothing Then
            If adoRecordset.RecordCount > 0 Then
                For Each oDistN1 In oDistsNivel1
                    adoRecordset.MoveFirst
                    Do While Not adoRecordset.EOF
                        UON1tmp = ""
                        UON1tmp = NullToStr(adoRecordset("UON1").Value)
                        If (UON1tmp = oDistN1.CodUON1) Then
                            iValidoN1 = iValidoN1 + 1
                            Exit Do
                        End If
                        adoRecordset.MoveNext
                    Loop
                Next
            End If
        End If
        
        If Not oDistsNivel2 Is Nothing Then
            If adoRecordset.RecordCount > 0 Then
                For Each oDistN2 In oDistsNivel2
                    adoRecordset.MoveFirst
                    Do While Not adoRecordset.EOF
                        UON1tmp = ""
                        UON2tmp = ""
                        UON1tmp = NullToStr(adoRecordset("UON1").Value)
                        UON2tmp = NullToStr(adoRecordset("UON2").Value)
                        If (UON1tmp = oDistN2.CodUON1) And (UON2tmp = oDistN2.CodUON2 Or UON2tmp = "") Then
                            iValidoN2 = iValidoN2 + 1
                            Exit Do
                        End If
                        adoRecordset.MoveNext
                    Loop
                Next
            End If
        End If
                    
        If Not oDistsNivel3 Is Nothing Then
            If adoRecordset.RecordCount > 0 Then
                For Each oDistN3 In oDistsNivel3
                    adoRecordset.MoveFirst
                    Do While Not adoRecordset.EOF
                        UON1tmp = ""
                        UON2tmp = ""
                        UON3tmp = ""
                        UON1tmp = NullToStr(adoRecordset("UON1").Value)
                        UON2tmp = NullToStr(adoRecordset("UON2").Value)
                        UON3tmp = NullToStr(adoRecordset("UON3").Value)
                        If (UON1tmp = oDistN3.CodUON1) And (UON2tmp = oDistN3.CodUON2 Or UON2tmp = "") And (UON3tmp = oDistN3.CodUON3 Or UON3tmp = "") Then
                            iValidoN3 = iValidoN3 + 1
                            Exit Do
                        End If
                        adoRecordset.MoveNext
                    Loop
                Next
            End If
        End If
                    
        'Damos los intem como validos cuando:
        'Tiene alguna distribucion a nivel 3 que coincide con la del proceso
        If ((iValidoN3 <> oDistsNivel3.Count Or iValidoN2 <> oDistsNivel2.Count Or iValidoN1 <> oDistsNivel1.Count) And NoPasarNulos) Then
            If adoRecordset.RecordCount > 0 Then
                adoRecordset.MoveFirst
                While Not adoRecordset.EOF
                    sUON = NullToStr(adoRecordset("UON1").Value)
                    If Not IsNull(adoRecordset("UON2").Value) Then
                        sUON = sUON & "-" & NullToStr(adoRecordset("UON2").Value)
                        If Not IsNull(adoRecordset("UON3").Value) Then
                            sUON = sUON & "-" & NullToStr(adoRecordset("UON3").Value)
                        End If
                    End If
                    
                    If Not IsNull(adoRecordset("DEN").Value) Then
                        sUON = sUON & "-" & adoRecordset("DEN").Value
                    End If

            
                    sUonMostrar = sUonMostrar & sUON & vbCrLf & " "
                    adoRecordset.MoveNext
                Wend
                adoRecordset.Close
                Set adoRecordset = Nothing
            End If
                                        
            oMensajes.ImposibleAnyadirItemIndividualADistribucion sUonMostrar, iValidoN3 + iValidoN2 + iValidoN1
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
            
        If Not adoRecordset Is Nothing Then
            adoRecordset.Close
            Set adoRecordset = Nothing
        End If
    
    End If
    
    Screen.MousePointer = vbNormal
    teserror = g_oItem.ActualizarDistribucion(oDistsNivel1, oDistsNivel2, oDistsNivel3)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
    Else
        If numUons = 1 Then
            If NullToStr(sCad3) <> "" Then
                frmPROCE.sdbgItems.Columns("DIST").Value = sCad1 & "-" & sCad2 & "-" & sCad3
            ElseIf NullToStr(sCad2) <> "" Then
                frmPROCE.sdbgItems.Columns("DIST").Value = sCad1 & "-" & sCad2
            Else
                frmPROCE.sdbgItems.Columns("DIST").Value = NullToStr(sCad1)
            End If
        ElseIf numUons > 1 Then
            frmPROCE.sdbgItems.Columns("DIST").Value = m_sVariasUnidades
        End If
        basSeguridad.RegistrarAccion AccionesSummit.ACCProceModifDistUO, "Anyo:" & Trim(g_oItem.proceso.Anyo) & " Gmn1:" & Trim(g_oItem.proceso.GMN1Cod) & " Proce:" & g_oItem.proceso.Cod & " Id:" & g_oItem.Id
        Unload Me
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
                
    lblSinDistPorcentaje.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
    lblSinDistCantidad.caption = dSinCantidad
            
    frmPROCE.g_bHaPulsadoDist = True
    'Se desbloquea si no estamos en modo edici�n
    frmPROCE.DesbloquearProceso ("IT")
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Salir sin grabar de la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdModif_Click()
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer

    'Bloqueo del proceso
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems
    
    picPorcen.Enabled = True
    picNavigate.Visible = False
    picEdit.Visible = True
    
    ConfigurarInterfaz tvwestrorg.selectedItem
    g_bModoEdicion = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "cmdModif_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCE_ITEMDIST, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdModif.caption = Ador(0).Value
        Ador.MoveNext
        frmPROCEItemDist.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        Label2.caption = Ador(0).Value
        Ador.MoveNext
        Label3.caption = Ador(0).Value
        Ador.MoveNext
        Label5.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar(0).caption = Ador(0).Value
        cmdRestaurar(1).caption = Ador(0).Value
        Ador.MoveNext
        m_sVariasUnidades = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub


Private Sub cmdRestaurar_Click(Index As Integer)

    ' Generar la estructura de la organizacion en el treeview
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    GenerarEstructuraOrg (False)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "cmdRestaurar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
    If g_bModif Then
        If Me.Enabled And Me.Visible Then tvwestrorg.SetFocus
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Load()
Dim nodx As MSComctlLib.node  ' Declare an object variable for the Node.
Dim i As Integer  ' Declare a variable for use as a counter.

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Width = 8820
    Me.Height = 5115
    
    CargarRecursos
    
    frmPROCE.g_bHaPulsadoDist = True
    
    picPorcen.Enabled = False
    
    If NoHayParametro(g_oItem.Cantidad) Then bSinCantidad = True
    dDistPorcen = 0
    dCantidad = StrToDbl0(g_oItem.Cantidad)
    dSinCantidad = 0
    dDistCantidad = 0
    If bSinCantidad Then txtCantidad.Locked = True
    ' Configurar la seguridad
    ConfigurarSeguridad
        
    m_bCargandoForm = True
    
    ' Generar la estructura de la organizacion en el treeview
    GenerarEstructuraOrg (False)
    
    tvwestrorg_NodeClick tvwestrorg.Nodes(1)
    
    m_bCargandoForm = False
    
    If g_bModoEdicion Then
        picPorcen.Enabled = True
        picNavigate.Visible = False
        picEdit.Visible = True
    Else
        picPorcen.Enabled = False
    End If
    
    If Not g_bModif Then
        'tvwestrorg.Enabled = False
        picPorcen.Enabled = False
        picNavigate.Visible = False
        If g_bModoEdicion Then
            picEdit.Visible = False
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub ConfigurarSeguridad()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) Then
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUOPers)) Is Nothing) Then
            m_bRuo = True
        End If
        
    End If
    
    If Not frmPROCE.g_bModifItems Then
        cmdModif.Visible = False
    End If
    
    'Restriccion a la distribuci�n de la compra
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUOPers)) Is Nothing) Then
        m_bRestrDistrUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDisCompraUONPerf)) Is Nothing) Then
        m_bRestrDistrPerf = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

Dim oDistNivel1 As CDistItemNivel1
Dim oDistNivel2 As CDistItemNivel2
Dim oDistNivel3 As CDistItemNivel3

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3

' Otras
Dim nodx As MSComctlLib.node
Dim bOrdCod As Boolean
Dim bOrdDen As Boolean

Dim adoRecordset As ADODB.Recordset
Dim UON1tmp As Variant
Dim UON2tmp As Variant
Dim UON3tmp As Variant
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next

    ' Si alguien ha distribu�do la compra sin tener la restricci�n de UO,
    ' y despu�s entra alguien con la restricci�n de UO, no hay que mostrar esas distribuciones
    
    Screen.MousePointer = vbHourglass
    
    'Limpia el �rbol
    tvwestrorg.Nodes.clear
    
    dDistPorcen = 0
    
    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    
    Set oDistsNivel1 = oFSGSRaiz.generar_CDistItemsNivel1
    Set oDistsNivel2 = oFSGSRaiz.generar_CDistItemsNivel2
    Set oDistsNivel3 = oFSGSRaiz.generar_CDistItemsNivel3
    

    Dim lIdPerfil As Integer
    If Not oUsuarioSummit.Perfil Is Nothing Then
        lIdPerfil = oUsuarioSummit.Perfil.Id
    End If
    'Cargamos toda la estrucutura organizativa
    Select Case gParametrosGenerales.giNEO
        
            Case 1
                    oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRestrDistrUsu, m_bRDep, , False, , bOrdenadoPorDen, False, , , , False, frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod, , EnItem, g_oItem.Id, , , , m_bRestrDistrPerf, lIdPerfil, g_bHaGrabadoArtPorBtDist
                    If Not g_bModifItem Then
                        dDistPorcen = oDistsNivel1.CargarTodasLasDistribucionesItem(g_oItem)
                    End If
                    
                    'If gParametrosGenerales.gbOBLAsigUonArt Then
                        If dDistPorcen = 0 Then
                           
                           'El ultimo que se ha a�adido
                            g_oItem.GMN1Cod = g_oGMN4Seleccionado.GMN1Cod ' frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).GMN1Cod
                            g_oItem.GMN2Cod = g_oGMN4Seleccionado.GMN2Cod ' frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).GMN2Cod
                            g_oItem.GMN3Cod = g_oGMN4Seleccionado.GMN3Cod 'frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).GMN3Cod
                            g_oItem.GMN4Cod = g_oGMN4Seleccionado.Cod 'frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).Cod
                            
                            Set adoRecordset = g_oItem.DevolverUONsNivelItems(frmPROCE.sdbgItems.Columns("C�DIGO").Value, 1)
                            If Not adoRecordset Is Nothing Then
                                UON1tmp = Null
                                UON1tmp = NullToStr(adoRecordset("UON1").Value)
                                
                                oDistsNivel1.Add g_oItem, UON1tmp, 1
                                dDistPorcen = 1
                                txtPorcentaje.Text = CDec(100)
                                Slider1.Value = Int(txtPorcentaje.Text)
                                                                    
                                
                                adoRecordset.Close
                                Set adoRecordset = Nothing
                            End If
                            
                            
                        End If
                    'End If
                    
                    dDistCantidad = CDec(dDistPorcen * dCantidad)
                    dSinCantidad = dCantidad - dDistCantidad
                                        
            Case 2
                    oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRestrDistrUsu, m_bRDep, , , , bOrdenadoPorDen, , , , , False, frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod, , EnItem, g_oItem.Id, , , , m_bRestrDistrPerf, lIdPerfil, g_bHaGrabadoArtPorBtDist
                    oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRestrDistrUsu, m_bRDep, , , , bOrdenadoPorDen, , , , , False, frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod, , EnItem, g_oItem.Id, , , , , m_bRestrDistrPerf, lIdPerfil, , g_bHaGrabadoArtPorBtDist
                    If Not g_bModifItem Then
                        dDistPorcen = oDistsNivel1.CargarTodasLasDistribucionesItem(g_oItem)
                        dDistPorcen = dDistPorcen + oDistsNivel2.CargarTodasLasDistribucionesItem(g_oItem)
                    End If
                    
                    'If gParametrosGenerales.gbOBLAsigUonArt Then
                        If dDistPorcen = 0 Then
                            g_oItem.GMN1Cod = g_oGMN4Seleccionado.GMN1Cod ' frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).GMN1Cod
                            g_oItem.GMN2Cod = g_oGMN4Seleccionado.GMN2Cod ' frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).GMN2Cod
                            g_oItem.GMN3Cod = g_oGMN4Seleccionado.GMN3Cod 'frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).GMN3Cod
                            g_oItem.GMN4Cod = g_oGMN4Seleccionado.Cod ' frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).Cod
                            Set adoRecordset = g_oItem.DevolverUONsNivelItems(frmPROCE.sdbgItems.Columns("C�DIGO").Value, 2)
                            If Not adoRecordset Is Nothing Then
                                UON1tmp = Null
                                UON2tmp = Null
                                UON1tmp = NullToStr(adoRecordset("UON1").Value)
                                If Not IsNull(adoRecordset("UON2").Value) Then
                                    UON2tmp = NullToStr(adoRecordset("UON2").Value)
                                        oDistsNivel2.Add g_oItem, UON1tmp, UON2tmp, 1
                                        dDistPorcen = 1
                                        txtPorcentaje.Text = CDec(100)
                                        Slider1.Value = Int(txtPorcentaje.Text)
                                Else
                                    oDistsNivel1.Add g_oItem, UON1tmp, 1
                                    dDistPorcen = 1
                                    txtPorcentaje.Text = CDec(100)
                                    Slider1.Value = Int(txtPorcentaje.Text)
                                    
                                End If
                                
                                adoRecordset.Close
                                Set adoRecordset = Nothing
                            End If
                            
                            
                        End If
                    
                  'End If
                
                dDistCantidad = CDec(dDistPorcen * dCantidad)
                dSinCantidad = dCantidad - dDistCantidad
                                
            Case 3
                    oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRestrDistrUsu, m_bRDep, , , , bOrdenadoPorDen, False, , , , False, frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod, , EnItem, g_oItem.Id, , , , m_bRestrDistrPerf, lIdPerfil, g_bHaGrabadoArtPorBtDist
                    oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRestrDistrUsu, m_bRDep, , , , bOrdenadoPorDen, False, , , , False, frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod, , EnItem, g_oItem.Id, , , , , m_bRestrDistrPerf, lIdPerfil, , g_bHaGrabadoArtPorBtDist
                    oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRestrDistrUsu, m_bRDep, , , , bOrdenadoPorDen, False, , , , False, frmPROCE.g_oProcesoSeleccionado.Anyo, frmPROCE.g_oProcesoSeleccionado.GMN1Cod, frmPROCE.g_oProcesoSeleccionado.Cod, , EnItem, g_oItem.Id, , , , , , m_bRestrDistrPerf, lIdPerfil, , g_bHaGrabadoArtPorBtDist
                                    
                    If Not g_bModifItem Then
                        dDistPorcen = oDistsNivel1.CargarTodasLasDistribucionesItem(g_oItem)
                        dDistPorcen = dDistPorcen + oDistsNivel2.CargarTodasLasDistribucionesItem(g_oItem)
                        dDistPorcen = dDistPorcen + oDistsNivel3.CargarTodasLasDistribucionesItem(g_oItem)
                    End If
                    
                    'Tema de los articulos
                    'If gParametrosGenerales.gbOBLAsigUonArt Then
                        If dDistPorcen = 0 Then
                            g_oItem.GMN1Cod = g_oGMN4Seleccionado.GMN1Cod ' frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).GMN1Cod
                            g_oItem.GMN2Cod = g_oGMN4Seleccionado.GMN2Cod ' frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).GMN2Cod
                            g_oItem.GMN3Cod = g_oGMN4Seleccionado.GMN3Cod 'frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).GMN3Cod
                            g_oItem.GMN4Cod = g_oGMN4Seleccionado.Cod ' frmPROCE.g_oProcesoSeleccionado.MaterialProce.Item(i).Cod
                            Set adoRecordset = g_oItem.DevolverUONsNivelItems(frmPROCE.sdbgItems.Columns("C�DIGO").Value, 3)
                            If Not adoRecordset Is Nothing Then
                                UON1tmp = Null
                                UON2tmp = Null
                                UON3tmp = Null
                                UON1tmp = NullToStr(adoRecordset("UON1").Value)
                                If Not IsNull(adoRecordset("UON2").Value) Then
                                    UON2tmp = NullToStr(adoRecordset("UON2").Value)
                                    If Not IsNull(adoRecordset("UON3").Value) Then
                                            UON3tmp = NullToStr(adoRecordset("UON3").Value)
                                            oDistsNivel3.Add g_oItem, UON1tmp, UON2tmp, UON3tmp, 1
                                            dDistPorcen = 1
                                            txtPorcentaje.Text = CDec(100)
                                            Slider1.Value = Int(txtPorcentaje.Text)
                                            
                                    Else
                                        oDistsNivel2.Add g_oItem, UON1tmp, UON2tmp, 1
                                        dDistPorcen = 1
                                        txtPorcentaje.Text = CDec(100)
                                        Slider1.Value = Int(txtPorcentaje.Text)
                                        
                                    End If
                                Else
                                    oDistsNivel1.Add g_oItem, UON1tmp, 1
                                    dDistPorcen = 1
                                    txtPorcentaje.Text = CDec(100)
                                    Slider1.Value = Int(txtPorcentaje.Text)
                                    
                                End If
                                
                                adoRecordset.Close
                                Set adoRecordset = Nothing
                            End If
                            
                            
                        End If
                    'End If
                    
                    dDistCantidad = CDec(dDistPorcen * dCantidad)
                    dSinCantidad = dCantidad - dDistCantidad
                                    
        
    End Select
            
    lblPorcentaje.caption = Format(CDec(dDistPorcen * 100), "Standard") & " %"
    If Not bSinCantidad Then lblCantidad.caption = Format(dDistCantidad, "Standard")
    'lblCantidad.Caption = dDistCantidad
    lblSinDistPorcentaje.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & " %"
    If Not bSinCantidad Then lblSinDistCantidad.caption = dSinCantidad
        
   '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        If oUON1.BajaLog Then
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1B")
        Else
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        End If
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
        If Not oUON1.activa Then
            bloquearNodo nodx
        End If
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        If oUON2.BajaLog Then
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON1" & scod1 & "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2B")
        Else
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON1" & scod1 & "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        End If
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
        If Not oUON2.activa Then
            bloquearNodo nodx
        End If
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        If oUON3.BajaLog Then
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1 & "UON2" & scod2, tvwChild, "UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3B")
        Else
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1 & "UON2" & scod2, tvwChild, "UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        End If
        
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
        If Not oUON3.activa Then
            bloquearNodo nodx
        End If
    Next
    
    'Distribuciones
    For Each oDistNivel1 In oDistsNivel1
        Set nodx = Nothing
        scod1 = oDistNivel1.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel1.CodUON1))
        Set nodx = tvwestrorg.Nodes("UON1" & scod1)
        nodx.Text = nodx.Text & " (" & CDec(oDistNivel1.Porcentaje * 100) & "%)"
        If gParametrosGenerales.giNIVDIST < 2 Then
            If nodx.Image = "UON1" Then
                nodx.Image = "UON1A"
            Else
                nodx.Image = "UON1B"
            End If
        End If
    Next
    
    
    For Each oDistNivel2 In oDistsNivel2
        Set nodx = Nothing
        scod1 = oDistNivel2.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel2.CodUON1))
        scod2 = oDistNivel2.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel2.CodUON2))
        Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2)
        nodx.Text = nodx.Text & " (" & CDec(oDistNivel2.Porcentaje * 100) & "%)"
        If gParametrosGenerales.giNIVDIST < 3 Then
            If nodx.Image = "UON2" Then
                nodx.Image = "UON2A"
            Else
                nodx.Image = "UON2B"
            End If
        End If
        nodx.Parent.Expanded = True
    Next
    
    For Each oDistNivel3 In oDistsNivel3
        Set nodx = Nothing
        scod1 = oDistNivel3.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel3.CodUON1))
        scod2 = oDistNivel3.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel3.CodUON2))
        scod3 = oDistNivel3.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDistNivel3.CodUON3))
        Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3)
        nodx.Text = nodx.Text & " (" & CDec(oDistNivel3.Porcentaje * 100) & "%)"
        If gParametrosGenerales.giNIVDIST < 4 Then
            If nodx.Image = "UON3" Then
                nodx.Image = "UON3A"
            Else
                nodx.Image = "UON3B"
            End If
        End If
        nodx.Parent.Parent.Expanded = True
        nodx.Parent.Expanded = True
    Next
    
    tvwestrorg.Nodes.Item("UON0").Selected = True
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
        
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "GenerarEstructuraOrg", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub



Private Sub Form_Resize()
    
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 2000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    tvwestrorg.Height = Me.Height - 1740
    picPorcen.Top = Me.Height - 1350
    tvwestrorg.Width = Me.Width - 200
    picPorcen.Width = tvwestrorg.Width
    cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 50
    cmdCancelar.Left = Me.Width / 2 + 50
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

    
End Sub

''' <summary>
''' Valida q todo este correcto y salir.
''' </summary>
''' <param name="Cancel">Cancelar el cierre pq no valida</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Dim iRow As Integer
    Dim vBookmark As Variant
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If Not m_bDescargarFrm Then
    If g_bModif = True And Not bSinCantidad Then
        If CDbl(lblSinDistCantidad.caption) = dCantidad Then
                oMensajes.ProceDistObligatoria
                Cancel = True
                Exit Sub
        Else
            If frmPROCE.sdbgItems.Columns("DIST").Value = "" Then
                    oMensajes.ProceDistObligatoria
                    Cancel = True
                    Exit Sub
            End If
        End If
    End If
End If
      m_bDescargarFrm = False
    Set oDistsNivel1 = Nothing
    Set oDistsNivel2 = Nothing
    Set oDistsNivel3 = Nothing
    
    dDistPorcen = 0
    dCantidad = 0
    dSinCantidad = 0
    dDistCantidad = 0
    bSinCantidad = False
    'Presupuestos
    frmPROCE.DesbloquearProceso ("IT")
  
    frmPROCE.Accion = ACCProceItemCon
    frmPROCE.g_bHaPulsadoDist = False
    g_bModoEdicion = False
    g_bModifItem = False
    g_bHaGrabadoArtPorBtDist = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
   
End Sub




Private Sub Slider1_Change()
Dim dPorcen As Double
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If tvwestrorg.selectedItem Is Nothing Then 'ninguno
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If tvwestrorg.selectedItem.Parent Is Nothing Then 'Raiz
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        Exit Sub
    End If
     
    dPorcen = CDec(DevolverPorcentaje(tvwestrorg.selectedItem) * 100)
     
    If Slider1.Value <> dPorcen Then
        If Slider1.Value >= Slider1.Max Then
            dPorcen = dPorcen + 100 - CDec(dDistPorcen * 100)
        Else
            dPorcen = Slider1.Value
        End If
        txtPorcentaje.Text = dPorcen
    
        If dPorcen = 0 Then
            m_bRespetarPorcen = True
            txtCantidad.Text = ""
            m_bRespetarPorcen = False
        Else
            m_bRespetarPorcen = True
            If Not bSinCantidad Then txtCantidad.Text = CDec(dPorcen / 100) * dCantidad
            m_bRespetarPorcen = False
        End If
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "Slider1_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub




Private Sub tvwestrorg_NodeClick(ByVal node As MSComctlLib.node)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ConfigurarInterfaz node
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "tvwestrorg_NodeClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub txtCantidad_Change()
Dim nodx As MSComctlLib.node
Dim dCant As Double
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    Set nodx = tvwestrorg.selectedItem
    
    If nodx Is Nothing Then 'ninguno
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If nodx.Parent Is Nothing Then 'Raiz
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    dCant = DevolverPorcentaje(nodx)
    dCant = dCantidad * dCant
         
    If Trim(txtCantidad.Text) = "" Then
        If dCant <> 0 Then
            m_bRespetarPorcen = True
            txtPorcentaje.Text = ""
            m_bRespetarPorcen = True
            Slider1.Value = 0
            m_stexto = QuitarPorcentaje(m_stexto)
            tvwestrorg.selectedItem.Text = m_stexto
            Select Case Left(tvwestrorg.selectedItem.Tag, 4)
                Case "UON1"
                    tvwestrorg.selectedItem.Image = "UON1"
                Case "UON2"
                    tvwestrorg.selectedItem.Image = "UON2"
                Case "UON3"
                    tvwestrorg.selectedItem.Image = "UON3"
            End Select
            m_bRespetarPorcen = False
            
            DistribuirCant nodx
            
            lblSinDistCantidad.caption = dSinCantidad
            lblSinDistPorcentaje.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
            lblCantidad.caption = Format(dDistCantidad, "Standard")
            lblPorcentaje.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"
            
        End If
        Exit Sub
    End If
        
    If Not IsNumeric(txtCantidad.Text) Then
        m_bRespetarPorcen = True
        txtCantidad.Text = Left(txtCantidad.Text, Len(txtCantidad.Text) - 1)
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    m_stexto = nodx.Text
    If Mid(nodx.Image, 5, 1) = "A" Or Mid(nodx.Image, 5, 1) = "B" Then
        m_stexto = QuitarPorcentaje(m_stexto)
    End If
    
    If CDbl(txtCantidad.Text) <= 0 Then
        If CDbl(txtCantidad.Text) <> 0 Then 'Pueden introducirse cantidades 0,X
            m_bRespetarPorcen = True
            txtCantidad.Text = ""
        End If
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = False
        tvwestrorg.selectedItem.Text = m_stexto
        Select Case Left(tvwestrorg.selectedItem.Tag, 4)
            Case "UON1"
                If tvwestrorg.selectedItem.Image <> "UON1B" Then
                    tvwestrorg.selectedItem.Image = "UON1"
                End If
            Case "UON2"
                If tvwestrorg.selectedItem.Image <> "UON2B" Then
                    tvwestrorg.selectedItem.Image = "UON2"
                End If
            Case "UON3"
                If tvwestrorg.selectedItem.Image <> "UON3B" Then
                    tvwestrorg.selectedItem.Image = "UON3"
                End If
        End Select
        
        DistribuirCant nodx
        
        lblSinDistCantidad.caption = dSinCantidad
        lblSinDistPorcentaje.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
        lblCantidad.caption = Format(dDistCantidad, "Standard")
        lblPorcentaje.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"

    Else
        If txtCantidad.Text <> dCant Then
            If CDec((txtCantidad.Text / dCantidad) * 100) < 32767 Then
                m_bRespetarPorcen = True
                Slider1.Value = Int(CDec((txtCantidad.Text / dCantidad) * 100))
            End If
            m_bRespetarPorcen = True
            txtPorcentaje.Text = CDec((txtCantidad.Text / dCantidad) * 100)
            m_bRespetarPorcen = False
            tvwestrorg.selectedItem.Text = m_stexto & " (" & txtPorcentaje.Text & "%" & ")"
            Select Case Left(tvwestrorg.selectedItem.Tag, 4)
                Case "UON1"
                    If tvwestrorg.selectedItem.Image <> "UON1B" Then
                        tvwestrorg.selectedItem.Image = "UON1A"
                    End If
                Case "UON2"
                    If tvwestrorg.selectedItem.Image <> "UON2B" Then
                        tvwestrorg.selectedItem.Image = "UON2A"
                    End If
                Case "UON3"
                    If tvwestrorg.selectedItem.Image <> "UON3B" Then
                        tvwestrorg.selectedItem.Image = "UON3A"
                    End If
            End Select
            
            LimpiarRama tvwestrorg.selectedItem
            
            DistribuirCant nodx
            
            lblSinDistCantidad.caption = dSinCantidad
            lblCantidad.caption = Format(dDistCantidad, "Standard")
            lblSinDistPorcentaje.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
            lblPorcentaje.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"
            
            If dDistCantidad > dCantidad Then
                txtPorcentaje.Text = Left(txtPorcentaje.Text, Len(txtPorcentaje.Text) - 1)
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "txtCantidad_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtPorcentaje_Change()
Dim nodx As MSComctlLib.node
Dim dPorcen As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bRespetarPorcen Then
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    Set nodx = tvwestrorg.selectedItem
    
    If nodx Is Nothing Then 'ninguno
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
        
    If nodx.Parent Is Nothing Then 'Raiz
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    dPorcen = DevolverPorcentaje(nodx)
    
    If Trim(txtPorcentaje.Text) = "" Then
        If dPorcen <> 0 Then
            m_bRespetarPorcen = True
            txtCantidad.Text = ""
            m_bRespetarPorcen = True
            Slider1.Value = 0
            m_stexto = QuitarPorcentaje(m_stexto)
            tvwestrorg.selectedItem.Text = m_stexto
            Select Case Left(tvwestrorg.selectedItem.Tag, 4)
                Case "UON1"
                    tvwestrorg.selectedItem.Image = "UON1"
                Case "UON2"
                    tvwestrorg.selectedItem.Image = "UON2"
                Case "UON3"
                    tvwestrorg.selectedItem.Image = "UON3"
            End Select
            m_bRespetarPorcen = False
            
            Distribuir nodx
            
            If Not bSinCantidad Then lblSinDistCantidad.caption = dSinCantidad
            lblSinDistPorcentaje.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
            If Not bSinCantidad Then lblCantidad.caption = Format(dDistCantidad, "Standard")
            lblPorcentaje.caption = Format(CDec(dDistPorcen * 100), "Standard") & "%"
            
        End If
        Exit Sub
    End If
        
    If Not IsNumeric(txtPorcentaje.Text) Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = Left(txtPorcentaje.Text, Len(txtPorcentaje.Text) - 1)
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    m_stexto = nodx.Text
    
    If Mid(nodx.Image, 5, 1) = "A" Or Mid(nodx.Image, 5, 1) = "B" Then
        m_stexto = QuitarPorcentaje(m_stexto)
    End If
    
    If txtPorcentaje.Text <= 0 Then
        If txtPorcentaje.Text <> 0 Then 'Pueden introducirse porcentajes 0,X
            m_bRespetarPorcen = True
            txtPorcentaje.Text = ""
        End If
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        tvwestrorg.selectedItem.Text = m_stexto
        Select Case Left(tvwestrorg.selectedItem.Tag, 4)
            Case "UON1"
                If tvwestrorg.selectedItem.Image <> "UON1B" Then
                    tvwestrorg.selectedItem.Image = "UON1"
                End If
            Case "UON2"
                If tvwestrorg.selectedItem.Image <> "UON2B" Then
                    tvwestrorg.selectedItem.Image = "UON2"
                End If
            Case "UON3"
                If tvwestrorg.selectedItem.Image <> "UON3B" Then
                    tvwestrorg.selectedItem.Image = "UON3"
                End If
        End Select
        Distribuir nodx
        
        If Not bSinCantidad Then lblSinDistCantidad.caption = dSinCantidad
        lblSinDistPorcentaje.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
        If Not bSinCantidad Then lblCantidad.caption = Format(dDistCantidad, "Standard")
        lblPorcentaje.caption = DblToStr(CDec(dDistPorcen * 100)) & "%"
        
    Else
        If txtPorcentaje.Text <> CDec(dPorcen * 100) Then
            m_bRespetarPorcen = True
            If txtPorcentaje.Text < 32767 Then
                Slider1.Value = Int(txtPorcentaje.Text)
            End If
            m_bRespetarPorcen = True
            If Not bSinCantidad Then txtCantidad.Text = CDec(txtPorcentaje.Text / 100) * dCantidad
            m_bRespetarPorcen = False
            
            tvwestrorg.selectedItem.Text = m_stexto & " (" & txtPorcentaje.Text & "%" & ")"
            Select Case Left(tvwestrorg.selectedItem.Tag, 4)
                Case "UON1"
                    If tvwestrorg.selectedItem.Image <> "UON1B" Then
                        tvwestrorg.selectedItem.Image = "UON1A"
                    End If
                Case "UON2"
                    If tvwestrorg.selectedItem.Image <> "UON2B" Then
                        tvwestrorg.selectedItem.Image = "UON2A"
                    End If
                Case "UON3"
                    If tvwestrorg.selectedItem.Image <> "UON3B" Then
                        tvwestrorg.selectedItem.Image = "UON3A"
                    End If
            End Select
            
            LimpiarRama tvwestrorg.selectedItem
            
            Distribuir nodx
            
            If Not bSinCantidad Then lblSinDistCantidad.caption = dSinCantidad
            lblSinDistPorcentaje.caption = Format(100 - CDec(dDistPorcen * 100), "Standard") & "%"
            If Not bSinCantidad Then lblCantidad.caption = Format(dDistCantidad, "Standard")
            lblPorcentaje.caption = DblToStr(CDec(dDistPorcen * 100)) & "%"
            If dDistCantidad > dCantidad Then
                txtPorcentaje.Text = Left(txtPorcentaje.Text, Len(txtPorcentaje.Text) - 1)
            End If
            
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "txtPorcentaje_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Function QuitarPorcentaje(ByVal sTexto As String) As String
Dim i As Integer
Dim sAux As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sAux = sTexto
    
    If sTexto = "" Then Exit Function
    
    i = Len(sTexto)
    
    If i = 1 Then Exit Function
    
    sTexto = Left(sTexto, Len(sTexto) - 1)
    
    While i > 1
        
        If Mid(sTexto, Len(sTexto), 1) <> "(" Then
            sTexto = Left(sTexto, Len(sTexto) - 1)
            i = i - 1
        Else
            sTexto = Left(sTexto, Len(sTexto) - 2)
            i = -1
        End If
    
    Wend
    
    If i = 0 Or i = 1 Then
        sTexto = sAux
    End If
    
    QuitarPorcentaje = sTexto
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "QuitarPorcentaje", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function DevolverPorcentaje(ByVal nod As MSComctlLib.node) As Double
Dim i As Integer
Dim dPorcen As Double
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nod Is Nothing Then Exit Function
    
    sCod = DevolverCodigoColeccion(nod)
    Select Case Left(nod.Tag, 4)
    Case "UON1"
        If oDistsNivel1.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = oDistsNivel1.Item(sCod).Porcentaje
        End If
    Case "UON2"
        If oDistsNivel2.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = oDistsNivel2.Item(sCod).Porcentaje
        End If
    Case "UON3"
        If oDistsNivel3.Item(sCod) Is Nothing Then
            dPorcen = 0
        Else
            dPorcen = oDistsNivel3.Item(sCod).Porcentaje
        End If
    End Select
    
    DevolverPorcentaje = dPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "DevolverPorcentaje", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub LimpiarRama(ByVal node As MSComctlLib.node, Optional ByVal bEliminar As Boolean)
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node

   'Primero hacia arriba
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nod1 = node.Parent
    
    While Not nod1 Is Nothing
        
        If Mid(nod1.Image, 5, 1) = "A" Or Mid(nod1.Image, 5, 1) = "B" Then
            nod1.Image = Mid(nod1.Image, 1, 4)
            nod1.Text = QuitarPorcentaje(nod1.Text)
            
            If bEliminar = True Then
                EliminarColeccion nod1
            End If
            
            Exit Sub
        Else
            Set nod1 = nod1.Parent
        End If
    
    Wend
    
    'Ahora hacia abajo
    
    Set nod1 = node.Child
    
    While Not nod1 Is Nothing
        
        If Mid(nod1.Image, 5, 1) = "A" Or Mid(nod1.Image, 5, 1) = "B" Then
            nod1.Image = Mid(nod1.Image, 1, 4)
            nod1.Text = QuitarPorcentaje(nod1.Text)
            
            If bEliminar = True Then
                EliminarColeccion nod1
            End If
            
        Else
            Set nod2 = nod1.Child
                
            While Not nod2 Is Nothing
                
                If Mid(nod2.Image, 5, 1) = "A" Or Mid(nod1.Image, 5, 1) = "B" Then
                    nod2.Image = Mid(nod2.Image, 1, 4)
                    nod2.Text = QuitarPorcentaje(nod2.Text)
                    
                    If bEliminar = True Then
                        EliminarColeccion nod2
                    End If
                    
                Else
                    Set nod3 = nod2.Child
                        
                    While Not nod3 Is Nothing
                        If Mid(nod3.Image, 5, 1) = "A" Or Mid(nod1.Image, 5, 1) = "B" Then
                            nod3.Image = Mid(nod3.Image, 1, 4)
                            nod3.Text = QuitarPorcentaje(nod3.Text)
                            
                            If bEliminar = True Then
                                EliminarColeccion nod3
                            End If
                        End If
                        Set nod3 = nod3.Next
                    Wend
                End If
                Set nod2 = nod2.Next
            Wend
        
        End If
        
        Set nod1 = nod1.Next
        
        Wend
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "LimpiarRama", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
''' <summary>
''' Configura la pantalla segun los datos
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cmdModif_Click, ttvwestrorg_NodeClick; Tiempo m�ximo:0,1</remarks>
''' <revision>EPB 24/11/2011</revision>
Private Sub ConfigurarInterfaz(ByVal node As MSComctlLib.node)
Dim dPorcen As Double
Dim iDistrib As Integer
Dim dDistrib As Double
Dim nodoHermano As MSComctlLib.node
Dim dPorcentajeHermano As Double
                    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    If node.Parent Is Nothing Then
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        Exit Sub
    End If
    
    If node Is Nothing Then Exit Sub
    If node.Parent Is Nothing Then Exit Sub
    
    m_stexto = node.Text
    If isBloqueado(node) Then
        picPorcen.Enabled = False
        Exit Sub
    End If
    'Configuramos seg�n nivel de de distribuci�n m�nimo
    If Not picNavigate.Visible Then
    
        Select Case gParametrosGenerales.giNIVDIST
            Case 2
                If Mid(node.Image, 4, 1) = "1" Then
                    picPorcen.Enabled = False
                    Exit Sub
                Else
                    If g_bModif Then picPorcen.Enabled = True
                End If
            Case 3
                If Mid(node.Image, 4, 1) = "1" Or Mid(node.Image, 4, 1) = "2" Then
                    picPorcen.Enabled = False
                    Exit Sub
                Else
                    If g_bModif Then picPorcen.Enabled = True
                End If
            Case Else
                If g_bModif Then picPorcen.Enabled = True
        End Select
        
'        'Confirguramos dependiendo de si est� restringido a su unidad organizativa
'        If m_bRuo Then
'            If node.Parent.Parent Is Nothing Then
'                'Ha seleccionado la de nivel1
'                'Tenemos que ver si la del usuario esta por encima de la seleccionada
'                If Trim(basOptimizacion.gUON2Usuario) = "" Then
'                    ' El usuario tiene permisos sobre la UON1 seleccionada
'                    If g_bModif Then picPorcen.Enabled = True
'                Else
'                    picPorcen.Enabled = False
'                    Exit Sub
'                End If
'            Else
'                If node.Parent.Parent.Parent Is Nothing Then
'                    'Ha seleccionado la de nivel2
'                    'Tenemos que ver si la del usuario esta por encima de la seleccionada
'                    If Trim(basOptimizacion.gUON3Usuario) = "" Then
'                        ' El usuario tiene permisos sobre la UON2 seleccionada
'                        If g_bModif Then picPorcen.Enabled = True
'                    Else
'                        picPorcen.Enabled = False
'                        Exit Sub
'                    End If
'                End If
'            End If
'        End If
            
    End If
    Slider1.Max = 100
    If node.Image = "UON1A" Or node.Image = "UON2A" Or node.Image = "UON3A" Or node.Image = "UON1B" Or node.Image = "UON2B" Or node.Image = "UON3B" Then
        m_bRespetarPorcen = True
        dPorcen = DevolverPorcentaje(node)
        m_bRespetarPorcen = True
        txtPorcentaje.Text = CDec(dPorcen * 100)
        m_bRespetarPorcen = True
        If txtPorcentaje.Text < 32767 Then
            Slider1.Value = Int(txtPorcentaje.Text)
        End If
        m_bRespetarPorcen = True
        If Not bSinCantidad Then txtCantidad.Text = dPorcen * dCantidad
        m_bRespetarPorcen = False
        If Slider1.Value = 0 Then
            If Int(100 - CDec(dDistPorcen * 100)) > 1 Then
                Slider1.Max = Int(100 - CDec(dDistPorcen * 100))
            Else
                Slider1.Max = 1
            End If
        Else
            Slider1.Max = Slider1.Value + Int(100 - CDec(dDistPorcen * 100))
        End If
    Else
        m_bRespetarPorcen = True
        txtPorcentaje.Text = ""
        m_bRespetarPorcen = True
        Slider1.Value = 0
        m_bRespetarPorcen = True
        txtCantidad.Text = ""
        m_bRespetarPorcen = False
        
        If g_bModoEdicion = True And m_bCargandoForm = False And node.Selected = True Then
            dDistrib = DevolverPorcentajePadreHijos(node)
            dDistPorcen = dDistPorcen - dDistrib
            
                        
            If dDistrib <> 0 Then
                LimpiarRama node, True
                'Y pone ese porcentaje en el nuevo nodo
                txtPorcentaje.Text = CDec(dDistrib * 100)
                If txtPorcentaje.Text < 32767 Then
                '    Slider1.Value = txtPorcentaje
                     Slider1.Max = IIf(Int(txtPorcentaje.Text) = 0, 1, Int(txtPorcentaje.Text))
                End If
            Else
            
                If CDbl(100 - CDec(dDistPorcen * 100)) > 0 Then
                    'Si no llega al 100% asigna lo que falta
                    txtPorcentaje.Text = CDbl(100 - CDec(dDistPorcen * 100))
                    If txtPorcentaje.Text < 32767 Then
                        Slider1.Max = txtPorcentaje.Text
                        Slider1.Value = Slider1.Max
                    End If
                Else
                    'Si hay m�s de un nodo asignado no hace nada.Si no lo asigna el 100% a este nodo en el que nos posicionamos.
                    iDistrib = 0
                    If Not oDistsNivel1 Is Nothing Then iDistrib = iDistrib + oDistsNivel1.Count
                    If Not oDistsNivel2 Is Nothing Then iDistrib = iDistrib + oDistsNivel2.Count
                    If Not oDistsNivel3 Is Nothing Then iDistrib = iDistrib + oDistsNivel3.Count
                    If iDistrib >= 2 Then   'Hay mas de un nodo asignado:no hace nada
                        'Si solo hay un nodo hermano asignado cogeremos su porcentaje y lo pondremos en el seleccionado
                        Set nodoHermano = Nothing
                        Set nodoHermano = SoloUnHermanoAsignado(node)
                        If Not nodoHermano Is Nothing Then
                            dPorcentajeHermano = CDec(DevolverPorcentaje(nodoHermano))
                            Slider1.Max = IIf(Int(CDec(dPorcentajeHermano * 100)) = 0, 1, Int(CDec(dPorcentajeHermano * 100)))
                            nodoHermano.Image = Mid(nodoHermano.Image, 1, 4)
                            nodoHermano.Text = QuitarPorcentaje(nodoHermano.Text)
                            dDistPorcen = dDistPorcen - dPorcentajeHermano
                            EliminarColeccion nodoHermano
                            Slider1.Value = Slider1.Max
                        Else
                            Slider1.Max = 1
                        End If
                        
                    Else  'Hay un nodo asignado al 100% o todav�a no hay nada distribuido: asigna el 100%
                        dDistPorcen = 0
                        LimpiarTodasLasRamas
                        Slider1.Max = 100
                        Slider1.Value = 100
                    End If
                End If
            End If
        Else
            If Int(100 - CDec(dDistPorcen * 100)) > 1 Then
                Slider1.Max = IIf(Int(100 - CDec(dDistPorcen * 100)) = 0, 1, Int(100 - CDec(dDistPorcen * 100)))
            Else
                Slider1.Max = 1
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "ConfigurarInterfaz", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub
Private Function SoloUnHermanoAsignado(ByVal node As MSComctlLib.node) As Object
Dim lCount As Long
Dim nod As MSComctlLib.node
Dim nodoHermano As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
lCount = 0


Set nod = node.FirstSibling
While Not nod Is Nothing
    If Mid(nod.Image, 5, 1) = "A" And nod.key <> node.key Then
        Set nodoHermano = Nothing
        Set nodoHermano = nod
        lCount = lCount + 1
    End If
    Set nod = nod.Next
Wend

    If lCount = 1 Then
        Set SoloUnHermanoAsignado = nodoHermano
    Else
        Set SoloUnHermanoAsignado = Nothing
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "SoloUnHermanoAsignado", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Private Function DevolverCodigoColeccion(ByVal nod As MSComctlLib.node) As String
Dim sCod As String
Dim scod1 As String
Dim scod2 As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sCod = Right(nod.Tag, Len(nod.Tag) - 4)
    Select Case Left(nod.Tag, 4)
    Case "UON1"
        sCod = sCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sCod))
    Case "UON2"
        scod1 = Right(nod.Parent.Tag, Len(nod.Parent.Tag) - 4)
        scod2 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(scod1))
        sCod = scod2 & sCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sCod))
    Case "UON3"
        scod1 = Right(nod.Parent.Parent.Tag, Len(nod.Parent.Parent.Tag) - 4)
        scod2 = Right(nod.Parent.Tag, Len(nod.Parent.Tag) - 4)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(scod1))
        scod2 = scod1 & scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(scod2))
        sCod = scod2 & sCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sCod))
    
    End Select
    DevolverCodigoColeccion = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "DevolverCodigoColeccion", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Sub Distribuir(nodo As MSComctlLib.node)
Dim sCod As String
Dim dPorcen As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nodo Is Nothing Then Exit Sub
            
    sCod = DevolverCodigoColeccion(nodo)
    
    If Trim(txtPorcentaje.Text) = "" Then
        dPorcen = 0
    Else
        dPorcen = txtPorcentaje.Text
    End If
    
    Select Case Left(nodo.Tag, 4)
    Case "UON1"
        ' Si est� distribu�do UON1
        If oDistsNivel1.Item(sCod) Is Nothing Then
            oDistsNivel1.Add g_oItem, Right(nodo.Tag, Len(nodo.Tag) - 4), CDec(dPorcen / 100)
            dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistPorcen = dDistPorcen - oDistsNivel1.Item(sCod).Porcentaje
            If dPorcen = 0 Then
                oDistsNivel1.Remove sCod
            Else
                oDistsNivel1.Item(sCod).Porcentaje = CDec(dPorcen / 100)
                dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            End If
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        End If
        
    Case "UON2"
                        
        If oDistsNivel2.Item(sCod) Is Nothing Then
            oDistsNivel2.Add g_oItem, Right(nodo.Parent.Tag, Len(nodo.Parent.Tag) - 4), Right(nodo.Tag, Len(nodo.Tag) - 4), CDec(dPorcen / 100)
            dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistPorcen = dDistPorcen - oDistsNivel2.Item(sCod).Porcentaje
            If dPorcen = 0 Then
                oDistsNivel2.Remove sCod
            Else
                oDistsNivel2.Item(sCod).Porcentaje = CDec(dPorcen / 100)
                dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            End If
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
            
        End If
    
    Case "UON3"
        If oDistsNivel3.Item(sCod) Is Nothing Then
            oDistsNivel3.Add g_oItem, Right(nodo.Parent.Parent.Tag, Len(nodo.Parent.Parent.Tag) - 4), Right(nodo.Parent.Tag, Len(nodo.Parent.Tag) - 4), Right(nodo.Tag, Len(nodo.Tag) - 4), CDec(dPorcen / 100)
            dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistPorcen = dDistPorcen - oDistsNivel3.Item(sCod).Porcentaje
            If dPorcen = 0 Then
                oDistsNivel3.Remove sCod
            Else
                oDistsNivel3.Item(sCod).Porcentaje = CDec(dPorcen / 100)
                dDistPorcen = dDistPorcen + CDec(dPorcen / 100)
            End If
            dDistCantidad = CDec(dDistPorcen * dCantidad)
            dSinCantidad = dCantidad - dDistCantidad
        End If
    
    End Select
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "Distribuir", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub DistribuirCant(nodo As MSComctlLib.node)
Dim sCod As String
Dim dCant As Double
Dim dPorcen As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nodo Is Nothing Then Exit Sub
            
    sCod = DevolverCodigoColeccion(nodo)
    
    If Trim(txtCantidad.Text) = "" Then
        dCant = 0
    Else
        dCant = CDbl(txtCantidad.Text)
    End If
    dPorcen = dCant / dCantidad
    
    Select Case Left(nodo.Tag, 4)
    Case "UON1"
        ' Si est� distribu�do UON1
        If oDistsNivel1.Item(sCod) Is Nothing Then
            oDistsNivel1.Add g_oItem, Right(nodo.Tag, Len(nodo.Tag) - 4), dPorcen
            dDistCantidad = dDistCantidad + dCant
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistCantidad = dDistCantidad - CDec(oDistsNivel1.Item(sCod).Porcentaje * dCantidad)
            If dCant = 0 Then
                oDistsNivel1.Remove sCod
            Else
                oDistsNivel1.Item(sCod).Porcentaje = dPorcen
                dDistCantidad = dDistCantidad + dCant
            End If
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        End If
        
    Case "UON2"
                        
        If oDistsNivel2.Item(sCod) Is Nothing Then
            oDistsNivel2.Add g_oItem, Right(nodo.Parent.Tag, Len(nodo.Parent.Tag) - 4), Right(nodo.Tag, Len(nodo.Tag) - 4), dPorcen
            dDistCantidad = dDistCantidad + dCant
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistCantidad = dDistCantidad - CDec(oDistsNivel2.Item(sCod).Porcentaje * dCantidad)
            If dCant = 0 Then
                oDistsNivel2.Remove sCod
            Else
                oDistsNivel2.Item(sCod).Porcentaje = dPorcen
                dDistCantidad = dDistCantidad + dCant
            End If
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        End If
    
    Case "UON3"
        If oDistsNivel3.Item(sCod) Is Nothing Then
            oDistsNivel3.Add g_oItem, Right(nodo.Parent.Parent.Tag, Len(nodo.Parent.Parent.Tag) - 4), Right(nodo.Parent.Tag, Len(nodo.Parent.Tag) - 4), Right(nodo.Tag, Len(nodo.Tag) - 4), dPorcen
            dDistCantidad = dDistCantidad + dCant
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        Else
            dDistCantidad = dDistCantidad - CDec(oDistsNivel3.Item(sCod).Porcentaje * dCantidad)
            If dCant = 0 Then
                oDistsNivel3.Remove sCod
            Else
                oDistsNivel3.Item(sCod).Porcentaje = dPorcen
                dDistCantidad = dDistCantidad + dCant
            End If
            dDistPorcen = dDistCantidad / dCantidad
            dSinCantidad = dCantidad - dDistCantidad
        End If
    
    End Select
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "DistribuirCant", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


Private Sub LimpiarTodasLasRamas()
    Dim nodx As MSComctlLib.node
    Dim oDistNivel1 As CDistItemNivel1
    Dim oDistNivel2 As CDistItemNivel2
    Dim oDistNivel3 As CDistItemNivel3
    Dim sCod As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    
    'Limpia todas las ramas con distribuciones:
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each oDistNivel1 In oDistsNivel1
        Set nodx = Nothing
        scod1 = oDistNivel1.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel1.CodUON1))
        Set nodx = tvwestrorg.Nodes("UON1" & scod1)
        LimpiarRama nodx
        nodx.Image = Mid(nodx.Image, 1, 4)
        nodx.Text = QuitarPorcentaje(nodx.Text)
        sCod = DevolverCodigoColeccion(nodx)
        oDistsNivel1.Remove sCod
    Next
    
    For Each oDistNivel2 In oDistsNivel2
        Set nodx = Nothing
        scod1 = oDistNivel2.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel2.CodUON1))
        scod2 = oDistNivel2.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel2.CodUON2))
        Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2)
        LimpiarRama nodx
        nodx.Image = Mid(nodx.Image, 1, 4)
        nodx.Text = QuitarPorcentaje(nodx.Text)
        sCod = DevolverCodigoColeccion(nodx)
        oDistsNivel2.Remove sCod
    Next
    
    For Each oDistNivel3 In oDistsNivel3
        Set nodx = Nothing
        scod1 = oDistNivel3.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDistNivel3.CodUON1))
        scod2 = oDistNivel3.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDistNivel3.CodUON2))
        scod3 = oDistNivel3.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDistNivel3.CodUON3))
        Set nodx = tvwestrorg.Nodes("UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3)
        LimpiarRama nodx
        nodx.Image = Mid(nodx.Image, 1, 4)
        nodx.Text = QuitarPorcentaje(nodx.Text)
        sCod = DevolverCodigoColeccion(nodx)
        oDistsNivel3.Remove sCod
        
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "LimpiarTodasLasRamas", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function DevolverPorcentajePadreHijos(ByVal nod As MSComctlLib.node) As Double
Dim i As Integer
Dim dPorcen As Double
Dim sCod As String
Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If nod Is Nothing Then Exit Function
    
    dPorcen = 0
    
    'Primero hacia arriba
    Set nod1 = nod.Parent
    
    While Not nod1 Is Nothing
        If Mid(nod1.Image, 5, 1) = "A" Then
            dPorcen = dPorcen + DevolverPorcentaje(nod1)
            DevolverPorcentajePadreHijos = dPorcen
            Exit Function
        Else
            Set nod1 = nod1.Parent
        End If
    Wend
    
    'Ahora hacia abajo
    
    Set nod1 = nod.Child
    
    While Not nod1 Is Nothing
        
        If Mid(nod1.Image, 5, 1) = "A" Then
            dPorcen = dPorcen + DevolverPorcentaje(nod1)
        Else
            Set nod2 = nod1.Child
                
            While Not nod2 Is Nothing
                
                If Mid(nod2.Image, 5, 1) = "A" Then
                    dPorcen = dPorcen + DevolverPorcentaje(nod2)
                Else
                    Set nod3 = nod2.Child
                        
                    While Not nod3 Is Nothing
                        If Mid(nod3.Image, 5, 1) = "A" Then
                            dPorcen = dPorcen + DevolverPorcentaje(nod3)
                        End If
                        Set nod3 = nod3.Next
                    Wend
                End If
                Set nod2 = nod2.Next
            Wend
        
        End If
        
        Set nod1 = nod1.Next
        
    Wend
    
    Set nod1 = Nothing
    Set nod2 = Nothing
    Set nod3 = Nothing
    
    DevolverPorcentajePadreHijos = dPorcen
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "DevolverPorcentajePadreHijos", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub EliminarColeccion(ByVal nod As MSComctlLib.node)
    Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sCod = DevolverCodigoColeccion(nod)
    
    If Not oDistsNivel1.Item(sCod) Is Nothing Then
        oDistsNivel1.Remove sCod
    ElseIf Not oDistsNivel2.Item(sCod) Is Nothing Then
        oDistsNivel2.Remove sCod
    ElseIf Not oDistsNivel3.Item(sCod) Is Nothing Then
        oDistsNivel3.Remove sCod
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "EliminarColeccion", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub bloquearNodo(ByRef nodo As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    nodo.Forecolor = &HC0C0C0
    nodo.Bold = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "bloquearNodo", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function isBloqueado(ByRef nodo As MSComctlLib.node)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    isBloqueado = nodo.Bold
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemDist", "isBloqueado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
